module liboci {
	exports com.broadsoft.oci;
	exports com.broadsoft.ocir;
	exports de.qsc.broadsoft.oci;
	exports de.qsc.broadsoft.oci.coding;
	exports de.qsc.broadsoft.oci.transport;
	
	opens com.broadsoft.oci to java.xml.bind;
	opens com.broadsoft.ocir to java.xml.bind;

	requires java.rmi;
	requires java.xml;
	requires java.xml.bind;
	requires org.apache.logging.log4j;
}