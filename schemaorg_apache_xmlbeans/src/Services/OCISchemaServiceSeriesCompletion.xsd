<?xml version="1.0" encoding="UTF-8"?>

<!-- ************************************************************************* -->
<!-- Copyright (c) 2004-2005 Broadsoft, Inc.  All rights reserved.             -->
<!-- ************************************************************************* -->
<!-- O C I   X M L   S C H E M A  :  S E R V I C E   P A R T                   -->
<!--                                                                           -->
<!-- S E R I E S   C O M P L E T I O N   S E R V I C E                         -->
<!--                                                                           -->
<!-- This file defines the XML Schema for the BroadSoft Application Server     -->
<!-- Open Client Interface (OCI).                                              -->
<!-- ************************************************************************* -->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:core="C" attributeFormDefault="qualified" elementFormDefault="qualified">

  <xs:import namespace="C" schemaLocation="../OCISchemaBASE.xsd"/>

  <xs:include schemaLocation="../OCISchemaDataTypes.xsd"/>
  <xs:include schemaLocation="../OCISchemaSearchCriteria.xsd"/>

  <!-- ****************************************************************************************** -->
  <!-- S E R I E S   C O M P L E T I O N   R E Q U E S T S   A N D   R E S P O N S E S            -->
  <!-- ****************************************************************************************** -->
  <!--
  Requests and responses are listed here in alphabetical order.
  The non-primitive attributes inside the commands are defined in another
  section of the schema.

  Requests in this schema file:
    GroupSeriesCompletionAddInstanceRequest
    GroupSeriesCompletionDeleteInstanceRequest
    GroupSeriesCompletionGetAvailableUserListRequest
    GroupSeriesCompletionGetInstanceListRequest
    GroupSeriesCompletionGetInstanceRequest
    GroupSeriesCompletionModifyInstanceRequest
    UserSeriesCompletionGetRequest
  -->

  <xs:complexType name="GroupSeriesCompletionAddInstanceRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_e863ad2a774e42eb9191de2bdaff913e"/>
      </xs:appinfo>
      <xs:documentation>
        Adds a Series Completion group.
        The response is either SuccessResponse or ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="serviceProviderId" type="ServiceProviderId"/>
          <xs:element name="groupId" type="GroupId"/>
          <xs:element name="name" type="ServiceInstanceName"/>
          <xs:element name="userId" type="UserId" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="GroupSeriesCompletionDeleteInstanceRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_5adad3092bf74f8c86c3c2e028e68157"/>
      </xs:appinfo>
      <xs:documentation>
        Deletes a Series Completion group.
        The response is either SuccessResponse or ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="serviceProviderId" type="ServiceProviderId"/>
          <xs:element name="groupId" type="GroupId"/>
          <xs:element name="name" type="ServiceInstanceName"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="GroupSeriesCompletionGetAvailableUserListRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_46fd971baf01477fafa60e93d2db06ee"/>
      </xs:appinfo>
      <xs:documentation>
        Get a list of users that can be assigned to a Series Completion group.
        The available user list for a new Series Completion group can be obtained
        by not setting the name.
        The response is either GroupSeriesCompletionGetAvailableUserListResponse or ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="serviceProviderId" type="ServiceProviderId"/>
          <xs:element name="groupId" type="GroupId"/>
          <xs:element name="name" type="ServiceInstanceName" minOccurs="0"/>
          <xs:element name="responseSizeLimit" type="ResponseSizeLimit" minOccurs="0"/>
          <xs:element name="searchCriteriaUserLastName" type="SearchCriteriaUserLastName" minOccurs="0" maxOccurs="unbounded"/>
          <xs:element name="searchCriteriaUserFirstName" type="SearchCriteriaUserFirstName" minOccurs="0" maxOccurs="unbounded"/>
          <xs:element name="searchCriteriaExactUserDepartment" type="SearchCriteriaExactUserDepartment" minOccurs="0"/>
          <xs:element name="searchCriteriaUserId" type="SearchCriteriaUserId" minOccurs="0" maxOccurs="unbounded"/>
          <xs:element name="searchCriteriaDn" type="SearchCriteriaDn" minOccurs="0" maxOccurs="unbounded"/>
          <xs:element name="searchCriteriaExtension" type="SearchCriteriaExtension" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="GroupSeriesCompletionGetAvailableUserListResponse">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_1a85eb22faf9480a93f1825aa497aa38"/>
      </xs:appinfo>
      <xs:documentation>
        Response to the GroupSeriesCompletionGetAvailableUserListRequest.
        Contains a table with column headings: "User Id", "Last Name", "First Name", "Hiragana Last Name", "Hiragana First Name",
        "Phone Number", "Extension", "Department", "Email Address".
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIDataResponse">
        <xs:sequence>
          <xs:element name="userTable" type="core:OCITable"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="GroupSeriesCompletionGetInstanceListRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_a825030e130f45a48e841e77c04d053d"/>
      </xs:appinfo>
      <xs:documentation>
        Request to get a list of Series Completion instances within a group.
        The response is either GroupSeriesCompletionGetInstanceListResponse or ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="serviceProviderId" type="ServiceProviderId"/>
          <xs:element name="groupId" type="GroupId"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="GroupSeriesCompletionGetInstanceListResponse">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_efe0cdf1a95743f495ff33720b77bd21"/>
      </xs:appinfo>
      <xs:documentation>
        Response to the GroupSeriesCompletionGetInstanceListRequest.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIDataResponse">
        <xs:sequence>
          <xs:element name="name" type="ServiceInstanceName" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="GroupSeriesCompletionGetInstanceRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_6a936dc60fe2499da1da8ace10881bd3"/>
      </xs:appinfo>
      <xs:documentation>
        Gets a Series Completion group.
        The response is either GroupSeriesCompletionGetInstanceResponse or ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="serviceProviderId" type="ServiceProviderId"/>
          <xs:element name="groupId" type="GroupId"/>
          <xs:element name="name" type="ServiceInstanceName"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="GroupSeriesCompletionGetInstanceResponse">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_18fec44bfa7b47da9aea6ecfae3c6848"/>
      </xs:appinfo>
      <xs:documentation>
        Response to the GroupSeriesCompletionGetInstanceRequest.
        Contains a table with column headings: "User Id", "Last Name", "First Name", "Hiragana Last Name", "Hiragana First Name",
        "Phone Number", "Extension", "Department", "Email Address".
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIDataResponse">
        <xs:sequence>
          <xs:element name="userTable" type="core:OCITable"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="GroupSeriesCompletionModifyInstanceRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_36c830f9cc5246149f42076e01d05cc4"/>
      </xs:appinfo>
      <xs:documentation>
        Modifies a Series Completion group. Replaces the entire list of users in the group.
        The response is either SuccessResponse or ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="serviceProviderId" type="ServiceProviderId"/>
          <xs:element name="groupId" type="GroupId"/>
          <xs:element name="name" type="ServiceInstanceName"/>
          <xs:element name="newName" type="ServiceInstanceName" minOccurs="0"/>
          <xs:element name="userIdList" type="ReplacementUserIdList" nillable="true" minOccurs="0"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="UserSeriesCompletionGetRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_d1aa0b20c6a04445ac61b3f8f16f5830"/>
      </xs:appinfo>
      <xs:documentation>
        Gets the details of the Series Completion group that a user belongs to (if any).
        Any user can only belong to one Series Completion group.
        The response is either UserSeriesCompletionGetResponse or ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="userId" type="UserId"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="UserSeriesCompletionGetResponse">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_bb0185a144224e46a50628c030f91d12"/>
      </xs:appinfo>
      <xs:documentation>
        Response to the UserSeriesCompletionGetRequest.
        Identifies which Series Completion group the user belongs to and the list of users in the group.
        Contains a table with column headings: "User Id", "Last Name", "First Name", "Hiragana Last Name", 
        "Hiragana First Name", "Department", "Phone Number", "Extension", "Email Address".
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIDataResponse">
        <xs:sequence>
          <xs:element name="name" type="ServiceInstanceName" minOccurs="0"/>
          <xs:element name="userTable" type="core:OCITable"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <!-- ******************************************************************** -->
  <!-- M E S S A G E   P A R A M E T E R S                                  -->
  <!-- ******************************************************************** -->
  <!--
  The Series Completion specific non-primitive attributes are listed here in alphabetical order.
  -->

</xs:schema>
