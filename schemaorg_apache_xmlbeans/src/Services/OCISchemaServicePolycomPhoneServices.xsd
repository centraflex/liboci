<?xml version="1.0" encoding="UTF-8"?>

<!-- ************************************************************************* -->
<!-- Copyright (c) 2004-2005 Broadsoft, Inc.  All rights reserved.             -->
<!-- ************************************************************************* -->
<!-- O C I   X M L   S C H E M A  :  S E R V I C E   P A R T                   -->
<!--                                                                           -->
<!-- P O L Y C O M   P H O N E   S E R V I C E S   S E R V I C E               -->
<!--                                                                           -->
<!-- This file defines the XML Schema for the BroadSoft Application Server     -->
<!-- Open Client Interface (OCI).                                              -->
<!-- ************************************************************************* -->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:core="C" attributeFormDefault="qualified" elementFormDefault="qualified">

  <xs:import namespace="C" schemaLocation="../OCISchemaBASE.xsd"/>

  <xs:include schemaLocation="../OCISchemaDataTypes.xsd"/>

  <!-- ************************************************************************************* -->
  <!-- P O L Y C O M   P H O N E   S E R V I C E S                                           -->
  <!-- R E Q U E S T S   A N D   R E S P O N S E S                                           -->
  <!-- ************************************************************************************* -->
  <!--
  Requests and responses are listed here in alphabetical order.
  The non-primitive attributes inside the commands are defined in another
  section of the schema.

  Requests in this schema file:
    GroupPolycomPhoneServicesGetRequest
    GroupPolycomPhoneServicesGetResponse
    GroupPolycomPhoneServicesModifyRequest
    UserPolycomPhoneServicesGetRequest
    UserPolycomPhoneServicesGetResponse
    UserPolycomPhoneServicesModifyRequest
    UserPolycomPhoneServicesGetPrimaryEndpointListRequest
    UserPolycomPhoneServicesGetPrimaryEndpointListResponse
  -->

  <xs:complexType name="GroupPolycomPhoneServicesGetRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_4a8791b95991402480ac0c9d89fcf412"/>
      </xs:appinfo>
      <xs:documentation>
        Request the group's Polycom Phone Services attributes.
        The response is either a GroupPolycomPhoneServicesGetResponse or an ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="serviceProviderId" type="ServiceProviderId"/>
          <xs:element name="groupId" type="GroupId"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="GroupPolycomPhoneServicesGetResponse">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_ac3c9209afb84583b86ebaaa07baff85"/>
      </xs:appinfo>
      <xs:documentation>
        Response to GroupPolycomPhoneServicesGetRequest.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIDataResponse">
        <xs:sequence>
          <xs:element name="includeGroupCommonPhoneListInDirectory" type="xs:boolean"/>
          <xs:element name="includeGroupCustomContactDirectoryInDirectory" type="xs:boolean"/>
          <xs:element name="groupCustomContactDirectory" type="CustomContactDirectoryName" minOccurs="0"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="GroupPolycomPhoneServicesModifyRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_71c1359f563f46589ea5413f00255422"/>
      </xs:appinfo>
      <xs:documentation>
        Modify the group's Polycom Phone Services attributes.
        The response is either a SuccessResponse or an ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="serviceProviderId" type="ServiceProviderId"/>
          <xs:element name="groupId" type="GroupId"/>
          <xs:element name="includeGroupCommonPhoneListInDirectory" type="xs:boolean" minOccurs="0"/>
          <xs:element name="includeGroupCustomContactDirectoryInDirectory" type="xs:boolean" minOccurs="0"/>
          <xs:element name="groupCustomContactDirectory" type="CustomContactDirectoryName" minOccurs="0" nillable="true"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="UserPolycomPhoneServicesGetRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_ebbeb8ba647742e5b0426e2fc8a6a79a"/>
      </xs:appinfo>
      <xs:documentation>
        Request the user's Polycom Phone Services attributes.
        The response is either a UserPolycomPhoneServicesGetResponse or an ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="userId" type="UserId"/>
          <xs:element name="accessDevice" type="AccessDevice"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="UserPolycomPhoneServicesGetResponse">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_1fba9c3cee3c4e2aa46781abc884f61b"/>
      </xs:appinfo>
      <xs:documentation>
        Response to UserPolycomPhoneServicesGetRequest.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIDataResponse">
        <xs:sequence>
          <xs:element name="integratePhoneDirectoryWithBroadWorks" type="xs:boolean"/>
          <xs:element name="includeUserPersonalPhoneListInDirectory" type="xs:boolean"/>
          <xs:element name="includeGroupCustomContactDirectoryInDirectory" type="xs:boolean"/>
          <xs:element name="groupCustomContactDirectory" type="CustomContactDirectoryName" minOccurs="0"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="UserPolycomPhoneServicesModifyRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_a288bf2bf23b4cdb9142d1fefff7d795"/>
      </xs:appinfo>
      <xs:documentation>
        Modify the user's Polycom Phone Services attributes.
        The response is either a SuccessResponse or an ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="userId" type="UserId"/>
          <xs:element name="accessDevice" type="AccessDevice"/>
          <xs:element name="integratePhoneDirectoryWithBroadWorks" type="xs:boolean" minOccurs="0"/>
          <xs:element name="includeUserPersonalPhoneListInDirectory" type="xs:boolean" minOccurs="0"/>
          <xs:element name="includeGroupCustomContactDirectoryInDirectory" type="xs:boolean" minOccurs="0"/>
          <xs:element name="groupCustomContactDirectory" type="CustomContactDirectoryName" minOccurs="0" nillable="true"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="UserPolycomPhoneServicesGetPrimaryEndpointListRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_fb1ac8e2619247eb88850d9904a21884"/>
      </xs:appinfo>
      <xs:documentation>
        Request the user's list of device profiles on which the user is the primary user.
        The response is either a UserPolycomPhoneServicesGetPrimaryEndpointListResponse or an ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="userId" type="UserId"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="UserPolycomPhoneServicesGetPrimaryEndpointListResponse">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_8fcad5477e5c4938a35abf7ab4d1f5e3"/>
      </xs:appinfo>
      <xs:documentation>
        Response to UserPolycomPhoneServicesGetPrimaryEndpointListRequest.
        The column headings for the deviceUserTable are: "Device Level", "Device Name", "Line/Port",  "Private Identity".
        The Private Identity column is empty is AS mode.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIDataResponse">
        <xs:sequence>
          <xs:element name="deviceUserTable" type="core:OCITable"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <!-- ******************************************************************** -->
  <!-- M E S S A G E   P A R A M E T E R S                                  -->
  <!-- ******************************************************************** -->
  <!--
  The polycom phone services specific non-primitive attributes are listed here in alphabetical order.
  -->

</xs:schema>
