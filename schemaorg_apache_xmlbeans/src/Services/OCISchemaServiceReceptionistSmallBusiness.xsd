<?xml version="1.0" encoding="UTF-8"?>

<!-- ************************************************************************* -->
<!-- Copyright (c) 2004-2005 Broadsoft, Inc.  All rights reserved.             -->
<!-- ************************************************************************* -->
<!-- O C I   X M L   S C H E M A  :  S E R V I C E   P A R T                   -->
<!--                                                                           -->
<!-- R E C E P T I O N I S T  S M A L L  B U S I N E S S   S E R V I C E       -->
<!--                                                                           -->
<!-- This file defines the XML Schema for the BroadSoft Application Server     -->
<!-- Open Client Interface (OCI).                                              -->
<!-- ************************************************************************* -->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:core="C" attributeFormDefault="qualified" elementFormDefault="qualified">

  <xs:import namespace="C" schemaLocation="../OCISchemaBASE.xsd"/>

  <xs:include schemaLocation="../OCISchemaDataTypes.xsd"/>
  <xs:include schemaLocation="../OCISchemaSearchCriteria.xsd"/>

  <!-- ************************************************************************************************* -->
  <!-- R E C E P T I O N I S T  S M A L L  B U S I N E S S   R E Q U E S T S   A N D   R E S P O N S E S -->
  <!-- ************************************************************************************************* -->
  <!--
  Requests and responses are listed here in alphabetical order.
  The non-primitive attributes inside the commands are defined in another
  section of the schema.

  Requests in this schema file:
    UserBroadWorksReceptionistSmallBusinessGetAvailableUserListRequest
    UserBroadWorksReceptionistSmallBusinessGetRequest
    UserBroadWorksReceptionistSmallBusinessModifyRequest
  -->

  <xs:complexType name="UserBroadWorksReceptionistSmallBusinessGetAvailableUserListRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_16d3af578ac0426994df6c625ac6859f"/>
      </xs:appinfo>
      <xs:documentation>
        Get a list of available users for the Receptionist Small Business service.
        The response is either UserBroadWorksReceptionistSmallBusinessGetAvailableUserListResponse or ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="userId" type="UserId"/>
          <xs:element name="responseSizeLimit" type="ResponseSizeLimit" minOccurs="0"/>
          <xs:element name="searchCriteriaUserLastName" type="SearchCriteriaUserLastName" minOccurs="0" maxOccurs="unbounded"/>
          <xs:element name="searchCriteriaUserFirstName" type="SearchCriteriaUserFirstName" minOccurs="0" maxOccurs="unbounded"/>
          <xs:element name="searchCriteriaExactUserDepartment" type="SearchCriteriaExactUserDepartment" minOccurs="0"/>
          <xs:element name="searchCriteriaExactUserGroup" type="SearchCriteriaExactUserGroup" minOccurs="0"/>
          <xs:element name="searchCriteriaUserId" type="SearchCriteriaUserId" minOccurs="0" maxOccurs="unbounded"/>
          <xs:element name="searchCriteriaDn" type="SearchCriteriaDn" minOccurs="0" maxOccurs="unbounded"/>
          <xs:element name="searchCriteriaExtension" type="SearchCriteriaExtension" minOccurs="0" maxOccurs="unbounded"/>
          <xs:element name="searchCriteriaImpId" type="SearchCriteriaImpId" minOccurs="0" maxOccurs="unbounded"/> 
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="UserBroadWorksReceptionistSmallBusinessGetAvailableUserListResponse">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_a3625aa2f23d439298ba6f24745b3ac0"/>
      </xs:appinfo>
      <xs:documentation>
        Response to the UserBroadWorksReceptionistSmallBusinessGetAvailableUserListRequest.
        Returns a 11 column table with column headings:
          "User Id", "Last Name", "First Name", "Hiragana Last Name", "Hiragana First Name", 
          "Group Id", "Phone Number", "Extension", "Mobile", "Email Address", "Department", "IMP Id".
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIDataResponse">
        <xs:sequence>
          <xs:element name="userTable" type="core:OCITable"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="UserBroadWorksReceptionistSmallBusinessGetRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_3e4b65ee32a145ab8c297dbd41fb2980"/>
      </xs:appinfo>
      <xs:documentation>
        Request the Receptionist Small Business parameters.
        The response is either a UserBroadWorksReceptionistSmallBusinessGetResponse or an ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="userId" type="UserId"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="UserBroadWorksReceptionistSmallBusinessGetResponse">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_28c16413e6e842d0972adf6a97ac5758"/>
      </xs:appinfo>
      <xs:documentation>
        Response to UserBroadWorksReceptionistSmallBusinessGetRequest.
        Returns a 12 column table with column headings:
          "User Id", "Last Name", "First Name", "Hiragana Last Name", "Hiragana First Name", 
          "Group Id", "Phone Number", "Extension", "Mobile", "Email Address", "Department", "IMP Id".
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIDataResponse">
        <xs:sequence>
          <xs:element name="monitoredUserTable" type="core:OCITable"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="UserBroadWorksReceptionistSmallBusinessModifyRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_120902c6911f4c0eb08186f4291f0328"/>
      </xs:appinfo>
      <xs:documentation>
        Replace the Receptionist Small Business monitored user list.
        The response is either a SuccessResponse or an ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="userId" type="UserId"/>
          <xs:element name="monitoredUserIdList" type="ReplacementUserIdList" nillable="true" minOccurs="0"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

</xs:schema>
