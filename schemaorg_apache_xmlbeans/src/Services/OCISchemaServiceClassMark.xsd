<?xml version="1.0" encoding="UTF-8"?>

<!-- ************************************************************************* -->
<!-- Copyright (c) 2004-2005 Broadsoft, Inc.  All rights reserved.             -->
<!-- ************************************************************************* -->
<!-- O C I   X M L   S C H E M A  :  S E R V I C E   P A R T                   -->
<!--                                                                           -->
<!-- C L A S S M A R K   S E R V I C E               -->
<!--                                                                           -->
<!-- This file defines the XML Schema for the BroadSoft Application Server     -->
<!-- Open Client Interface (OCI).                                              -->
<!-- ************************************************************************* -->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:core="C" attributeFormDefault="qualified" elementFormDefault="qualified">

  <xs:import namespace="C" schemaLocation="../OCISchemaBASE.xsd"/>

  <xs:include schemaLocation="../OCISchemaDataTypes.xsd"/>
  <xs:include schemaLocation="../OCISchemaSearchCriteria.xsd"/>

  <!-- ***************************************************************************************** -->
  <!-- C L A S S M A R K   R E Q U E S T S   A N D   R E S P O N S E S -->
  <!-- ***************************************************************************************** -->
  <!--
  Requests and responses are listed here in alphabetical order.
  The non-primitive attributes inside the commands are defined in another
  section of the schema.

  Requests in this schema file:
    SystemClassmarkAddRequest
    SystemClassmarkDeleteRequest
    SystemClassmarkGetListRequest  
    SystemClassmarkUtilizationGetListRequest  
    SystemClassmarkModifyRequest
    UserClassmarkGetRequest
    UserClassmarkModifyRequest
  -->

  <xs:complexType name="SystemClassmarkAddRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_5d2e4e1735e6473382ee0ca277223dd5"/>
      </xs:appinfo>
      <xs:documentation>
        Add a Class Mark to system.
        The response is either a SuccessResponse or an ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="classmark" type="Classmark"/>
          <xs:element name="value" type="ClassmarkValue"/>
          <xs:element name="webDisplayKey" type="WebDisplayKey" minOccurs="0"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="SystemClassmarkDeleteRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_aa985778c147471788aefa3fc74058c1"/>
      </xs:appinfo>
      <xs:documentation>
        Delete a Class Mark from system. It cannot be deleted if it is in use by any users.
        The response is either a SuccessResponse or an ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="classmark" type="Classmark"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="SystemClassmarkGetListRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_1e048542109d4b3e83e75345ee6bb083"/>
      </xs:appinfo>
      <xs:documentation>
        Get the list of all Class Mark in system.
        The response is either a SystemClassmarkGetListResponse or an ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  
  <xs:complexType name="SystemClassmarkGetListResponse">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_abad9adefdd44418bbbb1dfcf6ac7ac2"/>
      </xs:appinfo>
      <xs:documentation>
        Response to SystemClassmarkGetListRequest. 
        Contains a table of with the column headings: "Class Mark", "Value" and "Web Display Key". 
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIDataResponse">
        <xs:sequence>
          <xs:element name="classmarkTable" type="core:OCITable"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="SystemClassmarkModifyRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_f70c74e0e81645f6943c0b13b8f373b8"/>
      </xs:appinfo>
      <xs:documentation>
        Modify a Class Mark in system.
        The response is either a SuccessResponse or an ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="classmark" type="Classmark"/>
          <xs:element name="value" type="ClassmarkValue" minOccurs="0"/>
          <xs:element name="webDisplayKey" type="WebDisplayKey" nillable="true" minOccurs="0"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>
  
  <xs:complexType name="SystemClassmarkGetUtilizationListRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_583ae70b05f44370b9386d6068251326"/>
      </xs:appinfo>
      <xs:documentation>
        Get the list of all users associated with a Class Mark in system. It is possible to search by various criteria.
        Multiple search criteria are logically ANDed together.
        The response is either a SystemClassmarkGetUtilizationListResponse or an ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="classmark" type="Classmark"/>
          <xs:element name="responseSizeLimit" type="ResponseSizeLimit" minOccurs="0"/>
          <xs:element name="searchCriteriaGroupId" type="SearchCriteriaGroupId" minOccurs="0" maxOccurs="unbounded"/>
          <xs:element name="searchCriteriaExactServiceProviderId" type="SearchCriteriaExactServiceProvider" minOccurs="0"/>
          <xs:element name="searchCriteriaUserFirstName" type="SearchCriteriaUserFirstName" minOccurs="0" maxOccurs="unbounded"/>
          <xs:element name="searchCriteriaUserLastName" type="SearchCriteriaUserLastName" minOccurs="0" maxOccurs="unbounded"/>
          <xs:element name="searchCriteriaDn" type="SearchCriteriaDn" minOccurs="0" maxOccurs="unbounded"/>
          <xs:element name="searchCriteriaUserId" type="SearchCriteriaUserId" minOccurs="0" maxOccurs="unbounded"/>          
          <xs:element name="searchCriteriaExtension" type="SearchCriteriaExtension" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="SystemClassmarkGetUtilizationListResponse">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_aa45dcdffe744d8ca4ae90fb6c5d9135"/>
      </xs:appinfo>
      <xs:documentation>
        Response to SystemClassmarkGetUtilizationListRequest. 
        Contains a table with the column headings: "User Id", "Group Id", "Service Provider Id",
        "Last Name", "First Name", and "Phone Number", "Extension", "Department", "Email Address". 
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIDataResponse">
        <xs:sequence>
          <xs:element name="classmarkUserTable" type="core:OCITable"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="UserClassmarkGetRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_9e668055c59448f08bc3a900a0ed2095"/>
      </xs:appinfo>
      <xs:documentation>
        Request the Class Mark data associated with User.
        The response is either a UserClassmarkGetResponse or an ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="userId" type="UserId"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="UserClassmarkGetResponse">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_76b2123481c544c1bcbbfae6a0831817"/>
      </xs:appinfo>
      <xs:documentation>
        Response to UserClassmarkGetRequest.
        Contains the Class Mark data
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIDataResponse">
        <xs:sequence>
          <xs:element name="classmark" type="Classmark" minOccurs="0"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="UserClassmarkModifyRequest">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_aa0b58c4124d4d82a17a925f529d3efc"/>
      </xs:appinfo>
      <xs:documentation>
        Modify the user level data associated with Class Mark.
        The response is either a SuccessResponse or an ErrorResponse.
      </xs:documentation>
    </xs:annotation>
    <xs:complexContent>
      <xs:extension base="core:OCIRequest">
        <xs:sequence>
          <xs:element name="userId" type="UserId"/>
          <xs:element name="classmark" type="Classmark" nillable="true" minOccurs="0"/>
        </xs:sequence>
      </xs:extension>
    </xs:complexContent>
  </xs:complexType>

  <!-- ******************************************************************** -->
  <!-- M E S S A G E   P A R A M E T E R S                                  -->
  <!-- ******************************************************************** -->
  <!--
  The Class Mark specific non-primitive attributes are listed here in alphabetical order.
  -->

  <xs:simpleType name="Classmark">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_ef6c7ee0edca44a2ae757db08ac36912"/>
      </xs:appinfo>
      <xs:documentation>
        Name for the Class Mark.
      </xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:token">
      <xs:minLength value="1"/>
      <xs:maxLength value="40"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="ClassmarkValue">
    <xs:annotation>
      <xs:appinfo>
        <bwAppInfo bwtag="_9ad40496baec43088d3258d94aaa94c9"/>
      </xs:appinfo>
      <xs:documentation>
        Value for the Class Mark used in signaling.
      </xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:token">
      <xs:minLength value="1"/>
      <xs:maxLength value="32"/>
    </xs:restriction>
  </xs:simpleType>

</xs:schema>
