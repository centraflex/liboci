/**
 *
 */
package foo;

import jakarta.xml.bind.JAXBException;

import com.broadsoft.ocir.OCIReportingMessage;

import de.qsc.broadsoft.oci.coding.OCIRCoding;

/**
 * @author prelle
 *
 */
public class TestReportingDecode {

	//-----------------------------------------------------------------
	/**s
	 * @param args
	 * @throws JAXBException
	 */
	public static void main(String[] args) throws JAXBException {
		OCIRCoding.init();

		String mess = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><BroadsoftOCIReportingDocument protocol=\"OCIReporting\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><command xsi:type=\"OCIReportingReportNotification\"><id>write1018801</id><userId>*XS localhost Admin*</userId><loginType>System</loginType><request><![CDATA[<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><BroadsoftDocument protocol=\"OCI\" xmlns=\"C\"><sessionId xmlns=\"\">XS_62.206.162.10.1599860599926</sessionId><command echo=\"150646601\" xsi:type=\"UserCallForwardingAlwaysModifyRequest\" xmlns=\"\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><userId>05115356916@qsc.de</userId><isActive>true</isActive><forwardToPhoneNumber>535690</forwardToPhoneNumber></command></BroadsoftDocument>]]></request></command></BroadsoftOCIReportingDocument>";

		try {
			OCIReportingMessage foo = OCIRCoding.decode(mess);
			System.out.println("found = "+foo);
		} catch (JAXBException e) {
			if (e.getLinkedException()!=null)
				e.getLinkedException().printStackTrace();
			else if (e.getCause()!=null)
				e.getCause().printStackTrace();
			else
				e.printStackTrace();
		}
	}

}
