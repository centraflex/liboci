/**
 *
 */
package foo;

import java.io.StringWriter;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import com.broadsoft.oci.AuthenticationRequest;
import com.broadsoft.oci.OCIMessage;
import com.broadsoft.oci.ObjectFactory;

/**
 * @author prelle
 *
 */
public class TestEncoder {

	//-----------------------------------------------------------------
	/**
	 * @param args
	 * @throws JAXBException
	 */
	public static void main(String[] args) throws JAXBException {
		ObjectFactory factory = new ObjectFactory();
		JAXBContext	jaxb = JAXBContext.newInstance(ObjectFactory.class);
		Marshaller	marshaller = jaxb.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
//		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);

		AuthenticationRequest request = new AuthenticationRequest();
		request.setUserId("myUser");

		StringWriter writer = new StringWriter();

        OCIMessage message = factory.createOCIMessage();
        message.setSessionId("foo");
        message.setProtocol("OCI");
        message.getCommand().add(request);
        JAXBElement<OCIMessage> element = factory.createBroadsoftDocument(message);

		marshaller.marshal(element, writer);

		System.out.println(writer.toString());
	}

}
