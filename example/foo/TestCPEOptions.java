/**
 *
 */
package foo;

import java.io.IOException;
import java.util.Properties;

import jakarta.xml.bind.JAXBException;

import com.broadsoft.oci.CPEDeviceOptionsRead22;
import com.broadsoft.oci.CPEDeviceOptionsRead22V4;
import com.broadsoft.oci.FileResource;
import com.broadsoft.oci.GroupCPEConfigSetConfigFileRequest;
import com.broadsoft.oci.OCIResponse;
import com.broadsoft.oci.SystemSIPDeviceTypeGetRequest23V9;
import com.broadsoft.oci.SystemSIPDeviceTypeGetResponse23V9;
import com.broadsoft.oci.SystemSIPDeviceTypeModifyRequest;
import com.broadsoft.oci.SystemSIPDeviceTypeModifyRequest22V5;

import de.qsc.broadsoft.oci.OCIException;
import de.qsc.broadsoft.oci.OCISession;

/**
 * @author prelle
 *
 */
public class TestCPEOptions {

	//-----------------------------------------------------------------
	/**
	 * @param args
	 * @throws OCIException
	 * @throws IOException
	 * @throws JAXBException
	 */
	public static void main(String[] args) throws IOException, OCIException  {
		Properties pro = new Properties();
		pro.setProperty("oci.server", "web1-lab.bmcag.com");
		pro.setProperty("oci.username", "oci-gw-admin-as1-lab");
		pro.setProperty("oci.password", "ConNtestonat");
		pro.setProperty("oci.encoding", "UTF-8");

		OCISession oci = new OCISession(pro);

		SystemSIPDeviceTypeGetRequest23V9 req = new SystemSIPDeviceTypeGetRequest23V9();
		req.setDeviceType("Audiocodes MP-112 DMS");

		SystemSIPDeviceTypeGetResponse23V9 resp = (SystemSIPDeviceTypeGetResponse23V9) oci.send(req);
		System.err.println("Resp = "+resp);
		CPEDeviceOptionsRead22V4 opt = resp.getCpeDeviceOptions();
	}

}
