/**
 *
 */
package foo;

import java.io.StringReader;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import com.broadsoft.oci.OCIMessage;
import com.broadsoft.oci.ObjectFactory;

/**
 * @author prelle
 *
 */
public class Test2 {

	//-----------------------------------------------------------------
	/**
	 * @param args
	 * @throws JAXBException
	 */
	public static void main(String[] args) throws JAXBException {
		new ObjectFactory();
		JAXBContext	jaxb = JAXBContext.newInstance(ObjectFactory.class);
		Unmarshaller	unmarshaller = jaxb.createUnmarshaller();

//		String data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
//				"<BroadsoftDocument protocol=\"OCI\" xmlns=\"C\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><sessionId xmlns=\"\">foo</sessionId><command echo=\"\" xsi:type=\"AuthenticationResponse\" xmlns=\"\"><userId>oci-gw-admin-as4</userId><nonce>1382080958442</nonce><passwordAlgorithm>MD5</passwordAlgorithm></command></BroadsoftDocument>";
		String data = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><BroadsoftDocument protocol=\"OCI\" xmlns=\"C\"><userId xmlns=\"\">ac6039448</userId><command xsi:type=\"UserBusyLampFieldModifyRequest\" xmlns=\"\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><userId>069247514432@qsc.de</userId><listURI>069247514432@voip3.bmcag.com</listURI><monitoredUserIdList><userId>069247514414@qsc.de</userId><userId>069247514431@qsc.de</userId><userId>069247514443@qsc.de</userId><userId>069247514410@qsc.de</userId></monitoredUserIdList><enableCallParkNotification>false</enableCallParkNotification></command></BroadsoftDocument>";
		StreamSource s = new StreamSource(new StringReader(data));

		Object foo = unmarshaller.unmarshal(s);
		System.out.println("Parsed as "+foo.getClass());
		if (foo instanceof JAXBElement<?>)
			foo = ((JAXBElement<?>)foo).getValue();
		System.out.println("Really parsed as = "+foo.getClass());

		OCIMessage mess = (OCIMessage)foo;
		System.out.println("SessionId = "+mess.getSessionId());
	}

}
