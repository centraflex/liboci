/**
 *
 */
package foo;

import java.io.IOException;
import java.util.Properties;

import jakarta.xml.bind.JAXBException;

import com.broadsoft.oci.FileResource;
import com.broadsoft.oci.GroupCPEConfigSetConfigFileRequest;
import com.broadsoft.oci.OCIResponse;
import com.broadsoft.oci.SystemDeviceManagementSwitchDeviceTypeDeviceRequest;

import de.qsc.broadsoft.oci.OCIException;
import de.qsc.broadsoft.oci.OCISession;

/**
 * @author prelle
 *
 */
public class TestGroupCPE {

	//-----------------------------------------------------------------
	/**
	 * @param args
	 * @throws OCIException
	 * @throws IOException
	 * @throws JAXBException
	 */
	public static void main(String[] args) throws IOException, OCIException  {
		Properties pro = new Properties();
		pro.load(ClassLoader.getSystemResourceAsStream("test.properties"));
//		pro.setProperty("oci.server", "web1-lab.bmcag.com");
//		pro.setProperty("oci.username", "oci-gw-admin-as1-lab");
//		pro.setProperty("oci.password", "ConNtestonat");
//		pro.setProperty("oci.encoding", "UTF-8");

		OCISession oci = new OCISession(pro);

//		FileResource file = new FileResource();
//		file.setSourceFileName("/bw/broadworks/IpDeviceConfig/audiocodes_MAC.txt");
//		GroupCPEConfigSetConfigFileRequest setReq = new GroupCPEConfigSetConfigFileRequest();
//		setReq.setDeviceType("AudiocodesMP-112-QSC");
//		setReq.setServiceProviderId("Enterprise");
//		setReq.setGroupId("Gruppe1");
//		setReq.setConfigFile(file);

		SystemDeviceManagementSwitchDeviceTypeDeviceRequest req = new SystemDeviceManagementSwitchDeviceTypeDeviceRequest();
		req.setSvcProviderId("c6624767");
		req.setGroupId("c95144711");
		req.setDeviceName("Test_ATA_SPR");
		req.setToDeviceType("AudiocodesMP-112-QSC");

		OCIResponse resp = oci.send(req);
		System.err.println("Resp = "+resp);

	}

}
