/**
 *
 */
package foo;

import java.io.IOException;
import java.util.Properties;

import jakarta.xml.bind.JAXBException;

import com.broadsoft.oci.FileResource;
import com.broadsoft.oci.GroupCPEConfigSetConfigFileRequest;
import com.broadsoft.oci.HoldNormalizationMode;
import com.broadsoft.oci.OCIResponse;
import com.broadsoft.oci.OCITableRow;
import com.broadsoft.oci.SystemAccessDeviceMonitorGetDeviceTypeListRequest;
import com.broadsoft.oci.SystemAccessDeviceMonitorGetDeviceTypeListResponse;
import com.broadsoft.oci.SystemAccessDeviceTypeGetListRequest;
import com.broadsoft.oci.SystemAccessDeviceTypeGetListResponse;
import com.broadsoft.oci.SystemSIPDeviceTypeGetListRequest;
import com.broadsoft.oci.SystemSIPDeviceTypeGetListResponse;
import com.broadsoft.oci.SystemSIPDeviceTypeGetRequest23V9;
import com.broadsoft.oci.SystemSIPDeviceTypeGetResponse23V9;

import de.qsc.broadsoft.oci.OCIException;
import de.qsc.broadsoft.oci.OCISession;

/**
 * @author prelle
 *
 */
public class TestDeviceProfile {

	//-----------------------------------------------------------------
	/**
	 * @param args
	 * @throws OCIException
	 * @throws IOException
	 * @throws JAXBException
	 */
	public static void main(String[] args) throws IOException, OCIException  {
		Properties pro = new Properties();
		pro.load(ClassLoader.getSystemResourceAsStream("test.properties"));
//		pro.setProperty("oci.server", "web1-lab.bmcag.com");
//		pro.setProperty("oci.username", "oci-gw-admin-as1-lab");
//		pro.setProperty("oci.password", "ConNtestonat");
//		pro.setProperty("oci.encoding", "UTF-8");

		OCISession oci = new OCISession(pro);

//		SystemAccessDeviceTypeGetListRequest req = new SystemAccessDeviceTypeGetListRequest();
//		SystemAccessDeviceTypeGetListResponse resp = (SystemAccessDeviceTypeGetListResponse) oci.send(req);

		SystemSIPDeviceTypeGetListRequest req2 = new SystemSIPDeviceTypeGetListRequest();
		SystemSIPDeviceTypeGetListResponse resp2 = (SystemSIPDeviceTypeGetListResponse)oci.send(req2);
		int i=0;
		int count=0;
		for (OCITableRow row : resp2.getDeviceTypeTable().getRow()) {
			System.out.print(String.format("%2d : %s", ++i, row.getCol().get(0)));

			SystemSIPDeviceTypeGetRequest23V9 req3 = new SystemSIPDeviceTypeGetRequest23V9();
			req3.setDeviceType(row.getCol().get(0));
			SystemSIPDeviceTypeGetResponse23V9 resp3 = (SystemSIPDeviceTypeGetResponse23V9) oci.send(req3);
			if (resp3.getHoldNormalization()!=HoldNormalizationMode.RFC_3264)
				count++;
			System.out.println("  \t"+resp3.getHoldNormalization());
		}

		System.out.println(count+" devices");
		System.exit(1);
	}

}
