package foo;

import java.io.StringReader;
import java.io.StringWriter;
import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import com.broadsoft.oci.AuthenticationRequest;
import com.broadsoft.oci.AuthenticationResponse;
import com.broadsoft.oci.LoginRequest14Sp4;
import com.broadsoft.oci.LoginResponse14Sp4;
import com.broadsoft.oci.OCIMessage;
import com.broadsoft.oci.ObjectFactory;

public class TestBWProvisioning {

	private Marshaller m_Marshaller;
	private Unmarshaller m_UnMarschaller;

//	private String m_Nonce;
	private String m_UserId;
//	private String m_Password;
	private String m_SessionId;

	public TestBWProvisioning(String a_UserId, String a_Password)
			throws JAXBException {
		m_UserId = a_UserId;
//		m_Password = a_Password;
		m_SessionId = Long.toString(System.currentTimeMillis());

		JAXBContext jaxbContext = JAXBContext.newInstance(OCIMessage.class);
		m_Marshaller = jaxbContext.createMarshaller();
		m_Marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
		m_Marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);

		m_UnMarschaller = jaxbContext.createUnmarshaller();
	}

	public void authentication()
			throws JAXBException, RemoteException
			{
		OCIMessage message = new OCIMessage();
		message.setSessionId(m_SessionId);
		message.setProtocol("OCI");

		AuthenticationRequest request = new AuthenticationRequest();
		request.setUserId(m_UserId);
		message.getCommand().add(request);

		StringWriter writer = new StringWriter();
		m_Marshaller.marshal(new ObjectFactory().createBroadsoftDocument(message), writer);
		System.out.println(writer);

		String retval = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"<BroadsoftDocument protocol=\"OCI\" xmlns=\"C\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><sessionId xmlns=\"\">foo</sessionId><command echo=\"\" xsi:type=\"AuthenticationResponse\" xmlns=\"\"><userId>oci-gw-admin-as4</userId><nonce>1382080958442</nonce><passwordAlgorithm>MD5</passwordAlgorithm></command></BroadsoftDocument>";
//		String retval = m_ProvService.processOCIMessage(writer.toString());
		OCIMessage ociResponse = m_UnMarschaller.unmarshal(new StreamSource(new StringReader(retval)),OCIMessage.class).getValue();
		AuthenticationResponse response = (AuthenticationResponse) ociResponse.getCommand().get(0);

//		m_Nonce = response.getNonce();

		System.out.println("Returns:");
		System.out.println(" Nonce: " + response.getNonce());
		System.out.println(" ALG: " + response.getPasswordAlgorithm());
		System.out.println(" Echo: " + response.getEcho());
		System.out.println(" UserID: " + response.getUserId());
		System.out.println("----------");
			}

	public void login() throws JAXBException, RemoteException, NoSuchAlgorithmException
			{
		OCIMessage msg = new OCIMessage();
		msg.setSessionId(m_SessionId);
		msg.setProtocol("OCI");

		LoginRequest14Sp4 loginReq = new LoginRequest14Sp4();
		loginReq.setUserId(m_UserId);

//		MessageDigest mdSHA = MessageDigest.getInstance("SHA");
//		MessageDigest mdMD5 = MessageDigest.getInstance("MD5");

//		String s2 = m_Nonce + ":" + bytesToHexString(mdSHA.digest(m_Password.getBytes()));
//		String pw = bytesToHexString(mdMD5.digest(s2.getBytes()));
		String pw = "as23ofhowif";
		loginReq.setSignedPassword(pw);

		msg.getCommand().add(loginReq);

		StringWriter writer = new StringWriter();
		m_Marshaller.marshal(new ObjectFactory().createBroadsoftDocument(msg), writer);
		System.out.println(writer);

//		String retval = m_ProvService.processOCIMessage(writer.toString());
		String retval = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"<BroadsoftDocument protocol=\"OCI\" xmlns=\"C\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><sessionId xmlns=\"\">foo</sessionId><command echo=\"\" xsi:type=\"AuthenticationResponse\" xmlns=\"\"><userId>oci-gw-admin-as4</userId><nonce>1382080958442</nonce><passwordAlgorithm>MD5</passwordAlgorithm></command></BroadsoftDocument>";
		OCIMessage ociResponse = m_UnMarschaller.unmarshal(new StreamSource( new StringReader(retval)),OCIMessage.class).getValue();
		LoginResponse14Sp4 response = (LoginResponse14Sp4) ociResponse.getCommand().get(0);

		System.out.println("Returns:");
		System.out.println(" GroupID: " + response.getGroupId());
		System.out.println(" ServiceProviderID: " + response.getServiceProviderId());
		System.out.println(" UserDomain: " + response.getUserDomain());
		System.out.println(" LoginType: " + response.getLoginType());
		System.out.println(" PWExpires: " + response.getPasswordExpiresDays());
		System.out.println("----------");
			}

	public static void main(String[] args)
			throws Exception
			{
		TestBWProvisioning service= new TestBWProvisioning("hallo","welt");
		service.authentication();
		service.login();
			}
}
