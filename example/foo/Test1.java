/**
 * 
 */
package foo;

import java.io.IOException;

import com.broadsoft.oci.UserGetRequest18;
import com.broadsoft.oci.UserGetResponse18;

import de.qsc.broadsoft.oci.OCIException;
import de.qsc.broadsoft.oci.transport.DirectConnection;

/**
 * @author prelle
 *
 */
public class Test1 {

	//-----------------------------------------------------------------
	/**
	 * @param args
	 * @throws IOException 
	 * @throws OCIException 
	 */
	public static void main(String[] args) throws IOException, OCIException {
		DirectConnection con = new DirectConnection("web4.bmcag.com", "adminaccount", "foo", "UTF-8");
//		OCIPConnection con = new OCIPConnection("ews.xdp.broadsoft.com", "spr_qscUser2@xdp.broadsoft.com", "taminake");
		
		UserGetRequest18 req2 = new UserGetRequest18();
		req2.setUserId("040668610764@qsc.de");
		
		try {
			UserGetResponse18 resp = (UserGetResponse18) con.sendRequest(req2);
			System.out.println("Username is "+resp.getFirstName()+" "+resp.getLastName());
		} catch (OCIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
