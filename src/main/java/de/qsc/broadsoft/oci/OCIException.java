/**
 * 
 */
package de.qsc.broadsoft.oci;

import com.broadsoft.oci.ErrorResponse;


/**
 * @author prelle
 *
 */
public class OCIException extends Exception {
	
	private static final long serialVersionUID = 5731582659928673048L;

	private int code;
	private String summary;
	private String detail;

	//-----------------------------------------------------------------
	private static int parseSummaryToCode(String summary) {
		if (!summary.startsWith("[Error "))
			return -1;
		int pos = summary.indexOf("]");
		String code_s = summary.substring(7, pos);
		return Integer.parseInt(code_s);
	}

	//-----------------------------------------------------------------
	/**
	 */
	public OCIException(String summary) {
		super(summary);
		this.summary = summary;
		this.code = parseSummaryToCode(summary);
	}

	//-----------------------------------------------------------------
	/**
	 */
	public OCIException(ErrorResponse error) {
		super(error.getSummary()+((error.getDetail()!=null)?("\n"+error.getDetail()):""));
		this.summary = error.getSummary();
		this.code    = error.getErrorCode();
		this.detail  = error.getDetail();
	}

	//-----------------------------------------------------------------
	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the summary
	 */
	public String getSummary() {
		return summary;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}

}
