/**
 * 
 */
package de.qsc.broadsoft.oci;


/**
 * @author prelle
 *
 */
public interface ConnectionStateListener {

	//-----------------------------------------------------------------
	/**
	 * Called whenever a connection state changes
	 * @param src  Connection that changed
	 * @param state New state
	 */
	public void connectionChanged(Object src, ConnectionState state);
	
}
