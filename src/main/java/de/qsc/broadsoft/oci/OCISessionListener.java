/**
 * 
 */
package de.qsc.broadsoft.oci;

import java.util.List;

import com.broadsoft.ocir.OCIReportingReportPublicIdentityNotification.AddedUser;
import com.broadsoft.ocir.OCIReportingReportPublicIdentityNotification.DeletedUser;
import com.broadsoft.ocir.OCIReportingReportPublicIdentityNotification.ModifiedUser;;

/**
 * @author prelle
 *
 */
public interface OCISessionListener {

	public void ociEventReceived(OCIREvent event);

	public void publicIdentitiesChanged(List<AddedUser> added, List<ModifiedUser> modified, List<DeletedUser> deleted);
	
}
