package de.qsc.broadsoft.oci;

public enum ConnectionState {
	NOT_CONNECTED,
	CONNECTED,
	AUTHENTICATED,
	STOPPED
}