package de.qsc.broadsoft.oci;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author prelle
 *
 */
public class OCIREventBus {

	private final static Logger logger = System.getLogger("oci.r");

	private static Collection<OCIREventListener> subscriber;
	
	//-----------------------------------------------------------------
	static {
		subscriber = new ArrayList<OCIREventListener>();
	}
	
	//-----------------------------------------------------------------
	/**
	 * Register a listener to receive OCI-R events
	 */
	public static void subscribe(OCIREventListener listen) {
		synchronized (subscriber) {
			if (!subscriber.contains(listen)) {
				logger.log(Level.INFO,"Registered to receive OCI-R events: "+listen.getClass());
				subscriber.add(listen);
			}
		}
	}
	
	//-----------------------------------------------------------------
	/**
	 * Distribute an event to all listeners
	 */
	public static void fireEvent(OCIREvent event) {
		synchronized (subscriber) {
			logger.log(Level.DEBUG,String.format("Fire %s event to %d listener", event.getReported(), subscriber.size()));
			for (OCIREventListener listen : subscriber) {
				try {
					listen.handleOCIR(event);
				} catch (Exception e) {
					logger.log(Level.ERROR,"Error processing "+event+" in listener "+listen.getClass(),e);
				}
			}
		}
	}

}
