/**
 * 
 */
package de.qsc.broadsoft.oci;

import com.broadsoft.oci.OCICommand;
import com.broadsoft.ocir.OCIReportingReportPublicIdentityNotification;

/**
 * @author prelle
 *
 */
public interface ReportingListener {

	//-----------------------------------------------------------------
	/**
	 * @param userID User executing the command
	 * @param command The command executed
	 */
	public void provisioningExecuted(String userID, OCICommand command, String xml);
	
	public void publicIdentitiesChanged(OCIReportingReportPublicIdentityNotification notify);
	
}
