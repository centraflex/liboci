package de.qsc.broadsoft.oci.transport;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import jakarta.xml.bind.JAXBException;

import com.broadsoft.oci.AuthenticationRequest;
import com.broadsoft.oci.AuthenticationResponse;
import com.broadsoft.oci.ErrorResponse;
import com.broadsoft.oci.LoginRequest14Sp4;
import com.broadsoft.oci.LoginResponse14Sp4;
import com.broadsoft.oci.OCICommand;
import com.broadsoft.oci.OCIMessage;
import com.broadsoft.oci.OCIRequest;
import com.broadsoft.oci.OCIResponse;

import de.qsc.broadsoft.oci.ConnectionState;
import de.qsc.broadsoft.oci.ConnectionStateListener;
import de.qsc.broadsoft.oci.OCIException;
import de.qsc.broadsoft.oci.coding.OCIPCoding;

/**
 * @author prelle
 *
 */
public class DirectConnection implements Runnable, ProvisioningConnection {

	private final static Logger logger = System.getLogger("oci.p");
	private final static int RESPONSE_TIMEOUT = 12000;

	private final static String PROP_OCI_SERVER = "oci.server";
	private final static String PROP_OCI_PORT   = "oci.port";
	private final static String DEFAULT_OCI_PORT = "2208";
	private final static String DEFAULT_OCI_ENCODING = "UTF-8";
    private final static String PROP_OCI_USERNAME= "oci.username";
    private final static String PROP_OCI_PASSWORD= "oci.password";
    private final static String PROP_OCI_ENCODING= "oci.encoding";

	private final static Random RANDOM = new Random();
	protected ConnectionState state;
	private List<ConnectionStateListener> listener;
	protected String host;
	private int port;
	private String user;
	private String pass;
	private Charset encoding;
	private Thread thread;
	private MessageDigest md5;
	private MessageDigest sha1;

	private String sessionID;
	private Socket socket;
	private InputStream in;
	private OutputStream out;
	private String responseXML;
	private OCIMessage response;

	private boolean responseExpected;

	//-----------------------------------------------------------------
	/**
	 * @throws IOException
	 * @throws OCIException
	 */
	public DirectConnection(String host, String user, String pass, String encoding) throws IOException, OCIException {
		if (host==null)
			throw new NullPointerException("Host is null");
		if (user==null)
			throw new NullPointerException("User is null");
		if (pass==null)
			throw new NullPointerException("Password is null");
		this.host = host;
		this.port = 2208;
		this.user = user;
		this.pass = pass;
		if (encoding!=null)
			this.encoding = Charset.forName(encoding);

		commonConstructor();
	}

	//-----------------------------------------------------------------
	/**
	 * @throws IOException
	 * @throws OCIException
	 */
	public DirectConnection(Properties pro) throws IOException, OCIException {
		// Read configuration
		host  = pro.getProperty(PROP_OCI_SERVER);
		port  = Integer.parseInt(pro.getProperty(PROP_OCI_PORT, DEFAULT_OCI_PORT));
		user  = pro.getProperty(PROP_OCI_USERNAME);
		pass  = pro.getProperty(PROP_OCI_PASSWORD);
		encoding  = Charset.forName(pro.getProperty(PROP_OCI_ENCODING, DEFAULT_OCI_ENCODING));
		sessionID = user+"-"+RANDOM.nextInt(99999);
		if (host==null)
			throw new NullPointerException("Host is null");
		if (user==null)
			throw new NullPointerException("User is null");
		if (pass==null)
			throw new NullPointerException("Password is null");

		commonConstructor();
	}

	//-----------------------------------------------------------------
	private void commonConstructor() throws IOException, OCIException {
		OCIPCoding.init();
		if (encoding!=null)
			OCIPCoding.setEncoding(encoding);
		listener = new ArrayList<ConnectionStateListener>();

		state = ConnectionState.NOT_CONNECTED;

		try {
			md5 = MessageDigest.getInstance("MD5");
			sha1= MessageDigest.getInstance("SHA1");
		} catch (NoSuchAlgorithmException e1) {
			logger.log(Level.ERROR,"Could not obtain MD5 digest: "+e1);
			throw new IOException(e1);
		}

		createSessionID();
		initConnection();
		login();

		/*
		 * Verify that the connection is up every 5 minutes
		 */
		logger.log(Level.DEBUG,"Starting re-connect timer");
		Timer timer = new Timer("OCI-P Re-Connect Timer", true);
		timer.scheduleAtFixedRate(new TimerTask(){
			public void run() {
				checkConnectionState();
			}}, 300000, 60000);
	}

	//-----------------------------------------------------------------
	private void createSessionID() {
		try {
			sessionID = "liboci-"+InetAddress.getLocalHost().getHostName()+"-"+System.currentTimeMillis();
		} catch (UnknownHostException e) {
			logger.log(Level.ERROR,e.toString());
			sessionID = "liboci-localhost-"+System.currentTimeMillis();
		}
		logger.log(Level.DEBUG,"Session is "+sessionID);
	}

	//-----------------------------------------------------------------
	private void initConnection() throws IOException {
		logger.log(Level.DEBUG,"trying to connect to "+host+":"+port);
		// Connect to OCI-R server
		try {
			socket = new Socket(host, port);
			socket.setSoTimeout(RESPONSE_TIMEOUT);
			socket.setKeepAlive(true);
			socket.setTrafficClass(8);
			in = socket.getInputStream();
			out = socket.getOutputStream();
		} catch (IOException e) {
			logger.log(Level.ERROR,"Could not connect to OCI-P server at "+host+" , port "+port);
			throw e;
		}

		// Start reading input
		thread = new Thread(this, "OCI-P");
		thread.start();

		changeState(ConnectionState.CONNECTED);
		logger.log(Level.INFO,"OCI connection initialized to "+host+":"+port);
	}

	//-----------------------------------------------------------------
	public void addListener(ConnectionStateListener callback) {
		if (!listener.contains(callback))
			listener.add(callback);
	}

	//-----------------------------------------------------------------
	private void changeState(ConnectionState newState) {
		if (state==newState)
			return;
		logger.log(Level.INFO,"Change state from "+state+" to "+newState);
		state = newState;

		// Inform listener
		for (ConnectionStateListener tmp : listener) {
			try {
				tmp.connectionChanged(this, state);
			} catch (Exception e) {
				logger.log(Level.ERROR,"Exception in listener: "+e,e);
			}
		}
	}

	//-----------------------------------------------------------------
	private static String toHex(byte[] bytes) {
        StringBuilder string = new StringBuilder();
        for (byte b: bytes) {
                String hexString = Integer.toHexString(0x00FF & b);
                string.append(hexString.length() == 1 ? "0" + hexString : hexString);
        }
        return string.toString();
	}

	//-----------------------------------------------------------------
	private boolean login() throws OCIException, IOException {
		logger.log(Level.DEBUG,"Login startet in state "+state);
		if (state!=ConnectionState.CONNECTED)
			throw new IllegalStateException(String.format("Only possible in state %s (currently %s)", ConnectionState.CONNECTED, state));

		AuthenticationRequest loginRequest = new AuthenticationRequest();
		loginRequest.setUserId(user);

		OCIResponse response = sendRequestInternal(loginRequest);
		logger.log(Level.DEBUG,"Response was "+response);
		if (response instanceof AuthenticationResponse) {
			AuthenticationResponse resp = (AuthenticationResponse)response;

			String passwd = toHex(sha1.digest(pass.getBytes()));
			String toHash = resp.getNonce()+":"+passwd;

			// MD5
			md5.update(toHash.getBytes());
			byte[] digest = md5.digest();

			LoginRequest14Sp4 req2 = new LoginRequest14Sp4();
			req2.setUserId(user);
			req2.setSignedPassword(toHex(digest));
			response = sendRequestInternal(req2);
			if (response instanceof LoginResponse14Sp4) {
				changeState(ConnectionState.AUTHENTICATED);
				logger.log(Level.INFO,"Successfully connected as "+user+" with "+host);
				return true;
			}
		} else
			logger.log(Level.WARNING,"Unexpected response: "+response);
		return false;
	}

	//-----------------------------------------------------------------
	private OCIResponse sendRequestInternal(OCIRequest request) throws IOException, OCIException {
		logger.log(Level.DEBUG,"SND "+request.getClass());

		OCIMessage ret = null;
		synchronized (this) {
			response = null;
			try {
				String xml = OCIPCoding.encode(request, sessionID);
				if (logger.isLoggable(Level.TRACE))
					logger.log(Level.TRACE,"SND "+xml);
				long sendTime = System.currentTimeMillis();
				responseExpected = true;
				out.write( (encoding!=null)?xml.getBytes(encoding):xml.getBytes());
				out.flush();
				this.wait(RESPONSE_TIMEOUT);
				responseExpected = false;
				long rcvTime = System.currentTimeMillis();
				int procTime = (int)(rcvTime-sendTime);
				logger.log(Level.DEBUG,"Server took "+procTime+" ms to process");
				ret = response;
				response = null;

				if (ret!=null) {
					for (OCICommand com : ret.getCommand()) {
						if (com instanceof ErrorResponse) {
							if (xml.length()>500) xml = xml.substring(0,500)+"...";
							logger.log(Level.WARNING,"SND "+xml);
							if (responseXML.length()>500) responseXML = responseXML.substring(0,500)+"...";
							logger.log(Level.WARNING,"RCV "+com.getClass()+"\n"+responseXML);
							throw new OCIException((ErrorResponse)com);
						} else if (com instanceof OCIResponse) {
							logger.log(Level.DEBUG,"RCV "+com.getClass());
							return (OCIResponse)com;
						} else
							logger.log(Level.ERROR,"Did not expect "+com);
					}
				} else {
					logger.log(Level.WARNING,"No response received to "+request);
					throw new OCIException("No response received or parsing failed");
				}
			} catch (IOException e) {
				logger.log(Level.ERROR,"Connection lost to OCI server "+host);
				changeState(ConnectionState.NOT_CONNECTED);
			} catch (InterruptedException e) {
				logger.log(Level.ERROR,"Exception: "+e,e);
			} catch (JAXBException e) {
				logger.log(Level.ERROR,"Failed encoding to XML: "+e,e);
				throw new IOException(e);
			}
		}
		return null;
	}

	//-----------------------------------------------------------------
	/**
	 * @see de.qsc.broadsoft.oci.transport.ProvisioningConnection#sendRequest(com.broadsoft.oci.OCIRequest)
	 */
	@Override
	public OCIResponse sendRequest(OCIRequest request) throws IOException, OCIException {
		if (state!=ConnectionState.AUTHENTICATED) {
			if (!login())
				throw new IllegalStateException(String.format("Only possible in state %s (currently %s)", ConnectionState.AUTHENTICATED, state));
		}

		return sendRequestInternal(request);
	}

	//-----------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		byte[] buf = new byte[65536*30];
		int pos =0;

		logger.log(Level.DEBUG,"Start waiting for server responses");
		try {
			while (true) {
					int length = 0;
					try {
						length = in.read(buf, pos, buf.length-pos);
						logger.log(Level.TRACE,"Received "+length+" bytes, copied to "+pos);
						if (length<0) {
							logger.log(Level.WARNING,"Can not read from connection to "+host+":"+port);
							changeState(ConnectionState.NOT_CONNECTED);
							return;
						}
						if (length==0) {
							pos=0;
							continue;
						}
					} catch (SocketTimeoutException e1) {
						// No response received
						if (responseExpected) {
							// Response was expected
							responseExpected = false;
							logger.log(Level.ERROR,"No response received from "+host);
							changeState(ConnectionState.NOT_CONNECTED);
							break;
						} else {
							// Its okay - it wasn't expected
//							logger.log(Level.DEBUG,"No response received, but none expected");
							continue;
						}
					}

					String data = (encoding!=null)?(new String(buf, 0, pos+length, encoding)):(new String(buf, 0, pos+length));
					if (logger.isLoggable(Level.TRACE))
						logger.log(Level.TRACE,"RCV "+data);

					try {
						OCIMessage mess = OCIPCoding.decode(data);
						pos=0;
						synchronized (this) {
							response = mess;
							responseXML = data;
							this.notify();
						}
					} catch (JAXBException e) {
						// XML not complete
						if (logger.isLoggable(Level.TRACE)) {
							logger.log(Level.TRACE,data);
						}
						logger.log(Level.DEBUG,"Continue reading after "+e.getClass()+" // "+e.getCause());
						pos+=length;
					}
			}
		} catch (IOException e) {
			logger.log(Level.ERROR,"Error reading and parsing data from socket: "+e,e);
			changeState(ConnectionState.NOT_CONNECTED);
		}

		logger.log(Level.INFO,"Stopped waiting for responses");
	}

	//-----------------------------------------------------------------
	void checkConnectionState() {
		logger.log(Level.INFO,"checkConnectionState");
		switch (state) {
		case STOPPED:
			// Not connected, but closing any moment now
			break;
		case AUTHENTICATED:
			// All is well
			break;
		case NOT_CONNECTED:
			logger.log(Level.DEBUG,"Trying to reconnect");
			try {
				createSessionID();
				initConnection();
				login();
			} catch (Exception e) {
				logger.log(Level.WARNING,"Reconnect failed: "+e);
			}
			break;
		default:
			logger.log(Level.WARNING,"State is neither "+ConnectionState.NOT_CONNECTED+" nor "+ConnectionState.STOPPED);
		}
	}

}
