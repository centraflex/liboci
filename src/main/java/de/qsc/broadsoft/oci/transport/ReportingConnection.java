/**
 * 
 */
package de.qsc.broadsoft.oci.transport;

import de.qsc.broadsoft.oci.ConnectionStateListener;
import de.qsc.broadsoft.oci.ReportingListener;

/**
 * @author prelle
 *
 */
public interface ReportingConnection {

	//-----------------------------------------------------------------
	public void addListener(ConnectionStateListener callback);

	//-----------------------------------------------------------------
	public void addListener(ReportingListener callback);

}
