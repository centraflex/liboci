/**
 * 
 */
package de.qsc.broadsoft.oci.transport;

import java.io.IOException;

import com.broadsoft.oci.OCIRequest;
import com.broadsoft.oci.OCIResponse;

import de.qsc.broadsoft.oci.ConnectionStateListener;
import de.qsc.broadsoft.oci.OCIException;

/**
 * @author prelle
 *
 */
public interface ProvisioningConnection {

	//-----------------------------------------------------------------
	public void addListener(ConnectionStateListener callback);

	//-----------------------------------------------------------------
	public OCIResponse sendRequest(OCIRequest request) throws IOException, OCIException;
	
}
