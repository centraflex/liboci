package de.qsc.broadsoft.oci.transport;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import jakarta.xml.bind.JAXBException;

import com.broadsoft.oci.OCICommand;
import com.broadsoft.oci.OCIMessage;
import com.broadsoft.ocir.OCIReportingCommand;
import com.broadsoft.ocir.OCIReportingMessage;
import com.broadsoft.ocir.OCIReportingReportNotification;
import com.broadsoft.ocir.OCIReportingReportPublicIdentityNotification;
import com.broadsoft.ocir.OCIReportingServerStatusNotification;

import de.qsc.broadsoft.oci.ConnectionState;
import de.qsc.broadsoft.oci.ConnectionStateListener;
import de.qsc.broadsoft.oci.OCIException;
import de.qsc.broadsoft.oci.ReportingListener;
import de.qsc.broadsoft.oci.coding.OCIPCoding;
import de.qsc.broadsoft.oci.coding.OCIRCoding;
import de.qsc.xml.XMLDocumentTokenizer;

/**
 * @author prelle
 *
 */
public class DirectReportingConnection implements Runnable, ReportingConnection {

	private final static Logger LOGGER = System.getLogger("oci.r");
	private final static Logger xmlLogger = System.getLogger("traces.oci-r.xml");


	public final static String PROP_OCIR_SERVER = "ocir.server";
	private final static String PROP_OCIR_PORT   = "ocir.port";
	private final static String DEFAULT_OCIR_PORT = "8025";
	private final static int RESPONSE_TIMEOUT = 31*60000;

	protected ConnectionState state;
	private List<ConnectionStateListener> listener;
	private List<ReportingListener> callbacks;
	protected String host;
	private int port;
	private Thread thread;

	private Socket socket;
	private XMLDocumentTokenizer in;
	//	private InputStream in;

	//-----------------------------------------------------------------
	/**
	 * @throws IOException
	 * @throws OCIException
	 */
	public DirectReportingConnection(String host) throws IOException {
		if (host==null)
			throw new NullPointerException("Host is null");
		this.host = host;
		this.port = 8025;
		commonConstructor();
	}

	//-----------------------------------------------------------------
	/**
	 * @throws IOException
	 * @throws OCIException
	 */
	public DirectReportingConnection(String host, int port) throws IOException {
		if (host==null)
			throw new NullPointerException("Host is null");
		this.host = host;
		this.port = port;
		commonConstructor();
	}

	//-----------------------------------------------------------------
	/**
	 * @throws IOException
	 * @throws OCIException
	 */
	public DirectReportingConnection(Properties pro) throws IOException {
		host = pro.getProperty(PROP_OCIR_SERVER);
		port = Integer.parseInt(pro.getProperty(PROP_OCIR_PORT, DEFAULT_OCIR_PORT));
		if (host==null)
			throw new NullPointerException("Property "+PROP_OCIR_SERVER+" not set");
		commonConstructor();
	}

	//-----------------------------------------------------------------
	private void commonConstructor() throws IOException {
		OCIRCoding.init();
		OCIPCoding.init();

		try {
			throw new RuntimeException("Trace");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("\n\n\n\nDirectReportingConnection<init>\n\n\n\n");
		}

		listener = new ArrayList<ConnectionStateListener>();
		callbacks= new ArrayList<ReportingListener>();

		state = ConnectionState.NOT_CONNECTED;

		initConnection();

		/*
		 * Verify that the connection is up every 5 minutes
		 */
		LOGGER.log(Level.DEBUG,"Starting re-connect timer");
		Timer timer = new Timer("OCI-R Re-Connect Timer", true);
		timer.scheduleAtFixedRate(new TimerTask(){
			public void run() {
				checkConnectionState();
			}}, 60000, 60000);
	}

	//-----------------------------------------------------------------
	private void initConnection() throws IOException {
		// Connect to OCI-R server
		try {
			LOGGER.log(Level.INFO,"Connect to "+host+":"+port);
			socket = new Socket(host, port);
			socket.setSoTimeout(RESPONSE_TIMEOUT);
			socket.setKeepAlive(true);
			LOGGER.log(Level.INFO,"Socket is open");
			in = new XMLDocumentTokenizer(socket.getInputStream());
			//			in = socket.getInputStream();
		} catch (IOException e) {
			LOGGER.log(Level.ERROR,"Could not connect to OCI-R server at "+host+" , port "+port);
			throw e;
		}

		// Start reading input
		thread = new Thread(this, "OCI-R");
		thread.start();

		changeState(ConnectionState.CONNECTED);
		LOGGER.log(Level.INFO,"OCI connection initialized to "+host+":"+port);
	}

	//-----------------------------------------------------------------
	/**
	 * @see de.qsc.broadsoft.oci.transport.ReportingConnection#addListener(de.qsc.broadsoft.oci.ConnectionStateListener)
	 */
	@Override
	public void addListener(ConnectionStateListener callback) {
		if (!listener.contains(callback))
			listener.add(callback);
	}

	//-----------------------------------------------------------------
	private void changeState(ConnectionState newState) {
		if (state==newState)
			return;
		LOGGER.log(Level.INFO,"Change state from "+state+" to "+newState);
		state = newState;

		// Inform listener
		for (ConnectionStateListener tmp : listener) {
			try {
				tmp.connectionChanged(this, state);
			} catch (Exception e) {
				LOGGER.log(Level.ERROR,"Exception in listener: "+e,e);
			}
		}
	}

	//-----------------------------------------------------------------
	/**
	 * @see de.qsc.broadsoft.oci.transport.ReportingConnection#addListener(de.qsc.broadsoft.oci.ReportingListener)
	 */
	@Override
	public void addListener(ReportingListener callback) {
		if (!callbacks.contains(callback))
			callbacks.add(callback);
	}

	//-----------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		LOGGER.log(Level.INFO,"Start waiting for OCI events");
		//		byte[] buf = new byte[4096];
		try {
			while (true) {
				String data = in.nextDocument();
				if (data==null) {
					// Connection lost
					changeState(ConnectionState.NOT_CONNECTED);
					LOGGER.log(Level.ERROR,"Lost OCI-R connection to "+host);
					return;
				}
				data = data.trim();
				if (LOGGER.isLoggable(Level.TRACE))
					LOGGER.log(Level.TRACE,"RCV ..[.."+data+"..]..");

				OCIReportingMessage mess = OCIRCoding.decode(data);

				for (OCIReportingCommand com : mess.getCommand()) {
					if (com instanceof OCIReportingReportNotification) {
						if (xmlLogger.isLoggable(Level.TRACE))
							xmlLogger.log(Level.TRACE,data);
						processEvent((OCIReportingReportNotification)com);
					} else if (com instanceof OCIReportingServerStatusNotification) {
						// Ignore keep alive
					} else if (com instanceof OCIReportingReportPublicIdentityNotification) {
						processEvent((OCIReportingReportPublicIdentityNotification)com);
					} else
						LOGGER.log(Level.WARNING,"Unsupported type: "+com.getClass());
				}
			}
		} catch (IOException | JAXBException e) {
			LOGGER.log(Level.ERROR,"Error reading and parsing data from socket: "+e,e);
			changeState(ConnectionState.NOT_CONNECTED);
		}
	}

	//-----------------------------------------------------------------
	private void processEvent(OCIReportingReportNotification notify) {
		LOGGER.log(Level.DEBUG,"User "+notify.getUserId()+" (Type "+notify.getLoginType()+")");
		String request = notify.getRequest();
		if (LOGGER.isLoggable(Level.TRACE))
			LOGGER.log(Level.TRACE,"embedded = "+request);

		OCIMessage ret = null;
		try {
			ret = OCIPCoding.decode(request);
		} catch (JAXBException e) {
			LOGGER.log(Level.ERROR,"JAXB error decoding embedded XML: "+e.getClass(),e);
			LOGGER.log(Level.ERROR,"embedded XML was: "+request);
			LOGGER.log(Level.ERROR,"Cause was ",e.getCause());
			if (e.getCause()!=null)
				LOGGER.log(Level.ERROR,"Cause's cause was "+e.getCause().getCause());
			return;
		}


		for (OCICommand com : ret.getCommand()) {
			LOGGER.log(Level.DEBUG,"User "+notify.getUserId()+" (Type "+notify.getLoginType()+") issued "+com.getClass().getSimpleName());
			// Inform listener
			for (ReportingListener callback : callbacks) {
				try {
					callback.provisioningExecuted(notify.getUserId(), com, request);
				} catch (Exception e) {
					LOGGER.log(Level.ERROR,"Error calling reporting listener "+callback.getClass()+": "+e,e);
				}
			}
		}
	}

	//-----------------------------------------------------------------
	private void processEvent(OCIReportingReportPublicIdentityNotification notify) throws JAXBException {
		LOGGER.log(Level.DEBUG,"Public identities changed");
		// Inform listener
		for (ReportingListener callback : callbacks) {
			try {
				callback.publicIdentitiesChanged(notify);
			} catch (Exception e) {
				LOGGER.log(Level.ERROR,"Error calling "+callback.getClass()+": "+e,e);
			}
		}
	}

	//-----------------------------------------------------------------
	void checkConnectionState() {
		LOGGER.log(Level.DEBUG,"checkConnectionState");
		switch (state) {
		case STOPPED:
			// Not connected, but closing any moment now
			break;
		case NOT_CONNECTED:
			LOGGER.log(Level.DEBUG,"Trying to reconnect");
			try {
				initConnection();
			} catch (Exception e) {
				LOGGER.log(Level.WARNING,"Reconnect failed: "+e);
			}
			break;
		case AUTHENTICATED:
			LOGGER.log(Level.TRACE,"Still connected");
			break;
		case CONNECTED:
			return;
		default:
			LOGGER.log(Level.WARNING,"State is "+state);
		}
	}

}
