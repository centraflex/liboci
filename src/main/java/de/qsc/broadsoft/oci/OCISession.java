package de.qsc.broadsoft.oci;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import com.broadsoft.oci.OCICommand;
import com.broadsoft.oci.OCIRequest;
import com.broadsoft.oci.OCIResponse;
import com.broadsoft.ocir.OCIReportingReportPublicIdentityNotification;

import de.qsc.broadsoft.oci.transport.DirectConnection;
import de.qsc.broadsoft.oci.transport.DirectReportingConnection;
import de.qsc.broadsoft.oci.transport.ProvisioningConnection;
import de.qsc.broadsoft.oci.transport.ReportingConnection;

/**
 * Container class managing all connections to and from an OCI-P/R enabled
 * server.
 * 
 * @author prelle
 *
 */
public class OCISession {

	private final static Logger LOGGER = System.getLogger("oci");
	
	private ProvisioningConnection ociP;
	private ReportingConnection ociR;
	private Collection<OCISessionListener> listener;

	//-----------------------------------------------------------------
	/**
	 * @param server The server to connect to
	 * @param user  OCI-P login
	 * @param pass  OCI-P password
	 * @throws OCIException 
	 * @throws IOException 
	 */
	public OCISession(Properties config) throws IOException, OCIException {
		LOGGER.log(Level.DEBUG,"Setting up OCI-P");
		ociP = new DirectConnection(config);
		
		// Eventually set up OCI-R
		if (config.getProperty(DirectReportingConnection.PROP_OCIR_SERVER)!=null) {
			LOGGER.log(Level.DEBUG,"Setting up OCI-R with "+config.getProperty(DirectReportingConnection.PROP_OCIR_SERVER));
			try {
				ociR = new DirectReportingConnection(config);
			} catch (Exception e) {
				LOGGER.log(Level.ERROR,"Failed setting up OCI-R to "+config.getProperty(DirectReportingConnection.PROP_OCIR_SERVER),e);
				System.err.println("Failed setting up OCI-R to "+config.getProperty(DirectReportingConnection.PROP_OCIR_SERVER)+": "+e);
			}
		}

		// Add listener
		listener = new ArrayList<OCISessionListener>();
		if (ociR!=null)
		ociR.addListener(new ReportingListener() {
			@Override
			public void provisioningExecuted(String userID, OCICommand command, String xml) {
				fireHandleReportingDocument(userID, command, xml);
			}
			public void publicIdentitiesChanged(OCIReportingReportPublicIdentityNotification notify) {
				// Inform listener
				for (OCISessionListener rcv : listener) {
					try {
						rcv.publicIdentitiesChanged(
								notify.getAddedUser(), 
								notify.getModifiedUser(), 
								notify.getDeletedUser());
					} catch (Exception e) {
						LOGGER.log(Level.ERROR,"Error delivering event: "+e,e);
					}
				}
			}
		});
	}

	//-----------------------------------------------------------------
	public OCIResponse send(OCIRequest request) throws OCIException, IOException {
		return ociP.sendRequest(request);
	}
	//-----------------------------------------------------------------
	public OCIResponse sendRequest(OCIRequest request) throws OCIException, IOException {
		return ociP.sendRequest(request);
	}

	//-----------------------------------------------------------------
	public void addListener(OCISessionListener eventListener) {
		if (!this.listener.contains(eventListener))
			listener.add(eventListener);
	}

	//-----------------------------------------------------------------
	private void fireHandleReportingDocument(String user, OCICommand command, String xml) {
		LOGGER.log(Level.DEBUG,"fireHandleReportingDoc "+command+"   listeners="+listener);
		OCIREvent event = new OCIREvent(this, user, command, xml);
		
		// One of the following may be useless.
		
		// Inform message bus
		OCIREventBus.fireEvent(event);
		// Inform listener
		for (OCISessionListener rcv : listener) {
			try {
				rcv.ociEventReceived(event);
			} catch (Exception e) {
				LOGGER.log(Level.ERROR,"Error delivering event: "+e,e);
			}
		}
	}

	//-----------------------------------------------------------------
	public DirectConnection getProvisioningConnection() {
		return (DirectConnection) ociP;
	}

}
