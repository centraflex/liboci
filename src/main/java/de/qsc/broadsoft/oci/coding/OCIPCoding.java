package de.qsc.broadsoft.oci.coding;

import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.PropertyException;
import jakarta.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;

import com.broadsoft.oci.ErrorResponse;
import com.broadsoft.oci.OCICommand;
import com.broadsoft.oci.OCIMessage;
import com.broadsoft.oci.OCIRequest;
import com.broadsoft.oci.ObjectFactory;

/**
 * @author prelle
 *
 */
public class OCIPCoding {

	private final static Logger LOGGER = System.getLogger("oci.coding");

	private static Pattern pattern = Pattern.compile("\\[[^\\d]* ([\\d]+)\\].*");

	private static ObjectFactory factory;
	private static JAXBContext jaxb;
	private static Marshaller marshaller;
	private static Unmarshaller unmarshaller;

	//-----------------------------------------------------------------
	static {
		LOGGER.log(Level.DEBUG,"Prepare JAXB");
		try {
			factory = new ObjectFactory();
			jaxb = JAXBContext.newInstance(ObjectFactory.class);
			marshaller = jaxb.createMarshaller();
			unmarshaller = jaxb.createUnmarshaller();
		} catch (JAXBException e) {
			LOGGER.log(Level.ERROR,"Could not initialize XML encoder/decoder",e);
		}
		LOGGER.log(Level.DEBUG,"Preparing JAXB done");
	}

	//-----------------------------------------------------------------
	public static ObjectFactory getFactory() {return factory;}

	//-----------------------------------------------------------------
	/**
	 * Just used for forcing a call to static constructor
	 */
	public static void init() {}

	//-----------------------------------------------------------------
	public static void setEncoding(Charset encoding) {
		LOGGER.log(Level.DEBUG,"Setting marshaller encoding to "+encoding.displayName()+" on marshaller "+marshaller);
		try {
			marshaller.setProperty("jaxb.encoding", encoding.displayName());
		} catch (PropertyException e) {
			LOGGER.log(Level.ERROR,"Failed setting JAXB encoding property: "+e,e);
		}
	}

	//	//-----------------------------------------------------------------
	//	private static String removeN2Prefix(String xml) {
	//		StringBuffer buf = new StringBuffer(xml);
	//		int pos=0;
	//		while (true) {
	//			int p0 = buf.indexOf("<ns2:sessionId", pos);
	//			int p1 = buf.indexOf("ns2:", pos);
	//			int p2 = buf.indexOf(":ns2", pos);
	//			int p4 = buf.indexOf("<ns2:command", pos);
	//			if (p0>0) {
	//				buf.replace(p0+1, p0+14, "sessionId xmlns=\"\"");
	//				continue;
	//			}
	//			if (p4>0) {
	//				buf.replace(p4+1, p4+12, "command xmlns=\"\"");
	//				continue;
	//			}
	//			if (p1==-1 && p2==-1)
	//				break;
	//			if (p1>0 && (p1<p2 || p2==-1)) {
	//				buf.replace(p1, p1+4, ""); pos=p1;
	//			} else if (p2>0 && (p2<p1 || p1==-1)) {
	//				buf.replace(p2, p2+4, ""); pos=p2;
	//			}
	//		}
	//		return buf.toString();
	//	}

	//-----------------------------------------------------------------
	public static String encode(OCIRequest request, String session) throws JAXBException {
		LOGGER.log(Level.TRACE,"encode : "+request);

		StringWriter writer = new StringWriter();

		synchronized (factory) {
			OCIMessage message = factory.createOCIMessage();
			message.setSessionId(session);
			message.setProtocol("OCI");
			message.getCommand().add(request);
			JAXBElement<OCIMessage> element = factory.createBroadsoftDocument(message);

			marshaller.marshal(element, writer);

			LOGGER.log(Level.TRACE,"encoded: "+writer);
			return writer.toString();
		}
	}

	//-----------------------------------------------------------------
	public static void encode(OCIRequest request, String session, Document doc) throws JAXBException {
		LOGGER.log(Level.TRACE,"encode : "+request);

		synchronized (factory) {
			OCIMessage message = factory.createOCIMessage();
			message.setSessionId(session);
			message.setProtocol("OCI");
			message.getCommand().add(request);
			JAXBElement<OCIMessage> element = factory.createBroadsoftDocument(message);

			marshaller.marshal(element, doc.getDocumentElement());

			LOGGER.log(Level.TRACE,"encoded: "+doc);
		}
	}

	//-----------------------------------------------------------------
	/**
	 * Fill error code field with value from summary, if necessary
	 */
	private static void fixErrorCodes(OCIMessage mess) {
		for (OCICommand com : mess.getCommand()) {
			if (com instanceof ErrorResponse) {
				ErrorResponse error = (ErrorResponse)com;
				if (error.getErrorCode()==null) {
					Matcher matcher = pattern.matcher(error.getSummary());
					if (matcher.matches())
						error.setErrorCode(Integer.parseInt(matcher.group(1)));
				}
			}
		}
	}

	//-----------------------------------------------------------------
	public static OCIMessage decode(String data) throws JAXBException {
		if (LOGGER.isLoggable(Level.TRACE))
			LOGGER.log(Level.TRACE,"decode : "+data);
		StreamSource s = new StreamSource(new StringReader(data));
		// Generic
		//		jaxb=JAXBContext.newInstance(ObjectFactory.class);
		synchronized (factory) {
			Object foo = unmarshaller.unmarshal(s);
			if (foo instanceof JAXBElement<?>)
				foo = ((JAXBElement<?>)foo).getValue();
			if (LOGGER.isLoggable(Level.TRACE))
				LOGGER.log(Level.TRACE,"decoded: "+foo);

			fixErrorCodes((OCIMessage)foo);
			return (OCIMessage)foo;
		}

	}

	//-----------------------------------------------------------------
	public static OCIMessage decode(InputStream ins) throws JAXBException {
		// Generic
		synchronized (factory) {
			Object foo = unmarshaller.unmarshal(ins);
			if (foo instanceof JAXBElement<?>)
				foo = ((JAXBElement<?>)foo).getValue();
			if (LOGGER.isLoggable(Level.TRACE))
				LOGGER.log(Level.TRACE,"decoded: "+foo);

			fixErrorCodes((OCIMessage)foo);
			return (OCIMessage)foo;
		}

	}

}
