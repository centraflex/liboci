/**
 *
 */
package de.qsc.broadsoft.oci.coding;

import java.io.StringReader;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.UnmarshalException;
import jakarta.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import com.broadsoft.ocir.OCIReportingMessage;
import com.broadsoft.ocir.ObjectFactory;

/**
 * @author prelle
 *
 */
public class OCIRCoding {

	private final static Logger LOGGER = System.getLogger("oci.coding");

	private static JAXBContext jaxb;
	private static Unmarshaller unmarshaller;

	//-----------------------------------------------------------------
	static {
		LOGGER.log(Level.DEBUG,"Prepare JAXB");
		try {
			jaxb = JAXBContext.newInstance(ObjectFactory.class);
			unmarshaller = jaxb.createUnmarshaller();
		} catch (JAXBException e) {
			LOGGER.log(Level.ERROR,"Could not initialize XML encoder/decoder",e);
			System.exit(1);
		}
		LOGGER.log(Level.DEBUG,"Prepare JAXB done");
	}

	//-----------------------------------------------------------------
	/**
	 * Just used for forcing a call to static constructor
	 */
	public static void init() {}

	//-----------------------------------------------------------------
	public static OCIReportingMessage decode(String data) throws JAXBException {
		LOGGER.log(Level.TRACE,"decode : "+data);
		StreamSource s = new StreamSource(new StringReader(data));
		// Generic
		try {
			Object foo = unmarshaller.unmarshal(s, OCIReportingMessage.class);
			if (foo instanceof JAXBElement<?>)
				foo = ((JAXBElement<?>)foo).getValue();
			LOGGER.log(Level.TRACE,"decoded: "+foo);

			return (OCIReportingMessage)foo;
		} catch (UnmarshalException e) {
			LOGGER.log(Level.ERROR,"Failed decoding message - "+e+"\nMessage was:\n"+data,e.getLinkedException());
			throw e;
		} catch (JAXBException e) {
			LOGGER.log(Level.ERROR,"Failed decoding message - "+e+"\nMessage was:\n"+data,e);
			throw e;
		} catch (Exception e) {
			LOGGER.log(Level.ERROR,"Failed decoding message - "+e+"\nMessage was:\n"+data,e);
			e.printStackTrace();
			throw new JAXBException("Decoding of message failed");
		}

	}
}
