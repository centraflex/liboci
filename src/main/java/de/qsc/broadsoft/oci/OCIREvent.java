/**
 * 
 */
package de.qsc.broadsoft.oci;

import java.util.HashMap;
import java.util.Map;

import com.broadsoft.oci.OCICommand;

/**
 * @author prelle
 *
 */
public class OCIREvent {

	private String user;
	private OCISession source;
	private OCICommand reported;
	private Map<String,Object> temporary;
	private String xml;
	
	//-----------------------------------------------------------------
	public OCIREvent(OCISession src, String user, OCICommand event, String xml) {
		source = src;
		this.user     = user;
		this.reported = event;
		this.xml      = xml;
		temporary = new HashMap<String,Object>();
	}

	//-----------------------------------------------------------------
	public String toString() {
		return user+" issued "+reported.getClass();
	}

	//-----------------------------------------------------------------
	/**
	 * @return
	 */
	public OCISession getSource() {
		return source;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the type
	 */
	public OCICommand getReported() {
		return reported;
	}

	//-----------------------------------------------------------------
	public void addTemporary(String key, Object obj) {
		temporary.put(key, obj);
	}

	//-----------------------------------------------------------------
	public Object getTemporary(String key) {
		return temporary.get(key);
	}

	//-----------------------------------------------------------------
	/**
	 * @return the xml
	 */
	public String getXml() {
		return xml;
	}

	//-----------------------------------------------------------------
	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}
	
}
