/**
 * 
 */
package de.qsc.broadsoft.oci;

/**
 * @author prelle
 *
 */
public interface OCIREventListener {

	public void handleOCIR(OCIREvent event);
	
}
