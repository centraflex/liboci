/**
 * 
 */
package de.qsc.broadsoft.oci;

import java.io.IOException;

import de.qsc.broadsoft.oci.coding.OCIPCoding;
import de.qsc.broadsoft.oci.coding.OCIRCoding;
import de.qsc.broadsoft.oci.transport.DirectConnection;
import de.qsc.broadsoft.oci.transport.DirectReportingConnection;
import de.qsc.broadsoft.oci.transport.ProvisioningConnection;
import de.qsc.broadsoft.oci.transport.ReportingConnection;

/**
 * @author prelle
 *
 */
public class OCIFactory {

	//-----------------------------------------------------------------
	static {
		OCIPCoding.init();
		OCIRCoding.init();
	}
	
	//-----------------------------------------------------------------
	public static ProvisioningConnection createDirectProvisioningConnection(String host, String user, String password, String encoding) throws IOException, OCIException {
		return new DirectConnection(host, user, password, encoding);
	}
	
	//-----------------------------------------------------------------
	public static ReportingConnection createReportingConnection(String host, ReportingListener listener) throws IOException, OCIException {
		ReportingConnection con = new DirectReportingConnection(host);
		con.addListener(listener);
		return con;
	}
	
}
