//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PersonalAssistantPresence.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="PersonalAssistantPresence">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="None"/>
 *     <enumeration value="Business Trip"/>
 *     <enumeration value="Gone for the Day"/>
 *     <enumeration value="Lunch"/>
 *     <enumeration value="Meeting"/>
 *     <enumeration value="Out Of Office"/>
 *     <enumeration value="Temporarily Out"/>
 *     <enumeration value="Training"/>
 *     <enumeration value="Unavailable"/>
 *     <enumeration value="Vacation"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "PersonalAssistantPresence")
@XmlEnum
public enum PersonalAssistantPresence {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Business Trip")
    BUSINESS_TRIP("Business Trip"),
    @XmlEnumValue("Gone for the Day")
    GONE_FOR_THE_DAY("Gone for the Day"),
    @XmlEnumValue("Lunch")
    LUNCH("Lunch"),
    @XmlEnumValue("Meeting")
    MEETING("Meeting"),
    @XmlEnumValue("Out Of Office")
    OUT_OF_OFFICE("Out Of Office"),
    @XmlEnumValue("Temporarily Out")
    TEMPORARILY_OUT("Temporarily Out"),
    @XmlEnumValue("Training")
    TRAINING("Training"),
    @XmlEnumValue("Unavailable")
    UNAVAILABLE("Unavailable"),
    @XmlEnumValue("Vacation")
    VACATION("Vacation");
    private final String value;

    PersonalAssistantPresence(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PersonalAssistantPresence fromValue(String v) {
        for (PersonalAssistantPresence c: PersonalAssistantPresence.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
