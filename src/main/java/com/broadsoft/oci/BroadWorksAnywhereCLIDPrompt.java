//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für BroadWorksAnywhereCLIDPrompt.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="BroadWorksAnywhereCLIDPrompt">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Always Prompt"/>
 *     <enumeration value="Never Prompt"/>
 *     <enumeration value="Prompt When Not Available"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "BroadWorksAnywhereCLIDPrompt")
@XmlEnum
public enum BroadWorksAnywhereCLIDPrompt {

    @XmlEnumValue("Always Prompt")
    ALWAYS_PROMPT("Always Prompt"),
    @XmlEnumValue("Never Prompt")
    NEVER_PROMPT("Never Prompt"),
    @XmlEnumValue("Prompt When Not Available")
    PROMPT_WHEN_NOT_AVAILABLE("Prompt When Not Available");
    private final String value;

    BroadWorksAnywhereCLIDPrompt(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BroadWorksAnywhereCLIDPrompt fromValue(String v) {
        for (BroadWorksAnywhereCLIDPrompt c: BroadWorksAnywhereCLIDPrompt.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
