//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify system number activation and enterprise trunk number range activation setting.
 *         The response is either SuccessResponse or ErrorResponse.
 * 
 *         The following element values are only applicable in AS data mode:
 *           enableEnterpriseTrunkNumberRangeActivation
 *           numberActivationMode = Group And User Activation Enabled will raise an error in XS data mode.
 *       
 * 
 * <p>Java-Klasse für SystemNumberActivationModifyRequest18sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNumberActivationModifyRequest18sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="numberActivationMode" type="{}NumberActivationMode" minOccurs="0"/>
 *         <element name="enableEnterpriseTrunkNumberRangeActivation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNumberActivationModifyRequest18sp1", propOrder = {
    "numberActivationMode",
    "enableEnterpriseTrunkNumberRangeActivation"
})
public class SystemNumberActivationModifyRequest18Sp1
    extends OCIRequest
{

    @XmlSchemaType(name = "token")
    protected NumberActivationMode numberActivationMode;
    protected Boolean enableEnterpriseTrunkNumberRangeActivation;

    /**
     * Ruft den Wert der numberActivationMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NumberActivationMode }
     *     
     */
    public NumberActivationMode getNumberActivationMode() {
        return numberActivationMode;
    }

    /**
     * Legt den Wert der numberActivationMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberActivationMode }
     *     
     */
    public void setNumberActivationMode(NumberActivationMode value) {
        this.numberActivationMode = value;
    }

    /**
     * Ruft den Wert der enableEnterpriseTrunkNumberRangeActivation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableEnterpriseTrunkNumberRangeActivation() {
        return enableEnterpriseTrunkNumberRangeActivation;
    }

    /**
     * Legt den Wert der enableEnterpriseTrunkNumberRangeActivation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableEnterpriseTrunkNumberRangeActivation(Boolean value) {
        this.enableEnterpriseTrunkNumberRangeActivation = value;
    }

}
