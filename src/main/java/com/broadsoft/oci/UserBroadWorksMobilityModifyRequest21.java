//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the user's BroadWorks Mobility service settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *                   
 *         The isActive, useMobileIdentityCallAnchoring, and preventCallsToOwnMobiles parameters can be modified by users with Group Authorization Level and above.
 *         The mobileIdentity, profileIdentityDevicesToRing, rofileIdentityIncludeSharedCallAppearance, profileIdentityIncludeBroadworksAnywhere mobileIdentity and profileIdentityMobilityNumbersAlerted parameters can be modified by users with User Authorization Level and above when the BroadWorks Mobility service is turned on.
 *       
 * 
 * <p>Java-Klasse für UserBroadWorksMobilityModifyRequest21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksMobilityModifyRequest21">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useMobileIdentityCallAnchoring" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="preventCallsToOwnMobiles" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="mobileIdentity" type="{}BroadWorksMobilityUserMobileIdentityModifyEntry" maxOccurs="10" minOccurs="0"/>
 *         <element name="profileIdentityDevicesToRing" type="{}BroadWorksMobilityPhoneToRing" minOccurs="0"/>
 *         <element name="profileIdentityIncludeSharedCallAppearance" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="profileIdentityIncludeBroadworksAnywhere" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="profileIdentityIncludeExecutiveAssistant" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="profileIdentityMobilityNumbersAlerted" type="{}BroadWorksMobilityAlertingMobileNumberReplacementList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksMobilityModifyRequest21", propOrder = {
    "userId",
    "isActive",
    "useMobileIdentityCallAnchoring",
    "preventCallsToOwnMobiles",
    "mobileIdentity",
    "profileIdentityDevicesToRing",
    "profileIdentityIncludeSharedCallAppearance",
    "profileIdentityIncludeBroadworksAnywhere",
    "profileIdentityIncludeExecutiveAssistant",
    "profileIdentityMobilityNumbersAlerted"
})
public class UserBroadWorksMobilityModifyRequest21
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean isActive;
    protected Boolean useMobileIdentityCallAnchoring;
    protected Boolean preventCallsToOwnMobiles;
    protected List<BroadWorksMobilityUserMobileIdentityModifyEntry> mobileIdentity;
    @XmlSchemaType(name = "token")
    protected BroadWorksMobilityPhoneToRing profileIdentityDevicesToRing;
    protected Boolean profileIdentityIncludeSharedCallAppearance;
    protected Boolean profileIdentityIncludeBroadworksAnywhere;
    protected Boolean profileIdentityIncludeExecutiveAssistant;
    @XmlElementRef(name = "profileIdentityMobilityNumbersAlerted", type = JAXBElement.class, required = false)
    protected JAXBElement<BroadWorksMobilityAlertingMobileNumberReplacementList> profileIdentityMobilityNumbersAlerted;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der useMobileIdentityCallAnchoring-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMobileIdentityCallAnchoring() {
        return useMobileIdentityCallAnchoring;
    }

    /**
     * Legt den Wert der useMobileIdentityCallAnchoring-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMobileIdentityCallAnchoring(Boolean value) {
        this.useMobileIdentityCallAnchoring = value;
    }

    /**
     * Ruft den Wert der preventCallsToOwnMobiles-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPreventCallsToOwnMobiles() {
        return preventCallsToOwnMobiles;
    }

    /**
     * Legt den Wert der preventCallsToOwnMobiles-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreventCallsToOwnMobiles(Boolean value) {
        this.preventCallsToOwnMobiles = value;
    }

    /**
     * Gets the value of the mobileIdentity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the mobileIdentity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMobileIdentity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BroadWorksMobilityUserMobileIdentityModifyEntry }
     * 
     * 
     * @return
     *     The value of the mobileIdentity property.
     */
    public List<BroadWorksMobilityUserMobileIdentityModifyEntry> getMobileIdentity() {
        if (mobileIdentity == null) {
            mobileIdentity = new ArrayList<>();
        }
        return this.mobileIdentity;
    }

    /**
     * Ruft den Wert der profileIdentityDevicesToRing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksMobilityPhoneToRing }
     *     
     */
    public BroadWorksMobilityPhoneToRing getProfileIdentityDevicesToRing() {
        return profileIdentityDevicesToRing;
    }

    /**
     * Legt den Wert der profileIdentityDevicesToRing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksMobilityPhoneToRing }
     *     
     */
    public void setProfileIdentityDevicesToRing(BroadWorksMobilityPhoneToRing value) {
        this.profileIdentityDevicesToRing = value;
    }

    /**
     * Ruft den Wert der profileIdentityIncludeSharedCallAppearance-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProfileIdentityIncludeSharedCallAppearance() {
        return profileIdentityIncludeSharedCallAppearance;
    }

    /**
     * Legt den Wert der profileIdentityIncludeSharedCallAppearance-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProfileIdentityIncludeSharedCallAppearance(Boolean value) {
        this.profileIdentityIncludeSharedCallAppearance = value;
    }

    /**
     * Ruft den Wert der profileIdentityIncludeBroadworksAnywhere-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProfileIdentityIncludeBroadworksAnywhere() {
        return profileIdentityIncludeBroadworksAnywhere;
    }

    /**
     * Legt den Wert der profileIdentityIncludeBroadworksAnywhere-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProfileIdentityIncludeBroadworksAnywhere(Boolean value) {
        this.profileIdentityIncludeBroadworksAnywhere = value;
    }

    /**
     * Ruft den Wert der profileIdentityIncludeExecutiveAssistant-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProfileIdentityIncludeExecutiveAssistant() {
        return profileIdentityIncludeExecutiveAssistant;
    }

    /**
     * Legt den Wert der profileIdentityIncludeExecutiveAssistant-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProfileIdentityIncludeExecutiveAssistant(Boolean value) {
        this.profileIdentityIncludeExecutiveAssistant = value;
    }

    /**
     * Ruft den Wert der profileIdentityMobilityNumbersAlerted-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BroadWorksMobilityAlertingMobileNumberReplacementList }{@code >}
     *     
     */
    public JAXBElement<BroadWorksMobilityAlertingMobileNumberReplacementList> getProfileIdentityMobilityNumbersAlerted() {
        return profileIdentityMobilityNumbersAlerted;
    }

    /**
     * Legt den Wert der profileIdentityMobilityNumbersAlerted-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BroadWorksMobilityAlertingMobileNumberReplacementList }{@code >}
     *     
     */
    public void setProfileIdentityMobilityNumbersAlerted(JAXBElement<BroadWorksMobilityAlertingMobileNumberReplacementList> value) {
        this.profileIdentityMobilityNumbersAlerted = value;
    }

}
