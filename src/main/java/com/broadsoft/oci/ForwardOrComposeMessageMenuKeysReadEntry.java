//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal forward or compose message menu keys.
 *       
 * 
 * <p>Java-Klasse für ForwardOrComposeMessageMenuKeysReadEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ForwardOrComposeMessageMenuKeysReadEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="sendToPerson" type="{}DigitAny" minOccurs="0"/>
 *         <element name="sendToAllGroupMembers" type="{}DigitAny" minOccurs="0"/>
 *         <element name="sendToDistributionList" type="{}DigitAny" minOccurs="0"/>
 *         <element name="changeCurrentIntroductionOrMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="listenToCurrentIntroductionOrMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setOrClearUrgentIndicator" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setOrClearConfidentialIndicator" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ForwardOrComposeMessageMenuKeysReadEntry", propOrder = {
    "sendToPerson",
    "sendToAllGroupMembers",
    "sendToDistributionList",
    "changeCurrentIntroductionOrMessage",
    "listenToCurrentIntroductionOrMessage",
    "setOrClearUrgentIndicator",
    "setOrClearConfidentialIndicator",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class ForwardOrComposeMessageMenuKeysReadEntry {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sendToPerson;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sendToAllGroupMembers;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sendToDistributionList;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String changeCurrentIntroductionOrMessage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String listenToCurrentIntroductionOrMessage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String setOrClearUrgentIndicator;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String setOrClearConfidentialIndicator;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String repeatMenu;

    /**
     * Ruft den Wert der sendToPerson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendToPerson() {
        return sendToPerson;
    }

    /**
     * Legt den Wert der sendToPerson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendToPerson(String value) {
        this.sendToPerson = value;
    }

    /**
     * Ruft den Wert der sendToAllGroupMembers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendToAllGroupMembers() {
        return sendToAllGroupMembers;
    }

    /**
     * Legt den Wert der sendToAllGroupMembers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendToAllGroupMembers(String value) {
        this.sendToAllGroupMembers = value;
    }

    /**
     * Ruft den Wert der sendToDistributionList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendToDistributionList() {
        return sendToDistributionList;
    }

    /**
     * Legt den Wert der sendToDistributionList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendToDistributionList(String value) {
        this.sendToDistributionList = value;
    }

    /**
     * Ruft den Wert der changeCurrentIntroductionOrMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeCurrentIntroductionOrMessage() {
        return changeCurrentIntroductionOrMessage;
    }

    /**
     * Legt den Wert der changeCurrentIntroductionOrMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeCurrentIntroductionOrMessage(String value) {
        this.changeCurrentIntroductionOrMessage = value;
    }

    /**
     * Ruft den Wert der listenToCurrentIntroductionOrMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListenToCurrentIntroductionOrMessage() {
        return listenToCurrentIntroductionOrMessage;
    }

    /**
     * Legt den Wert der listenToCurrentIntroductionOrMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListenToCurrentIntroductionOrMessage(String value) {
        this.listenToCurrentIntroductionOrMessage = value;
    }

    /**
     * Ruft den Wert der setOrClearUrgentIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetOrClearUrgentIndicator() {
        return setOrClearUrgentIndicator;
    }

    /**
     * Legt den Wert der setOrClearUrgentIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetOrClearUrgentIndicator(String value) {
        this.setOrClearUrgentIndicator = value;
    }

    /**
     * Ruft den Wert der setOrClearConfidentialIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetOrClearConfidentialIndicator() {
        return setOrClearConfidentialIndicator;
    }

    /**
     * Legt den Wert der setOrClearConfidentialIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetOrClearConfidentialIndicator(String value) {
        this.setOrClearConfidentialIndicator = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepeatMenu(String value) {
        this.repeatMenu = value;
    }

}
