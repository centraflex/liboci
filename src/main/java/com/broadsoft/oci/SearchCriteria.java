//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Abstract base type for specifying search criteria. A search criteria is an optional element
 *         used to restrict the number of rows returned when requesting a potentially large set of
 *         data from the provisioning server.
 *       
 * 
 * <p>Java-Klasse für SearchCriteria complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteria">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteria")
@XmlSeeAlso({
    SearchCriteriaAccessDeviceEndpointPrivateIdentity.class,
    SearchCriteriaAccessDeviceVersion.class,
    SearchCriteriaAdminFirstName.class,
    SearchCriteriaAdminId.class,
    SearchCriteriaAdminLastName.class,
    SearchCriteriaAgentThresholdProfile.class,
    SearchCriteriaAlternateTrunkIdentity.class,
    SearchCriteriaAlternateTrunkIdentityDomain.class,
    SearchCriteriaAlternateUserId.class,
    SearchCriteriaAnnouncementFileName.class,
    SearchCriteriaCallCenterName.class,
    SearchCriteriaCallCenterReportTemplateName.class,
    SearchCriteriaCallCenterScheduledReportName.class,
    SearchCriteriaCallParkName.class,
    SearchCriteriaCallPickupName.class,
    SearchCriteriaCommunicationBarringAuthorizationCode.class,
    SearchCriteriaCommunicationBarringAuthorizationCodeDescription.class,
    SearchCriteriaDepartmentName.class,
    SearchCriteriaDeviceMACAddress.class,
    SearchCriteriaDeviceManagementEventAdditionalInfo.class,
    SearchCriteriaDeviceManagementEventLoginId.class,
    SearchCriteriaDeviceName.class,
    SearchCriteriaDeviceNetAddress.class,
    SearchCriteriaDeviceSerialNumber.class,
    SearchCriteriaDeviceType.class,
    SearchCriteriaDigitPattern.class,
    SearchCriteriaDn.class,
    SearchCriteriaDomainName.class,
    SearchCriteriaEmailAddress.class,
    SearchCriteriaEnterpriseCommonMultiPartPhoneListName.class,
    SearchCriteriaEnterpriseCommonPhoneListName.class,
    SearchCriteriaEnterpriseCommonPhoneListNumber.class,
    SearchCriteriaEnterpriseTrunkName.class,
    SearchCriteriaExactAnnouncementFileType.class,
    SearchCriteriaExactAutoAttendantType.class,
    SearchCriteriaExactCallCenterReportTemplateKey.class,
    SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor.class,
    SearchCriteriaExactCallCenterScheduledReportGroup.class,
    SearchCriteriaExactCallCenterScheduledReportServiceProvider.class,
    SearchCriteriaExactCallCenterType.class,
    SearchCriteriaExactCustomContactDirectory.class,
    SearchCriteriaExactDeviceLevel.class,
    SearchCriteriaExactDeviceManagementEventAction.class,
    SearchCriteriaExactDeviceManagementEventLevel.class,
    SearchCriteriaExactDeviceManagementEventStatusCompleted.class,
    SearchCriteriaExactDeviceManagementEventStatusInProgressOrPending.class,
    SearchCriteriaExactDeviceManagementEventType.class,
    SearchCriteriaExactDeviceServiceProvider.class,
    SearchCriteriaExactDeviceType.class,
    SearchCriteriaExactDeviceTypeConfigurationOptionType.class,
    SearchCriteriaExactDnActivation.class,
    SearchCriteriaExactDnAvailability.class,
    SearchCriteriaExactDnDepartment.class,
    SearchCriteriaExactDomainLevel.class,
    SearchCriteriaExactEndpointType21Sp1 .class,
    SearchCriteriaExactGroupAdminType.class,
    SearchCriteriaExactHuntPolicy.class,
    SearchCriteriaExactLocationEnabled.class,
    SearchCriteriaExactMediaFileType.class,
    SearchCriteriaExactMobileDnAvailability.class,
    SearchCriteriaExactMobileNetwork.class,
    SearchCriteriaExactOrganizationType.class,
    SearchCriteriaExactPolicySelection.class,
    SearchCriteriaExactPortNumber.class,
    SearchCriteriaExactScheduleLevel.class,
    SearchCriteriaExactScheduleType.class,
    SearchCriteriaExactServiceProvider.class,
    SearchCriteriaExactServiceProviderAdminType.class,
    SearchCriteriaExactServiceType.class,
    SearchCriteriaExactSignalingAddressType.class,
    SearchCriteriaExactSkillLevel.class,
    SearchCriteriaExactUserDepartment.class,
    SearchCriteriaExactUserGroup.class,
    SearchCriteriaExactUserInTrunkGroup.class,
    SearchCriteriaExactUserNetworkClassOfService.class,
    SearchCriteriaExactUserRouteListAssigned.class,
    SearchCriteriaExactUserRouteListAssignment.class,
    SearchCriteriaExactUserType.class,
    SearchCriteriaExactVirtualOnNetCallTypeName.class,
    SearchCriteriaExtension.class,
    SearchCriteriaForwardedToNumber.class,
    SearchCriteriaGroupCommonMultiPartPhoneListName.class,
    SearchCriteriaGroupCommonPhoneListName.class,
    SearchCriteriaGroupCommonPhoneListNumber.class,
    SearchCriteriaGroupExternalId.class,
    SearchCriteriaGroupId.class,
    SearchCriteriaGroupLocationCode.class,
    SearchCriteriaGroupName.class,
    SearchCriteriaHomeMscAddress.class,
    SearchCriteriaImpId.class,
    SearchCriteriaIMRN.class,
    SearchCriteriaLanguage.class,
    SearchCriteriaLinePortDomain.class,
    SearchCriteriaLinePortUserPart.class,
    SearchCriteriaLocation.class,
    SearchCriteriaLoginId.class,
    SearchCriteriaMobilePhoneNumber.class,
    SearchCriteriaMobileSubscriberDirectoryNumber.class,
    SearchCriteriaMultiPartUserName.class,
    SearchCriteriaNetworkClassOfServiceName.class,
    SearchCriteriaNumberPortabilityQueryDigitPattern.class,
    SearchCriteriaNumberPortabilityStatus.class,
    SearchCriteriaOutgoingDNorSIPURI.class,
    SearchCriteriaPersonalAssistantExclusionNumber.class,
    SearchCriteriaPersonalAssistantExclusionNumberDescription.class,
    SearchCriteriaPhysicalLocation.class,
    SearchCriteriaProfileServiceCode.class,
    SearchCriteriaProfileServiceCodeDescription.class,
    SearchCriteriaReceptionistNote.class,
    SearchCriteriaRegistrationURI.class,
    SearchCriteriaResellerId.class,
    SearchCriteriaResellerName.class,
    SearchCriteriaRoamingMscAddress.class,
    SearchCriteriaRoutePointName.class,
    SearchCriteriaScheduleName.class,
    SearchCriteriaServiceCode.class,
    SearchCriteriaServiceCodeDescription.class,
    SearchCriteriaServiceInstanceName.class,
    SearchCriteriaServiceProviderId.class,
    SearchCriteriaServiceProviderName.class,
    SearchCriteriaServiceProviderNumberPortabilityQueryDigitPattern.class,
    SearchCriteriaServiceStatus.class,
    SearchCriteriaSIPContact.class,
    SearchCriteriaSystemServiceDn.class,
    SearchCriteriaTitle.class,
    SearchCriteriaTrunkGroupName.class,
    SearchCriteriaUserExternalId.class,
    SearchCriteriaUserFirstName.class,
    SearchCriteriaUserHotlineContact.class,
    SearchCriteriaUserId.class,
    SearchCriteriaUserLastName.class,
    SearchCriteriaUserName.class,
    SearchCriteriaUserPersonalMultiPartPhoneListName.class,
    SearchCriteriaUserPersonalPhoneListName.class,
    SearchCriteriaUserPersonalPhoneListNumber.class,
    SearchCriteriaUserPlaceType.class,
    SearchCriteriaYahooId.class,
    SearchCriteriaZoneIPAddress.class,
    SearchCriteriaExactEndpointType.class
})
public abstract class SearchCriteria {


}
