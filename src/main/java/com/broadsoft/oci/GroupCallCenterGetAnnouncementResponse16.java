//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupCallCenterGetAnnouncementRequest16.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterGetAnnouncementResponse16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterGetAnnouncementResponse16">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="playEntranceMessage" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="mandatoryEntranceMessage" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="entranceAudioMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="entranceAudioFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="entranceMessageAudioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="entranceAudioFileMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="entranceVideoMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="entranceVideoFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="entranceMessageVideoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="entranceVideoFileMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="playPeriodicComfortMessage" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="timeBetweenComfortMessagesSeconds" type="{}CallCenterTimeBetweenComfortMessagesSeconds"/>
 *         <element name="periodicComfortAudioMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="periodicComfortAudioFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="periodicComfortMessageAudioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="periodicComfortAudioFileMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="periodicComfortVideoMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="periodicComfortVideoFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="periodicComfortMessageVideoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="periodicComfortVideoFileMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="enableMediaOnHoldForQueuedCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="mediaOnHoldSource" type="{}CallCenterMediaOnHoldSourceRead16"/>
 *         <element name="mediaOnHoldUseAlternateSourceForInternalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="mediaOnHoldInternalSource" type="{}CallCenterMediaOnHoldSourceRead16" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterGetAnnouncementResponse16", propOrder = {
    "playEntranceMessage",
    "mandatoryEntranceMessage",
    "entranceAudioMessageSelection",
    "entranceAudioFileUrl",
    "entranceMessageAudioFileDescription",
    "entranceAudioFileMediaType",
    "entranceVideoMessageSelection",
    "entranceVideoFileUrl",
    "entranceMessageVideoFileDescription",
    "entranceVideoFileMediaType",
    "playPeriodicComfortMessage",
    "timeBetweenComfortMessagesSeconds",
    "periodicComfortAudioMessageSelection",
    "periodicComfortAudioFileUrl",
    "periodicComfortMessageAudioFileDescription",
    "periodicComfortAudioFileMediaType",
    "periodicComfortVideoMessageSelection",
    "periodicComfortVideoFileUrl",
    "periodicComfortMessageVideoFileDescription",
    "periodicComfortVideoFileMediaType",
    "enableMediaOnHoldForQueuedCalls",
    "mediaOnHoldSource",
    "mediaOnHoldUseAlternateSourceForInternalCalls",
    "mediaOnHoldInternalSource"
})
public class GroupCallCenterGetAnnouncementResponse16
    extends OCIDataResponse
{

    protected boolean playEntranceMessage;
    protected boolean mandatoryEntranceMessage;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection entranceAudioMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String entranceAudioFileUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String entranceMessageAudioFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String entranceAudioFileMediaType;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection entranceVideoMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String entranceVideoFileUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String entranceMessageVideoFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String entranceVideoFileMediaType;
    protected boolean playPeriodicComfortMessage;
    protected int timeBetweenComfortMessagesSeconds;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection periodicComfortAudioMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String periodicComfortAudioFileUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String periodicComfortMessageAudioFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String periodicComfortAudioFileMediaType;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection periodicComfortVideoMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String periodicComfortVideoFileUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String periodicComfortMessageVideoFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String periodicComfortVideoFileMediaType;
    protected boolean enableMediaOnHoldForQueuedCalls;
    @XmlElement(required = true)
    protected CallCenterMediaOnHoldSourceRead16 mediaOnHoldSource;
    protected Boolean mediaOnHoldUseAlternateSourceForInternalCalls;
    protected CallCenterMediaOnHoldSourceRead16 mediaOnHoldInternalSource;

    /**
     * Ruft den Wert der playEntranceMessage-Eigenschaft ab.
     * 
     */
    public boolean isPlayEntranceMessage() {
        return playEntranceMessage;
    }

    /**
     * Legt den Wert der playEntranceMessage-Eigenschaft fest.
     * 
     */
    public void setPlayEntranceMessage(boolean value) {
        this.playEntranceMessage = value;
    }

    /**
     * Ruft den Wert der mandatoryEntranceMessage-Eigenschaft ab.
     * 
     */
    public boolean isMandatoryEntranceMessage() {
        return mandatoryEntranceMessage;
    }

    /**
     * Legt den Wert der mandatoryEntranceMessage-Eigenschaft fest.
     * 
     */
    public void setMandatoryEntranceMessage(boolean value) {
        this.mandatoryEntranceMessage = value;
    }

    /**
     * Ruft den Wert der entranceAudioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getEntranceAudioMessageSelection() {
        return entranceAudioMessageSelection;
    }

    /**
     * Legt den Wert der entranceAudioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setEntranceAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.entranceAudioMessageSelection = value;
    }

    /**
     * Ruft den Wert der entranceAudioFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntranceAudioFileUrl() {
        return entranceAudioFileUrl;
    }

    /**
     * Legt den Wert der entranceAudioFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntranceAudioFileUrl(String value) {
        this.entranceAudioFileUrl = value;
    }

    /**
     * Ruft den Wert der entranceMessageAudioFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntranceMessageAudioFileDescription() {
        return entranceMessageAudioFileDescription;
    }

    /**
     * Legt den Wert der entranceMessageAudioFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntranceMessageAudioFileDescription(String value) {
        this.entranceMessageAudioFileDescription = value;
    }

    /**
     * Ruft den Wert der entranceAudioFileMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntranceAudioFileMediaType() {
        return entranceAudioFileMediaType;
    }

    /**
     * Legt den Wert der entranceAudioFileMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntranceAudioFileMediaType(String value) {
        this.entranceAudioFileMediaType = value;
    }

    /**
     * Ruft den Wert der entranceVideoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getEntranceVideoMessageSelection() {
        return entranceVideoMessageSelection;
    }

    /**
     * Legt den Wert der entranceVideoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setEntranceVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.entranceVideoMessageSelection = value;
    }

    /**
     * Ruft den Wert der entranceVideoFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntranceVideoFileUrl() {
        return entranceVideoFileUrl;
    }

    /**
     * Legt den Wert der entranceVideoFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntranceVideoFileUrl(String value) {
        this.entranceVideoFileUrl = value;
    }

    /**
     * Ruft den Wert der entranceMessageVideoFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntranceMessageVideoFileDescription() {
        return entranceMessageVideoFileDescription;
    }

    /**
     * Legt den Wert der entranceMessageVideoFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntranceMessageVideoFileDescription(String value) {
        this.entranceMessageVideoFileDescription = value;
    }

    /**
     * Ruft den Wert der entranceVideoFileMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntranceVideoFileMediaType() {
        return entranceVideoFileMediaType;
    }

    /**
     * Legt den Wert der entranceVideoFileMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntranceVideoFileMediaType(String value) {
        this.entranceVideoFileMediaType = value;
    }

    /**
     * Ruft den Wert der playPeriodicComfortMessage-Eigenschaft ab.
     * 
     */
    public boolean isPlayPeriodicComfortMessage() {
        return playPeriodicComfortMessage;
    }

    /**
     * Legt den Wert der playPeriodicComfortMessage-Eigenschaft fest.
     * 
     */
    public void setPlayPeriodicComfortMessage(boolean value) {
        this.playPeriodicComfortMessage = value;
    }

    /**
     * Ruft den Wert der timeBetweenComfortMessagesSeconds-Eigenschaft ab.
     * 
     */
    public int getTimeBetweenComfortMessagesSeconds() {
        return timeBetweenComfortMessagesSeconds;
    }

    /**
     * Legt den Wert der timeBetweenComfortMessagesSeconds-Eigenschaft fest.
     * 
     */
    public void setTimeBetweenComfortMessagesSeconds(int value) {
        this.timeBetweenComfortMessagesSeconds = value;
    }

    /**
     * Ruft den Wert der periodicComfortAudioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getPeriodicComfortAudioMessageSelection() {
        return periodicComfortAudioMessageSelection;
    }

    /**
     * Legt den Wert der periodicComfortAudioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setPeriodicComfortAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.periodicComfortAudioMessageSelection = value;
    }

    /**
     * Ruft den Wert der periodicComfortAudioFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodicComfortAudioFileUrl() {
        return periodicComfortAudioFileUrl;
    }

    /**
     * Legt den Wert der periodicComfortAudioFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodicComfortAudioFileUrl(String value) {
        this.periodicComfortAudioFileUrl = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageAudioFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodicComfortMessageAudioFileDescription() {
        return periodicComfortMessageAudioFileDescription;
    }

    /**
     * Legt den Wert der periodicComfortMessageAudioFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodicComfortMessageAudioFileDescription(String value) {
        this.periodicComfortMessageAudioFileDescription = value;
    }

    /**
     * Ruft den Wert der periodicComfortAudioFileMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodicComfortAudioFileMediaType() {
        return periodicComfortAudioFileMediaType;
    }

    /**
     * Legt den Wert der periodicComfortAudioFileMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodicComfortAudioFileMediaType(String value) {
        this.periodicComfortAudioFileMediaType = value;
    }

    /**
     * Ruft den Wert der periodicComfortVideoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getPeriodicComfortVideoMessageSelection() {
        return periodicComfortVideoMessageSelection;
    }

    /**
     * Legt den Wert der periodicComfortVideoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setPeriodicComfortVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.periodicComfortVideoMessageSelection = value;
    }

    /**
     * Ruft den Wert der periodicComfortVideoFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodicComfortVideoFileUrl() {
        return periodicComfortVideoFileUrl;
    }

    /**
     * Legt den Wert der periodicComfortVideoFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodicComfortVideoFileUrl(String value) {
        this.periodicComfortVideoFileUrl = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageVideoFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodicComfortMessageVideoFileDescription() {
        return periodicComfortMessageVideoFileDescription;
    }

    /**
     * Legt den Wert der periodicComfortMessageVideoFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodicComfortMessageVideoFileDescription(String value) {
        this.periodicComfortMessageVideoFileDescription = value;
    }

    /**
     * Ruft den Wert der periodicComfortVideoFileMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodicComfortVideoFileMediaType() {
        return periodicComfortVideoFileMediaType;
    }

    /**
     * Legt den Wert der periodicComfortVideoFileMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodicComfortVideoFileMediaType(String value) {
        this.periodicComfortVideoFileMediaType = value;
    }

    /**
     * Ruft den Wert der enableMediaOnHoldForQueuedCalls-Eigenschaft ab.
     * 
     */
    public boolean isEnableMediaOnHoldForQueuedCalls() {
        return enableMediaOnHoldForQueuedCalls;
    }

    /**
     * Legt den Wert der enableMediaOnHoldForQueuedCalls-Eigenschaft fest.
     * 
     */
    public void setEnableMediaOnHoldForQueuedCalls(boolean value) {
        this.enableMediaOnHoldForQueuedCalls = value;
    }

    /**
     * Ruft den Wert der mediaOnHoldSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldSourceRead16 }
     *     
     */
    public CallCenterMediaOnHoldSourceRead16 getMediaOnHoldSource() {
        return mediaOnHoldSource;
    }

    /**
     * Legt den Wert der mediaOnHoldSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldSourceRead16 }
     *     
     */
    public void setMediaOnHoldSource(CallCenterMediaOnHoldSourceRead16 value) {
        this.mediaOnHoldSource = value;
    }

    /**
     * Ruft den Wert der mediaOnHoldUseAlternateSourceForInternalCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMediaOnHoldUseAlternateSourceForInternalCalls() {
        return mediaOnHoldUseAlternateSourceForInternalCalls;
    }

    /**
     * Legt den Wert der mediaOnHoldUseAlternateSourceForInternalCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMediaOnHoldUseAlternateSourceForInternalCalls(Boolean value) {
        this.mediaOnHoldUseAlternateSourceForInternalCalls = value;
    }

    /**
     * Ruft den Wert der mediaOnHoldInternalSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldSourceRead16 }
     *     
     */
    public CallCenterMediaOnHoldSourceRead16 getMediaOnHoldInternalSource() {
        return mediaOnHoldInternalSource;
    }

    /**
     * Legt den Wert der mediaOnHoldInternalSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldSourceRead16 }
     *     
     */
    public void setMediaOnHoldInternalSource(CallCenterMediaOnHoldSourceRead16 value) {
        this.mediaOnHoldInternalSource = value;
    }

}
