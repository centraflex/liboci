//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemLocationBasedCallingRestrictionsGetRequest17sp3.
 *       
 * 
 * <p>Java-Klasse für SystemLocationBasedCallingRestrictionsGetResponse17sp3 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemLocationBasedCallingRestrictionsGetResponse17sp3">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="physicalLocationIndicator" type="{}PhysicalLocationIndicator"/>
 *         <element name="enforceMscValidation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableOfficeZoneAnnouncement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enhanceOfficeZone" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemLocationBasedCallingRestrictionsGetResponse17sp3", propOrder = {
    "physicalLocationIndicator",
    "enforceMscValidation",
    "enableOfficeZoneAnnouncement",
    "enhanceOfficeZone"
})
public class SystemLocationBasedCallingRestrictionsGetResponse17Sp3
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected PhysicalLocationIndicator physicalLocationIndicator;
    protected boolean enforceMscValidation;
    protected boolean enableOfficeZoneAnnouncement;
    protected boolean enhanceOfficeZone;

    /**
     * Ruft den Wert der physicalLocationIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PhysicalLocationIndicator }
     *     
     */
    public PhysicalLocationIndicator getPhysicalLocationIndicator() {
        return physicalLocationIndicator;
    }

    /**
     * Legt den Wert der physicalLocationIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PhysicalLocationIndicator }
     *     
     */
    public void setPhysicalLocationIndicator(PhysicalLocationIndicator value) {
        this.physicalLocationIndicator = value;
    }

    /**
     * Ruft den Wert der enforceMscValidation-Eigenschaft ab.
     * 
     */
    public boolean isEnforceMscValidation() {
        return enforceMscValidation;
    }

    /**
     * Legt den Wert der enforceMscValidation-Eigenschaft fest.
     * 
     */
    public void setEnforceMscValidation(boolean value) {
        this.enforceMscValidation = value;
    }

    /**
     * Ruft den Wert der enableOfficeZoneAnnouncement-Eigenschaft ab.
     * 
     */
    public boolean isEnableOfficeZoneAnnouncement() {
        return enableOfficeZoneAnnouncement;
    }

    /**
     * Legt den Wert der enableOfficeZoneAnnouncement-Eigenschaft fest.
     * 
     */
    public void setEnableOfficeZoneAnnouncement(boolean value) {
        this.enableOfficeZoneAnnouncement = value;
    }

    /**
     * Ruft den Wert der enhanceOfficeZone-Eigenschaft ab.
     * 
     */
    public boolean isEnhanceOfficeZone() {
        return enhanceOfficeZone;
    }

    /**
     * Legt den Wert der enhanceOfficeZone-Eigenschaft fest.
     * 
     */
    public void setEnhanceOfficeZone(boolean value) {
        this.enhanceOfficeZone = value;
    }

}
