//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupInstantGroupCallGetInstanceRequest14.
 *         Contains the service profile information and a list of phone numbers.
 *       
 * 
 * <p>Java-Klasse für GroupInstantGroupCallGetInstanceResponse14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupInstantGroupCallGetInstanceResponse14">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceReadProfile"/>
 *         <element name="destinationPhoneNumber" type="{}OutgoingDNorSIPURI" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="isAnswerTimeoutEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="answerTimeoutMinutes" type="{}InstantGroupCallAnswerTimeoutMinutes" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupInstantGroupCallGetInstanceResponse14", propOrder = {
    "serviceInstanceProfile",
    "destinationPhoneNumber",
    "isAnswerTimeoutEnabled",
    "answerTimeoutMinutes"
})
public class GroupInstantGroupCallGetInstanceResponse14
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected ServiceInstanceReadProfile serviceInstanceProfile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> destinationPhoneNumber;
    protected boolean isAnswerTimeoutEnabled;
    protected Integer answerTimeoutMinutes;

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceReadProfile }
     *     
     */
    public ServiceInstanceReadProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceReadProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceReadProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Gets the value of the destinationPhoneNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the destinationPhoneNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDestinationPhoneNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the destinationPhoneNumber property.
     */
    public List<String> getDestinationPhoneNumber() {
        if (destinationPhoneNumber == null) {
            destinationPhoneNumber = new ArrayList<>();
        }
        return this.destinationPhoneNumber;
    }

    /**
     * Ruft den Wert der isAnswerTimeoutEnabled-Eigenschaft ab.
     * 
     */
    public boolean isIsAnswerTimeoutEnabled() {
        return isAnswerTimeoutEnabled;
    }

    /**
     * Legt den Wert der isAnswerTimeoutEnabled-Eigenschaft fest.
     * 
     */
    public void setIsAnswerTimeoutEnabled(boolean value) {
        this.isAnswerTimeoutEnabled = value;
    }

    /**
     * Ruft den Wert der answerTimeoutMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAnswerTimeoutMinutes() {
        return answerTimeoutMinutes;
    }

    /**
     * Legt den Wert der answerTimeoutMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAnswerTimeoutMinutes(Integer value) {
        this.answerTimeoutMinutes = value;
    }

}
