//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupDialPlanPolicyGetAccessCodeListRequest
 *         Contains a table with column headings: "Access Code",
 *         "Enable Secondary Dial Tone", "Description"
 *       
 * 
 * <p>Java-Klasse für GroupDialPlanPolicyGetAccessCodeListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDialPlanPolicyGetAccessCodeListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="accessCodeTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDialPlanPolicyGetAccessCodeListResponse", propOrder = {
    "accessCodeTable"
})
public class GroupDialPlanPolicyGetAccessCodeListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable accessCodeTable;

    /**
     * Ruft den Wert der accessCodeTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAccessCodeTable() {
        return accessCodeTable;
    }

    /**
     * Legt den Wert der accessCodeTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAccessCodeTable(OCITable value) {
        this.accessCodeTable = value;
    }

}
