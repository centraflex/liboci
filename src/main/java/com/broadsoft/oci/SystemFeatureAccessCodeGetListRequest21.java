//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to get list of default Feature Access Codes defined on system
 *         level.
 *         The response is either SystemFeatureAccessCodeGetListResponse20 or
 *         ErrorResponse.
 * 
 *         In release 20 the "Call Recording" FAC name is changed to
 *         "Call Recording - Start".
 *       
 * 
 * <p>Java-Klasse für SystemFeatureAccessCodeGetListRequest21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemFeatureAccessCodeGetListRequest21">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemFeatureAccessCodeGetListRequest21")
public class SystemFeatureAccessCodeGetListRequest21
    extends OCIRequest
{


}
