//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Authorize (with quantity) or unauthorize a group service.
 *       
 * 
 * <p>Java-Klasse für GroupServiceAuthorization complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupServiceAuthorization">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="serviceName" type="{}GroupService"/>
 *         <choice>
 *           <element name="authorizedQuantity" type="{}UnboundedPositiveInt"/>
 *           <element name="unauthorized" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         </choice>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupServiceAuthorization", propOrder = {
    "serviceName",
    "authorizedQuantity",
    "unauthorized"
})
public class GroupServiceAuthorization {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupService serviceName;
    protected UnboundedPositiveInt authorizedQuantity;
    protected Boolean unauthorized;

    /**
     * Ruft den Wert der serviceName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupService }
     *     
     */
    public GroupService getServiceName() {
        return serviceName;
    }

    /**
     * Legt den Wert der serviceName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupService }
     *     
     */
    public void setServiceName(GroupService value) {
        this.serviceName = value;
    }

    /**
     * Ruft den Wert der authorizedQuantity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public UnboundedPositiveInt getAuthorizedQuantity() {
        return authorizedQuantity;
    }

    /**
     * Legt den Wert der authorizedQuantity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public void setAuthorizedQuantity(UnboundedPositiveInt value) {
        this.authorizedQuantity = value;
    }

    /**
     * Ruft den Wert der unauthorized-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUnauthorized() {
        return unauthorized;
    }

    /**
     * Legt den Wert der unauthorized-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnauthorized(Boolean value) {
        this.unauthorized = value;
    }

}
