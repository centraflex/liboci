//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system interface attributes for Messaging Server/BroadCloud.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemBroadCloudModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadCloudModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="provisioningUrl" type="{}URL" minOccurs="0"/>
 *         <element name="provisioningUserId" type="{}ProvisioningBroadCloudAuthenticationUserName" minOccurs="0"/>
 *         <element name="provisioningPassword" type="{}ProvisioningBroadCloudAuthenticationPassword" minOccurs="0"/>
 *         <element name="enableSynchronization" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadCloudModifyRequest", propOrder = {
    "provisioningUrl",
    "provisioningUserId",
    "provisioningPassword",
    "enableSynchronization"
})
public class SystemBroadCloudModifyRequest
    extends OCIRequest
{

    @XmlElementRef(name = "provisioningUrl", type = JAXBElement.class, required = false)
    protected JAXBElement<String> provisioningUrl;
    @XmlElementRef(name = "provisioningUserId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> provisioningUserId;
    @XmlElementRef(name = "provisioningPassword", type = JAXBElement.class, required = false)
    protected JAXBElement<String> provisioningPassword;
    protected Boolean enableSynchronization;

    /**
     * Ruft den Wert der provisioningUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProvisioningUrl() {
        return provisioningUrl;
    }

    /**
     * Legt den Wert der provisioningUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProvisioningUrl(JAXBElement<String> value) {
        this.provisioningUrl = value;
    }

    /**
     * Ruft den Wert der provisioningUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProvisioningUserId() {
        return provisioningUserId;
    }

    /**
     * Legt den Wert der provisioningUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProvisioningUserId(JAXBElement<String> value) {
        this.provisioningUserId = value;
    }

    /**
     * Ruft den Wert der provisioningPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProvisioningPassword() {
        return provisioningPassword;
    }

    /**
     * Legt den Wert der provisioningPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProvisioningPassword(JAXBElement<String> value) {
        this.provisioningPassword = value;
    }

    /**
     * Ruft den Wert der enableSynchronization-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableSynchronization() {
        return enableSynchronization;
    }

    /**
     * Legt den Wert der enableSynchronization-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSynchronization(Boolean value) {
        this.enableSynchronization = value;
    }

}
