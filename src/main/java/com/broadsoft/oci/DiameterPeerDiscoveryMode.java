//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DiameterPeerDiscoveryMode.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DiameterPeerDiscoveryMode">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Legacy"/>
 *     <enumeration value="Rfc6733Any"/>
 *     <enumeration value="Rfc6733TLSOnly"/>
 *     <enumeration value="Rfc6733TCPOnly"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DiameterPeerDiscoveryMode")
@XmlEnum
public enum DiameterPeerDiscoveryMode {

    @XmlEnumValue("Legacy")
    LEGACY("Legacy"),
    @XmlEnumValue("Rfc6733Any")
    RFC_6733_ANY("Rfc6733Any"),
    @XmlEnumValue("Rfc6733TLSOnly")
    RFC_6733_TLS_ONLY("Rfc6733TLSOnly"),
    @XmlEnumValue("Rfc6733TCPOnly")
    RFC_6733_TCP_ONLY("Rfc6733TCPOnly");
    private final String value;

    DiameterPeerDiscoveryMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DiameterPeerDiscoveryMode fromValue(String v) {
        for (DiameterPeerDiscoveryMode c: DiameterPeerDiscoveryMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
