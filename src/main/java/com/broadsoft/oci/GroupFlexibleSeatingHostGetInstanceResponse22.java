//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupFlexibleSeatingHostGetInstanceRequest22.
 *         Contains the service profile and access device information.
 *       
 * 
 * <p>Java-Klasse für GroupFlexibleSeatingHostGetInstanceResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupFlexibleSeatingHostGetInstanceResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceReadProfile19sp1"/>
 *         <element name="defaultAlias" type="{}SIPURI"/>
 *         <element name="accessDeviceEndpoint" type="{}AccessDeviceMultipleContactEndpointRead22" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupFlexibleSeatingHostGetInstanceResponse22", propOrder = {
    "serviceInstanceProfile",
    "defaultAlias",
    "accessDeviceEndpoint",
    "networkClassOfService"
})
public class GroupFlexibleSeatingHostGetInstanceResponse22
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected ServiceInstanceReadProfile19Sp1 serviceInstanceProfile;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultAlias;
    protected AccessDeviceMultipleContactEndpointRead22 accessDeviceEndpoint;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceReadProfile19Sp1 }
     *     
     */
    public ServiceInstanceReadProfile19Sp1 getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceReadProfile19Sp1 }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceReadProfile19Sp1 value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der defaultAlias-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultAlias() {
        return defaultAlias;
    }

    /**
     * Legt den Wert der defaultAlias-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultAlias(String value) {
        this.defaultAlias = value;
    }

    /**
     * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceMultipleContactEndpointRead22 }
     *     
     */
    public AccessDeviceMultipleContactEndpointRead22 getAccessDeviceEndpoint() {
        return accessDeviceEndpoint;
    }

    /**
     * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceMultipleContactEndpointRead22 }
     *     
     */
    public void setAccessDeviceEndpoint(AccessDeviceMultipleContactEndpointRead22 value) {
        this.accessDeviceEndpoint = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

}
