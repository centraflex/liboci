//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserAccessDeviceFileGetListRequest.
 *         Contains a table of user modifiable Device Management files.
 *         The column headings are: "File Format", "Template URL".
 *       
 * 
 * <p>Java-Klasse für UserAccessDeviceFileGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserAccessDeviceFileGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="deviceFilesTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserAccessDeviceFileGetListResponse", propOrder = {
    "deviceFilesTable"
})
public class UserAccessDeviceFileGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable deviceFilesTable;

    /**
     * Ruft den Wert der deviceFilesTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDeviceFilesTable() {
        return deviceFilesTable;
    }

    /**
     * Legt den Wert der deviceFilesTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDeviceFilesTable(OCITable value) {
        this.deviceFilesTable = value;
    }

}
