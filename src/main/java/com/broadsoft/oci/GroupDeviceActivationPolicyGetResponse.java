//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupDeviceActivationPolicyGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupDeviceActivationPolicyGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDeviceActivationPolicyGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useGroupSettings" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowActivationCodeRequestByUser" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sendActivationCodeInEmail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDeviceActivationPolicyGetResponse", propOrder = {
    "useGroupSettings",
    "allowActivationCodeRequestByUser",
    "sendActivationCodeInEmail"
})
public class GroupDeviceActivationPolicyGetResponse
    extends OCIDataResponse
{

    protected boolean useGroupSettings;
    protected boolean allowActivationCodeRequestByUser;
    protected boolean sendActivationCodeInEmail;

    /**
     * Ruft den Wert der useGroupSettings-Eigenschaft ab.
     * 
     */
    public boolean isUseGroupSettings() {
        return useGroupSettings;
    }

    /**
     * Legt den Wert der useGroupSettings-Eigenschaft fest.
     * 
     */
    public void setUseGroupSettings(boolean value) {
        this.useGroupSettings = value;
    }

    /**
     * Ruft den Wert der allowActivationCodeRequestByUser-Eigenschaft ab.
     * 
     */
    public boolean isAllowActivationCodeRequestByUser() {
        return allowActivationCodeRequestByUser;
    }

    /**
     * Legt den Wert der allowActivationCodeRequestByUser-Eigenschaft fest.
     * 
     */
    public void setAllowActivationCodeRequestByUser(boolean value) {
        this.allowActivationCodeRequestByUser = value;
    }

    /**
     * Ruft den Wert der sendActivationCodeInEmail-Eigenschaft ab.
     * 
     */
    public boolean isSendActivationCodeInEmail() {
        return sendActivationCodeInEmail;
    }

    /**
     * Legt den Wert der sendActivationCodeInEmail-Eigenschaft fest.
     * 
     */
    public void setSendActivationCodeInEmail(boolean value) {
        this.sendActivationCodeInEmail = value;
    }

}
