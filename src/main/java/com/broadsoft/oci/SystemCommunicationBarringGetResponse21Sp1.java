//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemCommunicationBarringGetRequest21sp1.
 *         The following elements are only used in AS data mode:
 *         vmCallbackScreening, value "false" is returned in XS data mode.       
 *       
 * 
 * <p>Java-Klasse für SystemCommunicationBarringGetResponse21sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCommunicationBarringGetResponse21sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="directTransferScreening" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="vmCallbackScreening" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCommunicationBarringGetResponse21sp1", propOrder = {
    "directTransferScreening",
    "vmCallbackScreening"
})
public class SystemCommunicationBarringGetResponse21Sp1
    extends OCIDataResponse
{

    protected boolean directTransferScreening;
    protected boolean vmCallbackScreening;

    /**
     * Ruft den Wert der directTransferScreening-Eigenschaft ab.
     * 
     */
    public boolean isDirectTransferScreening() {
        return directTransferScreening;
    }

    /**
     * Legt den Wert der directTransferScreening-Eigenschaft fest.
     * 
     */
    public void setDirectTransferScreening(boolean value) {
        this.directTransferScreening = value;
    }

    /**
     * Ruft den Wert der vmCallbackScreening-Eigenschaft ab.
     * 
     */
    public boolean isVmCallbackScreening() {
        return vmCallbackScreening;
    }

    /**
     * Legt den Wert der vmCallbackScreening-Eigenschaft fest.
     * 
     */
    public void setVmCallbackScreening(boolean value) {
        this.vmCallbackScreening = value;
    }

}
