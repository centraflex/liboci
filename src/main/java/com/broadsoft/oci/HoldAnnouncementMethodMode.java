//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für HoldAnnouncementMethodMode.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="HoldAnnouncementMethodMode">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Inactive"/>
 *     <enumeration value="Bandwidth Attributes"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "HoldAnnouncementMethodMode")
@XmlEnum
public enum HoldAnnouncementMethodMode {

    @XmlEnumValue("Inactive")
    INACTIVE("Inactive"),
    @XmlEnumValue("Bandwidth Attributes")
    BANDWIDTH_ATTRIBUTES("Bandwidth Attributes");
    private final String value;

    HoldAnnouncementMethodMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HoldAnnouncementMethodMode fromValue(String v) {
        for (HoldAnnouncementMethodMode c: HoldAnnouncementMethodMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
