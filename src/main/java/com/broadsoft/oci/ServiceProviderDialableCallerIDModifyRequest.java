//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the service provider?s Dialable Caller ID settings and criteria list.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderDialableCallerIDModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderDialableCallerIDModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="useServiceProviderCriteria" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="nsScreeningFailurePolicy" type="{}NsScreeningFailurePolicy" minOccurs="0"/>
 *         <element name="criteriaPriorityOrder" type="{}DialableCallerIDCriteriaPriorityOrder" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderDialableCallerIDModifyRequest", propOrder = {
    "serviceProviderId",
    "useServiceProviderCriteria",
    "nsScreeningFailurePolicy",
    "criteriaPriorityOrder"
})
public class ServiceProviderDialableCallerIDModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected Boolean useServiceProviderCriteria;
    @XmlSchemaType(name = "token")
    protected NsScreeningFailurePolicy nsScreeningFailurePolicy;
    protected List<DialableCallerIDCriteriaPriorityOrder> criteriaPriorityOrder;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der useServiceProviderCriteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseServiceProviderCriteria() {
        return useServiceProviderCriteria;
    }

    /**
     * Legt den Wert der useServiceProviderCriteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseServiceProviderCriteria(Boolean value) {
        this.useServiceProviderCriteria = value;
    }

    /**
     * Ruft den Wert der nsScreeningFailurePolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NsScreeningFailurePolicy }
     *     
     */
    public NsScreeningFailurePolicy getNsScreeningFailurePolicy() {
        return nsScreeningFailurePolicy;
    }

    /**
     * Legt den Wert der nsScreeningFailurePolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NsScreeningFailurePolicy }
     *     
     */
    public void setNsScreeningFailurePolicy(NsScreeningFailurePolicy value) {
        this.nsScreeningFailurePolicy = value;
    }

    /**
     * Gets the value of the criteriaPriorityOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the criteriaPriorityOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCriteriaPriorityOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DialableCallerIDCriteriaPriorityOrder }
     * 
     * 
     * @return
     *     The value of the criteriaPriorityOrder property.
     */
    public List<DialableCallerIDCriteriaPriorityOrder> getCriteriaPriorityOrder() {
        if (criteriaPriorityOrder == null) {
            criteriaPriorityOrder = new ArrayList<>();
        }
        return this.criteriaPriorityOrder;
    }

}
