//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a GETS Resource Priority.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemGETSResourcePriorityAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGETSResourcePriorityAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="priorityValue" type="{}GETSPriorityValue"/>
 *         <element name="priorityLevel" type="{}GETSPriorityLevel"/>
 *         <element name="priorityClass" type="{}GETSPriorityClass"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGETSResourcePriorityAddRequest", propOrder = {
    "priorityValue",
    "priorityLevel",
    "priorityClass"
})
public class SystemGETSResourcePriorityAddRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String priorityValue;
    protected int priorityLevel;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GETSPriorityClass priorityClass;

    /**
     * Ruft den Wert der priorityValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriorityValue() {
        return priorityValue;
    }

    /**
     * Legt den Wert der priorityValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriorityValue(String value) {
        this.priorityValue = value;
    }

    /**
     * Ruft den Wert der priorityLevel-Eigenschaft ab.
     * 
     */
    public int getPriorityLevel() {
        return priorityLevel;
    }

    /**
     * Legt den Wert der priorityLevel-Eigenschaft fest.
     * 
     */
    public void setPriorityLevel(int value) {
        this.priorityLevel = value;
    }

    /**
     * Ruft den Wert der priorityClass-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GETSPriorityClass }
     *     
     */
    public GETSPriorityClass getPriorityClass() {
        return priorityClass;
    }

    /**
     * Legt den Wert der priorityClass-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GETSPriorityClass }
     *     
     */
    public void setPriorityClass(GETSPriorityClass value) {
        this.priorityClass = value;
    }

}
