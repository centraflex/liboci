//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the list of transfer numbers for a group default and it's departments.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupOutgoingCallingPlanTransferNumbersModifyListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupOutgoingCallingPlanTransferNumbersModifyListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="groupNumbers" type="{}OutgoingCallingPlanTransferNumbersModify" minOccurs="0"/>
 *         <element name="departmentNumbers" type="{}OutgoingCallingPlanDepartmentTransferNumbersModify" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupOutgoingCallingPlanTransferNumbersModifyListRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "groupNumbers",
    "departmentNumbers"
})
public class GroupOutgoingCallingPlanTransferNumbersModifyListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected OutgoingCallingPlanTransferNumbersModify groupNumbers;
    protected List<OutgoingCallingPlanDepartmentTransferNumbersModify> departmentNumbers;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der groupNumbers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanTransferNumbersModify }
     *     
     */
    public OutgoingCallingPlanTransferNumbersModify getGroupNumbers() {
        return groupNumbers;
    }

    /**
     * Legt den Wert der groupNumbers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanTransferNumbersModify }
     *     
     */
    public void setGroupNumbers(OutgoingCallingPlanTransferNumbersModify value) {
        this.groupNumbers = value;
    }

    /**
     * Gets the value of the departmentNumbers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the departmentNumbers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDepartmentNumbers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OutgoingCallingPlanDepartmentTransferNumbersModify }
     * 
     * 
     * @return
     *     The value of the departmentNumbers property.
     */
    public List<OutgoingCallingPlanDepartmentTransferNumbersModify> getDepartmentNumbers() {
        if (departmentNumbers == null) {
            departmentNumbers = new ArrayList<>();
        }
        return this.departmentNumbers;
    }

}
