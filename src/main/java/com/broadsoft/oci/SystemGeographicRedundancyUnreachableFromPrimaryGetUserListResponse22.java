//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemGeographicRedundancyUnreachableFromPrimaryGetUserListRequest22. 
 *         The Unreachable From Primary User table column headings are: "User ID", "Lineport".
 *         The optional totalNumberOfUnreachableFromPrimaryUsers is returned only when the userListSizeLimit is set in the request and 
 *         if the total number of unreachable from primary users is greater than the value of userListSizeLimit.
 *       
 * 
 * <p>Java-Klasse für SystemGeographicRedundancyUnreachableFromPrimaryGetUserListResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGeographicRedundancyUnreachableFromPrimaryGetUserListResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="unreachableFromPrimaryUserTable" type="{C}OCITable"/>
 *         <element name="totalNumberOfUnreachableFromPrimaryUsers" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGeographicRedundancyUnreachableFromPrimaryGetUserListResponse22", propOrder = {
    "unreachableFromPrimaryUserTable",
    "totalNumberOfUnreachableFromPrimaryUsers"
})
public class SystemGeographicRedundancyUnreachableFromPrimaryGetUserListResponse22
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable unreachableFromPrimaryUserTable;
    protected Integer totalNumberOfUnreachableFromPrimaryUsers;

    /**
     * Ruft den Wert der unreachableFromPrimaryUserTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUnreachableFromPrimaryUserTable() {
        return unreachableFromPrimaryUserTable;
    }

    /**
     * Legt den Wert der unreachableFromPrimaryUserTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUnreachableFromPrimaryUserTable(OCITable value) {
        this.unreachableFromPrimaryUserTable = value;
    }

    /**
     * Ruft den Wert der totalNumberOfUnreachableFromPrimaryUsers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalNumberOfUnreachableFromPrimaryUsers() {
        return totalNumberOfUnreachableFromPrimaryUsers;
    }

    /**
     * Legt den Wert der totalNumberOfUnreachableFromPrimaryUsers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalNumberOfUnreachableFromPrimaryUsers(Integer value) {
        this.totalNumberOfUnreachableFromPrimaryUsers = value;
    }

}
