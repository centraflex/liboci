//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *       Modify the setting that are configured for all the DNIS in a Call Center.
 *       The response is either SuccessResponse or ErrorResponse.
 *     
 * 
 * <p>Java-Klasse für GroupCallCenterModifyDNISParametersRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterModifyDNISParametersRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="displayDNISNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="displayDNISName" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="promoteCallsFromPriority1to0" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="promoteCallsFromPriority2to1" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="promoteCallsFromPriority3to2" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="promoteCallsFromPriority1to0Seconds" type="{}DNISPromoteCallPrioritySeconds" minOccurs="0"/>
 *         <element name="promoteCallsFromPriority2to1Seconds" type="{}DNISPromoteCallPrioritySeconds" minOccurs="0"/>
 *         <element name="promoteCallsFromPriority3to2Seconds" type="{}DNISPromoteCallPrioritySeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterModifyDNISParametersRequest", propOrder = {
    "serviceUserId",
    "displayDNISNumber",
    "displayDNISName",
    "promoteCallsFromPriority1To0",
    "promoteCallsFromPriority2To1",
    "promoteCallsFromPriority3To2",
    "promoteCallsFromPriority1To0Seconds",
    "promoteCallsFromPriority2To1Seconds",
    "promoteCallsFromPriority3To2Seconds"
})
public class GroupCallCenterModifyDNISParametersRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected Boolean displayDNISNumber;
    protected Boolean displayDNISName;
    @XmlElement(name = "promoteCallsFromPriority1to0")
    protected Boolean promoteCallsFromPriority1To0;
    @XmlElement(name = "promoteCallsFromPriority2to1")
    protected Boolean promoteCallsFromPriority2To1;
    @XmlElement(name = "promoteCallsFromPriority3to2")
    protected Boolean promoteCallsFromPriority3To2;
    @XmlElement(name = "promoteCallsFromPriority1to0Seconds")
    protected Integer promoteCallsFromPriority1To0Seconds;
    @XmlElement(name = "promoteCallsFromPriority2to1Seconds")
    protected Integer promoteCallsFromPriority2To1Seconds;
    @XmlElement(name = "promoteCallsFromPriority3to2Seconds")
    protected Integer promoteCallsFromPriority3To2Seconds;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der displayDNISNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisplayDNISNumber() {
        return displayDNISNumber;
    }

    /**
     * Legt den Wert der displayDNISNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayDNISNumber(Boolean value) {
        this.displayDNISNumber = value;
    }

    /**
     * Ruft den Wert der displayDNISName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisplayDNISName() {
        return displayDNISName;
    }

    /**
     * Legt den Wert der displayDNISName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayDNISName(Boolean value) {
        this.displayDNISName = value;
    }

    /**
     * Ruft den Wert der promoteCallsFromPriority1To0-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPromoteCallsFromPriority1To0() {
        return promoteCallsFromPriority1To0;
    }

    /**
     * Legt den Wert der promoteCallsFromPriority1To0-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPromoteCallsFromPriority1To0(Boolean value) {
        this.promoteCallsFromPriority1To0 = value;
    }

    /**
     * Ruft den Wert der promoteCallsFromPriority2To1-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPromoteCallsFromPriority2To1() {
        return promoteCallsFromPriority2To1;
    }

    /**
     * Legt den Wert der promoteCallsFromPriority2To1-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPromoteCallsFromPriority2To1(Boolean value) {
        this.promoteCallsFromPriority2To1 = value;
    }

    /**
     * Ruft den Wert der promoteCallsFromPriority3To2-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPromoteCallsFromPriority3To2() {
        return promoteCallsFromPriority3To2;
    }

    /**
     * Legt den Wert der promoteCallsFromPriority3To2-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPromoteCallsFromPriority3To2(Boolean value) {
        this.promoteCallsFromPriority3To2 = value;
    }

    /**
     * Ruft den Wert der promoteCallsFromPriority1To0Seconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPromoteCallsFromPriority1To0Seconds() {
        return promoteCallsFromPriority1To0Seconds;
    }

    /**
     * Legt den Wert der promoteCallsFromPriority1To0Seconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPromoteCallsFromPriority1To0Seconds(Integer value) {
        this.promoteCallsFromPriority1To0Seconds = value;
    }

    /**
     * Ruft den Wert der promoteCallsFromPriority2To1Seconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPromoteCallsFromPriority2To1Seconds() {
        return promoteCallsFromPriority2To1Seconds;
    }

    /**
     * Legt den Wert der promoteCallsFromPriority2To1Seconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPromoteCallsFromPriority2To1Seconds(Integer value) {
        this.promoteCallsFromPriority2To1Seconds = value;
    }

    /**
     * Ruft den Wert der promoteCallsFromPriority3To2Seconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPromoteCallsFromPriority3To2Seconds() {
        return promoteCallsFromPriority3To2Seconds;
    }

    /**
     * Legt den Wert der promoteCallsFromPriority3To2Seconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPromoteCallsFromPriority3To2Seconds(Integer value) {
        this.promoteCallsFromPriority3To2Seconds = value;
    }

}
