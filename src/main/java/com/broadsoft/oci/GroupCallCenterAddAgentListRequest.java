//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add agent(s) to a call center.
 *         The response is either SuccessResponse or ErrorResponse.
 *         If the skill level is not present for skill based premium call centers, the users will be set to skill level 1.
 *         
 *         The following element is only used in AS data mode and ignored in XS data mode:
 *           agentSkillList
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterAddAgentListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterAddAgentListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <choice>
 *           <element name="agentUserId" type="{}UserId" maxOccurs="unbounded"/>
 *           <element name="agentSkillList" type="{}CallCenterSkillAgentList" maxOccurs="unbounded"/>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterAddAgentListRequest", propOrder = {
    "serviceUserId",
    "agentUserId",
    "agentSkillList"
})
public class GroupCallCenterAddAgentListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> agentUserId;
    protected List<CallCenterSkillAgentList> agentSkillList;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Gets the value of the agentUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the agentUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgentUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the agentUserId property.
     */
    public List<String> getAgentUserId() {
        if (agentUserId == null) {
            agentUserId = new ArrayList<>();
        }
        return this.agentUserId;
    }

    /**
     * Gets the value of the agentSkillList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the agentSkillList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgentSkillList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallCenterSkillAgentList }
     * 
     * 
     * @return
     *     The value of the agentSkillList property.
     */
    public List<CallCenterSkillAgentList> getAgentSkillList() {
        if (agentSkillList == null) {
            agentSkillList = new ArrayList<>();
        }
        return this.agentSkillList;
    }

}
