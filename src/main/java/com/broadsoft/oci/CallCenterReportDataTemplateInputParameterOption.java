//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterReportDataTemplateInputParameterOption.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterReportDataTemplateInputParameterOption">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Required"/>
 *     <enumeration value="Does Not Apply"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterReportDataTemplateInputParameterOption")
@XmlEnum
public enum CallCenterReportDataTemplateInputParameterOption {

    @XmlEnumValue("Required")
    REQUIRED("Required"),
    @XmlEnumValue("Does Not Apply")
    DOES_NOT_APPLY("Does Not Apply");
    private final String value;

    CallCenterReportDataTemplateInputParameterOption(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterReportDataTemplateInputParameterOption fromValue(String v) {
        for (CallCenterReportDataTemplateInputParameterOption c: CallCenterReportDataTemplateInputParameterOption.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
