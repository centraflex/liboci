//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains a 2 column table with column headings 'Key' and 'Display Name' and a row
 *         for each state or province.
 *       
 * 
 * <p>Java-Klasse für SystemStateOrProvinceGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemStateOrProvinceGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="stateOrProvinceTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemStateOrProvinceGetListResponse", propOrder = {
    "stateOrProvinceTable"
})
public class SystemStateOrProvinceGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable stateOrProvinceTable;

    /**
     * Ruft den Wert der stateOrProvinceTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getStateOrProvinceTable() {
        return stateOrProvinceTable;
    }

    /**
     * Legt den Wert der stateOrProvinceTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setStateOrProvinceTable(OCITable value) {
        this.stateOrProvinceTable = value;
    }

}
