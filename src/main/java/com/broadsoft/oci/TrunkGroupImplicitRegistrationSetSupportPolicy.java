//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TrunkGroupImplicitRegistrationSetSupportPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="TrunkGroupImplicitRegistrationSetSupportPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Enabled"/>
 *     <enumeration value="Disabled"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TrunkGroupImplicitRegistrationSetSupportPolicy")
@XmlEnum
public enum TrunkGroupImplicitRegistrationSetSupportPolicy {

    @XmlEnumValue("Enabled")
    ENABLED("Enabled"),
    @XmlEnumValue("Disabled")
    DISABLED("Disabled");
    private final String value;

    TrunkGroupImplicitRegistrationSetSupportPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrunkGroupImplicitRegistrationSetSupportPolicy fromValue(String v) {
        for (TrunkGroupImplicitRegistrationSetSupportPolicy c: TrunkGroupImplicitRegistrationSetSupportPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
