//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für UnscreenedPresentationIdentityPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="UnscreenedPresentationIdentityPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Profile Presentation Identity"/>
 *     <enumeration value="Unscreened Presentation Identity"/>
 *     <enumeration value="Unscreened Presentation Identity With Profile Domain"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "UnscreenedPresentationIdentityPolicy")
@XmlEnum
public enum UnscreenedPresentationIdentityPolicy {

    @XmlEnumValue("Profile Presentation Identity")
    PROFILE_PRESENTATION_IDENTITY("Profile Presentation Identity"),
    @XmlEnumValue("Unscreened Presentation Identity")
    UNSCREENED_PRESENTATION_IDENTITY("Unscreened Presentation Identity"),
    @XmlEnumValue("Unscreened Presentation Identity With Profile Domain")
    UNSCREENED_PRESENTATION_IDENTITY_WITH_PROFILE_DOMAIN("Unscreened Presentation Identity With Profile Domain");
    private final String value;

    UnscreenedPresentationIdentityPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UnscreenedPresentationIdentityPolicy fromValue(String v) {
        for (UnscreenedPresentationIdentityPolicy c: UnscreenedPresentationIdentityPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
