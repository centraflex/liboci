//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupCallCenterEnhancedReportingGetRequest19.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterEnhancedReportingGetResponse19 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterEnhancedReportingGetResponse19">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="reportingServer" type="{}CallCenterReportServerChoice19"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterEnhancedReportingGetResponse19", propOrder = {
    "reportingServer"
})
public class GroupCallCenterEnhancedReportingGetResponse19
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportServerChoice19 reportingServer;

    /**
     * Ruft den Wert der reportingServer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportServerChoice19 }
     *     
     */
    public CallCenterReportServerChoice19 getReportingServer() {
        return reportingServer;
    }

    /**
     * Legt den Wert der reportingServer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportServerChoice19 }
     *     
     */
    public void setReportingServer(CallCenterReportServerChoice19 value) {
        this.reportingServer = value;
    }

}
