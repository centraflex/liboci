//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a call center's comfort message bypass settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterComfortMessageBypassModifyRequest20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterComfortMessageBypassModifyRequest20">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="callWaitingAgeThresholdSeconds" type="{}CallCenterComfortMessageBypassThresholdSeconds" minOccurs="0"/>
 *         <element name="playAnnouncementAfterRinging" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="ringTimeBeforePlayingAnnouncementSeconds" type="{}CallCenterRingTimeBeforePlayingComfortMessageBypassAnnouncementSeconds" minOccurs="0"/>
 *         <element name="audioMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="audioUrlList" type="{}CallCenterAnnouncementURLListModify" minOccurs="0"/>
 *         <element name="audioFileList" type="{}CallCenterAnnouncementFileListModify20" minOccurs="0"/>
 *         <element name="videoMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="videoUrlList" type="{}CallCenterAnnouncementURLListModify" minOccurs="0"/>
 *         <element name="videoFileList" type="{}CallCenterAnnouncementFileListModify20" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterComfortMessageBypassModifyRequest20", propOrder = {
    "serviceUserId",
    "isActive",
    "callWaitingAgeThresholdSeconds",
    "playAnnouncementAfterRinging",
    "ringTimeBeforePlayingAnnouncementSeconds",
    "audioMessageSelection",
    "audioUrlList",
    "audioFileList",
    "videoMessageSelection",
    "videoUrlList",
    "videoFileList"
})
public class GroupCallCenterComfortMessageBypassModifyRequest20
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected Boolean isActive;
    protected Integer callWaitingAgeThresholdSeconds;
    protected Boolean playAnnouncementAfterRinging;
    protected Integer ringTimeBeforePlayingAnnouncementSeconds;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection audioMessageSelection;
    protected CallCenterAnnouncementURLListModify audioUrlList;
    protected CallCenterAnnouncementFileListModify20 audioFileList;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection videoMessageSelection;
    protected CallCenterAnnouncementURLListModify videoUrlList;
    protected CallCenterAnnouncementFileListModify20 videoFileList;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der callWaitingAgeThresholdSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCallWaitingAgeThresholdSeconds() {
        return callWaitingAgeThresholdSeconds;
    }

    /**
     * Legt den Wert der callWaitingAgeThresholdSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCallWaitingAgeThresholdSeconds(Integer value) {
        this.callWaitingAgeThresholdSeconds = value;
    }

    /**
     * Ruft den Wert der playAnnouncementAfterRinging-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayAnnouncementAfterRinging() {
        return playAnnouncementAfterRinging;
    }

    /**
     * Legt den Wert der playAnnouncementAfterRinging-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayAnnouncementAfterRinging(Boolean value) {
        this.playAnnouncementAfterRinging = value;
    }

    /**
     * Ruft den Wert der ringTimeBeforePlayingAnnouncementSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRingTimeBeforePlayingAnnouncementSeconds() {
        return ringTimeBeforePlayingAnnouncementSeconds;
    }

    /**
     * Legt den Wert der ringTimeBeforePlayingAnnouncementSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRingTimeBeforePlayingAnnouncementSeconds(Integer value) {
        this.ringTimeBeforePlayingAnnouncementSeconds = value;
    }

    /**
     * Ruft den Wert der audioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getAudioMessageSelection() {
        return audioMessageSelection;
    }

    /**
     * Legt den Wert der audioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.audioMessageSelection = value;
    }

    /**
     * Ruft den Wert der audioUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public CallCenterAnnouncementURLListModify getAudioUrlList() {
        return audioUrlList;
    }

    /**
     * Legt den Wert der audioUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public void setAudioUrlList(CallCenterAnnouncementURLListModify value) {
        this.audioUrlList = value;
    }

    /**
     * Ruft den Wert der audioFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListModify20 }
     *     
     */
    public CallCenterAnnouncementFileListModify20 getAudioFileList() {
        return audioFileList;
    }

    /**
     * Legt den Wert der audioFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListModify20 }
     *     
     */
    public void setAudioFileList(CallCenterAnnouncementFileListModify20 value) {
        this.audioFileList = value;
    }

    /**
     * Ruft den Wert der videoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getVideoMessageSelection() {
        return videoMessageSelection;
    }

    /**
     * Legt den Wert der videoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.videoMessageSelection = value;
    }

    /**
     * Ruft den Wert der videoUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public CallCenterAnnouncementURLListModify getVideoUrlList() {
        return videoUrlList;
    }

    /**
     * Legt den Wert der videoUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public void setVideoUrlList(CallCenterAnnouncementURLListModify value) {
        this.videoUrlList = value;
    }

    /**
     * Ruft den Wert der videoFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListModify20 }
     *     
     */
    public CallCenterAnnouncementFileListModify20 getVideoFileList() {
        return videoFileList;
    }

    /**
     * Legt den Wert der videoFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListModify20 }
     *     
     */
    public void setVideoFileList(CallCenterAnnouncementFileListModify20 value) {
        this.videoFileList = value;
    }

}
