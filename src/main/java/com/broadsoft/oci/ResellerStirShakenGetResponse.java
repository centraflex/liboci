//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the ResellerStirShakenGetRequest.
 *       
 * 
 * <p>Java-Klasse für ResellerStirShakenGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerStirShakenGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useParentLevelSettings" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="signingPolicy" type="{}StirShakenSigningPolicy"/>
 *         <element name="taggingPolicy" type="{}StirShakenTaggingPolicy"/>
 *         <element name="signEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="tagEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="signingServiceURL" type="{}URL" minOccurs="0"/>
 *         <element name="tagFromOrPAI" type="{}StirShakenTagFromOrPAI"/>
 *         <element name="verstatTag" type="{}StirShakenVerstatTag"/>
 *         <element name="useOSValueForOrigId" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="origUUID" type="{}StirShakenOrigUUID" minOccurs="0"/>
 *         <element name="attestationLevel" type="{}StirShakenAttestationLevel"/>
 *         <element name="enableVerification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="verificationServiceURL" type="{}URL" minOccurs="0"/>
 *         <element name="verificationErrorHandling" type="{}StirShakenVerificationErrorHandling"/>
 *         <element name="proxyVerstatToCNAMSubscribe" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useUnknownHeadersFromCNAMNotify" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableSigningForUnscreenedTrunkGroupOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableTaggingForUnscreenedTrunkGroupOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="unscreenedTrunkGroupOriginationAttestationLevel" type="{}StirShakenUnscreenedTrunkGroupOriginationAttestationLevel"/>
 *         <element name="verifyGETSCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerStirShakenGetResponse", propOrder = {
    "useParentLevelSettings",
    "signingPolicy",
    "taggingPolicy",
    "signEmergencyCalls",
    "tagEmergencyCalls",
    "signingServiceURL",
    "tagFromOrPAI",
    "verstatTag",
    "useOSValueForOrigId",
    "origUUID",
    "attestationLevel",
    "enableVerification",
    "verificationServiceURL",
    "verificationErrorHandling",
    "proxyVerstatToCNAMSubscribe",
    "useUnknownHeadersFromCNAMNotify",
    "enableSigningForUnscreenedTrunkGroupOriginations",
    "enableTaggingForUnscreenedTrunkGroupOriginations",
    "unscreenedTrunkGroupOriginationAttestationLevel",
    "verifyGETSCalls"
})
public class ResellerStirShakenGetResponse
    extends OCIDataResponse
{

    protected boolean useParentLevelSettings;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected StirShakenSigningPolicy signingPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected StirShakenTaggingPolicy taggingPolicy;
    protected boolean signEmergencyCalls;
    protected boolean tagEmergencyCalls;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String signingServiceURL;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected StirShakenTagFromOrPAI tagFromOrPAI;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected StirShakenVerstatTag verstatTag;
    protected boolean useOSValueForOrigId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String origUUID;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected StirShakenAttestationLevel attestationLevel;
    protected boolean enableVerification;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String verificationServiceURL;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected StirShakenVerificationErrorHandling verificationErrorHandling;
    protected boolean proxyVerstatToCNAMSubscribe;
    protected boolean useUnknownHeadersFromCNAMNotify;
    protected boolean enableSigningForUnscreenedTrunkGroupOriginations;
    protected boolean enableTaggingForUnscreenedTrunkGroupOriginations;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected StirShakenUnscreenedTrunkGroupOriginationAttestationLevel unscreenedTrunkGroupOriginationAttestationLevel;
    protected boolean verifyGETSCalls;

    /**
     * Ruft den Wert der useParentLevelSettings-Eigenschaft ab.
     * 
     */
    public boolean isUseParentLevelSettings() {
        return useParentLevelSettings;
    }

    /**
     * Legt den Wert der useParentLevelSettings-Eigenschaft fest.
     * 
     */
    public void setUseParentLevelSettings(boolean value) {
        this.useParentLevelSettings = value;
    }

    /**
     * Ruft den Wert der signingPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StirShakenSigningPolicy }
     *     
     */
    public StirShakenSigningPolicy getSigningPolicy() {
        return signingPolicy;
    }

    /**
     * Legt den Wert der signingPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StirShakenSigningPolicy }
     *     
     */
    public void setSigningPolicy(StirShakenSigningPolicy value) {
        this.signingPolicy = value;
    }

    /**
     * Ruft den Wert der taggingPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StirShakenTaggingPolicy }
     *     
     */
    public StirShakenTaggingPolicy getTaggingPolicy() {
        return taggingPolicy;
    }

    /**
     * Legt den Wert der taggingPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StirShakenTaggingPolicy }
     *     
     */
    public void setTaggingPolicy(StirShakenTaggingPolicy value) {
        this.taggingPolicy = value;
    }

    /**
     * Ruft den Wert der signEmergencyCalls-Eigenschaft ab.
     * 
     */
    public boolean isSignEmergencyCalls() {
        return signEmergencyCalls;
    }

    /**
     * Legt den Wert der signEmergencyCalls-Eigenschaft fest.
     * 
     */
    public void setSignEmergencyCalls(boolean value) {
        this.signEmergencyCalls = value;
    }

    /**
     * Ruft den Wert der tagEmergencyCalls-Eigenschaft ab.
     * 
     */
    public boolean isTagEmergencyCalls() {
        return tagEmergencyCalls;
    }

    /**
     * Legt den Wert der tagEmergencyCalls-Eigenschaft fest.
     * 
     */
    public void setTagEmergencyCalls(boolean value) {
        this.tagEmergencyCalls = value;
    }

    /**
     * Ruft den Wert der signingServiceURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSigningServiceURL() {
        return signingServiceURL;
    }

    /**
     * Legt den Wert der signingServiceURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSigningServiceURL(String value) {
        this.signingServiceURL = value;
    }

    /**
     * Ruft den Wert der tagFromOrPAI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StirShakenTagFromOrPAI }
     *     
     */
    public StirShakenTagFromOrPAI getTagFromOrPAI() {
        return tagFromOrPAI;
    }

    /**
     * Legt den Wert der tagFromOrPAI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StirShakenTagFromOrPAI }
     *     
     */
    public void setTagFromOrPAI(StirShakenTagFromOrPAI value) {
        this.tagFromOrPAI = value;
    }

    /**
     * Ruft den Wert der verstatTag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StirShakenVerstatTag }
     *     
     */
    public StirShakenVerstatTag getVerstatTag() {
        return verstatTag;
    }

    /**
     * Legt den Wert der verstatTag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StirShakenVerstatTag }
     *     
     */
    public void setVerstatTag(StirShakenVerstatTag value) {
        this.verstatTag = value;
    }

    /**
     * Ruft den Wert der useOSValueForOrigId-Eigenschaft ab.
     * 
     */
    public boolean isUseOSValueForOrigId() {
        return useOSValueForOrigId;
    }

    /**
     * Legt den Wert der useOSValueForOrigId-Eigenschaft fest.
     * 
     */
    public void setUseOSValueForOrigId(boolean value) {
        this.useOSValueForOrigId = value;
    }

    /**
     * Ruft den Wert der origUUID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigUUID() {
        return origUUID;
    }

    /**
     * Legt den Wert der origUUID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigUUID(String value) {
        this.origUUID = value;
    }

    /**
     * Ruft den Wert der attestationLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StirShakenAttestationLevel }
     *     
     */
    public StirShakenAttestationLevel getAttestationLevel() {
        return attestationLevel;
    }

    /**
     * Legt den Wert der attestationLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StirShakenAttestationLevel }
     *     
     */
    public void setAttestationLevel(StirShakenAttestationLevel value) {
        this.attestationLevel = value;
    }

    /**
     * Ruft den Wert der enableVerification-Eigenschaft ab.
     * 
     */
    public boolean isEnableVerification() {
        return enableVerification;
    }

    /**
     * Legt den Wert der enableVerification-Eigenschaft fest.
     * 
     */
    public void setEnableVerification(boolean value) {
        this.enableVerification = value;
    }

    /**
     * Ruft den Wert der verificationServiceURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerificationServiceURL() {
        return verificationServiceURL;
    }

    /**
     * Legt den Wert der verificationServiceURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerificationServiceURL(String value) {
        this.verificationServiceURL = value;
    }

    /**
     * Ruft den Wert der verificationErrorHandling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StirShakenVerificationErrorHandling }
     *     
     */
    public StirShakenVerificationErrorHandling getVerificationErrorHandling() {
        return verificationErrorHandling;
    }

    /**
     * Legt den Wert der verificationErrorHandling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StirShakenVerificationErrorHandling }
     *     
     */
    public void setVerificationErrorHandling(StirShakenVerificationErrorHandling value) {
        this.verificationErrorHandling = value;
    }

    /**
     * Ruft den Wert der proxyVerstatToCNAMSubscribe-Eigenschaft ab.
     * 
     */
    public boolean isProxyVerstatToCNAMSubscribe() {
        return proxyVerstatToCNAMSubscribe;
    }

    /**
     * Legt den Wert der proxyVerstatToCNAMSubscribe-Eigenschaft fest.
     * 
     */
    public void setProxyVerstatToCNAMSubscribe(boolean value) {
        this.proxyVerstatToCNAMSubscribe = value;
    }

    /**
     * Ruft den Wert der useUnknownHeadersFromCNAMNotify-Eigenschaft ab.
     * 
     */
    public boolean isUseUnknownHeadersFromCNAMNotify() {
        return useUnknownHeadersFromCNAMNotify;
    }

    /**
     * Legt den Wert der useUnknownHeadersFromCNAMNotify-Eigenschaft fest.
     * 
     */
    public void setUseUnknownHeadersFromCNAMNotify(boolean value) {
        this.useUnknownHeadersFromCNAMNotify = value;
    }

    /**
     * Ruft den Wert der enableSigningForUnscreenedTrunkGroupOriginations-Eigenschaft ab.
     * 
     */
    public boolean isEnableSigningForUnscreenedTrunkGroupOriginations() {
        return enableSigningForUnscreenedTrunkGroupOriginations;
    }

    /**
     * Legt den Wert der enableSigningForUnscreenedTrunkGroupOriginations-Eigenschaft fest.
     * 
     */
    public void setEnableSigningForUnscreenedTrunkGroupOriginations(boolean value) {
        this.enableSigningForUnscreenedTrunkGroupOriginations = value;
    }

    /**
     * Ruft den Wert der enableTaggingForUnscreenedTrunkGroupOriginations-Eigenschaft ab.
     * 
     */
    public boolean isEnableTaggingForUnscreenedTrunkGroupOriginations() {
        return enableTaggingForUnscreenedTrunkGroupOriginations;
    }

    /**
     * Legt den Wert der enableTaggingForUnscreenedTrunkGroupOriginations-Eigenschaft fest.
     * 
     */
    public void setEnableTaggingForUnscreenedTrunkGroupOriginations(boolean value) {
        this.enableTaggingForUnscreenedTrunkGroupOriginations = value;
    }

    /**
     * Ruft den Wert der unscreenedTrunkGroupOriginationAttestationLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StirShakenUnscreenedTrunkGroupOriginationAttestationLevel }
     *     
     */
    public StirShakenUnscreenedTrunkGroupOriginationAttestationLevel getUnscreenedTrunkGroupOriginationAttestationLevel() {
        return unscreenedTrunkGroupOriginationAttestationLevel;
    }

    /**
     * Legt den Wert der unscreenedTrunkGroupOriginationAttestationLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StirShakenUnscreenedTrunkGroupOriginationAttestationLevel }
     *     
     */
    public void setUnscreenedTrunkGroupOriginationAttestationLevel(StirShakenUnscreenedTrunkGroupOriginationAttestationLevel value) {
        this.unscreenedTrunkGroupOriginationAttestationLevel = value;
    }

    /**
     * Ruft den Wert der verifyGETSCalls-Eigenschaft ab.
     * 
     */
    public boolean isVerifyGETSCalls() {
        return verifyGETSCalls;
    }

    /**
     * Legt den Wert der verifyGETSCalls-Eigenschaft fest.
     * 
     */
    public void setVerifyGETSCalls(boolean value) {
        this.verifyGETSCalls = value;
    }

}
