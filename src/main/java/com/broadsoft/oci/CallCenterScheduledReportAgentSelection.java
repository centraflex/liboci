//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Either all agents or list of agents.
 *       
 * 
 * <p>Java-Klasse für CallCenterScheduledReportAgentSelection complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterScheduledReportAgentSelection">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <choice>
 *         <element name="allAgent" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="agentUserId" type="{}UserId" maxOccurs="100"/>
 *       </choice>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterScheduledReportAgentSelection", propOrder = {
    "allAgent",
    "agentUserId"
})
public class CallCenterScheduledReportAgentSelection {

    protected Boolean allAgent;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> agentUserId;

    /**
     * Ruft den Wert der allAgent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllAgent() {
        return allAgent;
    }

    /**
     * Legt den Wert der allAgent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllAgent(Boolean value) {
        this.allAgent = value;
    }

    /**
     * Gets the value of the agentUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the agentUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgentUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the agentUserId property.
     */
    public List<String> getAgentUserId() {
        if (agentUserId == null) {
            agentUserId = new ArrayList<>();
        }
        return this.agentUserId;
    }

}
