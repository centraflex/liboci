//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the Call Center statistics reporting frequency and destination.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterModifyInstanceStatisticsReportingRequest14sp9 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterModifyInstanceStatisticsReportingRequest14sp9">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="generateDailyReport" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="collectionPeriodMinutes" type="{}CallCenterStatisticsCollectionPeriodMinutes" minOccurs="0"/>
 *         <element name="reportingEmailAddress1" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="reportingEmailAddress2" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="statisticsSource" type="{}CallCenterStatisticsSource" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterModifyInstanceStatisticsReportingRequest14sp9", propOrder = {
    "serviceUserId",
    "generateDailyReport",
    "collectionPeriodMinutes",
    "reportingEmailAddress1",
    "reportingEmailAddress2",
    "statisticsSource"
})
public class GroupCallCenterModifyInstanceStatisticsReportingRequest14Sp9
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected Boolean generateDailyReport;
    protected Integer collectionPeriodMinutes;
    @XmlElementRef(name = "reportingEmailAddress1", type = JAXBElement.class, required = false)
    protected JAXBElement<String> reportingEmailAddress1;
    @XmlElementRef(name = "reportingEmailAddress2", type = JAXBElement.class, required = false)
    protected JAXBElement<String> reportingEmailAddress2;
    @XmlSchemaType(name = "token")
    protected CallCenterStatisticsSource statisticsSource;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der generateDailyReport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGenerateDailyReport() {
        return generateDailyReport;
    }

    /**
     * Legt den Wert der generateDailyReport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGenerateDailyReport(Boolean value) {
        this.generateDailyReport = value;
    }

    /**
     * Ruft den Wert der collectionPeriodMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCollectionPeriodMinutes() {
        return collectionPeriodMinutes;
    }

    /**
     * Legt den Wert der collectionPeriodMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCollectionPeriodMinutes(Integer value) {
        this.collectionPeriodMinutes = value;
    }

    /**
     * Ruft den Wert der reportingEmailAddress1-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReportingEmailAddress1() {
        return reportingEmailAddress1;
    }

    /**
     * Legt den Wert der reportingEmailAddress1-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReportingEmailAddress1(JAXBElement<String> value) {
        this.reportingEmailAddress1 = value;
    }

    /**
     * Ruft den Wert der reportingEmailAddress2-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReportingEmailAddress2() {
        return reportingEmailAddress2;
    }

    /**
     * Legt den Wert der reportingEmailAddress2-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReportingEmailAddress2(JAXBElement<String> value) {
        this.reportingEmailAddress2 = value;
    }

    /**
     * Ruft den Wert der statisticsSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterStatisticsSource }
     *     
     */
    public CallCenterStatisticsSource getStatisticsSource() {
        return statisticsSource;
    }

    /**
     * Legt den Wert der statisticsSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterStatisticsSource }
     *     
     */
    public void setStatisticsSource(CallCenterStatisticsSource value) {
        this.statisticsSource = value;
    }

}
