//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemOCIReportingParametersGetRequest22.
 *         Contains a list of system OCI Reporting parameters.
 *       
 * 
 * <p>Java-Klasse für SystemOCIReportingParametersGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemOCIReportingParametersGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serverPort" type="{}Port1025"/>
 *         <element name="enableConnectionPing" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="connectionPingIntervalSeconds" type="{}OCIReportingConnectionPingIntervalSeconds"/>
 *         <element name="alterPasswords" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enablePublicIdentityReporting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="secure" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemOCIReportingParametersGetResponse22", propOrder = {
    "serverPort",
    "enableConnectionPing",
    "connectionPingIntervalSeconds",
    "alterPasswords",
    "enablePublicIdentityReporting",
    "secure"
})
public class SystemOCIReportingParametersGetResponse22
    extends OCIDataResponse
{

    protected int serverPort;
    protected boolean enableConnectionPing;
    protected int connectionPingIntervalSeconds;
    protected boolean alterPasswords;
    protected boolean enablePublicIdentityReporting;
    protected boolean secure;

    /**
     * Ruft den Wert der serverPort-Eigenschaft ab.
     * 
     */
    public int getServerPort() {
        return serverPort;
    }

    /**
     * Legt den Wert der serverPort-Eigenschaft fest.
     * 
     */
    public void setServerPort(int value) {
        this.serverPort = value;
    }

    /**
     * Ruft den Wert der enableConnectionPing-Eigenschaft ab.
     * 
     */
    public boolean isEnableConnectionPing() {
        return enableConnectionPing;
    }

    /**
     * Legt den Wert der enableConnectionPing-Eigenschaft fest.
     * 
     */
    public void setEnableConnectionPing(boolean value) {
        this.enableConnectionPing = value;
    }

    /**
     * Ruft den Wert der connectionPingIntervalSeconds-Eigenschaft ab.
     * 
     */
    public int getConnectionPingIntervalSeconds() {
        return connectionPingIntervalSeconds;
    }

    /**
     * Legt den Wert der connectionPingIntervalSeconds-Eigenschaft fest.
     * 
     */
    public void setConnectionPingIntervalSeconds(int value) {
        this.connectionPingIntervalSeconds = value;
    }

    /**
     * Ruft den Wert der alterPasswords-Eigenschaft ab.
     * 
     */
    public boolean isAlterPasswords() {
        return alterPasswords;
    }

    /**
     * Legt den Wert der alterPasswords-Eigenschaft fest.
     * 
     */
    public void setAlterPasswords(boolean value) {
        this.alterPasswords = value;
    }

    /**
     * Ruft den Wert der enablePublicIdentityReporting-Eigenschaft ab.
     * 
     */
    public boolean isEnablePublicIdentityReporting() {
        return enablePublicIdentityReporting;
    }

    /**
     * Legt den Wert der enablePublicIdentityReporting-Eigenschaft fest.
     * 
     */
    public void setEnablePublicIdentityReporting(boolean value) {
        this.enablePublicIdentityReporting = value;
    }

    /**
     * Ruft den Wert der secure-Eigenschaft ab.
     * 
     */
    public boolean isSecure() {
        return secure;
    }

    /**
     * Legt den Wert der secure-Eigenschaft fest.
     * 
     */
    public void setSecure(boolean value) {
        this.secure = value;
    }

}
