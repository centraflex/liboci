//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a new Communication Barring Profile.
 *         The priorities for IncomingRules are requantized to consecutive integers as part of the add.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         The following elements are only used in XS data mode:
 *      incomingDefaultAction
 *      incomingDefaultCallTimeout
 *      incomingRule
 *       
 * 
 * <p>Java-Klasse für SystemCommunicationBarringProfileAddRequest17sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCommunicationBarringProfileAddRequest17sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="name" type="{}CommunicationBarringProfileName"/>
 *         <element name="description" type="{}CommunicationBarringProfileDescription" minOccurs="0"/>
 *         <element name="originatingDefaultAction" type="{}CommunicationBarringOriginatingAction"/>
 *         <element name="originatingDefaultTreatmentId" type="{}TreatmentId" minOccurs="0"/>
 *         <element name="originatingDefaultTransferNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="originatingDefaultCallTimeout" type="{}CommunicationBarringTimeoutSeconds" minOccurs="0"/>
 *         <element name="originatingRule" type="{}CommunicationBarringOriginatingRule" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="redirectingDefaultAction" type="{}CommunicationBarringRedirectingAction"/>
 *         <element name="redirectingDefaultCallTimeout" type="{}CommunicationBarringTimeoutSeconds" minOccurs="0"/>
 *         <element name="redirectingRule" type="{}CommunicationBarringRedirectingRule" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="incomingDefaultAction" type="{}CommunicationBarringIncomingAction"/>
 *         <element name="incomingDefaultCallTimeout" type="{}CommunicationBarringTimeoutSeconds" minOccurs="0"/>
 *         <element name="incomingRule" type="{}CommunicationBarringIncomingRule" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCommunicationBarringProfileAddRequest17sp1", propOrder = {
    "name",
    "description",
    "originatingDefaultAction",
    "originatingDefaultTreatmentId",
    "originatingDefaultTransferNumber",
    "originatingDefaultCallTimeout",
    "originatingRule",
    "redirectingDefaultAction",
    "redirectingDefaultCallTimeout",
    "redirectingRule",
    "incomingDefaultAction",
    "incomingDefaultCallTimeout",
    "incomingRule"
})
public class SystemCommunicationBarringProfileAddRequest17Sp1
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CommunicationBarringOriginatingAction originatingDefaultAction;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String originatingDefaultTreatmentId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String originatingDefaultTransferNumber;
    protected Integer originatingDefaultCallTimeout;
    protected List<CommunicationBarringOriginatingRule> originatingRule;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CommunicationBarringRedirectingAction redirectingDefaultAction;
    protected Integer redirectingDefaultCallTimeout;
    protected List<CommunicationBarringRedirectingRule> redirectingRule;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CommunicationBarringIncomingAction incomingDefaultAction;
    protected Integer incomingDefaultCallTimeout;
    protected List<CommunicationBarringIncomingRule> incomingRule;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der originatingDefaultAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringOriginatingAction }
     *     
     */
    public CommunicationBarringOriginatingAction getOriginatingDefaultAction() {
        return originatingDefaultAction;
    }

    /**
     * Legt den Wert der originatingDefaultAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringOriginatingAction }
     *     
     */
    public void setOriginatingDefaultAction(CommunicationBarringOriginatingAction value) {
        this.originatingDefaultAction = value;
    }

    /**
     * Ruft den Wert der originatingDefaultTreatmentId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatingDefaultTreatmentId() {
        return originatingDefaultTreatmentId;
    }

    /**
     * Legt den Wert der originatingDefaultTreatmentId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatingDefaultTreatmentId(String value) {
        this.originatingDefaultTreatmentId = value;
    }

    /**
     * Ruft den Wert der originatingDefaultTransferNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatingDefaultTransferNumber() {
        return originatingDefaultTransferNumber;
    }

    /**
     * Legt den Wert der originatingDefaultTransferNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatingDefaultTransferNumber(String value) {
        this.originatingDefaultTransferNumber = value;
    }

    /**
     * Ruft den Wert der originatingDefaultCallTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOriginatingDefaultCallTimeout() {
        return originatingDefaultCallTimeout;
    }

    /**
     * Legt den Wert der originatingDefaultCallTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOriginatingDefaultCallTimeout(Integer value) {
        this.originatingDefaultCallTimeout = value;
    }

    /**
     * Gets the value of the originatingRule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the originatingRule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOriginatingRule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationBarringOriginatingRule }
     * 
     * 
     * @return
     *     The value of the originatingRule property.
     */
    public List<CommunicationBarringOriginatingRule> getOriginatingRule() {
        if (originatingRule == null) {
            originatingRule = new ArrayList<>();
        }
        return this.originatingRule;
    }

    /**
     * Ruft den Wert der redirectingDefaultAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringRedirectingAction }
     *     
     */
    public CommunicationBarringRedirectingAction getRedirectingDefaultAction() {
        return redirectingDefaultAction;
    }

    /**
     * Legt den Wert der redirectingDefaultAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringRedirectingAction }
     *     
     */
    public void setRedirectingDefaultAction(CommunicationBarringRedirectingAction value) {
        this.redirectingDefaultAction = value;
    }

    /**
     * Ruft den Wert der redirectingDefaultCallTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRedirectingDefaultCallTimeout() {
        return redirectingDefaultCallTimeout;
    }

    /**
     * Legt den Wert der redirectingDefaultCallTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRedirectingDefaultCallTimeout(Integer value) {
        this.redirectingDefaultCallTimeout = value;
    }

    /**
     * Gets the value of the redirectingRule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the redirectingRule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRedirectingRule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationBarringRedirectingRule }
     * 
     * 
     * @return
     *     The value of the redirectingRule property.
     */
    public List<CommunicationBarringRedirectingRule> getRedirectingRule() {
        if (redirectingRule == null) {
            redirectingRule = new ArrayList<>();
        }
        return this.redirectingRule;
    }

    /**
     * Ruft den Wert der incomingDefaultAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringIncomingAction }
     *     
     */
    public CommunicationBarringIncomingAction getIncomingDefaultAction() {
        return incomingDefaultAction;
    }

    /**
     * Legt den Wert der incomingDefaultAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringIncomingAction }
     *     
     */
    public void setIncomingDefaultAction(CommunicationBarringIncomingAction value) {
        this.incomingDefaultAction = value;
    }

    /**
     * Ruft den Wert der incomingDefaultCallTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIncomingDefaultCallTimeout() {
        return incomingDefaultCallTimeout;
    }

    /**
     * Legt den Wert der incomingDefaultCallTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIncomingDefaultCallTimeout(Integer value) {
        this.incomingDefaultCallTimeout = value;
    }

    /**
     * Gets the value of the incomingRule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the incomingRule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncomingRule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationBarringIncomingRule }
     * 
     * 
     * @return
     *     The value of the incomingRule property.
     */
    public List<CommunicationBarringIncomingRule> getIncomingRule() {
        if (incomingRule == null) {
            incomingRule = new ArrayList<>();
        }
        return this.incomingRule;
    }

}
