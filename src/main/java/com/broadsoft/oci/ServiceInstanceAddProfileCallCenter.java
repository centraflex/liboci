//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Service Profile Information for a call center.
 *         Password is required.
 *       
 * 
 * <p>Java-Klasse für ServiceInstanceAddProfileCallCenter complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceInstanceAddProfileCallCenter">
 *   <complexContent>
 *     <restriction base="{}ServiceInstanceAddProfile">
 *       <sequence>
 *         <element name="name" type="{}ServiceInstanceProfileName"/>
 *         <element name="callingLineIdLastName" type="{}CallingLineIdLastName"/>
 *         <element name="callingLineIdFirstName" type="{}CallingLineIdFirstName"/>
 *         <element name="hiraganaLastName" type="{}HiraganaLastName" minOccurs="0"/>
 *         <element name="hiraganaFirstName" type="{}HiraganaFirstName" minOccurs="0"/>
 *         <element name="phoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="extension" type="{}Extension17" minOccurs="0"/>
 *         <element name="password" type="{}Password"/>
 *         <element name="department" type="{}DepartmentKey" minOccurs="0"/>
 *         <element name="language" type="{}Language" minOccurs="0"/>
 *         <element name="timeZone" type="{}TimeZone" minOccurs="0"/>
 *         <element name="alias" type="{}SIPURI" maxOccurs="3" minOccurs="0"/>
 *         <element name="publicUserIdentity" type="{}SIPURI" minOccurs="0"/>
 *         <element name="callingLineIdPhoneNumber" type="{}DN" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceInstanceAddProfileCallCenter")
public class ServiceInstanceAddProfileCallCenter
    extends ServiceInstanceAddProfile
{


}
