//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für VoiceMessagingStorageMode.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="VoiceMessagingStorageMode">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Cloud"/>
 *     <enumeration value="Mail Server"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "VoiceMessagingStorageMode")
@XmlEnum
public enum VoiceMessagingStorageMode {

    @XmlEnumValue("Cloud")
    CLOUD("Cloud"),
    @XmlEnumValue("Mail Server")
    MAIL_SERVER("Mail Server");
    private final String value;

    VoiceMessagingStorageMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VoiceMessagingStorageMode fromValue(String v) {
        for (VoiceMessagingStorageMode c: VoiceMessagingStorageMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
