//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupRoutePointGetAnnouncementRequest19.
 *       
 * 
 * <p>Java-Klasse für GroupRoutePointGetAnnouncementResponse19 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupRoutePointGetAnnouncementResponse19">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="mediaOnHoldSource" type="{}CallCenterMediaOnHoldSourceRead19"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupRoutePointGetAnnouncementResponse19", propOrder = {
    "mediaOnHoldSource"
})
public class GroupRoutePointGetAnnouncementResponse19
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected CallCenterMediaOnHoldSourceRead19 mediaOnHoldSource;

    /**
     * Ruft den Wert der mediaOnHoldSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldSourceRead19 }
     *     
     */
    public CallCenterMediaOnHoldSourceRead19 getMediaOnHoldSource() {
        return mediaOnHoldSource;
    }

    /**
     * Legt den Wert der mediaOnHoldSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldSourceRead19 }
     *     
     */
    public void setMediaOnHoldSource(CallCenterMediaOnHoldSourceRead19 value) {
        this.mediaOnHoldSource = value;
    }

}
