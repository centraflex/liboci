//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a call center's night service settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterNightServiceModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterNightServiceModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="action" type="{}CallCenterScheduledServiceAction" minOccurs="0"/>
 *         <element name="businessHours" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="forceNightService" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowManualOverrideViaFAC" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="transferPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="playAnnouncementBeforeAction" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="audioMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="audioFile" type="{}ExtendedMediaFileResource" minOccurs="0"/>
 *         <element name="videoMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="videoFile" type="{}ExtendedMediaFileResource" minOccurs="0"/>
 *         <element name="manualAnnouncementMode" type="{}CallCenterManualNightServiceAnnouncementMode" minOccurs="0"/>
 *         <element name="manualAudioMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="manualAudioFile" type="{}ExtendedMediaFileResource" minOccurs="0"/>
 *         <element name="manualVideoMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="manualVideoFile" type="{}ExtendedMediaFileResource" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterNightServiceModifyRequest", propOrder = {
    "serviceUserId",
    "action",
    "businessHours",
    "forceNightService",
    "allowManualOverrideViaFAC",
    "transferPhoneNumber",
    "playAnnouncementBeforeAction",
    "audioMessageSelection",
    "audioFile",
    "videoMessageSelection",
    "videoFile",
    "manualAnnouncementMode",
    "manualAudioMessageSelection",
    "manualAudioFile",
    "manualVideoMessageSelection",
    "manualVideoFile"
})
public class GroupCallCenterNightServiceModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlSchemaType(name = "token")
    protected CallCenterScheduledServiceAction action;
    @XmlElementRef(name = "businessHours", type = JAXBElement.class, required = false)
    protected JAXBElement<String> businessHours;
    protected Boolean forceNightService;
    protected Boolean allowManualOverrideViaFAC;
    @XmlElementRef(name = "transferPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transferPhoneNumber;
    protected Boolean playAnnouncementBeforeAction;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection audioMessageSelection;
    protected ExtendedMediaFileResource audioFile;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection videoMessageSelection;
    protected ExtendedMediaFileResource videoFile;
    @XmlSchemaType(name = "token")
    protected CallCenterManualNightServiceAnnouncementMode manualAnnouncementMode;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection manualAudioMessageSelection;
    protected ExtendedMediaFileResource manualAudioFile;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection manualVideoMessageSelection;
    protected ExtendedMediaFileResource manualVideoFile;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der action-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterScheduledServiceAction }
     *     
     */
    public CallCenterScheduledServiceAction getAction() {
        return action;
    }

    /**
     * Legt den Wert der action-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterScheduledServiceAction }
     *     
     */
    public void setAction(CallCenterScheduledServiceAction value) {
        this.action = value;
    }

    /**
     * Ruft den Wert der businessHours-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBusinessHours() {
        return businessHours;
    }

    /**
     * Legt den Wert der businessHours-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBusinessHours(JAXBElement<String> value) {
        this.businessHours = value;
    }

    /**
     * Ruft den Wert der forceNightService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceNightService() {
        return forceNightService;
    }

    /**
     * Legt den Wert der forceNightService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceNightService(Boolean value) {
        this.forceNightService = value;
    }

    /**
     * Ruft den Wert der allowManualOverrideViaFAC-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowManualOverrideViaFAC() {
        return allowManualOverrideViaFAC;
    }

    /**
     * Legt den Wert der allowManualOverrideViaFAC-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowManualOverrideViaFAC(Boolean value) {
        this.allowManualOverrideViaFAC = value;
    }

    /**
     * Ruft den Wert der transferPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransferPhoneNumber() {
        return transferPhoneNumber;
    }

    /**
     * Legt den Wert der transferPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransferPhoneNumber(JAXBElement<String> value) {
        this.transferPhoneNumber = value;
    }

    /**
     * Ruft den Wert der playAnnouncementBeforeAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayAnnouncementBeforeAction() {
        return playAnnouncementBeforeAction;
    }

    /**
     * Legt den Wert der playAnnouncementBeforeAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayAnnouncementBeforeAction(Boolean value) {
        this.playAnnouncementBeforeAction = value;
    }

    /**
     * Ruft den Wert der audioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getAudioMessageSelection() {
        return audioMessageSelection;
    }

    /**
     * Legt den Wert der audioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.audioMessageSelection = value;
    }

    /**
     * Ruft den Wert der audioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public ExtendedMediaFileResource getAudioFile() {
        return audioFile;
    }

    /**
     * Legt den Wert der audioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public void setAudioFile(ExtendedMediaFileResource value) {
        this.audioFile = value;
    }

    /**
     * Ruft den Wert der videoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getVideoMessageSelection() {
        return videoMessageSelection;
    }

    /**
     * Legt den Wert der videoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.videoMessageSelection = value;
    }

    /**
     * Ruft den Wert der videoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public ExtendedMediaFileResource getVideoFile() {
        return videoFile;
    }

    /**
     * Legt den Wert der videoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public void setVideoFile(ExtendedMediaFileResource value) {
        this.videoFile = value;
    }

    /**
     * Ruft den Wert der manualAnnouncementMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterManualNightServiceAnnouncementMode }
     *     
     */
    public CallCenterManualNightServiceAnnouncementMode getManualAnnouncementMode() {
        return manualAnnouncementMode;
    }

    /**
     * Legt den Wert der manualAnnouncementMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterManualNightServiceAnnouncementMode }
     *     
     */
    public void setManualAnnouncementMode(CallCenterManualNightServiceAnnouncementMode value) {
        this.manualAnnouncementMode = value;
    }

    /**
     * Ruft den Wert der manualAudioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getManualAudioMessageSelection() {
        return manualAudioMessageSelection;
    }

    /**
     * Legt den Wert der manualAudioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setManualAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.manualAudioMessageSelection = value;
    }

    /**
     * Ruft den Wert der manualAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public ExtendedMediaFileResource getManualAudioFile() {
        return manualAudioFile;
    }

    /**
     * Legt den Wert der manualAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public void setManualAudioFile(ExtendedMediaFileResource value) {
        this.manualAudioFile = value;
    }

    /**
     * Ruft den Wert der manualVideoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getManualVideoMessageSelection() {
        return manualVideoMessageSelection;
    }

    /**
     * Legt den Wert der manualVideoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setManualVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.manualVideoMessageSelection = value;
    }

    /**
     * Ruft den Wert der manualVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public ExtendedMediaFileResource getManualVideoFile() {
        return manualVideoFile;
    }

    /**
     * Legt den Wert der manualVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public void setManualVideoFile(ExtendedMediaFileResource value) {
        this.manualVideoFile = value;
    }

}
