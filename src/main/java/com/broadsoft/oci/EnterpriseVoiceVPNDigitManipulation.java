//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Enterprise Voice VPN Digit Manipulation Entry.
 *       
 * 
 * <p>Java-Klasse für EnterpriseVoiceVPNDigitManipulation complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseVoiceVPNDigitManipulation">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseVoiceVPNDigitManipulation")
@XmlSeeAlso({
    EnterpriseVoiceVPNDigitManipulationNoValue.class,
    EnterpriseVoiceVPNDigitManipulationOptionalValue.class,
    EnterpriseVoiceVPNDigitManipulationRequiredValue.class
})
public abstract class EnterpriseVoiceVPNDigitManipulation {


}
