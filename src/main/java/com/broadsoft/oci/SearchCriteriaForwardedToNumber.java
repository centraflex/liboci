//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Criteria for searching for a forwarded to number.
 *         This search criteria data type is only intended to be used by the commands 
 *         introduced by BW-2301. 
 *         The commands are EnterpriseUserCallForwardingSettingsGetListRequest 
 *         and GroupUserCallForwardingSettingsGetListRequest.
 *         The following Call Forwarding services are compatible for this search:
 *         Call Forwarding Always, Call Forwarding Always Secondary, Call Forwarding Busy,
 *         Call Forwarding No Answer, Call Forwarding Not Reachable, Call Forwarding Selective.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaForwardedToNumber complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaForwardedToNumber">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="mode" type="{}SearchMode"/>
 *         <element name="value" type="{}DN"/>
 *         <element name="isCaseInsensitive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaForwardedToNumber", propOrder = {
    "mode",
    "value",
    "isCaseInsensitive"
})
public class SearchCriteriaForwardedToNumber
    extends SearchCriteria
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SearchMode mode;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String value;
    @XmlElement(defaultValue = "true")
    protected boolean isCaseInsensitive;

    /**
     * Ruft den Wert der mode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchMode }
     *     
     */
    public SearchMode getMode() {
        return mode;
    }

    /**
     * Legt den Wert der mode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchMode }
     *     
     */
    public void setMode(SearchMode value) {
        this.mode = value;
    }

    /**
     * Ruft den Wert der value-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Legt den Wert der value-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Ruft den Wert der isCaseInsensitive-Eigenschaft ab.
     * 
     */
    public boolean isIsCaseInsensitive() {
        return isCaseInsensitive;
    }

    /**
     * Legt den Wert der isCaseInsensitive-Eigenschaft fest.
     * 
     */
    public void setIsCaseInsensitive(boolean value) {
        this.isCaseInsensitive = value;
    }

}
