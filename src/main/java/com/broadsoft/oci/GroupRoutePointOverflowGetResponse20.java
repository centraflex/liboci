//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupRoutePointOverflowGetRequest20.
 *       
 * 
 * <p>Java-Klasse für GroupRoutePointOverflowGetResponse20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupRoutePointOverflowGetResponse20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="action" type="{}CallCenterOverflowProcessingAction"/>
 *         <element name="transferPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="overflowAfterTimeout" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="timeoutSeconds" type="{}HuntForwardTimeoutSeconds"/>
 *         <element name="playAnnouncementBeforeOverflowProcessing" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="audioMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="audioUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="audioFileList" type="{}CallCenterAnnouncementFileListRead20" minOccurs="0"/>
 *         <element name="videoMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="videoUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="videoFileList" type="{}CallCenterAnnouncementFileListRead20" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupRoutePointOverflowGetResponse20", propOrder = {
    "action",
    "transferPhoneNumber",
    "overflowAfterTimeout",
    "timeoutSeconds",
    "playAnnouncementBeforeOverflowProcessing",
    "audioMessageSelection",
    "audioUrlList",
    "audioFileList",
    "videoMessageSelection",
    "videoUrlList",
    "videoFileList"
})
public class GroupRoutePointOverflowGetResponse20
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterOverflowProcessingAction action;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String transferPhoneNumber;
    protected boolean overflowAfterTimeout;
    protected int timeoutSeconds;
    protected boolean playAnnouncementBeforeOverflowProcessing;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection audioMessageSelection;
    protected CallCenterAnnouncementURLList audioUrlList;
    protected CallCenterAnnouncementFileListRead20 audioFileList;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection videoMessageSelection;
    protected CallCenterAnnouncementURLList videoUrlList;
    protected CallCenterAnnouncementFileListRead20 videoFileList;

    /**
     * Ruft den Wert der action-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterOverflowProcessingAction }
     *     
     */
    public CallCenterOverflowProcessingAction getAction() {
        return action;
    }

    /**
     * Legt den Wert der action-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterOverflowProcessingAction }
     *     
     */
    public void setAction(CallCenterOverflowProcessingAction value) {
        this.action = value;
    }

    /**
     * Ruft den Wert der transferPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferPhoneNumber() {
        return transferPhoneNumber;
    }

    /**
     * Legt den Wert der transferPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferPhoneNumber(String value) {
        this.transferPhoneNumber = value;
    }

    /**
     * Ruft den Wert der overflowAfterTimeout-Eigenschaft ab.
     * 
     */
    public boolean isOverflowAfterTimeout() {
        return overflowAfterTimeout;
    }

    /**
     * Legt den Wert der overflowAfterTimeout-Eigenschaft fest.
     * 
     */
    public void setOverflowAfterTimeout(boolean value) {
        this.overflowAfterTimeout = value;
    }

    /**
     * Ruft den Wert der timeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getTimeoutSeconds() {
        return timeoutSeconds;
    }

    /**
     * Legt den Wert der timeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setTimeoutSeconds(int value) {
        this.timeoutSeconds = value;
    }

    /**
     * Ruft den Wert der playAnnouncementBeforeOverflowProcessing-Eigenschaft ab.
     * 
     */
    public boolean isPlayAnnouncementBeforeOverflowProcessing() {
        return playAnnouncementBeforeOverflowProcessing;
    }

    /**
     * Legt den Wert der playAnnouncementBeforeOverflowProcessing-Eigenschaft fest.
     * 
     */
    public void setPlayAnnouncementBeforeOverflowProcessing(boolean value) {
        this.playAnnouncementBeforeOverflowProcessing = value;
    }

    /**
     * Ruft den Wert der audioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getAudioMessageSelection() {
        return audioMessageSelection;
    }

    /**
     * Legt den Wert der audioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.audioMessageSelection = value;
    }

    /**
     * Ruft den Wert der audioUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getAudioUrlList() {
        return audioUrlList;
    }

    /**
     * Legt den Wert der audioUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setAudioUrlList(CallCenterAnnouncementURLList value) {
        this.audioUrlList = value;
    }

    /**
     * Ruft den Wert der audioFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListRead20 }
     *     
     */
    public CallCenterAnnouncementFileListRead20 getAudioFileList() {
        return audioFileList;
    }

    /**
     * Legt den Wert der audioFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListRead20 }
     *     
     */
    public void setAudioFileList(CallCenterAnnouncementFileListRead20 value) {
        this.audioFileList = value;
    }

    /**
     * Ruft den Wert der videoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getVideoMessageSelection() {
        return videoMessageSelection;
    }

    /**
     * Legt den Wert der videoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.videoMessageSelection = value;
    }

    /**
     * Ruft den Wert der videoUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getVideoUrlList() {
        return videoUrlList;
    }

    /**
     * Legt den Wert der videoUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setVideoUrlList(CallCenterAnnouncementURLList value) {
        this.videoUrlList = value;
    }

    /**
     * Ruft den Wert der videoFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListRead20 }
     *     
     */
    public CallCenterAnnouncementFileListRead20 getVideoFileList() {
        return videoFileList;
    }

    /**
     * Legt den Wert der videoFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListRead20 }
     *     
     */
    public void setVideoFileList(CallCenterAnnouncementFileListRead20 value) {
        this.videoFileList = value;
    }

}
