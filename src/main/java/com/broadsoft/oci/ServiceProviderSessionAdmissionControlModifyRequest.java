//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify session admission control capacity for the service provider.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderSessionAdmissionControlModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderSessionAdmissionControlModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="restrictAggregateSessions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxSessions" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="maxUserOriginatingSessions" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="maxUserTerminatingSessions" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="countIntraServiceProviderSessions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderSessionAdmissionControlModifyRequest", propOrder = {
    "serviceProviderId",
    "restrictAggregateSessions",
    "maxSessions",
    "maxUserOriginatingSessions",
    "maxUserTerminatingSessions",
    "countIntraServiceProviderSessions"
})
public class ServiceProviderSessionAdmissionControlModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected Boolean restrictAggregateSessions;
    @XmlElementRef(name = "maxSessions", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxSessions;
    @XmlElementRef(name = "maxUserOriginatingSessions", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxUserOriginatingSessions;
    @XmlElementRef(name = "maxUserTerminatingSessions", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxUserTerminatingSessions;
    protected Boolean countIntraServiceProviderSessions;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der restrictAggregateSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictAggregateSessions() {
        return restrictAggregateSessions;
    }

    /**
     * Legt den Wert der restrictAggregateSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictAggregateSessions(Boolean value) {
        this.restrictAggregateSessions = value;
    }

    /**
     * Ruft den Wert der maxSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxSessions() {
        return maxSessions;
    }

    /**
     * Legt den Wert der maxSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxSessions(JAXBElement<Integer> value) {
        this.maxSessions = value;
    }

    /**
     * Ruft den Wert der maxUserOriginatingSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxUserOriginatingSessions() {
        return maxUserOriginatingSessions;
    }

    /**
     * Legt den Wert der maxUserOriginatingSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxUserOriginatingSessions(JAXBElement<Integer> value) {
        this.maxUserOriginatingSessions = value;
    }

    /**
     * Ruft den Wert der maxUserTerminatingSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxUserTerminatingSessions() {
        return maxUserTerminatingSessions;
    }

    /**
     * Legt den Wert der maxUserTerminatingSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxUserTerminatingSessions(JAXBElement<Integer> value) {
        this.maxUserTerminatingSessions = value;
    }

    /**
     * Ruft den Wert der countIntraServiceProviderSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCountIntraServiceProviderSessions() {
        return countIntraServiceProviderSessions;
    }

    /**
     * Legt den Wert der countIntraServiceProviderSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCountIntraServiceProviderSessions(Boolean value) {
        this.countIntraServiceProviderSessions = value;
    }

}
