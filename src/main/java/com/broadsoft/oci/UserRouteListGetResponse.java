//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserRouteListGetRequest.
 *         Contains the route list setting and a list of assigned number ranges and number prefixes.
 *         The column headings for assignedNumberRangeTable are "Number Range Start", "Number Range End" and "Is Active".
 *         
 *         Replaced by: UserRouteListGetResponse22
 *       
 * 
 * <p>Java-Klasse für UserRouteListGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserRouteListGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="treatOriginationsAndPBXRedirectionsAsScreened" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useRouteListIdentityForNonEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useRouteListIdentityForEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="assignedNumberRangeTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserRouteListGetResponse", propOrder = {
    "treatOriginationsAndPBXRedirectionsAsScreened",
    "useRouteListIdentityForNonEmergencyCalls",
    "useRouteListIdentityForEmergencyCalls",
    "assignedNumberRangeTable"
})
public class UserRouteListGetResponse
    extends OCIDataResponse
{

    protected boolean treatOriginationsAndPBXRedirectionsAsScreened;
    protected boolean useRouteListIdentityForNonEmergencyCalls;
    protected boolean useRouteListIdentityForEmergencyCalls;
    @XmlElement(required = true)
    protected OCITable assignedNumberRangeTable;

    /**
     * Ruft den Wert der treatOriginationsAndPBXRedirectionsAsScreened-Eigenschaft ab.
     * 
     */
    public boolean isTreatOriginationsAndPBXRedirectionsAsScreened() {
        return treatOriginationsAndPBXRedirectionsAsScreened;
    }

    /**
     * Legt den Wert der treatOriginationsAndPBXRedirectionsAsScreened-Eigenschaft fest.
     * 
     */
    public void setTreatOriginationsAndPBXRedirectionsAsScreened(boolean value) {
        this.treatOriginationsAndPBXRedirectionsAsScreened = value;
    }

    /**
     * Ruft den Wert der useRouteListIdentityForNonEmergencyCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseRouteListIdentityForNonEmergencyCalls() {
        return useRouteListIdentityForNonEmergencyCalls;
    }

    /**
     * Legt den Wert der useRouteListIdentityForNonEmergencyCalls-Eigenschaft fest.
     * 
     */
    public void setUseRouteListIdentityForNonEmergencyCalls(boolean value) {
        this.useRouteListIdentityForNonEmergencyCalls = value;
    }

    /**
     * Ruft den Wert der useRouteListIdentityForEmergencyCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseRouteListIdentityForEmergencyCalls() {
        return useRouteListIdentityForEmergencyCalls;
    }

    /**
     * Legt den Wert der useRouteListIdentityForEmergencyCalls-Eigenschaft fest.
     * 
     */
    public void setUseRouteListIdentityForEmergencyCalls(boolean value) {
        this.useRouteListIdentityForEmergencyCalls = value;
    }

    /**
     * Ruft den Wert der assignedNumberRangeTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAssignedNumberRangeTable() {
        return assignedNumberRangeTable;
    }

    /**
     * Legt den Wert der assignedNumberRangeTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAssignedNumberRangeTable(OCITable value) {
        this.assignedNumberRangeTable = value;
    }

}
