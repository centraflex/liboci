//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a device CPE config file server.
 *         The response is either SuccessResponse or ErrorResponse.
 *         Replaced By: SystemCPEConfigModifyFileServerRequest14sp6
 *       
 * 
 * <p>Java-Klasse für SystemCPEConfigModifyFileServerRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCPEConfigModifyFileServerRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="deviceType" type="{}AccessDeviceType"/>
 *         <element name="ftpHostNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="ftpUserId" type="{}FTPUserId" minOccurs="0"/>
 *         <element name="ftpUserPassword" type="{}FTPUserPassword" minOccurs="0"/>
 *         <element name="cpeFileDirectory" type="{}CPEFileDirectory" minOccurs="0"/>
 *         <element name="passiveFTP" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCPEConfigModifyFileServerRequest", propOrder = {
    "deviceType",
    "ftpHostNetAddress",
    "ftpUserId",
    "ftpUserPassword",
    "cpeFileDirectory",
    "passiveFTP"
})
public class SystemCPEConfigModifyFileServerRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String ftpHostNetAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String ftpUserId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String ftpUserPassword;
    @XmlElementRef(name = "cpeFileDirectory", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cpeFileDirectory;
    protected Boolean passiveFTP;

    /**
     * Ruft den Wert der deviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Legt den Wert der deviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceType(String value) {
        this.deviceType = value;
    }

    /**
     * Ruft den Wert der ftpHostNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFtpHostNetAddress() {
        return ftpHostNetAddress;
    }

    /**
     * Legt den Wert der ftpHostNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFtpHostNetAddress(String value) {
        this.ftpHostNetAddress = value;
    }

    /**
     * Ruft den Wert der ftpUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFtpUserId() {
        return ftpUserId;
    }

    /**
     * Legt den Wert der ftpUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFtpUserId(String value) {
        this.ftpUserId = value;
    }

    /**
     * Ruft den Wert der ftpUserPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFtpUserPassword() {
        return ftpUserPassword;
    }

    /**
     * Legt den Wert der ftpUserPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFtpUserPassword(String value) {
        this.ftpUserPassword = value;
    }

    /**
     * Ruft den Wert der cpeFileDirectory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCpeFileDirectory() {
        return cpeFileDirectory;
    }

    /**
     * Legt den Wert der cpeFileDirectory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCpeFileDirectory(JAXBElement<String> value) {
        this.cpeFileDirectory = value;
    }

    /**
     * Ruft den Wert der passiveFTP-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPassiveFTP() {
        return passiveFTP;
    }

    /**
     * Legt den Wert der passiveFTP-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPassiveFTP(Boolean value) {
        this.passiveFTP = value;
    }

}
