//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *          Response to UserCollaborateRoomGetRequest.
 *           The roomType and roomName parameters are returned for all rooms.
 *           The following parameters are returned for My room and Project Room:
 *           attendeeNotification, endCollaborateRoomSessionOnOwnerExit and 
 *           ownerRequired. In addition, the roomSchedule is returned for 
 *           Project Room and the instantRoomStartTime and instantRoomEndTime 
 *           are returned for Instant Room.
 *       
 * 
 * <p>Java-Klasse für UserCollaborateRoomGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCollaborateRoomGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="roomType" type="{}CollaborateRoomType"/>
 *         <element name="roomName" type="{}CollaborateRoomName"/>
 *         <element name="attendeeNotification" type="{}CollaborateRoomAttendeeNotification" minOccurs="0"/>
 *         <element name="endCollaborateRoomSessionOnOwnerExit" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="ownerRequired" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="instantRoomStartTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         <element name="instantRoomEndTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         <element name="roomSchedule" type="{}CollaborateRoomSchedule" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCollaborateRoomGetResponse", propOrder = {
    "roomType",
    "roomName",
    "attendeeNotification",
    "endCollaborateRoomSessionOnOwnerExit",
    "ownerRequired",
    "instantRoomStartTime",
    "instantRoomEndTime",
    "roomSchedule"
})
public class UserCollaborateRoomGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CollaborateRoomType roomType;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String roomName;
    @XmlSchemaType(name = "token")
    protected CollaborateRoomAttendeeNotification attendeeNotification;
    protected Boolean endCollaborateRoomSessionOnOwnerExit;
    protected Boolean ownerRequired;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar instantRoomStartTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar instantRoomEndTime;
    protected CollaborateRoomSchedule roomSchedule;

    /**
     * Ruft den Wert der roomType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CollaborateRoomType }
     *     
     */
    public CollaborateRoomType getRoomType() {
        return roomType;
    }

    /**
     * Legt den Wert der roomType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CollaborateRoomType }
     *     
     */
    public void setRoomType(CollaborateRoomType value) {
        this.roomType = value;
    }

    /**
     * Ruft den Wert der roomName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomName() {
        return roomName;
    }

    /**
     * Legt den Wert der roomName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomName(String value) {
        this.roomName = value;
    }

    /**
     * Ruft den Wert der attendeeNotification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CollaborateRoomAttendeeNotification }
     *     
     */
    public CollaborateRoomAttendeeNotification getAttendeeNotification() {
        return attendeeNotification;
    }

    /**
     * Legt den Wert der attendeeNotification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CollaborateRoomAttendeeNotification }
     *     
     */
    public void setAttendeeNotification(CollaborateRoomAttendeeNotification value) {
        this.attendeeNotification = value;
    }

    /**
     * Ruft den Wert der endCollaborateRoomSessionOnOwnerExit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEndCollaborateRoomSessionOnOwnerExit() {
        return endCollaborateRoomSessionOnOwnerExit;
    }

    /**
     * Legt den Wert der endCollaborateRoomSessionOnOwnerExit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEndCollaborateRoomSessionOnOwnerExit(Boolean value) {
        this.endCollaborateRoomSessionOnOwnerExit = value;
    }

    /**
     * Ruft den Wert der ownerRequired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOwnerRequired() {
        return ownerRequired;
    }

    /**
     * Legt den Wert der ownerRequired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOwnerRequired(Boolean value) {
        this.ownerRequired = value;
    }

    /**
     * Ruft den Wert der instantRoomStartTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInstantRoomStartTime() {
        return instantRoomStartTime;
    }

    /**
     * Legt den Wert der instantRoomStartTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInstantRoomStartTime(XMLGregorianCalendar value) {
        this.instantRoomStartTime = value;
    }

    /**
     * Ruft den Wert der instantRoomEndTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInstantRoomEndTime() {
        return instantRoomEndTime;
    }

    /**
     * Legt den Wert der instantRoomEndTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInstantRoomEndTime(XMLGregorianCalendar value) {
        this.instantRoomEndTime = value;
    }

    /**
     * Ruft den Wert der roomSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CollaborateRoomSchedule }
     *     
     */
    public CollaborateRoomSchedule getRoomSchedule() {
        return roomSchedule;
    }

    /**
     * Legt den Wert der roomSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CollaborateRoomSchedule }
     *     
     */
    public void setRoomSchedule(CollaborateRoomSchedule value) {
        this.roomSchedule = value;
    }

}
