//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the User Personal Assistant information.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserPersonalAssistantModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPersonalAssistantModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="presence" type="{}PersonalAssistantPresence" minOccurs="0"/>
 *         <element name="enableTransferToAttendant" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="attendantNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="enableRingSplash" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableExpirationTime" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="expirationTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         <element name="alertMeFirst" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="alertMeFirstNumberOfRings" type="{}PersonalAssistantAlertMeFirstNumberOfRings" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPersonalAssistantModifyRequest", propOrder = {
    "userId",
    "presence",
    "enableTransferToAttendant",
    "attendantNumber",
    "enableRingSplash",
    "enableExpirationTime",
    "expirationTime",
    "alertMeFirst",
    "alertMeFirstNumberOfRings"
})
public class UserPersonalAssistantModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlSchemaType(name = "token")
    protected PersonalAssistantPresence presence;
    protected Boolean enableTransferToAttendant;
    @XmlElementRef(name = "attendantNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attendantNumber;
    protected Boolean enableRingSplash;
    protected Boolean enableExpirationTime;
    @XmlElementRef(name = "expirationTime", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> expirationTime;
    protected Boolean alertMeFirst;
    protected Integer alertMeFirstNumberOfRings;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der presence-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PersonalAssistantPresence }
     *     
     */
    public PersonalAssistantPresence getPresence() {
        return presence;
    }

    /**
     * Legt den Wert der presence-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalAssistantPresence }
     *     
     */
    public void setPresence(PersonalAssistantPresence value) {
        this.presence = value;
    }

    /**
     * Ruft den Wert der enableTransferToAttendant-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableTransferToAttendant() {
        return enableTransferToAttendant;
    }

    /**
     * Legt den Wert der enableTransferToAttendant-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableTransferToAttendant(Boolean value) {
        this.enableTransferToAttendant = value;
    }

    /**
     * Ruft den Wert der attendantNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttendantNumber() {
        return attendantNumber;
    }

    /**
     * Legt den Wert der attendantNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttendantNumber(JAXBElement<String> value) {
        this.attendantNumber = value;
    }

    /**
     * Ruft den Wert der enableRingSplash-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableRingSplash() {
        return enableRingSplash;
    }

    /**
     * Legt den Wert der enableRingSplash-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableRingSplash(Boolean value) {
        this.enableRingSplash = value;
    }

    /**
     * Ruft den Wert der enableExpirationTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableExpirationTime() {
        return enableExpirationTime;
    }

    /**
     * Legt den Wert der enableExpirationTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableExpirationTime(Boolean value) {
        this.enableExpirationTime = value;
    }

    /**
     * Ruft den Wert der expirationTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getExpirationTime() {
        return expirationTime;
    }

    /**
     * Legt den Wert der expirationTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setExpirationTime(JAXBElement<XMLGregorianCalendar> value) {
        this.expirationTime = value;
    }

    /**
     * Ruft den Wert der alertMeFirst-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlertMeFirst() {
        return alertMeFirst;
    }

    /**
     * Legt den Wert der alertMeFirst-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlertMeFirst(Boolean value) {
        this.alertMeFirst = value;
    }

    /**
     * Ruft den Wert der alertMeFirstNumberOfRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAlertMeFirstNumberOfRings() {
        return alertMeFirstNumberOfRings;
    }

    /**
     * Legt den Wert der alertMeFirstNumberOfRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAlertMeFirstNumberOfRings(Integer value) {
        this.alertMeFirstNumberOfRings = value;
    }

}
