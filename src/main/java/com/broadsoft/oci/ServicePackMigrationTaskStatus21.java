//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ServicePackMigrationTaskStatus21.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ServicePackMigrationTaskStatus21">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Awaiting Edits"/>
 *     <enumeration value="Pending"/>
 *     <enumeration value="Processing"/>
 *     <enumeration value="Terminating"/>
 *     <enumeration value="Terminated"/>
 *     <enumeration value="Stopped By System"/>
 *     <enumeration value="Completed"/>
 *     <enumeration value="Expired"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ServicePackMigrationTaskStatus21")
@XmlEnum
public enum ServicePackMigrationTaskStatus21 {

    @XmlEnumValue("Awaiting Edits")
    AWAITING_EDITS("Awaiting Edits"),
    @XmlEnumValue("Pending")
    PENDING("Pending"),
    @XmlEnumValue("Processing")
    PROCESSING("Processing"),
    @XmlEnumValue("Terminating")
    TERMINATING("Terminating"),
    @XmlEnumValue("Terminated")
    TERMINATED("Terminated"),
    @XmlEnumValue("Stopped By System")
    STOPPED_BY_SYSTEM("Stopped By System"),
    @XmlEnumValue("Completed")
    COMPLETED("Completed"),
    @XmlEnumValue("Expired")
    EXPIRED("Expired");
    private final String value;

    ServicePackMigrationTaskStatus21(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ServicePackMigrationTaskStatus21 fromValue(String v) {
        for (ServicePackMigrationTaskStatus21 c: ServicePackMigrationTaskStatus21 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
