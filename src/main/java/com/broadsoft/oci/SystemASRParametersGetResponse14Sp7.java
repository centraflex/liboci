//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemASRParametersGetRequest14sp7.
 *         Contains a list of system Application Server Registration parameters.
 *         
 *         Replaced by: SystemASRParametersGetResponse23 in AS data mode.
 *       
 * 
 * <p>Java-Klasse für SystemASRParametersGetResponse14sp7 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemASRParametersGetResponse14sp7">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maxTransmissions" type="{}ASRMaxTransmissions"/>
 *         <element name="retransmissionDelayMilliSeconds" type="{}ASRRetransmissionDelayMilliSeconds"/>
 *         <element name="listeningPort" type="{}Port1025"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemASRParametersGetResponse14sp7", propOrder = {
    "maxTransmissions",
    "retransmissionDelayMilliSeconds",
    "listeningPort"
})
public class SystemASRParametersGetResponse14Sp7
    extends OCIDataResponse
{

    protected int maxTransmissions;
    protected int retransmissionDelayMilliSeconds;
    protected int listeningPort;

    /**
     * Ruft den Wert der maxTransmissions-Eigenschaft ab.
     * 
     */
    public int getMaxTransmissions() {
        return maxTransmissions;
    }

    /**
     * Legt den Wert der maxTransmissions-Eigenschaft fest.
     * 
     */
    public void setMaxTransmissions(int value) {
        this.maxTransmissions = value;
    }

    /**
     * Ruft den Wert der retransmissionDelayMilliSeconds-Eigenschaft ab.
     * 
     */
    public int getRetransmissionDelayMilliSeconds() {
        return retransmissionDelayMilliSeconds;
    }

    /**
     * Legt den Wert der retransmissionDelayMilliSeconds-Eigenschaft fest.
     * 
     */
    public void setRetransmissionDelayMilliSeconds(int value) {
        this.retransmissionDelayMilliSeconds = value;
    }

    /**
     * Ruft den Wert der listeningPort-Eigenschaft ab.
     * 
     */
    public int getListeningPort() {
        return listeningPort;
    }

    /**
     * Legt den Wert der listeningPort-Eigenschaft fest.
     * 
     */
    public void setListeningPort(int value) {
        this.listeningPort = value;
    }

}
