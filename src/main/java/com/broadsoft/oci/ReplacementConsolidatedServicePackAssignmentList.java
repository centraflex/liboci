//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         A list of service packs that replaces existing service packs assigned to the user.
 *         If a service pack is not authorized to the group, the service will be authorized. The authorizedQuantity will be used if provided; otherwise, the service quantity will be set to unlimited. The command will fail if the authorized Quantity set at the service provider is insufficient
 *         If a service pack is already authorized to the group, the service quantity will be ignored if included.
 *       
 * 
 * <p>Java-Klasse für ReplacementConsolidatedServicePackAssignmentList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ReplacementConsolidatedServicePackAssignmentList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="servicePack" type="{}ConsolidatedServicePackAssignment" maxOccurs="unbounded"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReplacementConsolidatedServicePackAssignmentList", propOrder = {
    "servicePack"
})
public class ReplacementConsolidatedServicePackAssignmentList {

    @XmlElement(required = true)
    protected List<ConsolidatedServicePackAssignment> servicePack;

    /**
     * Gets the value of the servicePack property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the servicePack property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicePack().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConsolidatedServicePackAssignment }
     * 
     * 
     * @return
     *     The value of the servicePack property.
     */
    public List<ConsolidatedServicePackAssignment> getServicePack() {
        if (servicePack == null) {
            servicePack = new ArrayList<>();
        }
        return this.servicePack;
    }

}
