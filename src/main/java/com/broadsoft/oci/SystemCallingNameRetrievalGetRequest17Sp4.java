//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request the system's calling name retrieval attributes.
 *         The response is either a SystemCallingNameRetrievalGetResponse17sp4 or an ErrorResponse.
 *         
 *         Replaced by: SystemCallingNameRetrievalGetRequest24
 *       
 * 
 * <p>Java-Klasse für SystemCallingNameRetrievalGetRequest17sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallingNameRetrievalGetRequest17sp4">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallingNameRetrievalGetRequest17sp4")
public class SystemCallingNameRetrievalGetRequest17Sp4
    extends OCIRequest
{


}
