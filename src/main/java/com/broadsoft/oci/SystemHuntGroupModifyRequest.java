//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with Hunt Group.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         Replaced by: SystemHuntGroupModifyRequest21 in AS data mode
 *       
 * 
 * <p>Java-Klasse für SystemHuntGroupModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemHuntGroupModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="anonymousInsteadOfPrivateCLID" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="removeHuntGroupNameFromCLID" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="uniformCallDistributionPolicyScope" type="{}HuntGroupUniformCallDistributionPolicyScope" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemHuntGroupModifyRequest", propOrder = {
    "anonymousInsteadOfPrivateCLID",
    "removeHuntGroupNameFromCLID",
    "uniformCallDistributionPolicyScope"
})
public class SystemHuntGroupModifyRequest
    extends OCIRequest
{

    protected Boolean anonymousInsteadOfPrivateCLID;
    protected Boolean removeHuntGroupNameFromCLID;
    @XmlSchemaType(name = "token")
    protected HuntGroupUniformCallDistributionPolicyScope uniformCallDistributionPolicyScope;

    /**
     * Ruft den Wert der anonymousInsteadOfPrivateCLID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAnonymousInsteadOfPrivateCLID() {
        return anonymousInsteadOfPrivateCLID;
    }

    /**
     * Legt den Wert der anonymousInsteadOfPrivateCLID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAnonymousInsteadOfPrivateCLID(Boolean value) {
        this.anonymousInsteadOfPrivateCLID = value;
    }

    /**
     * Ruft den Wert der removeHuntGroupNameFromCLID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRemoveHuntGroupNameFromCLID() {
        return removeHuntGroupNameFromCLID;
    }

    /**
     * Legt den Wert der removeHuntGroupNameFromCLID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRemoveHuntGroupNameFromCLID(Boolean value) {
        this.removeHuntGroupNameFromCLID = value;
    }

    /**
     * Ruft den Wert der uniformCallDistributionPolicyScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HuntGroupUniformCallDistributionPolicyScope }
     *     
     */
    public HuntGroupUniformCallDistributionPolicyScope getUniformCallDistributionPolicyScope() {
        return uniformCallDistributionPolicyScope;
    }

    /**
     * Legt den Wert der uniformCallDistributionPolicyScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HuntGroupUniformCallDistributionPolicyScope }
     *     
     */
    public void setUniformCallDistributionPolicyScope(HuntGroupUniformCallDistributionPolicyScope value) {
        this.uniformCallDistributionPolicyScope = value;
    }

}
