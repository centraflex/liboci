//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für StirShakenTaggingPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="StirShakenTaggingPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="All Eligible Calls"/>
 *     <enumeration value="Eligible Intra-Network Calls"/>
 *     <enumeration value="Off"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "StirShakenTaggingPolicy")
@XmlEnum
public enum StirShakenTaggingPolicy {

    @XmlEnumValue("All Eligible Calls")
    ALL_ELIGIBLE_CALLS("All Eligible Calls"),
    @XmlEnumValue("Eligible Intra-Network Calls")
    ELIGIBLE_INTRA_NETWORK_CALLS("Eligible Intra-Network Calls"),
    @XmlEnumValue("Off")
    OFF("Off");
    private final String value;

    StirShakenTaggingPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StirShakenTaggingPolicy fromValue(String v) {
        for (StirShakenTaggingPolicy c: StirShakenTaggingPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
