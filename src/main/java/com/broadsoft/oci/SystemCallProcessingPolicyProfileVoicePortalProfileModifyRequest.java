//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a call processing policy profile voice portal type profile in system.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *            useMaxCallsPerSecond
 *            maxCallsPerSecond
 *       
 * 
 * <p>Java-Klasse für SystemCallProcessingPolicyProfileVoicePortalProfileModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallProcessingPolicyProfileVoicePortalProfileModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="callProcessingPolicyProfileName" type="{}CallProcessingPolicyProfileName"/>
 *         <element name="useCLIDPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="clidPolicy" type="{}GroupCLIDPolicy" minOccurs="0"/>
 *         <element name="useGroupName" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="blockCallingNameForExternalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowConfigurableCLIDForRedirectingIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowDepartmentCLIDNameOverride" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enterpriseCallsCLIDPolicy" type="{}EnterpriseInternalCallsCLIDPolicy" minOccurs="0"/>
 *         <element name="enterpriseGroupCallsCLIDPolicy" type="{}EnterpriseInternalCallsCLIDPolicy" minOccurs="0"/>
 *         <element name="serviceProviderGroupCallsCLIDPolicy" type="{}ServiceProviderInternalCallsCLIDPolicy" minOccurs="0"/>
 *         <element name="useCallLimitsPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useMaxSimultaneousCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxSimultaneousCalls" type="{}CallProcessingMaxSimultaneousCalls19sp1" minOccurs="0"/>
 *         <element name="useMaxSimultaneousVideoCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxSimultaneousVideoCalls" type="{}CallProcessingMaxSimultaneousCalls19sp1" minOccurs="0"/>
 *         <element name="useMaxCallTimeForAnsweredCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxCallTimeForAnsweredCallsMinutes" type="{}CallProcessingMaxCallTimeForAnsweredCallsMinutes16" minOccurs="0"/>
 *         <element name="maxRedirectionDepth" type="{}CallProcessingMaxRedirectionDepth19sp1" minOccurs="0"/>
 *         <element name="useTranslationRoutingPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="networkUsageSelection" type="{}NetworkUsageSelection" minOccurs="0"/>
 *         <element name="enableEnterpriseExtensionDialing" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowEnterpriseGroupCallTypingForPrivateDialingPlan" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowEnterpriseGroupCallTypingForPublicDialingPlan" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeRedirectionsInMaximumNumberOfConcurrentCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useMaxCallsPerSecond" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxCallsPerSecond" type="{}MaxCallsPerSecondLimit" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallProcessingPolicyProfileVoicePortalProfileModifyRequest", propOrder = {
    "callProcessingPolicyProfileName",
    "useCLIDPolicy",
    "clidPolicy",
    "useGroupName",
    "blockCallingNameForExternalCalls",
    "allowConfigurableCLIDForRedirectingIdentity",
    "allowDepartmentCLIDNameOverride",
    "enterpriseCallsCLIDPolicy",
    "enterpriseGroupCallsCLIDPolicy",
    "serviceProviderGroupCallsCLIDPolicy",
    "useCallLimitsPolicy",
    "useMaxSimultaneousCalls",
    "maxSimultaneousCalls",
    "useMaxSimultaneousVideoCalls",
    "maxSimultaneousVideoCalls",
    "useMaxCallTimeForAnsweredCalls",
    "maxCallTimeForAnsweredCallsMinutes",
    "maxRedirectionDepth",
    "useTranslationRoutingPolicy",
    "networkUsageSelection",
    "enableEnterpriseExtensionDialing",
    "allowEnterpriseGroupCallTypingForPrivateDialingPlan",
    "allowEnterpriseGroupCallTypingForPublicDialingPlan",
    "includeRedirectionsInMaximumNumberOfConcurrentCalls",
    "useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable",
    "useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable",
    "useMaxCallsPerSecond",
    "maxCallsPerSecond"
})
public class SystemCallProcessingPolicyProfileVoicePortalProfileModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callProcessingPolicyProfileName;
    protected Boolean useCLIDPolicy;
    @XmlSchemaType(name = "token")
    protected GroupCLIDPolicy clidPolicy;
    protected Boolean useGroupName;
    protected Boolean blockCallingNameForExternalCalls;
    protected Boolean allowConfigurableCLIDForRedirectingIdentity;
    protected Boolean allowDepartmentCLIDNameOverride;
    @XmlSchemaType(name = "token")
    protected EnterpriseInternalCallsCLIDPolicy enterpriseCallsCLIDPolicy;
    @XmlSchemaType(name = "token")
    protected EnterpriseInternalCallsCLIDPolicy enterpriseGroupCallsCLIDPolicy;
    @XmlSchemaType(name = "token")
    protected ServiceProviderInternalCallsCLIDPolicy serviceProviderGroupCallsCLIDPolicy;
    protected Boolean useCallLimitsPolicy;
    protected Boolean useMaxSimultaneousCalls;
    protected Integer maxSimultaneousCalls;
    protected Boolean useMaxSimultaneousVideoCalls;
    protected Integer maxSimultaneousVideoCalls;
    protected Boolean useMaxCallTimeForAnsweredCalls;
    protected Integer maxCallTimeForAnsweredCallsMinutes;
    protected Integer maxRedirectionDepth;
    protected Boolean useTranslationRoutingPolicy;
    @XmlSchemaType(name = "token")
    protected NetworkUsageSelection networkUsageSelection;
    protected Boolean enableEnterpriseExtensionDialing;
    protected Boolean allowEnterpriseGroupCallTypingForPrivateDialingPlan;
    protected Boolean allowEnterpriseGroupCallTypingForPublicDialingPlan;
    protected Boolean includeRedirectionsInMaximumNumberOfConcurrentCalls;
    protected Boolean useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable;
    protected Boolean useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable;
    protected Boolean useMaxCallsPerSecond;
    protected Integer maxCallsPerSecond;

    /**
     * Ruft den Wert der callProcessingPolicyProfileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallProcessingPolicyProfileName() {
        return callProcessingPolicyProfileName;
    }

    /**
     * Legt den Wert der callProcessingPolicyProfileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallProcessingPolicyProfileName(String value) {
        this.callProcessingPolicyProfileName = value;
    }

    /**
     * Ruft den Wert der useCLIDPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseCLIDPolicy() {
        return useCLIDPolicy;
    }

    /**
     * Legt den Wert der useCLIDPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseCLIDPolicy(Boolean value) {
        this.useCLIDPolicy = value;
    }

    /**
     * Ruft den Wert der clidPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public GroupCLIDPolicy getClidPolicy() {
        return clidPolicy;
    }

    /**
     * Legt den Wert der clidPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public void setClidPolicy(GroupCLIDPolicy value) {
        this.clidPolicy = value;
    }

    /**
     * Ruft den Wert der useGroupName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseGroupName() {
        return useGroupName;
    }

    /**
     * Legt den Wert der useGroupName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseGroupName(Boolean value) {
        this.useGroupName = value;
    }

    /**
     * Ruft den Wert der blockCallingNameForExternalCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBlockCallingNameForExternalCalls() {
        return blockCallingNameForExternalCalls;
    }

    /**
     * Legt den Wert der blockCallingNameForExternalCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBlockCallingNameForExternalCalls(Boolean value) {
        this.blockCallingNameForExternalCalls = value;
    }

    /**
     * Ruft den Wert der allowConfigurableCLIDForRedirectingIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowConfigurableCLIDForRedirectingIdentity() {
        return allowConfigurableCLIDForRedirectingIdentity;
    }

    /**
     * Legt den Wert der allowConfigurableCLIDForRedirectingIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowConfigurableCLIDForRedirectingIdentity(Boolean value) {
        this.allowConfigurableCLIDForRedirectingIdentity = value;
    }

    /**
     * Ruft den Wert der allowDepartmentCLIDNameOverride-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowDepartmentCLIDNameOverride() {
        return allowDepartmentCLIDNameOverride;
    }

    /**
     * Legt den Wert der allowDepartmentCLIDNameOverride-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowDepartmentCLIDNameOverride(Boolean value) {
        this.allowDepartmentCLIDNameOverride = value;
    }

    /**
     * Ruft den Wert der enterpriseCallsCLIDPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public EnterpriseInternalCallsCLIDPolicy getEnterpriseCallsCLIDPolicy() {
        return enterpriseCallsCLIDPolicy;
    }

    /**
     * Legt den Wert der enterpriseCallsCLIDPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public void setEnterpriseCallsCLIDPolicy(EnterpriseInternalCallsCLIDPolicy value) {
        this.enterpriseCallsCLIDPolicy = value;
    }

    /**
     * Ruft den Wert der enterpriseGroupCallsCLIDPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public EnterpriseInternalCallsCLIDPolicy getEnterpriseGroupCallsCLIDPolicy() {
        return enterpriseGroupCallsCLIDPolicy;
    }

    /**
     * Legt den Wert der enterpriseGroupCallsCLIDPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public void setEnterpriseGroupCallsCLIDPolicy(EnterpriseInternalCallsCLIDPolicy value) {
        this.enterpriseGroupCallsCLIDPolicy = value;
    }

    /**
     * Ruft den Wert der serviceProviderGroupCallsCLIDPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderInternalCallsCLIDPolicy }
     *     
     */
    public ServiceProviderInternalCallsCLIDPolicy getServiceProviderGroupCallsCLIDPolicy() {
        return serviceProviderGroupCallsCLIDPolicy;
    }

    /**
     * Legt den Wert der serviceProviderGroupCallsCLIDPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderInternalCallsCLIDPolicy }
     *     
     */
    public void setServiceProviderGroupCallsCLIDPolicy(ServiceProviderInternalCallsCLIDPolicy value) {
        this.serviceProviderGroupCallsCLIDPolicy = value;
    }

    /**
     * Ruft den Wert der useCallLimitsPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseCallLimitsPolicy() {
        return useCallLimitsPolicy;
    }

    /**
     * Legt den Wert der useCallLimitsPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseCallLimitsPolicy(Boolean value) {
        this.useCallLimitsPolicy = value;
    }

    /**
     * Ruft den Wert der useMaxSimultaneousCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxSimultaneousCalls() {
        return useMaxSimultaneousCalls;
    }

    /**
     * Legt den Wert der useMaxSimultaneousCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxSimultaneousCalls(Boolean value) {
        this.useMaxSimultaneousCalls = value;
    }

    /**
     * Ruft den Wert der maxSimultaneousCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxSimultaneousCalls() {
        return maxSimultaneousCalls;
    }

    /**
     * Legt den Wert der maxSimultaneousCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxSimultaneousCalls(Integer value) {
        this.maxSimultaneousCalls = value;
    }

    /**
     * Ruft den Wert der useMaxSimultaneousVideoCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxSimultaneousVideoCalls() {
        return useMaxSimultaneousVideoCalls;
    }

    /**
     * Legt den Wert der useMaxSimultaneousVideoCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxSimultaneousVideoCalls(Boolean value) {
        this.useMaxSimultaneousVideoCalls = value;
    }

    /**
     * Ruft den Wert der maxSimultaneousVideoCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxSimultaneousVideoCalls() {
        return maxSimultaneousVideoCalls;
    }

    /**
     * Legt den Wert der maxSimultaneousVideoCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxSimultaneousVideoCalls(Integer value) {
        this.maxSimultaneousVideoCalls = value;
    }

    /**
     * Ruft den Wert der useMaxCallTimeForAnsweredCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxCallTimeForAnsweredCalls() {
        return useMaxCallTimeForAnsweredCalls;
    }

    /**
     * Legt den Wert der useMaxCallTimeForAnsweredCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxCallTimeForAnsweredCalls(Boolean value) {
        this.useMaxCallTimeForAnsweredCalls = value;
    }

    /**
     * Ruft den Wert der maxCallTimeForAnsweredCallsMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCallTimeForAnsweredCallsMinutes() {
        return maxCallTimeForAnsweredCallsMinutes;
    }

    /**
     * Legt den Wert der maxCallTimeForAnsweredCallsMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCallTimeForAnsweredCallsMinutes(Integer value) {
        this.maxCallTimeForAnsweredCallsMinutes = value;
    }

    /**
     * Ruft den Wert der maxRedirectionDepth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxRedirectionDepth() {
        return maxRedirectionDepth;
    }

    /**
     * Legt den Wert der maxRedirectionDepth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxRedirectionDepth(Integer value) {
        this.maxRedirectionDepth = value;
    }

    /**
     * Ruft den Wert der useTranslationRoutingPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseTranslationRoutingPolicy() {
        return useTranslationRoutingPolicy;
    }

    /**
     * Legt den Wert der useTranslationRoutingPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseTranslationRoutingPolicy(Boolean value) {
        this.useTranslationRoutingPolicy = value;
    }

    /**
     * Ruft den Wert der networkUsageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkUsageSelection }
     *     
     */
    public NetworkUsageSelection getNetworkUsageSelection() {
        return networkUsageSelection;
    }

    /**
     * Legt den Wert der networkUsageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkUsageSelection }
     *     
     */
    public void setNetworkUsageSelection(NetworkUsageSelection value) {
        this.networkUsageSelection = value;
    }

    /**
     * Ruft den Wert der enableEnterpriseExtensionDialing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableEnterpriseExtensionDialing() {
        return enableEnterpriseExtensionDialing;
    }

    /**
     * Legt den Wert der enableEnterpriseExtensionDialing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableEnterpriseExtensionDialing(Boolean value) {
        this.enableEnterpriseExtensionDialing = value;
    }

    /**
     * Ruft den Wert der allowEnterpriseGroupCallTypingForPrivateDialingPlan-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowEnterpriseGroupCallTypingForPrivateDialingPlan() {
        return allowEnterpriseGroupCallTypingForPrivateDialingPlan;
    }

    /**
     * Legt den Wert der allowEnterpriseGroupCallTypingForPrivateDialingPlan-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowEnterpriseGroupCallTypingForPrivateDialingPlan(Boolean value) {
        this.allowEnterpriseGroupCallTypingForPrivateDialingPlan = value;
    }

    /**
     * Ruft den Wert der allowEnterpriseGroupCallTypingForPublicDialingPlan-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowEnterpriseGroupCallTypingForPublicDialingPlan() {
        return allowEnterpriseGroupCallTypingForPublicDialingPlan;
    }

    /**
     * Legt den Wert der allowEnterpriseGroupCallTypingForPublicDialingPlan-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowEnterpriseGroupCallTypingForPublicDialingPlan(Boolean value) {
        this.allowEnterpriseGroupCallTypingForPublicDialingPlan = value;
    }

    /**
     * Ruft den Wert der includeRedirectionsInMaximumNumberOfConcurrentCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeRedirectionsInMaximumNumberOfConcurrentCalls() {
        return includeRedirectionsInMaximumNumberOfConcurrentCalls;
    }

    /**
     * Legt den Wert der includeRedirectionsInMaximumNumberOfConcurrentCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeRedirectionsInMaximumNumberOfConcurrentCalls(Boolean value) {
        this.includeRedirectionsInMaximumNumberOfConcurrentCalls = value;
    }

    /**
     * Ruft den Wert der useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable() {
        return useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable;
    }

    /**
     * Legt den Wert der useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable(Boolean value) {
        this.useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable = value;
    }

    /**
     * Ruft den Wert der useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable() {
        return useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable;
    }

    /**
     * Legt den Wert der useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable(Boolean value) {
        this.useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable = value;
    }

    /**
     * Ruft den Wert der useMaxCallsPerSecond-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxCallsPerSecond() {
        return useMaxCallsPerSecond;
    }

    /**
     * Legt den Wert der useMaxCallsPerSecond-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxCallsPerSecond(Boolean value) {
        this.useMaxCallsPerSecond = value;
    }

    /**
     * Ruft den Wert der maxCallsPerSecond-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCallsPerSecond() {
        return maxCallsPerSecond;
    }

    /**
     * Legt den Wert der maxCallsPerSecond-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCallsPerSecond(Integer value) {
        this.maxCallsPerSecond = value;
    }

}
