//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EnterpriseVoiceVPNDigitManipulationOperationOptionalValue.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="EnterpriseVoiceVPNDigitManipulationOperationOptionalValue">
 *   <restriction base="{}EnterpriseVoiceVPNDigitManipulationOperation">
 *     <enumeration value="Prepend"/>
 *     <enumeration value="Overwrite"/>
 *     <enumeration value="Right Trim"/>
 *     <enumeration value="Replace All"/>
 *     <enumeration value="Left Trim"/>
 *     <enumeration value="Append"/>
 *     <enumeration value="Insert"/>
 *     <enumeration value="Trim"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "EnterpriseVoiceVPNDigitManipulationOperationOptionalValue")
@XmlEnum(EnterpriseVoiceVPNDigitManipulationOperation.class)
public enum EnterpriseVoiceVPNDigitManipulationOperationOptionalValue {

    @XmlEnumValue("Prepend")
    PREPEND(EnterpriseVoiceVPNDigitManipulationOperation.PREPEND),
    @XmlEnumValue("Overwrite")
    OVERWRITE(EnterpriseVoiceVPNDigitManipulationOperation.OVERWRITE),
    @XmlEnumValue("Right Trim")
    RIGHT_TRIM(EnterpriseVoiceVPNDigitManipulationOperation.RIGHT_TRIM),
    @XmlEnumValue("Replace All")
    REPLACE_ALL(EnterpriseVoiceVPNDigitManipulationOperation.REPLACE_ALL),
    @XmlEnumValue("Left Trim")
    LEFT_TRIM(EnterpriseVoiceVPNDigitManipulationOperation.LEFT_TRIM),
    @XmlEnumValue("Append")
    APPEND(EnterpriseVoiceVPNDigitManipulationOperation.APPEND),
    @XmlEnumValue("Insert")
    INSERT(EnterpriseVoiceVPNDigitManipulationOperation.INSERT),
    @XmlEnumValue("Trim")
    TRIM(EnterpriseVoiceVPNDigitManipulationOperation.TRIM);
    private final EnterpriseVoiceVPNDigitManipulationOperation value;

    EnterpriseVoiceVPNDigitManipulationOperationOptionalValue(EnterpriseVoiceVPNDigitManipulationOperation v) {
        value = v;
    }

    public EnterpriseVoiceVPNDigitManipulationOperation value() {
        return value;
    }

    public static EnterpriseVoiceVPNDigitManipulationOperationOptionalValue fromValue(EnterpriseVoiceVPNDigitManipulationOperation v) {
        for (EnterpriseVoiceVPNDigitManipulationOperationOptionalValue c: EnterpriseVoiceVPNDigitManipulationOperationOptionalValue.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
