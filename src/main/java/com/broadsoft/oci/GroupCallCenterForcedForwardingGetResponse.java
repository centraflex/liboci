//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupCallCenterForcedForwardingGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterForcedForwardingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterForcedForwardingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forwardToPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="allowEnableViaFAC" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="playAnnouncementBeforeForwarding" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="audioMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="audioFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="audioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="audioMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="videoMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="videoFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="videoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="videoMediaType" type="{}MediaFileType" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterForcedForwardingGetResponse", propOrder = {
    "isActive",
    "forwardToPhoneNumber",
    "allowEnableViaFAC",
    "playAnnouncementBeforeForwarding",
    "audioMessageSelection",
    "audioFileUrl",
    "audioFileDescription",
    "audioMediaType",
    "videoMessageSelection",
    "videoFileUrl",
    "videoFileDescription",
    "videoMediaType"
})
public class GroupCallCenterForcedForwardingGetResponse
    extends OCIDataResponse
{

    protected boolean isActive;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String forwardToPhoneNumber;
    protected boolean allowEnableViaFAC;
    protected boolean playAnnouncementBeforeForwarding;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection audioMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioFileUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioMediaType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection videoMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String videoFileUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String videoFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String videoMediaType;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der forwardToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForwardToPhoneNumber() {
        return forwardToPhoneNumber;
    }

    /**
     * Legt den Wert der forwardToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForwardToPhoneNumber(String value) {
        this.forwardToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der allowEnableViaFAC-Eigenschaft ab.
     * 
     */
    public boolean isAllowEnableViaFAC() {
        return allowEnableViaFAC;
    }

    /**
     * Legt den Wert der allowEnableViaFAC-Eigenschaft fest.
     * 
     */
    public void setAllowEnableViaFAC(boolean value) {
        this.allowEnableViaFAC = value;
    }

    /**
     * Ruft den Wert der playAnnouncementBeforeForwarding-Eigenschaft ab.
     * 
     */
    public boolean isPlayAnnouncementBeforeForwarding() {
        return playAnnouncementBeforeForwarding;
    }

    /**
     * Legt den Wert der playAnnouncementBeforeForwarding-Eigenschaft fest.
     * 
     */
    public void setPlayAnnouncementBeforeForwarding(boolean value) {
        this.playAnnouncementBeforeForwarding = value;
    }

    /**
     * Ruft den Wert der audioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getAudioMessageSelection() {
        return audioMessageSelection;
    }

    /**
     * Legt den Wert der audioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.audioMessageSelection = value;
    }

    /**
     * Ruft den Wert der audioFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioFileUrl() {
        return audioFileUrl;
    }

    /**
     * Legt den Wert der audioFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioFileUrl(String value) {
        this.audioFileUrl = value;
    }

    /**
     * Ruft den Wert der audioFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioFileDescription() {
        return audioFileDescription;
    }

    /**
     * Legt den Wert der audioFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioFileDescription(String value) {
        this.audioFileDescription = value;
    }

    /**
     * Ruft den Wert der audioMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioMediaType() {
        return audioMediaType;
    }

    /**
     * Legt den Wert der audioMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioMediaType(String value) {
        this.audioMediaType = value;
    }

    /**
     * Ruft den Wert der videoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getVideoMessageSelection() {
        return videoMessageSelection;
    }

    /**
     * Legt den Wert der videoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.videoMessageSelection = value;
    }

    /**
     * Ruft den Wert der videoFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVideoFileUrl() {
        return videoFileUrl;
    }

    /**
     * Legt den Wert der videoFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVideoFileUrl(String value) {
        this.videoFileUrl = value;
    }

    /**
     * Ruft den Wert der videoFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVideoFileDescription() {
        return videoFileDescription;
    }

    /**
     * Legt den Wert der videoFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVideoFileDescription(String value) {
        this.videoFileDescription = value;
    }

    /**
     * Ruft den Wert der videoMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVideoMediaType() {
        return videoMediaType;
    }

    /**
     * Legt den Wert der videoMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVideoMediaType(String value) {
        this.videoMediaType = value;
    }

}
