//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         CommPilot Express Available In Office Settings.
 *       
 * 
 * <p>Java-Klasse für CommPilotExpressAvailableInOffice complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CommPilotExpressAvailableInOffice">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="additionalPhoneNumberToRing" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="busySetting" type="{}CommPilotExpressRedirection"/>
 *         <element name="noAnswerSetting" type="{}CommPilotExpressRedirection"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommPilotExpressAvailableInOffice", propOrder = {
    "additionalPhoneNumberToRing",
    "busySetting",
    "noAnswerSetting"
})
public class CommPilotExpressAvailableInOffice {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String additionalPhoneNumberToRing;
    @XmlElement(required = true)
    protected CommPilotExpressRedirection busySetting;
    @XmlElement(required = true)
    protected CommPilotExpressRedirection noAnswerSetting;

    /**
     * Ruft den Wert der additionalPhoneNumberToRing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalPhoneNumberToRing() {
        return additionalPhoneNumberToRing;
    }

    /**
     * Legt den Wert der additionalPhoneNumberToRing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalPhoneNumberToRing(String value) {
        this.additionalPhoneNumberToRing = value;
    }

    /**
     * Ruft den Wert der busySetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressRedirection }
     *     
     */
    public CommPilotExpressRedirection getBusySetting() {
        return busySetting;
    }

    /**
     * Legt den Wert der busySetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressRedirection }
     *     
     */
    public void setBusySetting(CommPilotExpressRedirection value) {
        this.busySetting = value;
    }

    /**
     * Ruft den Wert der noAnswerSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressRedirection }
     *     
     */
    public CommPilotExpressRedirection getNoAnswerSetting() {
        return noAnswerSetting;
    }

    /**
     * Legt den Wert der noAnswerSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressRedirection }
     *     
     */
    public void setNoAnswerSetting(CommPilotExpressRedirection value) {
        this.noAnswerSetting = value;
    }

}
