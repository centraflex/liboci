//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemNetworkClassOfServiceGetListRequest.
 *         The response contains a table of all Network Classes of Service
 *         in the system. The column headings are "Name" and "Description"
 *       
 * 
 * <p>Java-Klasse für SystemNetworkClassOfServiceGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNetworkClassOfServiceGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="networkClassOfServiceTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNetworkClassOfServiceGetListResponse", propOrder = {
    "networkClassOfServiceTable"
})
public class SystemNetworkClassOfServiceGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable networkClassOfServiceTable;

    /**
     * Ruft den Wert der networkClassOfServiceTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getNetworkClassOfServiceTable() {
        return networkClassOfServiceTable;
    }

    /**
     * Legt den Wert der networkClassOfServiceTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setNetworkClassOfServiceTable(OCITable value) {
        this.networkClassOfServiceTable = value;
    }

}
