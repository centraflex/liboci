//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with collaborate services.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         Replaced by SystemCollaborateModifyRequest20sp1V2
 *       
 * 
 * <p>Java-Klasse für SystemCollaborateModifyRequest20sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCollaborateModifyRequest20sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="collaborateRoomIdLength" type="{}CollaboratePassCodeLength" minOccurs="0"/>
 *         <element name="instantRoomIdleTimeoutSeconds" type="{}CollaborateInstantRoomIdleTimeoutSeconds20sp1" minOccurs="0"/>
 *         <element name="collaborateRoomMaximumDurationMinutes" type="{}CollaborateRoomMaximumDurationMinutes" minOccurs="0"/>
 *         <element name="supportOutdial" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxCollaborateRoomParticipants" type="{}CollaborateRoomMaximumParticipants" minOccurs="0"/>
 *         <element name="collaborateActiveTalkerRefreshIntervalSeconds" type="{}CollaborateActiveTalkerRefreshIntervalSeconds" minOccurs="0"/>
 *         <element name="terminateCollaborateAfterGracePeriod" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="collaborateGracePeriod" type="{}CollaborateGracePeriodDuration" minOccurs="0"/>
 *         <element name="enableActiveCollaborateNotification" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="collaborateFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCollaborateModifyRequest20sp1", propOrder = {
    "collaborateRoomIdLength",
    "instantRoomIdleTimeoutSeconds",
    "collaborateRoomMaximumDurationMinutes",
    "supportOutdial",
    "maxCollaborateRoomParticipants",
    "collaborateActiveTalkerRefreshIntervalSeconds",
    "terminateCollaborateAfterGracePeriod",
    "collaborateGracePeriod",
    "enableActiveCollaborateNotification",
    "collaborateFromAddress"
})
public class SystemCollaborateModifyRequest20Sp1
    extends OCIRequest
{

    protected Integer collaborateRoomIdLength;
    protected Integer instantRoomIdleTimeoutSeconds;
    protected Integer collaborateRoomMaximumDurationMinutes;
    protected Boolean supportOutdial;
    protected Integer maxCollaborateRoomParticipants;
    protected Integer collaborateActiveTalkerRefreshIntervalSeconds;
    protected Boolean terminateCollaborateAfterGracePeriod;
    protected CollaborateGracePeriodDuration collaborateGracePeriod;
    protected Boolean enableActiveCollaborateNotification;
    @XmlElementRef(name = "collaborateFromAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> collaborateFromAddress;

    /**
     * Ruft den Wert der collaborateRoomIdLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCollaborateRoomIdLength() {
        return collaborateRoomIdLength;
    }

    /**
     * Legt den Wert der collaborateRoomIdLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCollaborateRoomIdLength(Integer value) {
        this.collaborateRoomIdLength = value;
    }

    /**
     * Ruft den Wert der instantRoomIdleTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInstantRoomIdleTimeoutSeconds() {
        return instantRoomIdleTimeoutSeconds;
    }

    /**
     * Legt den Wert der instantRoomIdleTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInstantRoomIdleTimeoutSeconds(Integer value) {
        this.instantRoomIdleTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der collaborateRoomMaximumDurationMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCollaborateRoomMaximumDurationMinutes() {
        return collaborateRoomMaximumDurationMinutes;
    }

    /**
     * Legt den Wert der collaborateRoomMaximumDurationMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCollaborateRoomMaximumDurationMinutes(Integer value) {
        this.collaborateRoomMaximumDurationMinutes = value;
    }

    /**
     * Ruft den Wert der supportOutdial-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportOutdial() {
        return supportOutdial;
    }

    /**
     * Legt den Wert der supportOutdial-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportOutdial(Boolean value) {
        this.supportOutdial = value;
    }

    /**
     * Ruft den Wert der maxCollaborateRoomParticipants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCollaborateRoomParticipants() {
        return maxCollaborateRoomParticipants;
    }

    /**
     * Legt den Wert der maxCollaborateRoomParticipants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCollaborateRoomParticipants(Integer value) {
        this.maxCollaborateRoomParticipants = value;
    }

    /**
     * Ruft den Wert der collaborateActiveTalkerRefreshIntervalSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCollaborateActiveTalkerRefreshIntervalSeconds() {
        return collaborateActiveTalkerRefreshIntervalSeconds;
    }

    /**
     * Legt den Wert der collaborateActiveTalkerRefreshIntervalSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCollaborateActiveTalkerRefreshIntervalSeconds(Integer value) {
        this.collaborateActiveTalkerRefreshIntervalSeconds = value;
    }

    /**
     * Ruft den Wert der terminateCollaborateAfterGracePeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTerminateCollaborateAfterGracePeriod() {
        return terminateCollaborateAfterGracePeriod;
    }

    /**
     * Legt den Wert der terminateCollaborateAfterGracePeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTerminateCollaborateAfterGracePeriod(Boolean value) {
        this.terminateCollaborateAfterGracePeriod = value;
    }

    /**
     * Ruft den Wert der collaborateGracePeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CollaborateGracePeriodDuration }
     *     
     */
    public CollaborateGracePeriodDuration getCollaborateGracePeriod() {
        return collaborateGracePeriod;
    }

    /**
     * Legt den Wert der collaborateGracePeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CollaborateGracePeriodDuration }
     *     
     */
    public void setCollaborateGracePeriod(CollaborateGracePeriodDuration value) {
        this.collaborateGracePeriod = value;
    }

    /**
     * Ruft den Wert der enableActiveCollaborateNotification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableActiveCollaborateNotification() {
        return enableActiveCollaborateNotification;
    }

    /**
     * Legt den Wert der enableActiveCollaborateNotification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableActiveCollaborateNotification(Boolean value) {
        this.enableActiveCollaborateNotification = value;
    }

    /**
     * Ruft den Wert der collaborateFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCollaborateFromAddress() {
        return collaborateFromAddress;
    }

    /**
     * Legt den Wert der collaborateFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCollaborateFromAddress(JAXBElement<String> value) {
        this.collaborateFromAddress = value;
    }

}
