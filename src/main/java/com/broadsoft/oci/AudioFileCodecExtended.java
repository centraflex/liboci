//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AudioFileCodecExtended.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AudioFileCodecExtended">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="None"/>
 *     <enumeration value="G711"/>
 *     <enumeration value="G722"/>
 *     <enumeration value="G729"/>
 *     <enumeration value="G726"/>
 *     <enumeration value="AMR"/>
 *     <enumeration value="AMR-WB"/>
 *     <enumeration value="EVRC0"/>
 *     <enumeration value="EVRCNW"/>
 *     <enumeration value="EVRCNW0"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AudioFileCodecExtended")
@XmlEnum
public enum AudioFileCodecExtended {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("G711")
    G_711("G711"),
    @XmlEnumValue("G722")
    G_722("G722"),
    @XmlEnumValue("G729")
    G_729("G729"),
    @XmlEnumValue("G726")
    G_726("G726"),
    AMR("AMR"),
    @XmlEnumValue("AMR-WB")
    AMR_WB("AMR-WB"),
    @XmlEnumValue("EVRC0")
    EVRC_0("EVRC0"),
    EVRCNW("EVRCNW"),
    @XmlEnumValue("EVRCNW0")
    EVRCNW_0("EVRCNW0");
    private final String value;

    AudioFileCodecExtended(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AudioFileCodecExtended fromValue(String v) {
        for (AudioFileCodecExtended c: AudioFileCodecExtended.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
