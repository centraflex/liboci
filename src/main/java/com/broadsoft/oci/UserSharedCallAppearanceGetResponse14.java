//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserSharedCallAppearanceGetRequest14.
 *         The endpointTable contains columns:
 *           "Device Level", "Device Name", "Device Type", "Line/Port", "SIP Contact".
 *         The "Device Level" column contains one of the AccessDeviceLevel enumerated constants.
 *         Replaced By: UserSharedCallAppearanceGetResponse14sp2
 *       
 * 
 * <p>Java-Klasse für UserSharedCallAppearanceGetResponse14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserSharedCallAppearanceGetResponse14">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="alertAllAppearancesForClickToDialCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxAppearances" type="{}SharedCallAppearanceMaximumAppearances"/>
 *         <element name="enableMultipleCallArrangement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="multipleCallArrangementIsActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="endpointTable" type="{C}OCITable"/>
 *         <element name="allowBridgingBetweenLocations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="bridgeWarningTone" type="{}SharedCallAppearanceBridgeWarningTone"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserSharedCallAppearanceGetResponse14", propOrder = {
    "alertAllAppearancesForClickToDialCalls",
    "maxAppearances",
    "enableMultipleCallArrangement",
    "multipleCallArrangementIsActive",
    "endpointTable",
    "allowBridgingBetweenLocations",
    "bridgeWarningTone"
})
public class UserSharedCallAppearanceGetResponse14
    extends OCIDataResponse
{

    protected boolean alertAllAppearancesForClickToDialCalls;
    protected int maxAppearances;
    protected boolean enableMultipleCallArrangement;
    protected boolean multipleCallArrangementIsActive;
    @XmlElement(required = true)
    protected OCITable endpointTable;
    protected boolean allowBridgingBetweenLocations;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SharedCallAppearanceBridgeWarningTone bridgeWarningTone;

    /**
     * Ruft den Wert der alertAllAppearancesForClickToDialCalls-Eigenschaft ab.
     * 
     */
    public boolean isAlertAllAppearancesForClickToDialCalls() {
        return alertAllAppearancesForClickToDialCalls;
    }

    /**
     * Legt den Wert der alertAllAppearancesForClickToDialCalls-Eigenschaft fest.
     * 
     */
    public void setAlertAllAppearancesForClickToDialCalls(boolean value) {
        this.alertAllAppearancesForClickToDialCalls = value;
    }

    /**
     * Ruft den Wert der maxAppearances-Eigenschaft ab.
     * 
     */
    public int getMaxAppearances() {
        return maxAppearances;
    }

    /**
     * Legt den Wert der maxAppearances-Eigenschaft fest.
     * 
     */
    public void setMaxAppearances(int value) {
        this.maxAppearances = value;
    }

    /**
     * Ruft den Wert der enableMultipleCallArrangement-Eigenschaft ab.
     * 
     */
    public boolean isEnableMultipleCallArrangement() {
        return enableMultipleCallArrangement;
    }

    /**
     * Legt den Wert der enableMultipleCallArrangement-Eigenschaft fest.
     * 
     */
    public void setEnableMultipleCallArrangement(boolean value) {
        this.enableMultipleCallArrangement = value;
    }

    /**
     * Ruft den Wert der multipleCallArrangementIsActive-Eigenschaft ab.
     * 
     */
    public boolean isMultipleCallArrangementIsActive() {
        return multipleCallArrangementIsActive;
    }

    /**
     * Legt den Wert der multipleCallArrangementIsActive-Eigenschaft fest.
     * 
     */
    public void setMultipleCallArrangementIsActive(boolean value) {
        this.multipleCallArrangementIsActive = value;
    }

    /**
     * Ruft den Wert der endpointTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getEndpointTable() {
        return endpointTable;
    }

    /**
     * Legt den Wert der endpointTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setEndpointTable(OCITable value) {
        this.endpointTable = value;
    }

    /**
     * Ruft den Wert der allowBridgingBetweenLocations-Eigenschaft ab.
     * 
     */
    public boolean isAllowBridgingBetweenLocations() {
        return allowBridgingBetweenLocations;
    }

    /**
     * Legt den Wert der allowBridgingBetweenLocations-Eigenschaft fest.
     * 
     */
    public void setAllowBridgingBetweenLocations(boolean value) {
        this.allowBridgingBetweenLocations = value;
    }

    /**
     * Ruft den Wert der bridgeWarningTone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SharedCallAppearanceBridgeWarningTone }
     *     
     */
    public SharedCallAppearanceBridgeWarningTone getBridgeWarningTone() {
        return bridgeWarningTone;
    }

    /**
     * Legt den Wert der bridgeWarningTone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SharedCallAppearanceBridgeWarningTone }
     *     
     */
    public void setBridgeWarningTone(SharedCallAppearanceBridgeWarningTone value) {
        this.bridgeWarningTone = value;
    }

}
