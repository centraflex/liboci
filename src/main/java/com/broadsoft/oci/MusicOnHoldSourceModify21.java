//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains the music on hold source configuration.
 *         The following elements are only used in HSS data mode and ignored in AS data mode:
 *           labeledMediaFiles
 *         The following elements are only used in AS data mode and ignored in HSS data mode:
 *           announcementMediaFiles
 *       
 * 
 * <p>Java-Klasse für MusicOnHoldSourceModify21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="MusicOnHoldSourceModify21">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="audioFilePreferredCodec" type="{}AudioFileCodecExtended" minOccurs="0"/>
 *         <element name="messageSourceSelection" type="{}MusicOnHoldMessageSelection" minOccurs="0"/>
 *         <choice minOccurs="0">
 *           <element name="labeledCustomSourceMediaFiles">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="audioFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *                     <element name="videoFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *           <element name="announcementCustomSourceMediaFiles">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="audioFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *                     <element name="videoFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *         </choice>
 *         <element name="externalSource" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointModify"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MusicOnHoldSourceModify21", propOrder = {
    "audioFilePreferredCodec",
    "messageSourceSelection",
    "labeledCustomSourceMediaFiles",
    "announcementCustomSourceMediaFiles",
    "externalSource"
})
public class MusicOnHoldSourceModify21 {

    @XmlSchemaType(name = "token")
    protected AudioFileCodecExtended audioFilePreferredCodec;
    @XmlSchemaType(name = "token")
    protected MusicOnHoldMessageSelection messageSourceSelection;
    protected MusicOnHoldSourceModify21 .LabeledCustomSourceMediaFiles labeledCustomSourceMediaFiles;
    protected MusicOnHoldSourceModify21 .AnnouncementCustomSourceMediaFiles announcementCustomSourceMediaFiles;
    protected MusicOnHoldSourceModify21 .ExternalSource externalSource;

    /**
     * Ruft den Wert der audioFilePreferredCodec-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AudioFileCodecExtended }
     *     
     */
    public AudioFileCodecExtended getAudioFilePreferredCodec() {
        return audioFilePreferredCodec;
    }

    /**
     * Legt den Wert der audioFilePreferredCodec-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AudioFileCodecExtended }
     *     
     */
    public void setAudioFilePreferredCodec(AudioFileCodecExtended value) {
        this.audioFilePreferredCodec = value;
    }

    /**
     * Ruft den Wert der messageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public MusicOnHoldMessageSelection getMessageSourceSelection() {
        return messageSourceSelection;
    }

    /**
     * Legt den Wert der messageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public void setMessageSourceSelection(MusicOnHoldMessageSelection value) {
        this.messageSourceSelection = value;
    }

    /**
     * Ruft den Wert der labeledCustomSourceMediaFiles-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceModify21 .LabeledCustomSourceMediaFiles }
     *     
     */
    public MusicOnHoldSourceModify21 .LabeledCustomSourceMediaFiles getLabeledCustomSourceMediaFiles() {
        return labeledCustomSourceMediaFiles;
    }

    /**
     * Legt den Wert der labeledCustomSourceMediaFiles-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceModify21 .LabeledCustomSourceMediaFiles }
     *     
     */
    public void setLabeledCustomSourceMediaFiles(MusicOnHoldSourceModify21 .LabeledCustomSourceMediaFiles value) {
        this.labeledCustomSourceMediaFiles = value;
    }

    /**
     * Ruft den Wert der announcementCustomSourceMediaFiles-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceModify21 .AnnouncementCustomSourceMediaFiles }
     *     
     */
    public MusicOnHoldSourceModify21 .AnnouncementCustomSourceMediaFiles getAnnouncementCustomSourceMediaFiles() {
        return announcementCustomSourceMediaFiles;
    }

    /**
     * Legt den Wert der announcementCustomSourceMediaFiles-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceModify21 .AnnouncementCustomSourceMediaFiles }
     *     
     */
    public void setAnnouncementCustomSourceMediaFiles(MusicOnHoldSourceModify21 .AnnouncementCustomSourceMediaFiles value) {
        this.announcementCustomSourceMediaFiles = value;
    }

    /**
     * Ruft den Wert der externalSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceModify21 .ExternalSource }
     *     
     */
    public MusicOnHoldSourceModify21 .ExternalSource getExternalSource() {
        return externalSource;
    }

    /**
     * Legt den Wert der externalSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceModify21 .ExternalSource }
     *     
     */
    public void setExternalSource(MusicOnHoldSourceModify21 .ExternalSource value) {
        this.externalSource = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="audioFile" type="{}AnnouncementFileKey" minOccurs="0"/>
     *         <element name="videoFile" type="{}AnnouncementFileKey" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "audioFile",
        "videoFile"
    })
    public static class AnnouncementCustomSourceMediaFiles {

        @XmlElementRef(name = "audioFile", type = JAXBElement.class, required = false)
        protected JAXBElement<AnnouncementFileKey> audioFile;
        @XmlElementRef(name = "videoFile", type = JAXBElement.class, required = false)
        protected JAXBElement<AnnouncementFileKey> videoFile;

        /**
         * Ruft den Wert der audioFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
         *     
         */
        public JAXBElement<AnnouncementFileKey> getAudioFile() {
            return audioFile;
        }

        /**
         * Legt den Wert der audioFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
         *     
         */
        public void setAudioFile(JAXBElement<AnnouncementFileKey> value) {
            this.audioFile = value;
        }

        /**
         * Ruft den Wert der videoFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
         *     
         */
        public JAXBElement<AnnouncementFileKey> getVideoFile() {
            return videoFile;
        }

        /**
         * Legt den Wert der videoFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
         *     
         */
        public void setVideoFile(JAXBElement<AnnouncementFileKey> value) {
            this.videoFile = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointModify"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accessDeviceEndpoint"
    })
    public static class ExternalSource {

        @XmlElement(required = true, nillable = true)
        protected AccessDeviceEndpointModify accessDeviceEndpoint;

        /**
         * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AccessDeviceEndpointModify }
         *     
         */
        public AccessDeviceEndpointModify getAccessDeviceEndpoint() {
            return accessDeviceEndpoint;
        }

        /**
         * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AccessDeviceEndpointModify }
         *     
         */
        public void setAccessDeviceEndpoint(AccessDeviceEndpointModify value) {
            this.accessDeviceEndpoint = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="audioFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
     *         <element name="videoFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "audioFile",
        "videoFile"
    })
    public static class LabeledCustomSourceMediaFiles {

        protected LabeledMediaFileResource audioFile;
        protected LabeledMediaFileResource videoFile;

        /**
         * Ruft den Wert der audioFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public LabeledMediaFileResource getAudioFile() {
            return audioFile;
        }

        /**
         * Legt den Wert der audioFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public void setAudioFile(LabeledMediaFileResource value) {
            this.audioFile = value;
        }

        /**
         * Ruft den Wert der videoFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public LabeledMediaFileResource getVideoFile() {
            return videoFile;
        }

        /**
         * Legt den Wert der videoFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public void setVideoFile(LabeledMediaFileResource value) {
            this.videoFile = value;
        }

    }

}
