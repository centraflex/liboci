//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a Trunk Group Instance in a group.
 *         The access device cannot be modified or cleared if there are any users assigned to the Trunk Group.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupTrunkGroupModifyInstanceRequest14sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupTrunkGroupModifyInstanceRequest14sp4">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="trunkGroupKey" type="{}TrunkGroupKey"/>
 *         <element name="newName" type="{}TrunkGroupName" minOccurs="0"/>
 *         <element name="pilotUserId" type="{}UserId" minOccurs="0"/>
 *         <element name="department" type="{}DepartmentKey" minOccurs="0"/>
 *         <element name="accessDevice" type="{}AccessDevice" minOccurs="0"/>
 *         <element name="maxActiveCalls" type="{}MaxActiveCalls" minOccurs="0"/>
 *         <element name="maxIncomingCalls" type="{}MaxIncomingCalls" minOccurs="0"/>
 *         <element name="maxOutgoingCalls" type="{}MaxOutgoingCalls" minOccurs="0"/>
 *         <element name="enableBursting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="burstingMaxActiveCalls" type="{}BurstingMaxActiveCalls" minOccurs="0"/>
 *         <element name="burstingMaxIncomingCalls" type="{}BurstingMaxIncomingCalls" minOccurs="0"/>
 *         <element name="burstingMaxOutgoingCalls" type="{}BurstingMaxOutgoingCalls" minOccurs="0"/>
 *         <element name="capacityExceededAction" type="{}TrunkGroupCapacityExceededAction" minOccurs="0"/>
 *         <element name="capacityExceededForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="capacityExceededRerouteTrunkGroupKey" type="{}TrunkGroupKey" minOccurs="0"/>
 *         <element name="capacityExceededTrapInitialCalls" type="{}TrapInitialThreshold" minOccurs="0"/>
 *         <element name="capacityExceededTrapOffsetCalls" type="{}TrapOffsetThreshold" minOccurs="0"/>
 *         <element name="unreachableDestinationAction" type="{}TrunkGroupUnreachableDestinationAction" minOccurs="0"/>
 *         <element name="unreachableDestinationForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="unreachableDestinationRerouteTrunkGroupKey" type="{}TrunkGroupKey" minOccurs="0"/>
 *         <element name="unreachableDestinationTrapInitialCalls" type="{}TrapInitialThreshold" minOccurs="0"/>
 *         <element name="unreachableDestinationTrapOffsetCalls" type="{}TrapOffsetThreshold" minOccurs="0"/>
 *         <element name="invitationTimeout" type="{}TrunkGroupInvitationTimeoutSeconds" minOccurs="0"/>
 *         <element name="requireAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="sipAuthenticationUserName" type="{}SIPAuthenticationUserName" minOccurs="0"/>
 *         <element name="sipAuthenticationPassword" type="{}SIPAuthenticationPassword" minOccurs="0"/>
 *         <element name="hostedUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *         <element name="trunkGroupIdentity" type="{}SIPURI" minOccurs="0"/>
 *         <element name="otgDtgIdentity" type="{}OtgDtgIdentity" minOccurs="0"/>
 *         <element name="includeTrunkGroupIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeDtgIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableNetworkAddressIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowUnscreenedCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowUnscreenedEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="usePilotUserCallingLineIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="usePilotUserChargeNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="callForwardingAlwaysAction" type="{}TrunkGroupCallForwardingAlwaysAction" minOccurs="0"/>
 *         <element name="callForwardingAlwaysForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="callForwardingAlwaysRerouteTrunkGroupKey" type="{}TrunkGroupKey" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupTrunkGroupModifyInstanceRequest14sp4", propOrder = {
    "trunkGroupKey",
    "newName",
    "pilotUserId",
    "department",
    "accessDevice",
    "maxActiveCalls",
    "maxIncomingCalls",
    "maxOutgoingCalls",
    "enableBursting",
    "burstingMaxActiveCalls",
    "burstingMaxIncomingCalls",
    "burstingMaxOutgoingCalls",
    "capacityExceededAction",
    "capacityExceededForwardAddress",
    "capacityExceededRerouteTrunkGroupKey",
    "capacityExceededTrapInitialCalls",
    "capacityExceededTrapOffsetCalls",
    "unreachableDestinationAction",
    "unreachableDestinationForwardAddress",
    "unreachableDestinationRerouteTrunkGroupKey",
    "unreachableDestinationTrapInitialCalls",
    "unreachableDestinationTrapOffsetCalls",
    "invitationTimeout",
    "requireAuthentication",
    "sipAuthenticationUserName",
    "sipAuthenticationPassword",
    "hostedUserIdList",
    "trunkGroupIdentity",
    "otgDtgIdentity",
    "includeTrunkGroupIdentity",
    "includeDtgIdentity",
    "enableNetworkAddressIdentity",
    "allowUnscreenedCalls",
    "allowUnscreenedEmergencyCalls",
    "usePilotUserCallingLineIdentity",
    "usePilotUserChargeNumber",
    "callForwardingAlwaysAction",
    "callForwardingAlwaysForwardAddress",
    "callForwardingAlwaysRerouteTrunkGroupKey"
})
public class GroupTrunkGroupModifyInstanceRequest14Sp4
    extends OCIRequest
{

    @XmlElement(required = true)
    protected TrunkGroupKey trunkGroupKey;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newName;
    @XmlElementRef(name = "pilotUserId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pilotUserId;
    @XmlElementRef(name = "department", type = JAXBElement.class, required = false)
    protected JAXBElement<DepartmentKey> department;
    @XmlElementRef(name = "accessDevice", type = JAXBElement.class, required = false)
    protected JAXBElement<AccessDevice> accessDevice;
    protected Integer maxActiveCalls;
    @XmlElementRef(name = "maxIncomingCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxIncomingCalls;
    @XmlElementRef(name = "maxOutgoingCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxOutgoingCalls;
    protected Boolean enableBursting;
    @XmlElementRef(name = "burstingMaxActiveCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> burstingMaxActiveCalls;
    @XmlElementRef(name = "burstingMaxIncomingCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> burstingMaxIncomingCalls;
    @XmlElementRef(name = "burstingMaxOutgoingCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> burstingMaxOutgoingCalls;
    @XmlElementRef(name = "capacityExceededAction", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupCapacityExceededAction> capacityExceededAction;
    @XmlElementRef(name = "capacityExceededForwardAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> capacityExceededForwardAddress;
    @XmlElementRef(name = "capacityExceededRerouteTrunkGroupKey", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupKey> capacityExceededRerouteTrunkGroupKey;
    protected Integer capacityExceededTrapInitialCalls;
    protected Integer capacityExceededTrapOffsetCalls;
    @XmlElementRef(name = "unreachableDestinationAction", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupUnreachableDestinationAction> unreachableDestinationAction;
    @XmlElementRef(name = "unreachableDestinationForwardAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> unreachableDestinationForwardAddress;
    @XmlElementRef(name = "unreachableDestinationRerouteTrunkGroupKey", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupKey> unreachableDestinationRerouteTrunkGroupKey;
    protected Integer unreachableDestinationTrapInitialCalls;
    protected Integer unreachableDestinationTrapOffsetCalls;
    protected Integer invitationTimeout;
    protected Boolean requireAuthentication;
    @XmlElementRef(name = "sipAuthenticationUserName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sipAuthenticationUserName;
    @XmlElementRef(name = "sipAuthenticationPassword", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sipAuthenticationPassword;
    @XmlElementRef(name = "hostedUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> hostedUserIdList;
    @XmlElementRef(name = "trunkGroupIdentity", type = JAXBElement.class, required = false)
    protected JAXBElement<String> trunkGroupIdentity;
    @XmlElementRef(name = "otgDtgIdentity", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otgDtgIdentity;
    protected Boolean includeTrunkGroupIdentity;
    protected Boolean includeDtgIdentity;
    protected Boolean enableNetworkAddressIdentity;
    protected Boolean allowUnscreenedCalls;
    protected Boolean allowUnscreenedEmergencyCalls;
    protected Boolean usePilotUserCallingLineIdentity;
    protected Boolean usePilotUserChargeNumber;
    @XmlElementRef(name = "callForwardingAlwaysAction", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupCallForwardingAlwaysAction> callForwardingAlwaysAction;
    @XmlElementRef(name = "callForwardingAlwaysForwardAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callForwardingAlwaysForwardAddress;
    @XmlElementRef(name = "callForwardingAlwaysRerouteTrunkGroupKey", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupKey> callForwardingAlwaysRerouteTrunkGroupKey;

    /**
     * Ruft den Wert der trunkGroupKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupKey }
     *     
     */
    public TrunkGroupKey getTrunkGroupKey() {
        return trunkGroupKey;
    }

    /**
     * Legt den Wert der trunkGroupKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupKey }
     *     
     */
    public void setTrunkGroupKey(TrunkGroupKey value) {
        this.trunkGroupKey = value;
    }

    /**
     * Ruft den Wert der newName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewName() {
        return newName;
    }

    /**
     * Legt den Wert der newName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewName(String value) {
        this.newName = value;
    }

    /**
     * Ruft den Wert der pilotUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPilotUserId() {
        return pilotUserId;
    }

    /**
     * Legt den Wert der pilotUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPilotUserId(JAXBElement<String> value) {
        this.pilotUserId = value;
    }

    /**
     * Ruft den Wert der department-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DepartmentKey }{@code >}
     *     
     */
    public JAXBElement<DepartmentKey> getDepartment() {
        return department;
    }

    /**
     * Legt den Wert der department-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DepartmentKey }{@code >}
     *     
     */
    public void setDepartment(JAXBElement<DepartmentKey> value) {
        this.department = value;
    }

    /**
     * Ruft den Wert der accessDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccessDevice }{@code >}
     *     
     */
    public JAXBElement<AccessDevice> getAccessDevice() {
        return accessDevice;
    }

    /**
     * Legt den Wert der accessDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccessDevice }{@code >}
     *     
     */
    public void setAccessDevice(JAXBElement<AccessDevice> value) {
        this.accessDevice = value;
    }

    /**
     * Ruft den Wert der maxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxActiveCalls() {
        return maxActiveCalls;
    }

    /**
     * Legt den Wert der maxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxActiveCalls(Integer value) {
        this.maxActiveCalls = value;
    }

    /**
     * Ruft den Wert der maxIncomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxIncomingCalls() {
        return maxIncomingCalls;
    }

    /**
     * Legt den Wert der maxIncomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxIncomingCalls(JAXBElement<Integer> value) {
        this.maxIncomingCalls = value;
    }

    /**
     * Ruft den Wert der maxOutgoingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxOutgoingCalls() {
        return maxOutgoingCalls;
    }

    /**
     * Legt den Wert der maxOutgoingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxOutgoingCalls(JAXBElement<Integer> value) {
        this.maxOutgoingCalls = value;
    }

    /**
     * Ruft den Wert der enableBursting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableBursting() {
        return enableBursting;
    }

    /**
     * Legt den Wert der enableBursting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableBursting(Boolean value) {
        this.enableBursting = value;
    }

    /**
     * Ruft den Wert der burstingMaxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getBurstingMaxActiveCalls() {
        return burstingMaxActiveCalls;
    }

    /**
     * Legt den Wert der burstingMaxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setBurstingMaxActiveCalls(JAXBElement<Integer> value) {
        this.burstingMaxActiveCalls = value;
    }

    /**
     * Ruft den Wert der burstingMaxIncomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getBurstingMaxIncomingCalls() {
        return burstingMaxIncomingCalls;
    }

    /**
     * Legt den Wert der burstingMaxIncomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setBurstingMaxIncomingCalls(JAXBElement<Integer> value) {
        this.burstingMaxIncomingCalls = value;
    }

    /**
     * Ruft den Wert der burstingMaxOutgoingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getBurstingMaxOutgoingCalls() {
        return burstingMaxOutgoingCalls;
    }

    /**
     * Legt den Wert der burstingMaxOutgoingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setBurstingMaxOutgoingCalls(JAXBElement<Integer> value) {
        this.burstingMaxOutgoingCalls = value;
    }

    /**
     * Ruft den Wert der capacityExceededAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupCapacityExceededAction }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupCapacityExceededAction> getCapacityExceededAction() {
        return capacityExceededAction;
    }

    /**
     * Legt den Wert der capacityExceededAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupCapacityExceededAction }{@code >}
     *     
     */
    public void setCapacityExceededAction(JAXBElement<TrunkGroupCapacityExceededAction> value) {
        this.capacityExceededAction = value;
    }

    /**
     * Ruft den Wert der capacityExceededForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCapacityExceededForwardAddress() {
        return capacityExceededForwardAddress;
    }

    /**
     * Legt den Wert der capacityExceededForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCapacityExceededForwardAddress(JAXBElement<String> value) {
        this.capacityExceededForwardAddress = value;
    }

    /**
     * Ruft den Wert der capacityExceededRerouteTrunkGroupKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupKey }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupKey> getCapacityExceededRerouteTrunkGroupKey() {
        return capacityExceededRerouteTrunkGroupKey;
    }

    /**
     * Legt den Wert der capacityExceededRerouteTrunkGroupKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupKey }{@code >}
     *     
     */
    public void setCapacityExceededRerouteTrunkGroupKey(JAXBElement<TrunkGroupKey> value) {
        this.capacityExceededRerouteTrunkGroupKey = value;
    }

    /**
     * Ruft den Wert der capacityExceededTrapInitialCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCapacityExceededTrapInitialCalls() {
        return capacityExceededTrapInitialCalls;
    }

    /**
     * Legt den Wert der capacityExceededTrapInitialCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCapacityExceededTrapInitialCalls(Integer value) {
        this.capacityExceededTrapInitialCalls = value;
    }

    /**
     * Ruft den Wert der capacityExceededTrapOffsetCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCapacityExceededTrapOffsetCalls() {
        return capacityExceededTrapOffsetCalls;
    }

    /**
     * Legt den Wert der capacityExceededTrapOffsetCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCapacityExceededTrapOffsetCalls(Integer value) {
        this.capacityExceededTrapOffsetCalls = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupUnreachableDestinationAction }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupUnreachableDestinationAction> getUnreachableDestinationAction() {
        return unreachableDestinationAction;
    }

    /**
     * Legt den Wert der unreachableDestinationAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupUnreachableDestinationAction }{@code >}
     *     
     */
    public void setUnreachableDestinationAction(JAXBElement<TrunkGroupUnreachableDestinationAction> value) {
        this.unreachableDestinationAction = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUnreachableDestinationForwardAddress() {
        return unreachableDestinationForwardAddress;
    }

    /**
     * Legt den Wert der unreachableDestinationForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUnreachableDestinationForwardAddress(JAXBElement<String> value) {
        this.unreachableDestinationForwardAddress = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationRerouteTrunkGroupKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupKey }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupKey> getUnreachableDestinationRerouteTrunkGroupKey() {
        return unreachableDestinationRerouteTrunkGroupKey;
    }

    /**
     * Legt den Wert der unreachableDestinationRerouteTrunkGroupKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupKey }{@code >}
     *     
     */
    public void setUnreachableDestinationRerouteTrunkGroupKey(JAXBElement<TrunkGroupKey> value) {
        this.unreachableDestinationRerouteTrunkGroupKey = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationTrapInitialCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUnreachableDestinationTrapInitialCalls() {
        return unreachableDestinationTrapInitialCalls;
    }

    /**
     * Legt den Wert der unreachableDestinationTrapInitialCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUnreachableDestinationTrapInitialCalls(Integer value) {
        this.unreachableDestinationTrapInitialCalls = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationTrapOffsetCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUnreachableDestinationTrapOffsetCalls() {
        return unreachableDestinationTrapOffsetCalls;
    }

    /**
     * Legt den Wert der unreachableDestinationTrapOffsetCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUnreachableDestinationTrapOffsetCalls(Integer value) {
        this.unreachableDestinationTrapOffsetCalls = value;
    }

    /**
     * Ruft den Wert der invitationTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInvitationTimeout() {
        return invitationTimeout;
    }

    /**
     * Legt den Wert der invitationTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInvitationTimeout(Integer value) {
        this.invitationTimeout = value;
    }

    /**
     * Ruft den Wert der requireAuthentication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequireAuthentication() {
        return requireAuthentication;
    }

    /**
     * Legt den Wert der requireAuthentication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequireAuthentication(Boolean value) {
        this.requireAuthentication = value;
    }

    /**
     * Ruft den Wert der sipAuthenticationUserName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSipAuthenticationUserName() {
        return sipAuthenticationUserName;
    }

    /**
     * Legt den Wert der sipAuthenticationUserName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSipAuthenticationUserName(JAXBElement<String> value) {
        this.sipAuthenticationUserName = value;
    }

    /**
     * Ruft den Wert der sipAuthenticationPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSipAuthenticationPassword() {
        return sipAuthenticationPassword;
    }

    /**
     * Legt den Wert der sipAuthenticationPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSipAuthenticationPassword(JAXBElement<String> value) {
        this.sipAuthenticationPassword = value;
    }

    /**
     * Ruft den Wert der hostedUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getHostedUserIdList() {
        return hostedUserIdList;
    }

    /**
     * Legt den Wert der hostedUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setHostedUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.hostedUserIdList = value;
    }

    /**
     * Ruft den Wert der trunkGroupIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTrunkGroupIdentity() {
        return trunkGroupIdentity;
    }

    /**
     * Legt den Wert der trunkGroupIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTrunkGroupIdentity(JAXBElement<String> value) {
        this.trunkGroupIdentity = value;
    }

    /**
     * Ruft den Wert der otgDtgIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOtgDtgIdentity() {
        return otgDtgIdentity;
    }

    /**
     * Legt den Wert der otgDtgIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOtgDtgIdentity(JAXBElement<String> value) {
        this.otgDtgIdentity = value;
    }

    /**
     * Ruft den Wert der includeTrunkGroupIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeTrunkGroupIdentity() {
        return includeTrunkGroupIdentity;
    }

    /**
     * Legt den Wert der includeTrunkGroupIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeTrunkGroupIdentity(Boolean value) {
        this.includeTrunkGroupIdentity = value;
    }

    /**
     * Ruft den Wert der includeDtgIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeDtgIdentity() {
        return includeDtgIdentity;
    }

    /**
     * Legt den Wert der includeDtgIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeDtgIdentity(Boolean value) {
        this.includeDtgIdentity = value;
    }

    /**
     * Ruft den Wert der enableNetworkAddressIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableNetworkAddressIdentity() {
        return enableNetworkAddressIdentity;
    }

    /**
     * Legt den Wert der enableNetworkAddressIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableNetworkAddressIdentity(Boolean value) {
        this.enableNetworkAddressIdentity = value;
    }

    /**
     * Ruft den Wert der allowUnscreenedCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowUnscreenedCalls() {
        return allowUnscreenedCalls;
    }

    /**
     * Legt den Wert der allowUnscreenedCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowUnscreenedCalls(Boolean value) {
        this.allowUnscreenedCalls = value;
    }

    /**
     * Ruft den Wert der allowUnscreenedEmergencyCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowUnscreenedEmergencyCalls() {
        return allowUnscreenedEmergencyCalls;
    }

    /**
     * Legt den Wert der allowUnscreenedEmergencyCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowUnscreenedEmergencyCalls(Boolean value) {
        this.allowUnscreenedEmergencyCalls = value;
    }

    /**
     * Ruft den Wert der usePilotUserCallingLineIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUsePilotUserCallingLineIdentity() {
        return usePilotUserCallingLineIdentity;
    }

    /**
     * Legt den Wert der usePilotUserCallingLineIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUsePilotUserCallingLineIdentity(Boolean value) {
        this.usePilotUserCallingLineIdentity = value;
    }

    /**
     * Ruft den Wert der usePilotUserChargeNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUsePilotUserChargeNumber() {
        return usePilotUserChargeNumber;
    }

    /**
     * Legt den Wert der usePilotUserChargeNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUsePilotUserChargeNumber(Boolean value) {
        this.usePilotUserChargeNumber = value;
    }

    /**
     * Ruft den Wert der callForwardingAlwaysAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupCallForwardingAlwaysAction }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupCallForwardingAlwaysAction> getCallForwardingAlwaysAction() {
        return callForwardingAlwaysAction;
    }

    /**
     * Legt den Wert der callForwardingAlwaysAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupCallForwardingAlwaysAction }{@code >}
     *     
     */
    public void setCallForwardingAlwaysAction(JAXBElement<TrunkGroupCallForwardingAlwaysAction> value) {
        this.callForwardingAlwaysAction = value;
    }

    /**
     * Ruft den Wert der callForwardingAlwaysForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallForwardingAlwaysForwardAddress() {
        return callForwardingAlwaysForwardAddress;
    }

    /**
     * Legt den Wert der callForwardingAlwaysForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallForwardingAlwaysForwardAddress(JAXBElement<String> value) {
        this.callForwardingAlwaysForwardAddress = value;
    }

    /**
     * Ruft den Wert der callForwardingAlwaysRerouteTrunkGroupKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupKey }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupKey> getCallForwardingAlwaysRerouteTrunkGroupKey() {
        return callForwardingAlwaysRerouteTrunkGroupKey;
    }

    /**
     * Legt den Wert der callForwardingAlwaysRerouteTrunkGroupKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupKey }{@code >}
     *     
     */
    public void setCallForwardingAlwaysRerouteTrunkGroupKey(JAXBElement<TrunkGroupKey> value) {
        this.callForwardingAlwaysRerouteTrunkGroupKey = value;
    }

}
