//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CommunicationBarringOriginatingAction15sp2.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CommunicationBarringOriginatingAction15sp2">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Allow"/>
 *     <enumeration value="Block"/>
 *     <enumeration value="Authorization Code"/>
 *     <enumeration value="Treatment"/>
 *     <enumeration value="Transfer"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CommunicationBarringOriginatingAction15sp2")
@XmlEnum
public enum CommunicationBarringOriginatingAction15Sp2 {

    @XmlEnumValue("Allow")
    ALLOW("Allow"),
    @XmlEnumValue("Block")
    BLOCK("Block"),
    @XmlEnumValue("Authorization Code")
    AUTHORIZATION_CODE("Authorization Code"),
    @XmlEnumValue("Treatment")
    TREATMENT("Treatment"),
    @XmlEnumValue("Transfer")
    TRANSFER("Transfer");
    private final String value;

    CommunicationBarringOriginatingAction15Sp2(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommunicationBarringOriginatingAction15Sp2 fromValue(String v) {
        for (CommunicationBarringOriginatingAction15Sp2 c: CommunicationBarringOriginatingAction15Sp2 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
