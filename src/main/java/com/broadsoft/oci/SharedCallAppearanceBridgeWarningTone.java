//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SharedCallAppearanceBridgeWarningTone.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SharedCallAppearanceBridgeWarningTone">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="None"/>
 *     <enumeration value="Barge-In"/>
 *     <enumeration value="Barge-In and Repeat"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SharedCallAppearanceBridgeWarningTone")
@XmlEnum
public enum SharedCallAppearanceBridgeWarningTone {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Barge-In")
    BARGE_IN("Barge-In"),
    @XmlEnumValue("Barge-In and Repeat")
    BARGE_IN_AND_REPEAT("Barge-In and Repeat");
    private final String value;

    SharedCallAppearanceBridgeWarningTone(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SharedCallAppearanceBridgeWarningTone fromValue(String v) {
        for (SharedCallAppearanceBridgeWarningTone c: SharedCallAppearanceBridgeWarningTone.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
