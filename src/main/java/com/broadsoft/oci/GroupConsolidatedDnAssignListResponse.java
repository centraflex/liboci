//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupConsolidatedDnAssignListRequest.
 *       
 * 
 * <p>Java-Klasse für GroupConsolidatedDnAssignListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupConsolidatedDnAssignListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="dnValidationError" type="{}DNValidationStatusMessage" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupConsolidatedDnAssignListResponse", propOrder = {
    "dnValidationError"
})
public class GroupConsolidatedDnAssignListResponse
    extends OCIDataResponse
{

    protected List<DNValidationStatusMessage> dnValidationError;

    /**
     * Gets the value of the dnValidationError property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the dnValidationError property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDnValidationError().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DNValidationStatusMessage }
     * 
     * 
     * @return
     *     The value of the dnValidationError property.
     */
    public List<DNValidationStatusMessage> getDnValidationError() {
        if (dnValidationError == null) {
            dnValidationError = new ArrayList<>();
        }
        return this.dnValidationError;
    }

}
