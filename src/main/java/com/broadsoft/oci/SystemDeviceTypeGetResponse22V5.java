//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemDeviceTypeGetRequest22V5.
 * 
 *         The following elements are only used in AS data mode:
 *           resellerId
 *       
 * 
 * <p>Java-Klasse für SystemDeviceTypeGetResponse22V5 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceTypeGetResponse22V5">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isObsolete" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="profile" type="{}SignalingAddressType"/>
 *         <element name="webBasedConfigURL" type="{}WebBasedConfigURL" minOccurs="0"/>
 *         <element name="staticRegistrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="cpeDeviceOptions" type="{}CPEDeviceOptionsRead22V4" minOccurs="0"/>
 *         <element name="protocolChoice" type="{}AccessDeviceProtocol" maxOccurs="unbounded"/>
 *         <element name="isIpAddressOptional" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useDomain" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isMobilityManagerDevice" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="deviceTypeConfigurationOption" type="{}DeviceTypeConfigurationOptionType" minOccurs="0"/>
 *         <element name="staticLineOrdering" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="resellerId" type="{}ResellerId22" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceTypeGetResponse22V5", propOrder = {
    "isObsolete",
    "profile",
    "webBasedConfigURL",
    "staticRegistrationCapable",
    "cpeDeviceOptions",
    "protocolChoice",
    "isIpAddressOptional",
    "useDomain",
    "isMobilityManagerDevice",
    "deviceTypeConfigurationOption",
    "staticLineOrdering",
    "resellerId"
})
public class SystemDeviceTypeGetResponse22V5
    extends OCIDataResponse
{

    protected boolean isObsolete;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SignalingAddressType profile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String webBasedConfigURL;
    protected boolean staticRegistrationCapable;
    protected CPEDeviceOptionsRead22V4 cpeDeviceOptions;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> protocolChoice;
    protected boolean isIpAddressOptional;
    protected boolean useDomain;
    protected boolean isMobilityManagerDevice;
    @XmlSchemaType(name = "token")
    protected DeviceTypeConfigurationOptionType deviceTypeConfigurationOption;
    protected Boolean staticLineOrdering;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;

    /**
     * Ruft den Wert der isObsolete-Eigenschaft ab.
     * 
     */
    public boolean isIsObsolete() {
        return isObsolete;
    }

    /**
     * Legt den Wert der isObsolete-Eigenschaft fest.
     * 
     */
    public void setIsObsolete(boolean value) {
        this.isObsolete = value;
    }

    /**
     * Ruft den Wert der profile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SignalingAddressType }
     *     
     */
    public SignalingAddressType getProfile() {
        return profile;
    }

    /**
     * Legt den Wert der profile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SignalingAddressType }
     *     
     */
    public void setProfile(SignalingAddressType value) {
        this.profile = value;
    }

    /**
     * Ruft den Wert der webBasedConfigURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebBasedConfigURL() {
        return webBasedConfigURL;
    }

    /**
     * Legt den Wert der webBasedConfigURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebBasedConfigURL(String value) {
        this.webBasedConfigURL = value;
    }

    /**
     * Ruft den Wert der staticRegistrationCapable-Eigenschaft ab.
     * 
     */
    public boolean isStaticRegistrationCapable() {
        return staticRegistrationCapable;
    }

    /**
     * Legt den Wert der staticRegistrationCapable-Eigenschaft fest.
     * 
     */
    public void setStaticRegistrationCapable(boolean value) {
        this.staticRegistrationCapable = value;
    }

    /**
     * Ruft den Wert der cpeDeviceOptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CPEDeviceOptionsRead22V4 }
     *     
     */
    public CPEDeviceOptionsRead22V4 getCpeDeviceOptions() {
        return cpeDeviceOptions;
    }

    /**
     * Legt den Wert der cpeDeviceOptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CPEDeviceOptionsRead22V4 }
     *     
     */
    public void setCpeDeviceOptions(CPEDeviceOptionsRead22V4 value) {
        this.cpeDeviceOptions = value;
    }

    /**
     * Gets the value of the protocolChoice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the protocolChoice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProtocolChoice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the protocolChoice property.
     */
    public List<String> getProtocolChoice() {
        if (protocolChoice == null) {
            protocolChoice = new ArrayList<>();
        }
        return this.protocolChoice;
    }

    /**
     * Ruft den Wert der isIpAddressOptional-Eigenschaft ab.
     * 
     */
    public boolean isIsIpAddressOptional() {
        return isIpAddressOptional;
    }

    /**
     * Legt den Wert der isIpAddressOptional-Eigenschaft fest.
     * 
     */
    public void setIsIpAddressOptional(boolean value) {
        this.isIpAddressOptional = value;
    }

    /**
     * Ruft den Wert der useDomain-Eigenschaft ab.
     * 
     */
    public boolean isUseDomain() {
        return useDomain;
    }

    /**
     * Legt den Wert der useDomain-Eigenschaft fest.
     * 
     */
    public void setUseDomain(boolean value) {
        this.useDomain = value;
    }

    /**
     * Ruft den Wert der isMobilityManagerDevice-Eigenschaft ab.
     * 
     */
    public boolean isIsMobilityManagerDevice() {
        return isMobilityManagerDevice;
    }

    /**
     * Legt den Wert der isMobilityManagerDevice-Eigenschaft fest.
     * 
     */
    public void setIsMobilityManagerDevice(boolean value) {
        this.isMobilityManagerDevice = value;
    }

    /**
     * Ruft den Wert der deviceTypeConfigurationOption-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceTypeConfigurationOptionType }
     *     
     */
    public DeviceTypeConfigurationOptionType getDeviceTypeConfigurationOption() {
        return deviceTypeConfigurationOption;
    }

    /**
     * Legt den Wert der deviceTypeConfigurationOption-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceTypeConfigurationOptionType }
     *     
     */
    public void setDeviceTypeConfigurationOption(DeviceTypeConfigurationOptionType value) {
        this.deviceTypeConfigurationOption = value;
    }

    /**
     * Ruft den Wert der staticLineOrdering-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStaticLineOrdering() {
        return staticLineOrdering;
    }

    /**
     * Legt den Wert der staticLineOrdering-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStaticLineOrdering(Boolean value) {
        this.staticLineOrdering = value;
    }

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

}
