//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für RegistrationEndpointType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="RegistrationEndpointType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Primary"/>
 *     <enumeration value="Shared Call Appearance"/>
 *     <enumeration value="Video Add On"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "RegistrationEndpointType")
@XmlEnum
public enum RegistrationEndpointType {

    @XmlEnumValue("Primary")
    PRIMARY("Primary"),
    @XmlEnumValue("Shared Call Appearance")
    SHARED_CALL_APPEARANCE("Shared Call Appearance"),
    @XmlEnumValue("Video Add On")
    VIDEO_ADD_ON("Video Add On");
    private final String value;

    RegistrationEndpointType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RegistrationEndpointType fromValue(String v) {
        for (RegistrationEndpointType c: RegistrationEndpointType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
