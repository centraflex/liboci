//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Get a list of Auto Attendant instances within a group.
 *         The response is either GroupAutoAttendantGetInstancePagedSortedListResponse or ErrorResponse.
 *         If no sortOrder is included the response is sorted by Name ascending by default. 
 *         
 *         A limitation to the search by DN activation exists when the Number Activation mode is set to
 *         Off. In this case DNs not assigned to users are never returned by queries with the
 *         "dnActivationSearchCriteria" included.
 *         
 *         If the responsePagingControl element is not provided,
 *         the paging startIndex will be set to 1 by default,
 *         and the responsePageSize will be set to the maximum responsePageSize by
 *         default.
 *         Multiple search criteria are logically ANDed together unless the searchCriteriaModeOr option is included.
 *         Then the search criteria are logically ORed together.
 *       
 * 
 * <p>Java-Klasse für GroupAutoAttendantGetInstancePagedSortedListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAutoAttendantGetInstancePagedSortedListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl" minOccurs="0"/>
 *         <element name="sortOrder" type="{}SortOrderGroupAutoAttendantGetInstancePagedSortedList" maxOccurs="3" minOccurs="0"/>
 *         <element name="searchCriteriaUserId" type="{}SearchCriteriaUserId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaUserLastName" type="{}SearchCriteriaUserLastName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDn" type="{}SearchCriteriaDn" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExtension" type="{}SearchCriteriaExtension" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactDnActivation" type="{}SearchCriteriaExactDnActivation" minOccurs="0"/>
 *         <element name="searchCriteriaServiceStatus" type="{}SearchCriteriaServiceStatus" minOccurs="0"/>
 *         <element name="searchCriteriaExactAutoAttendantType" type="{}SearchCriteriaExactAutoAttendantType" minOccurs="0"/>
 *         <element name="searchCriteriaModeOr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAutoAttendantGetInstancePagedSortedListRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "responsePagingControl",
    "sortOrder",
    "searchCriteriaUserId",
    "searchCriteriaUserLastName",
    "searchCriteriaDn",
    "searchCriteriaExtension",
    "searchCriteriaExactDnActivation",
    "searchCriteriaServiceStatus",
    "searchCriteriaExactAutoAttendantType",
    "searchCriteriaModeOr"
})
public class GroupAutoAttendantGetInstancePagedSortedListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected ResponsePagingControl responsePagingControl;
    protected List<SortOrderGroupAutoAttendantGetInstancePagedSortedList> sortOrder;
    protected List<SearchCriteriaUserId> searchCriteriaUserId;
    protected List<SearchCriteriaUserLastName> searchCriteriaUserLastName;
    protected List<SearchCriteriaDn> searchCriteriaDn;
    protected List<SearchCriteriaExtension> searchCriteriaExtension;
    protected SearchCriteriaExactDnActivation searchCriteriaExactDnActivation;
    protected SearchCriteriaServiceStatus searchCriteriaServiceStatus;
    protected SearchCriteriaExactAutoAttendantType searchCriteriaExactAutoAttendantType;
    protected Boolean searchCriteriaModeOr;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Gets the value of the sortOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the sortOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSortOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SortOrderGroupAutoAttendantGetInstancePagedSortedList }
     * 
     * 
     * @return
     *     The value of the sortOrder property.
     */
    public List<SortOrderGroupAutoAttendantGetInstancePagedSortedList> getSortOrder() {
        if (sortOrder == null) {
            sortOrder = new ArrayList<>();
        }
        return this.sortOrder;
    }

    /**
     * Gets the value of the searchCriteriaUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserId property.
     */
    public List<SearchCriteriaUserId> getSearchCriteriaUserId() {
        if (searchCriteriaUserId == null) {
            searchCriteriaUserId = new ArrayList<>();
        }
        return this.searchCriteriaUserId;
    }

    /**
     * Gets the value of the searchCriteriaUserLastName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserLastName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserLastName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserLastName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserLastName property.
     */
    public List<SearchCriteriaUserLastName> getSearchCriteriaUserLastName() {
        if (searchCriteriaUserLastName == null) {
            searchCriteriaUserLastName = new ArrayList<>();
        }
        return this.searchCriteriaUserLastName;
    }

    /**
     * Gets the value of the searchCriteriaDn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDn }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDn property.
     */
    public List<SearchCriteriaDn> getSearchCriteriaDn() {
        if (searchCriteriaDn == null) {
            searchCriteriaDn = new ArrayList<>();
        }
        return this.searchCriteriaDn;
    }

    /**
     * Gets the value of the searchCriteriaExtension property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaExtension property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaExtension().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaExtension }
     * 
     * 
     * @return
     *     The value of the searchCriteriaExtension property.
     */
    public List<SearchCriteriaExtension> getSearchCriteriaExtension() {
        if (searchCriteriaExtension == null) {
            searchCriteriaExtension = new ArrayList<>();
        }
        return this.searchCriteriaExtension;
    }

    /**
     * Ruft den Wert der searchCriteriaExactDnActivation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactDnActivation }
     *     
     */
    public SearchCriteriaExactDnActivation getSearchCriteriaExactDnActivation() {
        return searchCriteriaExactDnActivation;
    }

    /**
     * Legt den Wert der searchCriteriaExactDnActivation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactDnActivation }
     *     
     */
    public void setSearchCriteriaExactDnActivation(SearchCriteriaExactDnActivation value) {
        this.searchCriteriaExactDnActivation = value;
    }

    /**
     * Ruft den Wert der searchCriteriaServiceStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaServiceStatus }
     *     
     */
    public SearchCriteriaServiceStatus getSearchCriteriaServiceStatus() {
        return searchCriteriaServiceStatus;
    }

    /**
     * Legt den Wert der searchCriteriaServiceStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaServiceStatus }
     *     
     */
    public void setSearchCriteriaServiceStatus(SearchCriteriaServiceStatus value) {
        this.searchCriteriaServiceStatus = value;
    }

    /**
     * Ruft den Wert der searchCriteriaExactAutoAttendantType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactAutoAttendantType }
     *     
     */
    public SearchCriteriaExactAutoAttendantType getSearchCriteriaExactAutoAttendantType() {
        return searchCriteriaExactAutoAttendantType;
    }

    /**
     * Legt den Wert der searchCriteriaExactAutoAttendantType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactAutoAttendantType }
     *     
     */
    public void setSearchCriteriaExactAutoAttendantType(SearchCriteriaExactAutoAttendantType value) {
        this.searchCriteriaExactAutoAttendantType = value;
    }

    /**
     * Ruft den Wert der searchCriteriaModeOr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchCriteriaModeOr() {
        return searchCriteriaModeOr;
    }

    /**
     * Legt den Wert der searchCriteriaModeOr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchCriteriaModeOr(Boolean value) {
        this.searchCriteriaModeOr = value;
    }

}
