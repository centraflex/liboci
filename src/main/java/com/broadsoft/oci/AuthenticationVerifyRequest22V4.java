//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlElementRefs;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         AuthenticationVerifyRequest22V4 is used to authenticate a user either by userId/password, userId/sip username/sip password,
 *         dn/passcode, lineport/password or a token previously authorized with the ExternalAuthenticationAuthorizeTokenRequest. 
 *         The phone number may be any DN associated with a user.
 *         The lineport may be any lineport associated with a user.
 *         The password used for the lineport is the user's password associated with userId.
 *           
 *         The response is a AuthenticationVerifyResponse22V4 or an ErrorResponse
 *       
 * 
 * <p>Java-Klasse für AuthenticationVerifyRequest22V4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="AuthenticationVerifyRequest22V4">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <choice>
 *         <sequence>
 *           <element name="userId" type="{}UserId"/>
 *           <element name="password" type="{}Password"/>
 *         </sequence>
 *         <sequence>
 *           <element name="phoneNumber" type="{}DN"/>
 *           <element name="passcode" type="{}Passcode"/>
 *         </sequence>
 *         <sequence>
 *           <element name="linePort" type="{}AccessDeviceEndpointLinePort"/>
 *           <element name="password" type="{}Password"/>
 *         </sequence>
 *         <sequence>
 *           <element name="loginToken" type="{}LoginToken"/>
 *         </sequence>
 *         <sequence>
 *           <element name="sipAuthenticationUserName" type="{}SIPAuthenticationUserName"/>
 *           <element name="sipAuthenticationPassword" type="{}SIPAuthenticationPassword"/>
 *           <element name="userId" type="{}UserId"/>
 *         </sequence>
 *       </choice>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuthenticationVerifyRequest22V4", propOrder = {
    "rest"
})
public class AuthenticationVerifyRequest22V4
    extends OCIRequest
{

    @XmlElementRefs({
        @XmlElementRef(name = "userId", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "password", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "phoneNumber", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "passcode", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "linePort", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "loginToken", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "sipAuthenticationUserName", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "sipAuthenticationPassword", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<String>> rest;

    /**
     * Ruft das restliche Contentmodell ab. 
     * 
     * <p>
     * Sie rufen diese "catch-all"-Eigenschaft aus folgendem Grund ab: 
     * Der Feldname "Password" wird von zwei verschiedenen Teilen eines Schemas verwendet. Siehe: 
     * Zeile 71 von file:/home/prelle/git/liboci/xsd-24/OCISchemaLogin.xsd
     * Zeile 63 von file:/home/prelle/git/liboci/xsd-24/OCISchemaLogin.xsd
     * <p>
     * Um diese Eigenschaft zu entfernen, wenden Sie eine Eigenschaftenanpassung für eine
     * der beiden folgenden Deklarationen an, um deren Namen zu ändern:Gets the value of the rest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the rest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * 
     * @return
     *     The value of the rest property.
     */
    public List<JAXBElement<String>> getRest() {
        if (rest == null) {
            rest = new ArrayList<>();
        }
        return this.rest;
    }

}
