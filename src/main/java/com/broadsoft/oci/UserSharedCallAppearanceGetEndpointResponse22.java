//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the UserSharedCallAppearanceGetEndpointRequest.
 * 
 *         The following elements are only used in AS data mode and not returned in XS data mode:
 *           hotlineContact
 * 
 *         The following elements are only used in AS data mode and a value false is returned in the XS mode:
 *           useHotline
 * 
 *       
 * 
 * <p>Java-Klasse für UserSharedCallAppearanceGetEndpointResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserSharedCallAppearanceGetEndpointResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowOrigination" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowTermination" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useHotline" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="hotlineContact" type="{}SIPURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserSharedCallAppearanceGetEndpointResponse22", propOrder = {
    "isActive",
    "allowOrigination",
    "allowTermination",
    "useHotline",
    "hotlineContact"
})
public class UserSharedCallAppearanceGetEndpointResponse22
    extends OCIDataResponse
{

    protected boolean isActive;
    protected boolean allowOrigination;
    protected boolean allowTermination;
    protected boolean useHotline;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String hotlineContact;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der allowOrigination-Eigenschaft ab.
     * 
     */
    public boolean isAllowOrigination() {
        return allowOrigination;
    }

    /**
     * Legt den Wert der allowOrigination-Eigenschaft fest.
     * 
     */
    public void setAllowOrigination(boolean value) {
        this.allowOrigination = value;
    }

    /**
     * Ruft den Wert der allowTermination-Eigenschaft ab.
     * 
     */
    public boolean isAllowTermination() {
        return allowTermination;
    }

    /**
     * Legt den Wert der allowTermination-Eigenschaft fest.
     * 
     */
    public void setAllowTermination(boolean value) {
        this.allowTermination = value;
    }

    /**
     * Ruft den Wert der useHotline-Eigenschaft ab.
     * 
     */
    public boolean isUseHotline() {
        return useHotline;
    }

    /**
     * Legt den Wert der useHotline-Eigenschaft fest.
     * 
     */
    public void setUseHotline(boolean value) {
        this.useHotline = value;
    }

    /**
     * Ruft den Wert der hotlineContact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotlineContact() {
        return hotlineContact;
    }

    /**
     * Legt den Wert der hotlineContact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotlineContact(String value) {
        this.hotlineContact = value;
    }

}
