//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *                 The sort criteria specifies the forwarded to phone number as the column for
 *                 the sort, whether the sort is ascending or descending, and whether the
 *                 sort is case sensitive.
 *                 This sort criteria data type is only intended to be used by the commands 
 *                 introduced by BW-2301. 
 *                 The commands are EnterpriseUserCallForwardingSettingsGetListRequest 
 *                 and GroupUserCallForwardingSettingsGetListRequest.
 *                 The following Call Forwarding services are compatible for this search:
 *                 Call Forwarding Always, Call Forwarding Always Secondary, Call Forwarding Busy,
 *                 Call Forwarding No Answer, Call Forwarding Not Reachable, Call Forwarding Selective.                
 *             
 * 
 * <p>Java-Klasse für SortByForwardedToNumber complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SortByForwardedToNumber">
 *   <complexContent>
 *     <extension base="{}SortCriteria">
 *       <sequence>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SortByForwardedToNumber")
public class SortByForwardedToNumber
    extends SortCriteria
{


}
