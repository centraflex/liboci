//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify OCI Reporting system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemOCIReportingParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemOCIReportingParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serverPort" type="{}Port1025" minOccurs="0"/>
 *         <element name="enableConnectionPing" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="connectionPingIntervalSeconds" type="{}OCIReportingConnectionPingIntervalSeconds" minOccurs="0"/>
 *         <element name="alterPasswords" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enablePublicIdentityReporting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="secure" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemOCIReportingParametersModifyRequest", propOrder = {
    "serverPort",
    "enableConnectionPing",
    "connectionPingIntervalSeconds",
    "alterPasswords",
    "enablePublicIdentityReporting",
    "secure"
})
public class SystemOCIReportingParametersModifyRequest
    extends OCIRequest
{

    protected Integer serverPort;
    protected Boolean enableConnectionPing;
    protected Integer connectionPingIntervalSeconds;
    protected Boolean alterPasswords;
    protected Boolean enablePublicIdentityReporting;
    protected Boolean secure;

    /**
     * Ruft den Wert der serverPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServerPort() {
        return serverPort;
    }

    /**
     * Legt den Wert der serverPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServerPort(Integer value) {
        this.serverPort = value;
    }

    /**
     * Ruft den Wert der enableConnectionPing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableConnectionPing() {
        return enableConnectionPing;
    }

    /**
     * Legt den Wert der enableConnectionPing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableConnectionPing(Boolean value) {
        this.enableConnectionPing = value;
    }

    /**
     * Ruft den Wert der connectionPingIntervalSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getConnectionPingIntervalSeconds() {
        return connectionPingIntervalSeconds;
    }

    /**
     * Legt den Wert der connectionPingIntervalSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setConnectionPingIntervalSeconds(Integer value) {
        this.connectionPingIntervalSeconds = value;
    }

    /**
     * Ruft den Wert der alterPasswords-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlterPasswords() {
        return alterPasswords;
    }

    /**
     * Legt den Wert der alterPasswords-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlterPasswords(Boolean value) {
        this.alterPasswords = value;
    }

    /**
     * Ruft den Wert der enablePublicIdentityReporting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnablePublicIdentityReporting() {
        return enablePublicIdentityReporting;
    }

    /**
     * Legt den Wert der enablePublicIdentityReporting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnablePublicIdentityReporting(Boolean value) {
        this.enablePublicIdentityReporting = value;
    }

    /**
     * Ruft den Wert der secure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSecure() {
        return secure;
    }

    /**
     * Legt den Wert der secure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSecure(Boolean value) {
        this.secure = value;
    }

}
