//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The GETS session priority map entry.
 *       
 * 
 * <p>Java-Klasse für GETSSessionPriorityMapEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GETSSessionPriorityMapEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="priorityLevel" type="{}GETSPriorityLevel"/>
 *         <element name="sessionPriority" type="{}GETSSessionPriority"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GETSSessionPriorityMapEntry", propOrder = {
    "priorityLevel",
    "sessionPriority"
})
public class GETSSessionPriorityMapEntry {

    protected int priorityLevel;
    protected int sessionPriority;

    /**
     * Ruft den Wert der priorityLevel-Eigenschaft ab.
     * 
     */
    public int getPriorityLevel() {
        return priorityLevel;
    }

    /**
     * Legt den Wert der priorityLevel-Eigenschaft fest.
     * 
     */
    public void setPriorityLevel(int value) {
        this.priorityLevel = value;
    }

    /**
     * Ruft den Wert der sessionPriority-Eigenschaft ab.
     * 
     */
    public int getSessionPriority() {
        return sessionPriority;
    }

    /**
     * Legt den Wert der sessionPriority-Eigenschaft fest.
     * 
     */
    public void setSessionPriority(int value) {
        this.sessionPriority = value;
    }

}
