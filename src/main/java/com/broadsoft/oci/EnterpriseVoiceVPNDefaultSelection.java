//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EnterpriseVoiceVPNDefaultSelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="EnterpriseVoiceVPNDefaultSelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Public"/>
 *     <enumeration value="Private"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "EnterpriseVoiceVPNDefaultSelection")
@XmlEnum
public enum EnterpriseVoiceVPNDefaultSelection {

    @XmlEnumValue("Public")
    PUBLIC("Public"),
    @XmlEnumValue("Private")
    PRIVATE("Private");
    private final String value;

    EnterpriseVoiceVPNDefaultSelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnterpriseVoiceVPNDefaultSelection fromValue(String v) {
        for (EnterpriseVoiceVPNDefaultSelection c: EnterpriseVoiceVPNDefaultSelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
