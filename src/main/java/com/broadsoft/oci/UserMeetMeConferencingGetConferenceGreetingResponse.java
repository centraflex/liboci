//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserMeetMeConferencingGetConferenceGreetingRequest.
 *         Contains the information of a conference custom greeting.
 *       
 * 
 * <p>Java-Klasse für UserMeetMeConferencingGetConferenceGreetingResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserMeetMeConferencingGetConferenceGreetingResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="playEntranceGreeting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="entranceGreetingAudioFile" type="{}FileDescription" minOccurs="0"/>
 *         <element name="entranceGreetingMediaType" type="{}MediaFileType" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserMeetMeConferencingGetConferenceGreetingResponse", propOrder = {
    "playEntranceGreeting",
    "entranceGreetingAudioFile",
    "entranceGreetingMediaType"
})
public class UserMeetMeConferencingGetConferenceGreetingResponse
    extends OCIDataResponse
{

    protected boolean playEntranceGreeting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String entranceGreetingAudioFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String entranceGreetingMediaType;

    /**
     * Ruft den Wert der playEntranceGreeting-Eigenschaft ab.
     * 
     */
    public boolean isPlayEntranceGreeting() {
        return playEntranceGreeting;
    }

    /**
     * Legt den Wert der playEntranceGreeting-Eigenschaft fest.
     * 
     */
    public void setPlayEntranceGreeting(boolean value) {
        this.playEntranceGreeting = value;
    }

    /**
     * Ruft den Wert der entranceGreetingAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntranceGreetingAudioFile() {
        return entranceGreetingAudioFile;
    }

    /**
     * Legt den Wert der entranceGreetingAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntranceGreetingAudioFile(String value) {
        this.entranceGreetingAudioFile = value;
    }

    /**
     * Ruft den Wert der entranceGreetingMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntranceGreetingMediaType() {
        return entranceGreetingMediaType;
    }

    /**
     * Legt den Wert der entranceGreetingMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntranceGreetingMediaType(String value) {
        this.entranceGreetingMediaType = value;
    }

}
