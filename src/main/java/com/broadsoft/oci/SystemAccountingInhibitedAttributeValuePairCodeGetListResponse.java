//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to
 *         SystemAccountingInhibitedAttributeValuePairCodeGetListRequest. Contains a 2 column
 *         table with column headings "Attribute Value Pair Code" and "Vendor Id".
 *       
 * 
 * <p>Java-Klasse für SystemAccountingInhibitedAttributeValuePairCodeGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAccountingInhibitedAttributeValuePairCodeGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="inhibitedAttributeValuePairCodeTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAccountingInhibitedAttributeValuePairCodeGetListResponse", propOrder = {
    "inhibitedAttributeValuePairCodeTable"
})
public class SystemAccountingInhibitedAttributeValuePairCodeGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable inhibitedAttributeValuePairCodeTable;

    /**
     * Ruft den Wert der inhibitedAttributeValuePairCodeTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getInhibitedAttributeValuePairCodeTable() {
        return inhibitedAttributeValuePairCodeTable;
    }

    /**
     * Legt den Wert der inhibitedAttributeValuePairCodeTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setInhibitedAttributeValuePairCodeTable(OCITable value) {
        this.inhibitedAttributeValuePairCodeTable = value;
    }

}
