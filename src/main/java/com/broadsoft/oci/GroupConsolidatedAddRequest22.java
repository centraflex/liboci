//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a group.
 *         
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           serviceProviderExternalId
 *           groupExternalId
 *         
 *         The following elements are only used in Amplify data mode and ignored in AS and XS data mode:
 *         servicePolicy,
 *         callProcessingSliceId, 
 *         provisioningSliceId, 
 *         subscriberPartition.
 *         When the callProcessingSliceId or provisioningSliceId is not specified in the AmplifyDataMode, 
 *         the default slice Id is assigned to the Group.
 *         Only Provisioning admin and above can change the callProcessingSliceId,  
 *         provisioningSliceId, and subscriberPartition.
 *         
 *         The following elements are only used in Amplify and XS data mode and ignored in AS mode:
 *         preferredDataCenter.
 *         Only Provisioning admin and above can change the preferredDataCenter.
 *         
 *         The following elements are only used in XS data mode and ignored in Amplify and AS data mode:
 *         defaultUserCallingLineIdPhoneNumber.
 *         
 *         The following elements are optional for the group. If the elements are included,
 *         they will be either added, authorized, or modified on the group. Should any of the 
 *         following elements be rejected to due existing system or service provider settings, 
 *         the group will not be added and the response will be an ErrorResponse:
 *           domain
 *           admin
 *           minExtensionLength
 *           maxExtensionLength
 *           defaultExtensionLength
 *           servicePackAuthorization
 *           groupServiceAuthorizationAndAssignment
 *           userServiceAuthorization
 *           servicePack
 *           activatablePhoneNumber
 *           activatableDNRange
 *           routingProfile
 *           trunkGroupMaxActiveCalls
 *           trunkGroupBurstingMaxActiveCalls
 *           meetMeConferencingAllocatedPorts
 *         When a group or user service is included that is not activated, is not licensed, 
 *         or not authorized to the service provider, the response will be an ErrorResponse.
 * 
 * 		If the group service authorized quantity is not included it will default to Unlimited.																																																			
 * 
 *         If activatablePhoneNumber and activatableDNRange elements are included, when
 *         activate element is present, this value overrides the system group default 
 *         activatable settings.  
 *        If the activatablePhoneNumber and activatableDNRange element are included, the phone numbers are added to the service provider if they are available to be assigned to the group and not currently added to the service provider.
 *         The response returned is a
 * -	SuccessResponse if all the data is successfully added.
 * -	ErrorResponse if any data other than the DN validation/assignment fails.
 * -	GroupConsolidatedAddResponse22 if any of the activatablePhoneNumber or activatableDNRange fails validation.
 * 
 *       
 * 
 * <p>Java-Klasse für GroupConsolidatedAddRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupConsolidatedAddRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <choice>
 *           <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *           <element name="serviceProviderExternalId" type="{}ExternalId"/>
 *         </choice>
 *         <element name="groupId" type="{}GroupId" minOccurs="0"/>
 *         <element name="groupExternalId" type="{}ExternalId" minOccurs="0"/>
 *         <element name="defaultDomain" type="{}NetAddress"/>
 *         <element name="userLimit" type="{}GroupUserLimit"/>
 *         <element name="groupName" type="{}GroupName" minOccurs="0"/>
 *         <element name="callingLineIdName" type="{}GroupCallingLineIdName" minOccurs="0"/>
 *         <element name="timeZone" type="{}TimeZone" minOccurs="0"/>
 *         <element name="locationDialingCode" type="{}LocationDialingCode" minOccurs="0"/>
 *         <element name="contact" type="{}Contact" minOccurs="0"/>
 *         <element name="address" type="{}StreetAddress" minOccurs="0"/>
 *         <element name="servicePolicy" type="{}ServicePolicyName" minOccurs="0"/>
 *         <element name="callProcessingSliceId" type="{}SliceId" minOccurs="0"/>
 *         <element name="provisioningSliceId" type="{}SliceId" minOccurs="0"/>
 *         <element name="subscriberPartition" type="{}SubscriberPartition" minOccurs="0"/>
 *         <element name="preferredDataCenter" type="{}DataCenter" minOccurs="0"/>
 *         <element name="defaultUserCallingLineIdPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="domain" type="{}NetAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="admin" type="{}GroupAdmin" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="minExtensionLength" type="{}ExtensionLength" minOccurs="0"/>
 *         <element name="maxExtensionLength" type="{}ExtensionLength" minOccurs="0"/>
 *         <element name="defaultExtensionLength" type="{}ExtensionLength" minOccurs="0"/>
 *         <element name="groupServiceAuthorizationAndAssignment" type="{}GroupServiceAuthorizationAndAssignment" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="userServiceAuthorization" type="{}UserServiceAuthorization" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="servicePackAuthorization" type="{}ServicePackAuthorization" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="activatablePhoneNumber" type="{}ActivatableDN" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="activatableDNRange" type="{}ActivatableDNRange" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="routingProfile" type="{}RoutingProfile" minOccurs="0"/>
 *         <element name="meetMeConferencingAllocatedPorts" type="{}MeetMeConferencingConferencePorts" minOccurs="0"/>
 *         <element name="trunkGroupMaxActiveCalls" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         <element name="trunkGroupBurstingMaxActiveCalls" type="{}UnboundedNonNegativeInt" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupConsolidatedAddRequest22", propOrder = {
    "serviceProviderId",
    "serviceProviderExternalId",
    "groupId",
    "groupExternalId",
    "defaultDomain",
    "userLimit",
    "groupName",
    "callingLineIdName",
    "timeZone",
    "locationDialingCode",
    "contact",
    "address",
    "servicePolicy",
    "callProcessingSliceId",
    "provisioningSliceId",
    "subscriberPartition",
    "preferredDataCenter",
    "defaultUserCallingLineIdPhoneNumber",
    "domain",
    "admin",
    "minExtensionLength",
    "maxExtensionLength",
    "defaultExtensionLength",
    "groupServiceAuthorizationAndAssignment",
    "userServiceAuthorization",
    "servicePackAuthorization",
    "activatablePhoneNumber",
    "activatableDNRange",
    "routingProfile",
    "meetMeConferencingAllocatedPorts",
    "trunkGroupMaxActiveCalls",
    "trunkGroupBurstingMaxActiveCalls"
})
public class GroupConsolidatedAddRequest22
    extends OCIRequest
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderExternalId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupExternalId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultDomain;
    protected int userLimit;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeZone;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String locationDialingCode;
    protected Contact contact;
    protected StreetAddress address;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String servicePolicy;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callProcessingSliceId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String provisioningSliceId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String subscriberPartition;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String preferredDataCenter;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultUserCallingLineIdPhoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> domain;
    protected List<GroupAdmin> admin;
    protected Integer minExtensionLength;
    protected Integer maxExtensionLength;
    protected Integer defaultExtensionLength;
    protected List<GroupServiceAuthorizationAndAssignment> groupServiceAuthorizationAndAssignment;
    protected List<UserServiceAuthorization> userServiceAuthorization;
    protected List<ServicePackAuthorization> servicePackAuthorization;
    protected List<ActivatableDN> activatablePhoneNumber;
    protected List<ActivatableDNRange> activatableDNRange;
    @XmlElementRef(name = "routingProfile", type = JAXBElement.class, required = false)
    protected JAXBElement<String> routingProfile;
    protected MeetMeConferencingConferencePorts meetMeConferencingAllocatedPorts;
    protected Integer trunkGroupMaxActiveCalls;
    protected UnboundedNonNegativeInt trunkGroupBurstingMaxActiveCalls;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der serviceProviderExternalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderExternalId() {
        return serviceProviderExternalId;
    }

    /**
     * Legt den Wert der serviceProviderExternalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderExternalId(String value) {
        this.serviceProviderExternalId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der groupExternalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupExternalId() {
        return groupExternalId;
    }

    /**
     * Legt den Wert der groupExternalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupExternalId(String value) {
        this.groupExternalId = value;
    }

    /**
     * Ruft den Wert der defaultDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultDomain() {
        return defaultDomain;
    }

    /**
     * Legt den Wert der defaultDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultDomain(String value) {
        this.defaultDomain = value;
    }

    /**
     * Ruft den Wert der userLimit-Eigenschaft ab.
     * 
     */
    public int getUserLimit() {
        return userLimit;
    }

    /**
     * Legt den Wert der userLimit-Eigenschaft fest.
     * 
     */
    public void setUserLimit(int value) {
        this.userLimit = value;
    }

    /**
     * Ruft den Wert der groupName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Legt den Wert der groupName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupName(String value) {
        this.groupName = value;
    }

    /**
     * Ruft den Wert der callingLineIdName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdName() {
        return callingLineIdName;
    }

    /**
     * Legt den Wert der callingLineIdName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdName(String value) {
        this.callingLineIdName = value;
    }

    /**
     * Ruft den Wert der timeZone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * Legt den Wert der timeZone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeZone(String value) {
        this.timeZone = value;
    }

    /**
     * Ruft den Wert der locationDialingCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationDialingCode() {
        return locationDialingCode;
    }

    /**
     * Legt den Wert der locationDialingCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationDialingCode(String value) {
        this.locationDialingCode = value;
    }

    /**
     * Ruft den Wert der contact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Contact }
     *     
     */
    public Contact getContact() {
        return contact;
    }

    /**
     * Legt den Wert der contact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Contact }
     *     
     */
    public void setContact(Contact value) {
        this.contact = value;
    }

    /**
     * Ruft den Wert der address-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StreetAddress }
     *     
     */
    public StreetAddress getAddress() {
        return address;
    }

    /**
     * Legt den Wert der address-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetAddress }
     *     
     */
    public void setAddress(StreetAddress value) {
        this.address = value;
    }

    /**
     * Ruft den Wert der servicePolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicePolicy() {
        return servicePolicy;
    }

    /**
     * Legt den Wert der servicePolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicePolicy(String value) {
        this.servicePolicy = value;
    }

    /**
     * Ruft den Wert der callProcessingSliceId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallProcessingSliceId() {
        return callProcessingSliceId;
    }

    /**
     * Legt den Wert der callProcessingSliceId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallProcessingSliceId(String value) {
        this.callProcessingSliceId = value;
    }

    /**
     * Ruft den Wert der provisioningSliceId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvisioningSliceId() {
        return provisioningSliceId;
    }

    /**
     * Legt den Wert der provisioningSliceId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvisioningSliceId(String value) {
        this.provisioningSliceId = value;
    }

    /**
     * Ruft den Wert der subscriberPartition-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberPartition() {
        return subscriberPartition;
    }

    /**
     * Legt den Wert der subscriberPartition-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberPartition(String value) {
        this.subscriberPartition = value;
    }

    /**
     * Ruft den Wert der preferredDataCenter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredDataCenter() {
        return preferredDataCenter;
    }

    /**
     * Legt den Wert der preferredDataCenter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredDataCenter(String value) {
        this.preferredDataCenter = value;
    }

    /**
     * Ruft den Wert der defaultUserCallingLineIdPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultUserCallingLineIdPhoneNumber() {
        return defaultUserCallingLineIdPhoneNumber;
    }

    /**
     * Legt den Wert der defaultUserCallingLineIdPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultUserCallingLineIdPhoneNumber(String value) {
        this.defaultUserCallingLineIdPhoneNumber = value;
    }

    /**
     * Gets the value of the domain property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the domain property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDomain().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the domain property.
     */
    public List<String> getDomain() {
        if (domain == null) {
            domain = new ArrayList<>();
        }
        return this.domain;
    }

    /**
     * Gets the value of the admin property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the admin property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdmin().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupAdmin }
     * 
     * 
     * @return
     *     The value of the admin property.
     */
    public List<GroupAdmin> getAdmin() {
        if (admin == null) {
            admin = new ArrayList<>();
        }
        return this.admin;
    }

    /**
     * Ruft den Wert der minExtensionLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinExtensionLength() {
        return minExtensionLength;
    }

    /**
     * Legt den Wert der minExtensionLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinExtensionLength(Integer value) {
        this.minExtensionLength = value;
    }

    /**
     * Ruft den Wert der maxExtensionLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxExtensionLength() {
        return maxExtensionLength;
    }

    /**
     * Legt den Wert der maxExtensionLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxExtensionLength(Integer value) {
        this.maxExtensionLength = value;
    }

    /**
     * Ruft den Wert der defaultExtensionLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDefaultExtensionLength() {
        return defaultExtensionLength;
    }

    /**
     * Legt den Wert der defaultExtensionLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDefaultExtensionLength(Integer value) {
        this.defaultExtensionLength = value;
    }

    /**
     * Gets the value of the groupServiceAuthorizationAndAssignment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the groupServiceAuthorizationAndAssignment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupServiceAuthorizationAndAssignment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupServiceAuthorizationAndAssignment }
     * 
     * 
     * @return
     *     The value of the groupServiceAuthorizationAndAssignment property.
     */
    public List<GroupServiceAuthorizationAndAssignment> getGroupServiceAuthorizationAndAssignment() {
        if (groupServiceAuthorizationAndAssignment == null) {
            groupServiceAuthorizationAndAssignment = new ArrayList<>();
        }
        return this.groupServiceAuthorizationAndAssignment;
    }

    /**
     * Gets the value of the userServiceAuthorization property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the userServiceAuthorization property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserServiceAuthorization().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserServiceAuthorization }
     * 
     * 
     * @return
     *     The value of the userServiceAuthorization property.
     */
    public List<UserServiceAuthorization> getUserServiceAuthorization() {
        if (userServiceAuthorization == null) {
            userServiceAuthorization = new ArrayList<>();
        }
        return this.userServiceAuthorization;
    }

    /**
     * Gets the value of the servicePackAuthorization property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the servicePackAuthorization property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicePackAuthorization().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServicePackAuthorization }
     * 
     * 
     * @return
     *     The value of the servicePackAuthorization property.
     */
    public List<ServicePackAuthorization> getServicePackAuthorization() {
        if (servicePackAuthorization == null) {
            servicePackAuthorization = new ArrayList<>();
        }
        return this.servicePackAuthorization;
    }

    /**
     * Gets the value of the activatablePhoneNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the activatablePhoneNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActivatablePhoneNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivatableDN }
     * 
     * 
     * @return
     *     The value of the activatablePhoneNumber property.
     */
    public List<ActivatableDN> getActivatablePhoneNumber() {
        if (activatablePhoneNumber == null) {
            activatablePhoneNumber = new ArrayList<>();
        }
        return this.activatablePhoneNumber;
    }

    /**
     * Gets the value of the activatableDNRange property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the activatableDNRange property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActivatableDNRange().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivatableDNRange }
     * 
     * 
     * @return
     *     The value of the activatableDNRange property.
     */
    public List<ActivatableDNRange> getActivatableDNRange() {
        if (activatableDNRange == null) {
            activatableDNRange = new ArrayList<>();
        }
        return this.activatableDNRange;
    }

    /**
     * Ruft den Wert der routingProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRoutingProfile() {
        return routingProfile;
    }

    /**
     * Legt den Wert der routingProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRoutingProfile(JAXBElement<String> value) {
        this.routingProfile = value;
    }

    /**
     * Ruft den Wert der meetMeConferencingAllocatedPorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferencePorts }
     *     
     */
    public MeetMeConferencingConferencePorts getMeetMeConferencingAllocatedPorts() {
        return meetMeConferencingAllocatedPorts;
    }

    /**
     * Legt den Wert der meetMeConferencingAllocatedPorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferencePorts }
     *     
     */
    public void setMeetMeConferencingAllocatedPorts(MeetMeConferencingConferencePorts value) {
        this.meetMeConferencingAllocatedPorts = value;
    }

    /**
     * Ruft den Wert der trunkGroupMaxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTrunkGroupMaxActiveCalls() {
        return trunkGroupMaxActiveCalls;
    }

    /**
     * Legt den Wert der trunkGroupMaxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTrunkGroupMaxActiveCalls(Integer value) {
        this.trunkGroupMaxActiveCalls = value;
    }

    /**
     * Ruft den Wert der trunkGroupBurstingMaxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public UnboundedNonNegativeInt getTrunkGroupBurstingMaxActiveCalls() {
        return trunkGroupBurstingMaxActiveCalls;
    }

    /**
     * Legt den Wert der trunkGroupBurstingMaxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public void setTrunkGroupBurstingMaxActiveCalls(UnboundedNonNegativeInt value) {
        this.trunkGroupBurstingMaxActiveCalls = value;
    }

}
