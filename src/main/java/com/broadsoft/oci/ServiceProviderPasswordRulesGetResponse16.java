//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to ServiceProviderPasswordRulesGetRequest16.
 *         Contains the group, department administrator and/or user password
 *         rules setting.
 *         
 *         Replaced by: ServiceProviderPasswordRulesGetResponse22 in AS data mode.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderPasswordRulesGetResponse16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderPasswordRulesGetResponse16">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="rulesApplyTo" type="{}ServiceProviderPasswordRulesApplyTo"/>
 *         <element name="allowWebAddExternalAuthenticationUsers" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disallowUserId" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disallowOldPassword" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disallowReversedOldPassword" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="restrictMinDigits" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minDigits" type="{}PasswordMinDigits"/>
 *         <element name="restrictMinUpperCaseLetters" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minUpperCaseLetters" type="{}PasswordMinUpperCaseLetters"/>
 *         <element name="restrictMinLowerCaseLetters" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minLowerCaseLetters" type="{}PasswordMinLowerCaseLetters"/>
 *         <element name="restrictMinNonAlphanumericCharacters" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minNonAlphanumericCharacters" type="{}PasswordMinNonAlphanumericCharacters"/>
 *         <element name="minLength" type="{}PasswordMinLength"/>
 *         <element name="maxFailedLoginAttempts" type="{}MaxFailedLoginAttempts"/>
 *         <element name="passwordExpiresDays" type="{}PasswordExpiresDays"/>
 *         <element name="sendLoginDisabledNotifyEmail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="loginDisabledNotifyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="disallowRulesModification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disallowPreviousPasswords" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="numberOfPreviousPasswords" type="{}PasswordHistoryCount"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderPasswordRulesGetResponse16", propOrder = {
    "rulesApplyTo",
    "allowWebAddExternalAuthenticationUsers",
    "disallowUserId",
    "disallowOldPassword",
    "disallowReversedOldPassword",
    "restrictMinDigits",
    "minDigits",
    "restrictMinUpperCaseLetters",
    "minUpperCaseLetters",
    "restrictMinLowerCaseLetters",
    "minLowerCaseLetters",
    "restrictMinNonAlphanumericCharacters",
    "minNonAlphanumericCharacters",
    "minLength",
    "maxFailedLoginAttempts",
    "passwordExpiresDays",
    "sendLoginDisabledNotifyEmail",
    "loginDisabledNotifyEmailAddress",
    "disallowRulesModification",
    "disallowPreviousPasswords",
    "numberOfPreviousPasswords"
})
public class ServiceProviderPasswordRulesGetResponse16
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderPasswordRulesApplyTo rulesApplyTo;
    protected boolean allowWebAddExternalAuthenticationUsers;
    protected boolean disallowUserId;
    protected boolean disallowOldPassword;
    protected boolean disallowReversedOldPassword;
    protected boolean restrictMinDigits;
    protected int minDigits;
    protected boolean restrictMinUpperCaseLetters;
    protected int minUpperCaseLetters;
    protected boolean restrictMinLowerCaseLetters;
    protected int minLowerCaseLetters;
    protected boolean restrictMinNonAlphanumericCharacters;
    protected int minNonAlphanumericCharacters;
    protected int minLength;
    protected int maxFailedLoginAttempts;
    protected int passwordExpiresDays;
    protected boolean sendLoginDisabledNotifyEmail;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String loginDisabledNotifyEmailAddress;
    protected boolean disallowRulesModification;
    protected boolean disallowPreviousPasswords;
    protected int numberOfPreviousPasswords;

    /**
     * Ruft den Wert der rulesApplyTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderPasswordRulesApplyTo }
     *     
     */
    public ServiceProviderPasswordRulesApplyTo getRulesApplyTo() {
        return rulesApplyTo;
    }

    /**
     * Legt den Wert der rulesApplyTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderPasswordRulesApplyTo }
     *     
     */
    public void setRulesApplyTo(ServiceProviderPasswordRulesApplyTo value) {
        this.rulesApplyTo = value;
    }

    /**
     * Ruft den Wert der allowWebAddExternalAuthenticationUsers-Eigenschaft ab.
     * 
     */
    public boolean isAllowWebAddExternalAuthenticationUsers() {
        return allowWebAddExternalAuthenticationUsers;
    }

    /**
     * Legt den Wert der allowWebAddExternalAuthenticationUsers-Eigenschaft fest.
     * 
     */
    public void setAllowWebAddExternalAuthenticationUsers(boolean value) {
        this.allowWebAddExternalAuthenticationUsers = value;
    }

    /**
     * Ruft den Wert der disallowUserId-Eigenschaft ab.
     * 
     */
    public boolean isDisallowUserId() {
        return disallowUserId;
    }

    /**
     * Legt den Wert der disallowUserId-Eigenschaft fest.
     * 
     */
    public void setDisallowUserId(boolean value) {
        this.disallowUserId = value;
    }

    /**
     * Ruft den Wert der disallowOldPassword-Eigenschaft ab.
     * 
     */
    public boolean isDisallowOldPassword() {
        return disallowOldPassword;
    }

    /**
     * Legt den Wert der disallowOldPassword-Eigenschaft fest.
     * 
     */
    public void setDisallowOldPassword(boolean value) {
        this.disallowOldPassword = value;
    }

    /**
     * Ruft den Wert der disallowReversedOldPassword-Eigenschaft ab.
     * 
     */
    public boolean isDisallowReversedOldPassword() {
        return disallowReversedOldPassword;
    }

    /**
     * Legt den Wert der disallowReversedOldPassword-Eigenschaft fest.
     * 
     */
    public void setDisallowReversedOldPassword(boolean value) {
        this.disallowReversedOldPassword = value;
    }

    /**
     * Ruft den Wert der restrictMinDigits-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinDigits() {
        return restrictMinDigits;
    }

    /**
     * Legt den Wert der restrictMinDigits-Eigenschaft fest.
     * 
     */
    public void setRestrictMinDigits(boolean value) {
        this.restrictMinDigits = value;
    }

    /**
     * Ruft den Wert der minDigits-Eigenschaft ab.
     * 
     */
    public int getMinDigits() {
        return minDigits;
    }

    /**
     * Legt den Wert der minDigits-Eigenschaft fest.
     * 
     */
    public void setMinDigits(int value) {
        this.minDigits = value;
    }

    /**
     * Ruft den Wert der restrictMinUpperCaseLetters-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinUpperCaseLetters() {
        return restrictMinUpperCaseLetters;
    }

    /**
     * Legt den Wert der restrictMinUpperCaseLetters-Eigenschaft fest.
     * 
     */
    public void setRestrictMinUpperCaseLetters(boolean value) {
        this.restrictMinUpperCaseLetters = value;
    }

    /**
     * Ruft den Wert der minUpperCaseLetters-Eigenschaft ab.
     * 
     */
    public int getMinUpperCaseLetters() {
        return minUpperCaseLetters;
    }

    /**
     * Legt den Wert der minUpperCaseLetters-Eigenschaft fest.
     * 
     */
    public void setMinUpperCaseLetters(int value) {
        this.minUpperCaseLetters = value;
    }

    /**
     * Ruft den Wert der restrictMinLowerCaseLetters-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinLowerCaseLetters() {
        return restrictMinLowerCaseLetters;
    }

    /**
     * Legt den Wert der restrictMinLowerCaseLetters-Eigenschaft fest.
     * 
     */
    public void setRestrictMinLowerCaseLetters(boolean value) {
        this.restrictMinLowerCaseLetters = value;
    }

    /**
     * Ruft den Wert der minLowerCaseLetters-Eigenschaft ab.
     * 
     */
    public int getMinLowerCaseLetters() {
        return minLowerCaseLetters;
    }

    /**
     * Legt den Wert der minLowerCaseLetters-Eigenschaft fest.
     * 
     */
    public void setMinLowerCaseLetters(int value) {
        this.minLowerCaseLetters = value;
    }

    /**
     * Ruft den Wert der restrictMinNonAlphanumericCharacters-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinNonAlphanumericCharacters() {
        return restrictMinNonAlphanumericCharacters;
    }

    /**
     * Legt den Wert der restrictMinNonAlphanumericCharacters-Eigenschaft fest.
     * 
     */
    public void setRestrictMinNonAlphanumericCharacters(boolean value) {
        this.restrictMinNonAlphanumericCharacters = value;
    }

    /**
     * Ruft den Wert der minNonAlphanumericCharacters-Eigenschaft ab.
     * 
     */
    public int getMinNonAlphanumericCharacters() {
        return minNonAlphanumericCharacters;
    }

    /**
     * Legt den Wert der minNonAlphanumericCharacters-Eigenschaft fest.
     * 
     */
    public void setMinNonAlphanumericCharacters(int value) {
        this.minNonAlphanumericCharacters = value;
    }

    /**
     * Ruft den Wert der minLength-Eigenschaft ab.
     * 
     */
    public int getMinLength() {
        return minLength;
    }

    /**
     * Legt den Wert der minLength-Eigenschaft fest.
     * 
     */
    public void setMinLength(int value) {
        this.minLength = value;
    }

    /**
     * Ruft den Wert der maxFailedLoginAttempts-Eigenschaft ab.
     * 
     */
    public int getMaxFailedLoginAttempts() {
        return maxFailedLoginAttempts;
    }

    /**
     * Legt den Wert der maxFailedLoginAttempts-Eigenschaft fest.
     * 
     */
    public void setMaxFailedLoginAttempts(int value) {
        this.maxFailedLoginAttempts = value;
    }

    /**
     * Ruft den Wert der passwordExpiresDays-Eigenschaft ab.
     * 
     */
    public int getPasswordExpiresDays() {
        return passwordExpiresDays;
    }

    /**
     * Legt den Wert der passwordExpiresDays-Eigenschaft fest.
     * 
     */
    public void setPasswordExpiresDays(int value) {
        this.passwordExpiresDays = value;
    }

    /**
     * Ruft den Wert der sendLoginDisabledNotifyEmail-Eigenschaft ab.
     * 
     */
    public boolean isSendLoginDisabledNotifyEmail() {
        return sendLoginDisabledNotifyEmail;
    }

    /**
     * Legt den Wert der sendLoginDisabledNotifyEmail-Eigenschaft fest.
     * 
     */
    public void setSendLoginDisabledNotifyEmail(boolean value) {
        this.sendLoginDisabledNotifyEmail = value;
    }

    /**
     * Ruft den Wert der loginDisabledNotifyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoginDisabledNotifyEmailAddress() {
        return loginDisabledNotifyEmailAddress;
    }

    /**
     * Legt den Wert der loginDisabledNotifyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoginDisabledNotifyEmailAddress(String value) {
        this.loginDisabledNotifyEmailAddress = value;
    }

    /**
     * Ruft den Wert der disallowRulesModification-Eigenschaft ab.
     * 
     */
    public boolean isDisallowRulesModification() {
        return disallowRulesModification;
    }

    /**
     * Legt den Wert der disallowRulesModification-Eigenschaft fest.
     * 
     */
    public void setDisallowRulesModification(boolean value) {
        this.disallowRulesModification = value;
    }

    /**
     * Ruft den Wert der disallowPreviousPasswords-Eigenschaft ab.
     * 
     */
    public boolean isDisallowPreviousPasswords() {
        return disallowPreviousPasswords;
    }

    /**
     * Legt den Wert der disallowPreviousPasswords-Eigenschaft fest.
     * 
     */
    public void setDisallowPreviousPasswords(boolean value) {
        this.disallowPreviousPasswords = value;
    }

    /**
     * Ruft den Wert der numberOfPreviousPasswords-Eigenschaft ab.
     * 
     */
    public int getNumberOfPreviousPasswords() {
        return numberOfPreviousPasswords;
    }

    /**
     * Legt den Wert der numberOfPreviousPasswords-Eigenschaft fest.
     * 
     */
    public void setNumberOfPreviousPasswords(int value) {
        this.numberOfPreviousPasswords = value;
    }

}
