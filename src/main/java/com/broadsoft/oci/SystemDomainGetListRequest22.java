//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Requests the list of all matching system-level domains and all matching reseller level domains. 
 *         If excludeReseller is specified, returns all matching system-level domain names only. 
 *         If resellerId is specified, returns all matching system-level domain names and the given reseller's domains. 
 *         If reseller administrator sends the request and resellerId is not specified, the administrator's resellerId is used.
 *         
 *         The response is either SystemDomainGetListResponse22 or ErrorResponse.
 *         
 *         The following elements are only used in AS data mode:
 *           includeReseller
 *           resellerId
 *           
 *           Replaced by SystemDomainGetListRequest22V2 in AS data mode.
 *       
 * 
 * <p>Java-Klasse für SystemDomainGetListRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDomainGetListRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <choice>
 *         <element name="excludeReseller" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="resellerId" type="{}ResellerId" minOccurs="0"/>
 *       </choice>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDomainGetListRequest22", propOrder = {
    "excludeReseller",
    "resellerId"
})
public class SystemDomainGetListRequest22
    extends OCIRequest
{

    protected Boolean excludeReseller;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;

    /**
     * Ruft den Wert der excludeReseller-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExcludeReseller() {
        return excludeReseller;
    }

    /**
     * Legt den Wert der excludeReseller-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludeReseller(Boolean value) {
        this.excludeReseller = value;
    }

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

}
