//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupCallCenterGetInstanceRequest16.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterGetInstanceResponse16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterGetInstanceResponse16">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceReadProfile"/>
 *         <element name="type" type="{}CallCenterType"/>
 *         <element name="policy" type="{}HuntPolicy"/>
 *         <element name="enableVideo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="queueLength" type="{}CallCenterQueueLength16"/>
 *         <element name="reportingServerName" type="{}CallCenterReportingServerName" minOccurs="0"/>
 *         <element name="allowCallerToDialEscapeDigit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="escapeDigit" type="{}DtmfDigit"/>
 *         <element name="resetCallStatisticsUponEntryInQueue" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowAgentLogoff" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowCallWaitingForAgents" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowCallsToAgentsInWrapUp" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="overrideAgentWrapUpTime" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="wrapUpSeconds" type="{}CallCenterWrapUpSeconds" minOccurs="0"/>
 *         <element name="forceDeliveryOfCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forceDeliveryWaitTimeSeconds" type="{}CallCenterForceDeliveryWaitTimeSeconds" minOccurs="0"/>
 *         <element name="externalPreferredAudioCodec" type="{}AudioFileCodec"/>
 *         <element name="internalPreferredAudioCodec" type="{}AudioFileCodec"/>
 *         <element name="playRingingWhenOfferingCall" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterGetInstanceResponse16", propOrder = {
    "serviceInstanceProfile",
    "type",
    "policy",
    "enableVideo",
    "queueLength",
    "reportingServerName",
    "allowCallerToDialEscapeDigit",
    "escapeDigit",
    "resetCallStatisticsUponEntryInQueue",
    "allowAgentLogoff",
    "allowCallWaitingForAgents",
    "allowCallsToAgentsInWrapUp",
    "overrideAgentWrapUpTime",
    "wrapUpSeconds",
    "forceDeliveryOfCalls",
    "forceDeliveryWaitTimeSeconds",
    "externalPreferredAudioCodec",
    "internalPreferredAudioCodec",
    "playRingingWhenOfferingCall"
})
public class GroupCallCenterGetInstanceResponse16
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected ServiceInstanceReadProfile serviceInstanceProfile;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterType type;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected HuntPolicy policy;
    protected boolean enableVideo;
    protected int queueLength;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String reportingServerName;
    protected boolean allowCallerToDialEscapeDigit;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String escapeDigit;
    protected Boolean resetCallStatisticsUponEntryInQueue;
    protected boolean allowAgentLogoff;
    protected boolean allowCallWaitingForAgents;
    protected Boolean allowCallsToAgentsInWrapUp;
    protected Boolean overrideAgentWrapUpTime;
    protected Integer wrapUpSeconds;
    protected Boolean forceDeliveryOfCalls;
    protected Integer forceDeliveryWaitTimeSeconds;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AudioFileCodec externalPreferredAudioCodec;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AudioFileCodec internalPreferredAudioCodec;
    protected boolean playRingingWhenOfferingCall;

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceReadProfile }
     *     
     */
    public ServiceInstanceReadProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceReadProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceReadProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterType }
     *     
     */
    public CallCenterType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterType }
     *     
     */
    public void setType(CallCenterType value) {
        this.type = value;
    }

    /**
     * Ruft den Wert der policy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HuntPolicy }
     *     
     */
    public HuntPolicy getPolicy() {
        return policy;
    }

    /**
     * Legt den Wert der policy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HuntPolicy }
     *     
     */
    public void setPolicy(HuntPolicy value) {
        this.policy = value;
    }

    /**
     * Ruft den Wert der enableVideo-Eigenschaft ab.
     * 
     */
    public boolean isEnableVideo() {
        return enableVideo;
    }

    /**
     * Legt den Wert der enableVideo-Eigenschaft fest.
     * 
     */
    public void setEnableVideo(boolean value) {
        this.enableVideo = value;
    }

    /**
     * Ruft den Wert der queueLength-Eigenschaft ab.
     * 
     */
    public int getQueueLength() {
        return queueLength;
    }

    /**
     * Legt den Wert der queueLength-Eigenschaft fest.
     * 
     */
    public void setQueueLength(int value) {
        this.queueLength = value;
    }

    /**
     * Ruft den Wert der reportingServerName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingServerName() {
        return reportingServerName;
    }

    /**
     * Legt den Wert der reportingServerName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingServerName(String value) {
        this.reportingServerName = value;
    }

    /**
     * Ruft den Wert der allowCallerToDialEscapeDigit-Eigenschaft ab.
     * 
     */
    public boolean isAllowCallerToDialEscapeDigit() {
        return allowCallerToDialEscapeDigit;
    }

    /**
     * Legt den Wert der allowCallerToDialEscapeDigit-Eigenschaft fest.
     * 
     */
    public void setAllowCallerToDialEscapeDigit(boolean value) {
        this.allowCallerToDialEscapeDigit = value;
    }

    /**
     * Ruft den Wert der escapeDigit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEscapeDigit() {
        return escapeDigit;
    }

    /**
     * Legt den Wert der escapeDigit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEscapeDigit(String value) {
        this.escapeDigit = value;
    }

    /**
     * Ruft den Wert der resetCallStatisticsUponEntryInQueue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isResetCallStatisticsUponEntryInQueue() {
        return resetCallStatisticsUponEntryInQueue;
    }

    /**
     * Legt den Wert der resetCallStatisticsUponEntryInQueue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setResetCallStatisticsUponEntryInQueue(Boolean value) {
        this.resetCallStatisticsUponEntryInQueue = value;
    }

    /**
     * Ruft den Wert der allowAgentLogoff-Eigenschaft ab.
     * 
     */
    public boolean isAllowAgentLogoff() {
        return allowAgentLogoff;
    }

    /**
     * Legt den Wert der allowAgentLogoff-Eigenschaft fest.
     * 
     */
    public void setAllowAgentLogoff(boolean value) {
        this.allowAgentLogoff = value;
    }

    /**
     * Ruft den Wert der allowCallWaitingForAgents-Eigenschaft ab.
     * 
     */
    public boolean isAllowCallWaitingForAgents() {
        return allowCallWaitingForAgents;
    }

    /**
     * Legt den Wert der allowCallWaitingForAgents-Eigenschaft fest.
     * 
     */
    public void setAllowCallWaitingForAgents(boolean value) {
        this.allowCallWaitingForAgents = value;
    }

    /**
     * Ruft den Wert der allowCallsToAgentsInWrapUp-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowCallsToAgentsInWrapUp() {
        return allowCallsToAgentsInWrapUp;
    }

    /**
     * Legt den Wert der allowCallsToAgentsInWrapUp-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowCallsToAgentsInWrapUp(Boolean value) {
        this.allowCallsToAgentsInWrapUp = value;
    }

    /**
     * Ruft den Wert der overrideAgentWrapUpTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOverrideAgentWrapUpTime() {
        return overrideAgentWrapUpTime;
    }

    /**
     * Legt den Wert der overrideAgentWrapUpTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverrideAgentWrapUpTime(Boolean value) {
        this.overrideAgentWrapUpTime = value;
    }

    /**
     * Ruft den Wert der wrapUpSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWrapUpSeconds() {
        return wrapUpSeconds;
    }

    /**
     * Legt den Wert der wrapUpSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWrapUpSeconds(Integer value) {
        this.wrapUpSeconds = value;
    }

    /**
     * Ruft den Wert der forceDeliveryOfCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceDeliveryOfCalls() {
        return forceDeliveryOfCalls;
    }

    /**
     * Legt den Wert der forceDeliveryOfCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceDeliveryOfCalls(Boolean value) {
        this.forceDeliveryOfCalls = value;
    }

    /**
     * Ruft den Wert der forceDeliveryWaitTimeSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getForceDeliveryWaitTimeSeconds() {
        return forceDeliveryWaitTimeSeconds;
    }

    /**
     * Legt den Wert der forceDeliveryWaitTimeSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setForceDeliveryWaitTimeSeconds(Integer value) {
        this.forceDeliveryWaitTimeSeconds = value;
    }

    /**
     * Ruft den Wert der externalPreferredAudioCodec-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AudioFileCodec }
     *     
     */
    public AudioFileCodec getExternalPreferredAudioCodec() {
        return externalPreferredAudioCodec;
    }

    /**
     * Legt den Wert der externalPreferredAudioCodec-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AudioFileCodec }
     *     
     */
    public void setExternalPreferredAudioCodec(AudioFileCodec value) {
        this.externalPreferredAudioCodec = value;
    }

    /**
     * Ruft den Wert der internalPreferredAudioCodec-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AudioFileCodec }
     *     
     */
    public AudioFileCodec getInternalPreferredAudioCodec() {
        return internalPreferredAudioCodec;
    }

    /**
     * Legt den Wert der internalPreferredAudioCodec-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AudioFileCodec }
     *     
     */
    public void setInternalPreferredAudioCodec(AudioFileCodec value) {
        this.internalPreferredAudioCodec = value;
    }

    /**
     * Ruft den Wert der playRingingWhenOfferingCall-Eigenschaft ab.
     * 
     */
    public boolean isPlayRingingWhenOfferingCall() {
        return playRingingWhenOfferingCall;
    }

    /**
     * Legt den Wert der playRingingWhenOfferingCall-Eigenschaft fest.
     * 
     */
    public void setPlayRingingWhenOfferingCall(boolean value) {
        this.playRingingWhenOfferingCall = value;
    }

}
