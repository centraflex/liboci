//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Clear a user's call logs associated with Basic Call Logs and Enhanced
 *         Call Logs features. The calls logs are deleted from both Basic Call Logs 
 *         and Enhanced Call Logs if both features are assigned.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserCallLogsClearRequest14sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallLogsClearRequest14sp4">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <choice>
 *           <element name="deleteAllCallLogs" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *           <element name="deleteSpecifiedCallLogs">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="placedCallLogId" type="{}CallLogId17sp4" maxOccurs="unbounded" minOccurs="0"/>
 *                     <element name="receivedCallLogId" type="{}CallLogId17sp4" maxOccurs="unbounded" minOccurs="0"/>
 *                     <element name="missedCallLogId" type="{}CallLogId17sp4" maxOccurs="unbounded" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallLogsClearRequest14sp4", propOrder = {
    "userId",
    "deleteAllCallLogs",
    "deleteSpecifiedCallLogs"
})
public class UserCallLogsClearRequest14Sp4
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean deleteAllCallLogs;
    protected UserCallLogsClearRequest14Sp4 .DeleteSpecifiedCallLogs deleteSpecifiedCallLogs;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der deleteAllCallLogs-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeleteAllCallLogs() {
        return deleteAllCallLogs;
    }

    /**
     * Legt den Wert der deleteAllCallLogs-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeleteAllCallLogs(Boolean value) {
        this.deleteAllCallLogs = value;
    }

    /**
     * Ruft den Wert der deleteSpecifiedCallLogs-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserCallLogsClearRequest14Sp4 .DeleteSpecifiedCallLogs }
     *     
     */
    public UserCallLogsClearRequest14Sp4 .DeleteSpecifiedCallLogs getDeleteSpecifiedCallLogs() {
        return deleteSpecifiedCallLogs;
    }

    /**
     * Legt den Wert der deleteSpecifiedCallLogs-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserCallLogsClearRequest14Sp4 .DeleteSpecifiedCallLogs }
     *     
     */
    public void setDeleteSpecifiedCallLogs(UserCallLogsClearRequest14Sp4 .DeleteSpecifiedCallLogs value) {
        this.deleteSpecifiedCallLogs = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="placedCallLogId" type="{}CallLogId17sp4" maxOccurs="unbounded" minOccurs="0"/>
     *         <element name="receivedCallLogId" type="{}CallLogId17sp4" maxOccurs="unbounded" minOccurs="0"/>
     *         <element name="missedCallLogId" type="{}CallLogId17sp4" maxOccurs="unbounded" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "placedCallLogId",
        "receivedCallLogId",
        "missedCallLogId"
    })
    public static class DeleteSpecifiedCallLogs {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> placedCallLogId;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> receivedCallLogId;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> missedCallLogId;

        /**
         * Gets the value of the placedCallLogId property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the placedCallLogId property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPlacedCallLogId().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the placedCallLogId property.
         */
        public List<String> getPlacedCallLogId() {
            if (placedCallLogId == null) {
                placedCallLogId = new ArrayList<>();
            }
            return this.placedCallLogId;
        }

        /**
         * Gets the value of the receivedCallLogId property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the receivedCallLogId property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getReceivedCallLogId().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the receivedCallLogId property.
         */
        public List<String> getReceivedCallLogId() {
            if (receivedCallLogId == null) {
                receivedCallLogId = new ArrayList<>();
            }
            return this.receivedCallLogId;
        }

        /**
         * Gets the value of the missedCallLogId property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the missedCallLogId property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMissedCallLogId().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the missedCallLogId property.
         */
        public List<String> getMissedCallLogId() {
            if (missedCallLogId == null) {
                missedCallLogId = new ArrayList<>();
            }
            return this.missedCallLogId;
        }

    }

}
