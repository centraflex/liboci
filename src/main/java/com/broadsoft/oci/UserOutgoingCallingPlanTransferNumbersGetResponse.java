//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserOutgoingCallingPlanTransferNumbersGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserOutgoingCallingPlanTransferNumbersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserOutgoingCallingPlanTransferNumbersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useCustomSettings" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="userNumbers" type="{}OutgoingCallingPlanTransferNumbers"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserOutgoingCallingPlanTransferNumbersGetResponse", propOrder = {
    "useCustomSettings",
    "userNumbers"
})
public class UserOutgoingCallingPlanTransferNumbersGetResponse
    extends OCIDataResponse
{

    protected boolean useCustomSettings;
    @XmlElement(required = true)
    protected OutgoingCallingPlanTransferNumbers userNumbers;

    /**
     * Ruft den Wert der useCustomSettings-Eigenschaft ab.
     * 
     */
    public boolean isUseCustomSettings() {
        return useCustomSettings;
    }

    /**
     * Legt den Wert der useCustomSettings-Eigenschaft fest.
     * 
     */
    public void setUseCustomSettings(boolean value) {
        this.useCustomSettings = value;
    }

    /**
     * Ruft den Wert der userNumbers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanTransferNumbers }
     *     
     */
    public OutgoingCallingPlanTransferNumbers getUserNumbers() {
        return userNumbers;
    }

    /**
     * Legt den Wert der userNumbers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanTransferNumbers }
     *     
     */
    public void setUserNumbers(OutgoingCallingPlanTransferNumbers value) {
        this.userNumbers = value;
    }

}
