//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderAdviceOfChargeGetRequest.
 *         Contains a list of Advice of Charge service provider parameters.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderAdviceOfChargeGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderAdviceOfChargeGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useSPLevelAoCSettings" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="delayBetweenNotificationSeconds" type="{}AdviceOfChargeDelayBetweenNotificationSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderAdviceOfChargeGetResponse", propOrder = {
    "useSPLevelAoCSettings",
    "delayBetweenNotificationSeconds"
})
public class ServiceProviderAdviceOfChargeGetResponse
    extends OCIDataResponse
{

    protected boolean useSPLevelAoCSettings;
    protected int delayBetweenNotificationSeconds;

    /**
     * Ruft den Wert der useSPLevelAoCSettings-Eigenschaft ab.
     * 
     */
    public boolean isUseSPLevelAoCSettings() {
        return useSPLevelAoCSettings;
    }

    /**
     * Legt den Wert der useSPLevelAoCSettings-Eigenschaft fest.
     * 
     */
    public void setUseSPLevelAoCSettings(boolean value) {
        this.useSPLevelAoCSettings = value;
    }

    /**
     * Ruft den Wert der delayBetweenNotificationSeconds-Eigenschaft ab.
     * 
     */
    public int getDelayBetweenNotificationSeconds() {
        return delayBetweenNotificationSeconds;
    }

    /**
     * Legt den Wert der delayBetweenNotificationSeconds-Eigenschaft fest.
     * 
     */
    public void setDelayBetweenNotificationSeconds(int value) {
        this.delayBetweenNotificationSeconds = value;
    }

}
