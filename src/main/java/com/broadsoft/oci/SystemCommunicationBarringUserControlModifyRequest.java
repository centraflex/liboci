//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modifies the system's Communication Barring User-Control settings.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCommunicationBarringUserControlModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCommunicationBarringUserControlModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enableLockout" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxNumberOfFailedAttempts" type="{}CommunicationBarringUserControlNumberOfAttempts" minOccurs="0"/>
 *         <element name="lockoutMinutes" type="{}CommunicationBarringUserControlLockoutMinutes" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCommunicationBarringUserControlModifyRequest", propOrder = {
    "enableLockout",
    "maxNumberOfFailedAttempts",
    "lockoutMinutes"
})
public class SystemCommunicationBarringUserControlModifyRequest
    extends OCIRequest
{

    protected Boolean enableLockout;
    protected Integer maxNumberOfFailedAttempts;
    protected Integer lockoutMinutes;

    /**
     * Ruft den Wert der enableLockout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableLockout() {
        return enableLockout;
    }

    /**
     * Legt den Wert der enableLockout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableLockout(Boolean value) {
        this.enableLockout = value;
    }

    /**
     * Ruft den Wert der maxNumberOfFailedAttempts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxNumberOfFailedAttempts() {
        return maxNumberOfFailedAttempts;
    }

    /**
     * Legt den Wert der maxNumberOfFailedAttempts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxNumberOfFailedAttempts(Integer value) {
        this.maxNumberOfFailedAttempts = value;
    }

    /**
     * Ruft den Wert der lockoutMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLockoutMinutes() {
        return lockoutMinutes;
    }

    /**
     * Legt den Wert der lockoutMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLockoutMinutes(Integer value) {
        this.lockoutMinutes = value;
    }

}
