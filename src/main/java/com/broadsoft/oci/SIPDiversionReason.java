//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SIPDiversionReason.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SIPDiversionReason">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="unknown"/>
 *     <enumeration value="user-busy"/>
 *     <enumeration value="no-answer"/>
 *     <enumeration value="unavailable"/>
 *     <enumeration value="unconditional"/>
 *     <enumeration value="deflection"/>
 *     <enumeration value="time-of-day"/>
 *     <enumeration value="do-not-disturb"/>
 *     <enumeration value="follow-me"/>
 *     <enumeration value="out-of-service"/>
 *     <enumeration value="away"/>
 *     <enumeration value="transfer"/>
 *     <enumeration value="voicemail"/>
 *     <enumeration value="hunt-group"/>
 *     <enumeration value="call-center"/>
 *     <enumeration value="route-point"/>
 *     <enumeration value="BW-ImplicitID"/>
 *     <enumeration value="BW-ExplicitID"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SIPDiversionReason")
@XmlEnum
public enum SIPDiversionReason {

    @XmlEnumValue("unknown")
    UNKNOWN("unknown"),
    @XmlEnumValue("user-busy")
    USER_BUSY("user-busy"),
    @XmlEnumValue("no-answer")
    NO_ANSWER("no-answer"),
    @XmlEnumValue("unavailable")
    UNAVAILABLE("unavailable"),
    @XmlEnumValue("unconditional")
    UNCONDITIONAL("unconditional"),
    @XmlEnumValue("deflection")
    DEFLECTION("deflection"),
    @XmlEnumValue("time-of-day")
    TIME_OF_DAY("time-of-day"),
    @XmlEnumValue("do-not-disturb")
    DO_NOT_DISTURB("do-not-disturb"),
    @XmlEnumValue("follow-me")
    FOLLOW_ME("follow-me"),
    @XmlEnumValue("out-of-service")
    OUT_OF_SERVICE("out-of-service"),
    @XmlEnumValue("away")
    AWAY("away"),
    @XmlEnumValue("transfer")
    TRANSFER("transfer"),
    @XmlEnumValue("voicemail")
    VOICEMAIL("voicemail"),
    @XmlEnumValue("hunt-group")
    HUNT_GROUP("hunt-group"),
    @XmlEnumValue("call-center")
    CALL_CENTER("call-center"),
    @XmlEnumValue("route-point")
    ROUTE_POINT("route-point"),
    @XmlEnumValue("BW-ImplicitID")
    BW_IMPLICIT_ID("BW-ImplicitID"),
    @XmlEnumValue("BW-ExplicitID")
    BW_EXPLICIT_ID("BW-ExplicitID");
    private final String value;

    SIPDiversionReason(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SIPDiversionReason fromValue(String v) {
        for (SIPDiversionReason c: SIPDiversionReason.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
