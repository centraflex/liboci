//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to add a group access device.
 *         The response is either SuccessResponse or ErrorResponse.
 * 
 *         The following elements are only used in AS data mode and ignored in XS data mode: 
 *           groupExternalId
 *           deviceExternalId
 *       
 * 
 * <p>Java-Klasse für GroupAccessDeviceAddRequest22V2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAccessDeviceAddRequest22V2">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <choice>
 *           <sequence>
 *             <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *             <element name="groupId" type="{}GroupId"/>
 *             <element name="deviceName" type="{}AccessDeviceName"/>
 *           </sequence>
 *           <sequence>
 *             <element name="groupExternalId" type="{}ExternalId"/>
 *             <element name="deviceExternalId" type="{}ExternalId"/>
 *           </sequence>
 *         </choice>
 *         <element name="deviceType" type="{}AccessDeviceType"/>
 *         <element name="protocol" type="{}AccessDeviceProtocol" minOccurs="0"/>
 *         <element name="netAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="port" type="{}Port1025" minOccurs="0"/>
 *         <element name="outboundProxyServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="stunServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="macAddress" type="{}AccessDeviceMACAddress" minOccurs="0"/>
 *         <element name="serialNumber" type="{}AccessDeviceSerialNumber" minOccurs="0"/>
 *         <element name="description" type="{}AccessDeviceDescription" minOccurs="0"/>
 *         <element name="physicalLocation" type="{}AccessDevicePhysicalLocation" minOccurs="0"/>
 *         <element name="transportProtocol" type="{}ExtendedTransportProtocol" minOccurs="0"/>
 *         <element name="useCustomUserNamePassword" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="accessDeviceCredentials" type="{}DeviceManagementUserNamePassword16" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAccessDeviceAddRequest22V2", propOrder = {
    "serviceProviderId",
    "groupId",
    "deviceName",
    "groupExternalId",
    "deviceExternalId",
    "deviceType",
    "protocol",
    "netAddress",
    "port",
    "outboundProxyServerNetAddress",
    "stunServerNetAddress",
    "macAddress",
    "serialNumber",
    "description",
    "physicalLocation",
    "transportProtocol",
    "useCustomUserNamePassword",
    "accessDeviceCredentials"
})
public class GroupAccessDeviceAddRequest22V2
    extends OCIRequest
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupExternalId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceExternalId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String protocol;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String netAddress;
    protected Integer port;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String outboundProxyServerNetAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String stunServerNetAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String macAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serialNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String physicalLocation;
    @XmlSchemaType(name = "token")
    protected ExtendedTransportProtocol transportProtocol;
    protected Boolean useCustomUserNamePassword;
    protected DeviceManagementUserNamePassword16 accessDeviceCredentials;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der deviceName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * Legt den Wert der deviceName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceName(String value) {
        this.deviceName = value;
    }

    /**
     * Ruft den Wert der groupExternalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupExternalId() {
        return groupExternalId;
    }

    /**
     * Legt den Wert der groupExternalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupExternalId(String value) {
        this.groupExternalId = value;
    }

    /**
     * Ruft den Wert der deviceExternalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceExternalId() {
        return deviceExternalId;
    }

    /**
     * Legt den Wert der deviceExternalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceExternalId(String value) {
        this.deviceExternalId = value;
    }

    /**
     * Ruft den Wert der deviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Legt den Wert der deviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceType(String value) {
        this.deviceType = value;
    }

    /**
     * Ruft den Wert der protocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocol() {
        return protocol;
    }

    /**
     * Legt den Wert der protocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocol(String value) {
        this.protocol = value;
    }

    /**
     * Ruft den Wert der netAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetAddress() {
        return netAddress;
    }

    /**
     * Legt den Wert der netAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetAddress(String value) {
        this.netAddress = value;
    }

    /**
     * Ruft den Wert der port-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPort() {
        return port;
    }

    /**
     * Legt den Wert der port-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPort(Integer value) {
        this.port = value;
    }

    /**
     * Ruft den Wert der outboundProxyServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutboundProxyServerNetAddress() {
        return outboundProxyServerNetAddress;
    }

    /**
     * Legt den Wert der outboundProxyServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutboundProxyServerNetAddress(String value) {
        this.outboundProxyServerNetAddress = value;
    }

    /**
     * Ruft den Wert der stunServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStunServerNetAddress() {
        return stunServerNetAddress;
    }

    /**
     * Legt den Wert der stunServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStunServerNetAddress(String value) {
        this.stunServerNetAddress = value;
    }

    /**
     * Ruft den Wert der macAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacAddress() {
        return macAddress;
    }

    /**
     * Legt den Wert der macAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacAddress(String value) {
        this.macAddress = value;
    }

    /**
     * Ruft den Wert der serialNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Legt den Wert der serialNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNumber(String value) {
        this.serialNumber = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der physicalLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhysicalLocation() {
        return physicalLocation;
    }

    /**
     * Legt den Wert der physicalLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhysicalLocation(String value) {
        this.physicalLocation = value;
    }

    /**
     * Ruft den Wert der transportProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedTransportProtocol }
     *     
     */
    public ExtendedTransportProtocol getTransportProtocol() {
        return transportProtocol;
    }

    /**
     * Legt den Wert der transportProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedTransportProtocol }
     *     
     */
    public void setTransportProtocol(ExtendedTransportProtocol value) {
        this.transportProtocol = value;
    }

    /**
     * Ruft den Wert der useCustomUserNamePassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseCustomUserNamePassword() {
        return useCustomUserNamePassword;
    }

    /**
     * Legt den Wert der useCustomUserNamePassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseCustomUserNamePassword(Boolean value) {
        this.useCustomUserNamePassword = value;
    }

    /**
     * Ruft den Wert der accessDeviceCredentials-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementUserNamePassword16 }
     *     
     */
    public DeviceManagementUserNamePassword16 getAccessDeviceCredentials() {
        return accessDeviceCredentials;
    }

    /**
     * Legt den Wert der accessDeviceCredentials-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementUserNamePassword16 }
     *     
     */
    public void setAccessDeviceCredentials(DeviceManagementUserNamePassword16 value) {
        this.accessDeviceCredentials = value;
    }

}
