//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupOutgoingCallingPlanCallMeNowGetListRequest.
 *       
 * 
 * <p>Java-Klasse für GroupOutgoingCallingPlanCallMeNowGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupOutgoingCallingPlanCallMeNowGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="groupPermissions" type="{}OutgoingCallingPlanCallMeNowPermissions"/>
 *         <element name="departmentPermissions" type="{}OutgoingCallingPlanCallMeNowDepartmentPermissions" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupOutgoingCallingPlanCallMeNowGetListResponse", propOrder = {
    "groupPermissions",
    "departmentPermissions"
})
public class GroupOutgoingCallingPlanCallMeNowGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OutgoingCallingPlanCallMeNowPermissions groupPermissions;
    protected List<OutgoingCallingPlanCallMeNowDepartmentPermissions> departmentPermissions;

    /**
     * Ruft den Wert der groupPermissions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanCallMeNowPermissions }
     *     
     */
    public OutgoingCallingPlanCallMeNowPermissions getGroupPermissions() {
        return groupPermissions;
    }

    /**
     * Legt den Wert der groupPermissions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanCallMeNowPermissions }
     *     
     */
    public void setGroupPermissions(OutgoingCallingPlanCallMeNowPermissions value) {
        this.groupPermissions = value;
    }

    /**
     * Gets the value of the departmentPermissions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the departmentPermissions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDepartmentPermissions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OutgoingCallingPlanCallMeNowDepartmentPermissions }
     * 
     * 
     * @return
     *     The value of the departmentPermissions property.
     */
    public List<OutgoingCallingPlanCallMeNowDepartmentPermissions> getDepartmentPermissions() {
        if (departmentPermissions == null) {
            departmentPermissions = new ArrayList<>();
        }
        return this.departmentPermissions;
    }

}
