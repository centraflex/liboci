//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für StirShakenSigningPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="StirShakenSigningPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="All Eligible Calls"/>
 *     <enumeration value="Eligible Inter-Network Calls"/>
 *     <enumeration value="Off"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "StirShakenSigningPolicy")
@XmlEnum
public enum StirShakenSigningPolicy {

    @XmlEnumValue("All Eligible Calls")
    ALL_ELIGIBLE_CALLS("All Eligible Calls"),
    @XmlEnumValue("Eligible Inter-Network Calls")
    ELIGIBLE_INTER_NETWORK_CALLS("Eligible Inter-Network Calls"),
    @XmlEnumValue("Off")
    OFF("Off");
    private final String value;

    StirShakenSigningPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StirShakenSigningPolicy fromValue(String v) {
        for (StirShakenSigningPolicy c: StirShakenSigningPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
