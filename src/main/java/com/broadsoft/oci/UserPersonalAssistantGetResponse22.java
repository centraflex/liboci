//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the UserPersonalAssistantGetRequest22.
 *         The response contains the user Personal Assistant information".
 *       
 * 
 * <p>Java-Klasse für UserPersonalAssistantGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPersonalAssistantGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="presence" type="{}PersonalAssistantPresence"/>
 *         <element name="enableTransferToAttendant" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="attendantNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="enableRingSplash" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableExpirationTime" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="expirationTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         <element name="alertMeFirst" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="alertMeFirstNumberOfRings" type="{}PersonalAssistantAlertMeFirstNumberOfRings"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPersonalAssistantGetResponse22", propOrder = {
    "presence",
    "enableTransferToAttendant",
    "attendantNumber",
    "enableRingSplash",
    "enableExpirationTime",
    "expirationTime",
    "alertMeFirst",
    "alertMeFirstNumberOfRings"
})
public class UserPersonalAssistantGetResponse22
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected PersonalAssistantPresence presence;
    protected boolean enableTransferToAttendant;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String attendantNumber;
    protected boolean enableRingSplash;
    protected boolean enableExpirationTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationTime;
    protected boolean alertMeFirst;
    protected int alertMeFirstNumberOfRings;

    /**
     * Ruft den Wert der presence-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PersonalAssistantPresence }
     *     
     */
    public PersonalAssistantPresence getPresence() {
        return presence;
    }

    /**
     * Legt den Wert der presence-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalAssistantPresence }
     *     
     */
    public void setPresence(PersonalAssistantPresence value) {
        this.presence = value;
    }

    /**
     * Ruft den Wert der enableTransferToAttendant-Eigenschaft ab.
     * 
     */
    public boolean isEnableTransferToAttendant() {
        return enableTransferToAttendant;
    }

    /**
     * Legt den Wert der enableTransferToAttendant-Eigenschaft fest.
     * 
     */
    public void setEnableTransferToAttendant(boolean value) {
        this.enableTransferToAttendant = value;
    }

    /**
     * Ruft den Wert der attendantNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttendantNumber() {
        return attendantNumber;
    }

    /**
     * Legt den Wert der attendantNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttendantNumber(String value) {
        this.attendantNumber = value;
    }

    /**
     * Ruft den Wert der enableRingSplash-Eigenschaft ab.
     * 
     */
    public boolean isEnableRingSplash() {
        return enableRingSplash;
    }

    /**
     * Legt den Wert der enableRingSplash-Eigenschaft fest.
     * 
     */
    public void setEnableRingSplash(boolean value) {
        this.enableRingSplash = value;
    }

    /**
     * Ruft den Wert der enableExpirationTime-Eigenschaft ab.
     * 
     */
    public boolean isEnableExpirationTime() {
        return enableExpirationTime;
    }

    /**
     * Legt den Wert der enableExpirationTime-Eigenschaft fest.
     * 
     */
    public void setEnableExpirationTime(boolean value) {
        this.enableExpirationTime = value;
    }

    /**
     * Ruft den Wert der expirationTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationTime() {
        return expirationTime;
    }

    /**
     * Legt den Wert der expirationTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationTime(XMLGregorianCalendar value) {
        this.expirationTime = value;
    }

    /**
     * Ruft den Wert der alertMeFirst-Eigenschaft ab.
     * 
     */
    public boolean isAlertMeFirst() {
        return alertMeFirst;
    }

    /**
     * Legt den Wert der alertMeFirst-Eigenschaft fest.
     * 
     */
    public void setAlertMeFirst(boolean value) {
        this.alertMeFirst = value;
    }

    /**
     * Ruft den Wert der alertMeFirstNumberOfRings-Eigenschaft ab.
     * 
     */
    public int getAlertMeFirstNumberOfRings() {
        return alertMeFirstNumberOfRings;
    }

    /**
     * Legt den Wert der alertMeFirstNumberOfRings-Eigenschaft fest.
     * 
     */
    public void setAlertMeFirstNumberOfRings(int value) {
        this.alertMeFirstNumberOfRings = value;
    }

}
