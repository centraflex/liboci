//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add the enterprise level data associated with a Voice VPN location code.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für EnterpriseVoiceVPNAddPolicyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseVoiceVPNAddPolicyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="locationDialingCode" type="{}EnterpriseVoiceVPNLocationCode"/>
 *         <element name="minExtensionLength" type="{}EnterpriseVoiceVPNExtensionLength"/>
 *         <element name="maxExtensionLength" type="{}EnterpriseVoiceVPNExtensionLength"/>
 *         <element name="description" type="{}EnterpriseVoiceVPNDescription" minOccurs="0"/>
 *         <element name="routeGroupId" type="{}GroupId" minOccurs="0"/>
 *         <element name="policySelection" type="{}EnterpriseVoiceVPNPolicySelection"/>
 *         <choice>
 *           <element name="digitManipulation" type="{}EnterpriseVoiceVPNDigitManipulation" maxOccurs="8" minOccurs="0"/>
 *           <element name="treatmentId" type="{}EnterpriseVoiceVPNTreatmentId" minOccurs="0"/>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseVoiceVPNAddPolicyRequest", propOrder = {
    "serviceProviderId",
    "locationDialingCode",
    "minExtensionLength",
    "maxExtensionLength",
    "description",
    "routeGroupId",
    "policySelection",
    "digitManipulation",
    "treatmentId"
})
public class EnterpriseVoiceVPNAddPolicyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String locationDialingCode;
    protected int minExtensionLength;
    protected int maxExtensionLength;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String routeGroupId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseVoiceVPNPolicySelection policySelection;
    protected List<EnterpriseVoiceVPNDigitManipulation> digitManipulation;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String treatmentId;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der locationDialingCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationDialingCode() {
        return locationDialingCode;
    }

    /**
     * Legt den Wert der locationDialingCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationDialingCode(String value) {
        this.locationDialingCode = value;
    }

    /**
     * Ruft den Wert der minExtensionLength-Eigenschaft ab.
     * 
     */
    public int getMinExtensionLength() {
        return minExtensionLength;
    }

    /**
     * Legt den Wert der minExtensionLength-Eigenschaft fest.
     * 
     */
    public void setMinExtensionLength(int value) {
        this.minExtensionLength = value;
    }

    /**
     * Ruft den Wert der maxExtensionLength-Eigenschaft ab.
     * 
     */
    public int getMaxExtensionLength() {
        return maxExtensionLength;
    }

    /**
     * Legt den Wert der maxExtensionLength-Eigenschaft fest.
     * 
     */
    public void setMaxExtensionLength(int value) {
        this.maxExtensionLength = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der routeGroupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRouteGroupId() {
        return routeGroupId;
    }

    /**
     * Legt den Wert der routeGroupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRouteGroupId(String value) {
        this.routeGroupId = value;
    }

    /**
     * Ruft den Wert der policySelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseVoiceVPNPolicySelection }
     *     
     */
    public EnterpriseVoiceVPNPolicySelection getPolicySelection() {
        return policySelection;
    }

    /**
     * Legt den Wert der policySelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseVoiceVPNPolicySelection }
     *     
     */
    public void setPolicySelection(EnterpriseVoiceVPNPolicySelection value) {
        this.policySelection = value;
    }

    /**
     * Gets the value of the digitManipulation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the digitManipulation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDigitManipulation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnterpriseVoiceVPNDigitManipulation }
     * 
     * 
     * @return
     *     The value of the digitManipulation property.
     */
    public List<EnterpriseVoiceVPNDigitManipulation> getDigitManipulation() {
        if (digitManipulation == null) {
            digitManipulation = new ArrayList<>();
        }
        return this.digitManipulation;
    }

    /**
     * Ruft den Wert der treatmentId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTreatmentId() {
        return treatmentId;
    }

    /**
     * Legt den Wert der treatmentId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTreatmentId(String value) {
        this.treatmentId = value;
    }

}
