//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify Media Server system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemMediaServerParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemMediaServerParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="mediaServerResponseTimerMilliseconds" type="{}MediaServerResponseTimerMilliseconds" minOccurs="0"/>
 *         <element name="mediaServerSelectionRouteTimerMilliseconds" type="{}MediaServerSelectionRouteTimerMilliseconds" minOccurs="0"/>
 *         <element name="useStaticMediaServerDevice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemMediaServerParametersModifyRequest", propOrder = {
    "mediaServerResponseTimerMilliseconds",
    "mediaServerSelectionRouteTimerMilliseconds",
    "useStaticMediaServerDevice"
})
public class SystemMediaServerParametersModifyRequest
    extends OCIRequest
{

    protected Integer mediaServerResponseTimerMilliseconds;
    protected Integer mediaServerSelectionRouteTimerMilliseconds;
    protected Boolean useStaticMediaServerDevice;

    /**
     * Ruft den Wert der mediaServerResponseTimerMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMediaServerResponseTimerMilliseconds() {
        return mediaServerResponseTimerMilliseconds;
    }

    /**
     * Legt den Wert der mediaServerResponseTimerMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMediaServerResponseTimerMilliseconds(Integer value) {
        this.mediaServerResponseTimerMilliseconds = value;
    }

    /**
     * Ruft den Wert der mediaServerSelectionRouteTimerMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMediaServerSelectionRouteTimerMilliseconds() {
        return mediaServerSelectionRouteTimerMilliseconds;
    }

    /**
     * Legt den Wert der mediaServerSelectionRouteTimerMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMediaServerSelectionRouteTimerMilliseconds(Integer value) {
        this.mediaServerSelectionRouteTimerMilliseconds = value;
    }

    /**
     * Ruft den Wert der useStaticMediaServerDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseStaticMediaServerDevice() {
        return useStaticMediaServerDevice;
    }

    /**
     * Legt den Wert der useStaticMediaServerDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseStaticMediaServerDevice(Boolean value) {
        this.useStaticMediaServerDevice = value;
    }

}
