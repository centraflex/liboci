//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserPhoneDirectoryGetSearchedListRequest.
 *         The "My Room Room Id" and "My Room Bridge Id" are only populated for
 * 	    users assigned the "Collaborate-Audio" service.
 *         Contains a table with  a row for each phone number and column headings :
 *         "Name", "Number", "Extension", "Mobile", "Email Address", "Department", 
 *         "Hiragana Name", "Group Id", "Yahoo Id", "User Id", "IMP Id", "First Name", "Last Name",
 *         "My Room Room Id", "My Room Bridge Id", "Service Name".
 *         The Service Name represents the localized service name for service instances. The localized values are taken from the BroadworksLabel.properties file.
 *         Service Name is currently supporting:
 *         AutoAttendant, AutoAttendantStandard, AutoAttendantVideo, CallCenter, CallCenterStandard, CallCenterPremium
 *         HuntGroup, InstantGroupCall, VoiceMessagingGroup, RoutePoint, BroadWorksAnywhere, GroupPaging, FindmeFollowme,
 *         VoiceXML, FlexibleSeatingGuest, CollaborateAudio, MeetMeConferencing.
 *         For a Regular User or a Virtual On Network Enterprise Extensions, the Service Name is empty.
 *         
 *         The following columns are only returned in AS data mode:       
 *           "My Room Room Id", "My Room Bridge Id", "Service Name"           
 *       
 * 
 * <p>Java-Klasse für UserPhoneDirectoryGetSearchedListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPhoneDirectoryGetSearchedListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="directoryTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPhoneDirectoryGetSearchedListResponse", propOrder = {
    "directoryTable"
})
public class UserPhoneDirectoryGetSearchedListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable directoryTable;

    /**
     * Ruft den Wert der directoryTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDirectoryTable() {
        return directoryTable;
    }

    /**
     * Legt den Wert der directoryTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDirectoryTable(OCITable value) {
        this.directoryTable = value;
    }

}
