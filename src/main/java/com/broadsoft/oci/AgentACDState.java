//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AgentACDState.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AgentACDState">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Sign-In"/>
 *     <enumeration value="Sign-Out"/>
 *     <enumeration value="Available"/>
 *     <enumeration value="Unavailable"/>
 *     <enumeration value="Wrap-Up"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AgentACDState")
@XmlEnum
public enum AgentACDState {

    @XmlEnumValue("Sign-In")
    SIGN_IN("Sign-In"),
    @XmlEnumValue("Sign-Out")
    SIGN_OUT("Sign-Out"),
    @XmlEnumValue("Available")
    AVAILABLE("Available"),
    @XmlEnumValue("Unavailable")
    UNAVAILABLE("Unavailable"),
    @XmlEnumValue("Wrap-Up")
    WRAP_UP("Wrap-Up");
    private final String value;

    AgentACDState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AgentACDState fromValue(String v) {
        for (AgentACDState c: AgentACDState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
