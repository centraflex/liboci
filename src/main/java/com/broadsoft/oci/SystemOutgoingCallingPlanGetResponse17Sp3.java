//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemOutgoingCallingPlanGetRequest17sp3.
 *       
 * 
 * <p>Java-Klasse für SystemOutgoingCallingPlanGetResponse17sp3 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemOutgoingCallingPlanGetResponse17sp3">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="directTransferScreening" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableEnhancedTollCallTyping" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemOutgoingCallingPlanGetResponse17sp3", propOrder = {
    "directTransferScreening",
    "enableEnhancedTollCallTyping"
})
public class SystemOutgoingCallingPlanGetResponse17Sp3
    extends OCIDataResponse
{

    protected boolean directTransferScreening;
    protected boolean enableEnhancedTollCallTyping;

    /**
     * Ruft den Wert der directTransferScreening-Eigenschaft ab.
     * 
     */
    public boolean isDirectTransferScreening() {
        return directTransferScreening;
    }

    /**
     * Legt den Wert der directTransferScreening-Eigenschaft fest.
     * 
     */
    public void setDirectTransferScreening(boolean value) {
        this.directTransferScreening = value;
    }

    /**
     * Ruft den Wert der enableEnhancedTollCallTyping-Eigenschaft ab.
     * 
     */
    public boolean isEnableEnhancedTollCallTyping() {
        return enableEnhancedTollCallTyping;
    }

    /**
     * Legt den Wert der enableEnhancedTollCallTyping-Eigenschaft fest.
     * 
     */
    public void setEnableEnhancedTollCallTyping(boolean value) {
        this.enableEnhancedTollCallTyping = value;
    }

}
