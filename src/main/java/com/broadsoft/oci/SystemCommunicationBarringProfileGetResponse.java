//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the SystemCommunicationBarringProfileGetRequest.
 *         The response contains the Communication Barring Profile information.
 *         Replaced by: SystemCommunicationBarringProfileGetResponse16
 *       
 * 
 * <p>Java-Klasse für SystemCommunicationBarringProfileGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCommunicationBarringProfileGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="description" type="{}CommunicationBarringProfileDescription" minOccurs="0"/>
 *         <element name="originatingDefaultAction" type="{}CommunicationBarringOriginatingAction15sp2"/>
 *         <element name="originatingDefaultTreatmentId" type="{}TreatmentId" minOccurs="0"/>
 *         <element name="originatingDefaultTransferNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="originatingRule" type="{}CommunicationBarringOriginatingRule15sp2" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="redirectingDefaultAction" type="{}CommunicationBarringRedirectingAction15sp2"/>
 *         <element name="redirectingRule" type="{}CommunicationBarringRedirectingRule15sp2" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCommunicationBarringProfileGetResponse", propOrder = {
    "description",
    "originatingDefaultAction",
    "originatingDefaultTreatmentId",
    "originatingDefaultTransferNumber",
    "originatingRule",
    "redirectingDefaultAction",
    "redirectingRule"
})
public class SystemCommunicationBarringProfileGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CommunicationBarringOriginatingAction15Sp2 originatingDefaultAction;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String originatingDefaultTreatmentId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String originatingDefaultTransferNumber;
    protected List<CommunicationBarringOriginatingRule15Sp2> originatingRule;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CommunicationBarringRedirectingAction15Sp2 redirectingDefaultAction;
    protected List<CommunicationBarringRedirectingRule15Sp2> redirectingRule;

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der originatingDefaultAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringOriginatingAction15Sp2 }
     *     
     */
    public CommunicationBarringOriginatingAction15Sp2 getOriginatingDefaultAction() {
        return originatingDefaultAction;
    }

    /**
     * Legt den Wert der originatingDefaultAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringOriginatingAction15Sp2 }
     *     
     */
    public void setOriginatingDefaultAction(CommunicationBarringOriginatingAction15Sp2 value) {
        this.originatingDefaultAction = value;
    }

    /**
     * Ruft den Wert der originatingDefaultTreatmentId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatingDefaultTreatmentId() {
        return originatingDefaultTreatmentId;
    }

    /**
     * Legt den Wert der originatingDefaultTreatmentId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatingDefaultTreatmentId(String value) {
        this.originatingDefaultTreatmentId = value;
    }

    /**
     * Ruft den Wert der originatingDefaultTransferNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatingDefaultTransferNumber() {
        return originatingDefaultTransferNumber;
    }

    /**
     * Legt den Wert der originatingDefaultTransferNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatingDefaultTransferNumber(String value) {
        this.originatingDefaultTransferNumber = value;
    }

    /**
     * Gets the value of the originatingRule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the originatingRule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOriginatingRule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationBarringOriginatingRule15Sp2 }
     * 
     * 
     * @return
     *     The value of the originatingRule property.
     */
    public List<CommunicationBarringOriginatingRule15Sp2> getOriginatingRule() {
        if (originatingRule == null) {
            originatingRule = new ArrayList<>();
        }
        return this.originatingRule;
    }

    /**
     * Ruft den Wert der redirectingDefaultAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringRedirectingAction15Sp2 }
     *     
     */
    public CommunicationBarringRedirectingAction15Sp2 getRedirectingDefaultAction() {
        return redirectingDefaultAction;
    }

    /**
     * Legt den Wert der redirectingDefaultAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringRedirectingAction15Sp2 }
     *     
     */
    public void setRedirectingDefaultAction(CommunicationBarringRedirectingAction15Sp2 value) {
        this.redirectingDefaultAction = value;
    }

    /**
     * Gets the value of the redirectingRule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the redirectingRule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRedirectingRule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationBarringRedirectingRule15Sp2 }
     * 
     * 
     * @return
     *     The value of the redirectingRule property.
     */
    public List<CommunicationBarringRedirectingRule15Sp2> getRedirectingRule() {
        if (redirectingRule == null) {
            redirectingRule = new ArrayList<>();
        }
        return this.redirectingRule;
    }

}
