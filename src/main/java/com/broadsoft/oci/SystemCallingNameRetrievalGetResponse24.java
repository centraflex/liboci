//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCallingNameRetrievalGetRequest24.
 *       
 * 
 * <p>Java-Klasse für SystemCallingNameRetrievalGetResponse24 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallingNameRetrievalGetResponse24">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="triggerCNAMQueriesForAllNetworkCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="triggerCNAMQueriesForGroupAndEnterpriseCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="queryProtocol" type="{}CallingNameRetrievalQueryProtocol"/>
 *         <element name="queryTimeoutMilliseconds" type="{}CallingNameRetrievalQueryTimeoutMilliseconds"/>
 *         <element name="sipExternalDatabaseNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="sipExternalDatabasePort" type="{}Port1025" minOccurs="0"/>
 *         <element name="sipExternalDatabaseTransport" type="{}ExtendedTransportProtocol"/>
 *         <element name="soapExternalDatabaseNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="callingNameSource" type="{}CallingNameRetrievalSourceIdentity"/>
 *         <element name="routeAdvanceTimer" type="{}CallingNameRetrievalRouteAdvanceTimerMilliseconds"/>
 *         <element name="retryFailedCNAMServerInterval" type="{}CallingNameRetrievalRetryFailedCNAMServerIntervalSeconds"/>
 *         <element name="ignoreRestrictedPresentationIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="supportsDNSSRV" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallingNameRetrievalGetResponse24", propOrder = {
    "triggerCNAMQueriesForAllNetworkCalls",
    "triggerCNAMQueriesForGroupAndEnterpriseCalls",
    "queryProtocol",
    "queryTimeoutMilliseconds",
    "sipExternalDatabaseNetAddress",
    "sipExternalDatabasePort",
    "sipExternalDatabaseTransport",
    "soapExternalDatabaseNetAddress",
    "callingNameSource",
    "routeAdvanceTimer",
    "retryFailedCNAMServerInterval",
    "ignoreRestrictedPresentationIndicator",
    "supportsDNSSRV"
})
public class SystemCallingNameRetrievalGetResponse24
    extends OCIDataResponse
{

    protected boolean triggerCNAMQueriesForAllNetworkCalls;
    protected boolean triggerCNAMQueriesForGroupAndEnterpriseCalls;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallingNameRetrievalQueryProtocol queryProtocol;
    protected int queryTimeoutMilliseconds;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sipExternalDatabaseNetAddress;
    protected Integer sipExternalDatabasePort;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedTransportProtocol sipExternalDatabaseTransport;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String soapExternalDatabaseNetAddress;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallingNameRetrievalSourceIdentity callingNameSource;
    protected int routeAdvanceTimer;
    protected int retryFailedCNAMServerInterval;
    protected boolean ignoreRestrictedPresentationIndicator;
    protected boolean supportsDNSSRV;

    /**
     * Ruft den Wert der triggerCNAMQueriesForAllNetworkCalls-Eigenschaft ab.
     * 
     */
    public boolean isTriggerCNAMQueriesForAllNetworkCalls() {
        return triggerCNAMQueriesForAllNetworkCalls;
    }

    /**
     * Legt den Wert der triggerCNAMQueriesForAllNetworkCalls-Eigenschaft fest.
     * 
     */
    public void setTriggerCNAMQueriesForAllNetworkCalls(boolean value) {
        this.triggerCNAMQueriesForAllNetworkCalls = value;
    }

    /**
     * Ruft den Wert der triggerCNAMQueriesForGroupAndEnterpriseCalls-Eigenschaft ab.
     * 
     */
    public boolean isTriggerCNAMQueriesForGroupAndEnterpriseCalls() {
        return triggerCNAMQueriesForGroupAndEnterpriseCalls;
    }

    /**
     * Legt den Wert der triggerCNAMQueriesForGroupAndEnterpriseCalls-Eigenschaft fest.
     * 
     */
    public void setTriggerCNAMQueriesForGroupAndEnterpriseCalls(boolean value) {
        this.triggerCNAMQueriesForGroupAndEnterpriseCalls = value;
    }

    /**
     * Ruft den Wert der queryProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallingNameRetrievalQueryProtocol }
     *     
     */
    public CallingNameRetrievalQueryProtocol getQueryProtocol() {
        return queryProtocol;
    }

    /**
     * Legt den Wert der queryProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallingNameRetrievalQueryProtocol }
     *     
     */
    public void setQueryProtocol(CallingNameRetrievalQueryProtocol value) {
        this.queryProtocol = value;
    }

    /**
     * Ruft den Wert der queryTimeoutMilliseconds-Eigenschaft ab.
     * 
     */
    public int getQueryTimeoutMilliseconds() {
        return queryTimeoutMilliseconds;
    }

    /**
     * Legt den Wert der queryTimeoutMilliseconds-Eigenschaft fest.
     * 
     */
    public void setQueryTimeoutMilliseconds(int value) {
        this.queryTimeoutMilliseconds = value;
    }

    /**
     * Ruft den Wert der sipExternalDatabaseNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSipExternalDatabaseNetAddress() {
        return sipExternalDatabaseNetAddress;
    }

    /**
     * Legt den Wert der sipExternalDatabaseNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSipExternalDatabaseNetAddress(String value) {
        this.sipExternalDatabaseNetAddress = value;
    }

    /**
     * Ruft den Wert der sipExternalDatabasePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSipExternalDatabasePort() {
        return sipExternalDatabasePort;
    }

    /**
     * Legt den Wert der sipExternalDatabasePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSipExternalDatabasePort(Integer value) {
        this.sipExternalDatabasePort = value;
    }

    /**
     * Ruft den Wert der sipExternalDatabaseTransport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedTransportProtocol }
     *     
     */
    public ExtendedTransportProtocol getSipExternalDatabaseTransport() {
        return sipExternalDatabaseTransport;
    }

    /**
     * Legt den Wert der sipExternalDatabaseTransport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedTransportProtocol }
     *     
     */
    public void setSipExternalDatabaseTransport(ExtendedTransportProtocol value) {
        this.sipExternalDatabaseTransport = value;
    }

    /**
     * Ruft den Wert der soapExternalDatabaseNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoapExternalDatabaseNetAddress() {
        return soapExternalDatabaseNetAddress;
    }

    /**
     * Legt den Wert der soapExternalDatabaseNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoapExternalDatabaseNetAddress(String value) {
        this.soapExternalDatabaseNetAddress = value;
    }

    /**
     * Ruft den Wert der callingNameSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallingNameRetrievalSourceIdentity }
     *     
     */
    public CallingNameRetrievalSourceIdentity getCallingNameSource() {
        return callingNameSource;
    }

    /**
     * Legt den Wert der callingNameSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallingNameRetrievalSourceIdentity }
     *     
     */
    public void setCallingNameSource(CallingNameRetrievalSourceIdentity value) {
        this.callingNameSource = value;
    }

    /**
     * Ruft den Wert der routeAdvanceTimer-Eigenschaft ab.
     * 
     */
    public int getRouteAdvanceTimer() {
        return routeAdvanceTimer;
    }

    /**
     * Legt den Wert der routeAdvanceTimer-Eigenschaft fest.
     * 
     */
    public void setRouteAdvanceTimer(int value) {
        this.routeAdvanceTimer = value;
    }

    /**
     * Ruft den Wert der retryFailedCNAMServerInterval-Eigenschaft ab.
     * 
     */
    public int getRetryFailedCNAMServerInterval() {
        return retryFailedCNAMServerInterval;
    }

    /**
     * Legt den Wert der retryFailedCNAMServerInterval-Eigenschaft fest.
     * 
     */
    public void setRetryFailedCNAMServerInterval(int value) {
        this.retryFailedCNAMServerInterval = value;
    }

    /**
     * Ruft den Wert der ignoreRestrictedPresentationIndicator-Eigenschaft ab.
     * 
     */
    public boolean isIgnoreRestrictedPresentationIndicator() {
        return ignoreRestrictedPresentationIndicator;
    }

    /**
     * Legt den Wert der ignoreRestrictedPresentationIndicator-Eigenschaft fest.
     * 
     */
    public void setIgnoreRestrictedPresentationIndicator(boolean value) {
        this.ignoreRestrictedPresentationIndicator = value;
    }

    /**
     * Ruft den Wert der supportsDNSSRV-Eigenschaft ab.
     * 
     */
    public boolean isSupportsDNSSRV() {
        return supportsDNSSRV;
    }

    /**
     * Legt den Wert der supportsDNSSRV-Eigenschaft fest.
     * 
     */
    public void setSupportsDNSSRV(boolean value) {
        this.supportsDNSSRV = value;
    }

}
