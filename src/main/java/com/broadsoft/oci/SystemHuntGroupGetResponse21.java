//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemHuntGroupGetRequest21.
 *       
 * 
 * <p>Java-Klasse für SystemHuntGroupGetResponse21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemHuntGroupGetResponse21">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="removeHuntGroupNameFromCLID" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="uniformCallDistributionPolicyScope" type="{}HuntGroupUniformCallDistributionPolicyScope"/>
 *         <element name="allowAgentDeviceInitiatedForward" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemHuntGroupGetResponse21", propOrder = {
    "removeHuntGroupNameFromCLID",
    "uniformCallDistributionPolicyScope",
    "allowAgentDeviceInitiatedForward"
})
public class SystemHuntGroupGetResponse21
    extends OCIDataResponse
{

    protected boolean removeHuntGroupNameFromCLID;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected HuntGroupUniformCallDistributionPolicyScope uniformCallDistributionPolicyScope;
    protected boolean allowAgentDeviceInitiatedForward;

    /**
     * Ruft den Wert der removeHuntGroupNameFromCLID-Eigenschaft ab.
     * 
     */
    public boolean isRemoveHuntGroupNameFromCLID() {
        return removeHuntGroupNameFromCLID;
    }

    /**
     * Legt den Wert der removeHuntGroupNameFromCLID-Eigenschaft fest.
     * 
     */
    public void setRemoveHuntGroupNameFromCLID(boolean value) {
        this.removeHuntGroupNameFromCLID = value;
    }

    /**
     * Ruft den Wert der uniformCallDistributionPolicyScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HuntGroupUniformCallDistributionPolicyScope }
     *     
     */
    public HuntGroupUniformCallDistributionPolicyScope getUniformCallDistributionPolicyScope() {
        return uniformCallDistributionPolicyScope;
    }

    /**
     * Legt den Wert der uniformCallDistributionPolicyScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HuntGroupUniformCallDistributionPolicyScope }
     *     
     */
    public void setUniformCallDistributionPolicyScope(HuntGroupUniformCallDistributionPolicyScope value) {
        this.uniformCallDistributionPolicyScope = value;
    }

    /**
     * Ruft den Wert der allowAgentDeviceInitiatedForward-Eigenschaft ab.
     * 
     */
    public boolean isAllowAgentDeviceInitiatedForward() {
        return allowAgentDeviceInitiatedForward;
    }

    /**
     * Legt den Wert der allowAgentDeviceInitiatedForward-Eigenschaft fest.
     * 
     */
    public void setAllowAgentDeviceInitiatedForward(boolean value) {
        this.allowAgentDeviceInitiatedForward = value;
    }

}
