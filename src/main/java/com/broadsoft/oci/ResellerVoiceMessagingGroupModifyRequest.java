//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the reseller level data associated with Voice Messaging.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ResellerVoiceMessagingGroupModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerVoiceMessagingGroupModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="resellerId" type="{}ResellerId22"/>
 *         <element name="deliveryFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="notificationFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="voicePortalLockoutFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerVoiceMessagingGroupModifyRequest", propOrder = {
    "resellerId",
    "deliveryFromAddress",
    "notificationFromAddress",
    "voicePortalLockoutFromAddress"
})
public class ResellerVoiceMessagingGroupModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    @XmlElementRef(name = "deliveryFromAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deliveryFromAddress;
    @XmlElementRef(name = "notificationFromAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> notificationFromAddress;
    @XmlElementRef(name = "voicePortalLockoutFromAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> voicePortalLockoutFromAddress;

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Ruft den Wert der deliveryFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeliveryFromAddress() {
        return deliveryFromAddress;
    }

    /**
     * Legt den Wert der deliveryFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeliveryFromAddress(JAXBElement<String> value) {
        this.deliveryFromAddress = value;
    }

    /**
     * Ruft den Wert der notificationFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNotificationFromAddress() {
        return notificationFromAddress;
    }

    /**
     * Legt den Wert der notificationFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNotificationFromAddress(JAXBElement<String> value) {
        this.notificationFromAddress = value;
    }

    /**
     * Ruft den Wert der voicePortalLockoutFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVoicePortalLockoutFromAddress() {
        return voicePortalLockoutFromAddress;
    }

    /**
     * Legt den Wert der voicePortalLockoutFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVoicePortalLockoutFromAddress(JAXBElement<String> value) {
        this.voicePortalLockoutFromAddress = value;
    }

}
