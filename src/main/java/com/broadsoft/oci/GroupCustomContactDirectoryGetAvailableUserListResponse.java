//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupCustomContactDirectoryGetAvailableUserListRequest.
 *         Contains a table with column headings: "User Id", "Last Name", "First Name", 
 *         "Hiragana Last Name", "Hiragana First Name".
 *         Replace by: GroupCustomContactDirectoryGetAvailableUserListResponse17
 *       
 * 
 * <p>Java-Klasse für GroupCustomContactDirectoryGetAvailableUserListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCustomContactDirectoryGetAvailableUserListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="userTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCustomContactDirectoryGetAvailableUserListResponse", propOrder = {
    "userTable"
})
public class GroupCustomContactDirectoryGetAvailableUserListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable userTable;

    /**
     * Ruft den Wert der userTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserTable() {
        return userTable;
    }

    /**
     * Legt den Wert der userTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserTable(OCITable value) {
        this.userTable = value;
    }

}
