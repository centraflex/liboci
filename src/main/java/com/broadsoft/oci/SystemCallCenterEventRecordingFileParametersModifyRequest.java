//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify Call Center Event Recording File system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCallCenterEventRecordingFileParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallCenterEventRecordingFileParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="fileRetentionTimeDays" type="{}CallCenterEventRecordingFileRetentionTimeDays" minOccurs="0"/>
 *         <element name="fileRotationPeriodMinutes" type="{}CallCenterEventRecordingFileRotationPeriodMinutes" minOccurs="0"/>
 *         <element name="fileRotationOffsetMinutes" type="{}CallCenterEventRecordingFileRotationOffsetMinutes" minOccurs="0"/>
 *         <element name="remoteUrl" type="{}URL" minOccurs="0"/>
 *         <element name="remoteUserId" type="{}CallCenterEventRecordingFileSFTPUserId" minOccurs="0"/>
 *         <element name="remotePassword" type="{}CallCenterEventRecordingFileSFTPUserPassword" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallCenterEventRecordingFileParametersModifyRequest", propOrder = {
    "fileRetentionTimeDays",
    "fileRotationPeriodMinutes",
    "fileRotationOffsetMinutes",
    "remoteUrl",
    "remoteUserId",
    "remotePassword"
})
public class SystemCallCenterEventRecordingFileParametersModifyRequest
    extends OCIRequest
{

    protected Integer fileRetentionTimeDays;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String fileRotationPeriodMinutes;
    protected Integer fileRotationOffsetMinutes;
    @XmlElementRef(name = "remoteUrl", type = JAXBElement.class, required = false)
    protected JAXBElement<String> remoteUrl;
    @XmlElementRef(name = "remoteUserId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> remoteUserId;
    @XmlElementRef(name = "remotePassword", type = JAXBElement.class, required = false)
    protected JAXBElement<String> remotePassword;

    /**
     * Ruft den Wert der fileRetentionTimeDays-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFileRetentionTimeDays() {
        return fileRetentionTimeDays;
    }

    /**
     * Legt den Wert der fileRetentionTimeDays-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFileRetentionTimeDays(Integer value) {
        this.fileRetentionTimeDays = value;
    }

    /**
     * Ruft den Wert der fileRotationPeriodMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileRotationPeriodMinutes() {
        return fileRotationPeriodMinutes;
    }

    /**
     * Legt den Wert der fileRotationPeriodMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileRotationPeriodMinutes(String value) {
        this.fileRotationPeriodMinutes = value;
    }

    /**
     * Ruft den Wert der fileRotationOffsetMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFileRotationOffsetMinutes() {
        return fileRotationOffsetMinutes;
    }

    /**
     * Legt den Wert der fileRotationOffsetMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFileRotationOffsetMinutes(Integer value) {
        this.fileRotationOffsetMinutes = value;
    }

    /**
     * Ruft den Wert der remoteUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRemoteUrl() {
        return remoteUrl;
    }

    /**
     * Legt den Wert der remoteUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRemoteUrl(JAXBElement<String> value) {
        this.remoteUrl = value;
    }

    /**
     * Ruft den Wert der remoteUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRemoteUserId() {
        return remoteUserId;
    }

    /**
     * Legt den Wert der remoteUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRemoteUserId(JAXBElement<String> value) {
        this.remoteUserId = value;
    }

    /**
     * Ruft den Wert der remotePassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRemotePassword() {
        return remotePassword;
    }

    /**
     * Legt den Wert der remotePassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRemotePassword(JAXBElement<String> value) {
        this.remotePassword = value;
    }

}
