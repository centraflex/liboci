//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         CommPilot Express Available Out Of Office Configuration used in the context of a modify.
 *       
 * 
 * <p>Java-Klasse für CommPilotExpressAvailableOutOfOfficeModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CommPilotExpressAvailableOutOfOfficeModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="incomingCalls" type="{}CommPilotExpressRedirectionModify" minOccurs="0"/>
 *         <element name="incomingCallNotify" type="{}CommPilotExpressEmailNotifyModify" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommPilotExpressAvailableOutOfOfficeModify", propOrder = {
    "incomingCalls",
    "incomingCallNotify"
})
public class CommPilotExpressAvailableOutOfOfficeModify {

    protected CommPilotExpressRedirectionModify incomingCalls;
    protected CommPilotExpressEmailNotifyModify incomingCallNotify;

    /**
     * Ruft den Wert der incomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressRedirectionModify }
     *     
     */
    public CommPilotExpressRedirectionModify getIncomingCalls() {
        return incomingCalls;
    }

    /**
     * Legt den Wert der incomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressRedirectionModify }
     *     
     */
    public void setIncomingCalls(CommPilotExpressRedirectionModify value) {
        this.incomingCalls = value;
    }

    /**
     * Ruft den Wert der incomingCallNotify-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressEmailNotifyModify }
     *     
     */
    public CommPilotExpressEmailNotifyModify getIncomingCallNotify() {
        return incomingCallNotify;
    }

    /**
     * Legt den Wert der incomingCallNotify-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressEmailNotifyModify }
     *     
     */
    public void setIncomingCallNotify(CommPilotExpressEmailNotifyModify value) {
        this.incomingCallNotify = value;
    }

}
