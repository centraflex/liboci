//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PerformanceMeasurementReportingEncoding.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="PerformanceMeasurementReportingEncoding">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="US-ACSII"/>
 *     <enumeration value="ISO-8859-1"/>
 *     <enumeration value="UTF-8"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "PerformanceMeasurementReportingEncoding")
@XmlEnum
public enum PerformanceMeasurementReportingEncoding {

    @XmlEnumValue("US-ACSII")
    US_ACSII("US-ACSII"),
    @XmlEnumValue("ISO-8859-1")
    ISO_8859_1("ISO-8859-1"),
    @XmlEnumValue("UTF-8")
    UTF_8("UTF-8");
    private final String value;

    PerformanceMeasurementReportingEncoding(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PerformanceMeasurementReportingEncoding fromValue(String v) {
        for (PerformanceMeasurementReportingEncoding c: PerformanceMeasurementReportingEncoding.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
