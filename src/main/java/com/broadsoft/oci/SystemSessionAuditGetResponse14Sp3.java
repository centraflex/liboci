//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemSessionAuditGetRequest14sp3.
 *       
 * 
 * <p>Java-Klasse für SystemSessionAuditGetResponse14sp3 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSessionAuditGetResponse14sp3">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isAuditActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="auditIntervalSeconds" type="{}SessionAuditIntervalSeconds"/>
 *         <element name="auditTimeoutSeconds" type="{}SessionAuditTimeoutPeriodSeconds"/>
 *         <element name="releaseCallOnAuditFailure" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isSIPRefreshAllowedOnAudit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowUpdateForSIPRefresh" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isSIPSessionTimerActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sipSessionExpiresMinimumSeconds" type="{}SIPSessionExpiresMinimumSeconds"/>
 *         <element name="enforceSIPSessionExpiresMaximum" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sipSessionExpiresMaximumSeconds" type="{}SIPSessionExpiresMaximumSeconds"/>
 *         <element name="sipSessionExpiresTimerSeconds" type="{}SIPSessionExpiresTimerSeconds"/>
 *         <element name="alwaysUseSessionTimerWhenSupported" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="preferredSessionTimerRefresher" type="{}SessionTimerRefresher"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSessionAuditGetResponse14sp3", propOrder = {
    "isAuditActive",
    "auditIntervalSeconds",
    "auditTimeoutSeconds",
    "releaseCallOnAuditFailure",
    "isSIPRefreshAllowedOnAudit",
    "allowUpdateForSIPRefresh",
    "isSIPSessionTimerActive",
    "sipSessionExpiresMinimumSeconds",
    "enforceSIPSessionExpiresMaximum",
    "sipSessionExpiresMaximumSeconds",
    "sipSessionExpiresTimerSeconds",
    "alwaysUseSessionTimerWhenSupported",
    "preferredSessionTimerRefresher"
})
public class SystemSessionAuditGetResponse14Sp3
    extends OCIDataResponse
{

    protected boolean isAuditActive;
    protected int auditIntervalSeconds;
    protected int auditTimeoutSeconds;
    protected boolean releaseCallOnAuditFailure;
    protected boolean isSIPRefreshAllowedOnAudit;
    protected boolean allowUpdateForSIPRefresh;
    protected boolean isSIPSessionTimerActive;
    protected int sipSessionExpiresMinimumSeconds;
    protected boolean enforceSIPSessionExpiresMaximum;
    protected int sipSessionExpiresMaximumSeconds;
    protected int sipSessionExpiresTimerSeconds;
    protected boolean alwaysUseSessionTimerWhenSupported;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SessionTimerRefresher preferredSessionTimerRefresher;

    /**
     * Ruft den Wert der isAuditActive-Eigenschaft ab.
     * 
     */
    public boolean isIsAuditActive() {
        return isAuditActive;
    }

    /**
     * Legt den Wert der isAuditActive-Eigenschaft fest.
     * 
     */
    public void setIsAuditActive(boolean value) {
        this.isAuditActive = value;
    }

    /**
     * Ruft den Wert der auditIntervalSeconds-Eigenschaft ab.
     * 
     */
    public int getAuditIntervalSeconds() {
        return auditIntervalSeconds;
    }

    /**
     * Legt den Wert der auditIntervalSeconds-Eigenschaft fest.
     * 
     */
    public void setAuditIntervalSeconds(int value) {
        this.auditIntervalSeconds = value;
    }

    /**
     * Ruft den Wert der auditTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getAuditTimeoutSeconds() {
        return auditTimeoutSeconds;
    }

    /**
     * Legt den Wert der auditTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setAuditTimeoutSeconds(int value) {
        this.auditTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der releaseCallOnAuditFailure-Eigenschaft ab.
     * 
     */
    public boolean isReleaseCallOnAuditFailure() {
        return releaseCallOnAuditFailure;
    }

    /**
     * Legt den Wert der releaseCallOnAuditFailure-Eigenschaft fest.
     * 
     */
    public void setReleaseCallOnAuditFailure(boolean value) {
        this.releaseCallOnAuditFailure = value;
    }

    /**
     * Ruft den Wert der isSIPRefreshAllowedOnAudit-Eigenschaft ab.
     * 
     */
    public boolean isIsSIPRefreshAllowedOnAudit() {
        return isSIPRefreshAllowedOnAudit;
    }

    /**
     * Legt den Wert der isSIPRefreshAllowedOnAudit-Eigenschaft fest.
     * 
     */
    public void setIsSIPRefreshAllowedOnAudit(boolean value) {
        this.isSIPRefreshAllowedOnAudit = value;
    }

    /**
     * Ruft den Wert der allowUpdateForSIPRefresh-Eigenschaft ab.
     * 
     */
    public boolean isAllowUpdateForSIPRefresh() {
        return allowUpdateForSIPRefresh;
    }

    /**
     * Legt den Wert der allowUpdateForSIPRefresh-Eigenschaft fest.
     * 
     */
    public void setAllowUpdateForSIPRefresh(boolean value) {
        this.allowUpdateForSIPRefresh = value;
    }

    /**
     * Ruft den Wert der isSIPSessionTimerActive-Eigenschaft ab.
     * 
     */
    public boolean isIsSIPSessionTimerActive() {
        return isSIPSessionTimerActive;
    }

    /**
     * Legt den Wert der isSIPSessionTimerActive-Eigenschaft fest.
     * 
     */
    public void setIsSIPSessionTimerActive(boolean value) {
        this.isSIPSessionTimerActive = value;
    }

    /**
     * Ruft den Wert der sipSessionExpiresMinimumSeconds-Eigenschaft ab.
     * 
     */
    public int getSipSessionExpiresMinimumSeconds() {
        return sipSessionExpiresMinimumSeconds;
    }

    /**
     * Legt den Wert der sipSessionExpiresMinimumSeconds-Eigenschaft fest.
     * 
     */
    public void setSipSessionExpiresMinimumSeconds(int value) {
        this.sipSessionExpiresMinimumSeconds = value;
    }

    /**
     * Ruft den Wert der enforceSIPSessionExpiresMaximum-Eigenschaft ab.
     * 
     */
    public boolean isEnforceSIPSessionExpiresMaximum() {
        return enforceSIPSessionExpiresMaximum;
    }

    /**
     * Legt den Wert der enforceSIPSessionExpiresMaximum-Eigenschaft fest.
     * 
     */
    public void setEnforceSIPSessionExpiresMaximum(boolean value) {
        this.enforceSIPSessionExpiresMaximum = value;
    }

    /**
     * Ruft den Wert der sipSessionExpiresMaximumSeconds-Eigenschaft ab.
     * 
     */
    public int getSipSessionExpiresMaximumSeconds() {
        return sipSessionExpiresMaximumSeconds;
    }

    /**
     * Legt den Wert der sipSessionExpiresMaximumSeconds-Eigenschaft fest.
     * 
     */
    public void setSipSessionExpiresMaximumSeconds(int value) {
        this.sipSessionExpiresMaximumSeconds = value;
    }

    /**
     * Ruft den Wert der sipSessionExpiresTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getSipSessionExpiresTimerSeconds() {
        return sipSessionExpiresTimerSeconds;
    }

    /**
     * Legt den Wert der sipSessionExpiresTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setSipSessionExpiresTimerSeconds(int value) {
        this.sipSessionExpiresTimerSeconds = value;
    }

    /**
     * Ruft den Wert der alwaysUseSessionTimerWhenSupported-Eigenschaft ab.
     * 
     */
    public boolean isAlwaysUseSessionTimerWhenSupported() {
        return alwaysUseSessionTimerWhenSupported;
    }

    /**
     * Legt den Wert der alwaysUseSessionTimerWhenSupported-Eigenschaft fest.
     * 
     */
    public void setAlwaysUseSessionTimerWhenSupported(boolean value) {
        this.alwaysUseSessionTimerWhenSupported = value;
    }

    /**
     * Ruft den Wert der preferredSessionTimerRefresher-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SessionTimerRefresher }
     *     
     */
    public SessionTimerRefresher getPreferredSessionTimerRefresher() {
        return preferredSessionTimerRefresher;
    }

    /**
     * Legt den Wert der preferredSessionTimerRefresher-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionTimerRefresher }
     *     
     */
    public void setPreferredSessionTimerRefresher(SessionTimerRefresher value) {
        this.preferredSessionTimerRefresher = value;
    }

}
