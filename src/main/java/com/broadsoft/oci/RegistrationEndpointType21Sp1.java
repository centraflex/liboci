//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für RegistrationEndpointType21sp1.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="RegistrationEndpointType21sp1">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Primary"/>
 *     <enumeration value="Shared Call Appearance"/>
 *     <enumeration value="Video Add On"/>
 *     <enumeration value="Mobility"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "RegistrationEndpointType21sp1")
@XmlEnum
public enum RegistrationEndpointType21Sp1 {

    @XmlEnumValue("Primary")
    PRIMARY("Primary"),
    @XmlEnumValue("Shared Call Appearance")
    SHARED_CALL_APPEARANCE("Shared Call Appearance"),
    @XmlEnumValue("Video Add On")
    VIDEO_ADD_ON("Video Add On"),
    @XmlEnumValue("Mobility")
    MOBILITY("Mobility");
    private final String value;

    RegistrationEndpointType21Sp1(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RegistrationEndpointType21Sp1 fromValue(String v) {
        for (RegistrationEndpointType21Sp1 c: RegistrationEndpointType21Sp1 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
