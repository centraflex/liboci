//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupMeetMeConferencingGetInstanceRequest17sp3.
 *         Contains the service profile information and a table of assigned hosts.
 *         The table has column headings: "User Id", "Last Name", "First Name", "Hiragana Last Name", and "Hiragana First Name".
 *       
 * 
 * <p>Java-Klasse für GroupMeetMeConferencingGetInstanceResponse17sp3 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupMeetMeConferencingGetInstanceResponse17sp3">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceReadProfile17"/>
 *         <element name="allocatedPorts" type="{}MeetMeConferencingConferencePorts"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="allowIndividualOutDial" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="operatorNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="conferenceHostUserTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupMeetMeConferencingGetInstanceResponse17sp3", propOrder = {
    "serviceInstanceProfile",
    "allocatedPorts",
    "networkClassOfService",
    "allowIndividualOutDial",
    "operatorNumber",
    "conferenceHostUserTable"
})
public class GroupMeetMeConferencingGetInstanceResponse17Sp3
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected ServiceInstanceReadProfile17 serviceInstanceProfile;
    @XmlElement(required = true)
    protected MeetMeConferencingConferencePorts allocatedPorts;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    protected boolean allowIndividualOutDial;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String operatorNumber;
    @XmlElement(required = true)
    protected OCITable conferenceHostUserTable;

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceReadProfile17 }
     *     
     */
    public ServiceInstanceReadProfile17 getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceReadProfile17 }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceReadProfile17 value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der allocatedPorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferencePorts }
     *     
     */
    public MeetMeConferencingConferencePorts getAllocatedPorts() {
        return allocatedPorts;
    }

    /**
     * Legt den Wert der allocatedPorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferencePorts }
     *     
     */
    public void setAllocatedPorts(MeetMeConferencingConferencePorts value) {
        this.allocatedPorts = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Ruft den Wert der allowIndividualOutDial-Eigenschaft ab.
     * 
     */
    public boolean isAllowIndividualOutDial() {
        return allowIndividualOutDial;
    }

    /**
     * Legt den Wert der allowIndividualOutDial-Eigenschaft fest.
     * 
     */
    public void setAllowIndividualOutDial(boolean value) {
        this.allowIndividualOutDial = value;
    }

    /**
     * Ruft den Wert der operatorNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatorNumber() {
        return operatorNumber;
    }

    /**
     * Legt den Wert der operatorNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatorNumber(String value) {
        this.operatorNumber = value;
    }

    /**
     * Ruft den Wert der conferenceHostUserTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getConferenceHostUserTable() {
        return conferenceHostUserTable;
    }

    /**
     * Legt den Wert der conferenceHostUserTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setConferenceHostUserTable(OCITable value) {
        this.conferenceHostUserTable = value;
    }

}
