//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemAccessDeviceGetLinkedTreeDeviceRequest.
 *       
 * 
 * <p>Java-Klasse für SystemAccessDeviceGetLinkedTreeDeviceResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAccessDeviceGetLinkedTreeDeviceResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="treeDeviceInfo" type="{}TreeDeviceInfo" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAccessDeviceGetLinkedTreeDeviceResponse", propOrder = {
    "treeDeviceInfo"
})
public class SystemAccessDeviceGetLinkedTreeDeviceResponse
    extends OCIDataResponse
{

    protected TreeDeviceInfo treeDeviceInfo;

    /**
     * Ruft den Wert der treeDeviceInfo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TreeDeviceInfo }
     *     
     */
    public TreeDeviceInfo getTreeDeviceInfo() {
        return treeDeviceInfo;
    }

    /**
     * Legt den Wert der treeDeviceInfo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TreeDeviceInfo }
     *     
     */
    public void setTreeDeviceInfo(TreeDeviceInfo value) {
        this.treeDeviceInfo = value;
    }

}
