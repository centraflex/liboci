//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to get the list of custom configuration tags managed by the Device Management System.
 *         The response is either SystemDeviceManagementTagGetListResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemDeviceManagementTagGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceManagementTagGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <choice>
 *           <element name="systemDefaultTagSet" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *           <element name="tagSetName" type="{}DeviceManagementTagSetName"/>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceManagementTagGetListRequest", propOrder = {
    "systemDefaultTagSet",
    "tagSetName"
})
public class SystemDeviceManagementTagGetListRequest
    extends OCIRequest
{

    protected Boolean systemDefaultTagSet;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String tagSetName;

    /**
     * Ruft den Wert der systemDefaultTagSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSystemDefaultTagSet() {
        return systemDefaultTagSet;
    }

    /**
     * Legt den Wert der systemDefaultTagSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSystemDefaultTagSet(Boolean value) {
        this.systemDefaultTagSet = value;
    }

    /**
     * Ruft den Wert der tagSetName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTagSetName() {
        return tagSetName;
    }

    /**
     * Legt den Wert der tagSetName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTagSetName(String value) {
        this.tagSetName = value;
    }

}
