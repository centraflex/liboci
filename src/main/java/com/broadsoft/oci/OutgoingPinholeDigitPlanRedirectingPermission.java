//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für OutgoingPinholeDigitPlanRedirectingPermission.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="OutgoingPinholeDigitPlanRedirectingPermission">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Ignore"/>
 *     <enumeration value="Allow"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "OutgoingPinholeDigitPlanRedirectingPermission")
@XmlEnum
public enum OutgoingPinholeDigitPlanRedirectingPermission {

    @XmlEnumValue("Ignore")
    IGNORE("Ignore"),
    @XmlEnumValue("Allow")
    ALLOW("Allow");
    private final String value;

    OutgoingPinholeDigitPlanRedirectingPermission(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OutgoingPinholeDigitPlanRedirectingPermission fromValue(String v) {
        for (OutgoingPinholeDigitPlanRedirectingPermission c: OutgoingPinholeDigitPlanRedirectingPermission.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
