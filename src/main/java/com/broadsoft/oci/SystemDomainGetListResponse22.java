//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Contains a table of all matching system-level domain names and all matching reseller level domains.
 *         The column headings are: "Domain Name" and "Reseller Id".
 *         The following elements are only used in AS and XS data mode and not returned in Amplify data mode:
 *           systemDefaultDomain
 *           
 *         Replaced by SystemDomainGetListResponse22V2 in AS data mode.
 *       
 * 
 * <p>Java-Klasse für SystemDomainGetListResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDomainGetListResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="systemDefaultDomain" type="{}NetAddress" minOccurs="0"/>
 *         <element name="domainTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDomainGetListResponse22", propOrder = {
    "systemDefaultDomain",
    "domainTable"
})
public class SystemDomainGetListResponse22
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String systemDefaultDomain;
    @XmlElement(required = true)
    protected OCITable domainTable;

    /**
     * Ruft den Wert der systemDefaultDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemDefaultDomain() {
        return systemDefaultDomain;
    }

    /**
     * Legt den Wert der systemDefaultDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemDefaultDomain(String value) {
        this.systemDefaultDomain = value;
    }

    /**
     * Ruft den Wert der domainTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDomainTable() {
        return domainTable;
    }

    /**
     * Legt den Wert der domainTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDomainTable(OCITable value) {
        this.domainTable = value;
    }

}
