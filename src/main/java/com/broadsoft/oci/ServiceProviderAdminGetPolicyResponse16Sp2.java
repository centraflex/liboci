//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderAdminGetPolicyRequest16sp1.
 *         Response to ServiceProviderAdminGetPolicyRequest16sp2.
 *         Contains the policy settings for the service provider administrator.
 *         The networkPolicyAccess is returned only for the enterprise administrator.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderAdminGetPolicyResponse16sp2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderAdminGetPolicyResponse16sp2">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="profileAccess" type="{}ServiceProviderAdminProfileAccess"/>
 *         <element name="groupAccess" type="{}ServiceProviderAdminGroupAccess"/>
 *         <element name="userAccess" type="{}ServiceProviderAdminUserAccess"/>
 *         <element name="adminAccess" type="{}ServiceProviderAdminAdminAccess"/>
 *         <element name="departmentAccess" type="{}ServiceProviderAdminDepartmentAccess"/>
 *         <element name="accessDeviceAccess" type="{}ServiceProviderAdminAccessDeviceAccess"/>
 *         <element name="phoneNumberExtensionAccess" type="{}ServiceProviderAdminPhoneNumberExtensionAccess"/>
 *         <element name="serviceAccess" type="{}ServiceProviderAdminServiceAccess"/>
 *         <element name="servicePackAccess" type="{}ServiceProviderAdminServicePackAccess"/>
 *         <element name="sessionAdmissionControlAccess" type="{}ServiceProviderAdminSessionAdmissionControlAccess"/>
 *         <element name="webBrandingAccess" type="{}ServiceProviderAdminWebBrandingAccess"/>
 *         <element name="networkPolicyAccess" type="{}EnterpriseAdminNetworkPolicyAccess" minOccurs="0"/>
 *         <element name="dialableCallerIDAccess" type="{}ServiceProviderAdminDialableCallerIDAccess"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderAdminGetPolicyResponse16sp2", propOrder = {
    "profileAccess",
    "groupAccess",
    "userAccess",
    "adminAccess",
    "departmentAccess",
    "accessDeviceAccess",
    "phoneNumberExtensionAccess",
    "serviceAccess",
    "servicePackAccess",
    "sessionAdmissionControlAccess",
    "webBrandingAccess",
    "networkPolicyAccess",
    "dialableCallerIDAccess"
})
public class ServiceProviderAdminGetPolicyResponse16Sp2
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminProfileAccess profileAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminGroupAccess groupAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminUserAccess userAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminAdminAccess adminAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminDepartmentAccess departmentAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminAccessDeviceAccess accessDeviceAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminPhoneNumberExtensionAccess phoneNumberExtensionAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminServiceAccess serviceAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminServicePackAccess servicePackAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminSessionAdmissionControlAccess sessionAdmissionControlAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminWebBrandingAccess webBrandingAccess;
    @XmlSchemaType(name = "token")
    protected EnterpriseAdminNetworkPolicyAccess networkPolicyAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminDialableCallerIDAccess dialableCallerIDAccess;

    /**
     * Ruft den Wert der profileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminProfileAccess }
     *     
     */
    public ServiceProviderAdminProfileAccess getProfileAccess() {
        return profileAccess;
    }

    /**
     * Legt den Wert der profileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminProfileAccess }
     *     
     */
    public void setProfileAccess(ServiceProviderAdminProfileAccess value) {
        this.profileAccess = value;
    }

    /**
     * Ruft den Wert der groupAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminGroupAccess }
     *     
     */
    public ServiceProviderAdminGroupAccess getGroupAccess() {
        return groupAccess;
    }

    /**
     * Legt den Wert der groupAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminGroupAccess }
     *     
     */
    public void setGroupAccess(ServiceProviderAdminGroupAccess value) {
        this.groupAccess = value;
    }

    /**
     * Ruft den Wert der userAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminUserAccess }
     *     
     */
    public ServiceProviderAdminUserAccess getUserAccess() {
        return userAccess;
    }

    /**
     * Legt den Wert der userAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminUserAccess }
     *     
     */
    public void setUserAccess(ServiceProviderAdminUserAccess value) {
        this.userAccess = value;
    }

    /**
     * Ruft den Wert der adminAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminAdminAccess }
     *     
     */
    public ServiceProviderAdminAdminAccess getAdminAccess() {
        return adminAccess;
    }

    /**
     * Legt den Wert der adminAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminAdminAccess }
     *     
     */
    public void setAdminAccess(ServiceProviderAdminAdminAccess value) {
        this.adminAccess = value;
    }

    /**
     * Ruft den Wert der departmentAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminDepartmentAccess }
     *     
     */
    public ServiceProviderAdminDepartmentAccess getDepartmentAccess() {
        return departmentAccess;
    }

    /**
     * Legt den Wert der departmentAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminDepartmentAccess }
     *     
     */
    public void setDepartmentAccess(ServiceProviderAdminDepartmentAccess value) {
        this.departmentAccess = value;
    }

    /**
     * Ruft den Wert der accessDeviceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminAccessDeviceAccess }
     *     
     */
    public ServiceProviderAdminAccessDeviceAccess getAccessDeviceAccess() {
        return accessDeviceAccess;
    }

    /**
     * Legt den Wert der accessDeviceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminAccessDeviceAccess }
     *     
     */
    public void setAccessDeviceAccess(ServiceProviderAdminAccessDeviceAccess value) {
        this.accessDeviceAccess = value;
    }

    /**
     * Ruft den Wert der phoneNumberExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminPhoneNumberExtensionAccess }
     *     
     */
    public ServiceProviderAdminPhoneNumberExtensionAccess getPhoneNumberExtensionAccess() {
        return phoneNumberExtensionAccess;
    }

    /**
     * Legt den Wert der phoneNumberExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminPhoneNumberExtensionAccess }
     *     
     */
    public void setPhoneNumberExtensionAccess(ServiceProviderAdminPhoneNumberExtensionAccess value) {
        this.phoneNumberExtensionAccess = value;
    }

    /**
     * Ruft den Wert der serviceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminServiceAccess }
     *     
     */
    public ServiceProviderAdminServiceAccess getServiceAccess() {
        return serviceAccess;
    }

    /**
     * Legt den Wert der serviceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminServiceAccess }
     *     
     */
    public void setServiceAccess(ServiceProviderAdminServiceAccess value) {
        this.serviceAccess = value;
    }

    /**
     * Ruft den Wert der servicePackAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminServicePackAccess }
     *     
     */
    public ServiceProviderAdminServicePackAccess getServicePackAccess() {
        return servicePackAccess;
    }

    /**
     * Legt den Wert der servicePackAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminServicePackAccess }
     *     
     */
    public void setServicePackAccess(ServiceProviderAdminServicePackAccess value) {
        this.servicePackAccess = value;
    }

    /**
     * Ruft den Wert der sessionAdmissionControlAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminSessionAdmissionControlAccess }
     *     
     */
    public ServiceProviderAdminSessionAdmissionControlAccess getSessionAdmissionControlAccess() {
        return sessionAdmissionControlAccess;
    }

    /**
     * Legt den Wert der sessionAdmissionControlAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminSessionAdmissionControlAccess }
     *     
     */
    public void setSessionAdmissionControlAccess(ServiceProviderAdminSessionAdmissionControlAccess value) {
        this.sessionAdmissionControlAccess = value;
    }

    /**
     * Ruft den Wert der webBrandingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminWebBrandingAccess }
     *     
     */
    public ServiceProviderAdminWebBrandingAccess getWebBrandingAccess() {
        return webBrandingAccess;
    }

    /**
     * Legt den Wert der webBrandingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminWebBrandingAccess }
     *     
     */
    public void setWebBrandingAccess(ServiceProviderAdminWebBrandingAccess value) {
        this.webBrandingAccess = value;
    }

    /**
     * Ruft den Wert der networkPolicyAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseAdminNetworkPolicyAccess }
     *     
     */
    public EnterpriseAdminNetworkPolicyAccess getNetworkPolicyAccess() {
        return networkPolicyAccess;
    }

    /**
     * Legt den Wert der networkPolicyAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseAdminNetworkPolicyAccess }
     *     
     */
    public void setNetworkPolicyAccess(EnterpriseAdminNetworkPolicyAccess value) {
        this.networkPolicyAccess = value;
    }

    /**
     * Ruft den Wert der dialableCallerIDAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminDialableCallerIDAccess }
     *     
     */
    public ServiceProviderAdminDialableCallerIDAccess getDialableCallerIDAccess() {
        return dialableCallerIDAccess;
    }

    /**
     * Legt den Wert der dialableCallerIDAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminDialableCallerIDAccess }
     *     
     */
    public void setDialableCallerIDAccess(ServiceProviderAdminDialableCallerIDAccess value) {
        this.dialableCallerIDAccess = value;
    }

}
