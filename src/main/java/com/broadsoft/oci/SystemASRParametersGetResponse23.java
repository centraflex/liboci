//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemASRParametersGetRequest23.
 *         Contains a list of system Application Server Registration parameters.
 *         
 *         The following elements are only used in AS data mode:
 *           enableCustomMessageControl, value "false" is returned in XS data mode
 *           customNumberOfUsersPerMessage, value "10" is returned in XS data mode
 *           customMessageIntervalMilliseconds, value "100000" is returned in XS data mode
 *       
 * 
 * <p>Java-Klasse für SystemASRParametersGetResponse23 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemASRParametersGetResponse23">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maxTransmissions" type="{}ASRMaxTransmissions"/>
 *         <element name="retransmissionDelayMilliSeconds" type="{}ASRRetransmissionDelayMilliSeconds"/>
 *         <element name="listeningPort" type="{}Port1025"/>
 *         <element name="enableCustomMessageControl" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="customNumberOfUsersPerMessage" type="{}ASRNumberOfUsersPerMessage"/>
 *         <element name="customMessageIntervalMilliseconds" type="{}ASRMessageIntervalMilliseconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemASRParametersGetResponse23", propOrder = {
    "maxTransmissions",
    "retransmissionDelayMilliSeconds",
    "listeningPort",
    "enableCustomMessageControl",
    "customNumberOfUsersPerMessage",
    "customMessageIntervalMilliseconds"
})
public class SystemASRParametersGetResponse23
    extends OCIDataResponse
{

    protected int maxTransmissions;
    protected int retransmissionDelayMilliSeconds;
    protected int listeningPort;
    protected boolean enableCustomMessageControl;
    protected int customNumberOfUsersPerMessage;
    protected int customMessageIntervalMilliseconds;

    /**
     * Ruft den Wert der maxTransmissions-Eigenschaft ab.
     * 
     */
    public int getMaxTransmissions() {
        return maxTransmissions;
    }

    /**
     * Legt den Wert der maxTransmissions-Eigenschaft fest.
     * 
     */
    public void setMaxTransmissions(int value) {
        this.maxTransmissions = value;
    }

    /**
     * Ruft den Wert der retransmissionDelayMilliSeconds-Eigenschaft ab.
     * 
     */
    public int getRetransmissionDelayMilliSeconds() {
        return retransmissionDelayMilliSeconds;
    }

    /**
     * Legt den Wert der retransmissionDelayMilliSeconds-Eigenschaft fest.
     * 
     */
    public void setRetransmissionDelayMilliSeconds(int value) {
        this.retransmissionDelayMilliSeconds = value;
    }

    /**
     * Ruft den Wert der listeningPort-Eigenschaft ab.
     * 
     */
    public int getListeningPort() {
        return listeningPort;
    }

    /**
     * Legt den Wert der listeningPort-Eigenschaft fest.
     * 
     */
    public void setListeningPort(int value) {
        this.listeningPort = value;
    }

    /**
     * Ruft den Wert der enableCustomMessageControl-Eigenschaft ab.
     * 
     */
    public boolean isEnableCustomMessageControl() {
        return enableCustomMessageControl;
    }

    /**
     * Legt den Wert der enableCustomMessageControl-Eigenschaft fest.
     * 
     */
    public void setEnableCustomMessageControl(boolean value) {
        this.enableCustomMessageControl = value;
    }

    /**
     * Ruft den Wert der customNumberOfUsersPerMessage-Eigenschaft ab.
     * 
     */
    public int getCustomNumberOfUsersPerMessage() {
        return customNumberOfUsersPerMessage;
    }

    /**
     * Legt den Wert der customNumberOfUsersPerMessage-Eigenschaft fest.
     * 
     */
    public void setCustomNumberOfUsersPerMessage(int value) {
        this.customNumberOfUsersPerMessage = value;
    }

    /**
     * Ruft den Wert der customMessageIntervalMilliseconds-Eigenschaft ab.
     * 
     */
    public int getCustomMessageIntervalMilliseconds() {
        return customMessageIntervalMilliseconds;
    }

    /**
     * Legt den Wert der customMessageIntervalMilliseconds-Eigenschaft fest.
     * 
     */
    public void setCustomMessageIntervalMilliseconds(int value) {
        this.customMessageIntervalMilliseconds = value;
    }

}
