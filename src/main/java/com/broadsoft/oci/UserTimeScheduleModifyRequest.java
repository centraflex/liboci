//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a user time schedule.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserTimeScheduleModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserTimeScheduleModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="timeScheduleName" type="{}ScheduleName"/>
 *         <element name="newTimeScheduleName" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="timeInterval01" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval02" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval03" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval04" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval05" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval06" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval07" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval08" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval09" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval10" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval11" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval12" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval13" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval14" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval15" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval16" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval17" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval18" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval19" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval20" type="{}TimeInterval" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserTimeScheduleModifyRequest", propOrder = {
    "userId",
    "timeScheduleName",
    "newTimeScheduleName",
    "timeInterval01",
    "timeInterval02",
    "timeInterval03",
    "timeInterval04",
    "timeInterval05",
    "timeInterval06",
    "timeInterval07",
    "timeInterval08",
    "timeInterval09",
    "timeInterval10",
    "timeInterval11",
    "timeInterval12",
    "timeInterval13",
    "timeInterval14",
    "timeInterval15",
    "timeInterval16",
    "timeInterval17",
    "timeInterval18",
    "timeInterval19",
    "timeInterval20"
})
public class UserTimeScheduleModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeScheduleName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newTimeScheduleName;
    @XmlElementRef(name = "timeInterval01", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval01;
    @XmlElementRef(name = "timeInterval02", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval02;
    @XmlElementRef(name = "timeInterval03", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval03;
    @XmlElementRef(name = "timeInterval04", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval04;
    @XmlElementRef(name = "timeInterval05", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval05;
    @XmlElementRef(name = "timeInterval06", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval06;
    @XmlElementRef(name = "timeInterval07", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval07;
    @XmlElementRef(name = "timeInterval08", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval08;
    @XmlElementRef(name = "timeInterval09", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval09;
    @XmlElementRef(name = "timeInterval10", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval10;
    @XmlElementRef(name = "timeInterval11", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval11;
    @XmlElementRef(name = "timeInterval12", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval12;
    @XmlElementRef(name = "timeInterval13", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval13;
    @XmlElementRef(name = "timeInterval14", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval14;
    @XmlElementRef(name = "timeInterval15", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval15;
    @XmlElementRef(name = "timeInterval16", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval16;
    @XmlElementRef(name = "timeInterval17", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval17;
    @XmlElementRef(name = "timeInterval18", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval18;
    @XmlElementRef(name = "timeInterval19", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval19;
    @XmlElementRef(name = "timeInterval20", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeInterval> timeInterval20;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der timeScheduleName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeScheduleName() {
        return timeScheduleName;
    }

    /**
     * Legt den Wert der timeScheduleName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeScheduleName(String value) {
        this.timeScheduleName = value;
    }

    /**
     * Ruft den Wert der newTimeScheduleName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewTimeScheduleName() {
        return newTimeScheduleName;
    }

    /**
     * Legt den Wert der newTimeScheduleName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewTimeScheduleName(String value) {
        this.newTimeScheduleName = value;
    }

    /**
     * Ruft den Wert der timeInterval01-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval01() {
        return timeInterval01;
    }

    /**
     * Legt den Wert der timeInterval01-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval01(JAXBElement<TimeInterval> value) {
        this.timeInterval01 = value;
    }

    /**
     * Ruft den Wert der timeInterval02-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval02() {
        return timeInterval02;
    }

    /**
     * Legt den Wert der timeInterval02-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval02(JAXBElement<TimeInterval> value) {
        this.timeInterval02 = value;
    }

    /**
     * Ruft den Wert der timeInterval03-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval03() {
        return timeInterval03;
    }

    /**
     * Legt den Wert der timeInterval03-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval03(JAXBElement<TimeInterval> value) {
        this.timeInterval03 = value;
    }

    /**
     * Ruft den Wert der timeInterval04-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval04() {
        return timeInterval04;
    }

    /**
     * Legt den Wert der timeInterval04-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval04(JAXBElement<TimeInterval> value) {
        this.timeInterval04 = value;
    }

    /**
     * Ruft den Wert der timeInterval05-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval05() {
        return timeInterval05;
    }

    /**
     * Legt den Wert der timeInterval05-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval05(JAXBElement<TimeInterval> value) {
        this.timeInterval05 = value;
    }

    /**
     * Ruft den Wert der timeInterval06-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval06() {
        return timeInterval06;
    }

    /**
     * Legt den Wert der timeInterval06-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval06(JAXBElement<TimeInterval> value) {
        this.timeInterval06 = value;
    }

    /**
     * Ruft den Wert der timeInterval07-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval07() {
        return timeInterval07;
    }

    /**
     * Legt den Wert der timeInterval07-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval07(JAXBElement<TimeInterval> value) {
        this.timeInterval07 = value;
    }

    /**
     * Ruft den Wert der timeInterval08-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval08() {
        return timeInterval08;
    }

    /**
     * Legt den Wert der timeInterval08-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval08(JAXBElement<TimeInterval> value) {
        this.timeInterval08 = value;
    }

    /**
     * Ruft den Wert der timeInterval09-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval09() {
        return timeInterval09;
    }

    /**
     * Legt den Wert der timeInterval09-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval09(JAXBElement<TimeInterval> value) {
        this.timeInterval09 = value;
    }

    /**
     * Ruft den Wert der timeInterval10-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval10() {
        return timeInterval10;
    }

    /**
     * Legt den Wert der timeInterval10-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval10(JAXBElement<TimeInterval> value) {
        this.timeInterval10 = value;
    }

    /**
     * Ruft den Wert der timeInterval11-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval11() {
        return timeInterval11;
    }

    /**
     * Legt den Wert der timeInterval11-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval11(JAXBElement<TimeInterval> value) {
        this.timeInterval11 = value;
    }

    /**
     * Ruft den Wert der timeInterval12-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval12() {
        return timeInterval12;
    }

    /**
     * Legt den Wert der timeInterval12-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval12(JAXBElement<TimeInterval> value) {
        this.timeInterval12 = value;
    }

    /**
     * Ruft den Wert der timeInterval13-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval13() {
        return timeInterval13;
    }

    /**
     * Legt den Wert der timeInterval13-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval13(JAXBElement<TimeInterval> value) {
        this.timeInterval13 = value;
    }

    /**
     * Ruft den Wert der timeInterval14-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval14() {
        return timeInterval14;
    }

    /**
     * Legt den Wert der timeInterval14-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval14(JAXBElement<TimeInterval> value) {
        this.timeInterval14 = value;
    }

    /**
     * Ruft den Wert der timeInterval15-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval15() {
        return timeInterval15;
    }

    /**
     * Legt den Wert der timeInterval15-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval15(JAXBElement<TimeInterval> value) {
        this.timeInterval15 = value;
    }

    /**
     * Ruft den Wert der timeInterval16-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval16() {
        return timeInterval16;
    }

    /**
     * Legt den Wert der timeInterval16-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval16(JAXBElement<TimeInterval> value) {
        this.timeInterval16 = value;
    }

    /**
     * Ruft den Wert der timeInterval17-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval17() {
        return timeInterval17;
    }

    /**
     * Legt den Wert der timeInterval17-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval17(JAXBElement<TimeInterval> value) {
        this.timeInterval17 = value;
    }

    /**
     * Ruft den Wert der timeInterval18-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval18() {
        return timeInterval18;
    }

    /**
     * Legt den Wert der timeInterval18-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval18(JAXBElement<TimeInterval> value) {
        this.timeInterval18 = value;
    }

    /**
     * Ruft den Wert der timeInterval19-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval19() {
        return timeInterval19;
    }

    /**
     * Legt den Wert der timeInterval19-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval19(JAXBElement<TimeInterval> value) {
        this.timeInterval19 = value;
    }

    /**
     * Ruft den Wert der timeInterval20-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public JAXBElement<TimeInterval> getTimeInterval20() {
        return timeInterval20;
    }

    /**
     * Legt den Wert der timeInterval20-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeInterval }{@code >}
     *     
     */
    public void setTimeInterval20(JAXBElement<TimeInterval> value) {
        this.timeInterval20 = value;
    }

}
