//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to EnterprisePhoneDirectoryGetListRequest17.
 *         Contains a table with  a row for each phone number and column headings :
 *         "Name", "Number", "Extension", "Mobile", "Email Address", 
 *         "Department", "Hiragana Name", "Group Id", "Yahoo Id", "Is Virtual On-Net User".
 *         If extended directory information is requested, the following columns are also included:
 *         "First Name", "Last Name", "User Id", "Pager", "Title", "Time Zone", 
 *         "Location", "Address Line 1", "Address Line 2",
 *         "City", "State", "Zip", "Country".
 *         
 *         Replaced by: EnterprisePhoneDirectoryGetListResponse18
 *       
 * 
 * <p>Java-Klasse für EnterprisePhoneDirectoryGetListResponse17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterprisePhoneDirectoryGetListResponse17">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="directoryTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterprisePhoneDirectoryGetListResponse17", propOrder = {
    "directoryTable"
})
public class EnterprisePhoneDirectoryGetListResponse17
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable directoryTable;

    /**
     * Ruft den Wert der directoryTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDirectoryTable() {
        return directoryTable;
    }

    /**
     * Legt den Wert der directoryTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDirectoryTable(OCITable value) {
        this.directoryTable = value;
    }

}
