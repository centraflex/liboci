//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EndpointType21sp1.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="EndpointType21sp1">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Primary"/>
 *     <enumeration value="Shared Call Appearance"/>
 *     <enumeration value="Video Add On"/>
 *     <enumeration value="Public Service Identity"/>
 *     <enumeration value="Music On Hold Internal"/>
 *     <enumeration value="Flexible Seating Guest"/>
 *     <enumeration value="Mobility"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "EndpointType21sp1")
@XmlEnum
public enum EndpointType21Sp1 {

    @XmlEnumValue("Primary")
    PRIMARY("Primary"),
    @XmlEnumValue("Shared Call Appearance")
    SHARED_CALL_APPEARANCE("Shared Call Appearance"),
    @XmlEnumValue("Video Add On")
    VIDEO_ADD_ON("Video Add On"),
    @XmlEnumValue("Public Service Identity")
    PUBLIC_SERVICE_IDENTITY("Public Service Identity"),
    @XmlEnumValue("Music On Hold Internal")
    MUSIC_ON_HOLD_INTERNAL("Music On Hold Internal"),
    @XmlEnumValue("Flexible Seating Guest")
    FLEXIBLE_SEATING_GUEST("Flexible Seating Guest"),
    @XmlEnumValue("Mobility")
    MOBILITY("Mobility");
    private final String value;

    EndpointType21Sp1(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EndpointType21Sp1 fromValue(String v) {
        for (EndpointType21Sp1 c: EndpointType21Sp1 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
