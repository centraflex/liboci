//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemXsiPolicyProfileGetListRequest
 *         Contains a table with column headings: "Name", "Level", "Description", "Default".
 *       
 * 
 * <p>Java-Klasse für SystemXsiPolicyProfileGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemXsiPolicyProfileGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="xsiPolicyProfileTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemXsiPolicyProfileGetListResponse", propOrder = {
    "xsiPolicyProfileTable"
})
public class SystemXsiPolicyProfileGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable xsiPolicyProfileTable;

    /**
     * Ruft den Wert der xsiPolicyProfileTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getXsiPolicyProfileTable() {
        return xsiPolicyProfileTable;
    }

    /**
     * Legt den Wert der xsiPolicyProfileTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setXsiPolicyProfileTable(OCITable value) {
        this.xsiPolicyProfileTable = value;
    }

}
