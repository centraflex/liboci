//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserThirdPartyVoiceMailSupportGetRequest13mp16.
 *         Replaced by: UserThirdPartyVoiceMailSupportGetResponse17
 *       
 * 
 * <p>Java-Klasse für UserThirdPartyVoiceMailSupportGetResponse13mp16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserThirdPartyVoiceMailSupportGetResponse13mp16">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="busyRedirectToVoiceMail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="noAnswerRedirectToVoiceMail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="serverSelection" type="{}ThirdPartyVoiceMailSupportServerSelection"/>
 *         <element name="userServer" type="{}ThirdPartyVoiceMailSupportMailServer" minOccurs="0"/>
 *         <element name="mailboxIdType" type="{}ThirdPartyVoiceMailSupportMailboxIdType"/>
 *         <element name="mailboxURL" type="{}SIPURI" minOccurs="0"/>
 *         <element name="noAnswerNumberOfRings" type="{}ThirdPartyVoiceMailSupportNumberOfRings"/>
 *         <element name="alwaysRedirectToVoiceMail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserThirdPartyVoiceMailSupportGetResponse13mp16", propOrder = {
    "isActive",
    "busyRedirectToVoiceMail",
    "noAnswerRedirectToVoiceMail",
    "serverSelection",
    "userServer",
    "mailboxIdType",
    "mailboxURL",
    "noAnswerNumberOfRings",
    "alwaysRedirectToVoiceMail"
})
public class UserThirdPartyVoiceMailSupportGetResponse13Mp16
    extends OCIDataResponse
{

    protected boolean isActive;
    protected boolean busyRedirectToVoiceMail;
    protected boolean noAnswerRedirectToVoiceMail;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ThirdPartyVoiceMailSupportServerSelection serverSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userServer;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ThirdPartyVoiceMailSupportMailboxIdType mailboxIdType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mailboxURL;
    protected int noAnswerNumberOfRings;
    protected boolean alwaysRedirectToVoiceMail;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der busyRedirectToVoiceMail-Eigenschaft ab.
     * 
     */
    public boolean isBusyRedirectToVoiceMail() {
        return busyRedirectToVoiceMail;
    }

    /**
     * Legt den Wert der busyRedirectToVoiceMail-Eigenschaft fest.
     * 
     */
    public void setBusyRedirectToVoiceMail(boolean value) {
        this.busyRedirectToVoiceMail = value;
    }

    /**
     * Ruft den Wert der noAnswerRedirectToVoiceMail-Eigenschaft ab.
     * 
     */
    public boolean isNoAnswerRedirectToVoiceMail() {
        return noAnswerRedirectToVoiceMail;
    }

    /**
     * Legt den Wert der noAnswerRedirectToVoiceMail-Eigenschaft fest.
     * 
     */
    public void setNoAnswerRedirectToVoiceMail(boolean value) {
        this.noAnswerRedirectToVoiceMail = value;
    }

    /**
     * Ruft den Wert der serverSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ThirdPartyVoiceMailSupportServerSelection }
     *     
     */
    public ThirdPartyVoiceMailSupportServerSelection getServerSelection() {
        return serverSelection;
    }

    /**
     * Legt den Wert der serverSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ThirdPartyVoiceMailSupportServerSelection }
     *     
     */
    public void setServerSelection(ThirdPartyVoiceMailSupportServerSelection value) {
        this.serverSelection = value;
    }

    /**
     * Ruft den Wert der userServer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserServer() {
        return userServer;
    }

    /**
     * Legt den Wert der userServer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserServer(String value) {
        this.userServer = value;
    }

    /**
     * Ruft den Wert der mailboxIdType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ThirdPartyVoiceMailSupportMailboxIdType }
     *     
     */
    public ThirdPartyVoiceMailSupportMailboxIdType getMailboxIdType() {
        return mailboxIdType;
    }

    /**
     * Legt den Wert der mailboxIdType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ThirdPartyVoiceMailSupportMailboxIdType }
     *     
     */
    public void setMailboxIdType(ThirdPartyVoiceMailSupportMailboxIdType value) {
        this.mailboxIdType = value;
    }

    /**
     * Ruft den Wert der mailboxURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailboxURL() {
        return mailboxURL;
    }

    /**
     * Legt den Wert der mailboxURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailboxURL(String value) {
        this.mailboxURL = value;
    }

    /**
     * Ruft den Wert der noAnswerNumberOfRings-Eigenschaft ab.
     * 
     */
    public int getNoAnswerNumberOfRings() {
        return noAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der noAnswerNumberOfRings-Eigenschaft fest.
     * 
     */
    public void setNoAnswerNumberOfRings(int value) {
        this.noAnswerNumberOfRings = value;
    }

    /**
     * Ruft den Wert der alwaysRedirectToVoiceMail-Eigenschaft ab.
     * 
     */
    public boolean isAlwaysRedirectToVoiceMail() {
        return alwaysRedirectToVoiceMail;
    }

    /**
     * Legt den Wert der alwaysRedirectToVoiceMail-Eigenschaft fest.
     * 
     */
    public void setAlwaysRedirectToVoiceMail(boolean value) {
        this.alwaysRedirectToVoiceMail = value;
    }

}
