//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupRoutePointDistinctiveRingingGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupRoutePointDistinctiveRingingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupRoutePointDistinctiveRingingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableDistinctiveRinging" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="distinctiveRingingRingPattern" type="{}RingPattern"/>
 *         <element name="distinctiveRingingForceDeliveryRingPattern" type="{}RingPattern"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupRoutePointDistinctiveRingingGetResponse", propOrder = {
    "enableDistinctiveRinging",
    "distinctiveRingingRingPattern",
    "distinctiveRingingForceDeliveryRingPattern"
})
public class GroupRoutePointDistinctiveRingingGetResponse
    extends OCIDataResponse
{

    protected boolean enableDistinctiveRinging;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected RingPattern distinctiveRingingRingPattern;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected RingPattern distinctiveRingingForceDeliveryRingPattern;

    /**
     * Ruft den Wert der enableDistinctiveRinging-Eigenschaft ab.
     * 
     */
    public boolean isEnableDistinctiveRinging() {
        return enableDistinctiveRinging;
    }

    /**
     * Legt den Wert der enableDistinctiveRinging-Eigenschaft fest.
     * 
     */
    public void setEnableDistinctiveRinging(boolean value) {
        this.enableDistinctiveRinging = value;
    }

    /**
     * Ruft den Wert der distinctiveRingingRingPattern-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RingPattern }
     *     
     */
    public RingPattern getDistinctiveRingingRingPattern() {
        return distinctiveRingingRingPattern;
    }

    /**
     * Legt den Wert der distinctiveRingingRingPattern-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RingPattern }
     *     
     */
    public void setDistinctiveRingingRingPattern(RingPattern value) {
        this.distinctiveRingingRingPattern = value;
    }

    /**
     * Ruft den Wert der distinctiveRingingForceDeliveryRingPattern-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RingPattern }
     *     
     */
    public RingPattern getDistinctiveRingingForceDeliveryRingPattern() {
        return distinctiveRingingForceDeliveryRingPattern;
    }

    /**
     * Legt den Wert der distinctiveRingingForceDeliveryRingPattern-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RingPattern }
     *     
     */
    public void setDistinctiveRingingForceDeliveryRingPattern(RingPattern value) {
        this.distinctiveRingingForceDeliveryRingPattern = value;
    }

}
