//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemLicensingGetRequest21sp1. The subscriber license table columns are: "Name", "Licensed", "Used" and "Available".
 *         The group service license table columns are: "Name", "Licensed", "Used" and "Available".
 *         The virtual service license table columns are: "Name", "Licensed", "Used" and "Available".
 *         The user service license table columns are: "Name", "Licensed", "Used", "Available", "Used By Hosted Users", "Used By Trunk Users", and "Expiration Date".
 *         The system param license table columns are: "Name", "Licensed", "Used", Available".
 *       
 * 
 * <p>Java-Klasse für SystemLicensingGetResponse21sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemLicensingGetResponse21sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="licenseStrictness" type="{}LicenseStrictness"/>
 *         <element name="groupUserlimit" type="{}GroupUserLicenseLimit"/>
 *         <element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         <element name="hostId" type="{}ServerHostId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="licenseName" type="{}LicenseName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="numberOfTrunkUsers" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="systemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         <element name="subscriberLicenseTable" type="{C}OCITable"/>
 *         <element name="groupServiceLicenseTable" type="{C}OCITable"/>
 *         <element name="virtualServiceLicenseTable" type="{C}OCITable"/>
 *         <element name="userServiceLicenseTable" type="{C}OCITable"/>
 *         <element name="systemParamLicenseTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemLicensingGetResponse21sp1", propOrder = {
    "licenseStrictness",
    "groupUserlimit",
    "expirationDate",
    "hostId",
    "licenseName",
    "numberOfTrunkUsers",
    "systemId",
    "subscriberLicenseTable",
    "groupServiceLicenseTable",
    "virtualServiceLicenseTable",
    "userServiceLicenseTable",
    "systemParamLicenseTable"
})
public class SystemLicensingGetResponse21Sp1
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected LicenseStrictness licenseStrictness;
    protected int groupUserlimit;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDate;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> hostId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> licenseName;
    protected int numberOfTrunkUsers;
    protected String systemId;
    @XmlElement(required = true)
    protected OCITable subscriberLicenseTable;
    @XmlElement(required = true)
    protected OCITable groupServiceLicenseTable;
    @XmlElement(required = true)
    protected OCITable virtualServiceLicenseTable;
    @XmlElement(required = true)
    protected OCITable userServiceLicenseTable;
    @XmlElement(required = true)
    protected OCITable systemParamLicenseTable;

    /**
     * Ruft den Wert der licenseStrictness-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LicenseStrictness }
     *     
     */
    public LicenseStrictness getLicenseStrictness() {
        return licenseStrictness;
    }

    /**
     * Legt den Wert der licenseStrictness-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseStrictness }
     *     
     */
    public void setLicenseStrictness(LicenseStrictness value) {
        this.licenseStrictness = value;
    }

    /**
     * Ruft den Wert der groupUserlimit-Eigenschaft ab.
     * 
     */
    public int getGroupUserlimit() {
        return groupUserlimit;
    }

    /**
     * Legt den Wert der groupUserlimit-Eigenschaft fest.
     * 
     */
    public void setGroupUserlimit(int value) {
        this.groupUserlimit = value;
    }

    /**
     * Ruft den Wert der expirationDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Legt den Wert der expirationDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the hostId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the hostId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHostId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the hostId property.
     */
    public List<String> getHostId() {
        if (hostId == null) {
            hostId = new ArrayList<>();
        }
        return this.hostId;
    }

    /**
     * Gets the value of the licenseName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the licenseName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLicenseName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the licenseName property.
     */
    public List<String> getLicenseName() {
        if (licenseName == null) {
            licenseName = new ArrayList<>();
        }
        return this.licenseName;
    }

    /**
     * Ruft den Wert der numberOfTrunkUsers-Eigenschaft ab.
     * 
     */
    public int getNumberOfTrunkUsers() {
        return numberOfTrunkUsers;
    }

    /**
     * Legt den Wert der numberOfTrunkUsers-Eigenschaft fest.
     * 
     */
    public void setNumberOfTrunkUsers(int value) {
        this.numberOfTrunkUsers = value;
    }

    /**
     * Ruft den Wert der systemId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemId() {
        return systemId;
    }

    /**
     * Legt den Wert der systemId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemId(String value) {
        this.systemId = value;
    }

    /**
     * Ruft den Wert der subscriberLicenseTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getSubscriberLicenseTable() {
        return subscriberLicenseTable;
    }

    /**
     * Legt den Wert der subscriberLicenseTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setSubscriberLicenseTable(OCITable value) {
        this.subscriberLicenseTable = value;
    }

    /**
     * Ruft den Wert der groupServiceLicenseTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getGroupServiceLicenseTable() {
        return groupServiceLicenseTable;
    }

    /**
     * Legt den Wert der groupServiceLicenseTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setGroupServiceLicenseTable(OCITable value) {
        this.groupServiceLicenseTable = value;
    }

    /**
     * Ruft den Wert der virtualServiceLicenseTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getVirtualServiceLicenseTable() {
        return virtualServiceLicenseTable;
    }

    /**
     * Legt den Wert der virtualServiceLicenseTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setVirtualServiceLicenseTable(OCITable value) {
        this.virtualServiceLicenseTable = value;
    }

    /**
     * Ruft den Wert der userServiceLicenseTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserServiceLicenseTable() {
        return userServiceLicenseTable;
    }

    /**
     * Legt den Wert der userServiceLicenseTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserServiceLicenseTable(OCITable value) {
        this.userServiceLicenseTable = value;
    }

    /**
     * Ruft den Wert der systemParamLicenseTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getSystemParamLicenseTable() {
        return systemParamLicenseTable;
    }

    /**
     * Legt den Wert der systemParamLicenseTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setSystemParamLicenseTable(OCITable value) {
        this.systemParamLicenseTable = value;
    }

}
