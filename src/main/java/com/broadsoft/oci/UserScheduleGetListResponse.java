//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserScheduleGetListRequest.
 *         The response contains a list of system schedules.
 *       
 * 
 * <p>Java-Klasse für UserScheduleGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserScheduleGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="scheduleName" type="{}ScheduleName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="scheduleType" type="{}ScheduleType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="scheduleLevel" type="{}ScheduleLevel" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserScheduleGetListResponse", propOrder = {
    "scheduleName",
    "scheduleType",
    "scheduleLevel"
})
public class UserScheduleGetListResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> scheduleName;
    @XmlSchemaType(name = "token")
    protected List<ScheduleType> scheduleType;
    @XmlSchemaType(name = "token")
    protected List<ScheduleLevel> scheduleLevel;

    /**
     * Gets the value of the scheduleName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the scheduleName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScheduleName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the scheduleName property.
     */
    public List<String> getScheduleName() {
        if (scheduleName == null) {
            scheduleName = new ArrayList<>();
        }
        return this.scheduleName;
    }

    /**
     * Gets the value of the scheduleType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the scheduleType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScheduleType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ScheduleType }
     * 
     * 
     * @return
     *     The value of the scheduleType property.
     */
    public List<ScheduleType> getScheduleType() {
        if (scheduleType == null) {
            scheduleType = new ArrayList<>();
        }
        return this.scheduleType;
    }

    /**
     * Gets the value of the scheduleLevel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the scheduleLevel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScheduleLevel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ScheduleLevel }
     * 
     * 
     * @return
     *     The value of the scheduleLevel property.
     */
    public List<ScheduleLevel> getScheduleLevel() {
        if (scheduleLevel == null) {
            scheduleLevel = new ArrayList<>();
        }
        return this.scheduleLevel;
    }

}
