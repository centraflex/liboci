//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für HuntPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="HuntPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Circular"/>
 *     <enumeration value="Regular"/>
 *     <enumeration value="Simultaneous"/>
 *     <enumeration value="Uniform"/>
 *     <enumeration value="Weighted"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "HuntPolicy")
@XmlEnum
public enum HuntPolicy {

    @XmlEnumValue("Circular")
    CIRCULAR("Circular"),
    @XmlEnumValue("Regular")
    REGULAR("Regular"),
    @XmlEnumValue("Simultaneous")
    SIMULTANEOUS("Simultaneous"),
    @XmlEnumValue("Uniform")
    UNIFORM("Uniform"),
    @XmlEnumValue("Weighted")
    WEIGHTED("Weighted");
    private final String value;

    HuntPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HuntPolicy fromValue(String v) {
        for (HuntPolicy c: HuntPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
