//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für VoiceMessagingMailServerProtocol.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="VoiceMessagingMailServerProtocol">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="POP3"/>
 *     <enumeration value="IMAP"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "VoiceMessagingMailServerProtocol")
@XmlEnum
public enum VoiceMessagingMailServerProtocol {

    @XmlEnumValue("POP3")
    POP_3("POP3"),
    IMAP("IMAP");
    private final String value;

    VoiceMessagingMailServerProtocol(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VoiceMessagingMailServerProtocol fromValue(String v) {
        for (VoiceMessagingMailServerProtocol c: VoiceMessagingMailServerProtocol.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
