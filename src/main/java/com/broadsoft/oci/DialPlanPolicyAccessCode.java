//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Dial Plan Access Code attributes.
 *       
 * 
 * <p>Java-Klasse für DialPlanPolicyAccessCode complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="DialPlanPolicyAccessCode">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="accessCode" type="{}DialPlanAccessCode"/>
 *         <element name="description" type="{}DialPlanAccessCodeDescription" minOccurs="0"/>
 *         <element name="includeCodeForNetworkTranslationsAndRouting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeCodeForScreeningServices" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableSecondaryDialTone" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DialPlanPolicyAccessCode", propOrder = {
    "accessCode",
    "description",
    "includeCodeForNetworkTranslationsAndRouting",
    "includeCodeForScreeningServices",
    "enableSecondaryDialTone"
})
public class DialPlanPolicyAccessCode {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accessCode;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    protected Boolean includeCodeForNetworkTranslationsAndRouting;
    protected Boolean includeCodeForScreeningServices;
    protected Boolean enableSecondaryDialTone;

    /**
     * Ruft den Wert der accessCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessCode() {
        return accessCode;
    }

    /**
     * Legt den Wert der accessCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessCode(String value) {
        this.accessCode = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der includeCodeForNetworkTranslationsAndRouting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeCodeForNetworkTranslationsAndRouting() {
        return includeCodeForNetworkTranslationsAndRouting;
    }

    /**
     * Legt den Wert der includeCodeForNetworkTranslationsAndRouting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeCodeForNetworkTranslationsAndRouting(Boolean value) {
        this.includeCodeForNetworkTranslationsAndRouting = value;
    }

    /**
     * Ruft den Wert der includeCodeForScreeningServices-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeCodeForScreeningServices() {
        return includeCodeForScreeningServices;
    }

    /**
     * Legt den Wert der includeCodeForScreeningServices-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeCodeForScreeningServices(Boolean value) {
        this.includeCodeForScreeningServices = value;
    }

    /**
     * Ruft den Wert der enableSecondaryDialTone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableSecondaryDialTone() {
        return enableSecondaryDialTone;
    }

    /**
     * Legt den Wert der enableSecondaryDialTone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSecondaryDialTone(Boolean value) {
        this.enableSecondaryDialTone = value;
    }

}
