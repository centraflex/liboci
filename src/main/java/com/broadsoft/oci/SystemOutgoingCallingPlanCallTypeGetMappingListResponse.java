//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemOutgoingCallingPlanCallTypeGetMappingListRequest. The table columns are:
 *         "Country Code", "Digit Map" and "Call Type".
 *       
 * 
 * <p>Java-Klasse für SystemOutgoingCallingPlanCallTypeGetMappingListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemOutgoingCallingPlanCallTypeGetMappingListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="callTypeMapping" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemOutgoingCallingPlanCallTypeGetMappingListResponse", propOrder = {
    "callTypeMapping"
})
public class SystemOutgoingCallingPlanCallTypeGetMappingListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable callTypeMapping;

    /**
     * Ruft den Wert der callTypeMapping-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getCallTypeMapping() {
        return callTypeMapping;
    }

    /**
     * Legt den Wert der callTypeMapping-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setCallTypeMapping(OCITable value) {
        this.callTypeMapping = value;
    }

}
