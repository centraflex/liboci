//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 *         
 *          Response to the UserCallForwardingSelectiveGetRequest. The criteria table's column headings are:        
 *          "Is Active", "Criteria Name", "Time Schedule", "Calls From" and "Forward To".      
 *       
 * 
 * <p>Java-Klasse für UserCallForwardingSelectiveGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallForwardingSelectiveGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="defaultForwardToPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="playRingReminder" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="criteriaTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallForwardingSelectiveGetResponse", propOrder = {
    "defaultForwardToPhoneNumber",
    "playRingReminder",
    "criteriaTable"
})
public class UserCallForwardingSelectiveGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultForwardToPhoneNumber;
    protected boolean playRingReminder;
    @XmlElement(required = true)
    protected OCITable criteriaTable;

    /**
     * Ruft den Wert der defaultForwardToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultForwardToPhoneNumber() {
        return defaultForwardToPhoneNumber;
    }

    /**
     * Legt den Wert der defaultForwardToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultForwardToPhoneNumber(String value) {
        this.defaultForwardToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der playRingReminder-Eigenschaft ab.
     * 
     */
    public boolean isPlayRingReminder() {
        return playRingReminder;
    }

    /**
     * Legt den Wert der playRingReminder-Eigenschaft fest.
     * 
     */
    public void setPlayRingReminder(boolean value) {
        this.playRingReminder = value;
    }

    /**
     * Ruft den Wert der criteriaTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getCriteriaTable() {
        return criteriaTable;
    }

    /**
     * Legt den Wert der criteriaTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setCriteriaTable(OCITable value) {
        this.criteriaTable = value;
    }

}
