//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemServicePackMigrationGetRequest.
 *         The response contains the Service Pack Migration system level settings.
 *       
 * 
 * <p>Java-Klasse für SystemServicePackMigrationGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemServicePackMigrationGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maxSimultaneousMigrationTasks" type="{}MaxSimultaneousMigrationTasks"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemServicePackMigrationGetResponse", propOrder = {
    "maxSimultaneousMigrationTasks"
})
public class SystemServicePackMigrationGetResponse
    extends OCIDataResponse
{

    protected int maxSimultaneousMigrationTasks;

    /**
     * Ruft den Wert der maxSimultaneousMigrationTasks-Eigenschaft ab.
     * 
     */
    public int getMaxSimultaneousMigrationTasks() {
        return maxSimultaneousMigrationTasks;
    }

    /**
     * Legt den Wert der maxSimultaneousMigrationTasks-Eigenschaft fest.
     * 
     */
    public void setMaxSimultaneousMigrationTasks(int value) {
        this.maxSimultaneousMigrationTasks = value;
    }

}
