//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         CPE device's options when used with a modify request.
 *         The following options are not changeable:
 *           configType
 *           systemFileName
 *           deviceFileFormat
 *       
 * 
 * <p>Java-Klasse für CPEDeviceModifyOptions complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CPEDeviceModifyOptions">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="enableMonitoring" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="resetEvent" type="{}CPEDeviceResetEventType" minOccurs="0"/>
 *         <element name="deviceManagementDeviceTypeOptions" type="{}DeviceManagementDeviceTypeModifyOptions" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CPEDeviceModifyOptions", propOrder = {
    "enableMonitoring",
    "resetEvent",
    "deviceManagementDeviceTypeOptions"
})
public class CPEDeviceModifyOptions {

    protected Boolean enableMonitoring;
    @XmlElementRef(name = "resetEvent", type = JAXBElement.class, required = false)
    protected JAXBElement<CPEDeviceResetEventType> resetEvent;
    protected DeviceManagementDeviceTypeModifyOptions deviceManagementDeviceTypeOptions;

    /**
     * Ruft den Wert der enableMonitoring-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMonitoring() {
        return enableMonitoring;
    }

    /**
     * Legt den Wert der enableMonitoring-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMonitoring(Boolean value) {
        this.enableMonitoring = value;
    }

    /**
     * Ruft den Wert der resetEvent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CPEDeviceResetEventType }{@code >}
     *     
     */
    public JAXBElement<CPEDeviceResetEventType> getResetEvent() {
        return resetEvent;
    }

    /**
     * Legt den Wert der resetEvent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CPEDeviceResetEventType }{@code >}
     *     
     */
    public void setResetEvent(JAXBElement<CPEDeviceResetEventType> value) {
        this.resetEvent = value;
    }

    /**
     * Ruft den Wert der deviceManagementDeviceTypeOptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementDeviceTypeModifyOptions }
     *     
     */
    public DeviceManagementDeviceTypeModifyOptions getDeviceManagementDeviceTypeOptions() {
        return deviceManagementDeviceTypeOptions;
    }

    /**
     * Legt den Wert der deviceManagementDeviceTypeOptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementDeviceTypeModifyOptions }
     *     
     */
    public void setDeviceManagementDeviceTypeOptions(DeviceManagementDeviceTypeModifyOptions value) {
        this.deviceManagementDeviceTypeOptions = value;
    }

}
