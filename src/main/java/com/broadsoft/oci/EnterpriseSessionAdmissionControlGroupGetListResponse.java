//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to EnterpriseSessionAdmissionControlGroupGetListRequest.
 *         Contains a table of session admission control group configured in the enterprise.
 *         The column headings are: "Name", "Is Default", "Maximum Sessions", "Maximum Originating Sessions", "Maximum Terminating Sessions"..
 *       
 * 
 * <p>Java-Klasse für EnterpriseSessionAdmissionControlGroupGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseSessionAdmissionControlGroupGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="sessionAdmissionControlGroupTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseSessionAdmissionControlGroupGetListResponse", propOrder = {
    "sessionAdmissionControlGroupTable"
})
public class EnterpriseSessionAdmissionControlGroupGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable sessionAdmissionControlGroupTable;

    /**
     * Ruft den Wert der sessionAdmissionControlGroupTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getSessionAdmissionControlGroupTable() {
        return sessionAdmissionControlGroupTable;
    }

    /**
     * Legt den Wert der sessionAdmissionControlGroupTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setSessionAdmissionControlGroupTable(OCITable value) {
        this.sessionAdmissionControlGroupTable = value;
    }

}
