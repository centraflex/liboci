//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify SMDI system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemSMDIParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSMDIParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enableSMDI" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="listeningPort" type="{}Port1025" minOccurs="0"/>
 *         <element name="maxConnections" type="{}SMDIMaxConnections" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSMDIParametersModifyRequest", propOrder = {
    "enableSMDI",
    "listeningPort",
    "maxConnections"
})
public class SystemSMDIParametersModifyRequest
    extends OCIRequest
{

    protected Boolean enableSMDI;
    protected Integer listeningPort;
    protected Integer maxConnections;

    /**
     * Ruft den Wert der enableSMDI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableSMDI() {
        return enableSMDI;
    }

    /**
     * Legt den Wert der enableSMDI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSMDI(Boolean value) {
        this.enableSMDI = value;
    }

    /**
     * Ruft den Wert der listeningPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getListeningPort() {
        return listeningPort;
    }

    /**
     * Legt den Wert der listeningPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setListeningPort(Integer value) {
        this.listeningPort = value;
    }

    /**
     * Ruft den Wert der maxConnections-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxConnections() {
        return maxConnections;
    }

    /**
     * Legt den Wert der maxConnections-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxConnections(Integer value) {
        this.maxConnections = value;
    }

}
