//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Get a list of Call Center instances within a group.
 *         The response is either GroupCallCenterGetInstancePagedSortedListResponse or ErrorResponse.
 *         It is possible to get the instances within a specified department.
 *         
 *         If the responsePagingControl element is not provided, the paging startIndex will be set to 1 
 *         by default, and the responsePageSize will be set to the maximum ResponsePageSize by default.
 *         It is possible to search by various criteria to restrict the number of rows returned.
 *         
 *         If no sortOrder is included the response is sorted by User Id ascending by default.
 *         
 *         Multiple search criteria are logically ANDed together unless the searchCriteriaModeOr option is included.
 *         Then the search criteria are logically ORed together.
 *         
 *         ErrorResponse is returned if searchCriteriaExactHuntPolicy or searchCriteriaExactCallCenterType have multiple entries and searchCriteriaModeOr is not include.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterGetInstancePagedSortedListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterGetInstancePagedSortedListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl" minOccurs="0"/>
 *         <element name="sortOrder" type="{}SortOrderGroupCallCenterGetInstancePagedSortedList" maxOccurs="3" minOccurs="0"/>
 *         <element name="searchCriteriaUserId" type="{}SearchCriteriaUserId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaCallCenterName" type="{}SearchCriteriaCallCenterName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDn" type="{}SearchCriteriaDn" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExtension" type="{}SearchCriteriaExtension" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDepartmentName" type="{}SearchCriteriaDepartmentName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaServiceStatus" type="{}SearchCriteriaServiceStatus" minOccurs="0"/>
 *         <element name="searchCriteriaExactHuntPolicy" type="{}SearchCriteriaExactHuntPolicy" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactCallCenterType" type="{}SearchCriteriaExactCallCenterType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaModeOr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterGetInstancePagedSortedListRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "responsePagingControl",
    "sortOrder",
    "searchCriteriaUserId",
    "searchCriteriaCallCenterName",
    "searchCriteriaDn",
    "searchCriteriaExtension",
    "searchCriteriaDepartmentName",
    "searchCriteriaServiceStatus",
    "searchCriteriaExactHuntPolicy",
    "searchCriteriaExactCallCenterType",
    "searchCriteriaModeOr"
})
public class GroupCallCenterGetInstancePagedSortedListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected ResponsePagingControl responsePagingControl;
    protected List<SortOrderGroupCallCenterGetInstancePagedSortedList> sortOrder;
    protected List<SearchCriteriaUserId> searchCriteriaUserId;
    protected List<SearchCriteriaCallCenterName> searchCriteriaCallCenterName;
    protected List<SearchCriteriaDn> searchCriteriaDn;
    protected List<SearchCriteriaExtension> searchCriteriaExtension;
    protected List<SearchCriteriaDepartmentName> searchCriteriaDepartmentName;
    protected SearchCriteriaServiceStatus searchCriteriaServiceStatus;
    protected List<SearchCriteriaExactHuntPolicy> searchCriteriaExactHuntPolicy;
    protected List<SearchCriteriaExactCallCenterType> searchCriteriaExactCallCenterType;
    protected Boolean searchCriteriaModeOr;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Gets the value of the sortOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the sortOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSortOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SortOrderGroupCallCenterGetInstancePagedSortedList }
     * 
     * 
     * @return
     *     The value of the sortOrder property.
     */
    public List<SortOrderGroupCallCenterGetInstancePagedSortedList> getSortOrder() {
        if (sortOrder == null) {
            sortOrder = new ArrayList<>();
        }
        return this.sortOrder;
    }

    /**
     * Gets the value of the searchCriteriaUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserId property.
     */
    public List<SearchCriteriaUserId> getSearchCriteriaUserId() {
        if (searchCriteriaUserId == null) {
            searchCriteriaUserId = new ArrayList<>();
        }
        return this.searchCriteriaUserId;
    }

    /**
     * Gets the value of the searchCriteriaCallCenterName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaCallCenterName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaCallCenterName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaCallCenterName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaCallCenterName property.
     */
    public List<SearchCriteriaCallCenterName> getSearchCriteriaCallCenterName() {
        if (searchCriteriaCallCenterName == null) {
            searchCriteriaCallCenterName = new ArrayList<>();
        }
        return this.searchCriteriaCallCenterName;
    }

    /**
     * Gets the value of the searchCriteriaDn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDn }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDn property.
     */
    public List<SearchCriteriaDn> getSearchCriteriaDn() {
        if (searchCriteriaDn == null) {
            searchCriteriaDn = new ArrayList<>();
        }
        return this.searchCriteriaDn;
    }

    /**
     * Gets the value of the searchCriteriaExtension property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaExtension property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaExtension().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaExtension }
     * 
     * 
     * @return
     *     The value of the searchCriteriaExtension property.
     */
    public List<SearchCriteriaExtension> getSearchCriteriaExtension() {
        if (searchCriteriaExtension == null) {
            searchCriteriaExtension = new ArrayList<>();
        }
        return this.searchCriteriaExtension;
    }

    /**
     * Gets the value of the searchCriteriaDepartmentName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDepartmentName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDepartmentName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDepartmentName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDepartmentName property.
     */
    public List<SearchCriteriaDepartmentName> getSearchCriteriaDepartmentName() {
        if (searchCriteriaDepartmentName == null) {
            searchCriteriaDepartmentName = new ArrayList<>();
        }
        return this.searchCriteriaDepartmentName;
    }

    /**
     * Ruft den Wert der searchCriteriaServiceStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaServiceStatus }
     *     
     */
    public SearchCriteriaServiceStatus getSearchCriteriaServiceStatus() {
        return searchCriteriaServiceStatus;
    }

    /**
     * Legt den Wert der searchCriteriaServiceStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaServiceStatus }
     *     
     */
    public void setSearchCriteriaServiceStatus(SearchCriteriaServiceStatus value) {
        this.searchCriteriaServiceStatus = value;
    }

    /**
     * Gets the value of the searchCriteriaExactHuntPolicy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaExactHuntPolicy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaExactHuntPolicy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaExactHuntPolicy }
     * 
     * 
     * @return
     *     The value of the searchCriteriaExactHuntPolicy property.
     */
    public List<SearchCriteriaExactHuntPolicy> getSearchCriteriaExactHuntPolicy() {
        if (searchCriteriaExactHuntPolicy == null) {
            searchCriteriaExactHuntPolicy = new ArrayList<>();
        }
        return this.searchCriteriaExactHuntPolicy;
    }

    /**
     * Gets the value of the searchCriteriaExactCallCenterType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaExactCallCenterType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaExactCallCenterType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaExactCallCenterType }
     * 
     * 
     * @return
     *     The value of the searchCriteriaExactCallCenterType property.
     */
    public List<SearchCriteriaExactCallCenterType> getSearchCriteriaExactCallCenterType() {
        if (searchCriteriaExactCallCenterType == null) {
            searchCriteriaExactCallCenterType = new ArrayList<>();
        }
        return this.searchCriteriaExactCallCenterType;
    }

    /**
     * Ruft den Wert der searchCriteriaModeOr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchCriteriaModeOr() {
        return searchCriteriaModeOr;
    }

    /**
     * Legt den Wert der searchCriteriaModeOr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchCriteriaModeOr(Boolean value) {
        this.searchCriteriaModeOr = value;
    }

}
