//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupTrunkGroupGetInstanceRequest.
 *         The publicUserIdentity in the ServiceInstanceReadProfile is not used for trunk groups.
 *         Returns the profile information for the Trunk Group.
 *       
 * 
 * <p>Java-Klasse für GroupTrunkGroupGetInstanceResponse14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupTrunkGroupGetInstanceResponse14">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceReadProfile"/>
 *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointRead14" minOccurs="0"/>
 *         <element name="maxActiveCalls" type="{}MaxActiveCalls"/>
 *         <element name="maxIncomingCalls" type="{}MaxIncomingCalls" minOccurs="0"/>
 *         <element name="maxOutgoingCalls" type="{}MaxOutgoingCalls" minOccurs="0"/>
 *         <element name="requireAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sipAuthenticationUserName" type="{}SIPAuthenticationUserName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupTrunkGroupGetInstanceResponse14", propOrder = {
    "serviceInstanceProfile",
    "accessDeviceEndpoint",
    "maxActiveCalls",
    "maxIncomingCalls",
    "maxOutgoingCalls",
    "requireAuthentication",
    "sipAuthenticationUserName"
})
public class GroupTrunkGroupGetInstanceResponse14
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected ServiceInstanceReadProfile serviceInstanceProfile;
    protected AccessDeviceEndpointRead14 accessDeviceEndpoint;
    protected int maxActiveCalls;
    protected Integer maxIncomingCalls;
    protected Integer maxOutgoingCalls;
    protected boolean requireAuthentication;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sipAuthenticationUserName;

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceReadProfile }
     *     
     */
    public ServiceInstanceReadProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceReadProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceReadProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEndpointRead14 }
     *     
     */
    public AccessDeviceEndpointRead14 getAccessDeviceEndpoint() {
        return accessDeviceEndpoint;
    }

    /**
     * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEndpointRead14 }
     *     
     */
    public void setAccessDeviceEndpoint(AccessDeviceEndpointRead14 value) {
        this.accessDeviceEndpoint = value;
    }

    /**
     * Ruft den Wert der maxActiveCalls-Eigenschaft ab.
     * 
     */
    public int getMaxActiveCalls() {
        return maxActiveCalls;
    }

    /**
     * Legt den Wert der maxActiveCalls-Eigenschaft fest.
     * 
     */
    public void setMaxActiveCalls(int value) {
        this.maxActiveCalls = value;
    }

    /**
     * Ruft den Wert der maxIncomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxIncomingCalls() {
        return maxIncomingCalls;
    }

    /**
     * Legt den Wert der maxIncomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxIncomingCalls(Integer value) {
        this.maxIncomingCalls = value;
    }

    /**
     * Ruft den Wert der maxOutgoingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxOutgoingCalls() {
        return maxOutgoingCalls;
    }

    /**
     * Legt den Wert der maxOutgoingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxOutgoingCalls(Integer value) {
        this.maxOutgoingCalls = value;
    }

    /**
     * Ruft den Wert der requireAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isRequireAuthentication() {
        return requireAuthentication;
    }

    /**
     * Legt den Wert der requireAuthentication-Eigenschaft fest.
     * 
     */
    public void setRequireAuthentication(boolean value) {
        this.requireAuthentication = value;
    }

    /**
     * Ruft den Wert der sipAuthenticationUserName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSipAuthenticationUserName() {
        return sipAuthenticationUserName;
    }

    /**
     * Legt den Wert der sipAuthenticationUserName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSipAuthenticationUserName(String value) {
        this.sipAuthenticationUserName = value;
    }

}
