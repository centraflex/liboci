//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request a summary table of all Mobile Subscriber Directory Numbers in a service provider. This command is applicable only for service providers and will fail for enterprises.
 *         The response is either ServiceProviderBroadWorksMobilityMobileSubscriberDirectoryNumberGetSummaryListResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderBroadWorksMobilityMobileSubscriberDirectoryNumberGetSummaryListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderBroadWorksMobilityMobileSubscriberDirectoryNumberGetSummaryListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaMobileSubscriberDirectoryNumber" type="{}SearchCriteriaMobileSubscriberDirectoryNumber" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaGroupId" type="{}SearchCriteriaGroupId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactMobileNetwork" type="{}SearchCriteriaExactMobileNetwork" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderBroadWorksMobilityMobileSubscriberDirectoryNumberGetSummaryListRequest", propOrder = {
    "serviceProviderId",
    "responseSizeLimit",
    "searchCriteriaMobileSubscriberDirectoryNumber",
    "searchCriteriaGroupId",
    "searchCriteriaExactMobileNetwork"
})
public class ServiceProviderBroadWorksMobilityMobileSubscriberDirectoryNumberGetSummaryListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected Integer responseSizeLimit;
    protected List<SearchCriteriaMobileSubscriberDirectoryNumber> searchCriteriaMobileSubscriberDirectoryNumber;
    protected List<SearchCriteriaGroupId> searchCriteriaGroupId;
    protected SearchCriteriaExactMobileNetwork searchCriteriaExactMobileNetwork;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaMobileSubscriberDirectoryNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaMobileSubscriberDirectoryNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaMobileSubscriberDirectoryNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaMobileSubscriberDirectoryNumber }
     * 
     * 
     * @return
     *     The value of the searchCriteriaMobileSubscriberDirectoryNumber property.
     */
    public List<SearchCriteriaMobileSubscriberDirectoryNumber> getSearchCriteriaMobileSubscriberDirectoryNumber() {
        if (searchCriteriaMobileSubscriberDirectoryNumber == null) {
            searchCriteriaMobileSubscriberDirectoryNumber = new ArrayList<>();
        }
        return this.searchCriteriaMobileSubscriberDirectoryNumber;
    }

    /**
     * Gets the value of the searchCriteriaGroupId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaGroupId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaGroupId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaGroupId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaGroupId property.
     */
    public List<SearchCriteriaGroupId> getSearchCriteriaGroupId() {
        if (searchCriteriaGroupId == null) {
            searchCriteriaGroupId = new ArrayList<>();
        }
        return this.searchCriteriaGroupId;
    }

    /**
     * Ruft den Wert der searchCriteriaExactMobileNetwork-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactMobileNetwork }
     *     
     */
    public SearchCriteriaExactMobileNetwork getSearchCriteriaExactMobileNetwork() {
        return searchCriteriaExactMobileNetwork;
    }

    /**
     * Legt den Wert der searchCriteriaExactMobileNetwork-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactMobileNetwork }
     *     
     */
    public void setSearchCriteriaExactMobileNetwork(SearchCriteriaExactMobileNetwork value) {
        this.searchCriteriaExactMobileNetwork = value;
    }

}
