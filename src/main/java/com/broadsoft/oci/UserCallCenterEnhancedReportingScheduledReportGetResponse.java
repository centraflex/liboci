//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserCallCenterEnhancedReportingScheduledReportGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserCallCenterEnhancedReportingScheduledReportGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallCenterEnhancedReportingScheduledReportGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="description" type="{}CallCenterScheduledReportDescription" minOccurs="0"/>
 *         <element name="reportTemplate" type="{}CallCenterReportTemplateKey"/>
 *         <element name="schedule" type="{}CallCenterReportSchedule"/>
 *         <element name="samplingPeriod" type="{}CallCenterReportSamplingPeriod" minOccurs="0"/>
 *         <element name="startDayOfWeek" type="{}DayOfWeek" minOccurs="0"/>
 *         <element name="reportTimeZone" type="{}TimeZone"/>
 *         <element name="reportDateFormat" type="{}CallCenterReportDateFormat"/>
 *         <element name="reportTimeFormat" type="{}CallCenterReportTimeFormat"/>
 *         <element name="reportInterval" type="{}CallCenterReportInterval"/>
 *         <element name="reportFormat" type="{}CallCenterReportFileFormat"/>
 *         <element name="agent" type="{}CallCenterScheduledReportAgentSelectionRead" minOccurs="0"/>
 *         <choice minOccurs="0">
 *           <element name="callCenter" type="{}CallCenterScheduledReportCallCenterSelection"/>
 *           <element name="dnis" type="{}CallCenterScheduledReportDNISSelection"/>
 *         </choice>
 *         <element name="callCompletionThresholdSeconds" type="{}CallCenterReportThresholdSeconds" minOccurs="0"/>
 *         <element name="shortDurationThresholdSeconds" type="{}CallCenterReportThresholdSeconds" minOccurs="0"/>
 *         <element name="serviceLevelThresholdSeconds" type="{}CallCenterReportThresholdSeconds" maxOccurs="5" minOccurs="0"/>
 *         <element name="serviceLevelInclusions" type="{}CallCenterScheduledReportServiceLevelInclusions" minOccurs="0"/>
 *         <element name="serviceLevelObjectivePercentage" type="{}CallCenterReportServiceLevelObjective" minOccurs="0"/>
 *         <element name="abandonedCallThresholdSeconds" type="{}CallCenterReportThresholdSeconds" maxOccurs="4" minOccurs="0"/>
 *         <element name="emailAddress" type="{}EmailAddress" maxOccurs="9"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallCenterEnhancedReportingScheduledReportGetResponse", propOrder = {
    "description",
    "reportTemplate",
    "schedule",
    "samplingPeriod",
    "startDayOfWeek",
    "reportTimeZone",
    "reportDateFormat",
    "reportTimeFormat",
    "reportInterval",
    "reportFormat",
    "agent",
    "callCenter",
    "dnis",
    "callCompletionThresholdSeconds",
    "shortDurationThresholdSeconds",
    "serviceLevelThresholdSeconds",
    "serviceLevelInclusions",
    "serviceLevelObjectivePercentage",
    "abandonedCallThresholdSeconds",
    "emailAddress"
})
public class UserCallCenterEnhancedReportingScheduledReportGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    @XmlElement(required = true)
    protected CallCenterReportTemplateKey reportTemplate;
    @XmlElement(required = true)
    protected CallCenterReportSchedule schedule;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String samplingPeriod;
    @XmlSchemaType(name = "NMTOKEN")
    protected DayOfWeek startDayOfWeek;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String reportTimeZone;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportDateFormat reportDateFormat;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String reportTimeFormat;
    @XmlElement(required = true)
    protected CallCenterReportInterval reportInterval;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportFileFormat reportFormat;
    protected CallCenterScheduledReportAgentSelectionRead agent;
    protected CallCenterScheduledReportCallCenterSelection callCenter;
    protected CallCenterScheduledReportDNISSelection dnis;
    protected Integer callCompletionThresholdSeconds;
    protected Integer shortDurationThresholdSeconds;
    @XmlElement(type = Integer.class)
    protected List<Integer> serviceLevelThresholdSeconds;
    protected CallCenterScheduledReportServiceLevelInclusions serviceLevelInclusions;
    protected Integer serviceLevelObjectivePercentage;
    @XmlElement(type = Integer.class)
    protected List<Integer> abandonedCallThresholdSeconds;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> emailAddress;

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der reportTemplate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportTemplateKey }
     *     
     */
    public CallCenterReportTemplateKey getReportTemplate() {
        return reportTemplate;
    }

    /**
     * Legt den Wert der reportTemplate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportTemplateKey }
     *     
     */
    public void setReportTemplate(CallCenterReportTemplateKey value) {
        this.reportTemplate = value;
    }

    /**
     * Ruft den Wert der schedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportSchedule }
     *     
     */
    public CallCenterReportSchedule getSchedule() {
        return schedule;
    }

    /**
     * Legt den Wert der schedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportSchedule }
     *     
     */
    public void setSchedule(CallCenterReportSchedule value) {
        this.schedule = value;
    }

    /**
     * Ruft den Wert der samplingPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSamplingPeriod() {
        return samplingPeriod;
    }

    /**
     * Legt den Wert der samplingPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSamplingPeriod(String value) {
        this.samplingPeriod = value;
    }

    /**
     * Ruft den Wert der startDayOfWeek-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DayOfWeek }
     *     
     */
    public DayOfWeek getStartDayOfWeek() {
        return startDayOfWeek;
    }

    /**
     * Legt den Wert der startDayOfWeek-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DayOfWeek }
     *     
     */
    public void setStartDayOfWeek(DayOfWeek value) {
        this.startDayOfWeek = value;
    }

    /**
     * Ruft den Wert der reportTimeZone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportTimeZone() {
        return reportTimeZone;
    }

    /**
     * Legt den Wert der reportTimeZone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportTimeZone(String value) {
        this.reportTimeZone = value;
    }

    /**
     * Ruft den Wert der reportDateFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportDateFormat }
     *     
     */
    public CallCenterReportDateFormat getReportDateFormat() {
        return reportDateFormat;
    }

    /**
     * Legt den Wert der reportDateFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportDateFormat }
     *     
     */
    public void setReportDateFormat(CallCenterReportDateFormat value) {
        this.reportDateFormat = value;
    }

    /**
     * Ruft den Wert der reportTimeFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportTimeFormat() {
        return reportTimeFormat;
    }

    /**
     * Legt den Wert der reportTimeFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportTimeFormat(String value) {
        this.reportTimeFormat = value;
    }

    /**
     * Ruft den Wert der reportInterval-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportInterval }
     *     
     */
    public CallCenterReportInterval getReportInterval() {
        return reportInterval;
    }

    /**
     * Legt den Wert der reportInterval-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportInterval }
     *     
     */
    public void setReportInterval(CallCenterReportInterval value) {
        this.reportInterval = value;
    }

    /**
     * Ruft den Wert der reportFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportFileFormat }
     *     
     */
    public CallCenterReportFileFormat getReportFormat() {
        return reportFormat;
    }

    /**
     * Legt den Wert der reportFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportFileFormat }
     *     
     */
    public void setReportFormat(CallCenterReportFileFormat value) {
        this.reportFormat = value;
    }

    /**
     * Ruft den Wert der agent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterScheduledReportAgentSelectionRead }
     *     
     */
    public CallCenterScheduledReportAgentSelectionRead getAgent() {
        return agent;
    }

    /**
     * Legt den Wert der agent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterScheduledReportAgentSelectionRead }
     *     
     */
    public void setAgent(CallCenterScheduledReportAgentSelectionRead value) {
        this.agent = value;
    }

    /**
     * Ruft den Wert der callCenter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterScheduledReportCallCenterSelection }
     *     
     */
    public CallCenterScheduledReportCallCenterSelection getCallCenter() {
        return callCenter;
    }

    /**
     * Legt den Wert der callCenter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterScheduledReportCallCenterSelection }
     *     
     */
    public void setCallCenter(CallCenterScheduledReportCallCenterSelection value) {
        this.callCenter = value;
    }

    /**
     * Ruft den Wert der dnis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterScheduledReportDNISSelection }
     *     
     */
    public CallCenterScheduledReportDNISSelection getDnis() {
        return dnis;
    }

    /**
     * Legt den Wert der dnis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterScheduledReportDNISSelection }
     *     
     */
    public void setDnis(CallCenterScheduledReportDNISSelection value) {
        this.dnis = value;
    }

    /**
     * Ruft den Wert der callCompletionThresholdSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCallCompletionThresholdSeconds() {
        return callCompletionThresholdSeconds;
    }

    /**
     * Legt den Wert der callCompletionThresholdSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCallCompletionThresholdSeconds(Integer value) {
        this.callCompletionThresholdSeconds = value;
    }

    /**
     * Ruft den Wert der shortDurationThresholdSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShortDurationThresholdSeconds() {
        return shortDurationThresholdSeconds;
    }

    /**
     * Legt den Wert der shortDurationThresholdSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShortDurationThresholdSeconds(Integer value) {
        this.shortDurationThresholdSeconds = value;
    }

    /**
     * Gets the value of the serviceLevelThresholdSeconds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the serviceLevelThresholdSeconds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceLevelThresholdSeconds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     * @return
     *     The value of the serviceLevelThresholdSeconds property.
     */
    public List<Integer> getServiceLevelThresholdSeconds() {
        if (serviceLevelThresholdSeconds == null) {
            serviceLevelThresholdSeconds = new ArrayList<>();
        }
        return this.serviceLevelThresholdSeconds;
    }

    /**
     * Ruft den Wert der serviceLevelInclusions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterScheduledReportServiceLevelInclusions }
     *     
     */
    public CallCenterScheduledReportServiceLevelInclusions getServiceLevelInclusions() {
        return serviceLevelInclusions;
    }

    /**
     * Legt den Wert der serviceLevelInclusions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterScheduledReportServiceLevelInclusions }
     *     
     */
    public void setServiceLevelInclusions(CallCenterScheduledReportServiceLevelInclusions value) {
        this.serviceLevelInclusions = value;
    }

    /**
     * Ruft den Wert der serviceLevelObjectivePercentage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceLevelObjectivePercentage() {
        return serviceLevelObjectivePercentage;
    }

    /**
     * Legt den Wert der serviceLevelObjectivePercentage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceLevelObjectivePercentage(Integer value) {
        this.serviceLevelObjectivePercentage = value;
    }

    /**
     * Gets the value of the abandonedCallThresholdSeconds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the abandonedCallThresholdSeconds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAbandonedCallThresholdSeconds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     * @return
     *     The value of the abandonedCallThresholdSeconds property.
     */
    public List<Integer> getAbandonedCallThresholdSeconds() {
        if (abandonedCallThresholdSeconds == null) {
            abandonedCallThresholdSeconds = new ArrayList<>();
        }
        return this.abandonedCallThresholdSeconds;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the emailAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmailAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the emailAddress property.
     */
    public List<String> getEmailAddress() {
        if (emailAddress == null) {
            emailAddress = new ArrayList<>();
        }
        return this.emailAddress;
    }

}
