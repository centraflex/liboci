//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the ServiceProviderIntegratedIMPGetRequest.
 *         The response contains the service provider Integrated IMP service attributes.
 *         Replaced by: ServiceProviderIntegratedIMPGetResponse21 in AS data mode
 *       
 * 
 * <p>Java-Klasse für ServiceProviderIntegratedIMPGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderIntegratedIMPGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useSystemServiceDomain" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="serviceDomain" type="{}DomainName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderIntegratedIMPGetResponse", propOrder = {
    "useSystemServiceDomain",
    "serviceDomain"
})
public class ServiceProviderIntegratedIMPGetResponse
    extends OCIDataResponse
{

    protected boolean useSystemServiceDomain;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceDomain;

    /**
     * Ruft den Wert der useSystemServiceDomain-Eigenschaft ab.
     * 
     */
    public boolean isUseSystemServiceDomain() {
        return useSystemServiceDomain;
    }

    /**
     * Legt den Wert der useSystemServiceDomain-Eigenschaft fest.
     * 
     */
    public void setUseSystemServiceDomain(boolean value) {
        this.useSystemServiceDomain = value;
    }

    /**
     * Ruft den Wert der serviceDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceDomain() {
        return serviceDomain;
    }

    /**
     * Legt den Wert der serviceDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceDomain(String value) {
        this.serviceDomain = value;
    }

}
