//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Contains Call Center statistics for a specified agent.
 *       
 * 
 * <p>Java-Klasse für CallCenterAgentStatistics13mp8 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterAgentStatistics13mp8">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="agentUserId" type="{}UserId"/>
 *         <element name="agentDisplayNames" type="{}UserDisplayNames"/>
 *         <element name="available" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="statisticsYesterday" type="{}CallCenterAgentDailyStatistics13mp8"/>
 *         <element name="statisticsToday" type="{}CallCenterAgentDailyStatistics13mp8"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterAgentStatistics13mp8", propOrder = {
    "agentUserId",
    "agentDisplayNames",
    "available",
    "statisticsYesterday",
    "statisticsToday"
})
public class CallCenterAgentStatistics13Mp8 {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String agentUserId;
    @XmlElement(required = true)
    protected UserDisplayNames agentDisplayNames;
    protected boolean available;
    @XmlElement(required = true)
    protected CallCenterAgentDailyStatistics13Mp8 statisticsYesterday;
    @XmlElement(required = true)
    protected CallCenterAgentDailyStatistics13Mp8 statisticsToday;

    /**
     * Ruft den Wert der agentUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentUserId() {
        return agentUserId;
    }

    /**
     * Legt den Wert der agentUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentUserId(String value) {
        this.agentUserId = value;
    }

    /**
     * Ruft den Wert der agentDisplayNames-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserDisplayNames }
     *     
     */
    public UserDisplayNames getAgentDisplayNames() {
        return agentDisplayNames;
    }

    /**
     * Legt den Wert der agentDisplayNames-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDisplayNames }
     *     
     */
    public void setAgentDisplayNames(UserDisplayNames value) {
        this.agentDisplayNames = value;
    }

    /**
     * Ruft den Wert der available-Eigenschaft ab.
     * 
     */
    public boolean isAvailable() {
        return available;
    }

    /**
     * Legt den Wert der available-Eigenschaft fest.
     * 
     */
    public void setAvailable(boolean value) {
        this.available = value;
    }

    /**
     * Ruft den Wert der statisticsYesterday-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAgentDailyStatistics13Mp8 }
     *     
     */
    public CallCenterAgentDailyStatistics13Mp8 getStatisticsYesterday() {
        return statisticsYesterday;
    }

    /**
     * Legt den Wert der statisticsYesterday-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAgentDailyStatistics13Mp8 }
     *     
     */
    public void setStatisticsYesterday(CallCenterAgentDailyStatistics13Mp8 value) {
        this.statisticsYesterday = value;
    }

    /**
     * Ruft den Wert der statisticsToday-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAgentDailyStatistics13Mp8 }
     *     
     */
    public CallCenterAgentDailyStatistics13Mp8 getStatisticsToday() {
        return statisticsToday;
    }

    /**
     * Legt den Wert der statisticsToday-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAgentDailyStatistics13Mp8 }
     *     
     */
    public void setStatisticsToday(CallCenterAgentDailyStatistics13Mp8 value) {
        this.statisticsToday = value;
    }

}
