//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TrunkGroupUserLookupPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="TrunkGroupUserLookupPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Basic"/>
 *     <enumeration value="Extended"/>
 *     <enumeration value="Basic Lookup Prefer From"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TrunkGroupUserLookupPolicy")
@XmlEnum
public enum TrunkGroupUserLookupPolicy {

    @XmlEnumValue("Basic")
    BASIC("Basic"),
    @XmlEnumValue("Extended")
    EXTENDED("Extended"),
    @XmlEnumValue("Basic Lookup Prefer From")
    BASIC_LOOKUP_PREFER_FROM("Basic Lookup Prefer From");
    private final String value;

    TrunkGroupUserLookupPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrunkGroupUserLookupPolicy fromValue(String v) {
        for (TrunkGroupUserLookupPolicy c: TrunkGroupUserLookupPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
