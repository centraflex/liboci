//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 * 				Get a user's personal phone list.
 * 				The response is either a UserPersonalPhoneListGetPagedSortedListResponse or an
 * 				ErrorResponse.
 * 				The search can be done using multiple criterion.
 * 				If the searchCriteriaModeOr is present, any result matching any one
 * 				criteria is included in the results.
 * 				Otherwise, only results matching all the search criterion are included in the
 * 				results.
 * 				If no search criteria is specified, all results are returned.
 * 				Specifying searchCriteriaModeOr without any search criteria results
 * 				in an ErrorResponse.
 * 				The sort can done by the personal phone list number or name.
 * 				The following elements are only used in AS data mode and ignored in XS data  
 * 				mode:
 * 				searchCriteriaUserPersonalMultiPartPhoneListName
 * 			
 * 
 * <p>Java-Klasse für UserPersonalPhoneListGetPagedSortedListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPersonalPhoneListGetPagedSortedListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl"/>
 *         <choice>
 *           <element name="sortByUserPersonalPhoneListNumber" type="{}SortByUserPersonalPhoneListNumber"/>
 *           <element name="sortByUserPersonalPhoneListName" type="{}SortByUserPersonalPhoneListName"/>
 *         </choice>
 *         <element name="searchCriteriaModeOr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="searchCriteriaUserPersonalPhoneListName" type="{}SearchCriteriaUserPersonalPhoneListName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaUserPersonalPhoneListNumber" type="{}SearchCriteriaUserPersonalPhoneListNumber" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaUserPersonalMultiPartPhoneListName" type="{}SearchCriteriaUserPersonalMultiPartPhoneListName" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPersonalPhoneListGetPagedSortedListRequest", propOrder = {
    "userId",
    "responsePagingControl",
    "sortByUserPersonalPhoneListNumber",
    "sortByUserPersonalPhoneListName",
    "searchCriteriaModeOr",
    "searchCriteriaUserPersonalPhoneListName",
    "searchCriteriaUserPersonalPhoneListNumber",
    "searchCriteriaUserPersonalMultiPartPhoneListName"
})
public class UserPersonalPhoneListGetPagedSortedListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    protected ResponsePagingControl responsePagingControl;
    protected SortByUserPersonalPhoneListNumber sortByUserPersonalPhoneListNumber;
    protected SortByUserPersonalPhoneListName sortByUserPersonalPhoneListName;
    protected Boolean searchCriteriaModeOr;
    protected List<SearchCriteriaUserPersonalPhoneListName> searchCriteriaUserPersonalPhoneListName;
    protected List<SearchCriteriaUserPersonalPhoneListNumber> searchCriteriaUserPersonalPhoneListNumber;
    protected List<SearchCriteriaUserPersonalMultiPartPhoneListName> searchCriteriaUserPersonalMultiPartPhoneListName;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Ruft den Wert der sortByUserPersonalPhoneListNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByUserPersonalPhoneListNumber }
     *     
     */
    public SortByUserPersonalPhoneListNumber getSortByUserPersonalPhoneListNumber() {
        return sortByUserPersonalPhoneListNumber;
    }

    /**
     * Legt den Wert der sortByUserPersonalPhoneListNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByUserPersonalPhoneListNumber }
     *     
     */
    public void setSortByUserPersonalPhoneListNumber(SortByUserPersonalPhoneListNumber value) {
        this.sortByUserPersonalPhoneListNumber = value;
    }

    /**
     * Ruft den Wert der sortByUserPersonalPhoneListName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByUserPersonalPhoneListName }
     *     
     */
    public SortByUserPersonalPhoneListName getSortByUserPersonalPhoneListName() {
        return sortByUserPersonalPhoneListName;
    }

    /**
     * Legt den Wert der sortByUserPersonalPhoneListName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByUserPersonalPhoneListName }
     *     
     */
    public void setSortByUserPersonalPhoneListName(SortByUserPersonalPhoneListName value) {
        this.sortByUserPersonalPhoneListName = value;
    }

    /**
     * Ruft den Wert der searchCriteriaModeOr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchCriteriaModeOr() {
        return searchCriteriaModeOr;
    }

    /**
     * Legt den Wert der searchCriteriaModeOr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchCriteriaModeOr(Boolean value) {
        this.searchCriteriaModeOr = value;
    }

    /**
     * Gets the value of the searchCriteriaUserPersonalPhoneListName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserPersonalPhoneListName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserPersonalPhoneListName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserPersonalPhoneListName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserPersonalPhoneListName property.
     */
    public List<SearchCriteriaUserPersonalPhoneListName> getSearchCriteriaUserPersonalPhoneListName() {
        if (searchCriteriaUserPersonalPhoneListName == null) {
            searchCriteriaUserPersonalPhoneListName = new ArrayList<>();
        }
        return this.searchCriteriaUserPersonalPhoneListName;
    }

    /**
     * Gets the value of the searchCriteriaUserPersonalPhoneListNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserPersonalPhoneListNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserPersonalPhoneListNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserPersonalPhoneListNumber }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserPersonalPhoneListNumber property.
     */
    public List<SearchCriteriaUserPersonalPhoneListNumber> getSearchCriteriaUserPersonalPhoneListNumber() {
        if (searchCriteriaUserPersonalPhoneListNumber == null) {
            searchCriteriaUserPersonalPhoneListNumber = new ArrayList<>();
        }
        return this.searchCriteriaUserPersonalPhoneListNumber;
    }

    /**
     * Gets the value of the searchCriteriaUserPersonalMultiPartPhoneListName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserPersonalMultiPartPhoneListName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserPersonalMultiPartPhoneListName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserPersonalMultiPartPhoneListName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserPersonalMultiPartPhoneListName property.
     */
    public List<SearchCriteriaUserPersonalMultiPartPhoneListName> getSearchCriteriaUserPersonalMultiPartPhoneListName() {
        if (searchCriteriaUserPersonalMultiPartPhoneListName == null) {
            searchCriteriaUserPersonalMultiPartPhoneListName = new ArrayList<>();
        }
        return this.searchCriteriaUserPersonalMultiPartPhoneListName;
    }

}
