//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *          Modify the user's Shared Call Appearance service setting.
 *          The response is either a SuccessResponse or an ErrorResponse.
 *          
 *          The following elements are only used in XS data mode and ignored in AS data mode:
 *            useUserPrimaryWithAlternateCallsSetting
 *            allowSimultaneousPrimaryAndAlternate
 *            restrictCallRetrieveOfPrimary
 *            restrictCallBridgingOfPrimary
 *        
 * 
 * <p>Java-Klasse für UserSharedCallAppearanceModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserSharedCallAppearanceModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="alertAllAppearancesForClickToDialCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="alertAllAppearancesForGroupPagingCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowSCACallRetrieve" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="multipleCallArrangementIsActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowBridgingBetweenLocations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="bridgeWarningTone" type="{}SharedCallAppearanceBridgeWarningTone" minOccurs="0"/>
 *         <element name="enableCallParkNotification" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useUserPrimaryWithAlternateCallsSetting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowSimultaneousPrimaryAndAlternateCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="restrictCallRetrieveOfPrimaryCall" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="restrictCallBridgingOfPrimaryCall" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserSharedCallAppearanceModifyRequest", propOrder = {
    "userId",
    "alertAllAppearancesForClickToDialCalls",
    "alertAllAppearancesForGroupPagingCalls",
    "allowSCACallRetrieve",
    "multipleCallArrangementIsActive",
    "allowBridgingBetweenLocations",
    "bridgeWarningTone",
    "enableCallParkNotification",
    "useUserPrimaryWithAlternateCallsSetting",
    "allowSimultaneousPrimaryAndAlternateCalls",
    "restrictCallRetrieveOfPrimaryCall",
    "restrictCallBridgingOfPrimaryCall"
})
public class UserSharedCallAppearanceModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean alertAllAppearancesForClickToDialCalls;
    protected Boolean alertAllAppearancesForGroupPagingCalls;
    protected Boolean allowSCACallRetrieve;
    protected Boolean multipleCallArrangementIsActive;
    protected Boolean allowBridgingBetweenLocations;
    @XmlSchemaType(name = "token")
    protected SharedCallAppearanceBridgeWarningTone bridgeWarningTone;
    protected Boolean enableCallParkNotification;
    protected Boolean useUserPrimaryWithAlternateCallsSetting;
    protected Boolean allowSimultaneousPrimaryAndAlternateCalls;
    protected Boolean restrictCallRetrieveOfPrimaryCall;
    protected Boolean restrictCallBridgingOfPrimaryCall;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der alertAllAppearancesForClickToDialCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlertAllAppearancesForClickToDialCalls() {
        return alertAllAppearancesForClickToDialCalls;
    }

    /**
     * Legt den Wert der alertAllAppearancesForClickToDialCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlertAllAppearancesForClickToDialCalls(Boolean value) {
        this.alertAllAppearancesForClickToDialCalls = value;
    }

    /**
     * Ruft den Wert der alertAllAppearancesForGroupPagingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlertAllAppearancesForGroupPagingCalls() {
        return alertAllAppearancesForGroupPagingCalls;
    }

    /**
     * Legt den Wert der alertAllAppearancesForGroupPagingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlertAllAppearancesForGroupPagingCalls(Boolean value) {
        this.alertAllAppearancesForGroupPagingCalls = value;
    }

    /**
     * Ruft den Wert der allowSCACallRetrieve-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowSCACallRetrieve() {
        return allowSCACallRetrieve;
    }

    /**
     * Legt den Wert der allowSCACallRetrieve-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowSCACallRetrieve(Boolean value) {
        this.allowSCACallRetrieve = value;
    }

    /**
     * Ruft den Wert der multipleCallArrangementIsActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMultipleCallArrangementIsActive() {
        return multipleCallArrangementIsActive;
    }

    /**
     * Legt den Wert der multipleCallArrangementIsActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultipleCallArrangementIsActive(Boolean value) {
        this.multipleCallArrangementIsActive = value;
    }

    /**
     * Ruft den Wert der allowBridgingBetweenLocations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowBridgingBetweenLocations() {
        return allowBridgingBetweenLocations;
    }

    /**
     * Legt den Wert der allowBridgingBetweenLocations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowBridgingBetweenLocations(Boolean value) {
        this.allowBridgingBetweenLocations = value;
    }

    /**
     * Ruft den Wert der bridgeWarningTone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SharedCallAppearanceBridgeWarningTone }
     *     
     */
    public SharedCallAppearanceBridgeWarningTone getBridgeWarningTone() {
        return bridgeWarningTone;
    }

    /**
     * Legt den Wert der bridgeWarningTone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SharedCallAppearanceBridgeWarningTone }
     *     
     */
    public void setBridgeWarningTone(SharedCallAppearanceBridgeWarningTone value) {
        this.bridgeWarningTone = value;
    }

    /**
     * Ruft den Wert der enableCallParkNotification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCallParkNotification() {
        return enableCallParkNotification;
    }

    /**
     * Legt den Wert der enableCallParkNotification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCallParkNotification(Boolean value) {
        this.enableCallParkNotification = value;
    }

    /**
     * Ruft den Wert der useUserPrimaryWithAlternateCallsSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseUserPrimaryWithAlternateCallsSetting() {
        return useUserPrimaryWithAlternateCallsSetting;
    }

    /**
     * Legt den Wert der useUserPrimaryWithAlternateCallsSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseUserPrimaryWithAlternateCallsSetting(Boolean value) {
        this.useUserPrimaryWithAlternateCallsSetting = value;
    }

    /**
     * Ruft den Wert der allowSimultaneousPrimaryAndAlternateCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowSimultaneousPrimaryAndAlternateCalls() {
        return allowSimultaneousPrimaryAndAlternateCalls;
    }

    /**
     * Legt den Wert der allowSimultaneousPrimaryAndAlternateCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowSimultaneousPrimaryAndAlternateCalls(Boolean value) {
        this.allowSimultaneousPrimaryAndAlternateCalls = value;
    }

    /**
     * Ruft den Wert der restrictCallRetrieveOfPrimaryCall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictCallRetrieveOfPrimaryCall() {
        return restrictCallRetrieveOfPrimaryCall;
    }

    /**
     * Legt den Wert der restrictCallRetrieveOfPrimaryCall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictCallRetrieveOfPrimaryCall(Boolean value) {
        this.restrictCallRetrieveOfPrimaryCall = value;
    }

    /**
     * Ruft den Wert der restrictCallBridgingOfPrimaryCall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictCallBridgingOfPrimaryCall() {
        return restrictCallBridgingOfPrimaryCall;
    }

    /**
     * Legt den Wert der restrictCallBridgingOfPrimaryCall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictCallBridgingOfPrimaryCall(Boolean value) {
        this.restrictCallBridgingOfPrimaryCall = value;
    }

}
