//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Get the OCI entries from the Broadworks Common Communication Transport (BCCT) protocol to interface mapping list.
 *         The response is SystemBCCTGetOCIInterfaceAddressListResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemBCCTGetOCIInterfaceAddressListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBCCTGetOCIInterfaceAddressListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBCCTGetOCIInterfaceAddressListRequest")
public class SystemBCCTGetOCIInterfaceAddressListRequest
    extends OCIRequest
{


}
