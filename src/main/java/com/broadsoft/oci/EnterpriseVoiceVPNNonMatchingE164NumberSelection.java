//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EnterpriseVoiceVPNNonMatchingE164NumberSelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="EnterpriseVoiceVPNNonMatchingE164NumberSelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Public"/>
 *     <enumeration value="Default"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "EnterpriseVoiceVPNNonMatchingE164NumberSelection")
@XmlEnum
public enum EnterpriseVoiceVPNNonMatchingE164NumberSelection {

    @XmlEnumValue("Public")
    PUBLIC("Public"),
    @XmlEnumValue("Default")
    DEFAULT("Default");
    private final String value;

    EnterpriseVoiceVPNNonMatchingE164NumberSelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnterpriseVoiceVPNNonMatchingE164NumberSelection fromValue(String v) {
        for (EnterpriseVoiceVPNNonMatchingE164NumberSelection c: EnterpriseVoiceVPNNonMatchingE164NumberSelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
