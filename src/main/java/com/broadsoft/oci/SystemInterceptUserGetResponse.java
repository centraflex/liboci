//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemInterceptUserGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemInterceptUserGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemInterceptUserGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="emergencyAndRepairIntercept" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemInterceptUserGetResponse", propOrder = {
    "emergencyAndRepairIntercept"
})
public class SystemInterceptUserGetResponse
    extends OCIDataResponse
{

    protected boolean emergencyAndRepairIntercept;

    /**
     * Ruft den Wert der emergencyAndRepairIntercept-Eigenschaft ab.
     * 
     */
    public boolean isEmergencyAndRepairIntercept() {
        return emergencyAndRepairIntercept;
    }

    /**
     * Legt den Wert der emergencyAndRepairIntercept-Eigenschaft fest.
     * 
     */
    public void setEmergencyAndRepairIntercept(boolean value) {
        this.emergencyAndRepairIntercept = value;
    }

}
