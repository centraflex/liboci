//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 * 				Response to the UserGroupCustomContactDirectoryGetPagedSortedListRequest.
 * 	
 * 				Returns the number of entries that would be returned if the response
 * 				Was not page size restricted.
 * 	
 * 				The response contains all the contacts in the group's given custom
 * 				contact directory. Contains a table with column headings: "User Id",
 * 				"Last Name", "First Name", "Hiragana Last Name",
 * 				"Hiragana First Name", "Virtual On-Net Phone Number", "Group Id",
 * 				"Is Virtual On-Net User", "Department", "Phone Number", "Extension",
 * 				"Mobile", "Email Address", "Yahoo Id", "Title", "IMP Id", "Receptionist Note".
 * 				If the entry represents a Virtual On-Net user then "User Id" is blank,
 * 				the "Virtual On-Net Phone Number" contains the phone Number of the
 * 				Virtual On-Net user, the "Group Id" contains the Virtual On-Net
 * 				user's
 * 				group and the "Is Virtual On-Net User" contains true.
 * 				If the entry represents a BroadWorks user then the "User Id" contains
 * 				his BroadWorks userId, the "Virtual On-Net Phone Number" and
 * 				"Group Id" fields are field is blank and the "Is Virtual On-Net User"
 * 				contains false.  The Receptionist Note column is only populated, if the 
 *         user sending the request is the owner of this Receptionist Note and a 
 *         Note exists.
 * 			
 * 
 * <p>Java-Klasse für UserGroupCustomContactDirectoryGetPagedSortedListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserGroupCustomContactDirectoryGetPagedSortedListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="totalNumberOfRows" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="userTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserGroupCustomContactDirectoryGetPagedSortedListResponse", propOrder = {
    "totalNumberOfRows",
    "userTable"
})
public class UserGroupCustomContactDirectoryGetPagedSortedListResponse
    extends OCIDataResponse
{

    protected int totalNumberOfRows;
    @XmlElement(required = true)
    protected OCITable userTable;

    /**
     * Ruft den Wert der totalNumberOfRows-Eigenschaft ab.
     * 
     */
    public int getTotalNumberOfRows() {
        return totalNumberOfRows;
    }

    /**
     * Legt den Wert der totalNumberOfRows-Eigenschaft fest.
     * 
     */
    public void setTotalNumberOfRows(int value) {
        this.totalNumberOfRows = value;
    }

    /**
     * Ruft den Wert der userTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserTable() {
        return userTable;
    }

    /**
     * Legt den Wert der userTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserTable(OCITable value) {
        this.userTable = value;
    }

}
