//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a service provider or enterprise's answer confirmation settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         Replaced By: ServiceProviderAnswerConfirmationModifyRequest16            
 *       
 * 
 * <p>Java-Klasse für ServiceProviderAnswerConfirmationModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderAnswerConfirmationModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="announcementMessageSelection" type="{}AnswerConfirmationAnnouncementSelection" minOccurs="0"/>
 *         <element name="confirmationMessageAudioFile" type="{}LabeledFileResource" minOccurs="0"/>
 *         <element name="confirmationTimoutSeconds" type="{}AnswerConfirmationTimeoutSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderAnswerConfirmationModifyRequest", propOrder = {
    "serviceProviderId",
    "announcementMessageSelection",
    "confirmationMessageAudioFile",
    "confirmationTimoutSeconds"
})
public class ServiceProviderAnswerConfirmationModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlSchemaType(name = "token")
    protected AnswerConfirmationAnnouncementSelection announcementMessageSelection;
    protected LabeledFileResource confirmationMessageAudioFile;
    protected Integer confirmationTimoutSeconds;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der announcementMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnswerConfirmationAnnouncementSelection }
     *     
     */
    public AnswerConfirmationAnnouncementSelection getAnnouncementMessageSelection() {
        return announcementMessageSelection;
    }

    /**
     * Legt den Wert der announcementMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnswerConfirmationAnnouncementSelection }
     *     
     */
    public void setAnnouncementMessageSelection(AnswerConfirmationAnnouncementSelection value) {
        this.announcementMessageSelection = value;
    }

    /**
     * Ruft den Wert der confirmationMessageAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledFileResource }
     *     
     */
    public LabeledFileResource getConfirmationMessageAudioFile() {
        return confirmationMessageAudioFile;
    }

    /**
     * Legt den Wert der confirmationMessageAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledFileResource }
     *     
     */
    public void setConfirmationMessageAudioFile(LabeledFileResource value) {
        this.confirmationMessageAudioFile = value;
    }

    /**
     * Ruft den Wert der confirmationTimoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getConfirmationTimoutSeconds() {
        return confirmationTimoutSeconds;
    }

    /**
     * Legt den Wert der confirmationTimoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setConfirmationTimoutSeconds(Integer value) {
        this.confirmationTimoutSeconds = value;
    }

}
