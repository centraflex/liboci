//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Directory number range. The minimum and maximum values are inclusive.
 *       
 * 
 * <p>Java-Klasse für EnterpriseTrunkNumberRange complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseTrunkNumberRange">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="dnRange" type="{}DNRange"/>
 *         <element name="extensionLength" type="{}ExtensionLength" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseTrunkNumberRange", propOrder = {
    "dnRange",
    "extensionLength"
})
public class EnterpriseTrunkNumberRange {

    @XmlElement(required = true)
    protected DNRange dnRange;
    protected Integer extensionLength;

    /**
     * Ruft den Wert der dnRange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DNRange }
     *     
     */
    public DNRange getDnRange() {
        return dnRange;
    }

    /**
     * Legt den Wert der dnRange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DNRange }
     *     
     */
    public void setDnRange(DNRange value) {
        this.dnRange = value;
    }

    /**
     * Ruft den Wert der extensionLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExtensionLength() {
        return extensionLength;
    }

    /**
     * Legt den Wert der extensionLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExtensionLength(Integer value) {
        this.extensionLength = value;
    }

}
