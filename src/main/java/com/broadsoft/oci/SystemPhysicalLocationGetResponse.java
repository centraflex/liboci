//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemPhysicalLocationGetRequest.
 *         Contains a list of system Physical Location parameters.
 *       
 * 
 * <p>Java-Klasse für SystemPhysicalLocationGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPhysicalLocationGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="alwaysAllowEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPhysicalLocationGetResponse", propOrder = {
    "alwaysAllowEmergencyCalls"
})
public class SystemPhysicalLocationGetResponse
    extends OCIDataResponse
{

    protected boolean alwaysAllowEmergencyCalls;

    /**
     * Ruft den Wert der alwaysAllowEmergencyCalls-Eigenschaft ab.
     * 
     */
    public boolean isAlwaysAllowEmergencyCalls() {
        return alwaysAllowEmergencyCalls;
    }

    /**
     * Legt den Wert der alwaysAllowEmergencyCalls-Eigenschaft fest.
     * 
     */
    public void setAlwaysAllowEmergencyCalls(boolean value) {
        this.alwaysAllowEmergencyCalls = value;
    }

}
