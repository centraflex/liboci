//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a list of home zones and/or home zone ranges.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupEmergencyZonesModifyHomeZoneListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupEmergencyZonesModifyHomeZoneListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="homeZoneIpAddressList" maxOccurs="unbounded" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="homeZoneIpAddress" type="{}IPAddress"/>
 *                   <element name="newHomeZoneIpAddress" type="{}IPAddress"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="homeZoneIpAddressRangeList" maxOccurs="unbounded" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="homeZoneIpAddressRange" type="{}IPAddressRange"/>
 *                   <element name="newHomeZoneIpAddressRange" type="{}IPAddressRange"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupEmergencyZonesModifyHomeZoneListRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "homeZoneIpAddressList",
    "homeZoneIpAddressRangeList"
})
public class GroupEmergencyZonesModifyHomeZoneListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected List<GroupEmergencyZonesModifyHomeZoneListRequest.HomeZoneIpAddressList> homeZoneIpAddressList;
    protected List<GroupEmergencyZonesModifyHomeZoneListRequest.HomeZoneIpAddressRangeList> homeZoneIpAddressRangeList;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Gets the value of the homeZoneIpAddressList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the homeZoneIpAddressList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHomeZoneIpAddressList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupEmergencyZonesModifyHomeZoneListRequest.HomeZoneIpAddressList }
     * 
     * 
     * @return
     *     The value of the homeZoneIpAddressList property.
     */
    public List<GroupEmergencyZonesModifyHomeZoneListRequest.HomeZoneIpAddressList> getHomeZoneIpAddressList() {
        if (homeZoneIpAddressList == null) {
            homeZoneIpAddressList = new ArrayList<>();
        }
        return this.homeZoneIpAddressList;
    }

    /**
     * Gets the value of the homeZoneIpAddressRangeList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the homeZoneIpAddressRangeList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHomeZoneIpAddressRangeList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupEmergencyZonesModifyHomeZoneListRequest.HomeZoneIpAddressRangeList }
     * 
     * 
     * @return
     *     The value of the homeZoneIpAddressRangeList property.
     */
    public List<GroupEmergencyZonesModifyHomeZoneListRequest.HomeZoneIpAddressRangeList> getHomeZoneIpAddressRangeList() {
        if (homeZoneIpAddressRangeList == null) {
            homeZoneIpAddressRangeList = new ArrayList<>();
        }
        return this.homeZoneIpAddressRangeList;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="homeZoneIpAddress" type="{}IPAddress"/>
     *         <element name="newHomeZoneIpAddress" type="{}IPAddress"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "homeZoneIpAddress",
        "newHomeZoneIpAddress"
    })
    public static class HomeZoneIpAddressList {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String homeZoneIpAddress;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String newHomeZoneIpAddress;

        /**
         * Ruft den Wert der homeZoneIpAddress-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHomeZoneIpAddress() {
            return homeZoneIpAddress;
        }

        /**
         * Legt den Wert der homeZoneIpAddress-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHomeZoneIpAddress(String value) {
            this.homeZoneIpAddress = value;
        }

        /**
         * Ruft den Wert der newHomeZoneIpAddress-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNewHomeZoneIpAddress() {
            return newHomeZoneIpAddress;
        }

        /**
         * Legt den Wert der newHomeZoneIpAddress-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNewHomeZoneIpAddress(String value) {
            this.newHomeZoneIpAddress = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="homeZoneIpAddressRange" type="{}IPAddressRange"/>
     *         <element name="newHomeZoneIpAddressRange" type="{}IPAddressRange"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "homeZoneIpAddressRange",
        "newHomeZoneIpAddressRange"
    })
    public static class HomeZoneIpAddressRangeList {

        @XmlElement(required = true)
        protected IPAddressRange homeZoneIpAddressRange;
        @XmlElement(required = true)
        protected IPAddressRange newHomeZoneIpAddressRange;

        /**
         * Ruft den Wert der homeZoneIpAddressRange-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link IPAddressRange }
         *     
         */
        public IPAddressRange getHomeZoneIpAddressRange() {
            return homeZoneIpAddressRange;
        }

        /**
         * Legt den Wert der homeZoneIpAddressRange-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link IPAddressRange }
         *     
         */
        public void setHomeZoneIpAddressRange(IPAddressRange value) {
            this.homeZoneIpAddressRange = value;
        }

        /**
         * Ruft den Wert der newHomeZoneIpAddressRange-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link IPAddressRange }
         *     
         */
        public IPAddressRange getNewHomeZoneIpAddressRange() {
            return newHomeZoneIpAddressRange;
        }

        /**
         * Legt den Wert der newHomeZoneIpAddressRange-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link IPAddressRange }
         *     
         */
        public void setNewHomeZoneIpAddressRange(IPAddressRange value) {
            this.newHomeZoneIpAddressRange = value;
        }

    }

}
