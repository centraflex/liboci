//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the SystemXsiPolicyProfileGetRequest.
 *         The response contains the Xsi policy profile.
 *       
 * 
 * <p>Java-Klasse für SystemXsiPolicyProfileGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemXsiPolicyProfileGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="description" type="{}XsiPolicyProfileDescription" minOccurs="0"/>
 *         <element name="maxTargetSubscription" type="{}MaxTargetSubscriptionPerEventPackage"/>
 *         <element name="default" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemXsiPolicyProfileGetResponse", propOrder = {
    "description",
    "maxTargetSubscription",
    "_default"
})
public class SystemXsiPolicyProfileGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    protected int maxTargetSubscription;
    @XmlElement(name = "default")
    protected boolean _default;

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der maxTargetSubscription-Eigenschaft ab.
     * 
     */
    public int getMaxTargetSubscription() {
        return maxTargetSubscription;
    }

    /**
     * Legt den Wert der maxTargetSubscription-Eigenschaft fest.
     * 
     */
    public void setMaxTargetSubscription(int value) {
        this.maxTargetSubscription = value;
    }

    /**
     * Ruft den Wert der default-Eigenschaft ab.
     * 
     */
    public boolean isDefault() {
        return _default;
    }

    /**
     * Legt den Wert der default-Eigenschaft fest.
     * 
     */
    public void setDefault(boolean value) {
        this._default = value;
    }

}
