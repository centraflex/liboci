//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserOutgoingCallingPlanRedirectedGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserOutgoingCallingPlanRedirectedGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserOutgoingCallingPlanRedirectedGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useCustomSettings" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="userPermissions" type="{}OutgoingCallingPlanRedirectedPermissions"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserOutgoingCallingPlanRedirectedGetResponse", propOrder = {
    "useCustomSettings",
    "userPermissions"
})
public class UserOutgoingCallingPlanRedirectedGetResponse
    extends OCIDataResponse
{

    protected boolean useCustomSettings;
    @XmlElement(required = true)
    protected OutgoingCallingPlanRedirectedPermissions userPermissions;

    /**
     * Ruft den Wert der useCustomSettings-Eigenschaft ab.
     * 
     */
    public boolean isUseCustomSettings() {
        return useCustomSettings;
    }

    /**
     * Legt den Wert der useCustomSettings-Eigenschaft fest.
     * 
     */
    public void setUseCustomSettings(boolean value) {
        this.useCustomSettings = value;
    }

    /**
     * Ruft den Wert der userPermissions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanRedirectedPermissions }
     *     
     */
    public OutgoingCallingPlanRedirectedPermissions getUserPermissions() {
        return userPermissions;
    }

    /**
     * Legt den Wert der userPermissions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanRedirectedPermissions }
     *     
     */
    public void setUserPermissions(OutgoingCallingPlanRedirectedPermissions value) {
        this.userPermissions = value;
    }

}
