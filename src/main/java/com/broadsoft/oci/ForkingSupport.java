//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ForkingSupport.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ForkingSupport">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Single Dialog"/>
 *     <enumeration value="Multiple Dialogs"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ForkingSupport")
@XmlEnum
public enum ForkingSupport {

    @XmlEnumValue("Single Dialog")
    SINGLE_DIALOG("Single Dialog"),
    @XmlEnumValue("Multiple Dialogs")
    MULTIPLE_DIALOGS("Multiple Dialogs");
    private final String value;

    ForkingSupport(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ForkingSupport fromValue(String v) {
        for (ForkingSupport c: ForkingSupport.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
