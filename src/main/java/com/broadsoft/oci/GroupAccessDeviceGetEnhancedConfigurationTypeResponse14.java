//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupAccessDeviceGetEnhancedConfigurationTypeRequest14.
 *       
 * 
 * <p>Java-Klasse für GroupAccessDeviceGetEnhancedConfigurationTypeResponse14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAccessDeviceGetEnhancedConfigurationTypeResponse14">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="supportsEnhancedConfiguration" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="supportsReset" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="configurationType" type="{}AccessDeviceEnhancedConfigurationType14" minOccurs="0"/>
 *         <element name="configurationFileName" type="{}AccessDeviceEnhancedConfigurationFileName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAccessDeviceGetEnhancedConfigurationTypeResponse14", propOrder = {
    "supportsEnhancedConfiguration",
    "supportsReset",
    "configurationType",
    "configurationFileName"
})
public class GroupAccessDeviceGetEnhancedConfigurationTypeResponse14
    extends OCIDataResponse
{

    protected boolean supportsEnhancedConfiguration;
    protected boolean supportsReset;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String configurationType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String configurationFileName;

    /**
     * Ruft den Wert der supportsEnhancedConfiguration-Eigenschaft ab.
     * 
     */
    public boolean isSupportsEnhancedConfiguration() {
        return supportsEnhancedConfiguration;
    }

    /**
     * Legt den Wert der supportsEnhancedConfiguration-Eigenschaft fest.
     * 
     */
    public void setSupportsEnhancedConfiguration(boolean value) {
        this.supportsEnhancedConfiguration = value;
    }

    /**
     * Ruft den Wert der supportsReset-Eigenschaft ab.
     * 
     */
    public boolean isSupportsReset() {
        return supportsReset;
    }

    /**
     * Legt den Wert der supportsReset-Eigenschaft fest.
     * 
     */
    public void setSupportsReset(boolean value) {
        this.supportsReset = value;
    }

    /**
     * Ruft den Wert der configurationType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfigurationType() {
        return configurationType;
    }

    /**
     * Legt den Wert der configurationType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfigurationType(String value) {
        this.configurationType = value;
    }

    /**
     * Ruft den Wert der configurationFileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfigurationFileName() {
        return configurationFileName;
    }

    /**
     * Legt den Wert der configurationFileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfigurationFileName(String value) {
        this.configurationFileName = value;
    }

}
