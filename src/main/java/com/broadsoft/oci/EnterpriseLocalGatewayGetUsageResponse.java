//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to EnterpriseLocalGatewayGetUsageRequest.
 *         Returns the group ID and group name where the local gateway belongs to, a boolean value to indicate if the local gateway is 
 *         used in the enterprise call processing policy.
 *         It also returns an OCITable containing the groups using the given local gateway. 
 *         Column headings are: "Group Id", "Group Name" and "Group External Id".  
 *         The following columns are only populated in AS data mode
 *         "Group External Id"
 *       
 * 
 * <p>Java-Klasse für EnterpriseLocalGatewayGetUsageResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseLocalGatewayGetUsageResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="gatewayGroupId" type="{}GroupId"/>
 *         <element name="gatewayGroupName" type="{}GroupName" minOccurs="0"/>
 *         <element name="usedByEnterprise" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="groupTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseLocalGatewayGetUsageResponse", propOrder = {
    "gatewayGroupId",
    "gatewayGroupName",
    "usedByEnterprise",
    "groupTable"
})
public class EnterpriseLocalGatewayGetUsageResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String gatewayGroupId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String gatewayGroupName;
    protected boolean usedByEnterprise;
    @XmlElement(required = true)
    protected OCITable groupTable;

    /**
     * Ruft den Wert der gatewayGroupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGatewayGroupId() {
        return gatewayGroupId;
    }

    /**
     * Legt den Wert der gatewayGroupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGatewayGroupId(String value) {
        this.gatewayGroupId = value;
    }

    /**
     * Ruft den Wert der gatewayGroupName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGatewayGroupName() {
        return gatewayGroupName;
    }

    /**
     * Legt den Wert der gatewayGroupName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGatewayGroupName(String value) {
        this.gatewayGroupName = value;
    }

    /**
     * Ruft den Wert der usedByEnterprise-Eigenschaft ab.
     * 
     */
    public boolean isUsedByEnterprise() {
        return usedByEnterprise;
    }

    /**
     * Legt den Wert der usedByEnterprise-Eigenschaft fest.
     * 
     */
    public void setUsedByEnterprise(boolean value) {
        this.usedByEnterprise = value;
    }

    /**
     * Ruft den Wert der groupTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getGroupTable() {
        return groupTable;
    }

    /**
     * Legt den Wert der groupTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setGroupTable(OCITable value) {
        this.groupTable = value;
    }

}
