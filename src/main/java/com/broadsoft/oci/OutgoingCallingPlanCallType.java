//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für OutgoingCallingPlanCallType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="OutgoingCallingPlanCallType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Casual Calls"/>
 *     <enumeration value="Chargeable Directory Assistance"/>
 *     <enumeration value="International"/>
 *     <enumeration value="Local"/>
 *     <enumeration value="Operator Assisted"/>
 *     <enumeration value="Premium Services I"/>
 *     <enumeration value="Premium Services II"/>
 *     <enumeration value="Special Services I"/>
 *     <enumeration value="Special Services II"/>
 *     <enumeration value="Toll"/>
 *     <enumeration value="Toll Free"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "OutgoingCallingPlanCallType")
@XmlEnum
public enum OutgoingCallingPlanCallType {

    @XmlEnumValue("Casual Calls")
    CASUAL_CALLS("Casual Calls"),
    @XmlEnumValue("Chargeable Directory Assistance")
    CHARGEABLE_DIRECTORY_ASSISTANCE("Chargeable Directory Assistance"),
    @XmlEnumValue("International")
    INTERNATIONAL("International"),
    @XmlEnumValue("Local")
    LOCAL("Local"),
    @XmlEnumValue("Operator Assisted")
    OPERATOR_ASSISTED("Operator Assisted"),
    @XmlEnumValue("Premium Services I")
    PREMIUM_SERVICES_I("Premium Services I"),
    @XmlEnumValue("Premium Services II")
    PREMIUM_SERVICES_II("Premium Services II"),
    @XmlEnumValue("Special Services I")
    SPECIAL_SERVICES_I("Special Services I"),
    @XmlEnumValue("Special Services II")
    SPECIAL_SERVICES_II("Special Services II"),
    @XmlEnumValue("Toll")
    TOLL("Toll"),
    @XmlEnumValue("Toll Free")
    TOLL_FREE("Toll Free");
    private final String value;

    OutgoingCallingPlanCallType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OutgoingCallingPlanCallType fromValue(String v) {
        for (OutgoingCallingPlanCallType c: OutgoingCallingPlanCallType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
