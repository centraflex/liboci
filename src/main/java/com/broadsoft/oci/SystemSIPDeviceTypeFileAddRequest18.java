//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *           Request to add a sip device type file.
 *           The response is either SuccessResponse or ErrorResponse.
 *         
 * 
 * <p>Java-Klasse für SystemSIPDeviceTypeFileAddRequest18 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPDeviceTypeFileAddRequest18">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="deviceType" type="{}AccessDeviceType"/>
 *         <element name="fileFormat" type="{}DeviceManagementFileFormat"/>
 *         <element name="remoteFileFormat" type="{}DeviceManagementFileFormat"/>
 *         <element name="fileCategory" type="{}DeviceManagementFileCategory"/>
 *         <element name="fileCustomization" type="{}DeviceManagementFileCustomization"/>
 *         <element name="fileSource" type="{}DeviceTypeFileEnhancedConfigurationMode"/>
 *         <element name="uploadFile" type="{}FileResource" minOccurs="0"/>
 *         <element name="useHttpDigestAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macBasedFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="userNamePasswordFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macInNonRequestURI" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macFormatInNonRequestURI" type="{}DeviceManagementAccessURI" minOccurs="0"/>
 *         <element name="allowHttp" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowHttps" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowTftp" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableCaching" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPDeviceTypeFileAddRequest18", propOrder = {
    "deviceType",
    "fileFormat",
    "remoteFileFormat",
    "fileCategory",
    "fileCustomization",
    "fileSource",
    "uploadFile",
    "useHttpDigestAuthentication",
    "macBasedFileAuthentication",
    "userNamePasswordFileAuthentication",
    "macInNonRequestURI",
    "macFormatInNonRequestURI",
    "allowHttp",
    "allowHttps",
    "allowTftp",
    "enableCaching"
})
public class SystemSIPDeviceTypeFileAddRequest18
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceType;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String fileFormat;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String remoteFileFormat;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceManagementFileCategory fileCategory;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceManagementFileCustomization fileCustomization;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceTypeFileEnhancedConfigurationMode fileSource;
    protected FileResource uploadFile;
    protected boolean useHttpDigestAuthentication;
    protected boolean macBasedFileAuthentication;
    protected boolean userNamePasswordFileAuthentication;
    protected boolean macInNonRequestURI;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String macFormatInNonRequestURI;
    protected boolean allowHttp;
    protected boolean allowHttps;
    protected boolean allowTftp;
    protected boolean enableCaching;

    /**
     * Ruft den Wert der deviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Legt den Wert der deviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceType(String value) {
        this.deviceType = value;
    }

    /**
     * Ruft den Wert der fileFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileFormat() {
        return fileFormat;
    }

    /**
     * Legt den Wert der fileFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileFormat(String value) {
        this.fileFormat = value;
    }

    /**
     * Ruft den Wert der remoteFileFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteFileFormat() {
        return remoteFileFormat;
    }

    /**
     * Legt den Wert der remoteFileFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteFileFormat(String value) {
        this.remoteFileFormat = value;
    }

    /**
     * Ruft den Wert der fileCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementFileCategory }
     *     
     */
    public DeviceManagementFileCategory getFileCategory() {
        return fileCategory;
    }

    /**
     * Legt den Wert der fileCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementFileCategory }
     *     
     */
    public void setFileCategory(DeviceManagementFileCategory value) {
        this.fileCategory = value;
    }

    /**
     * Ruft den Wert der fileCustomization-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementFileCustomization }
     *     
     */
    public DeviceManagementFileCustomization getFileCustomization() {
        return fileCustomization;
    }

    /**
     * Legt den Wert der fileCustomization-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementFileCustomization }
     *     
     */
    public void setFileCustomization(DeviceManagementFileCustomization value) {
        this.fileCustomization = value;
    }

    /**
     * Ruft den Wert der fileSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceTypeFileEnhancedConfigurationMode }
     *     
     */
    public DeviceTypeFileEnhancedConfigurationMode getFileSource() {
        return fileSource;
    }

    /**
     * Legt den Wert der fileSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceTypeFileEnhancedConfigurationMode }
     *     
     */
    public void setFileSource(DeviceTypeFileEnhancedConfigurationMode value) {
        this.fileSource = value;
    }

    /**
     * Ruft den Wert der uploadFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FileResource }
     *     
     */
    public FileResource getUploadFile() {
        return uploadFile;
    }

    /**
     * Legt den Wert der uploadFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FileResource }
     *     
     */
    public void setUploadFile(FileResource value) {
        this.uploadFile = value;
    }

    /**
     * Ruft den Wert der useHttpDigestAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isUseHttpDigestAuthentication() {
        return useHttpDigestAuthentication;
    }

    /**
     * Legt den Wert der useHttpDigestAuthentication-Eigenschaft fest.
     * 
     */
    public void setUseHttpDigestAuthentication(boolean value) {
        this.useHttpDigestAuthentication = value;
    }

    /**
     * Ruft den Wert der macBasedFileAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isMacBasedFileAuthentication() {
        return macBasedFileAuthentication;
    }

    /**
     * Legt den Wert der macBasedFileAuthentication-Eigenschaft fest.
     * 
     */
    public void setMacBasedFileAuthentication(boolean value) {
        this.macBasedFileAuthentication = value;
    }

    /**
     * Ruft den Wert der userNamePasswordFileAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isUserNamePasswordFileAuthentication() {
        return userNamePasswordFileAuthentication;
    }

    /**
     * Legt den Wert der userNamePasswordFileAuthentication-Eigenschaft fest.
     * 
     */
    public void setUserNamePasswordFileAuthentication(boolean value) {
        this.userNamePasswordFileAuthentication = value;
    }

    /**
     * Ruft den Wert der macInNonRequestURI-Eigenschaft ab.
     * 
     */
    public boolean isMacInNonRequestURI() {
        return macInNonRequestURI;
    }

    /**
     * Legt den Wert der macInNonRequestURI-Eigenschaft fest.
     * 
     */
    public void setMacInNonRequestURI(boolean value) {
        this.macInNonRequestURI = value;
    }

    /**
     * Ruft den Wert der macFormatInNonRequestURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacFormatInNonRequestURI() {
        return macFormatInNonRequestURI;
    }

    /**
     * Legt den Wert der macFormatInNonRequestURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacFormatInNonRequestURI(String value) {
        this.macFormatInNonRequestURI = value;
    }

    /**
     * Ruft den Wert der allowHttp-Eigenschaft ab.
     * 
     */
    public boolean isAllowHttp() {
        return allowHttp;
    }

    /**
     * Legt den Wert der allowHttp-Eigenschaft fest.
     * 
     */
    public void setAllowHttp(boolean value) {
        this.allowHttp = value;
    }

    /**
     * Ruft den Wert der allowHttps-Eigenschaft ab.
     * 
     */
    public boolean isAllowHttps() {
        return allowHttps;
    }

    /**
     * Legt den Wert der allowHttps-Eigenschaft fest.
     * 
     */
    public void setAllowHttps(boolean value) {
        this.allowHttps = value;
    }

    /**
     * Ruft den Wert der allowTftp-Eigenschaft ab.
     * 
     */
    public boolean isAllowTftp() {
        return allowTftp;
    }

    /**
     * Legt den Wert der allowTftp-Eigenschaft fest.
     * 
     */
    public void setAllowTftp(boolean value) {
        this.allowTftp = value;
    }

    /**
     * Ruft den Wert der enableCaching-Eigenschaft ab.
     * 
     */
    public boolean isEnableCaching() {
        return enableCaching;
    }

    /**
     * Legt den Wert der enableCaching-Eigenschaft fest.
     * 
     */
    public void setEnableCaching(boolean value) {
        this.enableCaching = value;
    }

}
