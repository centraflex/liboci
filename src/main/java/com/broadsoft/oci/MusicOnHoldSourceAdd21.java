//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains the music on hold source configuration.
 *         The following elements are only used in HSS data mode and ignored in AS data mode:
 *           labeledMediaFiles
 *         The following elements are only used in AS data mode and ignored in HSS data mode:
 *           announcementMediaFiles
 *       
 * 
 * <p>Java-Klasse für MusicOnHoldSourceAdd21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="MusicOnHoldSourceAdd21">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="audioFilePreferredCodec" type="{}AudioFileCodecExtended"/>
 *         <element name="messageSourceSelection" type="{}MusicOnHoldMessageSelection"/>
 *         <choice minOccurs="0">
 *           <element name="labeledCustomSourceMediaFiles">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="audioFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *                     <element name="videoFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *           <element name="announcementCustomSourceMediaFiles">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="audioFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *                     <element name="videoFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *         </choice>
 *         <element name="externalSource" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointAdd"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MusicOnHoldSourceAdd21", propOrder = {
    "audioFilePreferredCodec",
    "messageSourceSelection",
    "labeledCustomSourceMediaFiles",
    "announcementCustomSourceMediaFiles",
    "externalSource"
})
public class MusicOnHoldSourceAdd21 {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AudioFileCodecExtended audioFilePreferredCodec;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MusicOnHoldMessageSelection messageSourceSelection;
    protected MusicOnHoldSourceAdd21 .LabeledCustomSourceMediaFiles labeledCustomSourceMediaFiles;
    protected MusicOnHoldSourceAdd21 .AnnouncementCustomSourceMediaFiles announcementCustomSourceMediaFiles;
    protected MusicOnHoldSourceAdd21 .ExternalSource externalSource;

    /**
     * Ruft den Wert der audioFilePreferredCodec-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AudioFileCodecExtended }
     *     
     */
    public AudioFileCodecExtended getAudioFilePreferredCodec() {
        return audioFilePreferredCodec;
    }

    /**
     * Legt den Wert der audioFilePreferredCodec-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AudioFileCodecExtended }
     *     
     */
    public void setAudioFilePreferredCodec(AudioFileCodecExtended value) {
        this.audioFilePreferredCodec = value;
    }

    /**
     * Ruft den Wert der messageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public MusicOnHoldMessageSelection getMessageSourceSelection() {
        return messageSourceSelection;
    }

    /**
     * Legt den Wert der messageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public void setMessageSourceSelection(MusicOnHoldMessageSelection value) {
        this.messageSourceSelection = value;
    }

    /**
     * Ruft den Wert der labeledCustomSourceMediaFiles-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceAdd21 .LabeledCustomSourceMediaFiles }
     *     
     */
    public MusicOnHoldSourceAdd21 .LabeledCustomSourceMediaFiles getLabeledCustomSourceMediaFiles() {
        return labeledCustomSourceMediaFiles;
    }

    /**
     * Legt den Wert der labeledCustomSourceMediaFiles-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceAdd21 .LabeledCustomSourceMediaFiles }
     *     
     */
    public void setLabeledCustomSourceMediaFiles(MusicOnHoldSourceAdd21 .LabeledCustomSourceMediaFiles value) {
        this.labeledCustomSourceMediaFiles = value;
    }

    /**
     * Ruft den Wert der announcementCustomSourceMediaFiles-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceAdd21 .AnnouncementCustomSourceMediaFiles }
     *     
     */
    public MusicOnHoldSourceAdd21 .AnnouncementCustomSourceMediaFiles getAnnouncementCustomSourceMediaFiles() {
        return announcementCustomSourceMediaFiles;
    }

    /**
     * Legt den Wert der announcementCustomSourceMediaFiles-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceAdd21 .AnnouncementCustomSourceMediaFiles }
     *     
     */
    public void setAnnouncementCustomSourceMediaFiles(MusicOnHoldSourceAdd21 .AnnouncementCustomSourceMediaFiles value) {
        this.announcementCustomSourceMediaFiles = value;
    }

    /**
     * Ruft den Wert der externalSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceAdd21 .ExternalSource }
     *     
     */
    public MusicOnHoldSourceAdd21 .ExternalSource getExternalSource() {
        return externalSource;
    }

    /**
     * Legt den Wert der externalSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceAdd21 .ExternalSource }
     *     
     */
    public void setExternalSource(MusicOnHoldSourceAdd21 .ExternalSource value) {
        this.externalSource = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="audioFile" type="{}AnnouncementFileKey" minOccurs="0"/>
     *         <element name="videoFile" type="{}AnnouncementFileKey" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "audioFile",
        "videoFile"
    })
    public static class AnnouncementCustomSourceMediaFiles {

        protected AnnouncementFileKey audioFile;
        protected AnnouncementFileKey videoFile;

        /**
         * Ruft den Wert der audioFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AnnouncementFileKey }
         *     
         */
        public AnnouncementFileKey getAudioFile() {
            return audioFile;
        }

        /**
         * Legt den Wert der audioFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AnnouncementFileKey }
         *     
         */
        public void setAudioFile(AnnouncementFileKey value) {
            this.audioFile = value;
        }

        /**
         * Ruft den Wert der videoFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AnnouncementFileKey }
         *     
         */
        public AnnouncementFileKey getVideoFile() {
            return videoFile;
        }

        /**
         * Legt den Wert der videoFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AnnouncementFileKey }
         *     
         */
        public void setVideoFile(AnnouncementFileKey value) {
            this.videoFile = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointAdd"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accessDeviceEndpoint"
    })
    public static class ExternalSource {

        @XmlElement(required = true)
        protected AccessDeviceEndpointAdd accessDeviceEndpoint;

        /**
         * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AccessDeviceEndpointAdd }
         *     
         */
        public AccessDeviceEndpointAdd getAccessDeviceEndpoint() {
            return accessDeviceEndpoint;
        }

        /**
         * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AccessDeviceEndpointAdd }
         *     
         */
        public void setAccessDeviceEndpoint(AccessDeviceEndpointAdd value) {
            this.accessDeviceEndpoint = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="audioFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
     *         <element name="videoFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "audioFile",
        "videoFile"
    })
    public static class LabeledCustomSourceMediaFiles {

        protected LabeledMediaFileResource audioFile;
        protected LabeledMediaFileResource videoFile;

        /**
         * Ruft den Wert der audioFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public LabeledMediaFileResource getAudioFile() {
            return audioFile;
        }

        /**
         * Legt den Wert der audioFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public void setAudioFile(LabeledMediaFileResource value) {
            this.audioFile = value;
        }

        /**
         * Ruft den Wert der videoFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public LabeledMediaFileResource getVideoFile() {
            return videoFile;
        }

        /**
         * Legt den Wert der videoFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public void setVideoFile(LabeledMediaFileResource value) {
            this.videoFile = value;
        }

    }

}
