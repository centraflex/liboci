//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemSubscriberGetProvisioningParametersRequest.
 *         
 *         Replaced by: SystemSubscriberGetProvisioningParametersResponse24
 *       
 * 
 * <p>Java-Klasse für SystemSubscriberGetProvisioningParametersResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSubscriberGetProvisioningParametersResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="configurableCLIDNormalization" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSubscriberGetProvisioningParametersResponse", propOrder = {
    "configurableCLIDNormalization"
})
public class SystemSubscriberGetProvisioningParametersResponse
    extends OCIDataResponse
{

    protected boolean configurableCLIDNormalization;

    /**
     * Ruft den Wert der configurableCLIDNormalization-Eigenschaft ab.
     * 
     */
    public boolean isConfigurableCLIDNormalization() {
        return configurableCLIDNormalization;
    }

    /**
     * Legt den Wert der configurableCLIDNormalization-Eigenschaft fest.
     * 
     */
    public void setConfigurableCLIDNormalization(boolean value) {
        this.configurableCLIDNormalization = value;
    }

}
