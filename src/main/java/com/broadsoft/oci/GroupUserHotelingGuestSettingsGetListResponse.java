//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupUserHotelingGuestSettingsGetListRequest.
 *         Contains a table with column headings: "User Id", "Last Name", "First Name",
 *         "Hiragana Last Name", and "Hiragana First Name", "Phone Number",
 *         "Extension", "Department", "In Trunk Group", "Email Address", "Is Active".
 *         "Is Active" is "true" or "false".
 *         "Phone Number" is presented in the E164 format.
 *       
 * 
 * <p>Java-Klasse für GroupUserHotelingGuestSettingsGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupUserHotelingGuestSettingsGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="userHotelingGuestTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupUserHotelingGuestSettingsGetListResponse", propOrder = {
    "userHotelingGuestTable"
})
public class GroupUserHotelingGuestSettingsGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable userHotelingGuestTable;

    /**
     * Ruft den Wert der userHotelingGuestTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserHotelingGuestTable() {
        return userHotelingGuestTable;
    }

    /**
     * Legt den Wert der userHotelingGuestTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserHotelingGuestTable(OCITable value) {
        this.userHotelingGuestTable = value;
    }

}
