//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserCommPilotExpressGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserCommPilotExpressGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCommPilotExpressGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="profile" type="{}CommPilotExpressProfile" minOccurs="0"/>
 *         <element name="availableInOffice" type="{}CommPilotExpressAvailableInOffice"/>
 *         <element name="availableOutOfOffice" type="{}CommPilotExpressAvailableOutOfOffice"/>
 *         <element name="busy" type="{}CommPilotExpressBusy"/>
 *         <element name="unavailable" type="{}CommPilotExpressUnavailable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCommPilotExpressGetResponse", propOrder = {
    "profile",
    "availableInOffice",
    "availableOutOfOffice",
    "busy",
    "unavailable"
})
public class UserCommPilotExpressGetResponse
    extends OCIDataResponse
{

    @XmlSchemaType(name = "token")
    protected CommPilotExpressProfile profile;
    @XmlElement(required = true)
    protected CommPilotExpressAvailableInOffice availableInOffice;
    @XmlElement(required = true)
    protected CommPilotExpressAvailableOutOfOffice availableOutOfOffice;
    @XmlElement(required = true)
    protected CommPilotExpressBusy busy;
    @XmlElement(required = true)
    protected CommPilotExpressUnavailable unavailable;

    /**
     * Ruft den Wert der profile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressProfile }
     *     
     */
    public CommPilotExpressProfile getProfile() {
        return profile;
    }

    /**
     * Legt den Wert der profile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressProfile }
     *     
     */
    public void setProfile(CommPilotExpressProfile value) {
        this.profile = value;
    }

    /**
     * Ruft den Wert der availableInOffice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressAvailableInOffice }
     *     
     */
    public CommPilotExpressAvailableInOffice getAvailableInOffice() {
        return availableInOffice;
    }

    /**
     * Legt den Wert der availableInOffice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressAvailableInOffice }
     *     
     */
    public void setAvailableInOffice(CommPilotExpressAvailableInOffice value) {
        this.availableInOffice = value;
    }

    /**
     * Ruft den Wert der availableOutOfOffice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressAvailableOutOfOffice }
     *     
     */
    public CommPilotExpressAvailableOutOfOffice getAvailableOutOfOffice() {
        return availableOutOfOffice;
    }

    /**
     * Legt den Wert der availableOutOfOffice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressAvailableOutOfOffice }
     *     
     */
    public void setAvailableOutOfOffice(CommPilotExpressAvailableOutOfOffice value) {
        this.availableOutOfOffice = value;
    }

    /**
     * Ruft den Wert der busy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressBusy }
     *     
     */
    public CommPilotExpressBusy getBusy() {
        return busy;
    }

    /**
     * Legt den Wert der busy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressBusy }
     *     
     */
    public void setBusy(CommPilotExpressBusy value) {
        this.busy = value;
    }

    /**
     * Ruft den Wert der unavailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressUnavailable }
     *     
     */
    public CommPilotExpressUnavailable getUnavailable() {
        return unavailable;
    }

    /**
     * Legt den Wert der unavailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressUnavailable }
     *     
     */
    public void setUnavailable(CommPilotExpressUnavailable value) {
        this.unavailable = value;
    }

}
