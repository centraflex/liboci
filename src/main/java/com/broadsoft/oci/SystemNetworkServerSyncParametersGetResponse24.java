//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemNetworkServerSyncParametersGetRequest24.
 *         Contains a list of system Network Server Sync parameters.
 *         The following elements are only used in AS data mode:
 *           syncTrunkGroups
 *           
 *         Replaced by: SystemNetworkServerSyncParametersGetResponse24V2 in AS data mode.
 *       
 * 
 * <p>Java-Klasse für SystemNetworkServerSyncParametersGetResponse24 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNetworkServerSyncParametersGetResponse24">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableSync" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="syncLinePorts" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="syncDeviceManagementInfo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="syncTrunkGroups" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="syncConnectionTimeoutSeconds" type="{}NetworkServerSyncConnectionTimeoutSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNetworkServerSyncParametersGetResponse24", propOrder = {
    "enableSync",
    "syncLinePorts",
    "syncDeviceManagementInfo",
    "syncTrunkGroups",
    "syncConnectionTimeoutSeconds"
})
public class SystemNetworkServerSyncParametersGetResponse24
    extends OCIDataResponse
{

    protected boolean enableSync;
    protected boolean syncLinePorts;
    protected boolean syncDeviceManagementInfo;
    protected boolean syncTrunkGroups;
    protected Integer syncConnectionTimeoutSeconds;

    /**
     * Ruft den Wert der enableSync-Eigenschaft ab.
     * 
     */
    public boolean isEnableSync() {
        return enableSync;
    }

    /**
     * Legt den Wert der enableSync-Eigenschaft fest.
     * 
     */
    public void setEnableSync(boolean value) {
        this.enableSync = value;
    }

    /**
     * Ruft den Wert der syncLinePorts-Eigenschaft ab.
     * 
     */
    public boolean isSyncLinePorts() {
        return syncLinePorts;
    }

    /**
     * Legt den Wert der syncLinePorts-Eigenschaft fest.
     * 
     */
    public void setSyncLinePorts(boolean value) {
        this.syncLinePorts = value;
    }

    /**
     * Ruft den Wert der syncDeviceManagementInfo-Eigenschaft ab.
     * 
     */
    public boolean isSyncDeviceManagementInfo() {
        return syncDeviceManagementInfo;
    }

    /**
     * Legt den Wert der syncDeviceManagementInfo-Eigenschaft fest.
     * 
     */
    public void setSyncDeviceManagementInfo(boolean value) {
        this.syncDeviceManagementInfo = value;
    }

    /**
     * Ruft den Wert der syncTrunkGroups-Eigenschaft ab.
     * 
     */
    public boolean isSyncTrunkGroups() {
        return syncTrunkGroups;
    }

    /**
     * Legt den Wert der syncTrunkGroups-Eigenschaft fest.
     * 
     */
    public void setSyncTrunkGroups(boolean value) {
        this.syncTrunkGroups = value;
    }

    /**
     * Ruft den Wert der syncConnectionTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSyncConnectionTimeoutSeconds() {
        return syncConnectionTimeoutSeconds;
    }

    /**
     * Legt den Wert der syncConnectionTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSyncConnectionTimeoutSeconds(Integer value) {
        this.syncConnectionTimeoutSeconds = value;
    }

}
