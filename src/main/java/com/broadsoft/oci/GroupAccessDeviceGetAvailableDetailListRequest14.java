//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Requests the list of available access devices for assignment to a user
 *         within a group. The list includes devices created at the system, service provider, and group levels.
 *         The response is either GroupAccessDeviceGetAvailableDetailListResponse14 or
 *         ErrorResponse.
 *     
 *         Replaced by: GroupAccessDeviceGetAvailableDetailListRequest19.
 *       
 * 
 * <p>Java-Klasse für GroupAccessDeviceGetAvailableDetailListRequest14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAccessDeviceGetAvailableDetailListRequest14">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="isMusicOnHold" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="onlyVideoCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAccessDeviceGetAvailableDetailListRequest14", propOrder = {
    "serviceProviderId",
    "groupId",
    "isMusicOnHold",
    "onlyVideoCapable"
})
public class GroupAccessDeviceGetAvailableDetailListRequest14
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected boolean isMusicOnHold;
    protected boolean onlyVideoCapable;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der isMusicOnHold-Eigenschaft ab.
     * 
     */
    public boolean isIsMusicOnHold() {
        return isMusicOnHold;
    }

    /**
     * Legt den Wert der isMusicOnHold-Eigenschaft fest.
     * 
     */
    public void setIsMusicOnHold(boolean value) {
        this.isMusicOnHold = value;
    }

    /**
     * Ruft den Wert der onlyVideoCapable-Eigenschaft ab.
     * 
     */
    public boolean isOnlyVideoCapable() {
        return onlyVideoCapable;
    }

    /**
     * Legt den Wert der onlyVideoCapable-Eigenschaft fest.
     * 
     */
    public void setOnlyVideoCapable(boolean value) {
        this.onlyVideoCapable = value;
    }

}
