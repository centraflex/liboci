//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to ResellerMeetMeConferencingGetRequest22.
 *       
 * 
 * <p>Java-Klasse für ResellerMeetMeConferencingGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerMeetMeConferencingGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="conferenceFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="maxAllocatedPorts" type="{}ResellerMeetMeConferencingConferencePorts" minOccurs="0"/>
 *         <element name="disableUnlimitedMeetMePorts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableMaxAllocatedPorts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerMeetMeConferencingGetResponse22", propOrder = {
    "conferenceFromAddress",
    "maxAllocatedPorts",
    "disableUnlimitedMeetMePorts",
    "enableMaxAllocatedPorts"
})
public class ResellerMeetMeConferencingGetResponse22
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String conferenceFromAddress;
    protected Integer maxAllocatedPorts;
    protected Boolean disableUnlimitedMeetMePorts;
    protected Boolean enableMaxAllocatedPorts;

    /**
     * Ruft den Wert der conferenceFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConferenceFromAddress() {
        return conferenceFromAddress;
    }

    /**
     * Legt den Wert der conferenceFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConferenceFromAddress(String value) {
        this.conferenceFromAddress = value;
    }

    /**
     * Ruft den Wert der maxAllocatedPorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxAllocatedPorts() {
        return maxAllocatedPorts;
    }

    /**
     * Legt den Wert der maxAllocatedPorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxAllocatedPorts(Integer value) {
        this.maxAllocatedPorts = value;
    }

    /**
     * Ruft den Wert der disableUnlimitedMeetMePorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisableUnlimitedMeetMePorts() {
        return disableUnlimitedMeetMePorts;
    }

    /**
     * Legt den Wert der disableUnlimitedMeetMePorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisableUnlimitedMeetMePorts(Boolean value) {
        this.disableUnlimitedMeetMePorts = value;
    }

    /**
     * Ruft den Wert der enableMaxAllocatedPorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMaxAllocatedPorts() {
        return enableMaxAllocatedPorts;
    }

    /**
     * Legt den Wert der enableMaxAllocatedPorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMaxAllocatedPorts(Boolean value) {
        this.enableMaxAllocatedPorts = value;
    }

}
