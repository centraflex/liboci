//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to get the Service Provider Number Portability Query Digit Pattern Trigger List information.
 *         The response is either a ServiceProviderNumberPortabilityQueryGetResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderNumberPortabilityQueryGetRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderNumberPortabilityQueryGetRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="includeDigitPatterns" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="searchCriteriaServiceProviderNumberPortabilityQueryDigitPattern" type="{}SearchCriteriaServiceProviderNumberPortabilityQueryDigitPattern" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderNumberPortabilityQueryGetRequest", propOrder = {
    "serviceProviderId",
    "includeDigitPatterns",
    "searchCriteriaServiceProviderNumberPortabilityQueryDigitPattern",
    "responseSizeLimit"
})
public class ServiceProviderNumberPortabilityQueryGetRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected boolean includeDigitPatterns;
    protected List<SearchCriteriaServiceProviderNumberPortabilityQueryDigitPattern> searchCriteriaServiceProviderNumberPortabilityQueryDigitPattern;
    protected Integer responseSizeLimit;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der includeDigitPatterns-Eigenschaft ab.
     * 
     */
    public boolean isIncludeDigitPatterns() {
        return includeDigitPatterns;
    }

    /**
     * Legt den Wert der includeDigitPatterns-Eigenschaft fest.
     * 
     */
    public void setIncludeDigitPatterns(boolean value) {
        this.includeDigitPatterns = value;
    }

    /**
     * Gets the value of the searchCriteriaServiceProviderNumberPortabilityQueryDigitPattern property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaServiceProviderNumberPortabilityQueryDigitPattern property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaServiceProviderNumberPortabilityQueryDigitPattern().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaServiceProviderNumberPortabilityQueryDigitPattern }
     * 
     * 
     * @return
     *     The value of the searchCriteriaServiceProviderNumberPortabilityQueryDigitPattern property.
     */
    public List<SearchCriteriaServiceProviderNumberPortabilityQueryDigitPattern> getSearchCriteriaServiceProviderNumberPortabilityQueryDigitPattern() {
        if (searchCriteriaServiceProviderNumberPortabilityQueryDigitPattern == null) {
            searchCriteriaServiceProviderNumberPortabilityQueryDigitPattern = new ArrayList<>();
        }
        return this.searchCriteriaServiceProviderNumberPortabilityQueryDigitPattern;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

}
