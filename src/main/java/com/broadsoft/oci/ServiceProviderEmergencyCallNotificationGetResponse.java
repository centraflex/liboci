//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to ServiceProviderEmergencyCallNotificationGetRequest.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderEmergencyCallNotificationGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderEmergencyCallNotificationGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="sendEmergencyCallNotificationEmail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="emergencyCallNotifyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="allowGroupOverride" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderEmergencyCallNotificationGetResponse", propOrder = {
    "sendEmergencyCallNotificationEmail",
    "emergencyCallNotifyEmailAddress",
    "allowGroupOverride"
})
public class ServiceProviderEmergencyCallNotificationGetResponse
    extends OCIDataResponse
{

    protected boolean sendEmergencyCallNotificationEmail;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String emergencyCallNotifyEmailAddress;
    protected boolean allowGroupOverride;

    /**
     * Ruft den Wert der sendEmergencyCallNotificationEmail-Eigenschaft ab.
     * 
     */
    public boolean isSendEmergencyCallNotificationEmail() {
        return sendEmergencyCallNotificationEmail;
    }

    /**
     * Legt den Wert der sendEmergencyCallNotificationEmail-Eigenschaft fest.
     * 
     */
    public void setSendEmergencyCallNotificationEmail(boolean value) {
        this.sendEmergencyCallNotificationEmail = value;
    }

    /**
     * Ruft den Wert der emergencyCallNotifyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmergencyCallNotifyEmailAddress() {
        return emergencyCallNotifyEmailAddress;
    }

    /**
     * Legt den Wert der emergencyCallNotifyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmergencyCallNotifyEmailAddress(String value) {
        this.emergencyCallNotifyEmailAddress = value;
    }

    /**
     * Ruft den Wert der allowGroupOverride-Eigenschaft ab.
     * 
     */
    public boolean isAllowGroupOverride() {
        return allowGroupOverride;
    }

    /**
     * Legt den Wert der allowGroupOverride-Eigenschaft fest.
     * 
     */
    public void setAllowGroupOverride(boolean value) {
        this.allowGroupOverride = value;
    }

}
