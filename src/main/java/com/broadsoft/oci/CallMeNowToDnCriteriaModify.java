//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The To dn criteria used on the call me now external number to be modified.
 *       
 * 
 * <p>Java-Klasse für CallMeNowToDnCriteriaModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallMeNowToDnCriteriaModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="toDnCriteriaSelection" type="{}CriteriaDnSelection" minOccurs="0"/>
 *         <element name="phoneNumberList" type="{}CriteriaReplacementDNList" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallMeNowToDnCriteriaModify", propOrder = {
    "toDnCriteriaSelection",
    "phoneNumberList"
})
public class CallMeNowToDnCriteriaModify {

    @XmlSchemaType(name = "token")
    protected CriteriaDnSelection toDnCriteriaSelection;
    @XmlElementRef(name = "phoneNumberList", type = JAXBElement.class, required = false)
    protected JAXBElement<CriteriaReplacementDNList> phoneNumberList;

    /**
     * Ruft den Wert der toDnCriteriaSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaDnSelection }
     *     
     */
    public CriteriaDnSelection getToDnCriteriaSelection() {
        return toDnCriteriaSelection;
    }

    /**
     * Legt den Wert der toDnCriteriaSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaDnSelection }
     *     
     */
    public void setToDnCriteriaSelection(CriteriaDnSelection value) {
        this.toDnCriteriaSelection = value;
    }

    /**
     * Ruft den Wert der phoneNumberList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CriteriaReplacementDNList }{@code >}
     *     
     */
    public JAXBElement<CriteriaReplacementDNList> getPhoneNumberList() {
        return phoneNumberList;
    }

    /**
     * Legt den Wert der phoneNumberList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CriteriaReplacementDNList }{@code >}
     *     
     */
    public void setPhoneNumberList(JAXBElement<CriteriaReplacementDNList> value) {
        this.phoneNumberList = value;
    }

}
