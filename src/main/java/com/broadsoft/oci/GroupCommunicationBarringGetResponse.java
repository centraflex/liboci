//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupCommunicationBarringGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupCommunicationBarringGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCommunicationBarringGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useDefaultServiceProviderProfile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="profile" type="{}CommunicationBarringProfileName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCommunicationBarringGetResponse", propOrder = {
    "useDefaultServiceProviderProfile",
    "profile"
})
public class GroupCommunicationBarringGetResponse
    extends OCIDataResponse
{

    protected boolean useDefaultServiceProviderProfile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String profile;

    /**
     * Ruft den Wert der useDefaultServiceProviderProfile-Eigenschaft ab.
     * 
     */
    public boolean isUseDefaultServiceProviderProfile() {
        return useDefaultServiceProviderProfile;
    }

    /**
     * Legt den Wert der useDefaultServiceProviderProfile-Eigenschaft fest.
     * 
     */
    public void setUseDefaultServiceProviderProfile(boolean value) {
        this.useDefaultServiceProviderProfile = value;
    }

    /**
     * Ruft den Wert der profile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfile() {
        return profile;
    }

    /**
     * Legt den Wert der profile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfile(String value) {
        this.profile = value;
    }

}
