//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         	This is the configuration parameters for Call Transfer service
 *         	
 * 
 * <p>Java-Klasse für ProfileAndServiceCallTransferInfo complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ProfileAndServiceCallTransferInfo">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="isRecallActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="recallNumberOfRings" type="{}CallTransferRecallNumberOfRings"/>
 *         <element name="useDiversionInhibitorForBlindTransfer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useDiversionInhibitorForConsultativeCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableBusyCampOn" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="busyCampOnSeconds" type="{}CallTransferBusyCampOnSeconds"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfileAndServiceCallTransferInfo", propOrder = {
    "isRecallActive",
    "recallNumberOfRings",
    "useDiversionInhibitorForBlindTransfer",
    "useDiversionInhibitorForConsultativeCalls",
    "enableBusyCampOn",
    "busyCampOnSeconds"
})
public class ProfileAndServiceCallTransferInfo {

    protected boolean isRecallActive;
    protected int recallNumberOfRings;
    protected boolean useDiversionInhibitorForBlindTransfer;
    protected boolean useDiversionInhibitorForConsultativeCalls;
    protected boolean enableBusyCampOn;
    protected int busyCampOnSeconds;

    /**
     * Ruft den Wert der isRecallActive-Eigenschaft ab.
     * 
     */
    public boolean isIsRecallActive() {
        return isRecallActive;
    }

    /**
     * Legt den Wert der isRecallActive-Eigenschaft fest.
     * 
     */
    public void setIsRecallActive(boolean value) {
        this.isRecallActive = value;
    }

    /**
     * Ruft den Wert der recallNumberOfRings-Eigenschaft ab.
     * 
     */
    public int getRecallNumberOfRings() {
        return recallNumberOfRings;
    }

    /**
     * Legt den Wert der recallNumberOfRings-Eigenschaft fest.
     * 
     */
    public void setRecallNumberOfRings(int value) {
        this.recallNumberOfRings = value;
    }

    /**
     * Ruft den Wert der useDiversionInhibitorForBlindTransfer-Eigenschaft ab.
     * 
     */
    public boolean isUseDiversionInhibitorForBlindTransfer() {
        return useDiversionInhibitorForBlindTransfer;
    }

    /**
     * Legt den Wert der useDiversionInhibitorForBlindTransfer-Eigenschaft fest.
     * 
     */
    public void setUseDiversionInhibitorForBlindTransfer(boolean value) {
        this.useDiversionInhibitorForBlindTransfer = value;
    }

    /**
     * Ruft den Wert der useDiversionInhibitorForConsultativeCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseDiversionInhibitorForConsultativeCalls() {
        return useDiversionInhibitorForConsultativeCalls;
    }

    /**
     * Legt den Wert der useDiversionInhibitorForConsultativeCalls-Eigenschaft fest.
     * 
     */
    public void setUseDiversionInhibitorForConsultativeCalls(boolean value) {
        this.useDiversionInhibitorForConsultativeCalls = value;
    }

    /**
     * Ruft den Wert der enableBusyCampOn-Eigenschaft ab.
     * 
     */
    public boolean isEnableBusyCampOn() {
        return enableBusyCampOn;
    }

    /**
     * Legt den Wert der enableBusyCampOn-Eigenschaft fest.
     * 
     */
    public void setEnableBusyCampOn(boolean value) {
        this.enableBusyCampOn = value;
    }

    /**
     * Ruft den Wert der busyCampOnSeconds-Eigenschaft ab.
     * 
     */
    public int getBusyCampOnSeconds() {
        return busyCampOnSeconds;
    }

    /**
     * Legt den Wert der busyCampOnSeconds-Eigenschaft fest.
     * 
     */
    public void setBusyCampOnSeconds(int value) {
        this.busyCampOnSeconds = value;
    }

}
