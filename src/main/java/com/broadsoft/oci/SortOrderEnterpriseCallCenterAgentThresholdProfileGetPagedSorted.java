//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Used to sort the EnterpriseCallCenterAgentThresholdProfileGetPagedSortedRequest request.
 *       
 * 
 * <p>Java-Klasse für SortOrderEnterpriseCallCenterAgentThresholdProfileGetPagedSorted complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SortOrderEnterpriseCallCenterAgentThresholdProfileGetPagedSorted">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <choice>
 *         <element name="sortByUserId" type="{}SortByUserId"/>
 *         <element name="sortByUserLastName" type="{}SortByUserLastName"/>
 *         <element name="sortByUserFirstName" type="{}SortByUserFirstName"/>
 *         <element name="sortByDn" type="{}SortByDn"/>
 *         <element name="sortByExtension" type="{}SortByExtension"/>
 *         <element name="sortByDepartmentName" type="{}SortByDepartmentName"/>
 *         <element name="sortByEmailAddress" type="{}SortByEmailAddress"/>
 *       </choice>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SortOrderEnterpriseCallCenterAgentThresholdProfileGetPagedSorted", propOrder = {
    "sortByUserId",
    "sortByUserLastName",
    "sortByUserFirstName",
    "sortByDn",
    "sortByExtension",
    "sortByDepartmentName",
    "sortByEmailAddress"
})
public class SortOrderEnterpriseCallCenterAgentThresholdProfileGetPagedSorted {

    protected SortByUserId sortByUserId;
    protected SortByUserLastName sortByUserLastName;
    protected SortByUserFirstName sortByUserFirstName;
    protected SortByDn sortByDn;
    protected SortByExtension sortByExtension;
    protected SortByDepartmentName sortByDepartmentName;
    protected SortByEmailAddress sortByEmailAddress;

    /**
     * Ruft den Wert der sortByUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByUserId }
     *     
     */
    public SortByUserId getSortByUserId() {
        return sortByUserId;
    }

    /**
     * Legt den Wert der sortByUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByUserId }
     *     
     */
    public void setSortByUserId(SortByUserId value) {
        this.sortByUserId = value;
    }

    /**
     * Ruft den Wert der sortByUserLastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByUserLastName }
     *     
     */
    public SortByUserLastName getSortByUserLastName() {
        return sortByUserLastName;
    }

    /**
     * Legt den Wert der sortByUserLastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByUserLastName }
     *     
     */
    public void setSortByUserLastName(SortByUserLastName value) {
        this.sortByUserLastName = value;
    }

    /**
     * Ruft den Wert der sortByUserFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByUserFirstName }
     *     
     */
    public SortByUserFirstName getSortByUserFirstName() {
        return sortByUserFirstName;
    }

    /**
     * Legt den Wert der sortByUserFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByUserFirstName }
     *     
     */
    public void setSortByUserFirstName(SortByUserFirstName value) {
        this.sortByUserFirstName = value;
    }

    /**
     * Ruft den Wert der sortByDn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByDn }
     *     
     */
    public SortByDn getSortByDn() {
        return sortByDn;
    }

    /**
     * Legt den Wert der sortByDn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByDn }
     *     
     */
    public void setSortByDn(SortByDn value) {
        this.sortByDn = value;
    }

    /**
     * Ruft den Wert der sortByExtension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByExtension }
     *     
     */
    public SortByExtension getSortByExtension() {
        return sortByExtension;
    }

    /**
     * Legt den Wert der sortByExtension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByExtension }
     *     
     */
    public void setSortByExtension(SortByExtension value) {
        this.sortByExtension = value;
    }

    /**
     * Ruft den Wert der sortByDepartmentName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByDepartmentName }
     *     
     */
    public SortByDepartmentName getSortByDepartmentName() {
        return sortByDepartmentName;
    }

    /**
     * Legt den Wert der sortByDepartmentName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByDepartmentName }
     *     
     */
    public void setSortByDepartmentName(SortByDepartmentName value) {
        this.sortByDepartmentName = value;
    }

    /**
     * Ruft den Wert der sortByEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByEmailAddress }
     *     
     */
    public SortByEmailAddress getSortByEmailAddress() {
        return sortByEmailAddress;
    }

    /**
     * Legt den Wert der sortByEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByEmailAddress }
     *     
     */
    public void setSortByEmailAddress(SortByEmailAddress value) {
        this.sortByEmailAddress = value;
    }

}
