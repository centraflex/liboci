//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserCallCenterAgentDetailsGetRequest. 
 *         Contains the detail information for a Call Center Agent.
 *       
 * 
 * <p>Java-Klasse für UserCallCenterAgentDetailsGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallCenterAgentDetailsGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isCallCenterBasicAssigned" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isCallCenterStandardAssigned" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isCallCenterPremiumAssigned" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallCenterAgentDetailsGetResponse", propOrder = {
    "isCallCenterBasicAssigned",
    "isCallCenterStandardAssigned",
    "isCallCenterPremiumAssigned"
})
public class UserCallCenterAgentDetailsGetResponse
    extends OCIDataResponse
{

    protected boolean isCallCenterBasicAssigned;
    protected boolean isCallCenterStandardAssigned;
    protected boolean isCallCenterPremiumAssigned;

    /**
     * Ruft den Wert der isCallCenterBasicAssigned-Eigenschaft ab.
     * 
     */
    public boolean isIsCallCenterBasicAssigned() {
        return isCallCenterBasicAssigned;
    }

    /**
     * Legt den Wert der isCallCenterBasicAssigned-Eigenschaft fest.
     * 
     */
    public void setIsCallCenterBasicAssigned(boolean value) {
        this.isCallCenterBasicAssigned = value;
    }

    /**
     * Ruft den Wert der isCallCenterStandardAssigned-Eigenschaft ab.
     * 
     */
    public boolean isIsCallCenterStandardAssigned() {
        return isCallCenterStandardAssigned;
    }

    /**
     * Legt den Wert der isCallCenterStandardAssigned-Eigenschaft fest.
     * 
     */
    public void setIsCallCenterStandardAssigned(boolean value) {
        this.isCallCenterStandardAssigned = value;
    }

    /**
     * Ruft den Wert der isCallCenterPremiumAssigned-Eigenschaft ab.
     * 
     */
    public boolean isIsCallCenterPremiumAssigned() {
        return isCallCenterPremiumAssigned;
    }

    /**
     * Legt den Wert der isCallCenterPremiumAssigned-Eigenschaft fest.
     * 
     */
    public void setIsCallCenterPremiumAssigned(boolean value) {
        this.isCallCenterPremiumAssigned = value;
    }

}
