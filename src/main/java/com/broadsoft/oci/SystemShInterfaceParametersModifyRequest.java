//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modifies the Sh Interface system parameters.  This request must be submitted on both nodes in the redundant Application Server cluster in order for the changes to take effect on each node without requiring a restart.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 *         Replaced by: SystemShInterfaceParametersModifyRequest17
 *       
 * 
 * <p>Java-Klasse für SystemShInterfaceParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemShInterfaceParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="hssRealm" type="{}DomainName" minOccurs="0"/>
 *         <element name="requestTimeoutSeconds" type="{}ShInterfaceRequestTimeoutSeconds" minOccurs="0"/>
 *         <element name="publicIdentityRefreshDelaySeconds" type="{}ShInterfacePublicIdentityRefreshDelaySeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemShInterfaceParametersModifyRequest", propOrder = {
    "hssRealm",
    "requestTimeoutSeconds",
    "publicIdentityRefreshDelaySeconds"
})
public class SystemShInterfaceParametersModifyRequest
    extends OCIRequest
{

    @XmlElementRef(name = "hssRealm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> hssRealm;
    protected Integer requestTimeoutSeconds;
    protected Integer publicIdentityRefreshDelaySeconds;

    /**
     * Ruft den Wert der hssRealm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHssRealm() {
        return hssRealm;
    }

    /**
     * Legt den Wert der hssRealm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHssRealm(JAXBElement<String> value) {
        this.hssRealm = value;
    }

    /**
     * Ruft den Wert der requestTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRequestTimeoutSeconds() {
        return requestTimeoutSeconds;
    }

    /**
     * Legt den Wert der requestTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRequestTimeoutSeconds(Integer value) {
        this.requestTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der publicIdentityRefreshDelaySeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPublicIdentityRefreshDelaySeconds() {
        return publicIdentityRefreshDelaySeconds;
    }

    /**
     * Legt den Wert der publicIdentityRefreshDelaySeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPublicIdentityRefreshDelaySeconds(Integer value) {
        this.publicIdentityRefreshDelaySeconds = value;
    }

}
