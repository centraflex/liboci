//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für BroadWorksMobilityServiceProviderSettingLevel.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="BroadWorksMobilityServiceProviderSettingLevel">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="ServiceProvider"/>
 *     <enumeration value="System"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "BroadWorksMobilityServiceProviderSettingLevel")
@XmlEnum
public enum BroadWorksMobilityServiceProviderSettingLevel {

    @XmlEnumValue("ServiceProvider")
    SERVICE_PROVIDER("ServiceProvider"),
    @XmlEnumValue("System")
    SYSTEM("System");
    private final String value;

    BroadWorksMobilityServiceProviderSettingLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BroadWorksMobilityServiceProviderSettingLevel fromValue(String v) {
        for (BroadWorksMobilityServiceProviderSettingLevel c: BroadWorksMobilityServiceProviderSettingLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
