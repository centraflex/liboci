//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add an entry to the SIP access control list.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemSIPAddACLEntryRequest14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPAddACLEntryRequest14">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="netAddress" type="{}NetAddress"/>
 *         <element name="transportProtocol" type="{}SIPACLTransportProtocol"/>
 *         <element name="description" type="{}NetworkACLEntryDescription" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPAddACLEntryRequest14", propOrder = {
    "netAddress",
    "transportProtocol",
    "description"
})
public class SystemSIPAddACLEntryRequest14
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String netAddress;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SIPACLTransportProtocol transportProtocol;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;

    /**
     * Ruft den Wert der netAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetAddress() {
        return netAddress;
    }

    /**
     * Legt den Wert der netAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetAddress(String value) {
        this.netAddress = value;
    }

    /**
     * Ruft den Wert der transportProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SIPACLTransportProtocol }
     *     
     */
    public SIPACLTransportProtocol getTransportProtocol() {
        return transportProtocol;
    }

    /**
     * Legt den Wert der transportProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SIPACLTransportProtocol }
     *     
     */
    public void setTransportProtocol(SIPACLTransportProtocol value) {
        this.transportProtocol = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

}
