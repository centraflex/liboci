//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         	This is the configuration parameters for Call Transfer service
 *         	
 * 
 * <p>Java-Klasse für ProfileAndServiceCallWaitingInfo complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ProfileAndServiceCallWaitingInfo">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disableCallingLineIdDelivery" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfileAndServiceCallWaitingInfo", propOrder = {
    "isActive",
    "disableCallingLineIdDelivery"
})
public class ProfileAndServiceCallWaitingInfo {

    protected boolean isActive;
    protected boolean disableCallingLineIdDelivery;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der disableCallingLineIdDelivery-Eigenschaft ab.
     * 
     */
    public boolean isDisableCallingLineIdDelivery() {
        return disableCallingLineIdDelivery;
    }

    /**
     * Legt den Wert der disableCallingLineIdDelivery-Eigenschaft fest.
     * 
     */
    public void setDisableCallingLineIdDelivery(boolean value) {
        this.disableCallingLineIdDelivery = value;
    }

}
