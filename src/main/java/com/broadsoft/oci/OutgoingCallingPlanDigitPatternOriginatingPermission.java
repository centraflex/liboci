//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Indicates whether originating calls using specified digit patterns are permitted.
 *       
 * 
 * <p>Java-Klasse für OutgoingCallingPlanDigitPatternOriginatingPermission complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OutgoingCallingPlanDigitPatternOriginatingPermission">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="digitPatternName" type="{}CallingPlanDigitPatternName"/>
 *         <element name="permission" type="{}OutgoingCallingPlanOriginatingPermission"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingCallingPlanDigitPatternOriginatingPermission", propOrder = {
    "digitPatternName",
    "permission"
})
public class OutgoingCallingPlanDigitPatternOriginatingPermission {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String digitPatternName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OutgoingCallingPlanOriginatingPermission permission;

    /**
     * Ruft den Wert der digitPatternName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDigitPatternName() {
        return digitPatternName;
    }

    /**
     * Legt den Wert der digitPatternName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDigitPatternName(String value) {
        this.digitPatternName = value;
    }

    /**
     * Ruft den Wert der permission-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public OutgoingCallingPlanOriginatingPermission getPermission() {
        return permission;
    }

    /**
     * Legt den Wert der permission-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public void setPermission(OutgoingCallingPlanOriginatingPermission value) {
        this.permission = value;
    }

}
