//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *             Request to add a Media to the system.
 *             The response is either a SuccessResponse or an ErrorResponse.
 *          
 * 
 * <p>Java-Klasse für SystemMediaAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemMediaAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="mediaName" type="{}MediaName"/>
 *         <element name="codecName" type="{}CodecName"/>
 *         <element name="mediaType" type="{}MediaType"/>
 *         <element name="bandwidthEnforcementType" type="{}MediaBandwidthEnforcementType"/>
 *         <element name="mediaBandwidth" type="{}MediaBandwidthBitsPerSecond" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemMediaAddRequest", propOrder = {
    "mediaName",
    "codecName",
    "mediaType",
    "bandwidthEnforcementType",
    "mediaBandwidth"
})
public class SystemMediaAddRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mediaName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String codecName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MediaType mediaType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MediaBandwidthEnforcementType bandwidthEnforcementType;
    protected Integer mediaBandwidth;

    /**
     * Ruft den Wert der mediaName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMediaName() {
        return mediaName;
    }

    /**
     * Legt den Wert der mediaName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMediaName(String value) {
        this.mediaName = value;
    }

    /**
     * Ruft den Wert der codecName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodecName() {
        return codecName;
    }

    /**
     * Legt den Wert der codecName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodecName(String value) {
        this.codecName = value;
    }

    /**
     * Ruft den Wert der mediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MediaType }
     *     
     */
    public MediaType getMediaType() {
        return mediaType;
    }

    /**
     * Legt den Wert der mediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MediaType }
     *     
     */
    public void setMediaType(MediaType value) {
        this.mediaType = value;
    }

    /**
     * Ruft den Wert der bandwidthEnforcementType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MediaBandwidthEnforcementType }
     *     
     */
    public MediaBandwidthEnforcementType getBandwidthEnforcementType() {
        return bandwidthEnforcementType;
    }

    /**
     * Legt den Wert der bandwidthEnforcementType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MediaBandwidthEnforcementType }
     *     
     */
    public void setBandwidthEnforcementType(MediaBandwidthEnforcementType value) {
        this.bandwidthEnforcementType = value;
    }

    /**
     * Ruft den Wert der mediaBandwidth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMediaBandwidth() {
        return mediaBandwidth;
    }

    /**
     * Legt den Wert der mediaBandwidth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMediaBandwidth(Integer value) {
        this.mediaBandwidth = value;
    }

}
