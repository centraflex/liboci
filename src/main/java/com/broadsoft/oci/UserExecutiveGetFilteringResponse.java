//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserExecutiveGetFilteringRequest.
 *         Contains the filtering setting and a table of filtering criteria.
 *         The criteria table's column headings are: "Is Active", "Criteria Name", "Time Schedule", "Calls From", 
 *         "Filter", "Holiday Schedule", "Calls To Type", "Calls To Number" and "Calls To Extension".      
 *         The "Filter" column can contain "true" or "false".
 *         The possible values for the "Calls To Type" column are the following or a combination of them separated by comma:
 *           - Primary
 *           - Alternate X (where x is a number between 1 and 10)
 *           - Mobility        
 *         The possible values for the "Calls To Number" column are the following or a combination of them separated by comma:
 *           - The value of the phone number for the corresponding Calls To Type, when the number is available. i.e. Alternate 1 may have extension, but no number.
 *           - When no number is available a blank space is provided instead.
 *         The possible values for the "Calls To Extension" column are the following or a combination of them separated by comma:
 *           - The value of the extension for the corresponding Calls To Type, when the extension is available. i.e. Primary may have number, but no extension.
 *           - For Mobility Calls To Type, this is always blank.
 *           - When no extension is available a blank space is provided instead.
 *         
 * 
 * <p>Java-Klasse für UserExecutiveGetFilteringResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserExecutiveGetFilteringResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableFiltering" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="filteringMode" type="{}ExecutiveCallFilteringMode"/>
 *         <element name="simpleFilterType" type="{}ExecutiveCallFilteringSimpleFilterType"/>
 *         <element name="criteriaTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserExecutiveGetFilteringResponse", propOrder = {
    "enableFiltering",
    "filteringMode",
    "simpleFilterType",
    "criteriaTable"
})
public class UserExecutiveGetFilteringResponse
    extends OCIDataResponse
{

    protected boolean enableFiltering;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExecutiveCallFilteringMode filteringMode;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExecutiveCallFilteringSimpleFilterType simpleFilterType;
    @XmlElement(required = true)
    protected OCITable criteriaTable;

    /**
     * Ruft den Wert der enableFiltering-Eigenschaft ab.
     * 
     */
    public boolean isEnableFiltering() {
        return enableFiltering;
    }

    /**
     * Legt den Wert der enableFiltering-Eigenschaft fest.
     * 
     */
    public void setEnableFiltering(boolean value) {
        this.enableFiltering = value;
    }

    /**
     * Ruft den Wert der filteringMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveCallFilteringMode }
     *     
     */
    public ExecutiveCallFilteringMode getFilteringMode() {
        return filteringMode;
    }

    /**
     * Legt den Wert der filteringMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveCallFilteringMode }
     *     
     */
    public void setFilteringMode(ExecutiveCallFilteringMode value) {
        this.filteringMode = value;
    }

    /**
     * Ruft den Wert der simpleFilterType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveCallFilteringSimpleFilterType }
     *     
     */
    public ExecutiveCallFilteringSimpleFilterType getSimpleFilterType() {
        return simpleFilterType;
    }

    /**
     * Legt den Wert der simpleFilterType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveCallFilteringSimpleFilterType }
     *     
     */
    public void setSimpleFilterType(ExecutiveCallFilteringSimpleFilterType value) {
        this.simpleFilterType = value;
    }

    /**
     * Ruft den Wert der criteriaTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getCriteriaTable() {
        return criteriaTable;
    }

    /**
     * Legt den Wert der criteriaTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setCriteriaTable(OCITable value) {
        this.criteriaTable = value;
    }

}
