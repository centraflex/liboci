//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal voice messaging menu keys.
 *       
 * 
 * <p>Java-Klasse für VoiceMessagingMenuKeysReadEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="VoiceMessagingMenuKeysReadEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="playMessages" type="{}DigitAny" minOccurs="0"/>
 *         <element name="changeBusyGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="changeNoAnswerGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="changeExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="deleteAllMessages" type="{}DigitAny" minOccurs="0"/>
 *         <element name="passcode" type="{}DigitAny" minOccurs="0"/>
 *         <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
 *         <element name="messageDeposit" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoiceMessagingMenuKeysReadEntry", propOrder = {
    "playMessages",
    "changeBusyGreeting",
    "changeNoAnswerGreeting",
    "changeExtendedAwayGreeting",
    "composeMessage",
    "deleteAllMessages",
    "passcode",
    "personalizedName",
    "messageDeposit",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class VoiceMessagingMenuKeysReadEntry {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String playMessages;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String changeBusyGreeting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String changeNoAnswerGreeting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String changeExtendedAwayGreeting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String composeMessage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deleteAllMessages;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String passcode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String personalizedName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String messageDeposit;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String repeatMenu;

    /**
     * Ruft den Wert der playMessages-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlayMessages() {
        return playMessages;
    }

    /**
     * Legt den Wert der playMessages-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlayMessages(String value) {
        this.playMessages = value;
    }

    /**
     * Ruft den Wert der changeBusyGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeBusyGreeting() {
        return changeBusyGreeting;
    }

    /**
     * Legt den Wert der changeBusyGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeBusyGreeting(String value) {
        this.changeBusyGreeting = value;
    }

    /**
     * Ruft den Wert der changeNoAnswerGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeNoAnswerGreeting() {
        return changeNoAnswerGreeting;
    }

    /**
     * Legt den Wert der changeNoAnswerGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeNoAnswerGreeting(String value) {
        this.changeNoAnswerGreeting = value;
    }

    /**
     * Ruft den Wert der changeExtendedAwayGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeExtendedAwayGreeting() {
        return changeExtendedAwayGreeting;
    }

    /**
     * Legt den Wert der changeExtendedAwayGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeExtendedAwayGreeting(String value) {
        this.changeExtendedAwayGreeting = value;
    }

    /**
     * Ruft den Wert der composeMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComposeMessage() {
        return composeMessage;
    }

    /**
     * Legt den Wert der composeMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComposeMessage(String value) {
        this.composeMessage = value;
    }

    /**
     * Ruft den Wert der deleteAllMessages-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeleteAllMessages() {
        return deleteAllMessages;
    }

    /**
     * Legt den Wert der deleteAllMessages-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeleteAllMessages(String value) {
        this.deleteAllMessages = value;
    }

    /**
     * Ruft den Wert der passcode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasscode() {
        return passcode;
    }

    /**
     * Legt den Wert der passcode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasscode(String value) {
        this.passcode = value;
    }

    /**
     * Ruft den Wert der personalizedName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalizedName() {
        return personalizedName;
    }

    /**
     * Legt den Wert der personalizedName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalizedName(String value) {
        this.personalizedName = value;
    }

    /**
     * Ruft den Wert der messageDeposit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageDeposit() {
        return messageDeposit;
    }

    /**
     * Legt den Wert der messageDeposit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageDeposit(String value) {
        this.messageDeposit = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepeatMenu(String value) {
        this.repeatMenu = value;
    }

}
