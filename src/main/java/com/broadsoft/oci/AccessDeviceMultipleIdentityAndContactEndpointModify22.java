//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Access device end point used in the context of modify that can have more than one contact defined.
 *         The endpoint is identified by its linePort (public Identity) and possibly a private Identity.
 *         Only Static Registration capable devices may have more than one contact defined.
 *         Port numbers are only used by devices with static line ordering.
 *         The following elements are only used in XS data mode and ignored in AS data mode:
 *           privateIdentity
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           useHotline
 *           hotlineContact
 *       
 * 
 * <p>Java-Klasse für AccessDeviceMultipleIdentityAndContactEndpointModify22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="AccessDeviceMultipleIdentityAndContactEndpointModify22">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="accessDevice" type="{}AccessDevice" minOccurs="0"/>
 *         <element name="linePort" type="{}AccessDeviceEndpointLinePort" minOccurs="0"/>
 *         <element name="privateIdentity" type="{}AccessDeviceEndpointPrivateIdentity" minOccurs="0"/>
 *         <element name="contactList" type="{}ReplacementContactList22" minOccurs="0"/>
 *         <element name="portNumber" type="{}AccessDevicePortNumber" minOccurs="0"/>
 *         <element name="useHotline" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="hotlineContact" type="{}SIPURI" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccessDeviceMultipleIdentityAndContactEndpointModify22", propOrder = {
    "accessDevice",
    "linePort",
    "privateIdentity",
    "contactList",
    "portNumber",
    "useHotline",
    "hotlineContact"
})
public class AccessDeviceMultipleIdentityAndContactEndpointModify22 {

    protected AccessDevice accessDevice;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String linePort;
    @XmlElementRef(name = "privateIdentity", type = JAXBElement.class, required = false)
    protected JAXBElement<String> privateIdentity;
    @XmlElementRef(name = "contactList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementContactList22> contactList;
    protected Integer portNumber;
    protected Boolean useHotline;
    @XmlElementRef(name = "hotlineContact", type = JAXBElement.class, required = false)
    protected JAXBElement<String> hotlineContact;

    /**
     * Ruft den Wert der accessDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDevice }
     *     
     */
    public AccessDevice getAccessDevice() {
        return accessDevice;
    }

    /**
     * Legt den Wert der accessDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDevice }
     *     
     */
    public void setAccessDevice(AccessDevice value) {
        this.accessDevice = value;
    }

    /**
     * Ruft den Wert der linePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinePort() {
        return linePort;
    }

    /**
     * Legt den Wert der linePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinePort(String value) {
        this.linePort = value;
    }

    /**
     * Ruft den Wert der privateIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrivateIdentity() {
        return privateIdentity;
    }

    /**
     * Legt den Wert der privateIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrivateIdentity(JAXBElement<String> value) {
        this.privateIdentity = value;
    }

    /**
     * Ruft den Wert der contactList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementContactList22 }{@code >}
     *     
     */
    public JAXBElement<ReplacementContactList22> getContactList() {
        return contactList;
    }

    /**
     * Legt den Wert der contactList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementContactList22 }{@code >}
     *     
     */
    public void setContactList(JAXBElement<ReplacementContactList22> value) {
        this.contactList = value;
    }

    /**
     * Ruft den Wert der portNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPortNumber() {
        return portNumber;
    }

    /**
     * Legt den Wert der portNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPortNumber(Integer value) {
        this.portNumber = value;
    }

    /**
     * Ruft den Wert der useHotline-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseHotline() {
        return useHotline;
    }

    /**
     * Legt den Wert der useHotline-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseHotline(Boolean value) {
        this.useHotline = value;
    }

    /**
     * Ruft den Wert der hotlineContact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHotlineContact() {
        return hotlineContact;
    }

    /**
     * Legt den Wert der hotlineContact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHotlineContact(JAXBElement<String> value) {
        this.hotlineContact = value;
    }

}
