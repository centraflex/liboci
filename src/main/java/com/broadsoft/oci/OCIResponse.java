//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The OCIResponse is an abstract type from which all responses are derived.
 *       
 * 
 * <p>Java-Klasse für OCIResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OCIResponse">
 *   <complexContent>
 *     <extension base="{C}OCICommand">
 *       <sequence>
 *       </sequence>
 *       <attribute name="debugInfo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OCIResponse", namespace = "C")
@XmlSeeAlso({
    OCIDataResponse.class,
    SuccessResponse.class,
    ErrorResponse.class
})
public abstract class OCIResponse
    extends OCICommand
{

    @XmlAttribute(name = "debugInfo")
    protected String debugInfo;

    /**
     * Ruft den Wert der debugInfo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebugInfo() {
        return debugInfo;
    }

    /**
     * Legt den Wert der debugInfo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebugInfo(String value) {
        this.debugInfo = value;
    }

}
