//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserAdviceOfChargeGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserAdviceOfChargeGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserAdviceOfChargeGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="aocType" type="{}AdviceOfChargeType"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserAdviceOfChargeGetResponse", propOrder = {
    "isActive",
    "aocType"
})
public class UserAdviceOfChargeGetResponse
    extends OCIDataResponse
{

    protected boolean isActive;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AdviceOfChargeType aocType;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der aocType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AdviceOfChargeType }
     *     
     */
    public AdviceOfChargeType getAocType() {
        return aocType;
    }

    /**
     * Legt den Wert der aocType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdviceOfChargeType }
     *     
     */
    public void setAocType(AdviceOfChargeType value) {
        this.aocType = value;
    }

}
