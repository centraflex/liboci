//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the group level data associated with Emergency Zones.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupEmergencyZonesModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupEmergencyZonesModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="emergencyZonesProhibition" type="{}EmergencyZonesProhibition" minOccurs="0"/>
 *         <element name="sendEmergencyCallNotifyEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="emergencyCallNotifyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupEmergencyZonesModifyRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "isActive",
    "emergencyZonesProhibition",
    "sendEmergencyCallNotifyEmail",
    "emergencyCallNotifyEmailAddress"
})
public class GroupEmergencyZonesModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected Boolean isActive;
    @XmlSchemaType(name = "token")
    protected EmergencyZonesProhibition emergencyZonesProhibition;
    protected Boolean sendEmergencyCallNotifyEmail;
    @XmlElementRef(name = "emergencyCallNotifyEmailAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emergencyCallNotifyEmailAddress;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der emergencyZonesProhibition-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmergencyZonesProhibition }
     *     
     */
    public EmergencyZonesProhibition getEmergencyZonesProhibition() {
        return emergencyZonesProhibition;
    }

    /**
     * Legt den Wert der emergencyZonesProhibition-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmergencyZonesProhibition }
     *     
     */
    public void setEmergencyZonesProhibition(EmergencyZonesProhibition value) {
        this.emergencyZonesProhibition = value;
    }

    /**
     * Ruft den Wert der sendEmergencyCallNotifyEmail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendEmergencyCallNotifyEmail() {
        return sendEmergencyCallNotifyEmail;
    }

    /**
     * Legt den Wert der sendEmergencyCallNotifyEmail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendEmergencyCallNotifyEmail(Boolean value) {
        this.sendEmergencyCallNotifyEmail = value;
    }

    /**
     * Ruft den Wert der emergencyCallNotifyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmergencyCallNotifyEmailAddress() {
        return emergencyCallNotifyEmailAddress;
    }

    /**
     * Legt den Wert der emergencyCallNotifyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmergencyCallNotifyEmailAddress(JAXBElement<String> value) {
        this.emergencyCallNotifyEmailAddress = value;
    }

}
