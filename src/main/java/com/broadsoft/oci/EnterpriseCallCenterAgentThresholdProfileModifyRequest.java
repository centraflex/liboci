//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modifies an existing Call Center Agent Threshold Profile in the Enterprise.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für EnterpriseCallCenterAgentThresholdProfileModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseCallCenterAgentThresholdProfileModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="profileName" type="{}CallCenterAgentThresholdProfileName"/>
 *         <element name="newProfileName" type="{}CallCenterAgentThresholdProfileName" minOccurs="0"/>
 *         <element name="profileDescription" type="{}CallCenterAgentThresholdProfileDescription" minOccurs="0"/>
 *         <element name="thresholdCurrentCallStateIdleTimeYellow" type="{}CallCenterAgentThresholdCurrentCallStateIdleTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdCurrentCallStateIdleTimeRed" type="{}CallCenterAgentThresholdCurrentCallStateIdleTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdCurrentCallStateOnCallTimeYellow" type="{}CallCenterAgentThresholdCurrentCallStateOnCallTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdCurrentCallStateOnCallTimeRed" type="{}CallCenterAgentThresholdCurrentCallStateOnCallTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdCurrentAgentStateUnavailableTimeYellow" type="{}CallCenterAgentThresholdCurrentAgentStateUnavailableTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdCurrentAgentStateUnavailableTimeRed" type="{}CallCenterAgentThresholdCurrentAgentStateUnavailableTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageBusyInTimeYellow" type="{}CallCenterAgentThresholdAverageBusyInTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageBusyInTimeRed" type="{}CallCenterAgentThresholdAverageBusyInTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageBusyOutTimeYellow" type="{}CallCenterAgentThresholdAverageBusyOutTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageBusyOutTimeRed" type="{}CallCenterAgentThresholdAverageBusyOutTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageWrapUpTimeYellow" type="{}CallCenterAgentThresholdAverageWrapUpTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageWrapUpTimeRed" type="{}CallCenterAgentThresholdAverageWrapUpTimeSeconds" minOccurs="0"/>
 *         <element name="enableNotificationEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="notificationEmailAddressList" type="{}CallCenterAgentThresholdProfileReplacementNotificationEmailList" minOccurs="0"/>
 *         <element name="agentUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseCallCenterAgentThresholdProfileModifyRequest", propOrder = {
    "serviceProviderId",
    "profileName",
    "newProfileName",
    "profileDescription",
    "thresholdCurrentCallStateIdleTimeYellow",
    "thresholdCurrentCallStateIdleTimeRed",
    "thresholdCurrentCallStateOnCallTimeYellow",
    "thresholdCurrentCallStateOnCallTimeRed",
    "thresholdCurrentAgentStateUnavailableTimeYellow",
    "thresholdCurrentAgentStateUnavailableTimeRed",
    "thresholdAverageBusyInTimeYellow",
    "thresholdAverageBusyInTimeRed",
    "thresholdAverageBusyOutTimeYellow",
    "thresholdAverageBusyOutTimeRed",
    "thresholdAverageWrapUpTimeYellow",
    "thresholdAverageWrapUpTimeRed",
    "enableNotificationEmail",
    "notificationEmailAddressList",
    "agentUserIdList"
})
public class EnterpriseCallCenterAgentThresholdProfileModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String profileName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newProfileName;
    @XmlElementRef(name = "profileDescription", type = JAXBElement.class, required = false)
    protected JAXBElement<String> profileDescription;
    @XmlElementRef(name = "thresholdCurrentCallStateIdleTimeYellow", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdCurrentCallStateIdleTimeYellow;
    @XmlElementRef(name = "thresholdCurrentCallStateIdleTimeRed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdCurrentCallStateIdleTimeRed;
    @XmlElementRef(name = "thresholdCurrentCallStateOnCallTimeYellow", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdCurrentCallStateOnCallTimeYellow;
    @XmlElementRef(name = "thresholdCurrentCallStateOnCallTimeRed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdCurrentCallStateOnCallTimeRed;
    @XmlElementRef(name = "thresholdCurrentAgentStateUnavailableTimeYellow", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdCurrentAgentStateUnavailableTimeYellow;
    @XmlElementRef(name = "thresholdCurrentAgentStateUnavailableTimeRed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdCurrentAgentStateUnavailableTimeRed;
    @XmlElementRef(name = "thresholdAverageBusyInTimeYellow", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdAverageBusyInTimeYellow;
    @XmlElementRef(name = "thresholdAverageBusyInTimeRed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdAverageBusyInTimeRed;
    @XmlElementRef(name = "thresholdAverageBusyOutTimeYellow", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdAverageBusyOutTimeYellow;
    @XmlElementRef(name = "thresholdAverageBusyOutTimeRed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdAverageBusyOutTimeRed;
    @XmlElementRef(name = "thresholdAverageWrapUpTimeYellow", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdAverageWrapUpTimeYellow;
    @XmlElementRef(name = "thresholdAverageWrapUpTimeRed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdAverageWrapUpTimeRed;
    protected Boolean enableNotificationEmail;
    @XmlElementRef(name = "notificationEmailAddressList", type = JAXBElement.class, required = false)
    protected JAXBElement<CallCenterAgentThresholdProfileReplacementNotificationEmailList> notificationEmailAddressList;
    @XmlElementRef(name = "agentUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> agentUserIdList;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der profileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileName() {
        return profileName;
    }

    /**
     * Legt den Wert der profileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileName(String value) {
        this.profileName = value;
    }

    /**
     * Ruft den Wert der newProfileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewProfileName() {
        return newProfileName;
    }

    /**
     * Legt den Wert der newProfileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewProfileName(String value) {
        this.newProfileName = value;
    }

    /**
     * Ruft den Wert der profileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProfileDescription() {
        return profileDescription;
    }

    /**
     * Legt den Wert der profileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProfileDescription(JAXBElement<String> value) {
        this.profileDescription = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentCallStateIdleTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdCurrentCallStateIdleTimeYellow() {
        return thresholdCurrentCallStateIdleTimeYellow;
    }

    /**
     * Legt den Wert der thresholdCurrentCallStateIdleTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdCurrentCallStateIdleTimeYellow(JAXBElement<Integer> value) {
        this.thresholdCurrentCallStateIdleTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentCallStateIdleTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdCurrentCallStateIdleTimeRed() {
        return thresholdCurrentCallStateIdleTimeRed;
    }

    /**
     * Legt den Wert der thresholdCurrentCallStateIdleTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdCurrentCallStateIdleTimeRed(JAXBElement<Integer> value) {
        this.thresholdCurrentCallStateIdleTimeRed = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentCallStateOnCallTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdCurrentCallStateOnCallTimeYellow() {
        return thresholdCurrentCallStateOnCallTimeYellow;
    }

    /**
     * Legt den Wert der thresholdCurrentCallStateOnCallTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdCurrentCallStateOnCallTimeYellow(JAXBElement<Integer> value) {
        this.thresholdCurrentCallStateOnCallTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentCallStateOnCallTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdCurrentCallStateOnCallTimeRed() {
        return thresholdCurrentCallStateOnCallTimeRed;
    }

    /**
     * Legt den Wert der thresholdCurrentCallStateOnCallTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdCurrentCallStateOnCallTimeRed(JAXBElement<Integer> value) {
        this.thresholdCurrentCallStateOnCallTimeRed = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentAgentStateUnavailableTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdCurrentAgentStateUnavailableTimeYellow() {
        return thresholdCurrentAgentStateUnavailableTimeYellow;
    }

    /**
     * Legt den Wert der thresholdCurrentAgentStateUnavailableTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdCurrentAgentStateUnavailableTimeYellow(JAXBElement<Integer> value) {
        this.thresholdCurrentAgentStateUnavailableTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentAgentStateUnavailableTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdCurrentAgentStateUnavailableTimeRed() {
        return thresholdCurrentAgentStateUnavailableTimeRed;
    }

    /**
     * Legt den Wert der thresholdCurrentAgentStateUnavailableTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdCurrentAgentStateUnavailableTimeRed(JAXBElement<Integer> value) {
        this.thresholdCurrentAgentStateUnavailableTimeRed = value;
    }

    /**
     * Ruft den Wert der thresholdAverageBusyInTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdAverageBusyInTimeYellow() {
        return thresholdAverageBusyInTimeYellow;
    }

    /**
     * Legt den Wert der thresholdAverageBusyInTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdAverageBusyInTimeYellow(JAXBElement<Integer> value) {
        this.thresholdAverageBusyInTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdAverageBusyInTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdAverageBusyInTimeRed() {
        return thresholdAverageBusyInTimeRed;
    }

    /**
     * Legt den Wert der thresholdAverageBusyInTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdAverageBusyInTimeRed(JAXBElement<Integer> value) {
        this.thresholdAverageBusyInTimeRed = value;
    }

    /**
     * Ruft den Wert der thresholdAverageBusyOutTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdAverageBusyOutTimeYellow() {
        return thresholdAverageBusyOutTimeYellow;
    }

    /**
     * Legt den Wert der thresholdAverageBusyOutTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdAverageBusyOutTimeYellow(JAXBElement<Integer> value) {
        this.thresholdAverageBusyOutTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdAverageBusyOutTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdAverageBusyOutTimeRed() {
        return thresholdAverageBusyOutTimeRed;
    }

    /**
     * Legt den Wert der thresholdAverageBusyOutTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdAverageBusyOutTimeRed(JAXBElement<Integer> value) {
        this.thresholdAverageBusyOutTimeRed = value;
    }

    /**
     * Ruft den Wert der thresholdAverageWrapUpTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdAverageWrapUpTimeYellow() {
        return thresholdAverageWrapUpTimeYellow;
    }

    /**
     * Legt den Wert der thresholdAverageWrapUpTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdAverageWrapUpTimeYellow(JAXBElement<Integer> value) {
        this.thresholdAverageWrapUpTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdAverageWrapUpTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdAverageWrapUpTimeRed() {
        return thresholdAverageWrapUpTimeRed;
    }

    /**
     * Legt den Wert der thresholdAverageWrapUpTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdAverageWrapUpTimeRed(JAXBElement<Integer> value) {
        this.thresholdAverageWrapUpTimeRed = value;
    }

    /**
     * Ruft den Wert der enableNotificationEmail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableNotificationEmail() {
        return enableNotificationEmail;
    }

    /**
     * Legt den Wert der enableNotificationEmail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableNotificationEmail(Boolean value) {
        this.enableNotificationEmail = value;
    }

    /**
     * Ruft den Wert der notificationEmailAddressList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CallCenterAgentThresholdProfileReplacementNotificationEmailList }{@code >}
     *     
     */
    public JAXBElement<CallCenterAgentThresholdProfileReplacementNotificationEmailList> getNotificationEmailAddressList() {
        return notificationEmailAddressList;
    }

    /**
     * Legt den Wert der notificationEmailAddressList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CallCenterAgentThresholdProfileReplacementNotificationEmailList }{@code >}
     *     
     */
    public void setNotificationEmailAddressList(JAXBElement<CallCenterAgentThresholdProfileReplacementNotificationEmailList> value) {
        this.notificationEmailAddressList = value;
    }

    /**
     * Ruft den Wert der agentUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getAgentUserIdList() {
        return agentUserIdList;
    }

    /**
     * Legt den Wert der agentUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setAgentUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.agentUserIdList = value;
    }

}
