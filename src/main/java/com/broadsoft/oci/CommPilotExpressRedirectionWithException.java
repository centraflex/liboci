//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         CommPilot Express type to transfer to voice mail or forward to a number
 *         with certain exceptions used in the context of a get.
 *       
 * 
 * <p>Java-Klasse für CommPilotExpressRedirectionWithException complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CommPilotExpressRedirectionWithException">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="sendCallsToVoiceMailExceptExcludedNumbers" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="excludedPhoneNumber01" type="{}DN" minOccurs="0"/>
 *         <element name="excludedPhoneNumber02" type="{}DN" minOccurs="0"/>
 *         <element name="excludedPhoneNumber03" type="{}DN" minOccurs="0"/>
 *         <element name="forwardExcludedNumbersTo" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommPilotExpressRedirectionWithException", propOrder = {
    "sendCallsToVoiceMailExceptExcludedNumbers",
    "excludedPhoneNumber01",
    "excludedPhoneNumber02",
    "excludedPhoneNumber03",
    "forwardExcludedNumbersTo"
})
public class CommPilotExpressRedirectionWithException {

    protected boolean sendCallsToVoiceMailExceptExcludedNumbers;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String excludedPhoneNumber01;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String excludedPhoneNumber02;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String excludedPhoneNumber03;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String forwardExcludedNumbersTo;

    /**
     * Ruft den Wert der sendCallsToVoiceMailExceptExcludedNumbers-Eigenschaft ab.
     * 
     */
    public boolean isSendCallsToVoiceMailExceptExcludedNumbers() {
        return sendCallsToVoiceMailExceptExcludedNumbers;
    }

    /**
     * Legt den Wert der sendCallsToVoiceMailExceptExcludedNumbers-Eigenschaft fest.
     * 
     */
    public void setSendCallsToVoiceMailExceptExcludedNumbers(boolean value) {
        this.sendCallsToVoiceMailExceptExcludedNumbers = value;
    }

    /**
     * Ruft den Wert der excludedPhoneNumber01-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExcludedPhoneNumber01() {
        return excludedPhoneNumber01;
    }

    /**
     * Legt den Wert der excludedPhoneNumber01-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExcludedPhoneNumber01(String value) {
        this.excludedPhoneNumber01 = value;
    }

    /**
     * Ruft den Wert der excludedPhoneNumber02-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExcludedPhoneNumber02() {
        return excludedPhoneNumber02;
    }

    /**
     * Legt den Wert der excludedPhoneNumber02-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExcludedPhoneNumber02(String value) {
        this.excludedPhoneNumber02 = value;
    }

    /**
     * Ruft den Wert der excludedPhoneNumber03-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExcludedPhoneNumber03() {
        return excludedPhoneNumber03;
    }

    /**
     * Legt den Wert der excludedPhoneNumber03-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExcludedPhoneNumber03(String value) {
        this.excludedPhoneNumber03 = value;
    }

    /**
     * Ruft den Wert der forwardExcludedNumbersTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForwardExcludedNumbersTo() {
        return forwardExcludedNumbersTo;
    }

    /**
     * Legt den Wert der forwardExcludedNumbersTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForwardExcludedNumbersTo(String value) {
        this.forwardExcludedNumbersTo = value;
    }

}
