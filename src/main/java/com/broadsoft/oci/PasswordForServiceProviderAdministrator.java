//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Password to be generated for a service provider administrator. If the administratorId is not included, 
 *         or included but is not an exiting administrator for the service provider, a password will be generated
 *         based on only the rules applicable for a new user.
 *       
 * 
 * <p>Java-Klasse für PasswordForServiceProviderAdministrator complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="PasswordForServiceProviderAdministrator">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="administratorId" type="{}UserId" minOccurs="0"/>
 *         <element name="generatePassword" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PasswordForServiceProviderAdministrator", propOrder = {
    "serviceProviderId",
    "administratorId",
    "generatePassword"
})
public class PasswordForServiceProviderAdministrator {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String administratorId;
    protected boolean generatePassword;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der administratorId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdministratorId() {
        return administratorId;
    }

    /**
     * Legt den Wert der administratorId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdministratorId(String value) {
        this.administratorId = value;
    }

    /**
     * Ruft den Wert der generatePassword-Eigenschaft ab.
     * 
     */
    public boolean isGeneratePassword() {
        return generatePassword;
    }

    /**
     * Legt den Wert der generatePassword-Eigenschaft fest.
     * 
     */
    public void setGeneratePassword(boolean value) {
        this.generatePassword = value;
    }

}
