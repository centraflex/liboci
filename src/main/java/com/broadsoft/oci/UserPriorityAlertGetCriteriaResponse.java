//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 *         
 *          Response to the UserPriorityAlertGetCriteriaRequest.      
 *       
 * 
 * <p>Java-Klasse für UserPriorityAlertGetCriteriaResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPriorityAlertGetCriteriaResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="timeSchedule" type="{}TimeSchedule" minOccurs="0"/>
 *         <element name="fromDnCriteria" type="{}PriorityAlertCriteriaFromDn"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPriorityAlertGetCriteriaResponse", propOrder = {
    "timeSchedule",
    "fromDnCriteria"
})
public class UserPriorityAlertGetCriteriaResponse
    extends OCIDataResponse
{

    protected TimeSchedule timeSchedule;
    @XmlElement(required = true)
    protected PriorityAlertCriteriaFromDn fromDnCriteria;

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeSchedule }
     *     
     */
    public TimeSchedule getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeSchedule }
     *     
     */
    public void setTimeSchedule(TimeSchedule value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der fromDnCriteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PriorityAlertCriteriaFromDn }
     *     
     */
    public PriorityAlertCriteriaFromDn getFromDnCriteria() {
        return fromDnCriteria;
    }

    /**
     * Legt den Wert der fromDnCriteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PriorityAlertCriteriaFromDn }
     *     
     */
    public void setFromDnCriteria(PriorityAlertCriteriaFromDn value) {
        this.fromDnCriteria = value;
    }

}
