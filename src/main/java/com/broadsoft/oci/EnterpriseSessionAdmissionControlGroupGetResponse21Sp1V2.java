//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to EnterpriseSessionAdmissionControlGroupGetRequest21sp1V2.
 *         Returns the profile information for the session admission control group.
 *       
 * 
 * <p>Java-Klasse für EnterpriseSessionAdmissionControlGroupGetResponse21sp1V2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseSessionAdmissionControlGroupGetResponse21sp1V2">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maxSession" type="{}NonNegativeInt"/>
 *         <element name="maxUserOriginatingSessions" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="maxUserTerminatingSessions" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="reservedSession" type="{}NonNegativeInt"/>
 *         <element name="reservedUserOriginatingSessions" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="reservedUserTerminatingSessions" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="defaultGroup" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="countIntraSACGroupSessions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="devices" type="{}EnterpriseAccessDevice" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="blockEmergencyAndRepairCallsDueToSACLimits" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="mediaGroupName" type="{}SessionAdmissionControlMediaGroupName" minOccurs="0"/>
 *         <element name="accessInfoPattern" type="{}SessionAdmissionControlAccessInfoPattern" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseSessionAdmissionControlGroupGetResponse21sp1V2", propOrder = {
    "maxSession",
    "maxUserOriginatingSessions",
    "maxUserTerminatingSessions",
    "reservedSession",
    "reservedUserOriginatingSessions",
    "reservedUserTerminatingSessions",
    "defaultGroup",
    "countIntraSACGroupSessions",
    "devices",
    "blockEmergencyAndRepairCallsDueToSACLimits",
    "mediaGroupName",
    "accessInfoPattern"
})
public class EnterpriseSessionAdmissionControlGroupGetResponse21Sp1V2
    extends OCIDataResponse
{

    protected int maxSession;
    protected Integer maxUserOriginatingSessions;
    protected Integer maxUserTerminatingSessions;
    protected int reservedSession;
    protected Integer reservedUserOriginatingSessions;
    protected Integer reservedUserTerminatingSessions;
    protected boolean defaultGroup;
    protected boolean countIntraSACGroupSessions;
    protected List<EnterpriseAccessDevice> devices;
    protected boolean blockEmergencyAndRepairCallsDueToSACLimits;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mediaGroupName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accessInfoPattern;

    /**
     * Ruft den Wert der maxSession-Eigenschaft ab.
     * 
     */
    public int getMaxSession() {
        return maxSession;
    }

    /**
     * Legt den Wert der maxSession-Eigenschaft fest.
     * 
     */
    public void setMaxSession(int value) {
        this.maxSession = value;
    }

    /**
     * Ruft den Wert der maxUserOriginatingSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxUserOriginatingSessions() {
        return maxUserOriginatingSessions;
    }

    /**
     * Legt den Wert der maxUserOriginatingSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxUserOriginatingSessions(Integer value) {
        this.maxUserOriginatingSessions = value;
    }

    /**
     * Ruft den Wert der maxUserTerminatingSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxUserTerminatingSessions() {
        return maxUserTerminatingSessions;
    }

    /**
     * Legt den Wert der maxUserTerminatingSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxUserTerminatingSessions(Integer value) {
        this.maxUserTerminatingSessions = value;
    }

    /**
     * Ruft den Wert der reservedSession-Eigenschaft ab.
     * 
     */
    public int getReservedSession() {
        return reservedSession;
    }

    /**
     * Legt den Wert der reservedSession-Eigenschaft fest.
     * 
     */
    public void setReservedSession(int value) {
        this.reservedSession = value;
    }

    /**
     * Ruft den Wert der reservedUserOriginatingSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReservedUserOriginatingSessions() {
        return reservedUserOriginatingSessions;
    }

    /**
     * Legt den Wert der reservedUserOriginatingSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReservedUserOriginatingSessions(Integer value) {
        this.reservedUserOriginatingSessions = value;
    }

    /**
     * Ruft den Wert der reservedUserTerminatingSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReservedUserTerminatingSessions() {
        return reservedUserTerminatingSessions;
    }

    /**
     * Legt den Wert der reservedUserTerminatingSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReservedUserTerminatingSessions(Integer value) {
        this.reservedUserTerminatingSessions = value;
    }

    /**
     * Ruft den Wert der defaultGroup-Eigenschaft ab.
     * 
     */
    public boolean isDefaultGroup() {
        return defaultGroup;
    }

    /**
     * Legt den Wert der defaultGroup-Eigenschaft fest.
     * 
     */
    public void setDefaultGroup(boolean value) {
        this.defaultGroup = value;
    }

    /**
     * Ruft den Wert der countIntraSACGroupSessions-Eigenschaft ab.
     * 
     */
    public boolean isCountIntraSACGroupSessions() {
        return countIntraSACGroupSessions;
    }

    /**
     * Legt den Wert der countIntraSACGroupSessions-Eigenschaft fest.
     * 
     */
    public void setCountIntraSACGroupSessions(boolean value) {
        this.countIntraSACGroupSessions = value;
    }

    /**
     * Gets the value of the devices property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the devices property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDevices().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnterpriseAccessDevice }
     * 
     * 
     * @return
     *     The value of the devices property.
     */
    public List<EnterpriseAccessDevice> getDevices() {
        if (devices == null) {
            devices = new ArrayList<>();
        }
        return this.devices;
    }

    /**
     * Ruft den Wert der blockEmergencyAndRepairCallsDueToSACLimits-Eigenschaft ab.
     * 
     */
    public boolean isBlockEmergencyAndRepairCallsDueToSACLimits() {
        return blockEmergencyAndRepairCallsDueToSACLimits;
    }

    /**
     * Legt den Wert der blockEmergencyAndRepairCallsDueToSACLimits-Eigenschaft fest.
     * 
     */
    public void setBlockEmergencyAndRepairCallsDueToSACLimits(boolean value) {
        this.blockEmergencyAndRepairCallsDueToSACLimits = value;
    }

    /**
     * Ruft den Wert der mediaGroupName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMediaGroupName() {
        return mediaGroupName;
    }

    /**
     * Legt den Wert der mediaGroupName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMediaGroupName(String value) {
        this.mediaGroupName = value;
    }

    /**
     * Ruft den Wert der accessInfoPattern-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessInfoPattern() {
        return accessInfoPattern;
    }

    /**
     * Legt den Wert der accessInfoPattern-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessInfoPattern(String value) {
        this.accessInfoPattern = value;
    }

}
