//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modifies the Group's Call Park settings.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCallParkModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallParkModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="recallTimerSeconds" type="{}CallParkRecallTimerSeconds" minOccurs="0"/>
 *         <element name="displayTimerSeconds" type="{}CallParkDisplayTimerSeconds" minOccurs="0"/>
 *         <element name="enableDestinationAnnouncement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="recallAlternateUserId" type="{}UserId" minOccurs="0"/>
 *         <element name="recallRingPattern" type="{}RingPattern" minOccurs="0"/>
 *         <element name="recallTo" type="{}CallParkRecallTo" minOccurs="0"/>
 *         <element name="alternateUserRecallTimerSeconds" type="{}CallParkRecallTimerSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallParkModifyRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "recallTimerSeconds",
    "displayTimerSeconds",
    "enableDestinationAnnouncement",
    "recallAlternateUserId",
    "recallRingPattern",
    "recallTo",
    "alternateUserRecallTimerSeconds"
})
public class GroupCallParkModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected Integer recallTimerSeconds;
    protected Integer displayTimerSeconds;
    protected Boolean enableDestinationAnnouncement;
    @XmlElementRef(name = "recallAlternateUserId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recallAlternateUserId;
    @XmlSchemaType(name = "token")
    protected RingPattern recallRingPattern;
    @XmlSchemaType(name = "token")
    protected CallParkRecallTo recallTo;
    protected Integer alternateUserRecallTimerSeconds;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der recallTimerSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRecallTimerSeconds() {
        return recallTimerSeconds;
    }

    /**
     * Legt den Wert der recallTimerSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRecallTimerSeconds(Integer value) {
        this.recallTimerSeconds = value;
    }

    /**
     * Ruft den Wert der displayTimerSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDisplayTimerSeconds() {
        return displayTimerSeconds;
    }

    /**
     * Legt den Wert der displayTimerSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDisplayTimerSeconds(Integer value) {
        this.displayTimerSeconds = value;
    }

    /**
     * Ruft den Wert der enableDestinationAnnouncement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableDestinationAnnouncement() {
        return enableDestinationAnnouncement;
    }

    /**
     * Legt den Wert der enableDestinationAnnouncement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableDestinationAnnouncement(Boolean value) {
        this.enableDestinationAnnouncement = value;
    }

    /**
     * Ruft den Wert der recallAlternateUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecallAlternateUserId() {
        return recallAlternateUserId;
    }

    /**
     * Legt den Wert der recallAlternateUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecallAlternateUserId(JAXBElement<String> value) {
        this.recallAlternateUserId = value;
    }

    /**
     * Ruft den Wert der recallRingPattern-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RingPattern }
     *     
     */
    public RingPattern getRecallRingPattern() {
        return recallRingPattern;
    }

    /**
     * Legt den Wert der recallRingPattern-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RingPattern }
     *     
     */
    public void setRecallRingPattern(RingPattern value) {
        this.recallRingPattern = value;
    }

    /**
     * Ruft den Wert der recallTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallParkRecallTo }
     *     
     */
    public CallParkRecallTo getRecallTo() {
        return recallTo;
    }

    /**
     * Legt den Wert der recallTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallParkRecallTo }
     *     
     */
    public void setRecallTo(CallParkRecallTo value) {
        this.recallTo = value;
    }

    /**
     * Ruft den Wert der alternateUserRecallTimerSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAlternateUserRecallTimerSeconds() {
        return alternateUserRecallTimerSeconds;
    }

    /**
     * Legt den Wert der alternateUserRecallTimerSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAlternateUserRecallTimerSeconds(Integer value) {
        this.alternateUserRecallTimerSeconds = value;
    }

}
