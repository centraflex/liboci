//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserEnhancedCallLogsGetListRequest.
 *         Replaced By: UserEnhancedCallLogsGetListResponse14sp4
 *       
 * 
 * <p>Java-Klasse für UserEnhancedCallLogsGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserEnhancedCallLogsGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="placed" type="{}EnhancedCallLogsEntry" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="received" type="{}EnhancedCallLogsEntry" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="missed" type="{}EnhancedCallLogsEntry" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserEnhancedCallLogsGetListResponse", propOrder = {
    "placed",
    "received",
    "missed"
})
public class UserEnhancedCallLogsGetListResponse
    extends OCIDataResponse
{

    protected List<EnhancedCallLogsEntry> placed;
    protected List<EnhancedCallLogsEntry> received;
    protected List<EnhancedCallLogsEntry> missed;

    /**
     * Gets the value of the placed property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the placed property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlaced().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnhancedCallLogsEntry }
     * 
     * 
     * @return
     *     The value of the placed property.
     */
    public List<EnhancedCallLogsEntry> getPlaced() {
        if (placed == null) {
            placed = new ArrayList<>();
        }
        return this.placed;
    }

    /**
     * Gets the value of the received property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the received property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceived().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnhancedCallLogsEntry }
     * 
     * 
     * @return
     *     The value of the received property.
     */
    public List<EnhancedCallLogsEntry> getReceived() {
        if (received == null) {
            received = new ArrayList<>();
        }
        return this.received;
    }

    /**
     * Gets the value of the missed property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the missed property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMissed().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnhancedCallLogsEntry }
     * 
     * 
     * @return
     *     The value of the missed property.
     */
    public List<EnhancedCallLogsEntry> getMissed() {
        if (missed == null) {
            missed = new ArrayList<>();
        }
        return this.missed;
    }

}
