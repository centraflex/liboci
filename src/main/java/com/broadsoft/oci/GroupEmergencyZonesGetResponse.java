//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupEmergencyZonesGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupEmergencyZonesGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupEmergencyZonesGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="emergencyZonesProhibition" type="{}EmergencyZonesProhibition"/>
 *         <element name="sendEmergencyCallNotifyEmail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="emergencyCallNotifyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupEmergencyZonesGetResponse", propOrder = {
    "isActive",
    "emergencyZonesProhibition",
    "sendEmergencyCallNotifyEmail",
    "emergencyCallNotifyEmailAddress"
})
public class GroupEmergencyZonesGetResponse
    extends OCIDataResponse
{

    protected boolean isActive;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EmergencyZonesProhibition emergencyZonesProhibition;
    protected boolean sendEmergencyCallNotifyEmail;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String emergencyCallNotifyEmailAddress;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der emergencyZonesProhibition-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EmergencyZonesProhibition }
     *     
     */
    public EmergencyZonesProhibition getEmergencyZonesProhibition() {
        return emergencyZonesProhibition;
    }

    /**
     * Legt den Wert der emergencyZonesProhibition-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EmergencyZonesProhibition }
     *     
     */
    public void setEmergencyZonesProhibition(EmergencyZonesProhibition value) {
        this.emergencyZonesProhibition = value;
    }

    /**
     * Ruft den Wert der sendEmergencyCallNotifyEmail-Eigenschaft ab.
     * 
     */
    public boolean isSendEmergencyCallNotifyEmail() {
        return sendEmergencyCallNotifyEmail;
    }

    /**
     * Legt den Wert der sendEmergencyCallNotifyEmail-Eigenschaft fest.
     * 
     */
    public void setSendEmergencyCallNotifyEmail(boolean value) {
        this.sendEmergencyCallNotifyEmail = value;
    }

    /**
     * Ruft den Wert der emergencyCallNotifyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmergencyCallNotifyEmailAddress() {
        return emergencyCallNotifyEmailAddress;
    }

    /**
     * Legt den Wert der emergencyCallNotifyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmergencyCallNotifyEmailAddress(String value) {
        this.emergencyCallNotifyEmailAddress = value;
    }

}
