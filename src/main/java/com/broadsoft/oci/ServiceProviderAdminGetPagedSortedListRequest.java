//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Get a list of service provider administrators.
 *         The response is either a ServiceProviderAdminGetPagedSortedListResponse or an ErrorResponse.
 *         If no sortOrder is included, the response is sorted by Administrator ID ascending by default.
 *         If the responsePagingControl element is not provided, the paging startIndex will be 
 *         set to 1 by default, and the responsePageSize will be set to the maximum 
 *         responsePageSize by default.
 *         Multiple search criteria are logically ANDed together unless the searchCriteriaModeOr option is 
 *         included. Then the search criteria are logically ORed together.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderAdminGetPagedSortedListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderAdminGetPagedSortedListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl" minOccurs="0"/>
 *         <element name="sortOrder" type="{}SortOrderServiceProviderAdminGetPagedSortedList" maxOccurs="3" minOccurs="0"/>
 *         <element name="searchCriteriaAdminId" type="{}SearchCriteriaAdminId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaAdminLastName" type="{}SearchCriteriaAdminLastName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaAdminFirstName" type="{}SearchCriteriaAdminFirstName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactServiceProviderAdminType" type="{}SearchCriteriaExactServiceProviderAdminType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaLanguage" type="{}SearchCriteriaLanguage" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaModeOr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderAdminGetPagedSortedListRequest", propOrder = {
    "serviceProviderId",
    "responsePagingControl",
    "sortOrder",
    "searchCriteriaAdminId",
    "searchCriteriaAdminLastName",
    "searchCriteriaAdminFirstName",
    "searchCriteriaExactServiceProviderAdminType",
    "searchCriteriaLanguage",
    "searchCriteriaModeOr"
})
public class ServiceProviderAdminGetPagedSortedListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected ResponsePagingControl responsePagingControl;
    protected List<SortOrderServiceProviderAdminGetPagedSortedList> sortOrder;
    protected List<SearchCriteriaAdminId> searchCriteriaAdminId;
    protected List<SearchCriteriaAdminLastName> searchCriteriaAdminLastName;
    protected List<SearchCriteriaAdminFirstName> searchCriteriaAdminFirstName;
    protected List<SearchCriteriaExactServiceProviderAdminType> searchCriteriaExactServiceProviderAdminType;
    protected List<SearchCriteriaLanguage> searchCriteriaLanguage;
    protected Boolean searchCriteriaModeOr;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Gets the value of the sortOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the sortOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSortOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SortOrderServiceProviderAdminGetPagedSortedList }
     * 
     * 
     * @return
     *     The value of the sortOrder property.
     */
    public List<SortOrderServiceProviderAdminGetPagedSortedList> getSortOrder() {
        if (sortOrder == null) {
            sortOrder = new ArrayList<>();
        }
        return this.sortOrder;
    }

    /**
     * Gets the value of the searchCriteriaAdminId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaAdminId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaAdminId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaAdminId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaAdminId property.
     */
    public List<SearchCriteriaAdminId> getSearchCriteriaAdminId() {
        if (searchCriteriaAdminId == null) {
            searchCriteriaAdminId = new ArrayList<>();
        }
        return this.searchCriteriaAdminId;
    }

    /**
     * Gets the value of the searchCriteriaAdminLastName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaAdminLastName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaAdminLastName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaAdminLastName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaAdminLastName property.
     */
    public List<SearchCriteriaAdminLastName> getSearchCriteriaAdminLastName() {
        if (searchCriteriaAdminLastName == null) {
            searchCriteriaAdminLastName = new ArrayList<>();
        }
        return this.searchCriteriaAdminLastName;
    }

    /**
     * Gets the value of the searchCriteriaAdminFirstName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaAdminFirstName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaAdminFirstName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaAdminFirstName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaAdminFirstName property.
     */
    public List<SearchCriteriaAdminFirstName> getSearchCriteriaAdminFirstName() {
        if (searchCriteriaAdminFirstName == null) {
            searchCriteriaAdminFirstName = new ArrayList<>();
        }
        return this.searchCriteriaAdminFirstName;
    }

    /**
     * Gets the value of the searchCriteriaExactServiceProviderAdminType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaExactServiceProviderAdminType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaExactServiceProviderAdminType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaExactServiceProviderAdminType }
     * 
     * 
     * @return
     *     The value of the searchCriteriaExactServiceProviderAdminType property.
     */
    public List<SearchCriteriaExactServiceProviderAdminType> getSearchCriteriaExactServiceProviderAdminType() {
        if (searchCriteriaExactServiceProviderAdminType == null) {
            searchCriteriaExactServiceProviderAdminType = new ArrayList<>();
        }
        return this.searchCriteriaExactServiceProviderAdminType;
    }

    /**
     * Gets the value of the searchCriteriaLanguage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaLanguage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaLanguage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaLanguage }
     * 
     * 
     * @return
     *     The value of the searchCriteriaLanguage property.
     */
    public List<SearchCriteriaLanguage> getSearchCriteriaLanguage() {
        if (searchCriteriaLanguage == null) {
            searchCriteriaLanguage = new ArrayList<>();
        }
        return this.searchCriteriaLanguage;
    }

    /**
     * Ruft den Wert der searchCriteriaModeOr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchCriteriaModeOr() {
        return searchCriteriaModeOr;
    }

    /**
     * Legt den Wert der searchCriteriaModeOr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchCriteriaModeOr(Boolean value) {
        this.searchCriteriaModeOr = value;
    }

}
