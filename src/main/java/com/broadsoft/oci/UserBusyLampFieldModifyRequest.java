//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the settings for the busy lamp field service.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserBusyLampFieldModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBusyLampFieldModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="listURI" type="{}SIPURI" minOccurs="0"/>
 *         <element name="monitoredUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *         <element name="enableCallParkNotification" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBusyLampFieldModifyRequest", propOrder = {
    "userId",
    "listURI",
    "monitoredUserIdList",
    "enableCallParkNotification"
})
public class UserBusyLampFieldModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElementRef(name = "listURI", type = JAXBElement.class, required = false)
    protected JAXBElement<String> listURI;
    @XmlElementRef(name = "monitoredUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> monitoredUserIdList;
    protected Boolean enableCallParkNotification;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der listURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getListURI() {
        return listURI;
    }

    /**
     * Legt den Wert der listURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setListURI(JAXBElement<String> value) {
        this.listURI = value;
    }

    /**
     * Ruft den Wert der monitoredUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getMonitoredUserIdList() {
        return monitoredUserIdList;
    }

    /**
     * Legt den Wert der monitoredUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setMonitoredUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.monitoredUserIdList = value;
    }

    /**
     * Ruft den Wert der enableCallParkNotification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCallParkNotification() {
        return enableCallParkNotification;
    }

    /**
     * Legt den Wert der enableCallParkNotification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCallParkNotification(Boolean value) {
        this.enableCallParkNotification = value;
    }

}
