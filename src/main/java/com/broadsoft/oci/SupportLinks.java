//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SupportLinks.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SupportLinks">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Not Supported"/>
 *     <enumeration value="Support Links from Devices"/>
 *     <enumeration value="Support Link to Device"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SupportLinks")
@XmlEnum
public enum SupportLinks {

    @XmlEnumValue("Not Supported")
    NOT_SUPPORTED("Not Supported"),
    @XmlEnumValue("Support Links from Devices")
    SUPPORT_LINKS_FROM_DEVICES("Support Links from Devices"),
    @XmlEnumValue("Support Link to Device")
    SUPPORT_LINK_TO_DEVICE("Support Link to Device");
    private final String value;

    SupportLinks(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SupportLinks fromValue(String v) {
        for (SupportLinks c: SupportLinks.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
