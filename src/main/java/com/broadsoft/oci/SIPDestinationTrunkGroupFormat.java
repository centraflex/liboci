//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SIPDestinationTrunkGroupFormat.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SIPDestinationTrunkGroupFormat">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="DTG"/>
 *     <enumeration value="Tgrp In Contact"/>
 *     <enumeration value="Tgrp In Request URI"/>
 *     <enumeration value="X Nortel Profile"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SIPDestinationTrunkGroupFormat")
@XmlEnum
public enum SIPDestinationTrunkGroupFormat {

    DTG("DTG"),
    @XmlEnumValue("Tgrp In Contact")
    TGRP_IN_CONTACT("Tgrp In Contact"),
    @XmlEnumValue("Tgrp In Request URI")
    TGRP_IN_REQUEST_URI("Tgrp In Request URI"),
    @XmlEnumValue("X Nortel Profile")
    X_NORTEL_PROFILE("X Nortel Profile");
    private final String value;

    SIPDestinationTrunkGroupFormat(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SIPDestinationTrunkGroupFormat fromValue(String v) {
        for (SIPDestinationTrunkGroupFormat c: SIPDestinationTrunkGroupFormat.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
