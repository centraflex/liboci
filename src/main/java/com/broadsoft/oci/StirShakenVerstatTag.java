//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für StirShakenVerstatTag.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="StirShakenVerstatTag">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="TN-Validation-Passed"/>
 *     <enumeration value="TN-Validation-Failed"/>
 *     <enumeration value="No-TN-Validation"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "StirShakenVerstatTag")
@XmlEnum
public enum StirShakenVerstatTag {

    @XmlEnumValue("TN-Validation-Passed")
    TN_VALIDATION_PASSED("TN-Validation-Passed"),
    @XmlEnumValue("TN-Validation-Failed")
    TN_VALIDATION_FAILED("TN-Validation-Failed"),
    @XmlEnumValue("No-TN-Validation")
    NO_TN_VALIDATION("No-TN-Validation");
    private final String value;

    StirShakenVerstatTag(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StirShakenVerstatTag fromValue(String v) {
        for (StirShakenVerstatTag c: StirShakenVerstatTag.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
