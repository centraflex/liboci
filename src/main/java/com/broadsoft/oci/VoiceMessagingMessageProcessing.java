//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für VoiceMessagingMessageProcessing.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="VoiceMessagingMessageProcessing">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Unified Voice and Email Messaging"/>
 *     <enumeration value="Deliver To Email Address Only"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "VoiceMessagingMessageProcessing")
@XmlEnum
public enum VoiceMessagingMessageProcessing {

    @XmlEnumValue("Unified Voice and Email Messaging")
    UNIFIED_VOICE_AND_EMAIL_MESSAGING("Unified Voice and Email Messaging"),
    @XmlEnumValue("Deliver To Email Address Only")
    DELIVER_TO_EMAIL_ADDRESS_ONLY("Deliver To Email Address Only");
    private final String value;

    VoiceMessagingMessageProcessing(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VoiceMessagingMessageProcessing fromValue(String v) {
        for (VoiceMessagingMessageProcessing c: VoiceMessagingMessageProcessing.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
