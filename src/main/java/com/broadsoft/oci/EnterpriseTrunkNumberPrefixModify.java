//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Enterprise Trunk Number Prefix for modify.
 *       
 * 
 * <p>Java-Klasse für EnterpriseTrunkNumberPrefixModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseTrunkNumberPrefixModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="numberPrefix" type="{}EnterpriseTrunkNumberPrefix"/>
 *         <element name="extensionRange" type="{}ExtensionRange17" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseTrunkNumberPrefixModify", propOrder = {
    "numberPrefix",
    "extensionRange"
})
public class EnterpriseTrunkNumberPrefixModify {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String numberPrefix;
    @XmlElementRef(name = "extensionRange", type = JAXBElement.class, required = false)
    protected JAXBElement<ExtensionRange17> extensionRange;

    /**
     * Ruft den Wert der numberPrefix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberPrefix() {
        return numberPrefix;
    }

    /**
     * Legt den Wert der numberPrefix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberPrefix(String value) {
        this.numberPrefix = value;
    }

    /**
     * Ruft den Wert der extensionRange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ExtensionRange17 }{@code >}
     *     
     */
    public JAXBElement<ExtensionRange17> getExtensionRange() {
        return extensionRange;
    }

    /**
     * Legt den Wert der extensionRange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ExtensionRange17 }{@code >}
     *     
     */
    public void setExtensionRange(JAXBElement<ExtensionRange17> value) {
        this.extensionRange = value;
    }

}
