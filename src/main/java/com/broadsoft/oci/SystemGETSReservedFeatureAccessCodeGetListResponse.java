//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemGETSReservedFeatureAccessCodeGetListRequest. 
 *         The table columns are: "Code" and "Description".
 *       
 * 
 * <p>Java-Klasse für SystemGETSReservedFeatureAccessCodeGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGETSReservedFeatureAccessCodeGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="reservedCodeTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGETSReservedFeatureAccessCodeGetListResponse", propOrder = {
    "reservedCodeTable"
})
public class SystemGETSReservedFeatureAccessCodeGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable reservedCodeTable;

    /**
     * Ruft den Wert der reservedCodeTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getReservedCodeTable() {
        return reservedCodeTable;
    }

    /**
     * Legt den Wert der reservedCodeTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setReservedCodeTable(OCITable value) {
        this.reservedCodeTable = value;
    }

}
