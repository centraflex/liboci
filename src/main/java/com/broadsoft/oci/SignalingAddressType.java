//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SignalingAddressType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SignalingAddressType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Non-intelligent Device Addressing"/>
 *     <enumeration value="Non-intelligent Proxy Addressing"/>
 *     <enumeration value="Intelligent Device Addressing"/>
 *     <enumeration value="Intelligent Proxy Addressing"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SignalingAddressType")
@XmlEnum
public enum SignalingAddressType {

    @XmlEnumValue("Non-intelligent Device Addressing")
    NON_INTELLIGENT_DEVICE_ADDRESSING("Non-intelligent Device Addressing"),
    @XmlEnumValue("Non-intelligent Proxy Addressing")
    NON_INTELLIGENT_PROXY_ADDRESSING("Non-intelligent Proxy Addressing"),
    @XmlEnumValue("Intelligent Device Addressing")
    INTELLIGENT_DEVICE_ADDRESSING("Intelligent Device Addressing"),
    @XmlEnumValue("Intelligent Proxy Addressing")
    INTELLIGENT_PROXY_ADDRESSING("Intelligent Proxy Addressing");
    private final String value;

    SignalingAddressType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SignalingAddressType fromValue(String v) {
        for (SignalingAddressType c: SignalingAddressType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
