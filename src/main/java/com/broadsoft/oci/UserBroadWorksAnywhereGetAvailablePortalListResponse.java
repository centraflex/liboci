//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserBroadWorksAnywhereGetAvailablePortalListRequest.
 *         Contains a table with column headings: "Portal ID", "Phone Number", "Extension", "Language".
 *         Replaced by UserBroadWorksAnywhereGetAvailablePortalListResponse21sp1
 *      
 * 
 * <p>Java-Klasse für UserBroadWorksAnywhereGetAvailablePortalListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksAnywhereGetAvailablePortalListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="portalTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksAnywhereGetAvailablePortalListResponse", propOrder = {
    "portalTable"
})
public class UserBroadWorksAnywhereGetAvailablePortalListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable portalTable;

    /**
     * Ruft den Wert der portalTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getPortalTable() {
        return portalTable;
    }

    /**
     * Legt den Wert der portalTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setPortalTable(OCITable value) {
        this.portalTable = value;
    }

}
