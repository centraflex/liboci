//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to import a Device Type Archive File (DTAF) as a new Device Type.  The URL supports the HTTP and the FILE protocols.
 *         When the optional element resellerId is specified, the device type created is at reseller level.
 *         When the optional element deviceTypeRename is set, on import, the device type name part of the DTAF will be changed to the desired name on the destination system.
 *         When the optional element deviceTypeFileUpdate is set, on import, the files that are matched based on the file format are updated with the ones from the DTAF. The file format and the file category are the only attributes that can't be updated. 
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         The following data elements are only used in AS data mode and ignored in XS data mode:
 *           deviceTypeFileUpdate
 *           deviceTypeRename
 *           resellerId        
 *       
 * 
 * <p>Java-Klasse für SystemDeviceTypeImportRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceTypeImportRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="file" type="{}URL"/>
 *         <element name="resellerId" type="{}ResellerId22" minOccurs="0"/>
 *         <element name="deviceTypeRename" type="{}AccessDeviceTypeRename" minOccurs="0"/>
 *         <element name="deviceTypeFileUpdate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceTypeImportRequest", propOrder = {
    "file",
    "resellerId",
    "deviceTypeRename",
    "deviceTypeFileUpdate"
})
public class SystemDeviceTypeImportRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String file;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    protected AccessDeviceTypeRename deviceTypeRename;
    protected Boolean deviceTypeFileUpdate;

    /**
     * Ruft den Wert der file-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFile() {
        return file;
    }

    /**
     * Legt den Wert der file-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFile(String value) {
        this.file = value;
    }

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Ruft den Wert der deviceTypeRename-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceTypeRename }
     *     
     */
    public AccessDeviceTypeRename getDeviceTypeRename() {
        return deviceTypeRename;
    }

    /**
     * Legt den Wert der deviceTypeRename-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceTypeRename }
     *     
     */
    public void setDeviceTypeRename(AccessDeviceTypeRename value) {
        this.deviceTypeRename = value;
    }

    /**
     * Ruft den Wert der deviceTypeFileUpdate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeviceTypeFileUpdate() {
        return deviceTypeFileUpdate;
    }

    /**
     * Legt den Wert der deviceTypeFileUpdate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeviceTypeFileUpdate(Boolean value) {
        this.deviceTypeFileUpdate = value;
    }

}
