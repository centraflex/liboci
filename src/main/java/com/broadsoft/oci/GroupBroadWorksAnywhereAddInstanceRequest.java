//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a BroadWorks Anywhere instance to a group.
 *         The domain is required in the serviceUserId.
 *         The following elements are only used in AS data mode:
 *           networkClassOfService        
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupBroadWorksAnywhereAddInstanceRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupBroadWorksAnywhereAddInstanceRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceAddProfile"/>
 *         <element name="broadWorksAnywhereScope" type="{}BroadWorksAnywhereScope"/>
 *         <element name="promptForCLID" type="{}BroadWorksAnywhereCLIDPrompt"/>
 *         <element name="silentPromptMode" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="promptForPasscode" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupBroadWorksAnywhereAddInstanceRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "serviceUserId",
    "serviceInstanceProfile",
    "broadWorksAnywhereScope",
    "promptForCLID",
    "silentPromptMode",
    "promptForPasscode",
    "networkClassOfService"
})
public class GroupBroadWorksAnywhereAddInstanceRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlElement(required = true)
    protected ServiceInstanceAddProfile serviceInstanceProfile;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BroadWorksAnywhereScope broadWorksAnywhereScope;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BroadWorksAnywhereCLIDPrompt promptForCLID;
    protected boolean silentPromptMode;
    protected boolean promptForPasscode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceAddProfile }
     *     
     */
    public ServiceInstanceAddProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceAddProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceAddProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der broadWorksAnywhereScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksAnywhereScope }
     *     
     */
    public BroadWorksAnywhereScope getBroadWorksAnywhereScope() {
        return broadWorksAnywhereScope;
    }

    /**
     * Legt den Wert der broadWorksAnywhereScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksAnywhereScope }
     *     
     */
    public void setBroadWorksAnywhereScope(BroadWorksAnywhereScope value) {
        this.broadWorksAnywhereScope = value;
    }

    /**
     * Ruft den Wert der promptForCLID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksAnywhereCLIDPrompt }
     *     
     */
    public BroadWorksAnywhereCLIDPrompt getPromptForCLID() {
        return promptForCLID;
    }

    /**
     * Legt den Wert der promptForCLID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksAnywhereCLIDPrompt }
     *     
     */
    public void setPromptForCLID(BroadWorksAnywhereCLIDPrompt value) {
        this.promptForCLID = value;
    }

    /**
     * Ruft den Wert der silentPromptMode-Eigenschaft ab.
     * 
     */
    public boolean isSilentPromptMode() {
        return silentPromptMode;
    }

    /**
     * Legt den Wert der silentPromptMode-Eigenschaft fest.
     * 
     */
    public void setSilentPromptMode(boolean value) {
        this.silentPromptMode = value;
    }

    /**
     * Ruft den Wert der promptForPasscode-Eigenschaft ab.
     * 
     */
    public boolean isPromptForPasscode() {
        return promptForPasscode;
    }

    /**
     * Legt den Wert der promptForPasscode-Eigenschaft fest.
     * 
     */
    public void setPromptForPasscode(boolean value) {
        this.promptForPasscode = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

}
