//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Time Interval.
 *       
 * 
 * <p>Java-Klasse für TimeInterval complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="TimeInterval">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="startDayOfWeek" type="{}DayOfWeek"/>
 *         <element name="startHour" type="{}Hour"/>
 *         <element name="startMinute" type="{}Minute"/>
 *         <element name="endDayOfWeek" type="{}DayOfWeek"/>
 *         <element name="endHour" type="{}Hour"/>
 *         <element name="endMinute" type="{}Minute"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeInterval", propOrder = {
    "startDayOfWeek",
    "startHour",
    "startMinute",
    "endDayOfWeek",
    "endHour",
    "endMinute"
})
public class TimeInterval {

    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected DayOfWeek startDayOfWeek;
    protected int startHour;
    protected int startMinute;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected DayOfWeek endDayOfWeek;
    protected int endHour;
    protected int endMinute;

    /**
     * Ruft den Wert der startDayOfWeek-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DayOfWeek }
     *     
     */
    public DayOfWeek getStartDayOfWeek() {
        return startDayOfWeek;
    }

    /**
     * Legt den Wert der startDayOfWeek-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DayOfWeek }
     *     
     */
    public void setStartDayOfWeek(DayOfWeek value) {
        this.startDayOfWeek = value;
    }

    /**
     * Ruft den Wert der startHour-Eigenschaft ab.
     * 
     */
    public int getStartHour() {
        return startHour;
    }

    /**
     * Legt den Wert der startHour-Eigenschaft fest.
     * 
     */
    public void setStartHour(int value) {
        this.startHour = value;
    }

    /**
     * Ruft den Wert der startMinute-Eigenschaft ab.
     * 
     */
    public int getStartMinute() {
        return startMinute;
    }

    /**
     * Legt den Wert der startMinute-Eigenschaft fest.
     * 
     */
    public void setStartMinute(int value) {
        this.startMinute = value;
    }

    /**
     * Ruft den Wert der endDayOfWeek-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DayOfWeek }
     *     
     */
    public DayOfWeek getEndDayOfWeek() {
        return endDayOfWeek;
    }

    /**
     * Legt den Wert der endDayOfWeek-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DayOfWeek }
     *     
     */
    public void setEndDayOfWeek(DayOfWeek value) {
        this.endDayOfWeek = value;
    }

    /**
     * Ruft den Wert der endHour-Eigenschaft ab.
     * 
     */
    public int getEndHour() {
        return endHour;
    }

    /**
     * Legt den Wert der endHour-Eigenschaft fest.
     * 
     */
    public void setEndHour(int value) {
        this.endHour = value;
    }

    /**
     * Ruft den Wert der endMinute-Eigenschaft ab.
     * 
     */
    public int getEndMinute() {
        return endMinute;
    }

    /**
     * Legt den Wert der endMinute-Eigenschaft fest.
     * 
     */
    public void setEndMinute(int value) {
        this.endMinute = value;
    }

}
