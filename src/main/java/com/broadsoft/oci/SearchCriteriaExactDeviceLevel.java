//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for a particular device level.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactDeviceLevel complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactDeviceLevel">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="deviceLevel" type="{}AccessDeviceLevel"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactDeviceLevel", propOrder = {
    "deviceLevel"
})
public class SearchCriteriaExactDeviceLevel
    extends SearchCriteria
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AccessDeviceLevel deviceLevel;

    /**
     * Ruft den Wert der deviceLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceLevel }
     *     
     */
    public AccessDeviceLevel getDeviceLevel() {
        return deviceLevel;
    }

    /**
     * Legt den Wert der deviceLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceLevel }
     *     
     */
    public void setDeviceLevel(AccessDeviceLevel value) {
        this.deviceLevel = value;
    }

}
