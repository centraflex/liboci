//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Response to SystemAutomaticCallbackGetRequest15.
 *             Replaced By: SystemAutomaticCallbackGetResponse15sp2
 *          
 * 
 * <p>Java-Klasse für SystemAutomaticCallbackGetResponse15 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAutomaticCallbackGetResponse15">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="monitorMinutes" type="{}AutomaticCallbackMonitorMinutes"/>
 *         <element name="maxMonitorsPerOriginator" type="{}AutomaticCallbackMaxMonitorsPerOriginator"/>
 *         <element name="maxCallbackRings" type="{}AutomaticCallbackMaxCallbackRings"/>
 *         <element name="maxMonitorsPerTerminator" type="{}AutomaticCallbackMaxMonitorsPerTerminator"/>
 *         <element name="terminatorIdleGuardSeconds" type="{}AutomaticCallbackTerminatorIdleGuardSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAutomaticCallbackGetResponse15", propOrder = {
    "monitorMinutes",
    "maxMonitorsPerOriginator",
    "maxCallbackRings",
    "maxMonitorsPerTerminator",
    "terminatorIdleGuardSeconds"
})
public class SystemAutomaticCallbackGetResponse15
    extends OCIDataResponse
{

    protected int monitorMinutes;
    protected int maxMonitorsPerOriginator;
    protected int maxCallbackRings;
    protected int maxMonitorsPerTerminator;
    protected int terminatorIdleGuardSeconds;

    /**
     * Ruft den Wert der monitorMinutes-Eigenschaft ab.
     * 
     */
    public int getMonitorMinutes() {
        return monitorMinutes;
    }

    /**
     * Legt den Wert der monitorMinutes-Eigenschaft fest.
     * 
     */
    public void setMonitorMinutes(int value) {
        this.monitorMinutes = value;
    }

    /**
     * Ruft den Wert der maxMonitorsPerOriginator-Eigenschaft ab.
     * 
     */
    public int getMaxMonitorsPerOriginator() {
        return maxMonitorsPerOriginator;
    }

    /**
     * Legt den Wert der maxMonitorsPerOriginator-Eigenschaft fest.
     * 
     */
    public void setMaxMonitorsPerOriginator(int value) {
        this.maxMonitorsPerOriginator = value;
    }

    /**
     * Ruft den Wert der maxCallbackRings-Eigenschaft ab.
     * 
     */
    public int getMaxCallbackRings() {
        return maxCallbackRings;
    }

    /**
     * Legt den Wert der maxCallbackRings-Eigenschaft fest.
     * 
     */
    public void setMaxCallbackRings(int value) {
        this.maxCallbackRings = value;
    }

    /**
     * Ruft den Wert der maxMonitorsPerTerminator-Eigenschaft ab.
     * 
     */
    public int getMaxMonitorsPerTerminator() {
        return maxMonitorsPerTerminator;
    }

    /**
     * Legt den Wert der maxMonitorsPerTerminator-Eigenschaft fest.
     * 
     */
    public void setMaxMonitorsPerTerminator(int value) {
        this.maxMonitorsPerTerminator = value;
    }

    /**
     * Ruft den Wert der terminatorIdleGuardSeconds-Eigenschaft ab.
     * 
     */
    public int getTerminatorIdleGuardSeconds() {
        return terminatorIdleGuardSeconds;
    }

    /**
     * Legt den Wert der terminatorIdleGuardSeconds-Eigenschaft fest.
     * 
     */
    public void setTerminatorIdleGuardSeconds(int value) {
        this.terminatorIdleGuardSeconds = value;
    }

}
