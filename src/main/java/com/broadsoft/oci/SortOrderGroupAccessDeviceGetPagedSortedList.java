//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Used to sort the GroupAccessDeviceGetPagedSortedListRequest request.
 *       
 * 
 * <p>Java-Klasse für SortOrderGroupAccessDeviceGetPagedSortedList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SortOrderGroupAccessDeviceGetPagedSortedList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <choice>
 *         <element name="sortByDeviceName" type="{}SortByDeviceName"/>
 *         <element name="sortByDeviceType" type="{}SortByDeviceType"/>
 *         <element name="sortByDeviceNetAddress" type="{}SortByDeviceNetAddress"/>
 *         <element name="sortByDeviceMACAddress" type="{}SortByDeviceMACAddress"/>
 *       </choice>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SortOrderGroupAccessDeviceGetPagedSortedList", propOrder = {
    "sortByDeviceName",
    "sortByDeviceType",
    "sortByDeviceNetAddress",
    "sortByDeviceMACAddress"
})
public class SortOrderGroupAccessDeviceGetPagedSortedList {

    protected SortByDeviceName sortByDeviceName;
    protected SortByDeviceType sortByDeviceType;
    protected SortByDeviceNetAddress sortByDeviceNetAddress;
    protected SortByDeviceMACAddress sortByDeviceMACAddress;

    /**
     * Ruft den Wert der sortByDeviceName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByDeviceName }
     *     
     */
    public SortByDeviceName getSortByDeviceName() {
        return sortByDeviceName;
    }

    /**
     * Legt den Wert der sortByDeviceName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByDeviceName }
     *     
     */
    public void setSortByDeviceName(SortByDeviceName value) {
        this.sortByDeviceName = value;
    }

    /**
     * Ruft den Wert der sortByDeviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByDeviceType }
     *     
     */
    public SortByDeviceType getSortByDeviceType() {
        return sortByDeviceType;
    }

    /**
     * Legt den Wert der sortByDeviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByDeviceType }
     *     
     */
    public void setSortByDeviceType(SortByDeviceType value) {
        this.sortByDeviceType = value;
    }

    /**
     * Ruft den Wert der sortByDeviceNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByDeviceNetAddress }
     *     
     */
    public SortByDeviceNetAddress getSortByDeviceNetAddress() {
        return sortByDeviceNetAddress;
    }

    /**
     * Legt den Wert der sortByDeviceNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByDeviceNetAddress }
     *     
     */
    public void setSortByDeviceNetAddress(SortByDeviceNetAddress value) {
        this.sortByDeviceNetAddress = value;
    }

    /**
     * Ruft den Wert der sortByDeviceMACAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByDeviceMACAddress }
     *     
     */
    public SortByDeviceMACAddress getSortByDeviceMACAddress() {
        return sortByDeviceMACAddress;
    }

    /**
     * Legt den Wert der sortByDeviceMACAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByDeviceMACAddress }
     *     
     */
    public void setSortByDeviceMACAddress(SortByDeviceMACAddress value) {
        this.sortByDeviceMACAddress = value;
    }

}
