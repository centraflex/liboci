//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to ResellerMeetMeConferencingGetRequest.
 *       
 * 
 * <p>Java-Klasse für ResellerMeetMeConferencingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerMeetMeConferencingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="conferenceFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="maxAllocatedPorts" type="{}ResellerMeetMeConferencingConferencePorts" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerMeetMeConferencingGetResponse", propOrder = {
    "conferenceFromAddress",
    "maxAllocatedPorts"
})
public class ResellerMeetMeConferencingGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String conferenceFromAddress;
    protected Integer maxAllocatedPorts;

    /**
     * Ruft den Wert der conferenceFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConferenceFromAddress() {
        return conferenceFromAddress;
    }

    /**
     * Legt den Wert der conferenceFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConferenceFromAddress(String value) {
        this.conferenceFromAddress = value;
    }

    /**
     * Ruft den Wert der maxAllocatedPorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxAllocatedPorts() {
        return maxAllocatedPorts;
    }

    /**
     * Legt den Wert der maxAllocatedPorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxAllocatedPorts(Integer value) {
        this.maxAllocatedPorts = value;
    }

}
