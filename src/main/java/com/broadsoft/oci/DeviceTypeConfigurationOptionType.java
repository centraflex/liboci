//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeviceTypeConfigurationOptionType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DeviceTypeConfigurationOptionType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Not Supported"/>
 *     <enumeration value="Device Management"/>
 *     <enumeration value="Legacy"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DeviceTypeConfigurationOptionType")
@XmlEnum
public enum DeviceTypeConfigurationOptionType {

    @XmlEnumValue("Not Supported")
    NOT_SUPPORTED("Not Supported"),
    @XmlEnumValue("Device Management")
    DEVICE_MANAGEMENT("Device Management"),
    @XmlEnumValue("Legacy")
    LEGACY("Legacy");
    private final String value;

    DeviceTypeConfigurationOptionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeviceTypeConfigurationOptionType fromValue(String v) {
        for (DeviceTypeConfigurationOptionType c: DeviceTypeConfigurationOptionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
