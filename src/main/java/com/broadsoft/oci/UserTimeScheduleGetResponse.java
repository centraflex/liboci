//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the UserTimeScheduleGetRequest.
 *         The response contains the requested time schedule information.
 *       
 * 
 * <p>Java-Klasse für UserTimeScheduleGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserTimeScheduleGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="timeScheduleName" type="{}ScheduleName"/>
 *         <element name="timeInterval01" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval02" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval03" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval04" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval05" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval06" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval07" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval08" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval09" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval10" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval11" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval12" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval13" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval14" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval15" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval16" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval17" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval18" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval19" type="{}TimeInterval" minOccurs="0"/>
 *         <element name="timeInterval20" type="{}TimeInterval" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserTimeScheduleGetResponse", propOrder = {
    "timeScheduleName",
    "timeInterval01",
    "timeInterval02",
    "timeInterval03",
    "timeInterval04",
    "timeInterval05",
    "timeInterval06",
    "timeInterval07",
    "timeInterval08",
    "timeInterval09",
    "timeInterval10",
    "timeInterval11",
    "timeInterval12",
    "timeInterval13",
    "timeInterval14",
    "timeInterval15",
    "timeInterval16",
    "timeInterval17",
    "timeInterval18",
    "timeInterval19",
    "timeInterval20"
})
public class UserTimeScheduleGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeScheduleName;
    protected TimeInterval timeInterval01;
    protected TimeInterval timeInterval02;
    protected TimeInterval timeInterval03;
    protected TimeInterval timeInterval04;
    protected TimeInterval timeInterval05;
    protected TimeInterval timeInterval06;
    protected TimeInterval timeInterval07;
    protected TimeInterval timeInterval08;
    protected TimeInterval timeInterval09;
    protected TimeInterval timeInterval10;
    protected TimeInterval timeInterval11;
    protected TimeInterval timeInterval12;
    protected TimeInterval timeInterval13;
    protected TimeInterval timeInterval14;
    protected TimeInterval timeInterval15;
    protected TimeInterval timeInterval16;
    protected TimeInterval timeInterval17;
    protected TimeInterval timeInterval18;
    protected TimeInterval timeInterval19;
    protected TimeInterval timeInterval20;

    /**
     * Ruft den Wert der timeScheduleName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeScheduleName() {
        return timeScheduleName;
    }

    /**
     * Legt den Wert der timeScheduleName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeScheduleName(String value) {
        this.timeScheduleName = value;
    }

    /**
     * Ruft den Wert der timeInterval01-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval01() {
        return timeInterval01;
    }

    /**
     * Legt den Wert der timeInterval01-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval01(TimeInterval value) {
        this.timeInterval01 = value;
    }

    /**
     * Ruft den Wert der timeInterval02-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval02() {
        return timeInterval02;
    }

    /**
     * Legt den Wert der timeInterval02-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval02(TimeInterval value) {
        this.timeInterval02 = value;
    }

    /**
     * Ruft den Wert der timeInterval03-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval03() {
        return timeInterval03;
    }

    /**
     * Legt den Wert der timeInterval03-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval03(TimeInterval value) {
        this.timeInterval03 = value;
    }

    /**
     * Ruft den Wert der timeInterval04-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval04() {
        return timeInterval04;
    }

    /**
     * Legt den Wert der timeInterval04-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval04(TimeInterval value) {
        this.timeInterval04 = value;
    }

    /**
     * Ruft den Wert der timeInterval05-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval05() {
        return timeInterval05;
    }

    /**
     * Legt den Wert der timeInterval05-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval05(TimeInterval value) {
        this.timeInterval05 = value;
    }

    /**
     * Ruft den Wert der timeInterval06-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval06() {
        return timeInterval06;
    }

    /**
     * Legt den Wert der timeInterval06-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval06(TimeInterval value) {
        this.timeInterval06 = value;
    }

    /**
     * Ruft den Wert der timeInterval07-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval07() {
        return timeInterval07;
    }

    /**
     * Legt den Wert der timeInterval07-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval07(TimeInterval value) {
        this.timeInterval07 = value;
    }

    /**
     * Ruft den Wert der timeInterval08-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval08() {
        return timeInterval08;
    }

    /**
     * Legt den Wert der timeInterval08-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval08(TimeInterval value) {
        this.timeInterval08 = value;
    }

    /**
     * Ruft den Wert der timeInterval09-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval09() {
        return timeInterval09;
    }

    /**
     * Legt den Wert der timeInterval09-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval09(TimeInterval value) {
        this.timeInterval09 = value;
    }

    /**
     * Ruft den Wert der timeInterval10-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval10() {
        return timeInterval10;
    }

    /**
     * Legt den Wert der timeInterval10-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval10(TimeInterval value) {
        this.timeInterval10 = value;
    }

    /**
     * Ruft den Wert der timeInterval11-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval11() {
        return timeInterval11;
    }

    /**
     * Legt den Wert der timeInterval11-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval11(TimeInterval value) {
        this.timeInterval11 = value;
    }

    /**
     * Ruft den Wert der timeInterval12-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval12() {
        return timeInterval12;
    }

    /**
     * Legt den Wert der timeInterval12-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval12(TimeInterval value) {
        this.timeInterval12 = value;
    }

    /**
     * Ruft den Wert der timeInterval13-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval13() {
        return timeInterval13;
    }

    /**
     * Legt den Wert der timeInterval13-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval13(TimeInterval value) {
        this.timeInterval13 = value;
    }

    /**
     * Ruft den Wert der timeInterval14-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval14() {
        return timeInterval14;
    }

    /**
     * Legt den Wert der timeInterval14-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval14(TimeInterval value) {
        this.timeInterval14 = value;
    }

    /**
     * Ruft den Wert der timeInterval15-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval15() {
        return timeInterval15;
    }

    /**
     * Legt den Wert der timeInterval15-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval15(TimeInterval value) {
        this.timeInterval15 = value;
    }

    /**
     * Ruft den Wert der timeInterval16-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval16() {
        return timeInterval16;
    }

    /**
     * Legt den Wert der timeInterval16-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval16(TimeInterval value) {
        this.timeInterval16 = value;
    }

    /**
     * Ruft den Wert der timeInterval17-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval17() {
        return timeInterval17;
    }

    /**
     * Legt den Wert der timeInterval17-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval17(TimeInterval value) {
        this.timeInterval17 = value;
    }

    /**
     * Ruft den Wert der timeInterval18-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval18() {
        return timeInterval18;
    }

    /**
     * Legt den Wert der timeInterval18-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval18(TimeInterval value) {
        this.timeInterval18 = value;
    }

    /**
     * Ruft den Wert der timeInterval19-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval19() {
        return timeInterval19;
    }

    /**
     * Legt den Wert der timeInterval19-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval19(TimeInterval value) {
        this.timeInterval19 = value;
    }

    /**
     * Ruft den Wert der timeInterval20-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeInterval }
     *     
     */
    public TimeInterval getTimeInterval20() {
        return timeInterval20;
    }

    /**
     * Legt den Wert der timeInterval20-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeInterval }
     *     
     */
    public void setTimeInterval20(TimeInterval value) {
        this.timeInterval20 = value;
    }

}
