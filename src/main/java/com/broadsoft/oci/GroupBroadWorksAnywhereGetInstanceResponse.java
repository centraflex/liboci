//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupBroadWorksAnywhereGetInstanceRequest.
 *         Contains the service profile information.
 *       
 * 
 * <p>Java-Klasse für GroupBroadWorksAnywhereGetInstanceResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupBroadWorksAnywhereGetInstanceResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceReadProfile"/>
 *         <element name="broadWorksAnywhereScope" type="{}BroadWorksAnywhereScope"/>
 *         <element name="promptForCLID" type="{}BroadWorksAnywhereCLIDPrompt"/>
 *         <element name="silentPromptMode" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="promptForPasscode" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupBroadWorksAnywhereGetInstanceResponse", propOrder = {
    "serviceInstanceProfile",
    "broadWorksAnywhereScope",
    "promptForCLID",
    "silentPromptMode",
    "promptForPasscode"
})
public class GroupBroadWorksAnywhereGetInstanceResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected ServiceInstanceReadProfile serviceInstanceProfile;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BroadWorksAnywhereScope broadWorksAnywhereScope;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BroadWorksAnywhereCLIDPrompt promptForCLID;
    protected boolean silentPromptMode;
    protected boolean promptForPasscode;

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceReadProfile }
     *     
     */
    public ServiceInstanceReadProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceReadProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceReadProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der broadWorksAnywhereScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksAnywhereScope }
     *     
     */
    public BroadWorksAnywhereScope getBroadWorksAnywhereScope() {
        return broadWorksAnywhereScope;
    }

    /**
     * Legt den Wert der broadWorksAnywhereScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksAnywhereScope }
     *     
     */
    public void setBroadWorksAnywhereScope(BroadWorksAnywhereScope value) {
        this.broadWorksAnywhereScope = value;
    }

    /**
     * Ruft den Wert der promptForCLID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksAnywhereCLIDPrompt }
     *     
     */
    public BroadWorksAnywhereCLIDPrompt getPromptForCLID() {
        return promptForCLID;
    }

    /**
     * Legt den Wert der promptForCLID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksAnywhereCLIDPrompt }
     *     
     */
    public void setPromptForCLID(BroadWorksAnywhereCLIDPrompt value) {
        this.promptForCLID = value;
    }

    /**
     * Ruft den Wert der silentPromptMode-Eigenschaft ab.
     * 
     */
    public boolean isSilentPromptMode() {
        return silentPromptMode;
    }

    /**
     * Legt den Wert der silentPromptMode-Eigenschaft fest.
     * 
     */
    public void setSilentPromptMode(boolean value) {
        this.silentPromptMode = value;
    }

    /**
     * Ruft den Wert der promptForPasscode-Eigenschaft ab.
     * 
     */
    public boolean isPromptForPasscode() {
        return promptForPasscode;
    }

    /**
     * Legt den Wert der promptForPasscode-Eigenschaft fest.
     * 
     */
    public void setPromptForPasscode(boolean value) {
        this.promptForPasscode = value;
    }

}
