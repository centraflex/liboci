//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemClientSessionParametersGetRequest.
 *         Contains a list of system Client Session (web and CLI) parameters.
 *       
 * 
 * <p>Java-Klasse für SystemClientSessionParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemClientSessionParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableInactivityTimeout" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="inactivityTimeoutMinutes" type="{}ClientSessionTimeoutMinutes"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemClientSessionParametersGetResponse", propOrder = {
    "enableInactivityTimeout",
    "inactivityTimeoutMinutes"
})
public class SystemClientSessionParametersGetResponse
    extends OCIDataResponse
{

    protected boolean enableInactivityTimeout;
    protected int inactivityTimeoutMinutes;

    /**
     * Ruft den Wert der enableInactivityTimeout-Eigenschaft ab.
     * 
     */
    public boolean isEnableInactivityTimeout() {
        return enableInactivityTimeout;
    }

    /**
     * Legt den Wert der enableInactivityTimeout-Eigenschaft fest.
     * 
     */
    public void setEnableInactivityTimeout(boolean value) {
        this.enableInactivityTimeout = value;
    }

    /**
     * Ruft den Wert der inactivityTimeoutMinutes-Eigenschaft ab.
     * 
     */
    public int getInactivityTimeoutMinutes() {
        return inactivityTimeoutMinutes;
    }

    /**
     * Legt den Wert der inactivityTimeoutMinutes-Eigenschaft fest.
     * 
     */
    public void setInactivityTimeoutMinutes(int value) {
        this.inactivityTimeoutMinutes = value;
    }

}
