//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Assign a group level Xsi policy profile and a list of user Xsi policy profiles
 *         to a group.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupXsiPolicyProfileAssignListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupXsiPolicyProfileAssignListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="groupXsiPolicyProfile" type="{}XsiPolicyProfileName" minOccurs="0"/>
 *         <element name="userXsiPolicyProfile" type="{}XsiPolicyProfileAssignEntry" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupXsiPolicyProfileAssignListRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "groupXsiPolicyProfile",
    "userXsiPolicyProfile"
})
public class GroupXsiPolicyProfileAssignListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupXsiPolicyProfile;
    protected XsiPolicyProfileAssignEntry userXsiPolicyProfile;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der groupXsiPolicyProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupXsiPolicyProfile() {
        return groupXsiPolicyProfile;
    }

    /**
     * Legt den Wert der groupXsiPolicyProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupXsiPolicyProfile(String value) {
        this.groupXsiPolicyProfile = value;
    }

    /**
     * Ruft den Wert der userXsiPolicyProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XsiPolicyProfileAssignEntry }
     *     
     */
    public XsiPolicyProfileAssignEntry getUserXsiPolicyProfile() {
        return userXsiPolicyProfile;
    }

    /**
     * Legt den Wert der userXsiPolicyProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XsiPolicyProfileAssignEntry }
     *     
     */
    public void setUserXsiPolicyProfile(XsiPolicyProfileAssignEntry value) {
        this.userXsiPolicyProfile = value;
    }

}
