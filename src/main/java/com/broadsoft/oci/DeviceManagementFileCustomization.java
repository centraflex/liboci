//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeviceManagementFileCustomization.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DeviceManagementFileCustomization">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Disallow"/>
 *     <enumeration value="Administrator"/>
 *     <enumeration value="Administrator and User"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DeviceManagementFileCustomization")
@XmlEnum
public enum DeviceManagementFileCustomization {

    @XmlEnumValue("Disallow")
    DISALLOW("Disallow"),
    @XmlEnumValue("Administrator")
    ADMINISTRATOR("Administrator"),
    @XmlEnumValue("Administrator and User")
    ADMINISTRATOR_AND_USER("Administrator and User");
    private final String value;

    DeviceManagementFileCustomization(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeviceManagementFileCustomization fromValue(String v) {
        for (DeviceManagementFileCustomization c: DeviceManagementFileCustomization.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
