//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupAccountAuthorizationCodesGetRequest.
 *         The tables has the following column headings:
 *         "User Id", "Last Name", "First Name", "Hiragana Last Name", "Hiragana First Name",
 *         "Phone Number", "Extension", "Department", "Email Address".
 *       
 * 
 * <p>Java-Klasse für GroupAccountAuthorizationCodesGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAccountAuthorizationCodesGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="type" type="{}AccountAuthorizationCodeType"/>
 *         <element name="numberOfDigits" type="{}AccountAuthorizationCodeNumberOfDigits"/>
 *         <element name="allowLocalAndTollFreeCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="mandatoryUsageUserTable" type="{C}OCITable"/>
 *         <element name="optionalUsageUserTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAccountAuthorizationCodesGetResponse", propOrder = {
    "type",
    "numberOfDigits",
    "allowLocalAndTollFreeCalls",
    "mandatoryUsageUserTable",
    "optionalUsageUserTable"
})
public class GroupAccountAuthorizationCodesGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AccountAuthorizationCodeType type;
    protected int numberOfDigits;
    protected boolean allowLocalAndTollFreeCalls;
    @XmlElement(required = true)
    protected OCITable mandatoryUsageUserTable;
    @XmlElement(required = true)
    protected OCITable optionalUsageUserTable;

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccountAuthorizationCodeType }
     *     
     */
    public AccountAuthorizationCodeType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountAuthorizationCodeType }
     *     
     */
    public void setType(AccountAuthorizationCodeType value) {
        this.type = value;
    }

    /**
     * Ruft den Wert der numberOfDigits-Eigenschaft ab.
     * 
     */
    public int getNumberOfDigits() {
        return numberOfDigits;
    }

    /**
     * Legt den Wert der numberOfDigits-Eigenschaft fest.
     * 
     */
    public void setNumberOfDigits(int value) {
        this.numberOfDigits = value;
    }

    /**
     * Ruft den Wert der allowLocalAndTollFreeCalls-Eigenschaft ab.
     * 
     */
    public boolean isAllowLocalAndTollFreeCalls() {
        return allowLocalAndTollFreeCalls;
    }

    /**
     * Legt den Wert der allowLocalAndTollFreeCalls-Eigenschaft fest.
     * 
     */
    public void setAllowLocalAndTollFreeCalls(boolean value) {
        this.allowLocalAndTollFreeCalls = value;
    }

    /**
     * Ruft den Wert der mandatoryUsageUserTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getMandatoryUsageUserTable() {
        return mandatoryUsageUserTable;
    }

    /**
     * Legt den Wert der mandatoryUsageUserTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setMandatoryUsageUserTable(OCITable value) {
        this.mandatoryUsageUserTable = value;
    }

    /**
     * Ruft den Wert der optionalUsageUserTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getOptionalUsageUserTable() {
        return optionalUsageUserTable;
    }

    /**
     * Legt den Wert der optionalUsageUserTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setOptionalUsageUserTable(OCITable value) {
        this.optionalUsageUserTable = value;
    }

}
