//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ScheduleLevel.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ScheduleLevel">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Service Provider"/>
 *     <enumeration value="Group"/>
 *     <enumeration value="User"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ScheduleLevel")
@XmlEnum
public enum ScheduleLevel {

    @XmlEnumValue("Service Provider")
    SERVICE_PROVIDER("Service Provider"),
    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("User")
    USER("User");
    private final String value;

    ScheduleLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ScheduleLevel fromValue(String v) {
        for (ScheduleLevel c: ScheduleLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
