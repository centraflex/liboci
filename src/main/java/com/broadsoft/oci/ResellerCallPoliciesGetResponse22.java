//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ResellerCallPoliciesGetRequest22.
 *       
 * 
 * <p>Java-Klasse für ResellerCallPoliciesGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerCallPoliciesGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="forceRedirectingUserIdentityForRedirectedCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="applyRedirectingUserIdentityToNetworkLocations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerCallPoliciesGetResponse22", propOrder = {
    "forceRedirectingUserIdentityForRedirectedCalls",
    "applyRedirectingUserIdentityToNetworkLocations"
})
public class ResellerCallPoliciesGetResponse22
    extends OCIDataResponse
{

    protected boolean forceRedirectingUserIdentityForRedirectedCalls;
    protected boolean applyRedirectingUserIdentityToNetworkLocations;

    /**
     * Ruft den Wert der forceRedirectingUserIdentityForRedirectedCalls-Eigenschaft ab.
     * 
     */
    public boolean isForceRedirectingUserIdentityForRedirectedCalls() {
        return forceRedirectingUserIdentityForRedirectedCalls;
    }

    /**
     * Legt den Wert der forceRedirectingUserIdentityForRedirectedCalls-Eigenschaft fest.
     * 
     */
    public void setForceRedirectingUserIdentityForRedirectedCalls(boolean value) {
        this.forceRedirectingUserIdentityForRedirectedCalls = value;
    }

    /**
     * Ruft den Wert der applyRedirectingUserIdentityToNetworkLocations-Eigenschaft ab.
     * 
     */
    public boolean isApplyRedirectingUserIdentityToNetworkLocations() {
        return applyRedirectingUserIdentityToNetworkLocations;
    }

    /**
     * Legt den Wert der applyRedirectingUserIdentityToNetworkLocations-Eigenschaft fest.
     * 
     */
    public void setApplyRedirectingUserIdentityToNetworkLocations(boolean value) {
        this.applyRedirectingUserIdentityToNetworkLocations = value;
    }

}
