//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemSMPPGetRequest14sp5.
 *         
 *         Replaced by: SystemSMPPGetResponse21 in AS data mode
 *       
 * 
 * <p>Java-Klasse für SystemSMPPGetResponse14sp5 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSMPPGetResponse14sp5">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="primarySMPPServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="primarySMPPPort" type="{}Port"/>
 *         <element name="secondarySMPPServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="secondarySMPPPort" type="{}Port"/>
 *         <element name="systemId" type="{}SMPPSystemId" minOccurs="0"/>
 *         <element name="password" type="{}SMPPPassword" minOccurs="0"/>
 *         <element name="version" type="{}SMPPVersion"/>
 *         <element name="systemType" type="{}SMPPSystemType" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSMPPGetResponse14sp5", propOrder = {
    "primarySMPPServerNetAddress",
    "primarySMPPPort",
    "secondarySMPPServerNetAddress",
    "secondarySMPPPort",
    "systemId",
    "password",
    "version",
    "systemType"
})
public class SystemSMPPGetResponse14Sp5
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String primarySMPPServerNetAddress;
    protected int primarySMPPPort;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String secondarySMPPServerNetAddress;
    protected int secondarySMPPPort;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String systemId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String password;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String version;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String systemType;

    /**
     * Ruft den Wert der primarySMPPServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimarySMPPServerNetAddress() {
        return primarySMPPServerNetAddress;
    }

    /**
     * Legt den Wert der primarySMPPServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimarySMPPServerNetAddress(String value) {
        this.primarySMPPServerNetAddress = value;
    }

    /**
     * Ruft den Wert der primarySMPPPort-Eigenschaft ab.
     * 
     */
    public int getPrimarySMPPPort() {
        return primarySMPPPort;
    }

    /**
     * Legt den Wert der primarySMPPPort-Eigenschaft fest.
     * 
     */
    public void setPrimarySMPPPort(int value) {
        this.primarySMPPPort = value;
    }

    /**
     * Ruft den Wert der secondarySMPPServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondarySMPPServerNetAddress() {
        return secondarySMPPServerNetAddress;
    }

    /**
     * Legt den Wert der secondarySMPPServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondarySMPPServerNetAddress(String value) {
        this.secondarySMPPServerNetAddress = value;
    }

    /**
     * Ruft den Wert der secondarySMPPPort-Eigenschaft ab.
     * 
     */
    public int getSecondarySMPPPort() {
        return secondarySMPPPort;
    }

    /**
     * Legt den Wert der secondarySMPPPort-Eigenschaft fest.
     * 
     */
    public void setSecondarySMPPPort(int value) {
        this.secondarySMPPPort = value;
    }

    /**
     * Ruft den Wert der systemId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemId() {
        return systemId;
    }

    /**
     * Legt den Wert der systemId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemId(String value) {
        this.systemId = value;
    }

    /**
     * Ruft den Wert der password-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Legt den Wert der password-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Ruft den Wert der version-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Legt den Wert der version-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Ruft den Wert der systemType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemType() {
        return systemType;
    }

    /**
     * Legt den Wert der systemType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemType(String value) {
        this.systemType = value;
    }

}
