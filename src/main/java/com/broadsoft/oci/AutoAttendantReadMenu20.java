//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The configuration of the automated receptionist greeting
 *         prompt and dialing menu to be used during after business hours.
 *       
 * 
 * <p>Java-Klasse für AutoAttendantReadMenu20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="AutoAttendantReadMenu20">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="announcementSelection" type="{}AnnouncementSelection"/>
 *         <element name="audioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="videoFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="enableFirstMenuLevelExtensionDialing" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="keyConfiguration" type="{}AutoAttendantKeyReadConfiguration20" maxOccurs="12" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutoAttendantReadMenu20", propOrder = {
    "announcementSelection",
    "audioFile",
    "videoFile",
    "enableFirstMenuLevelExtensionDialing",
    "keyConfiguration"
})
public class AutoAttendantReadMenu20 {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AnnouncementSelection announcementSelection;
    protected AnnouncementFileLevelKey audioFile;
    protected AnnouncementFileLevelKey videoFile;
    protected boolean enableFirstMenuLevelExtensionDialing;
    protected List<AutoAttendantKeyReadConfiguration20> keyConfiguration;

    /**
     * Ruft den Wert der announcementSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementSelection }
     *     
     */
    public AnnouncementSelection getAnnouncementSelection() {
        return announcementSelection;
    }

    /**
     * Legt den Wert der announcementSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementSelection }
     *     
     */
    public void setAnnouncementSelection(AnnouncementSelection value) {
        this.announcementSelection = value;
    }

    /**
     * Ruft den Wert der audioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getAudioFile() {
        return audioFile;
    }

    /**
     * Legt den Wert der audioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setAudioFile(AnnouncementFileLevelKey value) {
        this.audioFile = value;
    }

    /**
     * Ruft den Wert der videoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getVideoFile() {
        return videoFile;
    }

    /**
     * Legt den Wert der videoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setVideoFile(AnnouncementFileLevelKey value) {
        this.videoFile = value;
    }

    /**
     * Ruft den Wert der enableFirstMenuLevelExtensionDialing-Eigenschaft ab.
     * 
     */
    public boolean isEnableFirstMenuLevelExtensionDialing() {
        return enableFirstMenuLevelExtensionDialing;
    }

    /**
     * Legt den Wert der enableFirstMenuLevelExtensionDialing-Eigenschaft fest.
     * 
     */
    public void setEnableFirstMenuLevelExtensionDialing(boolean value) {
        this.enableFirstMenuLevelExtensionDialing = value;
    }

    /**
     * Gets the value of the keyConfiguration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the keyConfiguration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKeyConfiguration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AutoAttendantKeyReadConfiguration20 }
     * 
     * 
     * @return
     *     The value of the keyConfiguration property.
     */
    public List<AutoAttendantKeyReadConfiguration20> getKeyConfiguration() {
        if (keyConfiguration == null) {
            keyConfiguration = new ArrayList<>();
        }
        return this.keyConfiguration;
    }

}
