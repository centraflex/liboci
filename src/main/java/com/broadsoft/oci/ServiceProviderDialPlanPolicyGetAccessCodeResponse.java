//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to ServiceProviderDialPlanPolicyGetAccessCodeRequest
 *       
 * 
 * <p>Java-Klasse für ServiceProviderDialPlanPolicyGetAccessCodeResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderDialPlanPolicyGetAccessCodeResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="includeCodeForNetworkTranslationsAndRouting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeCodeForScreeningServices" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableSecondaryDialTone" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="description" type="{}DialPlanAccessCodeDescription" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderDialPlanPolicyGetAccessCodeResponse", propOrder = {
    "includeCodeForNetworkTranslationsAndRouting",
    "includeCodeForScreeningServices",
    "enableSecondaryDialTone",
    "description"
})
public class ServiceProviderDialPlanPolicyGetAccessCodeResponse
    extends OCIDataResponse
{

    protected boolean includeCodeForNetworkTranslationsAndRouting;
    protected boolean includeCodeForScreeningServices;
    protected boolean enableSecondaryDialTone;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;

    /**
     * Ruft den Wert der includeCodeForNetworkTranslationsAndRouting-Eigenschaft ab.
     * 
     */
    public boolean isIncludeCodeForNetworkTranslationsAndRouting() {
        return includeCodeForNetworkTranslationsAndRouting;
    }

    /**
     * Legt den Wert der includeCodeForNetworkTranslationsAndRouting-Eigenschaft fest.
     * 
     */
    public void setIncludeCodeForNetworkTranslationsAndRouting(boolean value) {
        this.includeCodeForNetworkTranslationsAndRouting = value;
    }

    /**
     * Ruft den Wert der includeCodeForScreeningServices-Eigenschaft ab.
     * 
     */
    public boolean isIncludeCodeForScreeningServices() {
        return includeCodeForScreeningServices;
    }

    /**
     * Legt den Wert der includeCodeForScreeningServices-Eigenschaft fest.
     * 
     */
    public void setIncludeCodeForScreeningServices(boolean value) {
        this.includeCodeForScreeningServices = value;
    }

    /**
     * Ruft den Wert der enableSecondaryDialTone-Eigenschaft ab.
     * 
     */
    public boolean isEnableSecondaryDialTone() {
        return enableSecondaryDialTone;
    }

    /**
     * Legt den Wert der enableSecondaryDialTone-Eigenschaft fest.
     * 
     */
    public void setEnableSecondaryDialTone(boolean value) {
        this.enableSecondaryDialTone = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

}
