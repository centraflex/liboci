//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a Music on Hold Instance to a department.
 *         The response is either SuccessResponse or ErrorResponse.
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *       	- useDynamicMOHDuringCallHold 
 *       
 * 
 * <p>Java-Klasse für GroupMusicOnHoldAddInstanceRequest23 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupMusicOnHoldAddInstanceRequest23">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="department" type="{}DepartmentKey"/>
 *         <element name="isActiveDuringCallHold" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isActiveDuringCallPark" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isActiveDuringBusyCampOn" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="source" type="{}MusicOnHoldSourceAdd22"/>
 *         <element name="useAlternateSourceForInternalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="internalSource" type="{}MusicOnHoldSourceAdd22" minOccurs="0"/>
 *         <element name="useDynamicMOHDuringCallHold" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupMusicOnHoldAddInstanceRequest23", propOrder = {
    "serviceProviderId",
    "groupId",
    "department",
    "isActiveDuringCallHold",
    "isActiveDuringCallPark",
    "isActiveDuringBusyCampOn",
    "source",
    "useAlternateSourceForInternalCalls",
    "internalSource",
    "useDynamicMOHDuringCallHold"
})
public class GroupMusicOnHoldAddInstanceRequest23
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    protected DepartmentKey department;
    protected boolean isActiveDuringCallHold;
    protected boolean isActiveDuringCallPark;
    protected boolean isActiveDuringBusyCampOn;
    @XmlElement(required = true)
    protected MusicOnHoldSourceAdd22 source;
    protected boolean useAlternateSourceForInternalCalls;
    protected MusicOnHoldSourceAdd22 internalSource;
    protected boolean useDynamicMOHDuringCallHold;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der department-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DepartmentKey }
     *     
     */
    public DepartmentKey getDepartment() {
        return department;
    }

    /**
     * Legt den Wert der department-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DepartmentKey }
     *     
     */
    public void setDepartment(DepartmentKey value) {
        this.department = value;
    }

    /**
     * Ruft den Wert der isActiveDuringCallHold-Eigenschaft ab.
     * 
     */
    public boolean isIsActiveDuringCallHold() {
        return isActiveDuringCallHold;
    }

    /**
     * Legt den Wert der isActiveDuringCallHold-Eigenschaft fest.
     * 
     */
    public void setIsActiveDuringCallHold(boolean value) {
        this.isActiveDuringCallHold = value;
    }

    /**
     * Ruft den Wert der isActiveDuringCallPark-Eigenschaft ab.
     * 
     */
    public boolean isIsActiveDuringCallPark() {
        return isActiveDuringCallPark;
    }

    /**
     * Legt den Wert der isActiveDuringCallPark-Eigenschaft fest.
     * 
     */
    public void setIsActiveDuringCallPark(boolean value) {
        this.isActiveDuringCallPark = value;
    }

    /**
     * Ruft den Wert der isActiveDuringBusyCampOn-Eigenschaft ab.
     * 
     */
    public boolean isIsActiveDuringBusyCampOn() {
        return isActiveDuringBusyCampOn;
    }

    /**
     * Legt den Wert der isActiveDuringBusyCampOn-Eigenschaft fest.
     * 
     */
    public void setIsActiveDuringBusyCampOn(boolean value) {
        this.isActiveDuringBusyCampOn = value;
    }

    /**
     * Ruft den Wert der source-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceAdd22 }
     *     
     */
    public MusicOnHoldSourceAdd22 getSource() {
        return source;
    }

    /**
     * Legt den Wert der source-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceAdd22 }
     *     
     */
    public void setSource(MusicOnHoldSourceAdd22 value) {
        this.source = value;
    }

    /**
     * Ruft den Wert der useAlternateSourceForInternalCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseAlternateSourceForInternalCalls() {
        return useAlternateSourceForInternalCalls;
    }

    /**
     * Legt den Wert der useAlternateSourceForInternalCalls-Eigenschaft fest.
     * 
     */
    public void setUseAlternateSourceForInternalCalls(boolean value) {
        this.useAlternateSourceForInternalCalls = value;
    }

    /**
     * Ruft den Wert der internalSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceAdd22 }
     *     
     */
    public MusicOnHoldSourceAdd22 getInternalSource() {
        return internalSource;
    }

    /**
     * Legt den Wert der internalSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceAdd22 }
     *     
     */
    public void setInternalSource(MusicOnHoldSourceAdd22 value) {
        this.internalSource = value;
    }

    /**
     * Ruft den Wert der useDynamicMOHDuringCallHold-Eigenschaft ab.
     * 
     */
    public boolean isUseDynamicMOHDuringCallHold() {
        return useDynamicMOHDuringCallHold;
    }

    /**
     * Legt den Wert der useDynamicMOHDuringCallHold-Eigenschaft fest.
     * 
     */
    public void setUseDynamicMOHDuringCallHold(boolean value) {
        this.useDynamicMOHDuringCallHold = value;
    }

}
