//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemBroadWorksMobilityGeographicalPoolGetListRequest.
 *         Contains a table with column headings: “Pool”, "Country Code", "Is Default", “Description”.
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobilityGeographicalPoolGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobilityGeographicalPoolGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="geographicalPoolTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobilityGeographicalPoolGetListResponse", propOrder = {
    "geographicalPoolTable"
})
public class SystemBroadWorksMobilityGeographicalPoolGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable geographicalPoolTable;

    /**
     * Ruft den Wert der geographicalPoolTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getGeographicalPoolTable() {
        return geographicalPoolTable;
    }

    /**
     * Legt den Wert der geographicalPoolTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setGeographicalPoolTable(OCITable value) {
        this.geographicalPoolTable = value;
    }

}
