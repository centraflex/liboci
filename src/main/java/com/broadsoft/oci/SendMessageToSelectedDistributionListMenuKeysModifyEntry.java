//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal send message to selected distribution list menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für SendMessageToSelectedDistributionListMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SendMessageToSelectedDistributionListMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="confirmSendingToDistributionList" type="{}DigitStarPound" minOccurs="0"/>
 *         <element name="cancelSendingToDistributionList" type="{}DigitStarPound" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendMessageToSelectedDistributionListMenuKeysModifyEntry", propOrder = {
    "confirmSendingToDistributionList",
    "cancelSendingToDistributionList"
})
public class SendMessageToSelectedDistributionListMenuKeysModifyEntry {

    @XmlElementRef(name = "confirmSendingToDistributionList", type = JAXBElement.class, required = false)
    protected JAXBElement<String> confirmSendingToDistributionList;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String cancelSendingToDistributionList;

    /**
     * Ruft den Wert der confirmSendingToDistributionList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getConfirmSendingToDistributionList() {
        return confirmSendingToDistributionList;
    }

    /**
     * Legt den Wert der confirmSendingToDistributionList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setConfirmSendingToDistributionList(JAXBElement<String> value) {
        this.confirmSendingToDistributionList = value;
    }

    /**
     * Ruft den Wert der cancelSendingToDistributionList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancelSendingToDistributionList() {
        return cancelSendingToDistributionList;
    }

    /**
     * Legt den Wert der cancelSendingToDistributionList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancelSendingToDistributionList(String value) {
        this.cancelSendingToDistributionList = value;
    }

}
