//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the ServiceProviderDialableCallerIDGetRequest.
 *         The criteria table?s column headings are ?Active?, "Name", "Description", ?Prefix Digits?, and ?Priority?.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderDialableCallerIDGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderDialableCallerIDGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useServiceProviderCriteria" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="nsScreeningFailurePolicy" type="{}NsScreeningFailurePolicy"/>
 *         <element name="criteriaTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderDialableCallerIDGetResponse", propOrder = {
    "useServiceProviderCriteria",
    "nsScreeningFailurePolicy",
    "criteriaTable"
})
public class ServiceProviderDialableCallerIDGetResponse
    extends OCIDataResponse
{

    protected boolean useServiceProviderCriteria;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected NsScreeningFailurePolicy nsScreeningFailurePolicy;
    @XmlElement(required = true)
    protected OCITable criteriaTable;

    /**
     * Ruft den Wert der useServiceProviderCriteria-Eigenschaft ab.
     * 
     */
    public boolean isUseServiceProviderCriteria() {
        return useServiceProviderCriteria;
    }

    /**
     * Legt den Wert der useServiceProviderCriteria-Eigenschaft fest.
     * 
     */
    public void setUseServiceProviderCriteria(boolean value) {
        this.useServiceProviderCriteria = value;
    }

    /**
     * Ruft den Wert der nsScreeningFailurePolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NsScreeningFailurePolicy }
     *     
     */
    public NsScreeningFailurePolicy getNsScreeningFailurePolicy() {
        return nsScreeningFailurePolicy;
    }

    /**
     * Legt den Wert der nsScreeningFailurePolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NsScreeningFailurePolicy }
     *     
     */
    public void setNsScreeningFailurePolicy(NsScreeningFailurePolicy value) {
        this.nsScreeningFailurePolicy = value;
    }

    /**
     * Ruft den Wert der criteriaTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getCriteriaTable() {
        return criteriaTable;
    }

    /**
     * Legt den Wert der criteriaTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setCriteriaTable(OCITable value) {
        this.criteriaTable = value;
    }

}
