//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserCallMeNowGetCriteriaRequest.
 *       
 * 
 * <p>Java-Klasse für UserCallMeNowGetCriteriaResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallMeNowGetCriteriaResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="timeSchedule" type="{}TimeSchedule" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}HolidaySchedule" minOccurs="0"/>
 *         <element name="rejectCall" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="toDnCriteria" type="{}CallMeNowToDnCriteria"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallMeNowGetCriteriaResponse", propOrder = {
    "timeSchedule",
    "holidaySchedule",
    "rejectCall",
    "toDnCriteria"
})
public class UserCallMeNowGetCriteriaResponse
    extends OCIDataResponse
{

    protected TimeSchedule timeSchedule;
    protected HolidaySchedule holidaySchedule;
    protected boolean rejectCall;
    @XmlElement(required = true)
    protected CallMeNowToDnCriteria toDnCriteria;

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeSchedule }
     *     
     */
    public TimeSchedule getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeSchedule }
     *     
     */
    public void setTimeSchedule(TimeSchedule value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HolidaySchedule }
     *     
     */
    public HolidaySchedule getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HolidaySchedule }
     *     
     */
    public void setHolidaySchedule(HolidaySchedule value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der rejectCall-Eigenschaft ab.
     * 
     */
    public boolean isRejectCall() {
        return rejectCall;
    }

    /**
     * Legt den Wert der rejectCall-Eigenschaft fest.
     * 
     */
    public void setRejectCall(boolean value) {
        this.rejectCall = value;
    }

    /**
     * Ruft den Wert der toDnCriteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallMeNowToDnCriteria }
     *     
     */
    public CallMeNowToDnCriteria getToDnCriteria() {
        return toDnCriteria;
    }

    /**
     * Legt den Wert der toDnCriteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallMeNowToDnCriteria }
     *     
     */
    public void setToDnCriteria(CallMeNowToDnCriteria value) {
        this.toDnCriteria = value;
    }

}
