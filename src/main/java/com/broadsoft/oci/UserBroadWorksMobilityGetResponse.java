//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *          Response to a UserBroadWorksMobilityGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserBroadWorksMobilityGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksMobilityGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="phonesToRing" type="{}BroadWorksMobilityPhoneToRing"/>
 *         <element name="mobilePhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="alertClickToDialCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="alertGroupPagingCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableDiversionInhibitor" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="requireAnswerConfirmation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="broadworksCallControl" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useSettingLevel" type="{}BroadWorksMobilityUserSettingLevel"/>
 *         <element name="denyCallOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="denyCallTerminations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksMobilityGetResponse", propOrder = {
    "isActive",
    "phonesToRing",
    "mobilePhoneNumber",
    "alertClickToDialCalls",
    "alertGroupPagingCalls",
    "enableDiversionInhibitor",
    "requireAnswerConfirmation",
    "broadworksCallControl",
    "useSettingLevel",
    "denyCallOriginations",
    "denyCallTerminations"
})
public class UserBroadWorksMobilityGetResponse
    extends OCIDataResponse
{

    protected boolean isActive;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BroadWorksMobilityPhoneToRing phonesToRing;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mobilePhoneNumber;
    protected boolean alertClickToDialCalls;
    protected boolean alertGroupPagingCalls;
    protected boolean enableDiversionInhibitor;
    protected boolean requireAnswerConfirmation;
    protected boolean broadworksCallControl;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BroadWorksMobilityUserSettingLevel useSettingLevel;
    protected boolean denyCallOriginations;
    protected boolean denyCallTerminations;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der phonesToRing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksMobilityPhoneToRing }
     *     
     */
    public BroadWorksMobilityPhoneToRing getPhonesToRing() {
        return phonesToRing;
    }

    /**
     * Legt den Wert der phonesToRing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksMobilityPhoneToRing }
     *     
     */
    public void setPhonesToRing(BroadWorksMobilityPhoneToRing value) {
        this.phonesToRing = value;
    }

    /**
     * Ruft den Wert der mobilePhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    /**
     * Legt den Wert der mobilePhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobilePhoneNumber(String value) {
        this.mobilePhoneNumber = value;
    }

    /**
     * Ruft den Wert der alertClickToDialCalls-Eigenschaft ab.
     * 
     */
    public boolean isAlertClickToDialCalls() {
        return alertClickToDialCalls;
    }

    /**
     * Legt den Wert der alertClickToDialCalls-Eigenschaft fest.
     * 
     */
    public void setAlertClickToDialCalls(boolean value) {
        this.alertClickToDialCalls = value;
    }

    /**
     * Ruft den Wert der alertGroupPagingCalls-Eigenschaft ab.
     * 
     */
    public boolean isAlertGroupPagingCalls() {
        return alertGroupPagingCalls;
    }

    /**
     * Legt den Wert der alertGroupPagingCalls-Eigenschaft fest.
     * 
     */
    public void setAlertGroupPagingCalls(boolean value) {
        this.alertGroupPagingCalls = value;
    }

    /**
     * Ruft den Wert der enableDiversionInhibitor-Eigenschaft ab.
     * 
     */
    public boolean isEnableDiversionInhibitor() {
        return enableDiversionInhibitor;
    }

    /**
     * Legt den Wert der enableDiversionInhibitor-Eigenschaft fest.
     * 
     */
    public void setEnableDiversionInhibitor(boolean value) {
        this.enableDiversionInhibitor = value;
    }

    /**
     * Ruft den Wert der requireAnswerConfirmation-Eigenschaft ab.
     * 
     */
    public boolean isRequireAnswerConfirmation() {
        return requireAnswerConfirmation;
    }

    /**
     * Legt den Wert der requireAnswerConfirmation-Eigenschaft fest.
     * 
     */
    public void setRequireAnswerConfirmation(boolean value) {
        this.requireAnswerConfirmation = value;
    }

    /**
     * Ruft den Wert der broadworksCallControl-Eigenschaft ab.
     * 
     */
    public boolean isBroadworksCallControl() {
        return broadworksCallControl;
    }

    /**
     * Legt den Wert der broadworksCallControl-Eigenschaft fest.
     * 
     */
    public void setBroadworksCallControl(boolean value) {
        this.broadworksCallControl = value;
    }

    /**
     * Ruft den Wert der useSettingLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksMobilityUserSettingLevel }
     *     
     */
    public BroadWorksMobilityUserSettingLevel getUseSettingLevel() {
        return useSettingLevel;
    }

    /**
     * Legt den Wert der useSettingLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksMobilityUserSettingLevel }
     *     
     */
    public void setUseSettingLevel(BroadWorksMobilityUserSettingLevel value) {
        this.useSettingLevel = value;
    }

    /**
     * Ruft den Wert der denyCallOriginations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallOriginations() {
        return denyCallOriginations;
    }

    /**
     * Legt den Wert der denyCallOriginations-Eigenschaft fest.
     * 
     */
    public void setDenyCallOriginations(boolean value) {
        this.denyCallOriginations = value;
    }

    /**
     * Ruft den Wert der denyCallTerminations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallTerminations() {
        return denyCallTerminations;
    }

    /**
     * Legt den Wert der denyCallTerminations-Eigenschaft fest.
     * 
     */
    public void setDenyCallTerminations(boolean value) {
        this.denyCallTerminations = value;
    }

}
