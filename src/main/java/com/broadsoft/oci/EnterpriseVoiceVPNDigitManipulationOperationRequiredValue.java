//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EnterpriseVoiceVPNDigitManipulationOperationRequiredValue.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="EnterpriseVoiceVPNDigitManipulationOperationRequiredValue">
 *   <restriction base="{}EnterpriseVoiceVPNDigitManipulationOperation">
 *     <enumeration value="Position"/>
 *     <enumeration value="Delete"/>
 *     <enumeration value="Move"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "EnterpriseVoiceVPNDigitManipulationOperationRequiredValue")
@XmlEnum(EnterpriseVoiceVPNDigitManipulationOperation.class)
public enum EnterpriseVoiceVPNDigitManipulationOperationRequiredValue {

    @XmlEnumValue("Position")
    POSITION(EnterpriseVoiceVPNDigitManipulationOperation.POSITION),
    @XmlEnumValue("Delete")
    DELETE(EnterpriseVoiceVPNDigitManipulationOperation.DELETE),
    @XmlEnumValue("Move")
    MOVE(EnterpriseVoiceVPNDigitManipulationOperation.MOVE);
    private final EnterpriseVoiceVPNDigitManipulationOperation value;

    EnterpriseVoiceVPNDigitManipulationOperationRequiredValue(EnterpriseVoiceVPNDigitManipulationOperation v) {
        value = v;
    }

    public EnterpriseVoiceVPNDigitManipulationOperation value() {
        return value;
    }

    public static EnterpriseVoiceVPNDigitManipulationOperationRequiredValue fromValue(EnterpriseVoiceVPNDigitManipulationOperation v) {
        for (EnterpriseVoiceVPNDigitManipulationOperationRequiredValue c: EnterpriseVoiceVPNDigitManipulationOperationRequiredValue.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
