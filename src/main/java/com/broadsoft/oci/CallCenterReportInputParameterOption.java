//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterReportInputParameterOption.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterReportInputParameterOption">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Required"/>
 *     <enumeration value="Hidden"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterReportInputParameterOption")
@XmlEnum
public enum CallCenterReportInputParameterOption {

    @XmlEnumValue("Required")
    REQUIRED("Required"),
    @XmlEnumValue("Hidden")
    HIDDEN("Hidden");
    private final String value;

    CallCenterReportInputParameterOption(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterReportInputParameterOption fromValue(String v) {
        for (CallCenterReportInputParameterOption c: CallCenterReportInputParameterOption.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
