//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserPushNotificationRegistrationGetListRequest.
 *         
 *         A registration has more than one row in the response when the registration includes more than one token.
 *         
 *         Registration Date uses the format "yyyy-MM-dd'T'HH:mm:ss.SSSZ" in the time zone of the requested user.
 *         Example: 2010-10-01T09:30:00:000-0400.
 *         
 *         Contains a table with a row for each registration and token with column headings :
 *         "User Id",  "Registration Id",  "Application Id",  "Application Version",  "Device Os Type",  
 *         "Device Os Version",  "Registration Date",  "Token",  "Events"
 *         
 *         Replaced by: UserPushNotificationRegistrationGetListResponse21sp1
 *       
 * 
 * <p>Java-Klasse für UserPushNotificationRegistrationGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPushNotificationRegistrationGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="registrationsTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPushNotificationRegistrationGetListResponse", propOrder = {
    "registrationsTable"
})
public class UserPushNotificationRegistrationGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable registrationsTable;

    /**
     * Ruft den Wert der registrationsTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getRegistrationsTable() {
        return registrationsTable;
    }

    /**
     * Legt den Wert der registrationsTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setRegistrationsTable(OCITable value) {
        this.registrationsTable = value;
    }

}
