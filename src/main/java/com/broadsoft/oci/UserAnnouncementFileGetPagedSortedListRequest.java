//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Get the list of announcement files for a user.
 *         If the responsePagingControl element is not provided,
 *         the paging startIndex will be set to 1 by default,
 *         and the responsePageSize will be set to the maximum responsePageSize by
 *         default.
 *         If no sortOrder is provided, the response is sorted by Name ascending by default.
 *         Multiple search criteria are logically ANDed together unless the searchCriteriaModeOr option is included.
 *         Then the search criteria are logically ORed together.
 *         The response is either a
 *         UserAnnouncementFileGetPagedSortedListResponse or an
 *         ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserAnnouncementFileGetPagedSortedListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserAnnouncementFileGetPagedSortedListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl" minOccurs="0"/>
 *         <choice minOccurs="0">
 *           <element name="sortByAnnouncementFileName" type="{}SortByAnnouncementFileName"/>
 *           <element name="sortByAnnouncementFileSize" type="{}SortByAnnouncementFileSize"/>
 *         </choice>
 *         <element name="searchCriteriaAnnouncementFileName" type="{}SearchCriteriaAnnouncementFileName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactAnnouncementFileType" type="{}SearchCriteriaExactAnnouncementFileType" minOccurs="0"/>
 *         <element name="searchCriteriaExactMediaFileType" type="{}SearchCriteriaExactMediaFileType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaModeOr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserAnnouncementFileGetPagedSortedListRequest", propOrder = {
    "userId",
    "responsePagingControl",
    "sortByAnnouncementFileName",
    "sortByAnnouncementFileSize",
    "searchCriteriaAnnouncementFileName",
    "searchCriteriaExactAnnouncementFileType",
    "searchCriteriaExactMediaFileType",
    "searchCriteriaModeOr"
})
public class UserAnnouncementFileGetPagedSortedListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected ResponsePagingControl responsePagingControl;
    protected SortByAnnouncementFileName sortByAnnouncementFileName;
    protected SortByAnnouncementFileSize sortByAnnouncementFileSize;
    protected List<SearchCriteriaAnnouncementFileName> searchCriteriaAnnouncementFileName;
    protected SearchCriteriaExactAnnouncementFileType searchCriteriaExactAnnouncementFileType;
    protected List<SearchCriteriaExactMediaFileType> searchCriteriaExactMediaFileType;
    protected Boolean searchCriteriaModeOr;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Ruft den Wert der sortByAnnouncementFileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByAnnouncementFileName }
     *     
     */
    public SortByAnnouncementFileName getSortByAnnouncementFileName() {
        return sortByAnnouncementFileName;
    }

    /**
     * Legt den Wert der sortByAnnouncementFileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByAnnouncementFileName }
     *     
     */
    public void setSortByAnnouncementFileName(SortByAnnouncementFileName value) {
        this.sortByAnnouncementFileName = value;
    }

    /**
     * Ruft den Wert der sortByAnnouncementFileSize-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByAnnouncementFileSize }
     *     
     */
    public SortByAnnouncementFileSize getSortByAnnouncementFileSize() {
        return sortByAnnouncementFileSize;
    }

    /**
     * Legt den Wert der sortByAnnouncementFileSize-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByAnnouncementFileSize }
     *     
     */
    public void setSortByAnnouncementFileSize(SortByAnnouncementFileSize value) {
        this.sortByAnnouncementFileSize = value;
    }

    /**
     * Gets the value of the searchCriteriaAnnouncementFileName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaAnnouncementFileName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaAnnouncementFileName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaAnnouncementFileName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaAnnouncementFileName property.
     */
    public List<SearchCriteriaAnnouncementFileName> getSearchCriteriaAnnouncementFileName() {
        if (searchCriteriaAnnouncementFileName == null) {
            searchCriteriaAnnouncementFileName = new ArrayList<>();
        }
        return this.searchCriteriaAnnouncementFileName;
    }

    /**
     * Ruft den Wert der searchCriteriaExactAnnouncementFileType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactAnnouncementFileType }
     *     
     */
    public SearchCriteriaExactAnnouncementFileType getSearchCriteriaExactAnnouncementFileType() {
        return searchCriteriaExactAnnouncementFileType;
    }

    /**
     * Legt den Wert der searchCriteriaExactAnnouncementFileType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactAnnouncementFileType }
     *     
     */
    public void setSearchCriteriaExactAnnouncementFileType(SearchCriteriaExactAnnouncementFileType value) {
        this.searchCriteriaExactAnnouncementFileType = value;
    }

    /**
     * Gets the value of the searchCriteriaExactMediaFileType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaExactMediaFileType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaExactMediaFileType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaExactMediaFileType }
     * 
     * 
     * @return
     *     The value of the searchCriteriaExactMediaFileType property.
     */
    public List<SearchCriteriaExactMediaFileType> getSearchCriteriaExactMediaFileType() {
        if (searchCriteriaExactMediaFileType == null) {
            searchCriteriaExactMediaFileType = new ArrayList<>();
        }
        return this.searchCriteriaExactMediaFileType;
    }

    /**
     * Ruft den Wert der searchCriteriaModeOr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchCriteriaModeOr() {
        return searchCriteriaModeOr;
    }

    /**
     * Legt den Wert der searchCriteriaModeOr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchCriteriaModeOr(Boolean value) {
        this.searchCriteriaModeOr = value;
    }

}
