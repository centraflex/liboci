//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GETSCallIdentifierMode.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="GETSCallIdentifierMode">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Request-URI"/>
 *     <enumeration value="RPH"/>
 *     <enumeration value="RPH-Request-URI"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "GETSCallIdentifierMode")
@XmlEnum
public enum GETSCallIdentifierMode {

    @XmlEnumValue("Request-URI")
    REQUEST_URI("Request-URI"),
    RPH("RPH"),
    @XmlEnumValue("RPH-Request-URI")
    RPH_REQUEST_URI("RPH-Request-URI");
    private final String value;

    GETSCallIdentifierMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GETSCallIdentifierMode fromValue(String v) {
        for (GETSCallIdentifierMode c: GETSCallIdentifierMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
