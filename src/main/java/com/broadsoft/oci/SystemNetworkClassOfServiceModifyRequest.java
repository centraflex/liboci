//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify an existing Network Class of Service.
 *         The following elements are only used in AS data mode:
 *           callProcessingPolicyProfileName
 *           
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemNetworkClassOfServiceModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNetworkClassOfServiceModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="name" type="{}NetworkClassOfServiceName"/>
 *         <element name="newName" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="description" type="{}NetworkClassOfServiceDescription" minOccurs="0"/>
 *         <element name="communicationBarringProfile0" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile1" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile2" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile3" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile4" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile5" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile6" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile7" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile8" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile9" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="networkTranslationIndex" type="{}NetworkTranslationIndex" minOccurs="0"/>
 *         <element name="callProcessingPolicyProfileName" type="{}CallProcessingPolicyProfileName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNetworkClassOfServiceModifyRequest", propOrder = {
    "name",
    "newName",
    "description",
    "communicationBarringProfile0",
    "communicationBarringProfile1",
    "communicationBarringProfile2",
    "communicationBarringProfile3",
    "communicationBarringProfile4",
    "communicationBarringProfile5",
    "communicationBarringProfile6",
    "communicationBarringProfile7",
    "communicationBarringProfile8",
    "communicationBarringProfile9",
    "networkTranslationIndex",
    "callProcessingPolicyProfileName"
})
public class SystemNetworkClassOfServiceModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newName;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "communicationBarringProfile0", type = JAXBElement.class, required = false)
    protected JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> communicationBarringProfile0;
    @XmlElementRef(name = "communicationBarringProfile1", type = JAXBElement.class, required = false)
    protected JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> communicationBarringProfile1;
    @XmlElementRef(name = "communicationBarringProfile2", type = JAXBElement.class, required = false)
    protected JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> communicationBarringProfile2;
    @XmlElementRef(name = "communicationBarringProfile3", type = JAXBElement.class, required = false)
    protected JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> communicationBarringProfile3;
    @XmlElementRef(name = "communicationBarringProfile4", type = JAXBElement.class, required = false)
    protected JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> communicationBarringProfile4;
    @XmlElementRef(name = "communicationBarringProfile5", type = JAXBElement.class, required = false)
    protected JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> communicationBarringProfile5;
    @XmlElementRef(name = "communicationBarringProfile6", type = JAXBElement.class, required = false)
    protected JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> communicationBarringProfile6;
    @XmlElementRef(name = "communicationBarringProfile7", type = JAXBElement.class, required = false)
    protected JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> communicationBarringProfile7;
    @XmlElementRef(name = "communicationBarringProfile8", type = JAXBElement.class, required = false)
    protected JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> communicationBarringProfile8;
    @XmlElementRef(name = "communicationBarringProfile9", type = JAXBElement.class, required = false)
    protected JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> communicationBarringProfile9;
    @XmlElementRef(name = "networkTranslationIndex", type = JAXBElement.class, required = false)
    protected JAXBElement<String> networkTranslationIndex;
    @XmlElementRef(name = "callProcessingPolicyProfileName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callProcessingPolicyProfileName;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der newName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewName() {
        return newName;
    }

    /**
     * Legt den Wert der newName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewName(String value) {
        this.newName = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile0-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> getCommunicationBarringProfile0() {
        return communicationBarringProfile0;
    }

    /**
     * Legt den Wert der communicationBarringProfile0-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public void setCommunicationBarringProfile0(JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> value) {
        this.communicationBarringProfile0 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile1-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> getCommunicationBarringProfile1() {
        return communicationBarringProfile1;
    }

    /**
     * Legt den Wert der communicationBarringProfile1-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public void setCommunicationBarringProfile1(JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> value) {
        this.communicationBarringProfile1 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile2-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> getCommunicationBarringProfile2() {
        return communicationBarringProfile2;
    }

    /**
     * Legt den Wert der communicationBarringProfile2-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public void setCommunicationBarringProfile2(JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> value) {
        this.communicationBarringProfile2 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile3-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> getCommunicationBarringProfile3() {
        return communicationBarringProfile3;
    }

    /**
     * Legt den Wert der communicationBarringProfile3-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public void setCommunicationBarringProfile3(JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> value) {
        this.communicationBarringProfile3 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile4-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> getCommunicationBarringProfile4() {
        return communicationBarringProfile4;
    }

    /**
     * Legt den Wert der communicationBarringProfile4-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public void setCommunicationBarringProfile4(JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> value) {
        this.communicationBarringProfile4 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile5-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> getCommunicationBarringProfile5() {
        return communicationBarringProfile5;
    }

    /**
     * Legt den Wert der communicationBarringProfile5-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public void setCommunicationBarringProfile5(JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> value) {
        this.communicationBarringProfile5 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile6-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> getCommunicationBarringProfile6() {
        return communicationBarringProfile6;
    }

    /**
     * Legt den Wert der communicationBarringProfile6-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public void setCommunicationBarringProfile6(JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> value) {
        this.communicationBarringProfile6 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile7-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> getCommunicationBarringProfile7() {
        return communicationBarringProfile7;
    }

    /**
     * Legt den Wert der communicationBarringProfile7-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public void setCommunicationBarringProfile7(JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> value) {
        this.communicationBarringProfile7 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile8-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> getCommunicationBarringProfile8() {
        return communicationBarringProfile8;
    }

    /**
     * Legt den Wert der communicationBarringProfile8-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public void setCommunicationBarringProfile8(JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> value) {
        this.communicationBarringProfile8 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile9-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> getCommunicationBarringProfile9() {
        return communicationBarringProfile9;
    }

    /**
     * Legt den Wert der communicationBarringProfile9-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link NetworkClassOfServiceCommunicationBarringProfile }{@code >}
     *     
     */
    public void setCommunicationBarringProfile9(JAXBElement<NetworkClassOfServiceCommunicationBarringProfile> value) {
        this.communicationBarringProfile9 = value;
    }

    /**
     * Ruft den Wert der networkTranslationIndex-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNetworkTranslationIndex() {
        return networkTranslationIndex;
    }

    /**
     * Legt den Wert der networkTranslationIndex-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNetworkTranslationIndex(JAXBElement<String> value) {
        this.networkTranslationIndex = value;
    }

    /**
     * Ruft den Wert der callProcessingPolicyProfileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallProcessingPolicyProfileName() {
        return callProcessingPolicyProfileName;
    }

    /**
     * Legt den Wert der callProcessingPolicyProfileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallProcessingPolicyProfileName(JAXBElement<String> value) {
        this.callProcessingPolicyProfileName = value;
    }

}
