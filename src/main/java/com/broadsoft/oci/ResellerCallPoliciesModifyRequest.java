//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the reseller level data associated with Call Policies.
 *         The following elements are only used in AS data mode:
 *           forceRedirectingUserIdentityForRedirectedCalls
 *           applyRedirectingUserIdentityToNetworkLocations
 *           
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ResellerCallPoliciesModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerCallPoliciesModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="resellerId" type="{}ResellerId22"/>
 *         <element name="forceRedirectingUserIdentityForRedirectedCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="applyRedirectingUserIdentityToNetworkLocations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerCallPoliciesModifyRequest", propOrder = {
    "resellerId",
    "forceRedirectingUserIdentityForRedirectedCalls",
    "applyRedirectingUserIdentityToNetworkLocations"
})
public class ResellerCallPoliciesModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    protected Boolean forceRedirectingUserIdentityForRedirectedCalls;
    protected Boolean applyRedirectingUserIdentityToNetworkLocations;

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Ruft den Wert der forceRedirectingUserIdentityForRedirectedCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceRedirectingUserIdentityForRedirectedCalls() {
        return forceRedirectingUserIdentityForRedirectedCalls;
    }

    /**
     * Legt den Wert der forceRedirectingUserIdentityForRedirectedCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceRedirectingUserIdentityForRedirectedCalls(Boolean value) {
        this.forceRedirectingUserIdentityForRedirectedCalls = value;
    }

    /**
     * Ruft den Wert der applyRedirectingUserIdentityToNetworkLocations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isApplyRedirectingUserIdentityToNetworkLocations() {
        return applyRedirectingUserIdentityToNetworkLocations;
    }

    /**
     * Legt den Wert der applyRedirectingUserIdentityToNetworkLocations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setApplyRedirectingUserIdentityToNetworkLocations(Boolean value) {
        this.applyRedirectingUserIdentityToNetworkLocations = value;
    }

}
