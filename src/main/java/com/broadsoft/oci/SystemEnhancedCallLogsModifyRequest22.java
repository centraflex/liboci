//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with Enhanced Call Logs.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemEnhancedCallLogsModifyRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemEnhancedCallLogsModifyRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="maxNonPagedResponseSize" type="{}EnhancedCallLogsNonPagedResponseSize" minOccurs="0"/>
 *         <element name="eclQueryApplicationURL" type="{}URL" minOccurs="0"/>
 *         <element name="eclQueryDataRepositoryURL" type="{}URL" minOccurs="0"/>
 *         <element name="defaultSchema" type="{}DbSchemaInstanceName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemEnhancedCallLogsModifyRequest22", propOrder = {
    "maxNonPagedResponseSize",
    "eclQueryApplicationURL",
    "eclQueryDataRepositoryURL",
    "defaultSchema"
})
public class SystemEnhancedCallLogsModifyRequest22
    extends OCIRequest
{

    protected Integer maxNonPagedResponseSize;
    @XmlElementRef(name = "eclQueryApplicationURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eclQueryApplicationURL;
    @XmlElementRef(name = "eclQueryDataRepositoryURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eclQueryDataRepositoryURL;
    @XmlElementRef(name = "defaultSchema", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultSchema;

    /**
     * Ruft den Wert der maxNonPagedResponseSize-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxNonPagedResponseSize() {
        return maxNonPagedResponseSize;
    }

    /**
     * Legt den Wert der maxNonPagedResponseSize-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxNonPagedResponseSize(Integer value) {
        this.maxNonPagedResponseSize = value;
    }

    /**
     * Ruft den Wert der eclQueryApplicationURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEclQueryApplicationURL() {
        return eclQueryApplicationURL;
    }

    /**
     * Legt den Wert der eclQueryApplicationURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEclQueryApplicationURL(JAXBElement<String> value) {
        this.eclQueryApplicationURL = value;
    }

    /**
     * Ruft den Wert der eclQueryDataRepositoryURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEclQueryDataRepositoryURL() {
        return eclQueryDataRepositoryURL;
    }

    /**
     * Legt den Wert der eclQueryDataRepositoryURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEclQueryDataRepositoryURL(JAXBElement<String> value) {
        this.eclQueryDataRepositoryURL = value;
    }

    /**
     * Ruft den Wert der defaultSchema-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultSchema() {
        return defaultSchema;
    }

    /**
     * Legt den Wert der defaultSchema-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultSchema(JAXBElement<String> value) {
        this.defaultSchema = value;
    }

}
