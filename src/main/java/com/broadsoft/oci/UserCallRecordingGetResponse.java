//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserCallRecordingGetRequest.
 *         The response contains the user's Call Recording option information.
 *       
 * 
 * <p>Java-Klasse für UserCallRecordingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallRecordingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="recordingOption" type="{}RecordingOption"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallRecordingGetResponse", propOrder = {
    "recordingOption"
})
public class UserCallRecordingGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected RecordingOption recordingOption;

    /**
     * Ruft den Wert der recordingOption-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RecordingOption }
     *     
     */
    public RecordingOption getRecordingOption() {
        return recordingOption;
    }

    /**
     * Legt den Wert der recordingOption-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RecordingOption }
     *     
     */
    public void setRecordingOption(RecordingOption value) {
        this.recordingOption = value;
    }

}
