//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *           Criteria for searching for a particular Dn activation state.
 *         
 * 
 * <p>Java-Klasse für SearchCriteriaExactDnActivation complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactDnActivation">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="activated" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactDnActivation", propOrder = {
    "activated"
})
public class SearchCriteriaExactDnActivation
    extends SearchCriteria
{

    protected boolean activated;

    /**
     * Ruft den Wert der activated-Eigenschaft ab.
     * 
     */
    public boolean isActivated() {
        return activated;
    }

    /**
     * Legt den Wert der activated-Eigenschaft fest.
     * 
     */
    public void setActivated(boolean value) {
        this.activated = value;
    }

}
