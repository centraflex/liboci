//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für LoginType22.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="LoginType22">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="System"/>
 *     <enumeration value="Provisioning"/>
 *     <enumeration value="Reseller"/>
 *     <enumeration value="Service Provider"/>
 *     <enumeration value="Group"/>
 *     <enumeration value="Group Department"/>
 *     <enumeration value="User"/>
 *     <enumeration value="Lawful Intercept"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "LoginType22")
@XmlEnum
public enum LoginType22 {

    @XmlEnumValue("System")
    SYSTEM("System"),
    @XmlEnumValue("Provisioning")
    PROVISIONING("Provisioning"),
    @XmlEnumValue("Reseller")
    RESELLER("Reseller"),
    @XmlEnumValue("Service Provider")
    SERVICE_PROVIDER("Service Provider"),
    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("Group Department")
    GROUP_DEPARTMENT("Group Department"),
    @XmlEnumValue("User")
    USER("User"),
    @XmlEnumValue("Lawful Intercept")
    LAWFUL_INTERCEPT("Lawful Intercept");
    private final String value;

    LoginType22(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LoginType22 fromValue(String v) {
        for (LoginType22 c: LoginType22 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
