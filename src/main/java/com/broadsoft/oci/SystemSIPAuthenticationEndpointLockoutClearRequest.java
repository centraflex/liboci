//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *          Request to clear sip authentication endpoint lockouts in the system.
 *          The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemSIPAuthenticationEndpointLockoutClearRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPAuthenticationEndpointLockoutClearRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userEndpointKey" type="{}UserEndpointKey" maxOccurs="unbounded"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPAuthenticationEndpointLockoutClearRequest", propOrder = {
    "userEndpointKey"
})
public class SystemSIPAuthenticationEndpointLockoutClearRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    protected List<UserEndpointKey> userEndpointKey;

    /**
     * Gets the value of the userEndpointKey property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the userEndpointKey property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserEndpointKey().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserEndpointKey }
     * 
     * 
     * @return
     *     The value of the userEndpointKey property.
     */
    public List<UserEndpointKey> getUserEndpointKey() {
        if (userEndpointKey == null) {
            userEndpointKey = new ArrayList<>();
        }
        return this.userEndpointKey;
    }

}
