//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ExtendedTransportProtocol.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ExtendedTransportProtocol">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="UDP"/>
 *     <enumeration value="TCP"/>
 *     <enumeration value="TLS"/>
 *     <enumeration value="Unspecified"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ExtendedTransportProtocol")
@XmlEnum
public enum ExtendedTransportProtocol {

    UDP("UDP"),
    TCP("TCP"),
    TLS("TLS"),
    @XmlEnumValue("Unspecified")
    UNSPECIFIED("Unspecified");
    private final String value;

    ExtendedTransportProtocol(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExtendedTransportProtocol fromValue(String v) {
        for (ExtendedTransportProtocol c: ExtendedTransportProtocol.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
