//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *          Response to a UserBroadWorksMobilityMobileIdentityGetRequest.
 *          
 *          Replaced by: UserBroadWorksMobilityMobileIdentityGetResponse21sp1.
 *       
 * 
 * <p>Java-Klasse für UserBroadWorksMobilityMobileIdentityGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksMobilityMobileIdentityGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="description" type="{}BroadWorksMobilityUserMobileIdentityDescription" minOccurs="0"/>
 *         <element name="isPrimary" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableAlerting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="alertAgentCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="alertClickToDialCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="alertGroupPagingCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useMobilityCallingLineID" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableDiversionInhibitor" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="requireAnswerConfirmation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="broadworksCallControl" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useSettingLevel" type="{}BroadWorksMobilityUserSettingLevel"/>
 *         <element name="denyCallOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="denyCallTerminations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="devicesToRing" type="{}BroadWorksMobilityPhoneToRing"/>
 *         <element name="includeSharedCallAppearance" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeBroadworksAnywhere" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeExecutiveAssistant" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="mobileNumberAlerted" type="{}DN" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="enableCallAnchoring" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="timeSchedule" type="{}ScheduleGlobalKey" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}ScheduleGlobalKey" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksMobilityMobileIdentityGetResponse", propOrder = {
    "description",
    "isPrimary",
    "enableAlerting",
    "alertAgentCalls",
    "alertClickToDialCalls",
    "alertGroupPagingCalls",
    "useMobilityCallingLineID",
    "enableDiversionInhibitor",
    "requireAnswerConfirmation",
    "broadworksCallControl",
    "useSettingLevel",
    "denyCallOriginations",
    "denyCallTerminations",
    "devicesToRing",
    "includeSharedCallAppearance",
    "includeBroadworksAnywhere",
    "includeExecutiveAssistant",
    "mobileNumberAlerted",
    "enableCallAnchoring",
    "timeSchedule",
    "holidaySchedule"
})
public class UserBroadWorksMobilityMobileIdentityGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    protected boolean isPrimary;
    protected boolean enableAlerting;
    protected boolean alertAgentCalls;
    protected boolean alertClickToDialCalls;
    protected boolean alertGroupPagingCalls;
    protected boolean useMobilityCallingLineID;
    protected boolean enableDiversionInhibitor;
    protected boolean requireAnswerConfirmation;
    protected boolean broadworksCallControl;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BroadWorksMobilityUserSettingLevel useSettingLevel;
    protected boolean denyCallOriginations;
    protected boolean denyCallTerminations;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BroadWorksMobilityPhoneToRing devicesToRing;
    protected boolean includeSharedCallAppearance;
    protected boolean includeBroadworksAnywhere;
    protected boolean includeExecutiveAssistant;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> mobileNumberAlerted;
    protected boolean enableCallAnchoring;
    protected ScheduleGlobalKey timeSchedule;
    protected ScheduleGlobalKey holidaySchedule;

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der isPrimary-Eigenschaft ab.
     * 
     */
    public boolean isIsPrimary() {
        return isPrimary;
    }

    /**
     * Legt den Wert der isPrimary-Eigenschaft fest.
     * 
     */
    public void setIsPrimary(boolean value) {
        this.isPrimary = value;
    }

    /**
     * Ruft den Wert der enableAlerting-Eigenschaft ab.
     * 
     */
    public boolean isEnableAlerting() {
        return enableAlerting;
    }

    /**
     * Legt den Wert der enableAlerting-Eigenschaft fest.
     * 
     */
    public void setEnableAlerting(boolean value) {
        this.enableAlerting = value;
    }

    /**
     * Ruft den Wert der alertAgentCalls-Eigenschaft ab.
     * 
     */
    public boolean isAlertAgentCalls() {
        return alertAgentCalls;
    }

    /**
     * Legt den Wert der alertAgentCalls-Eigenschaft fest.
     * 
     */
    public void setAlertAgentCalls(boolean value) {
        this.alertAgentCalls = value;
    }

    /**
     * Ruft den Wert der alertClickToDialCalls-Eigenschaft ab.
     * 
     */
    public boolean isAlertClickToDialCalls() {
        return alertClickToDialCalls;
    }

    /**
     * Legt den Wert der alertClickToDialCalls-Eigenschaft fest.
     * 
     */
    public void setAlertClickToDialCalls(boolean value) {
        this.alertClickToDialCalls = value;
    }

    /**
     * Ruft den Wert der alertGroupPagingCalls-Eigenschaft ab.
     * 
     */
    public boolean isAlertGroupPagingCalls() {
        return alertGroupPagingCalls;
    }

    /**
     * Legt den Wert der alertGroupPagingCalls-Eigenschaft fest.
     * 
     */
    public void setAlertGroupPagingCalls(boolean value) {
        this.alertGroupPagingCalls = value;
    }

    /**
     * Ruft den Wert der useMobilityCallingLineID-Eigenschaft ab.
     * 
     */
    public boolean isUseMobilityCallingLineID() {
        return useMobilityCallingLineID;
    }

    /**
     * Legt den Wert der useMobilityCallingLineID-Eigenschaft fest.
     * 
     */
    public void setUseMobilityCallingLineID(boolean value) {
        this.useMobilityCallingLineID = value;
    }

    /**
     * Ruft den Wert der enableDiversionInhibitor-Eigenschaft ab.
     * 
     */
    public boolean isEnableDiversionInhibitor() {
        return enableDiversionInhibitor;
    }

    /**
     * Legt den Wert der enableDiversionInhibitor-Eigenschaft fest.
     * 
     */
    public void setEnableDiversionInhibitor(boolean value) {
        this.enableDiversionInhibitor = value;
    }

    /**
     * Ruft den Wert der requireAnswerConfirmation-Eigenschaft ab.
     * 
     */
    public boolean isRequireAnswerConfirmation() {
        return requireAnswerConfirmation;
    }

    /**
     * Legt den Wert der requireAnswerConfirmation-Eigenschaft fest.
     * 
     */
    public void setRequireAnswerConfirmation(boolean value) {
        this.requireAnswerConfirmation = value;
    }

    /**
     * Ruft den Wert der broadworksCallControl-Eigenschaft ab.
     * 
     */
    public boolean isBroadworksCallControl() {
        return broadworksCallControl;
    }

    /**
     * Legt den Wert der broadworksCallControl-Eigenschaft fest.
     * 
     */
    public void setBroadworksCallControl(boolean value) {
        this.broadworksCallControl = value;
    }

    /**
     * Ruft den Wert der useSettingLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksMobilityUserSettingLevel }
     *     
     */
    public BroadWorksMobilityUserSettingLevel getUseSettingLevel() {
        return useSettingLevel;
    }

    /**
     * Legt den Wert der useSettingLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksMobilityUserSettingLevel }
     *     
     */
    public void setUseSettingLevel(BroadWorksMobilityUserSettingLevel value) {
        this.useSettingLevel = value;
    }

    /**
     * Ruft den Wert der denyCallOriginations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallOriginations() {
        return denyCallOriginations;
    }

    /**
     * Legt den Wert der denyCallOriginations-Eigenschaft fest.
     * 
     */
    public void setDenyCallOriginations(boolean value) {
        this.denyCallOriginations = value;
    }

    /**
     * Ruft den Wert der denyCallTerminations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallTerminations() {
        return denyCallTerminations;
    }

    /**
     * Legt den Wert der denyCallTerminations-Eigenschaft fest.
     * 
     */
    public void setDenyCallTerminations(boolean value) {
        this.denyCallTerminations = value;
    }

    /**
     * Ruft den Wert der devicesToRing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksMobilityPhoneToRing }
     *     
     */
    public BroadWorksMobilityPhoneToRing getDevicesToRing() {
        return devicesToRing;
    }

    /**
     * Legt den Wert der devicesToRing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksMobilityPhoneToRing }
     *     
     */
    public void setDevicesToRing(BroadWorksMobilityPhoneToRing value) {
        this.devicesToRing = value;
    }

    /**
     * Ruft den Wert der includeSharedCallAppearance-Eigenschaft ab.
     * 
     */
    public boolean isIncludeSharedCallAppearance() {
        return includeSharedCallAppearance;
    }

    /**
     * Legt den Wert der includeSharedCallAppearance-Eigenschaft fest.
     * 
     */
    public void setIncludeSharedCallAppearance(boolean value) {
        this.includeSharedCallAppearance = value;
    }

    /**
     * Ruft den Wert der includeBroadworksAnywhere-Eigenschaft ab.
     * 
     */
    public boolean isIncludeBroadworksAnywhere() {
        return includeBroadworksAnywhere;
    }

    /**
     * Legt den Wert der includeBroadworksAnywhere-Eigenschaft fest.
     * 
     */
    public void setIncludeBroadworksAnywhere(boolean value) {
        this.includeBroadworksAnywhere = value;
    }

    /**
     * Ruft den Wert der includeExecutiveAssistant-Eigenschaft ab.
     * 
     */
    public boolean isIncludeExecutiveAssistant() {
        return includeExecutiveAssistant;
    }

    /**
     * Legt den Wert der includeExecutiveAssistant-Eigenschaft fest.
     * 
     */
    public void setIncludeExecutiveAssistant(boolean value) {
        this.includeExecutiveAssistant = value;
    }

    /**
     * Gets the value of the mobileNumberAlerted property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the mobileNumberAlerted property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMobileNumberAlerted().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the mobileNumberAlerted property.
     */
    public List<String> getMobileNumberAlerted() {
        if (mobileNumberAlerted == null) {
            mobileNumberAlerted = new ArrayList<>();
        }
        return this.mobileNumberAlerted;
    }

    /**
     * Ruft den Wert der enableCallAnchoring-Eigenschaft ab.
     * 
     */
    public boolean isEnableCallAnchoring() {
        return enableCallAnchoring;
    }

    /**
     * Legt den Wert der enableCallAnchoring-Eigenschaft fest.
     * 
     */
    public void setEnableCallAnchoring(boolean value) {
        this.enableCallAnchoring = value;
    }

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ScheduleGlobalKey }
     *     
     */
    public ScheduleGlobalKey getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ScheduleGlobalKey }
     *     
     */
    public void setTimeSchedule(ScheduleGlobalKey value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ScheduleGlobalKey }
     *     
     */
    public ScheduleGlobalKey getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ScheduleGlobalKey }
     *     
     */
    public void setHolidaySchedule(ScheduleGlobalKey value) {
        this.holidaySchedule = value;
    }

}
