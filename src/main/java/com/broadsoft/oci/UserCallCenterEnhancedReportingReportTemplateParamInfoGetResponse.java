//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserCallCenterEnhancedReportingReportTemplateParamInfoGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserCallCenterEnhancedReportingReportTemplateParamInfoGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallCenterEnhancedReportingReportTemplateParamInfoGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isRealtimeReport" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="requireAgentParam" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="requireCallCenterParam" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="requireCallCenterDnisParam" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="requireSamplingPeriodParam" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="callCompletionThresholdParam" type="{}CallCenterReportInputParameterOption" minOccurs="0"/>
 *         <element name="shortDurationThresholdParam" type="{}CallCenterReportInputParameterOption" minOccurs="0"/>
 *         <element name="serviceLevelThresholdParam" type="{}CallCenterReportInputParameterOption" minOccurs="0"/>
 *         <element name="serviceLevelInclusionsParam" type="{}CallCenterReportInputParameterOption" minOccurs="0"/>
 *         <element name="serviceLevelObjectiveThresholdParam" type="{}CallCenterReportInputParameterOption" minOccurs="0"/>
 *         <element name="abandonedCallThresholdParam" type="{}CallCenterReportInputParameterOption" minOccurs="0"/>
 *         <element name="serviceLevelThresholdParamNumber" type="{}CallCenterReportServiceLevelInputParameterNumber" minOccurs="0"/>
 *         <element name="abandonedCallThresholdParamNumber" type="{}CallCenterReportAbandonedCallInputParameterNumber" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallCenterEnhancedReportingReportTemplateParamInfoGetResponse", propOrder = {
    "isRealtimeReport",
    "requireAgentParam",
    "requireCallCenterParam",
    "requireCallCenterDnisParam",
    "requireSamplingPeriodParam",
    "callCompletionThresholdParam",
    "shortDurationThresholdParam",
    "serviceLevelThresholdParam",
    "serviceLevelInclusionsParam",
    "serviceLevelObjectiveThresholdParam",
    "abandonedCallThresholdParam",
    "serviceLevelThresholdParamNumber",
    "abandonedCallThresholdParamNumber"
})
public class UserCallCenterEnhancedReportingReportTemplateParamInfoGetResponse
    extends OCIDataResponse
{

    protected boolean isRealtimeReport;
    protected boolean requireAgentParam;
    protected boolean requireCallCenterParam;
    protected boolean requireCallCenterDnisParam;
    protected boolean requireSamplingPeriodParam;
    @XmlSchemaType(name = "token")
    protected CallCenterReportInputParameterOption callCompletionThresholdParam;
    @XmlSchemaType(name = "token")
    protected CallCenterReportInputParameterOption shortDurationThresholdParam;
    @XmlSchemaType(name = "token")
    protected CallCenterReportInputParameterOption serviceLevelThresholdParam;
    @XmlSchemaType(name = "token")
    protected CallCenterReportInputParameterOption serviceLevelInclusionsParam;
    @XmlSchemaType(name = "token")
    protected CallCenterReportInputParameterOption serviceLevelObjectiveThresholdParam;
    @XmlSchemaType(name = "token")
    protected CallCenterReportInputParameterOption abandonedCallThresholdParam;
    protected Integer serviceLevelThresholdParamNumber;
    protected Integer abandonedCallThresholdParamNumber;

    /**
     * Ruft den Wert der isRealtimeReport-Eigenschaft ab.
     * 
     */
    public boolean isIsRealtimeReport() {
        return isRealtimeReport;
    }

    /**
     * Legt den Wert der isRealtimeReport-Eigenschaft fest.
     * 
     */
    public void setIsRealtimeReport(boolean value) {
        this.isRealtimeReport = value;
    }

    /**
     * Ruft den Wert der requireAgentParam-Eigenschaft ab.
     * 
     */
    public boolean isRequireAgentParam() {
        return requireAgentParam;
    }

    /**
     * Legt den Wert der requireAgentParam-Eigenschaft fest.
     * 
     */
    public void setRequireAgentParam(boolean value) {
        this.requireAgentParam = value;
    }

    /**
     * Ruft den Wert der requireCallCenterParam-Eigenschaft ab.
     * 
     */
    public boolean isRequireCallCenterParam() {
        return requireCallCenterParam;
    }

    /**
     * Legt den Wert der requireCallCenterParam-Eigenschaft fest.
     * 
     */
    public void setRequireCallCenterParam(boolean value) {
        this.requireCallCenterParam = value;
    }

    /**
     * Ruft den Wert der requireCallCenterDnisParam-Eigenschaft ab.
     * 
     */
    public boolean isRequireCallCenterDnisParam() {
        return requireCallCenterDnisParam;
    }

    /**
     * Legt den Wert der requireCallCenterDnisParam-Eigenschaft fest.
     * 
     */
    public void setRequireCallCenterDnisParam(boolean value) {
        this.requireCallCenterDnisParam = value;
    }

    /**
     * Ruft den Wert der requireSamplingPeriodParam-Eigenschaft ab.
     * 
     */
    public boolean isRequireSamplingPeriodParam() {
        return requireSamplingPeriodParam;
    }

    /**
     * Legt den Wert der requireSamplingPeriodParam-Eigenschaft fest.
     * 
     */
    public void setRequireSamplingPeriodParam(boolean value) {
        this.requireSamplingPeriodParam = value;
    }

    /**
     * Ruft den Wert der callCompletionThresholdParam-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public CallCenterReportInputParameterOption getCallCompletionThresholdParam() {
        return callCompletionThresholdParam;
    }

    /**
     * Legt den Wert der callCompletionThresholdParam-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public void setCallCompletionThresholdParam(CallCenterReportInputParameterOption value) {
        this.callCompletionThresholdParam = value;
    }

    /**
     * Ruft den Wert der shortDurationThresholdParam-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public CallCenterReportInputParameterOption getShortDurationThresholdParam() {
        return shortDurationThresholdParam;
    }

    /**
     * Legt den Wert der shortDurationThresholdParam-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public void setShortDurationThresholdParam(CallCenterReportInputParameterOption value) {
        this.shortDurationThresholdParam = value;
    }

    /**
     * Ruft den Wert der serviceLevelThresholdParam-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public CallCenterReportInputParameterOption getServiceLevelThresholdParam() {
        return serviceLevelThresholdParam;
    }

    /**
     * Legt den Wert der serviceLevelThresholdParam-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public void setServiceLevelThresholdParam(CallCenterReportInputParameterOption value) {
        this.serviceLevelThresholdParam = value;
    }

    /**
     * Ruft den Wert der serviceLevelInclusionsParam-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public CallCenterReportInputParameterOption getServiceLevelInclusionsParam() {
        return serviceLevelInclusionsParam;
    }

    /**
     * Legt den Wert der serviceLevelInclusionsParam-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public void setServiceLevelInclusionsParam(CallCenterReportInputParameterOption value) {
        this.serviceLevelInclusionsParam = value;
    }

    /**
     * Ruft den Wert der serviceLevelObjectiveThresholdParam-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public CallCenterReportInputParameterOption getServiceLevelObjectiveThresholdParam() {
        return serviceLevelObjectiveThresholdParam;
    }

    /**
     * Legt den Wert der serviceLevelObjectiveThresholdParam-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public void setServiceLevelObjectiveThresholdParam(CallCenterReportInputParameterOption value) {
        this.serviceLevelObjectiveThresholdParam = value;
    }

    /**
     * Ruft den Wert der abandonedCallThresholdParam-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public CallCenterReportInputParameterOption getAbandonedCallThresholdParam() {
        return abandonedCallThresholdParam;
    }

    /**
     * Legt den Wert der abandonedCallThresholdParam-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public void setAbandonedCallThresholdParam(CallCenterReportInputParameterOption value) {
        this.abandonedCallThresholdParam = value;
    }

    /**
     * Ruft den Wert der serviceLevelThresholdParamNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceLevelThresholdParamNumber() {
        return serviceLevelThresholdParamNumber;
    }

    /**
     * Legt den Wert der serviceLevelThresholdParamNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceLevelThresholdParamNumber(Integer value) {
        this.serviceLevelThresholdParamNumber = value;
    }

    /**
     * Ruft den Wert der abandonedCallThresholdParamNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAbandonedCallThresholdParamNumber() {
        return abandonedCallThresholdParamNumber;
    }

    /**
     * Legt den Wert der abandonedCallThresholdParamNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAbandonedCallThresholdParamNumber(Integer value) {
        this.abandonedCallThresholdParamNumber = value;
    }

}
