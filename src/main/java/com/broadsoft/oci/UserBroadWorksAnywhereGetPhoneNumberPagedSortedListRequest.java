//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Get the list of all the BroadWorks Anywhere phone numbers for the user.
 *         If no sortOrder is included the response is sorted by Phone Number ascending by default. 
 *         The response is either a UserBroadWorksAnywhereGetPhoneNumberPagedSortedListResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserBroadWorksAnywhereGetPhoneNumberPagedSortedListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksAnywhereGetPhoneNumberPagedSortedListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl" minOccurs="0"/>
 *         <choice minOccurs="0">
 *           <element name="sortByLocation" type="{}SortByLocation"/>
 *           <element name="sortByEnabled" type="{}SortByEnabled"/>
 *         </choice>
 *         <element name="searchCriteriaLocation" type="{}SearchCriteriaLocation" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactLocationEnabled" type="{}SearchCriteriaExactLocationEnabled" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksAnywhereGetPhoneNumberPagedSortedListRequest", propOrder = {
    "userId",
    "responsePagingControl",
    "sortByLocation",
    "sortByEnabled",
    "searchCriteriaLocation",
    "searchCriteriaExactLocationEnabled"
})
public class UserBroadWorksAnywhereGetPhoneNumberPagedSortedListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected ResponsePagingControl responsePagingControl;
    protected SortByLocation sortByLocation;
    protected SortByEnabled sortByEnabled;
    protected List<SearchCriteriaLocation> searchCriteriaLocation;
    protected SearchCriteriaExactLocationEnabled searchCriteriaExactLocationEnabled;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Ruft den Wert der sortByLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByLocation }
     *     
     */
    public SortByLocation getSortByLocation() {
        return sortByLocation;
    }

    /**
     * Legt den Wert der sortByLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByLocation }
     *     
     */
    public void setSortByLocation(SortByLocation value) {
        this.sortByLocation = value;
    }

    /**
     * Ruft den Wert der sortByEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByEnabled }
     *     
     */
    public SortByEnabled getSortByEnabled() {
        return sortByEnabled;
    }

    /**
     * Legt den Wert der sortByEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByEnabled }
     *     
     */
    public void setSortByEnabled(SortByEnabled value) {
        this.sortByEnabled = value;
    }

    /**
     * Gets the value of the searchCriteriaLocation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaLocation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaLocation }
     * 
     * 
     * @return
     *     The value of the searchCriteriaLocation property.
     */
    public List<SearchCriteriaLocation> getSearchCriteriaLocation() {
        if (searchCriteriaLocation == null) {
            searchCriteriaLocation = new ArrayList<>();
        }
        return this.searchCriteriaLocation;
    }

    /**
     * Ruft den Wert der searchCriteriaExactLocationEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactLocationEnabled }
     *     
     */
    public SearchCriteriaExactLocationEnabled getSearchCriteriaExactLocationEnabled() {
        return searchCriteriaExactLocationEnabled;
    }

    /**
     * Legt den Wert der searchCriteriaExactLocationEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactLocationEnabled }
     *     
     */
    public void setSearchCriteriaExactLocationEnabled(SearchCriteriaExactLocationEnabled value) {
        this.searchCriteriaExactLocationEnabled = value;
    }

}
