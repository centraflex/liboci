//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the profile for a service provider or enterprise.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         The following elements are only used in Amplify data mode and ignored 
 *         in AS and XS data mode: 
 *         servicePolicy, 
 *         callProcessingSliceId, 
 *         provisioningSliceId, 
 *         subscriberPartition.
 *         When the callProcessingSliceId or provisioningSliceId is set to nillable, 
 *         it will be assigned to the default Slice.
 *         Only Provisioning admin and above can change the callProcessingSliceId, 
 *         provisioningSliceId, and subscriberPartition.
 *         
 *         The following element is only used in AS data mode and ignored 
 *         in Amplify and XS data mode: 
 *         resellerId
 *         resellerName
 *         
 *         resellerId and resellerName can only be configured by a Reseller or higher level administrator.
 * 
 * 	    The following behavior is only applicable in CloudPBX:
 * 	      - when existing resellerId is specified, enterprise/Service Provider shall be moved to
 * 	        requesting reseller.
 * 	      - when new resellerId, that does not exist in the system, is specified, the new 
 * 	        Reseller is created the given resellerId and resellerName (if provided) and 	enterprise/Service Provider is moved to the newly created Reseller.
 * 	    
 * 	    resellerName element is ignored if the reseller the service provider is being moved to 	already exists.
 * 
 *         
 *         The following elements are only used in Amplify and XS data mode and ignored in AS data mode:
 *         preferredDataCenter.
 *         Only Provisioning admin and above can change the preferredDataCenter.
 *         
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *    		 defaultExtensionLength 
 *    		 locationRoutingPrefixDigit
 *    		 locationCodeLength
 *         
 *       
 * 
 * <p>Java-Klasse für ServiceProviderModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="defaultDomain" type="{}NetAddress" minOccurs="0"/>
 *         <element name="serviceProviderName" type="{}ServiceProviderName" minOccurs="0"/>
 *         <element name="supportEmail" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="contact" type="{}Contact" minOccurs="0"/>
 *         <element name="address" type="{}StreetAddress" minOccurs="0"/>
 *         <element name="useServiceProviderLanguages" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="servicePolicy" type="{}ServicePolicyName" minOccurs="0"/>
 *         <element name="callProcessingSliceId" type="{}SliceId" minOccurs="0"/>
 *         <element name="provisioningSliceId" type="{}SliceId" minOccurs="0"/>
 *         <element name="subscriberPartition" type="{}SubscriberPartition" minOccurs="0"/>
 *         <element name="preferredDataCenter" type="{}DataCenter" minOccurs="0"/>
 *         <element name="defaultExtensionLength" type="{}ExtensionLength" minOccurs="0"/>
 *         <element name="locationRoutingPrefixDigit" type="{}LocationRoutingPrefixDigit" minOccurs="0"/>
 *         <element name="locationCodeLength" type="{}EnterpriseLocationDialingCodeLength" minOccurs="0"/>
 *         <element name="resellerId" type="{}ResellerId22" minOccurs="0"/>
 *         <element name="resellerName" type="{}ResellerName22" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderModifyRequest", propOrder = {
    "serviceProviderId",
    "defaultDomain",
    "serviceProviderName",
    "supportEmail",
    "contact",
    "address",
    "useServiceProviderLanguages",
    "servicePolicy",
    "callProcessingSliceId",
    "provisioningSliceId",
    "subscriberPartition",
    "preferredDataCenter",
    "defaultExtensionLength",
    "locationRoutingPrefixDigit",
    "locationCodeLength",
    "resellerId",
    "resellerName"
})
public class ServiceProviderModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultDomain;
    @XmlElementRef(name = "serviceProviderName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serviceProviderName;
    @XmlElementRef(name = "supportEmail", type = JAXBElement.class, required = false)
    protected JAXBElement<String> supportEmail;
    protected Contact contact;
    protected StreetAddress address;
    protected Boolean useServiceProviderLanguages;
    @XmlElementRef(name = "servicePolicy", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servicePolicy;
    @XmlElementRef(name = "callProcessingSliceId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callProcessingSliceId;
    @XmlElementRef(name = "provisioningSliceId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> provisioningSliceId;
    @XmlElementRef(name = "subscriberPartition", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subscriberPartition;
    @XmlElementRef(name = "preferredDataCenter", type = JAXBElement.class, required = false)
    protected JAXBElement<String> preferredDataCenter;
    @XmlElementRef(name = "defaultExtensionLength", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> defaultExtensionLength;
    @XmlElementRef(name = "locationRoutingPrefixDigit", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> locationRoutingPrefixDigit;
    @XmlElementRef(name = "locationCodeLength", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> locationCodeLength;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerName;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der defaultDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultDomain() {
        return defaultDomain;
    }

    /**
     * Legt den Wert der defaultDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultDomain(String value) {
        this.defaultDomain = value;
    }

    /**
     * Ruft den Wert der serviceProviderName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServiceProviderName() {
        return serviceProviderName;
    }

    /**
     * Legt den Wert der serviceProviderName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServiceProviderName(JAXBElement<String> value) {
        this.serviceProviderName = value;
    }

    /**
     * Ruft den Wert der supportEmail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSupportEmail() {
        return supportEmail;
    }

    /**
     * Legt den Wert der supportEmail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSupportEmail(JAXBElement<String> value) {
        this.supportEmail = value;
    }

    /**
     * Ruft den Wert der contact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Contact }
     *     
     */
    public Contact getContact() {
        return contact;
    }

    /**
     * Legt den Wert der contact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Contact }
     *     
     */
    public void setContact(Contact value) {
        this.contact = value;
    }

    /**
     * Ruft den Wert der address-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StreetAddress }
     *     
     */
    public StreetAddress getAddress() {
        return address;
    }

    /**
     * Legt den Wert der address-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetAddress }
     *     
     */
    public void setAddress(StreetAddress value) {
        this.address = value;
    }

    /**
     * Ruft den Wert der useServiceProviderLanguages-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseServiceProviderLanguages() {
        return useServiceProviderLanguages;
    }

    /**
     * Legt den Wert der useServiceProviderLanguages-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseServiceProviderLanguages(Boolean value) {
        this.useServiceProviderLanguages = value;
    }

    /**
     * Ruft den Wert der servicePolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServicePolicy() {
        return servicePolicy;
    }

    /**
     * Legt den Wert der servicePolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServicePolicy(JAXBElement<String> value) {
        this.servicePolicy = value;
    }

    /**
     * Ruft den Wert der callProcessingSliceId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallProcessingSliceId() {
        return callProcessingSliceId;
    }

    /**
     * Legt den Wert der callProcessingSliceId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallProcessingSliceId(JAXBElement<String> value) {
        this.callProcessingSliceId = value;
    }

    /**
     * Ruft den Wert der provisioningSliceId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProvisioningSliceId() {
        return provisioningSliceId;
    }

    /**
     * Legt den Wert der provisioningSliceId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProvisioningSliceId(JAXBElement<String> value) {
        this.provisioningSliceId = value;
    }

    /**
     * Ruft den Wert der subscriberPartition-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubscriberPartition() {
        return subscriberPartition;
    }

    /**
     * Legt den Wert der subscriberPartition-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubscriberPartition(JAXBElement<String> value) {
        this.subscriberPartition = value;
    }

    /**
     * Ruft den Wert der preferredDataCenter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPreferredDataCenter() {
        return preferredDataCenter;
    }

    /**
     * Legt den Wert der preferredDataCenter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPreferredDataCenter(JAXBElement<String> value) {
        this.preferredDataCenter = value;
    }

    /**
     * Ruft den Wert der defaultExtensionLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getDefaultExtensionLength() {
        return defaultExtensionLength;
    }

    /**
     * Legt den Wert der defaultExtensionLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setDefaultExtensionLength(JAXBElement<Integer> value) {
        this.defaultExtensionLength = value;
    }

    /**
     * Ruft den Wert der locationRoutingPrefixDigit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getLocationRoutingPrefixDigit() {
        return locationRoutingPrefixDigit;
    }

    /**
     * Legt den Wert der locationRoutingPrefixDigit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setLocationRoutingPrefixDigit(JAXBElement<Integer> value) {
        this.locationRoutingPrefixDigit = value;
    }

    /**
     * Ruft den Wert der locationCodeLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getLocationCodeLength() {
        return locationCodeLength;
    }

    /**
     * Legt den Wert der locationCodeLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setLocationCodeLength(JAXBElement<Integer> value) {
        this.locationCodeLength = value;
    }

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Ruft den Wert der resellerName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerName() {
        return resellerName;
    }

    /**
     * Legt den Wert der resellerName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerName(String value) {
        this.resellerName = value;
    }

}
