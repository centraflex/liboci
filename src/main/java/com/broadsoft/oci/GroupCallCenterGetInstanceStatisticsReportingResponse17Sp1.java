//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupCallCenterGetInstanceStatisticsReportingRequest17sp1.
 *         Contains Call Center statistics reporting settings.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterGetInstanceStatisticsReportingResponse17sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterGetInstanceStatisticsReportingResponse17sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="generateDailyReport" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="collectionPeriodMinutes" type="{}CallCenterStatisticsCollectionPeriodMinutes"/>
 *         <element name="reportingEmailAddress1" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="reportingEmailAddress2" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="statisticsSource" type="{}CallCenterStatisticsSource17sp1"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterGetInstanceStatisticsReportingResponse17sp1", propOrder = {
    "generateDailyReport",
    "collectionPeriodMinutes",
    "reportingEmailAddress1",
    "reportingEmailAddress2",
    "statisticsSource"
})
public class GroupCallCenterGetInstanceStatisticsReportingResponse17Sp1
    extends OCIDataResponse
{

    protected boolean generateDailyReport;
    protected int collectionPeriodMinutes;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String reportingEmailAddress1;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String reportingEmailAddress2;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterStatisticsSource17Sp1 statisticsSource;

    /**
     * Ruft den Wert der generateDailyReport-Eigenschaft ab.
     * 
     */
    public boolean isGenerateDailyReport() {
        return generateDailyReport;
    }

    /**
     * Legt den Wert der generateDailyReport-Eigenschaft fest.
     * 
     */
    public void setGenerateDailyReport(boolean value) {
        this.generateDailyReport = value;
    }

    /**
     * Ruft den Wert der collectionPeriodMinutes-Eigenschaft ab.
     * 
     */
    public int getCollectionPeriodMinutes() {
        return collectionPeriodMinutes;
    }

    /**
     * Legt den Wert der collectionPeriodMinutes-Eigenschaft fest.
     * 
     */
    public void setCollectionPeriodMinutes(int value) {
        this.collectionPeriodMinutes = value;
    }

    /**
     * Ruft den Wert der reportingEmailAddress1-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingEmailAddress1() {
        return reportingEmailAddress1;
    }

    /**
     * Legt den Wert der reportingEmailAddress1-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingEmailAddress1(String value) {
        this.reportingEmailAddress1 = value;
    }

    /**
     * Ruft den Wert der reportingEmailAddress2-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingEmailAddress2() {
        return reportingEmailAddress2;
    }

    /**
     * Legt den Wert der reportingEmailAddress2-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingEmailAddress2(String value) {
        this.reportingEmailAddress2 = value;
    }

    /**
     * Ruft den Wert der statisticsSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterStatisticsSource17Sp1 }
     *     
     */
    public CallCenterStatisticsSource17Sp1 getStatisticsSource() {
        return statisticsSource;
    }

    /**
     * Legt den Wert der statisticsSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterStatisticsSource17Sp1 }
     *     
     */
    public void setStatisticsSource(CallCenterStatisticsSource17Sp1 value) {
        this.statisticsSource = value;
    }

}
