//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemAutomaticCallbackGetRequest17.
 *       
 * 
 * <p>Java-Klasse für SystemAutomaticCallbackGetResponse17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAutomaticCallbackGetResponse17">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="monitorMinutes" type="{}AutomaticCallbackMonitorMinutes"/>
 *         <element name="maxMonitorsPerOriginator" type="{}AutomaticCallbackMaxMonitorsPerOriginator"/>
 *         <element name="maxCallbackRings" type="{}AutomaticCallbackMaxCallbackRings"/>
 *         <element name="maxMonitorsPerTerminator" type="{}AutomaticCallbackMaxMonitorsPerTerminator"/>
 *         <element name="terminatorIdleGuardSeconds" type="{}AutomaticCallbackTerminatorIdleGuardSeconds"/>
 *         <element name="callbackMethod" type="{}AutomaticCallbackMethod"/>
 *         <element name="pollingIntervalSeconds" type="{}AutomaticCallbackPollingIntervalSeconds"/>
 *         <element name="activationDigit" type="{}AutomaticCallbackActivationDigit"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAutomaticCallbackGetResponse17", propOrder = {
    "monitorMinutes",
    "maxMonitorsPerOriginator",
    "maxCallbackRings",
    "maxMonitorsPerTerminator",
    "terminatorIdleGuardSeconds",
    "callbackMethod",
    "pollingIntervalSeconds",
    "activationDigit"
})
public class SystemAutomaticCallbackGetResponse17
    extends OCIDataResponse
{

    protected int monitorMinutes;
    protected int maxMonitorsPerOriginator;
    protected int maxCallbackRings;
    protected int maxMonitorsPerTerminator;
    protected int terminatorIdleGuardSeconds;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AutomaticCallbackMethod callbackMethod;
    protected int pollingIntervalSeconds;
    protected int activationDigit;

    /**
     * Ruft den Wert der monitorMinutes-Eigenschaft ab.
     * 
     */
    public int getMonitorMinutes() {
        return monitorMinutes;
    }

    /**
     * Legt den Wert der monitorMinutes-Eigenschaft fest.
     * 
     */
    public void setMonitorMinutes(int value) {
        this.monitorMinutes = value;
    }

    /**
     * Ruft den Wert der maxMonitorsPerOriginator-Eigenschaft ab.
     * 
     */
    public int getMaxMonitorsPerOriginator() {
        return maxMonitorsPerOriginator;
    }

    /**
     * Legt den Wert der maxMonitorsPerOriginator-Eigenschaft fest.
     * 
     */
    public void setMaxMonitorsPerOriginator(int value) {
        this.maxMonitorsPerOriginator = value;
    }

    /**
     * Ruft den Wert der maxCallbackRings-Eigenschaft ab.
     * 
     */
    public int getMaxCallbackRings() {
        return maxCallbackRings;
    }

    /**
     * Legt den Wert der maxCallbackRings-Eigenschaft fest.
     * 
     */
    public void setMaxCallbackRings(int value) {
        this.maxCallbackRings = value;
    }

    /**
     * Ruft den Wert der maxMonitorsPerTerminator-Eigenschaft ab.
     * 
     */
    public int getMaxMonitorsPerTerminator() {
        return maxMonitorsPerTerminator;
    }

    /**
     * Legt den Wert der maxMonitorsPerTerminator-Eigenschaft fest.
     * 
     */
    public void setMaxMonitorsPerTerminator(int value) {
        this.maxMonitorsPerTerminator = value;
    }

    /**
     * Ruft den Wert der terminatorIdleGuardSeconds-Eigenschaft ab.
     * 
     */
    public int getTerminatorIdleGuardSeconds() {
        return terminatorIdleGuardSeconds;
    }

    /**
     * Legt den Wert der terminatorIdleGuardSeconds-Eigenschaft fest.
     * 
     */
    public void setTerminatorIdleGuardSeconds(int value) {
        this.terminatorIdleGuardSeconds = value;
    }

    /**
     * Ruft den Wert der callbackMethod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutomaticCallbackMethod }
     *     
     */
    public AutomaticCallbackMethod getCallbackMethod() {
        return callbackMethod;
    }

    /**
     * Legt den Wert der callbackMethod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutomaticCallbackMethod }
     *     
     */
    public void setCallbackMethod(AutomaticCallbackMethod value) {
        this.callbackMethod = value;
    }

    /**
     * Ruft den Wert der pollingIntervalSeconds-Eigenschaft ab.
     * 
     */
    public int getPollingIntervalSeconds() {
        return pollingIntervalSeconds;
    }

    /**
     * Legt den Wert der pollingIntervalSeconds-Eigenschaft fest.
     * 
     */
    public void setPollingIntervalSeconds(int value) {
        this.pollingIntervalSeconds = value;
    }

    /**
     * Ruft den Wert der activationDigit-Eigenschaft ab.
     * 
     */
    public int getActivationDigit() {
        return activationDigit;
    }

    /**
     * Legt den Wert der activationDigit-Eigenschaft fest.
     * 
     */
    public void setActivationDigit(int value) {
        this.activationDigit = value;
    }

}
