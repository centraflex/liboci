//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCPEConfigParametersGetListRequest20.
 *         Contains a list of system CPE Config parameters.
 *         Replaced by: SystemCPEConfigParametersGetResponse21
 *       
 * 
 * <p>Java-Klasse für SystemCPEConfigParametersGetResponse20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCPEConfigParametersGetResponse20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableIPDeviceManagement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="ftpConnectTimeoutSeconds" type="{}DeviceManagementFTPConnectTimeoutSeconds"/>
 *         <element name="ftpFileTransferTimeoutSeconds" type="{}DeviceManagementFTPFileTransferTimeoutSeconds"/>
 *         <element name="pauseBetweenFileRebuildMilliseconds" type="{}DeviceManagementPauseBetweenFileRebuildMilliseconds"/>
 *         <element name="maxBusyTimeMinutes" type="{}DeviceManagementMaxBusyTimeMinutes"/>
 *         <element name="deviceAccessAppServerClusterName" type="{}NetAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCPEConfigParametersGetResponse20", propOrder = {
    "enableIPDeviceManagement",
    "ftpConnectTimeoutSeconds",
    "ftpFileTransferTimeoutSeconds",
    "pauseBetweenFileRebuildMilliseconds",
    "maxBusyTimeMinutes",
    "deviceAccessAppServerClusterName"
})
public class SystemCPEConfigParametersGetResponse20
    extends OCIDataResponse
{

    protected boolean enableIPDeviceManagement;
    protected int ftpConnectTimeoutSeconds;
    protected int ftpFileTransferTimeoutSeconds;
    protected int pauseBetweenFileRebuildMilliseconds;
    protected int maxBusyTimeMinutes;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceAccessAppServerClusterName;

    /**
     * Ruft den Wert der enableIPDeviceManagement-Eigenschaft ab.
     * 
     */
    public boolean isEnableIPDeviceManagement() {
        return enableIPDeviceManagement;
    }

    /**
     * Legt den Wert der enableIPDeviceManagement-Eigenschaft fest.
     * 
     */
    public void setEnableIPDeviceManagement(boolean value) {
        this.enableIPDeviceManagement = value;
    }

    /**
     * Ruft den Wert der ftpConnectTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getFtpConnectTimeoutSeconds() {
        return ftpConnectTimeoutSeconds;
    }

    /**
     * Legt den Wert der ftpConnectTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setFtpConnectTimeoutSeconds(int value) {
        this.ftpConnectTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der ftpFileTransferTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getFtpFileTransferTimeoutSeconds() {
        return ftpFileTransferTimeoutSeconds;
    }

    /**
     * Legt den Wert der ftpFileTransferTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setFtpFileTransferTimeoutSeconds(int value) {
        this.ftpFileTransferTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der pauseBetweenFileRebuildMilliseconds-Eigenschaft ab.
     * 
     */
    public int getPauseBetweenFileRebuildMilliseconds() {
        return pauseBetweenFileRebuildMilliseconds;
    }

    /**
     * Legt den Wert der pauseBetweenFileRebuildMilliseconds-Eigenschaft fest.
     * 
     */
    public void setPauseBetweenFileRebuildMilliseconds(int value) {
        this.pauseBetweenFileRebuildMilliseconds = value;
    }

    /**
     * Ruft den Wert der maxBusyTimeMinutes-Eigenschaft ab.
     * 
     */
    public int getMaxBusyTimeMinutes() {
        return maxBusyTimeMinutes;
    }

    /**
     * Legt den Wert der maxBusyTimeMinutes-Eigenschaft fest.
     * 
     */
    public void setMaxBusyTimeMinutes(int value) {
        this.maxBusyTimeMinutes = value;
    }

    /**
     * Ruft den Wert der deviceAccessAppServerClusterName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceAccessAppServerClusterName() {
        return deviceAccessAppServerClusterName;
    }

    /**
     * Legt den Wert der deviceAccessAppServerClusterName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceAccessAppServerClusterName(String value) {
        this.deviceAccessAppServerClusterName = value;
    }

}
