//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system settings for the call center enhanced reporting scheduling tasks.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         The following elements are only used in AS data mode:
 *           callCenterEventMode, value “Legacy ECCR" is returned in Amplify data mode
 *       
 * 
 * <p>Java-Klasse für SystemCallCenterEnhancedReportingScheduledTaskParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallCenterEnhancedReportingScheduledTaskParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="scheduledReportSearchIntervalMinutes" type="{}CallCenterScheduledReportSearchIntervalMinutes" minOccurs="0"/>
 *         <element name="maximumScheduledReportsPerInterval" type="{}CallCenterMaximumScheduledReportsPerInterval" minOccurs="0"/>
 *         <element name="deleteScheduledReportDaysAfterCompletion" type="{}CallCenterDaysAfterScheduledReportCompletion" minOccurs="0"/>
 *         <element name="callCenterEventMode" type="{}CallCenterEventRecordingCallCenterEventMode" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallCenterEnhancedReportingScheduledTaskParametersModifyRequest", propOrder = {
    "scheduledReportSearchIntervalMinutes",
    "maximumScheduledReportsPerInterval",
    "deleteScheduledReportDaysAfterCompletion",
    "callCenterEventMode"
})
public class SystemCallCenterEnhancedReportingScheduledTaskParametersModifyRequest
    extends OCIRequest
{

    protected Integer scheduledReportSearchIntervalMinutes;
    protected Integer maximumScheduledReportsPerInterval;
    protected Integer deleteScheduledReportDaysAfterCompletion;
    @XmlSchemaType(name = "token")
    protected CallCenterEventRecordingCallCenterEventMode callCenterEventMode;

    /**
     * Ruft den Wert der scheduledReportSearchIntervalMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getScheduledReportSearchIntervalMinutes() {
        return scheduledReportSearchIntervalMinutes;
    }

    /**
     * Legt den Wert der scheduledReportSearchIntervalMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setScheduledReportSearchIntervalMinutes(Integer value) {
        this.scheduledReportSearchIntervalMinutes = value;
    }

    /**
     * Ruft den Wert der maximumScheduledReportsPerInterval-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaximumScheduledReportsPerInterval() {
        return maximumScheduledReportsPerInterval;
    }

    /**
     * Legt den Wert der maximumScheduledReportsPerInterval-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaximumScheduledReportsPerInterval(Integer value) {
        this.maximumScheduledReportsPerInterval = value;
    }

    /**
     * Ruft den Wert der deleteScheduledReportDaysAfterCompletion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDeleteScheduledReportDaysAfterCompletion() {
        return deleteScheduledReportDaysAfterCompletion;
    }

    /**
     * Legt den Wert der deleteScheduledReportDaysAfterCompletion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDeleteScheduledReportDaysAfterCompletion(Integer value) {
        this.deleteScheduledReportDaysAfterCompletion = value;
    }

    /**
     * Ruft den Wert der callCenterEventMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterEventRecordingCallCenterEventMode }
     *     
     */
    public CallCenterEventRecordingCallCenterEventMode getCallCenterEventMode() {
        return callCenterEventMode;
    }

    /**
     * Legt den Wert der callCenterEventMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterEventRecordingCallCenterEventMode }
     *     
     */
    public void setCallCenterEventMode(CallCenterEventRecordingCallCenterEventMode value) {
        this.callCenterEventMode = value;
    }

}
