//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserMeetMeConferencingGetConferenceGreetingRequest20.
 *         Contains the information of a conference custom greeting.
 *       
 * 
 * <p>Java-Klasse für UserMeetMeConferencingGetConferenceGreetingResponse20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserMeetMeConferencingGetConferenceGreetingResponse20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="playEntranceGreeting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="entranceGreetingFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserMeetMeConferencingGetConferenceGreetingResponse20", propOrder = {
    "playEntranceGreeting",
    "entranceGreetingFile"
})
public class UserMeetMeConferencingGetConferenceGreetingResponse20
    extends OCIDataResponse
{

    protected boolean playEntranceGreeting;
    protected AnnouncementFileKey entranceGreetingFile;

    /**
     * Ruft den Wert der playEntranceGreeting-Eigenschaft ab.
     * 
     */
    public boolean isPlayEntranceGreeting() {
        return playEntranceGreeting;
    }

    /**
     * Legt den Wert der playEntranceGreeting-Eigenschaft fest.
     * 
     */
    public void setPlayEntranceGreeting(boolean value) {
        this.playEntranceGreeting = value;
    }

    /**
     * Ruft den Wert der entranceGreetingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileKey }
     *     
     */
    public AnnouncementFileKey getEntranceGreetingFile() {
        return entranceGreetingFile;
    }

    /**
     * Legt den Wert der entranceGreetingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileKey }
     *     
     */
    public void setEntranceGreetingFile(AnnouncementFileKey value) {
        this.entranceGreetingFile = value;
    }

}
