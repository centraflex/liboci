//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Get the system Xsi policy profile list.
 *         The response is either SystemXsiPolicyProfileGetListResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemXsiPolicyProfileGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemXsiPolicyProfileGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="xsiPolicyProfileLevel" type="{}XsiPolicyProfileLevel" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemXsiPolicyProfileGetListRequest", propOrder = {
    "xsiPolicyProfileLevel"
})
public class SystemXsiPolicyProfileGetListRequest
    extends OCIRequest
{

    @XmlSchemaType(name = "token")
    protected XsiPolicyProfileLevel xsiPolicyProfileLevel;

    /**
     * Ruft den Wert der xsiPolicyProfileLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XsiPolicyProfileLevel }
     *     
     */
    public XsiPolicyProfileLevel getXsiPolicyProfileLevel() {
        return xsiPolicyProfileLevel;
    }

    /**
     * Legt den Wert der xsiPolicyProfileLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XsiPolicyProfileLevel }
     *     
     */
    public void setXsiPolicyProfileLevel(XsiPolicyProfileLevel value) {
        this.xsiPolicyProfileLevel = value;
    }

}
