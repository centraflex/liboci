//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to ServiceProviderServicePackMigrationTaskGetRequest14sp4.
 *         The groupTable column headings are: "Group Id", "Group Name", "User Count".
 *         
 *         Replaced By: ServiceProviderServicePackMigrationTaskGetResponse21 in AS data mode
 *       
 * 
 * <p>Java-Klasse für ServiceProviderServicePackMigrationTaskGetResponse14sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderServicePackMigrationTaskGetResponse14sp4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="taskName" type="{}ServicePackMigrationTaskName"/>
 *         <element name="startTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         <element name="maxDurationHours" type="{}ServicePackMigrationMaxDurationHours"/>
 *         <element name="sendReportEmail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="reportDeliveryEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="abortOnError" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="abortErrorThreshold" type="{}ServicePackMigrationAbortErrorThreshold" minOccurs="0"/>
 *         <element name="reportAllUsers" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="automaticallyIncrementServiceQuantity" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="errorCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="status" type="{}ServicePackMigrationTaskStatus"/>
 *         <element name="groupsProcessed" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="groupsTotal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="usersProcessed" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="usersTotal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="userSelectionType" type="{}ServicePackMigrationTaskUserSelectionType"/>
 *         <element name="reportFilePathName" type="{}FileName"/>
 *         <choice>
 *           <element name="migrateAllGroups" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *           <element name="groupTable" type="{C}OCITable"/>
 *         </choice>
 *         <element name="userSelectionServicePackName" type="{}ServicePackName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="userSelectionServiceName" type="{}UserService" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="removeServicePackName" type="{}ServicePackName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="removeServiceName" type="{}UserService" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="assignServicePackName" type="{}ServicePackName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="assignServiceName" type="{}UserService" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderServicePackMigrationTaskGetResponse14sp4", propOrder = {
    "taskName",
    "startTimestamp",
    "maxDurationHours",
    "sendReportEmail",
    "reportDeliveryEmailAddress",
    "abortOnError",
    "abortErrorThreshold",
    "reportAllUsers",
    "automaticallyIncrementServiceQuantity",
    "errorCount",
    "status",
    "groupsProcessed",
    "groupsTotal",
    "usersProcessed",
    "usersTotal",
    "userSelectionType",
    "reportFilePathName",
    "migrateAllGroups",
    "groupTable",
    "userSelectionServicePackName",
    "userSelectionServiceName",
    "removeServicePackName",
    "removeServiceName",
    "assignServicePackName",
    "assignServiceName"
})
public class ServiceProviderServicePackMigrationTaskGetResponse14Sp4
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String taskName;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startTimestamp;
    protected int maxDurationHours;
    protected boolean sendReportEmail;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String reportDeliveryEmailAddress;
    protected boolean abortOnError;
    protected Integer abortErrorThreshold;
    protected boolean reportAllUsers;
    protected boolean automaticallyIncrementServiceQuantity;
    protected int errorCount;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServicePackMigrationTaskStatus status;
    protected int groupsProcessed;
    protected int groupsTotal;
    protected int usersProcessed;
    protected int usersTotal;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected ServicePackMigrationTaskUserSelectionType userSelectionType;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String reportFilePathName;
    protected Boolean migrateAllGroups;
    protected OCITable groupTable;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> userSelectionServicePackName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> userSelectionServiceName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> removeServicePackName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> removeServiceName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> assignServicePackName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> assignServiceName;

    /**
     * Ruft den Wert der taskName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * Legt den Wert der taskName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskName(String value) {
        this.taskName = value;
    }

    /**
     * Ruft den Wert der startTimestamp-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartTimestamp() {
        return startTimestamp;
    }

    /**
     * Legt den Wert der startTimestamp-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartTimestamp(XMLGregorianCalendar value) {
        this.startTimestamp = value;
    }

    /**
     * Ruft den Wert der maxDurationHours-Eigenschaft ab.
     * 
     */
    public int getMaxDurationHours() {
        return maxDurationHours;
    }

    /**
     * Legt den Wert der maxDurationHours-Eigenschaft fest.
     * 
     */
    public void setMaxDurationHours(int value) {
        this.maxDurationHours = value;
    }

    /**
     * Ruft den Wert der sendReportEmail-Eigenschaft ab.
     * 
     */
    public boolean isSendReportEmail() {
        return sendReportEmail;
    }

    /**
     * Legt den Wert der sendReportEmail-Eigenschaft fest.
     * 
     */
    public void setSendReportEmail(boolean value) {
        this.sendReportEmail = value;
    }

    /**
     * Ruft den Wert der reportDeliveryEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportDeliveryEmailAddress() {
        return reportDeliveryEmailAddress;
    }

    /**
     * Legt den Wert der reportDeliveryEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportDeliveryEmailAddress(String value) {
        this.reportDeliveryEmailAddress = value;
    }

    /**
     * Ruft den Wert der abortOnError-Eigenschaft ab.
     * 
     */
    public boolean isAbortOnError() {
        return abortOnError;
    }

    /**
     * Legt den Wert der abortOnError-Eigenschaft fest.
     * 
     */
    public void setAbortOnError(boolean value) {
        this.abortOnError = value;
    }

    /**
     * Ruft den Wert der abortErrorThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAbortErrorThreshold() {
        return abortErrorThreshold;
    }

    /**
     * Legt den Wert der abortErrorThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAbortErrorThreshold(Integer value) {
        this.abortErrorThreshold = value;
    }

    /**
     * Ruft den Wert der reportAllUsers-Eigenschaft ab.
     * 
     */
    public boolean isReportAllUsers() {
        return reportAllUsers;
    }

    /**
     * Legt den Wert der reportAllUsers-Eigenschaft fest.
     * 
     */
    public void setReportAllUsers(boolean value) {
        this.reportAllUsers = value;
    }

    /**
     * Ruft den Wert der automaticallyIncrementServiceQuantity-Eigenschaft ab.
     * 
     */
    public boolean isAutomaticallyIncrementServiceQuantity() {
        return automaticallyIncrementServiceQuantity;
    }

    /**
     * Legt den Wert der automaticallyIncrementServiceQuantity-Eigenschaft fest.
     * 
     */
    public void setAutomaticallyIncrementServiceQuantity(boolean value) {
        this.automaticallyIncrementServiceQuantity = value;
    }

    /**
     * Ruft den Wert der errorCount-Eigenschaft ab.
     * 
     */
    public int getErrorCount() {
        return errorCount;
    }

    /**
     * Legt den Wert der errorCount-Eigenschaft fest.
     * 
     */
    public void setErrorCount(int value) {
        this.errorCount = value;
    }

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServicePackMigrationTaskStatus }
     *     
     */
    public ServicePackMigrationTaskStatus getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServicePackMigrationTaskStatus }
     *     
     */
    public void setStatus(ServicePackMigrationTaskStatus value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der groupsProcessed-Eigenschaft ab.
     * 
     */
    public int getGroupsProcessed() {
        return groupsProcessed;
    }

    /**
     * Legt den Wert der groupsProcessed-Eigenschaft fest.
     * 
     */
    public void setGroupsProcessed(int value) {
        this.groupsProcessed = value;
    }

    /**
     * Ruft den Wert der groupsTotal-Eigenschaft ab.
     * 
     */
    public int getGroupsTotal() {
        return groupsTotal;
    }

    /**
     * Legt den Wert der groupsTotal-Eigenschaft fest.
     * 
     */
    public void setGroupsTotal(int value) {
        this.groupsTotal = value;
    }

    /**
     * Ruft den Wert der usersProcessed-Eigenschaft ab.
     * 
     */
    public int getUsersProcessed() {
        return usersProcessed;
    }

    /**
     * Legt den Wert der usersProcessed-Eigenschaft fest.
     * 
     */
    public void setUsersProcessed(int value) {
        this.usersProcessed = value;
    }

    /**
     * Ruft den Wert der usersTotal-Eigenschaft ab.
     * 
     */
    public int getUsersTotal() {
        return usersTotal;
    }

    /**
     * Legt den Wert der usersTotal-Eigenschaft fest.
     * 
     */
    public void setUsersTotal(int value) {
        this.usersTotal = value;
    }

    /**
     * Ruft den Wert der userSelectionType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServicePackMigrationTaskUserSelectionType }
     *     
     */
    public ServicePackMigrationTaskUserSelectionType getUserSelectionType() {
        return userSelectionType;
    }

    /**
     * Legt den Wert der userSelectionType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServicePackMigrationTaskUserSelectionType }
     *     
     */
    public void setUserSelectionType(ServicePackMigrationTaskUserSelectionType value) {
        this.userSelectionType = value;
    }

    /**
     * Ruft den Wert der reportFilePathName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportFilePathName() {
        return reportFilePathName;
    }

    /**
     * Legt den Wert der reportFilePathName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportFilePathName(String value) {
        this.reportFilePathName = value;
    }

    /**
     * Ruft den Wert der migrateAllGroups-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMigrateAllGroups() {
        return migrateAllGroups;
    }

    /**
     * Legt den Wert der migrateAllGroups-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMigrateAllGroups(Boolean value) {
        this.migrateAllGroups = value;
    }

    /**
     * Ruft den Wert der groupTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getGroupTable() {
        return groupTable;
    }

    /**
     * Legt den Wert der groupTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setGroupTable(OCITable value) {
        this.groupTable = value;
    }

    /**
     * Gets the value of the userSelectionServicePackName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the userSelectionServicePackName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserSelectionServicePackName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the userSelectionServicePackName property.
     */
    public List<String> getUserSelectionServicePackName() {
        if (userSelectionServicePackName == null) {
            userSelectionServicePackName = new ArrayList<>();
        }
        return this.userSelectionServicePackName;
    }

    /**
     * Gets the value of the userSelectionServiceName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the userSelectionServiceName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserSelectionServiceName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the userSelectionServiceName property.
     */
    public List<String> getUserSelectionServiceName() {
        if (userSelectionServiceName == null) {
            userSelectionServiceName = new ArrayList<>();
        }
        return this.userSelectionServiceName;
    }

    /**
     * Gets the value of the removeServicePackName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the removeServicePackName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemoveServicePackName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the removeServicePackName property.
     */
    public List<String> getRemoveServicePackName() {
        if (removeServicePackName == null) {
            removeServicePackName = new ArrayList<>();
        }
        return this.removeServicePackName;
    }

    /**
     * Gets the value of the removeServiceName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the removeServiceName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemoveServiceName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the removeServiceName property.
     */
    public List<String> getRemoveServiceName() {
        if (removeServiceName == null) {
            removeServiceName = new ArrayList<>();
        }
        return this.removeServiceName;
    }

    /**
     * Gets the value of the assignServicePackName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the assignServicePackName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssignServicePackName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the assignServicePackName property.
     */
    public List<String> getAssignServicePackName() {
        if (assignServicePackName == null) {
            assignServicePackName = new ArrayList<>();
        }
        return this.assignServicePackName;
    }

    /**
     * Gets the value of the assignServiceName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the assignServiceName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssignServiceName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the assignServiceName property.
     */
    public List<String> getAssignServiceName() {
        if (assignServiceName == null) {
            assignServiceName = new ArrayList<>();
        }
        return this.assignServiceName;
    }

}
