//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemVoiceMessagingGroupGetVoicePortalMenusRequest20.
 *       
 * 
 * <p>Java-Klasse für SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemVoiceMessagingGroupGetVoicePortalMenusResponse20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useVoicePortalCustomization" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="voicePortalMainMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="voiceMessaging" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="commPilotExpressProfile" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="greetings" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="callForwardingOptions" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="voicePortalCalling" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="hoteling" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="passcode" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="exitVoicePortal" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="externalRouting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="announcement" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="announcementMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="recordAudio" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="recordAudioVideo" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="announcementRecordingMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="acceptRecording" type="{}DigitAny"/>
 *                   <element name="rejectRerecord" type="{}DigitAny"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="end" type="{}VoicePortalDigitSequence"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="greetingsMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="conferenceGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="conferenceGreetingMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="activateConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deactivateConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="recordNewConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToCurrentConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="voiceMessagingMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="playMessages" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="changeBusyGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="changeNoAnswerGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="changeExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deleteAllMessages" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="passcode" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="messageDeposit" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="playGreetingMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="skipBackward" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="pauseOrResume" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="skipForward" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="jumpToBegin" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="jumpToEnd" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="changeBusyOrNoAnswerGreetingMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="recordNewGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToCurrentGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="revertToSystemDefaultGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="changeExtendedAwayGreetingMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="activateExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deactivateExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="recordNewGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToCurrentGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="recordNewGreetingOrPersonalizedNameMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="endRecording" type="{}DigitAny"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="deleteAllMessagesMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="confirmDeletion" type="{}DigitAny"/>
 *                   <element name="cancelDeletion" type="{}DigitAny"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="commPilotExpressProfileMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="activateAvailableInOfficeProfile" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="activateAvailableOutOfOfficeProfile" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="activateBusyProfile" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="activateUnavailableProfile" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="noProfile" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="personalizedNameMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="recordNewPersonalizedName" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToCurrentPersonalizedName" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deletePersonalizedName" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="callForwardingOptionsMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="activateCallForwarding" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deactivateCallForwarding" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="changeCallForwardingDestination" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToCallForwardingStatus" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="changeCallForwardingDestinationMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="finishEnteringNewDestinationNumber" type="{}DigitStarPound"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="voicePortalCallingMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="endCurrentCallAndGoBackToPreviousMenu" type="{}VoicePortalDigitSequence"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="hotelingMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="checkHostStatus" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="associateWithHost" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="disassociateFromHost" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="disassociateFromRemoteHost" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="passcodeMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="finishEnteringOrReenteringPasscode" type="{}DigitStarPound"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitStarPound"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="playMessagesMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="saveMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deleteMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="playMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="previousMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="nextMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="callbackCaller" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="replyMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="forwardMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="additionalMessageOptions" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="passcode" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="playMessageMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="skipBackward" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="pauseOrResume" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="skipForward" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="jumpToBegin" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="jumpToEnd" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="additionalMessageOptionsMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="saveMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deleteMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="callbackCaller" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="replyMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="forwardMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="passcode" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="forwardOrComposeMessageMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="sendToPerson" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="sendToAllGroupMembers" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="sendToDistributionList" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="changeCurrentIntroductionOrMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToCurrentIntroductionOrMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setOrClearUrgentIndicator" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setOrClearConfidentialIndicator" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="replyMessageMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="sendReplyToCaller" type="{}DigitAny"/>
 *                   <element name="changeCurrentReply" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToCurrentReply" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setOrClearUrgentIndicator" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setOrClearConfidentialIndicator" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="sendToDistributionListMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="sendMessageToSelectedDistributionList" type="{}DigitAny"/>
 *                   <element name="selectDistributionList" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="reviewSelectedDistributionList" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="selectDistributionListMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="returnToPreviousMenu" type="{}DigitStarPound"/>
 *                   <element name="repeatMenuOrFinishEnteringDistributionListNumber" type="{}DigitStarPound" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="reviewSelectedDistributionListMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="interruptPlaybackAndReturnToPreviousMenu" type="{}DigitAny"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="sendMessageToSelectedDistributionListMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="confirmSendingToDistributionList" type="{}DigitStarPound" minOccurs="0"/>
 *                   <element name="cancelSendingToDistributionList" type="{}DigitStarPound"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="sendToAllGroupMembersMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="confirmSendingToEntireGroup" type="{}DigitAny"/>
 *                   <element name="cancelSendingToEntireGroup" type="{}DigitAny"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="sendToPersonMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="confirmSendingMessage" type="{}DigitAny"/>
 *                   <element name="cancelSendingMessage" type="{}DigitAny"/>
 *                   <element name="finishEnteringNumberWhereToSendMessageTo" type="{}DigitStarPound"/>
 *                   <element name="finishForwardingOrSendingMessage" type="{}DigitAny"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="changeCurrentIntroductionOrMessageOrReplyMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="endRecording" type="{}DigitAny"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="voicePortalLoginMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="accessUsingOtherMailboxId" type="{}VoicePortalDigitSequence" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="faxMessagingMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="saveFaxMessageAndSkipToNext" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="previousFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="nextFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deleteFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="printFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="messageDepositMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="enableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="disableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToMessageDepositStatus" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="disableMessageDepositMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="disconnectAfterGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="forwardAfterGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="changeForwardingDestination" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="greetingOnlyForwardingDestinationMenuKeys">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="greetingOnlyForwardingDestination" type="{}DigitStarPound"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemVoiceMessagingGroupGetVoicePortalMenusResponse20", propOrder = {
    "useVoicePortalCustomization",
    "voicePortalMainMenuKeys",
    "announcementMenuKeys",
    "announcementRecordingMenuKeys",
    "greetingsMenuKeys",
    "conferenceGreetingMenuKeys",
    "voiceMessagingMenuKeys",
    "playGreetingMenuKeys",
    "changeBusyOrNoAnswerGreetingMenuKeys",
    "changeExtendedAwayGreetingMenuKeys",
    "recordNewGreetingOrPersonalizedNameMenuKeys",
    "deleteAllMessagesMenuKeys",
    "commPilotExpressProfileMenuKeys",
    "personalizedNameMenuKeys",
    "callForwardingOptionsMenuKeys",
    "changeCallForwardingDestinationMenuKeys",
    "voicePortalCallingMenuKeys",
    "hotelingMenuKeys",
    "passcodeMenuKeys",
    "playMessagesMenuKeys",
    "playMessageMenuKeys",
    "additionalMessageOptionsMenuKeys",
    "forwardOrComposeMessageMenuKeys",
    "replyMessageMenuKeys",
    "sendToDistributionListMenuKeys",
    "selectDistributionListMenuKeys",
    "reviewSelectedDistributionListMenuKeys",
    "sendMessageToSelectedDistributionListMenuKeys",
    "sendToAllGroupMembersMenuKeys",
    "sendToPersonMenuKeys",
    "changeCurrentIntroductionOrMessageOrReplyMenuKeys",
    "voicePortalLoginMenuKeys",
    "faxMessagingMenuKeys",
    "messageDepositMenuKeys",
    "disableMessageDepositMenuKeys",
    "greetingOnlyForwardingDestinationMenuKeys"
})
public class SystemVoiceMessagingGroupGetVoicePortalMenusResponse20
    extends OCIDataResponse
{

    protected boolean useVoicePortalCustomization;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoicePortalMainMenuKeys voicePortalMainMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .AnnouncementMenuKeys announcementMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .AnnouncementRecordingMenuKeys announcementRecordingMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .GreetingsMenuKeys greetingsMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ConferenceGreetingMenuKeys conferenceGreetingMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoiceMessagingMenuKeys voiceMessagingMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PlayGreetingMenuKeys playGreetingMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeBusyOrNoAnswerGreetingMenuKeys changeBusyOrNoAnswerGreetingMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeExtendedAwayGreetingMenuKeys changeExtendedAwayGreetingMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .RecordNewGreetingOrPersonalizedNameMenuKeys recordNewGreetingOrPersonalizedNameMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .DeleteAllMessagesMenuKeys deleteAllMessagesMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .CommPilotExpressProfileMenuKeys commPilotExpressProfileMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PersonalizedNameMenuKeys personalizedNameMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .CallForwardingOptionsMenuKeys callForwardingOptionsMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeCallForwardingDestinationMenuKeys changeCallForwardingDestinationMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoicePortalCallingMenuKeys voicePortalCallingMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .HotelingMenuKeys hotelingMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PasscodeMenuKeys passcodeMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PlayMessagesMenuKeys playMessagesMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PlayMessageMenuKeys playMessageMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .AdditionalMessageOptionsMenuKeys additionalMessageOptionsMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ForwardOrComposeMessageMenuKeys forwardOrComposeMessageMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ReplyMessageMenuKeys replyMessageMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendToDistributionListMenuKeys sendToDistributionListMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SelectDistributionListMenuKeys selectDistributionListMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ReviewSelectedDistributionListMenuKeys reviewSelectedDistributionListMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendMessageToSelectedDistributionListMenuKeys sendMessageToSelectedDistributionListMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendToAllGroupMembersMenuKeys sendToAllGroupMembersMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendToPersonMenuKeys sendToPersonMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeCurrentIntroductionOrMessageOrReplyMenuKeys changeCurrentIntroductionOrMessageOrReplyMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoicePortalLoginMenuKeys voicePortalLoginMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .FaxMessagingMenuKeys faxMessagingMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .MessageDepositMenuKeys messageDepositMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .DisableMessageDepositMenuKeys disableMessageDepositMenuKeys;
    @XmlElement(required = true)
    protected SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .GreetingOnlyForwardingDestinationMenuKeys greetingOnlyForwardingDestinationMenuKeys;

    /**
     * Ruft den Wert der useVoicePortalCustomization-Eigenschaft ab.
     * 
     */
    public boolean isUseVoicePortalCustomization() {
        return useVoicePortalCustomization;
    }

    /**
     * Legt den Wert der useVoicePortalCustomization-Eigenschaft fest.
     * 
     */
    public void setUseVoicePortalCustomization(boolean value) {
        this.useVoicePortalCustomization = value;
    }

    /**
     * Ruft den Wert der voicePortalMainMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoicePortalMainMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoicePortalMainMenuKeys getVoicePortalMainMenuKeys() {
        return voicePortalMainMenuKeys;
    }

    /**
     * Legt den Wert der voicePortalMainMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoicePortalMainMenuKeys }
     *     
     */
    public void setVoicePortalMainMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoicePortalMainMenuKeys value) {
        this.voicePortalMainMenuKeys = value;
    }

    /**
     * Ruft den Wert der announcementMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .AnnouncementMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .AnnouncementMenuKeys getAnnouncementMenuKeys() {
        return announcementMenuKeys;
    }

    /**
     * Legt den Wert der announcementMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .AnnouncementMenuKeys }
     *     
     */
    public void setAnnouncementMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .AnnouncementMenuKeys value) {
        this.announcementMenuKeys = value;
    }

    /**
     * Ruft den Wert der announcementRecordingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .AnnouncementRecordingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .AnnouncementRecordingMenuKeys getAnnouncementRecordingMenuKeys() {
        return announcementRecordingMenuKeys;
    }

    /**
     * Legt den Wert der announcementRecordingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .AnnouncementRecordingMenuKeys }
     *     
     */
    public void setAnnouncementRecordingMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .AnnouncementRecordingMenuKeys value) {
        this.announcementRecordingMenuKeys = value;
    }

    /**
     * Ruft den Wert der greetingsMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .GreetingsMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .GreetingsMenuKeys getGreetingsMenuKeys() {
        return greetingsMenuKeys;
    }

    /**
     * Legt den Wert der greetingsMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .GreetingsMenuKeys }
     *     
     */
    public void setGreetingsMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .GreetingsMenuKeys value) {
        this.greetingsMenuKeys = value;
    }

    /**
     * Ruft den Wert der conferenceGreetingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ConferenceGreetingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ConferenceGreetingMenuKeys getConferenceGreetingMenuKeys() {
        return conferenceGreetingMenuKeys;
    }

    /**
     * Legt den Wert der conferenceGreetingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ConferenceGreetingMenuKeys }
     *     
     */
    public void setConferenceGreetingMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ConferenceGreetingMenuKeys value) {
        this.conferenceGreetingMenuKeys = value;
    }

    /**
     * Ruft den Wert der voiceMessagingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoiceMessagingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoiceMessagingMenuKeys getVoiceMessagingMenuKeys() {
        return voiceMessagingMenuKeys;
    }

    /**
     * Legt den Wert der voiceMessagingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoiceMessagingMenuKeys }
     *     
     */
    public void setVoiceMessagingMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoiceMessagingMenuKeys value) {
        this.voiceMessagingMenuKeys = value;
    }

    /**
     * Ruft den Wert der playGreetingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PlayGreetingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PlayGreetingMenuKeys getPlayGreetingMenuKeys() {
        return playGreetingMenuKeys;
    }

    /**
     * Legt den Wert der playGreetingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PlayGreetingMenuKeys }
     *     
     */
    public void setPlayGreetingMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PlayGreetingMenuKeys value) {
        this.playGreetingMenuKeys = value;
    }

    /**
     * Ruft den Wert der changeBusyOrNoAnswerGreetingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeBusyOrNoAnswerGreetingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeBusyOrNoAnswerGreetingMenuKeys getChangeBusyOrNoAnswerGreetingMenuKeys() {
        return changeBusyOrNoAnswerGreetingMenuKeys;
    }

    /**
     * Legt den Wert der changeBusyOrNoAnswerGreetingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeBusyOrNoAnswerGreetingMenuKeys }
     *     
     */
    public void setChangeBusyOrNoAnswerGreetingMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeBusyOrNoAnswerGreetingMenuKeys value) {
        this.changeBusyOrNoAnswerGreetingMenuKeys = value;
    }

    /**
     * Ruft den Wert der changeExtendedAwayGreetingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeExtendedAwayGreetingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeExtendedAwayGreetingMenuKeys getChangeExtendedAwayGreetingMenuKeys() {
        return changeExtendedAwayGreetingMenuKeys;
    }

    /**
     * Legt den Wert der changeExtendedAwayGreetingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeExtendedAwayGreetingMenuKeys }
     *     
     */
    public void setChangeExtendedAwayGreetingMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeExtendedAwayGreetingMenuKeys value) {
        this.changeExtendedAwayGreetingMenuKeys = value;
    }

    /**
     * Ruft den Wert der recordNewGreetingOrPersonalizedNameMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .RecordNewGreetingOrPersonalizedNameMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .RecordNewGreetingOrPersonalizedNameMenuKeys getRecordNewGreetingOrPersonalizedNameMenuKeys() {
        return recordNewGreetingOrPersonalizedNameMenuKeys;
    }

    /**
     * Legt den Wert der recordNewGreetingOrPersonalizedNameMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .RecordNewGreetingOrPersonalizedNameMenuKeys }
     *     
     */
    public void setRecordNewGreetingOrPersonalizedNameMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .RecordNewGreetingOrPersonalizedNameMenuKeys value) {
        this.recordNewGreetingOrPersonalizedNameMenuKeys = value;
    }

    /**
     * Ruft den Wert der deleteAllMessagesMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .DeleteAllMessagesMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .DeleteAllMessagesMenuKeys getDeleteAllMessagesMenuKeys() {
        return deleteAllMessagesMenuKeys;
    }

    /**
     * Legt den Wert der deleteAllMessagesMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .DeleteAllMessagesMenuKeys }
     *     
     */
    public void setDeleteAllMessagesMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .DeleteAllMessagesMenuKeys value) {
        this.deleteAllMessagesMenuKeys = value;
    }

    /**
     * Ruft den Wert der commPilotExpressProfileMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .CommPilotExpressProfileMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .CommPilotExpressProfileMenuKeys getCommPilotExpressProfileMenuKeys() {
        return commPilotExpressProfileMenuKeys;
    }

    /**
     * Legt den Wert der commPilotExpressProfileMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .CommPilotExpressProfileMenuKeys }
     *     
     */
    public void setCommPilotExpressProfileMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .CommPilotExpressProfileMenuKeys value) {
        this.commPilotExpressProfileMenuKeys = value;
    }

    /**
     * Ruft den Wert der personalizedNameMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PersonalizedNameMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PersonalizedNameMenuKeys getPersonalizedNameMenuKeys() {
        return personalizedNameMenuKeys;
    }

    /**
     * Legt den Wert der personalizedNameMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PersonalizedNameMenuKeys }
     *     
     */
    public void setPersonalizedNameMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PersonalizedNameMenuKeys value) {
        this.personalizedNameMenuKeys = value;
    }

    /**
     * Ruft den Wert der callForwardingOptionsMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .CallForwardingOptionsMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .CallForwardingOptionsMenuKeys getCallForwardingOptionsMenuKeys() {
        return callForwardingOptionsMenuKeys;
    }

    /**
     * Legt den Wert der callForwardingOptionsMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .CallForwardingOptionsMenuKeys }
     *     
     */
    public void setCallForwardingOptionsMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .CallForwardingOptionsMenuKeys value) {
        this.callForwardingOptionsMenuKeys = value;
    }

    /**
     * Ruft den Wert der changeCallForwardingDestinationMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeCallForwardingDestinationMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeCallForwardingDestinationMenuKeys getChangeCallForwardingDestinationMenuKeys() {
        return changeCallForwardingDestinationMenuKeys;
    }

    /**
     * Legt den Wert der changeCallForwardingDestinationMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeCallForwardingDestinationMenuKeys }
     *     
     */
    public void setChangeCallForwardingDestinationMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeCallForwardingDestinationMenuKeys value) {
        this.changeCallForwardingDestinationMenuKeys = value;
    }

    /**
     * Ruft den Wert der voicePortalCallingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoicePortalCallingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoicePortalCallingMenuKeys getVoicePortalCallingMenuKeys() {
        return voicePortalCallingMenuKeys;
    }

    /**
     * Legt den Wert der voicePortalCallingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoicePortalCallingMenuKeys }
     *     
     */
    public void setVoicePortalCallingMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoicePortalCallingMenuKeys value) {
        this.voicePortalCallingMenuKeys = value;
    }

    /**
     * Ruft den Wert der hotelingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .HotelingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .HotelingMenuKeys getHotelingMenuKeys() {
        return hotelingMenuKeys;
    }

    /**
     * Legt den Wert der hotelingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .HotelingMenuKeys }
     *     
     */
    public void setHotelingMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .HotelingMenuKeys value) {
        this.hotelingMenuKeys = value;
    }

    /**
     * Ruft den Wert der passcodeMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PasscodeMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PasscodeMenuKeys getPasscodeMenuKeys() {
        return passcodeMenuKeys;
    }

    /**
     * Legt den Wert der passcodeMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PasscodeMenuKeys }
     *     
     */
    public void setPasscodeMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PasscodeMenuKeys value) {
        this.passcodeMenuKeys = value;
    }

    /**
     * Ruft den Wert der playMessagesMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PlayMessagesMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PlayMessagesMenuKeys getPlayMessagesMenuKeys() {
        return playMessagesMenuKeys;
    }

    /**
     * Legt den Wert der playMessagesMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PlayMessagesMenuKeys }
     *     
     */
    public void setPlayMessagesMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PlayMessagesMenuKeys value) {
        this.playMessagesMenuKeys = value;
    }

    /**
     * Ruft den Wert der playMessageMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PlayMessageMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PlayMessageMenuKeys getPlayMessageMenuKeys() {
        return playMessageMenuKeys;
    }

    /**
     * Legt den Wert der playMessageMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PlayMessageMenuKeys }
     *     
     */
    public void setPlayMessageMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .PlayMessageMenuKeys value) {
        this.playMessageMenuKeys = value;
    }

    /**
     * Ruft den Wert der additionalMessageOptionsMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .AdditionalMessageOptionsMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .AdditionalMessageOptionsMenuKeys getAdditionalMessageOptionsMenuKeys() {
        return additionalMessageOptionsMenuKeys;
    }

    /**
     * Legt den Wert der additionalMessageOptionsMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .AdditionalMessageOptionsMenuKeys }
     *     
     */
    public void setAdditionalMessageOptionsMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .AdditionalMessageOptionsMenuKeys value) {
        this.additionalMessageOptionsMenuKeys = value;
    }

    /**
     * Ruft den Wert der forwardOrComposeMessageMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ForwardOrComposeMessageMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ForwardOrComposeMessageMenuKeys getForwardOrComposeMessageMenuKeys() {
        return forwardOrComposeMessageMenuKeys;
    }

    /**
     * Legt den Wert der forwardOrComposeMessageMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ForwardOrComposeMessageMenuKeys }
     *     
     */
    public void setForwardOrComposeMessageMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ForwardOrComposeMessageMenuKeys value) {
        this.forwardOrComposeMessageMenuKeys = value;
    }

    /**
     * Ruft den Wert der replyMessageMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ReplyMessageMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ReplyMessageMenuKeys getReplyMessageMenuKeys() {
        return replyMessageMenuKeys;
    }

    /**
     * Legt den Wert der replyMessageMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ReplyMessageMenuKeys }
     *     
     */
    public void setReplyMessageMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ReplyMessageMenuKeys value) {
        this.replyMessageMenuKeys = value;
    }

    /**
     * Ruft den Wert der sendToDistributionListMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendToDistributionListMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendToDistributionListMenuKeys getSendToDistributionListMenuKeys() {
        return sendToDistributionListMenuKeys;
    }

    /**
     * Legt den Wert der sendToDistributionListMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendToDistributionListMenuKeys }
     *     
     */
    public void setSendToDistributionListMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendToDistributionListMenuKeys value) {
        this.sendToDistributionListMenuKeys = value;
    }

    /**
     * Ruft den Wert der selectDistributionListMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SelectDistributionListMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SelectDistributionListMenuKeys getSelectDistributionListMenuKeys() {
        return selectDistributionListMenuKeys;
    }

    /**
     * Legt den Wert der selectDistributionListMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SelectDistributionListMenuKeys }
     *     
     */
    public void setSelectDistributionListMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SelectDistributionListMenuKeys value) {
        this.selectDistributionListMenuKeys = value;
    }

    /**
     * Ruft den Wert der reviewSelectedDistributionListMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ReviewSelectedDistributionListMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ReviewSelectedDistributionListMenuKeys getReviewSelectedDistributionListMenuKeys() {
        return reviewSelectedDistributionListMenuKeys;
    }

    /**
     * Legt den Wert der reviewSelectedDistributionListMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ReviewSelectedDistributionListMenuKeys }
     *     
     */
    public void setReviewSelectedDistributionListMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ReviewSelectedDistributionListMenuKeys value) {
        this.reviewSelectedDistributionListMenuKeys = value;
    }

    /**
     * Ruft den Wert der sendMessageToSelectedDistributionListMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendMessageToSelectedDistributionListMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendMessageToSelectedDistributionListMenuKeys getSendMessageToSelectedDistributionListMenuKeys() {
        return sendMessageToSelectedDistributionListMenuKeys;
    }

    /**
     * Legt den Wert der sendMessageToSelectedDistributionListMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendMessageToSelectedDistributionListMenuKeys }
     *     
     */
    public void setSendMessageToSelectedDistributionListMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendMessageToSelectedDistributionListMenuKeys value) {
        this.sendMessageToSelectedDistributionListMenuKeys = value;
    }

    /**
     * Ruft den Wert der sendToAllGroupMembersMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendToAllGroupMembersMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendToAllGroupMembersMenuKeys getSendToAllGroupMembersMenuKeys() {
        return sendToAllGroupMembersMenuKeys;
    }

    /**
     * Legt den Wert der sendToAllGroupMembersMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendToAllGroupMembersMenuKeys }
     *     
     */
    public void setSendToAllGroupMembersMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendToAllGroupMembersMenuKeys value) {
        this.sendToAllGroupMembersMenuKeys = value;
    }

    /**
     * Ruft den Wert der sendToPersonMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendToPersonMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendToPersonMenuKeys getSendToPersonMenuKeys() {
        return sendToPersonMenuKeys;
    }

    /**
     * Legt den Wert der sendToPersonMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendToPersonMenuKeys }
     *     
     */
    public void setSendToPersonMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .SendToPersonMenuKeys value) {
        this.sendToPersonMenuKeys = value;
    }

    /**
     * Ruft den Wert der changeCurrentIntroductionOrMessageOrReplyMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeCurrentIntroductionOrMessageOrReplyMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeCurrentIntroductionOrMessageOrReplyMenuKeys getChangeCurrentIntroductionOrMessageOrReplyMenuKeys() {
        return changeCurrentIntroductionOrMessageOrReplyMenuKeys;
    }

    /**
     * Legt den Wert der changeCurrentIntroductionOrMessageOrReplyMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeCurrentIntroductionOrMessageOrReplyMenuKeys }
     *     
     */
    public void setChangeCurrentIntroductionOrMessageOrReplyMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .ChangeCurrentIntroductionOrMessageOrReplyMenuKeys value) {
        this.changeCurrentIntroductionOrMessageOrReplyMenuKeys = value;
    }

    /**
     * Ruft den Wert der voicePortalLoginMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoicePortalLoginMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoicePortalLoginMenuKeys getVoicePortalLoginMenuKeys() {
        return voicePortalLoginMenuKeys;
    }

    /**
     * Legt den Wert der voicePortalLoginMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoicePortalLoginMenuKeys }
     *     
     */
    public void setVoicePortalLoginMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .VoicePortalLoginMenuKeys value) {
        this.voicePortalLoginMenuKeys = value;
    }

    /**
     * Ruft den Wert der faxMessagingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .FaxMessagingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .FaxMessagingMenuKeys getFaxMessagingMenuKeys() {
        return faxMessagingMenuKeys;
    }

    /**
     * Legt den Wert der faxMessagingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .FaxMessagingMenuKeys }
     *     
     */
    public void setFaxMessagingMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .FaxMessagingMenuKeys value) {
        this.faxMessagingMenuKeys = value;
    }

    /**
     * Ruft den Wert der messageDepositMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .MessageDepositMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .MessageDepositMenuKeys getMessageDepositMenuKeys() {
        return messageDepositMenuKeys;
    }

    /**
     * Legt den Wert der messageDepositMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .MessageDepositMenuKeys }
     *     
     */
    public void setMessageDepositMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .MessageDepositMenuKeys value) {
        this.messageDepositMenuKeys = value;
    }

    /**
     * Ruft den Wert der disableMessageDepositMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .DisableMessageDepositMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .DisableMessageDepositMenuKeys getDisableMessageDepositMenuKeys() {
        return disableMessageDepositMenuKeys;
    }

    /**
     * Legt den Wert der disableMessageDepositMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .DisableMessageDepositMenuKeys }
     *     
     */
    public void setDisableMessageDepositMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .DisableMessageDepositMenuKeys value) {
        this.disableMessageDepositMenuKeys = value;
    }

    /**
     * Ruft den Wert der greetingOnlyForwardingDestinationMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .GreetingOnlyForwardingDestinationMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .GreetingOnlyForwardingDestinationMenuKeys getGreetingOnlyForwardingDestinationMenuKeys() {
        return greetingOnlyForwardingDestinationMenuKeys;
    }

    /**
     * Legt den Wert der greetingOnlyForwardingDestinationMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .GreetingOnlyForwardingDestinationMenuKeys }
     *     
     */
    public void setGreetingOnlyForwardingDestinationMenuKeys(SystemVoiceMessagingGroupGetVoicePortalMenusResponse20 .GreetingOnlyForwardingDestinationMenuKeys value) {
        this.greetingOnlyForwardingDestinationMenuKeys = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="saveMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deleteMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
     *         <element name="callbackCaller" type="{}DigitAny" minOccurs="0"/>
     *         <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="replyMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="forwardMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
     *         <element name="passcode" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "saveMessage",
        "deleteMessage",
        "playEnvelope",
        "callbackCaller",
        "composeMessage",
        "replyMessage",
        "forwardMessage",
        "personalizedName",
        "passcode",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class AdditionalMessageOptionsMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String saveMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String deleteMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String playEnvelope;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String callbackCaller;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String composeMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String replyMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String forwardMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String personalizedName;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String passcode;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der saveMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSaveMessage() {
            return saveMessage;
        }

        /**
         * Legt den Wert der saveMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSaveMessage(String value) {
            this.saveMessage = value;
        }

        /**
         * Ruft den Wert der deleteMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeleteMessage() {
            return deleteMessage;
        }

        /**
         * Legt den Wert der deleteMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeleteMessage(String value) {
            this.deleteMessage = value;
        }

        /**
         * Ruft den Wert der playEnvelope-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPlayEnvelope() {
            return playEnvelope;
        }

        /**
         * Legt den Wert der playEnvelope-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPlayEnvelope(String value) {
            this.playEnvelope = value;
        }

        /**
         * Ruft den Wert der callbackCaller-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCallbackCaller() {
            return callbackCaller;
        }

        /**
         * Legt den Wert der callbackCaller-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCallbackCaller(String value) {
            this.callbackCaller = value;
        }

        /**
         * Ruft den Wert der composeMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getComposeMessage() {
            return composeMessage;
        }

        /**
         * Legt den Wert der composeMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setComposeMessage(String value) {
            this.composeMessage = value;
        }

        /**
         * Ruft den Wert der replyMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReplyMessage() {
            return replyMessage;
        }

        /**
         * Legt den Wert der replyMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReplyMessage(String value) {
            this.replyMessage = value;
        }

        /**
         * Ruft den Wert der forwardMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getForwardMessage() {
            return forwardMessage;
        }

        /**
         * Legt den Wert der forwardMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setForwardMessage(String value) {
            this.forwardMessage = value;
        }

        /**
         * Ruft den Wert der personalizedName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPersonalizedName() {
            return personalizedName;
        }

        /**
         * Legt den Wert der personalizedName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPersonalizedName(String value) {
            this.personalizedName = value;
        }

        /**
         * Ruft den Wert der passcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPasscode() {
            return passcode;
        }

        /**
         * Legt den Wert der passcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPasscode(String value) {
            this.passcode = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="recordAudio" type="{}DigitAny" minOccurs="0"/>
     *         <element name="recordAudioVideo" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "recordAudio",
        "recordAudioVideo",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class AnnouncementMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String recordAudio;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String recordAudioVideo;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der recordAudio-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRecordAudio() {
            return recordAudio;
        }

        /**
         * Legt den Wert der recordAudio-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRecordAudio(String value) {
            this.recordAudio = value;
        }

        /**
         * Ruft den Wert der recordAudioVideo-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRecordAudioVideo() {
            return recordAudioVideo;
        }

        /**
         * Legt den Wert der recordAudioVideo-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRecordAudioVideo(String value) {
            this.recordAudioVideo = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="acceptRecording" type="{}DigitAny"/>
     *         <element name="rejectRerecord" type="{}DigitAny"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="end" type="{}VoicePortalDigitSequence"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acceptRecording",
        "rejectRerecord",
        "returnToPreviousMenu",
        "repeatMenu",
        "end"
    })
    public static class AnnouncementRecordingMenuKeys {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String acceptRecording;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String rejectRerecord;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String end;

        /**
         * Ruft den Wert der acceptRecording-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcceptRecording() {
            return acceptRecording;
        }

        /**
         * Legt den Wert der acceptRecording-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcceptRecording(String value) {
            this.acceptRecording = value;
        }

        /**
         * Ruft den Wert der rejectRerecord-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRejectRerecord() {
            return rejectRerecord;
        }

        /**
         * Legt den Wert der rejectRerecord-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRejectRerecord(String value) {
            this.rejectRerecord = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

        /**
         * Ruft den Wert der end-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEnd() {
            return end;
        }

        /**
         * Legt den Wert der end-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEnd(String value) {
            this.end = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="activateCallForwarding" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deactivateCallForwarding" type="{}DigitAny" minOccurs="0"/>
     *         <element name="changeCallForwardingDestination" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToCallForwardingStatus" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "activateCallForwarding",
        "deactivateCallForwarding",
        "changeCallForwardingDestination",
        "listenToCallForwardingStatus",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class CallForwardingOptionsMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String activateCallForwarding;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String deactivateCallForwarding;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String changeCallForwardingDestination;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String listenToCallForwardingStatus;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der activateCallForwarding-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActivateCallForwarding() {
            return activateCallForwarding;
        }

        /**
         * Legt den Wert der activateCallForwarding-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActivateCallForwarding(String value) {
            this.activateCallForwarding = value;
        }

        /**
         * Ruft den Wert der deactivateCallForwarding-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeactivateCallForwarding() {
            return deactivateCallForwarding;
        }

        /**
         * Legt den Wert der deactivateCallForwarding-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeactivateCallForwarding(String value) {
            this.deactivateCallForwarding = value;
        }

        /**
         * Ruft den Wert der changeCallForwardingDestination-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChangeCallForwardingDestination() {
            return changeCallForwardingDestination;
        }

        /**
         * Legt den Wert der changeCallForwardingDestination-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChangeCallForwardingDestination(String value) {
            this.changeCallForwardingDestination = value;
        }

        /**
         * Ruft den Wert der listenToCallForwardingStatus-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getListenToCallForwardingStatus() {
            return listenToCallForwardingStatus;
        }

        /**
         * Legt den Wert der listenToCallForwardingStatus-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setListenToCallForwardingStatus(String value) {
            this.listenToCallForwardingStatus = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="recordNewGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToCurrentGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="revertToSystemDefaultGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "recordNewGreeting",
        "listenToCurrentGreeting",
        "revertToSystemDefaultGreeting",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class ChangeBusyOrNoAnswerGreetingMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String recordNewGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String listenToCurrentGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String revertToSystemDefaultGreeting;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der recordNewGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRecordNewGreeting() {
            return recordNewGreeting;
        }

        /**
         * Legt den Wert der recordNewGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRecordNewGreeting(String value) {
            this.recordNewGreeting = value;
        }

        /**
         * Ruft den Wert der listenToCurrentGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getListenToCurrentGreeting() {
            return listenToCurrentGreeting;
        }

        /**
         * Legt den Wert der listenToCurrentGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setListenToCurrentGreeting(String value) {
            this.listenToCurrentGreeting = value;
        }

        /**
         * Ruft den Wert der revertToSystemDefaultGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRevertToSystemDefaultGreeting() {
            return revertToSystemDefaultGreeting;
        }

        /**
         * Legt den Wert der revertToSystemDefaultGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRevertToSystemDefaultGreeting(String value) {
            this.revertToSystemDefaultGreeting = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="finishEnteringNewDestinationNumber" type="{}DigitStarPound"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "finishEnteringNewDestinationNumber"
    })
    public static class ChangeCallForwardingDestinationMenuKeys {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String finishEnteringNewDestinationNumber;

        /**
         * Ruft den Wert der finishEnteringNewDestinationNumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFinishEnteringNewDestinationNumber() {
            return finishEnteringNewDestinationNumber;
        }

        /**
         * Legt den Wert der finishEnteringNewDestinationNumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFinishEnteringNewDestinationNumber(String value) {
            this.finishEnteringNewDestinationNumber = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="endRecording" type="{}DigitAny"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "endRecording"
    })
    public static class ChangeCurrentIntroductionOrMessageOrReplyMenuKeys {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String endRecording;

        /**
         * Ruft den Wert der endRecording-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndRecording() {
            return endRecording;
        }

        /**
         * Legt den Wert der endRecording-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndRecording(String value) {
            this.endRecording = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="activateExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deactivateExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="recordNewGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToCurrentGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "activateExtendedAwayGreeting",
        "deactivateExtendedAwayGreeting",
        "recordNewGreeting",
        "listenToCurrentGreeting",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class ChangeExtendedAwayGreetingMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String activateExtendedAwayGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String deactivateExtendedAwayGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String recordNewGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String listenToCurrentGreeting;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der activateExtendedAwayGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActivateExtendedAwayGreeting() {
            return activateExtendedAwayGreeting;
        }

        /**
         * Legt den Wert der activateExtendedAwayGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActivateExtendedAwayGreeting(String value) {
            this.activateExtendedAwayGreeting = value;
        }

        /**
         * Ruft den Wert der deactivateExtendedAwayGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeactivateExtendedAwayGreeting() {
            return deactivateExtendedAwayGreeting;
        }

        /**
         * Legt den Wert der deactivateExtendedAwayGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeactivateExtendedAwayGreeting(String value) {
            this.deactivateExtendedAwayGreeting = value;
        }

        /**
         * Ruft den Wert der recordNewGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRecordNewGreeting() {
            return recordNewGreeting;
        }

        /**
         * Legt den Wert der recordNewGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRecordNewGreeting(String value) {
            this.recordNewGreeting = value;
        }

        /**
         * Ruft den Wert der listenToCurrentGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getListenToCurrentGreeting() {
            return listenToCurrentGreeting;
        }

        /**
         * Legt den Wert der listenToCurrentGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setListenToCurrentGreeting(String value) {
            this.listenToCurrentGreeting = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="activateAvailableInOfficeProfile" type="{}DigitAny" minOccurs="0"/>
     *         <element name="activateAvailableOutOfOfficeProfile" type="{}DigitAny" minOccurs="0"/>
     *         <element name="activateBusyProfile" type="{}DigitAny" minOccurs="0"/>
     *         <element name="activateUnavailableProfile" type="{}DigitAny" minOccurs="0"/>
     *         <element name="noProfile" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "activateAvailableInOfficeProfile",
        "activateAvailableOutOfOfficeProfile",
        "activateBusyProfile",
        "activateUnavailableProfile",
        "noProfile",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class CommPilotExpressProfileMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String activateAvailableInOfficeProfile;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String activateAvailableOutOfOfficeProfile;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String activateBusyProfile;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String activateUnavailableProfile;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String noProfile;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der activateAvailableInOfficeProfile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActivateAvailableInOfficeProfile() {
            return activateAvailableInOfficeProfile;
        }

        /**
         * Legt den Wert der activateAvailableInOfficeProfile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActivateAvailableInOfficeProfile(String value) {
            this.activateAvailableInOfficeProfile = value;
        }

        /**
         * Ruft den Wert der activateAvailableOutOfOfficeProfile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActivateAvailableOutOfOfficeProfile() {
            return activateAvailableOutOfOfficeProfile;
        }

        /**
         * Legt den Wert der activateAvailableOutOfOfficeProfile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActivateAvailableOutOfOfficeProfile(String value) {
            this.activateAvailableOutOfOfficeProfile = value;
        }

        /**
         * Ruft den Wert der activateBusyProfile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActivateBusyProfile() {
            return activateBusyProfile;
        }

        /**
         * Legt den Wert der activateBusyProfile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActivateBusyProfile(String value) {
            this.activateBusyProfile = value;
        }

        /**
         * Ruft den Wert der activateUnavailableProfile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActivateUnavailableProfile() {
            return activateUnavailableProfile;
        }

        /**
         * Legt den Wert der activateUnavailableProfile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActivateUnavailableProfile(String value) {
            this.activateUnavailableProfile = value;
        }

        /**
         * Ruft den Wert der noProfile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNoProfile() {
            return noProfile;
        }

        /**
         * Legt den Wert der noProfile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNoProfile(String value) {
            this.noProfile = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="activateConfGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deactivateConfGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="recordNewConfGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToCurrentConfGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "activateConfGreeting",
        "deactivateConfGreeting",
        "recordNewConfGreeting",
        "listenToCurrentConfGreeting",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class ConferenceGreetingMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String activateConfGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String deactivateConfGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String recordNewConfGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String listenToCurrentConfGreeting;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der activateConfGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActivateConfGreeting() {
            return activateConfGreeting;
        }

        /**
         * Legt den Wert der activateConfGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActivateConfGreeting(String value) {
            this.activateConfGreeting = value;
        }

        /**
         * Ruft den Wert der deactivateConfGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeactivateConfGreeting() {
            return deactivateConfGreeting;
        }

        /**
         * Legt den Wert der deactivateConfGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeactivateConfGreeting(String value) {
            this.deactivateConfGreeting = value;
        }

        /**
         * Ruft den Wert der recordNewConfGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRecordNewConfGreeting() {
            return recordNewConfGreeting;
        }

        /**
         * Legt den Wert der recordNewConfGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRecordNewConfGreeting(String value) {
            this.recordNewConfGreeting = value;
        }

        /**
         * Ruft den Wert der listenToCurrentConfGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getListenToCurrentConfGreeting() {
            return listenToCurrentConfGreeting;
        }

        /**
         * Legt den Wert der listenToCurrentConfGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setListenToCurrentConfGreeting(String value) {
            this.listenToCurrentConfGreeting = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="confirmDeletion" type="{}DigitAny"/>
     *         <element name="cancelDeletion" type="{}DigitAny"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "confirmDeletion",
        "cancelDeletion"
    })
    public static class DeleteAllMessagesMenuKeys {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String confirmDeletion;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String cancelDeletion;

        /**
         * Ruft den Wert der confirmDeletion-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConfirmDeletion() {
            return confirmDeletion;
        }

        /**
         * Legt den Wert der confirmDeletion-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConfirmDeletion(String value) {
            this.confirmDeletion = value;
        }

        /**
         * Ruft den Wert der cancelDeletion-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCancelDeletion() {
            return cancelDeletion;
        }

        /**
         * Legt den Wert der cancelDeletion-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCancelDeletion(String value) {
            this.cancelDeletion = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="disconnectAfterGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="forwardAfterGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="changeForwardingDestination" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "disconnectAfterGreeting",
        "forwardAfterGreeting",
        "changeForwardingDestination",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class DisableMessageDepositMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String disconnectAfterGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String forwardAfterGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String changeForwardingDestination;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der disconnectAfterGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDisconnectAfterGreeting() {
            return disconnectAfterGreeting;
        }

        /**
         * Legt den Wert der disconnectAfterGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisconnectAfterGreeting(String value) {
            this.disconnectAfterGreeting = value;
        }

        /**
         * Ruft den Wert der forwardAfterGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getForwardAfterGreeting() {
            return forwardAfterGreeting;
        }

        /**
         * Legt den Wert der forwardAfterGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setForwardAfterGreeting(String value) {
            this.forwardAfterGreeting = value;
        }

        /**
         * Ruft den Wert der changeForwardingDestination-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChangeForwardingDestination() {
            return changeForwardingDestination;
        }

        /**
         * Legt den Wert der changeForwardingDestination-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChangeForwardingDestination(String value) {
            this.changeForwardingDestination = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="saveFaxMessageAndSkipToNext" type="{}DigitAny" minOccurs="0"/>
     *         <element name="previousFaxMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
     *         <element name="nextFaxMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deleteFaxMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="printFaxMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "saveFaxMessageAndSkipToNext",
        "previousFaxMessage",
        "playEnvelope",
        "nextFaxMessage",
        "deleteFaxMessage",
        "printFaxMessage",
        "returnToPreviousMenu"
    })
    public static class FaxMessagingMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String saveFaxMessageAndSkipToNext;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String previousFaxMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String playEnvelope;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String nextFaxMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String deleteFaxMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String printFaxMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;

        /**
         * Ruft den Wert der saveFaxMessageAndSkipToNext-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSaveFaxMessageAndSkipToNext() {
            return saveFaxMessageAndSkipToNext;
        }

        /**
         * Legt den Wert der saveFaxMessageAndSkipToNext-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSaveFaxMessageAndSkipToNext(String value) {
            this.saveFaxMessageAndSkipToNext = value;
        }

        /**
         * Ruft den Wert der previousFaxMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPreviousFaxMessage() {
            return previousFaxMessage;
        }

        /**
         * Legt den Wert der previousFaxMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPreviousFaxMessage(String value) {
            this.previousFaxMessage = value;
        }

        /**
         * Ruft den Wert der playEnvelope-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPlayEnvelope() {
            return playEnvelope;
        }

        /**
         * Legt den Wert der playEnvelope-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPlayEnvelope(String value) {
            this.playEnvelope = value;
        }

        /**
         * Ruft den Wert der nextFaxMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNextFaxMessage() {
            return nextFaxMessage;
        }

        /**
         * Legt den Wert der nextFaxMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNextFaxMessage(String value) {
            this.nextFaxMessage = value;
        }

        /**
         * Ruft den Wert der deleteFaxMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeleteFaxMessage() {
            return deleteFaxMessage;
        }

        /**
         * Legt den Wert der deleteFaxMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeleteFaxMessage(String value) {
            this.deleteFaxMessage = value;
        }

        /**
         * Ruft den Wert der printFaxMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrintFaxMessage() {
            return printFaxMessage;
        }

        /**
         * Legt den Wert der printFaxMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrintFaxMessage(String value) {
            this.printFaxMessage = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="sendToPerson" type="{}DigitAny" minOccurs="0"/>
     *         <element name="sendToAllGroupMembers" type="{}DigitAny" minOccurs="0"/>
     *         <element name="sendToDistributionList" type="{}DigitAny" minOccurs="0"/>
     *         <element name="changeCurrentIntroductionOrMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToCurrentIntroductionOrMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setOrClearUrgentIndicator" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setOrClearConfidentialIndicator" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sendToPerson",
        "sendToAllGroupMembers",
        "sendToDistributionList",
        "changeCurrentIntroductionOrMessage",
        "listenToCurrentIntroductionOrMessage",
        "setOrClearUrgentIndicator",
        "setOrClearConfidentialIndicator",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class ForwardOrComposeMessageMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String sendToPerson;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String sendToAllGroupMembers;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String sendToDistributionList;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String changeCurrentIntroductionOrMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String listenToCurrentIntroductionOrMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String setOrClearUrgentIndicator;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String setOrClearConfidentialIndicator;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der sendToPerson-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSendToPerson() {
            return sendToPerson;
        }

        /**
         * Legt den Wert der sendToPerson-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSendToPerson(String value) {
            this.sendToPerson = value;
        }

        /**
         * Ruft den Wert der sendToAllGroupMembers-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSendToAllGroupMembers() {
            return sendToAllGroupMembers;
        }

        /**
         * Legt den Wert der sendToAllGroupMembers-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSendToAllGroupMembers(String value) {
            this.sendToAllGroupMembers = value;
        }

        /**
         * Ruft den Wert der sendToDistributionList-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSendToDistributionList() {
            return sendToDistributionList;
        }

        /**
         * Legt den Wert der sendToDistributionList-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSendToDistributionList(String value) {
            this.sendToDistributionList = value;
        }

        /**
         * Ruft den Wert der changeCurrentIntroductionOrMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChangeCurrentIntroductionOrMessage() {
            return changeCurrentIntroductionOrMessage;
        }

        /**
         * Legt den Wert der changeCurrentIntroductionOrMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChangeCurrentIntroductionOrMessage(String value) {
            this.changeCurrentIntroductionOrMessage = value;
        }

        /**
         * Ruft den Wert der listenToCurrentIntroductionOrMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getListenToCurrentIntroductionOrMessage() {
            return listenToCurrentIntroductionOrMessage;
        }

        /**
         * Legt den Wert der listenToCurrentIntroductionOrMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setListenToCurrentIntroductionOrMessage(String value) {
            this.listenToCurrentIntroductionOrMessage = value;
        }

        /**
         * Ruft den Wert der setOrClearUrgentIndicator-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSetOrClearUrgentIndicator() {
            return setOrClearUrgentIndicator;
        }

        /**
         * Legt den Wert der setOrClearUrgentIndicator-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSetOrClearUrgentIndicator(String value) {
            this.setOrClearUrgentIndicator = value;
        }

        /**
         * Ruft den Wert der setOrClearConfidentialIndicator-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSetOrClearConfidentialIndicator() {
            return setOrClearConfidentialIndicator;
        }

        /**
         * Legt den Wert der setOrClearConfidentialIndicator-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSetOrClearConfidentialIndicator(String value) {
            this.setOrClearConfidentialIndicator = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="greetingOnlyForwardingDestination" type="{}DigitStarPound"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "greetingOnlyForwardingDestination"
    })
    public static class GreetingOnlyForwardingDestinationMenuKeys {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String greetingOnlyForwardingDestination;

        /**
         * Ruft den Wert der greetingOnlyForwardingDestination-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGreetingOnlyForwardingDestination() {
            return greetingOnlyForwardingDestination;
        }

        /**
         * Legt den Wert der greetingOnlyForwardingDestination-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGreetingOnlyForwardingDestination(String value) {
            this.greetingOnlyForwardingDestination = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
     *         <element name="conferenceGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "personalizedName",
        "conferenceGreeting",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class GreetingsMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String personalizedName;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String conferenceGreeting;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der personalizedName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPersonalizedName() {
            return personalizedName;
        }

        /**
         * Legt den Wert der personalizedName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPersonalizedName(String value) {
            this.personalizedName = value;
        }

        /**
         * Ruft den Wert der conferenceGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConferenceGreeting() {
            return conferenceGreeting;
        }

        /**
         * Legt den Wert der conferenceGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConferenceGreeting(String value) {
            this.conferenceGreeting = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="checkHostStatus" type="{}DigitAny" minOccurs="0"/>
     *         <element name="associateWithHost" type="{}DigitAny" minOccurs="0"/>
     *         <element name="disassociateFromHost" type="{}DigitAny" minOccurs="0"/>
     *         <element name="disassociateFromRemoteHost" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "checkHostStatus",
        "associateWithHost",
        "disassociateFromHost",
        "disassociateFromRemoteHost",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class HotelingMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String checkHostStatus;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String associateWithHost;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String disassociateFromHost;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String disassociateFromRemoteHost;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der checkHostStatus-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCheckHostStatus() {
            return checkHostStatus;
        }

        /**
         * Legt den Wert der checkHostStatus-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCheckHostStatus(String value) {
            this.checkHostStatus = value;
        }

        /**
         * Ruft den Wert der associateWithHost-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAssociateWithHost() {
            return associateWithHost;
        }

        /**
         * Legt den Wert der associateWithHost-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAssociateWithHost(String value) {
            this.associateWithHost = value;
        }

        /**
         * Ruft den Wert der disassociateFromHost-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDisassociateFromHost() {
            return disassociateFromHost;
        }

        /**
         * Legt den Wert der disassociateFromHost-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisassociateFromHost(String value) {
            this.disassociateFromHost = value;
        }

        /**
         * Ruft den Wert der disassociateFromRemoteHost-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDisassociateFromRemoteHost() {
            return disassociateFromRemoteHost;
        }

        /**
         * Legt den Wert der disassociateFromRemoteHost-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisassociateFromRemoteHost(String value) {
            this.disassociateFromRemoteHost = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="enableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
     *         <element name="disableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToMessageDepositStatus" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "enableMessageDeposit",
        "disableMessageDeposit",
        "listenToMessageDepositStatus",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class MessageDepositMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String enableMessageDeposit;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String disableMessageDeposit;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String listenToMessageDepositStatus;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der enableMessageDeposit-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEnableMessageDeposit() {
            return enableMessageDeposit;
        }

        /**
         * Legt den Wert der enableMessageDeposit-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEnableMessageDeposit(String value) {
            this.enableMessageDeposit = value;
        }

        /**
         * Ruft den Wert der disableMessageDeposit-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDisableMessageDeposit() {
            return disableMessageDeposit;
        }

        /**
         * Legt den Wert der disableMessageDeposit-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisableMessageDeposit(String value) {
            this.disableMessageDeposit = value;
        }

        /**
         * Ruft den Wert der listenToMessageDepositStatus-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getListenToMessageDepositStatus() {
            return listenToMessageDepositStatus;
        }

        /**
         * Legt den Wert der listenToMessageDepositStatus-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setListenToMessageDepositStatus(String value) {
            this.listenToMessageDepositStatus = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="finishEnteringOrReenteringPasscode" type="{}DigitStarPound"/>
     *         <element name="returnToPreviousMenu" type="{}DigitStarPound"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "finishEnteringOrReenteringPasscode",
        "returnToPreviousMenu"
    })
    public static class PasscodeMenuKeys {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String finishEnteringOrReenteringPasscode;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;

        /**
         * Ruft den Wert der finishEnteringOrReenteringPasscode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFinishEnteringOrReenteringPasscode() {
            return finishEnteringOrReenteringPasscode;
        }

        /**
         * Legt den Wert der finishEnteringOrReenteringPasscode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFinishEnteringOrReenteringPasscode(String value) {
            this.finishEnteringOrReenteringPasscode = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="recordNewPersonalizedName" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToCurrentPersonalizedName" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deletePersonalizedName" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "recordNewPersonalizedName",
        "listenToCurrentPersonalizedName",
        "deletePersonalizedName",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class PersonalizedNameMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String recordNewPersonalizedName;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String listenToCurrentPersonalizedName;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String deletePersonalizedName;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der recordNewPersonalizedName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRecordNewPersonalizedName() {
            return recordNewPersonalizedName;
        }

        /**
         * Legt den Wert der recordNewPersonalizedName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRecordNewPersonalizedName(String value) {
            this.recordNewPersonalizedName = value;
        }

        /**
         * Ruft den Wert der listenToCurrentPersonalizedName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getListenToCurrentPersonalizedName() {
            return listenToCurrentPersonalizedName;
        }

        /**
         * Legt den Wert der listenToCurrentPersonalizedName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setListenToCurrentPersonalizedName(String value) {
            this.listenToCurrentPersonalizedName = value;
        }

        /**
         * Ruft den Wert der deletePersonalizedName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeletePersonalizedName() {
            return deletePersonalizedName;
        }

        /**
         * Legt den Wert der deletePersonalizedName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeletePersonalizedName(String value) {
            this.deletePersonalizedName = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="skipBackward" type="{}DigitAny" minOccurs="0"/>
     *         <element name="pauseOrResume" type="{}DigitAny" minOccurs="0"/>
     *         <element name="skipForward" type="{}DigitAny" minOccurs="0"/>
     *         <element name="jumpToBegin" type="{}DigitAny" minOccurs="0"/>
     *         <element name="jumpToEnd" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "skipBackward",
        "pauseOrResume",
        "skipForward",
        "jumpToBegin",
        "jumpToEnd"
    })
    public static class PlayGreetingMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String skipBackward;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String pauseOrResume;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String skipForward;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String jumpToBegin;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String jumpToEnd;

        /**
         * Ruft den Wert der skipBackward-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSkipBackward() {
            return skipBackward;
        }

        /**
         * Legt den Wert der skipBackward-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSkipBackward(String value) {
            this.skipBackward = value;
        }

        /**
         * Ruft den Wert der pauseOrResume-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPauseOrResume() {
            return pauseOrResume;
        }

        /**
         * Legt den Wert der pauseOrResume-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPauseOrResume(String value) {
            this.pauseOrResume = value;
        }

        /**
         * Ruft den Wert der skipForward-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSkipForward() {
            return skipForward;
        }

        /**
         * Legt den Wert der skipForward-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSkipForward(String value) {
            this.skipForward = value;
        }

        /**
         * Ruft den Wert der jumpToBegin-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getJumpToBegin() {
            return jumpToBegin;
        }

        /**
         * Legt den Wert der jumpToBegin-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setJumpToBegin(String value) {
            this.jumpToBegin = value;
        }

        /**
         * Ruft den Wert der jumpToEnd-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getJumpToEnd() {
            return jumpToEnd;
        }

        /**
         * Legt den Wert der jumpToEnd-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setJumpToEnd(String value) {
            this.jumpToEnd = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="skipBackward" type="{}DigitAny" minOccurs="0"/>
     *         <element name="pauseOrResume" type="{}DigitAny" minOccurs="0"/>
     *         <element name="skipForward" type="{}DigitAny" minOccurs="0"/>
     *         <element name="jumpToBegin" type="{}DigitAny" minOccurs="0"/>
     *         <element name="jumpToEnd" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "skipBackward",
        "pauseOrResume",
        "skipForward",
        "jumpToBegin",
        "jumpToEnd"
    })
    public static class PlayMessageMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String skipBackward;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String pauseOrResume;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String skipForward;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String jumpToBegin;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String jumpToEnd;

        /**
         * Ruft den Wert der skipBackward-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSkipBackward() {
            return skipBackward;
        }

        /**
         * Legt den Wert der skipBackward-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSkipBackward(String value) {
            this.skipBackward = value;
        }

        /**
         * Ruft den Wert der pauseOrResume-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPauseOrResume() {
            return pauseOrResume;
        }

        /**
         * Legt den Wert der pauseOrResume-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPauseOrResume(String value) {
            this.pauseOrResume = value;
        }

        /**
         * Ruft den Wert der skipForward-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSkipForward() {
            return skipForward;
        }

        /**
         * Legt den Wert der skipForward-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSkipForward(String value) {
            this.skipForward = value;
        }

        /**
         * Ruft den Wert der jumpToBegin-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getJumpToBegin() {
            return jumpToBegin;
        }

        /**
         * Legt den Wert der jumpToBegin-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setJumpToBegin(String value) {
            this.jumpToBegin = value;
        }

        /**
         * Ruft den Wert der jumpToEnd-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getJumpToEnd() {
            return jumpToEnd;
        }

        /**
         * Legt den Wert der jumpToEnd-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setJumpToEnd(String value) {
            this.jumpToEnd = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="saveMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deleteMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="playMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="previousMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
     *         <element name="nextMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="callbackCaller" type="{}DigitAny" minOccurs="0"/>
     *         <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="replyMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="forwardMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="additionalMessageOptions" type="{}DigitAny" minOccurs="0"/>
     *         <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
     *         <element name="passcode" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "saveMessage",
        "deleteMessage",
        "playMessage",
        "previousMessage",
        "playEnvelope",
        "nextMessage",
        "callbackCaller",
        "composeMessage",
        "replyMessage",
        "forwardMessage",
        "additionalMessageOptions",
        "personalizedName",
        "passcode",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class PlayMessagesMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String saveMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String deleteMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String playMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String previousMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String playEnvelope;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String nextMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String callbackCaller;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String composeMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String replyMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String forwardMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String additionalMessageOptions;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String personalizedName;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String passcode;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der saveMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSaveMessage() {
            return saveMessage;
        }

        /**
         * Legt den Wert der saveMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSaveMessage(String value) {
            this.saveMessage = value;
        }

        /**
         * Ruft den Wert der deleteMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeleteMessage() {
            return deleteMessage;
        }

        /**
         * Legt den Wert der deleteMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeleteMessage(String value) {
            this.deleteMessage = value;
        }

        /**
         * Ruft den Wert der playMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPlayMessage() {
            return playMessage;
        }

        /**
         * Legt den Wert der playMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPlayMessage(String value) {
            this.playMessage = value;
        }

        /**
         * Ruft den Wert der previousMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPreviousMessage() {
            return previousMessage;
        }

        /**
         * Legt den Wert der previousMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPreviousMessage(String value) {
            this.previousMessage = value;
        }

        /**
         * Ruft den Wert der playEnvelope-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPlayEnvelope() {
            return playEnvelope;
        }

        /**
         * Legt den Wert der playEnvelope-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPlayEnvelope(String value) {
            this.playEnvelope = value;
        }

        /**
         * Ruft den Wert der nextMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNextMessage() {
            return nextMessage;
        }

        /**
         * Legt den Wert der nextMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNextMessage(String value) {
            this.nextMessage = value;
        }

        /**
         * Ruft den Wert der callbackCaller-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCallbackCaller() {
            return callbackCaller;
        }

        /**
         * Legt den Wert der callbackCaller-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCallbackCaller(String value) {
            this.callbackCaller = value;
        }

        /**
         * Ruft den Wert der composeMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getComposeMessage() {
            return composeMessage;
        }

        /**
         * Legt den Wert der composeMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setComposeMessage(String value) {
            this.composeMessage = value;
        }

        /**
         * Ruft den Wert der replyMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReplyMessage() {
            return replyMessage;
        }

        /**
         * Legt den Wert der replyMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReplyMessage(String value) {
            this.replyMessage = value;
        }

        /**
         * Ruft den Wert der forwardMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getForwardMessage() {
            return forwardMessage;
        }

        /**
         * Legt den Wert der forwardMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setForwardMessage(String value) {
            this.forwardMessage = value;
        }

        /**
         * Ruft den Wert der additionalMessageOptions-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAdditionalMessageOptions() {
            return additionalMessageOptions;
        }

        /**
         * Legt den Wert der additionalMessageOptions-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAdditionalMessageOptions(String value) {
            this.additionalMessageOptions = value;
        }

        /**
         * Ruft den Wert der personalizedName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPersonalizedName() {
            return personalizedName;
        }

        /**
         * Legt den Wert der personalizedName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPersonalizedName(String value) {
            this.personalizedName = value;
        }

        /**
         * Ruft den Wert der passcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPasscode() {
            return passcode;
        }

        /**
         * Legt den Wert der passcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPasscode(String value) {
            this.passcode = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="endRecording" type="{}DigitAny"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "endRecording"
    })
    public static class RecordNewGreetingOrPersonalizedNameMenuKeys {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String endRecording;

        /**
         * Ruft den Wert der endRecording-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndRecording() {
            return endRecording;
        }

        /**
         * Legt den Wert der endRecording-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndRecording(String value) {
            this.endRecording = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="sendReplyToCaller" type="{}DigitAny"/>
     *         <element name="changeCurrentReply" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToCurrentReply" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setOrClearUrgentIndicator" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setOrClearConfidentialIndicator" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sendReplyToCaller",
        "changeCurrentReply",
        "listenToCurrentReply",
        "setOrClearUrgentIndicator",
        "setOrClearConfidentialIndicator",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class ReplyMessageMenuKeys {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String sendReplyToCaller;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String changeCurrentReply;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String listenToCurrentReply;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String setOrClearUrgentIndicator;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String setOrClearConfidentialIndicator;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der sendReplyToCaller-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSendReplyToCaller() {
            return sendReplyToCaller;
        }

        /**
         * Legt den Wert der sendReplyToCaller-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSendReplyToCaller(String value) {
            this.sendReplyToCaller = value;
        }

        /**
         * Ruft den Wert der changeCurrentReply-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChangeCurrentReply() {
            return changeCurrentReply;
        }

        /**
         * Legt den Wert der changeCurrentReply-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChangeCurrentReply(String value) {
            this.changeCurrentReply = value;
        }

        /**
         * Ruft den Wert der listenToCurrentReply-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getListenToCurrentReply() {
            return listenToCurrentReply;
        }

        /**
         * Legt den Wert der listenToCurrentReply-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setListenToCurrentReply(String value) {
            this.listenToCurrentReply = value;
        }

        /**
         * Ruft den Wert der setOrClearUrgentIndicator-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSetOrClearUrgentIndicator() {
            return setOrClearUrgentIndicator;
        }

        /**
         * Legt den Wert der setOrClearUrgentIndicator-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSetOrClearUrgentIndicator(String value) {
            this.setOrClearUrgentIndicator = value;
        }

        /**
         * Ruft den Wert der setOrClearConfidentialIndicator-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSetOrClearConfidentialIndicator() {
            return setOrClearConfidentialIndicator;
        }

        /**
         * Legt den Wert der setOrClearConfidentialIndicator-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSetOrClearConfidentialIndicator(String value) {
            this.setOrClearConfidentialIndicator = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="interruptPlaybackAndReturnToPreviousMenu" type="{}DigitAny"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "interruptPlaybackAndReturnToPreviousMenu"
    })
    public static class ReviewSelectedDistributionListMenuKeys {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String interruptPlaybackAndReturnToPreviousMenu;

        /**
         * Ruft den Wert der interruptPlaybackAndReturnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInterruptPlaybackAndReturnToPreviousMenu() {
            return interruptPlaybackAndReturnToPreviousMenu;
        }

        /**
         * Legt den Wert der interruptPlaybackAndReturnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInterruptPlaybackAndReturnToPreviousMenu(String value) {
            this.interruptPlaybackAndReturnToPreviousMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="returnToPreviousMenu" type="{}DigitStarPound"/>
     *         <element name="repeatMenuOrFinishEnteringDistributionListNumber" type="{}DigitStarPound" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "returnToPreviousMenu",
        "repeatMenuOrFinishEnteringDistributionListNumber"
    })
    public static class SelectDistributionListMenuKeys {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenuOrFinishEnteringDistributionListNumber;

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenuOrFinishEnteringDistributionListNumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenuOrFinishEnteringDistributionListNumber() {
            return repeatMenuOrFinishEnteringDistributionListNumber;
        }

        /**
         * Legt den Wert der repeatMenuOrFinishEnteringDistributionListNumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenuOrFinishEnteringDistributionListNumber(String value) {
            this.repeatMenuOrFinishEnteringDistributionListNumber = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="confirmSendingToDistributionList" type="{}DigitStarPound" minOccurs="0"/>
     *         <element name="cancelSendingToDistributionList" type="{}DigitStarPound"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "confirmSendingToDistributionList",
        "cancelSendingToDistributionList"
    })
    public static class SendMessageToSelectedDistributionListMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String confirmSendingToDistributionList;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String cancelSendingToDistributionList;

        /**
         * Ruft den Wert der confirmSendingToDistributionList-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConfirmSendingToDistributionList() {
            return confirmSendingToDistributionList;
        }

        /**
         * Legt den Wert der confirmSendingToDistributionList-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConfirmSendingToDistributionList(String value) {
            this.confirmSendingToDistributionList = value;
        }

        /**
         * Ruft den Wert der cancelSendingToDistributionList-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCancelSendingToDistributionList() {
            return cancelSendingToDistributionList;
        }

        /**
         * Legt den Wert der cancelSendingToDistributionList-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCancelSendingToDistributionList(String value) {
            this.cancelSendingToDistributionList = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="confirmSendingToEntireGroup" type="{}DigitAny"/>
     *         <element name="cancelSendingToEntireGroup" type="{}DigitAny"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "confirmSendingToEntireGroup",
        "cancelSendingToEntireGroup"
    })
    public static class SendToAllGroupMembersMenuKeys {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String confirmSendingToEntireGroup;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String cancelSendingToEntireGroup;

        /**
         * Ruft den Wert der confirmSendingToEntireGroup-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConfirmSendingToEntireGroup() {
            return confirmSendingToEntireGroup;
        }

        /**
         * Legt den Wert der confirmSendingToEntireGroup-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConfirmSendingToEntireGroup(String value) {
            this.confirmSendingToEntireGroup = value;
        }

        /**
         * Ruft den Wert der cancelSendingToEntireGroup-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCancelSendingToEntireGroup() {
            return cancelSendingToEntireGroup;
        }

        /**
         * Legt den Wert der cancelSendingToEntireGroup-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCancelSendingToEntireGroup(String value) {
            this.cancelSendingToEntireGroup = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="sendMessageToSelectedDistributionList" type="{}DigitAny"/>
     *         <element name="selectDistributionList" type="{}DigitAny" minOccurs="0"/>
     *         <element name="reviewSelectedDistributionList" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sendMessageToSelectedDistributionList",
        "selectDistributionList",
        "reviewSelectedDistributionList",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class SendToDistributionListMenuKeys {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String sendMessageToSelectedDistributionList;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String selectDistributionList;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String reviewSelectedDistributionList;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der sendMessageToSelectedDistributionList-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSendMessageToSelectedDistributionList() {
            return sendMessageToSelectedDistributionList;
        }

        /**
         * Legt den Wert der sendMessageToSelectedDistributionList-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSendMessageToSelectedDistributionList(String value) {
            this.sendMessageToSelectedDistributionList = value;
        }

        /**
         * Ruft den Wert der selectDistributionList-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSelectDistributionList() {
            return selectDistributionList;
        }

        /**
         * Legt den Wert der selectDistributionList-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSelectDistributionList(String value) {
            this.selectDistributionList = value;
        }

        /**
         * Ruft den Wert der reviewSelectedDistributionList-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReviewSelectedDistributionList() {
            return reviewSelectedDistributionList;
        }

        /**
         * Legt den Wert der reviewSelectedDistributionList-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReviewSelectedDistributionList(String value) {
            this.reviewSelectedDistributionList = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="confirmSendingMessage" type="{}DigitAny"/>
     *         <element name="cancelSendingMessage" type="{}DigitAny"/>
     *         <element name="finishEnteringNumberWhereToSendMessageTo" type="{}DigitStarPound"/>
     *         <element name="finishForwardingOrSendingMessage" type="{}DigitAny"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "confirmSendingMessage",
        "cancelSendingMessage",
        "finishEnteringNumberWhereToSendMessageTo",
        "finishForwardingOrSendingMessage"
    })
    public static class SendToPersonMenuKeys {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String confirmSendingMessage;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String cancelSendingMessage;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String finishEnteringNumberWhereToSendMessageTo;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String finishForwardingOrSendingMessage;

        /**
         * Ruft den Wert der confirmSendingMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConfirmSendingMessage() {
            return confirmSendingMessage;
        }

        /**
         * Legt den Wert der confirmSendingMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConfirmSendingMessage(String value) {
            this.confirmSendingMessage = value;
        }

        /**
         * Ruft den Wert der cancelSendingMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCancelSendingMessage() {
            return cancelSendingMessage;
        }

        /**
         * Legt den Wert der cancelSendingMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCancelSendingMessage(String value) {
            this.cancelSendingMessage = value;
        }

        /**
         * Ruft den Wert der finishEnteringNumberWhereToSendMessageTo-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFinishEnteringNumberWhereToSendMessageTo() {
            return finishEnteringNumberWhereToSendMessageTo;
        }

        /**
         * Legt den Wert der finishEnteringNumberWhereToSendMessageTo-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFinishEnteringNumberWhereToSendMessageTo(String value) {
            this.finishEnteringNumberWhereToSendMessageTo = value;
        }

        /**
         * Ruft den Wert der finishForwardingOrSendingMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFinishForwardingOrSendingMessage() {
            return finishForwardingOrSendingMessage;
        }

        /**
         * Legt den Wert der finishForwardingOrSendingMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFinishForwardingOrSendingMessage(String value) {
            this.finishForwardingOrSendingMessage = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="playMessages" type="{}DigitAny" minOccurs="0"/>
     *         <element name="changeBusyGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="changeNoAnswerGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="changeExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deleteAllMessages" type="{}DigitAny" minOccurs="0"/>
     *         <element name="passcode" type="{}DigitAny" minOccurs="0"/>
     *         <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
     *         <element name="messageDeposit" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "playMessages",
        "changeBusyGreeting",
        "changeNoAnswerGreeting",
        "changeExtendedAwayGreeting",
        "composeMessage",
        "deleteAllMessages",
        "passcode",
        "personalizedName",
        "messageDeposit",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class VoiceMessagingMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String playMessages;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String changeBusyGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String changeNoAnswerGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String changeExtendedAwayGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String composeMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String deleteAllMessages;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String passcode;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String personalizedName;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String messageDeposit;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;

        /**
         * Ruft den Wert der playMessages-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPlayMessages() {
            return playMessages;
        }

        /**
         * Legt den Wert der playMessages-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPlayMessages(String value) {
            this.playMessages = value;
        }

        /**
         * Ruft den Wert der changeBusyGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChangeBusyGreeting() {
            return changeBusyGreeting;
        }

        /**
         * Legt den Wert der changeBusyGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChangeBusyGreeting(String value) {
            this.changeBusyGreeting = value;
        }

        /**
         * Ruft den Wert der changeNoAnswerGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChangeNoAnswerGreeting() {
            return changeNoAnswerGreeting;
        }

        /**
         * Legt den Wert der changeNoAnswerGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChangeNoAnswerGreeting(String value) {
            this.changeNoAnswerGreeting = value;
        }

        /**
         * Ruft den Wert der changeExtendedAwayGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChangeExtendedAwayGreeting() {
            return changeExtendedAwayGreeting;
        }

        /**
         * Legt den Wert der changeExtendedAwayGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChangeExtendedAwayGreeting(String value) {
            this.changeExtendedAwayGreeting = value;
        }

        /**
         * Ruft den Wert der composeMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getComposeMessage() {
            return composeMessage;
        }

        /**
         * Legt den Wert der composeMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setComposeMessage(String value) {
            this.composeMessage = value;
        }

        /**
         * Ruft den Wert der deleteAllMessages-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeleteAllMessages() {
            return deleteAllMessages;
        }

        /**
         * Legt den Wert der deleteAllMessages-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeleteAllMessages(String value) {
            this.deleteAllMessages = value;
        }

        /**
         * Ruft den Wert der passcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPasscode() {
            return passcode;
        }

        /**
         * Legt den Wert der passcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPasscode(String value) {
            this.passcode = value;
        }

        /**
         * Ruft den Wert der personalizedName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPersonalizedName() {
            return personalizedName;
        }

        /**
         * Legt den Wert der personalizedName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPersonalizedName(String value) {
            this.personalizedName = value;
        }

        /**
         * Ruft den Wert der messageDeposit-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMessageDeposit() {
            return messageDeposit;
        }

        /**
         * Legt den Wert der messageDeposit-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMessageDeposit(String value) {
            this.messageDeposit = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="endCurrentCallAndGoBackToPreviousMenu" type="{}VoicePortalDigitSequence"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "endCurrentCallAndGoBackToPreviousMenu",
        "returnToPreviousMenu"
    })
    public static class VoicePortalCallingMenuKeys {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String endCurrentCallAndGoBackToPreviousMenu;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;

        /**
         * Ruft den Wert der endCurrentCallAndGoBackToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndCurrentCallAndGoBackToPreviousMenu() {
            return endCurrentCallAndGoBackToPreviousMenu;
        }

        /**
         * Legt den Wert der endCurrentCallAndGoBackToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndCurrentCallAndGoBackToPreviousMenu(String value) {
            this.endCurrentCallAndGoBackToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="accessUsingOtherMailboxId" type="{}VoicePortalDigitSequence" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accessUsingOtherMailboxId"
    })
    public static class VoicePortalLoginMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String accessUsingOtherMailboxId;

        /**
         * Ruft den Wert der accessUsingOtherMailboxId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccessUsingOtherMailboxId() {
            return accessUsingOtherMailboxId;
        }

        /**
         * Legt den Wert der accessUsingOtherMailboxId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccessUsingOtherMailboxId(String value) {
            this.accessUsingOtherMailboxId = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="voiceMessaging" type="{}DigitAny" minOccurs="0"/>
     *         <element name="commPilotExpressProfile" type="{}DigitAny" minOccurs="0"/>
     *         <element name="greetings" type="{}DigitAny" minOccurs="0"/>
     *         <element name="callForwardingOptions" type="{}DigitAny" minOccurs="0"/>
     *         <element name="voicePortalCalling" type="{}DigitAny" minOccurs="0"/>
     *         <element name="hoteling" type="{}DigitAny" minOccurs="0"/>
     *         <element name="passcode" type="{}DigitAny" minOccurs="0"/>
     *         <element name="exitVoicePortal" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="externalRouting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="announcement" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "voiceMessaging",
        "commPilotExpressProfile",
        "greetings",
        "callForwardingOptions",
        "voicePortalCalling",
        "hoteling",
        "passcode",
        "exitVoicePortal",
        "repeatMenu",
        "externalRouting",
        "announcement"
    })
    public static class VoicePortalMainMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String voiceMessaging;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String commPilotExpressProfile;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String greetings;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String callForwardingOptions;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String voicePortalCalling;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String hoteling;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String passcode;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String exitVoicePortal;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String repeatMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String externalRouting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String announcement;

        /**
         * Ruft den Wert der voiceMessaging-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVoiceMessaging() {
            return voiceMessaging;
        }

        /**
         * Legt den Wert der voiceMessaging-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVoiceMessaging(String value) {
            this.voiceMessaging = value;
        }

        /**
         * Ruft den Wert der commPilotExpressProfile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCommPilotExpressProfile() {
            return commPilotExpressProfile;
        }

        /**
         * Legt den Wert der commPilotExpressProfile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCommPilotExpressProfile(String value) {
            this.commPilotExpressProfile = value;
        }

        /**
         * Ruft den Wert der greetings-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGreetings() {
            return greetings;
        }

        /**
         * Legt den Wert der greetings-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGreetings(String value) {
            this.greetings = value;
        }

        /**
         * Ruft den Wert der callForwardingOptions-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCallForwardingOptions() {
            return callForwardingOptions;
        }

        /**
         * Legt den Wert der callForwardingOptions-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCallForwardingOptions(String value) {
            this.callForwardingOptions = value;
        }

        /**
         * Ruft den Wert der voicePortalCalling-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVoicePortalCalling() {
            return voicePortalCalling;
        }

        /**
         * Legt den Wert der voicePortalCalling-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVoicePortalCalling(String value) {
            this.voicePortalCalling = value;
        }

        /**
         * Ruft den Wert der hoteling-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHoteling() {
            return hoteling;
        }

        /**
         * Legt den Wert der hoteling-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHoteling(String value) {
            this.hoteling = value;
        }

        /**
         * Ruft den Wert der passcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPasscode() {
            return passcode;
        }

        /**
         * Legt den Wert der passcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPasscode(String value) {
            this.passcode = value;
        }

        /**
         * Ruft den Wert der exitVoicePortal-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExitVoicePortal() {
            return exitVoicePortal;
        }

        /**
         * Legt den Wert der exitVoicePortal-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExitVoicePortal(String value) {
            this.exitVoicePortal = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepeatMenu(String value) {
            this.repeatMenu = value;
        }

        /**
         * Ruft den Wert der externalRouting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExternalRouting() {
            return externalRouting;
        }

        /**
         * Legt den Wert der externalRouting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExternalRouting(String value) {
            this.externalRouting = value;
        }

        /**
         * Ruft den Wert der announcement-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAnnouncement() {
            return announcement;
        }

        /**
         * Legt den Wert der announcement-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAnnouncement(String value) {
            this.announcement = value;
        }

    }

}
