//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallBlockingService.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallBlockingService">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="OCP"/>
 *     <enumeration value="EOCP"/>
 *     <enumeration value="ICP"/>
 *     <enumeration value="ACR"/>
 *     <enumeration value="SCR"/>
 *     <enumeration value="SCA"/>
 *     <enumeration value="AAC"/>
 *     <enumeration value="Intercept"/>
 *     <enumeration value="PTT"/>
 *     <enumeration value="Communication Barring"/>
 *     <enumeration value="SAC"/>
 *     <enumeration value="Incoming Communication Barring"/>
 *     <enumeration value="Hierarchical Communication Barring"/>
 *     <enumeration value="Incoming Hierarchical Communication Barring"/>
 *     <enumeration value="BroadWorks Mobility Deny Originations"/>
 *     <enumeration value="BroadWorks Mobility Deny Terminations"/>
 *     <enumeration value="Enterprise Trunk Route Exhaust"/>
 *     <enumeration value="Call Park"/>
 *     <enumeration value="Number Portability"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallBlockingService")
@XmlEnum
public enum CallBlockingService {

    OCP("OCP"),
    EOCP("EOCP"),
    ICP("ICP"),
    ACR("ACR"),
    SCR("SCR"),
    SCA("SCA"),
    AAC("AAC"),
    @XmlEnumValue("Intercept")
    INTERCEPT("Intercept"),
    PTT("PTT"),
    @XmlEnumValue("Communication Barring")
    COMMUNICATION_BARRING("Communication Barring"),
    SAC("SAC"),
    @XmlEnumValue("Incoming Communication Barring")
    INCOMING_COMMUNICATION_BARRING("Incoming Communication Barring"),
    @XmlEnumValue("Hierarchical Communication Barring")
    HIERARCHICAL_COMMUNICATION_BARRING("Hierarchical Communication Barring"),
    @XmlEnumValue("Incoming Hierarchical Communication Barring")
    INCOMING_HIERARCHICAL_COMMUNICATION_BARRING("Incoming Hierarchical Communication Barring"),
    @XmlEnumValue("BroadWorks Mobility Deny Originations")
    BROAD_WORKS_MOBILITY_DENY_ORIGINATIONS("BroadWorks Mobility Deny Originations"),
    @XmlEnumValue("BroadWorks Mobility Deny Terminations")
    BROAD_WORKS_MOBILITY_DENY_TERMINATIONS("BroadWorks Mobility Deny Terminations"),
    @XmlEnumValue("Enterprise Trunk Route Exhaust")
    ENTERPRISE_TRUNK_ROUTE_EXHAUST("Enterprise Trunk Route Exhaust"),
    @XmlEnumValue("Call Park")
    CALL_PARK("Call Park"),
    @XmlEnumValue("Number Portability")
    NUMBER_PORTABILITY("Number Portability");
    private final String value;

    CallBlockingService(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallBlockingService fromValue(String v) {
        for (CallBlockingService c: CallBlockingService.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
