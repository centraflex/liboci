//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeviceManagementEventQueueType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DeviceManagementEventQueueType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Completed"/>
 *     <enumeration value="In Progress Or Pending"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DeviceManagementEventQueueType")
@XmlEnum
public enum DeviceManagementEventQueueType {

    @XmlEnumValue("Completed")
    COMPLETED("Completed"),
    @XmlEnumValue("In Progress Or Pending")
    IN_PROGRESS_OR_PENDING("In Progress Or Pending");
    private final String value;

    DeviceManagementEventQueueType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeviceManagementEventQueueType fromValue(String v) {
        for (DeviceManagementEventQueueType c: DeviceManagementEventQueueType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
