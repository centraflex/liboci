//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Gets an existing non-default Call Center Agent Threshold Profile in a Group.
 *         
 *         If the responsePagingControl element is not provided, the paging startIndex will be set to 1 
 *         by default, and the responsePageSize will be set to the maximum ResponsePageSize by default.
 *         It is possible to search by various criteria to restrict the number of rows returned.
 *         
 *         If no sortOrder is included the response is sorted by User Id ascending by default.
 *         
 *         Multiple search criteria are logically ANDed together unless the searchCriteriaModeOr option is included.
 *         Then the search criteria are logically ORed together.
 *         
 *         The response is either a GroupCallCenterAgentThresholdProfileGetPagedSortedResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterAgentThresholdProfileGetPagedSortedRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterAgentThresholdProfileGetPagedSortedRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="profileName" type="{}CallCenterAgentThresholdProfileName"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl" minOccurs="0"/>
 *         <element name="sortOrder" type="{}SortOrderGroupCallCenterAgentThresholdProfileGetPagedSorted" maxOccurs="3" minOccurs="0"/>
 *         <element name="searchCriteriaUserId" type="{}SearchCriteriaUserId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaUserLastName" type="{}SearchCriteriaUserLastName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaUserFirstName" type="{}SearchCriteriaUserFirstName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDn" type="{}SearchCriteriaDn" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExtension" type="{}SearchCriteriaExtension" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDepartmentName" type="{}SearchCriteriaDepartmentName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaEmailAddress" type="{}SearchCriteriaEmailAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaModeOr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterAgentThresholdProfileGetPagedSortedRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "profileName",
    "responsePagingControl",
    "sortOrder",
    "searchCriteriaUserId",
    "searchCriteriaUserLastName",
    "searchCriteriaUserFirstName",
    "searchCriteriaDn",
    "searchCriteriaExtension",
    "searchCriteriaDepartmentName",
    "searchCriteriaEmailAddress",
    "searchCriteriaModeOr"
})
public class GroupCallCenterAgentThresholdProfileGetPagedSortedRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String profileName;
    protected ResponsePagingControl responsePagingControl;
    protected List<SortOrderGroupCallCenterAgentThresholdProfileGetPagedSorted> sortOrder;
    protected List<SearchCriteriaUserId> searchCriteriaUserId;
    protected List<SearchCriteriaUserLastName> searchCriteriaUserLastName;
    protected List<SearchCriteriaUserFirstName> searchCriteriaUserFirstName;
    protected List<SearchCriteriaDn> searchCriteriaDn;
    protected List<SearchCriteriaExtension> searchCriteriaExtension;
    protected List<SearchCriteriaDepartmentName> searchCriteriaDepartmentName;
    protected List<SearchCriteriaEmailAddress> searchCriteriaEmailAddress;
    protected Boolean searchCriteriaModeOr;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der profileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileName() {
        return profileName;
    }

    /**
     * Legt den Wert der profileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileName(String value) {
        this.profileName = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Gets the value of the sortOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the sortOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSortOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SortOrderGroupCallCenterAgentThresholdProfileGetPagedSorted }
     * 
     * 
     * @return
     *     The value of the sortOrder property.
     */
    public List<SortOrderGroupCallCenterAgentThresholdProfileGetPagedSorted> getSortOrder() {
        if (sortOrder == null) {
            sortOrder = new ArrayList<>();
        }
        return this.sortOrder;
    }

    /**
     * Gets the value of the searchCriteriaUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserId property.
     */
    public List<SearchCriteriaUserId> getSearchCriteriaUserId() {
        if (searchCriteriaUserId == null) {
            searchCriteriaUserId = new ArrayList<>();
        }
        return this.searchCriteriaUserId;
    }

    /**
     * Gets the value of the searchCriteriaUserLastName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserLastName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserLastName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserLastName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserLastName property.
     */
    public List<SearchCriteriaUserLastName> getSearchCriteriaUserLastName() {
        if (searchCriteriaUserLastName == null) {
            searchCriteriaUserLastName = new ArrayList<>();
        }
        return this.searchCriteriaUserLastName;
    }

    /**
     * Gets the value of the searchCriteriaUserFirstName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserFirstName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserFirstName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserFirstName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserFirstName property.
     */
    public List<SearchCriteriaUserFirstName> getSearchCriteriaUserFirstName() {
        if (searchCriteriaUserFirstName == null) {
            searchCriteriaUserFirstName = new ArrayList<>();
        }
        return this.searchCriteriaUserFirstName;
    }

    /**
     * Gets the value of the searchCriteriaDn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDn }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDn property.
     */
    public List<SearchCriteriaDn> getSearchCriteriaDn() {
        if (searchCriteriaDn == null) {
            searchCriteriaDn = new ArrayList<>();
        }
        return this.searchCriteriaDn;
    }

    /**
     * Gets the value of the searchCriteriaExtension property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaExtension property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaExtension().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaExtension }
     * 
     * 
     * @return
     *     The value of the searchCriteriaExtension property.
     */
    public List<SearchCriteriaExtension> getSearchCriteriaExtension() {
        if (searchCriteriaExtension == null) {
            searchCriteriaExtension = new ArrayList<>();
        }
        return this.searchCriteriaExtension;
    }

    /**
     * Gets the value of the searchCriteriaDepartmentName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDepartmentName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDepartmentName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDepartmentName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDepartmentName property.
     */
    public List<SearchCriteriaDepartmentName> getSearchCriteriaDepartmentName() {
        if (searchCriteriaDepartmentName == null) {
            searchCriteriaDepartmentName = new ArrayList<>();
        }
        return this.searchCriteriaDepartmentName;
    }

    /**
     * Gets the value of the searchCriteriaEmailAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaEmailAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaEmailAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaEmailAddress }
     * 
     * 
     * @return
     *     The value of the searchCriteriaEmailAddress property.
     */
    public List<SearchCriteriaEmailAddress> getSearchCriteriaEmailAddress() {
        if (searchCriteriaEmailAddress == null) {
            searchCriteriaEmailAddress = new ArrayList<>();
        }
        return this.searchCriteriaEmailAddress;
    }

    /**
     * Ruft den Wert der searchCriteriaModeOr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchCriteriaModeOr() {
        return searchCriteriaModeOr;
    }

    /**
     * Legt den Wert der searchCriteriaModeOr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchCriteriaModeOr(Boolean value) {
        this.searchCriteriaModeOr = value;
    }

}
