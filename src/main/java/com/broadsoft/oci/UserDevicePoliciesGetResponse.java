//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *           Response to UserDevicePoliciesGetRequest. enableDeviceFeatureSynchronization is ignored by the application server in Multiple User Shared mode.
 *         
 * 
 * <p>Java-Klasse für UserDevicePoliciesGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserDevicePoliciesGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="lineMode" type="{}UserDevicePolicyLineMode"/>
 *         <element name="enableDeviceFeatureSynchronization" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDevicePoliciesGetResponse", propOrder = {
    "lineMode",
    "enableDeviceFeatureSynchronization"
})
public class UserDevicePoliciesGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected UserDevicePolicyLineMode lineMode;
    protected boolean enableDeviceFeatureSynchronization;

    /**
     * Ruft den Wert der lineMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserDevicePolicyLineMode }
     *     
     */
    public UserDevicePolicyLineMode getLineMode() {
        return lineMode;
    }

    /**
     * Legt den Wert der lineMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDevicePolicyLineMode }
     *     
     */
    public void setLineMode(UserDevicePolicyLineMode value) {
        this.lineMode = value;
    }

    /**
     * Ruft den Wert der enableDeviceFeatureSynchronization-Eigenschaft ab.
     * 
     */
    public boolean isEnableDeviceFeatureSynchronization() {
        return enableDeviceFeatureSynchronization;
    }

    /**
     * Legt den Wert der enableDeviceFeatureSynchronization-Eigenschaft fest.
     * 
     */
    public void setEnableDeviceFeatureSynchronization(boolean value) {
        this.enableDeviceFeatureSynchronization = value;
    }

}
