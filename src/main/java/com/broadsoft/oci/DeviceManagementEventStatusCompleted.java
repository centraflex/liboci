//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeviceManagementEventStatusCompleted.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DeviceManagementEventStatusCompleted">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Canceled"/>
 *     <enumeration value="Completed"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DeviceManagementEventStatusCompleted")
@XmlEnum
public enum DeviceManagementEventStatusCompleted {

    @XmlEnumValue("Canceled")
    CANCELED("Canceled"),
    @XmlEnumValue("Completed")
    COMPLETED("Completed");
    private final String value;

    DeviceManagementEventStatusCompleted(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeviceManagementEventStatusCompleted fromValue(String v) {
        for (DeviceManagementEventStatusCompleted c: DeviceManagementEventStatusCompleted.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
