//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the call center list for an agent.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserCallCenterModifyCallCenterListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallCenterModifyCallCenterListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="agentUserId" type="{}UserId"/>
 *         <element name="serviceUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallCenterModifyCallCenterListRequest", propOrder = {
    "agentUserId",
    "serviceUserIdList"
})
public class UserCallCenterModifyCallCenterListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String agentUserId;
    @XmlElementRef(name = "serviceUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> serviceUserIdList;

    /**
     * Ruft den Wert der agentUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentUserId() {
        return agentUserId;
    }

    /**
     * Legt den Wert der agentUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentUserId(String value) {
        this.agentUserId = value;
    }

    /**
     * Ruft den Wert der serviceUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getServiceUserIdList() {
        return serviceUserIdList;
    }

    /**
     * Legt den Wert der serviceUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setServiceUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.serviceUserIdList = value;
    }

}
