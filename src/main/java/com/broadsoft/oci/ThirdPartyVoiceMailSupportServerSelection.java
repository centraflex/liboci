//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ThirdPartyVoiceMailSupportServerSelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ThirdPartyVoiceMailSupportServerSelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Group Mail Server"/>
 *     <enumeration value="User Specific Mail Server"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ThirdPartyVoiceMailSupportServerSelection")
@XmlEnum
public enum ThirdPartyVoiceMailSupportServerSelection {

    @XmlEnumValue("Group Mail Server")
    GROUP_MAIL_SERVER("Group Mail Server"),
    @XmlEnumValue("User Specific Mail Server")
    USER_SPECIFIC_MAIL_SERVER("User Specific Mail Server");
    private final String value;

    ThirdPartyVoiceMailSupportServerSelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ThirdPartyVoiceMailSupportServerSelection fromValue(String v) {
        for (ThirdPartyVoiceMailSupportServerSelection c: ThirdPartyVoiceMailSupportServerSelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
