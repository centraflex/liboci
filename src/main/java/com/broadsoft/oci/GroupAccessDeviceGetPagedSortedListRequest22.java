//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Requests the list of access devices in a group.
 *         The response is either GroupAccessDeviceGetPagedSortedListResponse or ErrorResponse.
 *         If no sortOrder is included, the response is sorted by Device Name ascending
 *         by default.  If the responsePagingControl element is not provided, the paging startIndex 
 *         will be set to 1 by default, and the responsePageSize will be set to the maximum 
 *         responsePageSize by default.
 *         Multiple search criteria are logically ANDed together unless the 
 *         searchCriteriaModeOr option is included. Then the search criteria are logically 
 *         ORed together.
 *         The getVisualDeviceManagementAPIDevicesOnly element can only be set when CloudPBX is licenced.
 *         If the getVisualDeviceManagementAPIDevicesOnly element is not set, all devices are returned with Visual Device Management API flag set to empty.
 *         If the getVisualDeviceManagementAPIDevicesOnly element is set to true, only SIP devices supporting Visual Device Management API are returned.
 *         If the getVisualDeviceManagementAPIDevicesOnly element is set to false, all SIP devices are returned with their current Visual Device Management API flag value.
 *       
 * 
 * <p>Java-Klasse für GroupAccessDeviceGetPagedSortedListRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAccessDeviceGetPagedSortedListRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="includeTotalNumberOfRows" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl" minOccurs="0"/>
 *         <element name="sortOrder" type="{}SortOrderGroupAccessDeviceGetPagedSortedList" maxOccurs="3" minOccurs="0"/>
 *         <element name="searchCriteriaDeviceName" type="{}SearchCriteriaDeviceName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDeviceMACAddress" type="{}SearchCriteriaDeviceMACAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDeviceNetAddress" type="{}SearchCriteriaDeviceNetAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactDeviceType" type="{}SearchCriteriaExactDeviceType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaAccessDeviceVersion" type="{}SearchCriteriaAccessDeviceVersion" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaModeOr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="getVisualDeviceManagementAPIDevicesOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAccessDeviceGetPagedSortedListRequest22", propOrder = {
    "serviceProviderId",
    "groupId",
    "includeTotalNumberOfRows",
    "responsePagingControl",
    "sortOrder",
    "searchCriteriaDeviceName",
    "searchCriteriaDeviceMACAddress",
    "searchCriteriaDeviceNetAddress",
    "searchCriteriaExactDeviceType",
    "searchCriteriaAccessDeviceVersion",
    "searchCriteriaModeOr",
    "getVisualDeviceManagementAPIDevicesOnly"
})
public class GroupAccessDeviceGetPagedSortedListRequest22
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected Boolean includeTotalNumberOfRows;
    protected ResponsePagingControl responsePagingControl;
    protected List<SortOrderGroupAccessDeviceGetPagedSortedList> sortOrder;
    protected List<SearchCriteriaDeviceName> searchCriteriaDeviceName;
    protected List<SearchCriteriaDeviceMACAddress> searchCriteriaDeviceMACAddress;
    protected List<SearchCriteriaDeviceNetAddress> searchCriteriaDeviceNetAddress;
    protected List<SearchCriteriaExactDeviceType> searchCriteriaExactDeviceType;
    protected List<SearchCriteriaAccessDeviceVersion> searchCriteriaAccessDeviceVersion;
    protected Boolean searchCriteriaModeOr;
    protected Boolean getVisualDeviceManagementAPIDevicesOnly;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der includeTotalNumberOfRows-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeTotalNumberOfRows() {
        return includeTotalNumberOfRows;
    }

    /**
     * Legt den Wert der includeTotalNumberOfRows-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeTotalNumberOfRows(Boolean value) {
        this.includeTotalNumberOfRows = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Gets the value of the sortOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the sortOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSortOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SortOrderGroupAccessDeviceGetPagedSortedList }
     * 
     * 
     * @return
     *     The value of the sortOrder property.
     */
    public List<SortOrderGroupAccessDeviceGetPagedSortedList> getSortOrder() {
        if (sortOrder == null) {
            sortOrder = new ArrayList<>();
        }
        return this.sortOrder;
    }

    /**
     * Gets the value of the searchCriteriaDeviceName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDeviceName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDeviceName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDeviceName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDeviceName property.
     */
    public List<SearchCriteriaDeviceName> getSearchCriteriaDeviceName() {
        if (searchCriteriaDeviceName == null) {
            searchCriteriaDeviceName = new ArrayList<>();
        }
        return this.searchCriteriaDeviceName;
    }

    /**
     * Gets the value of the searchCriteriaDeviceMACAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDeviceMACAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDeviceMACAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDeviceMACAddress }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDeviceMACAddress property.
     */
    public List<SearchCriteriaDeviceMACAddress> getSearchCriteriaDeviceMACAddress() {
        if (searchCriteriaDeviceMACAddress == null) {
            searchCriteriaDeviceMACAddress = new ArrayList<>();
        }
        return this.searchCriteriaDeviceMACAddress;
    }

    /**
     * Gets the value of the searchCriteriaDeviceNetAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDeviceNetAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDeviceNetAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDeviceNetAddress }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDeviceNetAddress property.
     */
    public List<SearchCriteriaDeviceNetAddress> getSearchCriteriaDeviceNetAddress() {
        if (searchCriteriaDeviceNetAddress == null) {
            searchCriteriaDeviceNetAddress = new ArrayList<>();
        }
        return this.searchCriteriaDeviceNetAddress;
    }

    /**
     * Gets the value of the searchCriteriaExactDeviceType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaExactDeviceType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaExactDeviceType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaExactDeviceType }
     * 
     * 
     * @return
     *     The value of the searchCriteriaExactDeviceType property.
     */
    public List<SearchCriteriaExactDeviceType> getSearchCriteriaExactDeviceType() {
        if (searchCriteriaExactDeviceType == null) {
            searchCriteriaExactDeviceType = new ArrayList<>();
        }
        return this.searchCriteriaExactDeviceType;
    }

    /**
     * Gets the value of the searchCriteriaAccessDeviceVersion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaAccessDeviceVersion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaAccessDeviceVersion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaAccessDeviceVersion }
     * 
     * 
     * @return
     *     The value of the searchCriteriaAccessDeviceVersion property.
     */
    public List<SearchCriteriaAccessDeviceVersion> getSearchCriteriaAccessDeviceVersion() {
        if (searchCriteriaAccessDeviceVersion == null) {
            searchCriteriaAccessDeviceVersion = new ArrayList<>();
        }
        return this.searchCriteriaAccessDeviceVersion;
    }

    /**
     * Ruft den Wert der searchCriteriaModeOr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchCriteriaModeOr() {
        return searchCriteriaModeOr;
    }

    /**
     * Legt den Wert der searchCriteriaModeOr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchCriteriaModeOr(Boolean value) {
        this.searchCriteriaModeOr = value;
    }

    /**
     * Ruft den Wert der getVisualDeviceManagementAPIDevicesOnly-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGetVisualDeviceManagementAPIDevicesOnly() {
        return getVisualDeviceManagementAPIDevicesOnly;
    }

    /**
     * Legt den Wert der getVisualDeviceManagementAPIDevicesOnly-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGetVisualDeviceManagementAPIDevicesOnly(Boolean value) {
        this.getVisualDeviceManagementAPIDevicesOnly = value;
    }

}
