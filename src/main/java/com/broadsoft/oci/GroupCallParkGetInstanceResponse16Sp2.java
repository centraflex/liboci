//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupCallParkGetInstanceRequest16sp2.
 *         Contains a table with column headings: "User Id", "Last Name", "First Name", "Hiragana Last Name", 
 *         "Hiragana First Name", "Phone Number", "Extension", "Department", "Email Address".  
 *         The users are in the table are in the order they will try to be parked on.
 *       
 * 
 * <p>Java-Klasse für GroupCallParkGetInstanceResponse16sp2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallParkGetInstanceResponse16sp2">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="recallAlternateUserId" type="{}UserId" minOccurs="0"/>
 *         <element name="recallTo" type="{}CallParkRecallTo"/>
 *         <element name="userTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallParkGetInstanceResponse16sp2", propOrder = {
    "recallAlternateUserId",
    "recallTo",
    "userTable"
})
public class GroupCallParkGetInstanceResponse16Sp2
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String recallAlternateUserId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallParkRecallTo recallTo;
    @XmlElement(required = true)
    protected OCITable userTable;

    /**
     * Ruft den Wert der recallAlternateUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecallAlternateUserId() {
        return recallAlternateUserId;
    }

    /**
     * Legt den Wert der recallAlternateUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecallAlternateUserId(String value) {
        this.recallAlternateUserId = value;
    }

    /**
     * Ruft den Wert der recallTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallParkRecallTo }
     *     
     */
    public CallParkRecallTo getRecallTo() {
        return recallTo;
    }

    /**
     * Legt den Wert der recallTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallParkRecallTo }
     *     
     */
    public void setRecallTo(CallParkRecallTo value) {
        this.recallTo = value;
    }

    /**
     * Ruft den Wert der userTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserTable() {
        return userTable;
    }

    /**
     * Legt den Wert der userTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserTable(OCITable value) {
        this.userTable = value;
    }

}
