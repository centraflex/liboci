//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemRoutingProfileParametersGetRequest.
 *         Contains a list of Routing Profile parameters.
 *       
 * 
 * <p>Java-Klasse für SystemRoutingProfileParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemRoutingProfileParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enablePermissiveRouting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemRoutingProfileParametersGetResponse", propOrder = {
    "enablePermissiveRouting"
})
public class SystemRoutingProfileParametersGetResponse
    extends OCIDataResponse
{

    protected boolean enablePermissiveRouting;

    /**
     * Ruft den Wert der enablePermissiveRouting-Eigenschaft ab.
     * 
     */
    public boolean isEnablePermissiveRouting() {
        return enablePermissiveRouting;
    }

    /**
     * Legt den Wert der enablePermissiveRouting-Eigenschaft fest.
     * 
     */
    public void setEnablePermissiveRouting(boolean value) {
        this.enablePermissiveRouting = value;
    }

}
