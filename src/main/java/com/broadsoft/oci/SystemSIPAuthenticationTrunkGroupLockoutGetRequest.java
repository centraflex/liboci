//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *          Request to get sip authentication trunk group lockout data in the system.
 *          The response is either a SystemSIPAuthenticationTrunkGroupLockoutGetResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemSIPAuthenticationTrunkGroupLockoutGetRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPAuthenticationTrunkGroupLockoutGetRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaServiceProviderId" type="{}SearchCriteriaServiceProviderId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactOrganizationType" type="{}SearchCriteriaExactOrganizationType" minOccurs="0"/>
 *         <element name="searchCriteriaGroupId" type="{}SearchCriteriaGroupId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaTrunkGroupName" type="{}SearchCriteriaTrunkGroupName" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPAuthenticationTrunkGroupLockoutGetRequest", propOrder = {
    "responseSizeLimit",
    "searchCriteriaServiceProviderId",
    "searchCriteriaExactOrganizationType",
    "searchCriteriaGroupId",
    "searchCriteriaTrunkGroupName"
})
public class SystemSIPAuthenticationTrunkGroupLockoutGetRequest
    extends OCIRequest
{

    protected Integer responseSizeLimit;
    protected List<SearchCriteriaServiceProviderId> searchCriteriaServiceProviderId;
    protected SearchCriteriaExactOrganizationType searchCriteriaExactOrganizationType;
    protected List<SearchCriteriaGroupId> searchCriteriaGroupId;
    protected List<SearchCriteriaTrunkGroupName> searchCriteriaTrunkGroupName;

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaServiceProviderId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaServiceProviderId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaServiceProviderId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaServiceProviderId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaServiceProviderId property.
     */
    public List<SearchCriteriaServiceProviderId> getSearchCriteriaServiceProviderId() {
        if (searchCriteriaServiceProviderId == null) {
            searchCriteriaServiceProviderId = new ArrayList<>();
        }
        return this.searchCriteriaServiceProviderId;
    }

    /**
     * Ruft den Wert der searchCriteriaExactOrganizationType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactOrganizationType }
     *     
     */
    public SearchCriteriaExactOrganizationType getSearchCriteriaExactOrganizationType() {
        return searchCriteriaExactOrganizationType;
    }

    /**
     * Legt den Wert der searchCriteriaExactOrganizationType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactOrganizationType }
     *     
     */
    public void setSearchCriteriaExactOrganizationType(SearchCriteriaExactOrganizationType value) {
        this.searchCriteriaExactOrganizationType = value;
    }

    /**
     * Gets the value of the searchCriteriaGroupId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaGroupId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaGroupId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaGroupId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaGroupId property.
     */
    public List<SearchCriteriaGroupId> getSearchCriteriaGroupId() {
        if (searchCriteriaGroupId == null) {
            searchCriteriaGroupId = new ArrayList<>();
        }
        return this.searchCriteriaGroupId;
    }

    /**
     * Gets the value of the searchCriteriaTrunkGroupName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaTrunkGroupName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaTrunkGroupName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaTrunkGroupName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaTrunkGroupName property.
     */
    public List<SearchCriteriaTrunkGroupName> getSearchCriteriaTrunkGroupName() {
        if (searchCriteriaTrunkGroupName == null) {
            searchCriteriaTrunkGroupName = new ArrayList<>();
        }
        return this.searchCriteriaTrunkGroupName;
    }

}
