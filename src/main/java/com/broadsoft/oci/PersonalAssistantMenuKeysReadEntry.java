//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal personal assistant menu keys.
 *       
 * 
 * <p>Java-Klasse für PersonalAssistantMenuKeysReadEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="PersonalAssistantMenuKeysReadEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="setPresenceToNone" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToBusinessTrip" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToGoneForTheDay" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToLunch" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToMeeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToOutOfOffice" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToTemporarilyOut" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToTraining" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToUnavailable" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToVacation" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonalAssistantMenuKeysReadEntry", propOrder = {
    "setPresenceToNone",
    "setPresenceToBusinessTrip",
    "setPresenceToGoneForTheDay",
    "setPresenceToLunch",
    "setPresenceToMeeting",
    "setPresenceToOutOfOffice",
    "setPresenceToTemporarilyOut",
    "setPresenceToTraining",
    "setPresenceToUnavailable",
    "setPresenceToVacation",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class PersonalAssistantMenuKeysReadEntry {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String setPresenceToNone;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String setPresenceToBusinessTrip;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String setPresenceToGoneForTheDay;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String setPresenceToLunch;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String setPresenceToMeeting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String setPresenceToOutOfOffice;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String setPresenceToTemporarilyOut;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String setPresenceToTraining;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String setPresenceToUnavailable;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String setPresenceToVacation;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String repeatMenu;

    /**
     * Ruft den Wert der setPresenceToNone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetPresenceToNone() {
        return setPresenceToNone;
    }

    /**
     * Legt den Wert der setPresenceToNone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetPresenceToNone(String value) {
        this.setPresenceToNone = value;
    }

    /**
     * Ruft den Wert der setPresenceToBusinessTrip-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetPresenceToBusinessTrip() {
        return setPresenceToBusinessTrip;
    }

    /**
     * Legt den Wert der setPresenceToBusinessTrip-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetPresenceToBusinessTrip(String value) {
        this.setPresenceToBusinessTrip = value;
    }

    /**
     * Ruft den Wert der setPresenceToGoneForTheDay-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetPresenceToGoneForTheDay() {
        return setPresenceToGoneForTheDay;
    }

    /**
     * Legt den Wert der setPresenceToGoneForTheDay-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetPresenceToGoneForTheDay(String value) {
        this.setPresenceToGoneForTheDay = value;
    }

    /**
     * Ruft den Wert der setPresenceToLunch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetPresenceToLunch() {
        return setPresenceToLunch;
    }

    /**
     * Legt den Wert der setPresenceToLunch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetPresenceToLunch(String value) {
        this.setPresenceToLunch = value;
    }

    /**
     * Ruft den Wert der setPresenceToMeeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetPresenceToMeeting() {
        return setPresenceToMeeting;
    }

    /**
     * Legt den Wert der setPresenceToMeeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetPresenceToMeeting(String value) {
        this.setPresenceToMeeting = value;
    }

    /**
     * Ruft den Wert der setPresenceToOutOfOffice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetPresenceToOutOfOffice() {
        return setPresenceToOutOfOffice;
    }

    /**
     * Legt den Wert der setPresenceToOutOfOffice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetPresenceToOutOfOffice(String value) {
        this.setPresenceToOutOfOffice = value;
    }

    /**
     * Ruft den Wert der setPresenceToTemporarilyOut-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetPresenceToTemporarilyOut() {
        return setPresenceToTemporarilyOut;
    }

    /**
     * Legt den Wert der setPresenceToTemporarilyOut-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetPresenceToTemporarilyOut(String value) {
        this.setPresenceToTemporarilyOut = value;
    }

    /**
     * Ruft den Wert der setPresenceToTraining-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetPresenceToTraining() {
        return setPresenceToTraining;
    }

    /**
     * Legt den Wert der setPresenceToTraining-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetPresenceToTraining(String value) {
        this.setPresenceToTraining = value;
    }

    /**
     * Ruft den Wert der setPresenceToUnavailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetPresenceToUnavailable() {
        return setPresenceToUnavailable;
    }

    /**
     * Legt den Wert der setPresenceToUnavailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetPresenceToUnavailable(String value) {
        this.setPresenceToUnavailable = value;
    }

    /**
     * Ruft den Wert der setPresenceToVacation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetPresenceToVacation() {
        return setPresenceToVacation;
    }

    /**
     * Legt den Wert der setPresenceToVacation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetPresenceToVacation(String value) {
        this.setPresenceToVacation = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepeatMenu(String value) {
        this.repeatMenu = value;
    }

}
