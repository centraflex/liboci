//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the user's Polycom Phone Services attributes.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserPolycomPhoneServicesModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPolycomPhoneServicesModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="accessDevice" type="{}AccessDevice"/>
 *         <element name="integratePhoneDirectoryWithBroadWorks" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeUserPersonalPhoneListInDirectory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeGroupCustomContactDirectoryInDirectory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="groupCustomContactDirectory" type="{}CustomContactDirectoryName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPolycomPhoneServicesModifyRequest", propOrder = {
    "userId",
    "accessDevice",
    "integratePhoneDirectoryWithBroadWorks",
    "includeUserPersonalPhoneListInDirectory",
    "includeGroupCustomContactDirectoryInDirectory",
    "groupCustomContactDirectory"
})
public class UserPolycomPhoneServicesModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    protected AccessDevice accessDevice;
    protected Boolean integratePhoneDirectoryWithBroadWorks;
    protected Boolean includeUserPersonalPhoneListInDirectory;
    protected Boolean includeGroupCustomContactDirectoryInDirectory;
    @XmlElementRef(name = "groupCustomContactDirectory", type = JAXBElement.class, required = false)
    protected JAXBElement<String> groupCustomContactDirectory;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der accessDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDevice }
     *     
     */
    public AccessDevice getAccessDevice() {
        return accessDevice;
    }

    /**
     * Legt den Wert der accessDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDevice }
     *     
     */
    public void setAccessDevice(AccessDevice value) {
        this.accessDevice = value;
    }

    /**
     * Ruft den Wert der integratePhoneDirectoryWithBroadWorks-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIntegratePhoneDirectoryWithBroadWorks() {
        return integratePhoneDirectoryWithBroadWorks;
    }

    /**
     * Legt den Wert der integratePhoneDirectoryWithBroadWorks-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIntegratePhoneDirectoryWithBroadWorks(Boolean value) {
        this.integratePhoneDirectoryWithBroadWorks = value;
    }

    /**
     * Ruft den Wert der includeUserPersonalPhoneListInDirectory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeUserPersonalPhoneListInDirectory() {
        return includeUserPersonalPhoneListInDirectory;
    }

    /**
     * Legt den Wert der includeUserPersonalPhoneListInDirectory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeUserPersonalPhoneListInDirectory(Boolean value) {
        this.includeUserPersonalPhoneListInDirectory = value;
    }

    /**
     * Ruft den Wert der includeGroupCustomContactDirectoryInDirectory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeGroupCustomContactDirectoryInDirectory() {
        return includeGroupCustomContactDirectoryInDirectory;
    }

    /**
     * Legt den Wert der includeGroupCustomContactDirectoryInDirectory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeGroupCustomContactDirectoryInDirectory(Boolean value) {
        this.includeGroupCustomContactDirectoryInDirectory = value;
    }

    /**
     * Ruft den Wert der groupCustomContactDirectory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGroupCustomContactDirectory() {
        return groupCustomContactDirectory;
    }

    /**
     * Legt den Wert der groupCustomContactDirectory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGroupCustomContactDirectory(JAXBElement<String> value) {
        this.groupCustomContactDirectory = value;
    }

}
