//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für RingPattern.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="RingPattern">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Normal"/>
 *     <enumeration value="Long-Long"/>
 *     <enumeration value="Short-Short-Long"/>
 *     <enumeration value="Short-Long-Short"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "RingPattern")
@XmlEnum
public enum RingPattern {

    @XmlEnumValue("Normal")
    NORMAL("Normal"),
    @XmlEnumValue("Long-Long")
    LONG_LONG("Long-Long"),
    @XmlEnumValue("Short-Short-Long")
    SHORT_SHORT_LONG("Short-Short-Long"),
    @XmlEnumValue("Short-Long-Short")
    SHORT_LONG_SHORT("Short-Long-Short");
    private final String value;

    RingPattern(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RingPattern fromValue(String v) {
        for (RingPattern c: RingPattern.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
