//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für StirShakenTagFromOrPAI.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="StirShakenTagFromOrPAI">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="PAI"/>
 *     <enumeration value="From"/>
 *     <enumeration value="Both"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "StirShakenTagFromOrPAI")
@XmlEnum
public enum StirShakenTagFromOrPAI {

    PAI("PAI"),
    @XmlEnumValue("From")
    FROM("From"),
    @XmlEnumValue("Both")
    BOTH("Both");
    private final String value;

    StirShakenTagFromOrPAI(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StirShakenTagFromOrPAI fromValue(String v) {
        for (StirShakenTagFromOrPAI c: StirShakenTagFromOrPAI.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
