//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Get a call center's DNIS Announcements
 *         The response is either a GroupCallCenterGetDNISAnnouncementResponse20 or an ErrorResponse.
 *         
 *         Replaced by: GroupCallCenterGetDNISAnnouncementRequest22.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterGetDNISAnnouncementRequest20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterGetDNISAnnouncementRequest20">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="dnisKey" type="{}DNISKey"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterGetDNISAnnouncementRequest20", propOrder = {
    "dnisKey"
})
public class GroupCallCenterGetDNISAnnouncementRequest20
    extends OCIRequest
{

    @XmlElement(required = true)
    protected DNISKey dnisKey;

    /**
     * Ruft den Wert der dnisKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DNISKey }
     *     
     */
    public DNISKey getDnisKey() {
        return dnisKey;
    }

    /**
     * Legt den Wert der dnisKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DNISKey }
     *     
     */
    public void setDnisKey(DNISKey value) {
        this.dnisKey = value;
    }

}
