//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with Multimedia Policy.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemMultimediaPolicyModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemMultimediaPolicyModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="restrictNonAudioVideoMediaTypes" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemMultimediaPolicyModifyRequest", propOrder = {
    "restrictNonAudioVideoMediaTypes"
})
public class SystemMultimediaPolicyModifyRequest
    extends OCIRequest
{

    protected Boolean restrictNonAudioVideoMediaTypes;

    /**
     * Ruft den Wert der restrictNonAudioVideoMediaTypes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictNonAudioVideoMediaTypes() {
        return restrictNonAudioVideoMediaTypes;
    }

    /**
     * Legt den Wert der restrictNonAudioVideoMediaTypes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictNonAudioVideoMediaTypes(Boolean value) {
        this.restrictNonAudioVideoMediaTypes = value;
    }

}
