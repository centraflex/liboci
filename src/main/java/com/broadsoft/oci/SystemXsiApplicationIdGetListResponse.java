//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemXsiApplicationIdGetListRequest
 *         Contains a table with column headings: "Xsi Application Id", "Description".
 *       
 * 
 * <p>Java-Klasse für SystemXsiApplicationIdGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemXsiApplicationIdGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="xsiApplicationIdTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemXsiApplicationIdGetListResponse", propOrder = {
    "xsiApplicationIdTable"
})
public class SystemXsiApplicationIdGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable xsiApplicationIdTable;

    /**
     * Ruft den Wert der xsiApplicationIdTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getXsiApplicationIdTable() {
        return xsiApplicationIdTable;
    }

    /**
     * Legt den Wert der xsiApplicationIdTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setXsiApplicationIdTable(OCITable value) {
        this.xsiApplicationIdTable = value;
    }

}
