//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the profile for a group.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         The following elements are only used in Amplify data mode and ignored 
 *         in AS and XS data mode: 
 *         servicePolicy, 
 *         callProcessingSliceId, 
 *         provisioningSliceId, 
 *         subscriberPartition.
 *         When the callProcessingSliceId or provisioningSliceId is set to nillable, 
 *         it will be assigned to the default Slice.
 *         Only Provisioning admin and above can change the callProcessingSliceId, provisioningSliceId, and subscriberPartition.
 *         
 *         The following elements are only used in Amplify and XS data mode and ignored in AS data mode:
 *         preferredDataCenter.
 *         Only Provisioning admin and above can change the preferredDataCenter.
 *         
 *         The following elements are only used in XS data mode and ignored in Amplify and AS data mode:
 *         defaultUserCallingLineIdPhoneNumber.
 *         
 *         The following elements are only used in AS data mode and ignored in the XS data mode:
 * 		 extensionLengthSettings
 * 		 dialPlanAccessCode
 * 		 dialPlanPolicy
 *         
 *       
 * 
 * <p>Java-Klasse für GroupModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="defaultDomain" type="{}NetAddress" minOccurs="0"/>
 *         <element name="userLimit" type="{}GroupUserLimit" minOccurs="0"/>
 *         <element name="groupName" type="{}GroupName" minOccurs="0"/>
 *         <element name="callingLineIdName" type="{}GroupCallingLineIdName" minOccurs="0"/>
 *         <element name="callingLineIdPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="timeZone" type="{}TimeZone" minOccurs="0"/>
 *         <element name="locationDialingCode" type="{}LocationDialingCode" minOccurs="0"/>
 *         <element name="contact" type="{}Contact" minOccurs="0"/>
 *         <element name="address" type="{}StreetAddress" minOccurs="0"/>
 *         <element name="servicePolicy" type="{}ServicePolicyName" minOccurs="0"/>
 *         <element name="callProcessingSliceId" type="{}SliceId" minOccurs="0"/>
 *         <element name="provisioningSliceId" type="{}SliceId" minOccurs="0"/>
 *         <element name="subscriberPartition" type="{}SubscriberPartition" minOccurs="0"/>
 *         <element name="preferredDataCenter" type="{}DataCenter" minOccurs="0"/>
 *         <element name="defaultUserCallingLineIdPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="extensionLengthSettings" type="{}GroupExtensionLengthSettings" minOccurs="0"/>
 *         <element name="dialPlanAccessCode" type="{}DialPlanPolicyAccessCode" minOccurs="0"/>
 *         <element name="dialPlanPolicy" type="{}DialPlanPolicy" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupModifyRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "defaultDomain",
    "userLimit",
    "groupName",
    "callingLineIdName",
    "callingLineIdPhoneNumber",
    "timeZone",
    "locationDialingCode",
    "contact",
    "address",
    "servicePolicy",
    "callProcessingSliceId",
    "provisioningSliceId",
    "subscriberPartition",
    "preferredDataCenter",
    "defaultUserCallingLineIdPhoneNumber",
    "extensionLengthSettings",
    "dialPlanAccessCode",
    "dialPlanPolicy"
})
public class GroupModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultDomain;
    protected Integer userLimit;
    @XmlElementRef(name = "groupName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> groupName;
    @XmlElementRef(name = "callingLineIdName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callingLineIdName;
    @XmlElementRef(name = "callingLineIdPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callingLineIdPhoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeZone;
    @XmlElementRef(name = "locationDialingCode", type = JAXBElement.class, required = false)
    protected JAXBElement<String> locationDialingCode;
    protected Contact contact;
    protected StreetAddress address;
    @XmlElementRef(name = "servicePolicy", type = JAXBElement.class, required = false)
    protected JAXBElement<String> servicePolicy;
    @XmlElementRef(name = "callProcessingSliceId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callProcessingSliceId;
    @XmlElementRef(name = "provisioningSliceId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> provisioningSliceId;
    @XmlElementRef(name = "subscriberPartition", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subscriberPartition;
    @XmlElementRef(name = "preferredDataCenter", type = JAXBElement.class, required = false)
    protected JAXBElement<String> preferredDataCenter;
    @XmlElementRef(name = "defaultUserCallingLineIdPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultUserCallingLineIdPhoneNumber;
    protected GroupExtensionLengthSettings extensionLengthSettings;
    @XmlElementRef(name = "dialPlanAccessCode", type = JAXBElement.class, required = false)
    protected JAXBElement<DialPlanPolicyAccessCode> dialPlanAccessCode;
    protected DialPlanPolicy dialPlanPolicy;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der defaultDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultDomain() {
        return defaultDomain;
    }

    /**
     * Legt den Wert der defaultDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultDomain(String value) {
        this.defaultDomain = value;
    }

    /**
     * Ruft den Wert der userLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUserLimit() {
        return userLimit;
    }

    /**
     * Legt den Wert der userLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUserLimit(Integer value) {
        this.userLimit = value;
    }

    /**
     * Ruft den Wert der groupName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGroupName() {
        return groupName;
    }

    /**
     * Legt den Wert der groupName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGroupName(JAXBElement<String> value) {
        this.groupName = value;
    }

    /**
     * Ruft den Wert der callingLineIdName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallingLineIdName() {
        return callingLineIdName;
    }

    /**
     * Legt den Wert der callingLineIdName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallingLineIdName(JAXBElement<String> value) {
        this.callingLineIdName = value;
    }

    /**
     * Ruft den Wert der callingLineIdPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallingLineIdPhoneNumber() {
        return callingLineIdPhoneNumber;
    }

    /**
     * Legt den Wert der callingLineIdPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallingLineIdPhoneNumber(JAXBElement<String> value) {
        this.callingLineIdPhoneNumber = value;
    }

    /**
     * Ruft den Wert der timeZone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * Legt den Wert der timeZone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeZone(String value) {
        this.timeZone = value;
    }

    /**
     * Ruft den Wert der locationDialingCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocationDialingCode() {
        return locationDialingCode;
    }

    /**
     * Legt den Wert der locationDialingCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocationDialingCode(JAXBElement<String> value) {
        this.locationDialingCode = value;
    }

    /**
     * Ruft den Wert der contact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Contact }
     *     
     */
    public Contact getContact() {
        return contact;
    }

    /**
     * Legt den Wert der contact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Contact }
     *     
     */
    public void setContact(Contact value) {
        this.contact = value;
    }

    /**
     * Ruft den Wert der address-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StreetAddress }
     *     
     */
    public StreetAddress getAddress() {
        return address;
    }

    /**
     * Legt den Wert der address-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetAddress }
     *     
     */
    public void setAddress(StreetAddress value) {
        this.address = value;
    }

    /**
     * Ruft den Wert der servicePolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServicePolicy() {
        return servicePolicy;
    }

    /**
     * Legt den Wert der servicePolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServicePolicy(JAXBElement<String> value) {
        this.servicePolicy = value;
    }

    /**
     * Ruft den Wert der callProcessingSliceId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallProcessingSliceId() {
        return callProcessingSliceId;
    }

    /**
     * Legt den Wert der callProcessingSliceId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallProcessingSliceId(JAXBElement<String> value) {
        this.callProcessingSliceId = value;
    }

    /**
     * Ruft den Wert der provisioningSliceId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProvisioningSliceId() {
        return provisioningSliceId;
    }

    /**
     * Legt den Wert der provisioningSliceId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProvisioningSliceId(JAXBElement<String> value) {
        this.provisioningSliceId = value;
    }

    /**
     * Ruft den Wert der subscriberPartition-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubscriberPartition() {
        return subscriberPartition;
    }

    /**
     * Legt den Wert der subscriberPartition-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubscriberPartition(JAXBElement<String> value) {
        this.subscriberPartition = value;
    }

    /**
     * Ruft den Wert der preferredDataCenter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPreferredDataCenter() {
        return preferredDataCenter;
    }

    /**
     * Legt den Wert der preferredDataCenter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPreferredDataCenter(JAXBElement<String> value) {
        this.preferredDataCenter = value;
    }

    /**
     * Ruft den Wert der defaultUserCallingLineIdPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultUserCallingLineIdPhoneNumber() {
        return defaultUserCallingLineIdPhoneNumber;
    }

    /**
     * Legt den Wert der defaultUserCallingLineIdPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultUserCallingLineIdPhoneNumber(JAXBElement<String> value) {
        this.defaultUserCallingLineIdPhoneNumber = value;
    }

    /**
     * Ruft den Wert der extensionLengthSettings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupExtensionLengthSettings }
     *     
     */
    public GroupExtensionLengthSettings getExtensionLengthSettings() {
        return extensionLengthSettings;
    }

    /**
     * Legt den Wert der extensionLengthSettings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupExtensionLengthSettings }
     *     
     */
    public void setExtensionLengthSettings(GroupExtensionLengthSettings value) {
        this.extensionLengthSettings = value;
    }

    /**
     * Ruft den Wert der dialPlanAccessCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DialPlanPolicyAccessCode }{@code >}
     *     
     */
    public JAXBElement<DialPlanPolicyAccessCode> getDialPlanAccessCode() {
        return dialPlanAccessCode;
    }

    /**
     * Legt den Wert der dialPlanAccessCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DialPlanPolicyAccessCode }{@code >}
     *     
     */
    public void setDialPlanAccessCode(JAXBElement<DialPlanPolicyAccessCode> value) {
        this.dialPlanAccessCode = value;
    }

    /**
     * Ruft den Wert der dialPlanPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DialPlanPolicy }
     *     
     */
    public DialPlanPolicy getDialPlanPolicy() {
        return dialPlanPolicy;
    }

    /**
     * Legt den Wert der dialPlanPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DialPlanPolicy }
     *     
     */
    public void setDialPlanPolicy(DialPlanPolicy value) {
        this.dialPlanPolicy = value;
    }

}
