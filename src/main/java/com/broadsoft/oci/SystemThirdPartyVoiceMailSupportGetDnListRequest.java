//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *        Request to get a list of ThirdPartyVoiceMailSupport User DN's defined in the system.
 *        The response is either a SystemThirdPartyVoiceMailSupportGetDnListResponse or an ErrorResponse.
 *        The search can be done using multiple criterion.
 *      
 * 
 * <p>Java-Klasse für SystemThirdPartyVoiceMailSupportGetDnListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemThirdPartyVoiceMailSupportGetDnListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaSystemServiceDn" type="{}SearchCriteriaSystemServiceDn" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemThirdPartyVoiceMailSupportGetDnListRequest", propOrder = {
    "responseSizeLimit",
    "searchCriteriaSystemServiceDn"
})
public class SystemThirdPartyVoiceMailSupportGetDnListRequest
    extends OCIRequest
{

    protected Integer responseSizeLimit;
    protected List<SearchCriteriaSystemServiceDn> searchCriteriaSystemServiceDn;

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaSystemServiceDn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaSystemServiceDn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaSystemServiceDn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaSystemServiceDn }
     * 
     * 
     * @return
     *     The value of the searchCriteriaSystemServiceDn property.
     */
    public List<SearchCriteriaSystemServiceDn> getSearchCriteriaSystemServiceDn() {
        if (searchCriteriaSystemServiceDn == null) {
            searchCriteriaSystemServiceDn = new ArrayList<>();
        }
        return this.searchCriteriaSystemServiceDn;
    }

}
