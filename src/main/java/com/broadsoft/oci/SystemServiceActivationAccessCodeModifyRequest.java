//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify Service Activation Access Code system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemServiceActivationAccessCodeModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemServiceActivationAccessCodeModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="terminatingAccessCode" type="{}ServiceActivationAccessCode" minOccurs="0"/>
 *         <element name="redirectingAccessCode" type="{}ServiceActivationAccessCode" minOccurs="0"/>
 *         <element name="clickToDialAccessCode" type="{}ServiceActivationAccessCode" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemServiceActivationAccessCodeModifyRequest", propOrder = {
    "isActive",
    "terminatingAccessCode",
    "redirectingAccessCode",
    "clickToDialAccessCode"
})
public class SystemServiceActivationAccessCodeModifyRequest
    extends OCIRequest
{

    protected Boolean isActive;
    @XmlElementRef(name = "terminatingAccessCode", type = JAXBElement.class, required = false)
    protected JAXBElement<String> terminatingAccessCode;
    @XmlElementRef(name = "redirectingAccessCode", type = JAXBElement.class, required = false)
    protected JAXBElement<String> redirectingAccessCode;
    @XmlElementRef(name = "clickToDialAccessCode", type = JAXBElement.class, required = false)
    protected JAXBElement<String> clickToDialAccessCode;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der terminatingAccessCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTerminatingAccessCode() {
        return terminatingAccessCode;
    }

    /**
     * Legt den Wert der terminatingAccessCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTerminatingAccessCode(JAXBElement<String> value) {
        this.terminatingAccessCode = value;
    }

    /**
     * Ruft den Wert der redirectingAccessCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRedirectingAccessCode() {
        return redirectingAccessCode;
    }

    /**
     * Legt den Wert der redirectingAccessCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRedirectingAccessCode(JAXBElement<String> value) {
        this.redirectingAccessCode = value;
    }

    /**
     * Ruft den Wert der clickToDialAccessCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClickToDialAccessCode() {
        return clickToDialAccessCode;
    }

    /**
     * Legt den Wert der clickToDialAccessCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClickToDialAccessCode(JAXBElement<String> value) {
        this.clickToDialAccessCode = value;
    }

}
