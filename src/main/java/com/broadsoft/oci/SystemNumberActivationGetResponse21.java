//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemNumberActivationGetRequest21.
 *         Contains the system number activation and enterprise trunk number range activation setting.
 *       
 * 
 * <p>Java-Klasse für SystemNumberActivationGetResponse21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNumberActivationGetResponse21">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="numberActivationMode" type="{}NumberActivationMode"/>
 *         <element name="enableEnterpriseTrunkNumberRangeActivation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNumberActivationGetResponse21", propOrder = {
    "numberActivationMode",
    "enableEnterpriseTrunkNumberRangeActivation"
})
public class SystemNumberActivationGetResponse21
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected NumberActivationMode numberActivationMode;
    protected boolean enableEnterpriseTrunkNumberRangeActivation;

    /**
     * Ruft den Wert der numberActivationMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NumberActivationMode }
     *     
     */
    public NumberActivationMode getNumberActivationMode() {
        return numberActivationMode;
    }

    /**
     * Legt den Wert der numberActivationMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberActivationMode }
     *     
     */
    public void setNumberActivationMode(NumberActivationMode value) {
        this.numberActivationMode = value;
    }

    /**
     * Ruft den Wert der enableEnterpriseTrunkNumberRangeActivation-Eigenschaft ab.
     * 
     */
    public boolean isEnableEnterpriseTrunkNumberRangeActivation() {
        return enableEnterpriseTrunkNumberRangeActivation;
    }

    /**
     * Legt den Wert der enableEnterpriseTrunkNumberRangeActivation-Eigenschaft fest.
     * 
     */
    public void setEnableEnterpriseTrunkNumberRangeActivation(boolean value) {
        this.enableEnterpriseTrunkNumberRangeActivation = value;
    }

}
