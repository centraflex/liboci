//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *      Request to modify the service provider level SIP authentication password rule settings. The useServiceProviderSettings element can only be modified by a system administrator or a provisioning administrator.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderSIPAuthenticationPasswordRulesModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderSIPAuthenticationPasswordRulesModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="useServiceProviderSettings" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disallowAuthenticationName" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disallowOldPassword" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disallowReversedOldPassword" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="restrictMinDigits" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="minDigits" type="{}PasswordMinDigits" minOccurs="0"/>
 *         <element name="restrictMinUpperCaseLetters" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="minUpperCaseLetters" type="{}PasswordMinUpperCaseLetters" minOccurs="0"/>
 *         <element name="restrictMinLowerCaseLetters" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="minLowerCaseLetters" type="{}PasswordMinLowerCaseLetters" minOccurs="0"/>
 *         <element name="restrictMinNonAlphanumericCharacters" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="minNonAlphanumericCharacters" type="{}PasswordMinNonAlphanumericCharacters" minOccurs="0"/>
 *         <element name="minLength" type="{}PasswordMinLength" minOccurs="0"/>
 *         <element name="sendPermanentLockoutNotification" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="permanentLockoutNotifyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="endpointAuthenticationLockoutType" type="{}AuthenticationLockoutType" minOccurs="0"/>
 *         <element name="endpointTemporaryLockoutThreshold" type="{}AuthenticationTemporaryLockoutThreshold" minOccurs="0"/>
 *         <element name="endpointWaitAlgorithm" type="{}AuthenticationLockoutWaitAlgorithmType" minOccurs="0"/>
 *         <element name="endpointLockoutFixedMinutes" type="{}AuthenticationLockoutFixedWaitTimeMinutes" minOccurs="0"/>
 *         <element name="endpointPermanentLockoutThreshold" type="{}AuthenticationPermanentLockoutThreshold" minOccurs="0"/>
 *         <element name="trunkGroupAuthenticationLockoutType" type="{}AuthenticationLockoutType" minOccurs="0"/>
 *         <element name="trunkGroupTemporaryLockoutThreshold" type="{}AuthenticationTemporaryLockoutThreshold" minOccurs="0"/>
 *         <element name="trunkGroupWaitAlgorithm" type="{}AuthenticationLockoutWaitAlgorithmType" minOccurs="0"/>
 *         <element name="trunkGroupLockoutFixedMinutes" type="{}AuthenticationLockoutFixedWaitTimeMinutes" minOccurs="0"/>
 *         <element name="trunkGroupPermanentLockoutThreshold" type="{}AuthenticationPermanentLockoutThreshold" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderSIPAuthenticationPasswordRulesModifyRequest", propOrder = {
    "serviceProviderId",
    "useServiceProviderSettings",
    "disallowAuthenticationName",
    "disallowOldPassword",
    "disallowReversedOldPassword",
    "restrictMinDigits",
    "minDigits",
    "restrictMinUpperCaseLetters",
    "minUpperCaseLetters",
    "restrictMinLowerCaseLetters",
    "minLowerCaseLetters",
    "restrictMinNonAlphanumericCharacters",
    "minNonAlphanumericCharacters",
    "minLength",
    "sendPermanentLockoutNotification",
    "permanentLockoutNotifyEmailAddress",
    "endpointAuthenticationLockoutType",
    "endpointTemporaryLockoutThreshold",
    "endpointWaitAlgorithm",
    "endpointLockoutFixedMinutes",
    "endpointPermanentLockoutThreshold",
    "trunkGroupAuthenticationLockoutType",
    "trunkGroupTemporaryLockoutThreshold",
    "trunkGroupWaitAlgorithm",
    "trunkGroupLockoutFixedMinutes",
    "trunkGroupPermanentLockoutThreshold"
})
public class ServiceProviderSIPAuthenticationPasswordRulesModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected Boolean useServiceProviderSettings;
    protected Boolean disallowAuthenticationName;
    protected Boolean disallowOldPassword;
    protected Boolean disallowReversedOldPassword;
    protected Boolean restrictMinDigits;
    protected Integer minDigits;
    protected Boolean restrictMinUpperCaseLetters;
    protected Integer minUpperCaseLetters;
    protected Boolean restrictMinLowerCaseLetters;
    protected Integer minLowerCaseLetters;
    protected Boolean restrictMinNonAlphanumericCharacters;
    protected Integer minNonAlphanumericCharacters;
    protected Integer minLength;
    protected Boolean sendPermanentLockoutNotification;
    @XmlElementRef(name = "permanentLockoutNotifyEmailAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> permanentLockoutNotifyEmailAddress;
    @XmlSchemaType(name = "token")
    protected AuthenticationLockoutType endpointAuthenticationLockoutType;
    protected Integer endpointTemporaryLockoutThreshold;
    @XmlSchemaType(name = "token")
    protected AuthenticationLockoutWaitAlgorithmType endpointWaitAlgorithm;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String endpointLockoutFixedMinutes;
    protected Integer endpointPermanentLockoutThreshold;
    @XmlSchemaType(name = "token")
    protected AuthenticationLockoutType trunkGroupAuthenticationLockoutType;
    protected Integer trunkGroupTemporaryLockoutThreshold;
    @XmlSchemaType(name = "token")
    protected AuthenticationLockoutWaitAlgorithmType trunkGroupWaitAlgorithm;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String trunkGroupLockoutFixedMinutes;
    protected Integer trunkGroupPermanentLockoutThreshold;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der useServiceProviderSettings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseServiceProviderSettings() {
        return useServiceProviderSettings;
    }

    /**
     * Legt den Wert der useServiceProviderSettings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseServiceProviderSettings(Boolean value) {
        this.useServiceProviderSettings = value;
    }

    /**
     * Ruft den Wert der disallowAuthenticationName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowAuthenticationName() {
        return disallowAuthenticationName;
    }

    /**
     * Legt den Wert der disallowAuthenticationName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowAuthenticationName(Boolean value) {
        this.disallowAuthenticationName = value;
    }

    /**
     * Ruft den Wert der disallowOldPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowOldPassword() {
        return disallowOldPassword;
    }

    /**
     * Legt den Wert der disallowOldPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowOldPassword(Boolean value) {
        this.disallowOldPassword = value;
    }

    /**
     * Ruft den Wert der disallowReversedOldPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowReversedOldPassword() {
        return disallowReversedOldPassword;
    }

    /**
     * Legt den Wert der disallowReversedOldPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowReversedOldPassword(Boolean value) {
        this.disallowReversedOldPassword = value;
    }

    /**
     * Ruft den Wert der restrictMinDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictMinDigits() {
        return restrictMinDigits;
    }

    /**
     * Legt den Wert der restrictMinDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictMinDigits(Boolean value) {
        this.restrictMinDigits = value;
    }

    /**
     * Ruft den Wert der minDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinDigits() {
        return minDigits;
    }

    /**
     * Legt den Wert der minDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinDigits(Integer value) {
        this.minDigits = value;
    }

    /**
     * Ruft den Wert der restrictMinUpperCaseLetters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictMinUpperCaseLetters() {
        return restrictMinUpperCaseLetters;
    }

    /**
     * Legt den Wert der restrictMinUpperCaseLetters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictMinUpperCaseLetters(Boolean value) {
        this.restrictMinUpperCaseLetters = value;
    }

    /**
     * Ruft den Wert der minUpperCaseLetters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinUpperCaseLetters() {
        return minUpperCaseLetters;
    }

    /**
     * Legt den Wert der minUpperCaseLetters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinUpperCaseLetters(Integer value) {
        this.minUpperCaseLetters = value;
    }

    /**
     * Ruft den Wert der restrictMinLowerCaseLetters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictMinLowerCaseLetters() {
        return restrictMinLowerCaseLetters;
    }

    /**
     * Legt den Wert der restrictMinLowerCaseLetters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictMinLowerCaseLetters(Boolean value) {
        this.restrictMinLowerCaseLetters = value;
    }

    /**
     * Ruft den Wert der minLowerCaseLetters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinLowerCaseLetters() {
        return minLowerCaseLetters;
    }

    /**
     * Legt den Wert der minLowerCaseLetters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinLowerCaseLetters(Integer value) {
        this.minLowerCaseLetters = value;
    }

    /**
     * Ruft den Wert der restrictMinNonAlphanumericCharacters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictMinNonAlphanumericCharacters() {
        return restrictMinNonAlphanumericCharacters;
    }

    /**
     * Legt den Wert der restrictMinNonAlphanumericCharacters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictMinNonAlphanumericCharacters(Boolean value) {
        this.restrictMinNonAlphanumericCharacters = value;
    }

    /**
     * Ruft den Wert der minNonAlphanumericCharacters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinNonAlphanumericCharacters() {
        return minNonAlphanumericCharacters;
    }

    /**
     * Legt den Wert der minNonAlphanumericCharacters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinNonAlphanumericCharacters(Integer value) {
        this.minNonAlphanumericCharacters = value;
    }

    /**
     * Ruft den Wert der minLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinLength() {
        return minLength;
    }

    /**
     * Legt den Wert der minLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinLength(Integer value) {
        this.minLength = value;
    }

    /**
     * Ruft den Wert der sendPermanentLockoutNotification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendPermanentLockoutNotification() {
        return sendPermanentLockoutNotification;
    }

    /**
     * Legt den Wert der sendPermanentLockoutNotification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendPermanentLockoutNotification(Boolean value) {
        this.sendPermanentLockoutNotification = value;
    }

    /**
     * Ruft den Wert der permanentLockoutNotifyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPermanentLockoutNotifyEmailAddress() {
        return permanentLockoutNotifyEmailAddress;
    }

    /**
     * Legt den Wert der permanentLockoutNotifyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPermanentLockoutNotifyEmailAddress(JAXBElement<String> value) {
        this.permanentLockoutNotifyEmailAddress = value;
    }

    /**
     * Ruft den Wert der endpointAuthenticationLockoutType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationLockoutType }
     *     
     */
    public AuthenticationLockoutType getEndpointAuthenticationLockoutType() {
        return endpointAuthenticationLockoutType;
    }

    /**
     * Legt den Wert der endpointAuthenticationLockoutType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationLockoutType }
     *     
     */
    public void setEndpointAuthenticationLockoutType(AuthenticationLockoutType value) {
        this.endpointAuthenticationLockoutType = value;
    }

    /**
     * Ruft den Wert der endpointTemporaryLockoutThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEndpointTemporaryLockoutThreshold() {
        return endpointTemporaryLockoutThreshold;
    }

    /**
     * Legt den Wert der endpointTemporaryLockoutThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEndpointTemporaryLockoutThreshold(Integer value) {
        this.endpointTemporaryLockoutThreshold = value;
    }

    /**
     * Ruft den Wert der endpointWaitAlgorithm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationLockoutWaitAlgorithmType }
     *     
     */
    public AuthenticationLockoutWaitAlgorithmType getEndpointWaitAlgorithm() {
        return endpointWaitAlgorithm;
    }

    /**
     * Legt den Wert der endpointWaitAlgorithm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationLockoutWaitAlgorithmType }
     *     
     */
    public void setEndpointWaitAlgorithm(AuthenticationLockoutWaitAlgorithmType value) {
        this.endpointWaitAlgorithm = value;
    }

    /**
     * Ruft den Wert der endpointLockoutFixedMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndpointLockoutFixedMinutes() {
        return endpointLockoutFixedMinutes;
    }

    /**
     * Legt den Wert der endpointLockoutFixedMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndpointLockoutFixedMinutes(String value) {
        this.endpointLockoutFixedMinutes = value;
    }

    /**
     * Ruft den Wert der endpointPermanentLockoutThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEndpointPermanentLockoutThreshold() {
        return endpointPermanentLockoutThreshold;
    }

    /**
     * Legt den Wert der endpointPermanentLockoutThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEndpointPermanentLockoutThreshold(Integer value) {
        this.endpointPermanentLockoutThreshold = value;
    }

    /**
     * Ruft den Wert der trunkGroupAuthenticationLockoutType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationLockoutType }
     *     
     */
    public AuthenticationLockoutType getTrunkGroupAuthenticationLockoutType() {
        return trunkGroupAuthenticationLockoutType;
    }

    /**
     * Legt den Wert der trunkGroupAuthenticationLockoutType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationLockoutType }
     *     
     */
    public void setTrunkGroupAuthenticationLockoutType(AuthenticationLockoutType value) {
        this.trunkGroupAuthenticationLockoutType = value;
    }

    /**
     * Ruft den Wert der trunkGroupTemporaryLockoutThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTrunkGroupTemporaryLockoutThreshold() {
        return trunkGroupTemporaryLockoutThreshold;
    }

    /**
     * Legt den Wert der trunkGroupTemporaryLockoutThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTrunkGroupTemporaryLockoutThreshold(Integer value) {
        this.trunkGroupTemporaryLockoutThreshold = value;
    }

    /**
     * Ruft den Wert der trunkGroupWaitAlgorithm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationLockoutWaitAlgorithmType }
     *     
     */
    public AuthenticationLockoutWaitAlgorithmType getTrunkGroupWaitAlgorithm() {
        return trunkGroupWaitAlgorithm;
    }

    /**
     * Legt den Wert der trunkGroupWaitAlgorithm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationLockoutWaitAlgorithmType }
     *     
     */
    public void setTrunkGroupWaitAlgorithm(AuthenticationLockoutWaitAlgorithmType value) {
        this.trunkGroupWaitAlgorithm = value;
    }

    /**
     * Ruft den Wert der trunkGroupLockoutFixedMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrunkGroupLockoutFixedMinutes() {
        return trunkGroupLockoutFixedMinutes;
    }

    /**
     * Legt den Wert der trunkGroupLockoutFixedMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrunkGroupLockoutFixedMinutes(String value) {
        this.trunkGroupLockoutFixedMinutes = value;
    }

    /**
     * Ruft den Wert der trunkGroupPermanentLockoutThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTrunkGroupPermanentLockoutThreshold() {
        return trunkGroupPermanentLockoutThreshold;
    }

    /**
     * Legt den Wert der trunkGroupPermanentLockoutThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTrunkGroupPermanentLockoutThreshold(Integer value) {
        this.trunkGroupPermanentLockoutThreshold = value;
    }

}
