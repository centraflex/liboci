//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the SystemNetworkClassOfServiceGetRequest19sp1.
 *         The response contains the Network Class of Service information.
 *       
 * 
 * <p>Java-Klasse für SystemNetworkClassOfServiceGetResponse19sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNetworkClassOfServiceGetResponse19sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="description" type="{}NetworkClassOfServiceDescription" minOccurs="0"/>
 *         <element name="communicationBarringProfile0" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile1" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile2" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile3" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile4" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile5" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile6" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile7" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile8" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="communicationBarringProfile9" type="{}NetworkClassOfServiceCommunicationBarringProfile" minOccurs="0"/>
 *         <element name="networkTranslationIndex" type="{}NetworkTranslationIndex" minOccurs="0"/>
 *         <element name="callProcessingPolicyProfileName" type="{}CallProcessingPolicyProfileName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNetworkClassOfServiceGetResponse19sp1", propOrder = {
    "description",
    "communicationBarringProfile0",
    "communicationBarringProfile1",
    "communicationBarringProfile2",
    "communicationBarringProfile3",
    "communicationBarringProfile4",
    "communicationBarringProfile5",
    "communicationBarringProfile6",
    "communicationBarringProfile7",
    "communicationBarringProfile8",
    "communicationBarringProfile9",
    "networkTranslationIndex",
    "callProcessingPolicyProfileName"
})
public class SystemNetworkClassOfServiceGetResponse19Sp1
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    protected NetworkClassOfServiceCommunicationBarringProfile communicationBarringProfile0;
    protected NetworkClassOfServiceCommunicationBarringProfile communicationBarringProfile1;
    protected NetworkClassOfServiceCommunicationBarringProfile communicationBarringProfile2;
    protected NetworkClassOfServiceCommunicationBarringProfile communicationBarringProfile3;
    protected NetworkClassOfServiceCommunicationBarringProfile communicationBarringProfile4;
    protected NetworkClassOfServiceCommunicationBarringProfile communicationBarringProfile5;
    protected NetworkClassOfServiceCommunicationBarringProfile communicationBarringProfile6;
    protected NetworkClassOfServiceCommunicationBarringProfile communicationBarringProfile7;
    protected NetworkClassOfServiceCommunicationBarringProfile communicationBarringProfile8;
    protected NetworkClassOfServiceCommunicationBarringProfile communicationBarringProfile9;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkTranslationIndex;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callProcessingPolicyProfileName;

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile0-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public NetworkClassOfServiceCommunicationBarringProfile getCommunicationBarringProfile0() {
        return communicationBarringProfile0;
    }

    /**
     * Legt den Wert der communicationBarringProfile0-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public void setCommunicationBarringProfile0(NetworkClassOfServiceCommunicationBarringProfile value) {
        this.communicationBarringProfile0 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile1-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public NetworkClassOfServiceCommunicationBarringProfile getCommunicationBarringProfile1() {
        return communicationBarringProfile1;
    }

    /**
     * Legt den Wert der communicationBarringProfile1-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public void setCommunicationBarringProfile1(NetworkClassOfServiceCommunicationBarringProfile value) {
        this.communicationBarringProfile1 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile2-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public NetworkClassOfServiceCommunicationBarringProfile getCommunicationBarringProfile2() {
        return communicationBarringProfile2;
    }

    /**
     * Legt den Wert der communicationBarringProfile2-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public void setCommunicationBarringProfile2(NetworkClassOfServiceCommunicationBarringProfile value) {
        this.communicationBarringProfile2 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile3-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public NetworkClassOfServiceCommunicationBarringProfile getCommunicationBarringProfile3() {
        return communicationBarringProfile3;
    }

    /**
     * Legt den Wert der communicationBarringProfile3-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public void setCommunicationBarringProfile3(NetworkClassOfServiceCommunicationBarringProfile value) {
        this.communicationBarringProfile3 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile4-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public NetworkClassOfServiceCommunicationBarringProfile getCommunicationBarringProfile4() {
        return communicationBarringProfile4;
    }

    /**
     * Legt den Wert der communicationBarringProfile4-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public void setCommunicationBarringProfile4(NetworkClassOfServiceCommunicationBarringProfile value) {
        this.communicationBarringProfile4 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile5-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public NetworkClassOfServiceCommunicationBarringProfile getCommunicationBarringProfile5() {
        return communicationBarringProfile5;
    }

    /**
     * Legt den Wert der communicationBarringProfile5-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public void setCommunicationBarringProfile5(NetworkClassOfServiceCommunicationBarringProfile value) {
        this.communicationBarringProfile5 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile6-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public NetworkClassOfServiceCommunicationBarringProfile getCommunicationBarringProfile6() {
        return communicationBarringProfile6;
    }

    /**
     * Legt den Wert der communicationBarringProfile6-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public void setCommunicationBarringProfile6(NetworkClassOfServiceCommunicationBarringProfile value) {
        this.communicationBarringProfile6 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile7-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public NetworkClassOfServiceCommunicationBarringProfile getCommunicationBarringProfile7() {
        return communicationBarringProfile7;
    }

    /**
     * Legt den Wert der communicationBarringProfile7-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public void setCommunicationBarringProfile7(NetworkClassOfServiceCommunicationBarringProfile value) {
        this.communicationBarringProfile7 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile8-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public NetworkClassOfServiceCommunicationBarringProfile getCommunicationBarringProfile8() {
        return communicationBarringProfile8;
    }

    /**
     * Legt den Wert der communicationBarringProfile8-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public void setCommunicationBarringProfile8(NetworkClassOfServiceCommunicationBarringProfile value) {
        this.communicationBarringProfile8 = value;
    }

    /**
     * Ruft den Wert der communicationBarringProfile9-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public NetworkClassOfServiceCommunicationBarringProfile getCommunicationBarringProfile9() {
        return communicationBarringProfile9;
    }

    /**
     * Legt den Wert der communicationBarringProfile9-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkClassOfServiceCommunicationBarringProfile }
     *     
     */
    public void setCommunicationBarringProfile9(NetworkClassOfServiceCommunicationBarringProfile value) {
        this.communicationBarringProfile9 = value;
    }

    /**
     * Ruft den Wert der networkTranslationIndex-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkTranslationIndex() {
        return networkTranslationIndex;
    }

    /**
     * Legt den Wert der networkTranslationIndex-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkTranslationIndex(String value) {
        this.networkTranslationIndex = value;
    }

    /**
     * Ruft den Wert der callProcessingPolicyProfileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallProcessingPolicyProfileName() {
        return callProcessingPolicyProfileName;
    }

    /**
     * Legt den Wert der callProcessingPolicyProfileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallProcessingPolicyProfileName(String value) {
        this.callProcessingPolicyProfileName = value;
    }

}
