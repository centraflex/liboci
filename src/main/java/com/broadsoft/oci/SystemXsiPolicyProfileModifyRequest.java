//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the system Xsi policy profile entry.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemXsiPolicyProfileModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemXsiPolicyProfileModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="xsiPolicyProfile" type="{}XsiPolicyProfileKey"/>
 *         <element name="newXsiPolicyProfileName" type="{}XsiPolicyProfileName" minOccurs="0"/>
 *         <element name="description" type="{}XsiPolicyProfileDescription" minOccurs="0"/>
 *         <element name="maxTargetSubscription" type="{}MaxTargetSubscriptionPerEventPackage" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemXsiPolicyProfileModifyRequest", propOrder = {
    "xsiPolicyProfile",
    "newXsiPolicyProfileName",
    "description",
    "maxTargetSubscription"
})
public class SystemXsiPolicyProfileModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    protected XsiPolicyProfileKey xsiPolicyProfile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newXsiPolicyProfileName;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    protected Integer maxTargetSubscription;

    /**
     * Ruft den Wert der xsiPolicyProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XsiPolicyProfileKey }
     *     
     */
    public XsiPolicyProfileKey getXsiPolicyProfile() {
        return xsiPolicyProfile;
    }

    /**
     * Legt den Wert der xsiPolicyProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XsiPolicyProfileKey }
     *     
     */
    public void setXsiPolicyProfile(XsiPolicyProfileKey value) {
        this.xsiPolicyProfile = value;
    }

    /**
     * Ruft den Wert der newXsiPolicyProfileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewXsiPolicyProfileName() {
        return newXsiPolicyProfileName;
    }

    /**
     * Legt den Wert der newXsiPolicyProfileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewXsiPolicyProfileName(String value) {
        this.newXsiPolicyProfileName = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der maxTargetSubscription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxTargetSubscription() {
        return maxTargetSubscription;
    }

    /**
     * Legt den Wert der maxTargetSubscription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxTargetSubscription(Integer value) {
        this.maxTargetSubscription = value;
    }

}
