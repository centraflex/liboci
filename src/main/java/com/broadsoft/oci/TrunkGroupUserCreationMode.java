//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TrunkGroupUserCreationMode.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="TrunkGroupUserCreationMode">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Extension"/>
 *     <enumeration value="Phone Number"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TrunkGroupUserCreationMode")
@XmlEnum
public enum TrunkGroupUserCreationMode {

    @XmlEnumValue("Extension")
    EXTENSION("Extension"),
    @XmlEnumValue("Phone Number")
    PHONE_NUMBER("Phone Number");
    private final String value;

    TrunkGroupUserCreationMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrunkGroupUserCreationMode fromValue(String v) {
        for (TrunkGroupUserCreationMode c: TrunkGroupUserCreationMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
