//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemSMDIParametersGetListRequest.
 *         Contains a list of system SMDI parameters.
 *       
 * 
 * <p>Java-Klasse für SystemSMDIParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSMDIParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableSMDI" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="listeningPort" type="{}Port1025"/>
 *         <element name="maxConnections" type="{}SMDIMaxConnections"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSMDIParametersGetResponse", propOrder = {
    "enableSMDI",
    "listeningPort",
    "maxConnections"
})
public class SystemSMDIParametersGetResponse
    extends OCIDataResponse
{

    protected boolean enableSMDI;
    protected int listeningPort;
    protected int maxConnections;

    /**
     * Ruft den Wert der enableSMDI-Eigenschaft ab.
     * 
     */
    public boolean isEnableSMDI() {
        return enableSMDI;
    }

    /**
     * Legt den Wert der enableSMDI-Eigenschaft fest.
     * 
     */
    public void setEnableSMDI(boolean value) {
        this.enableSMDI = value;
    }

    /**
     * Ruft den Wert der listeningPort-Eigenschaft ab.
     * 
     */
    public int getListeningPort() {
        return listeningPort;
    }

    /**
     * Legt den Wert der listeningPort-Eigenschaft fest.
     * 
     */
    public void setListeningPort(int value) {
        this.listeningPort = value;
    }

    /**
     * Ruft den Wert der maxConnections-Eigenschaft ab.
     * 
     */
    public int getMaxConnections() {
        return maxConnections;
    }

    /**
     * Legt den Wert der maxConnections-Eigenschaft fest.
     * 
     */
    public void setMaxConnections(int value) {
        this.maxConnections = value;
    }

}
