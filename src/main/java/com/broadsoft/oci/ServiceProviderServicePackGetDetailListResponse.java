//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to ServiceProviderServicePackGetDetailListRequest. It contains the service pack details
 *         and the list of services in a table format. The column headings are "Service", "Authorized"
 *         "Allocated" and "Available".
 *       
 * 
 * <p>Java-Klasse für ServiceProviderServicePackGetDetailListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderServicePackGetDetailListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="servicePackName" type="{}ServicePackName"/>
 *         <element name="servicePackDescription" type="{}ServicePackDescription" minOccurs="0"/>
 *         <element name="isAvailableForUse" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="servicePackQuantity" type="{}UnboundedPositiveInt"/>
 *         <element name="assignedQuantity" type="{}UnboundedNonNegativeInt"/>
 *         <element name="allowedQuantity" type="{}UnboundedPositiveInt"/>
 *         <element name="userServiceTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderServicePackGetDetailListResponse", propOrder = {
    "servicePackName",
    "servicePackDescription",
    "isAvailableForUse",
    "servicePackQuantity",
    "assignedQuantity",
    "allowedQuantity",
    "userServiceTable"
})
public class ServiceProviderServicePackGetDetailListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String servicePackName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String servicePackDescription;
    protected boolean isAvailableForUse;
    @XmlElement(required = true)
    protected UnboundedPositiveInt servicePackQuantity;
    @XmlElement(required = true)
    protected UnboundedNonNegativeInt assignedQuantity;
    @XmlElement(required = true)
    protected UnboundedPositiveInt allowedQuantity;
    @XmlElement(required = true)
    protected OCITable userServiceTable;

    /**
     * Ruft den Wert der servicePackName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicePackName() {
        return servicePackName;
    }

    /**
     * Legt den Wert der servicePackName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicePackName(String value) {
        this.servicePackName = value;
    }

    /**
     * Ruft den Wert der servicePackDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicePackDescription() {
        return servicePackDescription;
    }

    /**
     * Legt den Wert der servicePackDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicePackDescription(String value) {
        this.servicePackDescription = value;
    }

    /**
     * Ruft den Wert der isAvailableForUse-Eigenschaft ab.
     * 
     */
    public boolean isIsAvailableForUse() {
        return isAvailableForUse;
    }

    /**
     * Legt den Wert der isAvailableForUse-Eigenschaft fest.
     * 
     */
    public void setIsAvailableForUse(boolean value) {
        this.isAvailableForUse = value;
    }

    /**
     * Ruft den Wert der servicePackQuantity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public UnboundedPositiveInt getServicePackQuantity() {
        return servicePackQuantity;
    }

    /**
     * Legt den Wert der servicePackQuantity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public void setServicePackQuantity(UnboundedPositiveInt value) {
        this.servicePackQuantity = value;
    }

    /**
     * Ruft den Wert der assignedQuantity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public UnboundedNonNegativeInt getAssignedQuantity() {
        return assignedQuantity;
    }

    /**
     * Legt den Wert der assignedQuantity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public void setAssignedQuantity(UnboundedNonNegativeInt value) {
        this.assignedQuantity = value;
    }

    /**
     * Ruft den Wert der allowedQuantity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public UnboundedPositiveInt getAllowedQuantity() {
        return allowedQuantity;
    }

    /**
     * Legt den Wert der allowedQuantity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public void setAllowedQuantity(UnboundedPositiveInt value) {
        this.allowedQuantity = value;
    }

    /**
     * Ruft den Wert der userServiceTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserServiceTable() {
        return userServiceTable;
    }

    /**
     * Legt den Wert der userServiceTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserServiceTable(OCITable value) {
        this.userServiceTable = value;
    }

}
