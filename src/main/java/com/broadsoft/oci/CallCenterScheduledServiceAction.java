//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterScheduledServiceAction.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterScheduledServiceAction">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="None"/>
 *     <enumeration value="Busy"/>
 *     <enumeration value="Transfer"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterScheduledServiceAction")
@XmlEnum
public enum CallCenterScheduledServiceAction {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Busy")
    BUSY("Busy"),
    @XmlEnumValue("Transfer")
    TRANSFER("Transfer");
    private final String value;

    CallCenterScheduledServiceAction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterScheduledServiceAction fromValue(String v) {
        for (CallCenterScheduledServiceAction c: CallCenterScheduledServiceAction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
