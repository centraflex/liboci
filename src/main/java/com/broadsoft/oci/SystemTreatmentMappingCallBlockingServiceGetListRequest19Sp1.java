//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Requests a table of all the existing Call Blocking Service Mappings in the system.
 *         The response is either a SystemTreatmentMappingCallBlockingServicesGetListResponse19sp1
 *         or an ErrorResponse.
 *         
 *         Replaced by: SystemTreatmentMappingCallBlockingServiceGetListRequest22
 *       
 * 
 * <p>Java-Klasse für SystemTreatmentMappingCallBlockingServiceGetListRequest19sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemTreatmentMappingCallBlockingServiceGetListRequest19sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemTreatmentMappingCallBlockingServiceGetListRequest19sp1")
public class SystemTreatmentMappingCallBlockingServiceGetListRequest19Sp1
    extends OCIRequest
{


}
