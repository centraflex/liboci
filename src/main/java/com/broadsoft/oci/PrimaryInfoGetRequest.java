//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Requests information about the primary server for high-availability support.
 *       
 * 
 * <p>Java-Klasse für PrimaryInfoGetRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="PrimaryInfoGetRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="isPrivate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isAddressInfoRequested" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrimaryInfoGetRequest", propOrder = {
    "isPrivate",
    "isAddressInfoRequested"
})
public class PrimaryInfoGetRequest
    extends OCIRequest
{

    protected boolean isPrivate;
    protected boolean isAddressInfoRequested;

    /**
     * Ruft den Wert der isPrivate-Eigenschaft ab.
     * 
     */
    public boolean isIsPrivate() {
        return isPrivate;
    }

    /**
     * Legt den Wert der isPrivate-Eigenschaft fest.
     * 
     */
    public void setIsPrivate(boolean value) {
        this.isPrivate = value;
    }

    /**
     * Ruft den Wert der isAddressInfoRequested-Eigenschaft ab.
     * 
     */
    public boolean isIsAddressInfoRequested() {
        return isAddressInfoRequested;
    }

    /**
     * Legt den Wert der isAddressInfoRequested-Eigenschaft fest.
     * 
     */
    public void setIsAddressInfoRequested(boolean value) {
        this.isAddressInfoRequested = value;
    }

}
