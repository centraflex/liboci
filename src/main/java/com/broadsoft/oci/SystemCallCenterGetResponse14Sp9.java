//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCallCenterGetRequest14sp9.
 *       
 * 
 * <p>Java-Klasse für SystemCallCenterGetResponse14sp9 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallCenterGetResponse14sp9">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="defaultFromAddress" type="{}EmailAddress"/>
 *         <element name="statisticsSamplingPeriodMinutes" type="{}CallCenterStatisticsSamplingPeriodMinutes"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallCenterGetResponse14sp9", propOrder = {
    "defaultFromAddress",
    "statisticsSamplingPeriodMinutes"
})
public class SystemCallCenterGetResponse14Sp9
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultFromAddress;
    protected int statisticsSamplingPeriodMinutes;

    /**
     * Ruft den Wert der defaultFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultFromAddress() {
        return defaultFromAddress;
    }

    /**
     * Legt den Wert der defaultFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultFromAddress(String value) {
        this.defaultFromAddress = value;
    }

    /**
     * Ruft den Wert der statisticsSamplingPeriodMinutes-Eigenschaft ab.
     * 
     */
    public int getStatisticsSamplingPeriodMinutes() {
        return statisticsSamplingPeriodMinutes;
    }

    /**
     * Legt den Wert der statisticsSamplingPeriodMinutes-Eigenschaft fest.
     * 
     */
    public void setStatisticsSamplingPeriodMinutes(int value) {
        this.statisticsSamplingPeriodMinutes = value;
    }

}
