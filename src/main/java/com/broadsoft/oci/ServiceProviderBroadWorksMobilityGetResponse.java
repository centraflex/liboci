//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The response to a ServiceProviderBroadWorksMobilityGetRequest.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderBroadWorksMobilityGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderBroadWorksMobilityGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useSettingLevel" type="{}BroadWorksMobilityServiceProviderSettingLevel"/>
 *         <element name="enableLocationServices" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableMSRNLookup" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableMobileStateChecking" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="denyCallOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="denyCallTerminations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderBroadWorksMobilityGetResponse", propOrder = {
    "useSettingLevel",
    "enableLocationServices",
    "enableMSRNLookup",
    "enableMobileStateChecking",
    "denyCallOriginations",
    "denyCallTerminations"
})
public class ServiceProviderBroadWorksMobilityGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BroadWorksMobilityServiceProviderSettingLevel useSettingLevel;
    protected boolean enableLocationServices;
    protected boolean enableMSRNLookup;
    protected boolean enableMobileStateChecking;
    protected boolean denyCallOriginations;
    protected boolean denyCallTerminations;

    /**
     * Ruft den Wert der useSettingLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksMobilityServiceProviderSettingLevel }
     *     
     */
    public BroadWorksMobilityServiceProviderSettingLevel getUseSettingLevel() {
        return useSettingLevel;
    }

    /**
     * Legt den Wert der useSettingLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksMobilityServiceProviderSettingLevel }
     *     
     */
    public void setUseSettingLevel(BroadWorksMobilityServiceProviderSettingLevel value) {
        this.useSettingLevel = value;
    }

    /**
     * Ruft den Wert der enableLocationServices-Eigenschaft ab.
     * 
     */
    public boolean isEnableLocationServices() {
        return enableLocationServices;
    }

    /**
     * Legt den Wert der enableLocationServices-Eigenschaft fest.
     * 
     */
    public void setEnableLocationServices(boolean value) {
        this.enableLocationServices = value;
    }

    /**
     * Ruft den Wert der enableMSRNLookup-Eigenschaft ab.
     * 
     */
    public boolean isEnableMSRNLookup() {
        return enableMSRNLookup;
    }

    /**
     * Legt den Wert der enableMSRNLookup-Eigenschaft fest.
     * 
     */
    public void setEnableMSRNLookup(boolean value) {
        this.enableMSRNLookup = value;
    }

    /**
     * Ruft den Wert der enableMobileStateChecking-Eigenschaft ab.
     * 
     */
    public boolean isEnableMobileStateChecking() {
        return enableMobileStateChecking;
    }

    /**
     * Legt den Wert der enableMobileStateChecking-Eigenschaft fest.
     * 
     */
    public void setEnableMobileStateChecking(boolean value) {
        this.enableMobileStateChecking = value;
    }

    /**
     * Ruft den Wert der denyCallOriginations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallOriginations() {
        return denyCallOriginations;
    }

    /**
     * Legt den Wert der denyCallOriginations-Eigenschaft fest.
     * 
     */
    public void setDenyCallOriginations(boolean value) {
        this.denyCallOriginations = value;
    }

    /**
     * Ruft den Wert der denyCallTerminations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallTerminations() {
        return denyCallTerminations;
    }

    /**
     * Legt den Wert der denyCallTerminations-Eigenschaft fest.
     * 
     */
    public void setDenyCallTerminations(boolean value) {
        this.denyCallTerminations = value;
    }

}
