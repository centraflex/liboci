//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterReportAbadonedCallsInServiceLevel.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterReportAbadonedCallsInServiceLevel">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Ignore All Abandoned Calls"/>
 *     <enumeration value="Include All Abandoned Calls"/>
 *     <enumeration value="Include Abandoned Calls Except Before Entrance Completes"/>
 *     <enumeration value="Include Abandoned Calls Except In Interval"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterReportAbadonedCallsInServiceLevel")
@XmlEnum
public enum CallCenterReportAbadonedCallsInServiceLevel {

    @XmlEnumValue("Ignore All Abandoned Calls")
    IGNORE_ALL_ABANDONED_CALLS("Ignore All Abandoned Calls"),
    @XmlEnumValue("Include All Abandoned Calls")
    INCLUDE_ALL_ABANDONED_CALLS("Include All Abandoned Calls"),
    @XmlEnumValue("Include Abandoned Calls Except Before Entrance Completes")
    INCLUDE_ABANDONED_CALLS_EXCEPT_BEFORE_ENTRANCE_COMPLETES("Include Abandoned Calls Except Before Entrance Completes"),
    @XmlEnumValue("Include Abandoned Calls Except In Interval")
    INCLUDE_ABANDONED_CALLS_EXCEPT_IN_INTERVAL("Include Abandoned Calls Except In Interval");
    private final String value;

    CallCenterReportAbadonedCallsInServiceLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterReportAbadonedCallsInServiceLevel fromValue(String v) {
        for (CallCenterReportAbadonedCallsInServiceLevel c: CallCenterReportAbadonedCallsInServiceLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
