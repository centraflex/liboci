//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupTrunkGroupGetRequest.
 *         The response contains the maximum permissible active trunk group calls for the group.
 *       
 * 
 * <p>Java-Klasse für GroupTrunkGroupGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupTrunkGroupGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maxActiveCalls" type="{}UnboundedNonNegativeInt"/>
 *         <element name="maxAvailableActiveCalls" type="{}UnboundedNonNegativeInt"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupTrunkGroupGetResponse", propOrder = {
    "maxActiveCalls",
    "maxAvailableActiveCalls"
})
public class GroupTrunkGroupGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected UnboundedNonNegativeInt maxActiveCalls;
    @XmlElement(required = true)
    protected UnboundedNonNegativeInt maxAvailableActiveCalls;

    /**
     * Ruft den Wert der maxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public UnboundedNonNegativeInt getMaxActiveCalls() {
        return maxActiveCalls;
    }

    /**
     * Legt den Wert der maxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public void setMaxActiveCalls(UnboundedNonNegativeInt value) {
        this.maxActiveCalls = value;
    }

    /**
     * Ruft den Wert der maxAvailableActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public UnboundedNonNegativeInt getMaxAvailableActiveCalls() {
        return maxAvailableActiveCalls;
    }

    /**
     * Legt den Wert der maxAvailableActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public void setMaxAvailableActiveCalls(UnboundedNonNegativeInt value) {
        this.maxAvailableActiveCalls = value;
    }

}
