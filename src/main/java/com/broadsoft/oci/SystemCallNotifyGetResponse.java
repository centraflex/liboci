//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCallNotifyGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemCallNotifyGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallNotifyGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="defaultFromAddress" type="{}EmailAddress"/>
 *         <element name="useShortSubjectLine" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useDnInMailBody" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallNotifyGetResponse", propOrder = {
    "defaultFromAddress",
    "useShortSubjectLine",
    "useDnInMailBody"
})
public class SystemCallNotifyGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultFromAddress;
    protected boolean useShortSubjectLine;
    protected boolean useDnInMailBody;

    /**
     * Ruft den Wert der defaultFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultFromAddress() {
        return defaultFromAddress;
    }

    /**
     * Legt den Wert der defaultFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultFromAddress(String value) {
        this.defaultFromAddress = value;
    }

    /**
     * Ruft den Wert der useShortSubjectLine-Eigenschaft ab.
     * 
     */
    public boolean isUseShortSubjectLine() {
        return useShortSubjectLine;
    }

    /**
     * Legt den Wert der useShortSubjectLine-Eigenschaft fest.
     * 
     */
    public void setUseShortSubjectLine(boolean value) {
        this.useShortSubjectLine = value;
    }

    /**
     * Ruft den Wert der useDnInMailBody-Eigenschaft ab.
     * 
     */
    public boolean isUseDnInMailBody() {
        return useDnInMailBody;
    }

    /**
     * Legt den Wert der useDnInMailBody-Eigenschaft fest.
     * 
     */
    public void setUseDnInMailBody(boolean value) {
        this.useDnInMailBody = value;
    }

}
