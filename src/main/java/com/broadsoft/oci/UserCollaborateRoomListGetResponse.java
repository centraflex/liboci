//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserCollaborateRoomListGetRequest.
 *         Contains a table with column headings :  "Room Type", "Name", "Room Id",
 *         in a row for each collaborate room instance.  
 *         Possible values for Room Type column are MyRoom, Project Room and
 *         Instant Room. 
 *       
 * 
 * <p>Java-Klasse für UserCollaborateRoomListGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCollaborateRoomListGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="roomInstanceTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCollaborateRoomListGetResponse", propOrder = {
    "roomInstanceTable"
})
public class UserCollaborateRoomListGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable roomInstanceTable;

    /**
     * Ruft den Wert der roomInstanceTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getRoomInstanceTable() {
        return roomInstanceTable;
    }

    /**
     * Legt den Wert der roomInstanceTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setRoomInstanceTable(OCITable value) {
        this.roomInstanceTable = value;
    }

}
