//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemBusyLampFieldGetRequest23.
 *         
 *         The following elements are only used in AS data mode:
 *          forceUseOfTCP
 *          enableRedundancy, value "false" is returned in XS data mode
 *          redundancyTaskDelayMilliseconds, value "10000" is returned in Amplify and XS data mode
 *          redundancyTaskIntervalMilliseconds, value of "1000" is returned in Amplify and XS data mode
 *          maxNumberOfSubscriptionsPerRedundancyTaskInterval, value of "50" is returned in Amplify and XS data mode
 *         Replaced by: SystemBusyLampFieldGetResponse23V2 in AS data mode.
 *       
 * 
 * <p>Java-Klasse für SystemBusyLampFieldGetResponse23 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBusyLampFieldGetResponse23">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="displayLocalUserIdentityLastNameFirst" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forceUseOfTCP" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableRedundancy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="redundancyTaskDelayMilliseconds" type="{}BusyLampFieldTaskDelayMilliseconds"/>
 *         <element name="redundancyTaskIntervalMilliseconds" type="{}BusyLampFieldTaskIntervalMilliseconds"/>
 *         <element name="maxNumberOfSubscriptionsPerRedundancyTaskInterval" type="{}BusyLampFieldMaxSubscriptionPerInterval"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBusyLampFieldGetResponse23", propOrder = {
    "displayLocalUserIdentityLastNameFirst",
    "forceUseOfTCP",
    "enableRedundancy",
    "redundancyTaskDelayMilliseconds",
    "redundancyTaskIntervalMilliseconds",
    "maxNumberOfSubscriptionsPerRedundancyTaskInterval"
})
public class SystemBusyLampFieldGetResponse23
    extends OCIDataResponse
{

    protected boolean displayLocalUserIdentityLastNameFirst;
    protected boolean forceUseOfTCP;
    protected boolean enableRedundancy;
    protected int redundancyTaskDelayMilliseconds;
    protected int redundancyTaskIntervalMilliseconds;
    protected int maxNumberOfSubscriptionsPerRedundancyTaskInterval;

    /**
     * Ruft den Wert der displayLocalUserIdentityLastNameFirst-Eigenschaft ab.
     * 
     */
    public boolean isDisplayLocalUserIdentityLastNameFirst() {
        return displayLocalUserIdentityLastNameFirst;
    }

    /**
     * Legt den Wert der displayLocalUserIdentityLastNameFirst-Eigenschaft fest.
     * 
     */
    public void setDisplayLocalUserIdentityLastNameFirst(boolean value) {
        this.displayLocalUserIdentityLastNameFirst = value;
    }

    /**
     * Ruft den Wert der forceUseOfTCP-Eigenschaft ab.
     * 
     */
    public boolean isForceUseOfTCP() {
        return forceUseOfTCP;
    }

    /**
     * Legt den Wert der forceUseOfTCP-Eigenschaft fest.
     * 
     */
    public void setForceUseOfTCP(boolean value) {
        this.forceUseOfTCP = value;
    }

    /**
     * Ruft den Wert der enableRedundancy-Eigenschaft ab.
     * 
     */
    public boolean isEnableRedundancy() {
        return enableRedundancy;
    }

    /**
     * Legt den Wert der enableRedundancy-Eigenschaft fest.
     * 
     */
    public void setEnableRedundancy(boolean value) {
        this.enableRedundancy = value;
    }

    /**
     * Ruft den Wert der redundancyTaskDelayMilliseconds-Eigenschaft ab.
     * 
     */
    public int getRedundancyTaskDelayMilliseconds() {
        return redundancyTaskDelayMilliseconds;
    }

    /**
     * Legt den Wert der redundancyTaskDelayMilliseconds-Eigenschaft fest.
     * 
     */
    public void setRedundancyTaskDelayMilliseconds(int value) {
        this.redundancyTaskDelayMilliseconds = value;
    }

    /**
     * Ruft den Wert der redundancyTaskIntervalMilliseconds-Eigenschaft ab.
     * 
     */
    public int getRedundancyTaskIntervalMilliseconds() {
        return redundancyTaskIntervalMilliseconds;
    }

    /**
     * Legt den Wert der redundancyTaskIntervalMilliseconds-Eigenschaft fest.
     * 
     */
    public void setRedundancyTaskIntervalMilliseconds(int value) {
        this.redundancyTaskIntervalMilliseconds = value;
    }

    /**
     * Ruft den Wert der maxNumberOfSubscriptionsPerRedundancyTaskInterval-Eigenschaft ab.
     * 
     */
    public int getMaxNumberOfSubscriptionsPerRedundancyTaskInterval() {
        return maxNumberOfSubscriptionsPerRedundancyTaskInterval;
    }

    /**
     * Legt den Wert der maxNumberOfSubscriptionsPerRedundancyTaskInterval-Eigenschaft fest.
     * 
     */
    public void setMaxNumberOfSubscriptionsPerRedundancyTaskInterval(int value) {
        this.maxNumberOfSubscriptionsPerRedundancyTaskInterval = value;
    }

}
