//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The response to a SystemBroadWorksMobilityGetRequest22.
 *         
 *         Replaced by SystemBroadWorksMobilityGetResponse22V2. 
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobilityGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobilityGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableLocationServices" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableMSRNLookup" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableMobileStateChecking" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="denyCallOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="denyCallTerminations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="imrnTimeoutMillisecnds" type="{}IMRNTimeoutMilliseconds"/>
 *         <element name="enableInternalCLIDDelivery" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeRedirectForMobilityTermination" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobilityGetResponse22", propOrder = {
    "enableLocationServices",
    "enableMSRNLookup",
    "enableMobileStateChecking",
    "denyCallOriginations",
    "denyCallTerminations",
    "imrnTimeoutMillisecnds",
    "enableInternalCLIDDelivery",
    "includeRedirectForMobilityTermination"
})
public class SystemBroadWorksMobilityGetResponse22
    extends OCIDataResponse
{

    protected boolean enableLocationServices;
    protected boolean enableMSRNLookup;
    protected boolean enableMobileStateChecking;
    protected boolean denyCallOriginations;
    protected boolean denyCallTerminations;
    protected int imrnTimeoutMillisecnds;
    protected boolean enableInternalCLIDDelivery;
    protected boolean includeRedirectForMobilityTermination;

    /**
     * Ruft den Wert der enableLocationServices-Eigenschaft ab.
     * 
     */
    public boolean isEnableLocationServices() {
        return enableLocationServices;
    }

    /**
     * Legt den Wert der enableLocationServices-Eigenschaft fest.
     * 
     */
    public void setEnableLocationServices(boolean value) {
        this.enableLocationServices = value;
    }

    /**
     * Ruft den Wert der enableMSRNLookup-Eigenschaft ab.
     * 
     */
    public boolean isEnableMSRNLookup() {
        return enableMSRNLookup;
    }

    /**
     * Legt den Wert der enableMSRNLookup-Eigenschaft fest.
     * 
     */
    public void setEnableMSRNLookup(boolean value) {
        this.enableMSRNLookup = value;
    }

    /**
     * Ruft den Wert der enableMobileStateChecking-Eigenschaft ab.
     * 
     */
    public boolean isEnableMobileStateChecking() {
        return enableMobileStateChecking;
    }

    /**
     * Legt den Wert der enableMobileStateChecking-Eigenschaft fest.
     * 
     */
    public void setEnableMobileStateChecking(boolean value) {
        this.enableMobileStateChecking = value;
    }

    /**
     * Ruft den Wert der denyCallOriginations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallOriginations() {
        return denyCallOriginations;
    }

    /**
     * Legt den Wert der denyCallOriginations-Eigenschaft fest.
     * 
     */
    public void setDenyCallOriginations(boolean value) {
        this.denyCallOriginations = value;
    }

    /**
     * Ruft den Wert der denyCallTerminations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallTerminations() {
        return denyCallTerminations;
    }

    /**
     * Legt den Wert der denyCallTerminations-Eigenschaft fest.
     * 
     */
    public void setDenyCallTerminations(boolean value) {
        this.denyCallTerminations = value;
    }

    /**
     * Ruft den Wert der imrnTimeoutMillisecnds-Eigenschaft ab.
     * 
     */
    public int getImrnTimeoutMillisecnds() {
        return imrnTimeoutMillisecnds;
    }

    /**
     * Legt den Wert der imrnTimeoutMillisecnds-Eigenschaft fest.
     * 
     */
    public void setImrnTimeoutMillisecnds(int value) {
        this.imrnTimeoutMillisecnds = value;
    }

    /**
     * Ruft den Wert der enableInternalCLIDDelivery-Eigenschaft ab.
     * 
     */
    public boolean isEnableInternalCLIDDelivery() {
        return enableInternalCLIDDelivery;
    }

    /**
     * Legt den Wert der enableInternalCLIDDelivery-Eigenschaft fest.
     * 
     */
    public void setEnableInternalCLIDDelivery(boolean value) {
        this.enableInternalCLIDDelivery = value;
    }

    /**
     * Ruft den Wert der includeRedirectForMobilityTermination-Eigenschaft ab.
     * 
     */
    public boolean isIncludeRedirectForMobilityTermination() {
        return includeRedirectForMobilityTermination;
    }

    /**
     * Legt den Wert der includeRedirectForMobilityTermination-Eigenschaft fest.
     * 
     */
    public void setIncludeRedirectForMobilityTermination(boolean value) {
        this.includeRedirectForMobilityTermination = value;
    }

}
