//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *          Response to a UserBroadWorksMobilityGetRequest21.
 *       
 * 
 * <p>Java-Klasse für UserBroadWorksMobilityGetResponse21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksMobilityGetResponse21">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useMobileIdentityCallAnchoring" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="preventCallsToOwnMobiles" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="mobileIdentity" type="{}BroadWorksMobilityUserMobileIdentityEntry" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="profileIdentityDevicesToRing" type="{}BroadWorksMobilityPhoneToRing"/>
 *         <element name="profileIdentityIncludeSharedCallAppearance" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="profileIdentityIncludeBroadworksAnywhere" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="profileIdentityIncludeExecutiveAssistant" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="profileIdentityMobileNumberAlerted" type="{}DN" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksMobilityGetResponse21", propOrder = {
    "isActive",
    "useMobileIdentityCallAnchoring",
    "preventCallsToOwnMobiles",
    "mobileIdentity",
    "profileIdentityDevicesToRing",
    "profileIdentityIncludeSharedCallAppearance",
    "profileIdentityIncludeBroadworksAnywhere",
    "profileIdentityIncludeExecutiveAssistant",
    "profileIdentityMobileNumberAlerted"
})
public class UserBroadWorksMobilityGetResponse21
    extends OCIDataResponse
{

    protected boolean isActive;
    protected boolean useMobileIdentityCallAnchoring;
    protected boolean preventCallsToOwnMobiles;
    protected List<BroadWorksMobilityUserMobileIdentityEntry> mobileIdentity;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BroadWorksMobilityPhoneToRing profileIdentityDevicesToRing;
    protected boolean profileIdentityIncludeSharedCallAppearance;
    protected boolean profileIdentityIncludeBroadworksAnywhere;
    protected boolean profileIdentityIncludeExecutiveAssistant;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> profileIdentityMobileNumberAlerted;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der useMobileIdentityCallAnchoring-Eigenschaft ab.
     * 
     */
    public boolean isUseMobileIdentityCallAnchoring() {
        return useMobileIdentityCallAnchoring;
    }

    /**
     * Legt den Wert der useMobileIdentityCallAnchoring-Eigenschaft fest.
     * 
     */
    public void setUseMobileIdentityCallAnchoring(boolean value) {
        this.useMobileIdentityCallAnchoring = value;
    }

    /**
     * Ruft den Wert der preventCallsToOwnMobiles-Eigenschaft ab.
     * 
     */
    public boolean isPreventCallsToOwnMobiles() {
        return preventCallsToOwnMobiles;
    }

    /**
     * Legt den Wert der preventCallsToOwnMobiles-Eigenschaft fest.
     * 
     */
    public void setPreventCallsToOwnMobiles(boolean value) {
        this.preventCallsToOwnMobiles = value;
    }

    /**
     * Gets the value of the mobileIdentity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the mobileIdentity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMobileIdentity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BroadWorksMobilityUserMobileIdentityEntry }
     * 
     * 
     * @return
     *     The value of the mobileIdentity property.
     */
    public List<BroadWorksMobilityUserMobileIdentityEntry> getMobileIdentity() {
        if (mobileIdentity == null) {
            mobileIdentity = new ArrayList<>();
        }
        return this.mobileIdentity;
    }

    /**
     * Ruft den Wert der profileIdentityDevicesToRing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksMobilityPhoneToRing }
     *     
     */
    public BroadWorksMobilityPhoneToRing getProfileIdentityDevicesToRing() {
        return profileIdentityDevicesToRing;
    }

    /**
     * Legt den Wert der profileIdentityDevicesToRing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksMobilityPhoneToRing }
     *     
     */
    public void setProfileIdentityDevicesToRing(BroadWorksMobilityPhoneToRing value) {
        this.profileIdentityDevicesToRing = value;
    }

    /**
     * Ruft den Wert der profileIdentityIncludeSharedCallAppearance-Eigenschaft ab.
     * 
     */
    public boolean isProfileIdentityIncludeSharedCallAppearance() {
        return profileIdentityIncludeSharedCallAppearance;
    }

    /**
     * Legt den Wert der profileIdentityIncludeSharedCallAppearance-Eigenschaft fest.
     * 
     */
    public void setProfileIdentityIncludeSharedCallAppearance(boolean value) {
        this.profileIdentityIncludeSharedCallAppearance = value;
    }

    /**
     * Ruft den Wert der profileIdentityIncludeBroadworksAnywhere-Eigenschaft ab.
     * 
     */
    public boolean isProfileIdentityIncludeBroadworksAnywhere() {
        return profileIdentityIncludeBroadworksAnywhere;
    }

    /**
     * Legt den Wert der profileIdentityIncludeBroadworksAnywhere-Eigenschaft fest.
     * 
     */
    public void setProfileIdentityIncludeBroadworksAnywhere(boolean value) {
        this.profileIdentityIncludeBroadworksAnywhere = value;
    }

    /**
     * Ruft den Wert der profileIdentityIncludeExecutiveAssistant-Eigenschaft ab.
     * 
     */
    public boolean isProfileIdentityIncludeExecutiveAssistant() {
        return profileIdentityIncludeExecutiveAssistant;
    }

    /**
     * Legt den Wert der profileIdentityIncludeExecutiveAssistant-Eigenschaft fest.
     * 
     */
    public void setProfileIdentityIncludeExecutiveAssistant(boolean value) {
        this.profileIdentityIncludeExecutiveAssistant = value;
    }

    /**
     * Gets the value of the profileIdentityMobileNumberAlerted property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the profileIdentityMobileNumberAlerted property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProfileIdentityMobileNumberAlerted().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the profileIdentityMobileNumberAlerted property.
     */
    public List<String> getProfileIdentityMobileNumberAlerted() {
        if (profileIdentityMobileNumberAlerted == null) {
            profileIdentityMobileNumberAlerted = new ArrayList<>();
        }
        return this.profileIdentityMobileNumberAlerted;
    }

}
