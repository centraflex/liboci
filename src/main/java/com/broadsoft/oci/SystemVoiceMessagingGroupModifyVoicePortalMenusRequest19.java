//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the system voice portal menus setting.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="useVoicePortalCustomization" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="voicePortalMainMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="voiceMessaging" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="commPilotExpressProfile" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="greetings" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="callForwardingOptions" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="voicePortalCalling" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="hoteling" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="passcode" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="exitVoicePortal" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="externalRouting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="announcementMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="personalAssistant" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="announcementMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="recordAudio" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="recordAudioVideo" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="announcementRecordingMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="acceptRecording" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="rejectRerecord" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="end" type="{}VoicePortalDigitSequence" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="greetingsMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="conferenceGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="conferenceGreetingMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="activateConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deactivateConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="recordNewConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToCurrentConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="voiceMessagingMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="playMessages" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="changeBusyGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="changeNoAnswerGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="changeExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deleteAllMessages" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="passcode" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="messageDeposit" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="playGreetingMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="skipBackward" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="pauseOrResume" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="skipForward" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="jumpToBegin" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="jumpToEnd" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="changeBusyOrNoAnswerGreetingMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="recordNewGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToCurrentGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="revertToSystemDefaultGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="changeExtendedAwayGreetingMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="activateExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deactivateExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="recordNewGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToCurrentGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="enableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="disableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="recordNewGreetingOrPersonalizedNameMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="endRecording" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="deleteAllMessagesMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="confirmDeletion" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="cancelDeletion" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="commPilotExpressProfileMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="activateAvailableInOfficeProfile" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="activateAvailableOutOfOfficeProfile" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="activateBusyProfile" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="activateUnavailableProfile" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="noProfile" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="personalizedNameMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="recordNewPersonalizedName" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToCurrentPersonalizedName" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deletePersonalizedName" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="callForwardingOptionsMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="activateCallForwarding" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deactivateCallForwarding" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="changeCallForwardingDestination" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToCallForwardingStatus" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="changeCallForwardingDestinationMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="finishEnteringNewDestinationNumber" type="{}DigitStarPound" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="voicePortalCallingMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="endCurrentCallAndGoBackToPreviousMenu" type="{}VoicePortalDigitSequence" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="hotelingMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="checkHostStatus" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="associateWithHost" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="disassociateFromHost" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="disassociateFromRemoteHost" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="passcodeMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="finishEnteringOrReenteringPasscode" type="{}DigitStarPound" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitStarPound" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="playMessagesMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="saveMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deleteMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="playMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="previousMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="nextMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="callbackCaller" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="replyMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="forwardMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="additionalMessageOptions" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="passcode" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="playMessageMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="skipBackward" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="pauseOrResume" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="skipForward" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="jumpToBegin" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="jumpToEnd" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="additionalMessageOptionsMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="saveMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deleteMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="callbackCaller" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="replyMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="forwardMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="passcode" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="forwardOrComposeMessageMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="sendToPerson" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="sendToAllGroupMembers" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="sendToDistributionList" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="changeCurrentIntroductionOrMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToCurrentIntroductionOrMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setOrClearUrgentIndicator" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setOrClearConfidentialIndicator" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="replyMessageMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="sendReplyToCaller" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="changeCurrentReply" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToCurrentReply" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setOrClearUrgentIndicator" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setOrClearConfidentialIndicator" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="sendToDistributionListMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="sendMessageToSelectedDistributionList" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="selectDistributionList" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="reviewSelectedDistributionList" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="selectDistributionListMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="returnToPreviousMenu" type="{}DigitStarPound" minOccurs="0"/>
 *                   <element name="repeatMenuOrFinishEnteringDistributionListNumber" type="{}DigitStarPound" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="reviewSelectedDistributionListMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="interruptPlaybackAndReturnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="sendMessageToSelectedDistributionListMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="confirmSendingToDistributionList" type="{}DigitStarPound" minOccurs="0"/>
 *                   <element name="cancelSendingToDistributionList" type="{}DigitStarPound" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="sendToAllGroupMembersMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="confirmSendingToEntireGroup" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="cancelSendingToEntireGroup" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="sendToPersonMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="confirmSendingMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="cancelSendingMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="finishEnteringNumberWhereToSendMessageTo" type="{}DigitStarPound" minOccurs="0"/>
 *                   <element name="finishForwardingOrSendingMessage" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="changeCurrentIntroductionOrMessageOrReplyMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="endRecording" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="voicePortalLoginMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="accessUsingOtherMailboxId" type="{}VoicePortalDigitSequence" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="faxMessagingMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="saveFaxMessageAndSkipToNext" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="previousFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="nextFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="deleteFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="printFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="messageDepositMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="enableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="disableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="listenToMessageDepositStatus" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="disableMessageDepositMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="disconnectAfterGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="forwardAfterGreeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="changeForwardingDestination" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="greetingOnlyForwardingDestinationMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="greetingOnlyForwardingDestination" type="{}DigitStarPound" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="personalAssistantMenuKeys" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="setPresenceToNone" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setPresenceToBusinessTrip" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setPresenceToGoneForTheDay" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setPresenceToLunch" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setPresenceToMeeting" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setPresenceToOutOfOffice" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setPresenceToTemporarilyOut" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setPresenceToTraining" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setPresenceToUnavailable" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="setPresenceToVacation" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *                   <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19", propOrder = {
    "useVoicePortalCustomization",
    "voicePortalMainMenuKeys",
    "announcementMenuKeys",
    "announcementRecordingMenuKeys",
    "greetingsMenuKeys",
    "conferenceGreetingMenuKeys",
    "voiceMessagingMenuKeys",
    "playGreetingMenuKeys",
    "changeBusyOrNoAnswerGreetingMenuKeys",
    "changeExtendedAwayGreetingMenuKeys",
    "recordNewGreetingOrPersonalizedNameMenuKeys",
    "deleteAllMessagesMenuKeys",
    "commPilotExpressProfileMenuKeys",
    "personalizedNameMenuKeys",
    "callForwardingOptionsMenuKeys",
    "changeCallForwardingDestinationMenuKeys",
    "voicePortalCallingMenuKeys",
    "hotelingMenuKeys",
    "passcodeMenuKeys",
    "playMessagesMenuKeys",
    "playMessageMenuKeys",
    "additionalMessageOptionsMenuKeys",
    "forwardOrComposeMessageMenuKeys",
    "replyMessageMenuKeys",
    "sendToDistributionListMenuKeys",
    "selectDistributionListMenuKeys",
    "reviewSelectedDistributionListMenuKeys",
    "sendMessageToSelectedDistributionListMenuKeys",
    "sendToAllGroupMembersMenuKeys",
    "sendToPersonMenuKeys",
    "changeCurrentIntroductionOrMessageOrReplyMenuKeys",
    "voicePortalLoginMenuKeys",
    "faxMessagingMenuKeys",
    "messageDepositMenuKeys",
    "disableMessageDepositMenuKeys",
    "greetingOnlyForwardingDestinationMenuKeys",
    "personalAssistantMenuKeys"
})
public class SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19
    extends OCIRequest
{

    protected Boolean useVoicePortalCustomization;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoicePortalMainMenuKeys voicePortalMainMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .AnnouncementMenuKeys announcementMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .AnnouncementRecordingMenuKeys announcementRecordingMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .GreetingsMenuKeys greetingsMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ConferenceGreetingMenuKeys conferenceGreetingMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoiceMessagingMenuKeys voiceMessagingMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PlayGreetingMenuKeys playGreetingMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeBusyOrNoAnswerGreetingMenuKeys changeBusyOrNoAnswerGreetingMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeExtendedAwayGreetingMenuKeys changeExtendedAwayGreetingMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .RecordNewGreetingOrPersonalizedNameMenuKeys recordNewGreetingOrPersonalizedNameMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .DeleteAllMessagesMenuKeys deleteAllMessagesMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .CommPilotExpressProfileMenuKeys commPilotExpressProfileMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PersonalizedNameMenuKeys personalizedNameMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .CallForwardingOptionsMenuKeys callForwardingOptionsMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeCallForwardingDestinationMenuKeys changeCallForwardingDestinationMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoicePortalCallingMenuKeys voicePortalCallingMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .HotelingMenuKeys hotelingMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PasscodeMenuKeys passcodeMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PlayMessagesMenuKeys playMessagesMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PlayMessageMenuKeys playMessageMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .AdditionalMessageOptionsMenuKeys additionalMessageOptionsMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ForwardOrComposeMessageMenuKeys forwardOrComposeMessageMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ReplyMessageMenuKeys replyMessageMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendToDistributionListMenuKeys sendToDistributionListMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SelectDistributionListMenuKeys selectDistributionListMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ReviewSelectedDistributionListMenuKeys reviewSelectedDistributionListMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendMessageToSelectedDistributionListMenuKeys sendMessageToSelectedDistributionListMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendToAllGroupMembersMenuKeys sendToAllGroupMembersMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendToPersonMenuKeys sendToPersonMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeCurrentIntroductionOrMessageOrReplyMenuKeys changeCurrentIntroductionOrMessageOrReplyMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoicePortalLoginMenuKeys voicePortalLoginMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .FaxMessagingMenuKeys faxMessagingMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .MessageDepositMenuKeys messageDepositMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .DisableMessageDepositMenuKeys disableMessageDepositMenuKeys;
    @XmlElementRef(name = "greetingOnlyForwardingDestinationMenuKeys", type = JAXBElement.class, required = false)
    protected JAXBElement<SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .GreetingOnlyForwardingDestinationMenuKeys> greetingOnlyForwardingDestinationMenuKeys;
    protected SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PersonalAssistantMenuKeys personalAssistantMenuKeys;

    /**
     * Ruft den Wert der useVoicePortalCustomization-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseVoicePortalCustomization() {
        return useVoicePortalCustomization;
    }

    /**
     * Legt den Wert der useVoicePortalCustomization-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseVoicePortalCustomization(Boolean value) {
        this.useVoicePortalCustomization = value;
    }

    /**
     * Ruft den Wert der voicePortalMainMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoicePortalMainMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoicePortalMainMenuKeys getVoicePortalMainMenuKeys() {
        return voicePortalMainMenuKeys;
    }

    /**
     * Legt den Wert der voicePortalMainMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoicePortalMainMenuKeys }
     *     
     */
    public void setVoicePortalMainMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoicePortalMainMenuKeys value) {
        this.voicePortalMainMenuKeys = value;
    }

    /**
     * Ruft den Wert der announcementMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .AnnouncementMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .AnnouncementMenuKeys getAnnouncementMenuKeys() {
        return announcementMenuKeys;
    }

    /**
     * Legt den Wert der announcementMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .AnnouncementMenuKeys }
     *     
     */
    public void setAnnouncementMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .AnnouncementMenuKeys value) {
        this.announcementMenuKeys = value;
    }

    /**
     * Ruft den Wert der announcementRecordingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .AnnouncementRecordingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .AnnouncementRecordingMenuKeys getAnnouncementRecordingMenuKeys() {
        return announcementRecordingMenuKeys;
    }

    /**
     * Legt den Wert der announcementRecordingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .AnnouncementRecordingMenuKeys }
     *     
     */
    public void setAnnouncementRecordingMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .AnnouncementRecordingMenuKeys value) {
        this.announcementRecordingMenuKeys = value;
    }

    /**
     * Ruft den Wert der greetingsMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .GreetingsMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .GreetingsMenuKeys getGreetingsMenuKeys() {
        return greetingsMenuKeys;
    }

    /**
     * Legt den Wert der greetingsMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .GreetingsMenuKeys }
     *     
     */
    public void setGreetingsMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .GreetingsMenuKeys value) {
        this.greetingsMenuKeys = value;
    }

    /**
     * Ruft den Wert der conferenceGreetingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ConferenceGreetingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ConferenceGreetingMenuKeys getConferenceGreetingMenuKeys() {
        return conferenceGreetingMenuKeys;
    }

    /**
     * Legt den Wert der conferenceGreetingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ConferenceGreetingMenuKeys }
     *     
     */
    public void setConferenceGreetingMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ConferenceGreetingMenuKeys value) {
        this.conferenceGreetingMenuKeys = value;
    }

    /**
     * Ruft den Wert der voiceMessagingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoiceMessagingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoiceMessagingMenuKeys getVoiceMessagingMenuKeys() {
        return voiceMessagingMenuKeys;
    }

    /**
     * Legt den Wert der voiceMessagingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoiceMessagingMenuKeys }
     *     
     */
    public void setVoiceMessagingMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoiceMessagingMenuKeys value) {
        this.voiceMessagingMenuKeys = value;
    }

    /**
     * Ruft den Wert der playGreetingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PlayGreetingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PlayGreetingMenuKeys getPlayGreetingMenuKeys() {
        return playGreetingMenuKeys;
    }

    /**
     * Legt den Wert der playGreetingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PlayGreetingMenuKeys }
     *     
     */
    public void setPlayGreetingMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PlayGreetingMenuKeys value) {
        this.playGreetingMenuKeys = value;
    }

    /**
     * Ruft den Wert der changeBusyOrNoAnswerGreetingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeBusyOrNoAnswerGreetingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeBusyOrNoAnswerGreetingMenuKeys getChangeBusyOrNoAnswerGreetingMenuKeys() {
        return changeBusyOrNoAnswerGreetingMenuKeys;
    }

    /**
     * Legt den Wert der changeBusyOrNoAnswerGreetingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeBusyOrNoAnswerGreetingMenuKeys }
     *     
     */
    public void setChangeBusyOrNoAnswerGreetingMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeBusyOrNoAnswerGreetingMenuKeys value) {
        this.changeBusyOrNoAnswerGreetingMenuKeys = value;
    }

    /**
     * Ruft den Wert der changeExtendedAwayGreetingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeExtendedAwayGreetingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeExtendedAwayGreetingMenuKeys getChangeExtendedAwayGreetingMenuKeys() {
        return changeExtendedAwayGreetingMenuKeys;
    }

    /**
     * Legt den Wert der changeExtendedAwayGreetingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeExtendedAwayGreetingMenuKeys }
     *     
     */
    public void setChangeExtendedAwayGreetingMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeExtendedAwayGreetingMenuKeys value) {
        this.changeExtendedAwayGreetingMenuKeys = value;
    }

    /**
     * Ruft den Wert der recordNewGreetingOrPersonalizedNameMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .RecordNewGreetingOrPersonalizedNameMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .RecordNewGreetingOrPersonalizedNameMenuKeys getRecordNewGreetingOrPersonalizedNameMenuKeys() {
        return recordNewGreetingOrPersonalizedNameMenuKeys;
    }

    /**
     * Legt den Wert der recordNewGreetingOrPersonalizedNameMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .RecordNewGreetingOrPersonalizedNameMenuKeys }
     *     
     */
    public void setRecordNewGreetingOrPersonalizedNameMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .RecordNewGreetingOrPersonalizedNameMenuKeys value) {
        this.recordNewGreetingOrPersonalizedNameMenuKeys = value;
    }

    /**
     * Ruft den Wert der deleteAllMessagesMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .DeleteAllMessagesMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .DeleteAllMessagesMenuKeys getDeleteAllMessagesMenuKeys() {
        return deleteAllMessagesMenuKeys;
    }

    /**
     * Legt den Wert der deleteAllMessagesMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .DeleteAllMessagesMenuKeys }
     *     
     */
    public void setDeleteAllMessagesMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .DeleteAllMessagesMenuKeys value) {
        this.deleteAllMessagesMenuKeys = value;
    }

    /**
     * Ruft den Wert der commPilotExpressProfileMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .CommPilotExpressProfileMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .CommPilotExpressProfileMenuKeys getCommPilotExpressProfileMenuKeys() {
        return commPilotExpressProfileMenuKeys;
    }

    /**
     * Legt den Wert der commPilotExpressProfileMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .CommPilotExpressProfileMenuKeys }
     *     
     */
    public void setCommPilotExpressProfileMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .CommPilotExpressProfileMenuKeys value) {
        this.commPilotExpressProfileMenuKeys = value;
    }

    /**
     * Ruft den Wert der personalizedNameMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PersonalizedNameMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PersonalizedNameMenuKeys getPersonalizedNameMenuKeys() {
        return personalizedNameMenuKeys;
    }

    /**
     * Legt den Wert der personalizedNameMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PersonalizedNameMenuKeys }
     *     
     */
    public void setPersonalizedNameMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PersonalizedNameMenuKeys value) {
        this.personalizedNameMenuKeys = value;
    }

    /**
     * Ruft den Wert der callForwardingOptionsMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .CallForwardingOptionsMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .CallForwardingOptionsMenuKeys getCallForwardingOptionsMenuKeys() {
        return callForwardingOptionsMenuKeys;
    }

    /**
     * Legt den Wert der callForwardingOptionsMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .CallForwardingOptionsMenuKeys }
     *     
     */
    public void setCallForwardingOptionsMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .CallForwardingOptionsMenuKeys value) {
        this.callForwardingOptionsMenuKeys = value;
    }

    /**
     * Ruft den Wert der changeCallForwardingDestinationMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeCallForwardingDestinationMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeCallForwardingDestinationMenuKeys getChangeCallForwardingDestinationMenuKeys() {
        return changeCallForwardingDestinationMenuKeys;
    }

    /**
     * Legt den Wert der changeCallForwardingDestinationMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeCallForwardingDestinationMenuKeys }
     *     
     */
    public void setChangeCallForwardingDestinationMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeCallForwardingDestinationMenuKeys value) {
        this.changeCallForwardingDestinationMenuKeys = value;
    }

    /**
     * Ruft den Wert der voicePortalCallingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoicePortalCallingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoicePortalCallingMenuKeys getVoicePortalCallingMenuKeys() {
        return voicePortalCallingMenuKeys;
    }

    /**
     * Legt den Wert der voicePortalCallingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoicePortalCallingMenuKeys }
     *     
     */
    public void setVoicePortalCallingMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoicePortalCallingMenuKeys value) {
        this.voicePortalCallingMenuKeys = value;
    }

    /**
     * Ruft den Wert der hotelingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .HotelingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .HotelingMenuKeys getHotelingMenuKeys() {
        return hotelingMenuKeys;
    }

    /**
     * Legt den Wert der hotelingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .HotelingMenuKeys }
     *     
     */
    public void setHotelingMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .HotelingMenuKeys value) {
        this.hotelingMenuKeys = value;
    }

    /**
     * Ruft den Wert der passcodeMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PasscodeMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PasscodeMenuKeys getPasscodeMenuKeys() {
        return passcodeMenuKeys;
    }

    /**
     * Legt den Wert der passcodeMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PasscodeMenuKeys }
     *     
     */
    public void setPasscodeMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PasscodeMenuKeys value) {
        this.passcodeMenuKeys = value;
    }

    /**
     * Ruft den Wert der playMessagesMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PlayMessagesMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PlayMessagesMenuKeys getPlayMessagesMenuKeys() {
        return playMessagesMenuKeys;
    }

    /**
     * Legt den Wert der playMessagesMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PlayMessagesMenuKeys }
     *     
     */
    public void setPlayMessagesMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PlayMessagesMenuKeys value) {
        this.playMessagesMenuKeys = value;
    }

    /**
     * Ruft den Wert der playMessageMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PlayMessageMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PlayMessageMenuKeys getPlayMessageMenuKeys() {
        return playMessageMenuKeys;
    }

    /**
     * Legt den Wert der playMessageMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PlayMessageMenuKeys }
     *     
     */
    public void setPlayMessageMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PlayMessageMenuKeys value) {
        this.playMessageMenuKeys = value;
    }

    /**
     * Ruft den Wert der additionalMessageOptionsMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .AdditionalMessageOptionsMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .AdditionalMessageOptionsMenuKeys getAdditionalMessageOptionsMenuKeys() {
        return additionalMessageOptionsMenuKeys;
    }

    /**
     * Legt den Wert der additionalMessageOptionsMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .AdditionalMessageOptionsMenuKeys }
     *     
     */
    public void setAdditionalMessageOptionsMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .AdditionalMessageOptionsMenuKeys value) {
        this.additionalMessageOptionsMenuKeys = value;
    }

    /**
     * Ruft den Wert der forwardOrComposeMessageMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ForwardOrComposeMessageMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ForwardOrComposeMessageMenuKeys getForwardOrComposeMessageMenuKeys() {
        return forwardOrComposeMessageMenuKeys;
    }

    /**
     * Legt den Wert der forwardOrComposeMessageMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ForwardOrComposeMessageMenuKeys }
     *     
     */
    public void setForwardOrComposeMessageMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ForwardOrComposeMessageMenuKeys value) {
        this.forwardOrComposeMessageMenuKeys = value;
    }

    /**
     * Ruft den Wert der replyMessageMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ReplyMessageMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ReplyMessageMenuKeys getReplyMessageMenuKeys() {
        return replyMessageMenuKeys;
    }

    /**
     * Legt den Wert der replyMessageMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ReplyMessageMenuKeys }
     *     
     */
    public void setReplyMessageMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ReplyMessageMenuKeys value) {
        this.replyMessageMenuKeys = value;
    }

    /**
     * Ruft den Wert der sendToDistributionListMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendToDistributionListMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendToDistributionListMenuKeys getSendToDistributionListMenuKeys() {
        return sendToDistributionListMenuKeys;
    }

    /**
     * Legt den Wert der sendToDistributionListMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendToDistributionListMenuKeys }
     *     
     */
    public void setSendToDistributionListMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendToDistributionListMenuKeys value) {
        this.sendToDistributionListMenuKeys = value;
    }

    /**
     * Ruft den Wert der selectDistributionListMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SelectDistributionListMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SelectDistributionListMenuKeys getSelectDistributionListMenuKeys() {
        return selectDistributionListMenuKeys;
    }

    /**
     * Legt den Wert der selectDistributionListMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SelectDistributionListMenuKeys }
     *     
     */
    public void setSelectDistributionListMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SelectDistributionListMenuKeys value) {
        this.selectDistributionListMenuKeys = value;
    }

    /**
     * Ruft den Wert der reviewSelectedDistributionListMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ReviewSelectedDistributionListMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ReviewSelectedDistributionListMenuKeys getReviewSelectedDistributionListMenuKeys() {
        return reviewSelectedDistributionListMenuKeys;
    }

    /**
     * Legt den Wert der reviewSelectedDistributionListMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ReviewSelectedDistributionListMenuKeys }
     *     
     */
    public void setReviewSelectedDistributionListMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ReviewSelectedDistributionListMenuKeys value) {
        this.reviewSelectedDistributionListMenuKeys = value;
    }

    /**
     * Ruft den Wert der sendMessageToSelectedDistributionListMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendMessageToSelectedDistributionListMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendMessageToSelectedDistributionListMenuKeys getSendMessageToSelectedDistributionListMenuKeys() {
        return sendMessageToSelectedDistributionListMenuKeys;
    }

    /**
     * Legt den Wert der sendMessageToSelectedDistributionListMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendMessageToSelectedDistributionListMenuKeys }
     *     
     */
    public void setSendMessageToSelectedDistributionListMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendMessageToSelectedDistributionListMenuKeys value) {
        this.sendMessageToSelectedDistributionListMenuKeys = value;
    }

    /**
     * Ruft den Wert der sendToAllGroupMembersMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendToAllGroupMembersMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendToAllGroupMembersMenuKeys getSendToAllGroupMembersMenuKeys() {
        return sendToAllGroupMembersMenuKeys;
    }

    /**
     * Legt den Wert der sendToAllGroupMembersMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendToAllGroupMembersMenuKeys }
     *     
     */
    public void setSendToAllGroupMembersMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendToAllGroupMembersMenuKeys value) {
        this.sendToAllGroupMembersMenuKeys = value;
    }

    /**
     * Ruft den Wert der sendToPersonMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendToPersonMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendToPersonMenuKeys getSendToPersonMenuKeys() {
        return sendToPersonMenuKeys;
    }

    /**
     * Legt den Wert der sendToPersonMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendToPersonMenuKeys }
     *     
     */
    public void setSendToPersonMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .SendToPersonMenuKeys value) {
        this.sendToPersonMenuKeys = value;
    }

    /**
     * Ruft den Wert der changeCurrentIntroductionOrMessageOrReplyMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeCurrentIntroductionOrMessageOrReplyMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeCurrentIntroductionOrMessageOrReplyMenuKeys getChangeCurrentIntroductionOrMessageOrReplyMenuKeys() {
        return changeCurrentIntroductionOrMessageOrReplyMenuKeys;
    }

    /**
     * Legt den Wert der changeCurrentIntroductionOrMessageOrReplyMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeCurrentIntroductionOrMessageOrReplyMenuKeys }
     *     
     */
    public void setChangeCurrentIntroductionOrMessageOrReplyMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .ChangeCurrentIntroductionOrMessageOrReplyMenuKeys value) {
        this.changeCurrentIntroductionOrMessageOrReplyMenuKeys = value;
    }

    /**
     * Ruft den Wert der voicePortalLoginMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoicePortalLoginMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoicePortalLoginMenuKeys getVoicePortalLoginMenuKeys() {
        return voicePortalLoginMenuKeys;
    }

    /**
     * Legt den Wert der voicePortalLoginMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoicePortalLoginMenuKeys }
     *     
     */
    public void setVoicePortalLoginMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .VoicePortalLoginMenuKeys value) {
        this.voicePortalLoginMenuKeys = value;
    }

    /**
     * Ruft den Wert der faxMessagingMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .FaxMessagingMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .FaxMessagingMenuKeys getFaxMessagingMenuKeys() {
        return faxMessagingMenuKeys;
    }

    /**
     * Legt den Wert der faxMessagingMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .FaxMessagingMenuKeys }
     *     
     */
    public void setFaxMessagingMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .FaxMessagingMenuKeys value) {
        this.faxMessagingMenuKeys = value;
    }

    /**
     * Ruft den Wert der messageDepositMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .MessageDepositMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .MessageDepositMenuKeys getMessageDepositMenuKeys() {
        return messageDepositMenuKeys;
    }

    /**
     * Legt den Wert der messageDepositMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .MessageDepositMenuKeys }
     *     
     */
    public void setMessageDepositMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .MessageDepositMenuKeys value) {
        this.messageDepositMenuKeys = value;
    }

    /**
     * Ruft den Wert der disableMessageDepositMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .DisableMessageDepositMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .DisableMessageDepositMenuKeys getDisableMessageDepositMenuKeys() {
        return disableMessageDepositMenuKeys;
    }

    /**
     * Legt den Wert der disableMessageDepositMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .DisableMessageDepositMenuKeys }
     *     
     */
    public void setDisableMessageDepositMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .DisableMessageDepositMenuKeys value) {
        this.disableMessageDepositMenuKeys = value;
    }

    /**
     * Ruft den Wert der greetingOnlyForwardingDestinationMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .GreetingOnlyForwardingDestinationMenuKeys }{@code >}
     *     
     */
    public JAXBElement<SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .GreetingOnlyForwardingDestinationMenuKeys> getGreetingOnlyForwardingDestinationMenuKeys() {
        return greetingOnlyForwardingDestinationMenuKeys;
    }

    /**
     * Legt den Wert der greetingOnlyForwardingDestinationMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .GreetingOnlyForwardingDestinationMenuKeys }{@code >}
     *     
     */
    public void setGreetingOnlyForwardingDestinationMenuKeys(JAXBElement<SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .GreetingOnlyForwardingDestinationMenuKeys> value) {
        this.greetingOnlyForwardingDestinationMenuKeys = value;
    }

    /**
     * Ruft den Wert der personalAssistantMenuKeys-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PersonalAssistantMenuKeys }
     *     
     */
    public SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PersonalAssistantMenuKeys getPersonalAssistantMenuKeys() {
        return personalAssistantMenuKeys;
    }

    /**
     * Legt den Wert der personalAssistantMenuKeys-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PersonalAssistantMenuKeys }
     *     
     */
    public void setPersonalAssistantMenuKeys(SystemVoiceMessagingGroupModifyVoicePortalMenusRequest19 .PersonalAssistantMenuKeys value) {
        this.personalAssistantMenuKeys = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="saveMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deleteMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
     *         <element name="callbackCaller" type="{}DigitAny" minOccurs="0"/>
     *         <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="replyMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="forwardMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
     *         <element name="passcode" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "saveMessage",
        "deleteMessage",
        "playEnvelope",
        "callbackCaller",
        "composeMessage",
        "replyMessage",
        "forwardMessage",
        "personalizedName",
        "passcode",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class AdditionalMessageOptionsMenuKeys {

        @XmlElementRef(name = "saveMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> saveMessage;
        @XmlElementRef(name = "deleteMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> deleteMessage;
        @XmlElementRef(name = "playEnvelope", type = JAXBElement.class, required = false)
        protected JAXBElement<String> playEnvelope;
        @XmlElementRef(name = "callbackCaller", type = JAXBElement.class, required = false)
        protected JAXBElement<String> callbackCaller;
        @XmlElementRef(name = "composeMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> composeMessage;
        @XmlElementRef(name = "replyMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> replyMessage;
        @XmlElementRef(name = "forwardMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> forwardMessage;
        @XmlElementRef(name = "personalizedName", type = JAXBElement.class, required = false)
        protected JAXBElement<String> personalizedName;
        @XmlElementRef(name = "passcode", type = JAXBElement.class, required = false)
        protected JAXBElement<String> passcode;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der saveMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSaveMessage() {
            return saveMessage;
        }

        /**
         * Legt den Wert der saveMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSaveMessage(JAXBElement<String> value) {
            this.saveMessage = value;
        }

        /**
         * Ruft den Wert der deleteMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getDeleteMessage() {
            return deleteMessage;
        }

        /**
         * Legt den Wert der deleteMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setDeleteMessage(JAXBElement<String> value) {
            this.deleteMessage = value;
        }

        /**
         * Ruft den Wert der playEnvelope-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPlayEnvelope() {
            return playEnvelope;
        }

        /**
         * Legt den Wert der playEnvelope-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPlayEnvelope(JAXBElement<String> value) {
            this.playEnvelope = value;
        }

        /**
         * Ruft den Wert der callbackCaller-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getCallbackCaller() {
            return callbackCaller;
        }

        /**
         * Legt den Wert der callbackCaller-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setCallbackCaller(JAXBElement<String> value) {
            this.callbackCaller = value;
        }

        /**
         * Ruft den Wert der composeMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getComposeMessage() {
            return composeMessage;
        }

        /**
         * Legt den Wert der composeMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setComposeMessage(JAXBElement<String> value) {
            this.composeMessage = value;
        }

        /**
         * Ruft den Wert der replyMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getReplyMessage() {
            return replyMessage;
        }

        /**
         * Legt den Wert der replyMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setReplyMessage(JAXBElement<String> value) {
            this.replyMessage = value;
        }

        /**
         * Ruft den Wert der forwardMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getForwardMessage() {
            return forwardMessage;
        }

        /**
         * Legt den Wert der forwardMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setForwardMessage(JAXBElement<String> value) {
            this.forwardMessage = value;
        }

        /**
         * Ruft den Wert der personalizedName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPersonalizedName() {
            return personalizedName;
        }

        /**
         * Legt den Wert der personalizedName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPersonalizedName(JAXBElement<String> value) {
            this.personalizedName = value;
        }

        /**
         * Ruft den Wert der passcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPasscode() {
            return passcode;
        }

        /**
         * Legt den Wert der passcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPasscode(JAXBElement<String> value) {
            this.passcode = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="recordAudio" type="{}DigitAny" minOccurs="0"/>
     *         <element name="recordAudioVideo" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "recordAudio",
        "recordAudioVideo",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class AnnouncementMenuKeys {

        @XmlElementRef(name = "recordAudio", type = JAXBElement.class, required = false)
        protected JAXBElement<String> recordAudio;
        @XmlElementRef(name = "recordAudioVideo", type = JAXBElement.class, required = false)
        protected JAXBElement<String> recordAudioVideo;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der recordAudio-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRecordAudio() {
            return recordAudio;
        }

        /**
         * Legt den Wert der recordAudio-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRecordAudio(JAXBElement<String> value) {
            this.recordAudio = value;
        }

        /**
         * Ruft den Wert der recordAudioVideo-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRecordAudioVideo() {
            return recordAudioVideo;
        }

        /**
         * Legt den Wert der recordAudioVideo-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRecordAudioVideo(JAXBElement<String> value) {
            this.recordAudioVideo = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="acceptRecording" type="{}DigitAny" minOccurs="0"/>
     *         <element name="rejectRerecord" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="end" type="{}VoicePortalDigitSequence" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acceptRecording",
        "rejectRerecord",
        "returnToPreviousMenu",
        "repeatMenu",
        "end"
    })
    public static class AnnouncementRecordingMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String acceptRecording;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String rejectRerecord;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String end;

        /**
         * Ruft den Wert der acceptRecording-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcceptRecording() {
            return acceptRecording;
        }

        /**
         * Legt den Wert der acceptRecording-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcceptRecording(String value) {
            this.acceptRecording = value;
        }

        /**
         * Ruft den Wert der rejectRerecord-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRejectRerecord() {
            return rejectRerecord;
        }

        /**
         * Legt den Wert der rejectRerecord-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRejectRerecord(String value) {
            this.rejectRerecord = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

        /**
         * Ruft den Wert der end-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEnd() {
            return end;
        }

        /**
         * Legt den Wert der end-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEnd(String value) {
            this.end = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="activateCallForwarding" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deactivateCallForwarding" type="{}DigitAny" minOccurs="0"/>
     *         <element name="changeCallForwardingDestination" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToCallForwardingStatus" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "activateCallForwarding",
        "deactivateCallForwarding",
        "changeCallForwardingDestination",
        "listenToCallForwardingStatus",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class CallForwardingOptionsMenuKeys {

        @XmlElementRef(name = "activateCallForwarding", type = JAXBElement.class, required = false)
        protected JAXBElement<String> activateCallForwarding;
        @XmlElementRef(name = "deactivateCallForwarding", type = JAXBElement.class, required = false)
        protected JAXBElement<String> deactivateCallForwarding;
        @XmlElementRef(name = "changeCallForwardingDestination", type = JAXBElement.class, required = false)
        protected JAXBElement<String> changeCallForwardingDestination;
        @XmlElementRef(name = "listenToCallForwardingStatus", type = JAXBElement.class, required = false)
        protected JAXBElement<String> listenToCallForwardingStatus;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der activateCallForwarding-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getActivateCallForwarding() {
            return activateCallForwarding;
        }

        /**
         * Legt den Wert der activateCallForwarding-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setActivateCallForwarding(JAXBElement<String> value) {
            this.activateCallForwarding = value;
        }

        /**
         * Ruft den Wert der deactivateCallForwarding-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getDeactivateCallForwarding() {
            return deactivateCallForwarding;
        }

        /**
         * Legt den Wert der deactivateCallForwarding-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setDeactivateCallForwarding(JAXBElement<String> value) {
            this.deactivateCallForwarding = value;
        }

        /**
         * Ruft den Wert der changeCallForwardingDestination-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getChangeCallForwardingDestination() {
            return changeCallForwardingDestination;
        }

        /**
         * Legt den Wert der changeCallForwardingDestination-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setChangeCallForwardingDestination(JAXBElement<String> value) {
            this.changeCallForwardingDestination = value;
        }

        /**
         * Ruft den Wert der listenToCallForwardingStatus-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getListenToCallForwardingStatus() {
            return listenToCallForwardingStatus;
        }

        /**
         * Legt den Wert der listenToCallForwardingStatus-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setListenToCallForwardingStatus(JAXBElement<String> value) {
            this.listenToCallForwardingStatus = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="recordNewGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToCurrentGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="revertToSystemDefaultGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "recordNewGreeting",
        "listenToCurrentGreeting",
        "revertToSystemDefaultGreeting",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class ChangeBusyOrNoAnswerGreetingMenuKeys {

        @XmlElementRef(name = "recordNewGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> recordNewGreeting;
        @XmlElementRef(name = "listenToCurrentGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> listenToCurrentGreeting;
        @XmlElementRef(name = "revertToSystemDefaultGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> revertToSystemDefaultGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der recordNewGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRecordNewGreeting() {
            return recordNewGreeting;
        }

        /**
         * Legt den Wert der recordNewGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRecordNewGreeting(JAXBElement<String> value) {
            this.recordNewGreeting = value;
        }

        /**
         * Ruft den Wert der listenToCurrentGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getListenToCurrentGreeting() {
            return listenToCurrentGreeting;
        }

        /**
         * Legt den Wert der listenToCurrentGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setListenToCurrentGreeting(JAXBElement<String> value) {
            this.listenToCurrentGreeting = value;
        }

        /**
         * Ruft den Wert der revertToSystemDefaultGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRevertToSystemDefaultGreeting() {
            return revertToSystemDefaultGreeting;
        }

        /**
         * Legt den Wert der revertToSystemDefaultGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRevertToSystemDefaultGreeting(JAXBElement<String> value) {
            this.revertToSystemDefaultGreeting = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="finishEnteringNewDestinationNumber" type="{}DigitStarPound" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "finishEnteringNewDestinationNumber"
    })
    public static class ChangeCallForwardingDestinationMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String finishEnteringNewDestinationNumber;

        /**
         * Ruft den Wert der finishEnteringNewDestinationNumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFinishEnteringNewDestinationNumber() {
            return finishEnteringNewDestinationNumber;
        }

        /**
         * Legt den Wert der finishEnteringNewDestinationNumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFinishEnteringNewDestinationNumber(String value) {
            this.finishEnteringNewDestinationNumber = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="endRecording" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "endRecording"
    })
    public static class ChangeCurrentIntroductionOrMessageOrReplyMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String endRecording;

        /**
         * Ruft den Wert der endRecording-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndRecording() {
            return endRecording;
        }

        /**
         * Legt den Wert der endRecording-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndRecording(String value) {
            this.endRecording = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="activateExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deactivateExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="recordNewGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToCurrentGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="enableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
     *         <element name="disableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "activateExtendedAwayGreeting",
        "deactivateExtendedAwayGreeting",
        "recordNewGreeting",
        "listenToCurrentGreeting",
        "enableMessageDeposit",
        "disableMessageDeposit",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class ChangeExtendedAwayGreetingMenuKeys {

        @XmlElementRef(name = "activateExtendedAwayGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> activateExtendedAwayGreeting;
        @XmlElementRef(name = "deactivateExtendedAwayGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> deactivateExtendedAwayGreeting;
        @XmlElementRef(name = "recordNewGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> recordNewGreeting;
        @XmlElementRef(name = "listenToCurrentGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> listenToCurrentGreeting;
        @XmlElementRef(name = "enableMessageDeposit", type = JAXBElement.class, required = false)
        protected JAXBElement<String> enableMessageDeposit;
        @XmlElementRef(name = "disableMessageDeposit", type = JAXBElement.class, required = false)
        protected JAXBElement<String> disableMessageDeposit;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der activateExtendedAwayGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getActivateExtendedAwayGreeting() {
            return activateExtendedAwayGreeting;
        }

        /**
         * Legt den Wert der activateExtendedAwayGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setActivateExtendedAwayGreeting(JAXBElement<String> value) {
            this.activateExtendedAwayGreeting = value;
        }

        /**
         * Ruft den Wert der deactivateExtendedAwayGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getDeactivateExtendedAwayGreeting() {
            return deactivateExtendedAwayGreeting;
        }

        /**
         * Legt den Wert der deactivateExtendedAwayGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setDeactivateExtendedAwayGreeting(JAXBElement<String> value) {
            this.deactivateExtendedAwayGreeting = value;
        }

        /**
         * Ruft den Wert der recordNewGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRecordNewGreeting() {
            return recordNewGreeting;
        }

        /**
         * Legt den Wert der recordNewGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRecordNewGreeting(JAXBElement<String> value) {
            this.recordNewGreeting = value;
        }

        /**
         * Ruft den Wert der listenToCurrentGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getListenToCurrentGreeting() {
            return listenToCurrentGreeting;
        }

        /**
         * Legt den Wert der listenToCurrentGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setListenToCurrentGreeting(JAXBElement<String> value) {
            this.listenToCurrentGreeting = value;
        }

        /**
         * Ruft den Wert der enableMessageDeposit-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getEnableMessageDeposit() {
            return enableMessageDeposit;
        }

        /**
         * Legt den Wert der enableMessageDeposit-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setEnableMessageDeposit(JAXBElement<String> value) {
            this.enableMessageDeposit = value;
        }

        /**
         * Ruft den Wert der disableMessageDeposit-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getDisableMessageDeposit() {
            return disableMessageDeposit;
        }

        /**
         * Legt den Wert der disableMessageDeposit-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setDisableMessageDeposit(JAXBElement<String> value) {
            this.disableMessageDeposit = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="activateAvailableInOfficeProfile" type="{}DigitAny" minOccurs="0"/>
     *         <element name="activateAvailableOutOfOfficeProfile" type="{}DigitAny" minOccurs="0"/>
     *         <element name="activateBusyProfile" type="{}DigitAny" minOccurs="0"/>
     *         <element name="activateUnavailableProfile" type="{}DigitAny" minOccurs="0"/>
     *         <element name="noProfile" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "activateAvailableInOfficeProfile",
        "activateAvailableOutOfOfficeProfile",
        "activateBusyProfile",
        "activateUnavailableProfile",
        "noProfile",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class CommPilotExpressProfileMenuKeys {

        @XmlElementRef(name = "activateAvailableInOfficeProfile", type = JAXBElement.class, required = false)
        protected JAXBElement<String> activateAvailableInOfficeProfile;
        @XmlElementRef(name = "activateAvailableOutOfOfficeProfile", type = JAXBElement.class, required = false)
        protected JAXBElement<String> activateAvailableOutOfOfficeProfile;
        @XmlElementRef(name = "activateBusyProfile", type = JAXBElement.class, required = false)
        protected JAXBElement<String> activateBusyProfile;
        @XmlElementRef(name = "activateUnavailableProfile", type = JAXBElement.class, required = false)
        protected JAXBElement<String> activateUnavailableProfile;
        @XmlElementRef(name = "noProfile", type = JAXBElement.class, required = false)
        protected JAXBElement<String> noProfile;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der activateAvailableInOfficeProfile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getActivateAvailableInOfficeProfile() {
            return activateAvailableInOfficeProfile;
        }

        /**
         * Legt den Wert der activateAvailableInOfficeProfile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setActivateAvailableInOfficeProfile(JAXBElement<String> value) {
            this.activateAvailableInOfficeProfile = value;
        }

        /**
         * Ruft den Wert der activateAvailableOutOfOfficeProfile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getActivateAvailableOutOfOfficeProfile() {
            return activateAvailableOutOfOfficeProfile;
        }

        /**
         * Legt den Wert der activateAvailableOutOfOfficeProfile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setActivateAvailableOutOfOfficeProfile(JAXBElement<String> value) {
            this.activateAvailableOutOfOfficeProfile = value;
        }

        /**
         * Ruft den Wert der activateBusyProfile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getActivateBusyProfile() {
            return activateBusyProfile;
        }

        /**
         * Legt den Wert der activateBusyProfile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setActivateBusyProfile(JAXBElement<String> value) {
            this.activateBusyProfile = value;
        }

        /**
         * Ruft den Wert der activateUnavailableProfile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getActivateUnavailableProfile() {
            return activateUnavailableProfile;
        }

        /**
         * Legt den Wert der activateUnavailableProfile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setActivateUnavailableProfile(JAXBElement<String> value) {
            this.activateUnavailableProfile = value;
        }

        /**
         * Ruft den Wert der noProfile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getNoProfile() {
            return noProfile;
        }

        /**
         * Legt den Wert der noProfile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setNoProfile(JAXBElement<String> value) {
            this.noProfile = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="activateConfGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deactivateConfGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="recordNewConfGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToCurrentConfGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "activateConfGreeting",
        "deactivateConfGreeting",
        "recordNewConfGreeting",
        "listenToCurrentConfGreeting",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class ConferenceGreetingMenuKeys {

        @XmlElementRef(name = "activateConfGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> activateConfGreeting;
        @XmlElementRef(name = "deactivateConfGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> deactivateConfGreeting;
        @XmlElementRef(name = "recordNewConfGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> recordNewConfGreeting;
        @XmlElementRef(name = "listenToCurrentConfGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> listenToCurrentConfGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der activateConfGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getActivateConfGreeting() {
            return activateConfGreeting;
        }

        /**
         * Legt den Wert der activateConfGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setActivateConfGreeting(JAXBElement<String> value) {
            this.activateConfGreeting = value;
        }

        /**
         * Ruft den Wert der deactivateConfGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getDeactivateConfGreeting() {
            return deactivateConfGreeting;
        }

        /**
         * Legt den Wert der deactivateConfGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setDeactivateConfGreeting(JAXBElement<String> value) {
            this.deactivateConfGreeting = value;
        }

        /**
         * Ruft den Wert der recordNewConfGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRecordNewConfGreeting() {
            return recordNewConfGreeting;
        }

        /**
         * Legt den Wert der recordNewConfGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRecordNewConfGreeting(JAXBElement<String> value) {
            this.recordNewConfGreeting = value;
        }

        /**
         * Ruft den Wert der listenToCurrentConfGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getListenToCurrentConfGreeting() {
            return listenToCurrentConfGreeting;
        }

        /**
         * Legt den Wert der listenToCurrentConfGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setListenToCurrentConfGreeting(JAXBElement<String> value) {
            this.listenToCurrentConfGreeting = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="confirmDeletion" type="{}DigitAny" minOccurs="0"/>
     *         <element name="cancelDeletion" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "confirmDeletion",
        "cancelDeletion"
    })
    public static class DeleteAllMessagesMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String confirmDeletion;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String cancelDeletion;

        /**
         * Ruft den Wert der confirmDeletion-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConfirmDeletion() {
            return confirmDeletion;
        }

        /**
         * Legt den Wert der confirmDeletion-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConfirmDeletion(String value) {
            this.confirmDeletion = value;
        }

        /**
         * Ruft den Wert der cancelDeletion-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCancelDeletion() {
            return cancelDeletion;
        }

        /**
         * Legt den Wert der cancelDeletion-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCancelDeletion(String value) {
            this.cancelDeletion = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="disconnectAfterGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="forwardAfterGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="changeForwardingDestination" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "disconnectAfterGreeting",
        "forwardAfterGreeting",
        "changeForwardingDestination",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class DisableMessageDepositMenuKeys {

        @XmlElementRef(name = "disconnectAfterGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> disconnectAfterGreeting;
        @XmlElementRef(name = "forwardAfterGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> forwardAfterGreeting;
        @XmlElementRef(name = "changeForwardingDestination", type = JAXBElement.class, required = false)
        protected JAXBElement<String> changeForwardingDestination;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der disconnectAfterGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getDisconnectAfterGreeting() {
            return disconnectAfterGreeting;
        }

        /**
         * Legt den Wert der disconnectAfterGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setDisconnectAfterGreeting(JAXBElement<String> value) {
            this.disconnectAfterGreeting = value;
        }

        /**
         * Ruft den Wert der forwardAfterGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getForwardAfterGreeting() {
            return forwardAfterGreeting;
        }

        /**
         * Legt den Wert der forwardAfterGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setForwardAfterGreeting(JAXBElement<String> value) {
            this.forwardAfterGreeting = value;
        }

        /**
         * Ruft den Wert der changeForwardingDestination-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getChangeForwardingDestination() {
            return changeForwardingDestination;
        }

        /**
         * Legt den Wert der changeForwardingDestination-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setChangeForwardingDestination(JAXBElement<String> value) {
            this.changeForwardingDestination = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="saveFaxMessageAndSkipToNext" type="{}DigitAny" minOccurs="0"/>
     *         <element name="previousFaxMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
     *         <element name="nextFaxMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deleteFaxMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="printFaxMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "saveFaxMessageAndSkipToNext",
        "previousFaxMessage",
        "playEnvelope",
        "nextFaxMessage",
        "deleteFaxMessage",
        "printFaxMessage",
        "returnToPreviousMenu"
    })
    public static class FaxMessagingMenuKeys {

        @XmlElementRef(name = "saveFaxMessageAndSkipToNext", type = JAXBElement.class, required = false)
        protected JAXBElement<String> saveFaxMessageAndSkipToNext;
        @XmlElementRef(name = "previousFaxMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> previousFaxMessage;
        @XmlElementRef(name = "playEnvelope", type = JAXBElement.class, required = false)
        protected JAXBElement<String> playEnvelope;
        @XmlElementRef(name = "nextFaxMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> nextFaxMessage;
        @XmlElementRef(name = "deleteFaxMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> deleteFaxMessage;
        @XmlElementRef(name = "printFaxMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> printFaxMessage;
        @XmlElementRef(name = "returnToPreviousMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> returnToPreviousMenu;

        /**
         * Ruft den Wert der saveFaxMessageAndSkipToNext-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSaveFaxMessageAndSkipToNext() {
            return saveFaxMessageAndSkipToNext;
        }

        /**
         * Legt den Wert der saveFaxMessageAndSkipToNext-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSaveFaxMessageAndSkipToNext(JAXBElement<String> value) {
            this.saveFaxMessageAndSkipToNext = value;
        }

        /**
         * Ruft den Wert der previousFaxMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPreviousFaxMessage() {
            return previousFaxMessage;
        }

        /**
         * Legt den Wert der previousFaxMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPreviousFaxMessage(JAXBElement<String> value) {
            this.previousFaxMessage = value;
        }

        /**
         * Ruft den Wert der playEnvelope-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPlayEnvelope() {
            return playEnvelope;
        }

        /**
         * Legt den Wert der playEnvelope-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPlayEnvelope(JAXBElement<String> value) {
            this.playEnvelope = value;
        }

        /**
         * Ruft den Wert der nextFaxMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getNextFaxMessage() {
            return nextFaxMessage;
        }

        /**
         * Legt den Wert der nextFaxMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setNextFaxMessage(JAXBElement<String> value) {
            this.nextFaxMessage = value;
        }

        /**
         * Ruft den Wert der deleteFaxMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getDeleteFaxMessage() {
            return deleteFaxMessage;
        }

        /**
         * Legt den Wert der deleteFaxMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setDeleteFaxMessage(JAXBElement<String> value) {
            this.deleteFaxMessage = value;
        }

        /**
         * Ruft den Wert der printFaxMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPrintFaxMessage() {
            return printFaxMessage;
        }

        /**
         * Legt den Wert der printFaxMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPrintFaxMessage(JAXBElement<String> value) {
            this.printFaxMessage = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setReturnToPreviousMenu(JAXBElement<String> value) {
            this.returnToPreviousMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="sendToPerson" type="{}DigitAny" minOccurs="0"/>
     *         <element name="sendToAllGroupMembers" type="{}DigitAny" minOccurs="0"/>
     *         <element name="sendToDistributionList" type="{}DigitAny" minOccurs="0"/>
     *         <element name="changeCurrentIntroductionOrMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToCurrentIntroductionOrMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setOrClearUrgentIndicator" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setOrClearConfidentialIndicator" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sendToPerson",
        "sendToAllGroupMembers",
        "sendToDistributionList",
        "changeCurrentIntroductionOrMessage",
        "listenToCurrentIntroductionOrMessage",
        "setOrClearUrgentIndicator",
        "setOrClearConfidentialIndicator",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class ForwardOrComposeMessageMenuKeys {

        @XmlElementRef(name = "sendToPerson", type = JAXBElement.class, required = false)
        protected JAXBElement<String> sendToPerson;
        @XmlElementRef(name = "sendToAllGroupMembers", type = JAXBElement.class, required = false)
        protected JAXBElement<String> sendToAllGroupMembers;
        @XmlElementRef(name = "sendToDistributionList", type = JAXBElement.class, required = false)
        protected JAXBElement<String> sendToDistributionList;
        @XmlElementRef(name = "changeCurrentIntroductionOrMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> changeCurrentIntroductionOrMessage;
        @XmlElementRef(name = "listenToCurrentIntroductionOrMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> listenToCurrentIntroductionOrMessage;
        @XmlElementRef(name = "setOrClearUrgentIndicator", type = JAXBElement.class, required = false)
        protected JAXBElement<String> setOrClearUrgentIndicator;
        @XmlElementRef(name = "setOrClearConfidentialIndicator", type = JAXBElement.class, required = false)
        protected JAXBElement<String> setOrClearConfidentialIndicator;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der sendToPerson-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSendToPerson() {
            return sendToPerson;
        }

        /**
         * Legt den Wert der sendToPerson-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSendToPerson(JAXBElement<String> value) {
            this.sendToPerson = value;
        }

        /**
         * Ruft den Wert der sendToAllGroupMembers-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSendToAllGroupMembers() {
            return sendToAllGroupMembers;
        }

        /**
         * Legt den Wert der sendToAllGroupMembers-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSendToAllGroupMembers(JAXBElement<String> value) {
            this.sendToAllGroupMembers = value;
        }

        /**
         * Ruft den Wert der sendToDistributionList-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSendToDistributionList() {
            return sendToDistributionList;
        }

        /**
         * Legt den Wert der sendToDistributionList-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSendToDistributionList(JAXBElement<String> value) {
            this.sendToDistributionList = value;
        }

        /**
         * Ruft den Wert der changeCurrentIntroductionOrMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getChangeCurrentIntroductionOrMessage() {
            return changeCurrentIntroductionOrMessage;
        }

        /**
         * Legt den Wert der changeCurrentIntroductionOrMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setChangeCurrentIntroductionOrMessage(JAXBElement<String> value) {
            this.changeCurrentIntroductionOrMessage = value;
        }

        /**
         * Ruft den Wert der listenToCurrentIntroductionOrMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getListenToCurrentIntroductionOrMessage() {
            return listenToCurrentIntroductionOrMessage;
        }

        /**
         * Legt den Wert der listenToCurrentIntroductionOrMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setListenToCurrentIntroductionOrMessage(JAXBElement<String> value) {
            this.listenToCurrentIntroductionOrMessage = value;
        }

        /**
         * Ruft den Wert der setOrClearUrgentIndicator-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSetOrClearUrgentIndicator() {
            return setOrClearUrgentIndicator;
        }

        /**
         * Legt den Wert der setOrClearUrgentIndicator-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSetOrClearUrgentIndicator(JAXBElement<String> value) {
            this.setOrClearUrgentIndicator = value;
        }

        /**
         * Ruft den Wert der setOrClearConfidentialIndicator-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSetOrClearConfidentialIndicator() {
            return setOrClearConfidentialIndicator;
        }

        /**
         * Legt den Wert der setOrClearConfidentialIndicator-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSetOrClearConfidentialIndicator(JAXBElement<String> value) {
            this.setOrClearConfidentialIndicator = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="greetingOnlyForwardingDestination" type="{}DigitStarPound" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "greetingOnlyForwardingDestination"
    })
    public static class GreetingOnlyForwardingDestinationMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String greetingOnlyForwardingDestination;

        /**
         * Ruft den Wert der greetingOnlyForwardingDestination-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGreetingOnlyForwardingDestination() {
            return greetingOnlyForwardingDestination;
        }

        /**
         * Legt den Wert der greetingOnlyForwardingDestination-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGreetingOnlyForwardingDestination(String value) {
            this.greetingOnlyForwardingDestination = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
     *         <element name="conferenceGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "personalizedName",
        "conferenceGreeting",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class GreetingsMenuKeys {

        @XmlElementRef(name = "personalizedName", type = JAXBElement.class, required = false)
        protected JAXBElement<String> personalizedName;
        @XmlElementRef(name = "conferenceGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> conferenceGreeting;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der personalizedName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPersonalizedName() {
            return personalizedName;
        }

        /**
         * Legt den Wert der personalizedName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPersonalizedName(JAXBElement<String> value) {
            this.personalizedName = value;
        }

        /**
         * Ruft den Wert der conferenceGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getConferenceGreeting() {
            return conferenceGreeting;
        }

        /**
         * Legt den Wert der conferenceGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setConferenceGreeting(JAXBElement<String> value) {
            this.conferenceGreeting = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="checkHostStatus" type="{}DigitAny" minOccurs="0"/>
     *         <element name="associateWithHost" type="{}DigitAny" minOccurs="0"/>
     *         <element name="disassociateFromHost" type="{}DigitAny" minOccurs="0"/>
     *         <element name="disassociateFromRemoteHost" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "checkHostStatus",
        "associateWithHost",
        "disassociateFromHost",
        "disassociateFromRemoteHost",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class HotelingMenuKeys {

        @XmlElementRef(name = "checkHostStatus", type = JAXBElement.class, required = false)
        protected JAXBElement<String> checkHostStatus;
        @XmlElementRef(name = "associateWithHost", type = JAXBElement.class, required = false)
        protected JAXBElement<String> associateWithHost;
        @XmlElementRef(name = "disassociateFromHost", type = JAXBElement.class, required = false)
        protected JAXBElement<String> disassociateFromHost;
        @XmlElementRef(name = "disassociateFromRemoteHost", type = JAXBElement.class, required = false)
        protected JAXBElement<String> disassociateFromRemoteHost;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der checkHostStatus-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getCheckHostStatus() {
            return checkHostStatus;
        }

        /**
         * Legt den Wert der checkHostStatus-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setCheckHostStatus(JAXBElement<String> value) {
            this.checkHostStatus = value;
        }

        /**
         * Ruft den Wert der associateWithHost-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getAssociateWithHost() {
            return associateWithHost;
        }

        /**
         * Legt den Wert der associateWithHost-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setAssociateWithHost(JAXBElement<String> value) {
            this.associateWithHost = value;
        }

        /**
         * Ruft den Wert der disassociateFromHost-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getDisassociateFromHost() {
            return disassociateFromHost;
        }

        /**
         * Legt den Wert der disassociateFromHost-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setDisassociateFromHost(JAXBElement<String> value) {
            this.disassociateFromHost = value;
        }

        /**
         * Ruft den Wert der disassociateFromRemoteHost-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getDisassociateFromRemoteHost() {
            return disassociateFromRemoteHost;
        }

        /**
         * Legt den Wert der disassociateFromRemoteHost-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setDisassociateFromRemoteHost(JAXBElement<String> value) {
            this.disassociateFromRemoteHost = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="enableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
     *         <element name="disableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToMessageDepositStatus" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "enableMessageDeposit",
        "disableMessageDeposit",
        "listenToMessageDepositStatus",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class MessageDepositMenuKeys {

        @XmlElementRef(name = "enableMessageDeposit", type = JAXBElement.class, required = false)
        protected JAXBElement<String> enableMessageDeposit;
        @XmlElementRef(name = "disableMessageDeposit", type = JAXBElement.class, required = false)
        protected JAXBElement<String> disableMessageDeposit;
        @XmlElementRef(name = "listenToMessageDepositStatus", type = JAXBElement.class, required = false)
        protected JAXBElement<String> listenToMessageDepositStatus;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der enableMessageDeposit-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getEnableMessageDeposit() {
            return enableMessageDeposit;
        }

        /**
         * Legt den Wert der enableMessageDeposit-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setEnableMessageDeposit(JAXBElement<String> value) {
            this.enableMessageDeposit = value;
        }

        /**
         * Ruft den Wert der disableMessageDeposit-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getDisableMessageDeposit() {
            return disableMessageDeposit;
        }

        /**
         * Legt den Wert der disableMessageDeposit-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setDisableMessageDeposit(JAXBElement<String> value) {
            this.disableMessageDeposit = value;
        }

        /**
         * Ruft den Wert der listenToMessageDepositStatus-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getListenToMessageDepositStatus() {
            return listenToMessageDepositStatus;
        }

        /**
         * Legt den Wert der listenToMessageDepositStatus-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setListenToMessageDepositStatus(JAXBElement<String> value) {
            this.listenToMessageDepositStatus = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="finishEnteringOrReenteringPasscode" type="{}DigitStarPound" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitStarPound" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "finishEnteringOrReenteringPasscode",
        "returnToPreviousMenu"
    })
    public static class PasscodeMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String finishEnteringOrReenteringPasscode;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;

        /**
         * Ruft den Wert der finishEnteringOrReenteringPasscode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFinishEnteringOrReenteringPasscode() {
            return finishEnteringOrReenteringPasscode;
        }

        /**
         * Legt den Wert der finishEnteringOrReenteringPasscode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFinishEnteringOrReenteringPasscode(String value) {
            this.finishEnteringOrReenteringPasscode = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="setPresenceToNone" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setPresenceToBusinessTrip" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setPresenceToGoneForTheDay" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setPresenceToLunch" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setPresenceToMeeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setPresenceToOutOfOffice" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setPresenceToTemporarilyOut" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setPresenceToTraining" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setPresenceToUnavailable" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setPresenceToVacation" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "setPresenceToNone",
        "setPresenceToBusinessTrip",
        "setPresenceToGoneForTheDay",
        "setPresenceToLunch",
        "setPresenceToMeeting",
        "setPresenceToOutOfOffice",
        "setPresenceToTemporarilyOut",
        "setPresenceToTraining",
        "setPresenceToUnavailable",
        "setPresenceToVacation",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class PersonalAssistantMenuKeys {

        @XmlElementRef(name = "setPresenceToNone", type = JAXBElement.class, required = false)
        protected JAXBElement<String> setPresenceToNone;
        @XmlElementRef(name = "setPresenceToBusinessTrip", type = JAXBElement.class, required = false)
        protected JAXBElement<String> setPresenceToBusinessTrip;
        @XmlElementRef(name = "setPresenceToGoneForTheDay", type = JAXBElement.class, required = false)
        protected JAXBElement<String> setPresenceToGoneForTheDay;
        @XmlElementRef(name = "setPresenceToLunch", type = JAXBElement.class, required = false)
        protected JAXBElement<String> setPresenceToLunch;
        @XmlElementRef(name = "setPresenceToMeeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> setPresenceToMeeting;
        @XmlElementRef(name = "setPresenceToOutOfOffice", type = JAXBElement.class, required = false)
        protected JAXBElement<String> setPresenceToOutOfOffice;
        @XmlElementRef(name = "setPresenceToTemporarilyOut", type = JAXBElement.class, required = false)
        protected JAXBElement<String> setPresenceToTemporarilyOut;
        @XmlElementRef(name = "setPresenceToTraining", type = JAXBElement.class, required = false)
        protected JAXBElement<String> setPresenceToTraining;
        @XmlElementRef(name = "setPresenceToUnavailable", type = JAXBElement.class, required = false)
        protected JAXBElement<String> setPresenceToUnavailable;
        @XmlElementRef(name = "setPresenceToVacation", type = JAXBElement.class, required = false)
        protected JAXBElement<String> setPresenceToVacation;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der setPresenceToNone-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSetPresenceToNone() {
            return setPresenceToNone;
        }

        /**
         * Legt den Wert der setPresenceToNone-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSetPresenceToNone(JAXBElement<String> value) {
            this.setPresenceToNone = value;
        }

        /**
         * Ruft den Wert der setPresenceToBusinessTrip-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSetPresenceToBusinessTrip() {
            return setPresenceToBusinessTrip;
        }

        /**
         * Legt den Wert der setPresenceToBusinessTrip-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSetPresenceToBusinessTrip(JAXBElement<String> value) {
            this.setPresenceToBusinessTrip = value;
        }

        /**
         * Ruft den Wert der setPresenceToGoneForTheDay-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSetPresenceToGoneForTheDay() {
            return setPresenceToGoneForTheDay;
        }

        /**
         * Legt den Wert der setPresenceToGoneForTheDay-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSetPresenceToGoneForTheDay(JAXBElement<String> value) {
            this.setPresenceToGoneForTheDay = value;
        }

        /**
         * Ruft den Wert der setPresenceToLunch-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSetPresenceToLunch() {
            return setPresenceToLunch;
        }

        /**
         * Legt den Wert der setPresenceToLunch-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSetPresenceToLunch(JAXBElement<String> value) {
            this.setPresenceToLunch = value;
        }

        /**
         * Ruft den Wert der setPresenceToMeeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSetPresenceToMeeting() {
            return setPresenceToMeeting;
        }

        /**
         * Legt den Wert der setPresenceToMeeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSetPresenceToMeeting(JAXBElement<String> value) {
            this.setPresenceToMeeting = value;
        }

        /**
         * Ruft den Wert der setPresenceToOutOfOffice-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSetPresenceToOutOfOffice() {
            return setPresenceToOutOfOffice;
        }

        /**
         * Legt den Wert der setPresenceToOutOfOffice-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSetPresenceToOutOfOffice(JAXBElement<String> value) {
            this.setPresenceToOutOfOffice = value;
        }

        /**
         * Ruft den Wert der setPresenceToTemporarilyOut-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSetPresenceToTemporarilyOut() {
            return setPresenceToTemporarilyOut;
        }

        /**
         * Legt den Wert der setPresenceToTemporarilyOut-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSetPresenceToTemporarilyOut(JAXBElement<String> value) {
            this.setPresenceToTemporarilyOut = value;
        }

        /**
         * Ruft den Wert der setPresenceToTraining-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSetPresenceToTraining() {
            return setPresenceToTraining;
        }

        /**
         * Legt den Wert der setPresenceToTraining-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSetPresenceToTraining(JAXBElement<String> value) {
            this.setPresenceToTraining = value;
        }

        /**
         * Ruft den Wert der setPresenceToUnavailable-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSetPresenceToUnavailable() {
            return setPresenceToUnavailable;
        }

        /**
         * Legt den Wert der setPresenceToUnavailable-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSetPresenceToUnavailable(JAXBElement<String> value) {
            this.setPresenceToUnavailable = value;
        }

        /**
         * Ruft den Wert der setPresenceToVacation-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSetPresenceToVacation() {
            return setPresenceToVacation;
        }

        /**
         * Legt den Wert der setPresenceToVacation-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSetPresenceToVacation(JAXBElement<String> value) {
            this.setPresenceToVacation = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="recordNewPersonalizedName" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToCurrentPersonalizedName" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deletePersonalizedName" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "recordNewPersonalizedName",
        "listenToCurrentPersonalizedName",
        "deletePersonalizedName",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class PersonalizedNameMenuKeys {

        @XmlElementRef(name = "recordNewPersonalizedName", type = JAXBElement.class, required = false)
        protected JAXBElement<String> recordNewPersonalizedName;
        @XmlElementRef(name = "listenToCurrentPersonalizedName", type = JAXBElement.class, required = false)
        protected JAXBElement<String> listenToCurrentPersonalizedName;
        @XmlElementRef(name = "deletePersonalizedName", type = JAXBElement.class, required = false)
        protected JAXBElement<String> deletePersonalizedName;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der recordNewPersonalizedName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRecordNewPersonalizedName() {
            return recordNewPersonalizedName;
        }

        /**
         * Legt den Wert der recordNewPersonalizedName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRecordNewPersonalizedName(JAXBElement<String> value) {
            this.recordNewPersonalizedName = value;
        }

        /**
         * Ruft den Wert der listenToCurrentPersonalizedName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getListenToCurrentPersonalizedName() {
            return listenToCurrentPersonalizedName;
        }

        /**
         * Legt den Wert der listenToCurrentPersonalizedName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setListenToCurrentPersonalizedName(JAXBElement<String> value) {
            this.listenToCurrentPersonalizedName = value;
        }

        /**
         * Ruft den Wert der deletePersonalizedName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getDeletePersonalizedName() {
            return deletePersonalizedName;
        }

        /**
         * Legt den Wert der deletePersonalizedName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setDeletePersonalizedName(JAXBElement<String> value) {
            this.deletePersonalizedName = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="skipBackward" type="{}DigitAny" minOccurs="0"/>
     *         <element name="pauseOrResume" type="{}DigitAny" minOccurs="0"/>
     *         <element name="skipForward" type="{}DigitAny" minOccurs="0"/>
     *         <element name="jumpToBegin" type="{}DigitAny" minOccurs="0"/>
     *         <element name="jumpToEnd" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "skipBackward",
        "pauseOrResume",
        "skipForward",
        "jumpToBegin",
        "jumpToEnd"
    })
    public static class PlayGreetingMenuKeys {

        @XmlElementRef(name = "skipBackward", type = JAXBElement.class, required = false)
        protected JAXBElement<String> skipBackward;
        @XmlElementRef(name = "pauseOrResume", type = JAXBElement.class, required = false)
        protected JAXBElement<String> pauseOrResume;
        @XmlElementRef(name = "skipForward", type = JAXBElement.class, required = false)
        protected JAXBElement<String> skipForward;
        @XmlElementRef(name = "jumpToBegin", type = JAXBElement.class, required = false)
        protected JAXBElement<String> jumpToBegin;
        @XmlElementRef(name = "jumpToEnd", type = JAXBElement.class, required = false)
        protected JAXBElement<String> jumpToEnd;

        /**
         * Ruft den Wert der skipBackward-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSkipBackward() {
            return skipBackward;
        }

        /**
         * Legt den Wert der skipBackward-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSkipBackward(JAXBElement<String> value) {
            this.skipBackward = value;
        }

        /**
         * Ruft den Wert der pauseOrResume-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPauseOrResume() {
            return pauseOrResume;
        }

        /**
         * Legt den Wert der pauseOrResume-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPauseOrResume(JAXBElement<String> value) {
            this.pauseOrResume = value;
        }

        /**
         * Ruft den Wert der skipForward-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSkipForward() {
            return skipForward;
        }

        /**
         * Legt den Wert der skipForward-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSkipForward(JAXBElement<String> value) {
            this.skipForward = value;
        }

        /**
         * Ruft den Wert der jumpToBegin-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getJumpToBegin() {
            return jumpToBegin;
        }

        /**
         * Legt den Wert der jumpToBegin-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setJumpToBegin(JAXBElement<String> value) {
            this.jumpToBegin = value;
        }

        /**
         * Ruft den Wert der jumpToEnd-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getJumpToEnd() {
            return jumpToEnd;
        }

        /**
         * Legt den Wert der jumpToEnd-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setJumpToEnd(JAXBElement<String> value) {
            this.jumpToEnd = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="skipBackward" type="{}DigitAny" minOccurs="0"/>
     *         <element name="pauseOrResume" type="{}DigitAny" minOccurs="0"/>
     *         <element name="skipForward" type="{}DigitAny" minOccurs="0"/>
     *         <element name="jumpToBegin" type="{}DigitAny" minOccurs="0"/>
     *         <element name="jumpToEnd" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "skipBackward",
        "pauseOrResume",
        "skipForward",
        "jumpToBegin",
        "jumpToEnd"
    })
    public static class PlayMessageMenuKeys {

        @XmlElementRef(name = "skipBackward", type = JAXBElement.class, required = false)
        protected JAXBElement<String> skipBackward;
        @XmlElementRef(name = "pauseOrResume", type = JAXBElement.class, required = false)
        protected JAXBElement<String> pauseOrResume;
        @XmlElementRef(name = "skipForward", type = JAXBElement.class, required = false)
        protected JAXBElement<String> skipForward;
        @XmlElementRef(name = "jumpToBegin", type = JAXBElement.class, required = false)
        protected JAXBElement<String> jumpToBegin;
        @XmlElementRef(name = "jumpToEnd", type = JAXBElement.class, required = false)
        protected JAXBElement<String> jumpToEnd;

        /**
         * Ruft den Wert der skipBackward-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSkipBackward() {
            return skipBackward;
        }

        /**
         * Legt den Wert der skipBackward-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSkipBackward(JAXBElement<String> value) {
            this.skipBackward = value;
        }

        /**
         * Ruft den Wert der pauseOrResume-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPauseOrResume() {
            return pauseOrResume;
        }

        /**
         * Legt den Wert der pauseOrResume-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPauseOrResume(JAXBElement<String> value) {
            this.pauseOrResume = value;
        }

        /**
         * Ruft den Wert der skipForward-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSkipForward() {
            return skipForward;
        }

        /**
         * Legt den Wert der skipForward-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSkipForward(JAXBElement<String> value) {
            this.skipForward = value;
        }

        /**
         * Ruft den Wert der jumpToBegin-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getJumpToBegin() {
            return jumpToBegin;
        }

        /**
         * Legt den Wert der jumpToBegin-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setJumpToBegin(JAXBElement<String> value) {
            this.jumpToBegin = value;
        }

        /**
         * Ruft den Wert der jumpToEnd-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getJumpToEnd() {
            return jumpToEnd;
        }

        /**
         * Legt den Wert der jumpToEnd-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setJumpToEnd(JAXBElement<String> value) {
            this.jumpToEnd = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="saveMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deleteMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="playMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="previousMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
     *         <element name="nextMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="callbackCaller" type="{}DigitAny" minOccurs="0"/>
     *         <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="replyMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="forwardMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="additionalMessageOptions" type="{}DigitAny" minOccurs="0"/>
     *         <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
     *         <element name="passcode" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "saveMessage",
        "deleteMessage",
        "playMessage",
        "previousMessage",
        "playEnvelope",
        "nextMessage",
        "callbackCaller",
        "composeMessage",
        "replyMessage",
        "forwardMessage",
        "additionalMessageOptions",
        "personalizedName",
        "passcode",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class PlayMessagesMenuKeys {

        @XmlElementRef(name = "saveMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> saveMessage;
        @XmlElementRef(name = "deleteMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> deleteMessage;
        @XmlElementRef(name = "playMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> playMessage;
        @XmlElementRef(name = "previousMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> previousMessage;
        @XmlElementRef(name = "playEnvelope", type = JAXBElement.class, required = false)
        protected JAXBElement<String> playEnvelope;
        @XmlElementRef(name = "nextMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> nextMessage;
        @XmlElementRef(name = "callbackCaller", type = JAXBElement.class, required = false)
        protected JAXBElement<String> callbackCaller;
        @XmlElementRef(name = "composeMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> composeMessage;
        @XmlElementRef(name = "replyMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> replyMessage;
        @XmlElementRef(name = "forwardMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> forwardMessage;
        @XmlElementRef(name = "additionalMessageOptions", type = JAXBElement.class, required = false)
        protected JAXBElement<String> additionalMessageOptions;
        @XmlElementRef(name = "personalizedName", type = JAXBElement.class, required = false)
        protected JAXBElement<String> personalizedName;
        @XmlElementRef(name = "passcode", type = JAXBElement.class, required = false)
        protected JAXBElement<String> passcode;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der saveMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSaveMessage() {
            return saveMessage;
        }

        /**
         * Legt den Wert der saveMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSaveMessage(JAXBElement<String> value) {
            this.saveMessage = value;
        }

        /**
         * Ruft den Wert der deleteMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getDeleteMessage() {
            return deleteMessage;
        }

        /**
         * Legt den Wert der deleteMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setDeleteMessage(JAXBElement<String> value) {
            this.deleteMessage = value;
        }

        /**
         * Ruft den Wert der playMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPlayMessage() {
            return playMessage;
        }

        /**
         * Legt den Wert der playMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPlayMessage(JAXBElement<String> value) {
            this.playMessage = value;
        }

        /**
         * Ruft den Wert der previousMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPreviousMessage() {
            return previousMessage;
        }

        /**
         * Legt den Wert der previousMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPreviousMessage(JAXBElement<String> value) {
            this.previousMessage = value;
        }

        /**
         * Ruft den Wert der playEnvelope-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPlayEnvelope() {
            return playEnvelope;
        }

        /**
         * Legt den Wert der playEnvelope-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPlayEnvelope(JAXBElement<String> value) {
            this.playEnvelope = value;
        }

        /**
         * Ruft den Wert der nextMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getNextMessage() {
            return nextMessage;
        }

        /**
         * Legt den Wert der nextMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setNextMessage(JAXBElement<String> value) {
            this.nextMessage = value;
        }

        /**
         * Ruft den Wert der callbackCaller-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getCallbackCaller() {
            return callbackCaller;
        }

        /**
         * Legt den Wert der callbackCaller-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setCallbackCaller(JAXBElement<String> value) {
            this.callbackCaller = value;
        }

        /**
         * Ruft den Wert der composeMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getComposeMessage() {
            return composeMessage;
        }

        /**
         * Legt den Wert der composeMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setComposeMessage(JAXBElement<String> value) {
            this.composeMessage = value;
        }

        /**
         * Ruft den Wert der replyMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getReplyMessage() {
            return replyMessage;
        }

        /**
         * Legt den Wert der replyMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setReplyMessage(JAXBElement<String> value) {
            this.replyMessage = value;
        }

        /**
         * Ruft den Wert der forwardMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getForwardMessage() {
            return forwardMessage;
        }

        /**
         * Legt den Wert der forwardMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setForwardMessage(JAXBElement<String> value) {
            this.forwardMessage = value;
        }

        /**
         * Ruft den Wert der additionalMessageOptions-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getAdditionalMessageOptions() {
            return additionalMessageOptions;
        }

        /**
         * Legt den Wert der additionalMessageOptions-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setAdditionalMessageOptions(JAXBElement<String> value) {
            this.additionalMessageOptions = value;
        }

        /**
         * Ruft den Wert der personalizedName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPersonalizedName() {
            return personalizedName;
        }

        /**
         * Legt den Wert der personalizedName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPersonalizedName(JAXBElement<String> value) {
            this.personalizedName = value;
        }

        /**
         * Ruft den Wert der passcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPasscode() {
            return passcode;
        }

        /**
         * Legt den Wert der passcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPasscode(JAXBElement<String> value) {
            this.passcode = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="endRecording" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "endRecording"
    })
    public static class RecordNewGreetingOrPersonalizedNameMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String endRecording;

        /**
         * Ruft den Wert der endRecording-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndRecording() {
            return endRecording;
        }

        /**
         * Legt den Wert der endRecording-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndRecording(String value) {
            this.endRecording = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="sendReplyToCaller" type="{}DigitAny" minOccurs="0"/>
     *         <element name="changeCurrentReply" type="{}DigitAny" minOccurs="0"/>
     *         <element name="listenToCurrentReply" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setOrClearUrgentIndicator" type="{}DigitAny" minOccurs="0"/>
     *         <element name="setOrClearConfidentialIndicator" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sendReplyToCaller",
        "changeCurrentReply",
        "listenToCurrentReply",
        "setOrClearUrgentIndicator",
        "setOrClearConfidentialIndicator",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class ReplyMessageMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String sendReplyToCaller;
        @XmlElementRef(name = "changeCurrentReply", type = JAXBElement.class, required = false)
        protected JAXBElement<String> changeCurrentReply;
        @XmlElementRef(name = "listenToCurrentReply", type = JAXBElement.class, required = false)
        protected JAXBElement<String> listenToCurrentReply;
        @XmlElementRef(name = "setOrClearUrgentIndicator", type = JAXBElement.class, required = false)
        protected JAXBElement<String> setOrClearUrgentIndicator;
        @XmlElementRef(name = "setOrClearConfidentialIndicator", type = JAXBElement.class, required = false)
        protected JAXBElement<String> setOrClearConfidentialIndicator;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der sendReplyToCaller-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSendReplyToCaller() {
            return sendReplyToCaller;
        }

        /**
         * Legt den Wert der sendReplyToCaller-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSendReplyToCaller(String value) {
            this.sendReplyToCaller = value;
        }

        /**
         * Ruft den Wert der changeCurrentReply-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getChangeCurrentReply() {
            return changeCurrentReply;
        }

        /**
         * Legt den Wert der changeCurrentReply-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setChangeCurrentReply(JAXBElement<String> value) {
            this.changeCurrentReply = value;
        }

        /**
         * Ruft den Wert der listenToCurrentReply-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getListenToCurrentReply() {
            return listenToCurrentReply;
        }

        /**
         * Legt den Wert der listenToCurrentReply-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setListenToCurrentReply(JAXBElement<String> value) {
            this.listenToCurrentReply = value;
        }

        /**
         * Ruft den Wert der setOrClearUrgentIndicator-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSetOrClearUrgentIndicator() {
            return setOrClearUrgentIndicator;
        }

        /**
         * Legt den Wert der setOrClearUrgentIndicator-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSetOrClearUrgentIndicator(JAXBElement<String> value) {
            this.setOrClearUrgentIndicator = value;
        }

        /**
         * Ruft den Wert der setOrClearConfidentialIndicator-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSetOrClearConfidentialIndicator() {
            return setOrClearConfidentialIndicator;
        }

        /**
         * Legt den Wert der setOrClearConfidentialIndicator-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSetOrClearConfidentialIndicator(JAXBElement<String> value) {
            this.setOrClearConfidentialIndicator = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="interruptPlaybackAndReturnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "interruptPlaybackAndReturnToPreviousMenu"
    })
    public static class ReviewSelectedDistributionListMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String interruptPlaybackAndReturnToPreviousMenu;

        /**
         * Ruft den Wert der interruptPlaybackAndReturnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInterruptPlaybackAndReturnToPreviousMenu() {
            return interruptPlaybackAndReturnToPreviousMenu;
        }

        /**
         * Legt den Wert der interruptPlaybackAndReturnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInterruptPlaybackAndReturnToPreviousMenu(String value) {
            this.interruptPlaybackAndReturnToPreviousMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="returnToPreviousMenu" type="{}DigitStarPound" minOccurs="0"/>
     *         <element name="repeatMenuOrFinishEnteringDistributionListNumber" type="{}DigitStarPound" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "returnToPreviousMenu",
        "repeatMenuOrFinishEnteringDistributionListNumber"
    })
    public static class SelectDistributionListMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenuOrFinishEnteringDistributionListNumber", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenuOrFinishEnteringDistributionListNumber;

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenuOrFinishEnteringDistributionListNumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenuOrFinishEnteringDistributionListNumber() {
            return repeatMenuOrFinishEnteringDistributionListNumber;
        }

        /**
         * Legt den Wert der repeatMenuOrFinishEnteringDistributionListNumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenuOrFinishEnteringDistributionListNumber(JAXBElement<String> value) {
            this.repeatMenuOrFinishEnteringDistributionListNumber = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="confirmSendingToDistributionList" type="{}DigitStarPound" minOccurs="0"/>
     *         <element name="cancelSendingToDistributionList" type="{}DigitStarPound" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "confirmSendingToDistributionList",
        "cancelSendingToDistributionList"
    })
    public static class SendMessageToSelectedDistributionListMenuKeys {

        @XmlElementRef(name = "confirmSendingToDistributionList", type = JAXBElement.class, required = false)
        protected JAXBElement<String> confirmSendingToDistributionList;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String cancelSendingToDistributionList;

        /**
         * Ruft den Wert der confirmSendingToDistributionList-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getConfirmSendingToDistributionList() {
            return confirmSendingToDistributionList;
        }

        /**
         * Legt den Wert der confirmSendingToDistributionList-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setConfirmSendingToDistributionList(JAXBElement<String> value) {
            this.confirmSendingToDistributionList = value;
        }

        /**
         * Ruft den Wert der cancelSendingToDistributionList-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCancelSendingToDistributionList() {
            return cancelSendingToDistributionList;
        }

        /**
         * Legt den Wert der cancelSendingToDistributionList-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCancelSendingToDistributionList(String value) {
            this.cancelSendingToDistributionList = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="confirmSendingToEntireGroup" type="{}DigitAny" minOccurs="0"/>
     *         <element name="cancelSendingToEntireGroup" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "confirmSendingToEntireGroup",
        "cancelSendingToEntireGroup"
    })
    public static class SendToAllGroupMembersMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String confirmSendingToEntireGroup;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String cancelSendingToEntireGroup;

        /**
         * Ruft den Wert der confirmSendingToEntireGroup-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConfirmSendingToEntireGroup() {
            return confirmSendingToEntireGroup;
        }

        /**
         * Legt den Wert der confirmSendingToEntireGroup-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConfirmSendingToEntireGroup(String value) {
            this.confirmSendingToEntireGroup = value;
        }

        /**
         * Ruft den Wert der cancelSendingToEntireGroup-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCancelSendingToEntireGroup() {
            return cancelSendingToEntireGroup;
        }

        /**
         * Legt den Wert der cancelSendingToEntireGroup-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCancelSendingToEntireGroup(String value) {
            this.cancelSendingToEntireGroup = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="sendMessageToSelectedDistributionList" type="{}DigitAny" minOccurs="0"/>
     *         <element name="selectDistributionList" type="{}DigitAny" minOccurs="0"/>
     *         <element name="reviewSelectedDistributionList" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sendMessageToSelectedDistributionList",
        "selectDistributionList",
        "reviewSelectedDistributionList",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class SendToDistributionListMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String sendMessageToSelectedDistributionList;
        @XmlElementRef(name = "selectDistributionList", type = JAXBElement.class, required = false)
        protected JAXBElement<String> selectDistributionList;
        @XmlElementRef(name = "reviewSelectedDistributionList", type = JAXBElement.class, required = false)
        protected JAXBElement<String> reviewSelectedDistributionList;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der sendMessageToSelectedDistributionList-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSendMessageToSelectedDistributionList() {
            return sendMessageToSelectedDistributionList;
        }

        /**
         * Legt den Wert der sendMessageToSelectedDistributionList-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSendMessageToSelectedDistributionList(String value) {
            this.sendMessageToSelectedDistributionList = value;
        }

        /**
         * Ruft den Wert der selectDistributionList-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getSelectDistributionList() {
            return selectDistributionList;
        }

        /**
         * Legt den Wert der selectDistributionList-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setSelectDistributionList(JAXBElement<String> value) {
            this.selectDistributionList = value;
        }

        /**
         * Ruft den Wert der reviewSelectedDistributionList-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getReviewSelectedDistributionList() {
            return reviewSelectedDistributionList;
        }

        /**
         * Legt den Wert der reviewSelectedDistributionList-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setReviewSelectedDistributionList(JAXBElement<String> value) {
            this.reviewSelectedDistributionList = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="confirmSendingMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="cancelSendingMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="finishEnteringNumberWhereToSendMessageTo" type="{}DigitStarPound" minOccurs="0"/>
     *         <element name="finishForwardingOrSendingMessage" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "confirmSendingMessage",
        "cancelSendingMessage",
        "finishEnteringNumberWhereToSendMessageTo",
        "finishForwardingOrSendingMessage"
    })
    public static class SendToPersonMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String confirmSendingMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String cancelSendingMessage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String finishEnteringNumberWhereToSendMessageTo;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String finishForwardingOrSendingMessage;

        /**
         * Ruft den Wert der confirmSendingMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConfirmSendingMessage() {
            return confirmSendingMessage;
        }

        /**
         * Legt den Wert der confirmSendingMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConfirmSendingMessage(String value) {
            this.confirmSendingMessage = value;
        }

        /**
         * Ruft den Wert der cancelSendingMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCancelSendingMessage() {
            return cancelSendingMessage;
        }

        /**
         * Legt den Wert der cancelSendingMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCancelSendingMessage(String value) {
            this.cancelSendingMessage = value;
        }

        /**
         * Ruft den Wert der finishEnteringNumberWhereToSendMessageTo-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFinishEnteringNumberWhereToSendMessageTo() {
            return finishEnteringNumberWhereToSendMessageTo;
        }

        /**
         * Legt den Wert der finishEnteringNumberWhereToSendMessageTo-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFinishEnteringNumberWhereToSendMessageTo(String value) {
            this.finishEnteringNumberWhereToSendMessageTo = value;
        }

        /**
         * Ruft den Wert der finishForwardingOrSendingMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFinishForwardingOrSendingMessage() {
            return finishForwardingOrSendingMessage;
        }

        /**
         * Legt den Wert der finishForwardingOrSendingMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFinishForwardingOrSendingMessage(String value) {
            this.finishForwardingOrSendingMessage = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="playMessages" type="{}DigitAny" minOccurs="0"/>
     *         <element name="changeBusyGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="changeNoAnswerGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="changeExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
     *         <element name="deleteAllMessages" type="{}DigitAny" minOccurs="0"/>
     *         <element name="passcode" type="{}DigitAny" minOccurs="0"/>
     *         <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
     *         <element name="messageDeposit" type="{}DigitAny" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "playMessages",
        "changeBusyGreeting",
        "changeNoAnswerGreeting",
        "changeExtendedAwayGreeting",
        "composeMessage",
        "deleteAllMessages",
        "passcode",
        "personalizedName",
        "messageDeposit",
        "returnToPreviousMenu",
        "repeatMenu"
    })
    public static class VoiceMessagingMenuKeys {

        @XmlElementRef(name = "playMessages", type = JAXBElement.class, required = false)
        protected JAXBElement<String> playMessages;
        @XmlElementRef(name = "changeBusyGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> changeBusyGreeting;
        @XmlElementRef(name = "changeNoAnswerGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> changeNoAnswerGreeting;
        @XmlElementRef(name = "changeExtendedAwayGreeting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> changeExtendedAwayGreeting;
        @XmlElementRef(name = "composeMessage", type = JAXBElement.class, required = false)
        protected JAXBElement<String> composeMessage;
        @XmlElementRef(name = "deleteAllMessages", type = JAXBElement.class, required = false)
        protected JAXBElement<String> deleteAllMessages;
        @XmlElementRef(name = "passcode", type = JAXBElement.class, required = false)
        protected JAXBElement<String> passcode;
        @XmlElementRef(name = "personalizedName", type = JAXBElement.class, required = false)
        protected JAXBElement<String> personalizedName;
        @XmlElementRef(name = "messageDeposit", type = JAXBElement.class, required = false)
        protected JAXBElement<String> messageDeposit;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;

        /**
         * Ruft den Wert der playMessages-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPlayMessages() {
            return playMessages;
        }

        /**
         * Legt den Wert der playMessages-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPlayMessages(JAXBElement<String> value) {
            this.playMessages = value;
        }

        /**
         * Ruft den Wert der changeBusyGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getChangeBusyGreeting() {
            return changeBusyGreeting;
        }

        /**
         * Legt den Wert der changeBusyGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setChangeBusyGreeting(JAXBElement<String> value) {
            this.changeBusyGreeting = value;
        }

        /**
         * Ruft den Wert der changeNoAnswerGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getChangeNoAnswerGreeting() {
            return changeNoAnswerGreeting;
        }

        /**
         * Legt den Wert der changeNoAnswerGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setChangeNoAnswerGreeting(JAXBElement<String> value) {
            this.changeNoAnswerGreeting = value;
        }

        /**
         * Ruft den Wert der changeExtendedAwayGreeting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getChangeExtendedAwayGreeting() {
            return changeExtendedAwayGreeting;
        }

        /**
         * Legt den Wert der changeExtendedAwayGreeting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setChangeExtendedAwayGreeting(JAXBElement<String> value) {
            this.changeExtendedAwayGreeting = value;
        }

        /**
         * Ruft den Wert der composeMessage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getComposeMessage() {
            return composeMessage;
        }

        /**
         * Legt den Wert der composeMessage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setComposeMessage(JAXBElement<String> value) {
            this.composeMessage = value;
        }

        /**
         * Ruft den Wert der deleteAllMessages-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getDeleteAllMessages() {
            return deleteAllMessages;
        }

        /**
         * Legt den Wert der deleteAllMessages-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setDeleteAllMessages(JAXBElement<String> value) {
            this.deleteAllMessages = value;
        }

        /**
         * Ruft den Wert der passcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPasscode() {
            return passcode;
        }

        /**
         * Legt den Wert der passcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPasscode(JAXBElement<String> value) {
            this.passcode = value;
        }

        /**
         * Ruft den Wert der personalizedName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPersonalizedName() {
            return personalizedName;
        }

        /**
         * Legt den Wert der personalizedName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPersonalizedName(JAXBElement<String> value) {
            this.personalizedName = value;
        }

        /**
         * Ruft den Wert der messageDeposit-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getMessageDeposit() {
            return messageDeposit;
        }

        /**
         * Legt den Wert der messageDeposit-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setMessageDeposit(JAXBElement<String> value) {
            this.messageDeposit = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="endCurrentCallAndGoBackToPreviousMenu" type="{}VoicePortalDigitSequence" minOccurs="0"/>
     *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "endCurrentCallAndGoBackToPreviousMenu",
        "returnToPreviousMenu"
    })
    public static class VoicePortalCallingMenuKeys {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String endCurrentCallAndGoBackToPreviousMenu;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String returnToPreviousMenu;

        /**
         * Ruft den Wert der endCurrentCallAndGoBackToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndCurrentCallAndGoBackToPreviousMenu() {
            return endCurrentCallAndGoBackToPreviousMenu;
        }

        /**
         * Legt den Wert der endCurrentCallAndGoBackToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndCurrentCallAndGoBackToPreviousMenu(String value) {
            this.endCurrentCallAndGoBackToPreviousMenu = value;
        }

        /**
         * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReturnToPreviousMenu() {
            return returnToPreviousMenu;
        }

        /**
         * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReturnToPreviousMenu(String value) {
            this.returnToPreviousMenu = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="accessUsingOtherMailboxId" type="{}VoicePortalDigitSequence" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accessUsingOtherMailboxId"
    })
    public static class VoicePortalLoginMenuKeys {

        @XmlElementRef(name = "accessUsingOtherMailboxId", type = JAXBElement.class, required = false)
        protected JAXBElement<String> accessUsingOtherMailboxId;

        /**
         * Ruft den Wert der accessUsingOtherMailboxId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getAccessUsingOtherMailboxId() {
            return accessUsingOtherMailboxId;
        }

        /**
         * Legt den Wert der accessUsingOtherMailboxId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setAccessUsingOtherMailboxId(JAXBElement<String> value) {
            this.accessUsingOtherMailboxId = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="voiceMessaging" type="{}DigitAny" minOccurs="0"/>
     *         <element name="commPilotExpressProfile" type="{}DigitAny" minOccurs="0"/>
     *         <element name="greetings" type="{}DigitAny" minOccurs="0"/>
     *         <element name="callForwardingOptions" type="{}DigitAny" minOccurs="0"/>
     *         <element name="voicePortalCalling" type="{}DigitAny" minOccurs="0"/>
     *         <element name="hoteling" type="{}DigitAny" minOccurs="0"/>
     *         <element name="passcode" type="{}DigitAny" minOccurs="0"/>
     *         <element name="exitVoicePortal" type="{}DigitAny" minOccurs="0"/>
     *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="externalRouting" type="{}DigitAny" minOccurs="0"/>
     *         <element name="announcementMenu" type="{}DigitAny" minOccurs="0"/>
     *         <element name="personalAssistant" type="{}DigitAny" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "voiceMessaging",
        "commPilotExpressProfile",
        "greetings",
        "callForwardingOptions",
        "voicePortalCalling",
        "hoteling",
        "passcode",
        "exitVoicePortal",
        "repeatMenu",
        "externalRouting",
        "announcementMenu",
        "personalAssistant"
    })
    public static class VoicePortalMainMenuKeys {

        @XmlElementRef(name = "voiceMessaging", type = JAXBElement.class, required = false)
        protected JAXBElement<String> voiceMessaging;
        @XmlElementRef(name = "commPilotExpressProfile", type = JAXBElement.class, required = false)
        protected JAXBElement<String> commPilotExpressProfile;
        @XmlElementRef(name = "greetings", type = JAXBElement.class, required = false)
        protected JAXBElement<String> greetings;
        @XmlElementRef(name = "callForwardingOptions", type = JAXBElement.class, required = false)
        protected JAXBElement<String> callForwardingOptions;
        @XmlElementRef(name = "voicePortalCalling", type = JAXBElement.class, required = false)
        protected JAXBElement<String> voicePortalCalling;
        @XmlElementRef(name = "hoteling", type = JAXBElement.class, required = false)
        protected JAXBElement<String> hoteling;
        @XmlElementRef(name = "passcode", type = JAXBElement.class, required = false)
        protected JAXBElement<String> passcode;
        @XmlElementRef(name = "exitVoicePortal", type = JAXBElement.class, required = false)
        protected JAXBElement<String> exitVoicePortal;
        @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> repeatMenu;
        @XmlElementRef(name = "externalRouting", type = JAXBElement.class, required = false)
        protected JAXBElement<String> externalRouting;
        @XmlElementRef(name = "announcementMenu", type = JAXBElement.class, required = false)
        protected JAXBElement<String> announcementMenu;
        @XmlElementRef(name = "personalAssistant", type = JAXBElement.class, required = false)
        protected JAXBElement<String> personalAssistant;

        /**
         * Ruft den Wert der voiceMessaging-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getVoiceMessaging() {
            return voiceMessaging;
        }

        /**
         * Legt den Wert der voiceMessaging-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setVoiceMessaging(JAXBElement<String> value) {
            this.voiceMessaging = value;
        }

        /**
         * Ruft den Wert der commPilotExpressProfile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getCommPilotExpressProfile() {
            return commPilotExpressProfile;
        }

        /**
         * Legt den Wert der commPilotExpressProfile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setCommPilotExpressProfile(JAXBElement<String> value) {
            this.commPilotExpressProfile = value;
        }

        /**
         * Ruft den Wert der greetings-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getGreetings() {
            return greetings;
        }

        /**
         * Legt den Wert der greetings-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setGreetings(JAXBElement<String> value) {
            this.greetings = value;
        }

        /**
         * Ruft den Wert der callForwardingOptions-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getCallForwardingOptions() {
            return callForwardingOptions;
        }

        /**
         * Legt den Wert der callForwardingOptions-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setCallForwardingOptions(JAXBElement<String> value) {
            this.callForwardingOptions = value;
        }

        /**
         * Ruft den Wert der voicePortalCalling-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getVoicePortalCalling() {
            return voicePortalCalling;
        }

        /**
         * Legt den Wert der voicePortalCalling-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setVoicePortalCalling(JAXBElement<String> value) {
            this.voicePortalCalling = value;
        }

        /**
         * Ruft den Wert der hoteling-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getHoteling() {
            return hoteling;
        }

        /**
         * Legt den Wert der hoteling-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setHoteling(JAXBElement<String> value) {
            this.hoteling = value;
        }

        /**
         * Ruft den Wert der passcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPasscode() {
            return passcode;
        }

        /**
         * Legt den Wert der passcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPasscode(JAXBElement<String> value) {
            this.passcode = value;
        }

        /**
         * Ruft den Wert der exitVoicePortal-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getExitVoicePortal() {
            return exitVoicePortal;
        }

        /**
         * Legt den Wert der exitVoicePortal-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setExitVoicePortal(JAXBElement<String> value) {
            this.exitVoicePortal = value;
        }

        /**
         * Ruft den Wert der repeatMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getRepeatMenu() {
            return repeatMenu;
        }

        /**
         * Legt den Wert der repeatMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setRepeatMenu(JAXBElement<String> value) {
            this.repeatMenu = value;
        }

        /**
         * Ruft den Wert der externalRouting-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getExternalRouting() {
            return externalRouting;
        }

        /**
         * Legt den Wert der externalRouting-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setExternalRouting(JAXBElement<String> value) {
            this.externalRouting = value;
        }

        /**
         * Ruft den Wert der announcementMenu-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getAnnouncementMenu() {
            return announcementMenu;
        }

        /**
         * Legt den Wert der announcementMenu-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setAnnouncementMenu(JAXBElement<String> value) {
            this.announcementMenu = value;
        }

        /**
         * Ruft den Wert der personalAssistant-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPersonalAssistant() {
            return personalAssistant;
        }

        /**
         * Legt den Wert der personalAssistant-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPersonalAssistant(JAXBElement<String> value) {
            this.personalAssistant = value;
        }

    }

}
