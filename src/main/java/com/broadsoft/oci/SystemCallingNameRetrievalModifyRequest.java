//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modifies the system's calling name retrieval attributes.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         Replaced by: SystemCallingNameRetrievalModifyRequest16sp1
 *       
 * 
 * <p>Java-Klasse für SystemCallingNameRetrievalModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallingNameRetrievalModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="queryTimerMilliSeconds" type="{}CallingNameRetrievalQueryTimerMilliSeconds" minOccurs="0"/>
 *         <element name="serverNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="serverPort" type="{}Port1025" minOccurs="0"/>
 *         <element name="serverTransportProtocol" type="{}TransportProtocol" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallingNameRetrievalModifyRequest", propOrder = {
    "queryTimerMilliSeconds",
    "serverNetAddress",
    "serverPort",
    "serverTransportProtocol"
})
public class SystemCallingNameRetrievalModifyRequest
    extends OCIRequest
{

    protected Integer queryTimerMilliSeconds;
    @XmlElementRef(name = "serverNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serverNetAddress;
    @XmlElementRef(name = "serverPort", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> serverPort;
    @XmlSchemaType(name = "token")
    protected TransportProtocol serverTransportProtocol;

    /**
     * Ruft den Wert der queryTimerMilliSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQueryTimerMilliSeconds() {
        return queryTimerMilliSeconds;
    }

    /**
     * Legt den Wert der queryTimerMilliSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQueryTimerMilliSeconds(Integer value) {
        this.queryTimerMilliSeconds = value;
    }

    /**
     * Ruft den Wert der serverNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServerNetAddress() {
        return serverNetAddress;
    }

    /**
     * Legt den Wert der serverNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServerNetAddress(JAXBElement<String> value) {
        this.serverNetAddress = value;
    }

    /**
     * Ruft den Wert der serverPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getServerPort() {
        return serverPort;
    }

    /**
     * Legt den Wert der serverPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setServerPort(JAXBElement<Integer> value) {
        this.serverPort = value;
    }

    /**
     * Ruft den Wert der serverTransportProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TransportProtocol }
     *     
     */
    public TransportProtocol getServerTransportProtocol() {
        return serverTransportProtocol;
    }

    /**
     * Legt den Wert der serverTransportProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TransportProtocol }
     *     
     */
    public void setServerTransportProtocol(TransportProtocol value) {
        this.serverTransportProtocol = value;
    }

}
