//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to get a list of active group level call center reporting scheduled reports.
 *         The response is either a GroupCallCenterEnhancedReportingScheduledReportGetActiveListResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterEnhancedReportingScheduledReportGetActiveListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterEnhancedReportingScheduledReportGetActiveListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaCallCenterScheduledReportName" type="{}SearchCriteriaCallCenterScheduledReportName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactCallCenterScheduledReportCreatedBySupervisor" type="{}SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor" minOccurs="0"/>
 *         <element name="searchCriteriaExactCallCenterReportTemplateKey" type="{}SearchCriteriaExactCallCenterReportTemplateKey" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterEnhancedReportingScheduledReportGetActiveListRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "responseSizeLimit",
    "searchCriteriaCallCenterScheduledReportName",
    "searchCriteriaExactCallCenterScheduledReportCreatedBySupervisor",
    "searchCriteriaExactCallCenterReportTemplateKey"
})
public class GroupCallCenterEnhancedReportingScheduledReportGetActiveListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected Integer responseSizeLimit;
    protected List<SearchCriteriaCallCenterScheduledReportName> searchCriteriaCallCenterScheduledReportName;
    protected SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor searchCriteriaExactCallCenterScheduledReportCreatedBySupervisor;
    protected SearchCriteriaExactCallCenterReportTemplateKey searchCriteriaExactCallCenterReportTemplateKey;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaCallCenterScheduledReportName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaCallCenterScheduledReportName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaCallCenterScheduledReportName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaCallCenterScheduledReportName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaCallCenterScheduledReportName property.
     */
    public List<SearchCriteriaCallCenterScheduledReportName> getSearchCriteriaCallCenterScheduledReportName() {
        if (searchCriteriaCallCenterScheduledReportName == null) {
            searchCriteriaCallCenterScheduledReportName = new ArrayList<>();
        }
        return this.searchCriteriaCallCenterScheduledReportName;
    }

    /**
     * Ruft den Wert der searchCriteriaExactCallCenterScheduledReportCreatedBySupervisor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor }
     *     
     */
    public SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor getSearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor() {
        return searchCriteriaExactCallCenterScheduledReportCreatedBySupervisor;
    }

    /**
     * Legt den Wert der searchCriteriaExactCallCenterScheduledReportCreatedBySupervisor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor }
     *     
     */
    public void setSearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor(SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor value) {
        this.searchCriteriaExactCallCenterScheduledReportCreatedBySupervisor = value;
    }

    /**
     * Ruft den Wert der searchCriteriaExactCallCenterReportTemplateKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactCallCenterReportTemplateKey }
     *     
     */
    public SearchCriteriaExactCallCenterReportTemplateKey getSearchCriteriaExactCallCenterReportTemplateKey() {
        return searchCriteriaExactCallCenterReportTemplateKey;
    }

    /**
     * Legt den Wert der searchCriteriaExactCallCenterReportTemplateKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactCallCenterReportTemplateKey }
     *     
     */
    public void setSearchCriteriaExactCallCenterReportTemplateKey(SearchCriteriaExactCallCenterReportTemplateKey value) {
        this.searchCriteriaExactCallCenterReportTemplateKey = value;
    }

}
