//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a trunk group's Security Classification configuration
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupTrunkGroupSecurityClassificationModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupTrunkGroupSecurityClassificationModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="trunkGroupKey" type="{}TrunkGroupKey"/>
 *         <element name="defaultSecurityClassification" type="{}SecurityClassificationName"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupTrunkGroupSecurityClassificationModifyRequest", propOrder = {
    "trunkGroupKey",
    "defaultSecurityClassification"
})
public class GroupTrunkGroupSecurityClassificationModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    protected TrunkGroupKey trunkGroupKey;
    @XmlElement(required = true, nillable = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultSecurityClassification;

    /**
     * Ruft den Wert der trunkGroupKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupKey }
     *     
     */
    public TrunkGroupKey getTrunkGroupKey() {
        return trunkGroupKey;
    }

    /**
     * Legt den Wert der trunkGroupKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupKey }
     *     
     */
    public void setTrunkGroupKey(TrunkGroupKey value) {
        this.trunkGroupKey = value;
    }

    /**
     * Ruft den Wert der defaultSecurityClassification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultSecurityClassification() {
        return defaultSecurityClassification;
    }

    /**
     * Legt den Wert der defaultSecurityClassification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultSecurityClassification(String value) {
        this.defaultSecurityClassification = value;
    }

}
