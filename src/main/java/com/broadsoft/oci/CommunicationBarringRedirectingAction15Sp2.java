//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CommunicationBarringRedirectingAction15sp2.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CommunicationBarringRedirectingAction15sp2">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Allow"/>
 *     <enumeration value="Block"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CommunicationBarringRedirectingAction15sp2")
@XmlEnum
public enum CommunicationBarringRedirectingAction15Sp2 {

    @XmlEnumValue("Allow")
    ALLOW("Allow"),
    @XmlEnumValue("Block")
    BLOCK("Block");
    private final String value;

    CommunicationBarringRedirectingAction15Sp2(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommunicationBarringRedirectingAction15Sp2 fromValue(String v) {
        for (CommunicationBarringRedirectingAction15Sp2 c: CommunicationBarringRedirectingAction15Sp2 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
