//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         A list of voice mail distribution lists
 *         It is used when setting a user's voice messaging distribution lists
 *       
 * 
 * <p>Java-Klasse für VoiceMessagingDistributionListModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="VoiceMessagingDistributionListModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="listId" type="{}VoiceMessagingDistributionListId"/>
 *         <element name="description" type="{}VoiceMessagingDistributionListDescription" minOccurs="0"/>
 *         <element name="phoneNumberList" type="{}ReplacementOutgoingDNorSIPURIList" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoiceMessagingDistributionListModify", propOrder = {
    "listId",
    "description",
    "phoneNumberList"
})
public class VoiceMessagingDistributionListModify {

    protected int listId;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "phoneNumberList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementOutgoingDNorSIPURIList> phoneNumberList;

    /**
     * Ruft den Wert der listId-Eigenschaft ab.
     * 
     */
    public int getListId() {
        return listId;
    }

    /**
     * Legt den Wert der listId-Eigenschaft fest.
     * 
     */
    public void setListId(int value) {
        this.listId = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der phoneNumberList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementOutgoingDNorSIPURIList }{@code >}
     *     
     */
    public JAXBElement<ReplacementOutgoingDNorSIPURIList> getPhoneNumberList() {
        return phoneNumberList;
    }

    /**
     * Legt den Wert der phoneNumberList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementOutgoingDNorSIPURIList }{@code >}
     *     
     */
    public void setPhoneNumberList(JAXBElement<ReplacementOutgoingDNorSIPURIList> value) {
        this.phoneNumberList = value;
    }

}
