//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupCallParkGetRequest16sp2.
 *         Contains the settings that apply to the whole group for Call Park.
 *       
 * 
 * <p>Java-Klasse für GroupCallParkGetResponse16sp2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallParkGetResponse16sp2">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="recallTimerSeconds" type="{}CallParkRecallTimerSeconds"/>
 *         <element name="displayTimerSeconds" type="{}CallParkDisplayTimerSeconds"/>
 *         <element name="enableDestinationAnnouncement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="recallAlternateUserId" type="{}UserId" minOccurs="0"/>
 *         <element name="recallRingPattern" type="{}RingPattern"/>
 *         <element name="recallTo" type="{}CallParkRecallTo"/>
 *         <element name="alternateUserRecallTimerSeconds" type="{}CallParkRecallTimerSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallParkGetResponse16sp2", propOrder = {
    "recallTimerSeconds",
    "displayTimerSeconds",
    "enableDestinationAnnouncement",
    "recallAlternateUserId",
    "recallRingPattern",
    "recallTo",
    "alternateUserRecallTimerSeconds"
})
public class GroupCallParkGetResponse16Sp2
    extends OCIDataResponse
{

    protected int recallTimerSeconds;
    protected int displayTimerSeconds;
    protected boolean enableDestinationAnnouncement;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String recallAlternateUserId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected RingPattern recallRingPattern;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallParkRecallTo recallTo;
    protected int alternateUserRecallTimerSeconds;

    /**
     * Ruft den Wert der recallTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getRecallTimerSeconds() {
        return recallTimerSeconds;
    }

    /**
     * Legt den Wert der recallTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setRecallTimerSeconds(int value) {
        this.recallTimerSeconds = value;
    }

    /**
     * Ruft den Wert der displayTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getDisplayTimerSeconds() {
        return displayTimerSeconds;
    }

    /**
     * Legt den Wert der displayTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setDisplayTimerSeconds(int value) {
        this.displayTimerSeconds = value;
    }

    /**
     * Ruft den Wert der enableDestinationAnnouncement-Eigenschaft ab.
     * 
     */
    public boolean isEnableDestinationAnnouncement() {
        return enableDestinationAnnouncement;
    }

    /**
     * Legt den Wert der enableDestinationAnnouncement-Eigenschaft fest.
     * 
     */
    public void setEnableDestinationAnnouncement(boolean value) {
        this.enableDestinationAnnouncement = value;
    }

    /**
     * Ruft den Wert der recallAlternateUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecallAlternateUserId() {
        return recallAlternateUserId;
    }

    /**
     * Legt den Wert der recallAlternateUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecallAlternateUserId(String value) {
        this.recallAlternateUserId = value;
    }

    /**
     * Ruft den Wert der recallRingPattern-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RingPattern }
     *     
     */
    public RingPattern getRecallRingPattern() {
        return recallRingPattern;
    }

    /**
     * Legt den Wert der recallRingPattern-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RingPattern }
     *     
     */
    public void setRecallRingPattern(RingPattern value) {
        this.recallRingPattern = value;
    }

    /**
     * Ruft den Wert der recallTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallParkRecallTo }
     *     
     */
    public CallParkRecallTo getRecallTo() {
        return recallTo;
    }

    /**
     * Legt den Wert der recallTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallParkRecallTo }
     *     
     */
    public void setRecallTo(CallParkRecallTo value) {
        this.recallTo = value;
    }

    /**
     * Ruft den Wert der alternateUserRecallTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getAlternateUserRecallTimerSeconds() {
        return alternateUserRecallTimerSeconds;
    }

    /**
     * Legt den Wert der alternateUserRecallTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setAlternateUserRecallTimerSeconds(int value) {
        this.alternateUserRecallTimerSeconds = value;
    }

}
