//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to get the list of sip device types in the system.
 *         If includeSystemLevel is specified, all system level device types and the reseller device types matching search criteria 
 *         are returned even when searchCriteriaResellerId is specified.        
 *         If reseller administrator sends the request, searchCriteriaResellerId is ignored. All system level device 
 *         types and the device types in the administrator's reseller meeting the search criteria are returned.
 *         See Also: SystemDeviceTypeGetAvailableListRequest22 in AS data mode, SystemDeviceTypeGetAvailableListRequest19 in XS data mode.
 *         The response is either SystemSIPDeviceTypeGetListResponse or ErrorResponse.
 *         
 *         The following data elements are only used in AS data mode and ignored in XS data mode:
 *           searchCriteriaExactDeviceTypeConfigurationOptionType        
 *       
 * 
 * <p>Java-Klasse für SystemSIPDeviceTypeGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPDeviceTypeGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaDeviceType" type="{}SearchCriteriaDeviceType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactSignalingAddressType" type="{}SearchCriteriaExactSignalingAddressType" minOccurs="0"/>
 *         <element name="searchCriteriaResellerId" type="{}SearchCriteriaResellerId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDeviceConfigurationOptions" type="{}SearchCriteriaExactDeviceTypeConfigurationOptionType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="includeSystemLevel" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPDeviceTypeGetListRequest", propOrder = {
    "responseSizeLimit",
    "searchCriteriaDeviceType",
    "searchCriteriaExactSignalingAddressType",
    "searchCriteriaResellerId",
    "searchCriteriaDeviceConfigurationOptions",
    "includeSystemLevel"
})
public class SystemSIPDeviceTypeGetListRequest
    extends OCIRequest
{

    protected Integer responseSizeLimit;
    protected List<SearchCriteriaDeviceType> searchCriteriaDeviceType;
    protected SearchCriteriaExactSignalingAddressType searchCriteriaExactSignalingAddressType;
    protected List<SearchCriteriaResellerId> searchCriteriaResellerId;
    protected List<SearchCriteriaExactDeviceTypeConfigurationOptionType> searchCriteriaDeviceConfigurationOptions;
    protected Boolean includeSystemLevel;

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaDeviceType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDeviceType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDeviceType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDeviceType }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDeviceType property.
     */
    public List<SearchCriteriaDeviceType> getSearchCriteriaDeviceType() {
        if (searchCriteriaDeviceType == null) {
            searchCriteriaDeviceType = new ArrayList<>();
        }
        return this.searchCriteriaDeviceType;
    }

    /**
     * Ruft den Wert der searchCriteriaExactSignalingAddressType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactSignalingAddressType }
     *     
     */
    public SearchCriteriaExactSignalingAddressType getSearchCriteriaExactSignalingAddressType() {
        return searchCriteriaExactSignalingAddressType;
    }

    /**
     * Legt den Wert der searchCriteriaExactSignalingAddressType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactSignalingAddressType }
     *     
     */
    public void setSearchCriteriaExactSignalingAddressType(SearchCriteriaExactSignalingAddressType value) {
        this.searchCriteriaExactSignalingAddressType = value;
    }

    /**
     * Gets the value of the searchCriteriaResellerId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaResellerId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaResellerId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaResellerId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaResellerId property.
     */
    public List<SearchCriteriaResellerId> getSearchCriteriaResellerId() {
        if (searchCriteriaResellerId == null) {
            searchCriteriaResellerId = new ArrayList<>();
        }
        return this.searchCriteriaResellerId;
    }

    /**
     * Gets the value of the searchCriteriaDeviceConfigurationOptions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDeviceConfigurationOptions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDeviceConfigurationOptions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaExactDeviceTypeConfigurationOptionType }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDeviceConfigurationOptions property.
     */
    public List<SearchCriteriaExactDeviceTypeConfigurationOptionType> getSearchCriteriaDeviceConfigurationOptions() {
        if (searchCriteriaDeviceConfigurationOptions == null) {
            searchCriteriaDeviceConfigurationOptions = new ArrayList<>();
        }
        return this.searchCriteriaDeviceConfigurationOptions;
    }

    /**
     * Ruft den Wert der includeSystemLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeSystemLevel() {
        return includeSystemLevel;
    }

    /**
     * Legt den Wert der includeSystemLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeSystemLevel(Boolean value) {
        this.includeSystemLevel = value;
    }

}
