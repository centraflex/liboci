//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for a particular group administrator type.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactGroupAdminType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactGroupAdminType">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="type" type="{}GroupAdminType"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactGroupAdminType", propOrder = {
    "type"
})
public class SearchCriteriaExactGroupAdminType
    extends SearchCriteria
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminType type;

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminType }
     *     
     */
    public GroupAdminType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminType }
     *     
     */
    public void setType(GroupAdminType value) {
        this.type = value;
    }

}
