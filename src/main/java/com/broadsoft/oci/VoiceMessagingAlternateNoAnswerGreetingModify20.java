//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The configuration of a alternate no answer greeting.
 *         It is used when modifying a user's voice messaging greeting.
 *       
 * 
 * <p>Java-Klasse für VoiceMessagingAlternateNoAnswerGreetingModify20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="VoiceMessagingAlternateNoAnswerGreetingModify20">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="name" type="{}VoiceMessagingAlternateNoAnswerGreetingName" minOccurs="0"/>
 *         <element name="audioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="videoFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoiceMessagingAlternateNoAnswerGreetingModify20", propOrder = {
    "name",
    "audioFile",
    "videoFile"
})
public class VoiceMessagingAlternateNoAnswerGreetingModify20 {

    @XmlElementRef(name = "name", type = JAXBElement.class, required = false)
    protected JAXBElement<String> name;
    @XmlElementRef(name = "audioFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> audioFile;
    @XmlElementRef(name = "videoFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> videoFile;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setName(JAXBElement<String> value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der audioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getAudioFile() {
        return audioFile;
    }

    /**
     * Legt den Wert der audioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setAudioFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.audioFile = value;
    }

    /**
     * Ruft den Wert der videoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getVideoFile() {
        return videoFile;
    }

    /**
     * Legt den Wert der videoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setVideoFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.videoFile = value;
    }

}
