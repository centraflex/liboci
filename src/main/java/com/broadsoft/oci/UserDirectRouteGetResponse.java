//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserDirectRouteGetRequest.
 *         Contains the direct route setting and the list of DTGs/Trunk Identities assigned to a user.
 *       
 * 
 * <p>Java-Klasse für UserDirectRouteGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserDirectRouteGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="outgoingDTGPolicy" type="{}DirectRouteOutgoingDTGPolicy"/>
 *         <element name="outgoingTrunkIdentityPolicy" type="{}DirectRouteOutgoingTrunkIdentityPolicy"/>
 *         <element name="directRouteIdentityList" type="{}DirectRouteIdentifiers" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDirectRouteGetResponse", propOrder = {
    "outgoingDTGPolicy",
    "outgoingTrunkIdentityPolicy",
    "directRouteIdentityList"
})
public class UserDirectRouteGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DirectRouteOutgoingDTGPolicy outgoingDTGPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DirectRouteOutgoingTrunkIdentityPolicy outgoingTrunkIdentityPolicy;
    protected DirectRouteIdentifiers directRouteIdentityList;

    /**
     * Ruft den Wert der outgoingDTGPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DirectRouteOutgoingDTGPolicy }
     *     
     */
    public DirectRouteOutgoingDTGPolicy getOutgoingDTGPolicy() {
        return outgoingDTGPolicy;
    }

    /**
     * Legt den Wert der outgoingDTGPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DirectRouteOutgoingDTGPolicy }
     *     
     */
    public void setOutgoingDTGPolicy(DirectRouteOutgoingDTGPolicy value) {
        this.outgoingDTGPolicy = value;
    }

    /**
     * Ruft den Wert der outgoingTrunkIdentityPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DirectRouteOutgoingTrunkIdentityPolicy }
     *     
     */
    public DirectRouteOutgoingTrunkIdentityPolicy getOutgoingTrunkIdentityPolicy() {
        return outgoingTrunkIdentityPolicy;
    }

    /**
     * Legt den Wert der outgoingTrunkIdentityPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DirectRouteOutgoingTrunkIdentityPolicy }
     *     
     */
    public void setOutgoingTrunkIdentityPolicy(DirectRouteOutgoingTrunkIdentityPolicy value) {
        this.outgoingTrunkIdentityPolicy = value;
    }

    /**
     * Ruft den Wert der directRouteIdentityList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DirectRouteIdentifiers }
     *     
     */
    public DirectRouteIdentifiers getDirectRouteIdentityList() {
        return directRouteIdentityList;
    }

    /**
     * Legt den Wert der directRouteIdentityList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DirectRouteIdentifiers }
     *     
     */
    public void setDirectRouteIdentityList(DirectRouteIdentifiers value) {
        this.directRouteIdentityList = value;
    }

}
