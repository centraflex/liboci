//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemServiceAttributeDefaultGetListRequest.
 *         Contains an array Service Attribute entries.
 *       
 * 
 * <p>Java-Klasse für SystemServiceAttributeDefaultGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemServiceAttributeDefaultGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceAttributeEntry" type="{}ServiceAttributeEntryRead" maxOccurs="unbounded"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemServiceAttributeDefaultGetListResponse", propOrder = {
    "serviceAttributeEntry"
})
public class SystemServiceAttributeDefaultGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected List<ServiceAttributeEntryRead> serviceAttributeEntry;

    /**
     * Gets the value of the serviceAttributeEntry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the serviceAttributeEntry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceAttributeEntry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceAttributeEntryRead }
     * 
     * 
     * @return
     *     The value of the serviceAttributeEntry property.
     */
    public List<ServiceAttributeEntryRead> getServiceAttributeEntry() {
        if (serviceAttributeEntry == null) {
            serviceAttributeEntry = new ArrayList<>();
        }
        return this.serviceAttributeEntry;
    }

}
