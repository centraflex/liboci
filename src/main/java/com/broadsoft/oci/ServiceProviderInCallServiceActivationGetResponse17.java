//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *           Response to ServiceProviderInCallServiceActivationGetRequest17.
 *         
 * 
 * <p>Java-Klasse für ServiceProviderInCallServiceActivationGetResponse17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderInCallServiceActivationGetResponse17">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="flashActivationDigits" type="{}InCallServiceActivationDigits"/>
 *         <element name="callTransferActivationDigits" type="{}InCallServiceActivationDigits"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderInCallServiceActivationGetResponse17", propOrder = {
    "flashActivationDigits",
    "callTransferActivationDigits"
})
public class ServiceProviderInCallServiceActivationGetResponse17
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String flashActivationDigits;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callTransferActivationDigits;

    /**
     * Ruft den Wert der flashActivationDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlashActivationDigits() {
        return flashActivationDigits;
    }

    /**
     * Legt den Wert der flashActivationDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlashActivationDigits(String value) {
        this.flashActivationDigits = value;
    }

    /**
     * Ruft den Wert der callTransferActivationDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallTransferActivationDigits() {
        return callTransferActivationDigits;
    }

    /**
     * Legt den Wert der callTransferActivationDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallTransferActivationDigits(String value) {
        this.callTransferActivationDigits = value;
    }

}
