//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EarlyMediaSupportType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="EarlyMediaSupportType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="No Early Media"/>
 *     <enumeration value="RTP - Session"/>
 *     <enumeration value="RTP - Early Session"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "EarlyMediaSupportType")
@XmlEnum
public enum EarlyMediaSupportType {

    @XmlEnumValue("No Early Media")
    NO_EARLY_MEDIA("No Early Media"),
    @XmlEnumValue("RTP - Session")
    RTP_SESSION("RTP - Session"),
    @XmlEnumValue("RTP - Early Session")
    RTP_EARLY_SESSION("RTP - Early Session");
    private final String value;

    EarlyMediaSupportType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EarlyMediaSupportType fromValue(String v) {
        for (EarlyMediaSupportType c: EarlyMediaSupportType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
