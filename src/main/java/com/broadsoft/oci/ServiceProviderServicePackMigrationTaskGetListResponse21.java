//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderServicePackMigrationTaskGetListRequest21.
 *         Contains a table with  a row for each service pack migration task and column headings :
 *         "Start Timestamp Milliseconds", "Name", "Status", "Error Count", "Users Processed", "Users Total".
 *         The start timestamp column is the number of milliseconds since the standard base time known as "the epoch",
 *         namely January 1, 1970, 00:00:00 GMT. The status column is the task status which can be Awaiting edit, Pending, 
 *         Processing, Terminating, Terminated, Stopped by system, Completed, or Expired.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderServicePackMigrationTaskGetListResponse21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderServicePackMigrationTaskGetListResponse21">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="taskTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderServicePackMigrationTaskGetListResponse21", propOrder = {
    "taskTable"
})
public class ServiceProviderServicePackMigrationTaskGetListResponse21
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable taskTable;

    /**
     * Ruft den Wert der taskTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getTaskTable() {
        return taskTable;
    }

    /**
     * Legt den Wert der taskTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setTaskTable(OCITable value) {
        this.taskTable = value;
    }

}
