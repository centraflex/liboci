//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SimultaneousRingSelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SimultaneousRingSelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Do not Ring if on a Call"/>
 *     <enumeration value="Ring for all Incoming Calls"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SimultaneousRingSelection")
@XmlEnum
public enum SimultaneousRingSelection {

    @XmlEnumValue("Do not Ring if on a Call")
    DO_NOT_RING_IF_ON_A_CALL("Do not Ring if on a Call"),
    @XmlEnumValue("Ring for all Incoming Calls")
    RING_FOR_ALL_INCOMING_CALLS("Ring for all Incoming Calls");
    private final String value;

    SimultaneousRingSelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SimultaneousRingSelection fromValue(String v) {
        for (SimultaneousRingSelection c: SimultaneousRingSelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
