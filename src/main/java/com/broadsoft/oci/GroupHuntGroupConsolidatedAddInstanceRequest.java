//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a Hunt Group instance to a group.
 *         The domain is required in the serviceUserId.
 *         
 *         If the phoneNumber has not been assigned to the group and addPhoneNumberToGroup is set to true,
 *         it will be added to group if the command is executed by a service provider administrator or above
 *         and the number is already assigned to the service provider. The command will fail otherwise.
 *         
 *         The weight element inside the agentWeight element will only take effect if the policy element is
 *         set to "Weighted" in the request.
 *         
 *         The response is either SuccessResponse or ErrorResponse.
 *         The following elements are only used in AS data mode:
 *            useSystemHuntGroupCLIDSetting, use value "true" in XS data mode
 *            includeHuntGroupNameInCLID, use value "true" in XS data mode
 *       
 * 
 * <p>Java-Klasse für GroupHuntGroupConsolidatedAddInstanceRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupHuntGroupConsolidatedAddInstanceRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="addPhoneNumberToGroup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceAddProfile"/>
 *         <element name="policy" type="{}HuntPolicy"/>
 *         <element name="huntAfterNoAnswer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="noAnswerNumberOfRings" type="{}HuntNoAnswerRings"/>
 *         <element name="forwardAfterTimeout" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forwardTimeoutSeconds" type="{}HuntForwardTimeoutSeconds"/>
 *         <element name="forwardToPhoneNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <choice minOccurs="0">
 *           <element name="agentUserId" type="{}UserId" maxOccurs="unbounded"/>
 *           <element name="agentWeight" type="{}HuntAgentWeight" maxOccurs="unbounded"/>
 *         </choice>
 *         <element name="allowCallWaitingForAgents" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useSystemHuntGroupCLIDSetting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeHuntGroupNameInCLID" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableNotReachableForwarding" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="notReachableForwardToPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="makeBusyWhenNotReachable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowMembersToControlGroupBusy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableGroupBusy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="applyGroupBusyWhenTerminatingToAgent" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="service" type="{}ConsolidatedUserServiceAssignment" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="directoryNumberHuntingAgentUserIdList" type="{}UserId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="directoryNumberHuntingUseTerminateCallToAgentFirst" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="directoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupHuntGroupConsolidatedAddInstanceRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "serviceUserId",
    "addPhoneNumberToGroup",
    "serviceInstanceProfile",
    "policy",
    "huntAfterNoAnswer",
    "noAnswerNumberOfRings",
    "forwardAfterTimeout",
    "forwardTimeoutSeconds",
    "forwardToPhoneNumber",
    "agentUserId",
    "agentWeight",
    "allowCallWaitingForAgents",
    "useSystemHuntGroupCLIDSetting",
    "includeHuntGroupNameInCLID",
    "enableNotReachableForwarding",
    "notReachableForwardToPhoneNumber",
    "makeBusyWhenNotReachable",
    "allowMembersToControlGroupBusy",
    "enableGroupBusy",
    "applyGroupBusyWhenTerminatingToAgent",
    "networkClassOfService",
    "service",
    "isActive",
    "directoryNumberHuntingAgentUserIdList",
    "directoryNumberHuntingUseTerminateCallToAgentFirst",
    "directoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls"
})
public class GroupHuntGroupConsolidatedAddInstanceRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected Boolean addPhoneNumberToGroup;
    @XmlElement(required = true)
    protected ServiceInstanceAddProfile serviceInstanceProfile;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected HuntPolicy policy;
    protected boolean huntAfterNoAnswer;
    protected int noAnswerNumberOfRings;
    protected boolean forwardAfterTimeout;
    protected int forwardTimeoutSeconds;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String forwardToPhoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> agentUserId;
    protected List<HuntAgentWeight> agentWeight;
    protected boolean allowCallWaitingForAgents;
    protected boolean useSystemHuntGroupCLIDSetting;
    protected boolean includeHuntGroupNameInCLID;
    protected boolean enableNotReachableForwarding;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String notReachableForwardToPhoneNumber;
    protected boolean makeBusyWhenNotReachable;
    protected boolean allowMembersToControlGroupBusy;
    protected boolean enableGroupBusy;
    protected boolean applyGroupBusyWhenTerminatingToAgent;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    protected List<ConsolidatedUserServiceAssignment> service;
    protected boolean isActive;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> directoryNumberHuntingAgentUserIdList;
    protected Boolean directoryNumberHuntingUseTerminateCallToAgentFirst;
    protected Boolean directoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der addPhoneNumberToGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAddPhoneNumberToGroup() {
        return addPhoneNumberToGroup;
    }

    /**
     * Legt den Wert der addPhoneNumberToGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAddPhoneNumberToGroup(Boolean value) {
        this.addPhoneNumberToGroup = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceAddProfile }
     *     
     */
    public ServiceInstanceAddProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceAddProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceAddProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der policy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HuntPolicy }
     *     
     */
    public HuntPolicy getPolicy() {
        return policy;
    }

    /**
     * Legt den Wert der policy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HuntPolicy }
     *     
     */
    public void setPolicy(HuntPolicy value) {
        this.policy = value;
    }

    /**
     * Ruft den Wert der huntAfterNoAnswer-Eigenschaft ab.
     * 
     */
    public boolean isHuntAfterNoAnswer() {
        return huntAfterNoAnswer;
    }

    /**
     * Legt den Wert der huntAfterNoAnswer-Eigenschaft fest.
     * 
     */
    public void setHuntAfterNoAnswer(boolean value) {
        this.huntAfterNoAnswer = value;
    }

    /**
     * Ruft den Wert der noAnswerNumberOfRings-Eigenschaft ab.
     * 
     */
    public int getNoAnswerNumberOfRings() {
        return noAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der noAnswerNumberOfRings-Eigenschaft fest.
     * 
     */
    public void setNoAnswerNumberOfRings(int value) {
        this.noAnswerNumberOfRings = value;
    }

    /**
     * Ruft den Wert der forwardAfterTimeout-Eigenschaft ab.
     * 
     */
    public boolean isForwardAfterTimeout() {
        return forwardAfterTimeout;
    }

    /**
     * Legt den Wert der forwardAfterTimeout-Eigenschaft fest.
     * 
     */
    public void setForwardAfterTimeout(boolean value) {
        this.forwardAfterTimeout = value;
    }

    /**
     * Ruft den Wert der forwardTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getForwardTimeoutSeconds() {
        return forwardTimeoutSeconds;
    }

    /**
     * Legt den Wert der forwardTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setForwardTimeoutSeconds(int value) {
        this.forwardTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der forwardToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForwardToPhoneNumber() {
        return forwardToPhoneNumber;
    }

    /**
     * Legt den Wert der forwardToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForwardToPhoneNumber(String value) {
        this.forwardToPhoneNumber = value;
    }

    /**
     * Gets the value of the agentUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the agentUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgentUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the agentUserId property.
     */
    public List<String> getAgentUserId() {
        if (agentUserId == null) {
            agentUserId = new ArrayList<>();
        }
        return this.agentUserId;
    }

    /**
     * Gets the value of the agentWeight property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the agentWeight property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgentWeight().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HuntAgentWeight }
     * 
     * 
     * @return
     *     The value of the agentWeight property.
     */
    public List<HuntAgentWeight> getAgentWeight() {
        if (agentWeight == null) {
            agentWeight = new ArrayList<>();
        }
        return this.agentWeight;
    }

    /**
     * Ruft den Wert der allowCallWaitingForAgents-Eigenschaft ab.
     * 
     */
    public boolean isAllowCallWaitingForAgents() {
        return allowCallWaitingForAgents;
    }

    /**
     * Legt den Wert der allowCallWaitingForAgents-Eigenschaft fest.
     * 
     */
    public void setAllowCallWaitingForAgents(boolean value) {
        this.allowCallWaitingForAgents = value;
    }

    /**
     * Ruft den Wert der useSystemHuntGroupCLIDSetting-Eigenschaft ab.
     * 
     */
    public boolean isUseSystemHuntGroupCLIDSetting() {
        return useSystemHuntGroupCLIDSetting;
    }

    /**
     * Legt den Wert der useSystemHuntGroupCLIDSetting-Eigenschaft fest.
     * 
     */
    public void setUseSystemHuntGroupCLIDSetting(boolean value) {
        this.useSystemHuntGroupCLIDSetting = value;
    }

    /**
     * Ruft den Wert der includeHuntGroupNameInCLID-Eigenschaft ab.
     * 
     */
    public boolean isIncludeHuntGroupNameInCLID() {
        return includeHuntGroupNameInCLID;
    }

    /**
     * Legt den Wert der includeHuntGroupNameInCLID-Eigenschaft fest.
     * 
     */
    public void setIncludeHuntGroupNameInCLID(boolean value) {
        this.includeHuntGroupNameInCLID = value;
    }

    /**
     * Ruft den Wert der enableNotReachableForwarding-Eigenschaft ab.
     * 
     */
    public boolean isEnableNotReachableForwarding() {
        return enableNotReachableForwarding;
    }

    /**
     * Legt den Wert der enableNotReachableForwarding-Eigenschaft fest.
     * 
     */
    public void setEnableNotReachableForwarding(boolean value) {
        this.enableNotReachableForwarding = value;
    }

    /**
     * Ruft den Wert der notReachableForwardToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotReachableForwardToPhoneNumber() {
        return notReachableForwardToPhoneNumber;
    }

    /**
     * Legt den Wert der notReachableForwardToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotReachableForwardToPhoneNumber(String value) {
        this.notReachableForwardToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der makeBusyWhenNotReachable-Eigenschaft ab.
     * 
     */
    public boolean isMakeBusyWhenNotReachable() {
        return makeBusyWhenNotReachable;
    }

    /**
     * Legt den Wert der makeBusyWhenNotReachable-Eigenschaft fest.
     * 
     */
    public void setMakeBusyWhenNotReachable(boolean value) {
        this.makeBusyWhenNotReachable = value;
    }

    /**
     * Ruft den Wert der allowMembersToControlGroupBusy-Eigenschaft ab.
     * 
     */
    public boolean isAllowMembersToControlGroupBusy() {
        return allowMembersToControlGroupBusy;
    }

    /**
     * Legt den Wert der allowMembersToControlGroupBusy-Eigenschaft fest.
     * 
     */
    public void setAllowMembersToControlGroupBusy(boolean value) {
        this.allowMembersToControlGroupBusy = value;
    }

    /**
     * Ruft den Wert der enableGroupBusy-Eigenschaft ab.
     * 
     */
    public boolean isEnableGroupBusy() {
        return enableGroupBusy;
    }

    /**
     * Legt den Wert der enableGroupBusy-Eigenschaft fest.
     * 
     */
    public void setEnableGroupBusy(boolean value) {
        this.enableGroupBusy = value;
    }

    /**
     * Ruft den Wert der applyGroupBusyWhenTerminatingToAgent-Eigenschaft ab.
     * 
     */
    public boolean isApplyGroupBusyWhenTerminatingToAgent() {
        return applyGroupBusyWhenTerminatingToAgent;
    }

    /**
     * Legt den Wert der applyGroupBusyWhenTerminatingToAgent-Eigenschaft fest.
     * 
     */
    public void setApplyGroupBusyWhenTerminatingToAgent(boolean value) {
        this.applyGroupBusyWhenTerminatingToAgent = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Gets the value of the service property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the service property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConsolidatedUserServiceAssignment }
     * 
     * 
     * @return
     *     The value of the service property.
     */
    public List<ConsolidatedUserServiceAssignment> getService() {
        if (service == null) {
            service = new ArrayList<>();
        }
        return this.service;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the directoryNumberHuntingAgentUserIdList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the directoryNumberHuntingAgentUserIdList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDirectoryNumberHuntingAgentUserIdList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the directoryNumberHuntingAgentUserIdList property.
     */
    public List<String> getDirectoryNumberHuntingAgentUserIdList() {
        if (directoryNumberHuntingAgentUserIdList == null) {
            directoryNumberHuntingAgentUserIdList = new ArrayList<>();
        }
        return this.directoryNumberHuntingAgentUserIdList;
    }

    /**
     * Ruft den Wert der directoryNumberHuntingUseTerminateCallToAgentFirst-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDirectoryNumberHuntingUseTerminateCallToAgentFirst() {
        return directoryNumberHuntingUseTerminateCallToAgentFirst;
    }

    /**
     * Legt den Wert der directoryNumberHuntingUseTerminateCallToAgentFirst-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDirectoryNumberHuntingUseTerminateCallToAgentFirst(Boolean value) {
        this.directoryNumberHuntingUseTerminateCallToAgentFirst = value;
    }

    /**
     * Ruft den Wert der directoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDirectoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls() {
        return directoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls;
    }

    /**
     * Legt den Wert der directoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDirectoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls(Boolean value) {
        this.directoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls = value;
    }

}
