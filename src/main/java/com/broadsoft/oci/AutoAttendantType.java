//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AutoAttendantType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AutoAttendantType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Basic"/>
 *     <enumeration value="Standard"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AutoAttendantType")
@XmlEnum
public enum AutoAttendantType {

    @XmlEnumValue("Basic")
    BASIC("Basic"),
    @XmlEnumValue("Standard")
    STANDARD("Standard");
    private final String value;

    AutoAttendantType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AutoAttendantType fromValue(String v) {
        for (AutoAttendantType c: AutoAttendantType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
