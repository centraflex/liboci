//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *          Request to modify Call External Emergency Routing system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemExternalEmergencyRoutingParametersModifyRequest13mp13 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemExternalEmergencyRoutingParametersModifyRequest13mp13">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceURI" type="{}NetAddress" minOccurs="0"/>
 *         <element name="defaultEmergencyNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="supportsDNSSRV" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="connectionTimeoutSeconds" type="{}ExternalEmergencyRoutingConnectionTimeoutSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemExternalEmergencyRoutingParametersModifyRequest13mp13", propOrder = {
    "serviceURI",
    "defaultEmergencyNumber",
    "isActive",
    "supportsDNSSRV",
    "connectionTimeoutSeconds"
})
public class SystemExternalEmergencyRoutingParametersModifyRequest13Mp13
    extends OCIRequest
{

    @XmlElementRef(name = "serviceURI", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serviceURI;
    @XmlElementRef(name = "defaultEmergencyNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultEmergencyNumber;
    protected Boolean isActive;
    protected Boolean supportsDNSSRV;
    protected Integer connectionTimeoutSeconds;

    /**
     * Ruft den Wert der serviceURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServiceURI() {
        return serviceURI;
    }

    /**
     * Legt den Wert der serviceURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServiceURI(JAXBElement<String> value) {
        this.serviceURI = value;
    }

    /**
     * Ruft den Wert der defaultEmergencyNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultEmergencyNumber() {
        return defaultEmergencyNumber;
    }

    /**
     * Legt den Wert der defaultEmergencyNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultEmergencyNumber(JAXBElement<String> value) {
        this.defaultEmergencyNumber = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der supportsDNSSRV-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportsDNSSRV() {
        return supportsDNSSRV;
    }

    /**
     * Legt den Wert der supportsDNSSRV-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportsDNSSRV(Boolean value) {
        this.supportsDNSSRV = value;
    }

    /**
     * Ruft den Wert der connectionTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getConnectionTimeoutSeconds() {
        return connectionTimeoutSeconds;
    }

    /**
     * Legt den Wert der connectionTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setConnectionTimeoutSeconds(Integer value) {
        this.connectionTimeoutSeconds = value;
    }

}
