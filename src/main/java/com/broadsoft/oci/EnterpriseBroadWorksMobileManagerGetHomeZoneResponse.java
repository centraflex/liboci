//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the EnterpriseBroadWorksMobileManagerGetHomeZoneRequest
 *       
 * 
 * <p>Java-Klasse für EnterpriseBroadWorksMobileManagerGetHomeZoneResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseBroadWorksMobileManagerGetHomeZoneResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="homeZoneDomainName" type="{}BroadWorksMobileManagerDomainName"/>
 *         <element name="mobileCountryCode" type="{}BroadWorksMobileManagerHomeZoneMobileCountryCode"/>
 *         <element name="mobileNetworkCode" type="{}BroadWorksMobileManagerHomeZoneNetworkCountryCode"/>
 *         <element name="locationAreaCode" type="{}BroadWorksMobileManagerHomeZoneLocationAreaCode"/>
 *         <element name="cellIdentity" type="{}BroadWorksMobileManagerHomeZoneCellId"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseBroadWorksMobileManagerGetHomeZoneResponse", propOrder = {
    "homeZoneDomainName",
    "mobileCountryCode",
    "mobileNetworkCode",
    "locationAreaCode",
    "cellIdentity"
})
public class EnterpriseBroadWorksMobileManagerGetHomeZoneResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String homeZoneDomainName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mobileCountryCode;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mobileNetworkCode;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String locationAreaCode;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String cellIdentity;

    /**
     * Ruft den Wert der homeZoneDomainName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHomeZoneDomainName() {
        return homeZoneDomainName;
    }

    /**
     * Legt den Wert der homeZoneDomainName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHomeZoneDomainName(String value) {
        this.homeZoneDomainName = value;
    }

    /**
     * Ruft den Wert der mobileCountryCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobileCountryCode() {
        return mobileCountryCode;
    }

    /**
     * Legt den Wert der mobileCountryCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobileCountryCode(String value) {
        this.mobileCountryCode = value;
    }

    /**
     * Ruft den Wert der mobileNetworkCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobileNetworkCode() {
        return mobileNetworkCode;
    }

    /**
     * Legt den Wert der mobileNetworkCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobileNetworkCode(String value) {
        this.mobileNetworkCode = value;
    }

    /**
     * Ruft den Wert der locationAreaCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationAreaCode() {
        return locationAreaCode;
    }

    /**
     * Legt den Wert der locationAreaCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationAreaCode(String value) {
        this.locationAreaCode = value;
    }

    /**
     * Ruft den Wert der cellIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCellIdentity() {
        return cellIdentity;
    }

    /**
     * Legt den Wert der cellIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCellIdentity(String value) {
        this.cellIdentity = value;
    }

}
