//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the group voice portal information for a voice messaging group.
 *         The following elements are only used in AS data mode:
 *           networkClassOfService
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *          expressMode         
 *           
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupVoiceMessagingGroupModifyVoicePortalRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupVoiceMessagingGroupModifyVoicePortalRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceModifyProfile" minOccurs="0"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableExtendedScope" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useVoicePortalWizard" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="voicePortalExternalRoutingScope" type="{}VoicePortalExternalRoutingScope" minOccurs="0"/>
 *         <element name="useExternalRouting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="externalRoutingAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="homeZoneName" type="{}ZoneName" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="expressMode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupVoiceMessagingGroupModifyVoicePortalRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "serviceInstanceProfile",
    "isActive",
    "enableExtendedScope",
    "allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin",
    "useVoicePortalWizard",
    "voicePortalExternalRoutingScope",
    "useExternalRouting",
    "externalRoutingAddress",
    "homeZoneName",
    "networkClassOfService",
    "expressMode"
})
public class GroupVoiceMessagingGroupModifyVoicePortalRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected ServiceInstanceModifyProfile serviceInstanceProfile;
    protected Boolean isActive;
    protected Boolean enableExtendedScope;
    protected Boolean allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin;
    protected Boolean useVoicePortalWizard;
    @XmlSchemaType(name = "token")
    protected VoicePortalExternalRoutingScope voicePortalExternalRoutingScope;
    protected Boolean useExternalRouting;
    @XmlElementRef(name = "externalRoutingAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externalRoutingAddress;
    @XmlElementRef(name = "homeZoneName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> homeZoneName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    protected Boolean expressMode;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public ServiceInstanceModifyProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceModifyProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der enableExtendedScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableExtendedScope() {
        return enableExtendedScope;
    }

    /**
     * Legt den Wert der enableExtendedScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableExtendedScope(Boolean value) {
        this.enableExtendedScope = value;
    }

    /**
     * Ruft den Wert der allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin() {
        return allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin;
    }

    /**
     * Legt den Wert der allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin(Boolean value) {
        this.allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin = value;
    }

    /**
     * Ruft den Wert der useVoicePortalWizard-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseVoicePortalWizard() {
        return useVoicePortalWizard;
    }

    /**
     * Legt den Wert der useVoicePortalWizard-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseVoicePortalWizard(Boolean value) {
        this.useVoicePortalWizard = value;
    }

    /**
     * Ruft den Wert der voicePortalExternalRoutingScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoicePortalExternalRoutingScope }
     *     
     */
    public VoicePortalExternalRoutingScope getVoicePortalExternalRoutingScope() {
        return voicePortalExternalRoutingScope;
    }

    /**
     * Legt den Wert der voicePortalExternalRoutingScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoicePortalExternalRoutingScope }
     *     
     */
    public void setVoicePortalExternalRoutingScope(VoicePortalExternalRoutingScope value) {
        this.voicePortalExternalRoutingScope = value;
    }

    /**
     * Ruft den Wert der useExternalRouting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseExternalRouting() {
        return useExternalRouting;
    }

    /**
     * Legt den Wert der useExternalRouting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseExternalRouting(Boolean value) {
        this.useExternalRouting = value;
    }

    /**
     * Ruft den Wert der externalRoutingAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternalRoutingAddress() {
        return externalRoutingAddress;
    }

    /**
     * Legt den Wert der externalRoutingAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternalRoutingAddress(JAXBElement<String> value) {
        this.externalRoutingAddress = value;
    }

    /**
     * Ruft den Wert der homeZoneName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHomeZoneName() {
        return homeZoneName;
    }

    /**
     * Legt den Wert der homeZoneName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHomeZoneName(JAXBElement<String> value) {
        this.homeZoneName = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Ruft den Wert der expressMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExpressMode() {
        return expressMode;
    }

    /**
     * Legt den Wert der expressMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExpressMode(Boolean value) {
        this.expressMode = value;
    }

}
