//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the system level data associated with the SMPP external interface.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemSMPPModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSMPPModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="primarySMPPServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="primarySMPPPort" type="{}Port" minOccurs="0"/>
 *         <element name="secondarySMPPServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="secondarySMPPPort" type="{}Port" minOccurs="0"/>
 *         <element name="systemId" type="{}SMPPSystemId" minOccurs="0"/>
 *         <element name="password" type="{}SMPPPassword" minOccurs="0"/>
 *         <element name="version" type="{}SMPPVersion" minOccurs="0"/>
 *         <element name="systemType" type="{}SMPPSystemType" minOccurs="0"/>
 *         <element name="enableMWICustomizedMessage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="supportMessagePayload" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxShortMessageLength" type="{}SMPPMaxShortMessageLength" minOccurs="0"/>
 *         <element name="useGsmMwiUcs2Encoding" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeOnlyNewMessageCount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSMPPModifyRequest", propOrder = {
    "primarySMPPServerNetAddress",
    "primarySMPPPort",
    "secondarySMPPServerNetAddress",
    "secondarySMPPPort",
    "systemId",
    "password",
    "version",
    "systemType",
    "enableMWICustomizedMessage",
    "supportMessagePayload",
    "maxShortMessageLength",
    "useGsmMwiUcs2Encoding",
    "includeOnlyNewMessageCount"
})
public class SystemSMPPModifyRequest
    extends OCIRequest
{

    @XmlElementRef(name = "primarySMPPServerNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> primarySMPPServerNetAddress;
    protected Integer primarySMPPPort;
    @XmlElementRef(name = "secondarySMPPServerNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> secondarySMPPServerNetAddress;
    protected Integer secondarySMPPPort;
    @XmlElementRef(name = "systemId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> systemId;
    @XmlElementRef(name = "password", type = JAXBElement.class, required = false)
    protected JAXBElement<String> password;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String version;
    @XmlElementRef(name = "systemType", type = JAXBElement.class, required = false)
    protected JAXBElement<String> systemType;
    protected Boolean enableMWICustomizedMessage;
    protected Boolean supportMessagePayload;
    protected Integer maxShortMessageLength;
    protected Boolean useGsmMwiUcs2Encoding;
    protected Boolean includeOnlyNewMessageCount;

    /**
     * Ruft den Wert der primarySMPPServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrimarySMPPServerNetAddress() {
        return primarySMPPServerNetAddress;
    }

    /**
     * Legt den Wert der primarySMPPServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrimarySMPPServerNetAddress(JAXBElement<String> value) {
        this.primarySMPPServerNetAddress = value;
    }

    /**
     * Ruft den Wert der primarySMPPPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPrimarySMPPPort() {
        return primarySMPPPort;
    }

    /**
     * Legt den Wert der primarySMPPPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPrimarySMPPPort(Integer value) {
        this.primarySMPPPort = value;
    }

    /**
     * Ruft den Wert der secondarySMPPServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSecondarySMPPServerNetAddress() {
        return secondarySMPPServerNetAddress;
    }

    /**
     * Legt den Wert der secondarySMPPServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSecondarySMPPServerNetAddress(JAXBElement<String> value) {
        this.secondarySMPPServerNetAddress = value;
    }

    /**
     * Ruft den Wert der secondarySMPPPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSecondarySMPPPort() {
        return secondarySMPPPort;
    }

    /**
     * Legt den Wert der secondarySMPPPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSecondarySMPPPort(Integer value) {
        this.secondarySMPPPort = value;
    }

    /**
     * Ruft den Wert der systemId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSystemId() {
        return systemId;
    }

    /**
     * Legt den Wert der systemId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSystemId(JAXBElement<String> value) {
        this.systemId = value;
    }

    /**
     * Ruft den Wert der password-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPassword() {
        return password;
    }

    /**
     * Legt den Wert der password-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPassword(JAXBElement<String> value) {
        this.password = value;
    }

    /**
     * Ruft den Wert der version-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Legt den Wert der version-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Ruft den Wert der systemType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSystemType() {
        return systemType;
    }

    /**
     * Legt den Wert der systemType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSystemType(JAXBElement<String> value) {
        this.systemType = value;
    }

    /**
     * Ruft den Wert der enableMWICustomizedMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMWICustomizedMessage() {
        return enableMWICustomizedMessage;
    }

    /**
     * Legt den Wert der enableMWICustomizedMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMWICustomizedMessage(Boolean value) {
        this.enableMWICustomizedMessage = value;
    }

    /**
     * Ruft den Wert der supportMessagePayload-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportMessagePayload() {
        return supportMessagePayload;
    }

    /**
     * Legt den Wert der supportMessagePayload-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportMessagePayload(Boolean value) {
        this.supportMessagePayload = value;
    }

    /**
     * Ruft den Wert der maxShortMessageLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxShortMessageLength() {
        return maxShortMessageLength;
    }

    /**
     * Legt den Wert der maxShortMessageLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxShortMessageLength(Integer value) {
        this.maxShortMessageLength = value;
    }

    /**
     * Ruft den Wert der useGsmMwiUcs2Encoding-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseGsmMwiUcs2Encoding() {
        return useGsmMwiUcs2Encoding;
    }

    /**
     * Legt den Wert der useGsmMwiUcs2Encoding-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseGsmMwiUcs2Encoding(Boolean value) {
        this.useGsmMwiUcs2Encoding = value;
    }

    /**
     * Ruft den Wert der includeOnlyNewMessageCount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeOnlyNewMessageCount() {
        return includeOnlyNewMessageCount;
    }

    /**
     * Legt den Wert der includeOnlyNewMessageCount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeOnlyNewMessageCount(Boolean value) {
        this.includeOnlyNewMessageCount = value;
    }

}
