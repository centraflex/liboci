//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the Call Recording attributes for a user. The recording options 
 *         "Always with Pause/Resume", "On Demand", and "On Demand with User 
 *         Initiated Start" are not accepted for service instances.
 * 
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         The following parameters are not accepted for service
 *         instance:
 *           - pauseResumeNotification
 *           - enableCallRecordingAnnouncement
 *           - enableRecordCallRepeatWarningTone
 *           - recordCallRepeatWarningToneTimerSeconds
 *           
 *         The enableVoiceMailRecording parameter is only applicable 
 *         if the Voice Messaging User service is assigned to the user.
 *         This applies to both users and service instances.
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           mediaStream  
 *       
 * 
 * <p>Java-Klasse für UserCallRecordingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallRecordingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="recordingOption" type="{}RecordingOption20" minOccurs="0"/>
 *         <element name="pauseResumeNotification" type="{}RecordingPauseResumeNotificationType" minOccurs="0"/>
 *         <element name="enableCallRecordingAnnouncement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableRecordCallRepeatWarningTone" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="recordCallRepeatWarningToneTimerSeconds" type="{}CallRecordingRecordCallRepeatWarningToneTimerSeconds" minOccurs="0"/>
 *         <element name="enableVoiceMailRecording" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="mediaStream" type="{}MediaStream" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallRecordingModifyRequest", propOrder = {
    "userId",
    "recordingOption",
    "pauseResumeNotification",
    "enableCallRecordingAnnouncement",
    "enableRecordCallRepeatWarningTone",
    "recordCallRepeatWarningToneTimerSeconds",
    "enableVoiceMailRecording",
    "mediaStream"
})
public class UserCallRecordingModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlSchemaType(name = "token")
    protected RecordingOption20 recordingOption;
    @XmlSchemaType(name = "token")
    protected RecordingPauseResumeNotificationType pauseResumeNotification;
    protected Boolean enableCallRecordingAnnouncement;
    protected Boolean enableRecordCallRepeatWarningTone;
    protected Integer recordCallRepeatWarningToneTimerSeconds;
    protected Boolean enableVoiceMailRecording;
    @XmlSchemaType(name = "token")
    protected MediaStream mediaStream;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der recordingOption-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RecordingOption20 }
     *     
     */
    public RecordingOption20 getRecordingOption() {
        return recordingOption;
    }

    /**
     * Legt den Wert der recordingOption-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RecordingOption20 }
     *     
     */
    public void setRecordingOption(RecordingOption20 value) {
        this.recordingOption = value;
    }

    /**
     * Ruft den Wert der pauseResumeNotification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RecordingPauseResumeNotificationType }
     *     
     */
    public RecordingPauseResumeNotificationType getPauseResumeNotification() {
        return pauseResumeNotification;
    }

    /**
     * Legt den Wert der pauseResumeNotification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RecordingPauseResumeNotificationType }
     *     
     */
    public void setPauseResumeNotification(RecordingPauseResumeNotificationType value) {
        this.pauseResumeNotification = value;
    }

    /**
     * Ruft den Wert der enableCallRecordingAnnouncement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCallRecordingAnnouncement() {
        return enableCallRecordingAnnouncement;
    }

    /**
     * Legt den Wert der enableCallRecordingAnnouncement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCallRecordingAnnouncement(Boolean value) {
        this.enableCallRecordingAnnouncement = value;
    }

    /**
     * Ruft den Wert der enableRecordCallRepeatWarningTone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableRecordCallRepeatWarningTone() {
        return enableRecordCallRepeatWarningTone;
    }

    /**
     * Legt den Wert der enableRecordCallRepeatWarningTone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableRecordCallRepeatWarningTone(Boolean value) {
        this.enableRecordCallRepeatWarningTone = value;
    }

    /**
     * Ruft den Wert der recordCallRepeatWarningToneTimerSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRecordCallRepeatWarningToneTimerSeconds() {
        return recordCallRepeatWarningToneTimerSeconds;
    }

    /**
     * Legt den Wert der recordCallRepeatWarningToneTimerSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRecordCallRepeatWarningToneTimerSeconds(Integer value) {
        this.recordCallRepeatWarningToneTimerSeconds = value;
    }

    /**
     * Ruft den Wert der enableVoiceMailRecording-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableVoiceMailRecording() {
        return enableVoiceMailRecording;
    }

    /**
     * Legt den Wert der enableVoiceMailRecording-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableVoiceMailRecording(Boolean value) {
        this.enableVoiceMailRecording = value;
    }

    /**
     * Ruft den Wert der mediaStream-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MediaStream }
     *     
     */
    public MediaStream getMediaStream() {
        return mediaStream;
    }

    /**
     * Legt den Wert der mediaStream-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MediaStream }
     *     
     */
    public void setMediaStream(MediaStream value) {
        this.mediaStream = value;
    }

}
