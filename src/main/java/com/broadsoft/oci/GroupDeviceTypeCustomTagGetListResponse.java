//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupDeviceTypeCustomTagGetListRequest.
 *         Contains a table of custom configuration tags managed by the Device Management System on a per-device type basis for a group.
 *         In As data mode, the column headings are:
 *           "Tag Name", "Tag Value", "Actual Tag Value".
 *         In XS data mode, the column headings are:
 *           "Tag Name", "Tag Value", "Actual Tag Value" if request is invoked by an admin without system privileges.
 *           "Tag Name", "Tag Value", "Is Encrypted", "Actual Tag Value" if request is invoked by an admin with system privileges.
 *       
 * 
 * <p>Java-Klasse für GroupDeviceTypeCustomTagGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDeviceTypeCustomTagGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="groupDeviceTypeCustomTagsTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDeviceTypeCustomTagGetListResponse", propOrder = {
    "groupDeviceTypeCustomTagsTable"
})
public class GroupDeviceTypeCustomTagGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable groupDeviceTypeCustomTagsTable;

    /**
     * Ruft den Wert der groupDeviceTypeCustomTagsTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getGroupDeviceTypeCustomTagsTable() {
        return groupDeviceTypeCustomTagsTable;
    }

    /**
     * Legt den Wert der groupDeviceTypeCustomTagsTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setGroupDeviceTypeCustomTagsTable(OCITable value) {
        this.groupDeviceTypeCustomTagsTable = value;
    }

}
