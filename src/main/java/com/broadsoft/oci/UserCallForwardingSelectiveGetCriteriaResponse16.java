//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the UserCallForwardingSelectiveGetCriteriaRequest16.
 *         Replaced by: UserCallForwardingSelectiveGetCriteriaResponse21 in AS data mode
 *       
 * 
 * <p>Java-Klasse für UserCallForwardingSelectiveGetCriteriaResponse16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallForwardingSelectiveGetCriteriaResponse16">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="timeSchedule" type="{}TimeSchedule" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}HolidaySchedule" minOccurs="0"/>
 *         <element name="forwardToNumberSelection" type="{}CallForwardingSelectiveNumberSelection16"/>
 *         <element name="forwardToPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="fromDnCriteria" type="{}CriteriaFromDn"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallForwardingSelectiveGetCriteriaResponse16", propOrder = {
    "timeSchedule",
    "holidaySchedule",
    "forwardToNumberSelection",
    "forwardToPhoneNumber",
    "fromDnCriteria"
})
public class UserCallForwardingSelectiveGetCriteriaResponse16
    extends OCIDataResponse
{

    protected TimeSchedule timeSchedule;
    protected HolidaySchedule holidaySchedule;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallForwardingSelectiveNumberSelection16 forwardToNumberSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String forwardToPhoneNumber;
    @XmlElement(required = true)
    protected CriteriaFromDn fromDnCriteria;

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeSchedule }
     *     
     */
    public TimeSchedule getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeSchedule }
     *     
     */
    public void setTimeSchedule(TimeSchedule value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HolidaySchedule }
     *     
     */
    public HolidaySchedule getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HolidaySchedule }
     *     
     */
    public void setHolidaySchedule(HolidaySchedule value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der forwardToNumberSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallForwardingSelectiveNumberSelection16 }
     *     
     */
    public CallForwardingSelectiveNumberSelection16 getForwardToNumberSelection() {
        return forwardToNumberSelection;
    }

    /**
     * Legt den Wert der forwardToNumberSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallForwardingSelectiveNumberSelection16 }
     *     
     */
    public void setForwardToNumberSelection(CallForwardingSelectiveNumberSelection16 value) {
        this.forwardToNumberSelection = value;
    }

    /**
     * Ruft den Wert der forwardToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForwardToPhoneNumber() {
        return forwardToPhoneNumber;
    }

    /**
     * Legt den Wert der forwardToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForwardToPhoneNumber(String value) {
        this.forwardToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der fromDnCriteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaFromDn }
     *     
     */
    public CriteriaFromDn getFromDnCriteria() {
        return fromDnCriteria;
    }

    /**
     * Legt den Wert der fromDnCriteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaFromDn }
     *     
     */
    public void setFromDnCriteria(CriteriaFromDn value) {
        this.fromDnCriteria = value;
    }

}
