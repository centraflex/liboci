//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserPushNotificationGetRequest24.
 *       
 * 
 * <p>Java-Klasse für UserPushNotificationGetResponse24 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPushNotificationGetResponse24">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="sendPushNotificationForClickToDial" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sendPushNotificationForGroupPaging" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPushNotificationGetResponse24", propOrder = {
    "sendPushNotificationForClickToDial",
    "sendPushNotificationForGroupPaging"
})
public class UserPushNotificationGetResponse24
    extends OCIDataResponse
{

    protected boolean sendPushNotificationForClickToDial;
    protected boolean sendPushNotificationForGroupPaging;

    /**
     * Ruft den Wert der sendPushNotificationForClickToDial-Eigenschaft ab.
     * 
     */
    public boolean isSendPushNotificationForClickToDial() {
        return sendPushNotificationForClickToDial;
    }

    /**
     * Legt den Wert der sendPushNotificationForClickToDial-Eigenschaft fest.
     * 
     */
    public void setSendPushNotificationForClickToDial(boolean value) {
        this.sendPushNotificationForClickToDial = value;
    }

    /**
     * Ruft den Wert der sendPushNotificationForGroupPaging-Eigenschaft ab.
     * 
     */
    public boolean isSendPushNotificationForGroupPaging() {
        return sendPushNotificationForGroupPaging;
    }

    /**
     * Legt den Wert der sendPushNotificationForGroupPaging-Eigenschaft fest.
     * 
     */
    public void setSendPushNotificationForGroupPaging(boolean value) {
        this.sendPushNotificationForGroupPaging = value;
    }

}
