//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         User's authentication service information.
 *       
 * 
 * <p>Java-Klasse für SIPAuthenticationUserNamePassword complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SIPAuthenticationUserNamePassword">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="sipAuthenticationUserName" type="{}SIPAuthenticationUserName"/>
 *         <element name="sipAuthenticationPassword" type="{}Password"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SIPAuthenticationUserNamePassword", propOrder = {
    "sipAuthenticationUserName",
    "sipAuthenticationPassword"
})
public class SIPAuthenticationUserNamePassword {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sipAuthenticationUserName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sipAuthenticationPassword;

    /**
     * Ruft den Wert der sipAuthenticationUserName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSipAuthenticationUserName() {
        return sipAuthenticationUserName;
    }

    /**
     * Legt den Wert der sipAuthenticationUserName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSipAuthenticationUserName(String value) {
        this.sipAuthenticationUserName = value;
    }

    /**
     * Ruft den Wert der sipAuthenticationPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSipAuthenticationPassword() {
        return sipAuthenticationPassword;
    }

    /**
     * Legt den Wert der sipAuthenticationPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSipAuthenticationPassword(String value) {
        this.sipAuthenticationPassword = value;
    }

}
