//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für HoldNormalizationMode.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="HoldNormalizationMode">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Unspecified Address"/>
 *     <enumeration value="Inactive"/>
 *     <enumeration value="Rfc3264"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "HoldNormalizationMode")
@XmlEnum
public enum HoldNormalizationMode {

    @XmlEnumValue("Unspecified Address")
    UNSPECIFIED_ADDRESS("Unspecified Address"),
    @XmlEnumValue("Inactive")
    INACTIVE("Inactive"),
    @XmlEnumValue("Rfc3264")
    RFC_3264("Rfc3264");
    private final String value;

    HoldNormalizationMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HoldNormalizationMode fromValue(String v) {
        for (HoldNormalizationMode c: HoldNormalizationMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
