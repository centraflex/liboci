//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AutomaticCallbackMethod.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AutomaticCallbackMethod">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Notify Only"/>
 *     <enumeration value="Notify If Possible And Polling Otherwise"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AutomaticCallbackMethod")
@XmlEnum
public enum AutomaticCallbackMethod {

    @XmlEnumValue("Notify Only")
    NOTIFY_ONLY("Notify Only"),
    @XmlEnumValue("Notify If Possible And Polling Otherwise")
    NOTIFY_IF_POSSIBLE_AND_POLLING_OTHERWISE("Notify If Possible And Polling Otherwise");
    private final String value;

    AutomaticCallbackMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AutomaticCallbackMethod fromValue(String v) {
        for (AutomaticCallbackMethod c: AutomaticCallbackMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
