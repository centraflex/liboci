//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Assign a list of group and user Xsi policy profile to a service provider.
 *         At service provider level, only one service provider Xsi policy profile can be assigned to service provider.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderXsiPolicyProfileAssignListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderXsiPolicyProfileAssignListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="spXsiPolicyProfile" type="{}XsiPolicyProfileName" minOccurs="0"/>
 *         <element name="groupXsiPolicyProfile" type="{}XsiPolicyProfileAssignEntry" minOccurs="0"/>
 *         <element name="userXsiPolicyProfile" type="{}XsiPolicyProfileAssignEntry" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderXsiPolicyProfileAssignListRequest", propOrder = {
    "serviceProviderId",
    "spXsiPolicyProfile",
    "groupXsiPolicyProfile",
    "userXsiPolicyProfile"
})
public class ServiceProviderXsiPolicyProfileAssignListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String spXsiPolicyProfile;
    protected XsiPolicyProfileAssignEntry groupXsiPolicyProfile;
    protected XsiPolicyProfileAssignEntry userXsiPolicyProfile;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der spXsiPolicyProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpXsiPolicyProfile() {
        return spXsiPolicyProfile;
    }

    /**
     * Legt den Wert der spXsiPolicyProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpXsiPolicyProfile(String value) {
        this.spXsiPolicyProfile = value;
    }

    /**
     * Ruft den Wert der groupXsiPolicyProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XsiPolicyProfileAssignEntry }
     *     
     */
    public XsiPolicyProfileAssignEntry getGroupXsiPolicyProfile() {
        return groupXsiPolicyProfile;
    }

    /**
     * Legt den Wert der groupXsiPolicyProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XsiPolicyProfileAssignEntry }
     *     
     */
    public void setGroupXsiPolicyProfile(XsiPolicyProfileAssignEntry value) {
        this.groupXsiPolicyProfile = value;
    }

    /**
     * Ruft den Wert der userXsiPolicyProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XsiPolicyProfileAssignEntry }
     *     
     */
    public XsiPolicyProfileAssignEntry getUserXsiPolicyProfile() {
        return userXsiPolicyProfile;
    }

    /**
     * Legt den Wert der userXsiPolicyProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XsiPolicyProfileAssignEntry }
     *     
     */
    public void setUserXsiPolicyProfile(XsiPolicyProfileAssignEntry value) {
        this.userXsiPolicyProfile = value;
    }

}
