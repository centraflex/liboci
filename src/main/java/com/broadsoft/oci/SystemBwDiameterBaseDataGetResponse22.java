//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemBwDiameterBaseDataGetRequest22.
 *         Contains a list of System Diameter base parameters.
 *       
 * 
 * <p>Java-Klasse für SystemBwDiameterBaseDataGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBwDiameterBaseDataGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="xsRealm" type="{}DomainName" minOccurs="0"/>
 *         <element name="xsListeningPort" type="{}Port1025"/>
 *         <element name="xsListeningPortEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="xsListeningSecurePort" type="{}Port1025"/>
 *         <element name="xsListeningSecurePortEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="psRealm" type="{}DomainName" minOccurs="0"/>
 *         <element name="psListeningPort" type="{}Port1025"/>
 *         <element name="psListeningPortEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="psListeningSecurePort" type="{}Port1025"/>
 *         <element name="psListeningSecurePortEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="psRelayThroughXs" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="xsRelayListeningPort" type="{}Port1025"/>
 *         <element name="tcTimerSeconds" type="{}BwDiameterTcTimerSeconds"/>
 *         <element name="twTimerSeconds" type="{}BwDiameterTwTimerSeconds"/>
 *         <element name="requestTimerSeconds" type="{}BwDiameterRequestTimerSeconds"/>
 *         <element name="busyPeerDetectionOutstandingTxnCount" type="{}BwDiameterBusyPeerOutstandingTxnCount"/>
 *         <element name="busyPeerRestoreOutstandingTxnCount" type="{}BwDiameterBusyPeerOutstandingTxnCount"/>
 *         <element name="dynamicEntryInactivityTimerHours" type="{}BwDiameterDynamicEntryInactivityTimerHours"/>
 *         <element name="advertisedOfflineBillingApplication" type="{}BwDiameterAdvertisedApplication"/>
 *         <element name="advertisedOnlineBillingApplication" type="{}BwDiameterAdvertisedApplication"/>
 *         <element name="peerDiscoveryMode" type="{}DiameterPeerDiscoveryMode"/>
 *         <element name="defaultPort" type="{}Port1025"/>
 *         <element name="defaultSecurePort" type="{}Port1025"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBwDiameterBaseDataGetResponse22", propOrder = {
    "xsRealm",
    "xsListeningPort",
    "xsListeningPortEnabled",
    "xsListeningSecurePort",
    "xsListeningSecurePortEnabled",
    "psRealm",
    "psListeningPort",
    "psListeningPortEnabled",
    "psListeningSecurePort",
    "psListeningSecurePortEnabled",
    "psRelayThroughXs",
    "xsRelayListeningPort",
    "tcTimerSeconds",
    "twTimerSeconds",
    "requestTimerSeconds",
    "busyPeerDetectionOutstandingTxnCount",
    "busyPeerRestoreOutstandingTxnCount",
    "dynamicEntryInactivityTimerHours",
    "advertisedOfflineBillingApplication",
    "advertisedOnlineBillingApplication",
    "peerDiscoveryMode",
    "defaultPort",
    "defaultSecurePort"
})
public class SystemBwDiameterBaseDataGetResponse22
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String xsRealm;
    protected int xsListeningPort;
    protected boolean xsListeningPortEnabled;
    protected int xsListeningSecurePort;
    protected boolean xsListeningSecurePortEnabled;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String psRealm;
    protected int psListeningPort;
    protected boolean psListeningPortEnabled;
    protected int psListeningSecurePort;
    protected boolean psListeningSecurePortEnabled;
    protected boolean psRelayThroughXs;
    protected int xsRelayListeningPort;
    protected int tcTimerSeconds;
    protected int twTimerSeconds;
    protected int requestTimerSeconds;
    protected int busyPeerDetectionOutstandingTxnCount;
    protected int busyPeerRestoreOutstandingTxnCount;
    protected int dynamicEntryInactivityTimerHours;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String advertisedOfflineBillingApplication;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String advertisedOnlineBillingApplication;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DiameterPeerDiscoveryMode peerDiscoveryMode;
    protected int defaultPort;
    protected int defaultSecurePort;

    /**
     * Ruft den Wert der xsRealm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXsRealm() {
        return xsRealm;
    }

    /**
     * Legt den Wert der xsRealm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXsRealm(String value) {
        this.xsRealm = value;
    }

    /**
     * Ruft den Wert der xsListeningPort-Eigenschaft ab.
     * 
     */
    public int getXsListeningPort() {
        return xsListeningPort;
    }

    /**
     * Legt den Wert der xsListeningPort-Eigenschaft fest.
     * 
     */
    public void setXsListeningPort(int value) {
        this.xsListeningPort = value;
    }

    /**
     * Ruft den Wert der xsListeningPortEnabled-Eigenschaft ab.
     * 
     */
    public boolean isXsListeningPortEnabled() {
        return xsListeningPortEnabled;
    }

    /**
     * Legt den Wert der xsListeningPortEnabled-Eigenschaft fest.
     * 
     */
    public void setXsListeningPortEnabled(boolean value) {
        this.xsListeningPortEnabled = value;
    }

    /**
     * Ruft den Wert der xsListeningSecurePort-Eigenschaft ab.
     * 
     */
    public int getXsListeningSecurePort() {
        return xsListeningSecurePort;
    }

    /**
     * Legt den Wert der xsListeningSecurePort-Eigenschaft fest.
     * 
     */
    public void setXsListeningSecurePort(int value) {
        this.xsListeningSecurePort = value;
    }

    /**
     * Ruft den Wert der xsListeningSecurePortEnabled-Eigenschaft ab.
     * 
     */
    public boolean isXsListeningSecurePortEnabled() {
        return xsListeningSecurePortEnabled;
    }

    /**
     * Legt den Wert der xsListeningSecurePortEnabled-Eigenschaft fest.
     * 
     */
    public void setXsListeningSecurePortEnabled(boolean value) {
        this.xsListeningSecurePortEnabled = value;
    }

    /**
     * Ruft den Wert der psRealm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPsRealm() {
        return psRealm;
    }

    /**
     * Legt den Wert der psRealm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPsRealm(String value) {
        this.psRealm = value;
    }

    /**
     * Ruft den Wert der psListeningPort-Eigenschaft ab.
     * 
     */
    public int getPsListeningPort() {
        return psListeningPort;
    }

    /**
     * Legt den Wert der psListeningPort-Eigenschaft fest.
     * 
     */
    public void setPsListeningPort(int value) {
        this.psListeningPort = value;
    }

    /**
     * Ruft den Wert der psListeningPortEnabled-Eigenschaft ab.
     * 
     */
    public boolean isPsListeningPortEnabled() {
        return psListeningPortEnabled;
    }

    /**
     * Legt den Wert der psListeningPortEnabled-Eigenschaft fest.
     * 
     */
    public void setPsListeningPortEnabled(boolean value) {
        this.psListeningPortEnabled = value;
    }

    /**
     * Ruft den Wert der psListeningSecurePort-Eigenschaft ab.
     * 
     */
    public int getPsListeningSecurePort() {
        return psListeningSecurePort;
    }

    /**
     * Legt den Wert der psListeningSecurePort-Eigenschaft fest.
     * 
     */
    public void setPsListeningSecurePort(int value) {
        this.psListeningSecurePort = value;
    }

    /**
     * Ruft den Wert der psListeningSecurePortEnabled-Eigenschaft ab.
     * 
     */
    public boolean isPsListeningSecurePortEnabled() {
        return psListeningSecurePortEnabled;
    }

    /**
     * Legt den Wert der psListeningSecurePortEnabled-Eigenschaft fest.
     * 
     */
    public void setPsListeningSecurePortEnabled(boolean value) {
        this.psListeningSecurePortEnabled = value;
    }

    /**
     * Ruft den Wert der psRelayThroughXs-Eigenschaft ab.
     * 
     */
    public boolean isPsRelayThroughXs() {
        return psRelayThroughXs;
    }

    /**
     * Legt den Wert der psRelayThroughXs-Eigenschaft fest.
     * 
     */
    public void setPsRelayThroughXs(boolean value) {
        this.psRelayThroughXs = value;
    }

    /**
     * Ruft den Wert der xsRelayListeningPort-Eigenschaft ab.
     * 
     */
    public int getXsRelayListeningPort() {
        return xsRelayListeningPort;
    }

    /**
     * Legt den Wert der xsRelayListeningPort-Eigenschaft fest.
     * 
     */
    public void setXsRelayListeningPort(int value) {
        this.xsRelayListeningPort = value;
    }

    /**
     * Ruft den Wert der tcTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getTcTimerSeconds() {
        return tcTimerSeconds;
    }

    /**
     * Legt den Wert der tcTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setTcTimerSeconds(int value) {
        this.tcTimerSeconds = value;
    }

    /**
     * Ruft den Wert der twTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getTwTimerSeconds() {
        return twTimerSeconds;
    }

    /**
     * Legt den Wert der twTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setTwTimerSeconds(int value) {
        this.twTimerSeconds = value;
    }

    /**
     * Ruft den Wert der requestTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getRequestTimerSeconds() {
        return requestTimerSeconds;
    }

    /**
     * Legt den Wert der requestTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setRequestTimerSeconds(int value) {
        this.requestTimerSeconds = value;
    }

    /**
     * Ruft den Wert der busyPeerDetectionOutstandingTxnCount-Eigenschaft ab.
     * 
     */
    public int getBusyPeerDetectionOutstandingTxnCount() {
        return busyPeerDetectionOutstandingTxnCount;
    }

    /**
     * Legt den Wert der busyPeerDetectionOutstandingTxnCount-Eigenschaft fest.
     * 
     */
    public void setBusyPeerDetectionOutstandingTxnCount(int value) {
        this.busyPeerDetectionOutstandingTxnCount = value;
    }

    /**
     * Ruft den Wert der busyPeerRestoreOutstandingTxnCount-Eigenschaft ab.
     * 
     */
    public int getBusyPeerRestoreOutstandingTxnCount() {
        return busyPeerRestoreOutstandingTxnCount;
    }

    /**
     * Legt den Wert der busyPeerRestoreOutstandingTxnCount-Eigenschaft fest.
     * 
     */
    public void setBusyPeerRestoreOutstandingTxnCount(int value) {
        this.busyPeerRestoreOutstandingTxnCount = value;
    }

    /**
     * Ruft den Wert der dynamicEntryInactivityTimerHours-Eigenschaft ab.
     * 
     */
    public int getDynamicEntryInactivityTimerHours() {
        return dynamicEntryInactivityTimerHours;
    }

    /**
     * Legt den Wert der dynamicEntryInactivityTimerHours-Eigenschaft fest.
     * 
     */
    public void setDynamicEntryInactivityTimerHours(int value) {
        this.dynamicEntryInactivityTimerHours = value;
    }

    /**
     * Ruft den Wert der advertisedOfflineBillingApplication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvertisedOfflineBillingApplication() {
        return advertisedOfflineBillingApplication;
    }

    /**
     * Legt den Wert der advertisedOfflineBillingApplication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvertisedOfflineBillingApplication(String value) {
        this.advertisedOfflineBillingApplication = value;
    }

    /**
     * Ruft den Wert der advertisedOnlineBillingApplication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvertisedOnlineBillingApplication() {
        return advertisedOnlineBillingApplication;
    }

    /**
     * Legt den Wert der advertisedOnlineBillingApplication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvertisedOnlineBillingApplication(String value) {
        this.advertisedOnlineBillingApplication = value;
    }

    /**
     * Ruft den Wert der peerDiscoveryMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DiameterPeerDiscoveryMode }
     *     
     */
    public DiameterPeerDiscoveryMode getPeerDiscoveryMode() {
        return peerDiscoveryMode;
    }

    /**
     * Legt den Wert der peerDiscoveryMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DiameterPeerDiscoveryMode }
     *     
     */
    public void setPeerDiscoveryMode(DiameterPeerDiscoveryMode value) {
        this.peerDiscoveryMode = value;
    }

    /**
     * Ruft den Wert der defaultPort-Eigenschaft ab.
     * 
     */
    public int getDefaultPort() {
        return defaultPort;
    }

    /**
     * Legt den Wert der defaultPort-Eigenschaft fest.
     * 
     */
    public void setDefaultPort(int value) {
        this.defaultPort = value;
    }

    /**
     * Ruft den Wert der defaultSecurePort-Eigenschaft ab.
     * 
     */
    public int getDefaultSecurePort() {
        return defaultSecurePort;
    }

    /**
     * Legt den Wert der defaultSecurePort-Eigenschaft fest.
     * 
     */
    public void setDefaultSecurePort(int value) {
        this.defaultSecurePort = value;
    }

}
