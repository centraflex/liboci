//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserVideoAddOnGetRequest22.
 *       
 * 
 * <p>Java-Klasse für UserVideoAddOnGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserVideoAddOnGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxOriginatingCallDelaySeconds" type="{}VideoAddOnMaxOriginatingCallDelaySeconds"/>
 *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointWithPortNumberRead22" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserVideoAddOnGetResponse22", propOrder = {
    "isActive",
    "maxOriginatingCallDelaySeconds",
    "accessDeviceEndpoint"
})
public class UserVideoAddOnGetResponse22
    extends OCIDataResponse
{

    protected boolean isActive;
    protected int maxOriginatingCallDelaySeconds;
    protected AccessDeviceEndpointWithPortNumberRead22 accessDeviceEndpoint;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der maxOriginatingCallDelaySeconds-Eigenschaft ab.
     * 
     */
    public int getMaxOriginatingCallDelaySeconds() {
        return maxOriginatingCallDelaySeconds;
    }

    /**
     * Legt den Wert der maxOriginatingCallDelaySeconds-Eigenschaft fest.
     * 
     */
    public void setMaxOriginatingCallDelaySeconds(int value) {
        this.maxOriginatingCallDelaySeconds = value;
    }

    /**
     * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEndpointWithPortNumberRead22 }
     *     
     */
    public AccessDeviceEndpointWithPortNumberRead22 getAccessDeviceEndpoint() {
        return accessDeviceEndpoint;
    }

    /**
     * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEndpointWithPortNumberRead22 }
     *     
     */
    public void setAccessDeviceEndpoint(AccessDeviceEndpointWithPortNumberRead22 value) {
        this.accessDeviceEndpoint = value;
    }

}
