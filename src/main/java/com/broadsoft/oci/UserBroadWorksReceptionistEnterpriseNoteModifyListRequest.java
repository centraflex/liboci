//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a list of notes for an existing Receptionist user.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         If some of the users add and some users fail to add, then the
 *         response will contain a WarningResponse containing only the 
 *         a comma delimited list of users that failed to add. If the user 
 *         sending the request is the not the owner of the Receptionist Note, 
 *         then an ErrorResponse will be returned.
 *       
 * 
 * <p>Java-Klasse für UserBroadWorksReceptionistEnterpriseNoteModifyListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksReceptionistEnterpriseNoteModifyListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="receptionistUserId" type="{}UserId"/>
 *         <element name="receptionistUserAndNote" type="{}ReceptionistContactUserAndNote" maxOccurs="1000"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksReceptionistEnterpriseNoteModifyListRequest", propOrder = {
    "receptionistUserId",
    "receptionistUserAndNote"
})
public class UserBroadWorksReceptionistEnterpriseNoteModifyListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String receptionistUserId;
    @XmlElement(required = true)
    protected List<ReceptionistContactUserAndNote> receptionistUserAndNote;

    /**
     * Ruft den Wert der receptionistUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceptionistUserId() {
        return receptionistUserId;
    }

    /**
     * Legt den Wert der receptionistUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceptionistUserId(String value) {
        this.receptionistUserId = value;
    }

    /**
     * Gets the value of the receptionistUserAndNote property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the receptionistUserAndNote property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceptionistUserAndNote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReceptionistContactUserAndNote }
     * 
     * 
     * @return
     *     The value of the receptionistUserAndNote property.
     */
    public List<ReceptionistContactUserAndNote> getReceptionistUserAndNote() {
        if (receptionistUserAndNote == null) {
            receptionistUserAndNote = new ArrayList<>();
        }
        return this.receptionistUserAndNote;
    }

}
