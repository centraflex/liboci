//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify an Auto Attendant submenu instance.
 *         The response is either SuccessResponse or ErrorResponse.
 *         This request is only valid for Standard auto attendants.
 *         
 *         Replaced by: GroupAutoAttendantSubmenuModifyRequest20 in AS data mode
 *       
 * 
 * <p>Java-Klasse für GroupAutoAttendantSubmenuModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAutoAttendantSubmenuModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="submenuId" type="{}AutoAttendantSubmenuId"/>
 *         <element name="newSubmenuId" type="{}AutoAttendantSubmenuId" minOccurs="0"/>
 *         <element name="announcementSelection" type="{}AnnouncementSelection" minOccurs="0"/>
 *         <element name="audioFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *         <element name="videoFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *         <element name="enableLevelExtensionDialing" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="keyConfiguration" type="{}AutoAttendantKeyModifyConfiguration" maxOccurs="12" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAutoAttendantSubmenuModifyRequest", propOrder = {
    "serviceUserId",
    "submenuId",
    "newSubmenuId",
    "announcementSelection",
    "audioFile",
    "videoFile",
    "enableLevelExtensionDialing",
    "keyConfiguration"
})
public class GroupAutoAttendantSubmenuModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String submenuId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newSubmenuId;
    @XmlSchemaType(name = "token")
    protected AnnouncementSelection announcementSelection;
    protected LabeledMediaFileResource audioFile;
    protected LabeledMediaFileResource videoFile;
    protected Boolean enableLevelExtensionDialing;
    protected List<AutoAttendantKeyModifyConfiguration> keyConfiguration;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der submenuId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubmenuId() {
        return submenuId;
    }

    /**
     * Legt den Wert der submenuId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubmenuId(String value) {
        this.submenuId = value;
    }

    /**
     * Ruft den Wert der newSubmenuId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewSubmenuId() {
        return newSubmenuId;
    }

    /**
     * Legt den Wert der newSubmenuId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewSubmenuId(String value) {
        this.newSubmenuId = value;
    }

    /**
     * Ruft den Wert der announcementSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementSelection }
     *     
     */
    public AnnouncementSelection getAnnouncementSelection() {
        return announcementSelection;
    }

    /**
     * Legt den Wert der announcementSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementSelection }
     *     
     */
    public void setAnnouncementSelection(AnnouncementSelection value) {
        this.announcementSelection = value;
    }

    /**
     * Ruft den Wert der audioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public LabeledMediaFileResource getAudioFile() {
        return audioFile;
    }

    /**
     * Legt den Wert der audioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public void setAudioFile(LabeledMediaFileResource value) {
        this.audioFile = value;
    }

    /**
     * Ruft den Wert der videoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public LabeledMediaFileResource getVideoFile() {
        return videoFile;
    }

    /**
     * Legt den Wert der videoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public void setVideoFile(LabeledMediaFileResource value) {
        this.videoFile = value;
    }

    /**
     * Ruft den Wert der enableLevelExtensionDialing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableLevelExtensionDialing() {
        return enableLevelExtensionDialing;
    }

    /**
     * Legt den Wert der enableLevelExtensionDialing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableLevelExtensionDialing(Boolean value) {
        this.enableLevelExtensionDialing = value;
    }

    /**
     * Gets the value of the keyConfiguration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the keyConfiguration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKeyConfiguration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AutoAttendantKeyModifyConfiguration }
     * 
     * 
     * @return
     *     The value of the keyConfiguration property.
     */
    public List<AutoAttendantKeyModifyConfiguration> getKeyConfiguration() {
        if (keyConfiguration == null) {
            keyConfiguration = new ArrayList<>();
        }
        return this.keyConfiguration;
    }

}
