//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für RecordingPauseResumeNotificationType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="RecordingPauseResumeNotificationType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="None"/>
 *     <enumeration value="Beep"/>
 *     <enumeration value="Play Announcement"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "RecordingPauseResumeNotificationType")
@XmlEnum
public enum RecordingPauseResumeNotificationType {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Beep")
    BEEP("Beep"),
    @XmlEnumValue("Play Announcement")
    PLAY_ANNOUNCEMENT("Play Announcement");
    private final String value;

    RecordingPauseResumeNotificationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RecordingPauseResumeNotificationType fromValue(String v) {
        for (RecordingPauseResumeNotificationType c: RecordingPauseResumeNotificationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
