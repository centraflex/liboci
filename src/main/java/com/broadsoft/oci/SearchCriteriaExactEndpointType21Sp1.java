//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for a particular endpoint type.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactEndpointType21sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactEndpointType21sp1">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="endpointType" type="{}EndpointType21sp1"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactEndpointType21sp1", propOrder = {
    "endpointType"
})
public class SearchCriteriaExactEndpointType21Sp1
    extends SearchCriteria
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EndpointType21Sp1 endpointType;

    /**
     * Ruft den Wert der endpointType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EndpointType21Sp1 }
     *     
     */
    public EndpointType21Sp1 getEndpointType() {
        return endpointType;
    }

    /**
     * Legt den Wert der endpointType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EndpointType21Sp1 }
     *     
     */
    public void setEndpointType(EndpointType21Sp1 value) {
        this.endpointType = value;
    }

}
