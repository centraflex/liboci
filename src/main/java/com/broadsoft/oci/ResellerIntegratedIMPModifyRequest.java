//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the Integrated IMP service attributes for the reseller.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ResellerIntegratedIMPModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerIntegratedIMPModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="resellerId" type="{}ResellerId22"/>
 *         <element name="useSystemServiceDomain" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="serviceDomain" type="{}DomainName" minOccurs="0"/>
 *         <element name="servicePort" type="{}Port" minOccurs="0"/>
 *         <element name="useSystemMessagingServer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="provisioningUrl" type="{}URL" minOccurs="0"/>
 *         <element name="provisioningUserId" type="{}ProvisioningBroadCloudAuthenticationUserName" minOccurs="0"/>
 *         <element name="provisioningPassword" type="{}ProvisioningBroadCloudAuthenticationPassword" minOccurs="0"/>
 *         <element name="boshURL" type="{}URL" minOccurs="0"/>
 *         <element name="defaultImpIdType" type="{}IntegratedIMPUserIDType" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerIntegratedIMPModifyRequest", propOrder = {
    "resellerId",
    "useSystemServiceDomain",
    "serviceDomain",
    "servicePort",
    "useSystemMessagingServer",
    "provisioningUrl",
    "provisioningUserId",
    "provisioningPassword",
    "boshURL",
    "defaultImpIdType"
})
public class ResellerIntegratedIMPModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    protected Boolean useSystemServiceDomain;
    @XmlElementRef(name = "serviceDomain", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serviceDomain;
    @XmlElementRef(name = "servicePort", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> servicePort;
    protected Boolean useSystemMessagingServer;
    @XmlElementRef(name = "provisioningUrl", type = JAXBElement.class, required = false)
    protected JAXBElement<String> provisioningUrl;
    @XmlElementRef(name = "provisioningUserId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> provisioningUserId;
    @XmlElementRef(name = "provisioningPassword", type = JAXBElement.class, required = false)
    protected JAXBElement<String> provisioningPassword;
    @XmlElementRef(name = "boshURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> boshURL;
    @XmlSchemaType(name = "token")
    protected IntegratedIMPUserIDType defaultImpIdType;

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Ruft den Wert der useSystemServiceDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseSystemServiceDomain() {
        return useSystemServiceDomain;
    }

    /**
     * Legt den Wert der useSystemServiceDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSystemServiceDomain(Boolean value) {
        this.useSystemServiceDomain = value;
    }

    /**
     * Ruft den Wert der serviceDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServiceDomain() {
        return serviceDomain;
    }

    /**
     * Legt den Wert der serviceDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServiceDomain(JAXBElement<String> value) {
        this.serviceDomain = value;
    }

    /**
     * Ruft den Wert der servicePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getServicePort() {
        return servicePort;
    }

    /**
     * Legt den Wert der servicePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setServicePort(JAXBElement<Integer> value) {
        this.servicePort = value;
    }

    /**
     * Ruft den Wert der useSystemMessagingServer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseSystemMessagingServer() {
        return useSystemMessagingServer;
    }

    /**
     * Legt den Wert der useSystemMessagingServer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSystemMessagingServer(Boolean value) {
        this.useSystemMessagingServer = value;
    }

    /**
     * Ruft den Wert der provisioningUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProvisioningUrl() {
        return provisioningUrl;
    }

    /**
     * Legt den Wert der provisioningUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProvisioningUrl(JAXBElement<String> value) {
        this.provisioningUrl = value;
    }

    /**
     * Ruft den Wert der provisioningUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProvisioningUserId() {
        return provisioningUserId;
    }

    /**
     * Legt den Wert der provisioningUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProvisioningUserId(JAXBElement<String> value) {
        this.provisioningUserId = value;
    }

    /**
     * Ruft den Wert der provisioningPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProvisioningPassword() {
        return provisioningPassword;
    }

    /**
     * Legt den Wert der provisioningPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProvisioningPassword(JAXBElement<String> value) {
        this.provisioningPassword = value;
    }

    /**
     * Ruft den Wert der boshURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBoshURL() {
        return boshURL;
    }

    /**
     * Legt den Wert der boshURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBoshURL(JAXBElement<String> value) {
        this.boshURL = value;
    }

    /**
     * Ruft den Wert der defaultImpIdType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IntegratedIMPUserIDType }
     *     
     */
    public IntegratedIMPUserIDType getDefaultImpIdType() {
        return defaultImpIdType;
    }

    /**
     * Legt den Wert der defaultImpIdType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IntegratedIMPUserIDType }
     *     
     */
    public void setDefaultImpIdType(IntegratedIMPUserIDType value) {
        this.defaultImpIdType = value;
    }

}
