//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Trunk group endpoint that can have multiple contacts.
 *         Replaced by: TrunkAddressingMultipleContactRead21
 *       
 * 
 * <p>Java-Klasse für TrunkAddressingMultipleContactRead complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="TrunkAddressingMultipleContactRead">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="trunkGroupDeviceEndpoint" type="{}TrunkGroupDeviceMultipleContactEndpointRead" minOccurs="0"/>
 *         <element name="enterpriseTrunkName" type="{}EnterpriseTrunkName" minOccurs="0"/>
 *         <element name="alternateTrunkIdentity" type="{}AlternateTrunkIdentity" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrunkAddressingMultipleContactRead", propOrder = {
    "trunkGroupDeviceEndpoint",
    "enterpriseTrunkName",
    "alternateTrunkIdentity"
})
public class TrunkAddressingMultipleContactRead {

    protected TrunkGroupDeviceMultipleContactEndpointRead trunkGroupDeviceEndpoint;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String enterpriseTrunkName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String alternateTrunkIdentity;

    /**
     * Ruft den Wert der trunkGroupDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupDeviceMultipleContactEndpointRead }
     *     
     */
    public TrunkGroupDeviceMultipleContactEndpointRead getTrunkGroupDeviceEndpoint() {
        return trunkGroupDeviceEndpoint;
    }

    /**
     * Legt den Wert der trunkGroupDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupDeviceMultipleContactEndpointRead }
     *     
     */
    public void setTrunkGroupDeviceEndpoint(TrunkGroupDeviceMultipleContactEndpointRead value) {
        this.trunkGroupDeviceEndpoint = value;
    }

    /**
     * Ruft den Wert der enterpriseTrunkName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnterpriseTrunkName() {
        return enterpriseTrunkName;
    }

    /**
     * Legt den Wert der enterpriseTrunkName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnterpriseTrunkName(String value) {
        this.enterpriseTrunkName = value;
    }

    /**
     * Ruft den Wert der alternateTrunkIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateTrunkIdentity() {
        return alternateTrunkIdentity;
    }

    /**
     * Legt den Wert der alternateTrunkIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateTrunkIdentity(String value) {
        this.alternateTrunkIdentity = value;
    }

}
