//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ServiceProviderAdminServiceAccess.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ServiceProviderAdminServiceAccess">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Full"/>
 *     <enumeration value="No Authorization"/>
 *     <enumeration value="Read-Only"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ServiceProviderAdminServiceAccess")
@XmlEnum
public enum ServiceProviderAdminServiceAccess {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("No Authorization")
    NO_AUTHORIZATION("No Authorization"),
    @XmlEnumValue("Read-Only")
    READ_ONLY("Read-Only");
    private final String value;

    ServiceProviderAdminServiceAccess(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ServiceProviderAdminServiceAccess fromValue(String v) {
        for (ServiceProviderAdminServiceAccess c: ServiceProviderAdminServiceAccess.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
