//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request a list of service providers and/or enterprises in the system.
 *         The response is either a ServiceProviderGetPagedSortedListResponse or an ErrorResponse.
 *         If reseller administrator sends the request, searchCriteriaResellerId is ignored. Service providers/enterprise 
 *         within the administrator's reseller meeting the search criteria are returned.        
 *         If the responsePagingControl element is not provided, the paging startIndex will be set to 1 by 
 *         default, and the responsePageSize will be set to the maximum responsePageSize by default.
 *         If no sortOrder is included, the response is sorted by Service Provider Id ascending by default.
 *         Multiple search criteria are logically ANDed together unless the searchCriteriaModeOr option is 
 *         included. Then the search criteria are logically ORed together.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderGetPagedSortedListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderGetPagedSortedListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl" minOccurs="0"/>
 *         <element name="sortOrder" type="{}SortOrderServiceProviderGetPagedSortedList" maxOccurs="3" minOccurs="0"/>
 *         <element name="searchCriteriaServiceProviderId" type="{}SearchCriteriaServiceProviderId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaServiceProviderName" type="{}SearchCriteriaServiceProviderName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactOrganizationType" type="{}SearchCriteriaExactOrganizationType" minOccurs="0"/>
 *         <element name="searchCriteriaResellerId" type="{}SearchCriteriaResellerId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaModeOr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderGetPagedSortedListRequest", propOrder = {
    "responsePagingControl",
    "sortOrder",
    "searchCriteriaServiceProviderId",
    "searchCriteriaServiceProviderName",
    "searchCriteriaExactOrganizationType",
    "searchCriteriaResellerId",
    "searchCriteriaModeOr"
})
public class ServiceProviderGetPagedSortedListRequest
    extends OCIRequest
{

    protected ResponsePagingControl responsePagingControl;
    protected List<SortOrderServiceProviderGetPagedSortedList> sortOrder;
    protected List<SearchCriteriaServiceProviderId> searchCriteriaServiceProviderId;
    protected List<SearchCriteriaServiceProviderName> searchCriteriaServiceProviderName;
    protected SearchCriteriaExactOrganizationType searchCriteriaExactOrganizationType;
    protected List<SearchCriteriaResellerId> searchCriteriaResellerId;
    protected Boolean searchCriteriaModeOr;

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Gets the value of the sortOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the sortOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSortOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SortOrderServiceProviderGetPagedSortedList }
     * 
     * 
     * @return
     *     The value of the sortOrder property.
     */
    public List<SortOrderServiceProviderGetPagedSortedList> getSortOrder() {
        if (sortOrder == null) {
            sortOrder = new ArrayList<>();
        }
        return this.sortOrder;
    }

    /**
     * Gets the value of the searchCriteriaServiceProviderId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaServiceProviderId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaServiceProviderId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaServiceProviderId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaServiceProviderId property.
     */
    public List<SearchCriteriaServiceProviderId> getSearchCriteriaServiceProviderId() {
        if (searchCriteriaServiceProviderId == null) {
            searchCriteriaServiceProviderId = new ArrayList<>();
        }
        return this.searchCriteriaServiceProviderId;
    }

    /**
     * Gets the value of the searchCriteriaServiceProviderName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaServiceProviderName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaServiceProviderName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaServiceProviderName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaServiceProviderName property.
     */
    public List<SearchCriteriaServiceProviderName> getSearchCriteriaServiceProviderName() {
        if (searchCriteriaServiceProviderName == null) {
            searchCriteriaServiceProviderName = new ArrayList<>();
        }
        return this.searchCriteriaServiceProviderName;
    }

    /**
     * Ruft den Wert der searchCriteriaExactOrganizationType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactOrganizationType }
     *     
     */
    public SearchCriteriaExactOrganizationType getSearchCriteriaExactOrganizationType() {
        return searchCriteriaExactOrganizationType;
    }

    /**
     * Legt den Wert der searchCriteriaExactOrganizationType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactOrganizationType }
     *     
     */
    public void setSearchCriteriaExactOrganizationType(SearchCriteriaExactOrganizationType value) {
        this.searchCriteriaExactOrganizationType = value;
    }

    /**
     * Gets the value of the searchCriteriaResellerId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaResellerId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaResellerId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaResellerId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaResellerId property.
     */
    public List<SearchCriteriaResellerId> getSearchCriteriaResellerId() {
        if (searchCriteriaResellerId == null) {
            searchCriteriaResellerId = new ArrayList<>();
        }
        return this.searchCriteriaResellerId;
    }

    /**
     * Ruft den Wert der searchCriteriaModeOr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchCriteriaModeOr() {
        return searchCriteriaModeOr;
    }

    /**
     * Legt den Wert der searchCriteriaModeOr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchCriteriaModeOr(Boolean value) {
        this.searchCriteriaModeOr = value;
    }

}
