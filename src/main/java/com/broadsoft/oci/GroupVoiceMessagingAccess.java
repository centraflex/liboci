//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GroupVoiceMessagingAccess.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="GroupVoiceMessagingAccess">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Full"/>
 *     <enumeration value="Restricted"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "GroupVoiceMessagingAccess")
@XmlEnum
public enum GroupVoiceMessagingAccess {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("Restricted")
    RESTRICTED("Restricted");
    private final String value;

    GroupVoiceMessagingAccess(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupVoiceMessagingAccess fromValue(String v) {
        for (GroupVoiceMessagingAccess c: GroupVoiceMessagingAccess.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
