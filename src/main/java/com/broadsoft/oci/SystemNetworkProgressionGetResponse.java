//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemNetworkProgressionGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemNetworkProgressionGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNetworkProgressionGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="waitPeriodSeconds" type="{}NetworkProgressionWaitPeriodSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNetworkProgressionGetResponse", propOrder = {
    "isActive",
    "waitPeriodSeconds"
})
public class SystemNetworkProgressionGetResponse
    extends OCIDataResponse
{

    protected boolean isActive;
    protected int waitPeriodSeconds;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der waitPeriodSeconds-Eigenschaft ab.
     * 
     */
    public int getWaitPeriodSeconds() {
        return waitPeriodSeconds;
    }

    /**
     * Legt den Wert der waitPeriodSeconds-Eigenschaft fest.
     * 
     */
    public void setWaitPeriodSeconds(int value) {
        this.waitPeriodSeconds = value;
    }

}
