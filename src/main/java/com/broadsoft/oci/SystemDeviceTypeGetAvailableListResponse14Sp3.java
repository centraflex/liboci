//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemDeviceTypeGetAvailableListRequest14sp3.
 *       
 * 
 * <p>Java-Klasse für SystemDeviceTypeGetAvailableListResponse14sp3 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceTypeGetAvailableListResponse14sp3">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="deviceType" type="{}AccessDeviceType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="typeInfo" maxOccurs="unbounded" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="profile" type="{}SignalingAddressType"/>
 *                   <element name="staticRegistrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   <element name="configType" type="{}AccessDeviceEnhancedConfigurationType14" minOccurs="0"/>
 *                   <element name="protocolChoice" type="{}AccessDeviceProtocol" maxOccurs="unbounded"/>
 *                   <element name="isIpAddressOptional" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   <element name="useDomain" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   <element name="isMobilityManagerDevice" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   <element name="deviceConfigurationOption" type="{}DeviceTypeConfigurationOptionType" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceTypeGetAvailableListResponse14sp3", propOrder = {
    "deviceType",
    "typeInfo"
})
public class SystemDeviceTypeGetAvailableListResponse14Sp3
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> deviceType;
    protected List<SystemDeviceTypeGetAvailableListResponse14Sp3 .TypeInfo> typeInfo;

    /**
     * Gets the value of the deviceType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the deviceType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeviceType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the deviceType property.
     */
    public List<String> getDeviceType() {
        if (deviceType == null) {
            deviceType = new ArrayList<>();
        }
        return this.deviceType;
    }

    /**
     * Gets the value of the typeInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the typeInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTypeInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SystemDeviceTypeGetAvailableListResponse14Sp3 .TypeInfo }
     * 
     * 
     * @return
     *     The value of the typeInfo property.
     */
    public List<SystemDeviceTypeGetAvailableListResponse14Sp3 .TypeInfo> getTypeInfo() {
        if (typeInfo == null) {
            typeInfo = new ArrayList<>();
        }
        return this.typeInfo;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="profile" type="{}SignalingAddressType"/>
     *         <element name="staticRegistrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         <element name="configType" type="{}AccessDeviceEnhancedConfigurationType14" minOccurs="0"/>
     *         <element name="protocolChoice" type="{}AccessDeviceProtocol" maxOccurs="unbounded"/>
     *         <element name="isIpAddressOptional" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         <element name="useDomain" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         <element name="isMobilityManagerDevice" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         <element name="deviceConfigurationOption" type="{}DeviceTypeConfigurationOptionType" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "profile",
        "staticRegistrationCapable",
        "configType",
        "protocolChoice",
        "isIpAddressOptional",
        "useDomain",
        "isMobilityManagerDevice",
        "deviceConfigurationOption"
    })
    public static class TypeInfo {

        @XmlElement(required = true)
        @XmlSchemaType(name = "token")
        protected SignalingAddressType profile;
        protected boolean staticRegistrationCapable;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String configType;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> protocolChoice;
        protected boolean isIpAddressOptional;
        protected boolean useDomain;
        protected boolean isMobilityManagerDevice;
        @XmlSchemaType(name = "token")
        protected DeviceTypeConfigurationOptionType deviceConfigurationOption;

        /**
         * Ruft den Wert der profile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link SignalingAddressType }
         *     
         */
        public SignalingAddressType getProfile() {
            return profile;
        }

        /**
         * Legt den Wert der profile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link SignalingAddressType }
         *     
         */
        public void setProfile(SignalingAddressType value) {
            this.profile = value;
        }

        /**
         * Ruft den Wert der staticRegistrationCapable-Eigenschaft ab.
         * 
         */
        public boolean isStaticRegistrationCapable() {
            return staticRegistrationCapable;
        }

        /**
         * Legt den Wert der staticRegistrationCapable-Eigenschaft fest.
         * 
         */
        public void setStaticRegistrationCapable(boolean value) {
            this.staticRegistrationCapable = value;
        }

        /**
         * Ruft den Wert der configType-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConfigType() {
            return configType;
        }

        /**
         * Legt den Wert der configType-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConfigType(String value) {
            this.configType = value;
        }

        /**
         * Gets the value of the protocolChoice property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the protocolChoice property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProtocolChoice().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the protocolChoice property.
         */
        public List<String> getProtocolChoice() {
            if (protocolChoice == null) {
                protocolChoice = new ArrayList<>();
            }
            return this.protocolChoice;
        }

        /**
         * Ruft den Wert der isIpAddressOptional-Eigenschaft ab.
         * 
         */
        public boolean isIsIpAddressOptional() {
            return isIpAddressOptional;
        }

        /**
         * Legt den Wert der isIpAddressOptional-Eigenschaft fest.
         * 
         */
        public void setIsIpAddressOptional(boolean value) {
            this.isIpAddressOptional = value;
        }

        /**
         * Ruft den Wert der useDomain-Eigenschaft ab.
         * 
         */
        public boolean isUseDomain() {
            return useDomain;
        }

        /**
         * Legt den Wert der useDomain-Eigenschaft fest.
         * 
         */
        public void setUseDomain(boolean value) {
            this.useDomain = value;
        }

        /**
         * Ruft den Wert der isMobilityManagerDevice-Eigenschaft ab.
         * 
         */
        public boolean isIsMobilityManagerDevice() {
            return isMobilityManagerDevice;
        }

        /**
         * Legt den Wert der isMobilityManagerDevice-Eigenschaft fest.
         * 
         */
        public void setIsMobilityManagerDevice(boolean value) {
            this.isMobilityManagerDevice = value;
        }

        /**
         * Ruft den Wert der deviceConfigurationOption-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link DeviceTypeConfigurationOptionType }
         *     
         */
        public DeviceTypeConfigurationOptionType getDeviceConfigurationOption() {
            return deviceConfigurationOption;
        }

        /**
         * Legt den Wert der deviceConfigurationOption-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link DeviceTypeConfigurationOptionType }
         *     
         */
        public void setDeviceConfigurationOption(DeviceTypeConfigurationOptionType value) {
            this.deviceConfigurationOption = value;
        }

    }

}
