//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EnterpriseInternalCallsCLIDPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="EnterpriseInternalCallsCLIDPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Use Extension"/>
 *     <enumeration value="Use Location Code plus Extension"/>
 *     <enumeration value="Use External Calls Policy"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "EnterpriseInternalCallsCLIDPolicy")
@XmlEnum
public enum EnterpriseInternalCallsCLIDPolicy {

    @XmlEnumValue("Use Extension")
    USE_EXTENSION("Use Extension"),
    @XmlEnumValue("Use Location Code plus Extension")
    USE_LOCATION_CODE_PLUS_EXTENSION("Use Location Code plus Extension"),
    @XmlEnumValue("Use External Calls Policy")
    USE_EXTERNAL_CALLS_POLICY("Use External Calls Policy");
    private final String value;

    EnterpriseInternalCallsCLIDPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnterpriseInternalCallsCLIDPolicy fromValue(String v) {
        for (EnterpriseInternalCallsCLIDPolicy c: EnterpriseInternalCallsCLIDPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
