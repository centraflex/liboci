//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCallCenterGetRequest19.
 *         Replaced by SystemCallCenterGetRequest21.
 *       
 * 
 * <p>Java-Klasse für SystemCallCenterGetResponse19 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallCenterGetResponse19">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="defaultFromAddress" type="{}EmailAddress"/>
 *         <element name="statisticsSamplingPeriodMinutes" type="{}CallCenterStatisticsSamplingPeriodMinutes"/>
 *         <element name="defaultEnableGuardTimer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="defaultGuardTimerSeconds" type="{}CallCenterGuardTimerSeconds"/>
 *         <element name="forceAgentUnavailableOnDNDActivation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forceAgentUnavailableOnPersonalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forceAgentUnavailableOnBouncedCallLimit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="numberConsecutiveBouncedCallsToForceAgentUnavailable" type="{}CallCenterConsecutiveBouncedCallsToForceAgentUnavailable"/>
 *         <element name="forceAgentUnavailableOnNotReachable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="defaultPlayRingWhenOfferCall" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="uniformCallDistributionPolicyScope" type="{}CallCenterUniformCallDistributionPolicyScope"/>
 *         <element name="callHandlingSamplingPeriodMinutes" type="{}CallHandlingSamplingPeriodMinutes"/>
 *         <element name="callHandlingMinimumSamplingSize" type="{}CallHandlingMinimumSamplingSize"/>
 *         <element name="playToneToAgentForEmergencyCall" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="emergencyCallCLIDPrefix" type="{}CallCenterEmergencyCallCLIDPrefix"/>
 *         <element name="thresholdCrossingNotificationEmailGuardTimerSeconds" type="{}CallCenterThresholdCrossingNotificationEmailGuardTimerSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallCenterGetResponse19", propOrder = {
    "defaultFromAddress",
    "statisticsSamplingPeriodMinutes",
    "defaultEnableGuardTimer",
    "defaultGuardTimerSeconds",
    "forceAgentUnavailableOnDNDActivation",
    "forceAgentUnavailableOnPersonalCalls",
    "forceAgentUnavailableOnBouncedCallLimit",
    "numberConsecutiveBouncedCallsToForceAgentUnavailable",
    "forceAgentUnavailableOnNotReachable",
    "defaultPlayRingWhenOfferCall",
    "uniformCallDistributionPolicyScope",
    "callHandlingSamplingPeriodMinutes",
    "callHandlingMinimumSamplingSize",
    "playToneToAgentForEmergencyCall",
    "emergencyCallCLIDPrefix",
    "thresholdCrossingNotificationEmailGuardTimerSeconds"
})
public class SystemCallCenterGetResponse19
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultFromAddress;
    protected int statisticsSamplingPeriodMinutes;
    protected boolean defaultEnableGuardTimer;
    protected int defaultGuardTimerSeconds;
    protected boolean forceAgentUnavailableOnDNDActivation;
    protected boolean forceAgentUnavailableOnPersonalCalls;
    protected boolean forceAgentUnavailableOnBouncedCallLimit;
    protected int numberConsecutiveBouncedCallsToForceAgentUnavailable;
    protected boolean forceAgentUnavailableOnNotReachable;
    protected boolean defaultPlayRingWhenOfferCall;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterUniformCallDistributionPolicyScope uniformCallDistributionPolicyScope;
    protected int callHandlingSamplingPeriodMinutes;
    protected int callHandlingMinimumSamplingSize;
    protected boolean playToneToAgentForEmergencyCall;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String emergencyCallCLIDPrefix;
    protected int thresholdCrossingNotificationEmailGuardTimerSeconds;

    /**
     * Ruft den Wert der defaultFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultFromAddress() {
        return defaultFromAddress;
    }

    /**
     * Legt den Wert der defaultFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultFromAddress(String value) {
        this.defaultFromAddress = value;
    }

    /**
     * Ruft den Wert der statisticsSamplingPeriodMinutes-Eigenschaft ab.
     * 
     */
    public int getStatisticsSamplingPeriodMinutes() {
        return statisticsSamplingPeriodMinutes;
    }

    /**
     * Legt den Wert der statisticsSamplingPeriodMinutes-Eigenschaft fest.
     * 
     */
    public void setStatisticsSamplingPeriodMinutes(int value) {
        this.statisticsSamplingPeriodMinutes = value;
    }

    /**
     * Ruft den Wert der defaultEnableGuardTimer-Eigenschaft ab.
     * 
     */
    public boolean isDefaultEnableGuardTimer() {
        return defaultEnableGuardTimer;
    }

    /**
     * Legt den Wert der defaultEnableGuardTimer-Eigenschaft fest.
     * 
     */
    public void setDefaultEnableGuardTimer(boolean value) {
        this.defaultEnableGuardTimer = value;
    }

    /**
     * Ruft den Wert der defaultGuardTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getDefaultGuardTimerSeconds() {
        return defaultGuardTimerSeconds;
    }

    /**
     * Legt den Wert der defaultGuardTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setDefaultGuardTimerSeconds(int value) {
        this.defaultGuardTimerSeconds = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnDNDActivation-Eigenschaft ab.
     * 
     */
    public boolean isForceAgentUnavailableOnDNDActivation() {
        return forceAgentUnavailableOnDNDActivation;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnDNDActivation-Eigenschaft fest.
     * 
     */
    public void setForceAgentUnavailableOnDNDActivation(boolean value) {
        this.forceAgentUnavailableOnDNDActivation = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnPersonalCalls-Eigenschaft ab.
     * 
     */
    public boolean isForceAgentUnavailableOnPersonalCalls() {
        return forceAgentUnavailableOnPersonalCalls;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnPersonalCalls-Eigenschaft fest.
     * 
     */
    public void setForceAgentUnavailableOnPersonalCalls(boolean value) {
        this.forceAgentUnavailableOnPersonalCalls = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnBouncedCallLimit-Eigenschaft ab.
     * 
     */
    public boolean isForceAgentUnavailableOnBouncedCallLimit() {
        return forceAgentUnavailableOnBouncedCallLimit;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnBouncedCallLimit-Eigenschaft fest.
     * 
     */
    public void setForceAgentUnavailableOnBouncedCallLimit(boolean value) {
        this.forceAgentUnavailableOnBouncedCallLimit = value;
    }

    /**
     * Ruft den Wert der numberConsecutiveBouncedCallsToForceAgentUnavailable-Eigenschaft ab.
     * 
     */
    public int getNumberConsecutiveBouncedCallsToForceAgentUnavailable() {
        return numberConsecutiveBouncedCallsToForceAgentUnavailable;
    }

    /**
     * Legt den Wert der numberConsecutiveBouncedCallsToForceAgentUnavailable-Eigenschaft fest.
     * 
     */
    public void setNumberConsecutiveBouncedCallsToForceAgentUnavailable(int value) {
        this.numberConsecutiveBouncedCallsToForceAgentUnavailable = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnNotReachable-Eigenschaft ab.
     * 
     */
    public boolean isForceAgentUnavailableOnNotReachable() {
        return forceAgentUnavailableOnNotReachable;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnNotReachable-Eigenschaft fest.
     * 
     */
    public void setForceAgentUnavailableOnNotReachable(boolean value) {
        this.forceAgentUnavailableOnNotReachable = value;
    }

    /**
     * Ruft den Wert der defaultPlayRingWhenOfferCall-Eigenschaft ab.
     * 
     */
    public boolean isDefaultPlayRingWhenOfferCall() {
        return defaultPlayRingWhenOfferCall;
    }

    /**
     * Legt den Wert der defaultPlayRingWhenOfferCall-Eigenschaft fest.
     * 
     */
    public void setDefaultPlayRingWhenOfferCall(boolean value) {
        this.defaultPlayRingWhenOfferCall = value;
    }

    /**
     * Ruft den Wert der uniformCallDistributionPolicyScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterUniformCallDistributionPolicyScope }
     *     
     */
    public CallCenterUniformCallDistributionPolicyScope getUniformCallDistributionPolicyScope() {
        return uniformCallDistributionPolicyScope;
    }

    /**
     * Legt den Wert der uniformCallDistributionPolicyScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterUniformCallDistributionPolicyScope }
     *     
     */
    public void setUniformCallDistributionPolicyScope(CallCenterUniformCallDistributionPolicyScope value) {
        this.uniformCallDistributionPolicyScope = value;
    }

    /**
     * Ruft den Wert der callHandlingSamplingPeriodMinutes-Eigenschaft ab.
     * 
     */
    public int getCallHandlingSamplingPeriodMinutes() {
        return callHandlingSamplingPeriodMinutes;
    }

    /**
     * Legt den Wert der callHandlingSamplingPeriodMinutes-Eigenschaft fest.
     * 
     */
    public void setCallHandlingSamplingPeriodMinutes(int value) {
        this.callHandlingSamplingPeriodMinutes = value;
    }

    /**
     * Ruft den Wert der callHandlingMinimumSamplingSize-Eigenschaft ab.
     * 
     */
    public int getCallHandlingMinimumSamplingSize() {
        return callHandlingMinimumSamplingSize;
    }

    /**
     * Legt den Wert der callHandlingMinimumSamplingSize-Eigenschaft fest.
     * 
     */
    public void setCallHandlingMinimumSamplingSize(int value) {
        this.callHandlingMinimumSamplingSize = value;
    }

    /**
     * Ruft den Wert der playToneToAgentForEmergencyCall-Eigenschaft ab.
     * 
     */
    public boolean isPlayToneToAgentForEmergencyCall() {
        return playToneToAgentForEmergencyCall;
    }

    /**
     * Legt den Wert der playToneToAgentForEmergencyCall-Eigenschaft fest.
     * 
     */
    public void setPlayToneToAgentForEmergencyCall(boolean value) {
        this.playToneToAgentForEmergencyCall = value;
    }

    /**
     * Ruft den Wert der emergencyCallCLIDPrefix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmergencyCallCLIDPrefix() {
        return emergencyCallCLIDPrefix;
    }

    /**
     * Legt den Wert der emergencyCallCLIDPrefix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmergencyCallCLIDPrefix(String value) {
        this.emergencyCallCLIDPrefix = value;
    }

    /**
     * Ruft den Wert der thresholdCrossingNotificationEmailGuardTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getThresholdCrossingNotificationEmailGuardTimerSeconds() {
        return thresholdCrossingNotificationEmailGuardTimerSeconds;
    }

    /**
     * Legt den Wert der thresholdCrossingNotificationEmailGuardTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setThresholdCrossingNotificationEmailGuardTimerSeconds(int value) {
        this.thresholdCrossingNotificationEmailGuardTimerSeconds = value;
    }

}
