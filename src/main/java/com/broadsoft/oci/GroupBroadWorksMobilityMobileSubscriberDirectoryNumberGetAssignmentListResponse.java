//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentListRequest.
 *         The response contains a table with columns: "Mobile Number", "User Id",
 *         "Last Name", "First Name","Phone Number", "Extension", "Department",.
 *         The "Mobile Number" column contains a single DN.
 *         The "User Id", "Last Name" and "First Name" columns contains the corresponding attributes of the user possessing the DN(s).
 *         The "Phone Number" column contains a single DN.
 *         The "Department" column contains the department of the user if it is part of a department.
 *       
 * 
 * <p>Java-Klasse für GroupBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="mobileSubscriberDirectoryNumberTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentListResponse", propOrder = {
    "mobileSubscriberDirectoryNumberTable"
})
public class GroupBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable mobileSubscriberDirectoryNumberTable;

    /**
     * Ruft den Wert der mobileSubscriberDirectoryNumberTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getMobileSubscriberDirectoryNumberTable() {
        return mobileSubscriberDirectoryNumberTable;
    }

    /**
     * Legt den Wert der mobileSubscriberDirectoryNumberTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setMobileSubscriberDirectoryNumberTable(OCITable value) {
        this.mobileSubscriberDirectoryNumberTable = value;
    }

}
