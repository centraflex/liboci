//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the SystemCallCenterEnhancedReportingBrandingGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemCallCenterEnhancedReportingBrandingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallCenterEnhancedReportingBrandingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="brandingChoice" type="{}CallCenterEnhancedReportingSystemBrandingChoice"/>
 *         <element name="customBrandingFileDescription" type="{}FileDescription" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallCenterEnhancedReportingBrandingGetResponse", propOrder = {
    "brandingChoice",
    "customBrandingFileDescription"
})
public class SystemCallCenterEnhancedReportingBrandingGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterEnhancedReportingSystemBrandingChoice brandingChoice;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String customBrandingFileDescription;

    /**
     * Ruft den Wert der brandingChoice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterEnhancedReportingSystemBrandingChoice }
     *     
     */
    public CallCenterEnhancedReportingSystemBrandingChoice getBrandingChoice() {
        return brandingChoice;
    }

    /**
     * Legt den Wert der brandingChoice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterEnhancedReportingSystemBrandingChoice }
     *     
     */
    public void setBrandingChoice(CallCenterEnhancedReportingSystemBrandingChoice value) {
        this.brandingChoice = value;
    }

    /**
     * Ruft den Wert der customBrandingFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomBrandingFileDescription() {
        return customBrandingFileDescription;
    }

    /**
     * Legt den Wert der customBrandingFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomBrandingFileDescription(String value) {
        this.customBrandingFileDescription = value;
    }

}
