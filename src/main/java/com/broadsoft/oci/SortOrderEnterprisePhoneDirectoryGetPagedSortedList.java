//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Used to sort the EnterprisePhoneDirectoryGetPagedSortedListRequest request.
 *       
 * 
 * <p>Java-Klasse für SortOrderEnterprisePhoneDirectoryGetPagedSortedList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SortOrderEnterprisePhoneDirectoryGetPagedSortedList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <choice>
 *         <element name="sortByUserLastName" type="{}SortByUserLastName"/>
 *         <element name="sortByUserFirstName" type="{}SortByUserFirstName"/>
 *         <element name="sortByGroupLocationCode" type="{}SortByGroupLocationCode"/>
 *         <element name="sortByMobilePhoneNumber" type="{}SortByMobilePhoneNumber"/>
 *         <element name="sortByEmailAddress" type="{}SortByEmailAddress"/>
 *         <element name="sortByDepartmentName" type="{}SortByDepartmentName"/>
 *         <element name="sortByGroupName" type="{}SortByGroupName"/>
 *         <element name="sortByYahooId" type="{}SortByYahooId"/>
 *         <element name="sortByUserId" type="{}SortByUserId"/>
 *         <element name="sortByImpId" type="{}SortByImpId"/>
 *       </choice>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SortOrderEnterprisePhoneDirectoryGetPagedSortedList", propOrder = {
    "sortByUserLastName",
    "sortByUserFirstName",
    "sortByGroupLocationCode",
    "sortByMobilePhoneNumber",
    "sortByEmailAddress",
    "sortByDepartmentName",
    "sortByGroupName",
    "sortByYahooId",
    "sortByUserId",
    "sortByImpId"
})
public class SortOrderEnterprisePhoneDirectoryGetPagedSortedList {

    protected SortByUserLastName sortByUserLastName;
    protected SortByUserFirstName sortByUserFirstName;
    protected SortByGroupLocationCode sortByGroupLocationCode;
    protected SortByMobilePhoneNumber sortByMobilePhoneNumber;
    protected SortByEmailAddress sortByEmailAddress;
    protected SortByDepartmentName sortByDepartmentName;
    protected SortByGroupName sortByGroupName;
    protected SortByYahooId sortByYahooId;
    protected SortByUserId sortByUserId;
    protected SortByImpId sortByImpId;

    /**
     * Ruft den Wert der sortByUserLastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByUserLastName }
     *     
     */
    public SortByUserLastName getSortByUserLastName() {
        return sortByUserLastName;
    }

    /**
     * Legt den Wert der sortByUserLastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByUserLastName }
     *     
     */
    public void setSortByUserLastName(SortByUserLastName value) {
        this.sortByUserLastName = value;
    }

    /**
     * Ruft den Wert der sortByUserFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByUserFirstName }
     *     
     */
    public SortByUserFirstName getSortByUserFirstName() {
        return sortByUserFirstName;
    }

    /**
     * Legt den Wert der sortByUserFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByUserFirstName }
     *     
     */
    public void setSortByUserFirstName(SortByUserFirstName value) {
        this.sortByUserFirstName = value;
    }

    /**
     * Ruft den Wert der sortByGroupLocationCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByGroupLocationCode }
     *     
     */
    public SortByGroupLocationCode getSortByGroupLocationCode() {
        return sortByGroupLocationCode;
    }

    /**
     * Legt den Wert der sortByGroupLocationCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByGroupLocationCode }
     *     
     */
    public void setSortByGroupLocationCode(SortByGroupLocationCode value) {
        this.sortByGroupLocationCode = value;
    }

    /**
     * Ruft den Wert der sortByMobilePhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByMobilePhoneNumber }
     *     
     */
    public SortByMobilePhoneNumber getSortByMobilePhoneNumber() {
        return sortByMobilePhoneNumber;
    }

    /**
     * Legt den Wert der sortByMobilePhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByMobilePhoneNumber }
     *     
     */
    public void setSortByMobilePhoneNumber(SortByMobilePhoneNumber value) {
        this.sortByMobilePhoneNumber = value;
    }

    /**
     * Ruft den Wert der sortByEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByEmailAddress }
     *     
     */
    public SortByEmailAddress getSortByEmailAddress() {
        return sortByEmailAddress;
    }

    /**
     * Legt den Wert der sortByEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByEmailAddress }
     *     
     */
    public void setSortByEmailAddress(SortByEmailAddress value) {
        this.sortByEmailAddress = value;
    }

    /**
     * Ruft den Wert der sortByDepartmentName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByDepartmentName }
     *     
     */
    public SortByDepartmentName getSortByDepartmentName() {
        return sortByDepartmentName;
    }

    /**
     * Legt den Wert der sortByDepartmentName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByDepartmentName }
     *     
     */
    public void setSortByDepartmentName(SortByDepartmentName value) {
        this.sortByDepartmentName = value;
    }

    /**
     * Ruft den Wert der sortByGroupName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByGroupName }
     *     
     */
    public SortByGroupName getSortByGroupName() {
        return sortByGroupName;
    }

    /**
     * Legt den Wert der sortByGroupName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByGroupName }
     *     
     */
    public void setSortByGroupName(SortByGroupName value) {
        this.sortByGroupName = value;
    }

    /**
     * Ruft den Wert der sortByYahooId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByYahooId }
     *     
     */
    public SortByYahooId getSortByYahooId() {
        return sortByYahooId;
    }

    /**
     * Legt den Wert der sortByYahooId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByYahooId }
     *     
     */
    public void setSortByYahooId(SortByYahooId value) {
        this.sortByYahooId = value;
    }

    /**
     * Ruft den Wert der sortByUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByUserId }
     *     
     */
    public SortByUserId getSortByUserId() {
        return sortByUserId;
    }

    /**
     * Legt den Wert der sortByUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByUserId }
     *     
     */
    public void setSortByUserId(SortByUserId value) {
        this.sortByUserId = value;
    }

    /**
     * Ruft den Wert der sortByImpId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByImpId }
     *     
     */
    public SortByImpId getSortByImpId() {
        return sortByImpId;
    }

    /**
     * Legt den Wert der sortByImpId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByImpId }
     *     
     */
    public void setSortByImpId(SortByImpId value) {
        this.sortByImpId = value;
    }

}
