//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemVisualDeviceManagementGetDeviceInfoRequest.
 *       
 * 
 * <p>Java-Klasse für SystemVisualDeviceManagementGetDeviceInfoResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemVisualDeviceManagementGetDeviceInfoResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="deviceType" type="{}AccessDeviceType"/>
 *         <element name="supportVisualDeviceManagement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macAddress" type="{}AccessDeviceMACAddress" minOccurs="0"/>
 *         <element name="primaryUser" type="{}PrimaryUserInfo" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemVisualDeviceManagementGetDeviceInfoResponse", propOrder = {
    "deviceType",
    "supportVisualDeviceManagement",
    "macAddress",
    "primaryUser"
})
public class SystemVisualDeviceManagementGetDeviceInfoResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceType;
    protected boolean supportVisualDeviceManagement;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String macAddress;
    protected PrimaryUserInfo primaryUser;

    /**
     * Ruft den Wert der deviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Legt den Wert der deviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceType(String value) {
        this.deviceType = value;
    }

    /**
     * Ruft den Wert der supportVisualDeviceManagement-Eigenschaft ab.
     * 
     */
    public boolean isSupportVisualDeviceManagement() {
        return supportVisualDeviceManagement;
    }

    /**
     * Legt den Wert der supportVisualDeviceManagement-Eigenschaft fest.
     * 
     */
    public void setSupportVisualDeviceManagement(boolean value) {
        this.supportVisualDeviceManagement = value;
    }

    /**
     * Ruft den Wert der macAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacAddress() {
        return macAddress;
    }

    /**
     * Legt den Wert der macAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacAddress(String value) {
        this.macAddress = value;
    }

    /**
     * Ruft den Wert der primaryUser-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PrimaryUserInfo }
     *     
     */
    public PrimaryUserInfo getPrimaryUser() {
        return primaryUser;
    }

    /**
     * Legt den Wert der primaryUser-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PrimaryUserInfo }
     *     
     */
    public void setPrimaryUser(PrimaryUserInfo value) {
        this.primaryUser = value;
    }

}
