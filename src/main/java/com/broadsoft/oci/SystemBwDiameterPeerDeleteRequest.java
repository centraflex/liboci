//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Remove a Diameter peer.  A peer cannot be removed if is referenced by a Realm Routing Table entry.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemBwDiameterPeerDeleteRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBwDiameterPeerDeleteRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="instance" type="{}BwDiameterPeerInstance"/>
 *         <element name="identity" type="{}DomainName"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBwDiameterPeerDeleteRequest", propOrder = {
    "instance",
    "identity"
})
public class SystemBwDiameterPeerDeleteRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BwDiameterPeerInstance instance;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String identity;

    /**
     * Ruft den Wert der instance-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BwDiameterPeerInstance }
     *     
     */
    public BwDiameterPeerInstance getInstance() {
        return instance;
    }

    /**
     * Legt den Wert der instance-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BwDiameterPeerInstance }
     *     
     */
    public void setInstance(BwDiameterPeerInstance value) {
        this.instance = value;
    }

    /**
     * Ruft den Wert der identity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentity() {
        return identity;
    }

    /**
     * Legt den Wert der identity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentity(String value) {
        this.identity = value;
    }

}
