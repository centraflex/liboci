//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserPasswordInfoGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserPasswordInfoGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPasswordInfoGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isLoginDisabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <choice>
 *           <element name="expirationDays" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *           <element name="doesNotExpire" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPasswordInfoGetResponse", propOrder = {
    "isLoginDisabled",
    "expirationDays",
    "doesNotExpire"
})
public class UserPasswordInfoGetResponse
    extends OCIDataResponse
{

    protected boolean isLoginDisabled;
    protected Integer expirationDays;
    protected Boolean doesNotExpire;

    /**
     * Ruft den Wert der isLoginDisabled-Eigenschaft ab.
     * 
     */
    public boolean isIsLoginDisabled() {
        return isLoginDisabled;
    }

    /**
     * Legt den Wert der isLoginDisabled-Eigenschaft fest.
     * 
     */
    public void setIsLoginDisabled(boolean value) {
        this.isLoginDisabled = value;
    }

    /**
     * Ruft den Wert der expirationDays-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExpirationDays() {
        return expirationDays;
    }

    /**
     * Legt den Wert der expirationDays-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExpirationDays(Integer value) {
        this.expirationDays = value;
    }

    /**
     * Ruft den Wert der doesNotExpire-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDoesNotExpire() {
        return doesNotExpire;
    }

    /**
     * Legt den Wert der doesNotExpire-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDoesNotExpire(Boolean value) {
        this.doesNotExpire = value;
    }

}
