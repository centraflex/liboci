//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupOutgoingCallingPlanTransferNumbersGetListRequest.
 *       
 * 
 * <p>Java-Klasse für GroupOutgoingCallingPlanTransferNumbersGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupOutgoingCallingPlanTransferNumbersGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="groupNumbers" type="{}OutgoingCallingPlanTransferNumbers"/>
 *         <element name="departmentNumbers" type="{}OutgoingCallingPlanDepartmentTransferNumbers" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupOutgoingCallingPlanTransferNumbersGetListResponse", propOrder = {
    "groupNumbers",
    "departmentNumbers"
})
public class GroupOutgoingCallingPlanTransferNumbersGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OutgoingCallingPlanTransferNumbers groupNumbers;
    protected List<OutgoingCallingPlanDepartmentTransferNumbers> departmentNumbers;

    /**
     * Ruft den Wert der groupNumbers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanTransferNumbers }
     *     
     */
    public OutgoingCallingPlanTransferNumbers getGroupNumbers() {
        return groupNumbers;
    }

    /**
     * Legt den Wert der groupNumbers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanTransferNumbers }
     *     
     */
    public void setGroupNumbers(OutgoingCallingPlanTransferNumbers value) {
        this.groupNumbers = value;
    }

    /**
     * Gets the value of the departmentNumbers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the departmentNumbers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDepartmentNumbers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OutgoingCallingPlanDepartmentTransferNumbers }
     * 
     * 
     * @return
     *     The value of the departmentNumbers property.
     */
    public List<OutgoingCallingPlanDepartmentTransferNumbers> getDepartmentNumbers() {
        if (departmentNumbers == null) {
            departmentNumbers = new ArrayList<>();
        }
        return this.departmentNumbers;
    }

}
