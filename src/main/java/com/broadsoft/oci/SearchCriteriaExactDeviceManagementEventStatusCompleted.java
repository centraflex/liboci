//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for a particular fully specified Device Management completed event status.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactDeviceManagementEventStatusCompleted complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactDeviceManagementEventStatusCompleted">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="dmEventStatusCompleted" type="{}DeviceManagementEventStatusCompleted"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactDeviceManagementEventStatusCompleted", propOrder = {
    "dmEventStatusCompleted"
})
public class SearchCriteriaExactDeviceManagementEventStatusCompleted
    extends SearchCriteria
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceManagementEventStatusCompleted dmEventStatusCompleted;

    /**
     * Ruft den Wert der dmEventStatusCompleted-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementEventStatusCompleted }
     *     
     */
    public DeviceManagementEventStatusCompleted getDmEventStatusCompleted() {
        return dmEventStatusCompleted;
    }

    /**
     * Legt den Wert der dmEventStatusCompleted-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementEventStatusCompleted }
     *     
     */
    public void setDmEventStatusCompleted(DeviceManagementEventStatusCompleted value) {
        this.dmEventStatusCompleted = value;
    }

}
