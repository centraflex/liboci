//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify the system's default policy settings.
 *         The response is either SuccessResponse or ErrorResponse.
 *         The following elements are only used in AS data mode:
 *             GroupAdminDialableCallerIDAccess
 *             ServiceProviderAdminDialableCallerIDAccess
 *             GroupAdminCommunicationBarringUserProfileAccess (This element is only used for groups in an Enterprise)
 *             GroupAdminVerifyTranslationAndRoutingAccess
 *             ServiceProviderVerifyTranslationAndRoutingAccess
 *             groupUserAutoAttendantNameDialingAccess
 *         The following elements are only used in XS data mode:
 *             serviceProviderAdminCommunicationBarringAccess
 *       
 * 
 * <p>Java-Klasse für SystemPolicyModifyDefaultRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPolicyModifyDefaultRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="groupCallingPlanAccess" type="{}GroupCallingPlanAccess" minOccurs="0"/>
 *         <element name="groupExtensionAccess" type="{}GroupExtensionAccess" minOccurs="0"/>
 *         <element name="groupVoiceMessagingAccess" type="{}GroupVoiceMessagingAccess" minOccurs="0"/>
 *         <element name="groupDepartmentAdminUserAccess" type="{}GroupDepartmentAdminUserAccess" minOccurs="0"/>
 *         <element name="groupDepartmentAdminTrunkGroupAccess" type="{}GroupDepartmentAdminTrunkGroupAccess" minOccurs="0"/>
 *         <element name="groupDepartmentAdminPhoneNumberExtensionAccess" type="{}GroupDepartmentAdminPhoneNumberExtensionAccess" minOccurs="0"/>
 *         <element name="groupDepartmentAdminCallingLineIdNumberAccess" type="{}GroupDepartmentAdminCallingLineIdNumberAccess" minOccurs="0"/>
 *         <element name="groupUserAuthenticationAccess" type="{}GroupUserAuthenticationAccess" minOccurs="0"/>
 *         <element name="groupUserGroupDirectoryAccess" type="{}GroupUserGroupDirectoryAccess" minOccurs="0"/>
 *         <element name="groupUserProfileAccess" type="{}GroupUserProfileAccess" minOccurs="0"/>
 *         <element name="groupUserEnhancedCallLogsAccess" type="{}GroupUserCallLogAccess" minOccurs="0"/>
 *         <element name="groupUserAutoAttendantNameDialingAccess" type="{}GroupUserAutoAttendantNameDialingAccess" minOccurs="0"/>
 *         <element name="groupAdminProfileAccess" type="{}GroupAdminProfileAccess" minOccurs="0"/>
 *         <element name="groupAdminUserAccess" type="{}GroupAdminUserAccess" minOccurs="0"/>
 *         <element name="groupAdminAdminAccess" type="{}GroupAdminAdminAccess" minOccurs="0"/>
 *         <element name="groupAdminDepartmentAccess" type="{}GroupAdminDepartmentAccess" minOccurs="0"/>
 *         <element name="groupAdminAccessDeviceAccess" type="{}GroupAdminAccessDeviceAccess" minOccurs="0"/>
 *         <element name="groupAdminEnhancedServiceInstanceAccess" type="{}GroupAdminEnhancedServiceInstanceAccess" minOccurs="0"/>
 *         <element name="groupAdminFeatureAccessCodeAccess" type="{}GroupAdminFeatureAccessCodeAccess" minOccurs="0"/>
 *         <element name="groupAdminPhoneNumberExtensionAccess" type="{}GroupAdminPhoneNumberExtensionAccess" minOccurs="0"/>
 *         <element name="groupAdminCallingLineIdNumberAccess" type="{}GroupAdminCallingLineIdNumberAccess" minOccurs="0"/>
 *         <element name="groupAdminServiceAccess" type="{}GroupAdminServiceAccess" minOccurs="0"/>
 *         <element name="groupAdminTrunkGroupAccess" type="{}GroupAdminTrunkGroupAccess" minOccurs="0"/>
 *         <element name="groupAdminVerifyTranslationAndRoutingAccess" type="{}GroupAdminVerifyTranslationAndRoutingAccess" minOccurs="0"/>
 *         <element name="groupAdminSessionAdmissionControlAccess" type="{}GroupAdminSessionAdmissionControlAccess" minOccurs="0"/>
 *         <element name="groupAdminDialableCallerIDAccess" type="{}GroupAdminDialableCallerIDAccess" minOccurs="0"/>
 *         <element name="groupAdminOfficeZoneAccess" type="{}GroupAdminOfficeZoneAccess" minOccurs="0"/>
 *         <element name="groupAdminNumberActivationAccess" type="{}GroupAdminNumberActivationAccess" minOccurs="0"/>
 *         <element name="groupAdminCommunicationBarringUserProfileAccess" type="{}GroupAdminCommunicationBarringUserProfileAccess" minOccurs="0"/>
 *         <element name="serviceProviderAdminProfileAccess" type="{}ServiceProviderAdminProfileAccess" minOccurs="0"/>
 *         <element name="serviceProviderAdminGroupAccess" type="{}ServiceProviderAdminGroupAccess" minOccurs="0"/>
 *         <element name="serviceProviderAdminUserAccess" type="{}ServiceProviderAdminUserAccess" minOccurs="0"/>
 *         <element name="serviceProviderAdminAdminAccess" type="{}ServiceProviderAdminAdminAccess" minOccurs="0"/>
 *         <element name="ServiceProviderAdminDepartmentAccess" type="{}ServiceProviderAdminDepartmentAccess" minOccurs="0"/>
 *         <element name="serviceProviderAdminAccessDeviceAccess" type="{}ServiceProviderAdminAccessDeviceAccess" minOccurs="0"/>
 *         <element name="serviceProviderAdminPhoneNumberExtensionAccess" type="{}ServiceProviderAdminPhoneNumberExtensionAccess" minOccurs="0"/>
 *         <element name="serviceProviderAdminCallingLineIdNumberAccess" type="{}ServiceProviderAdminCallingLineIdNumberAccess" minOccurs="0"/>
 *         <element name="serviceProviderAdminServiceAccess" type="{}ServiceProviderAdminServiceAccess" minOccurs="0"/>
 *         <element name="serviceProviderAdminServicePackAccess" type="{}ServiceProviderAdminServicePackAccess" minOccurs="0"/>
 *         <element name="serviceProviderAdminSessionAdmissionControlAccess" type="{}ServiceProviderAdminSessionAdmissionControlAccess" minOccurs="0"/>
 *         <element name="serviceProviderAdminVerifyTranslationAndRoutingAccess" type="{}ServiceProviderAdminVerifyTranslationAndRoutingAccess" minOccurs="0"/>
 *         <element name="serviceProviderAdminWebBrandingAccess" type="{}ServiceProviderAdminWebBrandingAccess" minOccurs="0"/>
 *         <element name="serviceProviderAdminOfficeZoneAccess" type="{}ServiceProviderAdminOfficeZoneAccess" minOccurs="0"/>
 *         <element name="serviceProviderAdminCommunicationBarringAccess" type="{}ServiceProviderAdminCommunicationBarringAccess" minOccurs="0"/>
 *         <element name="enterpriseAdminNetworkPolicyAccess" type="{}EnterpriseAdminNetworkPolicyAccess" minOccurs="0"/>
 *         <element name="serviceProviderAdminDialableCallerIDAccess" type="{}ServiceProviderAdminDialableCallerIDAccess" minOccurs="0"/>
 *         <element name="enterpriseAdminNumberActivationAccess" type="{}EnterpriseAdminNumberActivationAccess" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPolicyModifyDefaultRequest22", propOrder = {
    "groupCallingPlanAccess",
    "groupExtensionAccess",
    "groupVoiceMessagingAccess",
    "groupDepartmentAdminUserAccess",
    "groupDepartmentAdminTrunkGroupAccess",
    "groupDepartmentAdminPhoneNumberExtensionAccess",
    "groupDepartmentAdminCallingLineIdNumberAccess",
    "groupUserAuthenticationAccess",
    "groupUserGroupDirectoryAccess",
    "groupUserProfileAccess",
    "groupUserEnhancedCallLogsAccess",
    "groupUserAutoAttendantNameDialingAccess",
    "groupAdminProfileAccess",
    "groupAdminUserAccess",
    "groupAdminAdminAccess",
    "groupAdminDepartmentAccess",
    "groupAdminAccessDeviceAccess",
    "groupAdminEnhancedServiceInstanceAccess",
    "groupAdminFeatureAccessCodeAccess",
    "groupAdminPhoneNumberExtensionAccess",
    "groupAdminCallingLineIdNumberAccess",
    "groupAdminServiceAccess",
    "groupAdminTrunkGroupAccess",
    "groupAdminVerifyTranslationAndRoutingAccess",
    "groupAdminSessionAdmissionControlAccess",
    "groupAdminDialableCallerIDAccess",
    "groupAdminOfficeZoneAccess",
    "groupAdminNumberActivationAccess",
    "groupAdminCommunicationBarringUserProfileAccess",
    "serviceProviderAdminProfileAccess",
    "serviceProviderAdminGroupAccess",
    "serviceProviderAdminUserAccess",
    "serviceProviderAdminAdminAccess",
    "serviceProviderAdminDepartmentAccess",
    "serviceProviderAdminAccessDeviceAccess",
    "serviceProviderAdminPhoneNumberExtensionAccess",
    "serviceProviderAdminCallingLineIdNumberAccess",
    "serviceProviderAdminServiceAccess",
    "serviceProviderAdminServicePackAccess",
    "serviceProviderAdminSessionAdmissionControlAccess",
    "serviceProviderAdminVerifyTranslationAndRoutingAccess",
    "serviceProviderAdminWebBrandingAccess",
    "serviceProviderAdminOfficeZoneAccess",
    "serviceProviderAdminCommunicationBarringAccess",
    "enterpriseAdminNetworkPolicyAccess",
    "serviceProviderAdminDialableCallerIDAccess",
    "enterpriseAdminNumberActivationAccess"
})
public class SystemPolicyModifyDefaultRequest22
    extends OCIRequest
{

    @XmlSchemaType(name = "token")
    protected GroupCallingPlanAccess groupCallingPlanAccess;
    @XmlSchemaType(name = "token")
    protected GroupExtensionAccess groupExtensionAccess;
    @XmlSchemaType(name = "token")
    protected GroupVoiceMessagingAccess groupVoiceMessagingAccess;
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminUserAccess groupDepartmentAdminUserAccess;
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminTrunkGroupAccess groupDepartmentAdminTrunkGroupAccess;
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminPhoneNumberExtensionAccess groupDepartmentAdminPhoneNumberExtensionAccess;
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminCallingLineIdNumberAccess groupDepartmentAdminCallingLineIdNumberAccess;
    @XmlSchemaType(name = "token")
    protected GroupUserAuthenticationAccess groupUserAuthenticationAccess;
    @XmlSchemaType(name = "token")
    protected GroupUserGroupDirectoryAccess groupUserGroupDirectoryAccess;
    @XmlSchemaType(name = "token")
    protected GroupUserProfileAccess groupUserProfileAccess;
    @XmlSchemaType(name = "token")
    protected GroupUserCallLogAccess groupUserEnhancedCallLogsAccess;
    @XmlSchemaType(name = "token")
    protected GroupUserAutoAttendantNameDialingAccess groupUserAutoAttendantNameDialingAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminProfileAccess groupAdminProfileAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminUserAccess groupAdminUserAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminAdminAccess groupAdminAdminAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminDepartmentAccess groupAdminDepartmentAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminAccessDeviceAccess groupAdminAccessDeviceAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminEnhancedServiceInstanceAccess groupAdminEnhancedServiceInstanceAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminFeatureAccessCodeAccess groupAdminFeatureAccessCodeAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminPhoneNumberExtensionAccess groupAdminPhoneNumberExtensionAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminCallingLineIdNumberAccess groupAdminCallingLineIdNumberAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminServiceAccess groupAdminServiceAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminTrunkGroupAccess groupAdminTrunkGroupAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminVerifyTranslationAndRoutingAccess groupAdminVerifyTranslationAndRoutingAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminSessionAdmissionControlAccess groupAdminSessionAdmissionControlAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminDialableCallerIDAccess groupAdminDialableCallerIDAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminOfficeZoneAccess groupAdminOfficeZoneAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminNumberActivationAccess groupAdminNumberActivationAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminCommunicationBarringUserProfileAccess groupAdminCommunicationBarringUserProfileAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminProfileAccess serviceProviderAdminProfileAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminGroupAccess serviceProviderAdminGroupAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminUserAccess serviceProviderAdminUserAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminAdminAccess serviceProviderAdminAdminAccess;
    @XmlElement(name = "ServiceProviderAdminDepartmentAccess")
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminDepartmentAccess serviceProviderAdminDepartmentAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminAccessDeviceAccess serviceProviderAdminAccessDeviceAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminPhoneNumberExtensionAccess serviceProviderAdminPhoneNumberExtensionAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminCallingLineIdNumberAccess serviceProviderAdminCallingLineIdNumberAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminServiceAccess serviceProviderAdminServiceAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminServicePackAccess serviceProviderAdminServicePackAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminSessionAdmissionControlAccess serviceProviderAdminSessionAdmissionControlAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminVerifyTranslationAndRoutingAccess serviceProviderAdminVerifyTranslationAndRoutingAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminWebBrandingAccess serviceProviderAdminWebBrandingAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminOfficeZoneAccess serviceProviderAdminOfficeZoneAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminCommunicationBarringAccess serviceProviderAdminCommunicationBarringAccess;
    @XmlSchemaType(name = "token")
    protected EnterpriseAdminNetworkPolicyAccess enterpriseAdminNetworkPolicyAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminDialableCallerIDAccess serviceProviderAdminDialableCallerIDAccess;
    @XmlSchemaType(name = "token")
    protected EnterpriseAdminNumberActivationAccess enterpriseAdminNumberActivationAccess;

    /**
     * Ruft den Wert der groupCallingPlanAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupCallingPlanAccess }
     *     
     */
    public GroupCallingPlanAccess getGroupCallingPlanAccess() {
        return groupCallingPlanAccess;
    }

    /**
     * Legt den Wert der groupCallingPlanAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupCallingPlanAccess }
     *     
     */
    public void setGroupCallingPlanAccess(GroupCallingPlanAccess value) {
        this.groupCallingPlanAccess = value;
    }

    /**
     * Ruft den Wert der groupExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupExtensionAccess }
     *     
     */
    public GroupExtensionAccess getGroupExtensionAccess() {
        return groupExtensionAccess;
    }

    /**
     * Legt den Wert der groupExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupExtensionAccess }
     *     
     */
    public void setGroupExtensionAccess(GroupExtensionAccess value) {
        this.groupExtensionAccess = value;
    }

    /**
     * Ruft den Wert der groupVoiceMessagingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupVoiceMessagingAccess }
     *     
     */
    public GroupVoiceMessagingAccess getGroupVoiceMessagingAccess() {
        return groupVoiceMessagingAccess;
    }

    /**
     * Legt den Wert der groupVoiceMessagingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupVoiceMessagingAccess }
     *     
     */
    public void setGroupVoiceMessagingAccess(GroupVoiceMessagingAccess value) {
        this.groupVoiceMessagingAccess = value;
    }

    /**
     * Ruft den Wert der groupDepartmentAdminUserAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminUserAccess }
     *     
     */
    public GroupDepartmentAdminUserAccess getGroupDepartmentAdminUserAccess() {
        return groupDepartmentAdminUserAccess;
    }

    /**
     * Legt den Wert der groupDepartmentAdminUserAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminUserAccess }
     *     
     */
    public void setGroupDepartmentAdminUserAccess(GroupDepartmentAdminUserAccess value) {
        this.groupDepartmentAdminUserAccess = value;
    }

    /**
     * Ruft den Wert der groupDepartmentAdminTrunkGroupAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminTrunkGroupAccess }
     *     
     */
    public GroupDepartmentAdminTrunkGroupAccess getGroupDepartmentAdminTrunkGroupAccess() {
        return groupDepartmentAdminTrunkGroupAccess;
    }

    /**
     * Legt den Wert der groupDepartmentAdminTrunkGroupAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminTrunkGroupAccess }
     *     
     */
    public void setGroupDepartmentAdminTrunkGroupAccess(GroupDepartmentAdminTrunkGroupAccess value) {
        this.groupDepartmentAdminTrunkGroupAccess = value;
    }

    /**
     * Ruft den Wert der groupDepartmentAdminPhoneNumberExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminPhoneNumberExtensionAccess }
     *     
     */
    public GroupDepartmentAdminPhoneNumberExtensionAccess getGroupDepartmentAdminPhoneNumberExtensionAccess() {
        return groupDepartmentAdminPhoneNumberExtensionAccess;
    }

    /**
     * Legt den Wert der groupDepartmentAdminPhoneNumberExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminPhoneNumberExtensionAccess }
     *     
     */
    public void setGroupDepartmentAdminPhoneNumberExtensionAccess(GroupDepartmentAdminPhoneNumberExtensionAccess value) {
        this.groupDepartmentAdminPhoneNumberExtensionAccess = value;
    }

    /**
     * Ruft den Wert der groupDepartmentAdminCallingLineIdNumberAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminCallingLineIdNumberAccess }
     *     
     */
    public GroupDepartmentAdminCallingLineIdNumberAccess getGroupDepartmentAdminCallingLineIdNumberAccess() {
        return groupDepartmentAdminCallingLineIdNumberAccess;
    }

    /**
     * Legt den Wert der groupDepartmentAdminCallingLineIdNumberAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminCallingLineIdNumberAccess }
     *     
     */
    public void setGroupDepartmentAdminCallingLineIdNumberAccess(GroupDepartmentAdminCallingLineIdNumberAccess value) {
        this.groupDepartmentAdminCallingLineIdNumberAccess = value;
    }

    /**
     * Ruft den Wert der groupUserAuthenticationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserAuthenticationAccess }
     *     
     */
    public GroupUserAuthenticationAccess getGroupUserAuthenticationAccess() {
        return groupUserAuthenticationAccess;
    }

    /**
     * Legt den Wert der groupUserAuthenticationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserAuthenticationAccess }
     *     
     */
    public void setGroupUserAuthenticationAccess(GroupUserAuthenticationAccess value) {
        this.groupUserAuthenticationAccess = value;
    }

    /**
     * Ruft den Wert der groupUserGroupDirectoryAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserGroupDirectoryAccess }
     *     
     */
    public GroupUserGroupDirectoryAccess getGroupUserGroupDirectoryAccess() {
        return groupUserGroupDirectoryAccess;
    }

    /**
     * Legt den Wert der groupUserGroupDirectoryAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserGroupDirectoryAccess }
     *     
     */
    public void setGroupUserGroupDirectoryAccess(GroupUserGroupDirectoryAccess value) {
        this.groupUserGroupDirectoryAccess = value;
    }

    /**
     * Ruft den Wert der groupUserProfileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserProfileAccess }
     *     
     */
    public GroupUserProfileAccess getGroupUserProfileAccess() {
        return groupUserProfileAccess;
    }

    /**
     * Legt den Wert der groupUserProfileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserProfileAccess }
     *     
     */
    public void setGroupUserProfileAccess(GroupUserProfileAccess value) {
        this.groupUserProfileAccess = value;
    }

    /**
     * Ruft den Wert der groupUserEnhancedCallLogsAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserCallLogAccess }
     *     
     */
    public GroupUserCallLogAccess getGroupUserEnhancedCallLogsAccess() {
        return groupUserEnhancedCallLogsAccess;
    }

    /**
     * Legt den Wert der groupUserEnhancedCallLogsAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserCallLogAccess }
     *     
     */
    public void setGroupUserEnhancedCallLogsAccess(GroupUserCallLogAccess value) {
        this.groupUserEnhancedCallLogsAccess = value;
    }

    /**
     * Ruft den Wert der groupUserAutoAttendantNameDialingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserAutoAttendantNameDialingAccess }
     *     
     */
    public GroupUserAutoAttendantNameDialingAccess getGroupUserAutoAttendantNameDialingAccess() {
        return groupUserAutoAttendantNameDialingAccess;
    }

    /**
     * Legt den Wert der groupUserAutoAttendantNameDialingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserAutoAttendantNameDialingAccess }
     *     
     */
    public void setGroupUserAutoAttendantNameDialingAccess(GroupUserAutoAttendantNameDialingAccess value) {
        this.groupUserAutoAttendantNameDialingAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminProfileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminProfileAccess }
     *     
     */
    public GroupAdminProfileAccess getGroupAdminProfileAccess() {
        return groupAdminProfileAccess;
    }

    /**
     * Legt den Wert der groupAdminProfileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminProfileAccess }
     *     
     */
    public void setGroupAdminProfileAccess(GroupAdminProfileAccess value) {
        this.groupAdminProfileAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminUserAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminUserAccess }
     *     
     */
    public GroupAdminUserAccess getGroupAdminUserAccess() {
        return groupAdminUserAccess;
    }

    /**
     * Legt den Wert der groupAdminUserAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminUserAccess }
     *     
     */
    public void setGroupAdminUserAccess(GroupAdminUserAccess value) {
        this.groupAdminUserAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminAdminAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminAdminAccess }
     *     
     */
    public GroupAdminAdminAccess getGroupAdminAdminAccess() {
        return groupAdminAdminAccess;
    }

    /**
     * Legt den Wert der groupAdminAdminAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminAdminAccess }
     *     
     */
    public void setGroupAdminAdminAccess(GroupAdminAdminAccess value) {
        this.groupAdminAdminAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminDepartmentAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminDepartmentAccess }
     *     
     */
    public GroupAdminDepartmentAccess getGroupAdminDepartmentAccess() {
        return groupAdminDepartmentAccess;
    }

    /**
     * Legt den Wert der groupAdminDepartmentAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminDepartmentAccess }
     *     
     */
    public void setGroupAdminDepartmentAccess(GroupAdminDepartmentAccess value) {
        this.groupAdminDepartmentAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminAccessDeviceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminAccessDeviceAccess }
     *     
     */
    public GroupAdminAccessDeviceAccess getGroupAdminAccessDeviceAccess() {
        return groupAdminAccessDeviceAccess;
    }

    /**
     * Legt den Wert der groupAdminAccessDeviceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminAccessDeviceAccess }
     *     
     */
    public void setGroupAdminAccessDeviceAccess(GroupAdminAccessDeviceAccess value) {
        this.groupAdminAccessDeviceAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminEnhancedServiceInstanceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminEnhancedServiceInstanceAccess }
     *     
     */
    public GroupAdminEnhancedServiceInstanceAccess getGroupAdminEnhancedServiceInstanceAccess() {
        return groupAdminEnhancedServiceInstanceAccess;
    }

    /**
     * Legt den Wert der groupAdminEnhancedServiceInstanceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminEnhancedServiceInstanceAccess }
     *     
     */
    public void setGroupAdminEnhancedServiceInstanceAccess(GroupAdminEnhancedServiceInstanceAccess value) {
        this.groupAdminEnhancedServiceInstanceAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminFeatureAccessCodeAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminFeatureAccessCodeAccess }
     *     
     */
    public GroupAdminFeatureAccessCodeAccess getGroupAdminFeatureAccessCodeAccess() {
        return groupAdminFeatureAccessCodeAccess;
    }

    /**
     * Legt den Wert der groupAdminFeatureAccessCodeAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminFeatureAccessCodeAccess }
     *     
     */
    public void setGroupAdminFeatureAccessCodeAccess(GroupAdminFeatureAccessCodeAccess value) {
        this.groupAdminFeatureAccessCodeAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminPhoneNumberExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminPhoneNumberExtensionAccess }
     *     
     */
    public GroupAdminPhoneNumberExtensionAccess getGroupAdminPhoneNumberExtensionAccess() {
        return groupAdminPhoneNumberExtensionAccess;
    }

    /**
     * Legt den Wert der groupAdminPhoneNumberExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminPhoneNumberExtensionAccess }
     *     
     */
    public void setGroupAdminPhoneNumberExtensionAccess(GroupAdminPhoneNumberExtensionAccess value) {
        this.groupAdminPhoneNumberExtensionAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminCallingLineIdNumberAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminCallingLineIdNumberAccess }
     *     
     */
    public GroupAdminCallingLineIdNumberAccess getGroupAdminCallingLineIdNumberAccess() {
        return groupAdminCallingLineIdNumberAccess;
    }

    /**
     * Legt den Wert der groupAdminCallingLineIdNumberAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminCallingLineIdNumberAccess }
     *     
     */
    public void setGroupAdminCallingLineIdNumberAccess(GroupAdminCallingLineIdNumberAccess value) {
        this.groupAdminCallingLineIdNumberAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminServiceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminServiceAccess }
     *     
     */
    public GroupAdminServiceAccess getGroupAdminServiceAccess() {
        return groupAdminServiceAccess;
    }

    /**
     * Legt den Wert der groupAdminServiceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminServiceAccess }
     *     
     */
    public void setGroupAdminServiceAccess(GroupAdminServiceAccess value) {
        this.groupAdminServiceAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminTrunkGroupAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminTrunkGroupAccess }
     *     
     */
    public GroupAdminTrunkGroupAccess getGroupAdminTrunkGroupAccess() {
        return groupAdminTrunkGroupAccess;
    }

    /**
     * Legt den Wert der groupAdminTrunkGroupAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminTrunkGroupAccess }
     *     
     */
    public void setGroupAdminTrunkGroupAccess(GroupAdminTrunkGroupAccess value) {
        this.groupAdminTrunkGroupAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminVerifyTranslationAndRoutingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminVerifyTranslationAndRoutingAccess }
     *     
     */
    public GroupAdminVerifyTranslationAndRoutingAccess getGroupAdminVerifyTranslationAndRoutingAccess() {
        return groupAdminVerifyTranslationAndRoutingAccess;
    }

    /**
     * Legt den Wert der groupAdminVerifyTranslationAndRoutingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminVerifyTranslationAndRoutingAccess }
     *     
     */
    public void setGroupAdminVerifyTranslationAndRoutingAccess(GroupAdminVerifyTranslationAndRoutingAccess value) {
        this.groupAdminVerifyTranslationAndRoutingAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminSessionAdmissionControlAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminSessionAdmissionControlAccess }
     *     
     */
    public GroupAdminSessionAdmissionControlAccess getGroupAdminSessionAdmissionControlAccess() {
        return groupAdminSessionAdmissionControlAccess;
    }

    /**
     * Legt den Wert der groupAdminSessionAdmissionControlAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminSessionAdmissionControlAccess }
     *     
     */
    public void setGroupAdminSessionAdmissionControlAccess(GroupAdminSessionAdmissionControlAccess value) {
        this.groupAdminSessionAdmissionControlAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminDialableCallerIDAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminDialableCallerIDAccess }
     *     
     */
    public GroupAdminDialableCallerIDAccess getGroupAdminDialableCallerIDAccess() {
        return groupAdminDialableCallerIDAccess;
    }

    /**
     * Legt den Wert der groupAdminDialableCallerIDAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminDialableCallerIDAccess }
     *     
     */
    public void setGroupAdminDialableCallerIDAccess(GroupAdminDialableCallerIDAccess value) {
        this.groupAdminDialableCallerIDAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminOfficeZoneAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminOfficeZoneAccess }
     *     
     */
    public GroupAdminOfficeZoneAccess getGroupAdminOfficeZoneAccess() {
        return groupAdminOfficeZoneAccess;
    }

    /**
     * Legt den Wert der groupAdminOfficeZoneAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminOfficeZoneAccess }
     *     
     */
    public void setGroupAdminOfficeZoneAccess(GroupAdminOfficeZoneAccess value) {
        this.groupAdminOfficeZoneAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminNumberActivationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminNumberActivationAccess }
     *     
     */
    public GroupAdminNumberActivationAccess getGroupAdminNumberActivationAccess() {
        return groupAdminNumberActivationAccess;
    }

    /**
     * Legt den Wert der groupAdminNumberActivationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminNumberActivationAccess }
     *     
     */
    public void setGroupAdminNumberActivationAccess(GroupAdminNumberActivationAccess value) {
        this.groupAdminNumberActivationAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminCommunicationBarringUserProfileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminCommunicationBarringUserProfileAccess }
     *     
     */
    public GroupAdminCommunicationBarringUserProfileAccess getGroupAdminCommunicationBarringUserProfileAccess() {
        return groupAdminCommunicationBarringUserProfileAccess;
    }

    /**
     * Legt den Wert der groupAdminCommunicationBarringUserProfileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminCommunicationBarringUserProfileAccess }
     *     
     */
    public void setGroupAdminCommunicationBarringUserProfileAccess(GroupAdminCommunicationBarringUserProfileAccess value) {
        this.groupAdminCommunicationBarringUserProfileAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminProfileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminProfileAccess }
     *     
     */
    public ServiceProviderAdminProfileAccess getServiceProviderAdminProfileAccess() {
        return serviceProviderAdminProfileAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminProfileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminProfileAccess }
     *     
     */
    public void setServiceProviderAdminProfileAccess(ServiceProviderAdminProfileAccess value) {
        this.serviceProviderAdminProfileAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminGroupAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminGroupAccess }
     *     
     */
    public ServiceProviderAdminGroupAccess getServiceProviderAdminGroupAccess() {
        return serviceProviderAdminGroupAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminGroupAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminGroupAccess }
     *     
     */
    public void setServiceProviderAdminGroupAccess(ServiceProviderAdminGroupAccess value) {
        this.serviceProviderAdminGroupAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminUserAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminUserAccess }
     *     
     */
    public ServiceProviderAdminUserAccess getServiceProviderAdminUserAccess() {
        return serviceProviderAdminUserAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminUserAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminUserAccess }
     *     
     */
    public void setServiceProviderAdminUserAccess(ServiceProviderAdminUserAccess value) {
        this.serviceProviderAdminUserAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminAdminAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminAdminAccess }
     *     
     */
    public ServiceProviderAdminAdminAccess getServiceProviderAdminAdminAccess() {
        return serviceProviderAdminAdminAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminAdminAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminAdminAccess }
     *     
     */
    public void setServiceProviderAdminAdminAccess(ServiceProviderAdminAdminAccess value) {
        this.serviceProviderAdminAdminAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminDepartmentAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminDepartmentAccess }
     *     
     */
    public ServiceProviderAdminDepartmentAccess getServiceProviderAdminDepartmentAccess() {
        return serviceProviderAdminDepartmentAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminDepartmentAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminDepartmentAccess }
     *     
     */
    public void setServiceProviderAdminDepartmentAccess(ServiceProviderAdminDepartmentAccess value) {
        this.serviceProviderAdminDepartmentAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminAccessDeviceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminAccessDeviceAccess }
     *     
     */
    public ServiceProviderAdminAccessDeviceAccess getServiceProviderAdminAccessDeviceAccess() {
        return serviceProviderAdminAccessDeviceAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminAccessDeviceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminAccessDeviceAccess }
     *     
     */
    public void setServiceProviderAdminAccessDeviceAccess(ServiceProviderAdminAccessDeviceAccess value) {
        this.serviceProviderAdminAccessDeviceAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminPhoneNumberExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminPhoneNumberExtensionAccess }
     *     
     */
    public ServiceProviderAdminPhoneNumberExtensionAccess getServiceProviderAdminPhoneNumberExtensionAccess() {
        return serviceProviderAdminPhoneNumberExtensionAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminPhoneNumberExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminPhoneNumberExtensionAccess }
     *     
     */
    public void setServiceProviderAdminPhoneNumberExtensionAccess(ServiceProviderAdminPhoneNumberExtensionAccess value) {
        this.serviceProviderAdminPhoneNumberExtensionAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminCallingLineIdNumberAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminCallingLineIdNumberAccess }
     *     
     */
    public ServiceProviderAdminCallingLineIdNumberAccess getServiceProviderAdminCallingLineIdNumberAccess() {
        return serviceProviderAdminCallingLineIdNumberAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminCallingLineIdNumberAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminCallingLineIdNumberAccess }
     *     
     */
    public void setServiceProviderAdminCallingLineIdNumberAccess(ServiceProviderAdminCallingLineIdNumberAccess value) {
        this.serviceProviderAdminCallingLineIdNumberAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminServiceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminServiceAccess }
     *     
     */
    public ServiceProviderAdminServiceAccess getServiceProviderAdminServiceAccess() {
        return serviceProviderAdminServiceAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminServiceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminServiceAccess }
     *     
     */
    public void setServiceProviderAdminServiceAccess(ServiceProviderAdminServiceAccess value) {
        this.serviceProviderAdminServiceAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminServicePackAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminServicePackAccess }
     *     
     */
    public ServiceProviderAdminServicePackAccess getServiceProviderAdminServicePackAccess() {
        return serviceProviderAdminServicePackAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminServicePackAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminServicePackAccess }
     *     
     */
    public void setServiceProviderAdminServicePackAccess(ServiceProviderAdminServicePackAccess value) {
        this.serviceProviderAdminServicePackAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminSessionAdmissionControlAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminSessionAdmissionControlAccess }
     *     
     */
    public ServiceProviderAdminSessionAdmissionControlAccess getServiceProviderAdminSessionAdmissionControlAccess() {
        return serviceProviderAdminSessionAdmissionControlAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminSessionAdmissionControlAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminSessionAdmissionControlAccess }
     *     
     */
    public void setServiceProviderAdminSessionAdmissionControlAccess(ServiceProviderAdminSessionAdmissionControlAccess value) {
        this.serviceProviderAdminSessionAdmissionControlAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminVerifyTranslationAndRoutingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminVerifyTranslationAndRoutingAccess }
     *     
     */
    public ServiceProviderAdminVerifyTranslationAndRoutingAccess getServiceProviderAdminVerifyTranslationAndRoutingAccess() {
        return serviceProviderAdminVerifyTranslationAndRoutingAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminVerifyTranslationAndRoutingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminVerifyTranslationAndRoutingAccess }
     *     
     */
    public void setServiceProviderAdminVerifyTranslationAndRoutingAccess(ServiceProviderAdminVerifyTranslationAndRoutingAccess value) {
        this.serviceProviderAdminVerifyTranslationAndRoutingAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminWebBrandingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminWebBrandingAccess }
     *     
     */
    public ServiceProviderAdminWebBrandingAccess getServiceProviderAdminWebBrandingAccess() {
        return serviceProviderAdminWebBrandingAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminWebBrandingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminWebBrandingAccess }
     *     
     */
    public void setServiceProviderAdminWebBrandingAccess(ServiceProviderAdminWebBrandingAccess value) {
        this.serviceProviderAdminWebBrandingAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminOfficeZoneAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminOfficeZoneAccess }
     *     
     */
    public ServiceProviderAdminOfficeZoneAccess getServiceProviderAdminOfficeZoneAccess() {
        return serviceProviderAdminOfficeZoneAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminOfficeZoneAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminOfficeZoneAccess }
     *     
     */
    public void setServiceProviderAdminOfficeZoneAccess(ServiceProviderAdminOfficeZoneAccess value) {
        this.serviceProviderAdminOfficeZoneAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminCommunicationBarringAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminCommunicationBarringAccess }
     *     
     */
    public ServiceProviderAdminCommunicationBarringAccess getServiceProviderAdminCommunicationBarringAccess() {
        return serviceProviderAdminCommunicationBarringAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminCommunicationBarringAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminCommunicationBarringAccess }
     *     
     */
    public void setServiceProviderAdminCommunicationBarringAccess(ServiceProviderAdminCommunicationBarringAccess value) {
        this.serviceProviderAdminCommunicationBarringAccess = value;
    }

    /**
     * Ruft den Wert der enterpriseAdminNetworkPolicyAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseAdminNetworkPolicyAccess }
     *     
     */
    public EnterpriseAdminNetworkPolicyAccess getEnterpriseAdminNetworkPolicyAccess() {
        return enterpriseAdminNetworkPolicyAccess;
    }

    /**
     * Legt den Wert der enterpriseAdminNetworkPolicyAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseAdminNetworkPolicyAccess }
     *     
     */
    public void setEnterpriseAdminNetworkPolicyAccess(EnterpriseAdminNetworkPolicyAccess value) {
        this.enterpriseAdminNetworkPolicyAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminDialableCallerIDAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminDialableCallerIDAccess }
     *     
     */
    public ServiceProviderAdminDialableCallerIDAccess getServiceProviderAdminDialableCallerIDAccess() {
        return serviceProviderAdminDialableCallerIDAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminDialableCallerIDAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminDialableCallerIDAccess }
     *     
     */
    public void setServiceProviderAdminDialableCallerIDAccess(ServiceProviderAdminDialableCallerIDAccess value) {
        this.serviceProviderAdminDialableCallerIDAccess = value;
    }

    /**
     * Ruft den Wert der enterpriseAdminNumberActivationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseAdminNumberActivationAccess }
     *     
     */
    public EnterpriseAdminNumberActivationAccess getEnterpriseAdminNumberActivationAccess() {
        return enterpriseAdminNumberActivationAccess;
    }

    /**
     * Legt den Wert der enterpriseAdminNumberActivationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseAdminNumberActivationAccess }
     *     
     */
    public void setEnterpriseAdminNumberActivationAccess(EnterpriseAdminNumberActivationAccess value) {
        this.enterpriseAdminNumberActivationAccess = value;
    }

}
