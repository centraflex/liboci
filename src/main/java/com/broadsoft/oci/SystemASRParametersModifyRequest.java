//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify Application Server Registration system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemASRParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemASRParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="maxTransmissions" type="{}ASRMaxTransmissions" minOccurs="0"/>
 *         <element name="retransmissionDelayMilliSeconds" type="{}ASRRetransmissionDelayMilliSeconds" minOccurs="0"/>
 *         <element name="listeningPort" type="{}Port1025" minOccurs="0"/>
 *         <element name="sourceAddress" type="{}NetAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemASRParametersModifyRequest", propOrder = {
    "maxTransmissions",
    "retransmissionDelayMilliSeconds",
    "listeningPort",
    "sourceAddress"
})
public class SystemASRParametersModifyRequest
    extends OCIRequest
{

    protected Integer maxTransmissions;
    protected Integer retransmissionDelayMilliSeconds;
    protected Integer listeningPort;
    @XmlElementRef(name = "sourceAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sourceAddress;

    /**
     * Ruft den Wert der maxTransmissions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxTransmissions() {
        return maxTransmissions;
    }

    /**
     * Legt den Wert der maxTransmissions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxTransmissions(Integer value) {
        this.maxTransmissions = value;
    }

    /**
     * Ruft den Wert der retransmissionDelayMilliSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRetransmissionDelayMilliSeconds() {
        return retransmissionDelayMilliSeconds;
    }

    /**
     * Legt den Wert der retransmissionDelayMilliSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRetransmissionDelayMilliSeconds(Integer value) {
        this.retransmissionDelayMilliSeconds = value;
    }

    /**
     * Ruft den Wert der listeningPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getListeningPort() {
        return listeningPort;
    }

    /**
     * Legt den Wert der listeningPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setListeningPort(Integer value) {
        this.listeningPort = value;
    }

    /**
     * Ruft den Wert der sourceAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSourceAddress() {
        return sourceAddress;
    }

    /**
     * Legt den Wert der sourceAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSourceAddress(JAXBElement<String> value) {
        this.sourceAddress = value;
    }

}
