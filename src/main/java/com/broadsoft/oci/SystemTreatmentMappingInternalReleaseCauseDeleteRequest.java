//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Delete an Internal Release Cause mapping.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemTreatmentMappingInternalReleaseCauseDeleteRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemTreatmentMappingInternalReleaseCauseDeleteRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="internalReleaseCause" type="{}InternalReleaseCause16"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemTreatmentMappingInternalReleaseCauseDeleteRequest", propOrder = {
    "internalReleaseCause"
})
public class SystemTreatmentMappingInternalReleaseCauseDeleteRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected InternalReleaseCause16 internalReleaseCause;

    /**
     * Ruft den Wert der internalReleaseCause-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InternalReleaseCause16 }
     *     
     */
    public InternalReleaseCause16 getInternalReleaseCause() {
        return internalReleaseCause;
    }

    /**
     * Legt den Wert der internalReleaseCause-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InternalReleaseCause16 }
     *     
     */
    public void setInternalReleaseCause(InternalReleaseCause16 value) {
        this.internalReleaseCause = value;
    }

}
