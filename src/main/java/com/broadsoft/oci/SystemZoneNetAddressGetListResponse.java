//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *     Response to SystemZoneNetAddressGetListRequest.
 *       
 * 
 * <p>Java-Klasse für SystemZoneNetAddressGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemZoneNetAddressGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="netAddress" type="{}IPAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="netAddressRange" type="{}IPAddressRange" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemZoneNetAddressGetListResponse", propOrder = {
    "netAddress",
    "netAddressRange"
})
public class SystemZoneNetAddressGetListResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> netAddress;
    protected List<IPAddressRange> netAddressRange;

    /**
     * Gets the value of the netAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the netAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNetAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the netAddress property.
     */
    public List<String> getNetAddress() {
        if (netAddress == null) {
            netAddress = new ArrayList<>();
        }
        return this.netAddress;
    }

    /**
     * Gets the value of the netAddressRange property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the netAddressRange property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNetAddressRange().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IPAddressRange }
     * 
     * 
     * @return
     *     The value of the netAddressRange property.
     */
    public List<IPAddressRange> getNetAddressRange() {
        if (netAddressRange == null) {
            netAddressRange = new ArrayList<>();
        }
        return this.netAddressRange;
    }

}
