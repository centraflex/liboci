//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Get a group's common phone list.
 *         The response is either a GroupCommonPhoneListGetListResponse or an ErrorResponse.
 *         The search can be done using multiple criterion.
 *         If the searchCriteriaModeOr is present, any result matching any one criteria is included in the results. 
 *         Otherwise, only results matching all the search criterion are included in the results. 
 *         If no search criteria is specified, all results are returned.
 *         Specifying searchCriteriaModeOr without any search criteria results in an ErrorResponse.
 *         In all cases, if a responseSizeLimit is specified and the number of matching results is more than this limit, then an
 *         ErrorResponse is returned.
 *       
 * 
 * <p>Java-Klasse für GroupCommonPhoneListGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCommonPhoneListGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaModeOr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="searchCriteriaGroupCommonPhoneListName" type="{}SearchCriteriaGroupCommonPhoneListName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaGroupCommonPhoneListNumber" type="{}SearchCriteriaGroupCommonPhoneListNumber" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCommonPhoneListGetListRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "responseSizeLimit",
    "searchCriteriaModeOr",
    "searchCriteriaGroupCommonPhoneListName",
    "searchCriteriaGroupCommonPhoneListNumber"
})
public class GroupCommonPhoneListGetListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected Integer responseSizeLimit;
    protected Boolean searchCriteriaModeOr;
    protected List<SearchCriteriaGroupCommonPhoneListName> searchCriteriaGroupCommonPhoneListName;
    protected List<SearchCriteriaGroupCommonPhoneListNumber> searchCriteriaGroupCommonPhoneListNumber;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Ruft den Wert der searchCriteriaModeOr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchCriteriaModeOr() {
        return searchCriteriaModeOr;
    }

    /**
     * Legt den Wert der searchCriteriaModeOr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchCriteriaModeOr(Boolean value) {
        this.searchCriteriaModeOr = value;
    }

    /**
     * Gets the value of the searchCriteriaGroupCommonPhoneListName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaGroupCommonPhoneListName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaGroupCommonPhoneListName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaGroupCommonPhoneListName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaGroupCommonPhoneListName property.
     */
    public List<SearchCriteriaGroupCommonPhoneListName> getSearchCriteriaGroupCommonPhoneListName() {
        if (searchCriteriaGroupCommonPhoneListName == null) {
            searchCriteriaGroupCommonPhoneListName = new ArrayList<>();
        }
        return this.searchCriteriaGroupCommonPhoneListName;
    }

    /**
     * Gets the value of the searchCriteriaGroupCommonPhoneListNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaGroupCommonPhoneListNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaGroupCommonPhoneListNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaGroupCommonPhoneListNumber }
     * 
     * 
     * @return
     *     The value of the searchCriteriaGroupCommonPhoneListNumber property.
     */
    public List<SearchCriteriaGroupCommonPhoneListNumber> getSearchCriteriaGroupCommonPhoneListNumber() {
        if (searchCriteriaGroupCommonPhoneListNumber == null) {
            searchCriteriaGroupCommonPhoneListNumber = new ArrayList<>();
        }
        return this.searchCriteriaGroupCommonPhoneListNumber;
    }

}
