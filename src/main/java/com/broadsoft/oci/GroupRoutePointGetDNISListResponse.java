//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupRoutePointGetDNISListRequest.
 *         Contains a table with column headings: "Name", "Phone Number", "Extension".
 *       
 * 
 * <p>Java-Klasse für GroupRoutePointGetDNISListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupRoutePointGetDNISListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="displayDNISNumber" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="displayDNISName" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="dnisTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupRoutePointGetDNISListResponse", propOrder = {
    "displayDNISNumber",
    "displayDNISName",
    "dnisTable"
})
public class GroupRoutePointGetDNISListResponse
    extends OCIDataResponse
{

    protected boolean displayDNISNumber;
    protected boolean displayDNISName;
    @XmlElement(required = true)
    protected OCITable dnisTable;

    /**
     * Ruft den Wert der displayDNISNumber-Eigenschaft ab.
     * 
     */
    public boolean isDisplayDNISNumber() {
        return displayDNISNumber;
    }

    /**
     * Legt den Wert der displayDNISNumber-Eigenschaft fest.
     * 
     */
    public void setDisplayDNISNumber(boolean value) {
        this.displayDNISNumber = value;
    }

    /**
     * Ruft den Wert der displayDNISName-Eigenschaft ab.
     * 
     */
    public boolean isDisplayDNISName() {
        return displayDNISName;
    }

    /**
     * Legt den Wert der displayDNISName-Eigenschaft fest.
     * 
     */
    public void setDisplayDNISName(boolean value) {
        this.displayDNISName = value;
    }

    /**
     * Ruft den Wert der dnisTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDnisTable() {
        return dnisTable;
    }

    /**
     * Legt den Wert der dnisTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDnisTable(OCITable value) {
        this.dnisTable = value;
    }

}
