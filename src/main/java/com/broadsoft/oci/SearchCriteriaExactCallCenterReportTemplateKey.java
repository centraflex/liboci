//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for a particular call center enhanced reporting report template.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactCallCenterReportTemplateKey complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactCallCenterReportTemplateKey">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="reportTemplate" type="{}CallCenterReportTemplateKey"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactCallCenterReportTemplateKey", propOrder = {
    "reportTemplate"
})
public class SearchCriteriaExactCallCenterReportTemplateKey
    extends SearchCriteria
{

    @XmlElement(required = true)
    protected CallCenterReportTemplateKey reportTemplate;

    /**
     * Ruft den Wert der reportTemplate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportTemplateKey }
     *     
     */
    public CallCenterReportTemplateKey getReportTemplate() {
        return reportTemplate;
    }

    /**
     * Legt den Wert der reportTemplate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportTemplateKey }
     *     
     */
    public void setReportTemplate(CallCenterReportTemplateKey value) {
        this.reportTemplate = value;
    }

}
