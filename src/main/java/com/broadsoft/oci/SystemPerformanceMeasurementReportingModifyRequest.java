//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the performance measurements reporting settings.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemPerformanceMeasurementReportingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPerformanceMeasurementReportingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="reportingInterval" type="{}PerformanceMeasurementReportingIntervalMinutes" minOccurs="0"/>
 *         <element name="resetMeasurementsAfterEachReport" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="reportEnterprise" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="reportServiceProvider" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="reportDevice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="reportTable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="reportEncoding" type="{}PerformanceMeasurementReportingEncoding" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPerformanceMeasurementReportingModifyRequest", propOrder = {
    "isActive",
    "reportingInterval",
    "resetMeasurementsAfterEachReport",
    "reportEnterprise",
    "reportServiceProvider",
    "reportDevice",
    "reportTable",
    "reportEncoding"
})
public class SystemPerformanceMeasurementReportingModifyRequest
    extends OCIRequest
{

    protected Boolean isActive;
    protected Integer reportingInterval;
    protected Boolean resetMeasurementsAfterEachReport;
    protected Boolean reportEnterprise;
    protected Boolean reportServiceProvider;
    protected Boolean reportDevice;
    protected Boolean reportTable;
    @XmlSchemaType(name = "token")
    protected PerformanceMeasurementReportingEncoding reportEncoding;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der reportingInterval-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReportingInterval() {
        return reportingInterval;
    }

    /**
     * Legt den Wert der reportingInterval-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReportingInterval(Integer value) {
        this.reportingInterval = value;
    }

    /**
     * Ruft den Wert der resetMeasurementsAfterEachReport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isResetMeasurementsAfterEachReport() {
        return resetMeasurementsAfterEachReport;
    }

    /**
     * Legt den Wert der resetMeasurementsAfterEachReport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setResetMeasurementsAfterEachReport(Boolean value) {
        this.resetMeasurementsAfterEachReport = value;
    }

    /**
     * Ruft den Wert der reportEnterprise-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReportEnterprise() {
        return reportEnterprise;
    }

    /**
     * Legt den Wert der reportEnterprise-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReportEnterprise(Boolean value) {
        this.reportEnterprise = value;
    }

    /**
     * Ruft den Wert der reportServiceProvider-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReportServiceProvider() {
        return reportServiceProvider;
    }

    /**
     * Legt den Wert der reportServiceProvider-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReportServiceProvider(Boolean value) {
        this.reportServiceProvider = value;
    }

    /**
     * Ruft den Wert der reportDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReportDevice() {
        return reportDevice;
    }

    /**
     * Legt den Wert der reportDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReportDevice(Boolean value) {
        this.reportDevice = value;
    }

    /**
     * Ruft den Wert der reportTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReportTable() {
        return reportTable;
    }

    /**
     * Legt den Wert der reportTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReportTable(Boolean value) {
        this.reportTable = value;
    }

    /**
     * Ruft den Wert der reportEncoding-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PerformanceMeasurementReportingEncoding }
     *     
     */
    public PerformanceMeasurementReportingEncoding getReportEncoding() {
        return reportEncoding;
    }

    /**
     * Legt den Wert der reportEncoding-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PerformanceMeasurementReportingEncoding }
     *     
     */
    public void setReportEncoding(PerformanceMeasurementReportingEncoding value) {
        this.reportEncoding = value;
    }

}
