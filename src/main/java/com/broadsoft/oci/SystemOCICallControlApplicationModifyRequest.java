//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify an application from the OCI call control application list.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemOCICallControlApplicationModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemOCICallControlApplicationModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="applicationId" type="{}OCICallControlApplicationId"/>
 *         <element name="enableSystemWide" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="notificationTimeoutSeconds" type="{}OCICallApplicationNotificationTimeOutSeconds" minOccurs="0"/>
 *         <element name="description" type="{}OCICallControlApplicationDescription" minOccurs="0"/>
 *         <element name="maxEventChannelsPerSet" type="{}EventNotificationChannelsPerSet" minOccurs="0"/>
 *         <element name="unresponsiveChannelSetGracePeriodSeconds" type="{}OCICallControlUnresponsiveChannelSetGracePeriodSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemOCICallControlApplicationModifyRequest", propOrder = {
    "applicationId",
    "enableSystemWide",
    "notificationTimeoutSeconds",
    "description",
    "maxEventChannelsPerSet",
    "unresponsiveChannelSetGracePeriodSeconds"
})
public class SystemOCICallControlApplicationModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String applicationId;
    protected Boolean enableSystemWide;
    protected Integer notificationTimeoutSeconds;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    protected Integer maxEventChannelsPerSet;
    protected Integer unresponsiveChannelSetGracePeriodSeconds;

    /**
     * Ruft den Wert der applicationId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationId() {
        return applicationId;
    }

    /**
     * Legt den Wert der applicationId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationId(String value) {
        this.applicationId = value;
    }

    /**
     * Ruft den Wert der enableSystemWide-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableSystemWide() {
        return enableSystemWide;
    }

    /**
     * Legt den Wert der enableSystemWide-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSystemWide(Boolean value) {
        this.enableSystemWide = value;
    }

    /**
     * Ruft den Wert der notificationTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNotificationTimeoutSeconds() {
        return notificationTimeoutSeconds;
    }

    /**
     * Legt den Wert der notificationTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNotificationTimeoutSeconds(Integer value) {
        this.notificationTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der maxEventChannelsPerSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxEventChannelsPerSet() {
        return maxEventChannelsPerSet;
    }

    /**
     * Legt den Wert der maxEventChannelsPerSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxEventChannelsPerSet(Integer value) {
        this.maxEventChannelsPerSet = value;
    }

    /**
     * Ruft den Wert der unresponsiveChannelSetGracePeriodSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUnresponsiveChannelSetGracePeriodSeconds() {
        return unresponsiveChannelSetGracePeriodSeconds;
    }

    /**
     * Legt den Wert der unresponsiveChannelSetGracePeriodSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUnresponsiveChannelSetGracePeriodSeconds(Integer value) {
        this.unresponsiveChannelSetGracePeriodSeconds = value;
    }

}
