//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *       Redirected call selection.
 *       When " redirectedCall " is set to true, all call logs with redirected call are returned. When it
 *       set to false, all call logs without redirected call are returned.
 *       The redirected call can be defined by including a subset of Service Invocation Disposition here. 
 *       If none included, any call has a ServiceInvocationDisposition value defined in ServiceInvocationDisposition21
 *       is considered as a redirected call.
 *       
 * 
 * <p>Java-Klasse für EnhancedCallLogsRedirectedCallSelection21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnhancedCallLogsRedirectedCallSelection21">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="redirectedCall" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="redirectType" type="{}ServiceInvocationDisposition21" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnhancedCallLogsRedirectedCallSelection21", propOrder = {
    "redirectedCall",
    "redirectType"
})
public class EnhancedCallLogsRedirectedCallSelection21 {

    protected boolean redirectedCall;
    @XmlSchemaType(name = "token")
    protected List<ServiceInvocationDisposition21> redirectType;

    /**
     * Ruft den Wert der redirectedCall-Eigenschaft ab.
     * 
     */
    public boolean isRedirectedCall() {
        return redirectedCall;
    }

    /**
     * Legt den Wert der redirectedCall-Eigenschaft fest.
     * 
     */
    public void setRedirectedCall(boolean value) {
        this.redirectedCall = value;
    }

    /**
     * Gets the value of the redirectType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the redirectType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRedirectType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceInvocationDisposition21 }
     * 
     * 
     * @return
     *     The value of the redirectType property.
     */
    public List<ServiceInvocationDisposition21> getRedirectType() {
        if (redirectType == null) {
            redirectType = new ArrayList<>();
        }
        return this.redirectType;
    }

}
