//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AnnouncementFileType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AnnouncementFileType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Audio"/>
 *     <enumeration value="Video"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AnnouncementFileType")
@XmlEnum
public enum AnnouncementFileType {

    @XmlEnumValue("Audio")
    AUDIO("Audio"),
    @XmlEnumValue("Video")
    VIDEO("Video");
    private final String value;

    AnnouncementFileType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AnnouncementFileType fromValue(String v) {
        for (AnnouncementFileType c: AnnouncementFileType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
