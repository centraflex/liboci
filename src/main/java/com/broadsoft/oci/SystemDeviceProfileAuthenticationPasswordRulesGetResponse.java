//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemDeviceProfileAuthenticationPasswordRulesGetRequest.
 *         Contains the device profile authentication password rules for the system.
 *       
 * 
 * <p>Java-Klasse für SystemDeviceProfileAuthenticationPasswordRulesGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceProfileAuthenticationPasswordRulesGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="disallowAuthenticationName" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disallowOldPassword" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disallowReversedOldPassword" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="restrictMinDigits" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minDigits" type="{}PasswordMinDigits"/>
 *         <element name="restrictMinUpperCaseLetters" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minUpperCaseLetters" type="{}PasswordMinUpperCaseLetters"/>
 *         <element name="restrictMinLowerCaseLetters" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minLowerCaseLetters" type="{}PasswordMinLowerCaseLetters"/>
 *         <element name="restrictMinNonAlphanumericCharacters" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minNonAlphanumericCharacters" type="{}PasswordMinNonAlphanumericCharacters"/>
 *         <element name="minLength" type="{}PasswordMinLength"/>
 *         <element name="sendPermanentLockoutNotification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="permanentLockoutNotifyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="deviceProfileAuthenticationLockoutType" type="{}AuthenticationLockoutType"/>
 *         <element name="deviceProfileTemporaryLockoutThreshold" type="{}AuthenticationTemporaryLockoutThreshold"/>
 *         <element name="deviceProfileWaitAlgorithm" type="{}AuthenticationLockoutWaitAlgorithmType"/>
 *         <element name="deviceProfileLockoutFixedMinutes" type="{}AuthenticationLockoutFixedWaitTimeMinutes"/>
 *         <element name="deviceProfilePermanentLockoutThreshold" type="{}AuthenticationPermanentLockoutThreshold"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceProfileAuthenticationPasswordRulesGetResponse", propOrder = {
    "disallowAuthenticationName",
    "disallowOldPassword",
    "disallowReversedOldPassword",
    "restrictMinDigits",
    "minDigits",
    "restrictMinUpperCaseLetters",
    "minUpperCaseLetters",
    "restrictMinLowerCaseLetters",
    "minLowerCaseLetters",
    "restrictMinNonAlphanumericCharacters",
    "minNonAlphanumericCharacters",
    "minLength",
    "sendPermanentLockoutNotification",
    "permanentLockoutNotifyEmailAddress",
    "deviceProfileAuthenticationLockoutType",
    "deviceProfileTemporaryLockoutThreshold",
    "deviceProfileWaitAlgorithm",
    "deviceProfileLockoutFixedMinutes",
    "deviceProfilePermanentLockoutThreshold"
})
public class SystemDeviceProfileAuthenticationPasswordRulesGetResponse
    extends OCIDataResponse
{

    protected boolean disallowAuthenticationName;
    protected boolean disallowOldPassword;
    protected boolean disallowReversedOldPassword;
    protected boolean restrictMinDigits;
    protected int minDigits;
    protected boolean restrictMinUpperCaseLetters;
    protected int minUpperCaseLetters;
    protected boolean restrictMinLowerCaseLetters;
    protected int minLowerCaseLetters;
    protected boolean restrictMinNonAlphanumericCharacters;
    protected int minNonAlphanumericCharacters;
    protected int minLength;
    protected boolean sendPermanentLockoutNotification;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String permanentLockoutNotifyEmailAddress;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AuthenticationLockoutType deviceProfileAuthenticationLockoutType;
    protected int deviceProfileTemporaryLockoutThreshold;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AuthenticationLockoutWaitAlgorithmType deviceProfileWaitAlgorithm;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceProfileLockoutFixedMinutes;
    protected int deviceProfilePermanentLockoutThreshold;

    /**
     * Ruft den Wert der disallowAuthenticationName-Eigenschaft ab.
     * 
     */
    public boolean isDisallowAuthenticationName() {
        return disallowAuthenticationName;
    }

    /**
     * Legt den Wert der disallowAuthenticationName-Eigenschaft fest.
     * 
     */
    public void setDisallowAuthenticationName(boolean value) {
        this.disallowAuthenticationName = value;
    }

    /**
     * Ruft den Wert der disallowOldPassword-Eigenschaft ab.
     * 
     */
    public boolean isDisallowOldPassword() {
        return disallowOldPassword;
    }

    /**
     * Legt den Wert der disallowOldPassword-Eigenschaft fest.
     * 
     */
    public void setDisallowOldPassword(boolean value) {
        this.disallowOldPassword = value;
    }

    /**
     * Ruft den Wert der disallowReversedOldPassword-Eigenschaft ab.
     * 
     */
    public boolean isDisallowReversedOldPassword() {
        return disallowReversedOldPassword;
    }

    /**
     * Legt den Wert der disallowReversedOldPassword-Eigenschaft fest.
     * 
     */
    public void setDisallowReversedOldPassword(boolean value) {
        this.disallowReversedOldPassword = value;
    }

    /**
     * Ruft den Wert der restrictMinDigits-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinDigits() {
        return restrictMinDigits;
    }

    /**
     * Legt den Wert der restrictMinDigits-Eigenschaft fest.
     * 
     */
    public void setRestrictMinDigits(boolean value) {
        this.restrictMinDigits = value;
    }

    /**
     * Ruft den Wert der minDigits-Eigenschaft ab.
     * 
     */
    public int getMinDigits() {
        return minDigits;
    }

    /**
     * Legt den Wert der minDigits-Eigenschaft fest.
     * 
     */
    public void setMinDigits(int value) {
        this.minDigits = value;
    }

    /**
     * Ruft den Wert der restrictMinUpperCaseLetters-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinUpperCaseLetters() {
        return restrictMinUpperCaseLetters;
    }

    /**
     * Legt den Wert der restrictMinUpperCaseLetters-Eigenschaft fest.
     * 
     */
    public void setRestrictMinUpperCaseLetters(boolean value) {
        this.restrictMinUpperCaseLetters = value;
    }

    /**
     * Ruft den Wert der minUpperCaseLetters-Eigenschaft ab.
     * 
     */
    public int getMinUpperCaseLetters() {
        return minUpperCaseLetters;
    }

    /**
     * Legt den Wert der minUpperCaseLetters-Eigenschaft fest.
     * 
     */
    public void setMinUpperCaseLetters(int value) {
        this.minUpperCaseLetters = value;
    }

    /**
     * Ruft den Wert der restrictMinLowerCaseLetters-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinLowerCaseLetters() {
        return restrictMinLowerCaseLetters;
    }

    /**
     * Legt den Wert der restrictMinLowerCaseLetters-Eigenschaft fest.
     * 
     */
    public void setRestrictMinLowerCaseLetters(boolean value) {
        this.restrictMinLowerCaseLetters = value;
    }

    /**
     * Ruft den Wert der minLowerCaseLetters-Eigenschaft ab.
     * 
     */
    public int getMinLowerCaseLetters() {
        return minLowerCaseLetters;
    }

    /**
     * Legt den Wert der minLowerCaseLetters-Eigenschaft fest.
     * 
     */
    public void setMinLowerCaseLetters(int value) {
        this.minLowerCaseLetters = value;
    }

    /**
     * Ruft den Wert der restrictMinNonAlphanumericCharacters-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinNonAlphanumericCharacters() {
        return restrictMinNonAlphanumericCharacters;
    }

    /**
     * Legt den Wert der restrictMinNonAlphanumericCharacters-Eigenschaft fest.
     * 
     */
    public void setRestrictMinNonAlphanumericCharacters(boolean value) {
        this.restrictMinNonAlphanumericCharacters = value;
    }

    /**
     * Ruft den Wert der minNonAlphanumericCharacters-Eigenschaft ab.
     * 
     */
    public int getMinNonAlphanumericCharacters() {
        return minNonAlphanumericCharacters;
    }

    /**
     * Legt den Wert der minNonAlphanumericCharacters-Eigenschaft fest.
     * 
     */
    public void setMinNonAlphanumericCharacters(int value) {
        this.minNonAlphanumericCharacters = value;
    }

    /**
     * Ruft den Wert der minLength-Eigenschaft ab.
     * 
     */
    public int getMinLength() {
        return minLength;
    }

    /**
     * Legt den Wert der minLength-Eigenschaft fest.
     * 
     */
    public void setMinLength(int value) {
        this.minLength = value;
    }

    /**
     * Ruft den Wert der sendPermanentLockoutNotification-Eigenschaft ab.
     * 
     */
    public boolean isSendPermanentLockoutNotification() {
        return sendPermanentLockoutNotification;
    }

    /**
     * Legt den Wert der sendPermanentLockoutNotification-Eigenschaft fest.
     * 
     */
    public void setSendPermanentLockoutNotification(boolean value) {
        this.sendPermanentLockoutNotification = value;
    }

    /**
     * Ruft den Wert der permanentLockoutNotifyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPermanentLockoutNotifyEmailAddress() {
        return permanentLockoutNotifyEmailAddress;
    }

    /**
     * Legt den Wert der permanentLockoutNotifyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPermanentLockoutNotifyEmailAddress(String value) {
        this.permanentLockoutNotifyEmailAddress = value;
    }

    /**
     * Ruft den Wert der deviceProfileAuthenticationLockoutType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationLockoutType }
     *     
     */
    public AuthenticationLockoutType getDeviceProfileAuthenticationLockoutType() {
        return deviceProfileAuthenticationLockoutType;
    }

    /**
     * Legt den Wert der deviceProfileAuthenticationLockoutType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationLockoutType }
     *     
     */
    public void setDeviceProfileAuthenticationLockoutType(AuthenticationLockoutType value) {
        this.deviceProfileAuthenticationLockoutType = value;
    }

    /**
     * Ruft den Wert der deviceProfileTemporaryLockoutThreshold-Eigenschaft ab.
     * 
     */
    public int getDeviceProfileTemporaryLockoutThreshold() {
        return deviceProfileTemporaryLockoutThreshold;
    }

    /**
     * Legt den Wert der deviceProfileTemporaryLockoutThreshold-Eigenschaft fest.
     * 
     */
    public void setDeviceProfileTemporaryLockoutThreshold(int value) {
        this.deviceProfileTemporaryLockoutThreshold = value;
    }

    /**
     * Ruft den Wert der deviceProfileWaitAlgorithm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationLockoutWaitAlgorithmType }
     *     
     */
    public AuthenticationLockoutWaitAlgorithmType getDeviceProfileWaitAlgorithm() {
        return deviceProfileWaitAlgorithm;
    }

    /**
     * Legt den Wert der deviceProfileWaitAlgorithm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationLockoutWaitAlgorithmType }
     *     
     */
    public void setDeviceProfileWaitAlgorithm(AuthenticationLockoutWaitAlgorithmType value) {
        this.deviceProfileWaitAlgorithm = value;
    }

    /**
     * Ruft den Wert der deviceProfileLockoutFixedMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceProfileLockoutFixedMinutes() {
        return deviceProfileLockoutFixedMinutes;
    }

    /**
     * Legt den Wert der deviceProfileLockoutFixedMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceProfileLockoutFixedMinutes(String value) {
        this.deviceProfileLockoutFixedMinutes = value;
    }

    /**
     * Ruft den Wert der deviceProfilePermanentLockoutThreshold-Eigenschaft ab.
     * 
     */
    public int getDeviceProfilePermanentLockoutThreshold() {
        return deviceProfilePermanentLockoutThreshold;
    }

    /**
     * Legt den Wert der deviceProfilePermanentLockoutThreshold-Eigenschaft fest.
     * 
     */
    public void setDeviceProfilePermanentLockoutThreshold(int value) {
        this.deviceProfilePermanentLockoutThreshold = value;
    }

}
