//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GroupCLIDPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="GroupCLIDPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Use DN"/>
 *     <enumeration value="Use Configurable CLID"/>
 *     <enumeration value="Use Group CLID"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "GroupCLIDPolicy")
@XmlEnum
public enum GroupCLIDPolicy {

    @XmlEnumValue("Use DN")
    USE_DN("Use DN"),
    @XmlEnumValue("Use Configurable CLID")
    USE_CONFIGURABLE_CLID("Use Configurable CLID"),
    @XmlEnumValue("Use Group CLID")
    USE_GROUP_CLID("Use Group CLID");
    private final String value;

    GroupCLIDPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupCLIDPolicy fromValue(String v) {
        for (GroupCLIDPolicy c: GroupCLIDPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
