//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with the Trunk Group Service. 
 *         Following attributes are only used in IMS mode. The values are saved if specified not in IMS mode:
 *           implicitRegistrationSetSupportPolicy
 *           sipIdentityForPilotAndProxyTrunkModesPolicy
 *           useMostRecentEntryOnDeflection
 * 
 *         The response is either a SuccessResponse or an ErrorResponse. 
 *       
 * 
 * <p>Java-Klasse für SystemTrunkGroupModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemTrunkGroupModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enforceCLIDServiceAssignmentForPilotUser" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="terminateUnreachableTriggerDetectionOnReceiptOf18x" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="pilotUserCallingLineAssertedIdentityPolicy" type="{}TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy" minOccurs="0"/>
 *         <element name="enforceOutOfDialogPBXRedirectionPolicies" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="unscreenedRedirectionHandling" type="{}TrunkGroupUnscreenedRedirectionHandling" minOccurs="0"/>
 *         <element name="enableHoldoverOfHighwaterCallCounts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="holdoverPeriod" type="{}TrunkGroupHighwaterCallCountHoldoverPeriodMinutes" minOccurs="0"/>
 *         <element name="timeZoneOffsetMinutes" type="{}TrunkGroupTimeZoneOffsetMinutes" minOccurs="0"/>
 *         <element name="clidSourceForScreenedCallsPolicy" type="{}TrunkGroupCLIDSourceForScreenedCallsPolicy" minOccurs="0"/>
 *         <element name="userLookupPolicy" type="{}TrunkGroupUserLookupPolicy" minOccurs="0"/>
 *         <element name="outOfDialogPBXRedirectionCLIDMapping" type="{}TrunkGroupOutOfDialogPBXRedirectionCLIDMapping" minOccurs="0"/>
 *         <element name="enforceOutOfDialogPBXRedirectionTrunkGroupCapacity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="implicitRegistrationSetSupportPolicy" type="{}TrunkGroupImplicitRegistrationSetSupportPolicy" minOccurs="0"/>
 *         <element name="sipIdentityForPilotAndProxyTrunkModesPolicy" type="{}TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy" minOccurs="0"/>
 *         <element name="supportConnectedIdentityPolicy" type="{}TrunkGroupSupportConnectedIdentityPolicy" minOccurs="0"/>
 *         <element name="useUnmappedSessionsForTrunkUsers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowPAILookupForOutOfDialogPBXRedirection" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="outOfDialogPBXRedirectionOriginatorLookupPolicy" type="{}TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy" minOccurs="0"/>
 *         <element name="allowTrunkIdentityForAllOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useMostRecentEntryOnDeflection" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemTrunkGroupModifyRequest", propOrder = {
    "enforceCLIDServiceAssignmentForPilotUser",
    "terminateUnreachableTriggerDetectionOnReceiptOf18X",
    "pilotUserCallingLineAssertedIdentityPolicy",
    "enforceOutOfDialogPBXRedirectionPolicies",
    "unscreenedRedirectionHandling",
    "enableHoldoverOfHighwaterCallCounts",
    "holdoverPeriod",
    "timeZoneOffsetMinutes",
    "clidSourceForScreenedCallsPolicy",
    "userLookupPolicy",
    "outOfDialogPBXRedirectionCLIDMapping",
    "enforceOutOfDialogPBXRedirectionTrunkGroupCapacity",
    "implicitRegistrationSetSupportPolicy",
    "sipIdentityForPilotAndProxyTrunkModesPolicy",
    "supportConnectedIdentityPolicy",
    "useUnmappedSessionsForTrunkUsers",
    "allowPAILookupForOutOfDialogPBXRedirection",
    "outOfDialogPBXRedirectionOriginatorLookupPolicy",
    "allowTrunkIdentityForAllOriginations",
    "useMostRecentEntryOnDeflection"
})
public class SystemTrunkGroupModifyRequest
    extends OCIRequest
{

    protected Boolean enforceCLIDServiceAssignmentForPilotUser;
    @XmlElement(name = "terminateUnreachableTriggerDetectionOnReceiptOf18x")
    protected Boolean terminateUnreachableTriggerDetectionOnReceiptOf18X;
    @XmlSchemaType(name = "token")
    protected TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy pilotUserCallingLineAssertedIdentityPolicy;
    protected Boolean enforceOutOfDialogPBXRedirectionPolicies;
    @XmlSchemaType(name = "token")
    protected TrunkGroupUnscreenedRedirectionHandling unscreenedRedirectionHandling;
    protected Boolean enableHoldoverOfHighwaterCallCounts;
    protected Integer holdoverPeriod;
    protected Integer timeZoneOffsetMinutes;
    @XmlSchemaType(name = "token")
    protected TrunkGroupCLIDSourceForScreenedCallsPolicy clidSourceForScreenedCallsPolicy;
    @XmlSchemaType(name = "token")
    protected TrunkGroupUserLookupPolicy userLookupPolicy;
    @XmlSchemaType(name = "token")
    protected TrunkGroupOutOfDialogPBXRedirectionCLIDMapping outOfDialogPBXRedirectionCLIDMapping;
    protected Boolean enforceOutOfDialogPBXRedirectionTrunkGroupCapacity;
    @XmlSchemaType(name = "token")
    protected TrunkGroupImplicitRegistrationSetSupportPolicy implicitRegistrationSetSupportPolicy;
    @XmlSchemaType(name = "token")
    protected TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy sipIdentityForPilotAndProxyTrunkModesPolicy;
    @XmlSchemaType(name = "token")
    protected TrunkGroupSupportConnectedIdentityPolicy supportConnectedIdentityPolicy;
    protected Boolean useUnmappedSessionsForTrunkUsers;
    protected Boolean allowPAILookupForOutOfDialogPBXRedirection;
    @XmlSchemaType(name = "token")
    protected TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy outOfDialogPBXRedirectionOriginatorLookupPolicy;
    protected Boolean allowTrunkIdentityForAllOriginations;
    protected Boolean useMostRecentEntryOnDeflection;

    /**
     * Ruft den Wert der enforceCLIDServiceAssignmentForPilotUser-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnforceCLIDServiceAssignmentForPilotUser() {
        return enforceCLIDServiceAssignmentForPilotUser;
    }

    /**
     * Legt den Wert der enforceCLIDServiceAssignmentForPilotUser-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnforceCLIDServiceAssignmentForPilotUser(Boolean value) {
        this.enforceCLIDServiceAssignmentForPilotUser = value;
    }

    /**
     * Ruft den Wert der terminateUnreachableTriggerDetectionOnReceiptOf18X-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTerminateUnreachableTriggerDetectionOnReceiptOf18X() {
        return terminateUnreachableTriggerDetectionOnReceiptOf18X;
    }

    /**
     * Legt den Wert der terminateUnreachableTriggerDetectionOnReceiptOf18X-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTerminateUnreachableTriggerDetectionOnReceiptOf18X(Boolean value) {
        this.terminateUnreachableTriggerDetectionOnReceiptOf18X = value;
    }

    /**
     * Ruft den Wert der pilotUserCallingLineAssertedIdentityPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy }
     *     
     */
    public TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy getPilotUserCallingLineAssertedIdentityPolicy() {
        return pilotUserCallingLineAssertedIdentityPolicy;
    }

    /**
     * Legt den Wert der pilotUserCallingLineAssertedIdentityPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy }
     *     
     */
    public void setPilotUserCallingLineAssertedIdentityPolicy(TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy value) {
        this.pilotUserCallingLineAssertedIdentityPolicy = value;
    }

    /**
     * Ruft den Wert der enforceOutOfDialogPBXRedirectionPolicies-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnforceOutOfDialogPBXRedirectionPolicies() {
        return enforceOutOfDialogPBXRedirectionPolicies;
    }

    /**
     * Legt den Wert der enforceOutOfDialogPBXRedirectionPolicies-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnforceOutOfDialogPBXRedirectionPolicies(Boolean value) {
        this.enforceOutOfDialogPBXRedirectionPolicies = value;
    }

    /**
     * Ruft den Wert der unscreenedRedirectionHandling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupUnscreenedRedirectionHandling }
     *     
     */
    public TrunkGroupUnscreenedRedirectionHandling getUnscreenedRedirectionHandling() {
        return unscreenedRedirectionHandling;
    }

    /**
     * Legt den Wert der unscreenedRedirectionHandling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupUnscreenedRedirectionHandling }
     *     
     */
    public void setUnscreenedRedirectionHandling(TrunkGroupUnscreenedRedirectionHandling value) {
        this.unscreenedRedirectionHandling = value;
    }

    /**
     * Ruft den Wert der enableHoldoverOfHighwaterCallCounts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableHoldoverOfHighwaterCallCounts() {
        return enableHoldoverOfHighwaterCallCounts;
    }

    /**
     * Legt den Wert der enableHoldoverOfHighwaterCallCounts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableHoldoverOfHighwaterCallCounts(Boolean value) {
        this.enableHoldoverOfHighwaterCallCounts = value;
    }

    /**
     * Ruft den Wert der holdoverPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHoldoverPeriod() {
        return holdoverPeriod;
    }

    /**
     * Legt den Wert der holdoverPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHoldoverPeriod(Integer value) {
        this.holdoverPeriod = value;
    }

    /**
     * Ruft den Wert der timeZoneOffsetMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTimeZoneOffsetMinutes() {
        return timeZoneOffsetMinutes;
    }

    /**
     * Legt den Wert der timeZoneOffsetMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTimeZoneOffsetMinutes(Integer value) {
        this.timeZoneOffsetMinutes = value;
    }

    /**
     * Ruft den Wert der clidSourceForScreenedCallsPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupCLIDSourceForScreenedCallsPolicy }
     *     
     */
    public TrunkGroupCLIDSourceForScreenedCallsPolicy getClidSourceForScreenedCallsPolicy() {
        return clidSourceForScreenedCallsPolicy;
    }

    /**
     * Legt den Wert der clidSourceForScreenedCallsPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupCLIDSourceForScreenedCallsPolicy }
     *     
     */
    public void setClidSourceForScreenedCallsPolicy(TrunkGroupCLIDSourceForScreenedCallsPolicy value) {
        this.clidSourceForScreenedCallsPolicy = value;
    }

    /**
     * Ruft den Wert der userLookupPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupUserLookupPolicy }
     *     
     */
    public TrunkGroupUserLookupPolicy getUserLookupPolicy() {
        return userLookupPolicy;
    }

    /**
     * Legt den Wert der userLookupPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupUserLookupPolicy }
     *     
     */
    public void setUserLookupPolicy(TrunkGroupUserLookupPolicy value) {
        this.userLookupPolicy = value;
    }

    /**
     * Ruft den Wert der outOfDialogPBXRedirectionCLIDMapping-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupOutOfDialogPBXRedirectionCLIDMapping }
     *     
     */
    public TrunkGroupOutOfDialogPBXRedirectionCLIDMapping getOutOfDialogPBXRedirectionCLIDMapping() {
        return outOfDialogPBXRedirectionCLIDMapping;
    }

    /**
     * Legt den Wert der outOfDialogPBXRedirectionCLIDMapping-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupOutOfDialogPBXRedirectionCLIDMapping }
     *     
     */
    public void setOutOfDialogPBXRedirectionCLIDMapping(TrunkGroupOutOfDialogPBXRedirectionCLIDMapping value) {
        this.outOfDialogPBXRedirectionCLIDMapping = value;
    }

    /**
     * Ruft den Wert der enforceOutOfDialogPBXRedirectionTrunkGroupCapacity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnforceOutOfDialogPBXRedirectionTrunkGroupCapacity() {
        return enforceOutOfDialogPBXRedirectionTrunkGroupCapacity;
    }

    /**
     * Legt den Wert der enforceOutOfDialogPBXRedirectionTrunkGroupCapacity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnforceOutOfDialogPBXRedirectionTrunkGroupCapacity(Boolean value) {
        this.enforceOutOfDialogPBXRedirectionTrunkGroupCapacity = value;
    }

    /**
     * Ruft den Wert der implicitRegistrationSetSupportPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupImplicitRegistrationSetSupportPolicy }
     *     
     */
    public TrunkGroupImplicitRegistrationSetSupportPolicy getImplicitRegistrationSetSupportPolicy() {
        return implicitRegistrationSetSupportPolicy;
    }

    /**
     * Legt den Wert der implicitRegistrationSetSupportPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupImplicitRegistrationSetSupportPolicy }
     *     
     */
    public void setImplicitRegistrationSetSupportPolicy(TrunkGroupImplicitRegistrationSetSupportPolicy value) {
        this.implicitRegistrationSetSupportPolicy = value;
    }

    /**
     * Ruft den Wert der sipIdentityForPilotAndProxyTrunkModesPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy }
     *     
     */
    public TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy getSipIdentityForPilotAndProxyTrunkModesPolicy() {
        return sipIdentityForPilotAndProxyTrunkModesPolicy;
    }

    /**
     * Legt den Wert der sipIdentityForPilotAndProxyTrunkModesPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy }
     *     
     */
    public void setSipIdentityForPilotAndProxyTrunkModesPolicy(TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy value) {
        this.sipIdentityForPilotAndProxyTrunkModesPolicy = value;
    }

    /**
     * Ruft den Wert der supportConnectedIdentityPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupSupportConnectedIdentityPolicy }
     *     
     */
    public TrunkGroupSupportConnectedIdentityPolicy getSupportConnectedIdentityPolicy() {
        return supportConnectedIdentityPolicy;
    }

    /**
     * Legt den Wert der supportConnectedIdentityPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupSupportConnectedIdentityPolicy }
     *     
     */
    public void setSupportConnectedIdentityPolicy(TrunkGroupSupportConnectedIdentityPolicy value) {
        this.supportConnectedIdentityPolicy = value;
    }

    /**
     * Ruft den Wert der useUnmappedSessionsForTrunkUsers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseUnmappedSessionsForTrunkUsers() {
        return useUnmappedSessionsForTrunkUsers;
    }

    /**
     * Legt den Wert der useUnmappedSessionsForTrunkUsers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseUnmappedSessionsForTrunkUsers(Boolean value) {
        this.useUnmappedSessionsForTrunkUsers = value;
    }

    /**
     * Ruft den Wert der allowPAILookupForOutOfDialogPBXRedirection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowPAILookupForOutOfDialogPBXRedirection() {
        return allowPAILookupForOutOfDialogPBXRedirection;
    }

    /**
     * Legt den Wert der allowPAILookupForOutOfDialogPBXRedirection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowPAILookupForOutOfDialogPBXRedirection(Boolean value) {
        this.allowPAILookupForOutOfDialogPBXRedirection = value;
    }

    /**
     * Ruft den Wert der outOfDialogPBXRedirectionOriginatorLookupPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy }
     *     
     */
    public TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy getOutOfDialogPBXRedirectionOriginatorLookupPolicy() {
        return outOfDialogPBXRedirectionOriginatorLookupPolicy;
    }

    /**
     * Legt den Wert der outOfDialogPBXRedirectionOriginatorLookupPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy }
     *     
     */
    public void setOutOfDialogPBXRedirectionOriginatorLookupPolicy(TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy value) {
        this.outOfDialogPBXRedirectionOriginatorLookupPolicy = value;
    }

    /**
     * Ruft den Wert der allowTrunkIdentityForAllOriginations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowTrunkIdentityForAllOriginations() {
        return allowTrunkIdentityForAllOriginations;
    }

    /**
     * Legt den Wert der allowTrunkIdentityForAllOriginations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowTrunkIdentityForAllOriginations(Boolean value) {
        this.allowTrunkIdentityForAllOriginations = value;
    }

    /**
     * Ruft den Wert der useMostRecentEntryOnDeflection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMostRecentEntryOnDeflection() {
        return useMostRecentEntryOnDeflection;
    }

    /**
     * Legt den Wert der useMostRecentEntryOnDeflection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMostRecentEntryOnDeflection(Boolean value) {
        this.useMostRecentEntryOnDeflection = value;
    }

}
