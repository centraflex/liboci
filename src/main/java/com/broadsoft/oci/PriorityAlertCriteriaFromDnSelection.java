//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PriorityAlertCriteriaFromDnSelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="PriorityAlertCriteriaFromDnSelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Any External"/>
 *     <enumeration value="Specified Only"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "PriorityAlertCriteriaFromDnSelection")
@XmlEnum
public enum PriorityAlertCriteriaFromDnSelection {

    @XmlEnumValue("Any External")
    ANY_EXTERNAL("Any External"),
    @XmlEnumValue("Specified Only")
    SPECIFIED_ONLY("Specified Only");
    private final String value;

    PriorityAlertCriteriaFromDnSelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PriorityAlertCriteriaFromDnSelection fromValue(String v) {
        for (PriorityAlertCriteriaFromDnSelection c: PriorityAlertCriteriaFromDnSelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
