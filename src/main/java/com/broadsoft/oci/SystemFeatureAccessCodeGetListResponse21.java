//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemFeatureAccessCodeGetListRequest21.
 * 
 *         In release 20 the "Call Recording" FAC name is changed to
 *         "Call Recording - Start".
 *       
 * 
 * <p>Java-Klasse für SystemFeatureAccessCodeGetListResponse21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemFeatureAccessCodeGetListResponse21">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="featureAccessCode" type="{}FeatureAccessCodeReadEntry" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemFeatureAccessCodeGetListResponse21", propOrder = {
    "featureAccessCode"
})
public class SystemFeatureAccessCodeGetListResponse21
    extends OCIDataResponse
{

    protected List<FeatureAccessCodeReadEntry> featureAccessCode;

    /**
     * Gets the value of the featureAccessCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the featureAccessCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeatureAccessCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeatureAccessCodeReadEntry }
     * 
     * 
     * @return
     *     The value of the featureAccessCode property.
     */
    public List<FeatureAccessCodeReadEntry> getFeatureAccessCode() {
        if (featureAccessCode == null) {
            featureAccessCode = new ArrayList<>();
        }
        return this.featureAccessCode;
    }

}
