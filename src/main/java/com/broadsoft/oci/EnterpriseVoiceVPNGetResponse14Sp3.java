//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to EnterpriseVoiceVPNGetRequest14sp3.
 *       
 * 
 * <p>Java-Klasse für EnterpriseVoiceVPNGetResponse14sp3 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseVoiceVPNGetResponse14sp3">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="defaultSelection" type="{}EnterpriseVoiceVPNDefaultSelection"/>
 *         <element name="e164Selection" type="{}EnterpriseVoiceVPNNonMatchingE164NumberSelection"/>
 *         <element name="usePhoneContext" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseVoiceVPNGetResponse14sp3", propOrder = {
    "isActive",
    "defaultSelection",
    "e164Selection",
    "usePhoneContext"
})
public class EnterpriseVoiceVPNGetResponse14Sp3
    extends OCIDataResponse
{

    protected boolean isActive;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseVoiceVPNDefaultSelection defaultSelection;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseVoiceVPNNonMatchingE164NumberSelection e164Selection;
    protected boolean usePhoneContext;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der defaultSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseVoiceVPNDefaultSelection }
     *     
     */
    public EnterpriseVoiceVPNDefaultSelection getDefaultSelection() {
        return defaultSelection;
    }

    /**
     * Legt den Wert der defaultSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseVoiceVPNDefaultSelection }
     *     
     */
    public void setDefaultSelection(EnterpriseVoiceVPNDefaultSelection value) {
        this.defaultSelection = value;
    }

    /**
     * Ruft den Wert der e164Selection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseVoiceVPNNonMatchingE164NumberSelection }
     *     
     */
    public EnterpriseVoiceVPNNonMatchingE164NumberSelection getE164Selection() {
        return e164Selection;
    }

    /**
     * Legt den Wert der e164Selection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseVoiceVPNNonMatchingE164NumberSelection }
     *     
     */
    public void setE164Selection(EnterpriseVoiceVPNNonMatchingE164NumberSelection value) {
        this.e164Selection = value;
    }

    /**
     * Ruft den Wert der usePhoneContext-Eigenschaft ab.
     * 
     */
    public boolean isUsePhoneContext() {
        return usePhoneContext;
    }

    /**
     * Legt den Wert der usePhoneContext-Eigenschaft fest.
     * 
     */
    public void setUsePhoneContext(boolean value) {
        this.usePhoneContext = value;
    }

}
