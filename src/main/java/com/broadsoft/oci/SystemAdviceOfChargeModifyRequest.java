//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify Advice of Charge system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *         
 *         Replaced by: SystemAdviceOfChargeModifyRequest19sp1
 *       
 * 
 * <p>Java-Klasse für SystemAdviceOfChargeModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAdviceOfChargeModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="delayBetweenNotificationSeconds" type="{}AdviceOfChargeDelayBetweenNotificationSeconds" minOccurs="0"/>
 *         <element name="incomingAocHandling" type="{}AdviceOfChargeIncomingAocHandling" minOccurs="0"/>
 *         <element name="costInformationSource" type="{}NetAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAdviceOfChargeModifyRequest", propOrder = {
    "delayBetweenNotificationSeconds",
    "incomingAocHandling",
    "costInformationSource"
})
public class SystemAdviceOfChargeModifyRequest
    extends OCIRequest
{

    protected Integer delayBetweenNotificationSeconds;
    @XmlSchemaType(name = "token")
    protected AdviceOfChargeIncomingAocHandling incomingAocHandling;
    @XmlElementRef(name = "costInformationSource", type = JAXBElement.class, required = false)
    protected JAXBElement<String> costInformationSource;

    /**
     * Ruft den Wert der delayBetweenNotificationSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDelayBetweenNotificationSeconds() {
        return delayBetweenNotificationSeconds;
    }

    /**
     * Legt den Wert der delayBetweenNotificationSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDelayBetweenNotificationSeconds(Integer value) {
        this.delayBetweenNotificationSeconds = value;
    }

    /**
     * Ruft den Wert der incomingAocHandling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AdviceOfChargeIncomingAocHandling }
     *     
     */
    public AdviceOfChargeIncomingAocHandling getIncomingAocHandling() {
        return incomingAocHandling;
    }

    /**
     * Legt den Wert der incomingAocHandling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdviceOfChargeIncomingAocHandling }
     *     
     */
    public void setIncomingAocHandling(AdviceOfChargeIncomingAocHandling value) {
        this.incomingAocHandling = value;
    }

    /**
     * Ruft den Wert der costInformationSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCostInformationSource() {
        return costInformationSource;
    }

    /**
     * Legt den Wert der costInformationSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCostInformationSource(JAXBElement<String> value) {
        this.costInformationSource = value;
    }

}
