//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterRoutingPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterRoutingPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Longest Wait Time"/>
 *     <enumeration value="Priority"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterRoutingPolicy")
@XmlEnum
public enum CallCenterRoutingPolicy {

    @XmlEnumValue("Longest Wait Time")
    LONGEST_WAIT_TIME("Longest Wait Time"),
    @XmlEnumValue("Priority")
    PRIORITY("Priority");
    private final String value;

    CallCenterRoutingPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterRoutingPolicy fromValue(String v) {
        for (CallCenterRoutingPolicy c: CallCenterRoutingPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
