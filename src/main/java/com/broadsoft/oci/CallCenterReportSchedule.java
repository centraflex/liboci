//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         A schedule for call center enhanced reporting scheduled report. It can either be a fixed time schedule
 *         or recurring schedule
 *       
 * 
 * <p>Java-Klasse für CallCenterReportSchedule complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterReportSchedule">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <choice>
 *         <element name="scheduleTime" type="{}CallCenterReportScheduleTime"/>
 *         <element name="recurrence" type="{}CallCenterReportScheduleRecurrence"/>
 *       </choice>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterReportSchedule", propOrder = {
    "scheduleTime",
    "recurrence"
})
public class CallCenterReportSchedule {

    protected CallCenterReportScheduleTime scheduleTime;
    protected CallCenterReportScheduleRecurrence recurrence;

    /**
     * Ruft den Wert der scheduleTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportScheduleTime }
     *     
     */
    public CallCenterReportScheduleTime getScheduleTime() {
        return scheduleTime;
    }

    /**
     * Legt den Wert der scheduleTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportScheduleTime }
     *     
     */
    public void setScheduleTime(CallCenterReportScheduleTime value) {
        this.scheduleTime = value;
    }

    /**
     * Ruft den Wert der recurrence-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportScheduleRecurrence }
     *     
     */
    public CallCenterReportScheduleRecurrence getRecurrence() {
        return recurrence;
    }

    /**
     * Legt den Wert der recurrence-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportScheduleRecurrence }
     *     
     */
    public void setRecurrence(CallCenterReportScheduleRecurrence value) {
        this.recurrence = value;
    }

}
