//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DirectRouteOutgoingTrunkIdentityPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DirectRouteOutgoingTrunkIdentityPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Direct Route Trunk Identity"/>
 *     <enumeration value="Trunk Group Trunk Identity"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DirectRouteOutgoingTrunkIdentityPolicy")
@XmlEnum
public enum DirectRouteOutgoingTrunkIdentityPolicy {

    @XmlEnumValue("Direct Route Trunk Identity")
    DIRECT_ROUTE_TRUNK_IDENTITY("Direct Route Trunk Identity"),
    @XmlEnumValue("Trunk Group Trunk Identity")
    TRUNK_GROUP_TRUNK_IDENTITY("Trunk Group Trunk Identity");
    private final String value;

    DirectRouteOutgoingTrunkIdentityPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DirectRouteOutgoingTrunkIdentityPolicy fromValue(String v) {
        for (DirectRouteOutgoingTrunkIdentityPolicy c: DirectRouteOutgoingTrunkIdentityPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
