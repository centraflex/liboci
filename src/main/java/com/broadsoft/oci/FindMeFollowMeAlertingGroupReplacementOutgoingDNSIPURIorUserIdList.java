//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlElementRefs;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         A list of phone numbers/sipuris or user ids that replaces a previously configured list.
 *         By convention, an element of this type may be set nill to clear the list.
 *       
 * 
 * <p>Java-Klasse für FindMeFollowMeAlertingGroupReplacementOutgoingDNSIPURIorUserIdList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="FindMeFollowMeAlertingGroupReplacementOutgoingDNSIPURIorUserIdList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <choice maxOccurs="10" minOccurs="0">
 *           <element name="phoneNumber" type="{}OutgoingDNorSIPURI"/>
 *           <element name="userId" type="{}UserId"/>
 *         </choice>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FindMeFollowMeAlertingGroupReplacementOutgoingDNSIPURIorUserIdList", propOrder = {
    "phoneNumberOrUserId"
})
public class FindMeFollowMeAlertingGroupReplacementOutgoingDNSIPURIorUserIdList {

    @XmlElementRefs({
        @XmlElementRef(name = "phoneNumber", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "userId", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<String>> phoneNumberOrUserId;

    /**
     * Gets the value of the phoneNumberOrUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the phoneNumberOrUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPhoneNumberOrUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * 
     * @return
     *     The value of the phoneNumberOrUserId property.
     */
    public List<JAXBElement<String>> getPhoneNumberOrUserId() {
        if (phoneNumberOrUserId == null) {
            phoneNumberOrUserId = new ArrayList<>();
        }
        return this.phoneNumberOrUserId;
    }

}
