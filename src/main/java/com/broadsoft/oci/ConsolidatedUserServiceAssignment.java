//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Assign a user service. If the service has not been authorized to the group, it will be authorized.
 *         The authorizedQuantity will be used at the group level if provided; otherwise, the service quantity will be set to unlimited. 
 *         The command will fail if the authorized quantity set at the service provider level is insufficient.
 *       
 * 
 * <p>Java-Klasse für ConsolidatedUserServiceAssignment complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ConsolidatedUserServiceAssignment">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="userServiceName" type="{}UserService"/>
 *         <element name="authorizedQuantity" type="{}UnboundedPositiveInt" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsolidatedUserServiceAssignment", propOrder = {
    "userServiceName",
    "authorizedQuantity"
})
public class ConsolidatedUserServiceAssignment {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userServiceName;
    protected UnboundedPositiveInt authorizedQuantity;

    /**
     * Ruft den Wert der userServiceName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserServiceName() {
        return userServiceName;
    }

    /**
     * Legt den Wert der userServiceName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserServiceName(String value) {
        this.userServiceName = value;
    }

    /**
     * Ruft den Wert der authorizedQuantity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public UnboundedPositiveInt getAuthorizedQuantity() {
        return authorizedQuantity;
    }

    /**
     * Legt den Wert der authorizedQuantity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public void setAuthorizedQuantity(UnboundedPositiveInt value) {
        this.authorizedQuantity = value;
    }

}
