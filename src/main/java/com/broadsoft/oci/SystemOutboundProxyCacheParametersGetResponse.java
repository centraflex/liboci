//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemOutboundProxyCacheParametersGetRequest.
 *         Contains a list of Outbound Proxy Cache system parameters.        
 *       
 * 
 * <p>Java-Klasse für SystemOutboundProxyCacheParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemOutboundProxyCacheParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="evictionTimeoutMinutes" type="{}OutboundProxyCacheEvictionTimeoutMinutes"/>
 *         <element name="refreshTimeoutMinutes" type="{}OutboundProxyCacheRefreshTimeoutMinutes"/>
 *         <element name="auditIntervalMinutes" type="{}OutboundProxyCacheAuditIntervalMinutes"/>
 *         <element name="maximumCacheSize" type="{}NonNegativeInt"/>
 *         <element name="dnsTypeDefaultValue" type="{}OutboundProxyCacheDefaultValue" minOccurs="0"/>
 *         <element name="useDnsSrvDefaultValue" type="{}OutboundProxyCacheDefaultValue" minOccurs="0"/>
 *         <element name="srvPrefixDefaultValue" type="{}OutboundProxyCacheDefaultValue" minOccurs="0"/>
 *         <element name="outboundProxyDefaultValue" type="{}OutboundProxyCacheDefaultValue" minOccurs="0"/>
 *         <element name="transportTypeDefaultValue" type="{}OutboundProxyCacheDefaultValue" minOccurs="0"/>
 *         <element name="secureRtpDefaultValue" type="{}OutboundProxyCacheDefaultValue" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemOutboundProxyCacheParametersGetResponse", propOrder = {
    "evictionTimeoutMinutes",
    "refreshTimeoutMinutes",
    "auditIntervalMinutes",
    "maximumCacheSize",
    "dnsTypeDefaultValue",
    "useDnsSrvDefaultValue",
    "srvPrefixDefaultValue",
    "outboundProxyDefaultValue",
    "transportTypeDefaultValue",
    "secureRtpDefaultValue"
})
public class SystemOutboundProxyCacheParametersGetResponse
    extends OCIDataResponse
{

    protected int evictionTimeoutMinutes;
    protected int refreshTimeoutMinutes;
    protected int auditIntervalMinutes;
    protected int maximumCacheSize;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String dnsTypeDefaultValue;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String useDnsSrvDefaultValue;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String srvPrefixDefaultValue;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String outboundProxyDefaultValue;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String transportTypeDefaultValue;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String secureRtpDefaultValue;

    /**
     * Ruft den Wert der evictionTimeoutMinutes-Eigenschaft ab.
     * 
     */
    public int getEvictionTimeoutMinutes() {
        return evictionTimeoutMinutes;
    }

    /**
     * Legt den Wert der evictionTimeoutMinutes-Eigenschaft fest.
     * 
     */
    public void setEvictionTimeoutMinutes(int value) {
        this.evictionTimeoutMinutes = value;
    }

    /**
     * Ruft den Wert der refreshTimeoutMinutes-Eigenschaft ab.
     * 
     */
    public int getRefreshTimeoutMinutes() {
        return refreshTimeoutMinutes;
    }

    /**
     * Legt den Wert der refreshTimeoutMinutes-Eigenschaft fest.
     * 
     */
    public void setRefreshTimeoutMinutes(int value) {
        this.refreshTimeoutMinutes = value;
    }

    /**
     * Ruft den Wert der auditIntervalMinutes-Eigenschaft ab.
     * 
     */
    public int getAuditIntervalMinutes() {
        return auditIntervalMinutes;
    }

    /**
     * Legt den Wert der auditIntervalMinutes-Eigenschaft fest.
     * 
     */
    public void setAuditIntervalMinutes(int value) {
        this.auditIntervalMinutes = value;
    }

    /**
     * Ruft den Wert der maximumCacheSize-Eigenschaft ab.
     * 
     */
    public int getMaximumCacheSize() {
        return maximumCacheSize;
    }

    /**
     * Legt den Wert der maximumCacheSize-Eigenschaft fest.
     * 
     */
    public void setMaximumCacheSize(int value) {
        this.maximumCacheSize = value;
    }

    /**
     * Ruft den Wert der dnsTypeDefaultValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDnsTypeDefaultValue() {
        return dnsTypeDefaultValue;
    }

    /**
     * Legt den Wert der dnsTypeDefaultValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDnsTypeDefaultValue(String value) {
        this.dnsTypeDefaultValue = value;
    }

    /**
     * Ruft den Wert der useDnsSrvDefaultValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseDnsSrvDefaultValue() {
        return useDnsSrvDefaultValue;
    }

    /**
     * Legt den Wert der useDnsSrvDefaultValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseDnsSrvDefaultValue(String value) {
        this.useDnsSrvDefaultValue = value;
    }

    /**
     * Ruft den Wert der srvPrefixDefaultValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrvPrefixDefaultValue() {
        return srvPrefixDefaultValue;
    }

    /**
     * Legt den Wert der srvPrefixDefaultValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrvPrefixDefaultValue(String value) {
        this.srvPrefixDefaultValue = value;
    }

    /**
     * Ruft den Wert der outboundProxyDefaultValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutboundProxyDefaultValue() {
        return outboundProxyDefaultValue;
    }

    /**
     * Legt den Wert der outboundProxyDefaultValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutboundProxyDefaultValue(String value) {
        this.outboundProxyDefaultValue = value;
    }

    /**
     * Ruft den Wert der transportTypeDefaultValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportTypeDefaultValue() {
        return transportTypeDefaultValue;
    }

    /**
     * Legt den Wert der transportTypeDefaultValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportTypeDefaultValue(String value) {
        this.transportTypeDefaultValue = value;
    }

    /**
     * Ruft den Wert der secureRtpDefaultValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecureRtpDefaultValue() {
        return secureRtpDefaultValue;
    }

    /**
     * Legt den Wert der secureRtpDefaultValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecureRtpDefaultValue(String value) {
        this.secureRtpDefaultValue = value;
    }

}
