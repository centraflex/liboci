//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupCallParkGetAvailableAlternateRecallUserPagedSortedListResponse.
 *         Contains a table with column headings: "User Id", "Last Name", "First Name", "Hiragana Last Name", "Hiragana First Name",
 *         “Phone Number”, “Extension”, “Department”, “Email Address”. The Email Address will never be populated, since Hunt Groups don't
 *         have Email Addresses.
 *       
 * 
 * <p>Java-Klasse für GroupCallParkGetAvailableAlternateRecallUserPagedSortedListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallParkGetAvailableAlternateRecallUserPagedSortedListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="availableHuntGroupTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallParkGetAvailableAlternateRecallUserPagedSortedListResponse", propOrder = {
    "availableHuntGroupTable"
})
public class GroupCallParkGetAvailableAlternateRecallUserPagedSortedListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable availableHuntGroupTable;

    /**
     * Ruft den Wert der availableHuntGroupTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAvailableHuntGroupTable() {
        return availableHuntGroupTable;
    }

    /**
     * Legt den Wert der availableHuntGroupTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAvailableHuntGroupTable(OCITable value) {
        this.availableHuntGroupTable = value;
    }

}
