//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Outgoing Calling Plan initiating call forwards/transfer permissions.
 *       
 * 
 * <p>Java-Klasse für OutgoingCallingPlanRedirectingPermissions complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OutgoingCallingPlanRedirectingPermissions">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="group" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="local" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="tollFree" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="toll" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="international" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="operatorAssisted" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="chargeableDirectoryAssisted" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="specialServicesI" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="specialServicesII" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="premiumServicesI" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="premiumServicesII" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="casual" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="urlDialing" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="unknown" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingCallingPlanRedirectingPermissions", propOrder = {
    "group",
    "local",
    "tollFree",
    "toll",
    "international",
    "operatorAssisted",
    "chargeableDirectoryAssisted",
    "specialServicesI",
    "specialServicesII",
    "premiumServicesI",
    "premiumServicesII",
    "casual",
    "urlDialing",
    "unknown"
})
public class OutgoingCallingPlanRedirectingPermissions {

    protected boolean group;
    protected boolean local;
    protected boolean tollFree;
    protected boolean toll;
    protected boolean international;
    protected boolean operatorAssisted;
    protected boolean chargeableDirectoryAssisted;
    protected boolean specialServicesI;
    protected boolean specialServicesII;
    protected boolean premiumServicesI;
    protected boolean premiumServicesII;
    protected boolean casual;
    protected boolean urlDialing;
    protected boolean unknown;

    /**
     * Ruft den Wert der group-Eigenschaft ab.
     * 
     */
    public boolean isGroup() {
        return group;
    }

    /**
     * Legt den Wert der group-Eigenschaft fest.
     * 
     */
    public void setGroup(boolean value) {
        this.group = value;
    }

    /**
     * Ruft den Wert der local-Eigenschaft ab.
     * 
     */
    public boolean isLocal() {
        return local;
    }

    /**
     * Legt den Wert der local-Eigenschaft fest.
     * 
     */
    public void setLocal(boolean value) {
        this.local = value;
    }

    /**
     * Ruft den Wert der tollFree-Eigenschaft ab.
     * 
     */
    public boolean isTollFree() {
        return tollFree;
    }

    /**
     * Legt den Wert der tollFree-Eigenschaft fest.
     * 
     */
    public void setTollFree(boolean value) {
        this.tollFree = value;
    }

    /**
     * Ruft den Wert der toll-Eigenschaft ab.
     * 
     */
    public boolean isToll() {
        return toll;
    }

    /**
     * Legt den Wert der toll-Eigenschaft fest.
     * 
     */
    public void setToll(boolean value) {
        this.toll = value;
    }

    /**
     * Ruft den Wert der international-Eigenschaft ab.
     * 
     */
    public boolean isInternational() {
        return international;
    }

    /**
     * Legt den Wert der international-Eigenschaft fest.
     * 
     */
    public void setInternational(boolean value) {
        this.international = value;
    }

    /**
     * Ruft den Wert der operatorAssisted-Eigenschaft ab.
     * 
     */
    public boolean isOperatorAssisted() {
        return operatorAssisted;
    }

    /**
     * Legt den Wert der operatorAssisted-Eigenschaft fest.
     * 
     */
    public void setOperatorAssisted(boolean value) {
        this.operatorAssisted = value;
    }

    /**
     * Ruft den Wert der chargeableDirectoryAssisted-Eigenschaft ab.
     * 
     */
    public boolean isChargeableDirectoryAssisted() {
        return chargeableDirectoryAssisted;
    }

    /**
     * Legt den Wert der chargeableDirectoryAssisted-Eigenschaft fest.
     * 
     */
    public void setChargeableDirectoryAssisted(boolean value) {
        this.chargeableDirectoryAssisted = value;
    }

    /**
     * Ruft den Wert der specialServicesI-Eigenschaft ab.
     * 
     */
    public boolean isSpecialServicesI() {
        return specialServicesI;
    }

    /**
     * Legt den Wert der specialServicesI-Eigenschaft fest.
     * 
     */
    public void setSpecialServicesI(boolean value) {
        this.specialServicesI = value;
    }

    /**
     * Ruft den Wert der specialServicesII-Eigenschaft ab.
     * 
     */
    public boolean isSpecialServicesII() {
        return specialServicesII;
    }

    /**
     * Legt den Wert der specialServicesII-Eigenschaft fest.
     * 
     */
    public void setSpecialServicesII(boolean value) {
        this.specialServicesII = value;
    }

    /**
     * Ruft den Wert der premiumServicesI-Eigenschaft ab.
     * 
     */
    public boolean isPremiumServicesI() {
        return premiumServicesI;
    }

    /**
     * Legt den Wert der premiumServicesI-Eigenschaft fest.
     * 
     */
    public void setPremiumServicesI(boolean value) {
        this.premiumServicesI = value;
    }

    /**
     * Ruft den Wert der premiumServicesII-Eigenschaft ab.
     * 
     */
    public boolean isPremiumServicesII() {
        return premiumServicesII;
    }

    /**
     * Legt den Wert der premiumServicesII-Eigenschaft fest.
     * 
     */
    public void setPremiumServicesII(boolean value) {
        this.premiumServicesII = value;
    }

    /**
     * Ruft den Wert der casual-Eigenschaft ab.
     * 
     */
    public boolean isCasual() {
        return casual;
    }

    /**
     * Legt den Wert der casual-Eigenschaft fest.
     * 
     */
    public void setCasual(boolean value) {
        this.casual = value;
    }

    /**
     * Ruft den Wert der urlDialing-Eigenschaft ab.
     * 
     */
    public boolean isUrlDialing() {
        return urlDialing;
    }

    /**
     * Legt den Wert der urlDialing-Eigenschaft fest.
     * 
     */
    public void setUrlDialing(boolean value) {
        this.urlDialing = value;
    }

    /**
     * Ruft den Wert der unknown-Eigenschaft ab.
     * 
     */
    public boolean isUnknown() {
        return unknown;
    }

    /**
     * Legt den Wert der unknown-Eigenschaft fest.
     * 
     */
    public void setUnknown(boolean value) {
        this.unknown = value;
    }

}
