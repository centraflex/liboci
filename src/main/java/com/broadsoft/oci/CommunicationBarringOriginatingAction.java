//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CommunicationBarringOriginatingAction.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CommunicationBarringOriginatingAction">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Allow"/>
 *     <enumeration value="Allow Timed"/>
 *     <enumeration value="Block"/>
 *     <enumeration value="Authorization Code"/>
 *     <enumeration value="Authorization Code Timed"/>
 *     <enumeration value="Treatment"/>
 *     <enumeration value="Transfer"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CommunicationBarringOriginatingAction")
@XmlEnum
public enum CommunicationBarringOriginatingAction {

    @XmlEnumValue("Allow")
    ALLOW("Allow"),
    @XmlEnumValue("Allow Timed")
    ALLOW_TIMED("Allow Timed"),
    @XmlEnumValue("Block")
    BLOCK("Block"),
    @XmlEnumValue("Authorization Code")
    AUTHORIZATION_CODE("Authorization Code"),
    @XmlEnumValue("Authorization Code Timed")
    AUTHORIZATION_CODE_TIMED("Authorization Code Timed"),
    @XmlEnumValue("Treatment")
    TREATMENT("Treatment"),
    @XmlEnumValue("Transfer")
    TRANSFER("Transfer");
    private final String value;

    CommunicationBarringOriginatingAction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommunicationBarringOriginatingAction fromValue(String v) {
        for (CommunicationBarringOriginatingAction c: CommunicationBarringOriginatingAction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
