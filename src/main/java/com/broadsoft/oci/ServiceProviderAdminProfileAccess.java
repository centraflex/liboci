//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ServiceProviderAdminProfileAccess.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ServiceProviderAdminProfileAccess">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Full"/>
 *     <enumeration value="Read-Only"/>
 *     <enumeration value="None"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ServiceProviderAdminProfileAccess")
@XmlEnum
public enum ServiceProviderAdminProfileAccess {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("Read-Only")
    READ_ONLY("Read-Only"),
    @XmlEnumValue("None")
    NONE("None");
    private final String value;

    ServiceProviderAdminProfileAccess(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ServiceProviderAdminProfileAccess fromValue(String v) {
        for (ServiceProviderAdminProfileAccess c: ServiceProviderAdminProfileAccess.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
