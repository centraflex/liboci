//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to get the announcement repository file information.  
 * 
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           announcementFileExternalId
 * 
 *         The response is either UserAnnouncementFileGetResponse22 or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserAnnouncementFileGetRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserAnnouncementFileGetRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <choice>
 *         <sequence>
 *           <element name="userId" type="{}UserId"/>
 *           <element name="announcementFileKey" type="{}AnnouncementFileKey"/>
 *         </sequence>
 *         <element name="announcementFileExternalId" type="{}ExternalId"/>
 *       </choice>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserAnnouncementFileGetRequest22", propOrder = {
    "userId",
    "announcementFileKey",
    "announcementFileExternalId"
})
public class UserAnnouncementFileGetRequest22
    extends OCIRequest
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected AnnouncementFileKey announcementFileKey;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String announcementFileExternalId;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der announcementFileKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileKey }
     *     
     */
    public AnnouncementFileKey getAnnouncementFileKey() {
        return announcementFileKey;
    }

    /**
     * Legt den Wert der announcementFileKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileKey }
     *     
     */
    public void setAnnouncementFileKey(AnnouncementFileKey value) {
        this.announcementFileKey = value;
    }

    /**
     * Ruft den Wert der announcementFileExternalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnouncementFileExternalId() {
        return announcementFileExternalId;
    }

    /**
     * Legt den Wert der announcementFileExternalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnouncementFileExternalId(String value) {
        this.announcementFileExternalId = value;
    }

}
