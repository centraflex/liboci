//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify an existing Communication Barring Profile.
 *         When the originatingDefaultAction is not Treatment, originatingDefaultTreatmentId
 *         will be automatically cleared. Also when the action of originatingRule is not Treatment,
 *         treatmentId will be automatically cleared for the rule.
 *         The priorities for IncomingRules are requantized to consecutive integers as part of the modify.
 *         The response is either a SuccessResponse or an ErrorResponse.
 * 	      The following elements are only used in AS data mode:
 *            callMeNowDefaultAction
 *            callMeNowDefaultCallTimeout
 *            callMeNowRule
 *            applyToAttendedCallTransfers
 *       
 * 
 * <p>Java-Klasse für SystemCommunicationBarringProfileModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCommunicationBarringProfileModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="name" type="{}CommunicationBarringProfileName"/>
 *         <element name="newName" type="{}CommunicationBarringProfileName" minOccurs="0"/>
 *         <element name="description" type="{}CommunicationBarringProfileDescription" minOccurs="0"/>
 *         <element name="originatingDefaultAction" type="{}CommunicationBarringOriginatingAction" minOccurs="0"/>
 *         <element name="originatingDefaultTreatmentId" type="{}TreatmentId" minOccurs="0"/>
 *         <element name="originatingDefaultTransferNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="originatingDefaultCallTimeout" type="{}CommunicationBarringTimeoutSeconds" minOccurs="0"/>
 *         <element name="originatingRule" type="{}ReplacementCommunicationBarringOriginatingRuleList" minOccurs="0"/>
 *         <element name="redirectingDefaultAction" type="{}CommunicationBarringRedirectingAction" minOccurs="0"/>
 *         <element name="redirectingDefaultCallTimeout" type="{}CommunicationBarringTimeoutSeconds" minOccurs="0"/>
 *         <element name="redirectingRule" type="{}ReplacementCommunicationBarringRedirectingRuleList" minOccurs="0"/>
 *         <element name="incomingDefaultAction" type="{}CommunicationBarringIncomingAction" minOccurs="0"/>
 *         <element name="incomingDefaultCallTimeout" type="{}CommunicationBarringTimeoutSeconds" minOccurs="0"/>
 *         <element name="incomingRule" type="{}ReplacementCommunicationBarringIncomingRuleList19sp1" minOccurs="0"/>
 *         <element name="callMeNowDefaultAction" type="{}CommunicationBarringCallMeNowAction" minOccurs="0"/>
 *         <element name="callMeNowDefaultCallTimeout" type="{}CommunicationBarringTimeoutSeconds" minOccurs="0"/>
 *         <element name="callMeNowRule" type="{}ReplacementCommunicationBarringCallMeNowRuleList" minOccurs="0"/>
 *         <element name="applyToAttendedCallTransfers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCommunicationBarringProfileModifyRequest", propOrder = {
    "name",
    "newName",
    "description",
    "originatingDefaultAction",
    "originatingDefaultTreatmentId",
    "originatingDefaultTransferNumber",
    "originatingDefaultCallTimeout",
    "originatingRule",
    "redirectingDefaultAction",
    "redirectingDefaultCallTimeout",
    "redirectingRule",
    "incomingDefaultAction",
    "incomingDefaultCallTimeout",
    "incomingRule",
    "callMeNowDefaultAction",
    "callMeNowDefaultCallTimeout",
    "callMeNowRule",
    "applyToAttendedCallTransfers"
})
public class SystemCommunicationBarringProfileModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newName;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlSchemaType(name = "token")
    protected CommunicationBarringOriginatingAction originatingDefaultAction;
    @XmlElementRef(name = "originatingDefaultTreatmentId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> originatingDefaultTreatmentId;
    @XmlElementRef(name = "originatingDefaultTransferNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> originatingDefaultTransferNumber;
    @XmlElementRef(name = "originatingDefaultCallTimeout", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> originatingDefaultCallTimeout;
    @XmlElementRef(name = "originatingRule", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementCommunicationBarringOriginatingRuleList> originatingRule;
    @XmlSchemaType(name = "token")
    protected CommunicationBarringRedirectingAction redirectingDefaultAction;
    @XmlElementRef(name = "redirectingDefaultCallTimeout", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> redirectingDefaultCallTimeout;
    @XmlElementRef(name = "redirectingRule", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementCommunicationBarringRedirectingRuleList> redirectingRule;
    @XmlSchemaType(name = "token")
    protected CommunicationBarringIncomingAction incomingDefaultAction;
    @XmlElementRef(name = "incomingDefaultCallTimeout", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> incomingDefaultCallTimeout;
    @XmlElementRef(name = "incomingRule", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementCommunicationBarringIncomingRuleList19Sp1> incomingRule;
    @XmlSchemaType(name = "token")
    protected CommunicationBarringCallMeNowAction callMeNowDefaultAction;
    @XmlElementRef(name = "callMeNowDefaultCallTimeout", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> callMeNowDefaultCallTimeout;
    @XmlElementRef(name = "callMeNowRule", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementCommunicationBarringCallMeNowRuleList> callMeNowRule;
    protected Boolean applyToAttendedCallTransfers;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der newName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewName() {
        return newName;
    }

    /**
     * Legt den Wert der newName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewName(String value) {
        this.newName = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der originatingDefaultAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringOriginatingAction }
     *     
     */
    public CommunicationBarringOriginatingAction getOriginatingDefaultAction() {
        return originatingDefaultAction;
    }

    /**
     * Legt den Wert der originatingDefaultAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringOriginatingAction }
     *     
     */
    public void setOriginatingDefaultAction(CommunicationBarringOriginatingAction value) {
        this.originatingDefaultAction = value;
    }

    /**
     * Ruft den Wert der originatingDefaultTreatmentId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOriginatingDefaultTreatmentId() {
        return originatingDefaultTreatmentId;
    }

    /**
     * Legt den Wert der originatingDefaultTreatmentId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOriginatingDefaultTreatmentId(JAXBElement<String> value) {
        this.originatingDefaultTreatmentId = value;
    }

    /**
     * Ruft den Wert der originatingDefaultTransferNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOriginatingDefaultTransferNumber() {
        return originatingDefaultTransferNumber;
    }

    /**
     * Legt den Wert der originatingDefaultTransferNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOriginatingDefaultTransferNumber(JAXBElement<String> value) {
        this.originatingDefaultTransferNumber = value;
    }

    /**
     * Ruft den Wert der originatingDefaultCallTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getOriginatingDefaultCallTimeout() {
        return originatingDefaultCallTimeout;
    }

    /**
     * Legt den Wert der originatingDefaultCallTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setOriginatingDefaultCallTimeout(JAXBElement<Integer> value) {
        this.originatingDefaultCallTimeout = value;
    }

    /**
     * Ruft den Wert der originatingRule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringOriginatingRuleList }{@code >}
     *     
     */
    public JAXBElement<ReplacementCommunicationBarringOriginatingRuleList> getOriginatingRule() {
        return originatingRule;
    }

    /**
     * Legt den Wert der originatingRule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringOriginatingRuleList }{@code >}
     *     
     */
    public void setOriginatingRule(JAXBElement<ReplacementCommunicationBarringOriginatingRuleList> value) {
        this.originatingRule = value;
    }

    /**
     * Ruft den Wert der redirectingDefaultAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringRedirectingAction }
     *     
     */
    public CommunicationBarringRedirectingAction getRedirectingDefaultAction() {
        return redirectingDefaultAction;
    }

    /**
     * Legt den Wert der redirectingDefaultAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringRedirectingAction }
     *     
     */
    public void setRedirectingDefaultAction(CommunicationBarringRedirectingAction value) {
        this.redirectingDefaultAction = value;
    }

    /**
     * Ruft den Wert der redirectingDefaultCallTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getRedirectingDefaultCallTimeout() {
        return redirectingDefaultCallTimeout;
    }

    /**
     * Legt den Wert der redirectingDefaultCallTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setRedirectingDefaultCallTimeout(JAXBElement<Integer> value) {
        this.redirectingDefaultCallTimeout = value;
    }

    /**
     * Ruft den Wert der redirectingRule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringRedirectingRuleList }{@code >}
     *     
     */
    public JAXBElement<ReplacementCommunicationBarringRedirectingRuleList> getRedirectingRule() {
        return redirectingRule;
    }

    /**
     * Legt den Wert der redirectingRule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringRedirectingRuleList }{@code >}
     *     
     */
    public void setRedirectingRule(JAXBElement<ReplacementCommunicationBarringRedirectingRuleList> value) {
        this.redirectingRule = value;
    }

    /**
     * Ruft den Wert der incomingDefaultAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringIncomingAction }
     *     
     */
    public CommunicationBarringIncomingAction getIncomingDefaultAction() {
        return incomingDefaultAction;
    }

    /**
     * Legt den Wert der incomingDefaultAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringIncomingAction }
     *     
     */
    public void setIncomingDefaultAction(CommunicationBarringIncomingAction value) {
        this.incomingDefaultAction = value;
    }

    /**
     * Ruft den Wert der incomingDefaultCallTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getIncomingDefaultCallTimeout() {
        return incomingDefaultCallTimeout;
    }

    /**
     * Legt den Wert der incomingDefaultCallTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setIncomingDefaultCallTimeout(JAXBElement<Integer> value) {
        this.incomingDefaultCallTimeout = value;
    }

    /**
     * Ruft den Wert der incomingRule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringIncomingRuleList19Sp1 }{@code >}
     *     
     */
    public JAXBElement<ReplacementCommunicationBarringIncomingRuleList19Sp1> getIncomingRule() {
        return incomingRule;
    }

    /**
     * Legt den Wert der incomingRule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringIncomingRuleList19Sp1 }{@code >}
     *     
     */
    public void setIncomingRule(JAXBElement<ReplacementCommunicationBarringIncomingRuleList19Sp1> value) {
        this.incomingRule = value;
    }

    /**
     * Ruft den Wert der callMeNowDefaultAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringCallMeNowAction }
     *     
     */
    public CommunicationBarringCallMeNowAction getCallMeNowDefaultAction() {
        return callMeNowDefaultAction;
    }

    /**
     * Legt den Wert der callMeNowDefaultAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringCallMeNowAction }
     *     
     */
    public void setCallMeNowDefaultAction(CommunicationBarringCallMeNowAction value) {
        this.callMeNowDefaultAction = value;
    }

    /**
     * Ruft den Wert der callMeNowDefaultCallTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCallMeNowDefaultCallTimeout() {
        return callMeNowDefaultCallTimeout;
    }

    /**
     * Legt den Wert der callMeNowDefaultCallTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCallMeNowDefaultCallTimeout(JAXBElement<Integer> value) {
        this.callMeNowDefaultCallTimeout = value;
    }

    /**
     * Ruft den Wert der callMeNowRule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringCallMeNowRuleList }{@code >}
     *     
     */
    public JAXBElement<ReplacementCommunicationBarringCallMeNowRuleList> getCallMeNowRule() {
        return callMeNowRule;
    }

    /**
     * Legt den Wert der callMeNowRule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringCallMeNowRuleList }{@code >}
     *     
     */
    public void setCallMeNowRule(JAXBElement<ReplacementCommunicationBarringCallMeNowRuleList> value) {
        this.callMeNowRule = value;
    }

    /**
     * Ruft den Wert der applyToAttendedCallTransfers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isApplyToAttendedCallTransfers() {
        return applyToAttendedCallTransfers;
    }

    /**
     * Legt den Wert der applyToAttendedCallTransfers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setApplyToAttendedCallTransfers(Boolean value) {
        this.applyToAttendedCallTransfers = value;
    }

}
