//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserVoiceMessagingUserGetVoicePortalRequest20.
 *       
 * 
 * <p>Java-Klasse für UserVoiceMessagingUserGetVoicePortalResponse20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserVoiceMessagingUserGetVoicePortalResponse20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="usePersonalizedName" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="voicePortalAutoLogin" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="personalizedNameAudioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserVoiceMessagingUserGetVoicePortalResponse20", propOrder = {
    "usePersonalizedName",
    "voicePortalAutoLogin",
    "personalizedNameAudioFile"
})
public class UserVoiceMessagingUserGetVoicePortalResponse20
    extends OCIDataResponse
{

    protected boolean usePersonalizedName;
    protected boolean voicePortalAutoLogin;
    protected AnnouncementFileLevelKey personalizedNameAudioFile;

    /**
     * Ruft den Wert der usePersonalizedName-Eigenschaft ab.
     * 
     */
    public boolean isUsePersonalizedName() {
        return usePersonalizedName;
    }

    /**
     * Legt den Wert der usePersonalizedName-Eigenschaft fest.
     * 
     */
    public void setUsePersonalizedName(boolean value) {
        this.usePersonalizedName = value;
    }

    /**
     * Ruft den Wert der voicePortalAutoLogin-Eigenschaft ab.
     * 
     */
    public boolean isVoicePortalAutoLogin() {
        return voicePortalAutoLogin;
    }

    /**
     * Legt den Wert der voicePortalAutoLogin-Eigenschaft fest.
     * 
     */
    public void setVoicePortalAutoLogin(boolean value) {
        this.voicePortalAutoLogin = value;
    }

    /**
     * Ruft den Wert der personalizedNameAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getPersonalizedNameAudioFile() {
        return personalizedNameAudioFile;
    }

    /**
     * Legt den Wert der personalizedNameAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setPersonalizedNameAudioFile(AnnouncementFileLevelKey value) {
        this.personalizedNameAudioFile = value;
    }

}
