//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Pilot User"/>
 *     <enumeration value="User"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy")
@XmlEnum
public enum TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy {

    @XmlEnumValue("Pilot User")
    PILOT_USER("Pilot User"),
    @XmlEnumValue("User")
    USER("User");
    private final String value;

    TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy fromValue(String v) {
        for (TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy c: TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
