//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for a particular fully specified DeviceManagement event action.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactDeviceManagementEventAction complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactDeviceManagementEventAction">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="dmEventAction" type="{}DeviceManagementEventAction"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactDeviceManagementEventAction", propOrder = {
    "dmEventAction"
})
public class SearchCriteriaExactDeviceManagementEventAction
    extends SearchCriteria
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceManagementEventAction dmEventAction;

    /**
     * Ruft den Wert der dmEventAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementEventAction }
     *     
     */
    public DeviceManagementEventAction getDmEventAction() {
        return dmEventAction;
    }

    /**
     * Legt den Wert der dmEventAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementEventAction }
     *     
     */
    public void setDmEventAction(DeviceManagementEventAction value) {
        this.dmEventAction = value;
    }

}
