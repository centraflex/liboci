//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The modify configuration of a key for Auto Attendant.
 *       
 * 
 * <p>Java-Klasse für AutoAttendantKeyModifyConfiguration20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="AutoAttendantKeyModifyConfiguration20">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="key" type="{}AutoAttendantMenuKey"/>
 *         <element name="entry" type="{}AutoAttendantKeyConfigurationModifyEntry20"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutoAttendantKeyModifyConfiguration20", propOrder = {
    "key",
    "entry"
})
public class AutoAttendantKeyModifyConfiguration20 {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String key;
    @XmlElement(required = true, nillable = true)
    protected AutoAttendantKeyConfigurationModifyEntry20 entry;

    /**
     * Ruft den Wert der key-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Legt den Wert der key-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Ruft den Wert der entry-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantKeyConfigurationModifyEntry20 }
     *     
     */
    public AutoAttendantKeyConfigurationModifyEntry20 getEntry() {
        return entry;
    }

    /**
     * Legt den Wert der entry-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantKeyConfigurationModifyEntry20 }
     *     
     */
    public void setEntry(AutoAttendantKeyConfigurationModifyEntry20 value) {
        this.entry = value;
    }

}
