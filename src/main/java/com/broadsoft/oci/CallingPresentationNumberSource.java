//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallingPresentationNumberSource.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallingPresentationNumberSource">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Configurable CLID"/>
 *     <enumeration value="DNIS"/>
 *     <enumeration value="Emergency"/>
 *     <enumeration value="Group"/>
 *     <enumeration value="Trunk Group"/>
 *     <enumeration value="User"/>
 *     <enumeration value="Department"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallingPresentationNumberSource")
@XmlEnum
public enum CallingPresentationNumberSource {

    @XmlEnumValue("Configurable CLID")
    CONFIGURABLE_CLID("Configurable CLID"),
    DNIS("DNIS"),
    @XmlEnumValue("Emergency")
    EMERGENCY("Emergency"),
    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("Trunk Group")
    TRUNK_GROUP("Trunk Group"),
    @XmlEnumValue("User")
    USER("User"),
    @XmlEnumValue("Department")
    DEPARTMENT("Department");
    private final String value;

    CallingPresentationNumberSource(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallingPresentationNumberSource fromValue(String v) {
        for (CallingPresentationNumberSource c: CallingPresentationNumberSource.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
