//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to get the input parameter info for a call center report template.
 *         The response is either a UserCallCenterEnhancedReportingReportTemplateParamInfoGetResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserCallCenterEnhancedReportingReportTemplateParamInfoGetRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallCenterEnhancedReportingReportTemplateParamInfoGetRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="reportTemplate" type="{}CallCenterReportTemplateKey"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallCenterEnhancedReportingReportTemplateParamInfoGetRequest", propOrder = {
    "reportTemplate"
})
public class UserCallCenterEnhancedReportingReportTemplateParamInfoGetRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    protected CallCenterReportTemplateKey reportTemplate;

    /**
     * Ruft den Wert der reportTemplate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportTemplateKey }
     *     
     */
    public CallCenterReportTemplateKey getReportTemplate() {
        return reportTemplate;
    }

    /**
     * Legt den Wert der reportTemplate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportTemplateKey }
     *     
     */
    public void setReportTemplate(CallCenterReportTemplateKey value) {
        this.reportTemplate = value;
    }

}
