//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemMigratedUsersGetListRequest22.
 *         The optional totalNumberOfMigratedUsers is returned only when the userListSizeLimit is set in the request and 
 *         if the total number of migrated users is greater than the value of userListSizeLimit.
 *       
 * 
 * <p>Java-Klasse für SystemMigratedUsersGetListResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemMigratedUsersGetListResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="userId" type="{}UserId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="totalNumberOfMigratedUsers" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemMigratedUsersGetListResponse22", propOrder = {
    "userId",
    "totalNumberOfMigratedUsers"
})
public class SystemMigratedUsersGetListResponse22
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> userId;
    protected Integer totalNumberOfMigratedUsers;

    /**
     * Gets the value of the userId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the userId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the userId property.
     */
    public List<String> getUserId() {
        if (userId == null) {
            userId = new ArrayList<>();
        }
        return this.userId;
    }

    /**
     * Ruft den Wert der totalNumberOfMigratedUsers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalNumberOfMigratedUsers() {
        return totalNumberOfMigratedUsers;
    }

    /**
     * Legt den Wert der totalNumberOfMigratedUsers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalNumberOfMigratedUsers(Integer value) {
        this.totalNumberOfMigratedUsers = value;
    }

}
