//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to enable or disable automatic trigger of DM events for
 *         OCI request prefixes, regardless of the request's
 *         version.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemDeviceManagementAutoRebuildConfigModifyListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceManagementAutoRebuildConfigModifyListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="autoRebuildConfigEntry" type="{}DeviceManagementAutoRebuildConfigEntry" maxOccurs="unbounded"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceManagementAutoRebuildConfigModifyListRequest", propOrder = {
    "autoRebuildConfigEntry"
})
public class SystemDeviceManagementAutoRebuildConfigModifyListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    protected List<DeviceManagementAutoRebuildConfigEntry> autoRebuildConfigEntry;

    /**
     * Gets the value of the autoRebuildConfigEntry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the autoRebuildConfigEntry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAutoRebuildConfigEntry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DeviceManagementAutoRebuildConfigEntry }
     * 
     * 
     * @return
     *     The value of the autoRebuildConfigEntry property.
     */
    public List<DeviceManagementAutoRebuildConfigEntry> getAutoRebuildConfigEntry() {
        if (autoRebuildConfigEntry == null) {
            autoRebuildConfigEntry = new ArrayList<>();
        }
        return this.autoRebuildConfigEntry;
    }

}
