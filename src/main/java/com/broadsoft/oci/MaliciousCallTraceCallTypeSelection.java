//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für MaliciousCallTraceCallTypeSelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="MaliciousCallTraceCallTypeSelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="All Incoming"/>
 *     <enumeration value="Answered Incoming"/>
 *     <enumeration value="All Incoming And Outgoing"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "MaliciousCallTraceCallTypeSelection")
@XmlEnum
public enum MaliciousCallTraceCallTypeSelection {

    @XmlEnumValue("All Incoming")
    ALL_INCOMING("All Incoming"),
    @XmlEnumValue("Answered Incoming")
    ANSWERED_INCOMING("Answered Incoming"),
    @XmlEnumValue("All Incoming And Outgoing")
    ALL_INCOMING_AND_OUTGOING("All Incoming And Outgoing");
    private final String value;

    MaliciousCallTraceCallTypeSelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MaliciousCallTraceCallTypeSelection fromValue(String v) {
        for (MaliciousCallTraceCallTypeSelection c: MaliciousCallTraceCallTypeSelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
