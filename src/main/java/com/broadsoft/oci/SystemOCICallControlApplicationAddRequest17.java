//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add an application to the OCI call control application list.
 *         The response is either SuccessResponse or ErrorResponse.
 *         
 *         Replaced by: SystemOCICallControlApplicationAddRequest22
 *       
 * 
 * <p>Java-Klasse für SystemOCICallControlApplicationAddRequest17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemOCICallControlApplicationAddRequest17">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="applicationId" type="{}OCICallControlApplicationId"/>
 *         <element name="enableSystemWide" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="notificationTimeoutSeconds" type="{}OCICallApplicationNotificationTimeOutSeconds"/>
 *         <element name="description" type="{}OCICallControlApplicationDescription" minOccurs="0"/>
 *         <element name="maxEventChannelsPerSet" type="{}EventNotificationChannelsPerSet"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemOCICallControlApplicationAddRequest17", propOrder = {
    "applicationId",
    "enableSystemWide",
    "notificationTimeoutSeconds",
    "description",
    "maxEventChannelsPerSet"
})
public class SystemOCICallControlApplicationAddRequest17
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String applicationId;
    protected boolean enableSystemWide;
    protected int notificationTimeoutSeconds;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    protected int maxEventChannelsPerSet;

    /**
     * Ruft den Wert der applicationId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationId() {
        return applicationId;
    }

    /**
     * Legt den Wert der applicationId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationId(String value) {
        this.applicationId = value;
    }

    /**
     * Ruft den Wert der enableSystemWide-Eigenschaft ab.
     * 
     */
    public boolean isEnableSystemWide() {
        return enableSystemWide;
    }

    /**
     * Legt den Wert der enableSystemWide-Eigenschaft fest.
     * 
     */
    public void setEnableSystemWide(boolean value) {
        this.enableSystemWide = value;
    }

    /**
     * Ruft den Wert der notificationTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getNotificationTimeoutSeconds() {
        return notificationTimeoutSeconds;
    }

    /**
     * Legt den Wert der notificationTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setNotificationTimeoutSeconds(int value) {
        this.notificationTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der maxEventChannelsPerSet-Eigenschaft ab.
     * 
     */
    public int getMaxEventChannelsPerSet() {
        return maxEventChannelsPerSet;
    }

    /**
     * Legt den Wert der maxEventChannelsPerSet-Eigenschaft fest.
     * 
     */
    public void setMaxEventChannelsPerSet(int value) {
        this.maxEventChannelsPerSet = value;
    }

}
