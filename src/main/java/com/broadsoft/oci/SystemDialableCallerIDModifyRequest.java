//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level Dialable Caller ID criteria list.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemDialableCallerIDModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDialableCallerIDModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="criteriaPriorityOrder" type="{}DialableCallerIDCriteriaPriorityOrder" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDialableCallerIDModifyRequest", propOrder = {
    "criteriaPriorityOrder"
})
public class SystemDialableCallerIDModifyRequest
    extends OCIRequest
{

    protected List<DialableCallerIDCriteriaPriorityOrder> criteriaPriorityOrder;

    /**
     * Gets the value of the criteriaPriorityOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the criteriaPriorityOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCriteriaPriorityOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DialableCallerIDCriteriaPriorityOrder }
     * 
     * 
     * @return
     *     The value of the criteriaPriorityOrder property.
     */
    public List<DialableCallerIDCriteriaPriorityOrder> getCriteriaPriorityOrder() {
        if (criteriaPriorityOrder == null) {
            criteriaPriorityOrder = new ArrayList<>();
        }
        return this.criteriaPriorityOrder;
    }

}
