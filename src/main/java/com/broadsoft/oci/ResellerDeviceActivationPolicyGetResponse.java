//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ResellerDeviceActivationPolicyGetRequest.
 *       
 * 
 * <p>Java-Klasse für ResellerDeviceActivationPolicyGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerDeviceActivationPolicyGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useResellerSettings" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowActivationCodeRequestByUser" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sendActivationCodeInEmail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerDeviceActivationPolicyGetResponse", propOrder = {
    "useResellerSettings",
    "allowActivationCodeRequestByUser",
    "sendActivationCodeInEmail"
})
public class ResellerDeviceActivationPolicyGetResponse
    extends OCIDataResponse
{

    protected boolean useResellerSettings;
    protected boolean allowActivationCodeRequestByUser;
    protected boolean sendActivationCodeInEmail;

    /**
     * Ruft den Wert der useResellerSettings-Eigenschaft ab.
     * 
     */
    public boolean isUseResellerSettings() {
        return useResellerSettings;
    }

    /**
     * Legt den Wert der useResellerSettings-Eigenschaft fest.
     * 
     */
    public void setUseResellerSettings(boolean value) {
        this.useResellerSettings = value;
    }

    /**
     * Ruft den Wert der allowActivationCodeRequestByUser-Eigenschaft ab.
     * 
     */
    public boolean isAllowActivationCodeRequestByUser() {
        return allowActivationCodeRequestByUser;
    }

    /**
     * Legt den Wert der allowActivationCodeRequestByUser-Eigenschaft fest.
     * 
     */
    public void setAllowActivationCodeRequestByUser(boolean value) {
        this.allowActivationCodeRequestByUser = value;
    }

    /**
     * Ruft den Wert der sendActivationCodeInEmail-Eigenschaft ab.
     * 
     */
    public boolean isSendActivationCodeInEmail() {
        return sendActivationCodeInEmail;
    }

    /**
     * Legt den Wert der sendActivationCodeInEmail-Eigenschaft fest.
     * 
     */
    public void setSendActivationCodeInEmail(boolean value) {
        this.sendActivationCodeInEmail = value;
    }

}
