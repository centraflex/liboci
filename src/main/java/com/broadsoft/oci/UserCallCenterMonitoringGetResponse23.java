//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserCallCenterMonitoringGetRequest23.
 *       
 * 
 * <p>Java-Klasse für UserCallCenterMonitoringGetResponse23 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallCenterMonitoringGetResponse23">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="playToneToAgentForSilentMonitoring" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="playToneToAgentForSupervisorCoaching" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallCenterMonitoringGetResponse23", propOrder = {
    "playToneToAgentForSilentMonitoring",
    "playToneToAgentForSupervisorCoaching"
})
public class UserCallCenterMonitoringGetResponse23
    extends OCIDataResponse
{

    protected boolean playToneToAgentForSilentMonitoring;
    protected boolean playToneToAgentForSupervisorCoaching;

    /**
     * Ruft den Wert der playToneToAgentForSilentMonitoring-Eigenschaft ab.
     * 
     */
    public boolean isPlayToneToAgentForSilentMonitoring() {
        return playToneToAgentForSilentMonitoring;
    }

    /**
     * Legt den Wert der playToneToAgentForSilentMonitoring-Eigenschaft fest.
     * 
     */
    public void setPlayToneToAgentForSilentMonitoring(boolean value) {
        this.playToneToAgentForSilentMonitoring = value;
    }

    /**
     * Ruft den Wert der playToneToAgentForSupervisorCoaching-Eigenschaft ab.
     * 
     */
    public boolean isPlayToneToAgentForSupervisorCoaching() {
        return playToneToAgentForSupervisorCoaching;
    }

    /**
     * Legt den Wert der playToneToAgentForSupervisorCoaching-Eigenschaft fest.
     * 
     */
    public void setPlayToneToAgentForSupervisorCoaching(boolean value) {
        this.playToneToAgentForSupervisorCoaching = value;
    }

}
