//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemEnterpriseTrunkGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemEnterpriseTrunkGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemEnterpriseTrunkGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableHoldoverOfHighwaterCallCounts" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="holdoverPeriod" type="{}EnterpriseTrunkHighwateCallCountHoldoverPeriodMinutes"/>
 *         <element name="timeZoneOffsetMinutes" type="{}EnterpriseTrunkTimeZoneOffsetMinutes"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemEnterpriseTrunkGetResponse", propOrder = {
    "enableHoldoverOfHighwaterCallCounts",
    "holdoverPeriod",
    "timeZoneOffsetMinutes"
})
public class SystemEnterpriseTrunkGetResponse
    extends OCIDataResponse
{

    protected boolean enableHoldoverOfHighwaterCallCounts;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String holdoverPeriod;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeZoneOffsetMinutes;

    /**
     * Ruft den Wert der enableHoldoverOfHighwaterCallCounts-Eigenschaft ab.
     * 
     */
    public boolean isEnableHoldoverOfHighwaterCallCounts() {
        return enableHoldoverOfHighwaterCallCounts;
    }

    /**
     * Legt den Wert der enableHoldoverOfHighwaterCallCounts-Eigenschaft fest.
     * 
     */
    public void setEnableHoldoverOfHighwaterCallCounts(boolean value) {
        this.enableHoldoverOfHighwaterCallCounts = value;
    }

    /**
     * Ruft den Wert der holdoverPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoldoverPeriod() {
        return holdoverPeriod;
    }

    /**
     * Legt den Wert der holdoverPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoldoverPeriod(String value) {
        this.holdoverPeriod = value;
    }

    /**
     * Ruft den Wert der timeZoneOffsetMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeZoneOffsetMinutes() {
        return timeZoneOffsetMinutes;
    }

    /**
     * Legt den Wert der timeZoneOffsetMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeZoneOffsetMinutes(String value) {
        this.timeZoneOffsetMinutes = value;
    }

}
