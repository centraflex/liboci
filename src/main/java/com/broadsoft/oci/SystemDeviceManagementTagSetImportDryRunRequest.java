//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to perform a dry run for the import of a Tag Set Archive File (TSAF) as a new Tag Set.  The URL supports the HTTP and the FILE protocols.
 *         When the optional element resellerId is specified, the device type created is at reseller level. 
 *         When the optional element tagSetOverride is set to true, an existing tag set on the destination system will be overridden.
 *         When the optional element tagSetRename is set, on import the tag set name part of the TSAF will be changed to the desired name on the destination system.
 *         The response is either a SystemDeviceManagementTagSetImportDryRunResponse or an ErrorResponse.
 *         
 *         The following data elements are only used in AS data mode and ignored in XS data mode:
 *           resellerId        
 *       
 * 
 * <p>Java-Klasse für SystemDeviceManagementTagSetImportDryRunRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceManagementTagSetImportDryRunRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="file" type="{}URL"/>
 *         <element name="resellerId" type="{}ResellerId22" minOccurs="0"/>
 *         <element name="tagSetOverride" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="tagSetRename" type="{}DeviceManagementTagSetRename" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceManagementTagSetImportDryRunRequest", propOrder = {
    "file",
    "resellerId",
    "tagSetOverride",
    "tagSetRename"
})
public class SystemDeviceManagementTagSetImportDryRunRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String file;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    protected Boolean tagSetOverride;
    protected DeviceManagementTagSetRename tagSetRename;

    /**
     * Ruft den Wert der file-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFile() {
        return file;
    }

    /**
     * Legt den Wert der file-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFile(String value) {
        this.file = value;
    }

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Ruft den Wert der tagSetOverride-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTagSetOverride() {
        return tagSetOverride;
    }

    /**
     * Legt den Wert der tagSetOverride-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTagSetOverride(Boolean value) {
        this.tagSetOverride = value;
    }

    /**
     * Ruft den Wert der tagSetRename-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementTagSetRename }
     *     
     */
    public DeviceManagementTagSetRename getTagSetRename() {
        return tagSetRename;
    }

    /**
     * Legt den Wert der tagSetRename-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementTagSetRename }
     *     
     */
    public void setTagSetRename(DeviceManagementTagSetRename value) {
        this.tagSetRename = value;
    }

}
