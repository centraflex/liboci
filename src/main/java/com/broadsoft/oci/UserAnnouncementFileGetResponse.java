//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserAnnouncementFileGetRequest.
 *         The response contains the file size (KB), uploaded timestamp, description and usage for 
 *         an announcement file in the user announcement repository.
 *         The usage table has columns "Service Name", "Criteria Name"
 *         The "Service Name"" values correspond to string values of the UserService data types.
 *         With the exception of the string "Voice Portal" which is returned when the announcement is being used by Voice Portal Personalized Name. 
 *         For Call Center and Route Point users the "Instance Name" column contains the instance id and 
 *         when the announcement is being used by a DNIS, "Intance Name" column contans the instance id and the DNIS id.
 *         For Auto Attendants with submenus and the announcement is used by a submenu the "Instance Name" column will contain the submenu name
 *         
 *         Replaced by: UserAnnouncementFileGetResponse22.
 *       
 * 
 * <p>Java-Klasse für UserAnnouncementFileGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserAnnouncementFileGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="description" type="{}FileDescription"/>
 *         <element name="filesize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="lastUploaded" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         <element name="usageTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserAnnouncementFileGetResponse", propOrder = {
    "description",
    "filesize",
    "lastUploaded",
    "usageTable"
})
public class UserAnnouncementFileGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    protected int filesize;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastUploaded;
    @XmlElement(required = true)
    protected OCITable usageTable;

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der filesize-Eigenschaft ab.
     * 
     */
    public int getFilesize() {
        return filesize;
    }

    /**
     * Legt den Wert der filesize-Eigenschaft fest.
     * 
     */
    public void setFilesize(int value) {
        this.filesize = value;
    }

    /**
     * Ruft den Wert der lastUploaded-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastUploaded() {
        return lastUploaded;
    }

    /**
     * Legt den Wert der lastUploaded-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastUploaded(XMLGregorianCalendar value) {
        this.lastUploaded = value;
    }

    /**
     * Ruft den Wert der usageTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUsageTable() {
        return usageTable;
    }

    /**
     * Legt den Wert der usageTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUsageTable(OCITable value) {
        this.usageTable = value;
    }

}
