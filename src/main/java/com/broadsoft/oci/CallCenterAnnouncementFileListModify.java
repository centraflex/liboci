//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains a list of audio or video files to modify.
 *       
 * 
 * <p>Java-Klasse für CallCenterAnnouncementFileListModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterAnnouncementFileListModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="file1" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *         <element name="file2" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *         <element name="file3" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *         <element name="file4" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterAnnouncementFileListModify", propOrder = {
    "file1",
    "file2",
    "file3",
    "file4"
})
public class CallCenterAnnouncementFileListModify {

    @XmlElementRef(name = "file1", type = JAXBElement.class, required = false)
    protected JAXBElement<LabeledMediaFileResource> file1;
    @XmlElementRef(name = "file2", type = JAXBElement.class, required = false)
    protected JAXBElement<LabeledMediaFileResource> file2;
    @XmlElementRef(name = "file3", type = JAXBElement.class, required = false)
    protected JAXBElement<LabeledMediaFileResource> file3;
    @XmlElementRef(name = "file4", type = JAXBElement.class, required = false)
    protected JAXBElement<LabeledMediaFileResource> file4;

    /**
     * Ruft den Wert der file1-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link LabeledMediaFileResource }{@code >}
     *     
     */
    public JAXBElement<LabeledMediaFileResource> getFile1() {
        return file1;
    }

    /**
     * Legt den Wert der file1-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link LabeledMediaFileResource }{@code >}
     *     
     */
    public void setFile1(JAXBElement<LabeledMediaFileResource> value) {
        this.file1 = value;
    }

    /**
     * Ruft den Wert der file2-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link LabeledMediaFileResource }{@code >}
     *     
     */
    public JAXBElement<LabeledMediaFileResource> getFile2() {
        return file2;
    }

    /**
     * Legt den Wert der file2-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link LabeledMediaFileResource }{@code >}
     *     
     */
    public void setFile2(JAXBElement<LabeledMediaFileResource> value) {
        this.file2 = value;
    }

    /**
     * Ruft den Wert der file3-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link LabeledMediaFileResource }{@code >}
     *     
     */
    public JAXBElement<LabeledMediaFileResource> getFile3() {
        return file3;
    }

    /**
     * Legt den Wert der file3-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link LabeledMediaFileResource }{@code >}
     *     
     */
    public void setFile3(JAXBElement<LabeledMediaFileResource> value) {
        this.file3 = value;
    }

    /**
     * Ruft den Wert der file4-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link LabeledMediaFileResource }{@code >}
     *     
     */
    public JAXBElement<LabeledMediaFileResource> getFile4() {
        return file4;
    }

    /**
     * Legt den Wert der file4-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link LabeledMediaFileResource }{@code >}
     *     
     */
    public void setFile4(JAXBElement<LabeledMediaFileResource> value) {
        this.file4 = value;
    }

}
