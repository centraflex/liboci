//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemNumberActivationGetRequest18sp1.
 *         Contains the system number activation setting.
 *       
 * 
 * <p>Java-Klasse für SystemNumberActivationGetResponse18sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNumberActivationGetResponse18sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="numberActivationMode" type="{}NumberActivationMode"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNumberActivationGetResponse18sp1", propOrder = {
    "numberActivationMode"
})
public class SystemNumberActivationGetResponse18Sp1
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected NumberActivationMode numberActivationMode;

    /**
     * Ruft den Wert der numberActivationMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NumberActivationMode }
     *     
     */
    public NumberActivationMode getNumberActivationMode() {
        return numberActivationMode;
    }

    /**
     * Legt den Wert der numberActivationMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberActivationMode }
     *     
     */
    public void setNumberActivationMode(NumberActivationMode value) {
        this.numberActivationMode = value;
    }

}
