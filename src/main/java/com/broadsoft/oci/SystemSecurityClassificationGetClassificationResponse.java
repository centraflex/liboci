//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the SystemSecurityClassificationGetClassificationRequest.
 *       
 * 
 * <p>Java-Klasse für SystemSecurityClassificationGetClassificationResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSecurityClassificationGetClassificationResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="audioAnnouncementFileDescription" type="{}FileDescription"/>
 *         <element name="audioAnnouncementFileType" type="{}MediaFileType"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSecurityClassificationGetClassificationResponse", propOrder = {
    "audioAnnouncementFileDescription",
    "audioAnnouncementFileType"
})
public class SystemSecurityClassificationGetClassificationResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioAnnouncementFileDescription;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioAnnouncementFileType;

    /**
     * Ruft den Wert der audioAnnouncementFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioAnnouncementFileDescription() {
        return audioAnnouncementFileDescription;
    }

    /**
     * Legt den Wert der audioAnnouncementFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioAnnouncementFileDescription(String value) {
        this.audioAnnouncementFileDescription = value;
    }

    /**
     * Ruft den Wert der audioAnnouncementFileType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioAnnouncementFileType() {
        return audioAnnouncementFileType;
    }

    /**
     * Legt den Wert der audioAnnouncementFileType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioAnnouncementFileType(String value) {
        this.audioAnnouncementFileType = value;
    }

}
