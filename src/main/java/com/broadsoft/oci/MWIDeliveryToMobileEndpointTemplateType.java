//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für MWIDeliveryToMobileEndpointTemplateType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="MWIDeliveryToMobileEndpointTemplateType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Default"/>
 *     <enumeration value="Single New No Previous Message"/>
 *     <enumeration value="Single New With Previous Message"/>
 *     <enumeration value="Multiple New Messages"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "MWIDeliveryToMobileEndpointTemplateType")
@XmlEnum
public enum MWIDeliveryToMobileEndpointTemplateType {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("Single New No Previous Message")
    SINGLE_NEW_NO_PREVIOUS_MESSAGE("Single New No Previous Message"),
    @XmlEnumValue("Single New With Previous Message")
    SINGLE_NEW_WITH_PREVIOUS_MESSAGE("Single New With Previous Message"),
    @XmlEnumValue("Multiple New Messages")
    MULTIPLE_NEW_MESSAGES("Multiple New Messages");
    private final String value;

    MWIDeliveryToMobileEndpointTemplateType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MWIDeliveryToMobileEndpointTemplateType fromValue(String v) {
        for (MWIDeliveryToMobileEndpointTemplateType c: MWIDeliveryToMobileEndpointTemplateType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
