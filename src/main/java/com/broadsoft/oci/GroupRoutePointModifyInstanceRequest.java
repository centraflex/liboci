//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a Route Point instance.
 *         The response is either SuccessResponse or ErrorResponse.
 *         Replaced by GroupRoutePointModifyInstanceRequest22.
 *       
 * 
 * <p>Java-Klasse für GroupRoutePointModifyInstanceRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupRoutePointModifyInstanceRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceModifyProfile" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="externalPreferredAudioCodec" type="{}AudioFileCodec" minOccurs="0"/>
 *         <element name="internalPreferredAudioCodec" type="{}AudioFileCodec" minOccurs="0"/>
 *         <element name="queueLength" type="{}CallCenterQueueLength16" minOccurs="0"/>
 *         <element name="noAnswerTimeoutRings" type="{}NoAnswerTimeoutRings" minOccurs="0"/>
 *         <element name="enableVideo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="playRingingWhenOfferingCall" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="overrideAgentWrapUpTime" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="wrapUpSeconds" type="{}CallCenterWrapUpSeconds" minOccurs="0"/>
 *         <element name="enableAutomaticStateChangeForAgents" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="agentStateAfterCall" type="{}AgentACDAutomaticState" minOccurs="0"/>
 *         <element name="agentUnavailableCode" type="{}CallCenterAgentUnavailableCode" minOccurs="0"/>
 *         <element name="forceDeliveryOfCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forceDeliveryWaitTimeSeconds" type="{}CallCenterForceDeliveryWaitTimeSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupRoutePointModifyInstanceRequest", propOrder = {
    "serviceUserId",
    "serviceInstanceProfile",
    "networkClassOfService",
    "externalPreferredAudioCodec",
    "internalPreferredAudioCodec",
    "queueLength",
    "noAnswerTimeoutRings",
    "enableVideo",
    "playRingingWhenOfferingCall",
    "overrideAgentWrapUpTime",
    "wrapUpSeconds",
    "enableAutomaticStateChangeForAgents",
    "agentStateAfterCall",
    "agentUnavailableCode",
    "forceDeliveryOfCalls",
    "forceDeliveryWaitTimeSeconds"
})
public class GroupRoutePointModifyInstanceRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected ServiceInstanceModifyProfile serviceInstanceProfile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    @XmlSchemaType(name = "token")
    protected AudioFileCodec externalPreferredAudioCodec;
    @XmlSchemaType(name = "token")
    protected AudioFileCodec internalPreferredAudioCodec;
    protected Integer queueLength;
    protected Integer noAnswerTimeoutRings;
    protected Boolean enableVideo;
    protected Boolean playRingingWhenOfferingCall;
    protected Boolean overrideAgentWrapUpTime;
    @XmlElementRef(name = "wrapUpSeconds", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> wrapUpSeconds;
    protected Boolean enableAutomaticStateChangeForAgents;
    @XmlSchemaType(name = "token")
    protected AgentACDAutomaticState agentStateAfterCall;
    @XmlElementRef(name = "agentUnavailableCode", type = JAXBElement.class, required = false)
    protected JAXBElement<String> agentUnavailableCode;
    protected Boolean forceDeliveryOfCalls;
    @XmlElementRef(name = "forceDeliveryWaitTimeSeconds", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> forceDeliveryWaitTimeSeconds;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public ServiceInstanceModifyProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceModifyProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Ruft den Wert der externalPreferredAudioCodec-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AudioFileCodec }
     *     
     */
    public AudioFileCodec getExternalPreferredAudioCodec() {
        return externalPreferredAudioCodec;
    }

    /**
     * Legt den Wert der externalPreferredAudioCodec-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AudioFileCodec }
     *     
     */
    public void setExternalPreferredAudioCodec(AudioFileCodec value) {
        this.externalPreferredAudioCodec = value;
    }

    /**
     * Ruft den Wert der internalPreferredAudioCodec-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AudioFileCodec }
     *     
     */
    public AudioFileCodec getInternalPreferredAudioCodec() {
        return internalPreferredAudioCodec;
    }

    /**
     * Legt den Wert der internalPreferredAudioCodec-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AudioFileCodec }
     *     
     */
    public void setInternalPreferredAudioCodec(AudioFileCodec value) {
        this.internalPreferredAudioCodec = value;
    }

    /**
     * Ruft den Wert der queueLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQueueLength() {
        return queueLength;
    }

    /**
     * Legt den Wert der queueLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQueueLength(Integer value) {
        this.queueLength = value;
    }

    /**
     * Ruft den Wert der noAnswerTimeoutRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNoAnswerTimeoutRings() {
        return noAnswerTimeoutRings;
    }

    /**
     * Legt den Wert der noAnswerTimeoutRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNoAnswerTimeoutRings(Integer value) {
        this.noAnswerTimeoutRings = value;
    }

    /**
     * Ruft den Wert der enableVideo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableVideo() {
        return enableVideo;
    }

    /**
     * Legt den Wert der enableVideo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableVideo(Boolean value) {
        this.enableVideo = value;
    }

    /**
     * Ruft den Wert der playRingingWhenOfferingCall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayRingingWhenOfferingCall() {
        return playRingingWhenOfferingCall;
    }

    /**
     * Legt den Wert der playRingingWhenOfferingCall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayRingingWhenOfferingCall(Boolean value) {
        this.playRingingWhenOfferingCall = value;
    }

    /**
     * Ruft den Wert der overrideAgentWrapUpTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOverrideAgentWrapUpTime() {
        return overrideAgentWrapUpTime;
    }

    /**
     * Legt den Wert der overrideAgentWrapUpTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverrideAgentWrapUpTime(Boolean value) {
        this.overrideAgentWrapUpTime = value;
    }

    /**
     * Ruft den Wert der wrapUpSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getWrapUpSeconds() {
        return wrapUpSeconds;
    }

    /**
     * Legt den Wert der wrapUpSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setWrapUpSeconds(JAXBElement<Integer> value) {
        this.wrapUpSeconds = value;
    }

    /**
     * Ruft den Wert der enableAutomaticStateChangeForAgents-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableAutomaticStateChangeForAgents() {
        return enableAutomaticStateChangeForAgents;
    }

    /**
     * Legt den Wert der enableAutomaticStateChangeForAgents-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableAutomaticStateChangeForAgents(Boolean value) {
        this.enableAutomaticStateChangeForAgents = value;
    }

    /**
     * Ruft den Wert der agentStateAfterCall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AgentACDAutomaticState }
     *     
     */
    public AgentACDAutomaticState getAgentStateAfterCall() {
        return agentStateAfterCall;
    }

    /**
     * Legt den Wert der agentStateAfterCall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentACDAutomaticState }
     *     
     */
    public void setAgentStateAfterCall(AgentACDAutomaticState value) {
        this.agentStateAfterCall = value;
    }

    /**
     * Ruft den Wert der agentUnavailableCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAgentUnavailableCode() {
        return agentUnavailableCode;
    }

    /**
     * Legt den Wert der agentUnavailableCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAgentUnavailableCode(JAXBElement<String> value) {
        this.agentUnavailableCode = value;
    }

    /**
     * Ruft den Wert der forceDeliveryOfCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceDeliveryOfCalls() {
        return forceDeliveryOfCalls;
    }

    /**
     * Legt den Wert der forceDeliveryOfCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceDeliveryOfCalls(Boolean value) {
        this.forceDeliveryOfCalls = value;
    }

    /**
     * Ruft den Wert der forceDeliveryWaitTimeSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getForceDeliveryWaitTimeSeconds() {
        return forceDeliveryWaitTimeSeconds;
    }

    /**
     * Legt den Wert der forceDeliveryWaitTimeSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setForceDeliveryWaitTimeSeconds(JAXBElement<Integer> value) {
        this.forceDeliveryWaitTimeSeconds = value;
    }

}
