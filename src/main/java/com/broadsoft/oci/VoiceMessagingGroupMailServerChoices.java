//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für VoiceMessagingGroupMailServerChoices.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="VoiceMessagingGroupMailServerChoices">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="System Mail Server"/>
 *     <enumeration value="Group Mail Server"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "VoiceMessagingGroupMailServerChoices")
@XmlEnum
public enum VoiceMessagingGroupMailServerChoices {

    @XmlEnumValue("System Mail Server")
    SYSTEM_MAIL_SERVER("System Mail Server"),
    @XmlEnumValue("Group Mail Server")
    GROUP_MAIL_SERVER("Group Mail Server");
    private final String value;

    VoiceMessagingGroupMailServerChoices(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VoiceMessagingGroupMailServerChoices fromValue(String v) {
        for (VoiceMessagingGroupMailServerChoices c: VoiceMessagingGroupMailServerChoices.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
