//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         A list of call center reporting abandoned call threshold seconds that replaces a previously configured list.
 *       
 * 
 * <p>Java-Klasse für CallCenterReportAbandonedCallThresholdReplacementList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterReportAbandonedCallThresholdReplacementList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="abandonedCallThresholdSeconds" type="{}CallCenterReportThresholdSeconds" maxOccurs="4"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterReportAbandonedCallThresholdReplacementList", propOrder = {
    "abandonedCallThresholdSeconds"
})
public class CallCenterReportAbandonedCallThresholdReplacementList {

    @XmlElement(type = Integer.class)
    protected List<Integer> abandonedCallThresholdSeconds;

    /**
     * Gets the value of the abandonedCallThresholdSeconds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the abandonedCallThresholdSeconds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAbandonedCallThresholdSeconds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     * @return
     *     The value of the abandonedCallThresholdSeconds property.
     */
    public List<Integer> getAbandonedCallThresholdSeconds() {
        if (abandonedCallThresholdSeconds == null) {
            abandonedCallThresholdSeconds = new ArrayList<>();
        }
        return this.abandonedCallThresholdSeconds;
    }

}
