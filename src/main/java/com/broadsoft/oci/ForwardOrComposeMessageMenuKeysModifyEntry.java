//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal forward or compose message menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für ForwardOrComposeMessageMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ForwardOrComposeMessageMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="sendToPerson" type="{}DigitAny" minOccurs="0"/>
 *         <element name="sendToAllGroupMembers" type="{}DigitAny" minOccurs="0"/>
 *         <element name="sendToDistributionList" type="{}DigitAny" minOccurs="0"/>
 *         <element name="changeCurrentIntroductionOrMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="listenToCurrentIntroductionOrMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setOrClearUrgentIndicator" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setOrClearConfidentialIndicator" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ForwardOrComposeMessageMenuKeysModifyEntry", propOrder = {
    "sendToPerson",
    "sendToAllGroupMembers",
    "sendToDistributionList",
    "changeCurrentIntroductionOrMessage",
    "listenToCurrentIntroductionOrMessage",
    "setOrClearUrgentIndicator",
    "setOrClearConfidentialIndicator",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class ForwardOrComposeMessageMenuKeysModifyEntry {

    @XmlElementRef(name = "sendToPerson", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sendToPerson;
    @XmlElementRef(name = "sendToAllGroupMembers", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sendToAllGroupMembers;
    @XmlElementRef(name = "sendToDistributionList", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sendToDistributionList;
    @XmlElementRef(name = "changeCurrentIntroductionOrMessage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changeCurrentIntroductionOrMessage;
    @XmlElementRef(name = "listenToCurrentIntroductionOrMessage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> listenToCurrentIntroductionOrMessage;
    @XmlElementRef(name = "setOrClearUrgentIndicator", type = JAXBElement.class, required = false)
    protected JAXBElement<String> setOrClearUrgentIndicator;
    @XmlElementRef(name = "setOrClearConfidentialIndicator", type = JAXBElement.class, required = false)
    protected JAXBElement<String> setOrClearConfidentialIndicator;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repeatMenu;

    /**
     * Ruft den Wert der sendToPerson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSendToPerson() {
        return sendToPerson;
    }

    /**
     * Legt den Wert der sendToPerson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSendToPerson(JAXBElement<String> value) {
        this.sendToPerson = value;
    }

    /**
     * Ruft den Wert der sendToAllGroupMembers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSendToAllGroupMembers() {
        return sendToAllGroupMembers;
    }

    /**
     * Legt den Wert der sendToAllGroupMembers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSendToAllGroupMembers(JAXBElement<String> value) {
        this.sendToAllGroupMembers = value;
    }

    /**
     * Ruft den Wert der sendToDistributionList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSendToDistributionList() {
        return sendToDistributionList;
    }

    /**
     * Legt den Wert der sendToDistributionList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSendToDistributionList(JAXBElement<String> value) {
        this.sendToDistributionList = value;
    }

    /**
     * Ruft den Wert der changeCurrentIntroductionOrMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangeCurrentIntroductionOrMessage() {
        return changeCurrentIntroductionOrMessage;
    }

    /**
     * Legt den Wert der changeCurrentIntroductionOrMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangeCurrentIntroductionOrMessage(JAXBElement<String> value) {
        this.changeCurrentIntroductionOrMessage = value;
    }

    /**
     * Ruft den Wert der listenToCurrentIntroductionOrMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getListenToCurrentIntroductionOrMessage() {
        return listenToCurrentIntroductionOrMessage;
    }

    /**
     * Legt den Wert der listenToCurrentIntroductionOrMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setListenToCurrentIntroductionOrMessage(JAXBElement<String> value) {
        this.listenToCurrentIntroductionOrMessage = value;
    }

    /**
     * Ruft den Wert der setOrClearUrgentIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSetOrClearUrgentIndicator() {
        return setOrClearUrgentIndicator;
    }

    /**
     * Legt den Wert der setOrClearUrgentIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSetOrClearUrgentIndicator(JAXBElement<String> value) {
        this.setOrClearUrgentIndicator = value;
    }

    /**
     * Ruft den Wert der setOrClearConfidentialIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSetOrClearConfidentialIndicator() {
        return setOrClearConfidentialIndicator;
    }

    /**
     * Legt den Wert der setOrClearConfidentialIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSetOrClearConfidentialIndicator(JAXBElement<String> value) {
        this.setOrClearConfidentialIndicator = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepeatMenu(JAXBElement<String> value) {
        this.repeatMenu = value;
    }

}
