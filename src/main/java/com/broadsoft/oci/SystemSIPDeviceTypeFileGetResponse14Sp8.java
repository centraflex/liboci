//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *           Response to SystemSIPDeviceTypeFileGetRequest14sp8.
 *         
 * 
 * <p>Java-Klasse für SystemSIPDeviceTypeFileGetResponse14sp8 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPDeviceTypeFileGetResponse14sp8">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="remoteFileFormat" type="{}DeviceManagementFileFormat"/>
 *         <element name="fileCategory" type="{}DeviceManagementFileCategory"/>
 *         <element name="allowFileCustomization" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="fileSource" type="{}DeviceTypeFileEnhancedConfigurationMode"/>
 *         <element name="configurationFileName" type="{}AccessDeviceEnhancedConfigurationFileName" minOccurs="0"/>
 *         <element name="useHttpDigestAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macBasedFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="userNamePasswordFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macInNonRequestURI" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macFormatInNonRequestURI" type="{}DeviceManagementAccessURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPDeviceTypeFileGetResponse14sp8", propOrder = {
    "remoteFileFormat",
    "fileCategory",
    "allowFileCustomization",
    "fileSource",
    "configurationFileName",
    "useHttpDigestAuthentication",
    "macBasedFileAuthentication",
    "userNamePasswordFileAuthentication",
    "macInNonRequestURI",
    "macFormatInNonRequestURI"
})
public class SystemSIPDeviceTypeFileGetResponse14Sp8
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String remoteFileFormat;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceManagementFileCategory fileCategory;
    protected boolean allowFileCustomization;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceTypeFileEnhancedConfigurationMode fileSource;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String configurationFileName;
    protected boolean useHttpDigestAuthentication;
    protected boolean macBasedFileAuthentication;
    protected boolean userNamePasswordFileAuthentication;
    protected boolean macInNonRequestURI;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String macFormatInNonRequestURI;

    /**
     * Ruft den Wert der remoteFileFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteFileFormat() {
        return remoteFileFormat;
    }

    /**
     * Legt den Wert der remoteFileFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteFileFormat(String value) {
        this.remoteFileFormat = value;
    }

    /**
     * Ruft den Wert der fileCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementFileCategory }
     *     
     */
    public DeviceManagementFileCategory getFileCategory() {
        return fileCategory;
    }

    /**
     * Legt den Wert der fileCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementFileCategory }
     *     
     */
    public void setFileCategory(DeviceManagementFileCategory value) {
        this.fileCategory = value;
    }

    /**
     * Ruft den Wert der allowFileCustomization-Eigenschaft ab.
     * 
     */
    public boolean isAllowFileCustomization() {
        return allowFileCustomization;
    }

    /**
     * Legt den Wert der allowFileCustomization-Eigenschaft fest.
     * 
     */
    public void setAllowFileCustomization(boolean value) {
        this.allowFileCustomization = value;
    }

    /**
     * Ruft den Wert der fileSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceTypeFileEnhancedConfigurationMode }
     *     
     */
    public DeviceTypeFileEnhancedConfigurationMode getFileSource() {
        return fileSource;
    }

    /**
     * Legt den Wert der fileSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceTypeFileEnhancedConfigurationMode }
     *     
     */
    public void setFileSource(DeviceTypeFileEnhancedConfigurationMode value) {
        this.fileSource = value;
    }

    /**
     * Ruft den Wert der configurationFileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfigurationFileName() {
        return configurationFileName;
    }

    /**
     * Legt den Wert der configurationFileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfigurationFileName(String value) {
        this.configurationFileName = value;
    }

    /**
     * Ruft den Wert der useHttpDigestAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isUseHttpDigestAuthentication() {
        return useHttpDigestAuthentication;
    }

    /**
     * Legt den Wert der useHttpDigestAuthentication-Eigenschaft fest.
     * 
     */
    public void setUseHttpDigestAuthentication(boolean value) {
        this.useHttpDigestAuthentication = value;
    }

    /**
     * Ruft den Wert der macBasedFileAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isMacBasedFileAuthentication() {
        return macBasedFileAuthentication;
    }

    /**
     * Legt den Wert der macBasedFileAuthentication-Eigenschaft fest.
     * 
     */
    public void setMacBasedFileAuthentication(boolean value) {
        this.macBasedFileAuthentication = value;
    }

    /**
     * Ruft den Wert der userNamePasswordFileAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isUserNamePasswordFileAuthentication() {
        return userNamePasswordFileAuthentication;
    }

    /**
     * Legt den Wert der userNamePasswordFileAuthentication-Eigenschaft fest.
     * 
     */
    public void setUserNamePasswordFileAuthentication(boolean value) {
        this.userNamePasswordFileAuthentication = value;
    }

    /**
     * Ruft den Wert der macInNonRequestURI-Eigenschaft ab.
     * 
     */
    public boolean isMacInNonRequestURI() {
        return macInNonRequestURI;
    }

    /**
     * Legt den Wert der macInNonRequestURI-Eigenschaft fest.
     * 
     */
    public void setMacInNonRequestURI(boolean value) {
        this.macInNonRequestURI = value;
    }

    /**
     * Ruft den Wert der macFormatInNonRequestURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacFormatInNonRequestURI() {
        return macFormatInNonRequestURI;
    }

    /**
     * Legt den Wert der macFormatInNonRequestURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacFormatInNonRequestURI(String value) {
        this.macFormatInNonRequestURI = value;
    }

}
