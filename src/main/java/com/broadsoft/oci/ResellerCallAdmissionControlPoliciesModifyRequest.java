//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the reseller call admission control policies.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ResellerCallAdmissionControlPoliciesModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerCallAdmissionControlPoliciesModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="resellerId" type="{}ResellerId22"/>
 *         <element name="enableCallAdmissionControl" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxConcurrentNetworkSessions" type="{}ConcurrentNetworkSessionsLimit" minOccurs="0"/>
 *         <element name="maxConcurrentNetworkSessionsThreshold" type="{}ConcurrentNetworkSessionsThresholdLimit" minOccurs="0"/>
 *         <element name="maxNetworkCallsPerSecond" type="{}NetworkCallsPerSecondLimit" minOccurs="0"/>
 *         <element name="maxNetworkCallsPerSecondThreshold" type="{}NetworkCallsPerSecondThresholdLimit" minOccurs="0"/>
 *         <element name="maxConcurrentExternalSIPRECSessions" type="{}ConcurrentExternalSIPRECSessionsLimit" minOccurs="0"/>
 *         <element name="maxConcurrentExternalSIPRECSessionsThreshold" type="{}ConcurrentExternalSIPRECSessionsThresholdLimit" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerCallAdmissionControlPoliciesModifyRequest", propOrder = {
    "resellerId",
    "enableCallAdmissionControl",
    "maxConcurrentNetworkSessions",
    "maxConcurrentNetworkSessionsThreshold",
    "maxNetworkCallsPerSecond",
    "maxNetworkCallsPerSecondThreshold",
    "maxConcurrentExternalSIPRECSessions",
    "maxConcurrentExternalSIPRECSessionsThreshold"
})
public class ResellerCallAdmissionControlPoliciesModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    protected Boolean enableCallAdmissionControl;
    protected Integer maxConcurrentNetworkSessions;
    @XmlElementRef(name = "maxConcurrentNetworkSessionsThreshold", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxConcurrentNetworkSessionsThreshold;
    protected Integer maxNetworkCallsPerSecond;
    @XmlElementRef(name = "maxNetworkCallsPerSecondThreshold", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxNetworkCallsPerSecondThreshold;
    protected Integer maxConcurrentExternalSIPRECSessions;
    @XmlElementRef(name = "maxConcurrentExternalSIPRECSessionsThreshold", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxConcurrentExternalSIPRECSessionsThreshold;

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Ruft den Wert der enableCallAdmissionControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCallAdmissionControl() {
        return enableCallAdmissionControl;
    }

    /**
     * Legt den Wert der enableCallAdmissionControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCallAdmissionControl(Boolean value) {
        this.enableCallAdmissionControl = value;
    }

    /**
     * Ruft den Wert der maxConcurrentNetworkSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxConcurrentNetworkSessions() {
        return maxConcurrentNetworkSessions;
    }

    /**
     * Legt den Wert der maxConcurrentNetworkSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxConcurrentNetworkSessions(Integer value) {
        this.maxConcurrentNetworkSessions = value;
    }

    /**
     * Ruft den Wert der maxConcurrentNetworkSessionsThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxConcurrentNetworkSessionsThreshold() {
        return maxConcurrentNetworkSessionsThreshold;
    }

    /**
     * Legt den Wert der maxConcurrentNetworkSessionsThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxConcurrentNetworkSessionsThreshold(JAXBElement<Integer> value) {
        this.maxConcurrentNetworkSessionsThreshold = value;
    }

    /**
     * Ruft den Wert der maxNetworkCallsPerSecond-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxNetworkCallsPerSecond() {
        return maxNetworkCallsPerSecond;
    }

    /**
     * Legt den Wert der maxNetworkCallsPerSecond-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxNetworkCallsPerSecond(Integer value) {
        this.maxNetworkCallsPerSecond = value;
    }

    /**
     * Ruft den Wert der maxNetworkCallsPerSecondThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxNetworkCallsPerSecondThreshold() {
        return maxNetworkCallsPerSecondThreshold;
    }

    /**
     * Legt den Wert der maxNetworkCallsPerSecondThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxNetworkCallsPerSecondThreshold(JAXBElement<Integer> value) {
        this.maxNetworkCallsPerSecondThreshold = value;
    }

    /**
     * Ruft den Wert der maxConcurrentExternalSIPRECSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxConcurrentExternalSIPRECSessions() {
        return maxConcurrentExternalSIPRECSessions;
    }

    /**
     * Legt den Wert der maxConcurrentExternalSIPRECSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxConcurrentExternalSIPRECSessions(Integer value) {
        this.maxConcurrentExternalSIPRECSessions = value;
    }

    /**
     * Ruft den Wert der maxConcurrentExternalSIPRECSessionsThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxConcurrentExternalSIPRECSessionsThreshold() {
        return maxConcurrentExternalSIPRECSessionsThreshold;
    }

    /**
     * Legt den Wert der maxConcurrentExternalSIPRECSessionsThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxConcurrentExternalSIPRECSessionsThreshold(JAXBElement<Integer> value) {
        this.maxConcurrentExternalSIPRECSessionsThreshold = value;
    }

}
