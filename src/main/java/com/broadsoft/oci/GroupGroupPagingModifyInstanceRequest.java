//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a Group Paging instance.
 *         The response is either SuccessResponse or ErrorResponse.
 *         
 *         The following element is only used in AS data mode and ignored in XS data mode:
 *            networkClassOfService
 *       
 * 
 * <p>Java-Klasse für GroupGroupPagingModifyInstanceRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupGroupPagingModifyInstanceRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceModifyProfile" minOccurs="0"/>
 *         <element name="confirmationToneTimeoutSeconds" type="{}GroupPagingConfirmationToneTimeoutSeconds" minOccurs="0"/>
 *         <element name="deliverOriginatorCLIDInstead" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="originatorCLIDPrefix" type="{}GroupPagingOriginatorCLIDPrefix" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupGroupPagingModifyInstanceRequest", propOrder = {
    "serviceUserId",
    "serviceInstanceProfile",
    "confirmationToneTimeoutSeconds",
    "deliverOriginatorCLIDInstead",
    "originatorCLIDPrefix",
    "networkClassOfService"
})
public class GroupGroupPagingModifyInstanceRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected ServiceInstanceModifyProfile serviceInstanceProfile;
    protected Integer confirmationToneTimeoutSeconds;
    protected Boolean deliverOriginatorCLIDInstead;
    @XmlElementRef(name = "originatorCLIDPrefix", type = JAXBElement.class, required = false)
    protected JAXBElement<String> originatorCLIDPrefix;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public ServiceInstanceModifyProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceModifyProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der confirmationToneTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getConfirmationToneTimeoutSeconds() {
        return confirmationToneTimeoutSeconds;
    }

    /**
     * Legt den Wert der confirmationToneTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setConfirmationToneTimeoutSeconds(Integer value) {
        this.confirmationToneTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der deliverOriginatorCLIDInstead-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeliverOriginatorCLIDInstead() {
        return deliverOriginatorCLIDInstead;
    }

    /**
     * Legt den Wert der deliverOriginatorCLIDInstead-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeliverOriginatorCLIDInstead(Boolean value) {
        this.deliverOriginatorCLIDInstead = value;
    }

    /**
     * Ruft den Wert der originatorCLIDPrefix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOriginatorCLIDPrefix() {
        return originatorCLIDPrefix;
    }

    /**
     * Legt den Wert der originatorCLIDPrefix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOriginatorCLIDPrefix(JAXBElement<String> value) {
        this.originatorCLIDPrefix = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

}
