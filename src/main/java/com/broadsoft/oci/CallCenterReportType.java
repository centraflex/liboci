//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterReportType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterReportType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Agent"/>
 *     <enumeration value="Call Center Dnis"/>
 *     <enumeration value="Call Center"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterReportType")
@XmlEnum
public enum CallCenterReportType {

    @XmlEnumValue("Agent")
    AGENT("Agent"),
    @XmlEnumValue("Call Center Dnis")
    CALL_CENTER_DNIS("Call Center Dnis"),
    @XmlEnumValue("Call Center")
    CALL_CENTER("Call Center");
    private final String value;

    CallCenterReportType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterReportType fromValue(String v) {
        for (CallCenterReportType c: CallCenterReportType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
