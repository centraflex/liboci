//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Requests the list of access devices in a group.
 * 
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           groupExternalId
 * 
 *         The response is either GroupAccessDeviceGetListResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupAccessDeviceGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAccessDeviceGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <choice>
 *           <sequence>
 *             <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *             <element name="groupId" type="{}GroupId"/>
 *           </sequence>
 *           <element name="groupExternalId" type="{}ExternalId"/>
 *         </choice>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaDeviceName" type="{}SearchCriteriaDeviceName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDeviceMACAddress" type="{}SearchCriteriaDeviceMACAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDeviceNetAddress" type="{}SearchCriteriaDeviceNetAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactDeviceType" type="{}SearchCriteriaExactDeviceType" minOccurs="0"/>
 *         <element name="searchCriteriaAccessDeviceVersion" type="{}SearchCriteriaAccessDeviceVersion" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAccessDeviceGetListRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "groupExternalId",
    "responseSizeLimit",
    "searchCriteriaDeviceName",
    "searchCriteriaDeviceMACAddress",
    "searchCriteriaDeviceNetAddress",
    "searchCriteriaExactDeviceType",
    "searchCriteriaAccessDeviceVersion"
})
public class GroupAccessDeviceGetListRequest
    extends OCIRequest
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupExternalId;
    protected Integer responseSizeLimit;
    protected List<SearchCriteriaDeviceName> searchCriteriaDeviceName;
    protected List<SearchCriteriaDeviceMACAddress> searchCriteriaDeviceMACAddress;
    protected List<SearchCriteriaDeviceNetAddress> searchCriteriaDeviceNetAddress;
    protected SearchCriteriaExactDeviceType searchCriteriaExactDeviceType;
    protected List<SearchCriteriaAccessDeviceVersion> searchCriteriaAccessDeviceVersion;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der groupExternalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupExternalId() {
        return groupExternalId;
    }

    /**
     * Legt den Wert der groupExternalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupExternalId(String value) {
        this.groupExternalId = value;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaDeviceName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDeviceName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDeviceName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDeviceName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDeviceName property.
     */
    public List<SearchCriteriaDeviceName> getSearchCriteriaDeviceName() {
        if (searchCriteriaDeviceName == null) {
            searchCriteriaDeviceName = new ArrayList<>();
        }
        return this.searchCriteriaDeviceName;
    }

    /**
     * Gets the value of the searchCriteriaDeviceMACAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDeviceMACAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDeviceMACAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDeviceMACAddress }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDeviceMACAddress property.
     */
    public List<SearchCriteriaDeviceMACAddress> getSearchCriteriaDeviceMACAddress() {
        if (searchCriteriaDeviceMACAddress == null) {
            searchCriteriaDeviceMACAddress = new ArrayList<>();
        }
        return this.searchCriteriaDeviceMACAddress;
    }

    /**
     * Gets the value of the searchCriteriaDeviceNetAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDeviceNetAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDeviceNetAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDeviceNetAddress }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDeviceNetAddress property.
     */
    public List<SearchCriteriaDeviceNetAddress> getSearchCriteriaDeviceNetAddress() {
        if (searchCriteriaDeviceNetAddress == null) {
            searchCriteriaDeviceNetAddress = new ArrayList<>();
        }
        return this.searchCriteriaDeviceNetAddress;
    }

    /**
     * Ruft den Wert der searchCriteriaExactDeviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactDeviceType }
     *     
     */
    public SearchCriteriaExactDeviceType getSearchCriteriaExactDeviceType() {
        return searchCriteriaExactDeviceType;
    }

    /**
     * Legt den Wert der searchCriteriaExactDeviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactDeviceType }
     *     
     */
    public void setSearchCriteriaExactDeviceType(SearchCriteriaExactDeviceType value) {
        this.searchCriteriaExactDeviceType = value;
    }

    /**
     * Gets the value of the searchCriteriaAccessDeviceVersion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaAccessDeviceVersion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaAccessDeviceVersion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaAccessDeviceVersion }
     * 
     * 
     * @return
     *     The value of the searchCriteriaAccessDeviceVersion property.
     */
    public List<SearchCriteriaAccessDeviceVersion> getSearchCriteriaAccessDeviceVersion() {
        if (searchCriteriaAccessDeviceVersion == null) {
            searchCriteriaAccessDeviceVersion = new ArrayList<>();
        }
        return this.searchCriteriaAccessDeviceVersion;
    }

}
