//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Extended Call Log entry describing a placed, received, or missed call.
 *         "countryCode" is the user's country code
 *         The following time elements are represented as timestamp, i.e., the number of milliseconds
 *         since January 1, 1970, 00:00:00 GMT.
 *         "startTime" represents the time when the system sends out a call invitation message (e.g. for
 *         placed calls) or receives a call invitation message (e.g. for missed/received calls). 
 *         "answerTime" represents the time when the call is answered by the terminating party.
 *         "detachedTime" represents the time when the call is successfully redirected by the system.  
 *         "releaseTime" represents the time when the call is released. This time corresponds to the 
 *         moment the call is released by the system, and not necessarily when one party hangs up, since this 
 *         does not always mean the call is released (e.g. Emergency/911 calls).
 *         The elements "userGroupId", "userId","userPrimaryDn" and "userPrimaryExtension"
 *         are only returned when the enterprise or group level requests are used.
 *         The following elements are only used in AS data mode and not returned in XS data mode:
 *           callAuthorizationCode
 *       
 * 
 * <p>Java-Klasse für ExtendedMixedCallLogsEntry17sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ExtendedMixedCallLogsEntry17sp4">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="callLogType" type="{}CallLogsType"/>
 *         <element name="countryCode" type="{}CountryCode"/>
 *         <element name="callLogId" type="{}CallLogId17sp4"/>
 *         <element name="callId" type="{}CallId"/>
 *         <element name="subscriberType" type="{}EnhancedCallLogsSubscriberType"/>
 *         <element name="dialedNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="calledNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="networkTranslatedAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="callingAssertedNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="callingPresentationNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="callingPresentationNumberSource" type="{}CallingPresentationNumberSource" minOccurs="0"/>
 *         <element name="callingPresentationName" type="{}CallLogsEntryName17sp4" minOccurs="0"/>
 *         <element name="callingPresentationIndicator" type="{}PresentationIndicator"/>
 *         <element name="callingGroupId" type="{}GroupId" minOccurs="0"/>
 *         <element name="calledDirectoryName" type="{}CallLogsEntryName" minOccurs="0"/>
 *         <element name="calledGroupId" type="{}GroupId" minOccurs="0"/>
 *         <element name="connectedNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="connectedNumberSource" type="{}ConnectedNumberSource" minOccurs="0"/>
 *         <element name="connectedName" type="{}CallLogsEntryName17sp4" minOccurs="0"/>
 *         <element name="connectedPresentationIndicator" type="{}PresentationIndicator" minOccurs="0"/>
 *         <element name="typeOfNetwork" type="{}NetworkType" minOccurs="0"/>
 *         <element name="callCategory" type="{}CallCategory" minOccurs="0"/>
 *         <element name="basicCallType" type="{}BasicCallType"/>
 *         <element name="configurableCallType" type="{}CommunicationBarringCallType" minOccurs="0"/>
 *         <element name="alternateCallIndicator" type="{}CommunicationBarringAlternateCallIndicator" minOccurs="0"/>
 *         <element name="virtualOnNetCallType" type="{}VirtualOnNetCallTypeName" minOccurs="0"/>
 *         <element name="time" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         <element name="startTime" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         <element name="answerTime" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         <element name="releaseTime" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         <element name="detachedTime" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         <element name="detachedAnswerTime" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         <element name="outgoingDnis" type="{}EnhancedCallLogsOutgoingDnis" minOccurs="0"/>
 *         <element name="serviceInvocationDisposition" type="{}ServiceInvocationDisposition" minOccurs="0"/>
 *         <element name="serviceInvocationDialedNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="serviceInvocationCalledNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="serviceInvocationNetworkTranslatedAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="serviceInvocationTypeOfNetwork" type="{}NetworkType" minOccurs="0"/>
 *         <element name="serviceInvocationCallCategory" type="{}CallCategory" minOccurs="0"/>
 *         <element name="serviceInvocationBasicCallType" type="{}BasicCallType" minOccurs="0"/>
 *         <element name="serviceInvocationConfigurableCallType" type="{}CommunicationBarringCallType" minOccurs="0"/>
 *         <element name="serviceInvocationAlternateCallIndicator" type="{}CommunicationBarringAlternateCallIndicator" minOccurs="0"/>
 *         <element name="serviceInvocationVirtualOnNetCallType" type="{}VirtualOnNetCallTypeName" minOccurs="0"/>
 *         <element name="serviceInvocationCalledDirectoryName" type="{}CallLogsEntryName" minOccurs="0"/>
 *         <element name="serviceInvocationCalledGroupId" type="{}GroupId" minOccurs="0"/>
 *         <element name="redirectingNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="redirectingName" type="{}CallLogsEntryName17sp4" minOccurs="0"/>
 *         <element name="redirectingPresentationIndicator" type="{}RedirectingPresentationIndicator" minOccurs="0"/>
 *         <element name="RedirectingReason" type="{}RedirectingReason" minOccurs="0"/>
 *         <element name="accountAuthorizationCode" type="{}OutgoingCallingPlanAuthorizationCode" minOccurs="0"/>
 *         <element name="callAuthorizationCode" type="{}OutgoingCallingPlanAuthorizationCode" minOccurs="0"/>
 *         <element name="userGroupId" type="{}GroupId" minOccurs="0"/>
 *         <element name="userId" type="{}UserId" minOccurs="0"/>
 *         <element name="userPrimaryDn" type="{}DN" minOccurs="0"/>
 *         <element name="userPrimaryExtension" type="{}Extension17" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtendedMixedCallLogsEntry17sp4", propOrder = {
    "callLogType",
    "countryCode",
    "callLogId",
    "callId",
    "subscriberType",
    "dialedNumber",
    "calledNumber",
    "networkTranslatedAddress",
    "callingAssertedNumber",
    "callingPresentationNumber",
    "callingPresentationNumberSource",
    "callingPresentationName",
    "callingPresentationIndicator",
    "callingGroupId",
    "calledDirectoryName",
    "calledGroupId",
    "connectedNumber",
    "connectedNumberSource",
    "connectedName",
    "connectedPresentationIndicator",
    "typeOfNetwork",
    "callCategory",
    "basicCallType",
    "configurableCallType",
    "alternateCallIndicator",
    "virtualOnNetCallType",
    "time",
    "startTime",
    "answerTime",
    "releaseTime",
    "detachedTime",
    "detachedAnswerTime",
    "outgoingDnis",
    "serviceInvocationDisposition",
    "serviceInvocationDialedNumber",
    "serviceInvocationCalledNumber",
    "serviceInvocationNetworkTranslatedAddress",
    "serviceInvocationTypeOfNetwork",
    "serviceInvocationCallCategory",
    "serviceInvocationBasicCallType",
    "serviceInvocationConfigurableCallType",
    "serviceInvocationAlternateCallIndicator",
    "serviceInvocationVirtualOnNetCallType",
    "serviceInvocationCalledDirectoryName",
    "serviceInvocationCalledGroupId",
    "redirectingNumber",
    "redirectingName",
    "redirectingPresentationIndicator",
    "redirectingReason",
    "accountAuthorizationCode",
    "callAuthorizationCode",
    "userGroupId",
    "userId",
    "userPrimaryDn",
    "userPrimaryExtension"
})
public class ExtendedMixedCallLogsEntry17Sp4 {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallLogsType callLogType;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NMTOKEN")
    protected String countryCode;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callLogId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnhancedCallLogsSubscriberType subscriberType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String dialedNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String calledNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkTranslatedAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingAssertedNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingPresentationNumber;
    @XmlSchemaType(name = "token")
    protected CallingPresentationNumberSource callingPresentationNumberSource;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingPresentationName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected PresentationIndicator callingPresentationIndicator;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingGroupId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String calledDirectoryName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String calledGroupId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String connectedNumber;
    @XmlSchemaType(name = "token")
    protected ConnectedNumberSource connectedNumberSource;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String connectedName;
    @XmlSchemaType(name = "token")
    protected PresentationIndicator connectedPresentationIndicator;
    @XmlSchemaType(name = "token")
    protected NetworkType typeOfNetwork;
    @XmlSchemaType(name = "token")
    protected CallCategory callCategory;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BasicCallType basicCallType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String configurableCallType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String alternateCallIndicator;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String virtualOnNetCallType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar time;
    protected long startTime;
    protected Long answerTime;
    protected Long releaseTime;
    protected Long detachedTime;
    protected Long detachedAnswerTime;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String outgoingDnis;
    @XmlSchemaType(name = "token")
    protected ServiceInvocationDisposition serviceInvocationDisposition;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceInvocationDialedNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceInvocationCalledNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceInvocationNetworkTranslatedAddress;
    @XmlSchemaType(name = "token")
    protected NetworkType serviceInvocationTypeOfNetwork;
    @XmlSchemaType(name = "token")
    protected CallCategory serviceInvocationCallCategory;
    @XmlSchemaType(name = "token")
    protected BasicCallType serviceInvocationBasicCallType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceInvocationConfigurableCallType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceInvocationAlternateCallIndicator;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceInvocationVirtualOnNetCallType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceInvocationCalledDirectoryName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceInvocationCalledGroupId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String redirectingNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String redirectingName;
    @XmlSchemaType(name = "token")
    protected RedirectingPresentationIndicator redirectingPresentationIndicator;
    @XmlElement(name = "RedirectingReason")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String redirectingReason;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accountAuthorizationCode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callAuthorizationCode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userGroupId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userPrimaryDn;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userPrimaryExtension;

    /**
     * Ruft den Wert der callLogType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallLogsType }
     *     
     */
    public CallLogsType getCallLogType() {
        return callLogType;
    }

    /**
     * Legt den Wert der callLogType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallLogsType }
     *     
     */
    public void setCallLogType(CallLogsType value) {
        this.callLogType = value;
    }

    /**
     * Ruft den Wert der countryCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Legt den Wert der countryCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Ruft den Wert der callLogId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallLogId() {
        return callLogId;
    }

    /**
     * Legt den Wert der callLogId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallLogId(String value) {
        this.callLogId = value;
    }

    /**
     * Ruft den Wert der callId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallId() {
        return callId;
    }

    /**
     * Legt den Wert der callId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallId(String value) {
        this.callId = value;
    }

    /**
     * Ruft den Wert der subscriberType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnhancedCallLogsSubscriberType }
     *     
     */
    public EnhancedCallLogsSubscriberType getSubscriberType() {
        return subscriberType;
    }

    /**
     * Legt den Wert der subscriberType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnhancedCallLogsSubscriberType }
     *     
     */
    public void setSubscriberType(EnhancedCallLogsSubscriberType value) {
        this.subscriberType = value;
    }

    /**
     * Ruft den Wert der dialedNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDialedNumber() {
        return dialedNumber;
    }

    /**
     * Legt den Wert der dialedNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDialedNumber(String value) {
        this.dialedNumber = value;
    }

    /**
     * Ruft den Wert der calledNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalledNumber() {
        return calledNumber;
    }

    /**
     * Legt den Wert der calledNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalledNumber(String value) {
        this.calledNumber = value;
    }

    /**
     * Ruft den Wert der networkTranslatedAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkTranslatedAddress() {
        return networkTranslatedAddress;
    }

    /**
     * Legt den Wert der networkTranslatedAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkTranslatedAddress(String value) {
        this.networkTranslatedAddress = value;
    }

    /**
     * Ruft den Wert der callingAssertedNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingAssertedNumber() {
        return callingAssertedNumber;
    }

    /**
     * Legt den Wert der callingAssertedNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingAssertedNumber(String value) {
        this.callingAssertedNumber = value;
    }

    /**
     * Ruft den Wert der callingPresentationNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingPresentationNumber() {
        return callingPresentationNumber;
    }

    /**
     * Legt den Wert der callingPresentationNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingPresentationNumber(String value) {
        this.callingPresentationNumber = value;
    }

    /**
     * Ruft den Wert der callingPresentationNumberSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallingPresentationNumberSource }
     *     
     */
    public CallingPresentationNumberSource getCallingPresentationNumberSource() {
        return callingPresentationNumberSource;
    }

    /**
     * Legt den Wert der callingPresentationNumberSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallingPresentationNumberSource }
     *     
     */
    public void setCallingPresentationNumberSource(CallingPresentationNumberSource value) {
        this.callingPresentationNumberSource = value;
    }

    /**
     * Ruft den Wert der callingPresentationName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingPresentationName() {
        return callingPresentationName;
    }

    /**
     * Legt den Wert der callingPresentationName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingPresentationName(String value) {
        this.callingPresentationName = value;
    }

    /**
     * Ruft den Wert der callingPresentationIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PresentationIndicator }
     *     
     */
    public PresentationIndicator getCallingPresentationIndicator() {
        return callingPresentationIndicator;
    }

    /**
     * Legt den Wert der callingPresentationIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PresentationIndicator }
     *     
     */
    public void setCallingPresentationIndicator(PresentationIndicator value) {
        this.callingPresentationIndicator = value;
    }

    /**
     * Ruft den Wert der callingGroupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingGroupId() {
        return callingGroupId;
    }

    /**
     * Legt den Wert der callingGroupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingGroupId(String value) {
        this.callingGroupId = value;
    }

    /**
     * Ruft den Wert der calledDirectoryName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalledDirectoryName() {
        return calledDirectoryName;
    }

    /**
     * Legt den Wert der calledDirectoryName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalledDirectoryName(String value) {
        this.calledDirectoryName = value;
    }

    /**
     * Ruft den Wert der calledGroupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalledGroupId() {
        return calledGroupId;
    }

    /**
     * Legt den Wert der calledGroupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalledGroupId(String value) {
        this.calledGroupId = value;
    }

    /**
     * Ruft den Wert der connectedNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConnectedNumber() {
        return connectedNumber;
    }

    /**
     * Legt den Wert der connectedNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConnectedNumber(String value) {
        this.connectedNumber = value;
    }

    /**
     * Ruft den Wert der connectedNumberSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ConnectedNumberSource }
     *     
     */
    public ConnectedNumberSource getConnectedNumberSource() {
        return connectedNumberSource;
    }

    /**
     * Legt den Wert der connectedNumberSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConnectedNumberSource }
     *     
     */
    public void setConnectedNumberSource(ConnectedNumberSource value) {
        this.connectedNumberSource = value;
    }

    /**
     * Ruft den Wert der connectedName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConnectedName() {
        return connectedName;
    }

    /**
     * Legt den Wert der connectedName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConnectedName(String value) {
        this.connectedName = value;
    }

    /**
     * Ruft den Wert der connectedPresentationIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PresentationIndicator }
     *     
     */
    public PresentationIndicator getConnectedPresentationIndicator() {
        return connectedPresentationIndicator;
    }

    /**
     * Legt den Wert der connectedPresentationIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PresentationIndicator }
     *     
     */
    public void setConnectedPresentationIndicator(PresentationIndicator value) {
        this.connectedPresentationIndicator = value;
    }

    /**
     * Ruft den Wert der typeOfNetwork-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkType }
     *     
     */
    public NetworkType getTypeOfNetwork() {
        return typeOfNetwork;
    }

    /**
     * Legt den Wert der typeOfNetwork-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkType }
     *     
     */
    public void setTypeOfNetwork(NetworkType value) {
        this.typeOfNetwork = value;
    }

    /**
     * Ruft den Wert der callCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCategory }
     *     
     */
    public CallCategory getCallCategory() {
        return callCategory;
    }

    /**
     * Legt den Wert der callCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCategory }
     *     
     */
    public void setCallCategory(CallCategory value) {
        this.callCategory = value;
    }

    /**
     * Ruft den Wert der basicCallType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BasicCallType }
     *     
     */
    public BasicCallType getBasicCallType() {
        return basicCallType;
    }

    /**
     * Legt den Wert der basicCallType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BasicCallType }
     *     
     */
    public void setBasicCallType(BasicCallType value) {
        this.basicCallType = value;
    }

    /**
     * Ruft den Wert der configurableCallType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfigurableCallType() {
        return configurableCallType;
    }

    /**
     * Legt den Wert der configurableCallType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfigurableCallType(String value) {
        this.configurableCallType = value;
    }

    /**
     * Ruft den Wert der alternateCallIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateCallIndicator() {
        return alternateCallIndicator;
    }

    /**
     * Legt den Wert der alternateCallIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateCallIndicator(String value) {
        this.alternateCallIndicator = value;
    }

    /**
     * Ruft den Wert der virtualOnNetCallType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVirtualOnNetCallType() {
        return virtualOnNetCallType;
    }

    /**
     * Legt den Wert der virtualOnNetCallType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVirtualOnNetCallType(String value) {
        this.virtualOnNetCallType = value;
    }

    /**
     * Ruft den Wert der time-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTime() {
        return time;
    }

    /**
     * Legt den Wert der time-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTime(XMLGregorianCalendar value) {
        this.time = value;
    }

    /**
     * Ruft den Wert der startTime-Eigenschaft ab.
     * 
     */
    public long getStartTime() {
        return startTime;
    }

    /**
     * Legt den Wert der startTime-Eigenschaft fest.
     * 
     */
    public void setStartTime(long value) {
        this.startTime = value;
    }

    /**
     * Ruft den Wert der answerTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAnswerTime() {
        return answerTime;
    }

    /**
     * Legt den Wert der answerTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAnswerTime(Long value) {
        this.answerTime = value;
    }

    /**
     * Ruft den Wert der releaseTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReleaseTime() {
        return releaseTime;
    }

    /**
     * Legt den Wert der releaseTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReleaseTime(Long value) {
        this.releaseTime = value;
    }

    /**
     * Ruft den Wert der detachedTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDetachedTime() {
        return detachedTime;
    }

    /**
     * Legt den Wert der detachedTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDetachedTime(Long value) {
        this.detachedTime = value;
    }

    /**
     * Ruft den Wert der detachedAnswerTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDetachedAnswerTime() {
        return detachedAnswerTime;
    }

    /**
     * Legt den Wert der detachedAnswerTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDetachedAnswerTime(Long value) {
        this.detachedAnswerTime = value;
    }

    /**
     * Ruft den Wert der outgoingDnis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutgoingDnis() {
        return outgoingDnis;
    }

    /**
     * Legt den Wert der outgoingDnis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutgoingDnis(String value) {
        this.outgoingDnis = value;
    }

    /**
     * Ruft den Wert der serviceInvocationDisposition-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInvocationDisposition }
     *     
     */
    public ServiceInvocationDisposition getServiceInvocationDisposition() {
        return serviceInvocationDisposition;
    }

    /**
     * Legt den Wert der serviceInvocationDisposition-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInvocationDisposition }
     *     
     */
    public void setServiceInvocationDisposition(ServiceInvocationDisposition value) {
        this.serviceInvocationDisposition = value;
    }

    /**
     * Ruft den Wert der serviceInvocationDialedNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceInvocationDialedNumber() {
        return serviceInvocationDialedNumber;
    }

    /**
     * Legt den Wert der serviceInvocationDialedNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceInvocationDialedNumber(String value) {
        this.serviceInvocationDialedNumber = value;
    }

    /**
     * Ruft den Wert der serviceInvocationCalledNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceInvocationCalledNumber() {
        return serviceInvocationCalledNumber;
    }

    /**
     * Legt den Wert der serviceInvocationCalledNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceInvocationCalledNumber(String value) {
        this.serviceInvocationCalledNumber = value;
    }

    /**
     * Ruft den Wert der serviceInvocationNetworkTranslatedAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceInvocationNetworkTranslatedAddress() {
        return serviceInvocationNetworkTranslatedAddress;
    }

    /**
     * Legt den Wert der serviceInvocationNetworkTranslatedAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceInvocationNetworkTranslatedAddress(String value) {
        this.serviceInvocationNetworkTranslatedAddress = value;
    }

    /**
     * Ruft den Wert der serviceInvocationTypeOfNetwork-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkType }
     *     
     */
    public NetworkType getServiceInvocationTypeOfNetwork() {
        return serviceInvocationTypeOfNetwork;
    }

    /**
     * Legt den Wert der serviceInvocationTypeOfNetwork-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkType }
     *     
     */
    public void setServiceInvocationTypeOfNetwork(NetworkType value) {
        this.serviceInvocationTypeOfNetwork = value;
    }

    /**
     * Ruft den Wert der serviceInvocationCallCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCategory }
     *     
     */
    public CallCategory getServiceInvocationCallCategory() {
        return serviceInvocationCallCategory;
    }

    /**
     * Legt den Wert der serviceInvocationCallCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCategory }
     *     
     */
    public void setServiceInvocationCallCategory(CallCategory value) {
        this.serviceInvocationCallCategory = value;
    }

    /**
     * Ruft den Wert der serviceInvocationBasicCallType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BasicCallType }
     *     
     */
    public BasicCallType getServiceInvocationBasicCallType() {
        return serviceInvocationBasicCallType;
    }

    /**
     * Legt den Wert der serviceInvocationBasicCallType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BasicCallType }
     *     
     */
    public void setServiceInvocationBasicCallType(BasicCallType value) {
        this.serviceInvocationBasicCallType = value;
    }

    /**
     * Ruft den Wert der serviceInvocationConfigurableCallType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceInvocationConfigurableCallType() {
        return serviceInvocationConfigurableCallType;
    }

    /**
     * Legt den Wert der serviceInvocationConfigurableCallType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceInvocationConfigurableCallType(String value) {
        this.serviceInvocationConfigurableCallType = value;
    }

    /**
     * Ruft den Wert der serviceInvocationAlternateCallIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceInvocationAlternateCallIndicator() {
        return serviceInvocationAlternateCallIndicator;
    }

    /**
     * Legt den Wert der serviceInvocationAlternateCallIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceInvocationAlternateCallIndicator(String value) {
        this.serviceInvocationAlternateCallIndicator = value;
    }

    /**
     * Ruft den Wert der serviceInvocationVirtualOnNetCallType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceInvocationVirtualOnNetCallType() {
        return serviceInvocationVirtualOnNetCallType;
    }

    /**
     * Legt den Wert der serviceInvocationVirtualOnNetCallType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceInvocationVirtualOnNetCallType(String value) {
        this.serviceInvocationVirtualOnNetCallType = value;
    }

    /**
     * Ruft den Wert der serviceInvocationCalledDirectoryName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceInvocationCalledDirectoryName() {
        return serviceInvocationCalledDirectoryName;
    }

    /**
     * Legt den Wert der serviceInvocationCalledDirectoryName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceInvocationCalledDirectoryName(String value) {
        this.serviceInvocationCalledDirectoryName = value;
    }

    /**
     * Ruft den Wert der serviceInvocationCalledGroupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceInvocationCalledGroupId() {
        return serviceInvocationCalledGroupId;
    }

    /**
     * Legt den Wert der serviceInvocationCalledGroupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceInvocationCalledGroupId(String value) {
        this.serviceInvocationCalledGroupId = value;
    }

    /**
     * Ruft den Wert der redirectingNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRedirectingNumber() {
        return redirectingNumber;
    }

    /**
     * Legt den Wert der redirectingNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRedirectingNumber(String value) {
        this.redirectingNumber = value;
    }

    /**
     * Ruft den Wert der redirectingName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRedirectingName() {
        return redirectingName;
    }

    /**
     * Legt den Wert der redirectingName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRedirectingName(String value) {
        this.redirectingName = value;
    }

    /**
     * Ruft den Wert der redirectingPresentationIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RedirectingPresentationIndicator }
     *     
     */
    public RedirectingPresentationIndicator getRedirectingPresentationIndicator() {
        return redirectingPresentationIndicator;
    }

    /**
     * Legt den Wert der redirectingPresentationIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RedirectingPresentationIndicator }
     *     
     */
    public void setRedirectingPresentationIndicator(RedirectingPresentationIndicator value) {
        this.redirectingPresentationIndicator = value;
    }

    /**
     * Ruft den Wert der redirectingReason-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRedirectingReason() {
        return redirectingReason;
    }

    /**
     * Legt den Wert der redirectingReason-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRedirectingReason(String value) {
        this.redirectingReason = value;
    }

    /**
     * Ruft den Wert der accountAuthorizationCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountAuthorizationCode() {
        return accountAuthorizationCode;
    }

    /**
     * Legt den Wert der accountAuthorizationCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountAuthorizationCode(String value) {
        this.accountAuthorizationCode = value;
    }

    /**
     * Ruft den Wert der callAuthorizationCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallAuthorizationCode() {
        return callAuthorizationCode;
    }

    /**
     * Legt den Wert der callAuthorizationCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallAuthorizationCode(String value) {
        this.callAuthorizationCode = value;
    }

    /**
     * Ruft den Wert der userGroupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserGroupId() {
        return userGroupId;
    }

    /**
     * Legt den Wert der userGroupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserGroupId(String value) {
        this.userGroupId = value;
    }

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der userPrimaryDn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserPrimaryDn() {
        return userPrimaryDn;
    }

    /**
     * Legt den Wert der userPrimaryDn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserPrimaryDn(String value) {
        this.userPrimaryDn = value;
    }

    /**
     * Ruft den Wert der userPrimaryExtension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserPrimaryExtension() {
        return userPrimaryExtension;
    }

    /**
     * Legt den Wert der userPrimaryExtension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserPrimaryExtension(String value) {
        this.userPrimaryExtension = value;
    }

}
