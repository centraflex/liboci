//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for a particular fully specified DeviceTypeConfigurationOptionType.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactDeviceTypeConfigurationOptionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactDeviceTypeConfigurationOptionType">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="deviceConfigOptions" type="{}DeviceTypeConfigurationOptionType"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactDeviceTypeConfigurationOptionType", propOrder = {
    "deviceConfigOptions"
})
public class SearchCriteriaExactDeviceTypeConfigurationOptionType
    extends SearchCriteria
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceTypeConfigurationOptionType deviceConfigOptions;

    /**
     * Ruft den Wert der deviceConfigOptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceTypeConfigurationOptionType }
     *     
     */
    public DeviceTypeConfigurationOptionType getDeviceConfigOptions() {
        return deviceConfigOptions;
    }

    /**
     * Legt den Wert der deviceConfigOptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceTypeConfigurationOptionType }
     *     
     */
    public void setDeviceConfigOptions(DeviceTypeConfigurationOptionType value) {
        this.deviceConfigOptions = value;
    }

}
