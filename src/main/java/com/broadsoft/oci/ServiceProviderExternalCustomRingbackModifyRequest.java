//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the service provider level data associated with External Custom Ringback.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderExternalCustomRingbackModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderExternalCustomRingbackModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="prefixDigits" type="{}ExternalCustomRingbackPrefixDigits" minOccurs="0"/>
 *         <element name="serverNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="serverPort" type="{}Port1025" minOccurs="0"/>
 *         <element name="timeoutSeconds" type="{}ExternalCustomRingbackTimeoutSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderExternalCustomRingbackModifyRequest", propOrder = {
    "serviceProviderId",
    "prefixDigits",
    "serverNetAddress",
    "serverPort",
    "timeoutSeconds"
})
public class ServiceProviderExternalCustomRingbackModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElementRef(name = "prefixDigits", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prefixDigits;
    @XmlElementRef(name = "serverNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serverNetAddress;
    @XmlElementRef(name = "serverPort", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> serverPort;
    protected Integer timeoutSeconds;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der prefixDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrefixDigits() {
        return prefixDigits;
    }

    /**
     * Legt den Wert der prefixDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrefixDigits(JAXBElement<String> value) {
        this.prefixDigits = value;
    }

    /**
     * Ruft den Wert der serverNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServerNetAddress() {
        return serverNetAddress;
    }

    /**
     * Legt den Wert der serverNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServerNetAddress(JAXBElement<String> value) {
        this.serverNetAddress = value;
    }

    /**
     * Ruft den Wert der serverPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getServerPort() {
        return serverPort;
    }

    /**
     * Legt den Wert der serverPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setServerPort(JAXBElement<Integer> value) {
        this.serverPort = value;
    }

    /**
     * Ruft den Wert der timeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTimeoutSeconds() {
        return timeoutSeconds;
    }

    /**
     * Legt den Wert der timeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTimeoutSeconds(Integer value) {
        this.timeoutSeconds = value;
    }

}
