//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal change busy or not answer greeting menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für ChangeBusyOrNoAnswerGreetingMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ChangeBusyOrNoAnswerGreetingMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="recordNewGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="listenToCurrentGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="revertToSystemDefaultGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeBusyOrNoAnswerGreetingMenuKeysModifyEntry", propOrder = {
    "recordNewGreeting",
    "listenToCurrentGreeting",
    "revertToSystemDefaultGreeting",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class ChangeBusyOrNoAnswerGreetingMenuKeysModifyEntry {

    @XmlElementRef(name = "recordNewGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recordNewGreeting;
    @XmlElementRef(name = "listenToCurrentGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> listenToCurrentGreeting;
    @XmlElementRef(name = "revertToSystemDefaultGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> revertToSystemDefaultGreeting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repeatMenu;

    /**
     * Ruft den Wert der recordNewGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecordNewGreeting() {
        return recordNewGreeting;
    }

    /**
     * Legt den Wert der recordNewGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecordNewGreeting(JAXBElement<String> value) {
        this.recordNewGreeting = value;
    }

    /**
     * Ruft den Wert der listenToCurrentGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getListenToCurrentGreeting() {
        return listenToCurrentGreeting;
    }

    /**
     * Legt den Wert der listenToCurrentGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setListenToCurrentGreeting(JAXBElement<String> value) {
        this.listenToCurrentGreeting = value;
    }

    /**
     * Ruft den Wert der revertToSystemDefaultGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRevertToSystemDefaultGreeting() {
        return revertToSystemDefaultGreeting;
    }

    /**
     * Legt den Wert der revertToSystemDefaultGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRevertToSystemDefaultGreeting(JAXBElement<String> value) {
        this.revertToSystemDefaultGreeting = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepeatMenu(JAXBElement<String> value) {
        this.repeatMenu = value;
    }

}
