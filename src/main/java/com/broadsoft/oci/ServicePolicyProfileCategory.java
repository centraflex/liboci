//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ServicePolicyProfileCategory.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ServicePolicyProfileCategory">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Call Processing Parameters"/>
 *     <enumeration value="Call Types Mapping"/>
 *     <enumeration value="Configurable Treatments"/>
 *     <enumeration value="External Emergency Routing Parameters"/>
 *     <enumeration value="GETS"/>
 *     <enumeration value="Provisioning Validation"/>
 *     <enumeration value="Services"/>
 *     <enumeration value="Service Codes"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ServicePolicyProfileCategory")
@XmlEnum
public enum ServicePolicyProfileCategory {

    @XmlEnumValue("Call Processing Parameters")
    CALL_PROCESSING_PARAMETERS("Call Processing Parameters"),
    @XmlEnumValue("Call Types Mapping")
    CALL_TYPES_MAPPING("Call Types Mapping"),
    @XmlEnumValue("Configurable Treatments")
    CONFIGURABLE_TREATMENTS("Configurable Treatments"),
    @XmlEnumValue("External Emergency Routing Parameters")
    EXTERNAL_EMERGENCY_ROUTING_PARAMETERS("External Emergency Routing Parameters"),
    GETS("GETS"),
    @XmlEnumValue("Provisioning Validation")
    PROVISIONING_VALIDATION("Provisioning Validation"),
    @XmlEnumValue("Services")
    SERVICES("Services"),
    @XmlEnumValue("Service Codes")
    SERVICE_CODES("Service Codes");
    private final String value;

    ServicePolicyProfileCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ServicePolicyProfileCategory fromValue(String v) {
        for (ServicePolicyProfileCategory c: ServicePolicyProfileCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
