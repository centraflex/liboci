//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a Auto Attendant instance to a group.
 *         The domain is required in the serviceUserId.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupAutoAttendantAddInstanceRequest16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAutoAttendantAddInstanceRequest16">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceAddProfile"/>
 *         <element name="enableVideo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="businessHours" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="holidayScheduleName" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="extensionDialingScope" type="{}AutoAttendantDialingScope"/>
 *         <element name="nameDialingScope" type="{}AutoAttendantDialingScope"/>
 *         <element name="nameDialingEntries" type="{}AutoAttendantNameDialingEntry"/>
 *         <element name="businessHoursMenu" type="{}AutoAttendantAddMenu16" minOccurs="0"/>
 *         <element name="afterHoursMenu" type="{}AutoAttendantAddMenu16" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAutoAttendantAddInstanceRequest16", propOrder = {
    "serviceProviderId",
    "groupId",
    "serviceUserId",
    "serviceInstanceProfile",
    "enableVideo",
    "businessHours",
    "holidayScheduleName",
    "extensionDialingScope",
    "nameDialingScope",
    "nameDialingEntries",
    "businessHoursMenu",
    "afterHoursMenu"
})
public class GroupAutoAttendantAddInstanceRequest16
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlElement(required = true)
    protected ServiceInstanceAddProfile serviceInstanceProfile;
    protected boolean enableVideo;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String businessHours;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String holidayScheduleName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AutoAttendantDialingScope extensionDialingScope;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AutoAttendantDialingScope nameDialingScope;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AutoAttendantNameDialingEntry nameDialingEntries;
    protected AutoAttendantAddMenu16 businessHoursMenu;
    protected AutoAttendantAddMenu16 afterHoursMenu;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceAddProfile }
     *     
     */
    public ServiceInstanceAddProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceAddProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceAddProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der enableVideo-Eigenschaft ab.
     * 
     */
    public boolean isEnableVideo() {
        return enableVideo;
    }

    /**
     * Legt den Wert der enableVideo-Eigenschaft fest.
     * 
     */
    public void setEnableVideo(boolean value) {
        this.enableVideo = value;
    }

    /**
     * Ruft den Wert der businessHours-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessHours() {
        return businessHours;
    }

    /**
     * Legt den Wert der businessHours-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessHours(String value) {
        this.businessHours = value;
    }

    /**
     * Ruft den Wert der holidayScheduleName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHolidayScheduleName() {
        return holidayScheduleName;
    }

    /**
     * Legt den Wert der holidayScheduleName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHolidayScheduleName(String value) {
        this.holidayScheduleName = value;
    }

    /**
     * Ruft den Wert der extensionDialingScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public AutoAttendantDialingScope getExtensionDialingScope() {
        return extensionDialingScope;
    }

    /**
     * Legt den Wert der extensionDialingScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public void setExtensionDialingScope(AutoAttendantDialingScope value) {
        this.extensionDialingScope = value;
    }

    /**
     * Ruft den Wert der nameDialingScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public AutoAttendantDialingScope getNameDialingScope() {
        return nameDialingScope;
    }

    /**
     * Legt den Wert der nameDialingScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public void setNameDialingScope(AutoAttendantDialingScope value) {
        this.nameDialingScope = value;
    }

    /**
     * Ruft den Wert der nameDialingEntries-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantNameDialingEntry }
     *     
     */
    public AutoAttendantNameDialingEntry getNameDialingEntries() {
        return nameDialingEntries;
    }

    /**
     * Legt den Wert der nameDialingEntries-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantNameDialingEntry }
     *     
     */
    public void setNameDialingEntries(AutoAttendantNameDialingEntry value) {
        this.nameDialingEntries = value;
    }

    /**
     * Ruft den Wert der businessHoursMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantAddMenu16 }
     *     
     */
    public AutoAttendantAddMenu16 getBusinessHoursMenu() {
        return businessHoursMenu;
    }

    /**
     * Legt den Wert der businessHoursMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantAddMenu16 }
     *     
     */
    public void setBusinessHoursMenu(AutoAttendantAddMenu16 value) {
        this.businessHoursMenu = value;
    }

    /**
     * Ruft den Wert der afterHoursMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantAddMenu16 }
     *     
     */
    public AutoAttendantAddMenu16 getAfterHoursMenu() {
        return afterHoursMenu;
    }

    /**
     * Legt den Wert der afterHoursMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantAddMenu16 }
     *     
     */
    public void setAfterHoursMenu(AutoAttendantAddMenu16 value) {
        this.afterHoursMenu = value;
    }

}
