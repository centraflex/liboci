//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the EnterpriseBroadWorksMobileManagerGetRequest
 *       
 * 
 * <p>Java-Klasse für EnterpriseBroadWorksMobileManagerGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseBroadWorksMobileManagerGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="mobileManagerId" type="{}BroadWorksMobileManagerDomainName"/>
 *         <element name="name" type="{}BroadWorksMobileManagerName"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="localToCarrier" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="deactivationReason" type="{}BroadWorksMobileManagerDeactivationReason" minOccurs="0"/>
 *         <element name="maxTxPerSecondEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxTxPerSecond" type="{}BroadWorksMobileManagerMaxTxPerSecond" minOccurs="0"/>
 *         <element name="tldnEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="genericNumberEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="mobileStateCheckEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="locationBasedServicesEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseBroadWorksMobileManagerGetResponse", propOrder = {
    "mobileManagerId",
    "name",
    "isActive",
    "localToCarrier",
    "deactivationReason",
    "maxTxPerSecondEnabled",
    "maxTxPerSecond",
    "tldnEnabled",
    "genericNumberEnabled",
    "mobileStateCheckEnabled",
    "locationBasedServicesEnabled"
})
public class EnterpriseBroadWorksMobileManagerGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mobileManagerId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    protected boolean isActive;
    protected boolean localToCarrier;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deactivationReason;
    protected boolean maxTxPerSecondEnabled;
    protected Integer maxTxPerSecond;
    protected boolean tldnEnabled;
    protected boolean genericNumberEnabled;
    protected boolean mobileStateCheckEnabled;
    protected boolean locationBasedServicesEnabled;

    /**
     * Ruft den Wert der mobileManagerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobileManagerId() {
        return mobileManagerId;
    }

    /**
     * Legt den Wert der mobileManagerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobileManagerId(String value) {
        this.mobileManagerId = value;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der localToCarrier-Eigenschaft ab.
     * 
     */
    public boolean isLocalToCarrier() {
        return localToCarrier;
    }

    /**
     * Legt den Wert der localToCarrier-Eigenschaft fest.
     * 
     */
    public void setLocalToCarrier(boolean value) {
        this.localToCarrier = value;
    }

    /**
     * Ruft den Wert der deactivationReason-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeactivationReason() {
        return deactivationReason;
    }

    /**
     * Legt den Wert der deactivationReason-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeactivationReason(String value) {
        this.deactivationReason = value;
    }

    /**
     * Ruft den Wert der maxTxPerSecondEnabled-Eigenschaft ab.
     * 
     */
    public boolean isMaxTxPerSecondEnabled() {
        return maxTxPerSecondEnabled;
    }

    /**
     * Legt den Wert der maxTxPerSecondEnabled-Eigenschaft fest.
     * 
     */
    public void setMaxTxPerSecondEnabled(boolean value) {
        this.maxTxPerSecondEnabled = value;
    }

    /**
     * Ruft den Wert der maxTxPerSecond-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxTxPerSecond() {
        return maxTxPerSecond;
    }

    /**
     * Legt den Wert der maxTxPerSecond-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxTxPerSecond(Integer value) {
        this.maxTxPerSecond = value;
    }

    /**
     * Ruft den Wert der tldnEnabled-Eigenschaft ab.
     * 
     */
    public boolean isTldnEnabled() {
        return tldnEnabled;
    }

    /**
     * Legt den Wert der tldnEnabled-Eigenschaft fest.
     * 
     */
    public void setTldnEnabled(boolean value) {
        this.tldnEnabled = value;
    }

    /**
     * Ruft den Wert der genericNumberEnabled-Eigenschaft ab.
     * 
     */
    public boolean isGenericNumberEnabled() {
        return genericNumberEnabled;
    }

    /**
     * Legt den Wert der genericNumberEnabled-Eigenschaft fest.
     * 
     */
    public void setGenericNumberEnabled(boolean value) {
        this.genericNumberEnabled = value;
    }

    /**
     * Ruft den Wert der mobileStateCheckEnabled-Eigenschaft ab.
     * 
     */
    public boolean isMobileStateCheckEnabled() {
        return mobileStateCheckEnabled;
    }

    /**
     * Legt den Wert der mobileStateCheckEnabled-Eigenschaft fest.
     * 
     */
    public void setMobileStateCheckEnabled(boolean value) {
        this.mobileStateCheckEnabled = value;
    }

    /**
     * Ruft den Wert der locationBasedServicesEnabled-Eigenschaft ab.
     * 
     */
    public boolean isLocationBasedServicesEnabled() {
        return locationBasedServicesEnabled;
    }

    /**
     * Legt den Wert der locationBasedServicesEnabled-Eigenschaft fest.
     * 
     */
    public void setLocationBasedServicesEnabled(boolean value) {
        this.locationBasedServicesEnabled = value;
    }

}
