//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemNetworkServerSyncParametersGetRequest16.
 *         Contains a list of system Network Server Sync parameters.
 *         Replaced By: SystemNetworkServerSyncParametersGetResponse17sp4
 *       
 * 
 * <p>Java-Klasse für SystemNetworkServerSyncParametersGetResponse16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNetworkServerSyncParametersGetResponse16">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableSync" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="syncLinePorts" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="syncDeviceManagementInfo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNetworkServerSyncParametersGetResponse16", propOrder = {
    "enableSync",
    "syncLinePorts",
    "syncDeviceManagementInfo"
})
public class SystemNetworkServerSyncParametersGetResponse16
    extends OCIDataResponse
{

    protected boolean enableSync;
    protected boolean syncLinePorts;
    protected boolean syncDeviceManagementInfo;

    /**
     * Ruft den Wert der enableSync-Eigenschaft ab.
     * 
     */
    public boolean isEnableSync() {
        return enableSync;
    }

    /**
     * Legt den Wert der enableSync-Eigenschaft fest.
     * 
     */
    public void setEnableSync(boolean value) {
        this.enableSync = value;
    }

    /**
     * Ruft den Wert der syncLinePorts-Eigenschaft ab.
     * 
     */
    public boolean isSyncLinePorts() {
        return syncLinePorts;
    }

    /**
     * Legt den Wert der syncLinePorts-Eigenschaft fest.
     * 
     */
    public void setSyncLinePorts(boolean value) {
        this.syncLinePorts = value;
    }

    /**
     * Ruft den Wert der syncDeviceManagementInfo-Eigenschaft ab.
     * 
     */
    public boolean isSyncDeviceManagementInfo() {
        return syncDeviceManagementInfo;
    }

    /**
     * Legt den Wert der syncDeviceManagementInfo-Eigenschaft fest.
     * 
     */
    public void setSyncDeviceManagementInfo(boolean value) {
        this.syncDeviceManagementInfo = value;
    }

}
