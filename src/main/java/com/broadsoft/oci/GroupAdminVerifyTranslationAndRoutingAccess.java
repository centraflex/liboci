//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GroupAdminVerifyTranslationAndRoutingAccess.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="GroupAdminVerifyTranslationAndRoutingAccess">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Full"/>
 *     <enumeration value="None"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "GroupAdminVerifyTranslationAndRoutingAccess")
@XmlEnum
public enum GroupAdminVerifyTranslationAndRoutingAccess {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("None")
    NONE("None");
    private final String value;

    GroupAdminVerifyTranslationAndRoutingAccess(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupAdminVerifyTranslationAndRoutingAccess fromValue(String v) {
        for (GroupAdminVerifyTranslationAndRoutingAccess c: GroupAdminVerifyTranslationAndRoutingAccess.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
