//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a new file repository.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemFileRepositoryDeviceAddRequest20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemFileRepositoryDeviceAddRequest20">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="name" type="{}FileRepositoryName"/>
 *         <element name="rootDirectory" type="{}CPEFileDirectory" minOccurs="0"/>
 *         <element name="port" type="{}Port" minOccurs="0"/>
 *         <choice>
 *           <element name="protocolWebDAV" type="{}FileRepositoryProtocolWebDAV20"/>
 *           <element name="protocolFTP" type="{}FileRepositoryProtocolFTP16"/>
 *           <element name="protocolSFTP" type="{}FileRepositoryProtocolFTP16"/>
 *           <element name="protocolFTPS" type="{}FileRepositoryProtocolFTP16"/>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemFileRepositoryDeviceAddRequest20", propOrder = {
    "name",
    "rootDirectory",
    "port",
    "protocolWebDAV",
    "protocolFTP",
    "protocolSFTP",
    "protocolFTPS"
})
public class SystemFileRepositoryDeviceAddRequest20
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String rootDirectory;
    protected Integer port;
    protected FileRepositoryProtocolWebDAV20 protocolWebDAV;
    protected FileRepositoryProtocolFTP16 protocolFTP;
    protected FileRepositoryProtocolFTP16 protocolSFTP;
    protected FileRepositoryProtocolFTP16 protocolFTPS;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der rootDirectory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRootDirectory() {
        return rootDirectory;
    }

    /**
     * Legt den Wert der rootDirectory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRootDirectory(String value) {
        this.rootDirectory = value;
    }

    /**
     * Ruft den Wert der port-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPort() {
        return port;
    }

    /**
     * Legt den Wert der port-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPort(Integer value) {
        this.port = value;
    }

    /**
     * Ruft den Wert der protocolWebDAV-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FileRepositoryProtocolWebDAV20 }
     *     
     */
    public FileRepositoryProtocolWebDAV20 getProtocolWebDAV() {
        return protocolWebDAV;
    }

    /**
     * Legt den Wert der protocolWebDAV-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FileRepositoryProtocolWebDAV20 }
     *     
     */
    public void setProtocolWebDAV(FileRepositoryProtocolWebDAV20 value) {
        this.protocolWebDAV = value;
    }

    /**
     * Ruft den Wert der protocolFTP-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FileRepositoryProtocolFTP16 }
     *     
     */
    public FileRepositoryProtocolFTP16 getProtocolFTP() {
        return protocolFTP;
    }

    /**
     * Legt den Wert der protocolFTP-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FileRepositoryProtocolFTP16 }
     *     
     */
    public void setProtocolFTP(FileRepositoryProtocolFTP16 value) {
        this.protocolFTP = value;
    }

    /**
     * Ruft den Wert der protocolSFTP-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FileRepositoryProtocolFTP16 }
     *     
     */
    public FileRepositoryProtocolFTP16 getProtocolSFTP() {
        return protocolSFTP;
    }

    /**
     * Legt den Wert der protocolSFTP-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FileRepositoryProtocolFTP16 }
     *     
     */
    public void setProtocolSFTP(FileRepositoryProtocolFTP16 value) {
        this.protocolSFTP = value;
    }

    /**
     * Ruft den Wert der protocolFTPS-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FileRepositoryProtocolFTP16 }
     *     
     */
    public FileRepositoryProtocolFTP16 getProtocolFTPS() {
        return protocolFTPS;
    }

    /**
     * Legt den Wert der protocolFTPS-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FileRepositoryProtocolFTP16 }
     *     
     */
    public void setProtocolFTPS(FileRepositoryProtocolFTP16 value) {
        this.protocolFTPS = value;
    }

}
