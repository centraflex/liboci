//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SIPSuppressRFC3312Preconditions.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SIPSuppressRFC3312Preconditions">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Always"/>
 *     <enumeration value="Never"/>
 *     <enumeration value="Suppress If Single Dialog"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SIPSuppressRFC3312Preconditions")
@XmlEnum
public enum SIPSuppressRFC3312Preconditions {

    @XmlEnumValue("Always")
    ALWAYS("Always"),
    @XmlEnumValue("Never")
    NEVER("Never"),
    @XmlEnumValue("Suppress If Single Dialog")
    SUPPRESS_IF_SINGLE_DIALOG("Suppress If Single Dialog");
    private final String value;

    SIPSuppressRFC3312Preconditions(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SIPSuppressRFC3312Preconditions fromValue(String v) {
        for (SIPSuppressRFC3312Preconditions c: SIPSuppressRFC3312Preconditions.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
