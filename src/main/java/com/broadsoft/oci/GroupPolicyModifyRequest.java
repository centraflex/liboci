//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *           Request to modify the policies for a Group.
 *           The response is either a SuccessResponse or an ErrorResponse.
 *           
 *         The following elements are only used in AS data mode:
 *           userAutoAttendantNameDialingAccess  
 *           
 *           Replaced by: GroupPolicyModifyRequest22 in AS data mode        
 *         
 * 
 * <p>Java-Klasse für GroupPolicyModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupPolicyModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="callingPlanAccess" type="{}GroupCallingPlanAccess" minOccurs="0"/>
 *         <element name="extensionAccess" type="{}GroupExtensionAccess" minOccurs="0"/>
 *         <element name="ldapIntegrationAccess" type="{}GroupLDAPIntegrationAccess" minOccurs="0"/>
 *         <element name="voiceMessagingAccess" type="{}GroupVoiceMessagingAccess" minOccurs="0"/>
 *         <element name="departmentAdminUserAccess" type="{}GroupDepartmentAdminUserAccess" minOccurs="0"/>
 *         <element name="departmentAdminTrunkGroupAccess" type="{}GroupDepartmentAdminTrunkGroupAccess" minOccurs="0"/>
 *         <element name="departmentAdminPhoneNumberExtensionAccess" type="{}GroupDepartmentAdminPhoneNumberExtensionAccess" minOccurs="0"/>
 *         <element name="departmentAdminCallingLineIdNumberAccess" type="{}GroupDepartmentAdminCallingLineIdNumberAccess" minOccurs="0"/>
 *         <element name="userAuthenticationAccess" type="{}GroupUserAuthenticationAccess" minOccurs="0"/>
 *         <element name="userGroupDirectoryAccess" type="{}GroupUserGroupDirectoryAccess" minOccurs="0"/>
 *         <element name="userProfileAccess" type="{}GroupUserProfileAccess" minOccurs="0"/>
 *         <element name="userEnhancedCallLogAccess" type="{}GroupUserCallLogAccess" minOccurs="0"/>
 *         <element name="userAutoAttendantNameDialingAccess" type="{}GroupUserAutoAttendantNameDialingAccess" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupPolicyModifyRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "callingPlanAccess",
    "extensionAccess",
    "ldapIntegrationAccess",
    "voiceMessagingAccess",
    "departmentAdminUserAccess",
    "departmentAdminTrunkGroupAccess",
    "departmentAdminPhoneNumberExtensionAccess",
    "departmentAdminCallingLineIdNumberAccess",
    "userAuthenticationAccess",
    "userGroupDirectoryAccess",
    "userProfileAccess",
    "userEnhancedCallLogAccess",
    "userAutoAttendantNameDialingAccess"
})
public class GroupPolicyModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlSchemaType(name = "token")
    protected GroupCallingPlanAccess callingPlanAccess;
    @XmlSchemaType(name = "token")
    protected GroupExtensionAccess extensionAccess;
    @XmlSchemaType(name = "token")
    protected GroupLDAPIntegrationAccess ldapIntegrationAccess;
    @XmlSchemaType(name = "token")
    protected GroupVoiceMessagingAccess voiceMessagingAccess;
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminUserAccess departmentAdminUserAccess;
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminTrunkGroupAccess departmentAdminTrunkGroupAccess;
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminPhoneNumberExtensionAccess departmentAdminPhoneNumberExtensionAccess;
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminCallingLineIdNumberAccess departmentAdminCallingLineIdNumberAccess;
    @XmlSchemaType(name = "token")
    protected GroupUserAuthenticationAccess userAuthenticationAccess;
    @XmlSchemaType(name = "token")
    protected GroupUserGroupDirectoryAccess userGroupDirectoryAccess;
    @XmlSchemaType(name = "token")
    protected GroupUserProfileAccess userProfileAccess;
    @XmlSchemaType(name = "token")
    protected GroupUserCallLogAccess userEnhancedCallLogAccess;
    @XmlSchemaType(name = "token")
    protected GroupUserAutoAttendantNameDialingAccess userAutoAttendantNameDialingAccess;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der callingPlanAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupCallingPlanAccess }
     *     
     */
    public GroupCallingPlanAccess getCallingPlanAccess() {
        return callingPlanAccess;
    }

    /**
     * Legt den Wert der callingPlanAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupCallingPlanAccess }
     *     
     */
    public void setCallingPlanAccess(GroupCallingPlanAccess value) {
        this.callingPlanAccess = value;
    }

    /**
     * Ruft den Wert der extensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupExtensionAccess }
     *     
     */
    public GroupExtensionAccess getExtensionAccess() {
        return extensionAccess;
    }

    /**
     * Legt den Wert der extensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupExtensionAccess }
     *     
     */
    public void setExtensionAccess(GroupExtensionAccess value) {
        this.extensionAccess = value;
    }

    /**
     * Ruft den Wert der ldapIntegrationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupLDAPIntegrationAccess }
     *     
     */
    public GroupLDAPIntegrationAccess getLdapIntegrationAccess() {
        return ldapIntegrationAccess;
    }

    /**
     * Legt den Wert der ldapIntegrationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupLDAPIntegrationAccess }
     *     
     */
    public void setLdapIntegrationAccess(GroupLDAPIntegrationAccess value) {
        this.ldapIntegrationAccess = value;
    }

    /**
     * Ruft den Wert der voiceMessagingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupVoiceMessagingAccess }
     *     
     */
    public GroupVoiceMessagingAccess getVoiceMessagingAccess() {
        return voiceMessagingAccess;
    }

    /**
     * Legt den Wert der voiceMessagingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupVoiceMessagingAccess }
     *     
     */
    public void setVoiceMessagingAccess(GroupVoiceMessagingAccess value) {
        this.voiceMessagingAccess = value;
    }

    /**
     * Ruft den Wert der departmentAdminUserAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminUserAccess }
     *     
     */
    public GroupDepartmentAdminUserAccess getDepartmentAdminUserAccess() {
        return departmentAdminUserAccess;
    }

    /**
     * Legt den Wert der departmentAdminUserAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminUserAccess }
     *     
     */
    public void setDepartmentAdminUserAccess(GroupDepartmentAdminUserAccess value) {
        this.departmentAdminUserAccess = value;
    }

    /**
     * Ruft den Wert der departmentAdminTrunkGroupAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminTrunkGroupAccess }
     *     
     */
    public GroupDepartmentAdminTrunkGroupAccess getDepartmentAdminTrunkGroupAccess() {
        return departmentAdminTrunkGroupAccess;
    }

    /**
     * Legt den Wert der departmentAdminTrunkGroupAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminTrunkGroupAccess }
     *     
     */
    public void setDepartmentAdminTrunkGroupAccess(GroupDepartmentAdminTrunkGroupAccess value) {
        this.departmentAdminTrunkGroupAccess = value;
    }

    /**
     * Ruft den Wert der departmentAdminPhoneNumberExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminPhoneNumberExtensionAccess }
     *     
     */
    public GroupDepartmentAdminPhoneNumberExtensionAccess getDepartmentAdminPhoneNumberExtensionAccess() {
        return departmentAdminPhoneNumberExtensionAccess;
    }

    /**
     * Legt den Wert der departmentAdminPhoneNumberExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminPhoneNumberExtensionAccess }
     *     
     */
    public void setDepartmentAdminPhoneNumberExtensionAccess(GroupDepartmentAdminPhoneNumberExtensionAccess value) {
        this.departmentAdminPhoneNumberExtensionAccess = value;
    }

    /**
     * Ruft den Wert der departmentAdminCallingLineIdNumberAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminCallingLineIdNumberAccess }
     *     
     */
    public GroupDepartmentAdminCallingLineIdNumberAccess getDepartmentAdminCallingLineIdNumberAccess() {
        return departmentAdminCallingLineIdNumberAccess;
    }

    /**
     * Legt den Wert der departmentAdminCallingLineIdNumberAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminCallingLineIdNumberAccess }
     *     
     */
    public void setDepartmentAdminCallingLineIdNumberAccess(GroupDepartmentAdminCallingLineIdNumberAccess value) {
        this.departmentAdminCallingLineIdNumberAccess = value;
    }

    /**
     * Ruft den Wert der userAuthenticationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserAuthenticationAccess }
     *     
     */
    public GroupUserAuthenticationAccess getUserAuthenticationAccess() {
        return userAuthenticationAccess;
    }

    /**
     * Legt den Wert der userAuthenticationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserAuthenticationAccess }
     *     
     */
    public void setUserAuthenticationAccess(GroupUserAuthenticationAccess value) {
        this.userAuthenticationAccess = value;
    }

    /**
     * Ruft den Wert der userGroupDirectoryAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserGroupDirectoryAccess }
     *     
     */
    public GroupUserGroupDirectoryAccess getUserGroupDirectoryAccess() {
        return userGroupDirectoryAccess;
    }

    /**
     * Legt den Wert der userGroupDirectoryAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserGroupDirectoryAccess }
     *     
     */
    public void setUserGroupDirectoryAccess(GroupUserGroupDirectoryAccess value) {
        this.userGroupDirectoryAccess = value;
    }

    /**
     * Ruft den Wert der userProfileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserProfileAccess }
     *     
     */
    public GroupUserProfileAccess getUserProfileAccess() {
        return userProfileAccess;
    }

    /**
     * Legt den Wert der userProfileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserProfileAccess }
     *     
     */
    public void setUserProfileAccess(GroupUserProfileAccess value) {
        this.userProfileAccess = value;
    }

    /**
     * Ruft den Wert der userEnhancedCallLogAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserCallLogAccess }
     *     
     */
    public GroupUserCallLogAccess getUserEnhancedCallLogAccess() {
        return userEnhancedCallLogAccess;
    }

    /**
     * Legt den Wert der userEnhancedCallLogAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserCallLogAccess }
     *     
     */
    public void setUserEnhancedCallLogAccess(GroupUserCallLogAccess value) {
        this.userEnhancedCallLogAccess = value;
    }

    /**
     * Ruft den Wert der userAutoAttendantNameDialingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserAutoAttendantNameDialingAccess }
     *     
     */
    public GroupUserAutoAttendantNameDialingAccess getUserAutoAttendantNameDialingAccess() {
        return userAutoAttendantNameDialingAccess;
    }

    /**
     * Legt den Wert der userAutoAttendantNameDialingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserAutoAttendantNameDialingAccess }
     *     
     */
    public void setUserAutoAttendantNameDialingAccess(GroupUserAutoAttendantNameDialingAccess value) {
        this.userAutoAttendantNameDialingAccess = value;
    }

}
