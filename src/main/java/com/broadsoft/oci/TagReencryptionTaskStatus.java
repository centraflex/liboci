//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TagReencryptionTaskStatus.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="TagReencryptionTaskStatus">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Not Started"/>
 *     <enumeration value="Processing"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TagReencryptionTaskStatus")
@XmlEnum
public enum TagReencryptionTaskStatus {

    @XmlEnumValue("Not Started")
    NOT_STARTED("Not Started"),
    @XmlEnumValue("Processing")
    PROCESSING("Processing");
    private final String value;

    TagReencryptionTaskStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TagReencryptionTaskStatus fromValue(String v) {
        for (TagReencryptionTaskStatus c: TagReencryptionTaskStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
