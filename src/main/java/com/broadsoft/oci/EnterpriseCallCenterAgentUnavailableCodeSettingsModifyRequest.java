//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the enterprise level data associated with Call Center Agents Unavailable Code Settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für EnterpriseCallCenterAgentUnavailableCodeSettingsModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseCallCenterAgentUnavailableCodeSettingsModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="enableAgentUnavailableCodes" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="defaultAgentUnavailableCodeOnDND" type="{}CallCenterAgentUnavailableCode" minOccurs="0"/>
 *         <element name="defaultAgentUnavailableCodeOnPersonalCalls" type="{}CallCenterAgentUnavailableCode" minOccurs="0"/>
 *         <element name="defaultAgentUnavailableCodeOnConsecutiveBounces" type="{}CallCenterAgentUnavailableCode" minOccurs="0"/>
 *         <element name="defaultAgentUnavailableCodeOnNotReachable" type="{}CallCenterAgentUnavailableCode" minOccurs="0"/>
 *         <element name="forceUseOfAgentUnavailableCodes" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="defaultAgentUnavailableCode" type="{}CallCenterAgentUnavailableCode" minOccurs="0"/>
 *         <element name="codeStateList" type="{}CallCenterAgentUnavailableCodeStateModify" maxOccurs="1000" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseCallCenterAgentUnavailableCodeSettingsModifyRequest", propOrder = {
    "serviceProviderId",
    "enableAgentUnavailableCodes",
    "defaultAgentUnavailableCodeOnDND",
    "defaultAgentUnavailableCodeOnPersonalCalls",
    "defaultAgentUnavailableCodeOnConsecutiveBounces",
    "defaultAgentUnavailableCodeOnNotReachable",
    "forceUseOfAgentUnavailableCodes",
    "defaultAgentUnavailableCode",
    "codeStateList"
})
public class EnterpriseCallCenterAgentUnavailableCodeSettingsModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected Boolean enableAgentUnavailableCodes;
    @XmlElementRef(name = "defaultAgentUnavailableCodeOnDND", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultAgentUnavailableCodeOnDND;
    @XmlElementRef(name = "defaultAgentUnavailableCodeOnPersonalCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultAgentUnavailableCodeOnPersonalCalls;
    @XmlElementRef(name = "defaultAgentUnavailableCodeOnConsecutiveBounces", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultAgentUnavailableCodeOnConsecutiveBounces;
    @XmlElementRef(name = "defaultAgentUnavailableCodeOnNotReachable", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultAgentUnavailableCodeOnNotReachable;
    protected Boolean forceUseOfAgentUnavailableCodes;
    @XmlElementRef(name = "defaultAgentUnavailableCode", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultAgentUnavailableCode;
    protected List<CallCenterAgentUnavailableCodeStateModify> codeStateList;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der enableAgentUnavailableCodes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableAgentUnavailableCodes() {
        return enableAgentUnavailableCodes;
    }

    /**
     * Legt den Wert der enableAgentUnavailableCodes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableAgentUnavailableCodes(Boolean value) {
        this.enableAgentUnavailableCodes = value;
    }

    /**
     * Ruft den Wert der defaultAgentUnavailableCodeOnDND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultAgentUnavailableCodeOnDND() {
        return defaultAgentUnavailableCodeOnDND;
    }

    /**
     * Legt den Wert der defaultAgentUnavailableCodeOnDND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultAgentUnavailableCodeOnDND(JAXBElement<String> value) {
        this.defaultAgentUnavailableCodeOnDND = value;
    }

    /**
     * Ruft den Wert der defaultAgentUnavailableCodeOnPersonalCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultAgentUnavailableCodeOnPersonalCalls() {
        return defaultAgentUnavailableCodeOnPersonalCalls;
    }

    /**
     * Legt den Wert der defaultAgentUnavailableCodeOnPersonalCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultAgentUnavailableCodeOnPersonalCalls(JAXBElement<String> value) {
        this.defaultAgentUnavailableCodeOnPersonalCalls = value;
    }

    /**
     * Ruft den Wert der defaultAgentUnavailableCodeOnConsecutiveBounces-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultAgentUnavailableCodeOnConsecutiveBounces() {
        return defaultAgentUnavailableCodeOnConsecutiveBounces;
    }

    /**
     * Legt den Wert der defaultAgentUnavailableCodeOnConsecutiveBounces-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultAgentUnavailableCodeOnConsecutiveBounces(JAXBElement<String> value) {
        this.defaultAgentUnavailableCodeOnConsecutiveBounces = value;
    }

    /**
     * Ruft den Wert der defaultAgentUnavailableCodeOnNotReachable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultAgentUnavailableCodeOnNotReachable() {
        return defaultAgentUnavailableCodeOnNotReachable;
    }

    /**
     * Legt den Wert der defaultAgentUnavailableCodeOnNotReachable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultAgentUnavailableCodeOnNotReachable(JAXBElement<String> value) {
        this.defaultAgentUnavailableCodeOnNotReachable = value;
    }

    /**
     * Ruft den Wert der forceUseOfAgentUnavailableCodes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceUseOfAgentUnavailableCodes() {
        return forceUseOfAgentUnavailableCodes;
    }

    /**
     * Legt den Wert der forceUseOfAgentUnavailableCodes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceUseOfAgentUnavailableCodes(Boolean value) {
        this.forceUseOfAgentUnavailableCodes = value;
    }

    /**
     * Ruft den Wert der defaultAgentUnavailableCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultAgentUnavailableCode() {
        return defaultAgentUnavailableCode;
    }

    /**
     * Legt den Wert der defaultAgentUnavailableCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultAgentUnavailableCode(JAXBElement<String> value) {
        this.defaultAgentUnavailableCode = value;
    }

    /**
     * Gets the value of the codeStateList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the codeStateList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodeStateList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallCenterAgentUnavailableCodeStateModify }
     * 
     * 
     * @return
     *     The value of the codeStateList property.
     */
    public List<CallCenterAgentUnavailableCodeStateModify> getCodeStateList() {
        if (codeStateList == null) {
            codeStateList = new ArrayList<>();
        }
        return this.codeStateList;
    }

}
