//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a device family.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemDeviceFamilyModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceFamilyModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="deviceFamilyName" type="{}DeviceFamilyName"/>
 *         <element name="newDeviceFamilyName" type="{}DeviceFamilyName" minOccurs="0"/>
 *         <element name="deviceTypeList" type="{}ReplacementDeviceTypeList" minOccurs="0"/>
 *         <element name="tagSetList" type="{}ReplacementTagSetList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceFamilyModifyRequest", propOrder = {
    "deviceFamilyName",
    "newDeviceFamilyName",
    "deviceTypeList",
    "tagSetList"
})
public class SystemDeviceFamilyModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceFamilyName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newDeviceFamilyName;
    @XmlElementRef(name = "deviceTypeList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementDeviceTypeList> deviceTypeList;
    @XmlElementRef(name = "tagSetList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementTagSetList> tagSetList;

    /**
     * Ruft den Wert der deviceFamilyName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceFamilyName() {
        return deviceFamilyName;
    }

    /**
     * Legt den Wert der deviceFamilyName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceFamilyName(String value) {
        this.deviceFamilyName = value;
    }

    /**
     * Ruft den Wert der newDeviceFamilyName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewDeviceFamilyName() {
        return newDeviceFamilyName;
    }

    /**
     * Legt den Wert der newDeviceFamilyName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewDeviceFamilyName(String value) {
        this.newDeviceFamilyName = value;
    }

    /**
     * Ruft den Wert der deviceTypeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementDeviceTypeList }{@code >}
     *     
     */
    public JAXBElement<ReplacementDeviceTypeList> getDeviceTypeList() {
        return deviceTypeList;
    }

    /**
     * Legt den Wert der deviceTypeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementDeviceTypeList }{@code >}
     *     
     */
    public void setDeviceTypeList(JAXBElement<ReplacementDeviceTypeList> value) {
        this.deviceTypeList = value;
    }

    /**
     * Ruft den Wert der tagSetList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementTagSetList }{@code >}
     *     
     */
    public JAXBElement<ReplacementTagSetList> getTagSetList() {
        return tagSetList;
    }

    /**
     * Legt den Wert der tagSetList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementTagSetList }{@code >}
     *     
     */
    public void setTagSetList(JAXBElement<ReplacementTagSetList> value) {
        this.tagSetList = value;
    }

}
