//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *       Cancel pending and in progress events.  Either all events, multiple
 *       events, or a list of specified events can be canceled.
 *       When specifying multiple events, an event must meet all specified
 *       criteria to be canceled.
 *       The response is either a SuccessResponse or an ErrorResponse.
 *     
 * 
 * <p>Java-Klasse für SystemDeviceManagementInProgressAndPendingEventCancelRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceManagementInProgressAndPendingEventCancelRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <choice>
 *           <element name="cancelAllEvents" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *           <element name="cancelMultipleEvents">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="dmEventStatusForCancel" type="{}DeviceManagementEventStatusForCancel" minOccurs="0"/>
 *                     <element name="dmEventAction" type="{}DeviceManagementEventAction" minOccurs="0"/>
 *                     <element name="dmEventLevel" type="{}DeviceManagementEventLevel" minOccurs="0"/>
 *                     <element name="dmEventType" type="{}DeviceManagementEventType" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *           <element name="eventId" type="{http://www.w3.org/2001/XMLSchema}int" maxOccurs="unbounded"/>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceManagementInProgressAndPendingEventCancelRequest", propOrder = {
    "cancelAllEvents",
    "cancelMultipleEvents",
    "eventId"
})
public class SystemDeviceManagementInProgressAndPendingEventCancelRequest
    extends OCIRequest
{

    protected Object cancelAllEvents;
    protected SystemDeviceManagementInProgressAndPendingEventCancelRequest.CancelMultipleEvents cancelMultipleEvents;
    @XmlElement(type = Integer.class)
    protected List<Integer> eventId;

    /**
     * Ruft den Wert der cancelAllEvents-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getCancelAllEvents() {
        return cancelAllEvents;
    }

    /**
     * Legt den Wert der cancelAllEvents-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setCancelAllEvents(Object value) {
        this.cancelAllEvents = value;
    }

    /**
     * Ruft den Wert der cancelMultipleEvents-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemDeviceManagementInProgressAndPendingEventCancelRequest.CancelMultipleEvents }
     *     
     */
    public SystemDeviceManagementInProgressAndPendingEventCancelRequest.CancelMultipleEvents getCancelMultipleEvents() {
        return cancelMultipleEvents;
    }

    /**
     * Legt den Wert der cancelMultipleEvents-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemDeviceManagementInProgressAndPendingEventCancelRequest.CancelMultipleEvents }
     *     
     */
    public void setCancelMultipleEvents(SystemDeviceManagementInProgressAndPendingEventCancelRequest.CancelMultipleEvents value) {
        this.cancelMultipleEvents = value;
    }

    /**
     * Gets the value of the eventId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the eventId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEventId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     * @return
     *     The value of the eventId property.
     */
    public List<Integer> getEventId() {
        if (eventId == null) {
            eventId = new ArrayList<>();
        }
        return this.eventId;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="dmEventStatusForCancel" type="{}DeviceManagementEventStatusForCancel" minOccurs="0"/>
     *         <element name="dmEventAction" type="{}DeviceManagementEventAction" minOccurs="0"/>
     *         <element name="dmEventLevel" type="{}DeviceManagementEventLevel" minOccurs="0"/>
     *         <element name="dmEventType" type="{}DeviceManagementEventType" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dmEventStatusForCancel",
        "dmEventAction",
        "dmEventLevel",
        "dmEventType"
    })
    public static class CancelMultipleEvents {

        @XmlSchemaType(name = "token")
        protected DeviceManagementEventStatusForCancel dmEventStatusForCancel;
        @XmlSchemaType(name = "token")
        protected DeviceManagementEventAction dmEventAction;
        @XmlSchemaType(name = "token")
        protected DeviceManagementEventLevel dmEventLevel;
        @XmlSchemaType(name = "token")
        protected DeviceManagementEventType dmEventType;

        /**
         * Ruft den Wert der dmEventStatusForCancel-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link DeviceManagementEventStatusForCancel }
         *     
         */
        public DeviceManagementEventStatusForCancel getDmEventStatusForCancel() {
            return dmEventStatusForCancel;
        }

        /**
         * Legt den Wert der dmEventStatusForCancel-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link DeviceManagementEventStatusForCancel }
         *     
         */
        public void setDmEventStatusForCancel(DeviceManagementEventStatusForCancel value) {
            this.dmEventStatusForCancel = value;
        }

        /**
         * Ruft den Wert der dmEventAction-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link DeviceManagementEventAction }
         *     
         */
        public DeviceManagementEventAction getDmEventAction() {
            return dmEventAction;
        }

        /**
         * Legt den Wert der dmEventAction-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link DeviceManagementEventAction }
         *     
         */
        public void setDmEventAction(DeviceManagementEventAction value) {
            this.dmEventAction = value;
        }

        /**
         * Ruft den Wert der dmEventLevel-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link DeviceManagementEventLevel }
         *     
         */
        public DeviceManagementEventLevel getDmEventLevel() {
            return dmEventLevel;
        }

        /**
         * Legt den Wert der dmEventLevel-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link DeviceManagementEventLevel }
         *     
         */
        public void setDmEventLevel(DeviceManagementEventLevel value) {
            this.dmEventLevel = value;
        }

        /**
         * Ruft den Wert der dmEventType-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link DeviceManagementEventType }
         *     
         */
        public DeviceManagementEventType getDmEventType() {
            return dmEventType;
        }

        /**
         * Legt den Wert der dmEventType-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link DeviceManagementEventType }
         *     
         */
        public void setDmEventType(DeviceManagementEventType value) {
            this.dmEventType = value;
        }

    }

}
