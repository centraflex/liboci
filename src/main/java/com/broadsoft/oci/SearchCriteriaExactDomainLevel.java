//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for a particular domain level.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactDomainLevel complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactDomainLevel">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="domainLevel" type="{}DomainLevel"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactDomainLevel", propOrder = {
    "domainLevel"
})
public class SearchCriteriaExactDomainLevel
    extends SearchCriteria
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DomainLevel domainLevel;

    /**
     * Ruft den Wert der domainLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DomainLevel }
     *     
     */
    public DomainLevel getDomainLevel() {
        return domainLevel;
    }

    /**
     * Legt den Wert der domainLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DomainLevel }
     *     
     */
    public void setDomainLevel(DomainLevel value) {
        this.domainLevel = value;
    }

}
