//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *          MWI Delivery To Mobile Endpoint template section associated with a specific tag.
 *       
 * 
 * <p>Java-Klasse für MWIDeliveryToMobileEndpointTemplateLine complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="MWIDeliveryToMobileEndpointTemplateLine">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="prefix" type="{}MWIDeliveryToMobileEndpointTemplatePhrase" minOccurs="0"/>
 *         <element name="tag" type="{}MWIDeliveryToMobileEndpointTemplateTag" minOccurs="0"/>
 *         <element name="postfix" type="{}MWIDeliveryToMobileEndpointTemplatePhrase" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MWIDeliveryToMobileEndpointTemplateLine", propOrder = {
    "prefix",
    "tag",
    "postfix"
})
public class MWIDeliveryToMobileEndpointTemplateLine {

    protected String prefix;
    @XmlSchemaType(name = "token")
    protected MWIDeliveryToMobileEndpointTemplateTag tag;
    protected String postfix;

    /**
     * Ruft den Wert der prefix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Legt den Wert der prefix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrefix(String value) {
        this.prefix = value;
    }

    /**
     * Ruft den Wert der tag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MWIDeliveryToMobileEndpointTemplateTag }
     *     
     */
    public MWIDeliveryToMobileEndpointTemplateTag getTag() {
        return tag;
    }

    /**
     * Legt den Wert der tag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MWIDeliveryToMobileEndpointTemplateTag }
     *     
     */
    public void setTag(MWIDeliveryToMobileEndpointTemplateTag value) {
        this.tag = value;
    }

    /**
     * Ruft den Wert der postfix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostfix() {
        return postfix;
    }

    /**
     * Legt den Wert der postfix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostfix(String value) {
        this.postfix = value;
    }

}
