//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify CPE Config system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *         
 *         The following elements are only used in the AS data mode and ignored in the XS data mode:
 *           allowDeviceCredentialsRetrieval
 *       
 * 
 * <p>Java-Klasse für SystemCPEConfigParametersModifyRequest21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCPEConfigParametersModifyRequest21">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enableIPDeviceManagement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="ftpConnectTimeoutSeconds" type="{}DeviceManagementFTPConnectTimeoutSeconds" minOccurs="0"/>
 *         <element name="ftpFileTransferTimeoutSeconds" type="{}DeviceManagementFTPFileTransferTimeoutSeconds" minOccurs="0"/>
 *         <element name="pauseBetweenFileRebuildMilliseconds" type="{}DeviceManagementPauseBetweenFileRebuildMilliseconds" minOccurs="0"/>
 *         <element name="deviceAccessAppServerClusterName" type="{}NetAddress" minOccurs="0"/>
 *         <element name="minTimeBetweenResetMilliseconds" type="{}DeviceManagementMinTimeBetweenResetMilliseconds" minOccurs="0"/>
 *         <element name="alwaysPushFilesOnRebuild" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxFileOperationRetryAttempts" type="{}DeviceManagementFileOperationRetryAttempts" minOccurs="0"/>
 *         <element name="enableAutoRebuildConfig" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="eventQueueSize" type="{}DeviceManagementEventQueueSize" minOccurs="0"/>
 *         <element name="allowDeviceCredentialsRetrieval" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCPEConfigParametersModifyRequest21", propOrder = {
    "enableIPDeviceManagement",
    "ftpConnectTimeoutSeconds",
    "ftpFileTransferTimeoutSeconds",
    "pauseBetweenFileRebuildMilliseconds",
    "deviceAccessAppServerClusterName",
    "minTimeBetweenResetMilliseconds",
    "alwaysPushFilesOnRebuild",
    "maxFileOperationRetryAttempts",
    "enableAutoRebuildConfig",
    "eventQueueSize",
    "allowDeviceCredentialsRetrieval"
})
public class SystemCPEConfigParametersModifyRequest21
    extends OCIRequest
{

    protected Boolean enableIPDeviceManagement;
    protected Integer ftpConnectTimeoutSeconds;
    protected Integer ftpFileTransferTimeoutSeconds;
    protected Integer pauseBetweenFileRebuildMilliseconds;
    @XmlElementRef(name = "deviceAccessAppServerClusterName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deviceAccessAppServerClusterName;
    protected Integer minTimeBetweenResetMilliseconds;
    protected Boolean alwaysPushFilesOnRebuild;
    protected Integer maxFileOperationRetryAttempts;
    protected Boolean enableAutoRebuildConfig;
    protected Integer eventQueueSize;
    protected Boolean allowDeviceCredentialsRetrieval;

    /**
     * Ruft den Wert der enableIPDeviceManagement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableIPDeviceManagement() {
        return enableIPDeviceManagement;
    }

    /**
     * Legt den Wert der enableIPDeviceManagement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableIPDeviceManagement(Boolean value) {
        this.enableIPDeviceManagement = value;
    }

    /**
     * Ruft den Wert der ftpConnectTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFtpConnectTimeoutSeconds() {
        return ftpConnectTimeoutSeconds;
    }

    /**
     * Legt den Wert der ftpConnectTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFtpConnectTimeoutSeconds(Integer value) {
        this.ftpConnectTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der ftpFileTransferTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFtpFileTransferTimeoutSeconds() {
        return ftpFileTransferTimeoutSeconds;
    }

    /**
     * Legt den Wert der ftpFileTransferTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFtpFileTransferTimeoutSeconds(Integer value) {
        this.ftpFileTransferTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der pauseBetweenFileRebuildMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPauseBetweenFileRebuildMilliseconds() {
        return pauseBetweenFileRebuildMilliseconds;
    }

    /**
     * Legt den Wert der pauseBetweenFileRebuildMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPauseBetweenFileRebuildMilliseconds(Integer value) {
        this.pauseBetweenFileRebuildMilliseconds = value;
    }

    /**
     * Ruft den Wert der deviceAccessAppServerClusterName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeviceAccessAppServerClusterName() {
        return deviceAccessAppServerClusterName;
    }

    /**
     * Legt den Wert der deviceAccessAppServerClusterName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeviceAccessAppServerClusterName(JAXBElement<String> value) {
        this.deviceAccessAppServerClusterName = value;
    }

    /**
     * Ruft den Wert der minTimeBetweenResetMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinTimeBetweenResetMilliseconds() {
        return minTimeBetweenResetMilliseconds;
    }

    /**
     * Legt den Wert der minTimeBetweenResetMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinTimeBetweenResetMilliseconds(Integer value) {
        this.minTimeBetweenResetMilliseconds = value;
    }

    /**
     * Ruft den Wert der alwaysPushFilesOnRebuild-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlwaysPushFilesOnRebuild() {
        return alwaysPushFilesOnRebuild;
    }

    /**
     * Legt den Wert der alwaysPushFilesOnRebuild-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlwaysPushFilesOnRebuild(Boolean value) {
        this.alwaysPushFilesOnRebuild = value;
    }

    /**
     * Ruft den Wert der maxFileOperationRetryAttempts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxFileOperationRetryAttempts() {
        return maxFileOperationRetryAttempts;
    }

    /**
     * Legt den Wert der maxFileOperationRetryAttempts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxFileOperationRetryAttempts(Integer value) {
        this.maxFileOperationRetryAttempts = value;
    }

    /**
     * Ruft den Wert der enableAutoRebuildConfig-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableAutoRebuildConfig() {
        return enableAutoRebuildConfig;
    }

    /**
     * Legt den Wert der enableAutoRebuildConfig-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableAutoRebuildConfig(Boolean value) {
        this.enableAutoRebuildConfig = value;
    }

    /**
     * Ruft den Wert der eventQueueSize-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEventQueueSize() {
        return eventQueueSize;
    }

    /**
     * Legt den Wert der eventQueueSize-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEventQueueSize(Integer value) {
        this.eventQueueSize = value;
    }

    /**
     * Ruft den Wert der allowDeviceCredentialsRetrieval-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowDeviceCredentialsRetrieval() {
        return allowDeviceCredentialsRetrieval;
    }

    /**
     * Legt den Wert der allowDeviceCredentialsRetrieval-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowDeviceCredentialsRetrieval(Boolean value) {
        this.allowDeviceCredentialsRetrieval = value;
    }

}
