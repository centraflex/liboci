//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SystemLicenseType22.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SystemLicenseType22">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Lawful Intercept Event Monitoring"/>
 *     <enumeration value="Lawful Intercept Media Monitoring"/>
 *     <enumeration value="Enterprise Voice Portal"/>
 *     <enumeration value="Service Packs"/>
 *     <enumeration value="SIP TCP"/>
 *     <enumeration value="Realtime Accounting"/>
 *     <enumeration value="Network-Wide Messaging"/>
 *     <enumeration value="Sh Interface"/>
 *     <enumeration value="Destination Trunk Group"/>
 *     <enumeration value="Deployment Studio"/>
 *     <enumeration value="Session Admission Control"/>
 *     <enumeration value="Session Data Replication"/>
 *     <enumeration value="BroadWorks Mobile Manager"/>
 *     <enumeration value="System Voice Portal"/>
 *     <enumeration value="Number Portability Query"/>
 *     <enumeration value="Deutsche Telekom hPBX Proprietary"/>
 *     <enumeration value="CLID Delivery Prefix"/>
 *     <enumeration value="Automatic Collect Call System"/>
 *     <enumeration value="Cloud PBX"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SystemLicenseType22")
@XmlEnum
public enum SystemLicenseType22 {

    @XmlEnumValue("Lawful Intercept Event Monitoring")
    LAWFUL_INTERCEPT_EVENT_MONITORING("Lawful Intercept Event Monitoring"),
    @XmlEnumValue("Lawful Intercept Media Monitoring")
    LAWFUL_INTERCEPT_MEDIA_MONITORING("Lawful Intercept Media Monitoring"),
    @XmlEnumValue("Enterprise Voice Portal")
    ENTERPRISE_VOICE_PORTAL("Enterprise Voice Portal"),
    @XmlEnumValue("Service Packs")
    SERVICE_PACKS("Service Packs"),
    @XmlEnumValue("SIP TCP")
    SIP_TCP("SIP TCP"),
    @XmlEnumValue("Realtime Accounting")
    REALTIME_ACCOUNTING("Realtime Accounting"),
    @XmlEnumValue("Network-Wide Messaging")
    NETWORK_WIDE_MESSAGING("Network-Wide Messaging"),
    @XmlEnumValue("Sh Interface")
    SH_INTERFACE("Sh Interface"),
    @XmlEnumValue("Destination Trunk Group")
    DESTINATION_TRUNK_GROUP("Destination Trunk Group"),
    @XmlEnumValue("Deployment Studio")
    DEPLOYMENT_STUDIO("Deployment Studio"),
    @XmlEnumValue("Session Admission Control")
    SESSION_ADMISSION_CONTROL("Session Admission Control"),
    @XmlEnumValue("Session Data Replication")
    SESSION_DATA_REPLICATION("Session Data Replication"),
    @XmlEnumValue("BroadWorks Mobile Manager")
    BROAD_WORKS_MOBILE_MANAGER("BroadWorks Mobile Manager"),
    @XmlEnumValue("System Voice Portal")
    SYSTEM_VOICE_PORTAL("System Voice Portal"),
    @XmlEnumValue("Number Portability Query")
    NUMBER_PORTABILITY_QUERY("Number Portability Query"),
    @XmlEnumValue("Deutsche Telekom hPBX Proprietary")
    DEUTSCHE_TELEKOM_H_PBX_PROPRIETARY("Deutsche Telekom hPBX Proprietary"),
    @XmlEnumValue("CLID Delivery Prefix")
    CLID_DELIVERY_PREFIX("CLID Delivery Prefix"),
    @XmlEnumValue("Automatic Collect Call System")
    AUTOMATIC_COLLECT_CALL_SYSTEM("Automatic Collect Call System"),
    @XmlEnumValue("Cloud PBX")
    CLOUD_PBX("Cloud PBX");
    private final String value;

    SystemLicenseType22(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SystemLicenseType22 fromValue(String v) {
        for (SystemLicenseType22 c: SystemLicenseType22 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
