//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Get a list of Communication Barring Authorization Codes for a group.
 *         The response is either GroupCommunicationBarringAuthorizationCodeGetListResponse21sp1 or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCommunicationBarringAuthorizationCodeGetListRequest21sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCommunicationBarringAuthorizationCodeGetListRequest21sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaCommunicationBarringAuthorizationCode" type="{}SearchCriteriaCommunicationBarringAuthorizationCode" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaCommunicationBarringAuthorizationCodeDescription" type="{}SearchCriteriaCommunicationBarringAuthorizationCodeDescription" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaNetworkClassOfServiceName" type="{}SearchCriteriaNetworkClassOfServiceName" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCommunicationBarringAuthorizationCodeGetListRequest21sp1", propOrder = {
    "serviceProviderId",
    "groupId",
    "responseSizeLimit",
    "searchCriteriaCommunicationBarringAuthorizationCode",
    "searchCriteriaCommunicationBarringAuthorizationCodeDescription",
    "searchCriteriaNetworkClassOfServiceName"
})
public class GroupCommunicationBarringAuthorizationCodeGetListRequest21Sp1
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected Integer responseSizeLimit;
    protected List<SearchCriteriaCommunicationBarringAuthorizationCode> searchCriteriaCommunicationBarringAuthorizationCode;
    protected List<SearchCriteriaCommunicationBarringAuthorizationCodeDescription> searchCriteriaCommunicationBarringAuthorizationCodeDescription;
    protected List<SearchCriteriaNetworkClassOfServiceName> searchCriteriaNetworkClassOfServiceName;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaCommunicationBarringAuthorizationCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaCommunicationBarringAuthorizationCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaCommunicationBarringAuthorizationCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaCommunicationBarringAuthorizationCode }
     * 
     * 
     * @return
     *     The value of the searchCriteriaCommunicationBarringAuthorizationCode property.
     */
    public List<SearchCriteriaCommunicationBarringAuthorizationCode> getSearchCriteriaCommunicationBarringAuthorizationCode() {
        if (searchCriteriaCommunicationBarringAuthorizationCode == null) {
            searchCriteriaCommunicationBarringAuthorizationCode = new ArrayList<>();
        }
        return this.searchCriteriaCommunicationBarringAuthorizationCode;
    }

    /**
     * Gets the value of the searchCriteriaCommunicationBarringAuthorizationCodeDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaCommunicationBarringAuthorizationCodeDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaCommunicationBarringAuthorizationCodeDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaCommunicationBarringAuthorizationCodeDescription }
     * 
     * 
     * @return
     *     The value of the searchCriteriaCommunicationBarringAuthorizationCodeDescription property.
     */
    public List<SearchCriteriaCommunicationBarringAuthorizationCodeDescription> getSearchCriteriaCommunicationBarringAuthorizationCodeDescription() {
        if (searchCriteriaCommunicationBarringAuthorizationCodeDescription == null) {
            searchCriteriaCommunicationBarringAuthorizationCodeDescription = new ArrayList<>();
        }
        return this.searchCriteriaCommunicationBarringAuthorizationCodeDescription;
    }

    /**
     * Gets the value of the searchCriteriaNetworkClassOfServiceName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaNetworkClassOfServiceName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaNetworkClassOfServiceName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaNetworkClassOfServiceName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaNetworkClassOfServiceName property.
     */
    public List<SearchCriteriaNetworkClassOfServiceName> getSearchCriteriaNetworkClassOfServiceName() {
        if (searchCriteriaNetworkClassOfServiceName == null) {
            searchCriteriaNetworkClassOfServiceName = new ArrayList<>();
        }
        return this.searchCriteriaNetworkClassOfServiceName;
    }

}
