//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Verification Translation and Routing parameters
 *         for creating a Verify Translation and Routing request from
 *         parameters.
 *       
 * 
 * <p>Java-Klasse für VerifyTranslationAndRoutingParameters complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="VerifyTranslationAndRoutingParameters">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="origination" type="{}VerifyTranslationAndRoutingOrigination"/>
 *         <element name="destination" type="{}VerifyTranslationAndRoutingDestination"/>
 *         <element name="contact" type="{}URL" minOccurs="0"/>
 *         <element name="diversion" type="{}URL" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifyTranslationAndRoutingParameters", propOrder = {
    "origination",
    "destination",
    "contact",
    "diversion"
})
public class VerifyTranslationAndRoutingParameters {

    @XmlElement(required = true)
    protected VerifyTranslationAndRoutingOrigination origination;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String destination;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String contact;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String diversion;

    /**
     * Ruft den Wert der origination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VerifyTranslationAndRoutingOrigination }
     *     
     */
    public VerifyTranslationAndRoutingOrigination getOrigination() {
        return origination;
    }

    /**
     * Legt den Wert der origination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VerifyTranslationAndRoutingOrigination }
     *     
     */
    public void setOrigination(VerifyTranslationAndRoutingOrigination value) {
        this.origination = value;
    }

    /**
     * Ruft den Wert der destination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Legt den Wert der destination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestination(String value) {
        this.destination = value;
    }

    /**
     * Ruft den Wert der contact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContact() {
        return contact;
    }

    /**
     * Legt den Wert der contact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContact(String value) {
        this.contact = value;
    }

    /**
     * Ruft den Wert der diversion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiversion() {
        return diversion;
    }

    /**
     * Legt den Wert der diversion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiversion(String value) {
        this.diversion = value;
    }

}
