//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Asserted"/>
 *     <enumeration value="Presentation"/>
 *     <enumeration value="Asserted Or Presentation"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy")
@XmlEnum
public enum TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy {

    @XmlEnumValue("Asserted")
    ASSERTED("Asserted"),
    @XmlEnumValue("Presentation")
    PRESENTATION("Presentation"),
    @XmlEnumValue("Asserted Or Presentation")
    ASSERTED_OR_PRESENTATION("Asserted Or Presentation");
    private final String value;

    TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy fromValue(String v) {
        for (TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy c: TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
