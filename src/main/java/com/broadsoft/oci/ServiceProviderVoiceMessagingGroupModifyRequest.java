//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a service provider's or enterprise's voice messaging settings.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderVoiceMessagingGroupModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderVoiceMessagingGroupModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="deliveryFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="notificationFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="voicePortalLockoutFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="useSystemDefaultDeliveryFromAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useSystemDefaultNotificationFromAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useSystemDefaultVoicePortalLockoutFromAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderVoiceMessagingGroupModifyRequest", propOrder = {
    "serviceProviderId",
    "deliveryFromAddress",
    "notificationFromAddress",
    "voicePortalLockoutFromAddress",
    "useSystemDefaultDeliveryFromAddress",
    "useSystemDefaultNotificationFromAddress",
    "useSystemDefaultVoicePortalLockoutFromAddress"
})
public class ServiceProviderVoiceMessagingGroupModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElementRef(name = "deliveryFromAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deliveryFromAddress;
    @XmlElementRef(name = "notificationFromAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> notificationFromAddress;
    @XmlElementRef(name = "voicePortalLockoutFromAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> voicePortalLockoutFromAddress;
    protected Boolean useSystemDefaultDeliveryFromAddress;
    protected Boolean useSystemDefaultNotificationFromAddress;
    protected Boolean useSystemDefaultVoicePortalLockoutFromAddress;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der deliveryFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeliveryFromAddress() {
        return deliveryFromAddress;
    }

    /**
     * Legt den Wert der deliveryFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeliveryFromAddress(JAXBElement<String> value) {
        this.deliveryFromAddress = value;
    }

    /**
     * Ruft den Wert der notificationFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNotificationFromAddress() {
        return notificationFromAddress;
    }

    /**
     * Legt den Wert der notificationFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNotificationFromAddress(JAXBElement<String> value) {
        this.notificationFromAddress = value;
    }

    /**
     * Ruft den Wert der voicePortalLockoutFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVoicePortalLockoutFromAddress() {
        return voicePortalLockoutFromAddress;
    }

    /**
     * Legt den Wert der voicePortalLockoutFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVoicePortalLockoutFromAddress(JAXBElement<String> value) {
        this.voicePortalLockoutFromAddress = value;
    }

    /**
     * Ruft den Wert der useSystemDefaultDeliveryFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseSystemDefaultDeliveryFromAddress() {
        return useSystemDefaultDeliveryFromAddress;
    }

    /**
     * Legt den Wert der useSystemDefaultDeliveryFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSystemDefaultDeliveryFromAddress(Boolean value) {
        this.useSystemDefaultDeliveryFromAddress = value;
    }

    /**
     * Ruft den Wert der useSystemDefaultNotificationFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseSystemDefaultNotificationFromAddress() {
        return useSystemDefaultNotificationFromAddress;
    }

    /**
     * Legt den Wert der useSystemDefaultNotificationFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSystemDefaultNotificationFromAddress(Boolean value) {
        this.useSystemDefaultNotificationFromAddress = value;
    }

    /**
     * Ruft den Wert der useSystemDefaultVoicePortalLockoutFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseSystemDefaultVoicePortalLockoutFromAddress() {
        return useSystemDefaultVoicePortalLockoutFromAddress;
    }

    /**
     * Legt den Wert der useSystemDefaultVoicePortalLockoutFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSystemDefaultVoicePortalLockoutFromAddress(Boolean value) {
        this.useSystemDefaultVoicePortalLockoutFromAddress = value;
    }

}
