//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupAdminGetPagedSortedListRequest.
 *         Contains a 8 column table with column headings "Administrator ID",
 *         "Last Name", "First Name", "Administrator Type", "Department",
 *         "Language", "Locale" and "Encoding".  
 *         The following columns are only returned in AS data mode:
 *         "Locale" and "Encoding".
 *       
 * 
 * <p>Java-Klasse für GroupAdminGetPagedSortedListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAdminGetPagedSortedListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="groupAdminTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAdminGetPagedSortedListResponse", propOrder = {
    "groupAdminTable"
})
public class GroupAdminGetPagedSortedListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable groupAdminTable;

    /**
     * Ruft den Wert der groupAdminTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getGroupAdminTable() {
        return groupAdminTable;
    }

    /**
     * Legt den Wert der groupAdminTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setGroupAdminTable(OCITable value) {
        this.groupAdminTable = value;
    }

}
