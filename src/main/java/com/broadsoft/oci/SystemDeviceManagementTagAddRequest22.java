//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to add a static configuration tag.
 *         
 *         The following elements are only used in XS data mode and ignored in AS data mode:
 *           isTagValueEncrypted
 *           tagValueToEncrypt
 *         
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemDeviceManagementTagAddRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceManagementTagAddRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <choice>
 *           <element name="systemDefaultTagSet" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *           <element name="tagSetName" type="{}DeviceManagementTagSetName"/>
 *         </choice>
 *         <element name="tagName" type="{}DeviceManagementTagName"/>
 *         <element name="isTagValueEncrypted" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <choice>
 *           <element name="tagValue" type="{}DeviceManagementTagValue" minOccurs="0"/>
 *           <element name="tagValueToEncrypt" type="{}DeviceManagementTagValue" minOccurs="0"/>
 *         </choice>
 *         <element name="isTagValueOverridable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceManagementTagAddRequest22", propOrder = {
    "systemDefaultTagSet",
    "tagSetName",
    "tagName",
    "isTagValueEncrypted",
    "tagValue",
    "tagValueToEncrypt",
    "isTagValueOverridable"
})
public class SystemDeviceManagementTagAddRequest22
    extends OCIRequest
{

    protected Boolean systemDefaultTagSet;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String tagSetName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String tagName;
    protected boolean isTagValueEncrypted;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String tagValue;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String tagValueToEncrypt;
    protected boolean isTagValueOverridable;

    /**
     * Ruft den Wert der systemDefaultTagSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSystemDefaultTagSet() {
        return systemDefaultTagSet;
    }

    /**
     * Legt den Wert der systemDefaultTagSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSystemDefaultTagSet(Boolean value) {
        this.systemDefaultTagSet = value;
    }

    /**
     * Ruft den Wert der tagSetName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTagSetName() {
        return tagSetName;
    }

    /**
     * Legt den Wert der tagSetName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTagSetName(String value) {
        this.tagSetName = value;
    }

    /**
     * Ruft den Wert der tagName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTagName() {
        return tagName;
    }

    /**
     * Legt den Wert der tagName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTagName(String value) {
        this.tagName = value;
    }

    /**
     * Ruft den Wert der isTagValueEncrypted-Eigenschaft ab.
     * 
     */
    public boolean isIsTagValueEncrypted() {
        return isTagValueEncrypted;
    }

    /**
     * Legt den Wert der isTagValueEncrypted-Eigenschaft fest.
     * 
     */
    public void setIsTagValueEncrypted(boolean value) {
        this.isTagValueEncrypted = value;
    }

    /**
     * Ruft den Wert der tagValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTagValue() {
        return tagValue;
    }

    /**
     * Legt den Wert der tagValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTagValue(String value) {
        this.tagValue = value;
    }

    /**
     * Ruft den Wert der tagValueToEncrypt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTagValueToEncrypt() {
        return tagValueToEncrypt;
    }

    /**
     * Legt den Wert der tagValueToEncrypt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTagValueToEncrypt(String value) {
        this.tagValueToEncrypt = value;
    }

    /**
     * Ruft den Wert der isTagValueOverridable-Eigenschaft ab.
     * 
     */
    public boolean isIsTagValueOverridable() {
        return isTagValueOverridable;
    }

    /**
     * Legt den Wert der isTagValueOverridable-Eigenschaft fest.
     * 
     */
    public void setIsTagValueOverridable(boolean value) {
        this.isTagValueOverridable = value;
    }

}
