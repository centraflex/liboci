//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Contains Call Center statistics for a specified agent.
 *       
 * 
 * <p>Java-Klasse für CallCenterAgentStatistics14sp9 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterAgentStatistics14sp9">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="agentUserId" type="{}UserId"/>
 *         <element name="agentDisplayNames" type="{}UserDisplayNames"/>
 *         <element name="available" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="statistics" type="{}AgentStatistics"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterAgentStatistics14sp9", propOrder = {
    "agentUserId",
    "agentDisplayNames",
    "available",
    "statistics"
})
public class CallCenterAgentStatistics14Sp9 {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String agentUserId;
    @XmlElement(required = true)
    protected UserDisplayNames agentDisplayNames;
    protected boolean available;
    @XmlElement(required = true)
    protected AgentStatistics statistics;

    /**
     * Ruft den Wert der agentUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentUserId() {
        return agentUserId;
    }

    /**
     * Legt den Wert der agentUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentUserId(String value) {
        this.agentUserId = value;
    }

    /**
     * Ruft den Wert der agentDisplayNames-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserDisplayNames }
     *     
     */
    public UserDisplayNames getAgentDisplayNames() {
        return agentDisplayNames;
    }

    /**
     * Legt den Wert der agentDisplayNames-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDisplayNames }
     *     
     */
    public void setAgentDisplayNames(UserDisplayNames value) {
        this.agentDisplayNames = value;
    }

    /**
     * Ruft den Wert der available-Eigenschaft ab.
     * 
     */
    public boolean isAvailable() {
        return available;
    }

    /**
     * Legt den Wert der available-Eigenschaft fest.
     * 
     */
    public void setAvailable(boolean value) {
        this.available = value;
    }

    /**
     * Ruft den Wert der statistics-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AgentStatistics }
     *     
     */
    public AgentStatistics getStatistics() {
        return statistics;
    }

    /**
     * Legt den Wert der statistics-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentStatistics }
     *     
     */
    public void setStatistics(AgentStatistics value) {
        this.statistics = value;
    }

}
