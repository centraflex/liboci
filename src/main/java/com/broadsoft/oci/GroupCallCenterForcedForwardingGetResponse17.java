//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupCallCenterForcedForwardingGetRequest17.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterForcedForwardingGetResponse17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterForcedForwardingGetResponse17">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forwardToPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="allowEnableViaFAC" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="playAnnouncementBeforeForwarding" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="audioMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="audioUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="audioFileList" type="{}CallCenterAnnouncementDescriptionList" minOccurs="0"/>
 *         <element name="audioMediaTypeList" type="{}CallCenterAnnouncementMediaFileTypeList" minOccurs="0"/>
 *         <element name="videoMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="videoUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="videoFileList" type="{}CallCenterAnnouncementDescriptionList" minOccurs="0"/>
 *         <element name="videoMediaTypeList" type="{}CallCenterAnnouncementMediaFileTypeList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterForcedForwardingGetResponse17", propOrder = {
    "isActive",
    "forwardToPhoneNumber",
    "allowEnableViaFAC",
    "playAnnouncementBeforeForwarding",
    "audioMessageSelection",
    "audioUrlList",
    "audioFileList",
    "audioMediaTypeList",
    "videoMessageSelection",
    "videoUrlList",
    "videoFileList",
    "videoMediaTypeList"
})
public class GroupCallCenterForcedForwardingGetResponse17
    extends OCIDataResponse
{

    protected boolean isActive;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String forwardToPhoneNumber;
    protected boolean allowEnableViaFAC;
    protected boolean playAnnouncementBeforeForwarding;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection audioMessageSelection;
    protected CallCenterAnnouncementURLList audioUrlList;
    protected CallCenterAnnouncementDescriptionList audioFileList;
    protected CallCenterAnnouncementMediaFileTypeList audioMediaTypeList;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection videoMessageSelection;
    protected CallCenterAnnouncementURLList videoUrlList;
    protected CallCenterAnnouncementDescriptionList videoFileList;
    protected CallCenterAnnouncementMediaFileTypeList videoMediaTypeList;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der forwardToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForwardToPhoneNumber() {
        return forwardToPhoneNumber;
    }

    /**
     * Legt den Wert der forwardToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForwardToPhoneNumber(String value) {
        this.forwardToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der allowEnableViaFAC-Eigenschaft ab.
     * 
     */
    public boolean isAllowEnableViaFAC() {
        return allowEnableViaFAC;
    }

    /**
     * Legt den Wert der allowEnableViaFAC-Eigenschaft fest.
     * 
     */
    public void setAllowEnableViaFAC(boolean value) {
        this.allowEnableViaFAC = value;
    }

    /**
     * Ruft den Wert der playAnnouncementBeforeForwarding-Eigenschaft ab.
     * 
     */
    public boolean isPlayAnnouncementBeforeForwarding() {
        return playAnnouncementBeforeForwarding;
    }

    /**
     * Legt den Wert der playAnnouncementBeforeForwarding-Eigenschaft fest.
     * 
     */
    public void setPlayAnnouncementBeforeForwarding(boolean value) {
        this.playAnnouncementBeforeForwarding = value;
    }

    /**
     * Ruft den Wert der audioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getAudioMessageSelection() {
        return audioMessageSelection;
    }

    /**
     * Legt den Wert der audioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.audioMessageSelection = value;
    }

    /**
     * Ruft den Wert der audioUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getAudioUrlList() {
        return audioUrlList;
    }

    /**
     * Legt den Wert der audioUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setAudioUrlList(CallCenterAnnouncementURLList value) {
        this.audioUrlList = value;
    }

    /**
     * Ruft den Wert der audioFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public CallCenterAnnouncementDescriptionList getAudioFileList() {
        return audioFileList;
    }

    /**
     * Legt den Wert der audioFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public void setAudioFileList(CallCenterAnnouncementDescriptionList value) {
        this.audioFileList = value;
    }

    /**
     * Ruft den Wert der audioMediaTypeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public CallCenterAnnouncementMediaFileTypeList getAudioMediaTypeList() {
        return audioMediaTypeList;
    }

    /**
     * Legt den Wert der audioMediaTypeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public void setAudioMediaTypeList(CallCenterAnnouncementMediaFileTypeList value) {
        this.audioMediaTypeList = value;
    }

    /**
     * Ruft den Wert der videoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getVideoMessageSelection() {
        return videoMessageSelection;
    }

    /**
     * Legt den Wert der videoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.videoMessageSelection = value;
    }

    /**
     * Ruft den Wert der videoUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getVideoUrlList() {
        return videoUrlList;
    }

    /**
     * Legt den Wert der videoUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setVideoUrlList(CallCenterAnnouncementURLList value) {
        this.videoUrlList = value;
    }

    /**
     * Ruft den Wert der videoFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public CallCenterAnnouncementDescriptionList getVideoFileList() {
        return videoFileList;
    }

    /**
     * Legt den Wert der videoFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public void setVideoFileList(CallCenterAnnouncementDescriptionList value) {
        this.videoFileList = value;
    }

    /**
     * Ruft den Wert der videoMediaTypeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public CallCenterAnnouncementMediaFileTypeList getVideoMediaTypeList() {
        return videoMediaTypeList;
    }

    /**
     * Legt den Wert der videoMediaTypeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public void setVideoMediaTypeList(CallCenterAnnouncementMediaFileTypeList value) {
        this.videoMediaTypeList = value;
    }

}
