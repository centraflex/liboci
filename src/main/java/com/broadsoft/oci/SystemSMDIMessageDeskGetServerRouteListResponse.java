//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemSMDIMessageDeskGetServerRouteListRequest.
 *         The SMDI Server route table column headings are: "Destination" and "SMDI Servers".
 *       
 * 
 * <p>Java-Klasse für SystemSMDIMessageDeskGetServerRouteListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSMDIMessageDeskGetServerRouteListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="smdiServerRouteTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSMDIMessageDeskGetServerRouteListResponse", propOrder = {
    "smdiServerRouteTable"
})
public class SystemSMDIMessageDeskGetServerRouteListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable smdiServerRouteTable;

    /**
     * Ruft den Wert der smdiServerRouteTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getSmdiServerRouteTable() {
        return smdiServerRouteTable;
    }

    /**
     * Legt den Wert der smdiServerRouteTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setSmdiServerRouteTable(OCITable value) {
        this.smdiServerRouteTable = value;
    }

}
