//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupRoutePointGetFailoverPolicyRequest.
 *       
 * 
 * <p>Java-Klasse für GroupRoutePointGetFailoverPolicyResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupRoutePointGetFailoverPolicyResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableFailoverSupport" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="externalSystem" type="{}RoutePointExternalSystem" minOccurs="0"/>
 *         <element name="failoverPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="failoverStatus" type="{}RoutePointFailoverStatus"/>
 *         <element name="perCallEnableFailoverSupport" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="perCallCallFailureTimeoutSeconds" type="{}RoutePointCallFailureTimeout"/>
 *         <element name="perCallOutboundCallFailureTimeoutSeconds" type="{}RoutePointCallFailureTimeout"/>
 *         <element name="perCallFailoverPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupRoutePointGetFailoverPolicyResponse", propOrder = {
    "enableFailoverSupport",
    "externalSystem",
    "failoverPhoneNumber",
    "failoverStatus",
    "perCallEnableFailoverSupport",
    "perCallCallFailureTimeoutSeconds",
    "perCallOutboundCallFailureTimeoutSeconds",
    "perCallFailoverPhoneNumber"
})
public class GroupRoutePointGetFailoverPolicyResponse
    extends OCIDataResponse
{

    protected boolean enableFailoverSupport;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String externalSystem;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String failoverPhoneNumber;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected RoutePointFailoverStatus failoverStatus;
    protected boolean perCallEnableFailoverSupport;
    protected int perCallCallFailureTimeoutSeconds;
    protected int perCallOutboundCallFailureTimeoutSeconds;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String perCallFailoverPhoneNumber;

    /**
     * Ruft den Wert der enableFailoverSupport-Eigenschaft ab.
     * 
     */
    public boolean isEnableFailoverSupport() {
        return enableFailoverSupport;
    }

    /**
     * Legt den Wert der enableFailoverSupport-Eigenschaft fest.
     * 
     */
    public void setEnableFailoverSupport(boolean value) {
        this.enableFailoverSupport = value;
    }

    /**
     * Ruft den Wert der externalSystem-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalSystem() {
        return externalSystem;
    }

    /**
     * Legt den Wert der externalSystem-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalSystem(String value) {
        this.externalSystem = value;
    }

    /**
     * Ruft den Wert der failoverPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFailoverPhoneNumber() {
        return failoverPhoneNumber;
    }

    /**
     * Legt den Wert der failoverPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFailoverPhoneNumber(String value) {
        this.failoverPhoneNumber = value;
    }

    /**
     * Ruft den Wert der failoverStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RoutePointFailoverStatus }
     *     
     */
    public RoutePointFailoverStatus getFailoverStatus() {
        return failoverStatus;
    }

    /**
     * Legt den Wert der failoverStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RoutePointFailoverStatus }
     *     
     */
    public void setFailoverStatus(RoutePointFailoverStatus value) {
        this.failoverStatus = value;
    }

    /**
     * Ruft den Wert der perCallEnableFailoverSupport-Eigenschaft ab.
     * 
     */
    public boolean isPerCallEnableFailoverSupport() {
        return perCallEnableFailoverSupport;
    }

    /**
     * Legt den Wert der perCallEnableFailoverSupport-Eigenschaft fest.
     * 
     */
    public void setPerCallEnableFailoverSupport(boolean value) {
        this.perCallEnableFailoverSupport = value;
    }

    /**
     * Ruft den Wert der perCallCallFailureTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getPerCallCallFailureTimeoutSeconds() {
        return perCallCallFailureTimeoutSeconds;
    }

    /**
     * Legt den Wert der perCallCallFailureTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setPerCallCallFailureTimeoutSeconds(int value) {
        this.perCallCallFailureTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der perCallOutboundCallFailureTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getPerCallOutboundCallFailureTimeoutSeconds() {
        return perCallOutboundCallFailureTimeoutSeconds;
    }

    /**
     * Legt den Wert der perCallOutboundCallFailureTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setPerCallOutboundCallFailureTimeoutSeconds(int value) {
        this.perCallOutboundCallFailureTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der perCallFailoverPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerCallFailoverPhoneNumber() {
        return perCallFailoverPhoneNumber;
    }

    /**
     * Legt den Wert der perCallFailoverPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerCallFailoverPhoneNumber(String value) {
        this.perCallFailoverPhoneNumber = value;
    }

}
