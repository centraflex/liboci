//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to EnterpriseVoiceVPNGetDefaultResponse.
 *       
 * 
 * <p>Java-Klasse für EnterpriseVoiceVPNGetDefaultResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseVoiceVPNGetDefaultResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="policySelection" type="{}EnterpriseVoiceVPNPolicySelection" maxOccurs="4" minOccurs="4"/>
 *         <element name="digitManipulationOperation" type="{}EnterpriseVoiceVPNDigitManipulationOperation" maxOccurs="12" minOccurs="12"/>
 *         <element name="routeGroupId" type="{}GroupId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="treatment" type="{}EnterpriseVoiceVPNTreatmentEntry" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseVoiceVPNGetDefaultResponse", propOrder = {
    "policySelection",
    "digitManipulationOperation",
    "routeGroupId",
    "treatment"
})
public class EnterpriseVoiceVPNGetDefaultResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected List<EnterpriseVoiceVPNPolicySelection> policySelection;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected List<EnterpriseVoiceVPNDigitManipulationOperation> digitManipulationOperation;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> routeGroupId;
    protected List<EnterpriseVoiceVPNTreatmentEntry> treatment;

    /**
     * Gets the value of the policySelection property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the policySelection property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicySelection().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnterpriseVoiceVPNPolicySelection }
     * 
     * 
     * @return
     *     The value of the policySelection property.
     */
    public List<EnterpriseVoiceVPNPolicySelection> getPolicySelection() {
        if (policySelection == null) {
            policySelection = new ArrayList<>();
        }
        return this.policySelection;
    }

    /**
     * Gets the value of the digitManipulationOperation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the digitManipulationOperation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDigitManipulationOperation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnterpriseVoiceVPNDigitManipulationOperation }
     * 
     * 
     * @return
     *     The value of the digitManipulationOperation property.
     */
    public List<EnterpriseVoiceVPNDigitManipulationOperation> getDigitManipulationOperation() {
        if (digitManipulationOperation == null) {
            digitManipulationOperation = new ArrayList<>();
        }
        return this.digitManipulationOperation;
    }

    /**
     * Gets the value of the routeGroupId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the routeGroupId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRouteGroupId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the routeGroupId property.
     */
    public List<String> getRouteGroupId() {
        if (routeGroupId == null) {
            routeGroupId = new ArrayList<>();
        }
        return this.routeGroupId;
    }

    /**
     * Gets the value of the treatment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the treatment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTreatment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnterpriseVoiceVPNTreatmentEntry }
     * 
     * 
     * @return
     *     The value of the treatment property.
     */
    public List<EnterpriseVoiceVPNTreatmentEntry> getTreatment() {
        if (treatment == null) {
            treatment = new ArrayList<>();
        }
        return this.treatment;
    }

}
