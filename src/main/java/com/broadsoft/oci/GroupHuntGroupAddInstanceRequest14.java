//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a Hunt Group instance to a group.
 *         The domain is required in the serviceUserId.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupHuntGroupAddInstanceRequest14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupHuntGroupAddInstanceRequest14">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceAddProfile"/>
 *         <element name="policy" type="{}HuntPolicy"/>
 *         <element name="huntAfterNoAnswer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="noAnswerNumberOfRings" type="{}HuntNoAnswerRings"/>
 *         <element name="forwardAfterTimeout" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forwardTimeoutSeconds" type="{}HuntForwardTimeoutSeconds"/>
 *         <element name="forwardToPhoneNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="agentUserId" type="{}UserId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="allowCallWaitingForAgents" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupHuntGroupAddInstanceRequest14", propOrder = {
    "serviceProviderId",
    "groupId",
    "serviceUserId",
    "serviceInstanceProfile",
    "policy",
    "huntAfterNoAnswer",
    "noAnswerNumberOfRings",
    "forwardAfterTimeout",
    "forwardTimeoutSeconds",
    "forwardToPhoneNumber",
    "agentUserId",
    "allowCallWaitingForAgents"
})
public class GroupHuntGroupAddInstanceRequest14
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlElement(required = true)
    protected ServiceInstanceAddProfile serviceInstanceProfile;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected HuntPolicy policy;
    protected boolean huntAfterNoAnswer;
    protected int noAnswerNumberOfRings;
    protected boolean forwardAfterTimeout;
    protected int forwardTimeoutSeconds;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String forwardToPhoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> agentUserId;
    protected boolean allowCallWaitingForAgents;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceAddProfile }
     *     
     */
    public ServiceInstanceAddProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceAddProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceAddProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der policy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HuntPolicy }
     *     
     */
    public HuntPolicy getPolicy() {
        return policy;
    }

    /**
     * Legt den Wert der policy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HuntPolicy }
     *     
     */
    public void setPolicy(HuntPolicy value) {
        this.policy = value;
    }

    /**
     * Ruft den Wert der huntAfterNoAnswer-Eigenschaft ab.
     * 
     */
    public boolean isHuntAfterNoAnswer() {
        return huntAfterNoAnswer;
    }

    /**
     * Legt den Wert der huntAfterNoAnswer-Eigenschaft fest.
     * 
     */
    public void setHuntAfterNoAnswer(boolean value) {
        this.huntAfterNoAnswer = value;
    }

    /**
     * Ruft den Wert der noAnswerNumberOfRings-Eigenschaft ab.
     * 
     */
    public int getNoAnswerNumberOfRings() {
        return noAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der noAnswerNumberOfRings-Eigenschaft fest.
     * 
     */
    public void setNoAnswerNumberOfRings(int value) {
        this.noAnswerNumberOfRings = value;
    }

    /**
     * Ruft den Wert der forwardAfterTimeout-Eigenschaft ab.
     * 
     */
    public boolean isForwardAfterTimeout() {
        return forwardAfterTimeout;
    }

    /**
     * Legt den Wert der forwardAfterTimeout-Eigenschaft fest.
     * 
     */
    public void setForwardAfterTimeout(boolean value) {
        this.forwardAfterTimeout = value;
    }

    /**
     * Ruft den Wert der forwardTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getForwardTimeoutSeconds() {
        return forwardTimeoutSeconds;
    }

    /**
     * Legt den Wert der forwardTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setForwardTimeoutSeconds(int value) {
        this.forwardTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der forwardToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForwardToPhoneNumber() {
        return forwardToPhoneNumber;
    }

    /**
     * Legt den Wert der forwardToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForwardToPhoneNumber(String value) {
        this.forwardToPhoneNumber = value;
    }

    /**
     * Gets the value of the agentUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the agentUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgentUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the agentUserId property.
     */
    public List<String> getAgentUserId() {
        if (agentUserId == null) {
            agentUserId = new ArrayList<>();
        }
        return this.agentUserId;
    }

    /**
     * Ruft den Wert der allowCallWaitingForAgents-Eigenschaft ab.
     * 
     */
    public boolean isAllowCallWaitingForAgents() {
        return allowCallWaitingForAgents;
    }

    /**
     * Legt den Wert der allowCallWaitingForAgents-Eigenschaft fest.
     * 
     */
    public void setAllowCallWaitingForAgents(boolean value) {
        this.allowCallWaitingForAgents = value;
    }

}
