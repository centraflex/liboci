//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupCallCenterQueueThresholdsGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterQueueThresholdsGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterQueueThresholdsGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="thresholdCurrentCallsInQueueYellow" type="{}CallCenterQueueThresholdCurrentCallsInQueue" minOccurs="0"/>
 *         <element name="thresholdCurrentCallsInQueueRed" type="{}CallCenterQueueThresholdCurrentCallsInQueue" minOccurs="0"/>
 *         <element name="thresholdCurrentLongestWaitingCallYellow" type="{}CallCenterQueueThresholdCurrentLongestWaitingCallsTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdCurrentLongestWaitingCallRed" type="{}CallCenterQueueThresholdCurrentLongestWaitingCallsTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageEstimatedWaitTimeYellow" type="{}CallCenterQueueThresholdAverageEstimatedWaitTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageEstimatedWaitTimeRed" type="{}CallCenterQueueThresholdAverageEstimatedWaitTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageHandlingTimeYellow" type="{}CallCenterQueueThresholdAverageHandlingTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageHandlingTimeRed" type="{}CallCenterQueueThresholdAverageHandlingTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageSpeedOfAnswerYellow" type="{}CallCenterQueueThresholdAverageSpeedOfAnswerTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageSpeedOfAnswerRed" type="{}CallCenterQueueThresholdAverageSpeedOfAnswerTimeSeconds" minOccurs="0"/>
 *         <element name="enableNotificationEmail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="notificationEmailAddress" type="{}EmailAddress" maxOccurs="8" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterQueueThresholdsGetResponse", propOrder = {
    "isActive",
    "thresholdCurrentCallsInQueueYellow",
    "thresholdCurrentCallsInQueueRed",
    "thresholdCurrentLongestWaitingCallYellow",
    "thresholdCurrentLongestWaitingCallRed",
    "thresholdAverageEstimatedWaitTimeYellow",
    "thresholdAverageEstimatedWaitTimeRed",
    "thresholdAverageHandlingTimeYellow",
    "thresholdAverageHandlingTimeRed",
    "thresholdAverageSpeedOfAnswerYellow",
    "thresholdAverageSpeedOfAnswerRed",
    "enableNotificationEmail",
    "notificationEmailAddress"
})
public class GroupCallCenterQueueThresholdsGetResponse
    extends OCIDataResponse
{

    protected boolean isActive;
    protected Integer thresholdCurrentCallsInQueueYellow;
    protected Integer thresholdCurrentCallsInQueueRed;
    protected Integer thresholdCurrentLongestWaitingCallYellow;
    protected Integer thresholdCurrentLongestWaitingCallRed;
    protected Integer thresholdAverageEstimatedWaitTimeYellow;
    protected Integer thresholdAverageEstimatedWaitTimeRed;
    protected Integer thresholdAverageHandlingTimeYellow;
    protected Integer thresholdAverageHandlingTimeRed;
    protected Integer thresholdAverageSpeedOfAnswerYellow;
    protected Integer thresholdAverageSpeedOfAnswerRed;
    protected boolean enableNotificationEmail;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> notificationEmailAddress;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentCallsInQueueYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdCurrentCallsInQueueYellow() {
        return thresholdCurrentCallsInQueueYellow;
    }

    /**
     * Legt den Wert der thresholdCurrentCallsInQueueYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdCurrentCallsInQueueYellow(Integer value) {
        this.thresholdCurrentCallsInQueueYellow = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentCallsInQueueRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdCurrentCallsInQueueRed() {
        return thresholdCurrentCallsInQueueRed;
    }

    /**
     * Legt den Wert der thresholdCurrentCallsInQueueRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdCurrentCallsInQueueRed(Integer value) {
        this.thresholdCurrentCallsInQueueRed = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentLongestWaitingCallYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdCurrentLongestWaitingCallYellow() {
        return thresholdCurrentLongestWaitingCallYellow;
    }

    /**
     * Legt den Wert der thresholdCurrentLongestWaitingCallYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdCurrentLongestWaitingCallYellow(Integer value) {
        this.thresholdCurrentLongestWaitingCallYellow = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentLongestWaitingCallRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdCurrentLongestWaitingCallRed() {
        return thresholdCurrentLongestWaitingCallRed;
    }

    /**
     * Legt den Wert der thresholdCurrentLongestWaitingCallRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdCurrentLongestWaitingCallRed(Integer value) {
        this.thresholdCurrentLongestWaitingCallRed = value;
    }

    /**
     * Ruft den Wert der thresholdAverageEstimatedWaitTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdAverageEstimatedWaitTimeYellow() {
        return thresholdAverageEstimatedWaitTimeYellow;
    }

    /**
     * Legt den Wert der thresholdAverageEstimatedWaitTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdAverageEstimatedWaitTimeYellow(Integer value) {
        this.thresholdAverageEstimatedWaitTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdAverageEstimatedWaitTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdAverageEstimatedWaitTimeRed() {
        return thresholdAverageEstimatedWaitTimeRed;
    }

    /**
     * Legt den Wert der thresholdAverageEstimatedWaitTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdAverageEstimatedWaitTimeRed(Integer value) {
        this.thresholdAverageEstimatedWaitTimeRed = value;
    }

    /**
     * Ruft den Wert der thresholdAverageHandlingTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdAverageHandlingTimeYellow() {
        return thresholdAverageHandlingTimeYellow;
    }

    /**
     * Legt den Wert der thresholdAverageHandlingTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdAverageHandlingTimeYellow(Integer value) {
        this.thresholdAverageHandlingTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdAverageHandlingTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdAverageHandlingTimeRed() {
        return thresholdAverageHandlingTimeRed;
    }

    /**
     * Legt den Wert der thresholdAverageHandlingTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdAverageHandlingTimeRed(Integer value) {
        this.thresholdAverageHandlingTimeRed = value;
    }

    /**
     * Ruft den Wert der thresholdAverageSpeedOfAnswerYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdAverageSpeedOfAnswerYellow() {
        return thresholdAverageSpeedOfAnswerYellow;
    }

    /**
     * Legt den Wert der thresholdAverageSpeedOfAnswerYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdAverageSpeedOfAnswerYellow(Integer value) {
        this.thresholdAverageSpeedOfAnswerYellow = value;
    }

    /**
     * Ruft den Wert der thresholdAverageSpeedOfAnswerRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdAverageSpeedOfAnswerRed() {
        return thresholdAverageSpeedOfAnswerRed;
    }

    /**
     * Legt den Wert der thresholdAverageSpeedOfAnswerRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdAverageSpeedOfAnswerRed(Integer value) {
        this.thresholdAverageSpeedOfAnswerRed = value;
    }

    /**
     * Ruft den Wert der enableNotificationEmail-Eigenschaft ab.
     * 
     */
    public boolean isEnableNotificationEmail() {
        return enableNotificationEmail;
    }

    /**
     * Legt den Wert der enableNotificationEmail-Eigenschaft fest.
     * 
     */
    public void setEnableNotificationEmail(boolean value) {
        this.enableNotificationEmail = value;
    }

    /**
     * Gets the value of the notificationEmailAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the notificationEmailAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNotificationEmailAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the notificationEmailAddress property.
     */
    public List<String> getNotificationEmailAddress() {
        if (notificationEmailAddress == null) {
            notificationEmailAddress = new ArrayList<>();
        }
        return this.notificationEmailAddress;
    }

}
