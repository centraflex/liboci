//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterEventRecordingCallCenterEventMode.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterEventRecordingCallCenterEventMode">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Call Center Event Recording"/>
 *     <enumeration value="Legacy ECCR"/>
 *     <enumeration value="Both"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterEventRecordingCallCenterEventMode")
@XmlEnum
public enum CallCenterEventRecordingCallCenterEventMode {

    @XmlEnumValue("Call Center Event Recording")
    CALL_CENTER_EVENT_RECORDING("Call Center Event Recording"),
    @XmlEnumValue("Legacy ECCR")
    LEGACY_ECCR("Legacy ECCR"),
    @XmlEnumValue("Both")
    BOTH("Both");
    private final String value;

    CallCenterEventRecordingCallCenterEventMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterEventRecordingCallCenterEventMode fromValue(String v) {
        for (CallCenterEventRecordingCallCenterEventMode c: CallCenterEventRecordingCallCenterEventMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
