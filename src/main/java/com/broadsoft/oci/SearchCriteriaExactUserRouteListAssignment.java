//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for users with Route List feature assignment.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactUserRouteListAssignment complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactUserRouteListAssignment">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="assigned" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactUserRouteListAssignment", propOrder = {
    "assigned"
})
public class SearchCriteriaExactUserRouteListAssignment
    extends SearchCriteria
{

    protected boolean assigned;

    /**
     * Ruft den Wert der assigned-Eigenschaft ab.
     * 
     */
    public boolean isAssigned() {
        return assigned;
    }

    /**
     * Legt den Wert der assigned-Eigenschaft fest.
     * 
     */
    public void setAssigned(boolean value) {
        this.assigned = value;
    }

}
