//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemGETSResourcePriorityGetListRequest. 
 *         The table columns are: "Priority Value", "Priority Level" and "Priority Class".
 *       
 * 
 * <p>Java-Klasse für SystemGETSResourcePriorityGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGETSResourcePriorityGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="resourcePriorityTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGETSResourcePriorityGetListResponse", propOrder = {
    "resourcePriorityTable"
})
public class SystemGETSResourcePriorityGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable resourcePriorityTable;

    /**
     * Ruft den Wert der resourcePriorityTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getResourcePriorityTable() {
        return resourcePriorityTable;
    }

    /**
     * Legt den Wert der resourcePriorityTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setResourcePriorityTable(OCITable value) {
        this.resourcePriorityTable = value;
    }

}
