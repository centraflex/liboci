//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemPushNotificationParametersGetRequest.
 *         
 *         The following elements are only used in AS data mode:
 *           subscriptionEventsPerSecond, value "100" is returned in Amplify data mode.
 *           
 *         Contains a list of system push notification parameters.
 *       
 * 
 * <p>Java-Klasse für SystemPushNotificationParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPushNotificationParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enforceAllowedApplicationList" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maximumRegistrationsPerUser" type="{}PushNotificationMaximumRegistrationsPerUser"/>
 *         <element name="maximumRegistrationAgeDays" type="{}PushNotificationMaximumRegistrationAgeDays"/>
 *         <element name="newCallTimeout" type="{}PushNotificationNewCallTimeout"/>
 *         <element name="subscriptionEventsPerSecond" type="{}PushNotificationSubscriptionEventsPerSecond"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPushNotificationParametersGetResponse", propOrder = {
    "enforceAllowedApplicationList",
    "maximumRegistrationsPerUser",
    "maximumRegistrationAgeDays",
    "newCallTimeout",
    "subscriptionEventsPerSecond"
})
public class SystemPushNotificationParametersGetResponse
    extends OCIDataResponse
{

    protected boolean enforceAllowedApplicationList;
    protected int maximumRegistrationsPerUser;
    protected int maximumRegistrationAgeDays;
    protected int newCallTimeout;
    protected int subscriptionEventsPerSecond;

    /**
     * Ruft den Wert der enforceAllowedApplicationList-Eigenschaft ab.
     * 
     */
    public boolean isEnforceAllowedApplicationList() {
        return enforceAllowedApplicationList;
    }

    /**
     * Legt den Wert der enforceAllowedApplicationList-Eigenschaft fest.
     * 
     */
    public void setEnforceAllowedApplicationList(boolean value) {
        this.enforceAllowedApplicationList = value;
    }

    /**
     * Ruft den Wert der maximumRegistrationsPerUser-Eigenschaft ab.
     * 
     */
    public int getMaximumRegistrationsPerUser() {
        return maximumRegistrationsPerUser;
    }

    /**
     * Legt den Wert der maximumRegistrationsPerUser-Eigenschaft fest.
     * 
     */
    public void setMaximumRegistrationsPerUser(int value) {
        this.maximumRegistrationsPerUser = value;
    }

    /**
     * Ruft den Wert der maximumRegistrationAgeDays-Eigenschaft ab.
     * 
     */
    public int getMaximumRegistrationAgeDays() {
        return maximumRegistrationAgeDays;
    }

    /**
     * Legt den Wert der maximumRegistrationAgeDays-Eigenschaft fest.
     * 
     */
    public void setMaximumRegistrationAgeDays(int value) {
        this.maximumRegistrationAgeDays = value;
    }

    /**
     * Ruft den Wert der newCallTimeout-Eigenschaft ab.
     * 
     */
    public int getNewCallTimeout() {
        return newCallTimeout;
    }

    /**
     * Legt den Wert der newCallTimeout-Eigenschaft fest.
     * 
     */
    public void setNewCallTimeout(int value) {
        this.newCallTimeout = value;
    }

    /**
     * Ruft den Wert der subscriptionEventsPerSecond-Eigenschaft ab.
     * 
     */
    public int getSubscriptionEventsPerSecond() {
        return subscriptionEventsPerSecond;
    }

    /**
     * Legt den Wert der subscriptionEventsPerSecond-Eigenschaft fest.
     * 
     */
    public void setSubscriptionEventsPerSecond(int value) {
        this.subscriptionEventsPerSecond = value;
    }

}
