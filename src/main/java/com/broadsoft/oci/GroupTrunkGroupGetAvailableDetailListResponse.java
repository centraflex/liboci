//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupTrunkGroupGetAvailableDetailListRequest.
 *       
 * 
 * <p>Java-Klasse für GroupTrunkGroupGetAvailableDetailListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupTrunkGroupGetAvailableDetailListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="trunkGroup" maxOccurs="unbounded" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="name" type="{}TrunkGroupDeviceName"/>
 *                   <element name="staticRegistrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   <element name="useDomain" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupTrunkGroupGetAvailableDetailListResponse", propOrder = {
    "trunkGroup"
})
public class GroupTrunkGroupGetAvailableDetailListResponse
    extends OCIDataResponse
{

    protected List<GroupTrunkGroupGetAvailableDetailListResponse.TrunkGroup> trunkGroup;

    /**
     * Gets the value of the trunkGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the trunkGroup property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTrunkGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupTrunkGroupGetAvailableDetailListResponse.TrunkGroup }
     * 
     * 
     * @return
     *     The value of the trunkGroup property.
     */
    public List<GroupTrunkGroupGetAvailableDetailListResponse.TrunkGroup> getTrunkGroup() {
        if (trunkGroup == null) {
            trunkGroup = new ArrayList<>();
        }
        return this.trunkGroup;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="name" type="{}TrunkGroupDeviceName"/>
     *         <element name="staticRegistrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         <element name="useDomain" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "name",
        "staticRegistrationCapable",
        "useDomain"
    })
    public static class TrunkGroup {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String name;
        protected boolean staticRegistrationCapable;
        protected boolean useDomain;

        /**
         * Ruft den Wert der name-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Legt den Wert der name-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Ruft den Wert der staticRegistrationCapable-Eigenschaft ab.
         * 
         */
        public boolean isStaticRegistrationCapable() {
            return staticRegistrationCapable;
        }

        /**
         * Legt den Wert der staticRegistrationCapable-Eigenschaft fest.
         * 
         */
        public void setStaticRegistrationCapable(boolean value) {
            this.staticRegistrationCapable = value;
        }

        /**
         * Ruft den Wert der useDomain-Eigenschaft ab.
         * 
         */
        public boolean isUseDomain() {
            return useDomain;
        }

        /**
         * Legt den Wert der useDomain-Eigenschaft fest.
         * 
         */
        public void setUseDomain(boolean value) {
            this.useDomain = value;
        }

    }

}
