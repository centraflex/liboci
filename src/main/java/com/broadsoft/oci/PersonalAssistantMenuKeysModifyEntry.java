//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal personal assistant menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für PersonalAssistantMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="PersonalAssistantMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="setPresenceToNone" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToBusinessTrip" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToGoneForTheDay" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToLunch" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToMeeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToOutOfOffice" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToTemporarilyOut" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToTraining" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToUnavailable" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setPresenceToVacation" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonalAssistantMenuKeysModifyEntry", propOrder = {
    "setPresenceToNone",
    "setPresenceToBusinessTrip",
    "setPresenceToGoneForTheDay",
    "setPresenceToLunch",
    "setPresenceToMeeting",
    "setPresenceToOutOfOffice",
    "setPresenceToTemporarilyOut",
    "setPresenceToTraining",
    "setPresenceToUnavailable",
    "setPresenceToVacation",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class PersonalAssistantMenuKeysModifyEntry {

    @XmlElementRef(name = "setPresenceToNone", type = JAXBElement.class, required = false)
    protected JAXBElement<String> setPresenceToNone;
    @XmlElementRef(name = "setPresenceToBusinessTrip", type = JAXBElement.class, required = false)
    protected JAXBElement<String> setPresenceToBusinessTrip;
    @XmlElementRef(name = "setPresenceToGoneForTheDay", type = JAXBElement.class, required = false)
    protected JAXBElement<String> setPresenceToGoneForTheDay;
    @XmlElementRef(name = "setPresenceToLunch", type = JAXBElement.class, required = false)
    protected JAXBElement<String> setPresenceToLunch;
    @XmlElementRef(name = "setPresenceToMeeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> setPresenceToMeeting;
    @XmlElementRef(name = "setPresenceToOutOfOffice", type = JAXBElement.class, required = false)
    protected JAXBElement<String> setPresenceToOutOfOffice;
    @XmlElementRef(name = "setPresenceToTemporarilyOut", type = JAXBElement.class, required = false)
    protected JAXBElement<String> setPresenceToTemporarilyOut;
    @XmlElementRef(name = "setPresenceToTraining", type = JAXBElement.class, required = false)
    protected JAXBElement<String> setPresenceToTraining;
    @XmlElementRef(name = "setPresenceToUnavailable", type = JAXBElement.class, required = false)
    protected JAXBElement<String> setPresenceToUnavailable;
    @XmlElementRef(name = "setPresenceToVacation", type = JAXBElement.class, required = false)
    protected JAXBElement<String> setPresenceToVacation;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repeatMenu;

    /**
     * Ruft den Wert der setPresenceToNone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSetPresenceToNone() {
        return setPresenceToNone;
    }

    /**
     * Legt den Wert der setPresenceToNone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSetPresenceToNone(JAXBElement<String> value) {
        this.setPresenceToNone = value;
    }

    /**
     * Ruft den Wert der setPresenceToBusinessTrip-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSetPresenceToBusinessTrip() {
        return setPresenceToBusinessTrip;
    }

    /**
     * Legt den Wert der setPresenceToBusinessTrip-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSetPresenceToBusinessTrip(JAXBElement<String> value) {
        this.setPresenceToBusinessTrip = value;
    }

    /**
     * Ruft den Wert der setPresenceToGoneForTheDay-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSetPresenceToGoneForTheDay() {
        return setPresenceToGoneForTheDay;
    }

    /**
     * Legt den Wert der setPresenceToGoneForTheDay-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSetPresenceToGoneForTheDay(JAXBElement<String> value) {
        this.setPresenceToGoneForTheDay = value;
    }

    /**
     * Ruft den Wert der setPresenceToLunch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSetPresenceToLunch() {
        return setPresenceToLunch;
    }

    /**
     * Legt den Wert der setPresenceToLunch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSetPresenceToLunch(JAXBElement<String> value) {
        this.setPresenceToLunch = value;
    }

    /**
     * Ruft den Wert der setPresenceToMeeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSetPresenceToMeeting() {
        return setPresenceToMeeting;
    }

    /**
     * Legt den Wert der setPresenceToMeeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSetPresenceToMeeting(JAXBElement<String> value) {
        this.setPresenceToMeeting = value;
    }

    /**
     * Ruft den Wert der setPresenceToOutOfOffice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSetPresenceToOutOfOffice() {
        return setPresenceToOutOfOffice;
    }

    /**
     * Legt den Wert der setPresenceToOutOfOffice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSetPresenceToOutOfOffice(JAXBElement<String> value) {
        this.setPresenceToOutOfOffice = value;
    }

    /**
     * Ruft den Wert der setPresenceToTemporarilyOut-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSetPresenceToTemporarilyOut() {
        return setPresenceToTemporarilyOut;
    }

    /**
     * Legt den Wert der setPresenceToTemporarilyOut-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSetPresenceToTemporarilyOut(JAXBElement<String> value) {
        this.setPresenceToTemporarilyOut = value;
    }

    /**
     * Ruft den Wert der setPresenceToTraining-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSetPresenceToTraining() {
        return setPresenceToTraining;
    }

    /**
     * Legt den Wert der setPresenceToTraining-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSetPresenceToTraining(JAXBElement<String> value) {
        this.setPresenceToTraining = value;
    }

    /**
     * Ruft den Wert der setPresenceToUnavailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSetPresenceToUnavailable() {
        return setPresenceToUnavailable;
    }

    /**
     * Legt den Wert der setPresenceToUnavailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSetPresenceToUnavailable(JAXBElement<String> value) {
        this.setPresenceToUnavailable = value;
    }

    /**
     * Ruft den Wert der setPresenceToVacation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSetPresenceToVacation() {
        return setPresenceToVacation;
    }

    /**
     * Legt den Wert der setPresenceToVacation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSetPresenceToVacation(JAXBElement<String> value) {
        this.setPresenceToVacation = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepeatMenu(JAXBElement<String> value) {
        this.repeatMenu = value;
    }

}
