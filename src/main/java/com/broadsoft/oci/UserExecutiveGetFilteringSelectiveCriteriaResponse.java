//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserExecutiveGetFilteringSelectiveCriteriaRequest.
 *         Replaced by: UserExecutiveGetFilteringSelectiveCriteriaResponse21
 *       
 * 
 * <p>Java-Klasse für UserExecutiveGetFilteringSelectiveCriteriaResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserExecutiveGetFilteringSelectiveCriteriaResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="timeSchedule" type="{}TimeSchedule" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}HolidaySchedule" minOccurs="0"/>
 *         <element name="filter" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="fromDnCriteria" type="{}ExecutiveCallFilteringCriteriaFromDn"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserExecutiveGetFilteringSelectiveCriteriaResponse", propOrder = {
    "timeSchedule",
    "holidaySchedule",
    "filter",
    "fromDnCriteria"
})
public class UserExecutiveGetFilteringSelectiveCriteriaResponse
    extends OCIDataResponse
{

    protected TimeSchedule timeSchedule;
    protected HolidaySchedule holidaySchedule;
    protected boolean filter;
    @XmlElement(required = true)
    protected ExecutiveCallFilteringCriteriaFromDn fromDnCriteria;

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeSchedule }
     *     
     */
    public TimeSchedule getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeSchedule }
     *     
     */
    public void setTimeSchedule(TimeSchedule value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HolidaySchedule }
     *     
     */
    public HolidaySchedule getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HolidaySchedule }
     *     
     */
    public void setHolidaySchedule(HolidaySchedule value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der filter-Eigenschaft ab.
     * 
     */
    public boolean isFilter() {
        return filter;
    }

    /**
     * Legt den Wert der filter-Eigenschaft fest.
     * 
     */
    public void setFilter(boolean value) {
        this.filter = value;
    }

    /**
     * Ruft den Wert der fromDnCriteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveCallFilteringCriteriaFromDn }
     *     
     */
    public ExecutiveCallFilteringCriteriaFromDn getFromDnCriteria() {
        return fromDnCriteria;
    }

    /**
     * Legt den Wert der fromDnCriteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveCallFilteringCriteriaFromDn }
     *     
     */
    public void setFromDnCriteria(ExecutiveCallFilteringCriteriaFromDn value) {
        this.fromDnCriteria = value;
    }

}
