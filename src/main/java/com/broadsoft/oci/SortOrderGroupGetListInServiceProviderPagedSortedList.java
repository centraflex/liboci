//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Used to sort the GroupGetListInServiceProviderPagedSortedListRequest request.
 *       
 * 
 * <p>Java-Klasse für SortOrderGroupGetListInServiceProviderPagedSortedList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SortOrderGroupGetListInServiceProviderPagedSortedList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <choice>
 *         <element name="sortByGroupId" type="{}SortByGroupId"/>
 *         <element name="sortByGroupName" type="{}SortByGroupName"/>
 *       </choice>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SortOrderGroupGetListInServiceProviderPagedSortedList", propOrder = {
    "sortByGroupId",
    "sortByGroupName"
})
public class SortOrderGroupGetListInServiceProviderPagedSortedList {

    protected SortByGroupId sortByGroupId;
    protected SortByGroupName sortByGroupName;

    /**
     * Ruft den Wert der sortByGroupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByGroupId }
     *     
     */
    public SortByGroupId getSortByGroupId() {
        return sortByGroupId;
    }

    /**
     * Legt den Wert der sortByGroupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByGroupId }
     *     
     */
    public void setSortByGroupId(SortByGroupId value) {
        this.sortByGroupId = value;
    }

    /**
     * Ruft den Wert der sortByGroupName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByGroupName }
     *     
     */
    public SortByGroupName getSortByGroupName() {
        return sortByGroupName;
    }

    /**
     * Legt den Wert der sortByGroupName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByGroupName }
     *     
     */
    public void setSortByGroupName(SortByGroupName value) {
        this.sortByGroupName = value;
    }

}
