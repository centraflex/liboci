//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a specified system access device.
 *         The response is either a SuccessResponse or an ErrorResponse.
 * 
 *         The following elements are only used in AS data mode and ignored in XS data mode: 
 *           isWebexTeamsDevice
 *           defaultPathHeader          
 *           deviceExternalId
 *       
 * 
 * <p>Java-Klasse für SystemAccessDeviceModifyRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAccessDeviceModifyRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <choice>
 *           <element name="deviceName" type="{}AccessDeviceName"/>
 *           <element name="deviceExternalId" type="{}ExternalId"/>
 *         </choice>
 *         <element name="protocol" type="{}AccessDeviceProtocol" minOccurs="0"/>
 *         <element name="netAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="port" type="{}Port1025" minOccurs="0"/>
 *         <element name="outboundProxyServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="stunServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="macAddress" type="{}AccessDeviceMACAddress" minOccurs="0"/>
 *         <element name="serialNumber" type="{}AccessDeviceSerialNumber" minOccurs="0"/>
 *         <element name="description" type="{}AccessDeviceDescription" minOccurs="0"/>
 *         <element name="physicalLocation" type="{}AccessDevicePhysicalLocation" minOccurs="0"/>
 *         <element name="transportProtocol" type="{}ExtendedTransportProtocol" minOccurs="0"/>
 *         <element name="useCustomUserNamePassword" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="accessDeviceCredentials" type="{}DeviceManagementUserNamePassword16" minOccurs="0"/>
 *         <element name="isWebexTeamsDevice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="defaultPathHeader" type="{}PathHeader" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAccessDeviceModifyRequest22", propOrder = {
    "deviceName",
    "deviceExternalId",
    "protocol",
    "netAddress",
    "port",
    "outboundProxyServerNetAddress",
    "stunServerNetAddress",
    "macAddress",
    "serialNumber",
    "description",
    "physicalLocation",
    "transportProtocol",
    "useCustomUserNamePassword",
    "accessDeviceCredentials",
    "isWebexTeamsDevice",
    "defaultPathHeader"
})
public class SystemAccessDeviceModifyRequest22
    extends OCIRequest
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceExternalId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String protocol;
    @XmlElementRef(name = "netAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> netAddress;
    @XmlElementRef(name = "port", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> port;
    @XmlElementRef(name = "outboundProxyServerNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> outboundProxyServerNetAddress;
    @XmlElementRef(name = "stunServerNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> stunServerNetAddress;
    @XmlElementRef(name = "macAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> macAddress;
    @XmlElementRef(name = "serialNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serialNumber;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "physicalLocation", type = JAXBElement.class, required = false)
    protected JAXBElement<String> physicalLocation;
    @XmlSchemaType(name = "token")
    protected ExtendedTransportProtocol transportProtocol;
    protected Boolean useCustomUserNamePassword;
    @XmlElementRef(name = "accessDeviceCredentials", type = JAXBElement.class, required = false)
    protected JAXBElement<DeviceManagementUserNamePassword16> accessDeviceCredentials;
    protected Boolean isWebexTeamsDevice;
    @XmlElementRef(name = "defaultPathHeader", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultPathHeader;

    /**
     * Ruft den Wert der deviceName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * Legt den Wert der deviceName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceName(String value) {
        this.deviceName = value;
    }

    /**
     * Ruft den Wert der deviceExternalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceExternalId() {
        return deviceExternalId;
    }

    /**
     * Legt den Wert der deviceExternalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceExternalId(String value) {
        this.deviceExternalId = value;
    }

    /**
     * Ruft den Wert der protocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocol() {
        return protocol;
    }

    /**
     * Legt den Wert der protocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocol(String value) {
        this.protocol = value;
    }

    /**
     * Ruft den Wert der netAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNetAddress() {
        return netAddress;
    }

    /**
     * Legt den Wert der netAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNetAddress(JAXBElement<String> value) {
        this.netAddress = value;
    }

    /**
     * Ruft den Wert der port-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getPort() {
        return port;
    }

    /**
     * Legt den Wert der port-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setPort(JAXBElement<Integer> value) {
        this.port = value;
    }

    /**
     * Ruft den Wert der outboundProxyServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOutboundProxyServerNetAddress() {
        return outboundProxyServerNetAddress;
    }

    /**
     * Legt den Wert der outboundProxyServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOutboundProxyServerNetAddress(JAXBElement<String> value) {
        this.outboundProxyServerNetAddress = value;
    }

    /**
     * Ruft den Wert der stunServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getStunServerNetAddress() {
        return stunServerNetAddress;
    }

    /**
     * Legt den Wert der stunServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setStunServerNetAddress(JAXBElement<String> value) {
        this.stunServerNetAddress = value;
    }

    /**
     * Ruft den Wert der macAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMacAddress() {
        return macAddress;
    }

    /**
     * Legt den Wert der macAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMacAddress(JAXBElement<String> value) {
        this.macAddress = value;
    }

    /**
     * Ruft den Wert der serialNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSerialNumber() {
        return serialNumber;
    }

    /**
     * Legt den Wert der serialNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSerialNumber(JAXBElement<String> value) {
        this.serialNumber = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der physicalLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPhysicalLocation() {
        return physicalLocation;
    }

    /**
     * Legt den Wert der physicalLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPhysicalLocation(JAXBElement<String> value) {
        this.physicalLocation = value;
    }

    /**
     * Ruft den Wert der transportProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedTransportProtocol }
     *     
     */
    public ExtendedTransportProtocol getTransportProtocol() {
        return transportProtocol;
    }

    /**
     * Legt den Wert der transportProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedTransportProtocol }
     *     
     */
    public void setTransportProtocol(ExtendedTransportProtocol value) {
        this.transportProtocol = value;
    }

    /**
     * Ruft den Wert der useCustomUserNamePassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseCustomUserNamePassword() {
        return useCustomUserNamePassword;
    }

    /**
     * Legt den Wert der useCustomUserNamePassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseCustomUserNamePassword(Boolean value) {
        this.useCustomUserNamePassword = value;
    }

    /**
     * Ruft den Wert der accessDeviceCredentials-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DeviceManagementUserNamePassword16 }{@code >}
     *     
     */
    public JAXBElement<DeviceManagementUserNamePassword16> getAccessDeviceCredentials() {
        return accessDeviceCredentials;
    }

    /**
     * Legt den Wert der accessDeviceCredentials-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DeviceManagementUserNamePassword16 }{@code >}
     *     
     */
    public void setAccessDeviceCredentials(JAXBElement<DeviceManagementUserNamePassword16> value) {
        this.accessDeviceCredentials = value;
    }

    /**
     * Ruft den Wert der isWebexTeamsDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsWebexTeamsDevice() {
        return isWebexTeamsDevice;
    }

    /**
     * Legt den Wert der isWebexTeamsDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsWebexTeamsDevice(Boolean value) {
        this.isWebexTeamsDevice = value;
    }

    /**
     * Ruft den Wert der defaultPathHeader-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultPathHeader() {
        return defaultPathHeader;
    }

    /**
     * Legt den Wert der defaultPathHeader-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultPathHeader(JAXBElement<String> value) {
        this.defaultPathHeader = value;
    }

}
