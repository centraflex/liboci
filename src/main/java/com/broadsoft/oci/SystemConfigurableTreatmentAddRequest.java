//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a configurable treatment.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemConfigurableTreatmentAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemConfigurableTreatmentAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="treatmentId" type="{}TreatmentId"/>
 *         <element name="chargeIndicator" type="{}ChargeIndicator"/>
 *         <element name="description" type="{}ConfigurableTreatmentDescription" minOccurs="0"/>
 *         <element name="accessSIPStatusCode" type="{}SIPFailureStatusCode" minOccurs="0"/>
 *         <element name="accessSIPStatusMessage" type="{}SIPStatusMessage" minOccurs="0"/>
 *         <element name="networkSIPStatusCode" type="{}SIPFailureStatusCode" minOccurs="0"/>
 *         <element name="networkSIPStatusMessage" type="{}SIPStatusMessage" minOccurs="0"/>
 *         <element name="q850CauseValue" type="{}Q850CauseValue" minOccurs="0"/>
 *         <element name="q850Text" type="{}Q850Text" minOccurs="0"/>
 *         <element name="accessTreatmentAudioFile" type="{}MediaTreatmentFileName" minOccurs="0"/>
 *         <element name="accessTreatmentVideoFile" type="{}MediaTreatmentFileName" minOccurs="0"/>
 *         <element name="networkTreatmentAudioFile" type="{}MediaTreatmentFileName" minOccurs="0"/>
 *         <element name="networkTreatmentVideoFile" type="{}MediaTreatmentFileName" minOccurs="0"/>
 *         <element name="cdrTerminationCause" type="{}CDRTerminationCause" minOccurs="0"/>
 *         <element name="routeAdvance" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="internalReleaseCause" type="{}InternalReleaseCause16" minOccurs="0"/>
 *         <element name="accessSendReasonHeader" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="networkSendReasonHeader" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemConfigurableTreatmentAddRequest", propOrder = {
    "treatmentId",
    "chargeIndicator",
    "description",
    "accessSIPStatusCode",
    "accessSIPStatusMessage",
    "networkSIPStatusCode",
    "networkSIPStatusMessage",
    "q850CauseValue",
    "q850Text",
    "accessTreatmentAudioFile",
    "accessTreatmentVideoFile",
    "networkTreatmentAudioFile",
    "networkTreatmentVideoFile",
    "cdrTerminationCause",
    "routeAdvance",
    "internalReleaseCause",
    "accessSendReasonHeader",
    "networkSendReasonHeader"
})
public class SystemConfigurableTreatmentAddRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String treatmentId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ChargeIndicator chargeIndicator;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    protected Integer accessSIPStatusCode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accessSIPStatusMessage;
    protected Integer networkSIPStatusCode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkSIPStatusMessage;
    protected Integer q850CauseValue;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String q850Text;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accessTreatmentAudioFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accessTreatmentVideoFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkTreatmentAudioFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkTreatmentVideoFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String cdrTerminationCause;
    protected Boolean routeAdvance;
    @XmlSchemaType(name = "token")
    protected InternalReleaseCause16 internalReleaseCause;
    protected Boolean accessSendReasonHeader;
    protected Boolean networkSendReasonHeader;

    /**
     * Ruft den Wert der treatmentId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTreatmentId() {
        return treatmentId;
    }

    /**
     * Legt den Wert der treatmentId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTreatmentId(String value) {
        this.treatmentId = value;
    }

    /**
     * Ruft den Wert der chargeIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ChargeIndicator }
     *     
     */
    public ChargeIndicator getChargeIndicator() {
        return chargeIndicator;
    }

    /**
     * Legt den Wert der chargeIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChargeIndicator }
     *     
     */
    public void setChargeIndicator(ChargeIndicator value) {
        this.chargeIndicator = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der accessSIPStatusCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAccessSIPStatusCode() {
        return accessSIPStatusCode;
    }

    /**
     * Legt den Wert der accessSIPStatusCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAccessSIPStatusCode(Integer value) {
        this.accessSIPStatusCode = value;
    }

    /**
     * Ruft den Wert der accessSIPStatusMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessSIPStatusMessage() {
        return accessSIPStatusMessage;
    }

    /**
     * Legt den Wert der accessSIPStatusMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessSIPStatusMessage(String value) {
        this.accessSIPStatusMessage = value;
    }

    /**
     * Ruft den Wert der networkSIPStatusCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNetworkSIPStatusCode() {
        return networkSIPStatusCode;
    }

    /**
     * Legt den Wert der networkSIPStatusCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNetworkSIPStatusCode(Integer value) {
        this.networkSIPStatusCode = value;
    }

    /**
     * Ruft den Wert der networkSIPStatusMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkSIPStatusMessage() {
        return networkSIPStatusMessage;
    }

    /**
     * Legt den Wert der networkSIPStatusMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkSIPStatusMessage(String value) {
        this.networkSIPStatusMessage = value;
    }

    /**
     * Ruft den Wert der q850CauseValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQ850CauseValue() {
        return q850CauseValue;
    }

    /**
     * Legt den Wert der q850CauseValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQ850CauseValue(Integer value) {
        this.q850CauseValue = value;
    }

    /**
     * Ruft den Wert der q850Text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQ850Text() {
        return q850Text;
    }

    /**
     * Legt den Wert der q850Text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQ850Text(String value) {
        this.q850Text = value;
    }

    /**
     * Ruft den Wert der accessTreatmentAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessTreatmentAudioFile() {
        return accessTreatmentAudioFile;
    }

    /**
     * Legt den Wert der accessTreatmentAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessTreatmentAudioFile(String value) {
        this.accessTreatmentAudioFile = value;
    }

    /**
     * Ruft den Wert der accessTreatmentVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessTreatmentVideoFile() {
        return accessTreatmentVideoFile;
    }

    /**
     * Legt den Wert der accessTreatmentVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessTreatmentVideoFile(String value) {
        this.accessTreatmentVideoFile = value;
    }

    /**
     * Ruft den Wert der networkTreatmentAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkTreatmentAudioFile() {
        return networkTreatmentAudioFile;
    }

    /**
     * Legt den Wert der networkTreatmentAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkTreatmentAudioFile(String value) {
        this.networkTreatmentAudioFile = value;
    }

    /**
     * Ruft den Wert der networkTreatmentVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkTreatmentVideoFile() {
        return networkTreatmentVideoFile;
    }

    /**
     * Legt den Wert der networkTreatmentVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkTreatmentVideoFile(String value) {
        this.networkTreatmentVideoFile = value;
    }

    /**
     * Ruft den Wert der cdrTerminationCause-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdrTerminationCause() {
        return cdrTerminationCause;
    }

    /**
     * Legt den Wert der cdrTerminationCause-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdrTerminationCause(String value) {
        this.cdrTerminationCause = value;
    }

    /**
     * Ruft den Wert der routeAdvance-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRouteAdvance() {
        return routeAdvance;
    }

    /**
     * Legt den Wert der routeAdvance-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRouteAdvance(Boolean value) {
        this.routeAdvance = value;
    }

    /**
     * Ruft den Wert der internalReleaseCause-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InternalReleaseCause16 }
     *     
     */
    public InternalReleaseCause16 getInternalReleaseCause() {
        return internalReleaseCause;
    }

    /**
     * Legt den Wert der internalReleaseCause-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InternalReleaseCause16 }
     *     
     */
    public void setInternalReleaseCause(InternalReleaseCause16 value) {
        this.internalReleaseCause = value;
    }

    /**
     * Ruft den Wert der accessSendReasonHeader-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAccessSendReasonHeader() {
        return accessSendReasonHeader;
    }

    /**
     * Legt den Wert der accessSendReasonHeader-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAccessSendReasonHeader(Boolean value) {
        this.accessSendReasonHeader = value;
    }

    /**
     * Ruft den Wert der networkSendReasonHeader-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNetworkSendReasonHeader() {
        return networkSendReasonHeader;
    }

    /**
     * Legt den Wert der networkSendReasonHeader-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNetworkSendReasonHeader(Boolean value) {
        this.networkSendReasonHeader = value;
    }

}
