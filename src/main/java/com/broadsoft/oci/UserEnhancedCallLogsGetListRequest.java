//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request user's call logs. It is possible to get a subset of the total list of calls
 *         by specifying a starting offset and the number of calls to get.
 *         If the callLogType is not specified, all types of calls are returned.
 *         The response is either a UserEnhancedCallLogsGetListResponse or an ErrorResponse.
 *         Replaced By: UserEnhancedCallLogsGetListRequest14sp4
 *       
 * 
 * <p>Java-Klasse für UserEnhancedCallLogsGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserEnhancedCallLogsGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="callLogType" type="{}EnhancedCallLogsType" minOccurs="0"/>
 *         <element name="startingOffset" type="{}EnhancedCallLogsOffset"/>
 *         <element name="numCalls" type="{}EnhancedCallLogsMaxLoggedCalls"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserEnhancedCallLogsGetListRequest", propOrder = {
    "userId",
    "callLogType",
    "startingOffset",
    "numCalls"
})
public class UserEnhancedCallLogsGetListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlSchemaType(name = "token")
    protected EnhancedCallLogsType callLogType;
    protected int startingOffset;
    protected int numCalls;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der callLogType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnhancedCallLogsType }
     *     
     */
    public EnhancedCallLogsType getCallLogType() {
        return callLogType;
    }

    /**
     * Legt den Wert der callLogType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnhancedCallLogsType }
     *     
     */
    public void setCallLogType(EnhancedCallLogsType value) {
        this.callLogType = value;
    }

    /**
     * Ruft den Wert der startingOffset-Eigenschaft ab.
     * 
     */
    public int getStartingOffset() {
        return startingOffset;
    }

    /**
     * Legt den Wert der startingOffset-Eigenschaft fest.
     * 
     */
    public void setStartingOffset(int value) {
        this.startingOffset = value;
    }

    /**
     * Ruft den Wert der numCalls-Eigenschaft ab.
     * 
     */
    public int getNumCalls() {
        return numCalls;
    }

    /**
     * Legt den Wert der numCalls-Eigenschaft fest.
     * 
     */
    public void setNumCalls(int value) {
        this.numCalls = value;
    }

}
