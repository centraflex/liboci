//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify room settings for a specific Project Room or My Room.
 *         Request to modify room settings for a specific Instant Room is responded
 *         with an ErrorResponse. The roomSchedule parameter is applicable for
 *         Project Room only. If the roomSchedule parameter is specified in the
 *         request to modify My Room, the request will be failed.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserCollaborateRoomModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCollaborateRoomModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="roomId" type="{}CollaborateRoomId"/>
 *         <element name="roomName" type="{}CollaborateRoomName" minOccurs="0"/>
 *         <element name="attendeeNotification" type="{}CollaborateRoomAttendeeNotification" minOccurs="0"/>
 *         <element name="endCollaborateRoomSessionOnOwnerExit" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="ownerRequired" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="roomSchedule" type="{}CollaborateRoomSchedule" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCollaborateRoomModifyRequest", propOrder = {
    "userId",
    "roomId",
    "roomName",
    "attendeeNotification",
    "endCollaborateRoomSessionOnOwnerExit",
    "ownerRequired",
    "roomSchedule"
})
public class UserCollaborateRoomModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String roomId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String roomName;
    @XmlSchemaType(name = "token")
    protected CollaborateRoomAttendeeNotification attendeeNotification;
    protected Boolean endCollaborateRoomSessionOnOwnerExit;
    protected Boolean ownerRequired;
    protected CollaborateRoomSchedule roomSchedule;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der roomId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomId() {
        return roomId;
    }

    /**
     * Legt den Wert der roomId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomId(String value) {
        this.roomId = value;
    }

    /**
     * Ruft den Wert der roomName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomName() {
        return roomName;
    }

    /**
     * Legt den Wert der roomName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomName(String value) {
        this.roomName = value;
    }

    /**
     * Ruft den Wert der attendeeNotification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CollaborateRoomAttendeeNotification }
     *     
     */
    public CollaborateRoomAttendeeNotification getAttendeeNotification() {
        return attendeeNotification;
    }

    /**
     * Legt den Wert der attendeeNotification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CollaborateRoomAttendeeNotification }
     *     
     */
    public void setAttendeeNotification(CollaborateRoomAttendeeNotification value) {
        this.attendeeNotification = value;
    }

    /**
     * Ruft den Wert der endCollaborateRoomSessionOnOwnerExit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEndCollaborateRoomSessionOnOwnerExit() {
        return endCollaborateRoomSessionOnOwnerExit;
    }

    /**
     * Legt den Wert der endCollaborateRoomSessionOnOwnerExit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEndCollaborateRoomSessionOnOwnerExit(Boolean value) {
        this.endCollaborateRoomSessionOnOwnerExit = value;
    }

    /**
     * Ruft den Wert der ownerRequired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOwnerRequired() {
        return ownerRequired;
    }

    /**
     * Legt den Wert der ownerRequired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOwnerRequired(Boolean value) {
        this.ownerRequired = value;
    }

    /**
     * Ruft den Wert der roomSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CollaborateRoomSchedule }
     *     
     */
    public CollaborateRoomSchedule getRoomSchedule() {
        return roomSchedule;
    }

    /**
     * Legt den Wert der roomSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CollaborateRoomSchedule }
     *     
     */
    public void setRoomSchedule(CollaborateRoomSchedule value) {
        this.roomSchedule = value;
    }

}
