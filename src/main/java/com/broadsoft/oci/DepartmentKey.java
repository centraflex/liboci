//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Uniquely identifies a department system-wide.
 *         Departments are contained in either an enterprise or a group. Enterprise departments can be
 *         used by any or all groups within the enterprise. Department names are unique within a group and
 *         within an enterprise, but the same department name can exist in 2 different groups or in both
 *         a group and its parent enterprise. Therefore, to uniquely identify a department, we must know
 *         the department name and which enterprise or group contains the department.
 *         This type is extended by group and enterprise department keys.
 *       
 * 
 * <p>Java-Klasse für DepartmentKey complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="DepartmentKey">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DepartmentKey")
@XmlSeeAlso({
    EnterpriseDepartmentKey.class,
    GroupDepartmentKey.class
})
public abstract class DepartmentKey {


}
