//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *       Modify a Trunk Group Instance in a group.
 *       The access device cannot be modified or cleared if there are any users assigned to the Trunk Group.
 *           The command will fail if enableNetworkAddressIdentity is set and 
 *           administrator is not System/Provisioning level administrator.
 *       The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         Replaced by GroupTrunkGroupModifyInstanceRequest19sp1.
 *      
 * 
 * <p>Java-Klasse für GroupTrunkGroupModifyInstanceRequest15 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupTrunkGroupModifyInstanceRequest15">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="trunkGroupKey" type="{}TrunkGroupKey"/>
 *         <element name="newName" type="{}TrunkGroupName" minOccurs="0"/>
 *         <element name="pilotUserId" type="{}UserId" minOccurs="0"/>
 *         <element name="department" type="{}DepartmentKey" minOccurs="0"/>
 *         <element name="accessDevice" type="{}AccessDevice" minOccurs="0"/>
 *         <element name="maxActiveCalls" type="{}MaxActiveCalls" minOccurs="0"/>
 *         <element name="maxIncomingCalls" type="{}MaxIncomingCalls" minOccurs="0"/>
 *         <element name="maxOutgoingCalls" type="{}MaxOutgoingCalls" minOccurs="0"/>
 *         <element name="enableBursting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="burstingMaxActiveCalls" type="{}BurstingMaxActiveCalls" minOccurs="0"/>
 *         <element name="burstingMaxIncomingCalls" type="{}BurstingMaxIncomingCalls" minOccurs="0"/>
 *         <element name="burstingMaxOutgoingCalls" type="{}BurstingMaxOutgoingCalls" minOccurs="0"/>
 *         <element name="capacityExceededAction" type="{}TrunkGroupCapacityExceededAction" minOccurs="0"/>
 *         <element name="capacityExceededForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="capacityExceededRerouteTrunkGroupKey" type="{}TrunkGroupKey" minOccurs="0"/>
 *         <element name="capacityExceededTrapInitialCalls" type="{}TrapInitialThreshold" minOccurs="0"/>
 *         <element name="capacityExceededTrapOffsetCalls" type="{}TrapOffsetThreshold" minOccurs="0"/>
 *         <element name="unreachableDestinationAction" type="{}TrunkGroupUnreachableDestinationAction" minOccurs="0"/>
 *         <element name="unreachableDestinationForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="unreachableDestinationRerouteTrunkGroupKey" type="{}TrunkGroupKey" minOccurs="0"/>
 *         <element name="invitationTimeout" type="{}TrunkGroupInvitationTimeoutSeconds" minOccurs="0"/>
 *         <element name="requireAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="sipAuthenticationUserName" type="{}SIPAuthenticationUserName" minOccurs="0"/>
 *         <element name="sipAuthenticationPassword" type="{}Password" minOccurs="0"/>
 *         <element name="hostedUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *         <element name="trunkGroupIdentity" type="{}SIPURI" minOccurs="0"/>
 *         <element name="otgDtgIdentity" type="{}OtgDtgIdentity" minOccurs="0"/>
 *         <element name="allowTerminationToTrunkGroupIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowTerminationToDtgIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeTrunkGroupIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeDtgIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeTrunkGroupIdentityForNetworkCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeOtgIdentityForNetworkCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableNetworkAddressIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowUnscreenedCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowUnscreenedEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="pilotUserCallingLineIdentityPolicy" type="{}TrunkGroupPilotUserCallingLineIdentityUsagePolicy" minOccurs="0"/>
 *         <element name="pilotUserChargeNumberPolicy" type="{}TrunkGroupPilotUserChargeNumberUsagePolicy" minOccurs="0"/>
 *         <element name="callForwardingAlwaysAction" type="{}TrunkGroupCallForwardingAlwaysAction" minOccurs="0"/>
 *         <element name="callForwardingAlwaysForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="callForwardingAlwaysRerouteTrunkGroupKey" type="{}TrunkGroupKey" minOccurs="0"/>
 *         <element name="peeringDomain" type="{}DomainName" minOccurs="0"/>
 *         <element name="routeToPeeringDomain" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="prefixEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="prefix" type="{}TrunkGroupPrefix" minOccurs="0"/>
 *         <element name="statefulReroutingEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="sendContinuousOptionsMessage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="continuousOptionsSendingIntervalSeconds" type="{}TrunkGroupContinuousOptionsSendingIntervalSeconds" minOccurs="0"/>
 *         <element name="failureOptionsSendingIntervalSeconds" type="{}TrunkGroupFailureOptionsSendingIntervalSeconds" minOccurs="0"/>
 *         <element name="failureThresholdCounter" type="{}TrunkGroupThresholdCounter" minOccurs="0"/>
 *         <element name="successThresholdCounter" type="{}TrunkGroupThresholdCounter" minOccurs="0"/>
 *         <element name="inviteFailureThresholdCounter" type="{}TrunkGroupThresholdCounter" minOccurs="0"/>
 *         <element name="inviteFailureThresholdWindowSeconds" type="{}TrunkGroupFailureThresholdWindowSeconds" minOccurs="0"/>
 *         <element name="pilotUserCallingLineAssertedIdentityPolicy" type="{}TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy" minOccurs="0"/>
 *         <element name="useSystemCallingLineAssertedIdentityPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="pilotUserCallOptimizationPolicy" type="{}TrunkGroupPilotUserCallOptimizationPolicy" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupTrunkGroupModifyInstanceRequest15", propOrder = {
    "trunkGroupKey",
    "newName",
    "pilotUserId",
    "department",
    "accessDevice",
    "maxActiveCalls",
    "maxIncomingCalls",
    "maxOutgoingCalls",
    "enableBursting",
    "burstingMaxActiveCalls",
    "burstingMaxIncomingCalls",
    "burstingMaxOutgoingCalls",
    "capacityExceededAction",
    "capacityExceededForwardAddress",
    "capacityExceededRerouteTrunkGroupKey",
    "capacityExceededTrapInitialCalls",
    "capacityExceededTrapOffsetCalls",
    "unreachableDestinationAction",
    "unreachableDestinationForwardAddress",
    "unreachableDestinationRerouteTrunkGroupKey",
    "invitationTimeout",
    "requireAuthentication",
    "sipAuthenticationUserName",
    "sipAuthenticationPassword",
    "hostedUserIdList",
    "trunkGroupIdentity",
    "otgDtgIdentity",
    "allowTerminationToTrunkGroupIdentity",
    "allowTerminationToDtgIdentity",
    "includeTrunkGroupIdentity",
    "includeDtgIdentity",
    "includeTrunkGroupIdentityForNetworkCalls",
    "includeOtgIdentityForNetworkCalls",
    "enableNetworkAddressIdentity",
    "allowUnscreenedCalls",
    "allowUnscreenedEmergencyCalls",
    "pilotUserCallingLineIdentityPolicy",
    "pilotUserChargeNumberPolicy",
    "callForwardingAlwaysAction",
    "callForwardingAlwaysForwardAddress",
    "callForwardingAlwaysRerouteTrunkGroupKey",
    "peeringDomain",
    "routeToPeeringDomain",
    "prefixEnabled",
    "prefix",
    "statefulReroutingEnabled",
    "sendContinuousOptionsMessage",
    "continuousOptionsSendingIntervalSeconds",
    "failureOptionsSendingIntervalSeconds",
    "failureThresholdCounter",
    "successThresholdCounter",
    "inviteFailureThresholdCounter",
    "inviteFailureThresholdWindowSeconds",
    "pilotUserCallingLineAssertedIdentityPolicy",
    "useSystemCallingLineAssertedIdentityPolicy",
    "pilotUserCallOptimizationPolicy"
})
public class GroupTrunkGroupModifyInstanceRequest15
    extends OCIRequest
{

    @XmlElement(required = true)
    protected TrunkGroupKey trunkGroupKey;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newName;
    @XmlElementRef(name = "pilotUserId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pilotUserId;
    @XmlElementRef(name = "department", type = JAXBElement.class, required = false)
    protected JAXBElement<DepartmentKey> department;
    @XmlElementRef(name = "accessDevice", type = JAXBElement.class, required = false)
    protected JAXBElement<AccessDevice> accessDevice;
    protected Integer maxActiveCalls;
    @XmlElementRef(name = "maxIncomingCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxIncomingCalls;
    @XmlElementRef(name = "maxOutgoingCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxOutgoingCalls;
    protected Boolean enableBursting;
    @XmlElementRef(name = "burstingMaxActiveCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> burstingMaxActiveCalls;
    @XmlElementRef(name = "burstingMaxIncomingCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> burstingMaxIncomingCalls;
    @XmlElementRef(name = "burstingMaxOutgoingCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> burstingMaxOutgoingCalls;
    @XmlElementRef(name = "capacityExceededAction", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupCapacityExceededAction> capacityExceededAction;
    @XmlElementRef(name = "capacityExceededForwardAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> capacityExceededForwardAddress;
    @XmlElementRef(name = "capacityExceededRerouteTrunkGroupKey", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupKey> capacityExceededRerouteTrunkGroupKey;
    protected Integer capacityExceededTrapInitialCalls;
    protected Integer capacityExceededTrapOffsetCalls;
    @XmlElementRef(name = "unreachableDestinationAction", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupUnreachableDestinationAction> unreachableDestinationAction;
    @XmlElementRef(name = "unreachableDestinationForwardAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> unreachableDestinationForwardAddress;
    @XmlElementRef(name = "unreachableDestinationRerouteTrunkGroupKey", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupKey> unreachableDestinationRerouteTrunkGroupKey;
    protected Integer invitationTimeout;
    protected Boolean requireAuthentication;
    @XmlElementRef(name = "sipAuthenticationUserName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sipAuthenticationUserName;
    @XmlElementRef(name = "sipAuthenticationPassword", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sipAuthenticationPassword;
    @XmlElementRef(name = "hostedUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> hostedUserIdList;
    @XmlElementRef(name = "trunkGroupIdentity", type = JAXBElement.class, required = false)
    protected JAXBElement<String> trunkGroupIdentity;
    @XmlElementRef(name = "otgDtgIdentity", type = JAXBElement.class, required = false)
    protected JAXBElement<String> otgDtgIdentity;
    protected Boolean allowTerminationToTrunkGroupIdentity;
    protected Boolean allowTerminationToDtgIdentity;
    protected Boolean includeTrunkGroupIdentity;
    protected Boolean includeDtgIdentity;
    protected Boolean includeTrunkGroupIdentityForNetworkCalls;
    protected Boolean includeOtgIdentityForNetworkCalls;
    protected Boolean enableNetworkAddressIdentity;
    protected Boolean allowUnscreenedCalls;
    protected Boolean allowUnscreenedEmergencyCalls;
    @XmlSchemaType(name = "token")
    protected TrunkGroupPilotUserCallingLineIdentityUsagePolicy pilotUserCallingLineIdentityPolicy;
    @XmlSchemaType(name = "token")
    protected TrunkGroupPilotUserChargeNumberUsagePolicy pilotUserChargeNumberPolicy;
    @XmlElementRef(name = "callForwardingAlwaysAction", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupCallForwardingAlwaysAction> callForwardingAlwaysAction;
    @XmlElementRef(name = "callForwardingAlwaysForwardAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callForwardingAlwaysForwardAddress;
    @XmlElementRef(name = "callForwardingAlwaysRerouteTrunkGroupKey", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupKey> callForwardingAlwaysRerouteTrunkGroupKey;
    @XmlElementRef(name = "peeringDomain", type = JAXBElement.class, required = false)
    protected JAXBElement<String> peeringDomain;
    protected Boolean routeToPeeringDomain;
    protected Boolean prefixEnabled;
    @XmlElementRef(name = "prefix", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prefix;
    protected Boolean statefulReroutingEnabled;
    protected Boolean sendContinuousOptionsMessage;
    protected Integer continuousOptionsSendingIntervalSeconds;
    protected Integer failureOptionsSendingIntervalSeconds;
    protected Integer failureThresholdCounter;
    protected Integer successThresholdCounter;
    protected Integer inviteFailureThresholdCounter;
    protected Integer inviteFailureThresholdWindowSeconds;
    @XmlSchemaType(name = "token")
    protected TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy pilotUserCallingLineAssertedIdentityPolicy;
    protected Boolean useSystemCallingLineAssertedIdentityPolicy;
    @XmlSchemaType(name = "token")
    protected TrunkGroupPilotUserCallOptimizationPolicy pilotUserCallOptimizationPolicy;

    /**
     * Ruft den Wert der trunkGroupKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupKey }
     *     
     */
    public TrunkGroupKey getTrunkGroupKey() {
        return trunkGroupKey;
    }

    /**
     * Legt den Wert der trunkGroupKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupKey }
     *     
     */
    public void setTrunkGroupKey(TrunkGroupKey value) {
        this.trunkGroupKey = value;
    }

    /**
     * Ruft den Wert der newName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewName() {
        return newName;
    }

    /**
     * Legt den Wert der newName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewName(String value) {
        this.newName = value;
    }

    /**
     * Ruft den Wert der pilotUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPilotUserId() {
        return pilotUserId;
    }

    /**
     * Legt den Wert der pilotUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPilotUserId(JAXBElement<String> value) {
        this.pilotUserId = value;
    }

    /**
     * Ruft den Wert der department-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DepartmentKey }{@code >}
     *     
     */
    public JAXBElement<DepartmentKey> getDepartment() {
        return department;
    }

    /**
     * Legt den Wert der department-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DepartmentKey }{@code >}
     *     
     */
    public void setDepartment(JAXBElement<DepartmentKey> value) {
        this.department = value;
    }

    /**
     * Ruft den Wert der accessDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccessDevice }{@code >}
     *     
     */
    public JAXBElement<AccessDevice> getAccessDevice() {
        return accessDevice;
    }

    /**
     * Legt den Wert der accessDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccessDevice }{@code >}
     *     
     */
    public void setAccessDevice(JAXBElement<AccessDevice> value) {
        this.accessDevice = value;
    }

    /**
     * Ruft den Wert der maxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxActiveCalls() {
        return maxActiveCalls;
    }

    /**
     * Legt den Wert der maxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxActiveCalls(Integer value) {
        this.maxActiveCalls = value;
    }

    /**
     * Ruft den Wert der maxIncomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxIncomingCalls() {
        return maxIncomingCalls;
    }

    /**
     * Legt den Wert der maxIncomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxIncomingCalls(JAXBElement<Integer> value) {
        this.maxIncomingCalls = value;
    }

    /**
     * Ruft den Wert der maxOutgoingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxOutgoingCalls() {
        return maxOutgoingCalls;
    }

    /**
     * Legt den Wert der maxOutgoingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxOutgoingCalls(JAXBElement<Integer> value) {
        this.maxOutgoingCalls = value;
    }

    /**
     * Ruft den Wert der enableBursting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableBursting() {
        return enableBursting;
    }

    /**
     * Legt den Wert der enableBursting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableBursting(Boolean value) {
        this.enableBursting = value;
    }

    /**
     * Ruft den Wert der burstingMaxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getBurstingMaxActiveCalls() {
        return burstingMaxActiveCalls;
    }

    /**
     * Legt den Wert der burstingMaxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setBurstingMaxActiveCalls(JAXBElement<Integer> value) {
        this.burstingMaxActiveCalls = value;
    }

    /**
     * Ruft den Wert der burstingMaxIncomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getBurstingMaxIncomingCalls() {
        return burstingMaxIncomingCalls;
    }

    /**
     * Legt den Wert der burstingMaxIncomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setBurstingMaxIncomingCalls(JAXBElement<Integer> value) {
        this.burstingMaxIncomingCalls = value;
    }

    /**
     * Ruft den Wert der burstingMaxOutgoingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getBurstingMaxOutgoingCalls() {
        return burstingMaxOutgoingCalls;
    }

    /**
     * Legt den Wert der burstingMaxOutgoingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setBurstingMaxOutgoingCalls(JAXBElement<Integer> value) {
        this.burstingMaxOutgoingCalls = value;
    }

    /**
     * Ruft den Wert der capacityExceededAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupCapacityExceededAction }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupCapacityExceededAction> getCapacityExceededAction() {
        return capacityExceededAction;
    }

    /**
     * Legt den Wert der capacityExceededAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupCapacityExceededAction }{@code >}
     *     
     */
    public void setCapacityExceededAction(JAXBElement<TrunkGroupCapacityExceededAction> value) {
        this.capacityExceededAction = value;
    }

    /**
     * Ruft den Wert der capacityExceededForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCapacityExceededForwardAddress() {
        return capacityExceededForwardAddress;
    }

    /**
     * Legt den Wert der capacityExceededForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCapacityExceededForwardAddress(JAXBElement<String> value) {
        this.capacityExceededForwardAddress = value;
    }

    /**
     * Ruft den Wert der capacityExceededRerouteTrunkGroupKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupKey }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupKey> getCapacityExceededRerouteTrunkGroupKey() {
        return capacityExceededRerouteTrunkGroupKey;
    }

    /**
     * Legt den Wert der capacityExceededRerouteTrunkGroupKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupKey }{@code >}
     *     
     */
    public void setCapacityExceededRerouteTrunkGroupKey(JAXBElement<TrunkGroupKey> value) {
        this.capacityExceededRerouteTrunkGroupKey = value;
    }

    /**
     * Ruft den Wert der capacityExceededTrapInitialCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCapacityExceededTrapInitialCalls() {
        return capacityExceededTrapInitialCalls;
    }

    /**
     * Legt den Wert der capacityExceededTrapInitialCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCapacityExceededTrapInitialCalls(Integer value) {
        this.capacityExceededTrapInitialCalls = value;
    }

    /**
     * Ruft den Wert der capacityExceededTrapOffsetCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCapacityExceededTrapOffsetCalls() {
        return capacityExceededTrapOffsetCalls;
    }

    /**
     * Legt den Wert der capacityExceededTrapOffsetCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCapacityExceededTrapOffsetCalls(Integer value) {
        this.capacityExceededTrapOffsetCalls = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupUnreachableDestinationAction }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupUnreachableDestinationAction> getUnreachableDestinationAction() {
        return unreachableDestinationAction;
    }

    /**
     * Legt den Wert der unreachableDestinationAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupUnreachableDestinationAction }{@code >}
     *     
     */
    public void setUnreachableDestinationAction(JAXBElement<TrunkGroupUnreachableDestinationAction> value) {
        this.unreachableDestinationAction = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUnreachableDestinationForwardAddress() {
        return unreachableDestinationForwardAddress;
    }

    /**
     * Legt den Wert der unreachableDestinationForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUnreachableDestinationForwardAddress(JAXBElement<String> value) {
        this.unreachableDestinationForwardAddress = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationRerouteTrunkGroupKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupKey }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupKey> getUnreachableDestinationRerouteTrunkGroupKey() {
        return unreachableDestinationRerouteTrunkGroupKey;
    }

    /**
     * Legt den Wert der unreachableDestinationRerouteTrunkGroupKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupKey }{@code >}
     *     
     */
    public void setUnreachableDestinationRerouteTrunkGroupKey(JAXBElement<TrunkGroupKey> value) {
        this.unreachableDestinationRerouteTrunkGroupKey = value;
    }

    /**
     * Ruft den Wert der invitationTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInvitationTimeout() {
        return invitationTimeout;
    }

    /**
     * Legt den Wert der invitationTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInvitationTimeout(Integer value) {
        this.invitationTimeout = value;
    }

    /**
     * Ruft den Wert der requireAuthentication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequireAuthentication() {
        return requireAuthentication;
    }

    /**
     * Legt den Wert der requireAuthentication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequireAuthentication(Boolean value) {
        this.requireAuthentication = value;
    }

    /**
     * Ruft den Wert der sipAuthenticationUserName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSipAuthenticationUserName() {
        return sipAuthenticationUserName;
    }

    /**
     * Legt den Wert der sipAuthenticationUserName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSipAuthenticationUserName(JAXBElement<String> value) {
        this.sipAuthenticationUserName = value;
    }

    /**
     * Ruft den Wert der sipAuthenticationPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSipAuthenticationPassword() {
        return sipAuthenticationPassword;
    }

    /**
     * Legt den Wert der sipAuthenticationPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSipAuthenticationPassword(JAXBElement<String> value) {
        this.sipAuthenticationPassword = value;
    }

    /**
     * Ruft den Wert der hostedUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getHostedUserIdList() {
        return hostedUserIdList;
    }

    /**
     * Legt den Wert der hostedUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setHostedUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.hostedUserIdList = value;
    }

    /**
     * Ruft den Wert der trunkGroupIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTrunkGroupIdentity() {
        return trunkGroupIdentity;
    }

    /**
     * Legt den Wert der trunkGroupIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTrunkGroupIdentity(JAXBElement<String> value) {
        this.trunkGroupIdentity = value;
    }

    /**
     * Ruft den Wert der otgDtgIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOtgDtgIdentity() {
        return otgDtgIdentity;
    }

    /**
     * Legt den Wert der otgDtgIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOtgDtgIdentity(JAXBElement<String> value) {
        this.otgDtgIdentity = value;
    }

    /**
     * Ruft den Wert der allowTerminationToTrunkGroupIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowTerminationToTrunkGroupIdentity() {
        return allowTerminationToTrunkGroupIdentity;
    }

    /**
     * Legt den Wert der allowTerminationToTrunkGroupIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowTerminationToTrunkGroupIdentity(Boolean value) {
        this.allowTerminationToTrunkGroupIdentity = value;
    }

    /**
     * Ruft den Wert der allowTerminationToDtgIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowTerminationToDtgIdentity() {
        return allowTerminationToDtgIdentity;
    }

    /**
     * Legt den Wert der allowTerminationToDtgIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowTerminationToDtgIdentity(Boolean value) {
        this.allowTerminationToDtgIdentity = value;
    }

    /**
     * Ruft den Wert der includeTrunkGroupIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeTrunkGroupIdentity() {
        return includeTrunkGroupIdentity;
    }

    /**
     * Legt den Wert der includeTrunkGroupIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeTrunkGroupIdentity(Boolean value) {
        this.includeTrunkGroupIdentity = value;
    }

    /**
     * Ruft den Wert der includeDtgIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeDtgIdentity() {
        return includeDtgIdentity;
    }

    /**
     * Legt den Wert der includeDtgIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeDtgIdentity(Boolean value) {
        this.includeDtgIdentity = value;
    }

    /**
     * Ruft den Wert der includeTrunkGroupIdentityForNetworkCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeTrunkGroupIdentityForNetworkCalls() {
        return includeTrunkGroupIdentityForNetworkCalls;
    }

    /**
     * Legt den Wert der includeTrunkGroupIdentityForNetworkCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeTrunkGroupIdentityForNetworkCalls(Boolean value) {
        this.includeTrunkGroupIdentityForNetworkCalls = value;
    }

    /**
     * Ruft den Wert der includeOtgIdentityForNetworkCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeOtgIdentityForNetworkCalls() {
        return includeOtgIdentityForNetworkCalls;
    }

    /**
     * Legt den Wert der includeOtgIdentityForNetworkCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeOtgIdentityForNetworkCalls(Boolean value) {
        this.includeOtgIdentityForNetworkCalls = value;
    }

    /**
     * Ruft den Wert der enableNetworkAddressIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableNetworkAddressIdentity() {
        return enableNetworkAddressIdentity;
    }

    /**
     * Legt den Wert der enableNetworkAddressIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableNetworkAddressIdentity(Boolean value) {
        this.enableNetworkAddressIdentity = value;
    }

    /**
     * Ruft den Wert der allowUnscreenedCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowUnscreenedCalls() {
        return allowUnscreenedCalls;
    }

    /**
     * Legt den Wert der allowUnscreenedCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowUnscreenedCalls(Boolean value) {
        this.allowUnscreenedCalls = value;
    }

    /**
     * Ruft den Wert der allowUnscreenedEmergencyCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowUnscreenedEmergencyCalls() {
        return allowUnscreenedEmergencyCalls;
    }

    /**
     * Legt den Wert der allowUnscreenedEmergencyCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowUnscreenedEmergencyCalls(Boolean value) {
        this.allowUnscreenedEmergencyCalls = value;
    }

    /**
     * Ruft den Wert der pilotUserCallingLineIdentityPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupPilotUserCallingLineIdentityUsagePolicy }
     *     
     */
    public TrunkGroupPilotUserCallingLineIdentityUsagePolicy getPilotUserCallingLineIdentityPolicy() {
        return pilotUserCallingLineIdentityPolicy;
    }

    /**
     * Legt den Wert der pilotUserCallingLineIdentityPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupPilotUserCallingLineIdentityUsagePolicy }
     *     
     */
    public void setPilotUserCallingLineIdentityPolicy(TrunkGroupPilotUserCallingLineIdentityUsagePolicy value) {
        this.pilotUserCallingLineIdentityPolicy = value;
    }

    /**
     * Ruft den Wert der pilotUserChargeNumberPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupPilotUserChargeNumberUsagePolicy }
     *     
     */
    public TrunkGroupPilotUserChargeNumberUsagePolicy getPilotUserChargeNumberPolicy() {
        return pilotUserChargeNumberPolicy;
    }

    /**
     * Legt den Wert der pilotUserChargeNumberPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupPilotUserChargeNumberUsagePolicy }
     *     
     */
    public void setPilotUserChargeNumberPolicy(TrunkGroupPilotUserChargeNumberUsagePolicy value) {
        this.pilotUserChargeNumberPolicy = value;
    }

    /**
     * Ruft den Wert der callForwardingAlwaysAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupCallForwardingAlwaysAction }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupCallForwardingAlwaysAction> getCallForwardingAlwaysAction() {
        return callForwardingAlwaysAction;
    }

    /**
     * Legt den Wert der callForwardingAlwaysAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupCallForwardingAlwaysAction }{@code >}
     *     
     */
    public void setCallForwardingAlwaysAction(JAXBElement<TrunkGroupCallForwardingAlwaysAction> value) {
        this.callForwardingAlwaysAction = value;
    }

    /**
     * Ruft den Wert der callForwardingAlwaysForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallForwardingAlwaysForwardAddress() {
        return callForwardingAlwaysForwardAddress;
    }

    /**
     * Legt den Wert der callForwardingAlwaysForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallForwardingAlwaysForwardAddress(JAXBElement<String> value) {
        this.callForwardingAlwaysForwardAddress = value;
    }

    /**
     * Ruft den Wert der callForwardingAlwaysRerouteTrunkGroupKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupKey }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupKey> getCallForwardingAlwaysRerouteTrunkGroupKey() {
        return callForwardingAlwaysRerouteTrunkGroupKey;
    }

    /**
     * Legt den Wert der callForwardingAlwaysRerouteTrunkGroupKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupKey }{@code >}
     *     
     */
    public void setCallForwardingAlwaysRerouteTrunkGroupKey(JAXBElement<TrunkGroupKey> value) {
        this.callForwardingAlwaysRerouteTrunkGroupKey = value;
    }

    /**
     * Ruft den Wert der peeringDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPeeringDomain() {
        return peeringDomain;
    }

    /**
     * Legt den Wert der peeringDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPeeringDomain(JAXBElement<String> value) {
        this.peeringDomain = value;
    }

    /**
     * Ruft den Wert der routeToPeeringDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRouteToPeeringDomain() {
        return routeToPeeringDomain;
    }

    /**
     * Legt den Wert der routeToPeeringDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRouteToPeeringDomain(Boolean value) {
        this.routeToPeeringDomain = value;
    }

    /**
     * Ruft den Wert der prefixEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPrefixEnabled() {
        return prefixEnabled;
    }

    /**
     * Legt den Wert der prefixEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrefixEnabled(Boolean value) {
        this.prefixEnabled = value;
    }

    /**
     * Ruft den Wert der prefix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrefix() {
        return prefix;
    }

    /**
     * Legt den Wert der prefix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrefix(JAXBElement<String> value) {
        this.prefix = value;
    }

    /**
     * Ruft den Wert der statefulReroutingEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStatefulReroutingEnabled() {
        return statefulReroutingEnabled;
    }

    /**
     * Legt den Wert der statefulReroutingEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStatefulReroutingEnabled(Boolean value) {
        this.statefulReroutingEnabled = value;
    }

    /**
     * Ruft den Wert der sendContinuousOptionsMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendContinuousOptionsMessage() {
        return sendContinuousOptionsMessage;
    }

    /**
     * Legt den Wert der sendContinuousOptionsMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendContinuousOptionsMessage(Boolean value) {
        this.sendContinuousOptionsMessage = value;
    }

    /**
     * Ruft den Wert der continuousOptionsSendingIntervalSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getContinuousOptionsSendingIntervalSeconds() {
        return continuousOptionsSendingIntervalSeconds;
    }

    /**
     * Legt den Wert der continuousOptionsSendingIntervalSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setContinuousOptionsSendingIntervalSeconds(Integer value) {
        this.continuousOptionsSendingIntervalSeconds = value;
    }

    /**
     * Ruft den Wert der failureOptionsSendingIntervalSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFailureOptionsSendingIntervalSeconds() {
        return failureOptionsSendingIntervalSeconds;
    }

    /**
     * Legt den Wert der failureOptionsSendingIntervalSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFailureOptionsSendingIntervalSeconds(Integer value) {
        this.failureOptionsSendingIntervalSeconds = value;
    }

    /**
     * Ruft den Wert der failureThresholdCounter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFailureThresholdCounter() {
        return failureThresholdCounter;
    }

    /**
     * Legt den Wert der failureThresholdCounter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFailureThresholdCounter(Integer value) {
        this.failureThresholdCounter = value;
    }

    /**
     * Ruft den Wert der successThresholdCounter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSuccessThresholdCounter() {
        return successThresholdCounter;
    }

    /**
     * Legt den Wert der successThresholdCounter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSuccessThresholdCounter(Integer value) {
        this.successThresholdCounter = value;
    }

    /**
     * Ruft den Wert der inviteFailureThresholdCounter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInviteFailureThresholdCounter() {
        return inviteFailureThresholdCounter;
    }

    /**
     * Legt den Wert der inviteFailureThresholdCounter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInviteFailureThresholdCounter(Integer value) {
        this.inviteFailureThresholdCounter = value;
    }

    /**
     * Ruft den Wert der inviteFailureThresholdWindowSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInviteFailureThresholdWindowSeconds() {
        return inviteFailureThresholdWindowSeconds;
    }

    /**
     * Legt den Wert der inviteFailureThresholdWindowSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInviteFailureThresholdWindowSeconds(Integer value) {
        this.inviteFailureThresholdWindowSeconds = value;
    }

    /**
     * Ruft den Wert der pilotUserCallingLineAssertedIdentityPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy }
     *     
     */
    public TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy getPilotUserCallingLineAssertedIdentityPolicy() {
        return pilotUserCallingLineAssertedIdentityPolicy;
    }

    /**
     * Legt den Wert der pilotUserCallingLineAssertedIdentityPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy }
     *     
     */
    public void setPilotUserCallingLineAssertedIdentityPolicy(TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy value) {
        this.pilotUserCallingLineAssertedIdentityPolicy = value;
    }

    /**
     * Ruft den Wert der useSystemCallingLineAssertedIdentityPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseSystemCallingLineAssertedIdentityPolicy() {
        return useSystemCallingLineAssertedIdentityPolicy;
    }

    /**
     * Legt den Wert der useSystemCallingLineAssertedIdentityPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSystemCallingLineAssertedIdentityPolicy(Boolean value) {
        this.useSystemCallingLineAssertedIdentityPolicy = value;
    }

    /**
     * Ruft den Wert der pilotUserCallOptimizationPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupPilotUserCallOptimizationPolicy }
     *     
     */
    public TrunkGroupPilotUserCallOptimizationPolicy getPilotUserCallOptimizationPolicy() {
        return pilotUserCallOptimizationPolicy;
    }

    /**
     * Legt den Wert der pilotUserCallOptimizationPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupPilotUserCallOptimizationPolicy }
     *     
     */
    public void setPilotUserCallOptimizationPolicy(TrunkGroupPilotUserCallOptimizationPolicy value) {
        this.pilotUserCallOptimizationPolicy = value;
    }

}
