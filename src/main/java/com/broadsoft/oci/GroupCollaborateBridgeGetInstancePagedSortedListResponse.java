//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupCollaborateBridgeGetInstancePagedSortedListRequest.
 *         Contains a table with column headings: "Service User Id", "Name", "Phone Number", "Is Phone Number Activated", 
 *         "Country Code","National Prefix", "Extension", "Department", "Department Type", 
 *         "Parent Department", "Parent Department Type", "Participants", "Is Default", "Max Room Participants", 
 *         "Is Support Outdial".
 *         The column values for "Is default", "Is Support Outdial" can either be true, or false.
 *       
 * 
 * <p>Java-Klasse für GroupCollaborateBridgeGetInstancePagedSortedListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCollaborateBridgeGetInstancePagedSortedListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="collaborateBridgeTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCollaborateBridgeGetInstancePagedSortedListResponse", propOrder = {
    "collaborateBridgeTable"
})
public class GroupCollaborateBridgeGetInstancePagedSortedListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable collaborateBridgeTable;

    /**
     * Ruft den Wert der collaborateBridgeTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getCollaborateBridgeTable() {
        return collaborateBridgeTable;
    }

    /**
     * Legt den Wert der collaborateBridgeTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setCollaborateBridgeTable(OCITable value) {
        this.collaborateBridgeTable = value;
    }

}
