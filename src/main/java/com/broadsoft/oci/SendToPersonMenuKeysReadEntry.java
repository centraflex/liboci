//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal send to person menu keys.
 *       
 * 
 * <p>Java-Klasse für SendToPersonMenuKeysReadEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SendToPersonMenuKeysReadEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="confirmSendingMessage" type="{}DigitAny"/>
 *         <element name="cancelSendingMessage" type="{}DigitAny"/>
 *         <element name="finishEnteringNumberWhereToSendMessageTo" type="{}DigitStarPound"/>
 *         <element name="finishForwardingOrSendingMessage" type="{}DigitAny"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendToPersonMenuKeysReadEntry", propOrder = {
    "confirmSendingMessage",
    "cancelSendingMessage",
    "finishEnteringNumberWhereToSendMessageTo",
    "finishForwardingOrSendingMessage"
})
public class SendToPersonMenuKeysReadEntry {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String confirmSendingMessage;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String cancelSendingMessage;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String finishEnteringNumberWhereToSendMessageTo;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String finishForwardingOrSendingMessage;

    /**
     * Ruft den Wert der confirmSendingMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmSendingMessage() {
        return confirmSendingMessage;
    }

    /**
     * Legt den Wert der confirmSendingMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmSendingMessage(String value) {
        this.confirmSendingMessage = value;
    }

    /**
     * Ruft den Wert der cancelSendingMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancelSendingMessage() {
        return cancelSendingMessage;
    }

    /**
     * Legt den Wert der cancelSendingMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancelSendingMessage(String value) {
        this.cancelSendingMessage = value;
    }

    /**
     * Ruft den Wert der finishEnteringNumberWhereToSendMessageTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinishEnteringNumberWhereToSendMessageTo() {
        return finishEnteringNumberWhereToSendMessageTo;
    }

    /**
     * Legt den Wert der finishEnteringNumberWhereToSendMessageTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinishEnteringNumberWhereToSendMessageTo(String value) {
        this.finishEnteringNumberWhereToSendMessageTo = value;
    }

    /**
     * Ruft den Wert der finishForwardingOrSendingMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinishForwardingOrSendingMessage() {
        return finishForwardingOrSendingMessage;
    }

    /**
     * Legt den Wert der finishForwardingOrSendingMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinishForwardingOrSendingMessage(String value) {
        this.finishForwardingOrSendingMessage = value;
    }

}
