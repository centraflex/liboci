//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains the music on hold user source configuration.
 *       
 * 
 * <p>Java-Klasse für MusicOnHoldUserSourceModify20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="MusicOnHoldUserSourceModify20">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="messageSourceSelection" type="{}MusicOnHoldUserMessageSelection" minOccurs="0"/>
 *         <element name="customSource" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="audioFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *                   <element name="videoFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MusicOnHoldUserSourceModify20", propOrder = {
    "messageSourceSelection",
    "customSource"
})
public class MusicOnHoldUserSourceModify20 {

    @XmlSchemaType(name = "token")
    protected MusicOnHoldUserMessageSelection messageSourceSelection;
    protected MusicOnHoldUserSourceModify20 .CustomSource customSource;

    /**
     * Ruft den Wert der messageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldUserMessageSelection }
     *     
     */
    public MusicOnHoldUserMessageSelection getMessageSourceSelection() {
        return messageSourceSelection;
    }

    /**
     * Legt den Wert der messageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldUserMessageSelection }
     *     
     */
    public void setMessageSourceSelection(MusicOnHoldUserMessageSelection value) {
        this.messageSourceSelection = value;
    }

    /**
     * Ruft den Wert der customSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldUserSourceModify20 .CustomSource }
     *     
     */
    public MusicOnHoldUserSourceModify20 .CustomSource getCustomSource() {
        return customSource;
    }

    /**
     * Legt den Wert der customSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldUserSourceModify20 .CustomSource }
     *     
     */
    public void setCustomSource(MusicOnHoldUserSourceModify20 .CustomSource value) {
        this.customSource = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="audioFile" type="{}AnnouncementFileKey" minOccurs="0"/>
     *         <element name="videoFile" type="{}AnnouncementFileKey" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "audioFile",
        "videoFile"
    })
    public static class CustomSource {

        @XmlElementRef(name = "audioFile", type = JAXBElement.class, required = false)
        protected JAXBElement<AnnouncementFileKey> audioFile;
        @XmlElementRef(name = "videoFile", type = JAXBElement.class, required = false)
        protected JAXBElement<AnnouncementFileKey> videoFile;

        /**
         * Ruft den Wert der audioFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
         *     
         */
        public JAXBElement<AnnouncementFileKey> getAudioFile() {
            return audioFile;
        }

        /**
         * Legt den Wert der audioFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
         *     
         */
        public void setAudioFile(JAXBElement<AnnouncementFileKey> value) {
            this.audioFile = value;
        }

        /**
         * Ruft den Wert der videoFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
         *     
         */
        public JAXBElement<AnnouncementFileKey> getVideoFile() {
            return videoFile;
        }

        /**
         * Legt den Wert der videoFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
         *     
         */
        public void setVideoFile(JAXBElement<AnnouncementFileKey> value) {
            this.videoFile = value;
        }

    }

}
