//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Requests the system voice portal Menus setting.
 *         The response is either SystemVoiceMessagingGroupGetVoicePortalMenusResponse21 or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemVoiceMessagingGroupGetVoicePortalMenusRequest21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemVoiceMessagingGroupGetVoicePortalMenusRequest21">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="getDefaultMenuKeys" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemVoiceMessagingGroupGetVoicePortalMenusRequest21", propOrder = {
    "getDefaultMenuKeys"
})
public class SystemVoiceMessagingGroupGetVoicePortalMenusRequest21
    extends OCIRequest
{

    protected boolean getDefaultMenuKeys;

    /**
     * Ruft den Wert der getDefaultMenuKeys-Eigenschaft ab.
     * 
     */
    public boolean isGetDefaultMenuKeys() {
        return getDefaultMenuKeys;
    }

    /**
     * Legt den Wert der getDefaultMenuKeys-Eigenschaft fest.
     * 
     */
    public void setGetDefaultMenuKeys(boolean value) {
        this.getDefaultMenuKeys = value;
    }

}
