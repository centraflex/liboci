//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a criteria for the user's custom ringback service.
 *         For the callToNumbers in the callToNumberList, the extension element is not used and the number element is only used when the type is BroadWorks Mobility.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserCustomRingbackUserModifyCriteriaRequest20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCustomRingbackUserModifyCriteriaRequest20">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="criteriaName" type="{}CriteriaName"/>
 *         <element name="newCriteriaName" type="{}CriteriaName" minOccurs="0"/>
 *         <element name="timeSchedule" type="{}TimeSchedule" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}HolidaySchedule" minOccurs="0"/>
 *         <element name="blacklisted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="fromDnCriteria" type="{}CriteriaFromDnModify" minOccurs="0"/>
 *         <element name="callToNumberList" type="{}ReplacementCallToNumberList" minOccurs="0"/>
 *         <element name="audioSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="audioFile" type="{}ExtendedMediaFileLevelResource20" minOccurs="0"/>
 *         <element name="videoSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="videoFile" type="{}ExtendedMediaFileLevelResource20" minOccurs="0"/>
 *         <element name="callWaitingAudioSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="callWaitingAudioFile" type="{}ExtendedMediaFileLevelResource20" minOccurs="0"/>
 *         <element name="callWaitingVideoSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="callWaitingVideoFile" type="{}ExtendedMediaFileLevelResource20" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCustomRingbackUserModifyCriteriaRequest20", propOrder = {
    "userId",
    "criteriaName",
    "newCriteriaName",
    "timeSchedule",
    "holidaySchedule",
    "blacklisted",
    "fromDnCriteria",
    "callToNumberList",
    "audioSelection",
    "audioFile",
    "videoSelection",
    "videoFile",
    "callWaitingAudioSelection",
    "callWaitingAudioFile",
    "callWaitingVideoSelection",
    "callWaitingVideoFile"
})
public class UserCustomRingbackUserModifyCriteriaRequest20
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String criteriaName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newCriteriaName;
    @XmlElementRef(name = "timeSchedule", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeSchedule> timeSchedule;
    @XmlElementRef(name = "holidaySchedule", type = JAXBElement.class, required = false)
    protected JAXBElement<HolidaySchedule> holidaySchedule;
    protected Boolean blacklisted;
    protected CriteriaFromDnModify fromDnCriteria;
    @XmlElementRef(name = "callToNumberList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementCallToNumberList> callToNumberList;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection audioSelection;
    protected ExtendedMediaFileLevelResource20 audioFile;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection videoSelection;
    protected ExtendedMediaFileLevelResource20 videoFile;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection callWaitingAudioSelection;
    protected ExtendedMediaFileLevelResource20 callWaitingAudioFile;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection callWaitingVideoSelection;
    protected ExtendedMediaFileLevelResource20 callWaitingVideoFile;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der criteriaName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCriteriaName() {
        return criteriaName;
    }

    /**
     * Legt den Wert der criteriaName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCriteriaName(String value) {
        this.criteriaName = value;
    }

    /**
     * Ruft den Wert der newCriteriaName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewCriteriaName() {
        return newCriteriaName;
    }

    /**
     * Legt den Wert der newCriteriaName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewCriteriaName(String value) {
        this.newCriteriaName = value;
    }

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeSchedule }{@code >}
     *     
     */
    public JAXBElement<TimeSchedule> getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeSchedule }{@code >}
     *     
     */
    public void setTimeSchedule(JAXBElement<TimeSchedule> value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link HolidaySchedule }{@code >}
     *     
     */
    public JAXBElement<HolidaySchedule> getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link HolidaySchedule }{@code >}
     *     
     */
    public void setHolidaySchedule(JAXBElement<HolidaySchedule> value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der blacklisted-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBlacklisted() {
        return blacklisted;
    }

    /**
     * Legt den Wert der blacklisted-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBlacklisted(Boolean value) {
        this.blacklisted = value;
    }

    /**
     * Ruft den Wert der fromDnCriteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaFromDnModify }
     *     
     */
    public CriteriaFromDnModify getFromDnCriteria() {
        return fromDnCriteria;
    }

    /**
     * Legt den Wert der fromDnCriteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaFromDnModify }
     *     
     */
    public void setFromDnCriteria(CriteriaFromDnModify value) {
        this.fromDnCriteria = value;
    }

    /**
     * Ruft den Wert der callToNumberList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCallToNumberList }{@code >}
     *     
     */
    public JAXBElement<ReplacementCallToNumberList> getCallToNumberList() {
        return callToNumberList;
    }

    /**
     * Legt den Wert der callToNumberList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCallToNumberList }{@code >}
     *     
     */
    public void setCallToNumberList(JAXBElement<ReplacementCallToNumberList> value) {
        this.callToNumberList = value;
    }

    /**
     * Ruft den Wert der audioSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getAudioSelection() {
        return audioSelection;
    }

    /**
     * Legt den Wert der audioSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setAudioSelection(ExtendedFileResourceSelection value) {
        this.audioSelection = value;
    }

    /**
     * Ruft den Wert der audioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileLevelResource20 }
     *     
     */
    public ExtendedMediaFileLevelResource20 getAudioFile() {
        return audioFile;
    }

    /**
     * Legt den Wert der audioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileLevelResource20 }
     *     
     */
    public void setAudioFile(ExtendedMediaFileLevelResource20 value) {
        this.audioFile = value;
    }

    /**
     * Ruft den Wert der videoSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getVideoSelection() {
        return videoSelection;
    }

    /**
     * Legt den Wert der videoSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setVideoSelection(ExtendedFileResourceSelection value) {
        this.videoSelection = value;
    }

    /**
     * Ruft den Wert der videoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileLevelResource20 }
     *     
     */
    public ExtendedMediaFileLevelResource20 getVideoFile() {
        return videoFile;
    }

    /**
     * Legt den Wert der videoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileLevelResource20 }
     *     
     */
    public void setVideoFile(ExtendedMediaFileLevelResource20 value) {
        this.videoFile = value;
    }

    /**
     * Ruft den Wert der callWaitingAudioSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getCallWaitingAudioSelection() {
        return callWaitingAudioSelection;
    }

    /**
     * Legt den Wert der callWaitingAudioSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setCallWaitingAudioSelection(ExtendedFileResourceSelection value) {
        this.callWaitingAudioSelection = value;
    }

    /**
     * Ruft den Wert der callWaitingAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileLevelResource20 }
     *     
     */
    public ExtendedMediaFileLevelResource20 getCallWaitingAudioFile() {
        return callWaitingAudioFile;
    }

    /**
     * Legt den Wert der callWaitingAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileLevelResource20 }
     *     
     */
    public void setCallWaitingAudioFile(ExtendedMediaFileLevelResource20 value) {
        this.callWaitingAudioFile = value;
    }

    /**
     * Ruft den Wert der callWaitingVideoSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getCallWaitingVideoSelection() {
        return callWaitingVideoSelection;
    }

    /**
     * Legt den Wert der callWaitingVideoSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setCallWaitingVideoSelection(ExtendedFileResourceSelection value) {
        this.callWaitingVideoSelection = value;
    }

    /**
     * Ruft den Wert der callWaitingVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileLevelResource20 }
     *     
     */
    public ExtendedMediaFileLevelResource20 getCallWaitingVideoFile() {
        return callWaitingVideoFile;
    }

    /**
     * Legt den Wert der callWaitingVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileLevelResource20 }
     *     
     */
    public void setCallWaitingVideoFile(ExtendedMediaFileLevelResource20 value) {
        this.callWaitingVideoFile = value;
    }

}
