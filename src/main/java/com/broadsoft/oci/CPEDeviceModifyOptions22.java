//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         CPE device's options when used with a modify request.
 *         The following options are not changeable:
 *           configType
 *           systemFileName
 *           deviceFileFormat
 *       
 * 
 * <p>Java-Klasse für CPEDeviceModifyOptions22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CPEDeviceModifyOptions22">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="enableMonitoring" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="deviceManagementDeviceTypeOptions" type="{}DeviceManagementDeviceTypeModifyOptions22" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CPEDeviceModifyOptions22", propOrder = {
    "enableMonitoring",
    "deviceManagementDeviceTypeOptions"
})
public class CPEDeviceModifyOptions22 {

    protected Boolean enableMonitoring;
    protected DeviceManagementDeviceTypeModifyOptions22 deviceManagementDeviceTypeOptions;

    /**
     * Ruft den Wert der enableMonitoring-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMonitoring() {
        return enableMonitoring;
    }

    /**
     * Legt den Wert der enableMonitoring-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMonitoring(Boolean value) {
        this.enableMonitoring = value;
    }

    /**
     * Ruft den Wert der deviceManagementDeviceTypeOptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementDeviceTypeModifyOptions22 }
     *     
     */
    public DeviceManagementDeviceTypeModifyOptions22 getDeviceManagementDeviceTypeOptions() {
        return deviceManagementDeviceTypeOptions;
    }

    /**
     * Legt den Wert der deviceManagementDeviceTypeOptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementDeviceTypeModifyOptions22 }
     *     
     */
    public void setDeviceManagementDeviceTypeOptions(DeviceManagementDeviceTypeModifyOptions22 value) {
        this.deviceManagementDeviceTypeOptions = value;
    }

}
