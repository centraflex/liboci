//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SIPACLTransportProtocol.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SIPACLTransportProtocol">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="UDP"/>
 *     <enumeration value="TCP"/>
 *     <enumeration value="All"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SIPACLTransportProtocol")
@XmlEnum
public enum SIPACLTransportProtocol {

    UDP("UDP"),
    TCP("TCP"),
    @XmlEnumValue("All")
    ALL("All");
    private final String value;

    SIPACLTransportProtocol(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SIPACLTransportProtocol fromValue(String v) {
        for (SIPACLTransportProtocol c: SIPACLTransportProtocol.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
