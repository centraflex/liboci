//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für UnassignPhoneNumbersLevel.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="UnassignPhoneNumbersLevel">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Group"/>
 *     <enumeration value="Service Provider"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "UnassignPhoneNumbersLevel")
@XmlEnum
public enum UnassignPhoneNumbersLevel {

    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("Service Provider")
    SERVICE_PROVIDER("Service Provider");
    private final String value;

    UnassignPhoneNumbersLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UnassignPhoneNumbersLevel fromValue(String v) {
        for (UnassignPhoneNumbersLevel c: UnassignPhoneNumbersLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
