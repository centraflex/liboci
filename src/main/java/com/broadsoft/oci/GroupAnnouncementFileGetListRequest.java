//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Get the list of available announcement files for a Group.
 *         
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           groupExternalId
 *         
 *         The response is either a GroupAnnouncementFileGetListResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupAnnouncementFileGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAnnouncementFileGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <choice>
 *           <sequence>
 *             <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *             <element name="groupId" type="{}GroupId"/>
 *           </sequence>
 *           <element name="groupExternalId" type="{}ExternalId"/>
 *         </choice>
 *         <element name="announcementFileType" type="{}AnnouncementFileType" minOccurs="0"/>
 *         <element name="includeAnnouncementTable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaAnnouncementFileName" type="{}SearchCriteriaAnnouncementFileName" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAnnouncementFileGetListRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "groupExternalId",
    "announcementFileType",
    "includeAnnouncementTable",
    "responseSizeLimit",
    "searchCriteriaAnnouncementFileName"
})
public class GroupAnnouncementFileGetListRequest
    extends OCIRequest
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupExternalId;
    @XmlSchemaType(name = "token")
    protected AnnouncementFileType announcementFileType;
    protected boolean includeAnnouncementTable;
    protected Integer responseSizeLimit;
    protected List<SearchCriteriaAnnouncementFileName> searchCriteriaAnnouncementFileName;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der groupExternalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupExternalId() {
        return groupExternalId;
    }

    /**
     * Legt den Wert der groupExternalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupExternalId(String value) {
        this.groupExternalId = value;
    }

    /**
     * Ruft den Wert der announcementFileType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileType }
     *     
     */
    public AnnouncementFileType getAnnouncementFileType() {
        return announcementFileType;
    }

    /**
     * Legt den Wert der announcementFileType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileType }
     *     
     */
    public void setAnnouncementFileType(AnnouncementFileType value) {
        this.announcementFileType = value;
    }

    /**
     * Ruft den Wert der includeAnnouncementTable-Eigenschaft ab.
     * 
     */
    public boolean isIncludeAnnouncementTable() {
        return includeAnnouncementTable;
    }

    /**
     * Legt den Wert der includeAnnouncementTable-Eigenschaft fest.
     * 
     */
    public void setIncludeAnnouncementTable(boolean value) {
        this.includeAnnouncementTable = value;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaAnnouncementFileName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaAnnouncementFileName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaAnnouncementFileName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaAnnouncementFileName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaAnnouncementFileName property.
     */
    public List<SearchCriteriaAnnouncementFileName> getSearchCriteriaAnnouncementFileName() {
        if (searchCriteriaAnnouncementFileName == null) {
            searchCriteriaAnnouncementFileName = new ArrayList<>();
        }
        return this.searchCriteriaAnnouncementFileName;
    }

}
