//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         User for admin read. Either all call centers or 2 lists of call centers: one for current and one for deleted call centers.
 *       
 * 
 * <p>Java-Klasse für CallCenterScheduledReportCallCenterSelectionRead complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterScheduledReportCallCenterSelectionRead">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <choice>
 *         <element name="allCallCenter" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <sequence>
 *           <element name="currentUserId" type="{}UserId" maxOccurs="100" minOccurs="0"/>
 *           <element name="pastUserId" type="{}UserId" maxOccurs="100" minOccurs="0"/>
 *         </sequence>
 *       </choice>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterScheduledReportCallCenterSelectionRead", propOrder = {
    "allCallCenter",
    "currentUserId",
    "pastUserId"
})
public class CallCenterScheduledReportCallCenterSelectionRead {

    protected Boolean allCallCenter;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> currentUserId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> pastUserId;

    /**
     * Ruft den Wert der allCallCenter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllCallCenter() {
        return allCallCenter;
    }

    /**
     * Legt den Wert der allCallCenter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllCallCenter(Boolean value) {
        this.allCallCenter = value;
    }

    /**
     * Gets the value of the currentUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the currentUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCurrentUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the currentUserId property.
     */
    public List<String> getCurrentUserId() {
        if (currentUserId == null) {
            currentUserId = new ArrayList<>();
        }
        return this.currentUserId;
    }

    /**
     * Gets the value of the pastUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the pastUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPastUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the pastUserId property.
     */
    public List<String> getPastUserId() {
        if (pastUserId == null) {
            pastUserId = new ArrayList<>();
        }
        return this.pastUserId;
    }

}
