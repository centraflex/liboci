//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to get the User Personal Assistant Exclusion Number List information. 
 *         The response is either a userPersonalAssistantExclusionNumberGetListResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserPersonalAssistantExclusionNumberGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPersonalAssistantExclusionNumberGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="searchCriteriaPersonalAssistantExclusionNumber" type="{}SearchCriteriaPersonalAssistantExclusionNumber" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaPersonalAssistantExclusionNumberDescription" type="{}SearchCriteriaPersonalAssistantExclusionNumberDescription" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPersonalAssistantExclusionNumberGetListRequest", propOrder = {
    "userId",
    "searchCriteriaPersonalAssistantExclusionNumber",
    "searchCriteriaPersonalAssistantExclusionNumberDescription",
    "responseSizeLimit"
})
public class UserPersonalAssistantExclusionNumberGetListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected List<SearchCriteriaPersonalAssistantExclusionNumber> searchCriteriaPersonalAssistantExclusionNumber;
    protected List<SearchCriteriaPersonalAssistantExclusionNumberDescription> searchCriteriaPersonalAssistantExclusionNumberDescription;
    protected Integer responseSizeLimit;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the searchCriteriaPersonalAssistantExclusionNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaPersonalAssistantExclusionNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaPersonalAssistantExclusionNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaPersonalAssistantExclusionNumber }
     * 
     * 
     * @return
     *     The value of the searchCriteriaPersonalAssistantExclusionNumber property.
     */
    public List<SearchCriteriaPersonalAssistantExclusionNumber> getSearchCriteriaPersonalAssistantExclusionNumber() {
        if (searchCriteriaPersonalAssistantExclusionNumber == null) {
            searchCriteriaPersonalAssistantExclusionNumber = new ArrayList<>();
        }
        return this.searchCriteriaPersonalAssistantExclusionNumber;
    }

    /**
     * Gets the value of the searchCriteriaPersonalAssistantExclusionNumberDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaPersonalAssistantExclusionNumberDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaPersonalAssistantExclusionNumberDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaPersonalAssistantExclusionNumberDescription }
     * 
     * 
     * @return
     *     The value of the searchCriteriaPersonalAssistantExclusionNumberDescription property.
     */
    public List<SearchCriteriaPersonalAssistantExclusionNumberDescription> getSearchCriteriaPersonalAssistantExclusionNumberDescription() {
        if (searchCriteriaPersonalAssistantExclusionNumberDescription == null) {
            searchCriteriaPersonalAssistantExclusionNumberDescription = new ArrayList<>();
        }
        return this.searchCriteriaPersonalAssistantExclusionNumberDescription;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

}
