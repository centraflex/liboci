//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AutoAttendantNameDialingEntry.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AutoAttendantNameDialingEntry">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="LastName + FirstName"/>
 *     <enumeration value="LastName + FirstName or FirstName + LastName"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AutoAttendantNameDialingEntry")
@XmlEnum
public enum AutoAttendantNameDialingEntry {

    @XmlEnumValue("LastName + FirstName")
    LAST_NAME_FIRST_NAME("LastName + FirstName"),
    @XmlEnumValue("LastName + FirstName or FirstName + LastName")
    LAST_NAME_FIRST_NAME_OR_FIRST_NAME_LAST_NAME("LastName + FirstName or FirstName + LastName");
    private final String value;

    AutoAttendantNameDialingEntry(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AutoAttendantNameDialingEntry fromValue(String v) {
        for (AutoAttendantNameDialingEntry c: AutoAttendantNameDialingEntry.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
