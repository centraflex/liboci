//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the service provider's password rules settings
 *         setting applicable to Administrators (Group and Department) and Users.
 *         
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *         forcePasswordChangeAfterReset
 *         
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderPasswordRulesModifyRequest14sp3 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderPasswordRulesModifyRequest14sp3">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="rulesApplyTo" type="{}ServiceProviderPasswordRulesApplyTo" minOccurs="0"/>
 *         <element name="allowWebAddExternalAuthenticationUsers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disallowUserId" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disallowOldPassword" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disallowReversedOldPassword" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="restrictMinDigits" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="minDigits" type="{}PasswordMinDigits" minOccurs="0"/>
 *         <element name="restrictMinUpperCaseLetters" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="minUpperCaseLetters" type="{}PasswordMinUpperCaseLetters" minOccurs="0"/>
 *         <element name="restrictMinLowerCaseLetters" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="minLowerCaseLetters" type="{}PasswordMinLowerCaseLetters" minOccurs="0"/>
 *         <element name="restrictMinNonAlphanumericCharacters" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="minNonAlphanumericCharacters" type="{}PasswordMinNonAlphanumericCharacters" minOccurs="0"/>
 *         <element name="minLength" type="{}PasswordMinLength" minOccurs="0"/>
 *         <element name="maxFailedLoginAttempts" type="{}MaxFailedLoginAttempts" minOccurs="0"/>
 *         <element name="passwordExpiresDays" type="{}PasswordExpiresDays" minOccurs="0"/>
 *         <element name="sendLoginDisabledNotifyEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="loginDisabledNotifyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="disallowPreviousPasswords" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="numberOfPreviousPasswords" type="{}PasswordHistoryCount" minOccurs="0"/>
 *         <element name="forcePasswordChangeAfterReset" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderPasswordRulesModifyRequest14sp3", propOrder = {
    "serviceProviderId",
    "rulesApplyTo",
    "allowWebAddExternalAuthenticationUsers",
    "disallowUserId",
    "disallowOldPassword",
    "disallowReversedOldPassword",
    "restrictMinDigits",
    "minDigits",
    "restrictMinUpperCaseLetters",
    "minUpperCaseLetters",
    "restrictMinLowerCaseLetters",
    "minLowerCaseLetters",
    "restrictMinNonAlphanumericCharacters",
    "minNonAlphanumericCharacters",
    "minLength",
    "maxFailedLoginAttempts",
    "passwordExpiresDays",
    "sendLoginDisabledNotifyEmail",
    "loginDisabledNotifyEmailAddress",
    "disallowPreviousPasswords",
    "numberOfPreviousPasswords",
    "forcePasswordChangeAfterReset"
})
public class ServiceProviderPasswordRulesModifyRequest14Sp3
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlSchemaType(name = "token")
    protected ServiceProviderPasswordRulesApplyTo rulesApplyTo;
    protected Boolean allowWebAddExternalAuthenticationUsers;
    protected Boolean disallowUserId;
    protected Boolean disallowOldPassword;
    protected Boolean disallowReversedOldPassword;
    protected Boolean restrictMinDigits;
    protected Integer minDigits;
    protected Boolean restrictMinUpperCaseLetters;
    protected Integer minUpperCaseLetters;
    protected Boolean restrictMinLowerCaseLetters;
    protected Integer minLowerCaseLetters;
    protected Boolean restrictMinNonAlphanumericCharacters;
    protected Integer minNonAlphanumericCharacters;
    protected Integer minLength;
    protected Integer maxFailedLoginAttempts;
    protected Integer passwordExpiresDays;
    protected Boolean sendLoginDisabledNotifyEmail;
    @XmlElementRef(name = "loginDisabledNotifyEmailAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> loginDisabledNotifyEmailAddress;
    protected Boolean disallowPreviousPasswords;
    protected Integer numberOfPreviousPasswords;
    protected Boolean forcePasswordChangeAfterReset;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der rulesApplyTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderPasswordRulesApplyTo }
     *     
     */
    public ServiceProviderPasswordRulesApplyTo getRulesApplyTo() {
        return rulesApplyTo;
    }

    /**
     * Legt den Wert der rulesApplyTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderPasswordRulesApplyTo }
     *     
     */
    public void setRulesApplyTo(ServiceProviderPasswordRulesApplyTo value) {
        this.rulesApplyTo = value;
    }

    /**
     * Ruft den Wert der allowWebAddExternalAuthenticationUsers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowWebAddExternalAuthenticationUsers() {
        return allowWebAddExternalAuthenticationUsers;
    }

    /**
     * Legt den Wert der allowWebAddExternalAuthenticationUsers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowWebAddExternalAuthenticationUsers(Boolean value) {
        this.allowWebAddExternalAuthenticationUsers = value;
    }

    /**
     * Ruft den Wert der disallowUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowUserId() {
        return disallowUserId;
    }

    /**
     * Legt den Wert der disallowUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowUserId(Boolean value) {
        this.disallowUserId = value;
    }

    /**
     * Ruft den Wert der disallowOldPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowOldPassword() {
        return disallowOldPassword;
    }

    /**
     * Legt den Wert der disallowOldPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowOldPassword(Boolean value) {
        this.disallowOldPassword = value;
    }

    /**
     * Ruft den Wert der disallowReversedOldPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowReversedOldPassword() {
        return disallowReversedOldPassword;
    }

    /**
     * Legt den Wert der disallowReversedOldPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowReversedOldPassword(Boolean value) {
        this.disallowReversedOldPassword = value;
    }

    /**
     * Ruft den Wert der restrictMinDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictMinDigits() {
        return restrictMinDigits;
    }

    /**
     * Legt den Wert der restrictMinDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictMinDigits(Boolean value) {
        this.restrictMinDigits = value;
    }

    /**
     * Ruft den Wert der minDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinDigits() {
        return minDigits;
    }

    /**
     * Legt den Wert der minDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinDigits(Integer value) {
        this.minDigits = value;
    }

    /**
     * Ruft den Wert der restrictMinUpperCaseLetters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictMinUpperCaseLetters() {
        return restrictMinUpperCaseLetters;
    }

    /**
     * Legt den Wert der restrictMinUpperCaseLetters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictMinUpperCaseLetters(Boolean value) {
        this.restrictMinUpperCaseLetters = value;
    }

    /**
     * Ruft den Wert der minUpperCaseLetters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinUpperCaseLetters() {
        return minUpperCaseLetters;
    }

    /**
     * Legt den Wert der minUpperCaseLetters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinUpperCaseLetters(Integer value) {
        this.minUpperCaseLetters = value;
    }

    /**
     * Ruft den Wert der restrictMinLowerCaseLetters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictMinLowerCaseLetters() {
        return restrictMinLowerCaseLetters;
    }

    /**
     * Legt den Wert der restrictMinLowerCaseLetters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictMinLowerCaseLetters(Boolean value) {
        this.restrictMinLowerCaseLetters = value;
    }

    /**
     * Ruft den Wert der minLowerCaseLetters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinLowerCaseLetters() {
        return minLowerCaseLetters;
    }

    /**
     * Legt den Wert der minLowerCaseLetters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinLowerCaseLetters(Integer value) {
        this.minLowerCaseLetters = value;
    }

    /**
     * Ruft den Wert der restrictMinNonAlphanumericCharacters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictMinNonAlphanumericCharacters() {
        return restrictMinNonAlphanumericCharacters;
    }

    /**
     * Legt den Wert der restrictMinNonAlphanumericCharacters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictMinNonAlphanumericCharacters(Boolean value) {
        this.restrictMinNonAlphanumericCharacters = value;
    }

    /**
     * Ruft den Wert der minNonAlphanumericCharacters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinNonAlphanumericCharacters() {
        return minNonAlphanumericCharacters;
    }

    /**
     * Legt den Wert der minNonAlphanumericCharacters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinNonAlphanumericCharacters(Integer value) {
        this.minNonAlphanumericCharacters = value;
    }

    /**
     * Ruft den Wert der minLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinLength() {
        return minLength;
    }

    /**
     * Legt den Wert der minLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinLength(Integer value) {
        this.minLength = value;
    }

    /**
     * Ruft den Wert der maxFailedLoginAttempts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxFailedLoginAttempts() {
        return maxFailedLoginAttempts;
    }

    /**
     * Legt den Wert der maxFailedLoginAttempts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxFailedLoginAttempts(Integer value) {
        this.maxFailedLoginAttempts = value;
    }

    /**
     * Ruft den Wert der passwordExpiresDays-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPasswordExpiresDays() {
        return passwordExpiresDays;
    }

    /**
     * Legt den Wert der passwordExpiresDays-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPasswordExpiresDays(Integer value) {
        this.passwordExpiresDays = value;
    }

    /**
     * Ruft den Wert der sendLoginDisabledNotifyEmail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendLoginDisabledNotifyEmail() {
        return sendLoginDisabledNotifyEmail;
    }

    /**
     * Legt den Wert der sendLoginDisabledNotifyEmail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendLoginDisabledNotifyEmail(Boolean value) {
        this.sendLoginDisabledNotifyEmail = value;
    }

    /**
     * Ruft den Wert der loginDisabledNotifyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLoginDisabledNotifyEmailAddress() {
        return loginDisabledNotifyEmailAddress;
    }

    /**
     * Legt den Wert der loginDisabledNotifyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLoginDisabledNotifyEmailAddress(JAXBElement<String> value) {
        this.loginDisabledNotifyEmailAddress = value;
    }

    /**
     * Ruft den Wert der disallowPreviousPasswords-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowPreviousPasswords() {
        return disallowPreviousPasswords;
    }

    /**
     * Legt den Wert der disallowPreviousPasswords-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowPreviousPasswords(Boolean value) {
        this.disallowPreviousPasswords = value;
    }

    /**
     * Ruft den Wert der numberOfPreviousPasswords-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfPreviousPasswords() {
        return numberOfPreviousPasswords;
    }

    /**
     * Legt den Wert der numberOfPreviousPasswords-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfPreviousPasswords(Integer value) {
        this.numberOfPreviousPasswords = value;
    }

    /**
     * Ruft den Wert der forcePasswordChangeAfterReset-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForcePasswordChangeAfterReset() {
        return forcePasswordChangeAfterReset;
    }

    /**
     * Legt den Wert der forcePasswordChangeAfterReset-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForcePasswordChangeAfterReset(Boolean value) {
        this.forcePasswordChangeAfterReset = value;
    }

}
