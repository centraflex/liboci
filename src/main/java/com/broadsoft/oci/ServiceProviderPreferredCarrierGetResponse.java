//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to a ServiceProviderPreferredCarrierGetRequest.
 *         Contains the currently configured carriers for a specified country code for a service provider / enterprise.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderPreferredCarrierGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderPreferredCarrierGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="intraLataCarrier" type="{}PreferredCarrierName" minOccurs="0"/>
 *         <element name="interLataCarrier" type="{}PreferredCarrierName" minOccurs="0"/>
 *         <element name="internationalCarrier" type="{}PreferredCarrierName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderPreferredCarrierGetResponse", propOrder = {
    "intraLataCarrier",
    "interLataCarrier",
    "internationalCarrier"
})
public class ServiceProviderPreferredCarrierGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String intraLataCarrier;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String interLataCarrier;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String internationalCarrier;

    /**
     * Ruft den Wert der intraLataCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntraLataCarrier() {
        return intraLataCarrier;
    }

    /**
     * Legt den Wert der intraLataCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntraLataCarrier(String value) {
        this.intraLataCarrier = value;
    }

    /**
     * Ruft den Wert der interLataCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterLataCarrier() {
        return interLataCarrier;
    }

    /**
     * Legt den Wert der interLataCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterLataCarrier(String value) {
        this.interLataCarrier = value;
    }

    /**
     * Ruft den Wert der internationalCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternationalCarrier() {
        return internationalCarrier;
    }

    /**
     * Legt den Wert der internationalCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternationalCarrier(String value) {
        this.internationalCarrier = value;
    }

}
