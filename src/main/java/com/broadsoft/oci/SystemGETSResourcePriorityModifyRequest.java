//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a GETS reserved Resource Priority.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemGETSResourcePriorityModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGETSResourcePriorityModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="priorityValue" type="{}GETSPriorityValue"/>
 *         <element name="newPriorityValue" type="{}GETSPriorityValue" minOccurs="0"/>
 *         <element name="priorityLevel" type="{}GETSPriorityLevel" minOccurs="0"/>
 *         <element name="priorityClass" type="{}GETSPriorityClass" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGETSResourcePriorityModifyRequest", propOrder = {
    "priorityValue",
    "newPriorityValue",
    "priorityLevel",
    "priorityClass"
})
public class SystemGETSResourcePriorityModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String priorityValue;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newPriorityValue;
    protected Integer priorityLevel;
    @XmlSchemaType(name = "token")
    protected GETSPriorityClass priorityClass;

    /**
     * Ruft den Wert der priorityValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriorityValue() {
        return priorityValue;
    }

    /**
     * Legt den Wert der priorityValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriorityValue(String value) {
        this.priorityValue = value;
    }

    /**
     * Ruft den Wert der newPriorityValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewPriorityValue() {
        return newPriorityValue;
    }

    /**
     * Legt den Wert der newPriorityValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewPriorityValue(String value) {
        this.newPriorityValue = value;
    }

    /**
     * Ruft den Wert der priorityLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPriorityLevel() {
        return priorityLevel;
    }

    /**
     * Legt den Wert der priorityLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPriorityLevel(Integer value) {
        this.priorityLevel = value;
    }

    /**
     * Ruft den Wert der priorityClass-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GETSPriorityClass }
     *     
     */
    public GETSPriorityClass getPriorityClass() {
        return priorityClass;
    }

    /**
     * Legt den Wert der priorityClass-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GETSPriorityClass }
     *     
     */
    public void setPriorityClass(GETSPriorityClass value) {
        this.priorityClass = value;
    }

}
