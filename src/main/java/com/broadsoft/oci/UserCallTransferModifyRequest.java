//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the user level data associated with Call Transfer.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserCallTransferModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallTransferModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="isRecallActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="recallNumberOfRings" type="{}CallTransferRecallNumberOfRings" minOccurs="0"/>
 *         <element name="useDiversionInhibitorForBlindTransfer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useDiversionInhibitorForConsultativeCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableBusyCampOn" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="busyCampOnSeconds" type="{}CallTransferBusyCampOnSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallTransferModifyRequest", propOrder = {
    "userId",
    "isRecallActive",
    "recallNumberOfRings",
    "useDiversionInhibitorForBlindTransfer",
    "useDiversionInhibitorForConsultativeCalls",
    "enableBusyCampOn",
    "busyCampOnSeconds"
})
public class UserCallTransferModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean isRecallActive;
    protected Integer recallNumberOfRings;
    protected Boolean useDiversionInhibitorForBlindTransfer;
    protected Boolean useDiversionInhibitorForConsultativeCalls;
    protected Boolean enableBusyCampOn;
    protected Integer busyCampOnSeconds;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der isRecallActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRecallActive() {
        return isRecallActive;
    }

    /**
     * Legt den Wert der isRecallActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRecallActive(Boolean value) {
        this.isRecallActive = value;
    }

    /**
     * Ruft den Wert der recallNumberOfRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRecallNumberOfRings() {
        return recallNumberOfRings;
    }

    /**
     * Legt den Wert der recallNumberOfRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRecallNumberOfRings(Integer value) {
        this.recallNumberOfRings = value;
    }

    /**
     * Ruft den Wert der useDiversionInhibitorForBlindTransfer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseDiversionInhibitorForBlindTransfer() {
        return useDiversionInhibitorForBlindTransfer;
    }

    /**
     * Legt den Wert der useDiversionInhibitorForBlindTransfer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseDiversionInhibitorForBlindTransfer(Boolean value) {
        this.useDiversionInhibitorForBlindTransfer = value;
    }

    /**
     * Ruft den Wert der useDiversionInhibitorForConsultativeCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseDiversionInhibitorForConsultativeCalls() {
        return useDiversionInhibitorForConsultativeCalls;
    }

    /**
     * Legt den Wert der useDiversionInhibitorForConsultativeCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseDiversionInhibitorForConsultativeCalls(Boolean value) {
        this.useDiversionInhibitorForConsultativeCalls = value;
    }

    /**
     * Ruft den Wert der enableBusyCampOn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableBusyCampOn() {
        return enableBusyCampOn;
    }

    /**
     * Legt den Wert der enableBusyCampOn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableBusyCampOn(Boolean value) {
        this.enableBusyCampOn = value;
    }

    /**
     * Ruft den Wert der busyCampOnSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBusyCampOnSeconds() {
        return busyCampOnSeconds;
    }

    /**
     * Legt den Wert der busyCampOnSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBusyCampOnSeconds(Integer value) {
        this.busyCampOnSeconds = value;
    }

}
