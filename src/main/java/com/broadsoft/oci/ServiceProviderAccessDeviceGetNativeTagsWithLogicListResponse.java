//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderAccessDeviceGetNativeTagsWithLogicListRequest.
 *         Contains a table of all native tags with logic managed by the Device Management System on a per-device profile basis.
 * 
 *         The column headings are: "Tag Name", "Tag Value".
 *       
 * 
 * <p>Java-Klasse für ServiceProviderAccessDeviceGetNativeTagsWithLogicListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderAccessDeviceGetNativeTagsWithLogicListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="deviceNativeTagsWithLogicTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderAccessDeviceGetNativeTagsWithLogicListResponse", propOrder = {
    "deviceNativeTagsWithLogicTable"
})
public class ServiceProviderAccessDeviceGetNativeTagsWithLogicListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable deviceNativeTagsWithLogicTable;

    /**
     * Ruft den Wert der deviceNativeTagsWithLogicTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDeviceNativeTagsWithLogicTable() {
        return deviceNativeTagsWithLogicTable;
    }

    /**
     * Legt den Wert der deviceNativeTagsWithLogicTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDeviceNativeTagsWithLogicTable(OCITable value) {
        this.deviceNativeTagsWithLogicTable = value;
    }

}
