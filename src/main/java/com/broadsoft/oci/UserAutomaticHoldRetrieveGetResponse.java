//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserAutomaticHoldRetrieveGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserAutomaticHoldRetrieveGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserAutomaticHoldRetrieveGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="recallTimerSeconds" type="{}AutomaticHoldRetrieveRecallTimerSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserAutomaticHoldRetrieveGetResponse", propOrder = {
    "isActive",
    "recallTimerSeconds"
})
public class UserAutomaticHoldRetrieveGetResponse
    extends OCIDataResponse
{

    protected boolean isActive;
    protected int recallTimerSeconds;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der recallTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getRecallTimerSeconds() {
        return recallTimerSeconds;
    }

    /**
     * Legt den Wert der recallTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setRecallTimerSeconds(int value) {
        this.recallTimerSeconds = value;
    }

}
