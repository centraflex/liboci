//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Get a list of BroadWorks Mobility IMRN numbers from a Mobile Network.
 *         The response is either a SystemBroadWorksMobilityGetMobileNetworkIMRNListResponse
 *         or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobilityGetMobileNetworkIMRNListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobilityGetMobileNetworkIMRNListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="mobileNetworkName" type="{}BroadWorksMobilityMobileNetworkName"/>
 *         <element name="searchCriteriaIMRN" type="{}SearchCriteriaIMRN" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobilityGetMobileNetworkIMRNListRequest", propOrder = {
    "mobileNetworkName",
    "searchCriteriaIMRN"
})
public class SystemBroadWorksMobilityGetMobileNetworkIMRNListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mobileNetworkName;
    protected List<SearchCriteriaIMRN> searchCriteriaIMRN;

    /**
     * Ruft den Wert der mobileNetworkName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobileNetworkName() {
        return mobileNetworkName;
    }

    /**
     * Legt den Wert der mobileNetworkName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobileNetworkName(String value) {
        this.mobileNetworkName = value;
    }

    /**
     * Gets the value of the searchCriteriaIMRN property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaIMRN property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaIMRN().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaIMRN }
     * 
     * 
     * @return
     *     The value of the searchCriteriaIMRN property.
     */
    public List<SearchCriteriaIMRN> getSearchCriteriaIMRN() {
        if (searchCriteriaIMRN == null) {
            searchCriteriaIMRN = new ArrayList<>();
        }
        return this.searchCriteriaIMRN;
    }

}
