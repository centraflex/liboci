//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modifies the System Diameter base parameters.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemBwDiameterBaseDataModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBwDiameterBaseDataModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="xsRealm" type="{}DomainName" minOccurs="0"/>
 *         <element name="xsListeningPort" type="{}Port1025" minOccurs="0"/>
 *         <element name="xsListeningPortEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="xsListeningSecurePort" type="{}Port1025" minOccurs="0"/>
 *         <element name="xsListeningSecurePortEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="psRealm" type="{}DomainName" minOccurs="0"/>
 *         <element name="psListeningPort" type="{}Port1025" minOccurs="0"/>
 *         <element name="psListeningPortEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="psListeningSecurePort" type="{}Port1025" minOccurs="0"/>
 *         <element name="psListeningSecurePortEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="psRelayThroughXs" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="xsRelayListeningPort" type="{}Port1025" minOccurs="0"/>
 *         <element name="tcTimerSeconds" type="{}BwDiameterTcTimerSeconds" minOccurs="0"/>
 *         <element name="twTimerSeconds" type="{}BwDiameterTwTimerSeconds" minOccurs="0"/>
 *         <element name="requestTimerSeconds" type="{}BwDiameterRequestTimerSeconds" minOccurs="0"/>
 *         <element name="busyPeerDetectionOutstandingTxnCount" type="{}BwDiameterBusyPeerOutstandingTxnCount" minOccurs="0"/>
 *         <element name="busyPeerRestoreOutstandingTxnCount" type="{}BwDiameterBusyPeerOutstandingTxnCount" minOccurs="0"/>
 *         <element name="dynamicEntryInactivityTimerHours" type="{}BwDiameterDynamicEntryInactivityTimerHours" minOccurs="0"/>
 *         <element name="advertisedOfflineBillingApplication" type="{}BwDiameterAdvertisedApplication" minOccurs="0"/>
 *         <element name="advertisedOnlineBillingApplication" type="{}BwDiameterAdvertisedApplication" minOccurs="0"/>
 *         <element name="defaultPort" type="{}Port1025" minOccurs="0"/>
 *         <element name="defaultSecurePort" type="{}Port1025" minOccurs="0"/>
 *         <element name="peerDiscoveryMode" type="{}DiameterPeerDiscoveryMode" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBwDiameterBaseDataModifyRequest", propOrder = {
    "xsRealm",
    "xsListeningPort",
    "xsListeningPortEnabled",
    "xsListeningSecurePort",
    "xsListeningSecurePortEnabled",
    "psRealm",
    "psListeningPort",
    "psListeningPortEnabled",
    "psListeningSecurePort",
    "psListeningSecurePortEnabled",
    "psRelayThroughXs",
    "xsRelayListeningPort",
    "tcTimerSeconds",
    "twTimerSeconds",
    "requestTimerSeconds",
    "busyPeerDetectionOutstandingTxnCount",
    "busyPeerRestoreOutstandingTxnCount",
    "dynamicEntryInactivityTimerHours",
    "advertisedOfflineBillingApplication",
    "advertisedOnlineBillingApplication",
    "defaultPort",
    "defaultSecurePort",
    "peerDiscoveryMode"
})
public class SystemBwDiameterBaseDataModifyRequest
    extends OCIRequest
{

    @XmlElementRef(name = "xsRealm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> xsRealm;
    protected Integer xsListeningPort;
    protected Boolean xsListeningPortEnabled;
    protected Integer xsListeningSecurePort;
    protected Boolean xsListeningSecurePortEnabled;
    @XmlElementRef(name = "psRealm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> psRealm;
    protected Integer psListeningPort;
    protected Boolean psListeningPortEnabled;
    protected Integer psListeningSecurePort;
    protected Boolean psListeningSecurePortEnabled;
    protected Boolean psRelayThroughXs;
    protected Integer xsRelayListeningPort;
    protected Integer tcTimerSeconds;
    protected Integer twTimerSeconds;
    protected Integer requestTimerSeconds;
    protected Integer busyPeerDetectionOutstandingTxnCount;
    protected Integer busyPeerRestoreOutstandingTxnCount;
    protected Integer dynamicEntryInactivityTimerHours;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String advertisedOfflineBillingApplication;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String advertisedOnlineBillingApplication;
    protected Integer defaultPort;
    protected Integer defaultSecurePort;
    @XmlSchemaType(name = "token")
    protected DiameterPeerDiscoveryMode peerDiscoveryMode;

    /**
     * Ruft den Wert der xsRealm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getXsRealm() {
        return xsRealm;
    }

    /**
     * Legt den Wert der xsRealm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setXsRealm(JAXBElement<String> value) {
        this.xsRealm = value;
    }

    /**
     * Ruft den Wert der xsListeningPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getXsListeningPort() {
        return xsListeningPort;
    }

    /**
     * Legt den Wert der xsListeningPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setXsListeningPort(Integer value) {
        this.xsListeningPort = value;
    }

    /**
     * Ruft den Wert der xsListeningPortEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isXsListeningPortEnabled() {
        return xsListeningPortEnabled;
    }

    /**
     * Legt den Wert der xsListeningPortEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setXsListeningPortEnabled(Boolean value) {
        this.xsListeningPortEnabled = value;
    }

    /**
     * Ruft den Wert der xsListeningSecurePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getXsListeningSecurePort() {
        return xsListeningSecurePort;
    }

    /**
     * Legt den Wert der xsListeningSecurePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setXsListeningSecurePort(Integer value) {
        this.xsListeningSecurePort = value;
    }

    /**
     * Ruft den Wert der xsListeningSecurePortEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isXsListeningSecurePortEnabled() {
        return xsListeningSecurePortEnabled;
    }

    /**
     * Legt den Wert der xsListeningSecurePortEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setXsListeningSecurePortEnabled(Boolean value) {
        this.xsListeningSecurePortEnabled = value;
    }

    /**
     * Ruft den Wert der psRealm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPsRealm() {
        return psRealm;
    }

    /**
     * Legt den Wert der psRealm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPsRealm(JAXBElement<String> value) {
        this.psRealm = value;
    }

    /**
     * Ruft den Wert der psListeningPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPsListeningPort() {
        return psListeningPort;
    }

    /**
     * Legt den Wert der psListeningPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPsListeningPort(Integer value) {
        this.psListeningPort = value;
    }

    /**
     * Ruft den Wert der psListeningPortEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPsListeningPortEnabled() {
        return psListeningPortEnabled;
    }

    /**
     * Legt den Wert der psListeningPortEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPsListeningPortEnabled(Boolean value) {
        this.psListeningPortEnabled = value;
    }

    /**
     * Ruft den Wert der psListeningSecurePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPsListeningSecurePort() {
        return psListeningSecurePort;
    }

    /**
     * Legt den Wert der psListeningSecurePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPsListeningSecurePort(Integer value) {
        this.psListeningSecurePort = value;
    }

    /**
     * Ruft den Wert der psListeningSecurePortEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPsListeningSecurePortEnabled() {
        return psListeningSecurePortEnabled;
    }

    /**
     * Legt den Wert der psListeningSecurePortEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPsListeningSecurePortEnabled(Boolean value) {
        this.psListeningSecurePortEnabled = value;
    }

    /**
     * Ruft den Wert der psRelayThroughXs-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPsRelayThroughXs() {
        return psRelayThroughXs;
    }

    /**
     * Legt den Wert der psRelayThroughXs-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPsRelayThroughXs(Boolean value) {
        this.psRelayThroughXs = value;
    }

    /**
     * Ruft den Wert der xsRelayListeningPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getXsRelayListeningPort() {
        return xsRelayListeningPort;
    }

    /**
     * Legt den Wert der xsRelayListeningPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setXsRelayListeningPort(Integer value) {
        this.xsRelayListeningPort = value;
    }

    /**
     * Ruft den Wert der tcTimerSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTcTimerSeconds() {
        return tcTimerSeconds;
    }

    /**
     * Legt den Wert der tcTimerSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTcTimerSeconds(Integer value) {
        this.tcTimerSeconds = value;
    }

    /**
     * Ruft den Wert der twTimerSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTwTimerSeconds() {
        return twTimerSeconds;
    }

    /**
     * Legt den Wert der twTimerSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTwTimerSeconds(Integer value) {
        this.twTimerSeconds = value;
    }

    /**
     * Ruft den Wert der requestTimerSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRequestTimerSeconds() {
        return requestTimerSeconds;
    }

    /**
     * Legt den Wert der requestTimerSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRequestTimerSeconds(Integer value) {
        this.requestTimerSeconds = value;
    }

    /**
     * Ruft den Wert der busyPeerDetectionOutstandingTxnCount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBusyPeerDetectionOutstandingTxnCount() {
        return busyPeerDetectionOutstandingTxnCount;
    }

    /**
     * Legt den Wert der busyPeerDetectionOutstandingTxnCount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBusyPeerDetectionOutstandingTxnCount(Integer value) {
        this.busyPeerDetectionOutstandingTxnCount = value;
    }

    /**
     * Ruft den Wert der busyPeerRestoreOutstandingTxnCount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBusyPeerRestoreOutstandingTxnCount() {
        return busyPeerRestoreOutstandingTxnCount;
    }

    /**
     * Legt den Wert der busyPeerRestoreOutstandingTxnCount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBusyPeerRestoreOutstandingTxnCount(Integer value) {
        this.busyPeerRestoreOutstandingTxnCount = value;
    }

    /**
     * Ruft den Wert der dynamicEntryInactivityTimerHours-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDynamicEntryInactivityTimerHours() {
        return dynamicEntryInactivityTimerHours;
    }

    /**
     * Legt den Wert der dynamicEntryInactivityTimerHours-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDynamicEntryInactivityTimerHours(Integer value) {
        this.dynamicEntryInactivityTimerHours = value;
    }

    /**
     * Ruft den Wert der advertisedOfflineBillingApplication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvertisedOfflineBillingApplication() {
        return advertisedOfflineBillingApplication;
    }

    /**
     * Legt den Wert der advertisedOfflineBillingApplication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvertisedOfflineBillingApplication(String value) {
        this.advertisedOfflineBillingApplication = value;
    }

    /**
     * Ruft den Wert der advertisedOnlineBillingApplication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvertisedOnlineBillingApplication() {
        return advertisedOnlineBillingApplication;
    }

    /**
     * Legt den Wert der advertisedOnlineBillingApplication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvertisedOnlineBillingApplication(String value) {
        this.advertisedOnlineBillingApplication = value;
    }

    /**
     * Ruft den Wert der defaultPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDefaultPort() {
        return defaultPort;
    }

    /**
     * Legt den Wert der defaultPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDefaultPort(Integer value) {
        this.defaultPort = value;
    }

    /**
     * Ruft den Wert der defaultSecurePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDefaultSecurePort() {
        return defaultSecurePort;
    }

    /**
     * Legt den Wert der defaultSecurePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDefaultSecurePort(Integer value) {
        this.defaultSecurePort = value;
    }

    /**
     * Ruft den Wert der peerDiscoveryMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DiameterPeerDiscoveryMode }
     *     
     */
    public DiameterPeerDiscoveryMode getPeerDiscoveryMode() {
        return peerDiscoveryMode;
    }

    /**
     * Legt den Wert der peerDiscoveryMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DiameterPeerDiscoveryMode }
     *     
     */
    public void setPeerDiscoveryMode(DiameterPeerDiscoveryMode value) {
        this.peerDiscoveryMode = value;
    }

}
