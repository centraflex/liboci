//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the UserCustomRingbackUserGetCriteriaRequest20.
 *         Replaced by: UserCustomRingbackUserGetCriteriaResponse21
 *       
 * 
 * <p>Java-Klasse für UserCustomRingbackUserGetCriteriaResponse20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCustomRingbackUserGetCriteriaResponse20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="timeSchedule" type="{}TimeSchedule" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}HolidaySchedule" minOccurs="0"/>
 *         <element name="blacklisted" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="fromDnCriteria" type="{}CriteriaFromDn"/>
 *         <element name="audioSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="audioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="audioFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="videoSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="videoFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="videoFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="callWaitingAudioSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="callWaitingAudioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="callWaitingAudioFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="callWaitingVideoSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="callWaitingVideoFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="callWaitingVideoFileUrl" type="{}URL" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCustomRingbackUserGetCriteriaResponse20", propOrder = {
    "timeSchedule",
    "holidaySchedule",
    "blacklisted",
    "fromDnCriteria",
    "audioSelection",
    "audioFile",
    "audioFileUrl",
    "videoSelection",
    "videoFile",
    "videoFileUrl",
    "callWaitingAudioSelection",
    "callWaitingAudioFile",
    "callWaitingAudioFileUrl",
    "callWaitingVideoSelection",
    "callWaitingVideoFile",
    "callWaitingVideoFileUrl"
})
public class UserCustomRingbackUserGetCriteriaResponse20
    extends OCIDataResponse
{

    protected TimeSchedule timeSchedule;
    protected HolidaySchedule holidaySchedule;
    protected boolean blacklisted;
    @XmlElement(required = true)
    protected CriteriaFromDn fromDnCriteria;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection audioSelection;
    protected AnnouncementFileLevelKey audioFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioFileUrl;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection videoSelection;
    protected AnnouncementFileLevelKey videoFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String videoFileUrl;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection callWaitingAudioSelection;
    protected AnnouncementFileLevelKey callWaitingAudioFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callWaitingAudioFileUrl;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection callWaitingVideoSelection;
    protected AnnouncementFileLevelKey callWaitingVideoFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callWaitingVideoFileUrl;

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeSchedule }
     *     
     */
    public TimeSchedule getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeSchedule }
     *     
     */
    public void setTimeSchedule(TimeSchedule value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HolidaySchedule }
     *     
     */
    public HolidaySchedule getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HolidaySchedule }
     *     
     */
    public void setHolidaySchedule(HolidaySchedule value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der blacklisted-Eigenschaft ab.
     * 
     */
    public boolean isBlacklisted() {
        return blacklisted;
    }

    /**
     * Legt den Wert der blacklisted-Eigenschaft fest.
     * 
     */
    public void setBlacklisted(boolean value) {
        this.blacklisted = value;
    }

    /**
     * Ruft den Wert der fromDnCriteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaFromDn }
     *     
     */
    public CriteriaFromDn getFromDnCriteria() {
        return fromDnCriteria;
    }

    /**
     * Legt den Wert der fromDnCriteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaFromDn }
     *     
     */
    public void setFromDnCriteria(CriteriaFromDn value) {
        this.fromDnCriteria = value;
    }

    /**
     * Ruft den Wert der audioSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getAudioSelection() {
        return audioSelection;
    }

    /**
     * Legt den Wert der audioSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setAudioSelection(ExtendedFileResourceSelection value) {
        this.audioSelection = value;
    }

    /**
     * Ruft den Wert der audioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getAudioFile() {
        return audioFile;
    }

    /**
     * Legt den Wert der audioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setAudioFile(AnnouncementFileLevelKey value) {
        this.audioFile = value;
    }

    /**
     * Ruft den Wert der audioFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioFileUrl() {
        return audioFileUrl;
    }

    /**
     * Legt den Wert der audioFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioFileUrl(String value) {
        this.audioFileUrl = value;
    }

    /**
     * Ruft den Wert der videoSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getVideoSelection() {
        return videoSelection;
    }

    /**
     * Legt den Wert der videoSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setVideoSelection(ExtendedFileResourceSelection value) {
        this.videoSelection = value;
    }

    /**
     * Ruft den Wert der videoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getVideoFile() {
        return videoFile;
    }

    /**
     * Legt den Wert der videoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setVideoFile(AnnouncementFileLevelKey value) {
        this.videoFile = value;
    }

    /**
     * Ruft den Wert der videoFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVideoFileUrl() {
        return videoFileUrl;
    }

    /**
     * Legt den Wert der videoFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVideoFileUrl(String value) {
        this.videoFileUrl = value;
    }

    /**
     * Ruft den Wert der callWaitingAudioSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getCallWaitingAudioSelection() {
        return callWaitingAudioSelection;
    }

    /**
     * Legt den Wert der callWaitingAudioSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setCallWaitingAudioSelection(ExtendedFileResourceSelection value) {
        this.callWaitingAudioSelection = value;
    }

    /**
     * Ruft den Wert der callWaitingAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getCallWaitingAudioFile() {
        return callWaitingAudioFile;
    }

    /**
     * Legt den Wert der callWaitingAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setCallWaitingAudioFile(AnnouncementFileLevelKey value) {
        this.callWaitingAudioFile = value;
    }

    /**
     * Ruft den Wert der callWaitingAudioFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallWaitingAudioFileUrl() {
        return callWaitingAudioFileUrl;
    }

    /**
     * Legt den Wert der callWaitingAudioFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallWaitingAudioFileUrl(String value) {
        this.callWaitingAudioFileUrl = value;
    }

    /**
     * Ruft den Wert der callWaitingVideoSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getCallWaitingVideoSelection() {
        return callWaitingVideoSelection;
    }

    /**
     * Legt den Wert der callWaitingVideoSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setCallWaitingVideoSelection(ExtendedFileResourceSelection value) {
        this.callWaitingVideoSelection = value;
    }

    /**
     * Ruft den Wert der callWaitingVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getCallWaitingVideoFile() {
        return callWaitingVideoFile;
    }

    /**
     * Legt den Wert der callWaitingVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setCallWaitingVideoFile(AnnouncementFileLevelKey value) {
        this.callWaitingVideoFile = value;
    }

    /**
     * Ruft den Wert der callWaitingVideoFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallWaitingVideoFileUrl() {
        return callWaitingVideoFileUrl;
    }

    /**
     * Legt den Wert der callWaitingVideoFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallWaitingVideoFileUrl(String value) {
        this.callWaitingVideoFileUrl = value;
    }

}
