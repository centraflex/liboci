//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemFAXMessagingGetRequest.
 * 
 *         The following elements are only used in AS data mode:
 *            statusDurationHours
 *            statusAuditIntervalHours
 *       
 * 
 * <p>Java-Klasse für SystemFaxMessagingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemFaxMessagingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="statusDurationHours" type="{}FaxMessagingStatusDurationHours"/>
 *         <element name="statusAuditIntervalHours" type="{}FaxMessagingStatusAuditIntervalHours"/>
 *         <element name="maximumConcurrentFaxesPerUser" type="{}FaxMessagingMaximumConcurrentFaxesPerUser"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemFaxMessagingGetResponse", propOrder = {
    "statusDurationHours",
    "statusAuditIntervalHours",
    "maximumConcurrentFaxesPerUser"
})
public class SystemFaxMessagingGetResponse
    extends OCIDataResponse
{

    protected int statusDurationHours;
    protected int statusAuditIntervalHours;
    protected int maximumConcurrentFaxesPerUser;

    /**
     * Ruft den Wert der statusDurationHours-Eigenschaft ab.
     * 
     */
    public int getStatusDurationHours() {
        return statusDurationHours;
    }

    /**
     * Legt den Wert der statusDurationHours-Eigenschaft fest.
     * 
     */
    public void setStatusDurationHours(int value) {
        this.statusDurationHours = value;
    }

    /**
     * Ruft den Wert der statusAuditIntervalHours-Eigenschaft ab.
     * 
     */
    public int getStatusAuditIntervalHours() {
        return statusAuditIntervalHours;
    }

    /**
     * Legt den Wert der statusAuditIntervalHours-Eigenschaft fest.
     * 
     */
    public void setStatusAuditIntervalHours(int value) {
        this.statusAuditIntervalHours = value;
    }

    /**
     * Ruft den Wert der maximumConcurrentFaxesPerUser-Eigenschaft ab.
     * 
     */
    public int getMaximumConcurrentFaxesPerUser() {
        return maximumConcurrentFaxesPerUser;
    }

    /**
     * Legt den Wert der maximumConcurrentFaxesPerUser-Eigenschaft fest.
     * 
     */
    public void setMaximumConcurrentFaxesPerUser(int value) {
        this.maximumConcurrentFaxesPerUser = value;
    }

}
