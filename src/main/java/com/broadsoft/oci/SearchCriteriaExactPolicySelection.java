//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *           Criteria for searching for a particular Voice VPN policy selection.
 *         
 * 
 * <p>Java-Klasse für SearchCriteriaExactPolicySelection complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactPolicySelection">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="policySelection" type="{}EnterpriseVoiceVPNPolicySelection"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactPolicySelection", propOrder = {
    "policySelection"
})
public class SearchCriteriaExactPolicySelection
    extends SearchCriteria
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseVoiceVPNPolicySelection policySelection;

    /**
     * Ruft den Wert der policySelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseVoiceVPNPolicySelection }
     *     
     */
    public EnterpriseVoiceVPNPolicySelection getPolicySelection() {
        return policySelection;
    }

    /**
     * Legt den Wert der policySelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseVoiceVPNPolicySelection }
     *     
     */
    public void setPolicySelection(EnterpriseVoiceVPNPolicySelection value) {
        this.policySelection = value;
    }

}
