//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserCallWaitingGetRequest17sp4.
 *         
 *         The following elements are only used in AS data mode:
 *           disableCallingLineIdDelivery        
 *       
 * 
 * <p>Java-Klasse für UserCallWaitingGetResponse17sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallWaitingGetResponse17sp4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disableCallingLineIdDelivery" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallWaitingGetResponse17sp4", propOrder = {
    "isActive",
    "disableCallingLineIdDelivery"
})
public class UserCallWaitingGetResponse17Sp4
    extends OCIDataResponse
{

    protected boolean isActive;
    protected boolean disableCallingLineIdDelivery;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der disableCallingLineIdDelivery-Eigenschaft ab.
     * 
     */
    public boolean isDisableCallingLineIdDelivery() {
        return disableCallingLineIdDelivery;
    }

    /**
     * Legt den Wert der disableCallingLineIdDelivery-Eigenschaft fest.
     * 
     */
    public void setDisableCallingLineIdDelivery(boolean value) {
        this.disableCallingLineIdDelivery = value;
    }

}
