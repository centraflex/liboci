//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modifies the system's legacy automatic callback line type attributes.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemLegacyAutomaticCallbackModifyLineTypeRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemLegacyAutomaticCallbackModifyLineTypeRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="lineType" type="{}LegacyAutomaticCallbackLineType"/>
 *         <element name="matchAction" type="{}LegacyAutomaticCallbackLineMatchAction" minOccurs="0"/>
 *         <element name="noMatchAction" type="{}LegacyAutomaticCallbackLineMatchAction" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemLegacyAutomaticCallbackModifyLineTypeRequest", propOrder = {
    "lineType",
    "matchAction",
    "noMatchAction"
})
public class SystemLegacyAutomaticCallbackModifyLineTypeRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected LegacyAutomaticCallbackLineType lineType;
    @XmlSchemaType(name = "token")
    protected LegacyAutomaticCallbackLineMatchAction matchAction;
    @XmlSchemaType(name = "token")
    protected LegacyAutomaticCallbackLineMatchAction noMatchAction;

    /**
     * Ruft den Wert der lineType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LegacyAutomaticCallbackLineType }
     *     
     */
    public LegacyAutomaticCallbackLineType getLineType() {
        return lineType;
    }

    /**
     * Legt den Wert der lineType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LegacyAutomaticCallbackLineType }
     *     
     */
    public void setLineType(LegacyAutomaticCallbackLineType value) {
        this.lineType = value;
    }

    /**
     * Ruft den Wert der matchAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LegacyAutomaticCallbackLineMatchAction }
     *     
     */
    public LegacyAutomaticCallbackLineMatchAction getMatchAction() {
        return matchAction;
    }

    /**
     * Legt den Wert der matchAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LegacyAutomaticCallbackLineMatchAction }
     *     
     */
    public void setMatchAction(LegacyAutomaticCallbackLineMatchAction value) {
        this.matchAction = value;
    }

    /**
     * Ruft den Wert der noMatchAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LegacyAutomaticCallbackLineMatchAction }
     *     
     */
    public LegacyAutomaticCallbackLineMatchAction getNoMatchAction() {
        return noMatchAction;
    }

    /**
     * Legt den Wert der noMatchAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LegacyAutomaticCallbackLineMatchAction }
     *     
     */
    public void setNoMatchAction(LegacyAutomaticCallbackLineMatchAction value) {
        this.noMatchAction = value;
    }

}
