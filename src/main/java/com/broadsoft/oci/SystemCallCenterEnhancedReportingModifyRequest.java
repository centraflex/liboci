//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system settings for call center enhanced reporting.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCallCenterEnhancedReportingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallCenterEnhancedReportingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="archiveReports" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="reportApplicationURL" type="{}URL" minOccurs="0"/>
 *         <element name="repositoryApplicationURL" type="{}URL" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallCenterEnhancedReportingModifyRequest", propOrder = {
    "archiveReports",
    "reportApplicationURL",
    "repositoryApplicationURL"
})
public class SystemCallCenterEnhancedReportingModifyRequest
    extends OCIRequest
{

    protected Boolean archiveReports;
    @XmlElementRef(name = "reportApplicationURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> reportApplicationURL;
    @XmlElementRef(name = "repositoryApplicationURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repositoryApplicationURL;

    /**
     * Ruft den Wert der archiveReports-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isArchiveReports() {
        return archiveReports;
    }

    /**
     * Legt den Wert der archiveReports-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setArchiveReports(Boolean value) {
        this.archiveReports = value;
    }

    /**
     * Ruft den Wert der reportApplicationURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReportApplicationURL() {
        return reportApplicationURL;
    }

    /**
     * Legt den Wert der reportApplicationURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReportApplicationURL(JAXBElement<String> value) {
        this.reportApplicationURL = value;
    }

    /**
     * Ruft den Wert der repositoryApplicationURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepositoryApplicationURL() {
        return repositoryApplicationURL;
    }

    /**
     * Legt den Wert der repositoryApplicationURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepositoryApplicationURL(JAXBElement<String> value) {
        this.repositoryApplicationURL = value;
    }

}
