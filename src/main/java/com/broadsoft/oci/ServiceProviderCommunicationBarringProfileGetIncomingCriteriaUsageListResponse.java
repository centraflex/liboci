//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderCommunicationBarringProfileGetIncomingCriteriaUsageListRequest.
 *         Contains a table of profiles that have the Communication Barring Incoming Criteria assigned.
 *         The column headings are: "Name" and "Description".
 *       
 * 
 * <p>Java-Klasse für ServiceProviderCommunicationBarringProfileGetIncomingCriteriaUsageListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderCommunicationBarringProfileGetIncomingCriteriaUsageListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="profileTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderCommunicationBarringProfileGetIncomingCriteriaUsageListResponse", propOrder = {
    "profileTable"
})
public class ServiceProviderCommunicationBarringProfileGetIncomingCriteriaUsageListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable profileTable;

    /**
     * Ruft den Wert der profileTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getProfileTable() {
        return profileTable;
    }

    /**
     * Legt den Wert der profileTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setProfileTable(OCITable value) {
        this.profileTable = value;
    }

}
