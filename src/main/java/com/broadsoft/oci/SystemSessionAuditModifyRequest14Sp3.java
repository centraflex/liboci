//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with session audit.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *         alwaysAllowRefreshForMS
 *         msAuditIntervalSeconds
 *       
 * 
 * <p>Java-Klasse für SystemSessionAuditModifyRequest14sp3 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSessionAuditModifyRequest14sp3">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="isAuditActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="auditIntervalSeconds" type="{}SessionAuditIntervalSeconds" minOccurs="0"/>
 *         <element name="auditTimeoutSeconds" type="{}SessionAuditTimeoutPeriodSeconds" minOccurs="0"/>
 *         <element name="releaseCallOnAuditFailure" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="isSIPRefreshAllowedOnAudit" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowUpdateForSIPRefresh" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="isSIPSessionTimerActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="sipSessionExpiresMinimumSeconds" type="{}SIPSessionExpiresMinimumSeconds" minOccurs="0"/>
 *         <element name="enforceSIPSessionExpiresMaximum" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="sipSessionExpiresMaximumSeconds" type="{}SIPSessionExpiresMaximumSeconds" minOccurs="0"/>
 *         <element name="sipSessionExpiresTimerSeconds" type="{}SIPSessionExpiresTimerSeconds" minOccurs="0"/>
 *         <element name="alwaysUseSessionTimerWhenSupported" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="preferredSessionTimerRefresher" type="{}SessionTimerRefresher" minOccurs="0"/>
 *         <element name="enableEmergencyCallAlarmTimer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="emergencyCallAlarmMinutes" type="{}EmergencyCallAlarmMinutes" minOccurs="0"/>
 *         <element name="enableEmergencyCallCleanupTimer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="emergencyCallCleanupMinutes" type="{}EmergencyCallCleanupMinutes" minOccurs="0"/>
 *         <element name="alwaysAllowRefreshForMS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="msAuditIntervalSeconds" type="{}MSAuditIntervalSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSessionAuditModifyRequest14sp3", propOrder = {
    "isAuditActive",
    "auditIntervalSeconds",
    "auditTimeoutSeconds",
    "releaseCallOnAuditFailure",
    "isSIPRefreshAllowedOnAudit",
    "allowUpdateForSIPRefresh",
    "isSIPSessionTimerActive",
    "sipSessionExpiresMinimumSeconds",
    "enforceSIPSessionExpiresMaximum",
    "sipSessionExpiresMaximumSeconds",
    "sipSessionExpiresTimerSeconds",
    "alwaysUseSessionTimerWhenSupported",
    "preferredSessionTimerRefresher",
    "enableEmergencyCallAlarmTimer",
    "emergencyCallAlarmMinutes",
    "enableEmergencyCallCleanupTimer",
    "emergencyCallCleanupMinutes",
    "alwaysAllowRefreshForMS",
    "msAuditIntervalSeconds"
})
public class SystemSessionAuditModifyRequest14Sp3
    extends OCIRequest
{

    protected Boolean isAuditActive;
    protected Integer auditIntervalSeconds;
    protected Integer auditTimeoutSeconds;
    protected Boolean releaseCallOnAuditFailure;
    protected Boolean isSIPRefreshAllowedOnAudit;
    protected Boolean allowUpdateForSIPRefresh;
    protected Boolean isSIPSessionTimerActive;
    protected Integer sipSessionExpiresMinimumSeconds;
    protected Boolean enforceSIPSessionExpiresMaximum;
    protected Integer sipSessionExpiresMaximumSeconds;
    protected Integer sipSessionExpiresTimerSeconds;
    protected Boolean alwaysUseSessionTimerWhenSupported;
    @XmlSchemaType(name = "token")
    protected SessionTimerRefresher preferredSessionTimerRefresher;
    protected Boolean enableEmergencyCallAlarmTimer;
    protected Integer emergencyCallAlarmMinutes;
    protected Boolean enableEmergencyCallCleanupTimer;
    protected Integer emergencyCallCleanupMinutes;
    protected Boolean alwaysAllowRefreshForMS;
    protected Integer msAuditIntervalSeconds;

    /**
     * Ruft den Wert der isAuditActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAuditActive() {
        return isAuditActive;
    }

    /**
     * Legt den Wert der isAuditActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAuditActive(Boolean value) {
        this.isAuditActive = value;
    }

    /**
     * Ruft den Wert der auditIntervalSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAuditIntervalSeconds() {
        return auditIntervalSeconds;
    }

    /**
     * Legt den Wert der auditIntervalSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAuditIntervalSeconds(Integer value) {
        this.auditIntervalSeconds = value;
    }

    /**
     * Ruft den Wert der auditTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAuditTimeoutSeconds() {
        return auditTimeoutSeconds;
    }

    /**
     * Legt den Wert der auditTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAuditTimeoutSeconds(Integer value) {
        this.auditTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der releaseCallOnAuditFailure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReleaseCallOnAuditFailure() {
        return releaseCallOnAuditFailure;
    }

    /**
     * Legt den Wert der releaseCallOnAuditFailure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReleaseCallOnAuditFailure(Boolean value) {
        this.releaseCallOnAuditFailure = value;
    }

    /**
     * Ruft den Wert der isSIPRefreshAllowedOnAudit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsSIPRefreshAllowedOnAudit() {
        return isSIPRefreshAllowedOnAudit;
    }

    /**
     * Legt den Wert der isSIPRefreshAllowedOnAudit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsSIPRefreshAllowedOnAudit(Boolean value) {
        this.isSIPRefreshAllowedOnAudit = value;
    }

    /**
     * Ruft den Wert der allowUpdateForSIPRefresh-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowUpdateForSIPRefresh() {
        return allowUpdateForSIPRefresh;
    }

    /**
     * Legt den Wert der allowUpdateForSIPRefresh-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowUpdateForSIPRefresh(Boolean value) {
        this.allowUpdateForSIPRefresh = value;
    }

    /**
     * Ruft den Wert der isSIPSessionTimerActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsSIPSessionTimerActive() {
        return isSIPSessionTimerActive;
    }

    /**
     * Legt den Wert der isSIPSessionTimerActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsSIPSessionTimerActive(Boolean value) {
        this.isSIPSessionTimerActive = value;
    }

    /**
     * Ruft den Wert der sipSessionExpiresMinimumSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSipSessionExpiresMinimumSeconds() {
        return sipSessionExpiresMinimumSeconds;
    }

    /**
     * Legt den Wert der sipSessionExpiresMinimumSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSipSessionExpiresMinimumSeconds(Integer value) {
        this.sipSessionExpiresMinimumSeconds = value;
    }

    /**
     * Ruft den Wert der enforceSIPSessionExpiresMaximum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnforceSIPSessionExpiresMaximum() {
        return enforceSIPSessionExpiresMaximum;
    }

    /**
     * Legt den Wert der enforceSIPSessionExpiresMaximum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnforceSIPSessionExpiresMaximum(Boolean value) {
        this.enforceSIPSessionExpiresMaximum = value;
    }

    /**
     * Ruft den Wert der sipSessionExpiresMaximumSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSipSessionExpiresMaximumSeconds() {
        return sipSessionExpiresMaximumSeconds;
    }

    /**
     * Legt den Wert der sipSessionExpiresMaximumSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSipSessionExpiresMaximumSeconds(Integer value) {
        this.sipSessionExpiresMaximumSeconds = value;
    }

    /**
     * Ruft den Wert der sipSessionExpiresTimerSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSipSessionExpiresTimerSeconds() {
        return sipSessionExpiresTimerSeconds;
    }

    /**
     * Legt den Wert der sipSessionExpiresTimerSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSipSessionExpiresTimerSeconds(Integer value) {
        this.sipSessionExpiresTimerSeconds = value;
    }

    /**
     * Ruft den Wert der alwaysUseSessionTimerWhenSupported-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlwaysUseSessionTimerWhenSupported() {
        return alwaysUseSessionTimerWhenSupported;
    }

    /**
     * Legt den Wert der alwaysUseSessionTimerWhenSupported-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlwaysUseSessionTimerWhenSupported(Boolean value) {
        this.alwaysUseSessionTimerWhenSupported = value;
    }

    /**
     * Ruft den Wert der preferredSessionTimerRefresher-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SessionTimerRefresher }
     *     
     */
    public SessionTimerRefresher getPreferredSessionTimerRefresher() {
        return preferredSessionTimerRefresher;
    }

    /**
     * Legt den Wert der preferredSessionTimerRefresher-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionTimerRefresher }
     *     
     */
    public void setPreferredSessionTimerRefresher(SessionTimerRefresher value) {
        this.preferredSessionTimerRefresher = value;
    }

    /**
     * Ruft den Wert der enableEmergencyCallAlarmTimer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableEmergencyCallAlarmTimer() {
        return enableEmergencyCallAlarmTimer;
    }

    /**
     * Legt den Wert der enableEmergencyCallAlarmTimer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableEmergencyCallAlarmTimer(Boolean value) {
        this.enableEmergencyCallAlarmTimer = value;
    }

    /**
     * Ruft den Wert der emergencyCallAlarmMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEmergencyCallAlarmMinutes() {
        return emergencyCallAlarmMinutes;
    }

    /**
     * Legt den Wert der emergencyCallAlarmMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEmergencyCallAlarmMinutes(Integer value) {
        this.emergencyCallAlarmMinutes = value;
    }

    /**
     * Ruft den Wert der enableEmergencyCallCleanupTimer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableEmergencyCallCleanupTimer() {
        return enableEmergencyCallCleanupTimer;
    }

    /**
     * Legt den Wert der enableEmergencyCallCleanupTimer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableEmergencyCallCleanupTimer(Boolean value) {
        this.enableEmergencyCallCleanupTimer = value;
    }

    /**
     * Ruft den Wert der emergencyCallCleanupMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEmergencyCallCleanupMinutes() {
        return emergencyCallCleanupMinutes;
    }

    /**
     * Legt den Wert der emergencyCallCleanupMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEmergencyCallCleanupMinutes(Integer value) {
        this.emergencyCallCleanupMinutes = value;
    }

    /**
     * Ruft den Wert der alwaysAllowRefreshForMS-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlwaysAllowRefreshForMS() {
        return alwaysAllowRefreshForMS;
    }

    /**
     * Legt den Wert der alwaysAllowRefreshForMS-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlwaysAllowRefreshForMS(Boolean value) {
        this.alwaysAllowRefreshForMS = value;
    }

    /**
     * Ruft den Wert der msAuditIntervalSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMsAuditIntervalSeconds() {
        return msAuditIntervalSeconds;
    }

    /**
     * Legt den Wert der msAuditIntervalSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMsAuditIntervalSeconds(Integer value) {
        this.msAuditIntervalSeconds = value;
    }

}
