//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ResellerXsiPolicyProfileGetAssignedServiceProviderListRequest.
 *         Contains a table of Service Providers that have the Xsi Policy Profile 
 *         assigned. The column headings are: "Organization ID", "Organization Name", "Organization Type".
 *       
 * 
 * <p>Java-Klasse für ResellerXsiPolicyProfileGetAssignedServiceProviderListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerXsiPolicyProfileGetAssignedServiceProviderListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="svcProviderTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerXsiPolicyProfileGetAssignedServiceProviderListResponse", propOrder = {
    "svcProviderTable"
})
public class ResellerXsiPolicyProfileGetAssignedServiceProviderListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable svcProviderTable;

    /**
     * Ruft den Wert der svcProviderTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getSvcProviderTable() {
        return svcProviderTable;
    }

    /**
     * Legt den Wert der svcProviderTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setSvcProviderTable(OCITable value) {
        this.svcProviderTable = value;
    }

}
