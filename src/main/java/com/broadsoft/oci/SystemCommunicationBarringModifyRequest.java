//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with Communication Barring.
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *          vmCallbackScreening
 *         
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCommunicationBarringModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCommunicationBarringModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="directTransferScreening" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="vmCallbackScreening" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCommunicationBarringModifyRequest", propOrder = {
    "directTransferScreening",
    "vmCallbackScreening"
})
public class SystemCommunicationBarringModifyRequest
    extends OCIRequest
{

    protected Boolean directTransferScreening;
    protected Boolean vmCallbackScreening;

    /**
     * Ruft den Wert der directTransferScreening-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDirectTransferScreening() {
        return directTransferScreening;
    }

    /**
     * Legt den Wert der directTransferScreening-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDirectTransferScreening(Boolean value) {
        this.directTransferScreening = value;
    }

    /**
     * Ruft den Wert der vmCallbackScreening-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVmCallbackScreening() {
        return vmCallbackScreening;
    }

    /**
     * Legt den Wert der vmCallbackScreening-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVmCallbackScreening(Boolean value) {
        this.vmCallbackScreening = value;
    }

}
