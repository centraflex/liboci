//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to a SystemTreatmentMappingCallBlockingServiceGetListRequest22V3. 
 *         Contains a table with one row per mapping.
 *         The table columns are: "Service", "Treatment Id".
 *         The Service column list the service or policy which is using corresponding treatment, and can be OCP, EOCP, ICP, ACR, SCR, SCA, AAC, Intercept, PTT, CommBarring, SAC, IncomingCommBarring, HierCommBarring, IncomHierCommBarring, BWMobilityDenyOrig, BWMobilityDenyTerm, ETRouteExhaust, CallPark, NumberPortability, ConcTerminatingAlertingPolicy, AutomaticCollectCall, TCUG.
 *       
 * 
 * <p>Java-Klasse für SystemTreatmentMappingCallBlockingServiceGetListResponse22V2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemTreatmentMappingCallBlockingServiceGetListResponse22V2">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="treatmentMappingTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemTreatmentMappingCallBlockingServiceGetListResponse22V2", propOrder = {
    "treatmentMappingTable"
})
public class SystemTreatmentMappingCallBlockingServiceGetListResponse22V2
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable treatmentMappingTable;

    /**
     * Ruft den Wert der treatmentMappingTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getTreatmentMappingTable() {
        return treatmentMappingTable;
    }

    /**
     * Legt den Wert der treatmentMappingTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setTreatmentMappingTable(OCITable value) {
        this.treatmentMappingTable = value;
    }

}
