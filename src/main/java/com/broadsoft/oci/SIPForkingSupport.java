//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SIPForkingSupport.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SIPForkingSupport">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Single Dialog"/>
 *     <enumeration value="Single Dialog With UPDATE"/>
 *     <enumeration value="Multiple Dialogs With Error Correction"/>
 *     <enumeration value="Multiple Dialogs"/>
 *     <enumeration value="Single Dialog With UPDATE If Allowed"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SIPForkingSupport")
@XmlEnum
public enum SIPForkingSupport {

    @XmlEnumValue("Single Dialog")
    SINGLE_DIALOG("Single Dialog"),
    @XmlEnumValue("Single Dialog With UPDATE")
    SINGLE_DIALOG_WITH_UPDATE("Single Dialog With UPDATE"),
    @XmlEnumValue("Multiple Dialogs With Error Correction")
    MULTIPLE_DIALOGS_WITH_ERROR_CORRECTION("Multiple Dialogs With Error Correction"),
    @XmlEnumValue("Multiple Dialogs")
    MULTIPLE_DIALOGS("Multiple Dialogs"),
    @XmlEnumValue("Single Dialog With UPDATE If Allowed")
    SINGLE_DIALOG_WITH_UPDATE_IF_ALLOWED("Single Dialog With UPDATE If Allowed");
    private final String value;

    SIPForkingSupport(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SIPForkingSupport fromValue(String v) {
        for (SIPForkingSupport c: SIPForkingSupport.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
