//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a route point's DNIS settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupRoutePointModifyDNISRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupRoutePointModifyDNISRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="dnisKey" type="{}DNISKey"/>
 *         <element name="newDNISName" type="{}DNISName" minOccurs="0"/>
 *         <element name="dnisPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="extension" type="{}Extension17" minOccurs="0"/>
 *         <element name="useCustomCLIDSettings" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="callingLineIdPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="callingLineIdLastName" type="{}CallingLineIdLastName" minOccurs="0"/>
 *         <element name="callingLineIdFirstName" type="{}CallingLineIdFirstName" minOccurs="0"/>
 *         <element name="useCustomDnisAnnouncementSettings" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowOutgoingACDCall" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupRoutePointModifyDNISRequest", propOrder = {
    "dnisKey",
    "newDNISName",
    "dnisPhoneNumber",
    "extension",
    "useCustomCLIDSettings",
    "callingLineIdPhoneNumber",
    "callingLineIdLastName",
    "callingLineIdFirstName",
    "useCustomDnisAnnouncementSettings",
    "allowOutgoingACDCall"
})
public class GroupRoutePointModifyDNISRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    protected DNISKey dnisKey;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newDNISName;
    @XmlElementRef(name = "dnisPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dnisPhoneNumber;
    @XmlElementRef(name = "extension", type = JAXBElement.class, required = false)
    protected JAXBElement<String> extension;
    protected Boolean useCustomCLIDSettings;
    @XmlElementRef(name = "callingLineIdPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callingLineIdPhoneNumber;
    @XmlElementRef(name = "callingLineIdLastName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callingLineIdLastName;
    @XmlElementRef(name = "callingLineIdFirstName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callingLineIdFirstName;
    protected Boolean useCustomDnisAnnouncementSettings;
    protected Boolean allowOutgoingACDCall;

    /**
     * Ruft den Wert der dnisKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DNISKey }
     *     
     */
    public DNISKey getDnisKey() {
        return dnisKey;
    }

    /**
     * Legt den Wert der dnisKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DNISKey }
     *     
     */
    public void setDnisKey(DNISKey value) {
        this.dnisKey = value;
    }

    /**
     * Ruft den Wert der newDNISName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewDNISName() {
        return newDNISName;
    }

    /**
     * Legt den Wert der newDNISName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewDNISName(String value) {
        this.newDNISName = value;
    }

    /**
     * Ruft den Wert der dnisPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDnisPhoneNumber() {
        return dnisPhoneNumber;
    }

    /**
     * Legt den Wert der dnisPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDnisPhoneNumber(JAXBElement<String> value) {
        this.dnisPhoneNumber = value;
    }

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExtension(JAXBElement<String> value) {
        this.extension = value;
    }

    /**
     * Ruft den Wert der useCustomCLIDSettings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseCustomCLIDSettings() {
        return useCustomCLIDSettings;
    }

    /**
     * Legt den Wert der useCustomCLIDSettings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseCustomCLIDSettings(Boolean value) {
        this.useCustomCLIDSettings = value;
    }

    /**
     * Ruft den Wert der callingLineIdPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallingLineIdPhoneNumber() {
        return callingLineIdPhoneNumber;
    }

    /**
     * Legt den Wert der callingLineIdPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallingLineIdPhoneNumber(JAXBElement<String> value) {
        this.callingLineIdPhoneNumber = value;
    }

    /**
     * Ruft den Wert der callingLineIdLastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallingLineIdLastName() {
        return callingLineIdLastName;
    }

    /**
     * Legt den Wert der callingLineIdLastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallingLineIdLastName(JAXBElement<String> value) {
        this.callingLineIdLastName = value;
    }

    /**
     * Ruft den Wert der callingLineIdFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallingLineIdFirstName() {
        return callingLineIdFirstName;
    }

    /**
     * Legt den Wert der callingLineIdFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallingLineIdFirstName(JAXBElement<String> value) {
        this.callingLineIdFirstName = value;
    }

    /**
     * Ruft den Wert der useCustomDnisAnnouncementSettings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseCustomDnisAnnouncementSettings() {
        return useCustomDnisAnnouncementSettings;
    }

    /**
     * Legt den Wert der useCustomDnisAnnouncementSettings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseCustomDnisAnnouncementSettings(Boolean value) {
        this.useCustomDnisAnnouncementSettings = value;
    }

    /**
     * Ruft den Wert der allowOutgoingACDCall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowOutgoingACDCall() {
        return allowOutgoingACDCall;
    }

    /**
     * Legt den Wert der allowOutgoingACDCall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowOutgoingACDCall(Boolean value) {
        this.allowOutgoingACDCall = value;
    }

}
