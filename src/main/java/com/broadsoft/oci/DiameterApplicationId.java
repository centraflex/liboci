//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DiameterApplicationId.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DiameterApplicationId">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Rf"/>
 *     <enumeration value="Ro"/>
 *     <enumeration value="Sh"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DiameterApplicationId")
@XmlEnum
public enum DiameterApplicationId {

    @XmlEnumValue("Rf")
    RF("Rf"),
    @XmlEnumValue("Ro")
    RO("Ro"),
    @XmlEnumValue("Sh")
    SH("Sh");
    private final String value;

    DiameterApplicationId(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DiameterApplicationId fromValue(String v) {
        for (DiameterApplicationId c: DiameterApplicationId.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
