//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserModifyGroupIdRequest.
 *         error indicates the failing conditions preventing the user move. 
 *         impact indicates any change to user and group as the result of a user move.
 *       
 * 
 * <p>Java-Klasse für UserModifyGroupIdResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserModifyGroupIdResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="error" type="{}UserMoveMessage" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="impact" type="{}UserMoveMessage" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserModifyGroupIdResponse", propOrder = {
    "error",
    "impact"
})
public class UserModifyGroupIdResponse
    extends OCIDataResponse
{

    protected List<UserMoveMessage> error;
    protected List<UserMoveMessage> impact;

    /**
     * Gets the value of the error property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the error property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getError().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserMoveMessage }
     * 
     * 
     * @return
     *     The value of the error property.
     */
    public List<UserMoveMessage> getError() {
        if (error == null) {
            error = new ArrayList<>();
        }
        return this.error;
    }

    /**
     * Gets the value of the impact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the impact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getImpact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserMoveMessage }
     * 
     * 
     * @return
     *     The value of the impact property.
     */
    public List<UserMoveMessage> getImpact() {
        if (impact == null) {
            impact = new ArrayList<>();
        }
        return this.impact;
    }

}
