//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemSubscriberGetProvisioningParametersRequest24.
 *       
 * 
 * <p>Java-Klasse für SystemSubscriberGetProvisioningParametersResponse24 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSubscriberGetProvisioningParametersResponse24">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="configurableCLIDNormalization" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeDefaultDomain" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSubscriberGetProvisioningParametersResponse24", propOrder = {
    "configurableCLIDNormalization",
    "includeDefaultDomain"
})
public class SystemSubscriberGetProvisioningParametersResponse24
    extends OCIDataResponse
{

    protected boolean configurableCLIDNormalization;
    protected boolean includeDefaultDomain;

    /**
     * Ruft den Wert der configurableCLIDNormalization-Eigenschaft ab.
     * 
     */
    public boolean isConfigurableCLIDNormalization() {
        return configurableCLIDNormalization;
    }

    /**
     * Legt den Wert der configurableCLIDNormalization-Eigenschaft fest.
     * 
     */
    public void setConfigurableCLIDNormalization(boolean value) {
        this.configurableCLIDNormalization = value;
    }

    /**
     * Ruft den Wert der includeDefaultDomain-Eigenschaft ab.
     * 
     */
    public boolean isIncludeDefaultDomain() {
        return includeDefaultDomain;
    }

    /**
     * Legt den Wert der includeDefaultDomain-Eigenschaft fest.
     * 
     */
    public void setIncludeDefaultDomain(boolean value) {
        this.includeDefaultDomain = value;
    }

}
