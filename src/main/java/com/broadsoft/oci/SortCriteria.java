//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The sort criteria specifies whether sort is ascending or descending,
 *         and	whether the sort is case sensitive. Sort order defaults to
 *         ascending and case sensitive.
 *       
 * 
 * <p>Java-Klasse für SortCriteria complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SortCriteria">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="isAscending" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isCaseSensitive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SortCriteria", propOrder = {
    "isAscending",
    "isCaseSensitive"
})
@XmlSeeAlso({
    SortByAdminFirstName.class,
    SortByAdminId.class,
    SortByAdminLastName.class,
    SortByAgentThresholdProfile.class,
    SortByAnnouncementFileName.class,
    SortByCallCenterName.class,
    SortByCallCenterType.class,
    SortByCallParkName.class,
    SortByCallPickupName.class,
    SortByDepartmentName.class,
    SortByDeviceMACAddress.class,
    SortByDeviceName.class,
    SortByDeviceNetAddress.class,
    SortByDeviceType.class,
    SortByDn.class,
    SortByDnActivated.class,
    SortByDnAvailable.class,
    SortByEmailAddress.class,
    SortByEnabled.class,
    SortByEnterpriseCommonPhoneListName.class,
    SortByEnterpriseCommonPhoneListNumber.class,
    SortByExtension.class,
    SortByForwardedToNumber.class,
    SortByGroupCommonPhoneListName.class,
    SortByGroupCommonPhoneListNumber.class,
    SortByGroupId.class,
    SortByGroupLocationCode.class,
    SortByGroupName.class,
    SortByHuntPolicy.class,
    SortByImpId.class,
    SortByLocation.class,
    SortByMobileDirectoryNumber.class,
    SortByMobilePhoneNumber.class,
    SortByReceptionistNote.class,
    SortByScheduleName.class,
    SortByServiceProviderId.class,
    SortByServiceProviderName.class,
    SortByServiceStatus.class,
    SortByTrunkGroupName.class,
    SortByUserDepartment.class,
    SortByUserFirstName.class,
    SortByUserId.class,
    SortByUserLastName.class,
    SortByUserPersonalPhoneListName.class,
    SortByUserPersonalPhoneListNumber.class,
    SortByYahooId.class
})
public abstract class SortCriteria {

    @XmlElement(defaultValue = "true")
    protected boolean isAscending;
    @XmlElement(defaultValue = "true")
    protected boolean isCaseSensitive;

    /**
     * Ruft den Wert der isAscending-Eigenschaft ab.
     * 
     */
    public boolean isIsAscending() {
        return isAscending;
    }

    /**
     * Legt den Wert der isAscending-Eigenschaft fest.
     * 
     */
    public void setIsAscending(boolean value) {
        this.isAscending = value;
    }

    /**
     * Ruft den Wert der isCaseSensitive-Eigenschaft ab.
     * 
     */
    public boolean isIsCaseSensitive() {
        return isCaseSensitive;
    }

    /**
     * Legt den Wert der isCaseSensitive-Eigenschaft fest.
     * 
     */
    public void setIsCaseSensitive(boolean value) {
        this.isCaseSensitive = value;
    }

}
