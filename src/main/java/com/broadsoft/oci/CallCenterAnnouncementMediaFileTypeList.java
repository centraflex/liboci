//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Contains list of file media types for audio or video files
 *       
 * 
 * <p>Java-Klasse für CallCenterAnnouncementMediaFileTypeList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterAnnouncementMediaFileTypeList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="mediaType1" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="mediaType2" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="mediaType3" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="mediaType4" type="{}MediaFileType" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterAnnouncementMediaFileTypeList", propOrder = {
    "mediaType1",
    "mediaType2",
    "mediaType3",
    "mediaType4"
})
public class CallCenterAnnouncementMediaFileTypeList {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mediaType1;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mediaType2;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mediaType3;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mediaType4;

    /**
     * Ruft den Wert der mediaType1-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMediaType1() {
        return mediaType1;
    }

    /**
     * Legt den Wert der mediaType1-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMediaType1(String value) {
        this.mediaType1 = value;
    }

    /**
     * Ruft den Wert der mediaType2-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMediaType2() {
        return mediaType2;
    }

    /**
     * Legt den Wert der mediaType2-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMediaType2(String value) {
        this.mediaType2 = value;
    }

    /**
     * Ruft den Wert der mediaType3-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMediaType3() {
        return mediaType3;
    }

    /**
     * Legt den Wert der mediaType3-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMediaType3(String value) {
        this.mediaType3 = value;
    }

    /**
     * Ruft den Wert der mediaType4-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMediaType4() {
        return mediaType4;
    }

    /**
     * Legt den Wert der mediaType4-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMediaType4(String value) {
        this.mediaType4 = value;
    }

}
