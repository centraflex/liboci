//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the UserExecutiveGetScreeningAlertingRequest.
 *         Contains the screening and alerting settings for an executive.
 *       
 * 
 * <p>Java-Klasse für UserExecutiveGetScreeningAlertingResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserExecutiveGetScreeningAlertingResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableScreening" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="screeningAlertType" type="{}ExecutiveScreeningAlertType"/>
 *         <element name="alertBroadWorksMobilityLocation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="alertBroadWorksAnywhereLocations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="alertSharedCallAppearanceLocations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="alertingMode" type="{}ExecutiveAlertingMode"/>
 *         <element name="alertingCallingLineIdNameMode" type="{}ExecutiveAlertingCallingLineIdNameMode"/>
 *         <element name="alertingCustomCallingLineIdName" type="{}ExecutiveAlertingCustomCallingLineIdName" minOccurs="0"/>
 *         <element name="unicodeAlertingCustomCallingLineIdName" type="{}ExecutiveAlertingCustomCallingLineIdName" minOccurs="0"/>
 *         <element name="alertingCallingLineIdPhoneNumberMode" type="{}ExecutiveAlertingCallingLineIdPhoneNumberMode"/>
 *         <element name="alertingCustomCallingLineIdPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="callPushRecallNumberOfRings" type="{}ExecutiveCallPushRecallNumberOfRings"/>
 *         <element name="nextAssistantNumberOfRings" type="{}ExecutiveNextAssistantNumberOfRings"/>
 *         <element name="enableRollover" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="rolloverWaitTimeSeconds" type="{}ExecutiveRolloverWaitTimeSeconds" minOccurs="0"/>
 *         <element name="rolloverAction" type="{}ExecutiveRolloverActionType"/>
 *         <element name="rolloverForwardToPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserExecutiveGetScreeningAlertingResponse", propOrder = {
    "enableScreening",
    "screeningAlertType",
    "alertBroadWorksMobilityLocation",
    "alertBroadWorksAnywhereLocations",
    "alertSharedCallAppearanceLocations",
    "alertingMode",
    "alertingCallingLineIdNameMode",
    "alertingCustomCallingLineIdName",
    "unicodeAlertingCustomCallingLineIdName",
    "alertingCallingLineIdPhoneNumberMode",
    "alertingCustomCallingLineIdPhoneNumber",
    "callPushRecallNumberOfRings",
    "nextAssistantNumberOfRings",
    "enableRollover",
    "rolloverWaitTimeSeconds",
    "rolloverAction",
    "rolloverForwardToPhoneNumber"
})
public class UserExecutiveGetScreeningAlertingResponse
    extends OCIDataResponse
{

    protected boolean enableScreening;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExecutiveScreeningAlertType screeningAlertType;
    protected boolean alertBroadWorksMobilityLocation;
    protected boolean alertBroadWorksAnywhereLocations;
    protected boolean alertSharedCallAppearanceLocations;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExecutiveAlertingMode alertingMode;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExecutiveAlertingCallingLineIdNameMode alertingCallingLineIdNameMode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String alertingCustomCallingLineIdName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String unicodeAlertingCustomCallingLineIdName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExecutiveAlertingCallingLineIdPhoneNumberMode alertingCallingLineIdPhoneNumberMode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String alertingCustomCallingLineIdPhoneNumber;
    protected int callPushRecallNumberOfRings;
    protected int nextAssistantNumberOfRings;
    protected boolean enableRollover;
    protected Integer rolloverWaitTimeSeconds;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExecutiveRolloverActionType rolloverAction;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String rolloverForwardToPhoneNumber;

    /**
     * Ruft den Wert der enableScreening-Eigenschaft ab.
     * 
     */
    public boolean isEnableScreening() {
        return enableScreening;
    }

    /**
     * Legt den Wert der enableScreening-Eigenschaft fest.
     * 
     */
    public void setEnableScreening(boolean value) {
        this.enableScreening = value;
    }

    /**
     * Ruft den Wert der screeningAlertType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveScreeningAlertType }
     *     
     */
    public ExecutiveScreeningAlertType getScreeningAlertType() {
        return screeningAlertType;
    }

    /**
     * Legt den Wert der screeningAlertType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveScreeningAlertType }
     *     
     */
    public void setScreeningAlertType(ExecutiveScreeningAlertType value) {
        this.screeningAlertType = value;
    }

    /**
     * Ruft den Wert der alertBroadWorksMobilityLocation-Eigenschaft ab.
     * 
     */
    public boolean isAlertBroadWorksMobilityLocation() {
        return alertBroadWorksMobilityLocation;
    }

    /**
     * Legt den Wert der alertBroadWorksMobilityLocation-Eigenschaft fest.
     * 
     */
    public void setAlertBroadWorksMobilityLocation(boolean value) {
        this.alertBroadWorksMobilityLocation = value;
    }

    /**
     * Ruft den Wert der alertBroadWorksAnywhereLocations-Eigenschaft ab.
     * 
     */
    public boolean isAlertBroadWorksAnywhereLocations() {
        return alertBroadWorksAnywhereLocations;
    }

    /**
     * Legt den Wert der alertBroadWorksAnywhereLocations-Eigenschaft fest.
     * 
     */
    public void setAlertBroadWorksAnywhereLocations(boolean value) {
        this.alertBroadWorksAnywhereLocations = value;
    }

    /**
     * Ruft den Wert der alertSharedCallAppearanceLocations-Eigenschaft ab.
     * 
     */
    public boolean isAlertSharedCallAppearanceLocations() {
        return alertSharedCallAppearanceLocations;
    }

    /**
     * Legt den Wert der alertSharedCallAppearanceLocations-Eigenschaft fest.
     * 
     */
    public void setAlertSharedCallAppearanceLocations(boolean value) {
        this.alertSharedCallAppearanceLocations = value;
    }

    /**
     * Ruft den Wert der alertingMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveAlertingMode }
     *     
     */
    public ExecutiveAlertingMode getAlertingMode() {
        return alertingMode;
    }

    /**
     * Legt den Wert der alertingMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveAlertingMode }
     *     
     */
    public void setAlertingMode(ExecutiveAlertingMode value) {
        this.alertingMode = value;
    }

    /**
     * Ruft den Wert der alertingCallingLineIdNameMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveAlertingCallingLineIdNameMode }
     *     
     */
    public ExecutiveAlertingCallingLineIdNameMode getAlertingCallingLineIdNameMode() {
        return alertingCallingLineIdNameMode;
    }

    /**
     * Legt den Wert der alertingCallingLineIdNameMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveAlertingCallingLineIdNameMode }
     *     
     */
    public void setAlertingCallingLineIdNameMode(ExecutiveAlertingCallingLineIdNameMode value) {
        this.alertingCallingLineIdNameMode = value;
    }

    /**
     * Ruft den Wert der alertingCustomCallingLineIdName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlertingCustomCallingLineIdName() {
        return alertingCustomCallingLineIdName;
    }

    /**
     * Legt den Wert der alertingCustomCallingLineIdName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlertingCustomCallingLineIdName(String value) {
        this.alertingCustomCallingLineIdName = value;
    }

    /**
     * Ruft den Wert der unicodeAlertingCustomCallingLineIdName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnicodeAlertingCustomCallingLineIdName() {
        return unicodeAlertingCustomCallingLineIdName;
    }

    /**
     * Legt den Wert der unicodeAlertingCustomCallingLineIdName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnicodeAlertingCustomCallingLineIdName(String value) {
        this.unicodeAlertingCustomCallingLineIdName = value;
    }

    /**
     * Ruft den Wert der alertingCallingLineIdPhoneNumberMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveAlertingCallingLineIdPhoneNumberMode }
     *     
     */
    public ExecutiveAlertingCallingLineIdPhoneNumberMode getAlertingCallingLineIdPhoneNumberMode() {
        return alertingCallingLineIdPhoneNumberMode;
    }

    /**
     * Legt den Wert der alertingCallingLineIdPhoneNumberMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveAlertingCallingLineIdPhoneNumberMode }
     *     
     */
    public void setAlertingCallingLineIdPhoneNumberMode(ExecutiveAlertingCallingLineIdPhoneNumberMode value) {
        this.alertingCallingLineIdPhoneNumberMode = value;
    }

    /**
     * Ruft den Wert der alertingCustomCallingLineIdPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlertingCustomCallingLineIdPhoneNumber() {
        return alertingCustomCallingLineIdPhoneNumber;
    }

    /**
     * Legt den Wert der alertingCustomCallingLineIdPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlertingCustomCallingLineIdPhoneNumber(String value) {
        this.alertingCustomCallingLineIdPhoneNumber = value;
    }

    /**
     * Ruft den Wert der callPushRecallNumberOfRings-Eigenschaft ab.
     * 
     */
    public int getCallPushRecallNumberOfRings() {
        return callPushRecallNumberOfRings;
    }

    /**
     * Legt den Wert der callPushRecallNumberOfRings-Eigenschaft fest.
     * 
     */
    public void setCallPushRecallNumberOfRings(int value) {
        this.callPushRecallNumberOfRings = value;
    }

    /**
     * Ruft den Wert der nextAssistantNumberOfRings-Eigenschaft ab.
     * 
     */
    public int getNextAssistantNumberOfRings() {
        return nextAssistantNumberOfRings;
    }

    /**
     * Legt den Wert der nextAssistantNumberOfRings-Eigenschaft fest.
     * 
     */
    public void setNextAssistantNumberOfRings(int value) {
        this.nextAssistantNumberOfRings = value;
    }

    /**
     * Ruft den Wert der enableRollover-Eigenschaft ab.
     * 
     */
    public boolean isEnableRollover() {
        return enableRollover;
    }

    /**
     * Legt den Wert der enableRollover-Eigenschaft fest.
     * 
     */
    public void setEnableRollover(boolean value) {
        this.enableRollover = value;
    }

    /**
     * Ruft den Wert der rolloverWaitTimeSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRolloverWaitTimeSeconds() {
        return rolloverWaitTimeSeconds;
    }

    /**
     * Legt den Wert der rolloverWaitTimeSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRolloverWaitTimeSeconds(Integer value) {
        this.rolloverWaitTimeSeconds = value;
    }

    /**
     * Ruft den Wert der rolloverAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveRolloverActionType }
     *     
     */
    public ExecutiveRolloverActionType getRolloverAction() {
        return rolloverAction;
    }

    /**
     * Legt den Wert der rolloverAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveRolloverActionType }
     *     
     */
    public void setRolloverAction(ExecutiveRolloverActionType value) {
        this.rolloverAction = value;
    }

    /**
     * Ruft den Wert der rolloverForwardToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRolloverForwardToPhoneNumber() {
        return rolloverForwardToPhoneNumber;
    }

    /**
     * Legt den Wert der rolloverForwardToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRolloverForwardToPhoneNumber(String value) {
        this.rolloverForwardToPhoneNumber = value;
    }

}
