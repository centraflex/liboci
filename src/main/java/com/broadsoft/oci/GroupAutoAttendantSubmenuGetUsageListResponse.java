//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupAutoAttendantSubmenuGetUsageListRequest.
 *         Contains a table with column headings:
 *         "Type" and "Submenu ID".
 *         
 *         The "Type" Column will contain one of the following: Business Hours Menu,
 *         After Hours Menu, Holiday Menu or Submenu.
 *         
 *         The "Submenu ID" Column will be left blank when the "Type" Column contains one
 *         of the base menu types (Business Hours Menu, After Hours Menu or Holiday Menu),
 *         and will contain the Submenu ID when the "Type" Column contains type Submenu.        
 *       
 * 
 * <p>Java-Klasse für GroupAutoAttendantSubmenuGetUsageListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAutoAttendantSubmenuGetUsageListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="submenuTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAutoAttendantSubmenuGetUsageListResponse", propOrder = {
    "submenuTable"
})
public class GroupAutoAttendantSubmenuGetUsageListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable submenuTable;

    /**
     * Ruft den Wert der submenuTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getSubmenuTable() {
        return submenuTable;
    }

    /**
     * Legt den Wert der submenuTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setSubmenuTable(OCITable value) {
        this.submenuTable = value;
    }

}
