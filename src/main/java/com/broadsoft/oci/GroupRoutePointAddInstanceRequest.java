//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a Route Point instance to a group.
 *         The Route Point is a Call Center queue that performs the same function but 
 *         allows an external system to perform the distribution of calls instead of making 
 *         those decisions itself.
 *         The domain is required in the serviceUserId.
 *         The response is either SuccessResponse or ErrorResponse.
 *         Replace by GroupRoutePointAddInstanceRequest22
 *       
 * 
 * <p>Java-Klasse für GroupRoutePointAddInstanceRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupRoutePointAddInstanceRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceAddProfileCallCenter"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="externalPreferredAudioCodec" type="{}AudioFileCodec"/>
 *         <element name="internalPreferredAudioCodec" type="{}AudioFileCodec"/>
 *         <element name="queueLength" type="{}CallCenterQueueLength16"/>
 *         <element name="noAnswerTimeoutRings" type="{}NoAnswerTimeoutRings"/>
 *         <element name="enableVideo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="playRingingWhenOfferingCall" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="overrideAgentWrapUpTime" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="wrapUpSeconds" type="{}CallCenterWrapUpSeconds" minOccurs="0"/>
 *         <element name="enableAutomaticStateChangeForAgents" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="agentStateAfterCall" type="{}AgentACDAutomaticState"/>
 *         <element name="agentUnavailableCode" type="{}CallCenterAgentUnavailableCode" minOccurs="0"/>
 *         <element name="forceDeliveryOfCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forceDeliveryWaitTimeSeconds" type="{}CallCenterForceDeliveryWaitTimeSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupRoutePointAddInstanceRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "serviceUserId",
    "serviceInstanceProfile",
    "networkClassOfService",
    "externalPreferredAudioCodec",
    "internalPreferredAudioCodec",
    "queueLength",
    "noAnswerTimeoutRings",
    "enableVideo",
    "playRingingWhenOfferingCall",
    "overrideAgentWrapUpTime",
    "wrapUpSeconds",
    "enableAutomaticStateChangeForAgents",
    "agentStateAfterCall",
    "agentUnavailableCode",
    "forceDeliveryOfCalls",
    "forceDeliveryWaitTimeSeconds"
})
public class GroupRoutePointAddInstanceRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlElement(required = true)
    protected ServiceInstanceAddProfileCallCenter serviceInstanceProfile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AudioFileCodec externalPreferredAudioCodec;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AudioFileCodec internalPreferredAudioCodec;
    protected int queueLength;
    protected int noAnswerTimeoutRings;
    protected boolean enableVideo;
    protected boolean playRingingWhenOfferingCall;
    protected boolean overrideAgentWrapUpTime;
    protected Integer wrapUpSeconds;
    protected boolean enableAutomaticStateChangeForAgents;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AgentACDAutomaticState agentStateAfterCall;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String agentUnavailableCode;
    protected boolean forceDeliveryOfCalls;
    protected Integer forceDeliveryWaitTimeSeconds;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceAddProfileCallCenter }
     *     
     */
    public ServiceInstanceAddProfileCallCenter getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceAddProfileCallCenter }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceAddProfileCallCenter value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Ruft den Wert der externalPreferredAudioCodec-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AudioFileCodec }
     *     
     */
    public AudioFileCodec getExternalPreferredAudioCodec() {
        return externalPreferredAudioCodec;
    }

    /**
     * Legt den Wert der externalPreferredAudioCodec-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AudioFileCodec }
     *     
     */
    public void setExternalPreferredAudioCodec(AudioFileCodec value) {
        this.externalPreferredAudioCodec = value;
    }

    /**
     * Ruft den Wert der internalPreferredAudioCodec-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AudioFileCodec }
     *     
     */
    public AudioFileCodec getInternalPreferredAudioCodec() {
        return internalPreferredAudioCodec;
    }

    /**
     * Legt den Wert der internalPreferredAudioCodec-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AudioFileCodec }
     *     
     */
    public void setInternalPreferredAudioCodec(AudioFileCodec value) {
        this.internalPreferredAudioCodec = value;
    }

    /**
     * Ruft den Wert der queueLength-Eigenschaft ab.
     * 
     */
    public int getQueueLength() {
        return queueLength;
    }

    /**
     * Legt den Wert der queueLength-Eigenschaft fest.
     * 
     */
    public void setQueueLength(int value) {
        this.queueLength = value;
    }

    /**
     * Ruft den Wert der noAnswerTimeoutRings-Eigenschaft ab.
     * 
     */
    public int getNoAnswerTimeoutRings() {
        return noAnswerTimeoutRings;
    }

    /**
     * Legt den Wert der noAnswerTimeoutRings-Eigenschaft fest.
     * 
     */
    public void setNoAnswerTimeoutRings(int value) {
        this.noAnswerTimeoutRings = value;
    }

    /**
     * Ruft den Wert der enableVideo-Eigenschaft ab.
     * 
     */
    public boolean isEnableVideo() {
        return enableVideo;
    }

    /**
     * Legt den Wert der enableVideo-Eigenschaft fest.
     * 
     */
    public void setEnableVideo(boolean value) {
        this.enableVideo = value;
    }

    /**
     * Ruft den Wert der playRingingWhenOfferingCall-Eigenschaft ab.
     * 
     */
    public boolean isPlayRingingWhenOfferingCall() {
        return playRingingWhenOfferingCall;
    }

    /**
     * Legt den Wert der playRingingWhenOfferingCall-Eigenschaft fest.
     * 
     */
    public void setPlayRingingWhenOfferingCall(boolean value) {
        this.playRingingWhenOfferingCall = value;
    }

    /**
     * Ruft den Wert der overrideAgentWrapUpTime-Eigenschaft ab.
     * 
     */
    public boolean isOverrideAgentWrapUpTime() {
        return overrideAgentWrapUpTime;
    }

    /**
     * Legt den Wert der overrideAgentWrapUpTime-Eigenschaft fest.
     * 
     */
    public void setOverrideAgentWrapUpTime(boolean value) {
        this.overrideAgentWrapUpTime = value;
    }

    /**
     * Ruft den Wert der wrapUpSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWrapUpSeconds() {
        return wrapUpSeconds;
    }

    /**
     * Legt den Wert der wrapUpSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWrapUpSeconds(Integer value) {
        this.wrapUpSeconds = value;
    }

    /**
     * Ruft den Wert der enableAutomaticStateChangeForAgents-Eigenschaft ab.
     * 
     */
    public boolean isEnableAutomaticStateChangeForAgents() {
        return enableAutomaticStateChangeForAgents;
    }

    /**
     * Legt den Wert der enableAutomaticStateChangeForAgents-Eigenschaft fest.
     * 
     */
    public void setEnableAutomaticStateChangeForAgents(boolean value) {
        this.enableAutomaticStateChangeForAgents = value;
    }

    /**
     * Ruft den Wert der agentStateAfterCall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AgentACDAutomaticState }
     *     
     */
    public AgentACDAutomaticState getAgentStateAfterCall() {
        return agentStateAfterCall;
    }

    /**
     * Legt den Wert der agentStateAfterCall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentACDAutomaticState }
     *     
     */
    public void setAgentStateAfterCall(AgentACDAutomaticState value) {
        this.agentStateAfterCall = value;
    }

    /**
     * Ruft den Wert der agentUnavailableCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentUnavailableCode() {
        return agentUnavailableCode;
    }

    /**
     * Legt den Wert der agentUnavailableCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentUnavailableCode(String value) {
        this.agentUnavailableCode = value;
    }

    /**
     * Ruft den Wert der forceDeliveryOfCalls-Eigenschaft ab.
     * 
     */
    public boolean isForceDeliveryOfCalls() {
        return forceDeliveryOfCalls;
    }

    /**
     * Legt den Wert der forceDeliveryOfCalls-Eigenschaft fest.
     * 
     */
    public void setForceDeliveryOfCalls(boolean value) {
        this.forceDeliveryOfCalls = value;
    }

    /**
     * Ruft den Wert der forceDeliveryWaitTimeSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getForceDeliveryWaitTimeSeconds() {
        return forceDeliveryWaitTimeSeconds;
    }

    /**
     * Legt den Wert der forceDeliveryWaitTimeSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setForceDeliveryWaitTimeSeconds(Integer value) {
        this.forceDeliveryWaitTimeSeconds = value;
    }

}
