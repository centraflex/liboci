//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to a SystemCommunicationBarringAlternateCallIndicatorGetListRequest. Contains a table with one row per Communication Barring Alternate Call Indicator.  The table column headings are: "Alternate Call Indicator" and "Network Server Alternate Call Indicator".
 *       
 * 
 * <p>Java-Klasse für SystemCommunicationBarringAlternateCallIndicatorGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCommunicationBarringAlternateCallIndicatorGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="alternateCallIndicatorTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCommunicationBarringAlternateCallIndicatorGetListResponse", propOrder = {
    "alternateCallIndicatorTable"
})
public class SystemCommunicationBarringAlternateCallIndicatorGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable alternateCallIndicatorTable;

    /**
     * Ruft den Wert der alternateCallIndicatorTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAlternateCallIndicatorTable() {
        return alternateCallIndicatorTable;
    }

    /**
     * Legt den Wert der alternateCallIndicatorTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAlternateCallIndicatorTable(OCITable value) {
        this.alternateCallIndicatorTable = value;
    }

}
