//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserEnhancedCallLogsGetListRequest17sp4.
 *         Total numbers of rows is:
 *         - the total number of retrievable logs of the call log type that was specified in the UserEnhancedCallLogsGetListRequest17sp4, 
 *           if a call log type was specified in the request.
 *         - the total number of retrievable logs, if no call log type was specified in the request.
 *         A list of MixedCallLogsEntry will be returned if the call logs are stored on CDS
 *         A list of ExtendedMixedCallLogsEntry will be returned if the call logs are stored on DBS
 *         The logs are sorted by date/time of the call.
 *         Replaced by UserEnhancedCallLogsGetListResponse17sp4V2.
 *         Originally deprecated in release 20.
 *       
 * 
 * <p>Java-Klasse für UserEnhancedCallLogsGetListResponse17sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserEnhancedCallLogsGetListResponse17sp4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="totalNumberOfRows" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <choice>
 *           <element name="legacyEntry">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="callLog" type="{}MixedCallLogsEntry" maxOccurs="unbounded" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *           <element name="extendedEntry">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="extendedCallLog" type="{}ExtendedMixedCallLogsEntry" maxOccurs="unbounded" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserEnhancedCallLogsGetListResponse17sp4", propOrder = {
    "totalNumberOfRows",
    "legacyEntry",
    "extendedEntry"
})
public class UserEnhancedCallLogsGetListResponse17Sp4
    extends OCIDataResponse
{

    protected int totalNumberOfRows;
    protected UserEnhancedCallLogsGetListResponse17Sp4 .LegacyEntry legacyEntry;
    protected UserEnhancedCallLogsGetListResponse17Sp4 .ExtendedEntry extendedEntry;

    /**
     * Ruft den Wert der totalNumberOfRows-Eigenschaft ab.
     * 
     */
    public int getTotalNumberOfRows() {
        return totalNumberOfRows;
    }

    /**
     * Legt den Wert der totalNumberOfRows-Eigenschaft fest.
     * 
     */
    public void setTotalNumberOfRows(int value) {
        this.totalNumberOfRows = value;
    }

    /**
     * Ruft den Wert der legacyEntry-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserEnhancedCallLogsGetListResponse17Sp4 .LegacyEntry }
     *     
     */
    public UserEnhancedCallLogsGetListResponse17Sp4 .LegacyEntry getLegacyEntry() {
        return legacyEntry;
    }

    /**
     * Legt den Wert der legacyEntry-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserEnhancedCallLogsGetListResponse17Sp4 .LegacyEntry }
     *     
     */
    public void setLegacyEntry(UserEnhancedCallLogsGetListResponse17Sp4 .LegacyEntry value) {
        this.legacyEntry = value;
    }

    /**
     * Ruft den Wert der extendedEntry-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserEnhancedCallLogsGetListResponse17Sp4 .ExtendedEntry }
     *     
     */
    public UserEnhancedCallLogsGetListResponse17Sp4 .ExtendedEntry getExtendedEntry() {
        return extendedEntry;
    }

    /**
     * Legt den Wert der extendedEntry-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserEnhancedCallLogsGetListResponse17Sp4 .ExtendedEntry }
     *     
     */
    public void setExtendedEntry(UserEnhancedCallLogsGetListResponse17Sp4 .ExtendedEntry value) {
        this.extendedEntry = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="extendedCallLog" type="{}ExtendedMixedCallLogsEntry" maxOccurs="unbounded" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "extendedCallLog"
    })
    public static class ExtendedEntry {

        protected List<ExtendedMixedCallLogsEntry> extendedCallLog;

        /**
         * Gets the value of the extendedCallLog property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the extendedCallLog property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getExtendedCallLog().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ExtendedMixedCallLogsEntry }
         * 
         * 
         * @return
         *     The value of the extendedCallLog property.
         */
        public List<ExtendedMixedCallLogsEntry> getExtendedCallLog() {
            if (extendedCallLog == null) {
                extendedCallLog = new ArrayList<>();
            }
            return this.extendedCallLog;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="callLog" type="{}MixedCallLogsEntry" maxOccurs="unbounded" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "callLog"
    })
    public static class LegacyEntry {

        protected List<MixedCallLogsEntry> callLog;

        /**
         * Gets the value of the callLog property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the callLog property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCallLog().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link MixedCallLogsEntry }
         * 
         * 
         * @return
         *     The value of the callLog property.
         */
        public List<MixedCallLogsEntry> getCallLog() {
            if (callLog == null) {
                callLog = new ArrayList<>();
            }
            return this.callLog;
        }

    }

}
