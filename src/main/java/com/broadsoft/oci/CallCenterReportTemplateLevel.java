//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterReportTemplateLevel.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterReportTemplateLevel">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="System"/>
 *     <enumeration value="Enterprise"/>
 *     <enumeration value="Group"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterReportTemplateLevel")
@XmlEnum
public enum CallCenterReportTemplateLevel {

    @XmlEnumValue("System")
    SYSTEM("System"),
    @XmlEnumValue("Enterprise")
    ENTERPRISE("Enterprise"),
    @XmlEnumValue("Group")
    GROUP("Group");
    private final String value;

    CallCenterReportTemplateLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterReportTemplateLevel fromValue(String v) {
        for (CallCenterReportTemplateLevel c: CallCenterReportTemplateLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
