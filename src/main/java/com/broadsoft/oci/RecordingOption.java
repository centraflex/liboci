//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für RecordingOption.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="RecordingOption">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Always"/>
 *     <enumeration value="Never"/>
 *     <enumeration value="On Demand"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "RecordingOption")
@XmlEnum
public enum RecordingOption {

    @XmlEnumValue("Always")
    ALWAYS("Always"),
    @XmlEnumValue("Never")
    NEVER("Never"),
    @XmlEnumValue("On Demand")
    ON_DEMAND("On Demand");
    private final String value;

    RecordingOption(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RecordingOption fromValue(String v) {
        for (RecordingOption c: RecordingOption.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
