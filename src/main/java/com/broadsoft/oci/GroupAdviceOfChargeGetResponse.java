//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupAdviceOfChargeGetRequest.
 *         Contains a list of Advice of Charge group parameters.
 *       
 * 
 * <p>Java-Klasse für GroupAdviceOfChargeGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAdviceOfChargeGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useGroupLevelAoCSettings" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="delayBetweenNotificationSeconds" type="{}AdviceOfChargeDelayBetweenNotificationSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAdviceOfChargeGetResponse", propOrder = {
    "useGroupLevelAoCSettings",
    "delayBetweenNotificationSeconds"
})
public class GroupAdviceOfChargeGetResponse
    extends OCIDataResponse
{

    protected boolean useGroupLevelAoCSettings;
    protected int delayBetweenNotificationSeconds;

    /**
     * Ruft den Wert der useGroupLevelAoCSettings-Eigenschaft ab.
     * 
     */
    public boolean isUseGroupLevelAoCSettings() {
        return useGroupLevelAoCSettings;
    }

    /**
     * Legt den Wert der useGroupLevelAoCSettings-Eigenschaft fest.
     * 
     */
    public void setUseGroupLevelAoCSettings(boolean value) {
        this.useGroupLevelAoCSettings = value;
    }

    /**
     * Ruft den Wert der delayBetweenNotificationSeconds-Eigenschaft ab.
     * 
     */
    public int getDelayBetweenNotificationSeconds() {
        return delayBetweenNotificationSeconds;
    }

    /**
     * Legt den Wert der delayBetweenNotificationSeconds-Eigenschaft fest.
     * 
     */
    public void setDelayBetweenNotificationSeconds(int value) {
        this.delayBetweenNotificationSeconds = value;
    }

}
