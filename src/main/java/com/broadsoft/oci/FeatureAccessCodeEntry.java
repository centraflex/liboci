//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Feature Access Code Entry
 * 
 * <p>Java-Klasse für FeatureAccessCodeEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="FeatureAccessCodeEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="featureAccessCodeName" type="{}FeatureAccessCodeName"/>
 *         <element name="mainCode" type="{}FeatureAccessCode" minOccurs="0"/>
 *         <element name="alternateCode" type="{}FeatureAccessCode" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FeatureAccessCodeEntry", propOrder = {
    "featureAccessCodeName",
    "mainCode",
    "alternateCode"
})
public class FeatureAccessCodeEntry {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String featureAccessCodeName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mainCode;
    @XmlElementRef(name = "alternateCode", type = JAXBElement.class, required = false)
    protected JAXBElement<String> alternateCode;

    /**
     * Ruft den Wert der featureAccessCodeName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeatureAccessCodeName() {
        return featureAccessCodeName;
    }

    /**
     * Legt den Wert der featureAccessCodeName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeatureAccessCodeName(String value) {
        this.featureAccessCodeName = value;
    }

    /**
     * Ruft den Wert der mainCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainCode() {
        return mainCode;
    }

    /**
     * Legt den Wert der mainCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainCode(String value) {
        this.mainCode = value;
    }

    /**
     * Ruft den Wert der alternateCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAlternateCode() {
        return alternateCode;
    }

    /**
     * Legt den Wert der alternateCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAlternateCode(JAXBElement<String> value) {
        this.alternateCode = value;
    }

}
