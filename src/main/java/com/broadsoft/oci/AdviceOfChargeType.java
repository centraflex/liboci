//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AdviceOfChargeType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AdviceOfChargeType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="During Call"/>
 *     <enumeration value="End Of Call"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AdviceOfChargeType")
@XmlEnum
public enum AdviceOfChargeType {

    @XmlEnumValue("During Call")
    DURING_CALL("During Call"),
    @XmlEnumValue("End Of Call")
    END_OF_CALL("End Of Call");
    private final String value;

    AdviceOfChargeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AdviceOfChargeType fromValue(String v) {
        for (AdviceOfChargeType c: AdviceOfChargeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
