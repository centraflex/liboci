//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemGroupNightForwardingGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemGroupNightForwardingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGroupNightForwardingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="nightForwardInterGroupCallsWithinEnterprise" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGroupNightForwardingGetResponse", propOrder = {
    "nightForwardInterGroupCallsWithinEnterprise"
})
public class SystemGroupNightForwardingGetResponse
    extends OCIDataResponse
{

    protected boolean nightForwardInterGroupCallsWithinEnterprise;

    /**
     * Ruft den Wert der nightForwardInterGroupCallsWithinEnterprise-Eigenschaft ab.
     * 
     */
    public boolean isNightForwardInterGroupCallsWithinEnterprise() {
        return nightForwardInterGroupCallsWithinEnterprise;
    }

    /**
     * Legt den Wert der nightForwardInterGroupCallsWithinEnterprise-Eigenschaft fest.
     * 
     */
    public void setNightForwardInterGroupCallsWithinEnterprise(boolean value) {
        this.nightForwardInterGroupCallsWithinEnterprise = value;
    }

}
