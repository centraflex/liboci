//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The response to a SystemBroadWorksAnywhereGetRequest. 
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksAnywhereGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksAnywhereGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableTransferNotification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksAnywhereGetResponse", propOrder = {
    "enableTransferNotification"
})
public class SystemBroadWorksAnywhereGetResponse
    extends OCIDataResponse
{

    protected boolean enableTransferNotification;

    /**
     * Ruft den Wert der enableTransferNotification-Eigenschaft ab.
     * 
     */
    public boolean isEnableTransferNotification() {
        return enableTransferNotification;
    }

    /**
     * Legt den Wert der enableTransferNotification-Eigenschaft fest.
     * 
     */
    public void setEnableTransferNotification(boolean value) {
        this.enableTransferNotification = value;
    }

}
