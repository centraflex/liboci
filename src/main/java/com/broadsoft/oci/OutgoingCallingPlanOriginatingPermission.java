//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für OutgoingCallingPlanOriginatingPermission.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="OutgoingCallingPlanOriginatingPermission">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Disallow"/>
 *     <enumeration value="Allow"/>
 *     <enumeration value="Authorization Code Required"/>
 *     <enumeration value="Transfer To First Transfer Number"/>
 *     <enumeration value="Transfer To Second Transfer Number"/>
 *     <enumeration value="Transfer To Third Transfer Number"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "OutgoingCallingPlanOriginatingPermission")
@XmlEnum
public enum OutgoingCallingPlanOriginatingPermission {

    @XmlEnumValue("Disallow")
    DISALLOW("Disallow"),
    @XmlEnumValue("Allow")
    ALLOW("Allow"),
    @XmlEnumValue("Authorization Code Required")
    AUTHORIZATION_CODE_REQUIRED("Authorization Code Required"),
    @XmlEnumValue("Transfer To First Transfer Number")
    TRANSFER_TO_FIRST_TRANSFER_NUMBER("Transfer To First Transfer Number"),
    @XmlEnumValue("Transfer To Second Transfer Number")
    TRANSFER_TO_SECOND_TRANSFER_NUMBER("Transfer To Second Transfer Number"),
    @XmlEnumValue("Transfer To Third Transfer Number")
    TRANSFER_TO_THIRD_TRANSFER_NUMBER("Transfer To Third Transfer Number");
    private final String value;

    OutgoingCallingPlanOriginatingPermission(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OutgoingCallingPlanOriginatingPermission fromValue(String v) {
        for (OutgoingCallingPlanOriginatingPermission c: OutgoingCallingPlanOriginatingPermission.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
