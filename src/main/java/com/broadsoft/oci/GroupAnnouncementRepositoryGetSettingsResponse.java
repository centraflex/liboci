//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupAnnouncementFileGetSettingsRequest.
 *         The response contains the current total file size (KB) for the group across
 *         all media types and the maximum total file size (MB) allowed for the group.
 *         It also indicates the maximum file size for individual audio and video files.
 *       
 * 
 * <p>Java-Klasse für GroupAnnouncementRepositoryGetSettingsResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAnnouncementRepositoryGetSettingsResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="totalFileSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="maxAudioFileSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="maxVideoFileSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="maxFileSize" type="{}RepositoryTotalFileSize"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAnnouncementRepositoryGetSettingsResponse", propOrder = {
    "totalFileSize",
    "maxAudioFileSize",
    "maxVideoFileSize",
    "maxFileSize"
})
public class GroupAnnouncementRepositoryGetSettingsResponse
    extends OCIDataResponse
{

    protected int totalFileSize;
    protected int maxAudioFileSize;
    protected int maxVideoFileSize;
    protected int maxFileSize;

    /**
     * Ruft den Wert der totalFileSize-Eigenschaft ab.
     * 
     */
    public int getTotalFileSize() {
        return totalFileSize;
    }

    /**
     * Legt den Wert der totalFileSize-Eigenschaft fest.
     * 
     */
    public void setTotalFileSize(int value) {
        this.totalFileSize = value;
    }

    /**
     * Ruft den Wert der maxAudioFileSize-Eigenschaft ab.
     * 
     */
    public int getMaxAudioFileSize() {
        return maxAudioFileSize;
    }

    /**
     * Legt den Wert der maxAudioFileSize-Eigenschaft fest.
     * 
     */
    public void setMaxAudioFileSize(int value) {
        this.maxAudioFileSize = value;
    }

    /**
     * Ruft den Wert der maxVideoFileSize-Eigenschaft ab.
     * 
     */
    public int getMaxVideoFileSize() {
        return maxVideoFileSize;
    }

    /**
     * Legt den Wert der maxVideoFileSize-Eigenschaft fest.
     * 
     */
    public void setMaxVideoFileSize(int value) {
        this.maxVideoFileSize = value;
    }

    /**
     * Ruft den Wert der maxFileSize-Eigenschaft ab.
     * 
     */
    public int getMaxFileSize() {
        return maxFileSize;
    }

    /**
     * Legt den Wert der maxFileSize-Eigenschaft fest.
     * 
     */
    public void setMaxFileSize(int value) {
        this.maxFileSize = value;
    }

}
