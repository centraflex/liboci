//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *       A list of user services that replaces existing user services assgined to the user.
 *       
 *       If a service is already assigned to the user, the service quantitiy will be updated if included.
 *     
 * 
 * <p>Java-Klasse für ReplacementCombinedUserServiceAssignmentList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ReplacementCombinedUserServiceAssignmentList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="serviceName" type="{}CombinedUserServiceAssignment" maxOccurs="unbounded"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReplacementCombinedUserServiceAssignmentList", propOrder = {
    "serviceName"
})
public class ReplacementCombinedUserServiceAssignmentList {

    @XmlElement(required = true)
    protected List<CombinedUserServiceAssignment> serviceName;

    /**
     * Gets the value of the serviceName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the serviceName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CombinedUserServiceAssignment }
     * 
     * 
     * @return
     *     The value of the serviceName property.
     */
    public List<CombinedUserServiceAssignment> getServiceName() {
        if (serviceName == null) {
            serviceName = new ArrayList<>();
        }
        return this.serviceName;
    }

}
