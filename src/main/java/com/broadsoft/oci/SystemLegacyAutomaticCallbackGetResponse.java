//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemLegacyAutomaticCallbackGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemLegacyAutomaticCallbackGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemLegacyAutomaticCallbackGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maxMonitorsPerOriginator" type="{}LegacyAutomaticCallbackMaxMonitorsPerOriginator"/>
 *         <element name="maxMonitorsPerTerminator" type="{}LegacyAutomaticCallbackMaxMonitorsPerTerminator"/>
 *         <element name="t2Minutes" type="{}LegacyAutomaticCallbackT2Minutes"/>
 *         <element name="t4Seconds" type="{}LegacyAutomaticCallbackT4Seconds"/>
 *         <element name="t5Seconds" type="{}LegacyAutomaticCallbackT5Seconds"/>
 *         <element name="t6Minutes" type="{}LegacyAutomaticCallbackT6Minutes"/>
 *         <element name="t7Minutes" type="{}LegacyAutomaticCallbackT7Minutes"/>
 *         <element name="t8Seconds" type="{}LegacyAutomaticCallbackT8Seconds"/>
 *         <element name="tRingSeconds" type="{}LegacyAutomaticCallbackTRingSeconds"/>
 *         <element name="t10OMinutes" type="{}LegacyAutomaticCallbackT10OMinutes"/>
 *         <element name="t10TMinutes" type="{}LegacyAutomaticCallbackT10TMinutes"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemLegacyAutomaticCallbackGetResponse", propOrder = {
    "maxMonitorsPerOriginator",
    "maxMonitorsPerTerminator",
    "t2Minutes",
    "t4Seconds",
    "t5Seconds",
    "t6Minutes",
    "t7Minutes",
    "t8Seconds",
    "tRingSeconds",
    "t10OMinutes",
    "t10TMinutes"
})
public class SystemLegacyAutomaticCallbackGetResponse
    extends OCIDataResponse
{

    protected int maxMonitorsPerOriginator;
    protected int maxMonitorsPerTerminator;
    protected int t2Minutes;
    protected int t4Seconds;
    protected int t5Seconds;
    protected int t6Minutes;
    protected int t7Minutes;
    protected int t8Seconds;
    protected int tRingSeconds;
    protected int t10OMinutes;
    protected int t10TMinutes;

    /**
     * Ruft den Wert der maxMonitorsPerOriginator-Eigenschaft ab.
     * 
     */
    public int getMaxMonitorsPerOriginator() {
        return maxMonitorsPerOriginator;
    }

    /**
     * Legt den Wert der maxMonitorsPerOriginator-Eigenschaft fest.
     * 
     */
    public void setMaxMonitorsPerOriginator(int value) {
        this.maxMonitorsPerOriginator = value;
    }

    /**
     * Ruft den Wert der maxMonitorsPerTerminator-Eigenschaft ab.
     * 
     */
    public int getMaxMonitorsPerTerminator() {
        return maxMonitorsPerTerminator;
    }

    /**
     * Legt den Wert der maxMonitorsPerTerminator-Eigenschaft fest.
     * 
     */
    public void setMaxMonitorsPerTerminator(int value) {
        this.maxMonitorsPerTerminator = value;
    }

    /**
     * Ruft den Wert der t2Minutes-Eigenschaft ab.
     * 
     */
    public int getT2Minutes() {
        return t2Minutes;
    }

    /**
     * Legt den Wert der t2Minutes-Eigenschaft fest.
     * 
     */
    public void setT2Minutes(int value) {
        this.t2Minutes = value;
    }

    /**
     * Ruft den Wert der t4Seconds-Eigenschaft ab.
     * 
     */
    public int getT4Seconds() {
        return t4Seconds;
    }

    /**
     * Legt den Wert der t4Seconds-Eigenschaft fest.
     * 
     */
    public void setT4Seconds(int value) {
        this.t4Seconds = value;
    }

    /**
     * Ruft den Wert der t5Seconds-Eigenschaft ab.
     * 
     */
    public int getT5Seconds() {
        return t5Seconds;
    }

    /**
     * Legt den Wert der t5Seconds-Eigenschaft fest.
     * 
     */
    public void setT5Seconds(int value) {
        this.t5Seconds = value;
    }

    /**
     * Ruft den Wert der t6Minutes-Eigenschaft ab.
     * 
     */
    public int getT6Minutes() {
        return t6Minutes;
    }

    /**
     * Legt den Wert der t6Minutes-Eigenschaft fest.
     * 
     */
    public void setT6Minutes(int value) {
        this.t6Minutes = value;
    }

    /**
     * Ruft den Wert der t7Minutes-Eigenschaft ab.
     * 
     */
    public int getT7Minutes() {
        return t7Minutes;
    }

    /**
     * Legt den Wert der t7Minutes-Eigenschaft fest.
     * 
     */
    public void setT7Minutes(int value) {
        this.t7Minutes = value;
    }

    /**
     * Ruft den Wert der t8Seconds-Eigenschaft ab.
     * 
     */
    public int getT8Seconds() {
        return t8Seconds;
    }

    /**
     * Legt den Wert der t8Seconds-Eigenschaft fest.
     * 
     */
    public void setT8Seconds(int value) {
        this.t8Seconds = value;
    }

    /**
     * Ruft den Wert der tRingSeconds-Eigenschaft ab.
     * 
     */
    public int getTRingSeconds() {
        return tRingSeconds;
    }

    /**
     * Legt den Wert der tRingSeconds-Eigenschaft fest.
     * 
     */
    public void setTRingSeconds(int value) {
        this.tRingSeconds = value;
    }

    /**
     * Ruft den Wert der t10OMinutes-Eigenschaft ab.
     * 
     */
    public int getT10OMinutes() {
        return t10OMinutes;
    }

    /**
     * Legt den Wert der t10OMinutes-Eigenschaft fest.
     * 
     */
    public void setT10OMinutes(int value) {
        this.t10OMinutes = value;
    }

    /**
     * Ruft den Wert der t10TMinutes-Eigenschaft ab.
     * 
     */
    public int getT10TMinutes() {
        return t10TMinutes;
    }

    /**
     * Legt den Wert der t10TMinutes-Eigenschaft fest.
     * 
     */
    public void setT10TMinutes(int value) {
        this.t10TMinutes = value;
    }

}
