//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Used to sort the SortOrderEnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentPagedSortedListRequest.
 *       
 * 
 * <p>Java-Klasse für SortOrderEnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentPagedSortedList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SortOrderEnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentPagedSortedList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <choice>
 *         <element name="sortByMobileDirectoryNumber" type="{}SortByMobileDirectoryNumber"/>
 *         <element name="sortByDn" type="{}SortByDn"/>
 *         <element name="sortByDepartmentName" type="{}SortByDepartmentName"/>
 *         <element name="sortByUserId" type="{}SortByUserId"/>
 *         <element name="sortByUserFirstName" type="{}SortByUserFirstName"/>
 *         <element name="sortByUserLastName" type="{}SortByUserLastName"/>
 *         <element name="sortByExtension" type="{}SortByExtension"/>
 *         <element name="sortByDnAvailable" type="{}SortByDnAvailable"/>
 *       </choice>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SortOrderEnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentPagedSortedList", propOrder = {
    "sortByMobileDirectoryNumber",
    "sortByDn",
    "sortByDepartmentName",
    "sortByUserId",
    "sortByUserFirstName",
    "sortByUserLastName",
    "sortByExtension",
    "sortByDnAvailable"
})
public class SortOrderEnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentPagedSortedList {

    protected SortByMobileDirectoryNumber sortByMobileDirectoryNumber;
    protected SortByDn sortByDn;
    protected SortByDepartmentName sortByDepartmentName;
    protected SortByUserId sortByUserId;
    protected SortByUserFirstName sortByUserFirstName;
    protected SortByUserLastName sortByUserLastName;
    protected SortByExtension sortByExtension;
    protected SortByDnAvailable sortByDnAvailable;

    /**
     * Ruft den Wert der sortByMobileDirectoryNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByMobileDirectoryNumber }
     *     
     */
    public SortByMobileDirectoryNumber getSortByMobileDirectoryNumber() {
        return sortByMobileDirectoryNumber;
    }

    /**
     * Legt den Wert der sortByMobileDirectoryNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByMobileDirectoryNumber }
     *     
     */
    public void setSortByMobileDirectoryNumber(SortByMobileDirectoryNumber value) {
        this.sortByMobileDirectoryNumber = value;
    }

    /**
     * Ruft den Wert der sortByDn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByDn }
     *     
     */
    public SortByDn getSortByDn() {
        return sortByDn;
    }

    /**
     * Legt den Wert der sortByDn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByDn }
     *     
     */
    public void setSortByDn(SortByDn value) {
        this.sortByDn = value;
    }

    /**
     * Ruft den Wert der sortByDepartmentName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByDepartmentName }
     *     
     */
    public SortByDepartmentName getSortByDepartmentName() {
        return sortByDepartmentName;
    }

    /**
     * Legt den Wert der sortByDepartmentName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByDepartmentName }
     *     
     */
    public void setSortByDepartmentName(SortByDepartmentName value) {
        this.sortByDepartmentName = value;
    }

    /**
     * Ruft den Wert der sortByUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByUserId }
     *     
     */
    public SortByUserId getSortByUserId() {
        return sortByUserId;
    }

    /**
     * Legt den Wert der sortByUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByUserId }
     *     
     */
    public void setSortByUserId(SortByUserId value) {
        this.sortByUserId = value;
    }

    /**
     * Ruft den Wert der sortByUserFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByUserFirstName }
     *     
     */
    public SortByUserFirstName getSortByUserFirstName() {
        return sortByUserFirstName;
    }

    /**
     * Legt den Wert der sortByUserFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByUserFirstName }
     *     
     */
    public void setSortByUserFirstName(SortByUserFirstName value) {
        this.sortByUserFirstName = value;
    }

    /**
     * Ruft den Wert der sortByUserLastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByUserLastName }
     *     
     */
    public SortByUserLastName getSortByUserLastName() {
        return sortByUserLastName;
    }

    /**
     * Legt den Wert der sortByUserLastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByUserLastName }
     *     
     */
    public void setSortByUserLastName(SortByUserLastName value) {
        this.sortByUserLastName = value;
    }

    /**
     * Ruft den Wert der sortByExtension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByExtension }
     *     
     */
    public SortByExtension getSortByExtension() {
        return sortByExtension;
    }

    /**
     * Legt den Wert der sortByExtension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByExtension }
     *     
     */
    public void setSortByExtension(SortByExtension value) {
        this.sortByExtension = value;
    }

    /**
     * Ruft den Wert der sortByDnAvailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByDnAvailable }
     *     
     */
    public SortByDnAvailable getSortByDnAvailable() {
        return sortByDnAvailable;
    }

    /**
     * Legt den Wert der sortByDnAvailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByDnAvailable }
     *     
     */
    public void setSortByDnAvailable(SortByDnAvailable value) {
        this.sortByDnAvailable = value;
    }

}
