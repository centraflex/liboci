//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the route list setting and the list of number ranges and number prefixes assigned to a user.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserRouteListModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserRouteListModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="treatOriginationsAndPBXRedirectionsAsScreened" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useRouteListIdentityForNonEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useRouteListIdentityForEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="assignedNumberRangeStartList" type="{}ReplacementDNList" minOccurs="0"/>
 *         <element name="assignedNumberPrefixList" type="{}EnterpriseTrunkReplacementNumberPrefixList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserRouteListModifyRequest", propOrder = {
    "userId",
    "treatOriginationsAndPBXRedirectionsAsScreened",
    "useRouteListIdentityForNonEmergencyCalls",
    "useRouteListIdentityForEmergencyCalls",
    "assignedNumberRangeStartList",
    "assignedNumberPrefixList"
})
public class UserRouteListModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean treatOriginationsAndPBXRedirectionsAsScreened;
    protected Boolean useRouteListIdentityForNonEmergencyCalls;
    protected Boolean useRouteListIdentityForEmergencyCalls;
    @XmlElementRef(name = "assignedNumberRangeStartList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementDNList> assignedNumberRangeStartList;
    @XmlElementRef(name = "assignedNumberPrefixList", type = JAXBElement.class, required = false)
    protected JAXBElement<EnterpriseTrunkReplacementNumberPrefixList> assignedNumberPrefixList;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der treatOriginationsAndPBXRedirectionsAsScreened-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTreatOriginationsAndPBXRedirectionsAsScreened() {
        return treatOriginationsAndPBXRedirectionsAsScreened;
    }

    /**
     * Legt den Wert der treatOriginationsAndPBXRedirectionsAsScreened-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTreatOriginationsAndPBXRedirectionsAsScreened(Boolean value) {
        this.treatOriginationsAndPBXRedirectionsAsScreened = value;
    }

    /**
     * Ruft den Wert der useRouteListIdentityForNonEmergencyCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseRouteListIdentityForNonEmergencyCalls() {
        return useRouteListIdentityForNonEmergencyCalls;
    }

    /**
     * Legt den Wert der useRouteListIdentityForNonEmergencyCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseRouteListIdentityForNonEmergencyCalls(Boolean value) {
        this.useRouteListIdentityForNonEmergencyCalls = value;
    }

    /**
     * Ruft den Wert der useRouteListIdentityForEmergencyCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseRouteListIdentityForEmergencyCalls() {
        return useRouteListIdentityForEmergencyCalls;
    }

    /**
     * Legt den Wert der useRouteListIdentityForEmergencyCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseRouteListIdentityForEmergencyCalls(Boolean value) {
        this.useRouteListIdentityForEmergencyCalls = value;
    }

    /**
     * Ruft den Wert der assignedNumberRangeStartList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementDNList }{@code >}
     *     
     */
    public JAXBElement<ReplacementDNList> getAssignedNumberRangeStartList() {
        return assignedNumberRangeStartList;
    }

    /**
     * Legt den Wert der assignedNumberRangeStartList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementDNList }{@code >}
     *     
     */
    public void setAssignedNumberRangeStartList(JAXBElement<ReplacementDNList> value) {
        this.assignedNumberRangeStartList = value;
    }

    /**
     * Ruft den Wert der assignedNumberPrefixList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link EnterpriseTrunkReplacementNumberPrefixList }{@code >}
     *     
     */
    public JAXBElement<EnterpriseTrunkReplacementNumberPrefixList> getAssignedNumberPrefixList() {
        return assignedNumberPrefixList;
    }

    /**
     * Legt den Wert der assignedNumberPrefixList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link EnterpriseTrunkReplacementNumberPrefixList }{@code >}
     *     
     */
    public void setAssignedNumberPrefixList(JAXBElement<EnterpriseTrunkReplacementNumberPrefixList> value) {
        this.assignedNumberPrefixList = value;
    }

}
