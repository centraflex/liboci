//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupPolicyGetRequest14sp4.
 *         Contains the policy settings for the group.
 *       
 * 
 * <p>Java-Klasse für GroupPolicyGetResponse14sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupPolicyGetResponse14sp4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="callingPlanAccess" type="{}GroupCallingPlanAccess"/>
 *         <element name="extensionAccess" type="{}GroupExtensionAccess"/>
 *         <element name="ldapIntegrationAccess" type="{}GroupLDAPIntegrationAccess"/>
 *         <element name="voiceMessagingAccess" type="{}GroupVoiceMessagingAccess"/>
 *         <element name="departmentAdminUserAccess" type="{}GroupDepartmentAdminUserAccess"/>
 *         <element name="departmentAdminTrunkGroupAccess" type="{}GroupDepartmentAdminTrunkGroupAccess"/>
 *         <element name="userAuthenticationAccess" type="{}GroupUserAuthenticationAccess"/>
 *         <element name="userGroupDirectoryAccess" type="{}GroupUserGroupDirectoryAccess"/>
 *         <element name="userProfileAccess" type="{}GroupUserProfileAccess"/>
 *         <element name="userEnhancedCallLogAccess" type="{}GroupUserCallLogAccess"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupPolicyGetResponse14sp4", propOrder = {
    "callingPlanAccess",
    "extensionAccess",
    "ldapIntegrationAccess",
    "voiceMessagingAccess",
    "departmentAdminUserAccess",
    "departmentAdminTrunkGroupAccess",
    "userAuthenticationAccess",
    "userGroupDirectoryAccess",
    "userProfileAccess",
    "userEnhancedCallLogAccess"
})
public class GroupPolicyGetResponse14Sp4
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupCallingPlanAccess callingPlanAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupExtensionAccess extensionAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupLDAPIntegrationAccess ldapIntegrationAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupVoiceMessagingAccess voiceMessagingAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminUserAccess departmentAdminUserAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminTrunkGroupAccess departmentAdminTrunkGroupAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupUserAuthenticationAccess userAuthenticationAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupUserGroupDirectoryAccess userGroupDirectoryAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupUserProfileAccess userProfileAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupUserCallLogAccess userEnhancedCallLogAccess;

    /**
     * Ruft den Wert der callingPlanAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupCallingPlanAccess }
     *     
     */
    public GroupCallingPlanAccess getCallingPlanAccess() {
        return callingPlanAccess;
    }

    /**
     * Legt den Wert der callingPlanAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupCallingPlanAccess }
     *     
     */
    public void setCallingPlanAccess(GroupCallingPlanAccess value) {
        this.callingPlanAccess = value;
    }

    /**
     * Ruft den Wert der extensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupExtensionAccess }
     *     
     */
    public GroupExtensionAccess getExtensionAccess() {
        return extensionAccess;
    }

    /**
     * Legt den Wert der extensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupExtensionAccess }
     *     
     */
    public void setExtensionAccess(GroupExtensionAccess value) {
        this.extensionAccess = value;
    }

    /**
     * Ruft den Wert der ldapIntegrationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupLDAPIntegrationAccess }
     *     
     */
    public GroupLDAPIntegrationAccess getLdapIntegrationAccess() {
        return ldapIntegrationAccess;
    }

    /**
     * Legt den Wert der ldapIntegrationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupLDAPIntegrationAccess }
     *     
     */
    public void setLdapIntegrationAccess(GroupLDAPIntegrationAccess value) {
        this.ldapIntegrationAccess = value;
    }

    /**
     * Ruft den Wert der voiceMessagingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupVoiceMessagingAccess }
     *     
     */
    public GroupVoiceMessagingAccess getVoiceMessagingAccess() {
        return voiceMessagingAccess;
    }

    /**
     * Legt den Wert der voiceMessagingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupVoiceMessagingAccess }
     *     
     */
    public void setVoiceMessagingAccess(GroupVoiceMessagingAccess value) {
        this.voiceMessagingAccess = value;
    }

    /**
     * Ruft den Wert der departmentAdminUserAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminUserAccess }
     *     
     */
    public GroupDepartmentAdminUserAccess getDepartmentAdminUserAccess() {
        return departmentAdminUserAccess;
    }

    /**
     * Legt den Wert der departmentAdminUserAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminUserAccess }
     *     
     */
    public void setDepartmentAdminUserAccess(GroupDepartmentAdminUserAccess value) {
        this.departmentAdminUserAccess = value;
    }

    /**
     * Ruft den Wert der departmentAdminTrunkGroupAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminTrunkGroupAccess }
     *     
     */
    public GroupDepartmentAdminTrunkGroupAccess getDepartmentAdminTrunkGroupAccess() {
        return departmentAdminTrunkGroupAccess;
    }

    /**
     * Legt den Wert der departmentAdminTrunkGroupAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminTrunkGroupAccess }
     *     
     */
    public void setDepartmentAdminTrunkGroupAccess(GroupDepartmentAdminTrunkGroupAccess value) {
        this.departmentAdminTrunkGroupAccess = value;
    }

    /**
     * Ruft den Wert der userAuthenticationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserAuthenticationAccess }
     *     
     */
    public GroupUserAuthenticationAccess getUserAuthenticationAccess() {
        return userAuthenticationAccess;
    }

    /**
     * Legt den Wert der userAuthenticationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserAuthenticationAccess }
     *     
     */
    public void setUserAuthenticationAccess(GroupUserAuthenticationAccess value) {
        this.userAuthenticationAccess = value;
    }

    /**
     * Ruft den Wert der userGroupDirectoryAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserGroupDirectoryAccess }
     *     
     */
    public GroupUserGroupDirectoryAccess getUserGroupDirectoryAccess() {
        return userGroupDirectoryAccess;
    }

    /**
     * Legt den Wert der userGroupDirectoryAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserGroupDirectoryAccess }
     *     
     */
    public void setUserGroupDirectoryAccess(GroupUserGroupDirectoryAccess value) {
        this.userGroupDirectoryAccess = value;
    }

    /**
     * Ruft den Wert der userProfileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserProfileAccess }
     *     
     */
    public GroupUserProfileAccess getUserProfileAccess() {
        return userProfileAccess;
    }

    /**
     * Legt den Wert der userProfileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserProfileAccess }
     *     
     */
    public void setUserProfileAccess(GroupUserProfileAccess value) {
        this.userProfileAccess = value;
    }

    /**
     * Ruft den Wert der userEnhancedCallLogAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserCallLogAccess }
     *     
     */
    public GroupUserCallLogAccess getUserEnhancedCallLogAccess() {
        return userEnhancedCallLogAccess;
    }

    /**
     * Legt den Wert der userEnhancedCallLogAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserCallLogAccess }
     *     
     */
    public void setUserEnhancedCallLogAccess(GroupUserCallLogAccess value) {
        this.userEnhancedCallLogAccess = value;
    }

}
