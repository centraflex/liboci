//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         CommPilot Express Unavailable Configuration used in the context of a modify.
 *       
 * 
 * <p>Java-Klasse für CommPilotExpressUnavailableModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CommPilotExpressUnavailableModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="incomingCalls" type="{}CommPilotExpressRedirectionWithExceptionModify" minOccurs="0"/>
 *         <element name="voiceMailGreeting" type="{}CommPilotExpressVoiceMailGreeting" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommPilotExpressUnavailableModify", propOrder = {
    "incomingCalls",
    "voiceMailGreeting"
})
public class CommPilotExpressUnavailableModify {

    protected CommPilotExpressRedirectionWithExceptionModify incomingCalls;
    @XmlSchemaType(name = "token")
    protected CommPilotExpressVoiceMailGreeting voiceMailGreeting;

    /**
     * Ruft den Wert der incomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressRedirectionWithExceptionModify }
     *     
     */
    public CommPilotExpressRedirectionWithExceptionModify getIncomingCalls() {
        return incomingCalls;
    }

    /**
     * Legt den Wert der incomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressRedirectionWithExceptionModify }
     *     
     */
    public void setIncomingCalls(CommPilotExpressRedirectionWithExceptionModify value) {
        this.incomingCalls = value;
    }

    /**
     * Ruft den Wert der voiceMailGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressVoiceMailGreeting }
     *     
     */
    public CommPilotExpressVoiceMailGreeting getVoiceMailGreeting() {
        return voiceMailGreeting;
    }

    /**
     * Legt den Wert der voiceMailGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressVoiceMailGreeting }
     *     
     */
    public void setVoiceMailGreeting(CommPilotExpressVoiceMailGreeting value) {
        this.voiceMailGreeting = value;
    }

}
