//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemAutomaticCollectCallGetRequest22.
 *         Returns system Automatic Collect Call service settings.
 *       
 * 
 * <p>Java-Klasse für SystemAutomaticCollectCallGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAutomaticCollectCallGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableAutomaticCollectCall" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableConnectTone" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeCountryCodeInCic" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAutomaticCollectCallGetResponse22", propOrder = {
    "enableAutomaticCollectCall",
    "enableConnectTone",
    "includeCountryCodeInCic"
})
public class SystemAutomaticCollectCallGetResponse22
    extends OCIDataResponse
{

    protected boolean enableAutomaticCollectCall;
    protected boolean enableConnectTone;
    protected boolean includeCountryCodeInCic;

    /**
     * Ruft den Wert der enableAutomaticCollectCall-Eigenschaft ab.
     * 
     */
    public boolean isEnableAutomaticCollectCall() {
        return enableAutomaticCollectCall;
    }

    /**
     * Legt den Wert der enableAutomaticCollectCall-Eigenschaft fest.
     * 
     */
    public void setEnableAutomaticCollectCall(boolean value) {
        this.enableAutomaticCollectCall = value;
    }

    /**
     * Ruft den Wert der enableConnectTone-Eigenschaft ab.
     * 
     */
    public boolean isEnableConnectTone() {
        return enableConnectTone;
    }

    /**
     * Legt den Wert der enableConnectTone-Eigenschaft fest.
     * 
     */
    public void setEnableConnectTone(boolean value) {
        this.enableConnectTone = value;
    }

    /**
     * Ruft den Wert der includeCountryCodeInCic-Eigenschaft ab.
     * 
     */
    public boolean isIncludeCountryCodeInCic() {
        return includeCountryCodeInCic;
    }

    /**
     * Legt den Wert der includeCountryCodeInCic-Eigenschaft fest.
     * 
     */
    public void setIncludeCountryCodeInCic(boolean value) {
        this.includeCountryCodeInCic = value;
    }

}
