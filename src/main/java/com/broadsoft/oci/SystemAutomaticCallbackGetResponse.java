//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemAutomaticCallbackGetRequest.
 *         Replaced By: SystemAutomaticCallbackGetResponse15
 *       
 * 
 * <p>Java-Klasse für SystemAutomaticCallbackGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAutomaticCallbackGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="monitorMinutes" type="{}AutomaticCallbackMonitorMinutes"/>
 *         <element name="waitBetweenRetryOriginatorMinutes" type="{}AutomaticCallbackWaitBetweenRetryOriginatorMinutes"/>
 *         <element name="maxMonitorsPerOriginator" type="{}AutomaticCallbackMaxMonitorsPerOriginator"/>
 *         <element name="maxCallbackRings" type="{}AutomaticCallbackMaxCallbackRings"/>
 *         <element name="maxRetryOriginatorMinutes" type="{}AutomaticCallbackMaxRetryOriginatorMinutes"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAutomaticCallbackGetResponse", propOrder = {
    "monitorMinutes",
    "waitBetweenRetryOriginatorMinutes",
    "maxMonitorsPerOriginator",
    "maxCallbackRings",
    "maxRetryOriginatorMinutes"
})
public class SystemAutomaticCallbackGetResponse
    extends OCIDataResponse
{

    protected int monitorMinutes;
    protected int waitBetweenRetryOriginatorMinutes;
    protected int maxMonitorsPerOriginator;
    protected int maxCallbackRings;
    protected int maxRetryOriginatorMinutes;

    /**
     * Ruft den Wert der monitorMinutes-Eigenschaft ab.
     * 
     */
    public int getMonitorMinutes() {
        return monitorMinutes;
    }

    /**
     * Legt den Wert der monitorMinutes-Eigenschaft fest.
     * 
     */
    public void setMonitorMinutes(int value) {
        this.monitorMinutes = value;
    }

    /**
     * Ruft den Wert der waitBetweenRetryOriginatorMinutes-Eigenschaft ab.
     * 
     */
    public int getWaitBetweenRetryOriginatorMinutes() {
        return waitBetweenRetryOriginatorMinutes;
    }

    /**
     * Legt den Wert der waitBetweenRetryOriginatorMinutes-Eigenschaft fest.
     * 
     */
    public void setWaitBetweenRetryOriginatorMinutes(int value) {
        this.waitBetweenRetryOriginatorMinutes = value;
    }

    /**
     * Ruft den Wert der maxMonitorsPerOriginator-Eigenschaft ab.
     * 
     */
    public int getMaxMonitorsPerOriginator() {
        return maxMonitorsPerOriginator;
    }

    /**
     * Legt den Wert der maxMonitorsPerOriginator-Eigenschaft fest.
     * 
     */
    public void setMaxMonitorsPerOriginator(int value) {
        this.maxMonitorsPerOriginator = value;
    }

    /**
     * Ruft den Wert der maxCallbackRings-Eigenschaft ab.
     * 
     */
    public int getMaxCallbackRings() {
        return maxCallbackRings;
    }

    /**
     * Legt den Wert der maxCallbackRings-Eigenschaft fest.
     * 
     */
    public void setMaxCallbackRings(int value) {
        this.maxCallbackRings = value;
    }

    /**
     * Ruft den Wert der maxRetryOriginatorMinutes-Eigenschaft ab.
     * 
     */
    public int getMaxRetryOriginatorMinutes() {
        return maxRetryOriginatorMinutes;
    }

    /**
     * Legt den Wert der maxRetryOriginatorMinutes-Eigenschaft fest.
     * 
     */
    public void setMaxRetryOriginatorMinutes(int value) {
        this.maxRetryOriginatorMinutes = value;
    }

}
