//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für BasicCallLogsType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="BasicCallLogsType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Placed"/>
 *     <enumeration value="Received"/>
 *     <enumeration value="Missed"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "BasicCallLogsType")
@XmlEnum
public enum BasicCallLogsType {

    @XmlEnumValue("Placed")
    PLACED("Placed"),
    @XmlEnumValue("Received")
    RECEIVED("Received"),
    @XmlEnumValue("Missed")
    MISSED("Missed");
    private final String value;

    BasicCallLogsType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BasicCallLogsType fromValue(String v) {
        for (BasicCallLogsType c: BasicCallLogsType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
