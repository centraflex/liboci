//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Requests the service provider's service authorization information for a specific service or service pack.
 *         The response is either ServiceProviderServiceGetAuthorizationResponse or ErrorResponse.
 *         
 *         The following element is used in AS mode and triggers an ErrorResponse in XS data mode:
 *           servicePackName
 *       
 * 
 * <p>Java-Klasse für ServiceProviderServiceGetAuthorizationRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderServiceGetAuthorizationRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <choice>
 *           <element name="userServiceName" type="{}UserService"/>
 *           <element name="groupServiceName" type="{}GroupService"/>
 *           <element name="servicePackName" type="{}ServicePackName"/>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderServiceGetAuthorizationRequest", propOrder = {
    "serviceProviderId",
    "userServiceName",
    "groupServiceName",
    "servicePackName"
})
public class ServiceProviderServiceGetAuthorizationRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userServiceName;
    @XmlSchemaType(name = "token")
    protected GroupService groupServiceName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String servicePackName;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der userServiceName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserServiceName() {
        return userServiceName;
    }

    /**
     * Legt den Wert der userServiceName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserServiceName(String value) {
        this.userServiceName = value;
    }

    /**
     * Ruft den Wert der groupServiceName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupService }
     *     
     */
    public GroupService getGroupServiceName() {
        return groupServiceName;
    }

    /**
     * Legt den Wert der groupServiceName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupService }
     *     
     */
    public void setGroupServiceName(GroupService value) {
        this.groupServiceName = value;
    }

    /**
     * Ruft den Wert der servicePackName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicePackName() {
        return servicePackName;
    }

    /**
     * Legt den Wert der servicePackName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicePackName(String value) {
        this.servicePackName = value;
    }

}
