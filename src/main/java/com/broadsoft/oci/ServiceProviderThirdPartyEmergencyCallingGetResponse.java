//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the ServiceProviderThirdPartyEmergencyCallingGetRequest.
 *         The response contains the third-party emergency call service settings for the service provider.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderThirdPartyEmergencyCallingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderThirdPartyEmergencyCallingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="allowActivation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="customerId" type="{}CustomerId" minOccurs="0"/>
 *         <element name="hasGroupEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderThirdPartyEmergencyCallingGetResponse", propOrder = {
    "allowActivation",
    "customerId",
    "hasGroupEnabled"
})
public class ServiceProviderThirdPartyEmergencyCallingGetResponse
    extends OCIDataResponse
{

    protected boolean allowActivation;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String customerId;
    protected boolean hasGroupEnabled;

    /**
     * Ruft den Wert der allowActivation-Eigenschaft ab.
     * 
     */
    public boolean isAllowActivation() {
        return allowActivation;
    }

    /**
     * Legt den Wert der allowActivation-Eigenschaft fest.
     * 
     */
    public void setAllowActivation(boolean value) {
        this.allowActivation = value;
    }

    /**
     * Ruft den Wert der customerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Legt den Wert der customerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerId(String value) {
        this.customerId = value;
    }

    /**
     * Ruft den Wert der hasGroupEnabled-Eigenschaft ab.
     * 
     */
    public boolean isHasGroupEnabled() {
        return hasGroupEnabled;
    }

    /**
     * Legt den Wert der hasGroupEnabled-Eigenschaft fest.
     * 
     */
    public void setHasGroupEnabled(boolean value) {
        this.hasGroupEnabled = value;
    }

}
