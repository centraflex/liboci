//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailableListRequest22.
 *         The response contains a table with columns: "Phone Number", "E164 Phone Number".
 * 
 *         The "Phone Number" column contains Mobile Subscriber DNs not yet assigned to any user.
 *         The "E164 Phone Number" column contains Mobile Subscriber DNs not yet assigned to any user in E.164 format.
 *       
 * 
 * <p>Java-Klasse für EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailableListResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailableListResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="availableMobileSubscriberDirectoryNumberTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailableListResponse22", propOrder = {
    "availableMobileSubscriberDirectoryNumberTable"
})
public class EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailableListResponse22
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable availableMobileSubscriberDirectoryNumberTable;

    /**
     * Ruft den Wert der availableMobileSubscriberDirectoryNumberTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAvailableMobileSubscriberDirectoryNumberTable() {
        return availableMobileSubscriberDirectoryNumberTable;
    }

    /**
     * Legt den Wert der availableMobileSubscriberDirectoryNumberTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAvailableMobileSubscriberDirectoryNumberTable(OCITable value) {
        this.availableMobileSubscriberDirectoryNumberTable = value;
    }

}
