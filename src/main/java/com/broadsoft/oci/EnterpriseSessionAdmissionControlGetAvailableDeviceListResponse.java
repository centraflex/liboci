//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to EnterpriseSessionAdmissionControlGetAvailableDeviceListRequest.
 *         Contains a table of devices can be assigned to session admission control group in the enterprise.
 *       
 * 
 * <p>Java-Klasse für EnterpriseSessionAdmissionControlGetAvailableDeviceListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseSessionAdmissionControlGetAvailableDeviceListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="accessDevice" type="{}EnterpriseAccessDevice" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseSessionAdmissionControlGetAvailableDeviceListResponse", propOrder = {
    "accessDevice"
})
public class EnterpriseSessionAdmissionControlGetAvailableDeviceListResponse
    extends OCIDataResponse
{

    protected List<EnterpriseAccessDevice> accessDevice;

    /**
     * Gets the value of the accessDevice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the accessDevice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessDevice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnterpriseAccessDevice }
     * 
     * 
     * @return
     *     The value of the accessDevice property.
     */
    public List<EnterpriseAccessDevice> getAccessDevice() {
        if (accessDevice == null) {
            accessDevice = new ArrayList<>();
        }
        return this.accessDevice;
    }

}
