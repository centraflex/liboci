//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to ServiceProviderExternalCustomRingbackGetRequest.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderExternalCustomRingbackGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderExternalCustomRingbackGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="prefixDigits" type="{}ExternalCustomRingbackPrefixDigits" minOccurs="0"/>
 *         <element name="serverNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="serverPort" type="{}Port1025" minOccurs="0"/>
 *         <element name="timeoutSeconds" type="{}ExternalCustomRingbackTimeoutSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderExternalCustomRingbackGetResponse", propOrder = {
    "prefixDigits",
    "serverNetAddress",
    "serverPort",
    "timeoutSeconds"
})
public class ServiceProviderExternalCustomRingbackGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String prefixDigits;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serverNetAddress;
    protected Integer serverPort;
    protected int timeoutSeconds;

    /**
     * Ruft den Wert der prefixDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrefixDigits() {
        return prefixDigits;
    }

    /**
     * Legt den Wert der prefixDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrefixDigits(String value) {
        this.prefixDigits = value;
    }

    /**
     * Ruft den Wert der serverNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServerNetAddress() {
        return serverNetAddress;
    }

    /**
     * Legt den Wert der serverNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServerNetAddress(String value) {
        this.serverNetAddress = value;
    }

    /**
     * Ruft den Wert der serverPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServerPort() {
        return serverPort;
    }

    /**
     * Legt den Wert der serverPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServerPort(Integer value) {
        this.serverPort = value;
    }

    /**
     * Ruft den Wert der timeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getTimeoutSeconds() {
        return timeoutSeconds;
    }

    /**
     * Legt den Wert der timeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setTimeoutSeconds(int value) {
        this.timeoutSeconds = value;
    }

}
