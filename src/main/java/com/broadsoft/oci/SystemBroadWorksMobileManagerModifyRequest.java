//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the BroadWorks Mobile Manager service system settings.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobileManagerModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobileManagerModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="scfApiNetAddress1" type="{}NetAddress" minOccurs="0"/>
 *         <element name="scfApiNetAddress2" type="{}NetAddress" minOccurs="0"/>
 *         <element name="userName" type="{}BroadWorksMobileManagerUserName" minOccurs="0"/>
 *         <element name="password" type="{}BroadWorksMobileManagerPassword" minOccurs="0"/>
 *         <element name="emailFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="scfIMSOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="signalingIPAddress" type="{}IPAddress" minOccurs="0"/>
 *         <element name="signalingPort" type="{}Port" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobileManagerModifyRequest", propOrder = {
    "scfApiNetAddress1",
    "scfApiNetAddress2",
    "userName",
    "password",
    "emailFromAddress",
    "scfIMSOnly",
    "signalingIPAddress",
    "signalingPort"
})
public class SystemBroadWorksMobileManagerModifyRequest
    extends OCIRequest
{

    @XmlElementRef(name = "scfApiNetAddress1", type = JAXBElement.class, required = false)
    protected JAXBElement<String> scfApiNetAddress1;
    @XmlElementRef(name = "scfApiNetAddress2", type = JAXBElement.class, required = false)
    protected JAXBElement<String> scfApiNetAddress2;
    @XmlElementRef(name = "userName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userName;
    @XmlElementRef(name = "password", type = JAXBElement.class, required = false)
    protected JAXBElement<String> password;
    @XmlElementRef(name = "emailFromAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emailFromAddress;
    protected Boolean scfIMSOnly;
    @XmlElementRef(name = "signalingIPAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> signalingIPAddress;
    @XmlElementRef(name = "signalingPort", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> signalingPort;

    /**
     * Ruft den Wert der scfApiNetAddress1-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getScfApiNetAddress1() {
        return scfApiNetAddress1;
    }

    /**
     * Legt den Wert der scfApiNetAddress1-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setScfApiNetAddress1(JAXBElement<String> value) {
        this.scfApiNetAddress1 = value;
    }

    /**
     * Ruft den Wert der scfApiNetAddress2-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getScfApiNetAddress2() {
        return scfApiNetAddress2;
    }

    /**
     * Legt den Wert der scfApiNetAddress2-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setScfApiNetAddress2(JAXBElement<String> value) {
        this.scfApiNetAddress2 = value;
    }

    /**
     * Ruft den Wert der userName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserName() {
        return userName;
    }

    /**
     * Legt den Wert der userName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserName(JAXBElement<String> value) {
        this.userName = value;
    }

    /**
     * Ruft den Wert der password-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPassword() {
        return password;
    }

    /**
     * Legt den Wert der password-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPassword(JAXBElement<String> value) {
        this.password = value;
    }

    /**
     * Ruft den Wert der emailFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmailFromAddress() {
        return emailFromAddress;
    }

    /**
     * Legt den Wert der emailFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmailFromAddress(JAXBElement<String> value) {
        this.emailFromAddress = value;
    }

    /**
     * Ruft den Wert der scfIMSOnly-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isScfIMSOnly() {
        return scfIMSOnly;
    }

    /**
     * Legt den Wert der scfIMSOnly-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setScfIMSOnly(Boolean value) {
        this.scfIMSOnly = value;
    }

    /**
     * Ruft den Wert der signalingIPAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSignalingIPAddress() {
        return signalingIPAddress;
    }

    /**
     * Legt den Wert der signalingIPAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSignalingIPAddress(JAXBElement<String> value) {
        this.signalingIPAddress = value;
    }

    /**
     * Ruft den Wert der signalingPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSignalingPort() {
        return signalingPort;
    }

    /**
     * Legt den Wert der signalingPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSignalingPort(JAXBElement<Integer> value) {
        this.signalingPort = value;
    }

}
