//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modifies the system's legacy automatic callback attributes.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemLegacyAutomaticCallbackModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemLegacyAutomaticCallbackModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="maxMonitorsPerOriginator" type="{}LegacyAutomaticCallbackMaxMonitorsPerOriginator" minOccurs="0"/>
 *         <element name="maxMonitorsPerTerminator" type="{}LegacyAutomaticCallbackMaxMonitorsPerTerminator" minOccurs="0"/>
 *         <element name="t2Minutes" type="{}LegacyAutomaticCallbackT2Minutes" minOccurs="0"/>
 *         <element name="t4Seconds" type="{}LegacyAutomaticCallbackT4Seconds" minOccurs="0"/>
 *         <element name="t5Seconds" type="{}LegacyAutomaticCallbackT5Seconds" minOccurs="0"/>
 *         <element name="t6Minutes" type="{}LegacyAutomaticCallbackT6Minutes" minOccurs="0"/>
 *         <element name="t7Minutes" type="{}LegacyAutomaticCallbackT7Minutes" minOccurs="0"/>
 *         <element name="t8Seconds" type="{}LegacyAutomaticCallbackT8Seconds" minOccurs="0"/>
 *         <element name="tRingSeconds" type="{}LegacyAutomaticCallbackTRingSeconds" minOccurs="0"/>
 *         <element name="t10OMinutes" type="{}LegacyAutomaticCallbackT10OMinutes" minOccurs="0"/>
 *         <element name="t10TMinutes" type="{}LegacyAutomaticCallbackT10TMinutes" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemLegacyAutomaticCallbackModifyRequest", propOrder = {
    "maxMonitorsPerOriginator",
    "maxMonitorsPerTerminator",
    "t2Minutes",
    "t4Seconds",
    "t5Seconds",
    "t6Minutes",
    "t7Minutes",
    "t8Seconds",
    "tRingSeconds",
    "t10OMinutes",
    "t10TMinutes"
})
public class SystemLegacyAutomaticCallbackModifyRequest
    extends OCIRequest
{

    protected Integer maxMonitorsPerOriginator;
    protected Integer maxMonitorsPerTerminator;
    protected Integer t2Minutes;
    protected Integer t4Seconds;
    protected Integer t5Seconds;
    protected Integer t6Minutes;
    protected Integer t7Minutes;
    protected Integer t8Seconds;
    protected Integer tRingSeconds;
    protected Integer t10OMinutes;
    protected Integer t10TMinutes;

    /**
     * Ruft den Wert der maxMonitorsPerOriginator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxMonitorsPerOriginator() {
        return maxMonitorsPerOriginator;
    }

    /**
     * Legt den Wert der maxMonitorsPerOriginator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxMonitorsPerOriginator(Integer value) {
        this.maxMonitorsPerOriginator = value;
    }

    /**
     * Ruft den Wert der maxMonitorsPerTerminator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxMonitorsPerTerminator() {
        return maxMonitorsPerTerminator;
    }

    /**
     * Legt den Wert der maxMonitorsPerTerminator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxMonitorsPerTerminator(Integer value) {
        this.maxMonitorsPerTerminator = value;
    }

    /**
     * Ruft den Wert der t2Minutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getT2Minutes() {
        return t2Minutes;
    }

    /**
     * Legt den Wert der t2Minutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setT2Minutes(Integer value) {
        this.t2Minutes = value;
    }

    /**
     * Ruft den Wert der t4Seconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getT4Seconds() {
        return t4Seconds;
    }

    /**
     * Legt den Wert der t4Seconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setT4Seconds(Integer value) {
        this.t4Seconds = value;
    }

    /**
     * Ruft den Wert der t5Seconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getT5Seconds() {
        return t5Seconds;
    }

    /**
     * Legt den Wert der t5Seconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setT5Seconds(Integer value) {
        this.t5Seconds = value;
    }

    /**
     * Ruft den Wert der t6Minutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getT6Minutes() {
        return t6Minutes;
    }

    /**
     * Legt den Wert der t6Minutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setT6Minutes(Integer value) {
        this.t6Minutes = value;
    }

    /**
     * Ruft den Wert der t7Minutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getT7Minutes() {
        return t7Minutes;
    }

    /**
     * Legt den Wert der t7Minutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setT7Minutes(Integer value) {
        this.t7Minutes = value;
    }

    /**
     * Ruft den Wert der t8Seconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getT8Seconds() {
        return t8Seconds;
    }

    /**
     * Legt den Wert der t8Seconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setT8Seconds(Integer value) {
        this.t8Seconds = value;
    }

    /**
     * Ruft den Wert der tRingSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTRingSeconds() {
        return tRingSeconds;
    }

    /**
     * Legt den Wert der tRingSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTRingSeconds(Integer value) {
        this.tRingSeconds = value;
    }

    /**
     * Ruft den Wert der t10OMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getT10OMinutes() {
        return t10OMinutes;
    }

    /**
     * Legt den Wert der t10OMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setT10OMinutes(Integer value) {
        this.t10OMinutes = value;
    }

    /**
     * Ruft den Wert der t10TMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getT10TMinutes() {
        return t10TMinutes;
    }

    /**
     * Legt den Wert der t10TMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setT10TMinutes(Integer value) {
        this.t10TMinutes = value;
    }

}
