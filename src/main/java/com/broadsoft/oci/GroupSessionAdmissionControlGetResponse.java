//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupSessionAdmissionControlGetRequest.
 *         The response contains the session admission control capacity allocated for the group.
 *       
 * 
 * <p>Java-Klasse für GroupSessionAdmissionControlGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupSessionAdmissionControlGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="restrictAggregateSessions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxSessions" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="maxUserOriginatingSessions" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="maxUserTerminatingSessions" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="countIntraGroupSessions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupSessionAdmissionControlGetResponse", propOrder = {
    "restrictAggregateSessions",
    "maxSessions",
    "maxUserOriginatingSessions",
    "maxUserTerminatingSessions",
    "countIntraGroupSessions"
})
public class GroupSessionAdmissionControlGetResponse
    extends OCIDataResponse
{

    protected boolean restrictAggregateSessions;
    protected Integer maxSessions;
    protected Integer maxUserOriginatingSessions;
    protected Integer maxUserTerminatingSessions;
    protected boolean countIntraGroupSessions;

    /**
     * Ruft den Wert der restrictAggregateSessions-Eigenschaft ab.
     * 
     */
    public boolean isRestrictAggregateSessions() {
        return restrictAggregateSessions;
    }

    /**
     * Legt den Wert der restrictAggregateSessions-Eigenschaft fest.
     * 
     */
    public void setRestrictAggregateSessions(boolean value) {
        this.restrictAggregateSessions = value;
    }

    /**
     * Ruft den Wert der maxSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxSessions() {
        return maxSessions;
    }

    /**
     * Legt den Wert der maxSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxSessions(Integer value) {
        this.maxSessions = value;
    }

    /**
     * Ruft den Wert der maxUserOriginatingSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxUserOriginatingSessions() {
        return maxUserOriginatingSessions;
    }

    /**
     * Legt den Wert der maxUserOriginatingSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxUserOriginatingSessions(Integer value) {
        this.maxUserOriginatingSessions = value;
    }

    /**
     * Ruft den Wert der maxUserTerminatingSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxUserTerminatingSessions() {
        return maxUserTerminatingSessions;
    }

    /**
     * Legt den Wert der maxUserTerminatingSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxUserTerminatingSessions(Integer value) {
        this.maxUserTerminatingSessions = value;
    }

    /**
     * Ruft den Wert der countIntraGroupSessions-Eigenschaft ab.
     * 
     */
    public boolean isCountIntraGroupSessions() {
        return countIntraGroupSessions;
    }

    /**
     * Legt den Wert der countIntraGroupSessions-Eigenschaft fest.
     * 
     */
    public void setCountIntraGroupSessions(boolean value) {
        this.countIntraGroupSessions = value;
    }

}
