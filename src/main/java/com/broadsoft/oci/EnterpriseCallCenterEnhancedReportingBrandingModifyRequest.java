//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the enterprise branding configuration.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für EnterpriseCallCenterEnhancedReportingBrandingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseCallCenterEnhancedReportingBrandingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="brandingChoice" type="{}CallCenterEnhancedReportingBrandingChoice" minOccurs="0"/>
 *         <element name="brandingFile" type="{}LabeledFileResource" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseCallCenterEnhancedReportingBrandingModifyRequest", propOrder = {
    "serviceProviderId",
    "brandingChoice",
    "brandingFile"
})
public class EnterpriseCallCenterEnhancedReportingBrandingModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlSchemaType(name = "token")
    protected CallCenterEnhancedReportingBrandingChoice brandingChoice;
    protected LabeledFileResource brandingFile;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der brandingChoice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterEnhancedReportingBrandingChoice }
     *     
     */
    public CallCenterEnhancedReportingBrandingChoice getBrandingChoice() {
        return brandingChoice;
    }

    /**
     * Legt den Wert der brandingChoice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterEnhancedReportingBrandingChoice }
     *     
     */
    public void setBrandingChoice(CallCenterEnhancedReportingBrandingChoice value) {
        this.brandingChoice = value;
    }

    /**
     * Ruft den Wert der brandingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledFileResource }
     *     
     */
    public LabeledFileResource getBrandingFile() {
        return brandingFile;
    }

    /**
     * Legt den Wert der brandingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledFileResource }
     *     
     */
    public void setBrandingFile(LabeledFileResource value) {
        this.brandingFile = value;
    }

}
