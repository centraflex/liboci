//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EnterpriseVoiceVPNDigitManipulationOperationNoValue.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="EnterpriseVoiceVPNDigitManipulationOperationNoValue">
 *   <restriction base="{}EnterpriseVoiceVPNDigitManipulationOperation">
 *     <enumeration value="End"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "EnterpriseVoiceVPNDigitManipulationOperationNoValue")
@XmlEnum(EnterpriseVoiceVPNDigitManipulationOperation.class)
public enum EnterpriseVoiceVPNDigitManipulationOperationNoValue {

    @XmlEnumValue("End")
    END(EnterpriseVoiceVPNDigitManipulationOperation.END);
    private final EnterpriseVoiceVPNDigitManipulationOperation value;

    EnterpriseVoiceVPNDigitManipulationOperationNoValue(EnterpriseVoiceVPNDigitManipulationOperation v) {
        value = v;
    }

    public EnterpriseVoiceVPNDigitManipulationOperation value() {
        return value;
    }

    public static EnterpriseVoiceVPNDigitManipulationOperationNoValue fromValue(EnterpriseVoiceVPNDigitManipulationOperation v) {
        for (EnterpriseVoiceVPNDigitManipulationOperationNoValue c: EnterpriseVoiceVPNDigitManipulationOperationNoValue.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
