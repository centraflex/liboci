//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the agent list for a call center.
 *         The response is either SuccessResponse or ErrorResponse.
 *         If the agentUserIdList is used for Skill Based Premium call centers, 
 *         the agents will be set to skill level 1.
 *         
 *         The following element is only used in AS data mode and ignored in XS data mode:
 *           skilledAgentUserIdList
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterModifyAgentListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterModifyAgentListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <choice>
 *           <element name="agentUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *           <element name="skilledAgentUserIdList" type="{}CallCenterReplacementSkilledAgents" maxOccurs="unbounded" minOccurs="0"/>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterModifyAgentListRequest", propOrder = {
    "serviceUserId",
    "agentUserIdList",
    "skilledAgentUserIdList"
})
public class GroupCallCenterModifyAgentListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlElementRef(name = "agentUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> agentUserIdList;
    protected List<CallCenterReplacementSkilledAgents> skilledAgentUserIdList;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der agentUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getAgentUserIdList() {
        return agentUserIdList;
    }

    /**
     * Legt den Wert der agentUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setAgentUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.agentUserIdList = value;
    }

    /**
     * Gets the value of the skilledAgentUserIdList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the skilledAgentUserIdList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkilledAgentUserIdList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallCenterReplacementSkilledAgents }
     * 
     * 
     * @return
     *     The value of the skilledAgentUserIdList property.
     */
    public List<CallCenterReplacementSkilledAgents> getSkilledAgentUserIdList() {
        if (skilledAgentUserIdList == null) {
            skilledAgentUserIdList = new ArrayList<>();
        }
        return this.skilledAgentUserIdList;
    }

}
