//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemBwDiameterBaseDataGetRequest.
 *         Contains a list of System Diameter base parameters.
 *       
 * 
 * <p>Java-Klasse für SystemBwDiameterBaseDataGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBwDiameterBaseDataGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="xsRealm" type="{}DomainName" minOccurs="0"/>
 *         <element name="xsListeningPort" type="{}Port1025"/>
 *         <element name="psRealm" type="{}DomainName" minOccurs="0"/>
 *         <element name="psListeningPort" type="{}Port1025"/>
 *         <element name="psRelayThroughXs" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="xsRelayListeningPort" type="{}Port1025"/>
 *         <element name="tcTimerSeconds" type="{}BwDiameterTcTimerSeconds"/>
 *         <element name="twTimerSeconds" type="{}BwDiameterTwTimerSeconds"/>
 *         <element name="requestTimerSeconds" type="{}BwDiameterRequestTimerSeconds"/>
 *         <element name="busyPeerDetectionOutstandingTxnCount" type="{}BwDiameterBusyPeerOutstandingTxnCount"/>
 *         <element name="busyPeerRestoreOutstandingTxnCount" type="{}BwDiameterBusyPeerOutstandingTxnCount"/>
 *         <element name="dynamicEntryInactivityTimerHours" type="{}BwDiameterDynamicEntryInactivityTimerHours"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBwDiameterBaseDataGetResponse", propOrder = {
    "xsRealm",
    "xsListeningPort",
    "psRealm",
    "psListeningPort",
    "psRelayThroughXs",
    "xsRelayListeningPort",
    "tcTimerSeconds",
    "twTimerSeconds",
    "requestTimerSeconds",
    "busyPeerDetectionOutstandingTxnCount",
    "busyPeerRestoreOutstandingTxnCount",
    "dynamicEntryInactivityTimerHours"
})
public class SystemBwDiameterBaseDataGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String xsRealm;
    protected int xsListeningPort;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String psRealm;
    protected int psListeningPort;
    protected boolean psRelayThroughXs;
    protected int xsRelayListeningPort;
    protected int tcTimerSeconds;
    protected int twTimerSeconds;
    protected int requestTimerSeconds;
    protected int busyPeerDetectionOutstandingTxnCount;
    protected int busyPeerRestoreOutstandingTxnCount;
    protected int dynamicEntryInactivityTimerHours;

    /**
     * Ruft den Wert der xsRealm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXsRealm() {
        return xsRealm;
    }

    /**
     * Legt den Wert der xsRealm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXsRealm(String value) {
        this.xsRealm = value;
    }

    /**
     * Ruft den Wert der xsListeningPort-Eigenschaft ab.
     * 
     */
    public int getXsListeningPort() {
        return xsListeningPort;
    }

    /**
     * Legt den Wert der xsListeningPort-Eigenschaft fest.
     * 
     */
    public void setXsListeningPort(int value) {
        this.xsListeningPort = value;
    }

    /**
     * Ruft den Wert der psRealm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPsRealm() {
        return psRealm;
    }

    /**
     * Legt den Wert der psRealm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPsRealm(String value) {
        this.psRealm = value;
    }

    /**
     * Ruft den Wert der psListeningPort-Eigenschaft ab.
     * 
     */
    public int getPsListeningPort() {
        return psListeningPort;
    }

    /**
     * Legt den Wert der psListeningPort-Eigenschaft fest.
     * 
     */
    public void setPsListeningPort(int value) {
        this.psListeningPort = value;
    }

    /**
     * Ruft den Wert der psRelayThroughXs-Eigenschaft ab.
     * 
     */
    public boolean isPsRelayThroughXs() {
        return psRelayThroughXs;
    }

    /**
     * Legt den Wert der psRelayThroughXs-Eigenschaft fest.
     * 
     */
    public void setPsRelayThroughXs(boolean value) {
        this.psRelayThroughXs = value;
    }

    /**
     * Ruft den Wert der xsRelayListeningPort-Eigenschaft ab.
     * 
     */
    public int getXsRelayListeningPort() {
        return xsRelayListeningPort;
    }

    /**
     * Legt den Wert der xsRelayListeningPort-Eigenschaft fest.
     * 
     */
    public void setXsRelayListeningPort(int value) {
        this.xsRelayListeningPort = value;
    }

    /**
     * Ruft den Wert der tcTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getTcTimerSeconds() {
        return tcTimerSeconds;
    }

    /**
     * Legt den Wert der tcTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setTcTimerSeconds(int value) {
        this.tcTimerSeconds = value;
    }

    /**
     * Ruft den Wert der twTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getTwTimerSeconds() {
        return twTimerSeconds;
    }

    /**
     * Legt den Wert der twTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setTwTimerSeconds(int value) {
        this.twTimerSeconds = value;
    }

    /**
     * Ruft den Wert der requestTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getRequestTimerSeconds() {
        return requestTimerSeconds;
    }

    /**
     * Legt den Wert der requestTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setRequestTimerSeconds(int value) {
        this.requestTimerSeconds = value;
    }

    /**
     * Ruft den Wert der busyPeerDetectionOutstandingTxnCount-Eigenschaft ab.
     * 
     */
    public int getBusyPeerDetectionOutstandingTxnCount() {
        return busyPeerDetectionOutstandingTxnCount;
    }

    /**
     * Legt den Wert der busyPeerDetectionOutstandingTxnCount-Eigenschaft fest.
     * 
     */
    public void setBusyPeerDetectionOutstandingTxnCount(int value) {
        this.busyPeerDetectionOutstandingTxnCount = value;
    }

    /**
     * Ruft den Wert der busyPeerRestoreOutstandingTxnCount-Eigenschaft ab.
     * 
     */
    public int getBusyPeerRestoreOutstandingTxnCount() {
        return busyPeerRestoreOutstandingTxnCount;
    }

    /**
     * Legt den Wert der busyPeerRestoreOutstandingTxnCount-Eigenschaft fest.
     * 
     */
    public void setBusyPeerRestoreOutstandingTxnCount(int value) {
        this.busyPeerRestoreOutstandingTxnCount = value;
    }

    /**
     * Ruft den Wert der dynamicEntryInactivityTimerHours-Eigenschaft ab.
     * 
     */
    public int getDynamicEntryInactivityTimerHours() {
        return dynamicEntryInactivityTimerHours;
    }

    /**
     * Legt den Wert der dynamicEntryInactivityTimerHours-Eigenschaft fest.
     * 
     */
    public void setDynamicEntryInactivityTimerHours(int value) {
        this.dynamicEntryInactivityTimerHours = value;
    }

}
