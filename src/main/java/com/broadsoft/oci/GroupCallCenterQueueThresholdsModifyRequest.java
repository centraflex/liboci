//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a call center's queue Thresholds settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterQueueThresholdsModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterQueueThresholdsModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="thresholdCurrentCallsInQueueYellow" type="{}CallCenterQueueThresholdCurrentCallsInQueue" minOccurs="0"/>
 *         <element name="thresholdCurrentCallsInQueueRed" type="{}CallCenterQueueThresholdCurrentCallsInQueue" minOccurs="0"/>
 *         <element name="thresholdCurrentLongestWaitingCallYellow" type="{}CallCenterQueueThresholdCurrentLongestWaitingCallsTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdCurrentLongestWaitingCallRed" type="{}CallCenterQueueThresholdCurrentLongestWaitingCallsTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageEstimatedWaitTimeYellow" type="{}CallCenterQueueThresholdAverageEstimatedWaitTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageEstimatedWaitTimeRed" type="{}CallCenterQueueThresholdAverageEstimatedWaitTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageHandlingTimeYellow" type="{}CallCenterQueueThresholdAverageHandlingTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageHandlingTimeRed" type="{}CallCenterQueueThresholdAverageHandlingTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageSpeedOfAnswerYellow" type="{}CallCenterQueueThresholdAverageSpeedOfAnswerTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageSpeedOfAnswerRed" type="{}CallCenterQueueThresholdAverageSpeedOfAnswerTimeSeconds" minOccurs="0"/>
 *         <element name="enableNotificationEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="notificationEmailAddressList" type="{}CallCenterQueueThresholdReplacementNotificationEmailList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterQueueThresholdsModifyRequest", propOrder = {
    "serviceUserId",
    "isActive",
    "thresholdCurrentCallsInQueueYellow",
    "thresholdCurrentCallsInQueueRed",
    "thresholdCurrentLongestWaitingCallYellow",
    "thresholdCurrentLongestWaitingCallRed",
    "thresholdAverageEstimatedWaitTimeYellow",
    "thresholdAverageEstimatedWaitTimeRed",
    "thresholdAverageHandlingTimeYellow",
    "thresholdAverageHandlingTimeRed",
    "thresholdAverageSpeedOfAnswerYellow",
    "thresholdAverageSpeedOfAnswerRed",
    "enableNotificationEmail",
    "notificationEmailAddressList"
})
public class GroupCallCenterQueueThresholdsModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected Boolean isActive;
    @XmlElementRef(name = "thresholdCurrentCallsInQueueYellow", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdCurrentCallsInQueueYellow;
    @XmlElementRef(name = "thresholdCurrentCallsInQueueRed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdCurrentCallsInQueueRed;
    @XmlElementRef(name = "thresholdCurrentLongestWaitingCallYellow", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdCurrentLongestWaitingCallYellow;
    @XmlElementRef(name = "thresholdCurrentLongestWaitingCallRed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdCurrentLongestWaitingCallRed;
    @XmlElementRef(name = "thresholdAverageEstimatedWaitTimeYellow", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdAverageEstimatedWaitTimeYellow;
    @XmlElementRef(name = "thresholdAverageEstimatedWaitTimeRed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdAverageEstimatedWaitTimeRed;
    @XmlElementRef(name = "thresholdAverageHandlingTimeYellow", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdAverageHandlingTimeYellow;
    @XmlElementRef(name = "thresholdAverageHandlingTimeRed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdAverageHandlingTimeRed;
    @XmlElementRef(name = "thresholdAverageSpeedOfAnswerYellow", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdAverageSpeedOfAnswerYellow;
    @XmlElementRef(name = "thresholdAverageSpeedOfAnswerRed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> thresholdAverageSpeedOfAnswerRed;
    protected Boolean enableNotificationEmail;
    @XmlElementRef(name = "notificationEmailAddressList", type = JAXBElement.class, required = false)
    protected JAXBElement<CallCenterQueueThresholdReplacementNotificationEmailList> notificationEmailAddressList;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentCallsInQueueYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdCurrentCallsInQueueYellow() {
        return thresholdCurrentCallsInQueueYellow;
    }

    /**
     * Legt den Wert der thresholdCurrentCallsInQueueYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdCurrentCallsInQueueYellow(JAXBElement<Integer> value) {
        this.thresholdCurrentCallsInQueueYellow = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentCallsInQueueRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdCurrentCallsInQueueRed() {
        return thresholdCurrentCallsInQueueRed;
    }

    /**
     * Legt den Wert der thresholdCurrentCallsInQueueRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdCurrentCallsInQueueRed(JAXBElement<Integer> value) {
        this.thresholdCurrentCallsInQueueRed = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentLongestWaitingCallYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdCurrentLongestWaitingCallYellow() {
        return thresholdCurrentLongestWaitingCallYellow;
    }

    /**
     * Legt den Wert der thresholdCurrentLongestWaitingCallYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdCurrentLongestWaitingCallYellow(JAXBElement<Integer> value) {
        this.thresholdCurrentLongestWaitingCallYellow = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentLongestWaitingCallRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdCurrentLongestWaitingCallRed() {
        return thresholdCurrentLongestWaitingCallRed;
    }

    /**
     * Legt den Wert der thresholdCurrentLongestWaitingCallRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdCurrentLongestWaitingCallRed(JAXBElement<Integer> value) {
        this.thresholdCurrentLongestWaitingCallRed = value;
    }

    /**
     * Ruft den Wert der thresholdAverageEstimatedWaitTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdAverageEstimatedWaitTimeYellow() {
        return thresholdAverageEstimatedWaitTimeYellow;
    }

    /**
     * Legt den Wert der thresholdAverageEstimatedWaitTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdAverageEstimatedWaitTimeYellow(JAXBElement<Integer> value) {
        this.thresholdAverageEstimatedWaitTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdAverageEstimatedWaitTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdAverageEstimatedWaitTimeRed() {
        return thresholdAverageEstimatedWaitTimeRed;
    }

    /**
     * Legt den Wert der thresholdAverageEstimatedWaitTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdAverageEstimatedWaitTimeRed(JAXBElement<Integer> value) {
        this.thresholdAverageEstimatedWaitTimeRed = value;
    }

    /**
     * Ruft den Wert der thresholdAverageHandlingTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdAverageHandlingTimeYellow() {
        return thresholdAverageHandlingTimeYellow;
    }

    /**
     * Legt den Wert der thresholdAverageHandlingTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdAverageHandlingTimeYellow(JAXBElement<Integer> value) {
        this.thresholdAverageHandlingTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdAverageHandlingTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdAverageHandlingTimeRed() {
        return thresholdAverageHandlingTimeRed;
    }

    /**
     * Legt den Wert der thresholdAverageHandlingTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdAverageHandlingTimeRed(JAXBElement<Integer> value) {
        this.thresholdAverageHandlingTimeRed = value;
    }

    /**
     * Ruft den Wert der thresholdAverageSpeedOfAnswerYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdAverageSpeedOfAnswerYellow() {
        return thresholdAverageSpeedOfAnswerYellow;
    }

    /**
     * Legt den Wert der thresholdAverageSpeedOfAnswerYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdAverageSpeedOfAnswerYellow(JAXBElement<Integer> value) {
        this.thresholdAverageSpeedOfAnswerYellow = value;
    }

    /**
     * Ruft den Wert der thresholdAverageSpeedOfAnswerRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getThresholdAverageSpeedOfAnswerRed() {
        return thresholdAverageSpeedOfAnswerRed;
    }

    /**
     * Legt den Wert der thresholdAverageSpeedOfAnswerRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setThresholdAverageSpeedOfAnswerRed(JAXBElement<Integer> value) {
        this.thresholdAverageSpeedOfAnswerRed = value;
    }

    /**
     * Ruft den Wert der enableNotificationEmail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableNotificationEmail() {
        return enableNotificationEmail;
    }

    /**
     * Legt den Wert der enableNotificationEmail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableNotificationEmail(Boolean value) {
        this.enableNotificationEmail = value;
    }

    /**
     * Ruft den Wert der notificationEmailAddressList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CallCenterQueueThresholdReplacementNotificationEmailList }{@code >}
     *     
     */
    public JAXBElement<CallCenterQueueThresholdReplacementNotificationEmailList> getNotificationEmailAddressList() {
        return notificationEmailAddressList;
    }

    /**
     * Legt den Wert der notificationEmailAddressList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CallCenterQueueThresholdReplacementNotificationEmailList }{@code >}
     *     
     */
    public void setNotificationEmailAddressList(JAXBElement<CallCenterQueueThresholdReplacementNotificationEmailList> value) {
        this.notificationEmailAddressList = value;
    }

}
