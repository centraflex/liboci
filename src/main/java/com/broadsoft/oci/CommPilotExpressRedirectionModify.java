//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         CommPilot Express type to transfer to voice Mail or forward to a number
 *         used in the context of a modify.
 *       
 * 
 * <p>Java-Klasse für CommPilotExpressRedirectionModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CommPilotExpressRedirectionModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="action" type="{}CommPilotExpressRedirectionAction" minOccurs="0"/>
 *         <element name="forwardingPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommPilotExpressRedirectionModify", propOrder = {
    "action",
    "forwardingPhoneNumber"
})
public class CommPilotExpressRedirectionModify {

    @XmlSchemaType(name = "token")
    protected CommPilotExpressRedirectionAction action;
    @XmlElementRef(name = "forwardingPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forwardingPhoneNumber;

    /**
     * Ruft den Wert der action-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressRedirectionAction }
     *     
     */
    public CommPilotExpressRedirectionAction getAction() {
        return action;
    }

    /**
     * Legt den Wert der action-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressRedirectionAction }
     *     
     */
    public void setAction(CommPilotExpressRedirectionAction value) {
        this.action = value;
    }

    /**
     * Ruft den Wert der forwardingPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForwardingPhoneNumber() {
        return forwardingPhoneNumber;
    }

    /**
     * Legt den Wert der forwardingPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForwardingPhoneNumber(JAXBElement<String> value) {
        this.forwardingPhoneNumber = value;
    }

}
