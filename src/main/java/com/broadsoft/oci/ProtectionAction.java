//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ProtectionAction.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ProtectionAction">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Decline"/>
 *     <enumeration value="Drop"/>
 *     <enumeration value="Error"/>
 *     <enumeration value="Redirect"/>
 *     <enumeration value="Unavailable"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ProtectionAction")
@XmlEnum
public enum ProtectionAction {

    @XmlEnumValue("Decline")
    DECLINE("Decline"),
    @XmlEnumValue("Drop")
    DROP("Drop"),
    @XmlEnumValue("Error")
    ERROR("Error"),
    @XmlEnumValue("Redirect")
    REDIRECT("Redirect"),
    @XmlEnumValue("Unavailable")
    UNAVAILABLE("Unavailable");
    private final String value;

    ProtectionAction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ProtectionAction fromValue(String v) {
        for (ProtectionAction c: ProtectionAction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
