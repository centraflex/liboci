//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *           Request to modify a sip device type file.
 *           The response is either SuccessResponse or ErrorResponse.
 * 
 *           The following logic applies to these elements:
 *             macInCert
 *             macInNonRequestURI
 *             The two elements are mutually exclusive.
 *             When both are set to true, the command fails.
 *             When macInCert is set to true, macInNonRequestURI will be reset to false.
 *             When macInNonRequestURI is set to true, macInCert will be reset to false.
 *             
 *         
 * 
 * <p>Java-Klasse für SystemSIPDeviceTypeFileModifyRequest16sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPDeviceTypeFileModifyRequest16sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="deviceType" type="{}AccessDeviceType"/>
 *         <element name="fileFormat" type="{}DeviceManagementFileFormat"/>
 *         <element name="fileCustomization" type="{}DeviceManagementFileCustomization" minOccurs="0"/>
 *         <element name="fileSource" type="{}DeviceTypeFileEnhancedConfigurationMode" minOccurs="0"/>
 *         <element name="uploadFile" type="{}FileResource" minOccurs="0"/>
 *         <element name="useHttpDigestAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="macBasedFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="userNamePasswordFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="macInNonRequestURI" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="macInCert" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="macFormatInNonRequestURI" type="{}DeviceManagementAccessURI" minOccurs="0"/>
 *         <element name="allowHttp" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowHttps" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowTftp" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableCaching" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowUploadFromDevice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="defaultExtendedFileCaptureMode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPDeviceTypeFileModifyRequest16sp1", propOrder = {
    "deviceType",
    "fileFormat",
    "fileCustomization",
    "fileSource",
    "uploadFile",
    "useHttpDigestAuthentication",
    "macBasedFileAuthentication",
    "userNamePasswordFileAuthentication",
    "macInNonRequestURI",
    "macInCert",
    "macFormatInNonRequestURI",
    "allowHttp",
    "allowHttps",
    "allowTftp",
    "enableCaching",
    "allowUploadFromDevice",
    "defaultExtendedFileCaptureMode"
})
public class SystemSIPDeviceTypeFileModifyRequest16Sp1
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceType;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String fileFormat;
    @XmlSchemaType(name = "token")
    protected DeviceManagementFileCustomization fileCustomization;
    @XmlSchemaType(name = "token")
    protected DeviceTypeFileEnhancedConfigurationMode fileSource;
    protected FileResource uploadFile;
    protected Boolean useHttpDigestAuthentication;
    protected Boolean macBasedFileAuthentication;
    protected Boolean userNamePasswordFileAuthentication;
    protected Boolean macInNonRequestURI;
    protected Boolean macInCert;
    @XmlElementRef(name = "macFormatInNonRequestURI", type = JAXBElement.class, required = false)
    protected JAXBElement<String> macFormatInNonRequestURI;
    protected Boolean allowHttp;
    protected Boolean allowHttps;
    protected Boolean allowTftp;
    protected Boolean enableCaching;
    protected Boolean allowUploadFromDevice;
    protected Boolean defaultExtendedFileCaptureMode;

    /**
     * Ruft den Wert der deviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Legt den Wert der deviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceType(String value) {
        this.deviceType = value;
    }

    /**
     * Ruft den Wert der fileFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileFormat() {
        return fileFormat;
    }

    /**
     * Legt den Wert der fileFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileFormat(String value) {
        this.fileFormat = value;
    }

    /**
     * Ruft den Wert der fileCustomization-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementFileCustomization }
     *     
     */
    public DeviceManagementFileCustomization getFileCustomization() {
        return fileCustomization;
    }

    /**
     * Legt den Wert der fileCustomization-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementFileCustomization }
     *     
     */
    public void setFileCustomization(DeviceManagementFileCustomization value) {
        this.fileCustomization = value;
    }

    /**
     * Ruft den Wert der fileSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceTypeFileEnhancedConfigurationMode }
     *     
     */
    public DeviceTypeFileEnhancedConfigurationMode getFileSource() {
        return fileSource;
    }

    /**
     * Legt den Wert der fileSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceTypeFileEnhancedConfigurationMode }
     *     
     */
    public void setFileSource(DeviceTypeFileEnhancedConfigurationMode value) {
        this.fileSource = value;
    }

    /**
     * Ruft den Wert der uploadFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FileResource }
     *     
     */
    public FileResource getUploadFile() {
        return uploadFile;
    }

    /**
     * Legt den Wert der uploadFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FileResource }
     *     
     */
    public void setUploadFile(FileResource value) {
        this.uploadFile = value;
    }

    /**
     * Ruft den Wert der useHttpDigestAuthentication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseHttpDigestAuthentication() {
        return useHttpDigestAuthentication;
    }

    /**
     * Legt den Wert der useHttpDigestAuthentication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseHttpDigestAuthentication(Boolean value) {
        this.useHttpDigestAuthentication = value;
    }

    /**
     * Ruft den Wert der macBasedFileAuthentication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMacBasedFileAuthentication() {
        return macBasedFileAuthentication;
    }

    /**
     * Legt den Wert der macBasedFileAuthentication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMacBasedFileAuthentication(Boolean value) {
        this.macBasedFileAuthentication = value;
    }

    /**
     * Ruft den Wert der userNamePasswordFileAuthentication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUserNamePasswordFileAuthentication() {
        return userNamePasswordFileAuthentication;
    }

    /**
     * Legt den Wert der userNamePasswordFileAuthentication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUserNamePasswordFileAuthentication(Boolean value) {
        this.userNamePasswordFileAuthentication = value;
    }

    /**
     * Ruft den Wert der macInNonRequestURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMacInNonRequestURI() {
        return macInNonRequestURI;
    }

    /**
     * Legt den Wert der macInNonRequestURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMacInNonRequestURI(Boolean value) {
        this.macInNonRequestURI = value;
    }

    /**
     * Ruft den Wert der macInCert-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMacInCert() {
        return macInCert;
    }

    /**
     * Legt den Wert der macInCert-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMacInCert(Boolean value) {
        this.macInCert = value;
    }

    /**
     * Ruft den Wert der macFormatInNonRequestURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMacFormatInNonRequestURI() {
        return macFormatInNonRequestURI;
    }

    /**
     * Legt den Wert der macFormatInNonRequestURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMacFormatInNonRequestURI(JAXBElement<String> value) {
        this.macFormatInNonRequestURI = value;
    }

    /**
     * Ruft den Wert der allowHttp-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowHttp() {
        return allowHttp;
    }

    /**
     * Legt den Wert der allowHttp-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowHttp(Boolean value) {
        this.allowHttp = value;
    }

    /**
     * Ruft den Wert der allowHttps-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowHttps() {
        return allowHttps;
    }

    /**
     * Legt den Wert der allowHttps-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowHttps(Boolean value) {
        this.allowHttps = value;
    }

    /**
     * Ruft den Wert der allowTftp-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowTftp() {
        return allowTftp;
    }

    /**
     * Legt den Wert der allowTftp-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowTftp(Boolean value) {
        this.allowTftp = value;
    }

    /**
     * Ruft den Wert der enableCaching-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCaching() {
        return enableCaching;
    }

    /**
     * Legt den Wert der enableCaching-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCaching(Boolean value) {
        this.enableCaching = value;
    }

    /**
     * Ruft den Wert der allowUploadFromDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowUploadFromDevice() {
        return allowUploadFromDevice;
    }

    /**
     * Legt den Wert der allowUploadFromDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowUploadFromDevice(Boolean value) {
        this.allowUploadFromDevice = value;
    }

    /**
     * Ruft den Wert der defaultExtendedFileCaptureMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefaultExtendedFileCaptureMode() {
        return defaultExtendedFileCaptureMode;
    }

    /**
     * Legt den Wert der defaultExtendedFileCaptureMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultExtendedFileCaptureMode(Boolean value) {
        this.defaultExtendedFileCaptureMode = value;
    }

}
