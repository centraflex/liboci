//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the Device Activation policy of the system.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemDeviceActivationPolicyModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceActivationPolicyModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="allowActivationCodeRequestByUser" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="sendActivationCodeInEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceActivationPolicyModifyRequest", propOrder = {
    "allowActivationCodeRequestByUser",
    "sendActivationCodeInEmail"
})
public class SystemDeviceActivationPolicyModifyRequest
    extends OCIRequest
{

    protected Boolean allowActivationCodeRequestByUser;
    protected Boolean sendActivationCodeInEmail;

    /**
     * Ruft den Wert der allowActivationCodeRequestByUser-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowActivationCodeRequestByUser() {
        return allowActivationCodeRequestByUser;
    }

    /**
     * Legt den Wert der allowActivationCodeRequestByUser-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowActivationCodeRequestByUser(Boolean value) {
        this.allowActivationCodeRequestByUser = value;
    }

    /**
     * Ruft den Wert der sendActivationCodeInEmail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendActivationCodeInEmail() {
        return sendActivationCodeInEmail;
    }

    /**
     * Legt den Wert der sendActivationCodeInEmail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendActivationCodeInEmail(Boolean value) {
        this.sendActivationCodeInEmail = value;
    }

}
