//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ServiceProviderConferenceURISettingLevel.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ServiceProviderConferenceURISettingLevel">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="System"/>
 *     <enumeration value="Service Provider"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ServiceProviderConferenceURISettingLevel")
@XmlEnum
public enum ServiceProviderConferenceURISettingLevel {

    @XmlEnumValue("System")
    SYSTEM("System"),
    @XmlEnumValue("Service Provider")
    SERVICE_PROVIDER("Service Provider");
    private final String value;

    ServiceProviderConferenceURISettingLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ServiceProviderConferenceURISettingLevel fromValue(String v) {
        for (ServiceProviderConferenceURISettingLevel c: ServiceProviderConferenceURISettingLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
