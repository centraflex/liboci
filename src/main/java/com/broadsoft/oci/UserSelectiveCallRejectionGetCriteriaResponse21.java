//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserSelectiveCallRejectionGetCriteriaRequest21.
 *         Private Phone Numbers are omitted from the fromDnCriteria.
 *       
 * 
 * <p>Java-Klasse für UserSelectiveCallRejectionGetCriteriaResponse21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserSelectiveCallRejectionGetCriteriaResponse21">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="timeSchedule" type="{}TimeSchedule" minOccurs="0"/>
 *         <element name="fromDnCriteria" type="{}SelectiveCallRejectionCriteriaCallType"/>
 *         <element name="blacklisted" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="holidaySchedule" type="{}HolidaySchedule" minOccurs="0"/>
 *         <element name="private" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="callToNumber" type="{}CallToNumber" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserSelectiveCallRejectionGetCriteriaResponse21", propOrder = {
    "timeSchedule",
    "fromDnCriteria",
    "blacklisted",
    "holidaySchedule",
    "_private",
    "callToNumber"
})
public class UserSelectiveCallRejectionGetCriteriaResponse21
    extends OCIDataResponse
{

    protected TimeSchedule timeSchedule;
    @XmlElement(required = true)
    protected SelectiveCallRejectionCriteriaCallType fromDnCriteria;
    protected boolean blacklisted;
    protected HolidaySchedule holidaySchedule;
    @XmlElement(name = "private")
    protected boolean _private;
    protected List<CallToNumber> callToNumber;

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeSchedule }
     *     
     */
    public TimeSchedule getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeSchedule }
     *     
     */
    public void setTimeSchedule(TimeSchedule value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der fromDnCriteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SelectiveCallRejectionCriteriaCallType }
     *     
     */
    public SelectiveCallRejectionCriteriaCallType getFromDnCriteria() {
        return fromDnCriteria;
    }

    /**
     * Legt den Wert der fromDnCriteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SelectiveCallRejectionCriteriaCallType }
     *     
     */
    public void setFromDnCriteria(SelectiveCallRejectionCriteriaCallType value) {
        this.fromDnCriteria = value;
    }

    /**
     * Ruft den Wert der blacklisted-Eigenschaft ab.
     * 
     */
    public boolean isBlacklisted() {
        return blacklisted;
    }

    /**
     * Legt den Wert der blacklisted-Eigenschaft fest.
     * 
     */
    public void setBlacklisted(boolean value) {
        this.blacklisted = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HolidaySchedule }
     *     
     */
    public HolidaySchedule getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HolidaySchedule }
     *     
     */
    public void setHolidaySchedule(HolidaySchedule value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der private-Eigenschaft ab.
     * 
     */
    public boolean isPrivate() {
        return _private;
    }

    /**
     * Legt den Wert der private-Eigenschaft fest.
     * 
     */
    public void setPrivate(boolean value) {
        this._private = value;
    }

    /**
     * Gets the value of the callToNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the callToNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCallToNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallToNumber }
     * 
     * 
     * @return
     *     The value of the callToNumber property.
     */
    public List<CallToNumber> getCallToNumber() {
        if (callToNumber == null) {
            callToNumber = new ArrayList<>();
        }
        return this.callToNumber;
    }

}
