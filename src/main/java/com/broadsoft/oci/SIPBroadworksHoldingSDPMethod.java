//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SIPBroadworksHoldingSDPMethod.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SIPBroadworksHoldingSDPMethod">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Hold SDP"/>
 *     <enumeration value="Modified Address SDP"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SIPBroadworksHoldingSDPMethod")
@XmlEnum
public enum SIPBroadworksHoldingSDPMethod {

    @XmlEnumValue("Hold SDP")
    HOLD_SDP("Hold SDP"),
    @XmlEnumValue("Modified Address SDP")
    MODIFIED_ADDRESS_SDP("Modified Address SDP");
    private final String value;

    SIPBroadworksHoldingSDPMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SIPBroadworksHoldingSDPMethod fromValue(String v) {
        for (SIPBroadworksHoldingSDPMethod c: SIPBroadworksHoldingSDPMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
