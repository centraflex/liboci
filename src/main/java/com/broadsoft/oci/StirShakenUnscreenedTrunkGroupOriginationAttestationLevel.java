//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für StirShakenUnscreenedTrunkGroupOriginationAttestationLevel.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="StirShakenUnscreenedTrunkGroupOriginationAttestationLevel">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Full"/>
 *     <enumeration value="Partial"/>
 *     <enumeration value="Gateway"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "StirShakenUnscreenedTrunkGroupOriginationAttestationLevel")
@XmlEnum
public enum StirShakenUnscreenedTrunkGroupOriginationAttestationLevel {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("Partial")
    PARTIAL("Partial"),
    @XmlEnumValue("Gateway")
    GATEWAY("Gateway");
    private final String value;

    StirShakenUnscreenedTrunkGroupOriginationAttestationLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StirShakenUnscreenedTrunkGroupOriginationAttestationLevel fromValue(String v) {
        for (StirShakenUnscreenedTrunkGroupOriginationAttestationLevel c: StirShakenUnscreenedTrunkGroupOriginationAttestationLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
