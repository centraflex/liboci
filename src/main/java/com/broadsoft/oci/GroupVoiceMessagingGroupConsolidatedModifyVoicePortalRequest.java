//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the group's voice messaging settings and voice portal branding settings.
 *         If UnassignPhoneNumbersLevel is set to 'Group', the user's primary phone number, fax number and any alternate numbers, will be un-assigned from the group if the command is executed by a service provider administrator or above.
 *         When set to 'Service Provider', they will be un-assigned from the group and service provider if the command is executed by a provisioning administrator or above.
 *         When omitted, the number(s) will be left assigned to the group.
 *         An ErrorResponse will be returned if any number cannot be unassigned because of insufficient privilege.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupVoiceMessagingGroupConsolidatedModifyVoicePortalRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupVoiceMessagingGroupConsolidatedModifyVoicePortalRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="unassignPhoneNumbers" type="{}UnassignPhoneNumbersLevel" minOccurs="0"/>
 *         <element name="addPhoneNumberToGroup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceModifyProfile" minOccurs="0"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableExtendedScope" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useVoicePortalWizard" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="voicePortalExternalRoutingScope" type="{}VoicePortalExternalRoutingScope" minOccurs="0"/>
 *         <element name="useExternalRouting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="externalRoutingAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="homeZoneName" type="{}ZoneName" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="voicePortalGreetingSelection" type="{}VoiceMessagingBrandingSelection" minOccurs="0"/>
 *         <element name="voicePortalGreetingFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *         <element name="voiceMessagingGreetingSelection" type="{}VoiceMessagingBrandingSelection" minOccurs="0"/>
 *         <element name="voiceMessagingGreetingFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupVoiceMessagingGroupConsolidatedModifyVoicePortalRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "unassignPhoneNumbers",
    "addPhoneNumberToGroup",
    "serviceInstanceProfile",
    "isActive",
    "enableExtendedScope",
    "allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin",
    "useVoicePortalWizard",
    "voicePortalExternalRoutingScope",
    "useExternalRouting",
    "externalRoutingAddress",
    "homeZoneName",
    "networkClassOfService",
    "voicePortalGreetingSelection",
    "voicePortalGreetingFile",
    "voiceMessagingGreetingSelection",
    "voiceMessagingGreetingFile"
})
public class GroupVoiceMessagingGroupConsolidatedModifyVoicePortalRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlSchemaType(name = "token")
    protected UnassignPhoneNumbersLevel unassignPhoneNumbers;
    protected Boolean addPhoneNumberToGroup;
    protected ServiceInstanceModifyProfile serviceInstanceProfile;
    protected Boolean isActive;
    protected Boolean enableExtendedScope;
    protected Boolean allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin;
    protected Boolean useVoicePortalWizard;
    @XmlSchemaType(name = "token")
    protected VoicePortalExternalRoutingScope voicePortalExternalRoutingScope;
    protected Boolean useExternalRouting;
    @XmlElementRef(name = "externalRoutingAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externalRoutingAddress;
    @XmlElementRef(name = "homeZoneName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> homeZoneName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingBrandingSelection voicePortalGreetingSelection;
    @XmlElementRef(name = "voicePortalGreetingFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileKey> voicePortalGreetingFile;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingBrandingSelection voiceMessagingGreetingSelection;
    @XmlElementRef(name = "voiceMessagingGreetingFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileKey> voiceMessagingGreetingFile;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der unassignPhoneNumbers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnassignPhoneNumbersLevel }
     *     
     */
    public UnassignPhoneNumbersLevel getUnassignPhoneNumbers() {
        return unassignPhoneNumbers;
    }

    /**
     * Legt den Wert der unassignPhoneNumbers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnassignPhoneNumbersLevel }
     *     
     */
    public void setUnassignPhoneNumbers(UnassignPhoneNumbersLevel value) {
        this.unassignPhoneNumbers = value;
    }

    /**
     * Ruft den Wert der addPhoneNumberToGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAddPhoneNumberToGroup() {
        return addPhoneNumberToGroup;
    }

    /**
     * Legt den Wert der addPhoneNumberToGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAddPhoneNumberToGroup(Boolean value) {
        this.addPhoneNumberToGroup = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public ServiceInstanceModifyProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceModifyProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der enableExtendedScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableExtendedScope() {
        return enableExtendedScope;
    }

    /**
     * Legt den Wert der enableExtendedScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableExtendedScope(Boolean value) {
        this.enableExtendedScope = value;
    }

    /**
     * Ruft den Wert der allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin() {
        return allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin;
    }

    /**
     * Legt den Wert der allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin(Boolean value) {
        this.allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin = value;
    }

    /**
     * Ruft den Wert der useVoicePortalWizard-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseVoicePortalWizard() {
        return useVoicePortalWizard;
    }

    /**
     * Legt den Wert der useVoicePortalWizard-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseVoicePortalWizard(Boolean value) {
        this.useVoicePortalWizard = value;
    }

    /**
     * Ruft den Wert der voicePortalExternalRoutingScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoicePortalExternalRoutingScope }
     *     
     */
    public VoicePortalExternalRoutingScope getVoicePortalExternalRoutingScope() {
        return voicePortalExternalRoutingScope;
    }

    /**
     * Legt den Wert der voicePortalExternalRoutingScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoicePortalExternalRoutingScope }
     *     
     */
    public void setVoicePortalExternalRoutingScope(VoicePortalExternalRoutingScope value) {
        this.voicePortalExternalRoutingScope = value;
    }

    /**
     * Ruft den Wert der useExternalRouting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseExternalRouting() {
        return useExternalRouting;
    }

    /**
     * Legt den Wert der useExternalRouting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseExternalRouting(Boolean value) {
        this.useExternalRouting = value;
    }

    /**
     * Ruft den Wert der externalRoutingAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternalRoutingAddress() {
        return externalRoutingAddress;
    }

    /**
     * Legt den Wert der externalRoutingAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternalRoutingAddress(JAXBElement<String> value) {
        this.externalRoutingAddress = value;
    }

    /**
     * Ruft den Wert der homeZoneName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHomeZoneName() {
        return homeZoneName;
    }

    /**
     * Legt den Wert der homeZoneName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHomeZoneName(JAXBElement<String> value) {
        this.homeZoneName = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Ruft den Wert der voicePortalGreetingSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public VoiceMessagingBrandingSelection getVoicePortalGreetingSelection() {
        return voicePortalGreetingSelection;
    }

    /**
     * Legt den Wert der voicePortalGreetingSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public void setVoicePortalGreetingSelection(VoiceMessagingBrandingSelection value) {
        this.voicePortalGreetingSelection = value;
    }

    /**
     * Ruft den Wert der voicePortalGreetingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileKey> getVoicePortalGreetingFile() {
        return voicePortalGreetingFile;
    }

    /**
     * Legt den Wert der voicePortalGreetingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
     *     
     */
    public void setVoicePortalGreetingFile(JAXBElement<AnnouncementFileKey> value) {
        this.voicePortalGreetingFile = value;
    }

    /**
     * Ruft den Wert der voiceMessagingGreetingSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public VoiceMessagingBrandingSelection getVoiceMessagingGreetingSelection() {
        return voiceMessagingGreetingSelection;
    }

    /**
     * Legt den Wert der voiceMessagingGreetingSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public void setVoiceMessagingGreetingSelection(VoiceMessagingBrandingSelection value) {
        this.voiceMessagingGreetingSelection = value;
    }

    /**
     * Ruft den Wert der voiceMessagingGreetingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileKey> getVoiceMessagingGreetingFile() {
        return voiceMessagingGreetingFile;
    }

    /**
     * Legt den Wert der voiceMessagingGreetingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
     *     
     */
    public void setVoiceMessagingGreetingFile(JAXBElement<AnnouncementFileKey> value) {
        this.voiceMessagingGreetingFile = value;
    }

}
