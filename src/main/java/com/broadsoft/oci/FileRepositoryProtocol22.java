//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für FileRepositoryProtocol22.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="FileRepositoryProtocol22">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="WebDAV"/>
 *     <enumeration value="FTP"/>
 *     <enumeration value="FTPS"/>
 *     <enumeration value="SFTP"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "FileRepositoryProtocol22")
@XmlEnum
public enum FileRepositoryProtocol22 {

    @XmlEnumValue("WebDAV")
    WEB_DAV("WebDAV"),
    FTP("FTP"),
    FTPS("FTPS"),
    SFTP("SFTP");
    private final String value;

    FileRepositoryProtocol22(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FileRepositoryProtocol22 fromValue(String v) {
        for (FileRepositoryProtocol22 c: FileRepositoryProtocol22 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
