//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EnterpriseVoiceVPNPolicySelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="EnterpriseVoiceVPNPolicySelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Private"/>
 *     <enumeration value="Public"/>
 *     <enumeration value="Route"/>
 *     <enumeration value="Treatment"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "EnterpriseVoiceVPNPolicySelection")
@XmlEnum
public enum EnterpriseVoiceVPNPolicySelection {

    @XmlEnumValue("Private")
    PRIVATE("Private"),
    @XmlEnumValue("Public")
    PUBLIC("Public"),
    @XmlEnumValue("Route")
    ROUTE("Route"),
    @XmlEnumValue("Treatment")
    TREATMENT("Treatment");
    private final String value;

    EnterpriseVoiceVPNPolicySelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnterpriseVoiceVPNPolicySelection fromValue(String v) {
        for (EnterpriseVoiceVPNPolicySelection c: EnterpriseVoiceVPNPolicySelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
