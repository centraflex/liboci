//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemFileRepositoryDeviceGetRequest16.
 *         
 *         Replaced by: SystemFileRepositoryDeviceGetResponse20 in AS data mode
 *       
 * 
 * <p>Java-Klasse für SystemFileRepositoryDeviceGetResponse16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemFileRepositoryDeviceGetResponse16">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="rootDirectory" type="{}CPEFileDirectory" minOccurs="0"/>
 *         <element name="port" type="{}Port" minOccurs="0"/>
 *         <choice>
 *           <element name="protocolWebDAV" type="{}FileRepositoryProtocolWebDAV"/>
 *           <element name="protocolFTP" type="{}FileRepositoryProtocolFTP16"/>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemFileRepositoryDeviceGetResponse16", propOrder = {
    "rootDirectory",
    "port",
    "protocolWebDAV",
    "protocolFTP"
})
public class SystemFileRepositoryDeviceGetResponse16
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String rootDirectory;
    protected Integer port;
    protected FileRepositoryProtocolWebDAV protocolWebDAV;
    protected FileRepositoryProtocolFTP16 protocolFTP;

    /**
     * Ruft den Wert der rootDirectory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRootDirectory() {
        return rootDirectory;
    }

    /**
     * Legt den Wert der rootDirectory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRootDirectory(String value) {
        this.rootDirectory = value;
    }

    /**
     * Ruft den Wert der port-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPort() {
        return port;
    }

    /**
     * Legt den Wert der port-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPort(Integer value) {
        this.port = value;
    }

    /**
     * Ruft den Wert der protocolWebDAV-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FileRepositoryProtocolWebDAV }
     *     
     */
    public FileRepositoryProtocolWebDAV getProtocolWebDAV() {
        return protocolWebDAV;
    }

    /**
     * Legt den Wert der protocolWebDAV-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FileRepositoryProtocolWebDAV }
     *     
     */
    public void setProtocolWebDAV(FileRepositoryProtocolWebDAV value) {
        this.protocolWebDAV = value;
    }

    /**
     * Ruft den Wert der protocolFTP-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FileRepositoryProtocolFTP16 }
     *     
     */
    public FileRepositoryProtocolFTP16 getProtocolFTP() {
        return protocolFTP;
    }

    /**
     * Legt den Wert der protocolFTP-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FileRepositoryProtocolFTP16 }
     *     
     */
    public void setProtocolFTP(FileRepositoryProtocolFTP16 value) {
        this.protocolFTP = value;
    }

}
