//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal fax messaging menu keys.
 *       
 * 
 * <p>Java-Klasse für FaxMessagingMenuKeysReadEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="FaxMessagingMenuKeysReadEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="saveFaxMessageAndSkipToNext" type="{}DigitAny" minOccurs="0"/>
 *         <element name="previousFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
 *         <element name="nextFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="deleteFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="printFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaxMessagingMenuKeysReadEntry", propOrder = {
    "saveFaxMessageAndSkipToNext",
    "previousFaxMessage",
    "playEnvelope",
    "nextFaxMessage",
    "deleteFaxMessage",
    "printFaxMessage",
    "returnToPreviousMenu"
})
public class FaxMessagingMenuKeysReadEntry {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String saveFaxMessageAndSkipToNext;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String previousFaxMessage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String playEnvelope;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String nextFaxMessage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deleteFaxMessage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String printFaxMessage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;

    /**
     * Ruft den Wert der saveFaxMessageAndSkipToNext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaveFaxMessageAndSkipToNext() {
        return saveFaxMessageAndSkipToNext;
    }

    /**
     * Legt den Wert der saveFaxMessageAndSkipToNext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaveFaxMessageAndSkipToNext(String value) {
        this.saveFaxMessageAndSkipToNext = value;
    }

    /**
     * Ruft den Wert der previousFaxMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousFaxMessage() {
        return previousFaxMessage;
    }

    /**
     * Legt den Wert der previousFaxMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousFaxMessage(String value) {
        this.previousFaxMessage = value;
    }

    /**
     * Ruft den Wert der playEnvelope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlayEnvelope() {
        return playEnvelope;
    }

    /**
     * Legt den Wert der playEnvelope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlayEnvelope(String value) {
        this.playEnvelope = value;
    }

    /**
     * Ruft den Wert der nextFaxMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextFaxMessage() {
        return nextFaxMessage;
    }

    /**
     * Legt den Wert der nextFaxMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextFaxMessage(String value) {
        this.nextFaxMessage = value;
    }

    /**
     * Ruft den Wert der deleteFaxMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeleteFaxMessage() {
        return deleteFaxMessage;
    }

    /**
     * Legt den Wert der deleteFaxMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeleteFaxMessage(String value) {
        this.deleteFaxMessage = value;
    }

    /**
     * Ruft den Wert der printFaxMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrintFaxMessage() {
        return printFaxMessage;
    }

    /**
     * Legt den Wert der printFaxMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrintFaxMessage(String value) {
        this.printFaxMessage = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

}
