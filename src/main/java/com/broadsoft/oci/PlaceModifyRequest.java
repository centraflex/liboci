//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to configure a WebEx room/place to have the primary endpoint of WebEx Teams device. 
 *         
 *         placeUserId refers to a WebEx room/place. 
 *         webExSIPAddress specifies the WebEx SIP address of the place. The format of this parameter is: user@domain. 
 *         The user part specified in webExSIPAddress will be set to the lineport of the place’s primary endpoint. The endpoint
 *         will have a static address with URI set to "sip:user@domain". 
 *         The domain specified in webExSIPAddress will be:
 *         - added to the system if it does not exist yet. 
 *         - assigned to the service provider/enterprise which the place belongs to, if it has not been assigned yet.  
 *         - assigned to the group which the place belongs to, if it has not been assigned yet.  
 * 
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für PlaceModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="PlaceModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="placeUserId" type="{}UserId"/>
 *         <element name="webExSIPAddress" type="{}AccessDeviceEndpointLinePort" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlaceModifyRequest", propOrder = {
    "placeUserId",
    "webExSIPAddress"
})
public class PlaceModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String placeUserId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String webExSIPAddress;

    /**
     * Ruft den Wert der placeUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaceUserId() {
        return placeUserId;
    }

    /**
     * Legt den Wert der placeUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaceUserId(String value) {
        this.placeUserId = value;
    }

    /**
     * Ruft den Wert der webExSIPAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebExSIPAddress() {
        return webExSIPAddress;
    }

    /**
     * Legt den Wert der webExSIPAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebExSIPAddress(String value) {
        this.webExSIPAddress = value;
    }

}
