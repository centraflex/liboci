//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         A list of alternate user ids that that replaces a previously configured list.
 *         By convention, an element of this type may be set nill to clear the list.
 *       
 * 
 * <p>Java-Klasse für ReplacementAlternateUserIdEntryList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ReplacementAlternateUserIdEntryList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="alternateUserId" type="{}AlternateUserIdEntry" maxOccurs="4"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReplacementAlternateUserIdEntryList", propOrder = {
    "alternateUserId"
})
public class ReplacementAlternateUserIdEntryList {

    @XmlElement(required = true)
    protected List<AlternateUserIdEntry> alternateUserId;

    /**
     * Gets the value of the alternateUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the alternateUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateUserIdEntry }
     * 
     * 
     * @return
     *     The value of the alternateUserId property.
     */
    public List<AlternateUserIdEntry> getAlternateUserId() {
        if (alternateUserId == null) {
            alternateUserId = new ArrayList<>();
        }
        return this.alternateUserId;
    }

}
