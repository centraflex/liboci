//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemCustomerOriginatedTraceGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemCustomerOriginatedTraceGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCustomerOriginatedTraceGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="screenMaliciousCallers" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCustomerOriginatedTraceGetResponse", propOrder = {
    "screenMaliciousCallers"
})
public class SystemCustomerOriginatedTraceGetResponse
    extends OCIDataResponse
{

    protected boolean screenMaliciousCallers;

    /**
     * Ruft den Wert der screenMaliciousCallers-Eigenschaft ab.
     * 
     */
    public boolean isScreenMaliciousCallers() {
        return screenMaliciousCallers;
    }

    /**
     * Legt den Wert der screenMaliciousCallers-Eigenschaft fest.
     * 
     */
    public void setScreenMaliciousCallers(boolean value) {
        this.screenMaliciousCallers = value;
    }

}
