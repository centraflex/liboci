//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for a DN OR an extension.
 *         Note: For this search criterion, the searchMode is always ‘Contains’ and the search criteria are always OR’ed.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaComposedOrDnExtension complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaComposedOrDnExtension">
 *   <complexContent>
 *     <extension base="{}SearchCriteriaComposedOr">
 *       <sequence>
 *         <element name="value" type="{}UserDNExtension"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaComposedOrDnExtension", propOrder = {
    "value"
})
public class SearchCriteriaComposedOrDnExtension
    extends SearchCriteriaComposedOr
{

    @XmlElement(required = true)
    protected UserDNExtension value;

    /**
     * Ruft den Wert der value-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserDNExtension }
     *     
     */
    public UserDNExtension getValue() {
        return value;
    }

    /**
     * Legt den Wert der value-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDNExtension }
     *     
     */
    public void setValue(UserDNExtension value) {
        this.value = value;
    }

}
