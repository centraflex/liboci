//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request the number of access device of a particular device type and service provider.
 *         If countOnlyResetSupportedDevice is true, count only access devices if the device type supports reset.        
 *         By default unmanaged device types are not allowed and devices are counted only if their device type supports Device Management.
 *         An error is returned if deviceType is specified but does not support device management.
 *         When allowUnmanagedDeviceType is true, unmanaged device type will be counted and a successful response is returned.
 *         The response is either ServiceProviderDeviceManagementGetAccessDeviceCountForDeviceTypeServiceProviderResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderDeviceManagementGetAccessDeviceCountForDeviceTypeServiceProviderRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderDeviceManagementGetAccessDeviceCountForDeviceTypeServiceProviderRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="deviceType" type="{}AccessDeviceType"/>
 *         <element name="countOnlyResetSupportedDevice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowUnmanagedDeviceType" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderDeviceManagementGetAccessDeviceCountForDeviceTypeServiceProviderRequest", propOrder = {
    "serviceProviderId",
    "deviceType",
    "countOnlyResetSupportedDevice",
    "allowUnmanagedDeviceType"
})
public class ServiceProviderDeviceManagementGetAccessDeviceCountForDeviceTypeServiceProviderRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceType;
    protected Boolean countOnlyResetSupportedDevice;
    protected Boolean allowUnmanagedDeviceType;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der deviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Legt den Wert der deviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceType(String value) {
        this.deviceType = value;
    }

    /**
     * Ruft den Wert der countOnlyResetSupportedDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCountOnlyResetSupportedDevice() {
        return countOnlyResetSupportedDevice;
    }

    /**
     * Legt den Wert der countOnlyResetSupportedDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCountOnlyResetSupportedDevice(Boolean value) {
        this.countOnlyResetSupportedDevice = value;
    }

    /**
     * Ruft den Wert der allowUnmanagedDeviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowUnmanagedDeviceType() {
        return allowUnmanagedDeviceType;
    }

    /**
     * Legt den Wert der allowUnmanagedDeviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowUnmanagedDeviceType(Boolean value) {
        this.allowUnmanagedDeviceType = value;
    }

}
