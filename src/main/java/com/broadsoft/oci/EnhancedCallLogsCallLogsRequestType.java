//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EnhancedCallLogsCallLogsRequestType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="EnhancedCallLogsCallLogsRequestType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Placed"/>
 *     <enumeration value="Received"/>
 *     <enumeration value="Missed"/>
 *     <enumeration value="ReceivedOrMissed"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "EnhancedCallLogsCallLogsRequestType")
@XmlEnum
public enum EnhancedCallLogsCallLogsRequestType {

    @XmlEnumValue("Placed")
    PLACED("Placed"),
    @XmlEnumValue("Received")
    RECEIVED("Received"),
    @XmlEnumValue("Missed")
    MISSED("Missed"),
    @XmlEnumValue("ReceivedOrMissed")
    RECEIVED_OR_MISSED("ReceivedOrMissed");
    private final String value;

    EnhancedCallLogsCallLogsRequestType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnhancedCallLogsCallLogsRequestType fromValue(String v) {
        for (EnhancedCallLogsCallLogsRequestType c: EnhancedCallLogsCallLogsRequestType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
