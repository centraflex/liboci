//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for a particular fully specified DeviceManagement event level.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactDeviceManagementEventLevel complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactDeviceManagementEventLevel">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="dmEventLevel" type="{}DeviceManagementEventLevel"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactDeviceManagementEventLevel", propOrder = {
    "dmEventLevel"
})
public class SearchCriteriaExactDeviceManagementEventLevel
    extends SearchCriteria
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceManagementEventLevel dmEventLevel;

    /**
     * Ruft den Wert der dmEventLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementEventLevel }
     *     
     */
    public DeviceManagementEventLevel getDmEventLevel() {
        return dmEventLevel;
    }

    /**
     * Legt den Wert der dmEventLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementEventLevel }
     *     
     */
    public void setDmEventLevel(DeviceManagementEventLevel value) {
        this.dmEventLevel = value;
    }

}
