//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system Resource Priority service attributes.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemResourcePriorityModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemResourcePriorityModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="sendResourcePriorityToNetwork" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="resourcePriority" type="{}ResourcePriorityValue" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemResourcePriorityModifyRequest", propOrder = {
    "sendResourcePriorityToNetwork",
    "resourcePriority"
})
public class SystemResourcePriorityModifyRequest
    extends OCIRequest
{

    protected Boolean sendResourcePriorityToNetwork;
    @XmlSchemaType(name = "token")
    protected ResourcePriorityValue resourcePriority;

    /**
     * Ruft den Wert der sendResourcePriorityToNetwork-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendResourcePriorityToNetwork() {
        return sendResourcePriorityToNetwork;
    }

    /**
     * Legt den Wert der sendResourcePriorityToNetwork-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendResourcePriorityToNetwork(Boolean value) {
        this.sendResourcePriorityToNetwork = value;
    }

    /**
     * Ruft den Wert der resourcePriority-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResourcePriorityValue }
     *     
     */
    public ResourcePriorityValue getResourcePriority() {
        return resourcePriority;
    }

    /**
     * Legt den Wert der resourcePriority-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResourcePriorityValue }
     *     
     */
    public void setResourcePriority(ResourcePriorityValue value) {
        this.resourcePriority = value;
    }

}
