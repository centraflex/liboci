//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The response to a SystemBroadWorksMobilityMobileNetworkGetRequest. 
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobilityMobileNetworkGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobilityMobileNetworkGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="scfSignalingNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="scfSignalingPort" type="{}Port" minOccurs="0"/>
 *         <element name="refreshPeriodSeconds" type="{}SCFRefreshPeriodSeconds"/>
 *         <element name="maxConsecutiveFailures" type="{}SCFMaxConsecutiveFailures"/>
 *         <element name="maxResponseWaitTimeMilliseconds" type="{}SCFMaxResponseWaitTimeMilliseconds"/>
 *         <element name="enableAnnouncementSuppression" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="serviceAccessCodeListName" type="{}ServiceAccessCodeListName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobilityMobileNetworkGetResponse", propOrder = {
    "scfSignalingNetAddress",
    "scfSignalingPort",
    "refreshPeriodSeconds",
    "maxConsecutiveFailures",
    "maxResponseWaitTimeMilliseconds",
    "enableAnnouncementSuppression",
    "serviceAccessCodeListName"
})
public class SystemBroadWorksMobilityMobileNetworkGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String scfSignalingNetAddress;
    protected Integer scfSignalingPort;
    protected int refreshPeriodSeconds;
    protected int maxConsecutiveFailures;
    protected int maxResponseWaitTimeMilliseconds;
    protected boolean enableAnnouncementSuppression;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceAccessCodeListName;

    /**
     * Ruft den Wert der scfSignalingNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScfSignalingNetAddress() {
        return scfSignalingNetAddress;
    }

    /**
     * Legt den Wert der scfSignalingNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScfSignalingNetAddress(String value) {
        this.scfSignalingNetAddress = value;
    }

    /**
     * Ruft den Wert der scfSignalingPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getScfSignalingPort() {
        return scfSignalingPort;
    }

    /**
     * Legt den Wert der scfSignalingPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setScfSignalingPort(Integer value) {
        this.scfSignalingPort = value;
    }

    /**
     * Ruft den Wert der refreshPeriodSeconds-Eigenschaft ab.
     * 
     */
    public int getRefreshPeriodSeconds() {
        return refreshPeriodSeconds;
    }

    /**
     * Legt den Wert der refreshPeriodSeconds-Eigenschaft fest.
     * 
     */
    public void setRefreshPeriodSeconds(int value) {
        this.refreshPeriodSeconds = value;
    }

    /**
     * Ruft den Wert der maxConsecutiveFailures-Eigenschaft ab.
     * 
     */
    public int getMaxConsecutiveFailures() {
        return maxConsecutiveFailures;
    }

    /**
     * Legt den Wert der maxConsecutiveFailures-Eigenschaft fest.
     * 
     */
    public void setMaxConsecutiveFailures(int value) {
        this.maxConsecutiveFailures = value;
    }

    /**
     * Ruft den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft ab.
     * 
     */
    public int getMaxResponseWaitTimeMilliseconds() {
        return maxResponseWaitTimeMilliseconds;
    }

    /**
     * Legt den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft fest.
     * 
     */
    public void setMaxResponseWaitTimeMilliseconds(int value) {
        this.maxResponseWaitTimeMilliseconds = value;
    }

    /**
     * Ruft den Wert der enableAnnouncementSuppression-Eigenschaft ab.
     * 
     */
    public boolean isEnableAnnouncementSuppression() {
        return enableAnnouncementSuppression;
    }

    /**
     * Legt den Wert der enableAnnouncementSuppression-Eigenschaft fest.
     * 
     */
    public void setEnableAnnouncementSuppression(boolean value) {
        this.enableAnnouncementSuppression = value;
    }

    /**
     * Ruft den Wert der serviceAccessCodeListName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceAccessCodeListName() {
        return serviceAccessCodeListName;
    }

    /**
     * Legt den Wert der serviceAccessCodeListName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceAccessCodeListName(String value) {
        this.serviceAccessCodeListName = value;
    }

}
