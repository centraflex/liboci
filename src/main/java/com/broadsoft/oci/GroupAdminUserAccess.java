//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GroupAdminUserAccess.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="GroupAdminUserAccess">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Full"/>
 *     <enumeration value="Full Profile"/>
 *     <enumeration value="Read-Only Profile"/>
 *     <enumeration value="No Profile"/>
 *     <enumeration value="None"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "GroupAdminUserAccess")
@XmlEnum
public enum GroupAdminUserAccess {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("Full Profile")
    FULL_PROFILE("Full Profile"),
    @XmlEnumValue("Read-Only Profile")
    READ_ONLY_PROFILE("Read-Only Profile"),
    @XmlEnumValue("No Profile")
    NO_PROFILE("No Profile"),
    @XmlEnumValue("None")
    NONE("None");
    private final String value;

    GroupAdminUserAccess(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupAdminUserAccess fromValue(String v) {
        for (GroupAdminUserAccess c: GroupAdminUserAccess.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
