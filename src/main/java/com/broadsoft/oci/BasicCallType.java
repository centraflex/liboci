//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für BasicCallType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="BasicCallType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Group"/>
 *     <enumeration value="Enterprise"/>
 *     <enumeration value="Network"/>
 *     <enumeration value="Network URL"/>
 *     <enumeration value="Repair"/>
 *     <enumeration value="Emergency"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "BasicCallType")
@XmlEnum
public enum BasicCallType {

    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("Enterprise")
    ENTERPRISE("Enterprise"),
    @XmlEnumValue("Network")
    NETWORK("Network"),
    @XmlEnumValue("Network URL")
    NETWORK_URL("Network URL"),
    @XmlEnumValue("Repair")
    REPAIR("Repair"),
    @XmlEnumValue("Emergency")
    EMERGENCY("Emergency");
    private final String value;

    BasicCallType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BasicCallType fromValue(String v) {
        for (BasicCallType c: BasicCallType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
