//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserPrivacyGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserPrivacyGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPrivacyGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableDirectoryPrivacy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPrivacyGetResponse", propOrder = {
    "enableDirectoryPrivacy"
})
public class UserPrivacyGetResponse
    extends OCIDataResponse
{

    protected boolean enableDirectoryPrivacy;

    /**
     * Ruft den Wert der enableDirectoryPrivacy-Eigenschaft ab.
     * 
     */
    public boolean isEnableDirectoryPrivacy() {
        return enableDirectoryPrivacy;
    }

    /**
     * Legt den Wert der enableDirectoryPrivacy-Eigenschaft fest.
     * 
     */
    public void setEnableDirectoryPrivacy(boolean value) {
        this.enableDirectoryPrivacy = value;
    }

}
