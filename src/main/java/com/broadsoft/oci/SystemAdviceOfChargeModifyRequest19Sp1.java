//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify Advice of Charge system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemAdviceOfChargeModifyRequest19sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAdviceOfChargeModifyRequest19sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="delayBetweenNotificationSeconds" type="{}AdviceOfChargeDelayBetweenNotificationSeconds" minOccurs="0"/>
 *         <element name="incomingAocHandling" type="{}AdviceOfChargeIncomingAocHandling" minOccurs="0"/>
 *         <element name="useOCSEnquiry" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="OCSEnquiryType" type="{}AdviceOfChargeOCSEnquiryType" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAdviceOfChargeModifyRequest19sp1", propOrder = {
    "delayBetweenNotificationSeconds",
    "incomingAocHandling",
    "useOCSEnquiry",
    "ocsEnquiryType"
})
public class SystemAdviceOfChargeModifyRequest19Sp1
    extends OCIRequest
{

    protected Integer delayBetweenNotificationSeconds;
    @XmlSchemaType(name = "token")
    protected AdviceOfChargeIncomingAocHandling incomingAocHandling;
    protected Boolean useOCSEnquiry;
    @XmlElement(name = "OCSEnquiryType")
    @XmlSchemaType(name = "token")
    protected AdviceOfChargeOCSEnquiryType ocsEnquiryType;

    /**
     * Ruft den Wert der delayBetweenNotificationSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDelayBetweenNotificationSeconds() {
        return delayBetweenNotificationSeconds;
    }

    /**
     * Legt den Wert der delayBetweenNotificationSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDelayBetweenNotificationSeconds(Integer value) {
        this.delayBetweenNotificationSeconds = value;
    }

    /**
     * Ruft den Wert der incomingAocHandling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AdviceOfChargeIncomingAocHandling }
     *     
     */
    public AdviceOfChargeIncomingAocHandling getIncomingAocHandling() {
        return incomingAocHandling;
    }

    /**
     * Legt den Wert der incomingAocHandling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdviceOfChargeIncomingAocHandling }
     *     
     */
    public void setIncomingAocHandling(AdviceOfChargeIncomingAocHandling value) {
        this.incomingAocHandling = value;
    }

    /**
     * Ruft den Wert der useOCSEnquiry-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseOCSEnquiry() {
        return useOCSEnquiry;
    }

    /**
     * Legt den Wert der useOCSEnquiry-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseOCSEnquiry(Boolean value) {
        this.useOCSEnquiry = value;
    }

    /**
     * Ruft den Wert der ocsEnquiryType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AdviceOfChargeOCSEnquiryType }
     *     
     */
    public AdviceOfChargeOCSEnquiryType getOCSEnquiryType() {
        return ocsEnquiryType;
    }

    /**
     * Legt den Wert der ocsEnquiryType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdviceOfChargeOCSEnquiryType }
     *     
     */
    public void setOCSEnquiryType(AdviceOfChargeOCSEnquiryType value) {
        this.ocsEnquiryType = value;
    }

}
