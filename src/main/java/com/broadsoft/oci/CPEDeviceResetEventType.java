//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CPEDeviceResetEventType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CPEDeviceResetEventType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Resync"/>
 *     <enumeration value="Check Sync"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CPEDeviceResetEventType")
@XmlEnum
public enum CPEDeviceResetEventType {

    @XmlEnumValue("Resync")
    RESYNC("Resync"),
    @XmlEnumValue("Check Sync")
    CHECK_SYNC("Check Sync");
    private final String value;

    CPEDeviceResetEventType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CPEDeviceResetEventType fromValue(String v) {
        for (CPEDeviceResetEventType c: CPEDeviceResetEventType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
