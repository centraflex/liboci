//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a Virtual On-Net Call Type.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemVirtualOnNetCallTypeModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemVirtualOnNetCallTypeModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="virtualOnNetCallTypeName" type="{}VirtualOnNetCallTypeName"/>
 *         <element name="virtualOnNetCallTypeCdrValue" type="{}VirtualOnNetCallTypeCdrValue" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemVirtualOnNetCallTypeModifyRequest", propOrder = {
    "virtualOnNetCallTypeName",
    "virtualOnNetCallTypeCdrValue"
})
public class SystemVirtualOnNetCallTypeModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String virtualOnNetCallTypeName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String virtualOnNetCallTypeCdrValue;

    /**
     * Ruft den Wert der virtualOnNetCallTypeName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVirtualOnNetCallTypeName() {
        return virtualOnNetCallTypeName;
    }

    /**
     * Legt den Wert der virtualOnNetCallTypeName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVirtualOnNetCallTypeName(String value) {
        this.virtualOnNetCallTypeName = value;
    }

    /**
     * Ruft den Wert der virtualOnNetCallTypeCdrValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVirtualOnNetCallTypeCdrValue() {
        return virtualOnNetCallTypeCdrValue;
    }

    /**
     * Legt den Wert der virtualOnNetCallTypeCdrValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVirtualOnNetCallTypeCdrValue(String value) {
        this.virtualOnNetCallTypeCdrValue = value;
    }

}
