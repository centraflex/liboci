//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the group's voice portal branding settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupVoiceMessagingGroupModifyVoicePortalBrandingRequest20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupVoiceMessagingGroupModifyVoicePortalBrandingRequest20">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="voicePortalGreetingSelection" type="{}VoiceMessagingBrandingSelection" minOccurs="0"/>
 *         <element name="voicePortalGreetingFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *         <element name="voiceMessagingGreetingSelection" type="{}VoiceMessagingBrandingSelection" minOccurs="0"/>
 *         <element name="voiceMessagingGreetingFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupVoiceMessagingGroupModifyVoicePortalBrandingRequest20", propOrder = {
    "serviceProviderId",
    "groupId",
    "voicePortalGreetingSelection",
    "voicePortalGreetingFile",
    "voiceMessagingGreetingSelection",
    "voiceMessagingGreetingFile"
})
public class GroupVoiceMessagingGroupModifyVoicePortalBrandingRequest20
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingBrandingSelection voicePortalGreetingSelection;
    @XmlElementRef(name = "voicePortalGreetingFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileKey> voicePortalGreetingFile;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingBrandingSelection voiceMessagingGreetingSelection;
    @XmlElementRef(name = "voiceMessagingGreetingFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileKey> voiceMessagingGreetingFile;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der voicePortalGreetingSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public VoiceMessagingBrandingSelection getVoicePortalGreetingSelection() {
        return voicePortalGreetingSelection;
    }

    /**
     * Legt den Wert der voicePortalGreetingSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public void setVoicePortalGreetingSelection(VoiceMessagingBrandingSelection value) {
        this.voicePortalGreetingSelection = value;
    }

    /**
     * Ruft den Wert der voicePortalGreetingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileKey> getVoicePortalGreetingFile() {
        return voicePortalGreetingFile;
    }

    /**
     * Legt den Wert der voicePortalGreetingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
     *     
     */
    public void setVoicePortalGreetingFile(JAXBElement<AnnouncementFileKey> value) {
        this.voicePortalGreetingFile = value;
    }

    /**
     * Ruft den Wert der voiceMessagingGreetingSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public VoiceMessagingBrandingSelection getVoiceMessagingGreetingSelection() {
        return voiceMessagingGreetingSelection;
    }

    /**
     * Legt den Wert der voiceMessagingGreetingSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public void setVoiceMessagingGreetingSelection(VoiceMessagingBrandingSelection value) {
        this.voiceMessagingGreetingSelection = value;
    }

    /**
     * Ruft den Wert der voiceMessagingGreetingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileKey> getVoiceMessagingGreetingFile() {
        return voiceMessagingGreetingFile;
    }

    /**
     * Legt den Wert der voiceMessagingGreetingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileKey }{@code >}
     *     
     */
    public void setVoiceMessagingGreetingFile(JAXBElement<AnnouncementFileKey> value) {
        this.voiceMessagingGreetingFile = value;
    }

}
