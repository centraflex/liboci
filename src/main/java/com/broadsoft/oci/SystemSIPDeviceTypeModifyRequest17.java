//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a sip device type.
 *         The following elements are not changeable:
 *           numberOfPorts
 *           SignalingAddressType
 *           isConferenceDevice
 *           isMusicOnHoldDevice
 *           isMobilityManagerDevice
 *         The response is either SuccessResponse or ErrorResponse.
 *         
 *         The following elements are only used in XS data mode:
 *           enable3G4GContinuity        
 *         
 *         Replaced by: SystemSIPDeviceTypeModifyRequest19     
 *       
 * 
 * <p>Java-Klasse für SystemSIPDeviceTypeModifyRequest17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPDeviceTypeModifyRequest17">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="deviceType" type="{}AccessDeviceType"/>
 *         <element name="isObsolete" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="registrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="RFC3264Hold" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="isTrusted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="E164Capable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="routeAdvance" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forwardingOverride" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="wirelessIntegration" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="webBasedConfigURL" type="{}WebBasedConfigURL" minOccurs="0"/>
 *         <element name="isVideoCapable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="PBXIntegration" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="staticRegistrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="cpeDeviceOptions" type="{}CPEDeviceModifyOptions16sp1" minOccurs="0"/>
 *         <element name="earlyMediaSupport" type="{}EarlyMediaSupportType" minOccurs="0"/>
 *         <element name="authenticateRefer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="autoConfigSoftClient" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="authenticationMode" type="{}AuthenticationMode" minOccurs="0"/>
 *         <element name="requiresBroadWorksDigitCollection" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="requiresBroadWorksCallWaitingTone" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="requiresMWISubscription" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useHistoryInfoHeaderOnAccessSide" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="adviceOfChargeCapable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="resetEvent" type="{}AccessDeviceResetEvent" minOccurs="0"/>
 *         <element name="supportCallCenterMIMEType" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="trunkMode" type="{}TrunkMode" minOccurs="0"/>
 *         <element name="addPCalledPartyId" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="supportIdentityInUpdateAndReInvite" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="unscreenedPresentationIdentityPolicy" type="{}UnscreenedPresentationIdentityPolicy" minOccurs="0"/>
 *         <element name="enable3G4GContinuity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="supportEmergencyDisconnectControl" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPDeviceTypeModifyRequest17", propOrder = {
    "deviceType",
    "isObsolete",
    "registrationCapable",
    "rfc3264Hold",
    "isTrusted",
    "e164Capable",
    "routeAdvance",
    "forwardingOverride",
    "wirelessIntegration",
    "webBasedConfigURL",
    "isVideoCapable",
    "pbxIntegration",
    "staticRegistrationCapable",
    "cpeDeviceOptions",
    "earlyMediaSupport",
    "authenticateRefer",
    "autoConfigSoftClient",
    "authenticationMode",
    "requiresBroadWorksDigitCollection",
    "requiresBroadWorksCallWaitingTone",
    "requiresMWISubscription",
    "useHistoryInfoHeaderOnAccessSide",
    "adviceOfChargeCapable",
    "resetEvent",
    "supportCallCenterMIMEType",
    "trunkMode",
    "addPCalledPartyId",
    "supportIdentityInUpdateAndReInvite",
    "unscreenedPresentationIdentityPolicy",
    "enable3G4GContinuity",
    "supportEmergencyDisconnectControl"
})
public class SystemSIPDeviceTypeModifyRequest17
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceType;
    protected Boolean isObsolete;
    protected Boolean registrationCapable;
    @XmlElement(name = "RFC3264Hold")
    protected Boolean rfc3264Hold;
    protected Boolean isTrusted;
    @XmlElement(name = "E164Capable")
    protected Boolean e164Capable;
    protected Boolean routeAdvance;
    protected Boolean forwardingOverride;
    protected Boolean wirelessIntegration;
    @XmlElementRef(name = "webBasedConfigURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> webBasedConfigURL;
    protected Boolean isVideoCapable;
    @XmlElement(name = "PBXIntegration")
    protected Boolean pbxIntegration;
    protected Boolean staticRegistrationCapable;
    protected CPEDeviceModifyOptions16Sp1 cpeDeviceOptions;
    @XmlSchemaType(name = "token")
    protected EarlyMediaSupportType earlyMediaSupport;
    protected Boolean authenticateRefer;
    protected Boolean autoConfigSoftClient;
    @XmlSchemaType(name = "token")
    protected AuthenticationMode authenticationMode;
    protected Boolean requiresBroadWorksDigitCollection;
    protected Boolean requiresBroadWorksCallWaitingTone;
    protected Boolean requiresMWISubscription;
    protected Boolean useHistoryInfoHeaderOnAccessSide;
    protected Boolean adviceOfChargeCapable;
    @XmlElementRef(name = "resetEvent", type = JAXBElement.class, required = false)
    protected JAXBElement<AccessDeviceResetEvent> resetEvent;
    protected Boolean supportCallCenterMIMEType;
    @XmlSchemaType(name = "token")
    protected TrunkMode trunkMode;
    protected Boolean addPCalledPartyId;
    protected Boolean supportIdentityInUpdateAndReInvite;
    @XmlSchemaType(name = "token")
    protected UnscreenedPresentationIdentityPolicy unscreenedPresentationIdentityPolicy;
    protected Boolean enable3G4GContinuity;
    protected Boolean supportEmergencyDisconnectControl;

    /**
     * Ruft den Wert der deviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Legt den Wert der deviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceType(String value) {
        this.deviceType = value;
    }

    /**
     * Ruft den Wert der isObsolete-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsObsolete() {
        return isObsolete;
    }

    /**
     * Legt den Wert der isObsolete-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsObsolete(Boolean value) {
        this.isObsolete = value;
    }

    /**
     * Ruft den Wert der registrationCapable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRegistrationCapable() {
        return registrationCapable;
    }

    /**
     * Legt den Wert der registrationCapable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRegistrationCapable(Boolean value) {
        this.registrationCapable = value;
    }

    /**
     * Ruft den Wert der rfc3264Hold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRFC3264Hold() {
        return rfc3264Hold;
    }

    /**
     * Legt den Wert der rfc3264Hold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRFC3264Hold(Boolean value) {
        this.rfc3264Hold = value;
    }

    /**
     * Ruft den Wert der isTrusted-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsTrusted() {
        return isTrusted;
    }

    /**
     * Legt den Wert der isTrusted-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsTrusted(Boolean value) {
        this.isTrusted = value;
    }

    /**
     * Ruft den Wert der e164Capable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isE164Capable() {
        return e164Capable;
    }

    /**
     * Legt den Wert der e164Capable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setE164Capable(Boolean value) {
        this.e164Capable = value;
    }

    /**
     * Ruft den Wert der routeAdvance-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRouteAdvance() {
        return routeAdvance;
    }

    /**
     * Legt den Wert der routeAdvance-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRouteAdvance(Boolean value) {
        this.routeAdvance = value;
    }

    /**
     * Ruft den Wert der forwardingOverride-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForwardingOverride() {
        return forwardingOverride;
    }

    /**
     * Legt den Wert der forwardingOverride-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForwardingOverride(Boolean value) {
        this.forwardingOverride = value;
    }

    /**
     * Ruft den Wert der wirelessIntegration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWirelessIntegration() {
        return wirelessIntegration;
    }

    /**
     * Legt den Wert der wirelessIntegration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWirelessIntegration(Boolean value) {
        this.wirelessIntegration = value;
    }

    /**
     * Ruft den Wert der webBasedConfigURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWebBasedConfigURL() {
        return webBasedConfigURL;
    }

    /**
     * Legt den Wert der webBasedConfigURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWebBasedConfigURL(JAXBElement<String> value) {
        this.webBasedConfigURL = value;
    }

    /**
     * Ruft den Wert der isVideoCapable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsVideoCapable() {
        return isVideoCapable;
    }

    /**
     * Legt den Wert der isVideoCapable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsVideoCapable(Boolean value) {
        this.isVideoCapable = value;
    }

    /**
     * Ruft den Wert der pbxIntegration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPBXIntegration() {
        return pbxIntegration;
    }

    /**
     * Legt den Wert der pbxIntegration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPBXIntegration(Boolean value) {
        this.pbxIntegration = value;
    }

    /**
     * Ruft den Wert der staticRegistrationCapable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStaticRegistrationCapable() {
        return staticRegistrationCapable;
    }

    /**
     * Legt den Wert der staticRegistrationCapable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStaticRegistrationCapable(Boolean value) {
        this.staticRegistrationCapable = value;
    }

    /**
     * Ruft den Wert der cpeDeviceOptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CPEDeviceModifyOptions16Sp1 }
     *     
     */
    public CPEDeviceModifyOptions16Sp1 getCpeDeviceOptions() {
        return cpeDeviceOptions;
    }

    /**
     * Legt den Wert der cpeDeviceOptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CPEDeviceModifyOptions16Sp1 }
     *     
     */
    public void setCpeDeviceOptions(CPEDeviceModifyOptions16Sp1 value) {
        this.cpeDeviceOptions = value;
    }

    /**
     * Ruft den Wert der earlyMediaSupport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EarlyMediaSupportType }
     *     
     */
    public EarlyMediaSupportType getEarlyMediaSupport() {
        return earlyMediaSupport;
    }

    /**
     * Legt den Wert der earlyMediaSupport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EarlyMediaSupportType }
     *     
     */
    public void setEarlyMediaSupport(EarlyMediaSupportType value) {
        this.earlyMediaSupport = value;
    }

    /**
     * Ruft den Wert der authenticateRefer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAuthenticateRefer() {
        return authenticateRefer;
    }

    /**
     * Legt den Wert der authenticateRefer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAuthenticateRefer(Boolean value) {
        this.authenticateRefer = value;
    }

    /**
     * Ruft den Wert der autoConfigSoftClient-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutoConfigSoftClient() {
        return autoConfigSoftClient;
    }

    /**
     * Legt den Wert der autoConfigSoftClient-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoConfigSoftClient(Boolean value) {
        this.autoConfigSoftClient = value;
    }

    /**
     * Ruft den Wert der authenticationMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationMode }
     *     
     */
    public AuthenticationMode getAuthenticationMode() {
        return authenticationMode;
    }

    /**
     * Legt den Wert der authenticationMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationMode }
     *     
     */
    public void setAuthenticationMode(AuthenticationMode value) {
        this.authenticationMode = value;
    }

    /**
     * Ruft den Wert der requiresBroadWorksDigitCollection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequiresBroadWorksDigitCollection() {
        return requiresBroadWorksDigitCollection;
    }

    /**
     * Legt den Wert der requiresBroadWorksDigitCollection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequiresBroadWorksDigitCollection(Boolean value) {
        this.requiresBroadWorksDigitCollection = value;
    }

    /**
     * Ruft den Wert der requiresBroadWorksCallWaitingTone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequiresBroadWorksCallWaitingTone() {
        return requiresBroadWorksCallWaitingTone;
    }

    /**
     * Legt den Wert der requiresBroadWorksCallWaitingTone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequiresBroadWorksCallWaitingTone(Boolean value) {
        this.requiresBroadWorksCallWaitingTone = value;
    }

    /**
     * Ruft den Wert der requiresMWISubscription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequiresMWISubscription() {
        return requiresMWISubscription;
    }

    /**
     * Legt den Wert der requiresMWISubscription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequiresMWISubscription(Boolean value) {
        this.requiresMWISubscription = value;
    }

    /**
     * Ruft den Wert der useHistoryInfoHeaderOnAccessSide-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseHistoryInfoHeaderOnAccessSide() {
        return useHistoryInfoHeaderOnAccessSide;
    }

    /**
     * Legt den Wert der useHistoryInfoHeaderOnAccessSide-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseHistoryInfoHeaderOnAccessSide(Boolean value) {
        this.useHistoryInfoHeaderOnAccessSide = value;
    }

    /**
     * Ruft den Wert der adviceOfChargeCapable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAdviceOfChargeCapable() {
        return adviceOfChargeCapable;
    }

    /**
     * Legt den Wert der adviceOfChargeCapable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAdviceOfChargeCapable(Boolean value) {
        this.adviceOfChargeCapable = value;
    }

    /**
     * Ruft den Wert der resetEvent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceResetEvent }{@code >}
     *     
     */
    public JAXBElement<AccessDeviceResetEvent> getResetEvent() {
        return resetEvent;
    }

    /**
     * Legt den Wert der resetEvent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceResetEvent }{@code >}
     *     
     */
    public void setResetEvent(JAXBElement<AccessDeviceResetEvent> value) {
        this.resetEvent = value;
    }

    /**
     * Ruft den Wert der supportCallCenterMIMEType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportCallCenterMIMEType() {
        return supportCallCenterMIMEType;
    }

    /**
     * Legt den Wert der supportCallCenterMIMEType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportCallCenterMIMEType(Boolean value) {
        this.supportCallCenterMIMEType = value;
    }

    /**
     * Ruft den Wert der trunkMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkMode }
     *     
     */
    public TrunkMode getTrunkMode() {
        return trunkMode;
    }

    /**
     * Legt den Wert der trunkMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkMode }
     *     
     */
    public void setTrunkMode(TrunkMode value) {
        this.trunkMode = value;
    }

    /**
     * Ruft den Wert der addPCalledPartyId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAddPCalledPartyId() {
        return addPCalledPartyId;
    }

    /**
     * Legt den Wert der addPCalledPartyId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAddPCalledPartyId(Boolean value) {
        this.addPCalledPartyId = value;
    }

    /**
     * Ruft den Wert der supportIdentityInUpdateAndReInvite-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportIdentityInUpdateAndReInvite() {
        return supportIdentityInUpdateAndReInvite;
    }

    /**
     * Legt den Wert der supportIdentityInUpdateAndReInvite-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportIdentityInUpdateAndReInvite(Boolean value) {
        this.supportIdentityInUpdateAndReInvite = value;
    }

    /**
     * Ruft den Wert der unscreenedPresentationIdentityPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnscreenedPresentationIdentityPolicy }
     *     
     */
    public UnscreenedPresentationIdentityPolicy getUnscreenedPresentationIdentityPolicy() {
        return unscreenedPresentationIdentityPolicy;
    }

    /**
     * Legt den Wert der unscreenedPresentationIdentityPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnscreenedPresentationIdentityPolicy }
     *     
     */
    public void setUnscreenedPresentationIdentityPolicy(UnscreenedPresentationIdentityPolicy value) {
        this.unscreenedPresentationIdentityPolicy = value;
    }

    /**
     * Ruft den Wert der enable3G4GContinuity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnable3G4GContinuity() {
        return enable3G4GContinuity;
    }

    /**
     * Legt den Wert der enable3G4GContinuity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnable3G4GContinuity(Boolean value) {
        this.enable3G4GContinuity = value;
    }

    /**
     * Ruft den Wert der supportEmergencyDisconnectControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportEmergencyDisconnectControl() {
        return supportEmergencyDisconnectControl;
    }

    /**
     * Legt den Wert der supportEmergencyDisconnectControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportEmergencyDisconnectControl(Boolean value) {
        this.supportEmergencyDisconnectControl = value;
    }

}
