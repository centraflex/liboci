//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemBroadWorksMobilityMobileSubscriberDirectoryNumberGetSummaryListRequest.
 *         The response contains a table with columns: "Mobile Number", "Mobile Network", "Service Provider Id", 
 *         "Is Enterprise", "Group Id", "User Id", "Last Name", "First Name", "Hiragana Last Name", 
 *         "Hiragana First Name", "Phone Number", "Extension" and "Reseller Id".
 *         
 *         The following columns are only returned in AS data mode:       
 *              "Reseller Id"
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobilityMobileSubscriberDirectoryNumberGetSummaryListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobilityMobileSubscriberDirectoryNumberGetSummaryListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="mobileSubscriberDirectoryNumbersSummaryTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobilityMobileSubscriberDirectoryNumberGetSummaryListResponse", propOrder = {
    "mobileSubscriberDirectoryNumbersSummaryTable"
})
public class SystemBroadWorksMobilityMobileSubscriberDirectoryNumberGetSummaryListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable mobileSubscriberDirectoryNumbersSummaryTable;

    /**
     * Ruft den Wert der mobileSubscriberDirectoryNumbersSummaryTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getMobileSubscriberDirectoryNumbersSummaryTable() {
        return mobileSubscriberDirectoryNumbersSummaryTable;
    }

    /**
     * Legt den Wert der mobileSubscriberDirectoryNumbersSummaryTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setMobileSubscriberDirectoryNumbersSummaryTable(OCITable value) {
        this.mobileSubscriberDirectoryNumbersSummaryTable = value;
    }

}
