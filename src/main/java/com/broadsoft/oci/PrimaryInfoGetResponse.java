//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Information about the primary server in the high-availablity cluster.
 *         For optimization, we only get the hostname and addresses for primary if they are
 *         explicitly requested or if the current server is not the primary.
 *       
 * 
 * <p>Java-Klasse für PrimaryInfoGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="PrimaryInfoGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isPrimary" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="hostnameForPrimary" type="{}NetAddress" minOccurs="0"/>
 *         <element name="addressForPrimary" type="{}NetAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="privateAddressForPrimary" type="{}NetAddress" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrimaryInfoGetResponse", propOrder = {
    "isPrimary",
    "hostnameForPrimary",
    "addressForPrimary",
    "privateAddressForPrimary"
})
public class PrimaryInfoGetResponse
    extends OCIDataResponse
{

    protected boolean isPrimary;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String hostnameForPrimary;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> addressForPrimary;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> privateAddressForPrimary;

    /**
     * Ruft den Wert der isPrimary-Eigenschaft ab.
     * 
     */
    public boolean isIsPrimary() {
        return isPrimary;
    }

    /**
     * Legt den Wert der isPrimary-Eigenschaft fest.
     * 
     */
    public void setIsPrimary(boolean value) {
        this.isPrimary = value;
    }

    /**
     * Ruft den Wert der hostnameForPrimary-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHostnameForPrimary() {
        return hostnameForPrimary;
    }

    /**
     * Legt den Wert der hostnameForPrimary-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHostnameForPrimary(String value) {
        this.hostnameForPrimary = value;
    }

    /**
     * Gets the value of the addressForPrimary property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the addressForPrimary property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddressForPrimary().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the addressForPrimary property.
     */
    public List<String> getAddressForPrimary() {
        if (addressForPrimary == null) {
            addressForPrimary = new ArrayList<>();
        }
        return this.addressForPrimary;
    }

    /**
     * Gets the value of the privateAddressForPrimary property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the privateAddressForPrimary property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrivateAddressForPrimary().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the privateAddressForPrimary property.
     */
    public List<String> getPrivateAddressForPrimary() {
        if (privateAddressForPrimary == null) {
            privateAddressForPrimary = new ArrayList<>();
        }
        return this.privateAddressForPrimary;
    }

}
