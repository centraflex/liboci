//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupCallCenterGetDistinctiveRingingRequest.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterGetDistinctiveRingingResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterGetDistinctiveRingingResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="distinctiveRingingCallCenterCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="distinctiveRingingRingPatternForCallCenter" type="{}RingPattern"/>
 *         <element name="distinctiveRingingForceDeliveryRingPattern" type="{}RingPattern" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterGetDistinctiveRingingResponse", propOrder = {
    "distinctiveRingingCallCenterCalls",
    "distinctiveRingingRingPatternForCallCenter",
    "distinctiveRingingForceDeliveryRingPattern"
})
public class GroupCallCenterGetDistinctiveRingingResponse
    extends OCIDataResponse
{

    protected boolean distinctiveRingingCallCenterCalls;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected RingPattern distinctiveRingingRingPatternForCallCenter;
    @XmlSchemaType(name = "token")
    protected RingPattern distinctiveRingingForceDeliveryRingPattern;

    /**
     * Ruft den Wert der distinctiveRingingCallCenterCalls-Eigenschaft ab.
     * 
     */
    public boolean isDistinctiveRingingCallCenterCalls() {
        return distinctiveRingingCallCenterCalls;
    }

    /**
     * Legt den Wert der distinctiveRingingCallCenterCalls-Eigenschaft fest.
     * 
     */
    public void setDistinctiveRingingCallCenterCalls(boolean value) {
        this.distinctiveRingingCallCenterCalls = value;
    }

    /**
     * Ruft den Wert der distinctiveRingingRingPatternForCallCenter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RingPattern }
     *     
     */
    public RingPattern getDistinctiveRingingRingPatternForCallCenter() {
        return distinctiveRingingRingPatternForCallCenter;
    }

    /**
     * Legt den Wert der distinctiveRingingRingPatternForCallCenter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RingPattern }
     *     
     */
    public void setDistinctiveRingingRingPatternForCallCenter(RingPattern value) {
        this.distinctiveRingingRingPatternForCallCenter = value;
    }

    /**
     * Ruft den Wert der distinctiveRingingForceDeliveryRingPattern-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RingPattern }
     *     
     */
    public RingPattern getDistinctiveRingingForceDeliveryRingPattern() {
        return distinctiveRingingForceDeliveryRingPattern;
    }

    /**
     * Legt den Wert der distinctiveRingingForceDeliveryRingPattern-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RingPattern }
     *     
     */
    public void setDistinctiveRingingForceDeliveryRingPattern(RingPattern value) {
        this.distinctiveRingingForceDeliveryRingPattern = value;
    }

}
