//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupFlexibleSeatingHostRoutingPoliciesGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupFlexibleSeatingHostRoutingPoliciesGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupFlexibleSeatingHostRoutingPoliciesGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="allowEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowCallsToVoicePortal" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupFlexibleSeatingHostRoutingPoliciesGetResponse", propOrder = {
    "allowEmergencyCalls",
    "allowCallsToVoicePortal"
})
public class GroupFlexibleSeatingHostRoutingPoliciesGetResponse
    extends OCIDataResponse
{

    protected boolean allowEmergencyCalls;
    protected boolean allowCallsToVoicePortal;

    /**
     * Ruft den Wert der allowEmergencyCalls-Eigenschaft ab.
     * 
     */
    public boolean isAllowEmergencyCalls() {
        return allowEmergencyCalls;
    }

    /**
     * Legt den Wert der allowEmergencyCalls-Eigenschaft fest.
     * 
     */
    public void setAllowEmergencyCalls(boolean value) {
        this.allowEmergencyCalls = value;
    }

    /**
     * Ruft den Wert der allowCallsToVoicePortal-Eigenschaft ab.
     * 
     */
    public boolean isAllowCallsToVoicePortal() {
        return allowCallsToVoicePortal;
    }

    /**
     * Legt den Wert der allowCallsToVoicePortal-Eigenschaft fest.
     * 
     */
    public void setAllowCallsToVoicePortal(boolean value) {
        this.allowCallsToVoicePortal = value;
    }

}
