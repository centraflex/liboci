//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the system level data associated with Call Center.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         The following elements are only used in AS data mode and ignored in Amplify data mode:
 *           callHandlingSamplingPeriodMinutes
 *           callHandlingMinimumSamplingSize
 *           thresholdCrossingNotificationEmailGuardTimerSeconds
 *       
 * 
 * <p>Java-Klasse für SystemCallCenterModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallCenterModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="defaultFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="statisticsSamplingPeriodMinutes" type="{}CallCenterStatisticsSamplingPeriodMinutes" minOccurs="0"/>
 *         <element name="defaultEnableGuardTimer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="defaultGuardTimerSeconds" type="{}CallCenterGuardTimerSeconds" minOccurs="0"/>
 *         <element name="forceAgentUnavailableOnDNDActivation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forceAgentUnavailableOnPersonalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forceAgentUnavailableOnBouncedCallLimit" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="numberConsecutiveBouncedCallsToForceAgentUnavailable" type="{}CallCenterConsecutiveBouncedCallsToForceAgentUnavailable" minOccurs="0"/>
 *         <element name="forceAgentUnavailableOnNotReachable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="defaultPlayRingWhenOfferCall" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="uniformCallDistributionPolicyScope" type="{}CallCenterUniformCallDistributionPolicyScope" minOccurs="0"/>
 *         <element name="callHandlingSamplingPeriodMinutes" type="{}CallHandlingSamplingPeriodMinutes" minOccurs="0"/>
 *         <element name="callHandlingMinimumSamplingSize" type="{}CallHandlingMinimumSamplingSize" minOccurs="0"/>
 *         <element name="playToneToAgentForEmergencyCall" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="emergencyCallCLIDPrefix" type="{}CallCenterEmergencyCallCLIDPrefix" minOccurs="0"/>
 *         <element name="thresholdCrossingNotificationEmailGuardTimerSeconds" type="{}CallCenterThresholdCrossingNotificationEmailGuardTimerSeconds" minOccurs="0"/>
 *         <element name="allowAgentDeviceInitiatedForward" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallCenterModifyRequest", propOrder = {
    "defaultFromAddress",
    "statisticsSamplingPeriodMinutes",
    "defaultEnableGuardTimer",
    "defaultGuardTimerSeconds",
    "forceAgentUnavailableOnDNDActivation",
    "forceAgentUnavailableOnPersonalCalls",
    "forceAgentUnavailableOnBouncedCallLimit",
    "numberConsecutiveBouncedCallsToForceAgentUnavailable",
    "forceAgentUnavailableOnNotReachable",
    "defaultPlayRingWhenOfferCall",
    "uniformCallDistributionPolicyScope",
    "callHandlingSamplingPeriodMinutes",
    "callHandlingMinimumSamplingSize",
    "playToneToAgentForEmergencyCall",
    "emergencyCallCLIDPrefix",
    "thresholdCrossingNotificationEmailGuardTimerSeconds",
    "allowAgentDeviceInitiatedForward"
})
public class SystemCallCenterModifyRequest
    extends OCIRequest
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultFromAddress;
    protected Integer statisticsSamplingPeriodMinutes;
    protected Boolean defaultEnableGuardTimer;
    protected Integer defaultGuardTimerSeconds;
    protected Boolean forceAgentUnavailableOnDNDActivation;
    protected Boolean forceAgentUnavailableOnPersonalCalls;
    protected Boolean forceAgentUnavailableOnBouncedCallLimit;
    protected Integer numberConsecutiveBouncedCallsToForceAgentUnavailable;
    protected Boolean forceAgentUnavailableOnNotReachable;
    protected Boolean defaultPlayRingWhenOfferCall;
    @XmlSchemaType(name = "token")
    protected CallCenterUniformCallDistributionPolicyScope uniformCallDistributionPolicyScope;
    protected Integer callHandlingSamplingPeriodMinutes;
    protected Integer callHandlingMinimumSamplingSize;
    protected Boolean playToneToAgentForEmergencyCall;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String emergencyCallCLIDPrefix;
    protected Integer thresholdCrossingNotificationEmailGuardTimerSeconds;
    protected Boolean allowAgentDeviceInitiatedForward;

    /**
     * Ruft den Wert der defaultFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultFromAddress() {
        return defaultFromAddress;
    }

    /**
     * Legt den Wert der defaultFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultFromAddress(String value) {
        this.defaultFromAddress = value;
    }

    /**
     * Ruft den Wert der statisticsSamplingPeriodMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStatisticsSamplingPeriodMinutes() {
        return statisticsSamplingPeriodMinutes;
    }

    /**
     * Legt den Wert der statisticsSamplingPeriodMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStatisticsSamplingPeriodMinutes(Integer value) {
        this.statisticsSamplingPeriodMinutes = value;
    }

    /**
     * Ruft den Wert der defaultEnableGuardTimer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefaultEnableGuardTimer() {
        return defaultEnableGuardTimer;
    }

    /**
     * Legt den Wert der defaultEnableGuardTimer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultEnableGuardTimer(Boolean value) {
        this.defaultEnableGuardTimer = value;
    }

    /**
     * Ruft den Wert der defaultGuardTimerSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDefaultGuardTimerSeconds() {
        return defaultGuardTimerSeconds;
    }

    /**
     * Legt den Wert der defaultGuardTimerSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDefaultGuardTimerSeconds(Integer value) {
        this.defaultGuardTimerSeconds = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnDNDActivation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceAgentUnavailableOnDNDActivation() {
        return forceAgentUnavailableOnDNDActivation;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnDNDActivation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceAgentUnavailableOnDNDActivation(Boolean value) {
        this.forceAgentUnavailableOnDNDActivation = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnPersonalCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceAgentUnavailableOnPersonalCalls() {
        return forceAgentUnavailableOnPersonalCalls;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnPersonalCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceAgentUnavailableOnPersonalCalls(Boolean value) {
        this.forceAgentUnavailableOnPersonalCalls = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnBouncedCallLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceAgentUnavailableOnBouncedCallLimit() {
        return forceAgentUnavailableOnBouncedCallLimit;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnBouncedCallLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceAgentUnavailableOnBouncedCallLimit(Boolean value) {
        this.forceAgentUnavailableOnBouncedCallLimit = value;
    }

    /**
     * Ruft den Wert der numberConsecutiveBouncedCallsToForceAgentUnavailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberConsecutiveBouncedCallsToForceAgentUnavailable() {
        return numberConsecutiveBouncedCallsToForceAgentUnavailable;
    }

    /**
     * Legt den Wert der numberConsecutiveBouncedCallsToForceAgentUnavailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberConsecutiveBouncedCallsToForceAgentUnavailable(Integer value) {
        this.numberConsecutiveBouncedCallsToForceAgentUnavailable = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnNotReachable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceAgentUnavailableOnNotReachable() {
        return forceAgentUnavailableOnNotReachable;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnNotReachable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceAgentUnavailableOnNotReachable(Boolean value) {
        this.forceAgentUnavailableOnNotReachable = value;
    }

    /**
     * Ruft den Wert der defaultPlayRingWhenOfferCall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefaultPlayRingWhenOfferCall() {
        return defaultPlayRingWhenOfferCall;
    }

    /**
     * Legt den Wert der defaultPlayRingWhenOfferCall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultPlayRingWhenOfferCall(Boolean value) {
        this.defaultPlayRingWhenOfferCall = value;
    }

    /**
     * Ruft den Wert der uniformCallDistributionPolicyScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterUniformCallDistributionPolicyScope }
     *     
     */
    public CallCenterUniformCallDistributionPolicyScope getUniformCallDistributionPolicyScope() {
        return uniformCallDistributionPolicyScope;
    }

    /**
     * Legt den Wert der uniformCallDistributionPolicyScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterUniformCallDistributionPolicyScope }
     *     
     */
    public void setUniformCallDistributionPolicyScope(CallCenterUniformCallDistributionPolicyScope value) {
        this.uniformCallDistributionPolicyScope = value;
    }

    /**
     * Ruft den Wert der callHandlingSamplingPeriodMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCallHandlingSamplingPeriodMinutes() {
        return callHandlingSamplingPeriodMinutes;
    }

    /**
     * Legt den Wert der callHandlingSamplingPeriodMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCallHandlingSamplingPeriodMinutes(Integer value) {
        this.callHandlingSamplingPeriodMinutes = value;
    }

    /**
     * Ruft den Wert der callHandlingMinimumSamplingSize-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCallHandlingMinimumSamplingSize() {
        return callHandlingMinimumSamplingSize;
    }

    /**
     * Legt den Wert der callHandlingMinimumSamplingSize-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCallHandlingMinimumSamplingSize(Integer value) {
        this.callHandlingMinimumSamplingSize = value;
    }

    /**
     * Ruft den Wert der playToneToAgentForEmergencyCall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayToneToAgentForEmergencyCall() {
        return playToneToAgentForEmergencyCall;
    }

    /**
     * Legt den Wert der playToneToAgentForEmergencyCall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayToneToAgentForEmergencyCall(Boolean value) {
        this.playToneToAgentForEmergencyCall = value;
    }

    /**
     * Ruft den Wert der emergencyCallCLIDPrefix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmergencyCallCLIDPrefix() {
        return emergencyCallCLIDPrefix;
    }

    /**
     * Legt den Wert der emergencyCallCLIDPrefix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmergencyCallCLIDPrefix(String value) {
        this.emergencyCallCLIDPrefix = value;
    }

    /**
     * Ruft den Wert der thresholdCrossingNotificationEmailGuardTimerSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdCrossingNotificationEmailGuardTimerSeconds() {
        return thresholdCrossingNotificationEmailGuardTimerSeconds;
    }

    /**
     * Legt den Wert der thresholdCrossingNotificationEmailGuardTimerSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdCrossingNotificationEmailGuardTimerSeconds(Integer value) {
        this.thresholdCrossingNotificationEmailGuardTimerSeconds = value;
    }

    /**
     * Ruft den Wert der allowAgentDeviceInitiatedForward-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowAgentDeviceInitiatedForward() {
        return allowAgentDeviceInitiatedForward;
    }

    /**
     * Legt den Wert der allowAgentDeviceInitiatedForward-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowAgentDeviceInitiatedForward(Boolean value) {
        this.allowAgentDeviceInitiatedForward = value;
    }

}
