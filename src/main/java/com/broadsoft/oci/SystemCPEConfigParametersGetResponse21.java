//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCPEConfigParametersGetRequest21.
 *         Contains a list of system CPE Config parameters.
 *         
 *         The following elements are only used in AS data mode:
 *           allowDeviceCredentialsRetrieval, value "false" is returned in XS data mode
 *       
 * 
 * <p>Java-Klasse für SystemCPEConfigParametersGetResponse21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCPEConfigParametersGetResponse21">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableIPDeviceManagement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="ftpConnectTimeoutSeconds" type="{}DeviceManagementFTPConnectTimeoutSeconds"/>
 *         <element name="ftpFileTransferTimeoutSeconds" type="{}DeviceManagementFTPFileTransferTimeoutSeconds"/>
 *         <element name="pauseBetweenFileRebuildMilliseconds" type="{}DeviceManagementPauseBetweenFileRebuildMilliseconds"/>
 *         <element name="deviceAccessAppServerClusterName" type="{}NetAddress" minOccurs="0"/>
 *         <element name="minTimeBetweenResetMilliseconds" type="{}DeviceManagementMinTimeBetweenResetMilliseconds"/>
 *         <element name="alwaysPushFilesOnRebuild" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxFileOperationRetryAttempts" type="{}DeviceManagementFileOperationRetryAttempts"/>
 *         <element name="enableAutoRebuildConfig" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="eventQueueSize" type="{}DeviceManagementEventQueueSize"/>
 *         <element name="allowDeviceCredentialsRetrieval" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCPEConfigParametersGetResponse21", propOrder = {
    "enableIPDeviceManagement",
    "ftpConnectTimeoutSeconds",
    "ftpFileTransferTimeoutSeconds",
    "pauseBetweenFileRebuildMilliseconds",
    "deviceAccessAppServerClusterName",
    "minTimeBetweenResetMilliseconds",
    "alwaysPushFilesOnRebuild",
    "maxFileOperationRetryAttempts",
    "enableAutoRebuildConfig",
    "eventQueueSize",
    "allowDeviceCredentialsRetrieval"
})
public class SystemCPEConfigParametersGetResponse21
    extends OCIDataResponse
{

    protected boolean enableIPDeviceManagement;
    protected int ftpConnectTimeoutSeconds;
    protected int ftpFileTransferTimeoutSeconds;
    protected int pauseBetweenFileRebuildMilliseconds;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceAccessAppServerClusterName;
    protected int minTimeBetweenResetMilliseconds;
    protected boolean alwaysPushFilesOnRebuild;
    protected int maxFileOperationRetryAttempts;
    protected boolean enableAutoRebuildConfig;
    protected int eventQueueSize;
    protected boolean allowDeviceCredentialsRetrieval;

    /**
     * Ruft den Wert der enableIPDeviceManagement-Eigenschaft ab.
     * 
     */
    public boolean isEnableIPDeviceManagement() {
        return enableIPDeviceManagement;
    }

    /**
     * Legt den Wert der enableIPDeviceManagement-Eigenschaft fest.
     * 
     */
    public void setEnableIPDeviceManagement(boolean value) {
        this.enableIPDeviceManagement = value;
    }

    /**
     * Ruft den Wert der ftpConnectTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getFtpConnectTimeoutSeconds() {
        return ftpConnectTimeoutSeconds;
    }

    /**
     * Legt den Wert der ftpConnectTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setFtpConnectTimeoutSeconds(int value) {
        this.ftpConnectTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der ftpFileTransferTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getFtpFileTransferTimeoutSeconds() {
        return ftpFileTransferTimeoutSeconds;
    }

    /**
     * Legt den Wert der ftpFileTransferTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setFtpFileTransferTimeoutSeconds(int value) {
        this.ftpFileTransferTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der pauseBetweenFileRebuildMilliseconds-Eigenschaft ab.
     * 
     */
    public int getPauseBetweenFileRebuildMilliseconds() {
        return pauseBetweenFileRebuildMilliseconds;
    }

    /**
     * Legt den Wert der pauseBetweenFileRebuildMilliseconds-Eigenschaft fest.
     * 
     */
    public void setPauseBetweenFileRebuildMilliseconds(int value) {
        this.pauseBetweenFileRebuildMilliseconds = value;
    }

    /**
     * Ruft den Wert der deviceAccessAppServerClusterName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceAccessAppServerClusterName() {
        return deviceAccessAppServerClusterName;
    }

    /**
     * Legt den Wert der deviceAccessAppServerClusterName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceAccessAppServerClusterName(String value) {
        this.deviceAccessAppServerClusterName = value;
    }

    /**
     * Ruft den Wert der minTimeBetweenResetMilliseconds-Eigenschaft ab.
     * 
     */
    public int getMinTimeBetweenResetMilliseconds() {
        return minTimeBetweenResetMilliseconds;
    }

    /**
     * Legt den Wert der minTimeBetweenResetMilliseconds-Eigenschaft fest.
     * 
     */
    public void setMinTimeBetweenResetMilliseconds(int value) {
        this.minTimeBetweenResetMilliseconds = value;
    }

    /**
     * Ruft den Wert der alwaysPushFilesOnRebuild-Eigenschaft ab.
     * 
     */
    public boolean isAlwaysPushFilesOnRebuild() {
        return alwaysPushFilesOnRebuild;
    }

    /**
     * Legt den Wert der alwaysPushFilesOnRebuild-Eigenschaft fest.
     * 
     */
    public void setAlwaysPushFilesOnRebuild(boolean value) {
        this.alwaysPushFilesOnRebuild = value;
    }

    /**
     * Ruft den Wert der maxFileOperationRetryAttempts-Eigenschaft ab.
     * 
     */
    public int getMaxFileOperationRetryAttempts() {
        return maxFileOperationRetryAttempts;
    }

    /**
     * Legt den Wert der maxFileOperationRetryAttempts-Eigenschaft fest.
     * 
     */
    public void setMaxFileOperationRetryAttempts(int value) {
        this.maxFileOperationRetryAttempts = value;
    }

    /**
     * Ruft den Wert der enableAutoRebuildConfig-Eigenschaft ab.
     * 
     */
    public boolean isEnableAutoRebuildConfig() {
        return enableAutoRebuildConfig;
    }

    /**
     * Legt den Wert der enableAutoRebuildConfig-Eigenschaft fest.
     * 
     */
    public void setEnableAutoRebuildConfig(boolean value) {
        this.enableAutoRebuildConfig = value;
    }

    /**
     * Ruft den Wert der eventQueueSize-Eigenschaft ab.
     * 
     */
    public int getEventQueueSize() {
        return eventQueueSize;
    }

    /**
     * Legt den Wert der eventQueueSize-Eigenschaft fest.
     * 
     */
    public void setEventQueueSize(int value) {
        this.eventQueueSize = value;
    }

    /**
     * Ruft den Wert der allowDeviceCredentialsRetrieval-Eigenschaft ab.
     * 
     */
    public boolean isAllowDeviceCredentialsRetrieval() {
        return allowDeviceCredentialsRetrieval;
    }

    /**
     * Legt den Wert der allowDeviceCredentialsRetrieval-Eigenschaft fest.
     * 
     */
    public void setAllowDeviceCredentialsRetrieval(boolean value) {
        this.allowDeviceCredentialsRetrieval = value;
    }

}
