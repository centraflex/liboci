//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupExtensionLengthGetRequest22.
 *         
 *         The following elements are only used in AS data mode and not returned in XS data mode:
 *            useExterpriseExtensionLengthSetting
 *       
 *       
 * 
 * <p>Java-Klasse für GroupExtensionLengthGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupExtensionLengthGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="minExtensionLength" type="{}ExtensionLength"/>
 *         <element name="maxExtensionLength" type="{}ExtensionLength"/>
 *         <element name="defaultExtensionLength" type="{}ExtensionLength"/>
 *         <element name="useEnterpriseExtensionLengthSetting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupExtensionLengthGetResponse22", propOrder = {
    "minExtensionLength",
    "maxExtensionLength",
    "defaultExtensionLength",
    "useEnterpriseExtensionLengthSetting"
})
public class GroupExtensionLengthGetResponse22
    extends OCIDataResponse
{

    protected int minExtensionLength;
    protected int maxExtensionLength;
    protected int defaultExtensionLength;
    protected Boolean useEnterpriseExtensionLengthSetting;

    /**
     * Ruft den Wert der minExtensionLength-Eigenschaft ab.
     * 
     */
    public int getMinExtensionLength() {
        return minExtensionLength;
    }

    /**
     * Legt den Wert der minExtensionLength-Eigenschaft fest.
     * 
     */
    public void setMinExtensionLength(int value) {
        this.minExtensionLength = value;
    }

    /**
     * Ruft den Wert der maxExtensionLength-Eigenschaft ab.
     * 
     */
    public int getMaxExtensionLength() {
        return maxExtensionLength;
    }

    /**
     * Legt den Wert der maxExtensionLength-Eigenschaft fest.
     * 
     */
    public void setMaxExtensionLength(int value) {
        this.maxExtensionLength = value;
    }

    /**
     * Ruft den Wert der defaultExtensionLength-Eigenschaft ab.
     * 
     */
    public int getDefaultExtensionLength() {
        return defaultExtensionLength;
    }

    /**
     * Legt den Wert der defaultExtensionLength-Eigenschaft fest.
     * 
     */
    public void setDefaultExtensionLength(int value) {
        this.defaultExtensionLength = value;
    }

    /**
     * Ruft den Wert der useEnterpriseExtensionLengthSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseEnterpriseExtensionLengthSetting() {
        return useEnterpriseExtensionLengthSetting;
    }

    /**
     * Legt den Wert der useEnterpriseExtensionLengthSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseEnterpriseExtensionLengthSetting(Boolean value) {
        this.useEnterpriseExtensionLengthSetting = value;
    }

}
