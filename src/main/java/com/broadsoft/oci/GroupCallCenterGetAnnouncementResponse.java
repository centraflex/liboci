//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupCallCenterGetAnnouncementRequest.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterGetAnnouncementResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterGetAnnouncementResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="entranceMessageSelection" type="{}CallCenterAnnouncementSelection"/>
 *         <element name="entranceMessageAudioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="entranceMessageVideoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="periodicComfortMessageSelection" type="{}CallCenterAnnouncementSelection"/>
 *         <element name="periodicComfortMessageAudioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="periodicComfortMessageVideoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="onHoldMessageSelection" type="{}CallCenterAnnouncementSelection"/>
 *         <element name="onHoldMessageAudioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="onHoldMessageVideoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterGetAnnouncementResponse", propOrder = {
    "entranceMessageSelection",
    "entranceMessageAudioFileDescription",
    "entranceMessageVideoFileDescription",
    "periodicComfortMessageSelection",
    "periodicComfortMessageAudioFileDescription",
    "periodicComfortMessageVideoFileDescription",
    "onHoldMessageSelection",
    "onHoldMessageAudioFileDescription",
    "onHoldMessageVideoFileDescription"
})
public class GroupCallCenterGetAnnouncementResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterAnnouncementSelection entranceMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String entranceMessageAudioFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String entranceMessageVideoFileDescription;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterAnnouncementSelection periodicComfortMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String periodicComfortMessageAudioFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String periodicComfortMessageVideoFileDescription;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterAnnouncementSelection onHoldMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String onHoldMessageAudioFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String onHoldMessageVideoFileDescription;

    /**
     * Ruft den Wert der entranceMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public CallCenterAnnouncementSelection getEntranceMessageSelection() {
        return entranceMessageSelection;
    }

    /**
     * Legt den Wert der entranceMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public void setEntranceMessageSelection(CallCenterAnnouncementSelection value) {
        this.entranceMessageSelection = value;
    }

    /**
     * Ruft den Wert der entranceMessageAudioFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntranceMessageAudioFileDescription() {
        return entranceMessageAudioFileDescription;
    }

    /**
     * Legt den Wert der entranceMessageAudioFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntranceMessageAudioFileDescription(String value) {
        this.entranceMessageAudioFileDescription = value;
    }

    /**
     * Ruft den Wert der entranceMessageVideoFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntranceMessageVideoFileDescription() {
        return entranceMessageVideoFileDescription;
    }

    /**
     * Legt den Wert der entranceMessageVideoFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntranceMessageVideoFileDescription(String value) {
        this.entranceMessageVideoFileDescription = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public CallCenterAnnouncementSelection getPeriodicComfortMessageSelection() {
        return periodicComfortMessageSelection;
    }

    /**
     * Legt den Wert der periodicComfortMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public void setPeriodicComfortMessageSelection(CallCenterAnnouncementSelection value) {
        this.periodicComfortMessageSelection = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageAudioFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodicComfortMessageAudioFileDescription() {
        return periodicComfortMessageAudioFileDescription;
    }

    /**
     * Legt den Wert der periodicComfortMessageAudioFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodicComfortMessageAudioFileDescription(String value) {
        this.periodicComfortMessageAudioFileDescription = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageVideoFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodicComfortMessageVideoFileDescription() {
        return periodicComfortMessageVideoFileDescription;
    }

    /**
     * Legt den Wert der periodicComfortMessageVideoFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodicComfortMessageVideoFileDescription(String value) {
        this.periodicComfortMessageVideoFileDescription = value;
    }

    /**
     * Ruft den Wert der onHoldMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public CallCenterAnnouncementSelection getOnHoldMessageSelection() {
        return onHoldMessageSelection;
    }

    /**
     * Legt den Wert der onHoldMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public void setOnHoldMessageSelection(CallCenterAnnouncementSelection value) {
        this.onHoldMessageSelection = value;
    }

    /**
     * Ruft den Wert der onHoldMessageAudioFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOnHoldMessageAudioFileDescription() {
        return onHoldMessageAudioFileDescription;
    }

    /**
     * Legt den Wert der onHoldMessageAudioFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnHoldMessageAudioFileDescription(String value) {
        this.onHoldMessageAudioFileDescription = value;
    }

    /**
     * Ruft den Wert der onHoldMessageVideoFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOnHoldMessageVideoFileDescription() {
        return onHoldMessageVideoFileDescription;
    }

    /**
     * Legt den Wert der onHoldMessageVideoFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnHoldMessageVideoFileDescription(String value) {
        this.onHoldMessageVideoFileDescription = value;
    }

}
