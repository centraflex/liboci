//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal conference greeting menu keys.
 *       
 * 
 * <p>Java-Klasse für ConferenceGreetingMenuKeysReadEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ConferenceGreetingMenuKeysReadEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="activateConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="deactivateConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="recordNewConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="listenToCurrentConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConferenceGreetingMenuKeysReadEntry", propOrder = {
    "activateConfGreeting",
    "deactivateConfGreeting",
    "recordNewConfGreeting",
    "listenToCurrentConfGreeting",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class ConferenceGreetingMenuKeysReadEntry {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String activateConfGreeting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deactivateConfGreeting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String recordNewConfGreeting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String listenToCurrentConfGreeting;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String repeatMenu;

    /**
     * Ruft den Wert der activateConfGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivateConfGreeting() {
        return activateConfGreeting;
    }

    /**
     * Legt den Wert der activateConfGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivateConfGreeting(String value) {
        this.activateConfGreeting = value;
    }

    /**
     * Ruft den Wert der deactivateConfGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeactivateConfGreeting() {
        return deactivateConfGreeting;
    }

    /**
     * Legt den Wert der deactivateConfGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeactivateConfGreeting(String value) {
        this.deactivateConfGreeting = value;
    }

    /**
     * Ruft den Wert der recordNewConfGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordNewConfGreeting() {
        return recordNewConfGreeting;
    }

    /**
     * Legt den Wert der recordNewConfGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordNewConfGreeting(String value) {
        this.recordNewConfGreeting = value;
    }

    /**
     * Ruft den Wert der listenToCurrentConfGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListenToCurrentConfGreeting() {
        return listenToCurrentConfGreeting;
    }

    /**
     * Legt den Wert der listenToCurrentConfGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListenToCurrentConfGreeting(String value) {
        this.listenToCurrentConfGreeting = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepeatMenu(String value) {
        this.repeatMenu = value;
    }

}
