//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Contains the call center media on hold source configuration.
 *       
 * 
 * <p>Java-Klasse für CallCenterMediaOnHoldSourceRead16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterMediaOnHoldSourceRead16">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="audioMessageSourceSelection" type="{}CallCenterMediaOnHoldMessageSelection"/>
 *         <element name="audioFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="audioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="audioFileMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="externalAudioSource" type="{}AccessDeviceEndpointRead14" minOccurs="0"/>
 *         <element name="videoMessageSourceSelection" type="{}CallCenterMediaOnHoldMessageSelection" minOccurs="0"/>
 *         <element name="videoFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="videoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="videoFileMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="externalVideoSource" type="{}AccessDeviceEndpointRead14" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterMediaOnHoldSourceRead16", propOrder = {
    "audioMessageSourceSelection",
    "audioFileUrl",
    "audioFileDescription",
    "audioFileMediaType",
    "externalAudioSource",
    "videoMessageSourceSelection",
    "videoFileUrl",
    "videoFileDescription",
    "videoFileMediaType",
    "externalVideoSource"
})
public class CallCenterMediaOnHoldSourceRead16 {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterMediaOnHoldMessageSelection audioMessageSourceSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioFileUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioFileMediaType;
    protected AccessDeviceEndpointRead14 externalAudioSource;
    @XmlSchemaType(name = "token")
    protected CallCenterMediaOnHoldMessageSelection videoMessageSourceSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String videoFileUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String videoFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String videoFileMediaType;
    protected AccessDeviceEndpointRead14 externalVideoSource;

    /**
     * Ruft den Wert der audioMessageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public CallCenterMediaOnHoldMessageSelection getAudioMessageSourceSelection() {
        return audioMessageSourceSelection;
    }

    /**
     * Legt den Wert der audioMessageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public void setAudioMessageSourceSelection(CallCenterMediaOnHoldMessageSelection value) {
        this.audioMessageSourceSelection = value;
    }

    /**
     * Ruft den Wert der audioFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioFileUrl() {
        return audioFileUrl;
    }

    /**
     * Legt den Wert der audioFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioFileUrl(String value) {
        this.audioFileUrl = value;
    }

    /**
     * Ruft den Wert der audioFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioFileDescription() {
        return audioFileDescription;
    }

    /**
     * Legt den Wert der audioFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioFileDescription(String value) {
        this.audioFileDescription = value;
    }

    /**
     * Ruft den Wert der audioFileMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioFileMediaType() {
        return audioFileMediaType;
    }

    /**
     * Legt den Wert der audioFileMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioFileMediaType(String value) {
        this.audioFileMediaType = value;
    }

    /**
     * Ruft den Wert der externalAudioSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEndpointRead14 }
     *     
     */
    public AccessDeviceEndpointRead14 getExternalAudioSource() {
        return externalAudioSource;
    }

    /**
     * Legt den Wert der externalAudioSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEndpointRead14 }
     *     
     */
    public void setExternalAudioSource(AccessDeviceEndpointRead14 value) {
        this.externalAudioSource = value;
    }

    /**
     * Ruft den Wert der videoMessageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public CallCenterMediaOnHoldMessageSelection getVideoMessageSourceSelection() {
        return videoMessageSourceSelection;
    }

    /**
     * Legt den Wert der videoMessageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public void setVideoMessageSourceSelection(CallCenterMediaOnHoldMessageSelection value) {
        this.videoMessageSourceSelection = value;
    }

    /**
     * Ruft den Wert der videoFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVideoFileUrl() {
        return videoFileUrl;
    }

    /**
     * Legt den Wert der videoFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVideoFileUrl(String value) {
        this.videoFileUrl = value;
    }

    /**
     * Ruft den Wert der videoFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVideoFileDescription() {
        return videoFileDescription;
    }

    /**
     * Legt den Wert der videoFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVideoFileDescription(String value) {
        this.videoFileDescription = value;
    }

    /**
     * Ruft den Wert der videoFileMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVideoFileMediaType() {
        return videoFileMediaType;
    }

    /**
     * Legt den Wert der videoFileMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVideoFileMediaType(String value) {
        this.videoFileMediaType = value;
    }

    /**
     * Ruft den Wert der externalVideoSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEndpointRead14 }
     *     
     */
    public AccessDeviceEndpointRead14 getExternalVideoSource() {
        return externalVideoSource;
    }

    /**
     * Legt den Wert der externalVideoSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEndpointRead14 }
     *     
     */
    public void setExternalVideoSource(AccessDeviceEndpointRead14 value) {
        this.externalVideoSource = value;
    }

}
