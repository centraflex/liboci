//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserPrimaryEndpointAdvancedSettingGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserPrimaryEndpointAdvancedSettingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPrimaryEndpointAdvancedSettingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="allowOrigination" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowTermination" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPrimaryEndpointAdvancedSettingGetResponse", propOrder = {
    "allowOrigination",
    "allowTermination"
})
public class UserPrimaryEndpointAdvancedSettingGetResponse
    extends OCIDataResponse
{

    protected boolean allowOrigination;
    protected boolean allowTermination;

    /**
     * Ruft den Wert der allowOrigination-Eigenschaft ab.
     * 
     */
    public boolean isAllowOrigination() {
        return allowOrigination;
    }

    /**
     * Legt den Wert der allowOrigination-Eigenschaft fest.
     * 
     */
    public void setAllowOrigination(boolean value) {
        this.allowOrigination = value;
    }

    /**
     * Ruft den Wert der allowTermination-Eigenschaft ab.
     * 
     */
    public boolean isAllowTermination() {
        return allowTermination;
    }

    /**
     * Legt den Wert der allowTermination-Eigenschaft fest.
     * 
     */
    public void setAllowTermination(boolean value) {
        this.allowTermination = value;
    }

}
