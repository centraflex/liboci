//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the system level data associated with Meet-Me Conferencing.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemMeetMeConferencingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemMeetMeConferencingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="conferenceIdLength" type="{}MeetMeConferencingConferencePassCodeLength" minOccurs="0"/>
 *         <element name="moderatorPinLength" type="{}MeetMeConferencingConferencePassCodeLength" minOccurs="0"/>
 *         <element name="enableConferenceEndDateRestriction" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="conferenceEndDateRestrictionMonths" type="{}MeetMeConferencingConferenceEndDateRestrictionMonths" minOccurs="0"/>
 *         <element name="deleteExpiredConferencesAfterHoldPeriod" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="expiredConferenceHoldPeriodDays" type="{}MeetMeConferencingExpiredConferenceHoldPeriodDays" minOccurs="0"/>
 *         <element name="recordingWebAppURL" type="{}URL" minOccurs="0"/>
 *         <element name="recordingFileFormat" type="{}MeetMeConferencingRecordingFileFormat" minOccurs="0"/>
 *         <element name="terminateAfterGracePeriod" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="conferenceGracePeriodMinutes" type="{}MeetMeConferencingConferenceDuration" minOccurs="0"/>
 *         <element name="conferenceParticipantEarlyEntryMinutes" type="{}MeetMeConferencingParticipantEarlyEntryMinutes" minOccurs="0"/>
 *         <element name="enableConferenceExpiryNotification" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableActiveConferenceNotification" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="conferenceFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="conferenceActiveTalkerRefreshIntervalSeconds" type="{}MeetMeConferencingActiveTalkerRefreshIntervalSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemMeetMeConferencingModifyRequest", propOrder = {
    "conferenceIdLength",
    "moderatorPinLength",
    "enableConferenceEndDateRestriction",
    "conferenceEndDateRestrictionMonths",
    "deleteExpiredConferencesAfterHoldPeriod",
    "expiredConferenceHoldPeriodDays",
    "recordingWebAppURL",
    "recordingFileFormat",
    "terminateAfterGracePeriod",
    "conferenceGracePeriodMinutes",
    "conferenceParticipantEarlyEntryMinutes",
    "enableConferenceExpiryNotification",
    "enableActiveConferenceNotification",
    "conferenceFromAddress",
    "conferenceActiveTalkerRefreshIntervalSeconds"
})
public class SystemMeetMeConferencingModifyRequest
    extends OCIRequest
{

    protected Integer conferenceIdLength;
    protected Integer moderatorPinLength;
    protected Boolean enableConferenceEndDateRestriction;
    protected Integer conferenceEndDateRestrictionMonths;
    protected Boolean deleteExpiredConferencesAfterHoldPeriod;
    protected Integer expiredConferenceHoldPeriodDays;
    @XmlElementRef(name = "recordingWebAppURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recordingWebAppURL;
    @XmlSchemaType(name = "token")
    protected MeetMeConferencingRecordingFileFormat recordingFileFormat;
    protected Boolean terminateAfterGracePeriod;
    protected MeetMeConferencingConferenceDuration conferenceGracePeriodMinutes;
    protected Integer conferenceParticipantEarlyEntryMinutes;
    protected Boolean enableConferenceExpiryNotification;
    protected Boolean enableActiveConferenceNotification;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String conferenceFromAddress;
    protected Integer conferenceActiveTalkerRefreshIntervalSeconds;

    /**
     * Ruft den Wert der conferenceIdLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getConferenceIdLength() {
        return conferenceIdLength;
    }

    /**
     * Legt den Wert der conferenceIdLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setConferenceIdLength(Integer value) {
        this.conferenceIdLength = value;
    }

    /**
     * Ruft den Wert der moderatorPinLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getModeratorPinLength() {
        return moderatorPinLength;
    }

    /**
     * Legt den Wert der moderatorPinLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setModeratorPinLength(Integer value) {
        this.moderatorPinLength = value;
    }

    /**
     * Ruft den Wert der enableConferenceEndDateRestriction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableConferenceEndDateRestriction() {
        return enableConferenceEndDateRestriction;
    }

    /**
     * Legt den Wert der enableConferenceEndDateRestriction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableConferenceEndDateRestriction(Boolean value) {
        this.enableConferenceEndDateRestriction = value;
    }

    /**
     * Ruft den Wert der conferenceEndDateRestrictionMonths-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getConferenceEndDateRestrictionMonths() {
        return conferenceEndDateRestrictionMonths;
    }

    /**
     * Legt den Wert der conferenceEndDateRestrictionMonths-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setConferenceEndDateRestrictionMonths(Integer value) {
        this.conferenceEndDateRestrictionMonths = value;
    }

    /**
     * Ruft den Wert der deleteExpiredConferencesAfterHoldPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeleteExpiredConferencesAfterHoldPeriod() {
        return deleteExpiredConferencesAfterHoldPeriod;
    }

    /**
     * Legt den Wert der deleteExpiredConferencesAfterHoldPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeleteExpiredConferencesAfterHoldPeriod(Boolean value) {
        this.deleteExpiredConferencesAfterHoldPeriod = value;
    }

    /**
     * Ruft den Wert der expiredConferenceHoldPeriodDays-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExpiredConferenceHoldPeriodDays() {
        return expiredConferenceHoldPeriodDays;
    }

    /**
     * Legt den Wert der expiredConferenceHoldPeriodDays-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExpiredConferenceHoldPeriodDays(Integer value) {
        this.expiredConferenceHoldPeriodDays = value;
    }

    /**
     * Ruft den Wert der recordingWebAppURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecordingWebAppURL() {
        return recordingWebAppURL;
    }

    /**
     * Legt den Wert der recordingWebAppURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecordingWebAppURL(JAXBElement<String> value) {
        this.recordingWebAppURL = value;
    }

    /**
     * Ruft den Wert der recordingFileFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingRecordingFileFormat }
     *     
     */
    public MeetMeConferencingRecordingFileFormat getRecordingFileFormat() {
        return recordingFileFormat;
    }

    /**
     * Legt den Wert der recordingFileFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingRecordingFileFormat }
     *     
     */
    public void setRecordingFileFormat(MeetMeConferencingRecordingFileFormat value) {
        this.recordingFileFormat = value;
    }

    /**
     * Ruft den Wert der terminateAfterGracePeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTerminateAfterGracePeriod() {
        return terminateAfterGracePeriod;
    }

    /**
     * Legt den Wert der terminateAfterGracePeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTerminateAfterGracePeriod(Boolean value) {
        this.terminateAfterGracePeriod = value;
    }

    /**
     * Ruft den Wert der conferenceGracePeriodMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferenceDuration }
     *     
     */
    public MeetMeConferencingConferenceDuration getConferenceGracePeriodMinutes() {
        return conferenceGracePeriodMinutes;
    }

    /**
     * Legt den Wert der conferenceGracePeriodMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferenceDuration }
     *     
     */
    public void setConferenceGracePeriodMinutes(MeetMeConferencingConferenceDuration value) {
        this.conferenceGracePeriodMinutes = value;
    }

    /**
     * Ruft den Wert der conferenceParticipantEarlyEntryMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getConferenceParticipantEarlyEntryMinutes() {
        return conferenceParticipantEarlyEntryMinutes;
    }

    /**
     * Legt den Wert der conferenceParticipantEarlyEntryMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setConferenceParticipantEarlyEntryMinutes(Integer value) {
        this.conferenceParticipantEarlyEntryMinutes = value;
    }

    /**
     * Ruft den Wert der enableConferenceExpiryNotification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableConferenceExpiryNotification() {
        return enableConferenceExpiryNotification;
    }

    /**
     * Legt den Wert der enableConferenceExpiryNotification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableConferenceExpiryNotification(Boolean value) {
        this.enableConferenceExpiryNotification = value;
    }

    /**
     * Ruft den Wert der enableActiveConferenceNotification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableActiveConferenceNotification() {
        return enableActiveConferenceNotification;
    }

    /**
     * Legt den Wert der enableActiveConferenceNotification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableActiveConferenceNotification(Boolean value) {
        this.enableActiveConferenceNotification = value;
    }

    /**
     * Ruft den Wert der conferenceFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConferenceFromAddress() {
        return conferenceFromAddress;
    }

    /**
     * Legt den Wert der conferenceFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConferenceFromAddress(String value) {
        this.conferenceFromAddress = value;
    }

    /**
     * Ruft den Wert der conferenceActiveTalkerRefreshIntervalSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getConferenceActiveTalkerRefreshIntervalSeconds() {
        return conferenceActiveTalkerRefreshIntervalSeconds;
    }

    /**
     * Legt den Wert der conferenceActiveTalkerRefreshIntervalSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setConferenceActiveTalkerRefreshIntervalSeconds(Integer value) {
        this.conferenceActiveTalkerRefreshIntervalSeconds = value;
    }

}
