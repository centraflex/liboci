//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupCallCenterGetDNISListResponse.
 *         Contains a table with column headings: "Name", "Phone Number", "Extension", "Priority", "Is Primary DNIS".
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterGetDNISListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterGetDNISListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="displayDNISNumber" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="displayDNISName" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="promoteCallsFromPriority1to0" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="promoteCallsFromPriority2to1" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="promoteCallsFromPriority3to2" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="promoteCallsFromPriority1to0Seconds" type="{}DNISPromoteCallPrioritySeconds"/>
 *         <element name="promoteCallsFromPriority2to1Seconds" type="{}DNISPromoteCallPrioritySeconds"/>
 *         <element name="promoteCallsFromPriority3to2Seconds" type="{}DNISPromoteCallPrioritySeconds"/>
 *         <element name="dnisTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterGetDNISListResponse", propOrder = {
    "displayDNISNumber",
    "displayDNISName",
    "promoteCallsFromPriority1To0",
    "promoteCallsFromPriority2To1",
    "promoteCallsFromPriority3To2",
    "promoteCallsFromPriority1To0Seconds",
    "promoteCallsFromPriority2To1Seconds",
    "promoteCallsFromPriority3To2Seconds",
    "dnisTable"
})
public class GroupCallCenterGetDNISListResponse
    extends OCIDataResponse
{

    protected boolean displayDNISNumber;
    protected boolean displayDNISName;
    @XmlElement(name = "promoteCallsFromPriority1to0")
    protected boolean promoteCallsFromPriority1To0;
    @XmlElement(name = "promoteCallsFromPriority2to1")
    protected boolean promoteCallsFromPriority2To1;
    @XmlElement(name = "promoteCallsFromPriority3to2")
    protected boolean promoteCallsFromPriority3To2;
    @XmlElement(name = "promoteCallsFromPriority1to0Seconds")
    protected int promoteCallsFromPriority1To0Seconds;
    @XmlElement(name = "promoteCallsFromPriority2to1Seconds")
    protected int promoteCallsFromPriority2To1Seconds;
    @XmlElement(name = "promoteCallsFromPriority3to2Seconds")
    protected int promoteCallsFromPriority3To2Seconds;
    @XmlElement(required = true)
    protected OCITable dnisTable;

    /**
     * Ruft den Wert der displayDNISNumber-Eigenschaft ab.
     * 
     */
    public boolean isDisplayDNISNumber() {
        return displayDNISNumber;
    }

    /**
     * Legt den Wert der displayDNISNumber-Eigenschaft fest.
     * 
     */
    public void setDisplayDNISNumber(boolean value) {
        this.displayDNISNumber = value;
    }

    /**
     * Ruft den Wert der displayDNISName-Eigenschaft ab.
     * 
     */
    public boolean isDisplayDNISName() {
        return displayDNISName;
    }

    /**
     * Legt den Wert der displayDNISName-Eigenschaft fest.
     * 
     */
    public void setDisplayDNISName(boolean value) {
        this.displayDNISName = value;
    }

    /**
     * Ruft den Wert der promoteCallsFromPriority1To0-Eigenschaft ab.
     * 
     */
    public boolean isPromoteCallsFromPriority1To0() {
        return promoteCallsFromPriority1To0;
    }

    /**
     * Legt den Wert der promoteCallsFromPriority1To0-Eigenschaft fest.
     * 
     */
    public void setPromoteCallsFromPriority1To0(boolean value) {
        this.promoteCallsFromPriority1To0 = value;
    }

    /**
     * Ruft den Wert der promoteCallsFromPriority2To1-Eigenschaft ab.
     * 
     */
    public boolean isPromoteCallsFromPriority2To1() {
        return promoteCallsFromPriority2To1;
    }

    /**
     * Legt den Wert der promoteCallsFromPriority2To1-Eigenschaft fest.
     * 
     */
    public void setPromoteCallsFromPriority2To1(boolean value) {
        this.promoteCallsFromPriority2To1 = value;
    }

    /**
     * Ruft den Wert der promoteCallsFromPriority3To2-Eigenschaft ab.
     * 
     */
    public boolean isPromoteCallsFromPriority3To2() {
        return promoteCallsFromPriority3To2;
    }

    /**
     * Legt den Wert der promoteCallsFromPriority3To2-Eigenschaft fest.
     * 
     */
    public void setPromoteCallsFromPriority3To2(boolean value) {
        this.promoteCallsFromPriority3To2 = value;
    }

    /**
     * Ruft den Wert der promoteCallsFromPriority1To0Seconds-Eigenschaft ab.
     * 
     */
    public int getPromoteCallsFromPriority1To0Seconds() {
        return promoteCallsFromPriority1To0Seconds;
    }

    /**
     * Legt den Wert der promoteCallsFromPriority1To0Seconds-Eigenschaft fest.
     * 
     */
    public void setPromoteCallsFromPriority1To0Seconds(int value) {
        this.promoteCallsFromPriority1To0Seconds = value;
    }

    /**
     * Ruft den Wert der promoteCallsFromPriority2To1Seconds-Eigenschaft ab.
     * 
     */
    public int getPromoteCallsFromPriority2To1Seconds() {
        return promoteCallsFromPriority2To1Seconds;
    }

    /**
     * Legt den Wert der promoteCallsFromPriority2To1Seconds-Eigenschaft fest.
     * 
     */
    public void setPromoteCallsFromPriority2To1Seconds(int value) {
        this.promoteCallsFromPriority2To1Seconds = value;
    }

    /**
     * Ruft den Wert der promoteCallsFromPriority3To2Seconds-Eigenschaft ab.
     * 
     */
    public int getPromoteCallsFromPriority3To2Seconds() {
        return promoteCallsFromPriority3To2Seconds;
    }

    /**
     * Legt den Wert der promoteCallsFromPriority3To2Seconds-Eigenschaft fest.
     * 
     */
    public void setPromoteCallsFromPriority3To2Seconds(int value) {
        this.promoteCallsFromPriority3To2Seconds = value;
    }

    /**
     * Ruft den Wert der dnisTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDnisTable() {
        return dnisTable;
    }

    /**
     * Legt den Wert der dnisTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDnisTable(OCITable value) {
        this.dnisTable = value;
    }

}
