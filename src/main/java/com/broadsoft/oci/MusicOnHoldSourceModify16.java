//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains the music on hold source configuration.
 *       
 * 
 * <p>Java-Klasse für MusicOnHoldSourceModify16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="MusicOnHoldSourceModify16">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="audioFilePreferredCodec" type="{}AudioFileCodec" minOccurs="0"/>
 *         <element name="messageSourceSelection" type="{}MusicOnHoldMessageSelection" minOccurs="0"/>
 *         <element name="customSource" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="audioFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *                   <element name="videoFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="externalSource" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointModify"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MusicOnHoldSourceModify16", propOrder = {
    "audioFilePreferredCodec",
    "messageSourceSelection",
    "customSource",
    "externalSource"
})
public class MusicOnHoldSourceModify16 {

    @XmlSchemaType(name = "token")
    protected AudioFileCodec audioFilePreferredCodec;
    @XmlSchemaType(name = "token")
    protected MusicOnHoldMessageSelection messageSourceSelection;
    protected MusicOnHoldSourceModify16 .CustomSource customSource;
    protected MusicOnHoldSourceModify16 .ExternalSource externalSource;

    /**
     * Ruft den Wert der audioFilePreferredCodec-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AudioFileCodec }
     *     
     */
    public AudioFileCodec getAudioFilePreferredCodec() {
        return audioFilePreferredCodec;
    }

    /**
     * Legt den Wert der audioFilePreferredCodec-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AudioFileCodec }
     *     
     */
    public void setAudioFilePreferredCodec(AudioFileCodec value) {
        this.audioFilePreferredCodec = value;
    }

    /**
     * Ruft den Wert der messageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public MusicOnHoldMessageSelection getMessageSourceSelection() {
        return messageSourceSelection;
    }

    /**
     * Legt den Wert der messageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public void setMessageSourceSelection(MusicOnHoldMessageSelection value) {
        this.messageSourceSelection = value;
    }

    /**
     * Ruft den Wert der customSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceModify16 .CustomSource }
     *     
     */
    public MusicOnHoldSourceModify16 .CustomSource getCustomSource() {
        return customSource;
    }

    /**
     * Legt den Wert der customSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceModify16 .CustomSource }
     *     
     */
    public void setCustomSource(MusicOnHoldSourceModify16 .CustomSource value) {
        this.customSource = value;
    }

    /**
     * Ruft den Wert der externalSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceModify16 .ExternalSource }
     *     
     */
    public MusicOnHoldSourceModify16 .ExternalSource getExternalSource() {
        return externalSource;
    }

    /**
     * Legt den Wert der externalSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceModify16 .ExternalSource }
     *     
     */
    public void setExternalSource(MusicOnHoldSourceModify16 .ExternalSource value) {
        this.externalSource = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="audioFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
     *         <element name="videoFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "audioFile",
        "videoFile"
    })
    public static class CustomSource {

        protected LabeledMediaFileResource audioFile;
        protected LabeledMediaFileResource videoFile;

        /**
         * Ruft den Wert der audioFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public LabeledMediaFileResource getAudioFile() {
            return audioFile;
        }

        /**
         * Legt den Wert der audioFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public void setAudioFile(LabeledMediaFileResource value) {
            this.audioFile = value;
        }

        /**
         * Ruft den Wert der videoFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public LabeledMediaFileResource getVideoFile() {
            return videoFile;
        }

        /**
         * Legt den Wert der videoFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public void setVideoFile(LabeledMediaFileResource value) {
            this.videoFile = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointModify"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accessDeviceEndpoint"
    })
    public static class ExternalSource {

        @XmlElement(required = true, nillable = true)
        protected AccessDeviceEndpointModify accessDeviceEndpoint;

        /**
         * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AccessDeviceEndpointModify }
         *     
         */
        public AccessDeviceEndpointModify getAccessDeviceEndpoint() {
            return accessDeviceEndpoint;
        }

        /**
         * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AccessDeviceEndpointModify }
         *     
         */
        public void setAccessDeviceEndpoint(AccessDeviceEndpointModify value) {
            this.accessDeviceEndpoint = value;
        }

    }

}
