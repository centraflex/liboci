//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *           Request to modify a specified group access device file.
 *           The response is either a SuccessResponse or an ErrorResponse.
 *           Replaced By: GroupAccessDeviceFileModifyRequest14sp8
 *         
 * 
 * <p>Java-Klasse für GroupAccessDeviceFileModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAccessDeviceFileModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="deviceName" type="{}AccessDeviceName"/>
 *         <element name="fileType" type="{}DeviceManagementFileType"/>
 *         <element name="fileSource" type="{}AccessDeviceEnhancedConfigurationMode" minOccurs="0"/>
 *         <element name="uploadFile" type="{}FileResource" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAccessDeviceFileModifyRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "deviceName",
    "fileType",
    "fileSource",
    "uploadFile"
})
public class GroupAccessDeviceFileModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String fileType;
    @XmlSchemaType(name = "token")
    protected AccessDeviceEnhancedConfigurationMode fileSource;
    protected FileResource uploadFile;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der deviceName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * Legt den Wert der deviceName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceName(String value) {
        this.deviceName = value;
    }

    /**
     * Ruft den Wert der fileType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileType() {
        return fileType;
    }

    /**
     * Legt den Wert der fileType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileType(String value) {
        this.fileType = value;
    }

    /**
     * Ruft den Wert der fileSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEnhancedConfigurationMode }
     *     
     */
    public AccessDeviceEnhancedConfigurationMode getFileSource() {
        return fileSource;
    }

    /**
     * Legt den Wert der fileSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEnhancedConfigurationMode }
     *     
     */
    public void setFileSource(AccessDeviceEnhancedConfigurationMode value) {
        this.fileSource = value;
    }

    /**
     * Ruft den Wert der uploadFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FileResource }
     *     
     */
    public FileResource getUploadFile() {
        return uploadFile;
    }

    /**
     * Legt den Wert der uploadFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FileResource }
     *     
     */
    public void setUploadFile(FileResource value) {
        this.uploadFile = value;
    }

}
