//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemAdviceOfChargeGetRequest.
 *         Contains a list of system Advice of Charge parameters.
 *         
 *         Replaced by: SystemAdviceOfChargeGetResponse19sp1
 *       
 * 
 * <p>Java-Klasse für SystemAdviceOfChargeGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAdviceOfChargeGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="delayBetweenNotificationSeconds" type="{}AdviceOfChargeDelayBetweenNotificationSeconds"/>
 *         <element name="incomingAocHandling" type="{}AdviceOfChargeIncomingAocHandling"/>
 *         <element name="costInformationSource" type="{}NetAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAdviceOfChargeGetResponse", propOrder = {
    "delayBetweenNotificationSeconds",
    "incomingAocHandling",
    "costInformationSource"
})
public class SystemAdviceOfChargeGetResponse
    extends OCIDataResponse
{

    protected int delayBetweenNotificationSeconds;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AdviceOfChargeIncomingAocHandling incomingAocHandling;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String costInformationSource;

    /**
     * Ruft den Wert der delayBetweenNotificationSeconds-Eigenschaft ab.
     * 
     */
    public int getDelayBetweenNotificationSeconds() {
        return delayBetweenNotificationSeconds;
    }

    /**
     * Legt den Wert der delayBetweenNotificationSeconds-Eigenschaft fest.
     * 
     */
    public void setDelayBetweenNotificationSeconds(int value) {
        this.delayBetweenNotificationSeconds = value;
    }

    /**
     * Ruft den Wert der incomingAocHandling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AdviceOfChargeIncomingAocHandling }
     *     
     */
    public AdviceOfChargeIncomingAocHandling getIncomingAocHandling() {
        return incomingAocHandling;
    }

    /**
     * Legt den Wert der incomingAocHandling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdviceOfChargeIncomingAocHandling }
     *     
     */
    public void setIncomingAocHandling(AdviceOfChargeIncomingAocHandling value) {
        this.incomingAocHandling = value;
    }

    /**
     * Ruft den Wert der costInformationSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostInformationSource() {
        return costInformationSource;
    }

    /**
     * Legt den Wert der costInformationSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostInformationSource(String value) {
        this.costInformationSource = value;
    }

}
