//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserServiceGetAssignmentListRequest.
 *         Contains two tables, one for the service packs, and one for the user services.
 *         The user table has the column headings: "Service Name", "Assigned",
 *         The service pack table's column headings are: "Service Pack Name", "Assigned", "Description".
 *         The "Assigned" column has either a true or false value
 *       
 * 
 * <p>Java-Klasse für UserServiceGetAssignmentListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserServiceGetAssignmentListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="servicePacksAssignmentTable" type="{C}OCITable"/>
 *         <element name="userServicesAssignmentTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserServiceGetAssignmentListResponse", propOrder = {
    "servicePacksAssignmentTable",
    "userServicesAssignmentTable"
})
public class UserServiceGetAssignmentListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable servicePacksAssignmentTable;
    @XmlElement(required = true)
    protected OCITable userServicesAssignmentTable;

    /**
     * Ruft den Wert der servicePacksAssignmentTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getServicePacksAssignmentTable() {
        return servicePacksAssignmentTable;
    }

    /**
     * Legt den Wert der servicePacksAssignmentTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setServicePacksAssignmentTable(OCITable value) {
        this.servicePacksAssignmentTable = value;
    }

    /**
     * Ruft den Wert der userServicesAssignmentTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserServicesAssignmentTable() {
        return userServicesAssignmentTable;
    }

    /**
     * Legt den Wert der userServicesAssignmentTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserServicesAssignmentTable(OCITable value) {
        this.userServicesAssignmentTable = value;
    }

}
