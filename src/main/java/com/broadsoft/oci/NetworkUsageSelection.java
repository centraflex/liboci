//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für NetworkUsageSelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="NetworkUsageSelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Force All Calls"/>
 *     <enumeration value="Force All Except Extension and Location Calls"/>
 *     <enumeration value="Do Not Force Enterprise and Group Calls"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "NetworkUsageSelection")
@XmlEnum
public enum NetworkUsageSelection {

    @XmlEnumValue("Force All Calls")
    FORCE_ALL_CALLS("Force All Calls"),
    @XmlEnumValue("Force All Except Extension and Location Calls")
    FORCE_ALL_EXCEPT_EXTENSION_AND_LOCATION_CALLS("Force All Except Extension and Location Calls"),
    @XmlEnumValue("Do Not Force Enterprise and Group Calls")
    DO_NOT_FORCE_ENTERPRISE_AND_GROUP_CALLS("Do Not Force Enterprise and Group Calls");
    private final String value;

    NetworkUsageSelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NetworkUsageSelection fromValue(String v) {
        for (NetworkUsageSelection c: NetworkUsageSelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
