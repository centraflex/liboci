//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The OCIReportingNotification is an abstract type.  Responses are not expected for notifications.
 *       
 * 
 * <p>Java-Klasse für OCIReportingNotification complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OCIReportingNotification">
 *   <complexContent>
 *     <extension base="{}OCIReportingCommand">
 *       <sequence>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OCIReportingNotification")
@XmlSeeAlso({
    OCIReportingServerStatusNotification.class,
    OCIReportingReportNotification.class,
    OCIReportingReportPublicIdentityNotification.class,
    OCIReportingProvisioningTaskReportNotification.class
})
public abstract class OCIReportingNotification
    extends OCIReportingCommand
{


}
