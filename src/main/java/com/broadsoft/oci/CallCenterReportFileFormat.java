//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterReportFileFormat.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterReportFileFormat">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="PDF"/>
 *     <enumeration value="XLS"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterReportFileFormat")
@XmlEnum
public enum CallCenterReportFileFormat {

    PDF,
    XLS;

    public String value() {
        return name();
    }

    public static CallCenterReportFileFormat fromValue(String v) {
        return valueOf(v);
    }

}
