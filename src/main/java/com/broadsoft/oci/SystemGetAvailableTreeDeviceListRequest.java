//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Requests the list of available tree devices to link to for the entire system.
 *         Requests the list of available tree devices to which a leaf device can be linked for the entire system.
 *         A tree device is a device associated with a device type that has the option 
 *         supportLinks set to "Support Links from Devices". Many leaf devices can be linked to it.
 *         The list returned includes devices created at the system, service provider, and group levels.
 *         The response is either SystemGetAvailableTreeDeviceListResponse or
 *         ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemGetAvailableTreeDeviceListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGetAvailableTreeDeviceListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGetAvailableTreeDeviceListRequest")
public class SystemGetAvailableTreeDeviceListRequest
    extends OCIRequest
{


}
