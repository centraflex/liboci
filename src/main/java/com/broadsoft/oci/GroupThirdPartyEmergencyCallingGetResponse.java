//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupThirdPartyEmergencyCallingGetRequest.
 *         The response contains the third-party emergency call service settings for the Group.
 *       
 * 
 * <p>Java-Klasse für GroupThirdPartyEmergencyCallingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupThirdPartyEmergencyCallingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableDeviceManagement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableRouting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupThirdPartyEmergencyCallingGetResponse", propOrder = {
    "enableDeviceManagement",
    "enableRouting"
})
public class GroupThirdPartyEmergencyCallingGetResponse
    extends OCIDataResponse
{

    protected boolean enableDeviceManagement;
    protected boolean enableRouting;

    /**
     * Ruft den Wert der enableDeviceManagement-Eigenschaft ab.
     * 
     */
    public boolean isEnableDeviceManagement() {
        return enableDeviceManagement;
    }

    /**
     * Legt den Wert der enableDeviceManagement-Eigenschaft fest.
     * 
     */
    public void setEnableDeviceManagement(boolean value) {
        this.enableDeviceManagement = value;
    }

    /**
     * Ruft den Wert der enableRouting-Eigenschaft ab.
     * 
     */
    public boolean isEnableRouting() {
        return enableRouting;
    }

    /**
     * Legt den Wert der enableRouting-Eigenschaft fest.
     * 
     */
    public void setEnableRouting(boolean value) {
        this.enableRouting = value;
    }

}
