//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Represents a Verify Translation and Routing request which can be either a request containing
 *         parameters or a request containing a SIP message. Returns a SystemVerifyTranslationAndRoutingResponse.
 *       
 * 
 * <p>Java-Klasse für SystemVerifyTranslationAndRoutingRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemVerifyTranslationAndRoutingRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <choice>
 *         <element name="parameters" type="{}VerifyTranslationAndRoutingParameters"/>
 *         <element name="sipMessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       </choice>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemVerifyTranslationAndRoutingRequest", propOrder = {
    "parameters",
    "sipMessage"
})
public class SystemVerifyTranslationAndRoutingRequest
    extends OCIRequest
{

    protected VerifyTranslationAndRoutingParameters parameters;
    protected String sipMessage;

    /**
     * Ruft den Wert der parameters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VerifyTranslationAndRoutingParameters }
     *     
     */
    public VerifyTranslationAndRoutingParameters getParameters() {
        return parameters;
    }

    /**
     * Legt den Wert der parameters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VerifyTranslationAndRoutingParameters }
     *     
     */
    public void setParameters(VerifyTranslationAndRoutingParameters value) {
        this.parameters = value;
    }

    /**
     * Ruft den Wert der sipMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSipMessage() {
        return sipMessage;
    }

    /**
     * Legt den Wert der sipMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSipMessage(String value) {
        this.sipMessage = value;
    }

}
