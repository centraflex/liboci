//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the SystemCommunicationBarringCriteriaGetRequest19sp1.
 *         The response contains the Communication Barring Criteria information.
 *       
 * 
 * <p>Java-Klasse für SystemCommunicationBarringCriteriaGetResponse19sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCommunicationBarringCriteriaGetResponse19sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="description" type="{}CommunicationBarringCriteriaDescription" minOccurs="0"/>
 *         <element name="matchCallType" type="{}CommunicationBarringCallType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="matchAlternateCallIndicator" type="{}CommunicationBarringAlternateCallIndicator" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="matchVirtualOnNetCallType" type="{}VirtualOnNetCallTypeName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="matchPublicNetwork" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchPrivateNetwork" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchLocalCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchNationalCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchInterlataCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchIntralataCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchInternationalCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchPrivateCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchEmergencyCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchOtherCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchInterNetwork" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchInterHostingNE" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchInterAS" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchIntraAS" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchChargeCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchNoChargeCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchGroupCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchEnterpriseCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchNetworkCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchNetworkURLCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchRepairCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchInternalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchLocation" type="{}LocationCriteria"/>
 *         <element name="matchRoaming" type="{}RoamingCriteria"/>
 *         <element name="timeSchedule" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="matchNumberPortabilityStatus" type="{}NumberPortabilityStatus" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCommunicationBarringCriteriaGetResponse19sp1", propOrder = {
    "description",
    "matchCallType",
    "matchAlternateCallIndicator",
    "matchVirtualOnNetCallType",
    "matchPublicNetwork",
    "matchPrivateNetwork",
    "matchLocalCategory",
    "matchNationalCategory",
    "matchInterlataCategory",
    "matchIntralataCategory",
    "matchInternationalCategory",
    "matchPrivateCategory",
    "matchEmergencyCategory",
    "matchOtherCategory",
    "matchInterNetwork",
    "matchInterHostingNE",
    "matchInterAS",
    "matchIntraAS",
    "matchChargeCalls",
    "matchNoChargeCalls",
    "matchGroupCalls",
    "matchEnterpriseCalls",
    "matchNetworkCalls",
    "matchNetworkURLCalls",
    "matchRepairCalls",
    "matchEmergencyCalls",
    "matchInternalCalls",
    "matchLocation",
    "matchRoaming",
    "timeSchedule",
    "holidaySchedule",
    "matchNumberPortabilityStatus"
})
public class SystemCommunicationBarringCriteriaGetResponse19Sp1
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> matchCallType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> matchAlternateCallIndicator;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> matchVirtualOnNetCallType;
    protected boolean matchPublicNetwork;
    protected boolean matchPrivateNetwork;
    protected boolean matchLocalCategory;
    protected boolean matchNationalCategory;
    protected boolean matchInterlataCategory;
    protected boolean matchIntralataCategory;
    protected boolean matchInternationalCategory;
    protected boolean matchPrivateCategory;
    protected boolean matchEmergencyCategory;
    protected boolean matchOtherCategory;
    protected boolean matchInterNetwork;
    protected boolean matchInterHostingNE;
    protected boolean matchInterAS;
    protected boolean matchIntraAS;
    protected boolean matchChargeCalls;
    protected boolean matchNoChargeCalls;
    protected boolean matchGroupCalls;
    protected boolean matchEnterpriseCalls;
    protected boolean matchNetworkCalls;
    protected boolean matchNetworkURLCalls;
    protected boolean matchRepairCalls;
    protected boolean matchEmergencyCalls;
    protected boolean matchInternalCalls;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected LocationCriteria matchLocation;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected RoamingCriteria matchRoaming;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeSchedule;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String holidaySchedule;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> matchNumberPortabilityStatus;

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the matchCallType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the matchCallType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMatchCallType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the matchCallType property.
     */
    public List<String> getMatchCallType() {
        if (matchCallType == null) {
            matchCallType = new ArrayList<>();
        }
        return this.matchCallType;
    }

    /**
     * Gets the value of the matchAlternateCallIndicator property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the matchAlternateCallIndicator property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMatchAlternateCallIndicator().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the matchAlternateCallIndicator property.
     */
    public List<String> getMatchAlternateCallIndicator() {
        if (matchAlternateCallIndicator == null) {
            matchAlternateCallIndicator = new ArrayList<>();
        }
        return this.matchAlternateCallIndicator;
    }

    /**
     * Gets the value of the matchVirtualOnNetCallType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the matchVirtualOnNetCallType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMatchVirtualOnNetCallType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the matchVirtualOnNetCallType property.
     */
    public List<String> getMatchVirtualOnNetCallType() {
        if (matchVirtualOnNetCallType == null) {
            matchVirtualOnNetCallType = new ArrayList<>();
        }
        return this.matchVirtualOnNetCallType;
    }

    /**
     * Ruft den Wert der matchPublicNetwork-Eigenschaft ab.
     * 
     */
    public boolean isMatchPublicNetwork() {
        return matchPublicNetwork;
    }

    /**
     * Legt den Wert der matchPublicNetwork-Eigenschaft fest.
     * 
     */
    public void setMatchPublicNetwork(boolean value) {
        this.matchPublicNetwork = value;
    }

    /**
     * Ruft den Wert der matchPrivateNetwork-Eigenschaft ab.
     * 
     */
    public boolean isMatchPrivateNetwork() {
        return matchPrivateNetwork;
    }

    /**
     * Legt den Wert der matchPrivateNetwork-Eigenschaft fest.
     * 
     */
    public void setMatchPrivateNetwork(boolean value) {
        this.matchPrivateNetwork = value;
    }

    /**
     * Ruft den Wert der matchLocalCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchLocalCategory() {
        return matchLocalCategory;
    }

    /**
     * Legt den Wert der matchLocalCategory-Eigenschaft fest.
     * 
     */
    public void setMatchLocalCategory(boolean value) {
        this.matchLocalCategory = value;
    }

    /**
     * Ruft den Wert der matchNationalCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchNationalCategory() {
        return matchNationalCategory;
    }

    /**
     * Legt den Wert der matchNationalCategory-Eigenschaft fest.
     * 
     */
    public void setMatchNationalCategory(boolean value) {
        this.matchNationalCategory = value;
    }

    /**
     * Ruft den Wert der matchInterlataCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchInterlataCategory() {
        return matchInterlataCategory;
    }

    /**
     * Legt den Wert der matchInterlataCategory-Eigenschaft fest.
     * 
     */
    public void setMatchInterlataCategory(boolean value) {
        this.matchInterlataCategory = value;
    }

    /**
     * Ruft den Wert der matchIntralataCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchIntralataCategory() {
        return matchIntralataCategory;
    }

    /**
     * Legt den Wert der matchIntralataCategory-Eigenschaft fest.
     * 
     */
    public void setMatchIntralataCategory(boolean value) {
        this.matchIntralataCategory = value;
    }

    /**
     * Ruft den Wert der matchInternationalCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchInternationalCategory() {
        return matchInternationalCategory;
    }

    /**
     * Legt den Wert der matchInternationalCategory-Eigenschaft fest.
     * 
     */
    public void setMatchInternationalCategory(boolean value) {
        this.matchInternationalCategory = value;
    }

    /**
     * Ruft den Wert der matchPrivateCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchPrivateCategory() {
        return matchPrivateCategory;
    }

    /**
     * Legt den Wert der matchPrivateCategory-Eigenschaft fest.
     * 
     */
    public void setMatchPrivateCategory(boolean value) {
        this.matchPrivateCategory = value;
    }

    /**
     * Ruft den Wert der matchEmergencyCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchEmergencyCategory() {
        return matchEmergencyCategory;
    }

    /**
     * Legt den Wert der matchEmergencyCategory-Eigenschaft fest.
     * 
     */
    public void setMatchEmergencyCategory(boolean value) {
        this.matchEmergencyCategory = value;
    }

    /**
     * Ruft den Wert der matchOtherCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchOtherCategory() {
        return matchOtherCategory;
    }

    /**
     * Legt den Wert der matchOtherCategory-Eigenschaft fest.
     * 
     */
    public void setMatchOtherCategory(boolean value) {
        this.matchOtherCategory = value;
    }

    /**
     * Ruft den Wert der matchInterNetwork-Eigenschaft ab.
     * 
     */
    public boolean isMatchInterNetwork() {
        return matchInterNetwork;
    }

    /**
     * Legt den Wert der matchInterNetwork-Eigenschaft fest.
     * 
     */
    public void setMatchInterNetwork(boolean value) {
        this.matchInterNetwork = value;
    }

    /**
     * Ruft den Wert der matchInterHostingNE-Eigenschaft ab.
     * 
     */
    public boolean isMatchInterHostingNE() {
        return matchInterHostingNE;
    }

    /**
     * Legt den Wert der matchInterHostingNE-Eigenschaft fest.
     * 
     */
    public void setMatchInterHostingNE(boolean value) {
        this.matchInterHostingNE = value;
    }

    /**
     * Ruft den Wert der matchInterAS-Eigenschaft ab.
     * 
     */
    public boolean isMatchInterAS() {
        return matchInterAS;
    }

    /**
     * Legt den Wert der matchInterAS-Eigenschaft fest.
     * 
     */
    public void setMatchInterAS(boolean value) {
        this.matchInterAS = value;
    }

    /**
     * Ruft den Wert der matchIntraAS-Eigenschaft ab.
     * 
     */
    public boolean isMatchIntraAS() {
        return matchIntraAS;
    }

    /**
     * Legt den Wert der matchIntraAS-Eigenschaft fest.
     * 
     */
    public void setMatchIntraAS(boolean value) {
        this.matchIntraAS = value;
    }

    /**
     * Ruft den Wert der matchChargeCalls-Eigenschaft ab.
     * 
     */
    public boolean isMatchChargeCalls() {
        return matchChargeCalls;
    }

    /**
     * Legt den Wert der matchChargeCalls-Eigenschaft fest.
     * 
     */
    public void setMatchChargeCalls(boolean value) {
        this.matchChargeCalls = value;
    }

    /**
     * Ruft den Wert der matchNoChargeCalls-Eigenschaft ab.
     * 
     */
    public boolean isMatchNoChargeCalls() {
        return matchNoChargeCalls;
    }

    /**
     * Legt den Wert der matchNoChargeCalls-Eigenschaft fest.
     * 
     */
    public void setMatchNoChargeCalls(boolean value) {
        this.matchNoChargeCalls = value;
    }

    /**
     * Ruft den Wert der matchGroupCalls-Eigenschaft ab.
     * 
     */
    public boolean isMatchGroupCalls() {
        return matchGroupCalls;
    }

    /**
     * Legt den Wert der matchGroupCalls-Eigenschaft fest.
     * 
     */
    public void setMatchGroupCalls(boolean value) {
        this.matchGroupCalls = value;
    }

    /**
     * Ruft den Wert der matchEnterpriseCalls-Eigenschaft ab.
     * 
     */
    public boolean isMatchEnterpriseCalls() {
        return matchEnterpriseCalls;
    }

    /**
     * Legt den Wert der matchEnterpriseCalls-Eigenschaft fest.
     * 
     */
    public void setMatchEnterpriseCalls(boolean value) {
        this.matchEnterpriseCalls = value;
    }

    /**
     * Ruft den Wert der matchNetworkCalls-Eigenschaft ab.
     * 
     */
    public boolean isMatchNetworkCalls() {
        return matchNetworkCalls;
    }

    /**
     * Legt den Wert der matchNetworkCalls-Eigenschaft fest.
     * 
     */
    public void setMatchNetworkCalls(boolean value) {
        this.matchNetworkCalls = value;
    }

    /**
     * Ruft den Wert der matchNetworkURLCalls-Eigenschaft ab.
     * 
     */
    public boolean isMatchNetworkURLCalls() {
        return matchNetworkURLCalls;
    }

    /**
     * Legt den Wert der matchNetworkURLCalls-Eigenschaft fest.
     * 
     */
    public void setMatchNetworkURLCalls(boolean value) {
        this.matchNetworkURLCalls = value;
    }

    /**
     * Ruft den Wert der matchRepairCalls-Eigenschaft ab.
     * 
     */
    public boolean isMatchRepairCalls() {
        return matchRepairCalls;
    }

    /**
     * Legt den Wert der matchRepairCalls-Eigenschaft fest.
     * 
     */
    public void setMatchRepairCalls(boolean value) {
        this.matchRepairCalls = value;
    }

    /**
     * Ruft den Wert der matchEmergencyCalls-Eigenschaft ab.
     * 
     */
    public boolean isMatchEmergencyCalls() {
        return matchEmergencyCalls;
    }

    /**
     * Legt den Wert der matchEmergencyCalls-Eigenschaft fest.
     * 
     */
    public void setMatchEmergencyCalls(boolean value) {
        this.matchEmergencyCalls = value;
    }

    /**
     * Ruft den Wert der matchInternalCalls-Eigenschaft ab.
     * 
     */
    public boolean isMatchInternalCalls() {
        return matchInternalCalls;
    }

    /**
     * Legt den Wert der matchInternalCalls-Eigenschaft fest.
     * 
     */
    public void setMatchInternalCalls(boolean value) {
        this.matchInternalCalls = value;
    }

    /**
     * Ruft den Wert der matchLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocationCriteria }
     *     
     */
    public LocationCriteria getMatchLocation() {
        return matchLocation;
    }

    /**
     * Legt den Wert der matchLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationCriteria }
     *     
     */
    public void setMatchLocation(LocationCriteria value) {
        this.matchLocation = value;
    }

    /**
     * Ruft den Wert der matchRoaming-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RoamingCriteria }
     *     
     */
    public RoamingCriteria getMatchRoaming() {
        return matchRoaming;
    }

    /**
     * Legt den Wert der matchRoaming-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RoamingCriteria }
     *     
     */
    public void setMatchRoaming(RoamingCriteria value) {
        this.matchRoaming = value;
    }

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeSchedule(String value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHolidaySchedule(String value) {
        this.holidaySchedule = value;
    }

    /**
     * Gets the value of the matchNumberPortabilityStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the matchNumberPortabilityStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMatchNumberPortabilityStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the matchNumberPortabilityStatus property.
     */
    public List<String> getMatchNumberPortabilityStatus() {
        if (matchNumberPortabilityStatus == null) {
            matchNumberPortabilityStatus = new ArrayList<>();
        }
        return this.matchNumberPortabilityStatus;
    }

}
