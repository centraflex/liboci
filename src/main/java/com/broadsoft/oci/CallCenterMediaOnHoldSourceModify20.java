//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains the call center media on hold source configuration.
 *       
 * 
 * <p>Java-Klasse für CallCenterMediaOnHoldSourceModify20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterMediaOnHoldSourceModify20">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="audioMessageSourceSelection" type="{}CallCenterMediaOnHoldMessageSelection" minOccurs="0"/>
 *         <element name="audioUrlList" type="{}CallCenterAnnouncementURLListModify" minOccurs="0"/>
 *         <element name="audioFileList" type="{}CallCenterAnnouncementFileListModify20" minOccurs="0"/>
 *         <element name="externalAudioSource" type="{}AccessDeviceEndpointModify" minOccurs="0"/>
 *         <element name="videoMessageSourceSelection" type="{}CallCenterMediaOnHoldMessageSelection" minOccurs="0"/>
 *         <element name="videoUrlList" type="{}CallCenterAnnouncementURLListModify" minOccurs="0"/>
 *         <element name="videoFileList" type="{}CallCenterAnnouncementFileListModify20" minOccurs="0"/>
 *         <element name="externalVideoSource" type="{}AccessDeviceEndpointModify" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterMediaOnHoldSourceModify20", propOrder = {
    "audioMessageSourceSelection",
    "audioUrlList",
    "audioFileList",
    "externalAudioSource",
    "videoMessageSourceSelection",
    "videoUrlList",
    "videoFileList",
    "externalVideoSource"
})
public class CallCenterMediaOnHoldSourceModify20 {

    @XmlSchemaType(name = "token")
    protected CallCenterMediaOnHoldMessageSelection audioMessageSourceSelection;
    protected CallCenterAnnouncementURLListModify audioUrlList;
    protected CallCenterAnnouncementFileListModify20 audioFileList;
    @XmlElementRef(name = "externalAudioSource", type = JAXBElement.class, required = false)
    protected JAXBElement<AccessDeviceEndpointModify> externalAudioSource;
    @XmlSchemaType(name = "token")
    protected CallCenterMediaOnHoldMessageSelection videoMessageSourceSelection;
    protected CallCenterAnnouncementURLListModify videoUrlList;
    protected CallCenterAnnouncementFileListModify20 videoFileList;
    @XmlElementRef(name = "externalVideoSource", type = JAXBElement.class, required = false)
    protected JAXBElement<AccessDeviceEndpointModify> externalVideoSource;

    /**
     * Ruft den Wert der audioMessageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public CallCenterMediaOnHoldMessageSelection getAudioMessageSourceSelection() {
        return audioMessageSourceSelection;
    }

    /**
     * Legt den Wert der audioMessageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public void setAudioMessageSourceSelection(CallCenterMediaOnHoldMessageSelection value) {
        this.audioMessageSourceSelection = value;
    }

    /**
     * Ruft den Wert der audioUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public CallCenterAnnouncementURLListModify getAudioUrlList() {
        return audioUrlList;
    }

    /**
     * Legt den Wert der audioUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public void setAudioUrlList(CallCenterAnnouncementURLListModify value) {
        this.audioUrlList = value;
    }

    /**
     * Ruft den Wert der audioFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListModify20 }
     *     
     */
    public CallCenterAnnouncementFileListModify20 getAudioFileList() {
        return audioFileList;
    }

    /**
     * Legt den Wert der audioFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListModify20 }
     *     
     */
    public void setAudioFileList(CallCenterAnnouncementFileListModify20 value) {
        this.audioFileList = value;
    }

    /**
     * Ruft den Wert der externalAudioSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceEndpointModify }{@code >}
     *     
     */
    public JAXBElement<AccessDeviceEndpointModify> getExternalAudioSource() {
        return externalAudioSource;
    }

    /**
     * Legt den Wert der externalAudioSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceEndpointModify }{@code >}
     *     
     */
    public void setExternalAudioSource(JAXBElement<AccessDeviceEndpointModify> value) {
        this.externalAudioSource = value;
    }

    /**
     * Ruft den Wert der videoMessageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public CallCenterMediaOnHoldMessageSelection getVideoMessageSourceSelection() {
        return videoMessageSourceSelection;
    }

    /**
     * Legt den Wert der videoMessageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public void setVideoMessageSourceSelection(CallCenterMediaOnHoldMessageSelection value) {
        this.videoMessageSourceSelection = value;
    }

    /**
     * Ruft den Wert der videoUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public CallCenterAnnouncementURLListModify getVideoUrlList() {
        return videoUrlList;
    }

    /**
     * Legt den Wert der videoUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public void setVideoUrlList(CallCenterAnnouncementURLListModify value) {
        this.videoUrlList = value;
    }

    /**
     * Ruft den Wert der videoFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListModify20 }
     *     
     */
    public CallCenterAnnouncementFileListModify20 getVideoFileList() {
        return videoFileList;
    }

    /**
     * Legt den Wert der videoFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListModify20 }
     *     
     */
    public void setVideoFileList(CallCenterAnnouncementFileListModify20 value) {
        this.videoFileList = value;
    }

    /**
     * Ruft den Wert der externalVideoSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceEndpointModify }{@code >}
     *     
     */
    public JAXBElement<AccessDeviceEndpointModify> getExternalVideoSource() {
        return externalVideoSource;
    }

    /**
     * Legt den Wert der externalVideoSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceEndpointModify }{@code >}
     *     
     */
    public void setExternalVideoSource(JAXBElement<AccessDeviceEndpointModify> value) {
        this.externalVideoSource = value;
    }

}
