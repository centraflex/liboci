//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java-Klasse für CallForwardingOptionsMenuKeysReadEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallForwardingOptionsMenuKeysReadEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="activateCallForwarding" type="{}DigitAny" minOccurs="0"/>
 *         <element name="deactivateCallForwarding" type="{}DigitAny" minOccurs="0"/>
 *         <element name="changeCallForwardingDestination" type="{}DigitAny" minOccurs="0"/>
 *         <element name="listenToCallForwardingStatus" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallForwardingOptionsMenuKeysReadEntry", propOrder = {
    "activateCallForwarding",
    "deactivateCallForwarding",
    "changeCallForwardingDestination",
    "listenToCallForwardingStatus",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class CallForwardingOptionsMenuKeysReadEntry {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String activateCallForwarding;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deactivateCallForwarding;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String changeCallForwardingDestination;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String listenToCallForwardingStatus;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String repeatMenu;

    /**
     * Ruft den Wert der activateCallForwarding-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivateCallForwarding() {
        return activateCallForwarding;
    }

    /**
     * Legt den Wert der activateCallForwarding-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivateCallForwarding(String value) {
        this.activateCallForwarding = value;
    }

    /**
     * Ruft den Wert der deactivateCallForwarding-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeactivateCallForwarding() {
        return deactivateCallForwarding;
    }

    /**
     * Legt den Wert der deactivateCallForwarding-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeactivateCallForwarding(String value) {
        this.deactivateCallForwarding = value;
    }

    /**
     * Ruft den Wert der changeCallForwardingDestination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeCallForwardingDestination() {
        return changeCallForwardingDestination;
    }

    /**
     * Legt den Wert der changeCallForwardingDestination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeCallForwardingDestination(String value) {
        this.changeCallForwardingDestination = value;
    }

    /**
     * Ruft den Wert der listenToCallForwardingStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListenToCallForwardingStatus() {
        return listenToCallForwardingStatus;
    }

    /**
     * Legt den Wert der listenToCallForwardingStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListenToCallForwardingStatus(String value) {
        this.listenToCallForwardingStatus = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepeatMenu(String value) {
        this.repeatMenu = value;
    }

}
