//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für HuntGroupUniformCallDistributionPolicyScope.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="HuntGroupUniformCallDistributionPolicyScope">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Agent"/>
 *     <enumeration value="Hunt Group"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "HuntGroupUniformCallDistributionPolicyScope")
@XmlEnum
public enum HuntGroupUniformCallDistributionPolicyScope {

    @XmlEnumValue("Agent")
    AGENT("Agent"),
    @XmlEnumValue("Hunt Group")
    HUNT_GROUP("Hunt Group");
    private final String value;

    HuntGroupUniformCallDistributionPolicyScope(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HuntGroupUniformCallDistributionPolicyScope fromValue(String v) {
        for (HuntGroupUniformCallDistributionPolicyScope c: HuntGroupUniformCallDistributionPolicyScope.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
