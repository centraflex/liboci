//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Contains list of file descriptions for audio or video files
 *       
 * 
 * <p>Java-Klasse für CallCenterAnnouncementDescriptionList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterAnnouncementDescriptionList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="fileDescription1" type="{}FileDescription" minOccurs="0"/>
 *         <element name="fileDescription2" type="{}FileDescription" minOccurs="0"/>
 *         <element name="fileDescription3" type="{}FileDescription" minOccurs="0"/>
 *         <element name="fileDescription4" type="{}FileDescription" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterAnnouncementDescriptionList", propOrder = {
    "fileDescription1",
    "fileDescription2",
    "fileDescription3",
    "fileDescription4"
})
public class CallCenterAnnouncementDescriptionList {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String fileDescription1;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String fileDescription2;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String fileDescription3;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String fileDescription4;

    /**
     * Ruft den Wert der fileDescription1-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileDescription1() {
        return fileDescription1;
    }

    /**
     * Legt den Wert der fileDescription1-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileDescription1(String value) {
        this.fileDescription1 = value;
    }

    /**
     * Ruft den Wert der fileDescription2-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileDescription2() {
        return fileDescription2;
    }

    /**
     * Legt den Wert der fileDescription2-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileDescription2(String value) {
        this.fileDescription2 = value;
    }

    /**
     * Ruft den Wert der fileDescription3-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileDescription3() {
        return fileDescription3;
    }

    /**
     * Legt den Wert der fileDescription3-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileDescription3(String value) {
        this.fileDescription3 = value;
    }

    /**
     * Ruft den Wert der fileDescription4-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileDescription4() {
        return fileDescription4;
    }

    /**
     * Legt den Wert der fileDescription4-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileDescription4(String value) {
        this.fileDescription4 = value;
    }

}
