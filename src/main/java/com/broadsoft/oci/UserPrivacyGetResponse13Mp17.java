//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserPrivacyGetRequest13mp17.  The
 *         permittedMonitorUserIdTable contains the members of
 *         the enterprise or group allowed to monitor the phone
 *         status of the user specified in the request.  Members
 *         of this table are allowed to monitor the user even if
 *         enablePhoneStatusPrivacy is set to true.  The table
 *         contains column headings: "User Id", "Last Name",
 *         "First Name", "Hiragana Last Name", "Hiragana First Name",
 *         "Phone Number", "Extension", "Department", "Email Address", "IMP Id".
 *       
 * 
 * <p>Java-Klasse für UserPrivacyGetResponse13mp17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPrivacyGetResponse13mp17">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableDirectoryPrivacy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableAutoAttendantExtensionDialingPrivacy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableAutoAttendantNameDialingPrivacy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enablePhoneStatusPrivacy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="permittedMonitorUserIdTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPrivacyGetResponse13mp17", propOrder = {
    "enableDirectoryPrivacy",
    "enableAutoAttendantExtensionDialingPrivacy",
    "enableAutoAttendantNameDialingPrivacy",
    "enablePhoneStatusPrivacy",
    "permittedMonitorUserIdTable"
})
public class UserPrivacyGetResponse13Mp17
    extends OCIDataResponse
{

    protected boolean enableDirectoryPrivacy;
    protected boolean enableAutoAttendantExtensionDialingPrivacy;
    protected boolean enableAutoAttendantNameDialingPrivacy;
    protected boolean enablePhoneStatusPrivacy;
    @XmlElement(required = true)
    protected OCITable permittedMonitorUserIdTable;

    /**
     * Ruft den Wert der enableDirectoryPrivacy-Eigenschaft ab.
     * 
     */
    public boolean isEnableDirectoryPrivacy() {
        return enableDirectoryPrivacy;
    }

    /**
     * Legt den Wert der enableDirectoryPrivacy-Eigenschaft fest.
     * 
     */
    public void setEnableDirectoryPrivacy(boolean value) {
        this.enableDirectoryPrivacy = value;
    }

    /**
     * Ruft den Wert der enableAutoAttendantExtensionDialingPrivacy-Eigenschaft ab.
     * 
     */
    public boolean isEnableAutoAttendantExtensionDialingPrivacy() {
        return enableAutoAttendantExtensionDialingPrivacy;
    }

    /**
     * Legt den Wert der enableAutoAttendantExtensionDialingPrivacy-Eigenschaft fest.
     * 
     */
    public void setEnableAutoAttendantExtensionDialingPrivacy(boolean value) {
        this.enableAutoAttendantExtensionDialingPrivacy = value;
    }

    /**
     * Ruft den Wert der enableAutoAttendantNameDialingPrivacy-Eigenschaft ab.
     * 
     */
    public boolean isEnableAutoAttendantNameDialingPrivacy() {
        return enableAutoAttendantNameDialingPrivacy;
    }

    /**
     * Legt den Wert der enableAutoAttendantNameDialingPrivacy-Eigenschaft fest.
     * 
     */
    public void setEnableAutoAttendantNameDialingPrivacy(boolean value) {
        this.enableAutoAttendantNameDialingPrivacy = value;
    }

    /**
     * Ruft den Wert der enablePhoneStatusPrivacy-Eigenschaft ab.
     * 
     */
    public boolean isEnablePhoneStatusPrivacy() {
        return enablePhoneStatusPrivacy;
    }

    /**
     * Legt den Wert der enablePhoneStatusPrivacy-Eigenschaft fest.
     * 
     */
    public void setEnablePhoneStatusPrivacy(boolean value) {
        this.enablePhoneStatusPrivacy = value;
    }

    /**
     * Ruft den Wert der permittedMonitorUserIdTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getPermittedMonitorUserIdTable() {
        return permittedMonitorUserIdTable;
    }

    /**
     * Legt den Wert der permittedMonitorUserIdTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setPermittedMonitorUserIdTable(OCITable value) {
        this.permittedMonitorUserIdTable = value;
    }

}
