//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterEnhancedReportingSystemBrandingChoice.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterEnhancedReportingSystemBrandingChoice">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Default"/>
 *     <enumeration value="Custom"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterEnhancedReportingSystemBrandingChoice")
@XmlEnum
public enum CallCenterEnhancedReportingSystemBrandingChoice {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("Custom")
    CUSTOM("Custom");
    private final String value;

    CallCenterEnhancedReportingSystemBrandingChoice(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterEnhancedReportingSystemBrandingChoice fromValue(String v) {
        for (CallCenterEnhancedReportingSystemBrandingChoice c: CallCenterEnhancedReportingSystemBrandingChoice.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
