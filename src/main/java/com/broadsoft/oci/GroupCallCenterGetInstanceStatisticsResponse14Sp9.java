//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains Call Center statistics.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterGetInstanceStatisticsResponse14sp9 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterGetInstanceStatisticsResponse14sp9">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="statisticsRange" type="{}CallCenterStatisticsRange"/>
 *         <element name="queueStatistics" type="{}CallCenterQueueStatistics14sp9"/>
 *         <element name="agentStatistics" type="{}CallCenterAgentStatistics14sp9" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterGetInstanceStatisticsResponse14sp9", propOrder = {
    "statisticsRange",
    "queueStatistics",
    "agentStatistics"
})
public class GroupCallCenterGetInstanceStatisticsResponse14Sp9
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected CallCenterStatisticsRange statisticsRange;
    @XmlElement(required = true)
    protected CallCenterQueueStatistics14Sp9 queueStatistics;
    protected List<CallCenterAgentStatistics14Sp9> agentStatistics;

    /**
     * Ruft den Wert der statisticsRange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterStatisticsRange }
     *     
     */
    public CallCenterStatisticsRange getStatisticsRange() {
        return statisticsRange;
    }

    /**
     * Legt den Wert der statisticsRange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterStatisticsRange }
     *     
     */
    public void setStatisticsRange(CallCenterStatisticsRange value) {
        this.statisticsRange = value;
    }

    /**
     * Ruft den Wert der queueStatistics-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterQueueStatistics14Sp9 }
     *     
     */
    public CallCenterQueueStatistics14Sp9 getQueueStatistics() {
        return queueStatistics;
    }

    /**
     * Legt den Wert der queueStatistics-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterQueueStatistics14Sp9 }
     *     
     */
    public void setQueueStatistics(CallCenterQueueStatistics14Sp9 value) {
        this.queueStatistics = value;
    }

    /**
     * Gets the value of the agentStatistics property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the agentStatistics property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgentStatistics().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallCenterAgentStatistics14Sp9 }
     * 
     * 
     * @return
     *     The value of the agentStatistics property.
     */
    public List<CallCenterAgentStatistics14Sp9> getAgentStatistics() {
        if (agentStatistics == null) {
            agentStatistics = new ArrayList<>();
        }
        return this.agentStatistics;
    }

}
