//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeviceManagementEventStatusForCancel.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DeviceManagementEventStatusForCancel">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Pending"/>
 *     <enumeration value="Queued"/>
 *     <enumeration value="In Progress"/>
 *     <enumeration value="Stale"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DeviceManagementEventStatusForCancel")
@XmlEnum
public enum DeviceManagementEventStatusForCancel {

    @XmlEnumValue("Pending")
    PENDING("Pending"),
    @XmlEnumValue("Queued")
    QUEUED("Queued"),
    @XmlEnumValue("In Progress")
    IN_PROGRESS("In Progress"),
    @XmlEnumValue("Stale")
    STALE("Stale");
    private final String value;

    DeviceManagementEventStatusForCancel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeviceManagementEventStatusForCancel fromValue(String v) {
        for (DeviceManagementEventStatusForCancel c: DeviceManagementEventStatusForCancel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
