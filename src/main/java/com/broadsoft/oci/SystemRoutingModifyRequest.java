//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modifies the system's general routing attributes.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemRoutingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemRoutingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="isRouteRoundRobin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="routeTimerSeconds" type="{}RouteTimerSeconds" minOccurs="0"/>
 *         <element name="dnsResolvedAddressSelectionPolicy" type="{}RoutingDNSResolvedAddressSelectionPolicy" minOccurs="0"/>
 *         <element name="statefulExpirationMinutes" type="{}RoutingStatefulExpirationMinutes" minOccurs="0"/>
 *         <element name="maxAddressesPerHostname" type="{}RoutingMaxAddresses" minOccurs="0"/>
 *         <element name="maxAddressesDuringSetup" type="{}RoutingMaxAddresses" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemRoutingModifyRequest", propOrder = {
    "isRouteRoundRobin",
    "routeTimerSeconds",
    "dnsResolvedAddressSelectionPolicy",
    "statefulExpirationMinutes",
    "maxAddressesPerHostname",
    "maxAddressesDuringSetup"
})
public class SystemRoutingModifyRequest
    extends OCIRequest
{

    protected Boolean isRouteRoundRobin;
    protected Integer routeTimerSeconds;
    @XmlSchemaType(name = "token")
    protected RoutingDNSResolvedAddressSelectionPolicy dnsResolvedAddressSelectionPolicy;
    protected Integer statefulExpirationMinutes;
    protected Integer maxAddressesPerHostname;
    protected Integer maxAddressesDuringSetup;

    /**
     * Ruft den Wert der isRouteRoundRobin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRouteRoundRobin() {
        return isRouteRoundRobin;
    }

    /**
     * Legt den Wert der isRouteRoundRobin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRouteRoundRobin(Boolean value) {
        this.isRouteRoundRobin = value;
    }

    /**
     * Ruft den Wert der routeTimerSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRouteTimerSeconds() {
        return routeTimerSeconds;
    }

    /**
     * Legt den Wert der routeTimerSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRouteTimerSeconds(Integer value) {
        this.routeTimerSeconds = value;
    }

    /**
     * Ruft den Wert der dnsResolvedAddressSelectionPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RoutingDNSResolvedAddressSelectionPolicy }
     *     
     */
    public RoutingDNSResolvedAddressSelectionPolicy getDnsResolvedAddressSelectionPolicy() {
        return dnsResolvedAddressSelectionPolicy;
    }

    /**
     * Legt den Wert der dnsResolvedAddressSelectionPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RoutingDNSResolvedAddressSelectionPolicy }
     *     
     */
    public void setDnsResolvedAddressSelectionPolicy(RoutingDNSResolvedAddressSelectionPolicy value) {
        this.dnsResolvedAddressSelectionPolicy = value;
    }

    /**
     * Ruft den Wert der statefulExpirationMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStatefulExpirationMinutes() {
        return statefulExpirationMinutes;
    }

    /**
     * Legt den Wert der statefulExpirationMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStatefulExpirationMinutes(Integer value) {
        this.statefulExpirationMinutes = value;
    }

    /**
     * Ruft den Wert der maxAddressesPerHostname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxAddressesPerHostname() {
        return maxAddressesPerHostname;
    }

    /**
     * Legt den Wert der maxAddressesPerHostname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxAddressesPerHostname(Integer value) {
        this.maxAddressesPerHostname = value;
    }

    /**
     * Ruft den Wert der maxAddressesDuringSetup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxAddressesDuringSetup() {
        return maxAddressesDuringSetup;
    }

    /**
     * Legt den Wert der maxAddressesDuringSetup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxAddressesDuringSetup(Integer value) {
        this.maxAddressesDuringSetup = value;
    }

}
