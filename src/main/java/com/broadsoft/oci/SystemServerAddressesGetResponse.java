//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemServerAddressesGetRequest.
 *         Contains a list of system Server Addresses.
 *         See also:
 *           PrimaryInfoGetResponse
 *           PublicClusterGetFullyQualifiedDomainNameResponse
 *           ServingInfoGetResponse
 *       
 * 
 * <p>Java-Klasse für SystemServerAddressesGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemServerAddressesGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="webServerClusterPublicFQDN" type="{}NetAddress" minOccurs="0"/>
 *         <element name="applicationServerClusterPrimaryPublicFQDN" type="{}NetAddress" minOccurs="0"/>
 *         <element name="applicationServerClusterSecondaryPublicFQDN" type="{}NetAddress" minOccurs="0"/>
 *         <element name="applicationServerClusterPrimaryPrivateFQDN" type="{}NetAddress" minOccurs="0"/>
 *         <element name="applicationServerClusterSecondaryPrivateFQDN" type="{}NetAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemServerAddressesGetResponse", propOrder = {
    "webServerClusterPublicFQDN",
    "applicationServerClusterPrimaryPublicFQDN",
    "applicationServerClusterSecondaryPublicFQDN",
    "applicationServerClusterPrimaryPrivateFQDN",
    "applicationServerClusterSecondaryPrivateFQDN"
})
public class SystemServerAddressesGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String webServerClusterPublicFQDN;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String applicationServerClusterPrimaryPublicFQDN;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String applicationServerClusterSecondaryPublicFQDN;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String applicationServerClusterPrimaryPrivateFQDN;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String applicationServerClusterSecondaryPrivateFQDN;

    /**
     * Ruft den Wert der webServerClusterPublicFQDN-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebServerClusterPublicFQDN() {
        return webServerClusterPublicFQDN;
    }

    /**
     * Legt den Wert der webServerClusterPublicFQDN-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebServerClusterPublicFQDN(String value) {
        this.webServerClusterPublicFQDN = value;
    }

    /**
     * Ruft den Wert der applicationServerClusterPrimaryPublicFQDN-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationServerClusterPrimaryPublicFQDN() {
        return applicationServerClusterPrimaryPublicFQDN;
    }

    /**
     * Legt den Wert der applicationServerClusterPrimaryPublicFQDN-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationServerClusterPrimaryPublicFQDN(String value) {
        this.applicationServerClusterPrimaryPublicFQDN = value;
    }

    /**
     * Ruft den Wert der applicationServerClusterSecondaryPublicFQDN-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationServerClusterSecondaryPublicFQDN() {
        return applicationServerClusterSecondaryPublicFQDN;
    }

    /**
     * Legt den Wert der applicationServerClusterSecondaryPublicFQDN-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationServerClusterSecondaryPublicFQDN(String value) {
        this.applicationServerClusterSecondaryPublicFQDN = value;
    }

    /**
     * Ruft den Wert der applicationServerClusterPrimaryPrivateFQDN-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationServerClusterPrimaryPrivateFQDN() {
        return applicationServerClusterPrimaryPrivateFQDN;
    }

    /**
     * Legt den Wert der applicationServerClusterPrimaryPrivateFQDN-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationServerClusterPrimaryPrivateFQDN(String value) {
        this.applicationServerClusterPrimaryPrivateFQDN = value;
    }

    /**
     * Ruft den Wert der applicationServerClusterSecondaryPrivateFQDN-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationServerClusterSecondaryPrivateFQDN() {
        return applicationServerClusterSecondaryPrivateFQDN;
    }

    /**
     * Legt den Wert der applicationServerClusterSecondaryPrivateFQDN-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationServerClusterSecondaryPrivateFQDN(String value) {
        this.applicationServerClusterSecondaryPrivateFQDN = value;
    }

}
