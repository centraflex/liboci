//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a Dialable Caller ID Criteria.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupDialableCallerIDCriteriaModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDialableCallerIDCriteriaModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="name" type="{}DialableCallerIDCriteriaName"/>
 *         <element name="newName" type="{}DialableCallerIDCriteriaName" minOccurs="0"/>
 *         <element name="description" type="{}DialableCallerIDCriteriaDescription" minOccurs="0"/>
 *         <element name="prefixDigits" type="{}DialableCallerIDPrefixDigits" minOccurs="0"/>
 *         <element name="matchCallType" type="{}ReplacementCommunicationBarringCallTypeList" minOccurs="0"/>
 *         <element name="matchAlternateCallIndicator" type="{}ReplacementCommunicationBarringAlternateCallIndicatorList" minOccurs="0"/>
 *         <element name="matchLocalCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchNationalCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchInterlataCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchIntralataCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchInternationalCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchPrivateCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchEmergencyCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchOtherCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDialableCallerIDCriteriaModifyRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "name",
    "newName",
    "description",
    "prefixDigits",
    "matchCallType",
    "matchAlternateCallIndicator",
    "matchLocalCategory",
    "matchNationalCategory",
    "matchInterlataCategory",
    "matchIntralataCategory",
    "matchInternationalCategory",
    "matchPrivateCategory",
    "matchEmergencyCategory",
    "matchOtherCategory"
})
public class GroupDialableCallerIDCriteriaModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newName;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "prefixDigits", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prefixDigits;
    @XmlElementRef(name = "matchCallType", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementCommunicationBarringCallTypeList> matchCallType;
    @XmlElementRef(name = "matchAlternateCallIndicator", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementCommunicationBarringAlternateCallIndicatorList> matchAlternateCallIndicator;
    protected Boolean matchLocalCategory;
    protected Boolean matchNationalCategory;
    protected Boolean matchInterlataCategory;
    protected Boolean matchIntralataCategory;
    protected Boolean matchInternationalCategory;
    protected Boolean matchPrivateCategory;
    protected Boolean matchEmergencyCategory;
    protected Boolean matchOtherCategory;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der newName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewName() {
        return newName;
    }

    /**
     * Legt den Wert der newName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewName(String value) {
        this.newName = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der prefixDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrefixDigits() {
        return prefixDigits;
    }

    /**
     * Legt den Wert der prefixDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrefixDigits(JAXBElement<String> value) {
        this.prefixDigits = value;
    }

    /**
     * Ruft den Wert der matchCallType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringCallTypeList }{@code >}
     *     
     */
    public JAXBElement<ReplacementCommunicationBarringCallTypeList> getMatchCallType() {
        return matchCallType;
    }

    /**
     * Legt den Wert der matchCallType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringCallTypeList }{@code >}
     *     
     */
    public void setMatchCallType(JAXBElement<ReplacementCommunicationBarringCallTypeList> value) {
        this.matchCallType = value;
    }

    /**
     * Ruft den Wert der matchAlternateCallIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringAlternateCallIndicatorList }{@code >}
     *     
     */
    public JAXBElement<ReplacementCommunicationBarringAlternateCallIndicatorList> getMatchAlternateCallIndicator() {
        return matchAlternateCallIndicator;
    }

    /**
     * Legt den Wert der matchAlternateCallIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringAlternateCallIndicatorList }{@code >}
     *     
     */
    public void setMatchAlternateCallIndicator(JAXBElement<ReplacementCommunicationBarringAlternateCallIndicatorList> value) {
        this.matchAlternateCallIndicator = value;
    }

    /**
     * Ruft den Wert der matchLocalCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchLocalCategory() {
        return matchLocalCategory;
    }

    /**
     * Legt den Wert der matchLocalCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchLocalCategory(Boolean value) {
        this.matchLocalCategory = value;
    }

    /**
     * Ruft den Wert der matchNationalCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchNationalCategory() {
        return matchNationalCategory;
    }

    /**
     * Legt den Wert der matchNationalCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchNationalCategory(Boolean value) {
        this.matchNationalCategory = value;
    }

    /**
     * Ruft den Wert der matchInterlataCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchInterlataCategory() {
        return matchInterlataCategory;
    }

    /**
     * Legt den Wert der matchInterlataCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchInterlataCategory(Boolean value) {
        this.matchInterlataCategory = value;
    }

    /**
     * Ruft den Wert der matchIntralataCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchIntralataCategory() {
        return matchIntralataCategory;
    }

    /**
     * Legt den Wert der matchIntralataCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchIntralataCategory(Boolean value) {
        this.matchIntralataCategory = value;
    }

    /**
     * Ruft den Wert der matchInternationalCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchInternationalCategory() {
        return matchInternationalCategory;
    }

    /**
     * Legt den Wert der matchInternationalCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchInternationalCategory(Boolean value) {
        this.matchInternationalCategory = value;
    }

    /**
     * Ruft den Wert der matchPrivateCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchPrivateCategory() {
        return matchPrivateCategory;
    }

    /**
     * Legt den Wert der matchPrivateCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchPrivateCategory(Boolean value) {
        this.matchPrivateCategory = value;
    }

    /**
     * Ruft den Wert der matchEmergencyCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchEmergencyCategory() {
        return matchEmergencyCategory;
    }

    /**
     * Legt den Wert der matchEmergencyCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchEmergencyCategory(Boolean value) {
        this.matchEmergencyCategory = value;
    }

    /**
     * Ruft den Wert der matchOtherCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchOtherCategory() {
        return matchOtherCategory;
    }

    /**
     * Legt den Wert der matchOtherCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchOtherCategory(Boolean value) {
        this.matchOtherCategory = value;
    }

}
