//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with Third-party Voice Mail Support.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         The following elements are only used in AS data mode:
 *           stripDiversionOnVMDestinationRetrieval   
 *       
 * 
 * <p>Java-Klasse für SystemThirdPartyVoiceMailSupportModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemThirdPartyVoiceMailSupportModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="overrideAltCallerIdForVMRetrieval" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="stripDiversionOnVMDestinationRetrieval" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemThirdPartyVoiceMailSupportModifyRequest", propOrder = {
    "overrideAltCallerIdForVMRetrieval",
    "stripDiversionOnVMDestinationRetrieval"
})
public class SystemThirdPartyVoiceMailSupportModifyRequest
    extends OCIRequest
{

    protected Boolean overrideAltCallerIdForVMRetrieval;
    protected Boolean stripDiversionOnVMDestinationRetrieval;

    /**
     * Ruft den Wert der overrideAltCallerIdForVMRetrieval-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOverrideAltCallerIdForVMRetrieval() {
        return overrideAltCallerIdForVMRetrieval;
    }

    /**
     * Legt den Wert der overrideAltCallerIdForVMRetrieval-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverrideAltCallerIdForVMRetrieval(Boolean value) {
        this.overrideAltCallerIdForVMRetrieval = value;
    }

    /**
     * Ruft den Wert der stripDiversionOnVMDestinationRetrieval-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStripDiversionOnVMDestinationRetrieval() {
        return stripDiversionOnVMDestinationRetrieval;
    }

    /**
     * Legt den Wert der stripDiversionOnVMDestinationRetrieval-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStripDiversionOnVMDestinationRetrieval(Boolean value) {
        this.stripDiversionOnVMDestinationRetrieval = value;
    }

}
