//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupAccessDeviceGetAvailableDetailListRequest19.
 *       
 * 
 * <p>Java-Klasse für GroupAccessDeviceGetAvailableDetailListResponse19 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAccessDeviceGetAvailableDetailListResponse19">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="availableAccessDevice" maxOccurs="unbounded" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="accessDevice" type="{}AccessDevice"/>
 *                   <element name="staticRegistrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   <element name="useDomain" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   <element name="staticLineOrdering" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAccessDeviceGetAvailableDetailListResponse19", propOrder = {
    "availableAccessDevice"
})
public class GroupAccessDeviceGetAvailableDetailListResponse19
    extends OCIDataResponse
{

    protected List<GroupAccessDeviceGetAvailableDetailListResponse19 .AvailableAccessDevice> availableAccessDevice;

    /**
     * Gets the value of the availableAccessDevice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the availableAccessDevice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAvailableAccessDevice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupAccessDeviceGetAvailableDetailListResponse19 .AvailableAccessDevice }
     * 
     * 
     * @return
     *     The value of the availableAccessDevice property.
     */
    public List<GroupAccessDeviceGetAvailableDetailListResponse19 .AvailableAccessDevice> getAvailableAccessDevice() {
        if (availableAccessDevice == null) {
            availableAccessDevice = new ArrayList<>();
        }
        return this.availableAccessDevice;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="accessDevice" type="{}AccessDevice"/>
     *         <element name="staticRegistrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         <element name="useDomain" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         <element name="staticLineOrdering" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accessDevice",
        "staticRegistrationCapable",
        "useDomain",
        "staticLineOrdering"
    })
    public static class AvailableAccessDevice {

        @XmlElement(required = true)
        protected AccessDevice accessDevice;
        protected boolean staticRegistrationCapable;
        protected boolean useDomain;
        protected boolean staticLineOrdering;

        /**
         * Ruft den Wert der accessDevice-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AccessDevice }
         *     
         */
        public AccessDevice getAccessDevice() {
            return accessDevice;
        }

        /**
         * Legt den Wert der accessDevice-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AccessDevice }
         *     
         */
        public void setAccessDevice(AccessDevice value) {
            this.accessDevice = value;
        }

        /**
         * Ruft den Wert der staticRegistrationCapable-Eigenschaft ab.
         * 
         */
        public boolean isStaticRegistrationCapable() {
            return staticRegistrationCapable;
        }

        /**
         * Legt den Wert der staticRegistrationCapable-Eigenschaft fest.
         * 
         */
        public void setStaticRegistrationCapable(boolean value) {
            this.staticRegistrationCapable = value;
        }

        /**
         * Ruft den Wert der useDomain-Eigenschaft ab.
         * 
         */
        public boolean isUseDomain() {
            return useDomain;
        }

        /**
         * Legt den Wert der useDomain-Eigenschaft fest.
         * 
         */
        public void setUseDomain(boolean value) {
            this.useDomain = value;
        }

        /**
         * Ruft den Wert der staticLineOrdering-Eigenschaft ab.
         * 
         */
        public boolean isStaticLineOrdering() {
            return staticLineOrdering;
        }

        /**
         * Legt den Wert der staticLineOrdering-Eigenschaft fest.
         * 
         */
        public void setStaticLineOrdering(boolean value) {
            this.staticLineOrdering = value;
        }

    }

}
