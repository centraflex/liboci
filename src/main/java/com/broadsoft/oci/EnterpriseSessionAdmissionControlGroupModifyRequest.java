//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a session admission control group for the enterprise.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         Note that to provision the accessInfoPattern element, the
 *         captureAccessInfoInPaniHeader system parameter needs to be set to "true".
 *       
 * 
 * <p>Java-Klasse für EnterpriseSessionAdmissionControlGroupModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseSessionAdmissionControlGroupModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="name" type="{}SessionAdmissionControlGroupName"/>
 *         <element name="newName" type="{}SessionAdmissionControlGroupName" minOccurs="0"/>
 *         <element name="maxSession" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="maxUserOriginatingSessions" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="maxUserTerminatingSessions" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="reservedSession" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="reservedUserOriginatingSessions" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="reservedUserTerminatingSessions" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="becomeDefaultGroup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="countIntraSACGroupSessions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="deviceList" type="{}ReplacementEnterpriseDeviceList" minOccurs="0"/>
 *         <element name="blockEmergencyAndRepairCallsDueToSACLimits" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="mediaGroupName" type="{}SessionAdmissionControlMediaGroupName" minOccurs="0"/>
 *         <element name="accessInfoPattern" type="{}SessionAdmissionControlAccessInfoPattern" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseSessionAdmissionControlGroupModifyRequest", propOrder = {
    "serviceProviderId",
    "name",
    "newName",
    "maxSession",
    "maxUserOriginatingSessions",
    "maxUserTerminatingSessions",
    "reservedSession",
    "reservedUserOriginatingSessions",
    "reservedUserTerminatingSessions",
    "becomeDefaultGroup",
    "countIntraSACGroupSessions",
    "deviceList",
    "blockEmergencyAndRepairCallsDueToSACLimits",
    "mediaGroupName",
    "accessInfoPattern"
})
public class EnterpriseSessionAdmissionControlGroupModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newName;
    protected Integer maxSession;
    @XmlElementRef(name = "maxUserOriginatingSessions", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxUserOriginatingSessions;
    @XmlElementRef(name = "maxUserTerminatingSessions", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxUserTerminatingSessions;
    protected Integer reservedSession;
    @XmlElementRef(name = "reservedUserOriginatingSessions", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> reservedUserOriginatingSessions;
    @XmlElementRef(name = "reservedUserTerminatingSessions", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> reservedUserTerminatingSessions;
    protected Boolean becomeDefaultGroup;
    protected Boolean countIntraSACGroupSessions;
    @XmlElementRef(name = "deviceList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementEnterpriseDeviceList> deviceList;
    protected Boolean blockEmergencyAndRepairCallsDueToSACLimits;
    @XmlElementRef(name = "mediaGroupName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mediaGroupName;
    @XmlElementRef(name = "accessInfoPattern", type = JAXBElement.class, required = false)
    protected JAXBElement<String> accessInfoPattern;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der newName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewName() {
        return newName;
    }

    /**
     * Legt den Wert der newName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewName(String value) {
        this.newName = value;
    }

    /**
     * Ruft den Wert der maxSession-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxSession() {
        return maxSession;
    }

    /**
     * Legt den Wert der maxSession-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxSession(Integer value) {
        this.maxSession = value;
    }

    /**
     * Ruft den Wert der maxUserOriginatingSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxUserOriginatingSessions() {
        return maxUserOriginatingSessions;
    }

    /**
     * Legt den Wert der maxUserOriginatingSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxUserOriginatingSessions(JAXBElement<Integer> value) {
        this.maxUserOriginatingSessions = value;
    }

    /**
     * Ruft den Wert der maxUserTerminatingSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxUserTerminatingSessions() {
        return maxUserTerminatingSessions;
    }

    /**
     * Legt den Wert der maxUserTerminatingSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxUserTerminatingSessions(JAXBElement<Integer> value) {
        this.maxUserTerminatingSessions = value;
    }

    /**
     * Ruft den Wert der reservedSession-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReservedSession() {
        return reservedSession;
    }

    /**
     * Legt den Wert der reservedSession-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReservedSession(Integer value) {
        this.reservedSession = value;
    }

    /**
     * Ruft den Wert der reservedUserOriginatingSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getReservedUserOriginatingSessions() {
        return reservedUserOriginatingSessions;
    }

    /**
     * Legt den Wert der reservedUserOriginatingSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setReservedUserOriginatingSessions(JAXBElement<Integer> value) {
        this.reservedUserOriginatingSessions = value;
    }

    /**
     * Ruft den Wert der reservedUserTerminatingSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getReservedUserTerminatingSessions() {
        return reservedUserTerminatingSessions;
    }

    /**
     * Legt den Wert der reservedUserTerminatingSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setReservedUserTerminatingSessions(JAXBElement<Integer> value) {
        this.reservedUserTerminatingSessions = value;
    }

    /**
     * Ruft den Wert der becomeDefaultGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBecomeDefaultGroup() {
        return becomeDefaultGroup;
    }

    /**
     * Legt den Wert der becomeDefaultGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBecomeDefaultGroup(Boolean value) {
        this.becomeDefaultGroup = value;
    }

    /**
     * Ruft den Wert der countIntraSACGroupSessions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCountIntraSACGroupSessions() {
        return countIntraSACGroupSessions;
    }

    /**
     * Legt den Wert der countIntraSACGroupSessions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCountIntraSACGroupSessions(Boolean value) {
        this.countIntraSACGroupSessions = value;
    }

    /**
     * Ruft den Wert der deviceList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementEnterpriseDeviceList }{@code >}
     *     
     */
    public JAXBElement<ReplacementEnterpriseDeviceList> getDeviceList() {
        return deviceList;
    }

    /**
     * Legt den Wert der deviceList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementEnterpriseDeviceList }{@code >}
     *     
     */
    public void setDeviceList(JAXBElement<ReplacementEnterpriseDeviceList> value) {
        this.deviceList = value;
    }

    /**
     * Ruft den Wert der blockEmergencyAndRepairCallsDueToSACLimits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBlockEmergencyAndRepairCallsDueToSACLimits() {
        return blockEmergencyAndRepairCallsDueToSACLimits;
    }

    /**
     * Legt den Wert der blockEmergencyAndRepairCallsDueToSACLimits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBlockEmergencyAndRepairCallsDueToSACLimits(Boolean value) {
        this.blockEmergencyAndRepairCallsDueToSACLimits = value;
    }

    /**
     * Ruft den Wert der mediaGroupName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMediaGroupName() {
        return mediaGroupName;
    }

    /**
     * Legt den Wert der mediaGroupName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMediaGroupName(JAXBElement<String> value) {
        this.mediaGroupName = value;
    }

    /**
     * Ruft den Wert der accessInfoPattern-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAccessInfoPattern() {
        return accessInfoPattern;
    }

    /**
     * Legt den Wert der accessInfoPattern-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAccessInfoPattern(JAXBElement<String> value) {
        this.accessInfoPattern = value;
    }

}
