//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupServiceGetAuthorizationListRequest.
 *         Contains three tables, one for the service packs, one for the group services, and one for
 *         the user services.
 *         The user table has the following column headings:
 *           "Service Name", "Authorized", "Assigned", "Limited", "Quantity", "Usage", "Licensed", "Allowed", "User Assignable", "Group Service Assignable".
 *         The group service table has the following column headings:
 *           "Service Name", "Authorized", "Assigned", "Limited", "Quantity", "Usage", "Licensed", "Allowed", "Instance Count".
 *         The service pack table's column headings are:
 *           "Service Pack Name", "Authorized", "Assigned", "Limited", "Allocated", "Allowed", "Usage", "Description".
 *       
 * 
 * <p>Java-Klasse für GroupServiceGetAuthorizationListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupServiceGetAuthorizationListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="servicePacksAuthorizationTable" type="{C}OCITable"/>
 *         <element name="groupServicesAuthorizationTable" type="{C}OCITable"/>
 *         <element name="userServicesAuthorizationTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupServiceGetAuthorizationListResponse", propOrder = {
    "servicePacksAuthorizationTable",
    "groupServicesAuthorizationTable",
    "userServicesAuthorizationTable"
})
public class GroupServiceGetAuthorizationListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable servicePacksAuthorizationTable;
    @XmlElement(required = true)
    protected OCITable groupServicesAuthorizationTable;
    @XmlElement(required = true)
    protected OCITable userServicesAuthorizationTable;

    /**
     * Ruft den Wert der servicePacksAuthorizationTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getServicePacksAuthorizationTable() {
        return servicePacksAuthorizationTable;
    }

    /**
     * Legt den Wert der servicePacksAuthorizationTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setServicePacksAuthorizationTable(OCITable value) {
        this.servicePacksAuthorizationTable = value;
    }

    /**
     * Ruft den Wert der groupServicesAuthorizationTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getGroupServicesAuthorizationTable() {
        return groupServicesAuthorizationTable;
    }

    /**
     * Legt den Wert der groupServicesAuthorizationTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setGroupServicesAuthorizationTable(OCITable value) {
        this.groupServicesAuthorizationTable = value;
    }

    /**
     * Ruft den Wert der userServicesAuthorizationTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserServicesAuthorizationTable() {
        return userServicesAuthorizationTable;
    }

    /**
     * Legt den Wert der userServicesAuthorizationTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserServicesAuthorizationTable(OCITable value) {
        this.userServicesAuthorizationTable = value;
    }

}
