//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a Call Center instance.
 *         The response is either SuccessResponse or ErrorResponse.
 *         
 *         Replaced By: GroupCallCenterModifyInstanceRequest16
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterModifyInstanceRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterModifyInstanceRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceModifyProfile" minOccurs="0"/>
 *         <element name="policy" type="{}HuntPolicy" minOccurs="0"/>
 *         <element name="huntAfterNoAnswer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="noAnswerNumberOfRings" type="{}HuntNoAnswerRings" minOccurs="0"/>
 *         <element name="forwardAfterTimeout" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forwardTimeoutSeconds" type="{}HuntForwardTimeoutSeconds" minOccurs="0"/>
 *         <element name="forwardToPhoneNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="enableVideo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="queueLength" type="{}CallCenterQueueLength" minOccurs="0"/>
 *         <element name="allowAgentLogoff" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="playMusicOnHold" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="playComfortMessage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="timeBetweenComfortMessagesSeconds" type="{}CallCenterTimeBetweenComfortMessagesSeconds" minOccurs="0"/>
 *         <element name="enableGuardTimer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="guardTimerSeconds" type="{}CallCenterGuardTimerSeconds" minOccurs="0"/>
 *         <element name="agentUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *         <element name="allowCallWaitingForAgents" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowCallsToAgentsInWrapUp" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableCallQueueWhenNoAgentsAvailable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="statisticsSource" type="{}CallCenterStatisticsSource" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterModifyInstanceRequest", propOrder = {
    "serviceUserId",
    "serviceInstanceProfile",
    "policy",
    "huntAfterNoAnswer",
    "noAnswerNumberOfRings",
    "forwardAfterTimeout",
    "forwardTimeoutSeconds",
    "forwardToPhoneNumber",
    "enableVideo",
    "queueLength",
    "allowAgentLogoff",
    "playMusicOnHold",
    "playComfortMessage",
    "timeBetweenComfortMessagesSeconds",
    "enableGuardTimer",
    "guardTimerSeconds",
    "agentUserIdList",
    "allowCallWaitingForAgents",
    "allowCallsToAgentsInWrapUp",
    "enableCallQueueWhenNoAgentsAvailable",
    "statisticsSource"
})
public class GroupCallCenterModifyInstanceRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected ServiceInstanceModifyProfile serviceInstanceProfile;
    @XmlSchemaType(name = "token")
    protected HuntPolicy policy;
    protected Boolean huntAfterNoAnswer;
    protected Integer noAnswerNumberOfRings;
    protected Boolean forwardAfterTimeout;
    protected Integer forwardTimeoutSeconds;
    @XmlElementRef(name = "forwardToPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forwardToPhoneNumber;
    protected Boolean enableVideo;
    protected Integer queueLength;
    protected Boolean allowAgentLogoff;
    protected Boolean playMusicOnHold;
    protected Boolean playComfortMessage;
    protected Integer timeBetweenComfortMessagesSeconds;
    protected Boolean enableGuardTimer;
    protected Integer guardTimerSeconds;
    @XmlElementRef(name = "agentUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> agentUserIdList;
    protected Boolean allowCallWaitingForAgents;
    protected Boolean allowCallsToAgentsInWrapUp;
    protected Boolean enableCallQueueWhenNoAgentsAvailable;
    @XmlSchemaType(name = "token")
    protected CallCenterStatisticsSource statisticsSource;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public ServiceInstanceModifyProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceModifyProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der policy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HuntPolicy }
     *     
     */
    public HuntPolicy getPolicy() {
        return policy;
    }

    /**
     * Legt den Wert der policy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HuntPolicy }
     *     
     */
    public void setPolicy(HuntPolicy value) {
        this.policy = value;
    }

    /**
     * Ruft den Wert der huntAfterNoAnswer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHuntAfterNoAnswer() {
        return huntAfterNoAnswer;
    }

    /**
     * Legt den Wert der huntAfterNoAnswer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHuntAfterNoAnswer(Boolean value) {
        this.huntAfterNoAnswer = value;
    }

    /**
     * Ruft den Wert der noAnswerNumberOfRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNoAnswerNumberOfRings() {
        return noAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der noAnswerNumberOfRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNoAnswerNumberOfRings(Integer value) {
        this.noAnswerNumberOfRings = value;
    }

    /**
     * Ruft den Wert der forwardAfterTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForwardAfterTimeout() {
        return forwardAfterTimeout;
    }

    /**
     * Legt den Wert der forwardAfterTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForwardAfterTimeout(Boolean value) {
        this.forwardAfterTimeout = value;
    }

    /**
     * Ruft den Wert der forwardTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getForwardTimeoutSeconds() {
        return forwardTimeoutSeconds;
    }

    /**
     * Legt den Wert der forwardTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setForwardTimeoutSeconds(Integer value) {
        this.forwardTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der forwardToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForwardToPhoneNumber() {
        return forwardToPhoneNumber;
    }

    /**
     * Legt den Wert der forwardToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForwardToPhoneNumber(JAXBElement<String> value) {
        this.forwardToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der enableVideo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableVideo() {
        return enableVideo;
    }

    /**
     * Legt den Wert der enableVideo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableVideo(Boolean value) {
        this.enableVideo = value;
    }

    /**
     * Ruft den Wert der queueLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQueueLength() {
        return queueLength;
    }

    /**
     * Legt den Wert der queueLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQueueLength(Integer value) {
        this.queueLength = value;
    }

    /**
     * Ruft den Wert der allowAgentLogoff-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowAgentLogoff() {
        return allowAgentLogoff;
    }

    /**
     * Legt den Wert der allowAgentLogoff-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowAgentLogoff(Boolean value) {
        this.allowAgentLogoff = value;
    }

    /**
     * Ruft den Wert der playMusicOnHold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayMusicOnHold() {
        return playMusicOnHold;
    }

    /**
     * Legt den Wert der playMusicOnHold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayMusicOnHold(Boolean value) {
        this.playMusicOnHold = value;
    }

    /**
     * Ruft den Wert der playComfortMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayComfortMessage() {
        return playComfortMessage;
    }

    /**
     * Legt den Wert der playComfortMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayComfortMessage(Boolean value) {
        this.playComfortMessage = value;
    }

    /**
     * Ruft den Wert der timeBetweenComfortMessagesSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTimeBetweenComfortMessagesSeconds() {
        return timeBetweenComfortMessagesSeconds;
    }

    /**
     * Legt den Wert der timeBetweenComfortMessagesSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTimeBetweenComfortMessagesSeconds(Integer value) {
        this.timeBetweenComfortMessagesSeconds = value;
    }

    /**
     * Ruft den Wert der enableGuardTimer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableGuardTimer() {
        return enableGuardTimer;
    }

    /**
     * Legt den Wert der enableGuardTimer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableGuardTimer(Boolean value) {
        this.enableGuardTimer = value;
    }

    /**
     * Ruft den Wert der guardTimerSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGuardTimerSeconds() {
        return guardTimerSeconds;
    }

    /**
     * Legt den Wert der guardTimerSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGuardTimerSeconds(Integer value) {
        this.guardTimerSeconds = value;
    }

    /**
     * Ruft den Wert der agentUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getAgentUserIdList() {
        return agentUserIdList;
    }

    /**
     * Legt den Wert der agentUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setAgentUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.agentUserIdList = value;
    }

    /**
     * Ruft den Wert der allowCallWaitingForAgents-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowCallWaitingForAgents() {
        return allowCallWaitingForAgents;
    }

    /**
     * Legt den Wert der allowCallWaitingForAgents-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowCallWaitingForAgents(Boolean value) {
        this.allowCallWaitingForAgents = value;
    }

    /**
     * Ruft den Wert der allowCallsToAgentsInWrapUp-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowCallsToAgentsInWrapUp() {
        return allowCallsToAgentsInWrapUp;
    }

    /**
     * Legt den Wert der allowCallsToAgentsInWrapUp-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowCallsToAgentsInWrapUp(Boolean value) {
        this.allowCallsToAgentsInWrapUp = value;
    }

    /**
     * Ruft den Wert der enableCallQueueWhenNoAgentsAvailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCallQueueWhenNoAgentsAvailable() {
        return enableCallQueueWhenNoAgentsAvailable;
    }

    /**
     * Legt den Wert der enableCallQueueWhenNoAgentsAvailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCallQueueWhenNoAgentsAvailable(Boolean value) {
        this.enableCallQueueWhenNoAgentsAvailable = value;
    }

    /**
     * Ruft den Wert der statisticsSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterStatisticsSource }
     *     
     */
    public CallCenterStatisticsSource getStatisticsSource() {
        return statisticsSource;
    }

    /**
     * Legt den Wert der statisticsSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterStatisticsSource }
     *     
     */
    public void setStatisticsSource(CallCenterStatisticsSource value) {
        this.statisticsSource = value;
    }

}
