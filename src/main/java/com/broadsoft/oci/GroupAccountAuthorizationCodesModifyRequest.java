//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Change the group's account/authorization codes setting.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupAccountAuthorizationCodesModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAccountAuthorizationCodesModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="codeType" type="{}AccountAuthorizationCodeType" minOccurs="0"/>
 *         <element name="numberOfDigits" type="{}AccountAuthorizationCodeNumberOfDigits" minOccurs="0"/>
 *         <element name="allowLocalAndTollFreeCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="mandatoryUsageUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *         <element name="optionalUsageUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAccountAuthorizationCodesModifyRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "codeType",
    "numberOfDigits",
    "allowLocalAndTollFreeCalls",
    "mandatoryUsageUserIdList",
    "optionalUsageUserIdList"
})
public class GroupAccountAuthorizationCodesModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlSchemaType(name = "token")
    protected AccountAuthorizationCodeType codeType;
    protected Integer numberOfDigits;
    protected Boolean allowLocalAndTollFreeCalls;
    @XmlElementRef(name = "mandatoryUsageUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> mandatoryUsageUserIdList;
    @XmlElementRef(name = "optionalUsageUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> optionalUsageUserIdList;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der codeType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccountAuthorizationCodeType }
     *     
     */
    public AccountAuthorizationCodeType getCodeType() {
        return codeType;
    }

    /**
     * Legt den Wert der codeType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountAuthorizationCodeType }
     *     
     */
    public void setCodeType(AccountAuthorizationCodeType value) {
        this.codeType = value;
    }

    /**
     * Ruft den Wert der numberOfDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfDigits() {
        return numberOfDigits;
    }

    /**
     * Legt den Wert der numberOfDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfDigits(Integer value) {
        this.numberOfDigits = value;
    }

    /**
     * Ruft den Wert der allowLocalAndTollFreeCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowLocalAndTollFreeCalls() {
        return allowLocalAndTollFreeCalls;
    }

    /**
     * Legt den Wert der allowLocalAndTollFreeCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowLocalAndTollFreeCalls(Boolean value) {
        this.allowLocalAndTollFreeCalls = value;
    }

    /**
     * Ruft den Wert der mandatoryUsageUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getMandatoryUsageUserIdList() {
        return mandatoryUsageUserIdList;
    }

    /**
     * Legt den Wert der mandatoryUsageUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setMandatoryUsageUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.mandatoryUsageUserIdList = value;
    }

    /**
     * Ruft den Wert der optionalUsageUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getOptionalUsageUserIdList() {
        return optionalUsageUserIdList;
    }

    /**
     * Legt den Wert der optionalUsageUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setOptionalUsageUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.optionalUsageUserIdList = value;
    }

}
