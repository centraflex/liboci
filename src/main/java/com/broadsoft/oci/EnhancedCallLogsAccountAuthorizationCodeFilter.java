//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Filter criteria based on the account code.
 *         When "callsWithCodes" is set to true, all call logs with account/authorization codes are returned. 
 *         When it set to false, all call logs without account/authorization codes are returned.
 *       
 * 
 * <p>Java-Klasse für EnhancedCallLogsAccountAuthorizationCodeFilter complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnhancedCallLogsAccountAuthorizationCodeFilter">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <choice>
 *           <element name="callsWithCodes" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *           <element name="accountAuthorizationCode" type="{}OutgoingCallingPlanAuthorizationCode"/>
 *         </choice>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnhancedCallLogsAccountAuthorizationCodeFilter", propOrder = {
    "callsWithCodes",
    "accountAuthorizationCode"
})
public class EnhancedCallLogsAccountAuthorizationCodeFilter {

    protected Boolean callsWithCodes;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accountAuthorizationCode;

    /**
     * Ruft den Wert der callsWithCodes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCallsWithCodes() {
        return callsWithCodes;
    }

    /**
     * Legt den Wert der callsWithCodes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCallsWithCodes(Boolean value) {
        this.callsWithCodes = value;
    }

    /**
     * Ruft den Wert der accountAuthorizationCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountAuthorizationCode() {
        return accountAuthorizationCode;
    }

    /**
     * Legt den Wert der accountAuthorizationCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountAuthorizationCode(String value) {
        this.accountAuthorizationCode = value;
    }

}
