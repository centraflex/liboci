//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GroupNightForwardingUserServiceActivationMode.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="GroupNightForwardingUserServiceActivationMode">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Use Group"/>
 *     <enumeration value="On"/>
 *     <enumeration value="Off"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "GroupNightForwardingUserServiceActivationMode")
@XmlEnum
public enum GroupNightForwardingUserServiceActivationMode {

    @XmlEnumValue("Use Group")
    USE_GROUP("Use Group"),
    @XmlEnumValue("On")
    ON("On"),
    @XmlEnumValue("Off")
    OFF("Off");
    private final String value;

    GroupNightForwardingUserServiceActivationMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupNightForwardingUserServiceActivationMode fromValue(String v) {
        for (GroupNightForwardingUserServiceActivationMode c: GroupNightForwardingUserServiceActivationMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
