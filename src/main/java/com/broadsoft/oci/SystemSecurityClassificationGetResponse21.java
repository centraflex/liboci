//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemSecurityClassificationGetRequest21.
 *         Contains a table with column headings:
 *         "Name", "Priority".
 *       
 * 
 * <p>Java-Klasse für SystemSecurityClassificationGetResponse21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSecurityClassificationGetResponse21">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="meetMeAnncThreshold" type="{}SecurityClassificationMeetMeConferenceAnnouncementThresholdSeconds"/>
 *         <element name="playTrunkUserSecurityClassificationAnnouncement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="SecurityClassificationTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSecurityClassificationGetResponse21", propOrder = {
    "meetMeAnncThreshold",
    "playTrunkUserSecurityClassificationAnnouncement",
    "securityClassificationTable"
})
public class SystemSecurityClassificationGetResponse21
    extends OCIDataResponse
{

    protected int meetMeAnncThreshold;
    protected boolean playTrunkUserSecurityClassificationAnnouncement;
    @XmlElement(name = "SecurityClassificationTable", required = true)
    protected OCITable securityClassificationTable;

    /**
     * Ruft den Wert der meetMeAnncThreshold-Eigenschaft ab.
     * 
     */
    public int getMeetMeAnncThreshold() {
        return meetMeAnncThreshold;
    }

    /**
     * Legt den Wert der meetMeAnncThreshold-Eigenschaft fest.
     * 
     */
    public void setMeetMeAnncThreshold(int value) {
        this.meetMeAnncThreshold = value;
    }

    /**
     * Ruft den Wert der playTrunkUserSecurityClassificationAnnouncement-Eigenschaft ab.
     * 
     */
    public boolean isPlayTrunkUserSecurityClassificationAnnouncement() {
        return playTrunkUserSecurityClassificationAnnouncement;
    }

    /**
     * Legt den Wert der playTrunkUserSecurityClassificationAnnouncement-Eigenschaft fest.
     * 
     */
    public void setPlayTrunkUserSecurityClassificationAnnouncement(boolean value) {
        this.playTrunkUserSecurityClassificationAnnouncement = value;
    }

    /**
     * Ruft den Wert der securityClassificationTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getSecurityClassificationTable() {
        return securityClassificationTable;
    }

    /**
     * Legt den Wert der securityClassificationTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setSecurityClassificationTable(OCITable value) {
        this.securityClassificationTable = value;
    }

}
