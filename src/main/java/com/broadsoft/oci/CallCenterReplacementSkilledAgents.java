//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         A list of agents grouped by skill levels.
 *       
 * 
 * <p>Java-Klasse für CallCenterReplacementSkilledAgents complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterReplacementSkilledAgents">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="skillLevel" type="{}CallCenterAgentSkillLevel"/>
 *         <element name="agents" type="{}ReplacementUserIdList"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterReplacementSkilledAgents", propOrder = {
    "skillLevel",
    "agents"
})
public class CallCenterReplacementSkilledAgents {

    protected int skillLevel;
    @XmlElement(required = true, nillable = true)
    protected ReplacementUserIdList agents;

    /**
     * Ruft den Wert der skillLevel-Eigenschaft ab.
     * 
     */
    public int getSkillLevel() {
        return skillLevel;
    }

    /**
     * Legt den Wert der skillLevel-Eigenschaft fest.
     * 
     */
    public void setSkillLevel(int value) {
        this.skillLevel = value;
    }

    /**
     * Ruft den Wert der agents-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReplacementUserIdList }
     *     
     */
    public ReplacementUserIdList getAgents() {
        return agents;
    }

    /**
     * Legt den Wert der agents-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplacementUserIdList }
     *     
     */
    public void setAgents(ReplacementUserIdList value) {
        this.agents = value;
    }

}
