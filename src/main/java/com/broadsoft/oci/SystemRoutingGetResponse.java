//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemRoutingGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemRoutingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemRoutingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isRouteRoundRobin" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="routeTimerSeconds" type="{}RouteTimerSeconds"/>
 *         <element name="dnsResolvedAddressSelectionPolicy" type="{}RoutingDNSResolvedAddressSelectionPolicy"/>
 *         <element name="statefulExpirationMinutes" type="{}RoutingStatefulExpirationMinutes"/>
 *         <element name="maxAddressesPerHostname" type="{}RoutingMaxAddresses"/>
 *         <element name="maxAddressesDuringSetup" type="{}RoutingMaxAddresses"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemRoutingGetResponse", propOrder = {
    "isRouteRoundRobin",
    "routeTimerSeconds",
    "dnsResolvedAddressSelectionPolicy",
    "statefulExpirationMinutes",
    "maxAddressesPerHostname",
    "maxAddressesDuringSetup"
})
public class SystemRoutingGetResponse
    extends OCIDataResponse
{

    protected boolean isRouteRoundRobin;
    protected int routeTimerSeconds;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected RoutingDNSResolvedAddressSelectionPolicy dnsResolvedAddressSelectionPolicy;
    protected int statefulExpirationMinutes;
    protected int maxAddressesPerHostname;
    protected int maxAddressesDuringSetup;

    /**
     * Ruft den Wert der isRouteRoundRobin-Eigenschaft ab.
     * 
     */
    public boolean isIsRouteRoundRobin() {
        return isRouteRoundRobin;
    }

    /**
     * Legt den Wert der isRouteRoundRobin-Eigenschaft fest.
     * 
     */
    public void setIsRouteRoundRobin(boolean value) {
        this.isRouteRoundRobin = value;
    }

    /**
     * Ruft den Wert der routeTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getRouteTimerSeconds() {
        return routeTimerSeconds;
    }

    /**
     * Legt den Wert der routeTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setRouteTimerSeconds(int value) {
        this.routeTimerSeconds = value;
    }

    /**
     * Ruft den Wert der dnsResolvedAddressSelectionPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RoutingDNSResolvedAddressSelectionPolicy }
     *     
     */
    public RoutingDNSResolvedAddressSelectionPolicy getDnsResolvedAddressSelectionPolicy() {
        return dnsResolvedAddressSelectionPolicy;
    }

    /**
     * Legt den Wert der dnsResolvedAddressSelectionPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RoutingDNSResolvedAddressSelectionPolicy }
     *     
     */
    public void setDnsResolvedAddressSelectionPolicy(RoutingDNSResolvedAddressSelectionPolicy value) {
        this.dnsResolvedAddressSelectionPolicy = value;
    }

    /**
     * Ruft den Wert der statefulExpirationMinutes-Eigenschaft ab.
     * 
     */
    public int getStatefulExpirationMinutes() {
        return statefulExpirationMinutes;
    }

    /**
     * Legt den Wert der statefulExpirationMinutes-Eigenschaft fest.
     * 
     */
    public void setStatefulExpirationMinutes(int value) {
        this.statefulExpirationMinutes = value;
    }

    /**
     * Ruft den Wert der maxAddressesPerHostname-Eigenschaft ab.
     * 
     */
    public int getMaxAddressesPerHostname() {
        return maxAddressesPerHostname;
    }

    /**
     * Legt den Wert der maxAddressesPerHostname-Eigenschaft fest.
     * 
     */
    public void setMaxAddressesPerHostname(int value) {
        this.maxAddressesPerHostname = value;
    }

    /**
     * Ruft den Wert der maxAddressesDuringSetup-Eigenschaft ab.
     * 
     */
    public int getMaxAddressesDuringSetup() {
        return maxAddressesDuringSetup;
    }

    /**
     * Legt den Wert der maxAddressesDuringSetup-Eigenschaft fest.
     * 
     */
    public void setMaxAddressesDuringSetup(int value) {
        this.maxAddressesDuringSetup = value;
    }

}
