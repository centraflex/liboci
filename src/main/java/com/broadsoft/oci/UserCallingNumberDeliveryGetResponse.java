//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserCallingNumberDeliveryGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserCallingNumberDeliveryGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallingNumberDeliveryGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActiveForExternalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isActiveForInternalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallingNumberDeliveryGetResponse", propOrder = {
    "isActiveForExternalCalls",
    "isActiveForInternalCalls"
})
public class UserCallingNumberDeliveryGetResponse
    extends OCIDataResponse
{

    protected boolean isActiveForExternalCalls;
    protected boolean isActiveForInternalCalls;

    /**
     * Ruft den Wert der isActiveForExternalCalls-Eigenschaft ab.
     * 
     */
    public boolean isIsActiveForExternalCalls() {
        return isActiveForExternalCalls;
    }

    /**
     * Legt den Wert der isActiveForExternalCalls-Eigenschaft fest.
     * 
     */
    public void setIsActiveForExternalCalls(boolean value) {
        this.isActiveForExternalCalls = value;
    }

    /**
     * Ruft den Wert der isActiveForInternalCalls-Eigenschaft ab.
     * 
     */
    public boolean isIsActiveForInternalCalls() {
        return isActiveForInternalCalls;
    }

    /**
     * Legt den Wert der isActiveForInternalCalls-Eigenschaft fest.
     * 
     */
    public void setIsActiveForInternalCalls(boolean value) {
        this.isActiveForInternalCalls = value;
    }

}
