//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserBroadWorksAnywhereGetRequest16sp2.
 *         The phoneNumberTable contains columns: "Phone Number", "Description"
 *       
 * 
 * <p>Java-Klasse für UserBroadWorksAnywhereGetResponse16sp2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksAnywhereGetResponse16sp2">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="alertAllLocationsForClickToDialCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="alertAllLocationsForGroupPagingCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="phoneNumberTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksAnywhereGetResponse16sp2", propOrder = {
    "alertAllLocationsForClickToDialCalls",
    "alertAllLocationsForGroupPagingCalls",
    "phoneNumberTable"
})
public class UserBroadWorksAnywhereGetResponse16Sp2
    extends OCIDataResponse
{

    protected boolean alertAllLocationsForClickToDialCalls;
    protected boolean alertAllLocationsForGroupPagingCalls;
    @XmlElement(required = true)
    protected OCITable phoneNumberTable;

    /**
     * Ruft den Wert der alertAllLocationsForClickToDialCalls-Eigenschaft ab.
     * 
     */
    public boolean isAlertAllLocationsForClickToDialCalls() {
        return alertAllLocationsForClickToDialCalls;
    }

    /**
     * Legt den Wert der alertAllLocationsForClickToDialCalls-Eigenschaft fest.
     * 
     */
    public void setAlertAllLocationsForClickToDialCalls(boolean value) {
        this.alertAllLocationsForClickToDialCalls = value;
    }

    /**
     * Ruft den Wert der alertAllLocationsForGroupPagingCalls-Eigenschaft ab.
     * 
     */
    public boolean isAlertAllLocationsForGroupPagingCalls() {
        return alertAllLocationsForGroupPagingCalls;
    }

    /**
     * Legt den Wert der alertAllLocationsForGroupPagingCalls-Eigenschaft fest.
     * 
     */
    public void setAlertAllLocationsForGroupPagingCalls(boolean value) {
        this.alertAllLocationsForGroupPagingCalls = value;
    }

    /**
     * Ruft den Wert der phoneNumberTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getPhoneNumberTable() {
        return phoneNumberTable;
    }

    /**
     * Legt den Wert der phoneNumberTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setPhoneNumberTable(OCITable value) {
        this.phoneNumberTable = value;
    }

}
