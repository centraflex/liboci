//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         A service provider's or enterprise's voice messaging settings used in the context of add.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderVoiceMessagingGroupSettingsAdd complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderVoiceMessagingGroupSettingsAdd">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="deliveryFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="notificationFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="voicePortalLockoutFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="useSystemDefaultDeliveryFromAddress" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useSystemDefaultNotificationFromAddress" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useSystemDefaultVoicePortalLockoutFromAddress" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderVoiceMessagingGroupSettingsAdd", propOrder = {
    "deliveryFromAddress",
    "notificationFromAddress",
    "voicePortalLockoutFromAddress",
    "useSystemDefaultDeliveryFromAddress",
    "useSystemDefaultNotificationFromAddress",
    "useSystemDefaultVoicePortalLockoutFromAddress"
})
public class ServiceProviderVoiceMessagingGroupSettingsAdd {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deliveryFromAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String notificationFromAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String voicePortalLockoutFromAddress;
    protected boolean useSystemDefaultDeliveryFromAddress;
    protected boolean useSystemDefaultNotificationFromAddress;
    protected boolean useSystemDefaultVoicePortalLockoutFromAddress;

    /**
     * Ruft den Wert der deliveryFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryFromAddress() {
        return deliveryFromAddress;
    }

    /**
     * Legt den Wert der deliveryFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryFromAddress(String value) {
        this.deliveryFromAddress = value;
    }

    /**
     * Ruft den Wert der notificationFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotificationFromAddress() {
        return notificationFromAddress;
    }

    /**
     * Legt den Wert der notificationFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotificationFromAddress(String value) {
        this.notificationFromAddress = value;
    }

    /**
     * Ruft den Wert der voicePortalLockoutFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoicePortalLockoutFromAddress() {
        return voicePortalLockoutFromAddress;
    }

    /**
     * Legt den Wert der voicePortalLockoutFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoicePortalLockoutFromAddress(String value) {
        this.voicePortalLockoutFromAddress = value;
    }

    /**
     * Ruft den Wert der useSystemDefaultDeliveryFromAddress-Eigenschaft ab.
     * 
     */
    public boolean isUseSystemDefaultDeliveryFromAddress() {
        return useSystemDefaultDeliveryFromAddress;
    }

    /**
     * Legt den Wert der useSystemDefaultDeliveryFromAddress-Eigenschaft fest.
     * 
     */
    public void setUseSystemDefaultDeliveryFromAddress(boolean value) {
        this.useSystemDefaultDeliveryFromAddress = value;
    }

    /**
     * Ruft den Wert der useSystemDefaultNotificationFromAddress-Eigenschaft ab.
     * 
     */
    public boolean isUseSystemDefaultNotificationFromAddress() {
        return useSystemDefaultNotificationFromAddress;
    }

    /**
     * Legt den Wert der useSystemDefaultNotificationFromAddress-Eigenschaft fest.
     * 
     */
    public void setUseSystemDefaultNotificationFromAddress(boolean value) {
        this.useSystemDefaultNotificationFromAddress = value;
    }

    /**
     * Ruft den Wert der useSystemDefaultVoicePortalLockoutFromAddress-Eigenschaft ab.
     * 
     */
    public boolean isUseSystemDefaultVoicePortalLockoutFromAddress() {
        return useSystemDefaultVoicePortalLockoutFromAddress;
    }

    /**
     * Legt den Wert der useSystemDefaultVoicePortalLockoutFromAddress-Eigenschaft fest.
     * 
     */
    public void setUseSystemDefaultVoicePortalLockoutFromAddress(boolean value) {
        this.useSystemDefaultVoicePortalLockoutFromAddress = value;
    }

}
