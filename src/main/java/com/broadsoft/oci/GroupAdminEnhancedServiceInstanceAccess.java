//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GroupAdminEnhancedServiceInstanceAccess.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="GroupAdminEnhancedServiceInstanceAccess">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Full"/>
 *     <enumeration value="Modify-Only"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "GroupAdminEnhancedServiceInstanceAccess")
@XmlEnum
public enum GroupAdminEnhancedServiceInstanceAccess {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("Modify-Only")
    MODIFY_ONLY("Modify-Only");
    private final String value;

    GroupAdminEnhancedServiceInstanceAccess(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupAdminEnhancedServiceInstanceAccess fromValue(String v) {
        for (GroupAdminEnhancedServiceInstanceAccess c: GroupAdminEnhancedServiceInstanceAccess.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
