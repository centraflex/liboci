//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für InterceptOutboundCall.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="InterceptOutboundCall">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Block All"/>
 *     <enumeration value="Allow Outbound Local Calls"/>
 *     <enumeration value="Allow Outbound Enterprise And Group Calls"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "InterceptOutboundCall")
@XmlEnum
public enum InterceptOutboundCall {

    @XmlEnumValue("Block All")
    BLOCK_ALL("Block All"),
    @XmlEnumValue("Allow Outbound Local Calls")
    ALLOW_OUTBOUND_LOCAL_CALLS("Allow Outbound Local Calls"),
    @XmlEnumValue("Allow Outbound Enterprise And Group Calls")
    ALLOW_OUTBOUND_ENTERPRISE_AND_GROUP_CALLS("Allow Outbound Enterprise And Group Calls");
    private final String value;

    InterceptOutboundCall(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InterceptOutboundCall fromValue(String v) {
        for (InterceptOutboundCall c: InterceptOutboundCall.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
