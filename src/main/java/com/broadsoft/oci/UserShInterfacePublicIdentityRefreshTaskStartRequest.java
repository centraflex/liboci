//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Dispatches a Public Identity refresh task on the local Application Server node for the specified public identity.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserShInterfacePublicIdentityRefreshTaskStartRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserShInterfacePublicIdentityRefreshTaskStartRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="publicUserIdentity" type="{}PublicUserIdentity"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserShInterfacePublicIdentityRefreshTaskStartRequest", propOrder = {
    "publicUserIdentity"
})
public class UserShInterfacePublicIdentityRefreshTaskStartRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    protected PublicUserIdentity publicUserIdentity;

    /**
     * Ruft den Wert der publicUserIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PublicUserIdentity }
     *     
     */
    public PublicUserIdentity getPublicUserIdentity() {
        return publicUserIdentity;
    }

    /**
     * Legt den Wert der publicUserIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PublicUserIdentity }
     *     
     */
    public void setPublicUserIdentity(PublicUserIdentity value) {
        this.publicUserIdentity = value;
    }

}
