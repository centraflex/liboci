//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a call center's bounced call settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           enableTransfer
 *           transferPhoneNumber
 *           bounceCallWhenAgentUnavailable
 *           alertCallCenterCallOnHold
 *           alertCallCenterCallOnHoldSeconds
 *           bounceCallCenterCallOnHold
 *           bounceCallCenterCallOnHoldSeconds
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterBouncedCallModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterBouncedCallModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="numberOfRingsBeforeBouncingCall" type="{}HuntNoAnswerRings" minOccurs="0"/>
 *         <element name="enableTransfer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="transferPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="bounceCallWhenAgentUnavailable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="alertCallCenterCallOnHold" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="alertCallCenterCallOnHoldSeconds" type="{}AlertCallCenterCallOnHoldSeconds" minOccurs="0"/>
 *         <element name="bounceCallCenterCallOnHold" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="bounceCallCenterCallOnHoldSeconds" type="{}BounceCallCenterCallOnHoldSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterBouncedCallModifyRequest", propOrder = {
    "serviceUserId",
    "isActive",
    "numberOfRingsBeforeBouncingCall",
    "enableTransfer",
    "transferPhoneNumber",
    "bounceCallWhenAgentUnavailable",
    "alertCallCenterCallOnHold",
    "alertCallCenterCallOnHoldSeconds",
    "bounceCallCenterCallOnHold",
    "bounceCallCenterCallOnHoldSeconds"
})
public class GroupCallCenterBouncedCallModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected Boolean isActive;
    protected Integer numberOfRingsBeforeBouncingCall;
    protected Boolean enableTransfer;
    @XmlElementRef(name = "transferPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transferPhoneNumber;
    protected Boolean bounceCallWhenAgentUnavailable;
    protected Boolean alertCallCenterCallOnHold;
    protected Integer alertCallCenterCallOnHoldSeconds;
    protected Boolean bounceCallCenterCallOnHold;
    protected Integer bounceCallCenterCallOnHoldSeconds;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der numberOfRingsBeforeBouncingCall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfRingsBeforeBouncingCall() {
        return numberOfRingsBeforeBouncingCall;
    }

    /**
     * Legt den Wert der numberOfRingsBeforeBouncingCall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfRingsBeforeBouncingCall(Integer value) {
        this.numberOfRingsBeforeBouncingCall = value;
    }

    /**
     * Ruft den Wert der enableTransfer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableTransfer() {
        return enableTransfer;
    }

    /**
     * Legt den Wert der enableTransfer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableTransfer(Boolean value) {
        this.enableTransfer = value;
    }

    /**
     * Ruft den Wert der transferPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransferPhoneNumber() {
        return transferPhoneNumber;
    }

    /**
     * Legt den Wert der transferPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransferPhoneNumber(JAXBElement<String> value) {
        this.transferPhoneNumber = value;
    }

    /**
     * Ruft den Wert der bounceCallWhenAgentUnavailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBounceCallWhenAgentUnavailable() {
        return bounceCallWhenAgentUnavailable;
    }

    /**
     * Legt den Wert der bounceCallWhenAgentUnavailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBounceCallWhenAgentUnavailable(Boolean value) {
        this.bounceCallWhenAgentUnavailable = value;
    }

    /**
     * Ruft den Wert der alertCallCenterCallOnHold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlertCallCenterCallOnHold() {
        return alertCallCenterCallOnHold;
    }

    /**
     * Legt den Wert der alertCallCenterCallOnHold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlertCallCenterCallOnHold(Boolean value) {
        this.alertCallCenterCallOnHold = value;
    }

    /**
     * Ruft den Wert der alertCallCenterCallOnHoldSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAlertCallCenterCallOnHoldSeconds() {
        return alertCallCenterCallOnHoldSeconds;
    }

    /**
     * Legt den Wert der alertCallCenterCallOnHoldSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAlertCallCenterCallOnHoldSeconds(Integer value) {
        this.alertCallCenterCallOnHoldSeconds = value;
    }

    /**
     * Ruft den Wert der bounceCallCenterCallOnHold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBounceCallCenterCallOnHold() {
        return bounceCallCenterCallOnHold;
    }

    /**
     * Legt den Wert der bounceCallCenterCallOnHold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBounceCallCenterCallOnHold(Boolean value) {
        this.bounceCallCenterCallOnHold = value;
    }

    /**
     * Ruft den Wert der bounceCallCenterCallOnHoldSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBounceCallCenterCallOnHoldSeconds() {
        return bounceCallCenterCallOnHoldSeconds;
    }

    /**
     * Legt den Wert der bounceCallCenterCallOnHoldSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBounceCallCenterCallOnHoldSeconds(Integer value) {
        this.bounceCallCenterCallOnHoldSeconds = value;
    }

}
