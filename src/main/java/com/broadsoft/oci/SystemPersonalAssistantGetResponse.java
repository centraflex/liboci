//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the SystemPersonalAssistantGetRequest.
 *         Returns system Personal Assistant Parameters.
 *       
 * 
 * <p>Java-Klasse für SystemPersonalAssistantGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPersonalAssistantGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="transferToAttendantKey" type="{}DigitAny"/>
 *         <element name="transferToVoiceMessagingKey" type="{}DigitAny"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPersonalAssistantGetResponse", propOrder = {
    "transferToAttendantKey",
    "transferToVoiceMessagingKey"
})
public class SystemPersonalAssistantGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String transferToAttendantKey;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String transferToVoiceMessagingKey;

    /**
     * Ruft den Wert der transferToAttendantKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferToAttendantKey() {
        return transferToAttendantKey;
    }

    /**
     * Legt den Wert der transferToAttendantKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferToAttendantKey(String value) {
        this.transferToAttendantKey = value;
    }

    /**
     * Ruft den Wert der transferToVoiceMessagingKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferToVoiceMessagingKey() {
        return transferToVoiceMessagingKey;
    }

    /**
     * Legt den Wert der transferToVoiceMessagingKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferToVoiceMessagingKey(String value) {
        this.transferToVoiceMessagingKey = value;
    }

}
