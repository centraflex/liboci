//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a holiday schedule in a group.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupHolidayScheduleModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupHolidayScheduleModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="holidayScheduleName" type="{}ScheduleName"/>
 *         <element name="newHolidayScheduleName" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="holiday01" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday02" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday03" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday04" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday05" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday06" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday07" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday08" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday09" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday10" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday11" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday12" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday13" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday14" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday15" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday16" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday17" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday18" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday19" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday20" type="{}Holiday" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupHolidayScheduleModifyRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "holidayScheduleName",
    "newHolidayScheduleName",
    "holiday01",
    "holiday02",
    "holiday03",
    "holiday04",
    "holiday05",
    "holiday06",
    "holiday07",
    "holiday08",
    "holiday09",
    "holiday10",
    "holiday11",
    "holiday12",
    "holiday13",
    "holiday14",
    "holiday15",
    "holiday16",
    "holiday17",
    "holiday18",
    "holiday19",
    "holiday20"
})
public class GroupHolidayScheduleModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String holidayScheduleName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newHolidayScheduleName;
    @XmlElementRef(name = "holiday01", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday01;
    @XmlElementRef(name = "holiday02", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday02;
    @XmlElementRef(name = "holiday03", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday03;
    @XmlElementRef(name = "holiday04", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday04;
    @XmlElementRef(name = "holiday05", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday05;
    @XmlElementRef(name = "holiday06", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday06;
    @XmlElementRef(name = "holiday07", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday07;
    @XmlElementRef(name = "holiday08", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday08;
    @XmlElementRef(name = "holiday09", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday09;
    @XmlElementRef(name = "holiday10", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday10;
    @XmlElementRef(name = "holiday11", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday11;
    @XmlElementRef(name = "holiday12", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday12;
    @XmlElementRef(name = "holiday13", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday13;
    @XmlElementRef(name = "holiday14", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday14;
    @XmlElementRef(name = "holiday15", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday15;
    @XmlElementRef(name = "holiday16", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday16;
    @XmlElementRef(name = "holiday17", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday17;
    @XmlElementRef(name = "holiday18", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday18;
    @XmlElementRef(name = "holiday19", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday19;
    @XmlElementRef(name = "holiday20", type = JAXBElement.class, required = false)
    protected JAXBElement<Holiday> holiday20;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der holidayScheduleName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHolidayScheduleName() {
        return holidayScheduleName;
    }

    /**
     * Legt den Wert der holidayScheduleName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHolidayScheduleName(String value) {
        this.holidayScheduleName = value;
    }

    /**
     * Ruft den Wert der newHolidayScheduleName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewHolidayScheduleName() {
        return newHolidayScheduleName;
    }

    /**
     * Legt den Wert der newHolidayScheduleName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewHolidayScheduleName(String value) {
        this.newHolidayScheduleName = value;
    }

    /**
     * Ruft den Wert der holiday01-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday01() {
        return holiday01;
    }

    /**
     * Legt den Wert der holiday01-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday01(JAXBElement<Holiday> value) {
        this.holiday01 = value;
    }

    /**
     * Ruft den Wert der holiday02-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday02() {
        return holiday02;
    }

    /**
     * Legt den Wert der holiday02-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday02(JAXBElement<Holiday> value) {
        this.holiday02 = value;
    }

    /**
     * Ruft den Wert der holiday03-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday03() {
        return holiday03;
    }

    /**
     * Legt den Wert der holiday03-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday03(JAXBElement<Holiday> value) {
        this.holiday03 = value;
    }

    /**
     * Ruft den Wert der holiday04-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday04() {
        return holiday04;
    }

    /**
     * Legt den Wert der holiday04-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday04(JAXBElement<Holiday> value) {
        this.holiday04 = value;
    }

    /**
     * Ruft den Wert der holiday05-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday05() {
        return holiday05;
    }

    /**
     * Legt den Wert der holiday05-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday05(JAXBElement<Holiday> value) {
        this.holiday05 = value;
    }

    /**
     * Ruft den Wert der holiday06-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday06() {
        return holiday06;
    }

    /**
     * Legt den Wert der holiday06-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday06(JAXBElement<Holiday> value) {
        this.holiday06 = value;
    }

    /**
     * Ruft den Wert der holiday07-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday07() {
        return holiday07;
    }

    /**
     * Legt den Wert der holiday07-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday07(JAXBElement<Holiday> value) {
        this.holiday07 = value;
    }

    /**
     * Ruft den Wert der holiday08-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday08() {
        return holiday08;
    }

    /**
     * Legt den Wert der holiday08-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday08(JAXBElement<Holiday> value) {
        this.holiday08 = value;
    }

    /**
     * Ruft den Wert der holiday09-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday09() {
        return holiday09;
    }

    /**
     * Legt den Wert der holiday09-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday09(JAXBElement<Holiday> value) {
        this.holiday09 = value;
    }

    /**
     * Ruft den Wert der holiday10-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday10() {
        return holiday10;
    }

    /**
     * Legt den Wert der holiday10-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday10(JAXBElement<Holiday> value) {
        this.holiday10 = value;
    }

    /**
     * Ruft den Wert der holiday11-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday11() {
        return holiday11;
    }

    /**
     * Legt den Wert der holiday11-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday11(JAXBElement<Holiday> value) {
        this.holiday11 = value;
    }

    /**
     * Ruft den Wert der holiday12-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday12() {
        return holiday12;
    }

    /**
     * Legt den Wert der holiday12-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday12(JAXBElement<Holiday> value) {
        this.holiday12 = value;
    }

    /**
     * Ruft den Wert der holiday13-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday13() {
        return holiday13;
    }

    /**
     * Legt den Wert der holiday13-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday13(JAXBElement<Holiday> value) {
        this.holiday13 = value;
    }

    /**
     * Ruft den Wert der holiday14-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday14() {
        return holiday14;
    }

    /**
     * Legt den Wert der holiday14-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday14(JAXBElement<Holiday> value) {
        this.holiday14 = value;
    }

    /**
     * Ruft den Wert der holiday15-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday15() {
        return holiday15;
    }

    /**
     * Legt den Wert der holiday15-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday15(JAXBElement<Holiday> value) {
        this.holiday15 = value;
    }

    /**
     * Ruft den Wert der holiday16-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday16() {
        return holiday16;
    }

    /**
     * Legt den Wert der holiday16-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday16(JAXBElement<Holiday> value) {
        this.holiday16 = value;
    }

    /**
     * Ruft den Wert der holiday17-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday17() {
        return holiday17;
    }

    /**
     * Legt den Wert der holiday17-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday17(JAXBElement<Holiday> value) {
        this.holiday17 = value;
    }

    /**
     * Ruft den Wert der holiday18-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday18() {
        return holiday18;
    }

    /**
     * Legt den Wert der holiday18-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday18(JAXBElement<Holiday> value) {
        this.holiday18 = value;
    }

    /**
     * Ruft den Wert der holiday19-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday19() {
        return holiday19;
    }

    /**
     * Legt den Wert der holiday19-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday19(JAXBElement<Holiday> value) {
        this.holiday19 = value;
    }

    /**
     * Ruft den Wert der holiday20-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public JAXBElement<Holiday> getHoliday20() {
        return holiday20;
    }

    /**
     * Legt den Wert der holiday20-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Holiday }{@code >}
     *     
     */
    public void setHoliday20(JAXBElement<Holiday> value) {
        this.holiday20 = value;
    }

}
