//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify Routing Profile system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemRoutingProfileParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemRoutingProfileParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enablePermissiveRouting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemRoutingProfileParametersModifyRequest", propOrder = {
    "enablePermissiveRouting"
})
public class SystemRoutingProfileParametersModifyRequest
    extends OCIRequest
{

    protected boolean enablePermissiveRouting;

    /**
     * Ruft den Wert der enablePermissiveRouting-Eigenschaft ab.
     * 
     */
    public boolean isEnablePermissiveRouting() {
        return enablePermissiveRouting;
    }

    /**
     * Legt den Wert der enablePermissiveRouting-Eigenschaft fest.
     * 
     */
    public void setEnablePermissiveRouting(boolean value) {
        this.enablePermissiveRouting = value;
    }

}
