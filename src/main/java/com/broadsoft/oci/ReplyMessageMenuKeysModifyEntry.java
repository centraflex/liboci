//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal reply message menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für ReplyMessageMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ReplyMessageMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="sendReplyToCaller" type="{}DigitAny" minOccurs="0"/>
 *         <element name="changeCurrentReply" type="{}DigitAny" minOccurs="0"/>
 *         <element name="listenToCurrentReply" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setOrClearUrgentIndicator" type="{}DigitAny" minOccurs="0"/>
 *         <element name="setOrClearConfidentialIndicator" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReplyMessageMenuKeysModifyEntry", propOrder = {
    "sendReplyToCaller",
    "changeCurrentReply",
    "listenToCurrentReply",
    "setOrClearUrgentIndicator",
    "setOrClearConfidentialIndicator",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class ReplyMessageMenuKeysModifyEntry {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sendReplyToCaller;
    @XmlElementRef(name = "changeCurrentReply", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changeCurrentReply;
    @XmlElementRef(name = "listenToCurrentReply", type = JAXBElement.class, required = false)
    protected JAXBElement<String> listenToCurrentReply;
    @XmlElementRef(name = "setOrClearUrgentIndicator", type = JAXBElement.class, required = false)
    protected JAXBElement<String> setOrClearUrgentIndicator;
    @XmlElementRef(name = "setOrClearConfidentialIndicator", type = JAXBElement.class, required = false)
    protected JAXBElement<String> setOrClearConfidentialIndicator;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repeatMenu;

    /**
     * Ruft den Wert der sendReplyToCaller-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendReplyToCaller() {
        return sendReplyToCaller;
    }

    /**
     * Legt den Wert der sendReplyToCaller-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendReplyToCaller(String value) {
        this.sendReplyToCaller = value;
    }

    /**
     * Ruft den Wert der changeCurrentReply-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangeCurrentReply() {
        return changeCurrentReply;
    }

    /**
     * Legt den Wert der changeCurrentReply-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangeCurrentReply(JAXBElement<String> value) {
        this.changeCurrentReply = value;
    }

    /**
     * Ruft den Wert der listenToCurrentReply-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getListenToCurrentReply() {
        return listenToCurrentReply;
    }

    /**
     * Legt den Wert der listenToCurrentReply-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setListenToCurrentReply(JAXBElement<String> value) {
        this.listenToCurrentReply = value;
    }

    /**
     * Ruft den Wert der setOrClearUrgentIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSetOrClearUrgentIndicator() {
        return setOrClearUrgentIndicator;
    }

    /**
     * Legt den Wert der setOrClearUrgentIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSetOrClearUrgentIndicator(JAXBElement<String> value) {
        this.setOrClearUrgentIndicator = value;
    }

    /**
     * Ruft den Wert der setOrClearConfidentialIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSetOrClearConfidentialIndicator() {
        return setOrClearConfidentialIndicator;
    }

    /**
     * Legt den Wert der setOrClearConfidentialIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSetOrClearConfidentialIndicator(JAXBElement<String> value) {
        this.setOrClearConfidentialIndicator = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepeatMenu(JAXBElement<String> value) {
        this.repeatMenu = value;
    }

}
