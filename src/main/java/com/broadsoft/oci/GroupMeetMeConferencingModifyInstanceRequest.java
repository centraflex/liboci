//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a Meet-Me Conferencing bridge.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupMeetMeConferencingModifyInstanceRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupMeetMeConferencingModifyInstanceRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceModifyProfile" minOccurs="0"/>
 *         <element name="allocatedPorts" type="{}MeetMeConferencingConferencePorts" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="securityPinLength" type="{}MeetMeConferencingConferenceSecurityPinLength" minOccurs="0"/>
 *         <element name="allowIndividualOutDial" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="operatorNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="conferenceHostUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *         <element name="playWarningPrompt" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="conferenceEndWarningPromptMinutes" type="{}MeetMeConferencingConferenceEndWarningPromptMinutes" minOccurs="0"/>
 *         <element name="enableMaxConferenceDuration" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxConferenceDurationMinutes" type="{}MeetMeConferencingConferenceDuration" minOccurs="0"/>
 *         <element name="maxScheduledConferenceDurationMinutes" type="{}MeetMeConferencingConferenceDuration" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupMeetMeConferencingModifyInstanceRequest", propOrder = {
    "serviceUserId",
    "serviceInstanceProfile",
    "allocatedPorts",
    "networkClassOfService",
    "securityPinLength",
    "allowIndividualOutDial",
    "operatorNumber",
    "conferenceHostUserIdList",
    "playWarningPrompt",
    "conferenceEndWarningPromptMinutes",
    "enableMaxConferenceDuration",
    "maxConferenceDurationMinutes",
    "maxScheduledConferenceDurationMinutes"
})
public class GroupMeetMeConferencingModifyInstanceRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected ServiceInstanceModifyProfile serviceInstanceProfile;
    protected MeetMeConferencingConferencePorts allocatedPorts;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    protected Integer securityPinLength;
    protected Boolean allowIndividualOutDial;
    @XmlElementRef(name = "operatorNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> operatorNumber;
    @XmlElementRef(name = "conferenceHostUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> conferenceHostUserIdList;
    protected Boolean playWarningPrompt;
    protected Integer conferenceEndWarningPromptMinutes;
    protected Boolean enableMaxConferenceDuration;
    protected MeetMeConferencingConferenceDuration maxConferenceDurationMinutes;
    protected MeetMeConferencingConferenceDuration maxScheduledConferenceDurationMinutes;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public ServiceInstanceModifyProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceModifyProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der allocatedPorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferencePorts }
     *     
     */
    public MeetMeConferencingConferencePorts getAllocatedPorts() {
        return allocatedPorts;
    }

    /**
     * Legt den Wert der allocatedPorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferencePorts }
     *     
     */
    public void setAllocatedPorts(MeetMeConferencingConferencePorts value) {
        this.allocatedPorts = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Ruft den Wert der securityPinLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSecurityPinLength() {
        return securityPinLength;
    }

    /**
     * Legt den Wert der securityPinLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSecurityPinLength(Integer value) {
        this.securityPinLength = value;
    }

    /**
     * Ruft den Wert der allowIndividualOutDial-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowIndividualOutDial() {
        return allowIndividualOutDial;
    }

    /**
     * Legt den Wert der allowIndividualOutDial-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowIndividualOutDial(Boolean value) {
        this.allowIndividualOutDial = value;
    }

    /**
     * Ruft den Wert der operatorNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOperatorNumber() {
        return operatorNumber;
    }

    /**
     * Legt den Wert der operatorNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOperatorNumber(JAXBElement<String> value) {
        this.operatorNumber = value;
    }

    /**
     * Ruft den Wert der conferenceHostUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getConferenceHostUserIdList() {
        return conferenceHostUserIdList;
    }

    /**
     * Legt den Wert der conferenceHostUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setConferenceHostUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.conferenceHostUserIdList = value;
    }

    /**
     * Ruft den Wert der playWarningPrompt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayWarningPrompt() {
        return playWarningPrompt;
    }

    /**
     * Legt den Wert der playWarningPrompt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayWarningPrompt(Boolean value) {
        this.playWarningPrompt = value;
    }

    /**
     * Ruft den Wert der conferenceEndWarningPromptMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getConferenceEndWarningPromptMinutes() {
        return conferenceEndWarningPromptMinutes;
    }

    /**
     * Legt den Wert der conferenceEndWarningPromptMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setConferenceEndWarningPromptMinutes(Integer value) {
        this.conferenceEndWarningPromptMinutes = value;
    }

    /**
     * Ruft den Wert der enableMaxConferenceDuration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMaxConferenceDuration() {
        return enableMaxConferenceDuration;
    }

    /**
     * Legt den Wert der enableMaxConferenceDuration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMaxConferenceDuration(Boolean value) {
        this.enableMaxConferenceDuration = value;
    }

    /**
     * Ruft den Wert der maxConferenceDurationMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferenceDuration }
     *     
     */
    public MeetMeConferencingConferenceDuration getMaxConferenceDurationMinutes() {
        return maxConferenceDurationMinutes;
    }

    /**
     * Legt den Wert der maxConferenceDurationMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferenceDuration }
     *     
     */
    public void setMaxConferenceDurationMinutes(MeetMeConferencingConferenceDuration value) {
        this.maxConferenceDurationMinutes = value;
    }

    /**
     * Ruft den Wert der maxScheduledConferenceDurationMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferenceDuration }
     *     
     */
    public MeetMeConferencingConferenceDuration getMaxScheduledConferenceDurationMinutes() {
        return maxScheduledConferenceDurationMinutes;
    }

    /**
     * Legt den Wert der maxScheduledConferenceDurationMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferenceDuration }
     *     
     */
    public void setMaxScheduledConferenceDurationMinutes(MeetMeConferencingConferenceDuration value) {
        this.maxScheduledConferenceDurationMinutes = value;
    }

}
