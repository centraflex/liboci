//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Device Management System device type options during a modify request.
 * 
 *         The following data elements are only used in AS data mode and ignored in XS data mode:
 *           enableDeviceActivation
 *           deviceModel
 *       
 * 
 * <p>Java-Klasse für DeviceManagementDeviceTypeModifyOptions22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="DeviceManagementDeviceTypeModifyOptions22">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="deviceAccessProtocol" type="{}DeviceAccessProtocol22" minOccurs="0"/>
 *         <element name="tagMode" type="{}DeviceManagementTagMode" minOccurs="0"/>
 *         <element name="tagSet" type="{}DeviceManagementTagSet" minOccurs="0"/>
 *         <element name="allowDeviceProfileCustomTagSet" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowGroupCustomTagSet" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowSpCustomTagSet" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="sendEmailUponResetFailure" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="deviceAccessNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="deviceAccessPort" type="{}Port" minOccurs="0"/>
 *         <element name="deviceAccessContext" type="{}DeviceAccessContext" minOccurs="0"/>
 *         <element name="defaultDeviceLanguage" type="{}DeviceLanguage" minOccurs="0"/>
 *         <element name="defaultDeviceEncoding" type="{}Encoding" minOccurs="0"/>
 *         <element name="accessDeviceCredentials" type="{}DeviceManagementUserNamePassword16" minOccurs="0"/>
 *         <element name="useHttpDigestAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="macBasedFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="userNamePasswordFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="macInNonRequestURI" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="macInCert" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="macFormatInNonRequestURI" type="{}DeviceManagementAccessURI" minOccurs="0"/>
 *         <element name="enableDeviceActivation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="deviceModel" type="{}AccessDeviceModel" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeviceManagementDeviceTypeModifyOptions22", propOrder = {
    "deviceAccessProtocol",
    "tagMode",
    "tagSet",
    "allowDeviceProfileCustomTagSet",
    "allowGroupCustomTagSet",
    "allowSpCustomTagSet",
    "sendEmailUponResetFailure",
    "deviceAccessNetAddress",
    "deviceAccessPort",
    "deviceAccessContext",
    "defaultDeviceLanguage",
    "defaultDeviceEncoding",
    "accessDeviceCredentials",
    "useHttpDigestAuthentication",
    "macBasedFileAuthentication",
    "userNamePasswordFileAuthentication",
    "macInNonRequestURI",
    "macInCert",
    "macFormatInNonRequestURI",
    "enableDeviceActivation",
    "deviceModel"
})
public class DeviceManagementDeviceTypeModifyOptions22 {

    @XmlSchemaType(name = "token")
    protected DeviceAccessProtocol22 deviceAccessProtocol;
    @XmlSchemaType(name = "token")
    protected DeviceManagementTagMode tagMode;
    @XmlElementRef(name = "tagSet", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tagSet;
    protected Boolean allowDeviceProfileCustomTagSet;
    protected Boolean allowGroupCustomTagSet;
    protected Boolean allowSpCustomTagSet;
    protected Boolean sendEmailUponResetFailure;
    @XmlElementRef(name = "deviceAccessNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deviceAccessNetAddress;
    @XmlElementRef(name = "deviceAccessPort", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> deviceAccessPort;
    @XmlElementRef(name = "deviceAccessContext", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deviceAccessContext;
    @XmlElementRef(name = "defaultDeviceLanguage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultDeviceLanguage;
    @XmlElementRef(name = "defaultDeviceEncoding", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultDeviceEncoding;
    @XmlElementRef(name = "accessDeviceCredentials", type = JAXBElement.class, required = false)
    protected JAXBElement<DeviceManagementUserNamePassword16> accessDeviceCredentials;
    protected Boolean useHttpDigestAuthentication;
    protected Boolean macBasedFileAuthentication;
    protected Boolean userNamePasswordFileAuthentication;
    protected Boolean macInNonRequestURI;
    protected Boolean macInCert;
    @XmlElementRef(name = "macFormatInNonRequestURI", type = JAXBElement.class, required = false)
    protected JAXBElement<String> macFormatInNonRequestURI;
    protected Boolean enableDeviceActivation;
    @XmlElementRef(name = "deviceModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deviceModel;

    /**
     * Ruft den Wert der deviceAccessProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceAccessProtocol22 }
     *     
     */
    public DeviceAccessProtocol22 getDeviceAccessProtocol() {
        return deviceAccessProtocol;
    }

    /**
     * Legt den Wert der deviceAccessProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceAccessProtocol22 }
     *     
     */
    public void setDeviceAccessProtocol(DeviceAccessProtocol22 value) {
        this.deviceAccessProtocol = value;
    }

    /**
     * Ruft den Wert der tagMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementTagMode }
     *     
     */
    public DeviceManagementTagMode getTagMode() {
        return tagMode;
    }

    /**
     * Legt den Wert der tagMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementTagMode }
     *     
     */
    public void setTagMode(DeviceManagementTagMode value) {
        this.tagMode = value;
    }

    /**
     * Ruft den Wert der tagSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTagSet() {
        return tagSet;
    }

    /**
     * Legt den Wert der tagSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTagSet(JAXBElement<String> value) {
        this.tagSet = value;
    }

    /**
     * Ruft den Wert der allowDeviceProfileCustomTagSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowDeviceProfileCustomTagSet() {
        return allowDeviceProfileCustomTagSet;
    }

    /**
     * Legt den Wert der allowDeviceProfileCustomTagSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowDeviceProfileCustomTagSet(Boolean value) {
        this.allowDeviceProfileCustomTagSet = value;
    }

    /**
     * Ruft den Wert der allowGroupCustomTagSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowGroupCustomTagSet() {
        return allowGroupCustomTagSet;
    }

    /**
     * Legt den Wert der allowGroupCustomTagSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowGroupCustomTagSet(Boolean value) {
        this.allowGroupCustomTagSet = value;
    }

    /**
     * Ruft den Wert der allowSpCustomTagSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowSpCustomTagSet() {
        return allowSpCustomTagSet;
    }

    /**
     * Legt den Wert der allowSpCustomTagSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowSpCustomTagSet(Boolean value) {
        this.allowSpCustomTagSet = value;
    }

    /**
     * Ruft den Wert der sendEmailUponResetFailure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendEmailUponResetFailure() {
        return sendEmailUponResetFailure;
    }

    /**
     * Legt den Wert der sendEmailUponResetFailure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendEmailUponResetFailure(Boolean value) {
        this.sendEmailUponResetFailure = value;
    }

    /**
     * Ruft den Wert der deviceAccessNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeviceAccessNetAddress() {
        return deviceAccessNetAddress;
    }

    /**
     * Legt den Wert der deviceAccessNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeviceAccessNetAddress(JAXBElement<String> value) {
        this.deviceAccessNetAddress = value;
    }

    /**
     * Ruft den Wert der deviceAccessPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getDeviceAccessPort() {
        return deviceAccessPort;
    }

    /**
     * Legt den Wert der deviceAccessPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setDeviceAccessPort(JAXBElement<Integer> value) {
        this.deviceAccessPort = value;
    }

    /**
     * Ruft den Wert der deviceAccessContext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeviceAccessContext() {
        return deviceAccessContext;
    }

    /**
     * Legt den Wert der deviceAccessContext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeviceAccessContext(JAXBElement<String> value) {
        this.deviceAccessContext = value;
    }

    /**
     * Ruft den Wert der defaultDeviceLanguage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultDeviceLanguage() {
        return defaultDeviceLanguage;
    }

    /**
     * Legt den Wert der defaultDeviceLanguage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultDeviceLanguage(JAXBElement<String> value) {
        this.defaultDeviceLanguage = value;
    }

    /**
     * Ruft den Wert der defaultDeviceEncoding-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultDeviceEncoding() {
        return defaultDeviceEncoding;
    }

    /**
     * Legt den Wert der defaultDeviceEncoding-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultDeviceEncoding(JAXBElement<String> value) {
        this.defaultDeviceEncoding = value;
    }

    /**
     * Ruft den Wert der accessDeviceCredentials-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DeviceManagementUserNamePassword16 }{@code >}
     *     
     */
    public JAXBElement<DeviceManagementUserNamePassword16> getAccessDeviceCredentials() {
        return accessDeviceCredentials;
    }

    /**
     * Legt den Wert der accessDeviceCredentials-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DeviceManagementUserNamePassword16 }{@code >}
     *     
     */
    public void setAccessDeviceCredentials(JAXBElement<DeviceManagementUserNamePassword16> value) {
        this.accessDeviceCredentials = value;
    }

    /**
     * Ruft den Wert der useHttpDigestAuthentication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseHttpDigestAuthentication() {
        return useHttpDigestAuthentication;
    }

    /**
     * Legt den Wert der useHttpDigestAuthentication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseHttpDigestAuthentication(Boolean value) {
        this.useHttpDigestAuthentication = value;
    }

    /**
     * Ruft den Wert der macBasedFileAuthentication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMacBasedFileAuthentication() {
        return macBasedFileAuthentication;
    }

    /**
     * Legt den Wert der macBasedFileAuthentication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMacBasedFileAuthentication(Boolean value) {
        this.macBasedFileAuthentication = value;
    }

    /**
     * Ruft den Wert der userNamePasswordFileAuthentication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUserNamePasswordFileAuthentication() {
        return userNamePasswordFileAuthentication;
    }

    /**
     * Legt den Wert der userNamePasswordFileAuthentication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUserNamePasswordFileAuthentication(Boolean value) {
        this.userNamePasswordFileAuthentication = value;
    }

    /**
     * Ruft den Wert der macInNonRequestURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMacInNonRequestURI() {
        return macInNonRequestURI;
    }

    /**
     * Legt den Wert der macInNonRequestURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMacInNonRequestURI(Boolean value) {
        this.macInNonRequestURI = value;
    }

    /**
     * Ruft den Wert der macInCert-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMacInCert() {
        return macInCert;
    }

    /**
     * Legt den Wert der macInCert-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMacInCert(Boolean value) {
        this.macInCert = value;
    }

    /**
     * Ruft den Wert der macFormatInNonRequestURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMacFormatInNonRequestURI() {
        return macFormatInNonRequestURI;
    }

    /**
     * Legt den Wert der macFormatInNonRequestURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMacFormatInNonRequestURI(JAXBElement<String> value) {
        this.macFormatInNonRequestURI = value;
    }

    /**
     * Ruft den Wert der enableDeviceActivation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableDeviceActivation() {
        return enableDeviceActivation;
    }

    /**
     * Legt den Wert der enableDeviceActivation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableDeviceActivation(Boolean value) {
        this.enableDeviceActivation = value;
    }

    /**
     * Ruft den Wert der deviceModel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeviceModel() {
        return deviceModel;
    }

    /**
     * Legt den Wert der deviceModel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeviceModel(JAXBElement<String> value) {
        this.deviceModel = value;
    }

}
