//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         CommPilot Express type to transfer to voice mail or forward to a number
 *         with certain exceptions used in the context of a modify.
 *       
 * 
 * <p>Java-Klasse für CommPilotExpressRedirectionWithExceptionModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CommPilotExpressRedirectionWithExceptionModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="sendCallsToVoiceMailExceptExcludedNumbers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="excludedPhoneNumber01" type="{}DN" minOccurs="0"/>
 *         <element name="excludedPhoneNumber02" type="{}DN" minOccurs="0"/>
 *         <element name="excludedPhoneNumber03" type="{}DN" minOccurs="0"/>
 *         <element name="forwardExcludedNumbersTo" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommPilotExpressRedirectionWithExceptionModify", propOrder = {
    "sendCallsToVoiceMailExceptExcludedNumbers",
    "excludedPhoneNumber01",
    "excludedPhoneNumber02",
    "excludedPhoneNumber03",
    "forwardExcludedNumbersTo"
})
public class CommPilotExpressRedirectionWithExceptionModify {

    protected Boolean sendCallsToVoiceMailExceptExcludedNumbers;
    @XmlElementRef(name = "excludedPhoneNumber01", type = JAXBElement.class, required = false)
    protected JAXBElement<String> excludedPhoneNumber01;
    @XmlElementRef(name = "excludedPhoneNumber02", type = JAXBElement.class, required = false)
    protected JAXBElement<String> excludedPhoneNumber02;
    @XmlElementRef(name = "excludedPhoneNumber03", type = JAXBElement.class, required = false)
    protected JAXBElement<String> excludedPhoneNumber03;
    @XmlElementRef(name = "forwardExcludedNumbersTo", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forwardExcludedNumbersTo;

    /**
     * Ruft den Wert der sendCallsToVoiceMailExceptExcludedNumbers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendCallsToVoiceMailExceptExcludedNumbers() {
        return sendCallsToVoiceMailExceptExcludedNumbers;
    }

    /**
     * Legt den Wert der sendCallsToVoiceMailExceptExcludedNumbers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendCallsToVoiceMailExceptExcludedNumbers(Boolean value) {
        this.sendCallsToVoiceMailExceptExcludedNumbers = value;
    }

    /**
     * Ruft den Wert der excludedPhoneNumber01-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExcludedPhoneNumber01() {
        return excludedPhoneNumber01;
    }

    /**
     * Legt den Wert der excludedPhoneNumber01-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExcludedPhoneNumber01(JAXBElement<String> value) {
        this.excludedPhoneNumber01 = value;
    }

    /**
     * Ruft den Wert der excludedPhoneNumber02-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExcludedPhoneNumber02() {
        return excludedPhoneNumber02;
    }

    /**
     * Legt den Wert der excludedPhoneNumber02-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExcludedPhoneNumber02(JAXBElement<String> value) {
        this.excludedPhoneNumber02 = value;
    }

    /**
     * Ruft den Wert der excludedPhoneNumber03-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExcludedPhoneNumber03() {
        return excludedPhoneNumber03;
    }

    /**
     * Legt den Wert der excludedPhoneNumber03-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExcludedPhoneNumber03(JAXBElement<String> value) {
        this.excludedPhoneNumber03 = value;
    }

    /**
     * Ruft den Wert der forwardExcludedNumbersTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForwardExcludedNumbersTo() {
        return forwardExcludedNumbersTo;
    }

    /**
     * Legt den Wert der forwardExcludedNumbersTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForwardExcludedNumbersTo(JAXBElement<String> value) {
        this.forwardExcludedNumbersTo = value;
    }

}
