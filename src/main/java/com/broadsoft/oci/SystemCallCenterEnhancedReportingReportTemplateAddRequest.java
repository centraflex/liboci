//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to add a system level call center report template.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCallCenterEnhancedReportingReportTemplateAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallCenterEnhancedReportingReportTemplateAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="name" type="{}CallCenterReportTemplateName"/>
 *         <element name="description" type="{}CallCenterReportTemplateDescription" minOccurs="0"/>
 *         <element name="dataTemplate" type="{}CallCenterReportDataTemplateName"/>
 *         <element name="filterNumber" type="{}CallCenterReportDataTemplateFilterNumber" minOccurs="0"/>
 *         <element name="xsltTemplate" type="{}LabeledFileResource"/>
 *         <element name="scope" type="{}CallCenterReportTemplateAccessOption"/>
 *         <element name="isEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isRealtimeReport" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="callCompletionThresholdParam" type="{}CallCenterReportInputParameterOption" minOccurs="0"/>
 *         <element name="shortDurationThresholdParam" type="{}CallCenterReportInputParameterOption" minOccurs="0"/>
 *         <element name="serviceLevelThresholdParam" type="{}CallCenterReportInputParameterOption" minOccurs="0"/>
 *         <element name="serviceLevelInclusionsParam" type="{}CallCenterReportInputParameterOption" minOccurs="0"/>
 *         <element name="serviceLevelObjectiveThresholdParam" type="{}CallCenterReportInputParameterOption" minOccurs="0"/>
 *         <element name="abandonedCallThresholdParam" type="{}CallCenterReportInputParameterOption" minOccurs="0"/>
 *         <element name="serviceLevelThresholdParamNumber" type="{}CallCenterReportServiceLevelInputParameterNumber" minOccurs="0"/>
 *         <element name="abandonedCallThresholdParamNumber" type="{}CallCenterReportAbandonedCallInputParameterNumber" minOccurs="0"/>
 *         <element name="filterValue" type="{}CallCenterReportDataTemplateQueryFilterValue" maxOccurs="5" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallCenterEnhancedReportingReportTemplateAddRequest", propOrder = {
    "name",
    "description",
    "dataTemplate",
    "filterNumber",
    "xsltTemplate",
    "scope",
    "isEnabled",
    "isRealtimeReport",
    "callCompletionThresholdParam",
    "shortDurationThresholdParam",
    "serviceLevelThresholdParam",
    "serviceLevelInclusionsParam",
    "serviceLevelObjectiveThresholdParam",
    "abandonedCallThresholdParam",
    "serviceLevelThresholdParamNumber",
    "abandonedCallThresholdParamNumber",
    "filterValue"
})
public class SystemCallCenterEnhancedReportingReportTemplateAddRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String dataTemplate;
    protected Integer filterNumber;
    @XmlElement(required = true)
    protected LabeledFileResource xsltTemplate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportTemplateAccessOption scope;
    protected boolean isEnabled;
    protected Boolean isRealtimeReport;
    @XmlSchemaType(name = "token")
    protected CallCenterReportInputParameterOption callCompletionThresholdParam;
    @XmlSchemaType(name = "token")
    protected CallCenterReportInputParameterOption shortDurationThresholdParam;
    @XmlSchemaType(name = "token")
    protected CallCenterReportInputParameterOption serviceLevelThresholdParam;
    @XmlSchemaType(name = "token")
    protected CallCenterReportInputParameterOption serviceLevelInclusionsParam;
    @XmlSchemaType(name = "token")
    protected CallCenterReportInputParameterOption serviceLevelObjectiveThresholdParam;
    @XmlSchemaType(name = "token")
    protected CallCenterReportInputParameterOption abandonedCallThresholdParam;
    protected Integer serviceLevelThresholdParamNumber;
    protected Integer abandonedCallThresholdParamNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> filterValue;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der dataTemplate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataTemplate() {
        return dataTemplate;
    }

    /**
     * Legt den Wert der dataTemplate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataTemplate(String value) {
        this.dataTemplate = value;
    }

    /**
     * Ruft den Wert der filterNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFilterNumber() {
        return filterNumber;
    }

    /**
     * Legt den Wert der filterNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFilterNumber(Integer value) {
        this.filterNumber = value;
    }

    /**
     * Ruft den Wert der xsltTemplate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledFileResource }
     *     
     */
    public LabeledFileResource getXsltTemplate() {
        return xsltTemplate;
    }

    /**
     * Legt den Wert der xsltTemplate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledFileResource }
     *     
     */
    public void setXsltTemplate(LabeledFileResource value) {
        this.xsltTemplate = value;
    }

    /**
     * Ruft den Wert der scope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportTemplateAccessOption }
     *     
     */
    public CallCenterReportTemplateAccessOption getScope() {
        return scope;
    }

    /**
     * Legt den Wert der scope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportTemplateAccessOption }
     *     
     */
    public void setScope(CallCenterReportTemplateAccessOption value) {
        this.scope = value;
    }

    /**
     * Ruft den Wert der isEnabled-Eigenschaft ab.
     * 
     */
    public boolean isIsEnabled() {
        return isEnabled;
    }

    /**
     * Legt den Wert der isEnabled-Eigenschaft fest.
     * 
     */
    public void setIsEnabled(boolean value) {
        this.isEnabled = value;
    }

    /**
     * Ruft den Wert der isRealtimeReport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRealtimeReport() {
        return isRealtimeReport;
    }

    /**
     * Legt den Wert der isRealtimeReport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRealtimeReport(Boolean value) {
        this.isRealtimeReport = value;
    }

    /**
     * Ruft den Wert der callCompletionThresholdParam-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public CallCenterReportInputParameterOption getCallCompletionThresholdParam() {
        return callCompletionThresholdParam;
    }

    /**
     * Legt den Wert der callCompletionThresholdParam-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public void setCallCompletionThresholdParam(CallCenterReportInputParameterOption value) {
        this.callCompletionThresholdParam = value;
    }

    /**
     * Ruft den Wert der shortDurationThresholdParam-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public CallCenterReportInputParameterOption getShortDurationThresholdParam() {
        return shortDurationThresholdParam;
    }

    /**
     * Legt den Wert der shortDurationThresholdParam-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public void setShortDurationThresholdParam(CallCenterReportInputParameterOption value) {
        this.shortDurationThresholdParam = value;
    }

    /**
     * Ruft den Wert der serviceLevelThresholdParam-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public CallCenterReportInputParameterOption getServiceLevelThresholdParam() {
        return serviceLevelThresholdParam;
    }

    /**
     * Legt den Wert der serviceLevelThresholdParam-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public void setServiceLevelThresholdParam(CallCenterReportInputParameterOption value) {
        this.serviceLevelThresholdParam = value;
    }

    /**
     * Ruft den Wert der serviceLevelInclusionsParam-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public CallCenterReportInputParameterOption getServiceLevelInclusionsParam() {
        return serviceLevelInclusionsParam;
    }

    /**
     * Legt den Wert der serviceLevelInclusionsParam-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public void setServiceLevelInclusionsParam(CallCenterReportInputParameterOption value) {
        this.serviceLevelInclusionsParam = value;
    }

    /**
     * Ruft den Wert der serviceLevelObjectiveThresholdParam-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public CallCenterReportInputParameterOption getServiceLevelObjectiveThresholdParam() {
        return serviceLevelObjectiveThresholdParam;
    }

    /**
     * Legt den Wert der serviceLevelObjectiveThresholdParam-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public void setServiceLevelObjectiveThresholdParam(CallCenterReportInputParameterOption value) {
        this.serviceLevelObjectiveThresholdParam = value;
    }

    /**
     * Ruft den Wert der abandonedCallThresholdParam-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public CallCenterReportInputParameterOption getAbandonedCallThresholdParam() {
        return abandonedCallThresholdParam;
    }

    /**
     * Legt den Wert der abandonedCallThresholdParam-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportInputParameterOption }
     *     
     */
    public void setAbandonedCallThresholdParam(CallCenterReportInputParameterOption value) {
        this.abandonedCallThresholdParam = value;
    }

    /**
     * Ruft den Wert der serviceLevelThresholdParamNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceLevelThresholdParamNumber() {
        return serviceLevelThresholdParamNumber;
    }

    /**
     * Legt den Wert der serviceLevelThresholdParamNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceLevelThresholdParamNumber(Integer value) {
        this.serviceLevelThresholdParamNumber = value;
    }

    /**
     * Ruft den Wert der abandonedCallThresholdParamNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAbandonedCallThresholdParamNumber() {
        return abandonedCallThresholdParamNumber;
    }

    /**
     * Legt den Wert der abandonedCallThresholdParamNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAbandonedCallThresholdParamNumber(Integer value) {
        this.abandonedCallThresholdParamNumber = value;
    }

    /**
     * Gets the value of the filterValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the filterValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFilterValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the filterValue property.
     */
    public List<String> getFilterValue() {
        if (filterValue == null) {
            filterValue = new ArrayList<>();
        }
        return this.filterValue;
    }

}
