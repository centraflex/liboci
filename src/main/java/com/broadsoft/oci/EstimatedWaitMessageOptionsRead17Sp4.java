//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Estimated Wait Message Options
 *       
 * 
 * <p>Java-Klasse für EstimatedWaitMessageOptionsRead17sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EstimatedWaitMessageOptionsRead17sp4">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="enabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="operatingMode" type="{}EstimatedWaitMessageOperatingMode"/>
 *         <element name="playPositionHighVolume" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="playTimeHighVolume" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maximumPositions" type="{}EstimatedWaitMessageMaximumPositions"/>
 *         <element name="maximumWaitingMinutes" type="{}EstimatedWaitMessageMaximumWaitingMinutes"/>
 *         <element name="defaultCallHandlingMinutes" type="{}EstimatedWaitMessageDefaultCallHandlingMinutes"/>
 *         <element name="playUpdatedEWM" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="timeBetweenEWMUpdatesSeconds" type="{}EstimatedWaitMessageTimeBetweenUpdatesSeconds" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EstimatedWaitMessageOptionsRead17sp4", propOrder = {
    "enabled",
    "operatingMode",
    "playPositionHighVolume",
    "playTimeHighVolume",
    "maximumPositions",
    "maximumWaitingMinutes",
    "defaultCallHandlingMinutes",
    "playUpdatedEWM",
    "timeBetweenEWMUpdatesSeconds"
})
public class EstimatedWaitMessageOptionsRead17Sp4 {

    protected boolean enabled;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EstimatedWaitMessageOperatingMode operatingMode;
    protected boolean playPositionHighVolume;
    protected boolean playTimeHighVolume;
    protected int maximumPositions;
    protected int maximumWaitingMinutes;
    protected int defaultCallHandlingMinutes;
    protected boolean playUpdatedEWM;
    protected Integer timeBetweenEWMUpdatesSeconds;

    /**
     * Ruft den Wert der enabled-Eigenschaft ab.
     * 
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Legt den Wert der enabled-Eigenschaft fest.
     * 
     */
    public void setEnabled(boolean value) {
        this.enabled = value;
    }

    /**
     * Ruft den Wert der operatingMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EstimatedWaitMessageOperatingMode }
     *     
     */
    public EstimatedWaitMessageOperatingMode getOperatingMode() {
        return operatingMode;
    }

    /**
     * Legt den Wert der operatingMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EstimatedWaitMessageOperatingMode }
     *     
     */
    public void setOperatingMode(EstimatedWaitMessageOperatingMode value) {
        this.operatingMode = value;
    }

    /**
     * Ruft den Wert der playPositionHighVolume-Eigenschaft ab.
     * 
     */
    public boolean isPlayPositionHighVolume() {
        return playPositionHighVolume;
    }

    /**
     * Legt den Wert der playPositionHighVolume-Eigenschaft fest.
     * 
     */
    public void setPlayPositionHighVolume(boolean value) {
        this.playPositionHighVolume = value;
    }

    /**
     * Ruft den Wert der playTimeHighVolume-Eigenschaft ab.
     * 
     */
    public boolean isPlayTimeHighVolume() {
        return playTimeHighVolume;
    }

    /**
     * Legt den Wert der playTimeHighVolume-Eigenschaft fest.
     * 
     */
    public void setPlayTimeHighVolume(boolean value) {
        this.playTimeHighVolume = value;
    }

    /**
     * Ruft den Wert der maximumPositions-Eigenschaft ab.
     * 
     */
    public int getMaximumPositions() {
        return maximumPositions;
    }

    /**
     * Legt den Wert der maximumPositions-Eigenschaft fest.
     * 
     */
    public void setMaximumPositions(int value) {
        this.maximumPositions = value;
    }

    /**
     * Ruft den Wert der maximumWaitingMinutes-Eigenschaft ab.
     * 
     */
    public int getMaximumWaitingMinutes() {
        return maximumWaitingMinutes;
    }

    /**
     * Legt den Wert der maximumWaitingMinutes-Eigenschaft fest.
     * 
     */
    public void setMaximumWaitingMinutes(int value) {
        this.maximumWaitingMinutes = value;
    }

    /**
     * Ruft den Wert der defaultCallHandlingMinutes-Eigenschaft ab.
     * 
     */
    public int getDefaultCallHandlingMinutes() {
        return defaultCallHandlingMinutes;
    }

    /**
     * Legt den Wert der defaultCallHandlingMinutes-Eigenschaft fest.
     * 
     */
    public void setDefaultCallHandlingMinutes(int value) {
        this.defaultCallHandlingMinutes = value;
    }

    /**
     * Ruft den Wert der playUpdatedEWM-Eigenschaft ab.
     * 
     */
    public boolean isPlayUpdatedEWM() {
        return playUpdatedEWM;
    }

    /**
     * Legt den Wert der playUpdatedEWM-Eigenschaft fest.
     * 
     */
    public void setPlayUpdatedEWM(boolean value) {
        this.playUpdatedEWM = value;
    }

    /**
     * Ruft den Wert der timeBetweenEWMUpdatesSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTimeBetweenEWMUpdatesSeconds() {
        return timeBetweenEWMUpdatesSeconds;
    }

    /**
     * Legt den Wert der timeBetweenEWMUpdatesSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTimeBetweenEWMUpdatesSeconds(Integer value) {
        this.timeBetweenEWMUpdatesSeconds = value;
    }

}
