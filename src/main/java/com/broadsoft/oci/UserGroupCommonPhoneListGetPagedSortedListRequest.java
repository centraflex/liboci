//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 * 				Get a user's group's common phone list.
 * 				The response is either a UserGroupCommonPhoneListGetPagedSortedListResponse or an
 * 				ErrorResponse.
 * 				The search can be done using multiple criterion.
 * 				If the searchCriteriaModeOr is present, any result matching any one
 * 				criteria is included in the results.
 * 				Otherwise, only results matching all the search criterion are included in the
 * 				results.
 * 				If no search criteria is specified, all results are returned.
 * 				Specifying searchCriteriaModeOr without any search criteria results
 * 				in an ErrorResponse.
 * 				The sort can be done on the name or the number in the common phone list.
 * 				The following elements are only used in AS data mode and ignored in XS data  
 * 				mode:
 * 				searchCriteriaGroupCommonMultiPartPhoneListName
 * 			
 * 
 * <p>Java-Klasse für UserGroupCommonPhoneListGetPagedSortedListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserGroupCommonPhoneListGetPagedSortedListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl"/>
 *         <choice>
 *           <element name="sortByGroupCommonPhoneListNumber" type="{}SortByGroupCommonPhoneListNumber"/>
 *           <element name="sortByGroupCommonPhoneListName" type="{}SortByGroupCommonPhoneListName"/>
 *         </choice>
 *         <element name="searchCriteriaModeOr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="searchCriteriaGroupCommonPhoneListName" type="{}SearchCriteriaGroupCommonPhoneListName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaGroupCommonPhoneListNumber" type="{}SearchCriteriaGroupCommonPhoneListNumber" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaGroupCommonMultiPartPhoneListName" type="{}SearchCriteriaGroupCommonMultiPartPhoneListName" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserGroupCommonPhoneListGetPagedSortedListRequest", propOrder = {
    "userId",
    "responsePagingControl",
    "sortByGroupCommonPhoneListNumber",
    "sortByGroupCommonPhoneListName",
    "searchCriteriaModeOr",
    "searchCriteriaGroupCommonPhoneListName",
    "searchCriteriaGroupCommonPhoneListNumber",
    "searchCriteriaGroupCommonMultiPartPhoneListName"
})
public class UserGroupCommonPhoneListGetPagedSortedListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    protected ResponsePagingControl responsePagingControl;
    protected SortByGroupCommonPhoneListNumber sortByGroupCommonPhoneListNumber;
    protected SortByGroupCommonPhoneListName sortByGroupCommonPhoneListName;
    protected Boolean searchCriteriaModeOr;
    protected List<SearchCriteriaGroupCommonPhoneListName> searchCriteriaGroupCommonPhoneListName;
    protected List<SearchCriteriaGroupCommonPhoneListNumber> searchCriteriaGroupCommonPhoneListNumber;
    protected List<SearchCriteriaGroupCommonMultiPartPhoneListName> searchCriteriaGroupCommonMultiPartPhoneListName;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Ruft den Wert der sortByGroupCommonPhoneListNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByGroupCommonPhoneListNumber }
     *     
     */
    public SortByGroupCommonPhoneListNumber getSortByGroupCommonPhoneListNumber() {
        return sortByGroupCommonPhoneListNumber;
    }

    /**
     * Legt den Wert der sortByGroupCommonPhoneListNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByGroupCommonPhoneListNumber }
     *     
     */
    public void setSortByGroupCommonPhoneListNumber(SortByGroupCommonPhoneListNumber value) {
        this.sortByGroupCommonPhoneListNumber = value;
    }

    /**
     * Ruft den Wert der sortByGroupCommonPhoneListName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByGroupCommonPhoneListName }
     *     
     */
    public SortByGroupCommonPhoneListName getSortByGroupCommonPhoneListName() {
        return sortByGroupCommonPhoneListName;
    }

    /**
     * Legt den Wert der sortByGroupCommonPhoneListName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByGroupCommonPhoneListName }
     *     
     */
    public void setSortByGroupCommonPhoneListName(SortByGroupCommonPhoneListName value) {
        this.sortByGroupCommonPhoneListName = value;
    }

    /**
     * Ruft den Wert der searchCriteriaModeOr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchCriteriaModeOr() {
        return searchCriteriaModeOr;
    }

    /**
     * Legt den Wert der searchCriteriaModeOr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchCriteriaModeOr(Boolean value) {
        this.searchCriteriaModeOr = value;
    }

    /**
     * Gets the value of the searchCriteriaGroupCommonPhoneListName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaGroupCommonPhoneListName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaGroupCommonPhoneListName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaGroupCommonPhoneListName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaGroupCommonPhoneListName property.
     */
    public List<SearchCriteriaGroupCommonPhoneListName> getSearchCriteriaGroupCommonPhoneListName() {
        if (searchCriteriaGroupCommonPhoneListName == null) {
            searchCriteriaGroupCommonPhoneListName = new ArrayList<>();
        }
        return this.searchCriteriaGroupCommonPhoneListName;
    }

    /**
     * Gets the value of the searchCriteriaGroupCommonPhoneListNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaGroupCommonPhoneListNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaGroupCommonPhoneListNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaGroupCommonPhoneListNumber }
     * 
     * 
     * @return
     *     The value of the searchCriteriaGroupCommonPhoneListNumber property.
     */
    public List<SearchCriteriaGroupCommonPhoneListNumber> getSearchCriteriaGroupCommonPhoneListNumber() {
        if (searchCriteriaGroupCommonPhoneListNumber == null) {
            searchCriteriaGroupCommonPhoneListNumber = new ArrayList<>();
        }
        return this.searchCriteriaGroupCommonPhoneListNumber;
    }

    /**
     * Gets the value of the searchCriteriaGroupCommonMultiPartPhoneListName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaGroupCommonMultiPartPhoneListName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaGroupCommonMultiPartPhoneListName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaGroupCommonMultiPartPhoneListName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaGroupCommonMultiPartPhoneListName property.
     */
    public List<SearchCriteriaGroupCommonMultiPartPhoneListName> getSearchCriteriaGroupCommonMultiPartPhoneListName() {
        if (searchCriteriaGroupCommonMultiPartPhoneListName == null) {
            searchCriteriaGroupCommonMultiPartPhoneListName = new ArrayList<>();
        }
        return this.searchCriteriaGroupCommonMultiPartPhoneListName;
    }

}
