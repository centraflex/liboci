//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemAutomaticCollectCallPrefixDigitsGetListRequest.
 *         Contains a table with column headings: "Country Code", "Prefix".
 *       
 * 
 * <p>Java-Klasse für SystemAutomaticCollectCallPrefixDigitsGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAutomaticCollectCallPrefixDigitsGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="prefixTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAutomaticCollectCallPrefixDigitsGetListResponse", propOrder = {
    "prefixTable"
})
public class SystemAutomaticCollectCallPrefixDigitsGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable prefixTable;

    /**
     * Ruft den Wert der prefixTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getPrefixTable() {
        return prefixTable;
    }

    /**
     * Legt den Wert der prefixTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setPrefixTable(OCITable value) {
        this.prefixTable = value;
    }

}
