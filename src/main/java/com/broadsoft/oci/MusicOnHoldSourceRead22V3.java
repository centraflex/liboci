//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Contains the music on hold source configuration.
 *         The following elements are only used in XS data mode and not returned in AS data mode:
 *           labeledCustomSourceMediaFiles
 *         The following elements are only used in AS data mode and not returned in XS data mode:
 *           announcementMediaFiles
 *           authenticationUserName
 *         The following element is only used in AS data mode:
 *           authenticationRequired, value "false" is returned in XS data mode
 *       
 * 
 * <p>Java-Klasse für MusicOnHoldSourceRead22V3 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="MusicOnHoldSourceRead22V3">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="audioFilePreferredCodec" type="{}AudioFileCodec22"/>
 *         <element name="messageSourceSelection" type="{}MusicOnHoldMessageSelection"/>
 *         <choice minOccurs="0">
 *           <element name="labeledCustomSourceMediaFiles">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="audioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *                     <element name="audioMediaType" type="{}MediaFileType" minOccurs="0"/>
 *                     <element name="videoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *                     <element name="videoMediaType" type="{}MediaFileType" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *           <element name="announcementCustomSourceMediaFiles">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="audioFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *                     <element name="videoFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *         </choice>
 *         <element name="externalSource" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointWithPortNumberRead22"/>
 *                   <element name="authenticationRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   <element name="authenticationUserName" type="{}SIPAuthenticationUserName" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MusicOnHoldSourceRead22V3", propOrder = {
    "audioFilePreferredCodec",
    "messageSourceSelection",
    "labeledCustomSourceMediaFiles",
    "announcementCustomSourceMediaFiles",
    "externalSource"
})
public class MusicOnHoldSourceRead22V3 {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AudioFileCodec22 audioFilePreferredCodec;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MusicOnHoldMessageSelection messageSourceSelection;
    protected MusicOnHoldSourceRead22V3 .LabeledCustomSourceMediaFiles labeledCustomSourceMediaFiles;
    protected MusicOnHoldSourceRead22V3 .AnnouncementCustomSourceMediaFiles announcementCustomSourceMediaFiles;
    protected MusicOnHoldSourceRead22V3 .ExternalSource externalSource;

    /**
     * Ruft den Wert der audioFilePreferredCodec-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AudioFileCodec22 }
     *     
     */
    public AudioFileCodec22 getAudioFilePreferredCodec() {
        return audioFilePreferredCodec;
    }

    /**
     * Legt den Wert der audioFilePreferredCodec-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AudioFileCodec22 }
     *     
     */
    public void setAudioFilePreferredCodec(AudioFileCodec22 value) {
        this.audioFilePreferredCodec = value;
    }

    /**
     * Ruft den Wert der messageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public MusicOnHoldMessageSelection getMessageSourceSelection() {
        return messageSourceSelection;
    }

    /**
     * Legt den Wert der messageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public void setMessageSourceSelection(MusicOnHoldMessageSelection value) {
        this.messageSourceSelection = value;
    }

    /**
     * Ruft den Wert der labeledCustomSourceMediaFiles-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceRead22V3 .LabeledCustomSourceMediaFiles }
     *     
     */
    public MusicOnHoldSourceRead22V3 .LabeledCustomSourceMediaFiles getLabeledCustomSourceMediaFiles() {
        return labeledCustomSourceMediaFiles;
    }

    /**
     * Legt den Wert der labeledCustomSourceMediaFiles-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceRead22V3 .LabeledCustomSourceMediaFiles }
     *     
     */
    public void setLabeledCustomSourceMediaFiles(MusicOnHoldSourceRead22V3 .LabeledCustomSourceMediaFiles value) {
        this.labeledCustomSourceMediaFiles = value;
    }

    /**
     * Ruft den Wert der announcementCustomSourceMediaFiles-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceRead22V3 .AnnouncementCustomSourceMediaFiles }
     *     
     */
    public MusicOnHoldSourceRead22V3 .AnnouncementCustomSourceMediaFiles getAnnouncementCustomSourceMediaFiles() {
        return announcementCustomSourceMediaFiles;
    }

    /**
     * Legt den Wert der announcementCustomSourceMediaFiles-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceRead22V3 .AnnouncementCustomSourceMediaFiles }
     *     
     */
    public void setAnnouncementCustomSourceMediaFiles(MusicOnHoldSourceRead22V3 .AnnouncementCustomSourceMediaFiles value) {
        this.announcementCustomSourceMediaFiles = value;
    }

    /**
     * Ruft den Wert der externalSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceRead22V3 .ExternalSource }
     *     
     */
    public MusicOnHoldSourceRead22V3 .ExternalSource getExternalSource() {
        return externalSource;
    }

    /**
     * Legt den Wert der externalSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceRead22V3 .ExternalSource }
     *     
     */
    public void setExternalSource(MusicOnHoldSourceRead22V3 .ExternalSource value) {
        this.externalSource = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="audioFile" type="{}AnnouncementFileKey" minOccurs="0"/>
     *         <element name="videoFile" type="{}AnnouncementFileKey" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "audioFile",
        "videoFile"
    })
    public static class AnnouncementCustomSourceMediaFiles {

        protected AnnouncementFileKey audioFile;
        protected AnnouncementFileKey videoFile;

        /**
         * Ruft den Wert der audioFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AnnouncementFileKey }
         *     
         */
        public AnnouncementFileKey getAudioFile() {
            return audioFile;
        }

        /**
         * Legt den Wert der audioFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AnnouncementFileKey }
         *     
         */
        public void setAudioFile(AnnouncementFileKey value) {
            this.audioFile = value;
        }

        /**
         * Ruft den Wert der videoFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AnnouncementFileKey }
         *     
         */
        public AnnouncementFileKey getVideoFile() {
            return videoFile;
        }

        /**
         * Legt den Wert der videoFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AnnouncementFileKey }
         *     
         */
        public void setVideoFile(AnnouncementFileKey value) {
            this.videoFile = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointWithPortNumberRead22"/>
     *         <element name="authenticationRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         <element name="authenticationUserName" type="{}SIPAuthenticationUserName" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accessDeviceEndpoint",
        "authenticationRequired",
        "authenticationUserName"
    })
    public static class ExternalSource {

        @XmlElement(required = true)
        protected AccessDeviceEndpointWithPortNumberRead22 accessDeviceEndpoint;
        protected boolean authenticationRequired;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String authenticationUserName;

        /**
         * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AccessDeviceEndpointWithPortNumberRead22 }
         *     
         */
        public AccessDeviceEndpointWithPortNumberRead22 getAccessDeviceEndpoint() {
            return accessDeviceEndpoint;
        }

        /**
         * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AccessDeviceEndpointWithPortNumberRead22 }
         *     
         */
        public void setAccessDeviceEndpoint(AccessDeviceEndpointWithPortNumberRead22 value) {
            this.accessDeviceEndpoint = value;
        }

        /**
         * Ruft den Wert der authenticationRequired-Eigenschaft ab.
         * 
         */
        public boolean isAuthenticationRequired() {
            return authenticationRequired;
        }

        /**
         * Legt den Wert der authenticationRequired-Eigenschaft fest.
         * 
         */
        public void setAuthenticationRequired(boolean value) {
            this.authenticationRequired = value;
        }

        /**
         * Ruft den Wert der authenticationUserName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAuthenticationUserName() {
            return authenticationUserName;
        }

        /**
         * Legt den Wert der authenticationUserName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAuthenticationUserName(String value) {
            this.authenticationUserName = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="audioFileDescription" type="{}FileDescription" minOccurs="0"/>
     *         <element name="audioMediaType" type="{}MediaFileType" minOccurs="0"/>
     *         <element name="videoFileDescription" type="{}FileDescription" minOccurs="0"/>
     *         <element name="videoMediaType" type="{}MediaFileType" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "audioFileDescription",
        "audioMediaType",
        "videoFileDescription",
        "videoMediaType"
    })
    public static class LabeledCustomSourceMediaFiles {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String audioFileDescription;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String audioMediaType;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String videoFileDescription;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String videoMediaType;

        /**
         * Ruft den Wert der audioFileDescription-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAudioFileDescription() {
            return audioFileDescription;
        }

        /**
         * Legt den Wert der audioFileDescription-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAudioFileDescription(String value) {
            this.audioFileDescription = value;
        }

        /**
         * Ruft den Wert der audioMediaType-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAudioMediaType() {
            return audioMediaType;
        }

        /**
         * Legt den Wert der audioMediaType-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAudioMediaType(String value) {
            this.audioMediaType = value;
        }

        /**
         * Ruft den Wert der videoFileDescription-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVideoFileDescription() {
            return videoFileDescription;
        }

        /**
         * Legt den Wert der videoFileDescription-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVideoFileDescription(String value) {
            this.videoFileDescription = value;
        }

        /**
         * Ruft den Wert der videoMediaType-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVideoMediaType() {
            return videoMediaType;
        }

        /**
         * Legt den Wert der videoMediaType-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVideoMediaType(String value) {
            this.videoMediaType = value;
        }

    }

}
