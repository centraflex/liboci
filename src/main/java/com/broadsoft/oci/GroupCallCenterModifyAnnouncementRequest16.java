//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a call center's announcement settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterModifyAnnouncementRequest16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterModifyAnnouncementRequest16">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="playEntranceMessage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="mandatoryEntranceMessage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="entranceAudioMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="entranceMessageAudioFile" type="{}ExtendedMediaFileResource" minOccurs="0"/>
 *         <element name="entranceVideoMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="entranceMessageVideoFile" type="{}ExtendedMediaFileResource" minOccurs="0"/>
 *         <element name="playPeriodicComfortMessage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="timeBetweenComfortMessagesSeconds" type="{}CallCenterTimeBetweenComfortMessagesSeconds" minOccurs="0"/>
 *         <element name="periodicComfortAudioMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="periodicComfortMessageAudioFile" type="{}ExtendedMediaFileResource" minOccurs="0"/>
 *         <element name="periodicComfortVideoMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="periodicComfortMessageVideoFile" type="{}ExtendedMediaFileResource" minOccurs="0"/>
 *         <element name="enableMediaOnHoldForQueuedCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="mediaOnHoldSource" type="{}CallCenterMediaOnHoldSourceModify16" minOccurs="0"/>
 *         <element name="mediaOnHoldUseAlternateSourceForInternalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="mediaOnHoldInternalSource" type="{}CallCenterMediaOnHoldSourceModify16" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterModifyAnnouncementRequest16", propOrder = {
    "serviceUserId",
    "playEntranceMessage",
    "mandatoryEntranceMessage",
    "entranceAudioMessageSelection",
    "entranceMessageAudioFile",
    "entranceVideoMessageSelection",
    "entranceMessageVideoFile",
    "playPeriodicComfortMessage",
    "timeBetweenComfortMessagesSeconds",
    "periodicComfortAudioMessageSelection",
    "periodicComfortMessageAudioFile",
    "periodicComfortVideoMessageSelection",
    "periodicComfortMessageVideoFile",
    "enableMediaOnHoldForQueuedCalls",
    "mediaOnHoldSource",
    "mediaOnHoldUseAlternateSourceForInternalCalls",
    "mediaOnHoldInternalSource"
})
public class GroupCallCenterModifyAnnouncementRequest16
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected Boolean playEntranceMessage;
    protected Boolean mandatoryEntranceMessage;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection entranceAudioMessageSelection;
    protected ExtendedMediaFileResource entranceMessageAudioFile;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection entranceVideoMessageSelection;
    protected ExtendedMediaFileResource entranceMessageVideoFile;
    protected Boolean playPeriodicComfortMessage;
    protected Integer timeBetweenComfortMessagesSeconds;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection periodicComfortAudioMessageSelection;
    protected ExtendedMediaFileResource periodicComfortMessageAudioFile;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection periodicComfortVideoMessageSelection;
    protected ExtendedMediaFileResource periodicComfortMessageVideoFile;
    protected Boolean enableMediaOnHoldForQueuedCalls;
    protected CallCenterMediaOnHoldSourceModify16 mediaOnHoldSource;
    protected Boolean mediaOnHoldUseAlternateSourceForInternalCalls;
    protected CallCenterMediaOnHoldSourceModify16 mediaOnHoldInternalSource;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der playEntranceMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayEntranceMessage() {
        return playEntranceMessage;
    }

    /**
     * Legt den Wert der playEntranceMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayEntranceMessage(Boolean value) {
        this.playEntranceMessage = value;
    }

    /**
     * Ruft den Wert der mandatoryEntranceMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMandatoryEntranceMessage() {
        return mandatoryEntranceMessage;
    }

    /**
     * Legt den Wert der mandatoryEntranceMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMandatoryEntranceMessage(Boolean value) {
        this.mandatoryEntranceMessage = value;
    }

    /**
     * Ruft den Wert der entranceAudioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getEntranceAudioMessageSelection() {
        return entranceAudioMessageSelection;
    }

    /**
     * Legt den Wert der entranceAudioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setEntranceAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.entranceAudioMessageSelection = value;
    }

    /**
     * Ruft den Wert der entranceMessageAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public ExtendedMediaFileResource getEntranceMessageAudioFile() {
        return entranceMessageAudioFile;
    }

    /**
     * Legt den Wert der entranceMessageAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public void setEntranceMessageAudioFile(ExtendedMediaFileResource value) {
        this.entranceMessageAudioFile = value;
    }

    /**
     * Ruft den Wert der entranceVideoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getEntranceVideoMessageSelection() {
        return entranceVideoMessageSelection;
    }

    /**
     * Legt den Wert der entranceVideoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setEntranceVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.entranceVideoMessageSelection = value;
    }

    /**
     * Ruft den Wert der entranceMessageVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public ExtendedMediaFileResource getEntranceMessageVideoFile() {
        return entranceMessageVideoFile;
    }

    /**
     * Legt den Wert der entranceMessageVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public void setEntranceMessageVideoFile(ExtendedMediaFileResource value) {
        this.entranceMessageVideoFile = value;
    }

    /**
     * Ruft den Wert der playPeriodicComfortMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayPeriodicComfortMessage() {
        return playPeriodicComfortMessage;
    }

    /**
     * Legt den Wert der playPeriodicComfortMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayPeriodicComfortMessage(Boolean value) {
        this.playPeriodicComfortMessage = value;
    }

    /**
     * Ruft den Wert der timeBetweenComfortMessagesSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTimeBetweenComfortMessagesSeconds() {
        return timeBetweenComfortMessagesSeconds;
    }

    /**
     * Legt den Wert der timeBetweenComfortMessagesSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTimeBetweenComfortMessagesSeconds(Integer value) {
        this.timeBetweenComfortMessagesSeconds = value;
    }

    /**
     * Ruft den Wert der periodicComfortAudioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getPeriodicComfortAudioMessageSelection() {
        return periodicComfortAudioMessageSelection;
    }

    /**
     * Legt den Wert der periodicComfortAudioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setPeriodicComfortAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.periodicComfortAudioMessageSelection = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public ExtendedMediaFileResource getPeriodicComfortMessageAudioFile() {
        return periodicComfortMessageAudioFile;
    }

    /**
     * Legt den Wert der periodicComfortMessageAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public void setPeriodicComfortMessageAudioFile(ExtendedMediaFileResource value) {
        this.periodicComfortMessageAudioFile = value;
    }

    /**
     * Ruft den Wert der periodicComfortVideoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getPeriodicComfortVideoMessageSelection() {
        return periodicComfortVideoMessageSelection;
    }

    /**
     * Legt den Wert der periodicComfortVideoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setPeriodicComfortVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.periodicComfortVideoMessageSelection = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public ExtendedMediaFileResource getPeriodicComfortMessageVideoFile() {
        return periodicComfortMessageVideoFile;
    }

    /**
     * Legt den Wert der periodicComfortMessageVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public void setPeriodicComfortMessageVideoFile(ExtendedMediaFileResource value) {
        this.periodicComfortMessageVideoFile = value;
    }

    /**
     * Ruft den Wert der enableMediaOnHoldForQueuedCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMediaOnHoldForQueuedCalls() {
        return enableMediaOnHoldForQueuedCalls;
    }

    /**
     * Legt den Wert der enableMediaOnHoldForQueuedCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMediaOnHoldForQueuedCalls(Boolean value) {
        this.enableMediaOnHoldForQueuedCalls = value;
    }

    /**
     * Ruft den Wert der mediaOnHoldSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldSourceModify16 }
     *     
     */
    public CallCenterMediaOnHoldSourceModify16 getMediaOnHoldSource() {
        return mediaOnHoldSource;
    }

    /**
     * Legt den Wert der mediaOnHoldSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldSourceModify16 }
     *     
     */
    public void setMediaOnHoldSource(CallCenterMediaOnHoldSourceModify16 value) {
        this.mediaOnHoldSource = value;
    }

    /**
     * Ruft den Wert der mediaOnHoldUseAlternateSourceForInternalCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMediaOnHoldUseAlternateSourceForInternalCalls() {
        return mediaOnHoldUseAlternateSourceForInternalCalls;
    }

    /**
     * Legt den Wert der mediaOnHoldUseAlternateSourceForInternalCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMediaOnHoldUseAlternateSourceForInternalCalls(Boolean value) {
        this.mediaOnHoldUseAlternateSourceForInternalCalls = value;
    }

    /**
     * Ruft den Wert der mediaOnHoldInternalSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldSourceModify16 }
     *     
     */
    public CallCenterMediaOnHoldSourceModify16 getMediaOnHoldInternalSource() {
        return mediaOnHoldInternalSource;
    }

    /**
     * Legt den Wert der mediaOnHoldInternalSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldSourceModify16 }
     *     
     */
    public void setMediaOnHoldInternalSource(CallCenterMediaOnHoldSourceModify16 value) {
        this.mediaOnHoldInternalSource = value;
    }

}
