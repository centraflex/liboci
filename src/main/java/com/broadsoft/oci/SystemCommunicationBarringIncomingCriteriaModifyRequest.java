//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify an existing Communication Barring Incoming Criteria.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCommunicationBarringIncomingCriteriaModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCommunicationBarringIncomingCriteriaModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="name" type="{}CommunicationBarringCriteriaName"/>
 *         <element name="newName" type="{}CommunicationBarringCriteriaName" minOccurs="0"/>
 *         <element name="description" type="{}CommunicationBarringCriteriaDescription" minOccurs="0"/>
 *         <element name="timeSchedule" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="matchNumberPortabilityStatus" type="{}ReplacementNumberPortabilityStatusList" minOccurs="0"/>
 *         <element name="callTaggedAsSpam" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCommunicationBarringIncomingCriteriaModifyRequest", propOrder = {
    "name",
    "newName",
    "description",
    "timeSchedule",
    "holidaySchedule",
    "matchNumberPortabilityStatus",
    "callTaggedAsSpam"
})
public class SystemCommunicationBarringIncomingCriteriaModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newName;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "timeSchedule", type = JAXBElement.class, required = false)
    protected JAXBElement<String> timeSchedule;
    @XmlElementRef(name = "holidaySchedule", type = JAXBElement.class, required = false)
    protected JAXBElement<String> holidaySchedule;
    @XmlElementRef(name = "matchNumberPortabilityStatus", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementNumberPortabilityStatusList> matchNumberPortabilityStatus;
    protected Boolean callTaggedAsSpam;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der newName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewName() {
        return newName;
    }

    /**
     * Legt den Wert der newName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewName(String value) {
        this.newName = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTimeSchedule(JAXBElement<String> value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHolidaySchedule(JAXBElement<String> value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der matchNumberPortabilityStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementNumberPortabilityStatusList }{@code >}
     *     
     */
    public JAXBElement<ReplacementNumberPortabilityStatusList> getMatchNumberPortabilityStatus() {
        return matchNumberPortabilityStatus;
    }

    /**
     * Legt den Wert der matchNumberPortabilityStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementNumberPortabilityStatusList }{@code >}
     *     
     */
    public void setMatchNumberPortabilityStatus(JAXBElement<ReplacementNumberPortabilityStatusList> value) {
        this.matchNumberPortabilityStatus = value;
    }

    /**
     * Ruft den Wert der callTaggedAsSpam-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCallTaggedAsSpam() {
        return callTaggedAsSpam;
    }

    /**
     * Legt den Wert der callTaggedAsSpam-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCallTaggedAsSpam(Boolean value) {
        this.callTaggedAsSpam = value;
    }

}
