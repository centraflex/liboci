//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserBroadWorksAnywhereGetRequest.
 *         The phoneNumberTable contains columns: "Phone Number", "Description"
 *         Replaced by: UserBroadWorksAnywhereGetResponse16sp2
 *       
 * 
 * <p>Java-Klasse für UserBroadWorksAnywhereGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksAnywhereGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="alertAllLocationsForClickToDialCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="phoneNumberTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksAnywhereGetResponse", propOrder = {
    "alertAllLocationsForClickToDialCalls",
    "phoneNumberTable"
})
public class UserBroadWorksAnywhereGetResponse
    extends OCIDataResponse
{

    protected boolean alertAllLocationsForClickToDialCalls;
    @XmlElement(required = true)
    protected OCITable phoneNumberTable;

    /**
     * Ruft den Wert der alertAllLocationsForClickToDialCalls-Eigenschaft ab.
     * 
     */
    public boolean isAlertAllLocationsForClickToDialCalls() {
        return alertAllLocationsForClickToDialCalls;
    }

    /**
     * Legt den Wert der alertAllLocationsForClickToDialCalls-Eigenschaft fest.
     * 
     */
    public void setAlertAllLocationsForClickToDialCalls(boolean value) {
        this.alertAllLocationsForClickToDialCalls = value;
    }

    /**
     * Ruft den Wert der phoneNumberTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getPhoneNumberTable() {
        return phoneNumberTable;
    }

    /**
     * Legt den Wert der phoneNumberTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setPhoneNumberTable(OCITable value) {
        this.phoneNumberTable = value;
    }

}
