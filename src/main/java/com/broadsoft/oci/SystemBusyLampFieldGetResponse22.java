//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemBusyLampFieldGetRequest22.
 *         
 *         The following elements are only used in AS data mode:
 *          forceUseOfTCP
 *         Replaced by: SystemBusyLampFieldGetResponse23V2 in AS data mode.
 *       
 * 
 * <p>Java-Klasse für SystemBusyLampFieldGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBusyLampFieldGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="displayLocalUserIdentityLastNameFirst" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forceUseOfTCP" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBusyLampFieldGetResponse22", propOrder = {
    "displayLocalUserIdentityLastNameFirst",
    "forceUseOfTCP"
})
public class SystemBusyLampFieldGetResponse22
    extends OCIDataResponse
{

    protected boolean displayLocalUserIdentityLastNameFirst;
    protected boolean forceUseOfTCP;

    /**
     * Ruft den Wert der displayLocalUserIdentityLastNameFirst-Eigenschaft ab.
     * 
     */
    public boolean isDisplayLocalUserIdentityLastNameFirst() {
        return displayLocalUserIdentityLastNameFirst;
    }

    /**
     * Legt den Wert der displayLocalUserIdentityLastNameFirst-Eigenschaft fest.
     * 
     */
    public void setDisplayLocalUserIdentityLastNameFirst(boolean value) {
        this.displayLocalUserIdentityLastNameFirst = value;
    }

    /**
     * Ruft den Wert der forceUseOfTCP-Eigenschaft ab.
     * 
     */
    public boolean isForceUseOfTCP() {
        return forceUseOfTCP;
    }

    /**
     * Legt den Wert der forceUseOfTCP-Eigenschaft fest.
     * 
     */
    public void setForceUseOfTCP(boolean value) {
        this.forceUseOfTCP = value;
    }

}
