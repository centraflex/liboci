//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains the call center media on hold source configuration.
 *     
 *     Replaced by: CallCenterMediaOnHoldSourceRead19.
 *       
 * 
 * <p>Java-Klasse für CallCenterMediaOnHoldSourceRead17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterMediaOnHoldSourceRead17">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="audioMessageSourceSelection" type="{}CallCenterMediaOnHoldMessageSelection"/>
 *         <element name="audioUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="audioFileList" type="{}CallCenterAnnouncementDescriptionList" minOccurs="0"/>
 *         <element name="audioMediaTypeList" type="{}CallCenterAnnouncementMediaFileTypeList" minOccurs="0"/>
 *         <element name="externalAudioSource" type="{}AccessDeviceEndpointRead14" minOccurs="0"/>
 *         <element name="videoMessageSourceSelection" type="{}CallCenterMediaOnHoldMessageSelection" minOccurs="0"/>
 *         <element name="videoUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="videoFileList" type="{}CallCenterAnnouncementDescriptionList" minOccurs="0"/>
 *         <element name="videoMediaTypeList" type="{}CallCenterAnnouncementMediaFileTypeList" minOccurs="0"/>
 *         <element name="externalVideoSource" type="{}AccessDeviceEndpointRead14" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterMediaOnHoldSourceRead17", propOrder = {
    "audioMessageSourceSelection",
    "audioUrlList",
    "audioFileList",
    "audioMediaTypeList",
    "externalAudioSource",
    "videoMessageSourceSelection",
    "videoUrlList",
    "videoFileList",
    "videoMediaTypeList",
    "externalVideoSource"
})
public class CallCenterMediaOnHoldSourceRead17 {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterMediaOnHoldMessageSelection audioMessageSourceSelection;
    protected CallCenterAnnouncementURLList audioUrlList;
    protected CallCenterAnnouncementDescriptionList audioFileList;
    protected CallCenterAnnouncementMediaFileTypeList audioMediaTypeList;
    protected AccessDeviceEndpointRead14 externalAudioSource;
    @XmlSchemaType(name = "token")
    protected CallCenterMediaOnHoldMessageSelection videoMessageSourceSelection;
    protected CallCenterAnnouncementURLList videoUrlList;
    protected CallCenterAnnouncementDescriptionList videoFileList;
    protected CallCenterAnnouncementMediaFileTypeList videoMediaTypeList;
    protected AccessDeviceEndpointRead14 externalVideoSource;

    /**
     * Ruft den Wert der audioMessageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public CallCenterMediaOnHoldMessageSelection getAudioMessageSourceSelection() {
        return audioMessageSourceSelection;
    }

    /**
     * Legt den Wert der audioMessageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public void setAudioMessageSourceSelection(CallCenterMediaOnHoldMessageSelection value) {
        this.audioMessageSourceSelection = value;
    }

    /**
     * Ruft den Wert der audioUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getAudioUrlList() {
        return audioUrlList;
    }

    /**
     * Legt den Wert der audioUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setAudioUrlList(CallCenterAnnouncementURLList value) {
        this.audioUrlList = value;
    }

    /**
     * Ruft den Wert der audioFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public CallCenterAnnouncementDescriptionList getAudioFileList() {
        return audioFileList;
    }

    /**
     * Legt den Wert der audioFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public void setAudioFileList(CallCenterAnnouncementDescriptionList value) {
        this.audioFileList = value;
    }

    /**
     * Ruft den Wert der audioMediaTypeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public CallCenterAnnouncementMediaFileTypeList getAudioMediaTypeList() {
        return audioMediaTypeList;
    }

    /**
     * Legt den Wert der audioMediaTypeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public void setAudioMediaTypeList(CallCenterAnnouncementMediaFileTypeList value) {
        this.audioMediaTypeList = value;
    }

    /**
     * Ruft den Wert der externalAudioSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEndpointRead14 }
     *     
     */
    public AccessDeviceEndpointRead14 getExternalAudioSource() {
        return externalAudioSource;
    }

    /**
     * Legt den Wert der externalAudioSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEndpointRead14 }
     *     
     */
    public void setExternalAudioSource(AccessDeviceEndpointRead14 value) {
        this.externalAudioSource = value;
    }

    /**
     * Ruft den Wert der videoMessageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public CallCenterMediaOnHoldMessageSelection getVideoMessageSourceSelection() {
        return videoMessageSourceSelection;
    }

    /**
     * Legt den Wert der videoMessageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public void setVideoMessageSourceSelection(CallCenterMediaOnHoldMessageSelection value) {
        this.videoMessageSourceSelection = value;
    }

    /**
     * Ruft den Wert der videoUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getVideoUrlList() {
        return videoUrlList;
    }

    /**
     * Legt den Wert der videoUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setVideoUrlList(CallCenterAnnouncementURLList value) {
        this.videoUrlList = value;
    }

    /**
     * Ruft den Wert der videoFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public CallCenterAnnouncementDescriptionList getVideoFileList() {
        return videoFileList;
    }

    /**
     * Legt den Wert der videoFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public void setVideoFileList(CallCenterAnnouncementDescriptionList value) {
        this.videoFileList = value;
    }

    /**
     * Ruft den Wert der videoMediaTypeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public CallCenterAnnouncementMediaFileTypeList getVideoMediaTypeList() {
        return videoMediaTypeList;
    }

    /**
     * Legt den Wert der videoMediaTypeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public void setVideoMediaTypeList(CallCenterAnnouncementMediaFileTypeList value) {
        this.videoMediaTypeList = value;
    }

    /**
     * Ruft den Wert der externalVideoSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEndpointRead14 }
     *     
     */
    public AccessDeviceEndpointRead14 getExternalVideoSource() {
        return externalVideoSource;
    }

    /**
     * Legt den Wert der externalVideoSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEndpointRead14 }
     *     
     */
    public void setExternalVideoSource(AccessDeviceEndpointRead14 value) {
        this.externalVideoSource = value;
    }

}
