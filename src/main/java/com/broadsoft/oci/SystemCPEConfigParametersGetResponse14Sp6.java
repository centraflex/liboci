//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCPEConfigParametersGetListRequest14sp6.
 *         Contains a list of system CPE Config parameters.
 *         
 *         Replaced by: SystemCPEConfigParametersGetResponse20
 *       
 * 
 * <p>Java-Klasse für SystemCPEConfigParametersGetResponse14sp6 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCPEConfigParametersGetResponse14sp6">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableIPDeviceManagement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="ftpConnectTimeoutSeconds" type="{}DeviceManagementFTPConnectTimeoutSeconds"/>
 *         <element name="ftpFileTransferTimeoutSeconds" type="{}DeviceManagementFTPFileTransferTimeoutSeconds"/>
 *         <element name="pauseBetweenFileRebuildMilliseconds" type="{}DeviceManagementPauseBetweenFileRebuildMilliseconds"/>
 *         <element name="maxBusyTimeMinutes" type="{}DeviceManagementMaxBusyTimeMinutes"/>
 *         <element name="deviceAccessAppServerClusterName" type="{}NetAddress" minOccurs="0"/>
 *         <choice>
 *           <element name="fileRebuildImmediate" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *           <element name="fileRebuildDaily">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="startHour" type="{}Hour"/>
 *                     <element name="startMinute" type="{}Minute"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *           <element name="fileRebuildHourly">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="startMinute" type="{}Minute"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCPEConfigParametersGetResponse14sp6", propOrder = {
    "enableIPDeviceManagement",
    "ftpConnectTimeoutSeconds",
    "ftpFileTransferTimeoutSeconds",
    "pauseBetweenFileRebuildMilliseconds",
    "maxBusyTimeMinutes",
    "deviceAccessAppServerClusterName",
    "fileRebuildImmediate",
    "fileRebuildDaily",
    "fileRebuildHourly"
})
public class SystemCPEConfigParametersGetResponse14Sp6
    extends OCIDataResponse
{

    protected boolean enableIPDeviceManagement;
    protected int ftpConnectTimeoutSeconds;
    protected int ftpFileTransferTimeoutSeconds;
    protected int pauseBetweenFileRebuildMilliseconds;
    protected int maxBusyTimeMinutes;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceAccessAppServerClusterName;
    protected Object fileRebuildImmediate;
    protected SystemCPEConfigParametersGetResponse14Sp6 .FileRebuildDaily fileRebuildDaily;
    protected SystemCPEConfigParametersGetResponse14Sp6 .FileRebuildHourly fileRebuildHourly;

    /**
     * Ruft den Wert der enableIPDeviceManagement-Eigenschaft ab.
     * 
     */
    public boolean isEnableIPDeviceManagement() {
        return enableIPDeviceManagement;
    }

    /**
     * Legt den Wert der enableIPDeviceManagement-Eigenschaft fest.
     * 
     */
    public void setEnableIPDeviceManagement(boolean value) {
        this.enableIPDeviceManagement = value;
    }

    /**
     * Ruft den Wert der ftpConnectTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getFtpConnectTimeoutSeconds() {
        return ftpConnectTimeoutSeconds;
    }

    /**
     * Legt den Wert der ftpConnectTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setFtpConnectTimeoutSeconds(int value) {
        this.ftpConnectTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der ftpFileTransferTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getFtpFileTransferTimeoutSeconds() {
        return ftpFileTransferTimeoutSeconds;
    }

    /**
     * Legt den Wert der ftpFileTransferTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setFtpFileTransferTimeoutSeconds(int value) {
        this.ftpFileTransferTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der pauseBetweenFileRebuildMilliseconds-Eigenschaft ab.
     * 
     */
    public int getPauseBetweenFileRebuildMilliseconds() {
        return pauseBetweenFileRebuildMilliseconds;
    }

    /**
     * Legt den Wert der pauseBetweenFileRebuildMilliseconds-Eigenschaft fest.
     * 
     */
    public void setPauseBetweenFileRebuildMilliseconds(int value) {
        this.pauseBetweenFileRebuildMilliseconds = value;
    }

    /**
     * Ruft den Wert der maxBusyTimeMinutes-Eigenschaft ab.
     * 
     */
    public int getMaxBusyTimeMinutes() {
        return maxBusyTimeMinutes;
    }

    /**
     * Legt den Wert der maxBusyTimeMinutes-Eigenschaft fest.
     * 
     */
    public void setMaxBusyTimeMinutes(int value) {
        this.maxBusyTimeMinutes = value;
    }

    /**
     * Ruft den Wert der deviceAccessAppServerClusterName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceAccessAppServerClusterName() {
        return deviceAccessAppServerClusterName;
    }

    /**
     * Legt den Wert der deviceAccessAppServerClusterName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceAccessAppServerClusterName(String value) {
        this.deviceAccessAppServerClusterName = value;
    }

    /**
     * Ruft den Wert der fileRebuildImmediate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getFileRebuildImmediate() {
        return fileRebuildImmediate;
    }

    /**
     * Legt den Wert der fileRebuildImmediate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setFileRebuildImmediate(Object value) {
        this.fileRebuildImmediate = value;
    }

    /**
     * Ruft den Wert der fileRebuildDaily-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemCPEConfigParametersGetResponse14Sp6 .FileRebuildDaily }
     *     
     */
    public SystemCPEConfigParametersGetResponse14Sp6 .FileRebuildDaily getFileRebuildDaily() {
        return fileRebuildDaily;
    }

    /**
     * Legt den Wert der fileRebuildDaily-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemCPEConfigParametersGetResponse14Sp6 .FileRebuildDaily }
     *     
     */
    public void setFileRebuildDaily(SystemCPEConfigParametersGetResponse14Sp6 .FileRebuildDaily value) {
        this.fileRebuildDaily = value;
    }

    /**
     * Ruft den Wert der fileRebuildHourly-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemCPEConfigParametersGetResponse14Sp6 .FileRebuildHourly }
     *     
     */
    public SystemCPEConfigParametersGetResponse14Sp6 .FileRebuildHourly getFileRebuildHourly() {
        return fileRebuildHourly;
    }

    /**
     * Legt den Wert der fileRebuildHourly-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemCPEConfigParametersGetResponse14Sp6 .FileRebuildHourly }
     *     
     */
    public void setFileRebuildHourly(SystemCPEConfigParametersGetResponse14Sp6 .FileRebuildHourly value) {
        this.fileRebuildHourly = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="startHour" type="{}Hour"/>
     *         <element name="startMinute" type="{}Minute"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "startHour",
        "startMinute"
    })
    public static class FileRebuildDaily {

        protected int startHour;
        protected int startMinute;

        /**
         * Ruft den Wert der startHour-Eigenschaft ab.
         * 
         */
        public int getStartHour() {
            return startHour;
        }

        /**
         * Legt den Wert der startHour-Eigenschaft fest.
         * 
         */
        public void setStartHour(int value) {
            this.startHour = value;
        }

        /**
         * Ruft den Wert der startMinute-Eigenschaft ab.
         * 
         */
        public int getStartMinute() {
            return startMinute;
        }

        /**
         * Legt den Wert der startMinute-Eigenschaft fest.
         * 
         */
        public void setStartMinute(int value) {
            this.startMinute = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="startMinute" type="{}Minute"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "startMinute"
    })
    public static class FileRebuildHourly {

        protected int startMinute;

        /**
         * Ruft den Wert der startMinute-Eigenschaft ab.
         * 
         */
        public int getStartMinute() {
            return startMinute;
        }

        /**
         * Legt den Wert der startMinute-Eigenschaft fest.
         * 
         */
        public void setStartMinute(int value) {
            this.startMinute = value;
        }

    }

}
