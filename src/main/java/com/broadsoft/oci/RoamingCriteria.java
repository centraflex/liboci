//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für RoamingCriteria.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="RoamingCriteria">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="In Home Network"/>
 *     <enumeration value="In Roaming Network"/>
 *     <enumeration value="Disregard Roaming"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "RoamingCriteria")
@XmlEnum
public enum RoamingCriteria {

    @XmlEnumValue("In Home Network")
    IN_HOME_NETWORK("In Home Network"),
    @XmlEnumValue("In Roaming Network")
    IN_ROAMING_NETWORK("In Roaming Network"),
    @XmlEnumValue("Disregard Roaming")
    DISREGARD_ROAMING("Disregard Roaming");
    private final String value;

    RoamingCriteria(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoamingCriteria fromValue(String v) {
        for (RoamingCriteria c: RoamingCriteria.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
