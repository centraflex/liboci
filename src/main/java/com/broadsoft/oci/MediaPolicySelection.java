//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für MediaPolicySelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="MediaPolicySelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Use Uncompressed Codec"/>
 *     <enumeration value="Use Supported Media Set"/>
 *     <enumeration value="No Restrictions"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "MediaPolicySelection")
@XmlEnum
public enum MediaPolicySelection {

    @XmlEnumValue("Use Uncompressed Codec")
    USE_UNCOMPRESSED_CODEC("Use Uncompressed Codec"),
    @XmlEnumValue("Use Supported Media Set")
    USE_SUPPORTED_MEDIA_SET("Use Supported Media Set"),
    @XmlEnumValue("No Restrictions")
    NO_RESTRICTIONS("No Restrictions");
    private final String value;

    MediaPolicySelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MediaPolicySelection fromValue(String v) {
        for (MediaPolicySelection c: MediaPolicySelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
