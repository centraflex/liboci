//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Outgoing Calling Plan being forwarded/transferred permissions for a department.
 *       
 * 
 * <p>Java-Klasse für OutgoingCallingPlanRedirectedDepartmentPermissionsModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OutgoingCallingPlanRedirectedDepartmentPermissionsModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="departmentKey" type="{}DepartmentKey"/>
 *         <element name="permissions" type="{}OutgoingCallingPlanRedirectedPermissionsModify"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingCallingPlanRedirectedDepartmentPermissionsModify", propOrder = {
    "departmentKey",
    "permissions"
})
public class OutgoingCallingPlanRedirectedDepartmentPermissionsModify {

    @XmlElement(required = true)
    protected DepartmentKey departmentKey;
    @XmlElement(required = true)
    protected OutgoingCallingPlanRedirectedPermissionsModify permissions;

    /**
     * Ruft den Wert der departmentKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DepartmentKey }
     *     
     */
    public DepartmentKey getDepartmentKey() {
        return departmentKey;
    }

    /**
     * Legt den Wert der departmentKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DepartmentKey }
     *     
     */
    public void setDepartmentKey(DepartmentKey value) {
        this.departmentKey = value;
    }

    /**
     * Ruft den Wert der permissions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanRedirectedPermissionsModify }
     *     
     */
    public OutgoingCallingPlanRedirectedPermissionsModify getPermissions() {
        return permissions;
    }

    /**
     * Legt den Wert der permissions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanRedirectedPermissionsModify }
     *     
     */
    public void setPermissions(OutgoingCallingPlanRedirectedPermissionsModify value) {
        this.permissions = value;
    }

}
