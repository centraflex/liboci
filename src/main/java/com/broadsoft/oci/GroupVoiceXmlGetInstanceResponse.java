//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupVoiceXmlGetInstanceRequest.
 *         Contains the service profile information and possibly access device information.
 *         
 *         Replaced by: GroupVoiceXmlGetInstanceResponse19sp1 in AS data mode
 *       
 * 
 * <p>Java-Klasse für GroupVoiceXmlGetInstanceResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupVoiceXmlGetInstanceResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceReadProfile17sp4"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointWithPortNumberRead" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupVoiceXmlGetInstanceResponse", propOrder = {
    "serviceInstanceProfile",
    "networkClassOfService",
    "accessDeviceEndpoint"
})
public class GroupVoiceXmlGetInstanceResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected ServiceInstanceReadProfile17Sp4 serviceInstanceProfile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    protected AccessDeviceEndpointWithPortNumberRead accessDeviceEndpoint;

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceReadProfile17Sp4 }
     *     
     */
    public ServiceInstanceReadProfile17Sp4 getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceReadProfile17Sp4 }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceReadProfile17Sp4 value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEndpointWithPortNumberRead }
     *     
     */
    public AccessDeviceEndpointWithPortNumberRead getAccessDeviceEndpoint() {
        return accessDeviceEndpoint;
    }

    /**
     * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEndpointWithPortNumberRead }
     *     
     */
    public void setAccessDeviceEndpoint(AccessDeviceEndpointWithPortNumberRead value) {
        this.accessDeviceEndpoint = value;
    }

}
