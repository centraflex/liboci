//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemSIPDeviceTypeServiceGetRequest.
 *         Contains the list of device type services integrated to BroadWorks.
 *       
 * 
 * <p>Java-Klasse für SystemSIPDeviceTypeServiceGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPDeviceTypeServiceGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="supportsPolycomPhoneServices" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPDeviceTypeServiceGetResponse", propOrder = {
    "supportsPolycomPhoneServices"
})
public class SystemSIPDeviceTypeServiceGetResponse
    extends OCIDataResponse
{

    protected boolean supportsPolycomPhoneServices;

    /**
     * Ruft den Wert der supportsPolycomPhoneServices-Eigenschaft ab.
     * 
     */
    public boolean isSupportsPolycomPhoneServices() {
        return supportsPolycomPhoneServices;
    }

    /**
     * Legt den Wert der supportsPolycomPhoneServices-Eigenschaft fest.
     * 
     */
    public void setSupportsPolycomPhoneServices(boolean value) {
        this.supportsPolycomPhoneServices = value;
    }

}
