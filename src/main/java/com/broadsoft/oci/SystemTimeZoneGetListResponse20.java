//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemTimeZoneGetListRequest20.
 *         Contains the configured time zone of the server processing the request and
 *         contains a 2 column table with column headings 'Key' and 'Display Name' and a row
 *         for each time zone.
 *       
 * 
 * <p>Java-Klasse für SystemTimeZoneGetListResponse20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemTimeZoneGetListResponse20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serverTimeZone" type="{}TimeZone"/>
 *         <element name="timeZoneTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemTimeZoneGetListResponse20", propOrder = {
    "serverTimeZone",
    "timeZoneTable"
})
public class SystemTimeZoneGetListResponse20
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serverTimeZone;
    @XmlElement(required = true)
    protected OCITable timeZoneTable;

    /**
     * Ruft den Wert der serverTimeZone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServerTimeZone() {
        return serverTimeZone;
    }

    /**
     * Legt den Wert der serverTimeZone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServerTimeZone(String value) {
        this.serverTimeZone = value;
    }

    /**
     * Ruft den Wert der timeZoneTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getTimeZoneTable() {
        return timeZoneTable;
    }

    /**
     * Legt den Wert der timeZoneTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setTimeZoneTable(OCITable value) {
        this.timeZoneTable = value;
    }

}
