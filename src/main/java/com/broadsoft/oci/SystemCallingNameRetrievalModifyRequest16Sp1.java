//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modifies the system's calling name retrieval attributes.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         Replaced by: SystemCallingNameRetrievalModifyRequest20
 *       
 * 
 * <p>Java-Klasse für SystemCallingNameRetrievalModifyRequest16sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallingNameRetrievalModifyRequest16sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="triggerCNAMQueriesForAllNetworkCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="triggerCNAMQueriesForGroupAndEnterpriseCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="queryProtocol" type="{}CallingNameRetrievalQueryProtocol" minOccurs="0"/>
 *         <element name="queryTimeoutMilliseconds" type="{}CallingNameRetrievalQueryTimeoutMilliseconds" minOccurs="0"/>
 *         <element name="sipExternalDatabaseNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="sipExternalDatabasePort" type="{}Port1025" minOccurs="0"/>
 *         <element name="sipExternalDatabaseTransport" type="{}TransportProtocol" minOccurs="0"/>
 *         <element name="soapExternalDatabaseNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="soapSupportsDNSSRV" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="callingNameSource" type="{}CallingNameRetrievalSourceIdentity" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallingNameRetrievalModifyRequest16sp1", propOrder = {
    "triggerCNAMQueriesForAllNetworkCalls",
    "triggerCNAMQueriesForGroupAndEnterpriseCalls",
    "queryProtocol",
    "queryTimeoutMilliseconds",
    "sipExternalDatabaseNetAddress",
    "sipExternalDatabasePort",
    "sipExternalDatabaseTransport",
    "soapExternalDatabaseNetAddress",
    "soapSupportsDNSSRV",
    "callingNameSource"
})
public class SystemCallingNameRetrievalModifyRequest16Sp1
    extends OCIRequest
{

    protected Boolean triggerCNAMQueriesForAllNetworkCalls;
    protected Boolean triggerCNAMQueriesForGroupAndEnterpriseCalls;
    @XmlSchemaType(name = "token")
    protected CallingNameRetrievalQueryProtocol queryProtocol;
    protected Integer queryTimeoutMilliseconds;
    @XmlElementRef(name = "sipExternalDatabaseNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sipExternalDatabaseNetAddress;
    @XmlElementRef(name = "sipExternalDatabasePort", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> sipExternalDatabasePort;
    @XmlSchemaType(name = "token")
    protected TransportProtocol sipExternalDatabaseTransport;
    @XmlElementRef(name = "soapExternalDatabaseNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> soapExternalDatabaseNetAddress;
    protected Boolean soapSupportsDNSSRV;
    @XmlSchemaType(name = "token")
    protected CallingNameRetrievalSourceIdentity callingNameSource;

    /**
     * Ruft den Wert der triggerCNAMQueriesForAllNetworkCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTriggerCNAMQueriesForAllNetworkCalls() {
        return triggerCNAMQueriesForAllNetworkCalls;
    }

    /**
     * Legt den Wert der triggerCNAMQueriesForAllNetworkCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTriggerCNAMQueriesForAllNetworkCalls(Boolean value) {
        this.triggerCNAMQueriesForAllNetworkCalls = value;
    }

    /**
     * Ruft den Wert der triggerCNAMQueriesForGroupAndEnterpriseCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTriggerCNAMQueriesForGroupAndEnterpriseCalls() {
        return triggerCNAMQueriesForGroupAndEnterpriseCalls;
    }

    /**
     * Legt den Wert der triggerCNAMQueriesForGroupAndEnterpriseCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTriggerCNAMQueriesForGroupAndEnterpriseCalls(Boolean value) {
        this.triggerCNAMQueriesForGroupAndEnterpriseCalls = value;
    }

    /**
     * Ruft den Wert der queryProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallingNameRetrievalQueryProtocol }
     *     
     */
    public CallingNameRetrievalQueryProtocol getQueryProtocol() {
        return queryProtocol;
    }

    /**
     * Legt den Wert der queryProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallingNameRetrievalQueryProtocol }
     *     
     */
    public void setQueryProtocol(CallingNameRetrievalQueryProtocol value) {
        this.queryProtocol = value;
    }

    /**
     * Ruft den Wert der queryTimeoutMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQueryTimeoutMilliseconds() {
        return queryTimeoutMilliseconds;
    }

    /**
     * Legt den Wert der queryTimeoutMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQueryTimeoutMilliseconds(Integer value) {
        this.queryTimeoutMilliseconds = value;
    }

    /**
     * Ruft den Wert der sipExternalDatabaseNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSipExternalDatabaseNetAddress() {
        return sipExternalDatabaseNetAddress;
    }

    /**
     * Legt den Wert der sipExternalDatabaseNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSipExternalDatabaseNetAddress(JAXBElement<String> value) {
        this.sipExternalDatabaseNetAddress = value;
    }

    /**
     * Ruft den Wert der sipExternalDatabasePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSipExternalDatabasePort() {
        return sipExternalDatabasePort;
    }

    /**
     * Legt den Wert der sipExternalDatabasePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSipExternalDatabasePort(JAXBElement<Integer> value) {
        this.sipExternalDatabasePort = value;
    }

    /**
     * Ruft den Wert der sipExternalDatabaseTransport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TransportProtocol }
     *     
     */
    public TransportProtocol getSipExternalDatabaseTransport() {
        return sipExternalDatabaseTransport;
    }

    /**
     * Legt den Wert der sipExternalDatabaseTransport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TransportProtocol }
     *     
     */
    public void setSipExternalDatabaseTransport(TransportProtocol value) {
        this.sipExternalDatabaseTransport = value;
    }

    /**
     * Ruft den Wert der soapExternalDatabaseNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSoapExternalDatabaseNetAddress() {
        return soapExternalDatabaseNetAddress;
    }

    /**
     * Legt den Wert der soapExternalDatabaseNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSoapExternalDatabaseNetAddress(JAXBElement<String> value) {
        this.soapExternalDatabaseNetAddress = value;
    }

    /**
     * Ruft den Wert der soapSupportsDNSSRV-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSoapSupportsDNSSRV() {
        return soapSupportsDNSSRV;
    }

    /**
     * Legt den Wert der soapSupportsDNSSRV-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSoapSupportsDNSSRV(Boolean value) {
        this.soapSupportsDNSSRV = value;
    }

    /**
     * Ruft den Wert der callingNameSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallingNameRetrievalSourceIdentity }
     *     
     */
    public CallingNameRetrievalSourceIdentity getCallingNameSource() {
        return callingNameSource;
    }

    /**
     * Legt den Wert der callingNameSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallingNameRetrievalSourceIdentity }
     *     
     */
    public void setCallingNameSource(CallingNameRetrievalSourceIdentity value) {
        this.callingNameSource = value;
    }

}
