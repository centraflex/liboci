//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * ?
 *         The common push notification token elements.
 *       
 * 
 * <p>Java-Klasse für PushNotificationTokenData complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="PushNotificationTokenData">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="pushNotificationToken" type="{}PushNotificationToken"/>
 *         <element name="pushNotificationType" type="{}PushNotificationType"/>
 *         <element name="pushNotificationEventData" type="{}PushNotificationEventData" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PushNotificationTokenData", propOrder = {
    "pushNotificationToken",
    "pushNotificationType",
    "pushNotificationEventData"
})
public class PushNotificationTokenData {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String pushNotificationToken;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String pushNotificationType;
    protected List<PushNotificationEventData> pushNotificationEventData;

    /**
     * Ruft den Wert der pushNotificationToken-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPushNotificationToken() {
        return pushNotificationToken;
    }

    /**
     * Legt den Wert der pushNotificationToken-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPushNotificationToken(String value) {
        this.pushNotificationToken = value;
    }

    /**
     * Ruft den Wert der pushNotificationType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPushNotificationType() {
        return pushNotificationType;
    }

    /**
     * Legt den Wert der pushNotificationType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPushNotificationType(String value) {
        this.pushNotificationType = value;
    }

    /**
     * Gets the value of the pushNotificationEventData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the pushNotificationEventData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPushNotificationEventData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PushNotificationEventData }
     * 
     * 
     * @return
     *     The value of the pushNotificationEventData property.
     */
    public List<PushNotificationEventData> getPushNotificationEventData() {
        if (pushNotificationEventData == null) {
            pushNotificationEventData = new ArrayList<>();
        }
        return this.pushNotificationEventData;
    }

}
