//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify enterprise level call center enhanced reporting settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         Replaced by EnterpriseCallCenterEnhancedReportingModifyRequest19
 *       
 * 
 * <p>Java-Klasse für EnterpriseCallCenterEnhancedReportingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseCallCenterEnhancedReportingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="reportingServer" type="{}CallCenterReportServerChoice" minOccurs="0"/>
 *         <element name="webStatisticSource" type="{}CallCenterReportWebStatisticsSource" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseCallCenterEnhancedReportingModifyRequest", propOrder = {
    "serviceProviderId",
    "reportingServer",
    "webStatisticSource"
})
public class EnterpriseCallCenterEnhancedReportingModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlSchemaType(name = "token")
    protected CallCenterReportServerChoice reportingServer;
    @XmlSchemaType(name = "token")
    protected CallCenterReportWebStatisticsSource webStatisticSource;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der reportingServer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportServerChoice }
     *     
     */
    public CallCenterReportServerChoice getReportingServer() {
        return reportingServer;
    }

    /**
     * Legt den Wert der reportingServer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportServerChoice }
     *     
     */
    public void setReportingServer(CallCenterReportServerChoice value) {
        this.reportingServer = value;
    }

    /**
     * Ruft den Wert der webStatisticSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportWebStatisticsSource }
     *     
     */
    public CallCenterReportWebStatisticsSource getWebStatisticSource() {
        return webStatisticSource;
    }

    /**
     * Legt den Wert der webStatisticSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportWebStatisticsSource }
     *     
     */
    public void setWebStatisticSource(CallCenterReportWebStatisticsSource value) {
        this.webStatisticSource = value;
    }

}
