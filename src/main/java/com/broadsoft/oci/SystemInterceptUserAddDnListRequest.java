//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *           Request to add an Intercept User number(s) to the system.
 *           The response is either a SuccessResponse or an ErrorResponse.
 *        
 * 
 * <p>Java-Klasse für SystemInterceptUserAddDnListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemInterceptUserAddDnListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="interceptDNList" type="{}InterceptDNListEntry" maxOccurs="100"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemInterceptUserAddDnListRequest", propOrder = {
    "interceptDNList"
})
public class SystemInterceptUserAddDnListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    protected List<InterceptDNListEntry> interceptDNList;

    /**
     * Gets the value of the interceptDNList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the interceptDNList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInterceptDNList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InterceptDNListEntry }
     * 
     * 
     * @return
     *     The value of the interceptDNList property.
     */
    public List<InterceptDNListEntry> getInterceptDNList() {
        if (interceptDNList == null) {
            interceptDNList = new ArrayList<>();
        }
        return this.interceptDNList;
    }

}
