//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GroupAdminAccessDeviceAccess.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="GroupAdminAccessDeviceAccess">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Full"/>
 *     <enumeration value="Associate User With Device"/>
 *     <enumeration value="Read-Only"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "GroupAdminAccessDeviceAccess")
@XmlEnum
public enum GroupAdminAccessDeviceAccess {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("Associate User With Device")
    ASSOCIATE_USER_WITH_DEVICE("Associate User With Device"),
    @XmlEnumValue("Read-Only")
    READ_ONLY("Read-Only");
    private final String value;

    GroupAdminAccessDeviceAccess(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupAdminAccessDeviceAccess fromValue(String v) {
        for (GroupAdminAccessDeviceAccess c: GroupAdminAccessDeviceAccess.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
