//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the specified Call Recording platform.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCallRecordingModifyPlatformRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallRecordingModifyPlatformRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="name" type="{}CallRecordingPlatformName"/>
 *         <element name="netAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="port" type="{}Port" minOccurs="0"/>
 *         <element name="mediaStream" type="{}MediaStream" minOccurs="0"/>
 *         <choice>
 *           <element name="becomeSystemDefault" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *           <element name="becomeResellerDefault" type="{}ResellerId22" minOccurs="0"/>
 *         </choice>
 *         <element name="transportType" type="{}ExtendedTransportProtocol" minOccurs="0"/>
 *         <element name="description" type="{}CallRecordingPlatformDescription" minOccurs="0"/>
 *         <element name="schemaVersion" type="{}CallRecordingPlatformSchemaVersion" minOccurs="0"/>
 *         <element name="supportVideoRecording" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="route" type="{}PathHeader" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallRecordingModifyPlatformRequest22", propOrder = {
    "name",
    "netAddress",
    "port",
    "mediaStream",
    "becomeSystemDefault",
    "becomeResellerDefault",
    "transportType",
    "description",
    "schemaVersion",
    "supportVideoRecording",
    "route"
})
public class SystemCallRecordingModifyPlatformRequest22
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String netAddress;
    @XmlElementRef(name = "port", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> port;
    @XmlSchemaType(name = "token")
    protected MediaStream mediaStream;
    protected Boolean becomeSystemDefault;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String becomeResellerDefault;
    @XmlSchemaType(name = "token")
    protected ExtendedTransportProtocol transportType;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String schemaVersion;
    protected Boolean supportVideoRecording;
    @XmlElementRef(name = "route", type = JAXBElement.class, required = false)
    protected JAXBElement<String> route;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der netAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetAddress() {
        return netAddress;
    }

    /**
     * Legt den Wert der netAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetAddress(String value) {
        this.netAddress = value;
    }

    /**
     * Ruft den Wert der port-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getPort() {
        return port;
    }

    /**
     * Legt den Wert der port-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setPort(JAXBElement<Integer> value) {
        this.port = value;
    }

    /**
     * Ruft den Wert der mediaStream-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MediaStream }
     *     
     */
    public MediaStream getMediaStream() {
        return mediaStream;
    }

    /**
     * Legt den Wert der mediaStream-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MediaStream }
     *     
     */
    public void setMediaStream(MediaStream value) {
        this.mediaStream = value;
    }

    /**
     * Ruft den Wert der becomeSystemDefault-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBecomeSystemDefault() {
        return becomeSystemDefault;
    }

    /**
     * Legt den Wert der becomeSystemDefault-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBecomeSystemDefault(Boolean value) {
        this.becomeSystemDefault = value;
    }

    /**
     * Ruft den Wert der becomeResellerDefault-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBecomeResellerDefault() {
        return becomeResellerDefault;
    }

    /**
     * Legt den Wert der becomeResellerDefault-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBecomeResellerDefault(String value) {
        this.becomeResellerDefault = value;
    }

    /**
     * Ruft den Wert der transportType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedTransportProtocol }
     *     
     */
    public ExtendedTransportProtocol getTransportType() {
        return transportType;
    }

    /**
     * Legt den Wert der transportType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedTransportProtocol }
     *     
     */
    public void setTransportType(ExtendedTransportProtocol value) {
        this.transportType = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der schemaVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchemaVersion() {
        return schemaVersion;
    }

    /**
     * Legt den Wert der schemaVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchemaVersion(String value) {
        this.schemaVersion = value;
    }

    /**
     * Ruft den Wert der supportVideoRecording-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportVideoRecording() {
        return supportVideoRecording;
    }

    /**
     * Legt den Wert der supportVideoRecording-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportVideoRecording(Boolean value) {
        this.supportVideoRecording = value;
    }

    /**
     * Ruft den Wert der route-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRoute() {
        return route;
    }

    /**
     * Legt den Wert der route-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRoute(JAXBElement<String> value) {
        this.route = value;
    }

}
