//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für InternalReleaseCause.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="InternalReleaseCause">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Busy"/>
 *     <enumeration value="Forbidden"/>
 *     <enumeration value="Routing Failure"/>
 *     <enumeration value="Global Failure"/>
 *     <enumeration value="Request Failure"/>
 *     <enumeration value="Server Failure"/>
 *     <enumeration value="Translation Failure"/>
 *     <enumeration value="Temporarily Unavailable"/>
 *     <enumeration value="User Not Found"/>
 *     <enumeration value="Request Timeout"/>
 *     <enumeration value="Dial Tone Timeout"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "InternalReleaseCause")
@XmlEnum
public enum InternalReleaseCause {

    @XmlEnumValue("Busy")
    BUSY("Busy"),
    @XmlEnumValue("Forbidden")
    FORBIDDEN("Forbidden"),
    @XmlEnumValue("Routing Failure")
    ROUTING_FAILURE("Routing Failure"),
    @XmlEnumValue("Global Failure")
    GLOBAL_FAILURE("Global Failure"),
    @XmlEnumValue("Request Failure")
    REQUEST_FAILURE("Request Failure"),
    @XmlEnumValue("Server Failure")
    SERVER_FAILURE("Server Failure"),
    @XmlEnumValue("Translation Failure")
    TRANSLATION_FAILURE("Translation Failure"),
    @XmlEnumValue("Temporarily Unavailable")
    TEMPORARILY_UNAVAILABLE("Temporarily Unavailable"),
    @XmlEnumValue("User Not Found")
    USER_NOT_FOUND("User Not Found"),
    @XmlEnumValue("Request Timeout")
    REQUEST_TIMEOUT("Request Timeout"),
    @XmlEnumValue("Dial Tone Timeout")
    DIAL_TONE_TIMEOUT("Dial Tone Timeout");
    private final String value;

    InternalReleaseCause(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InternalReleaseCause fromValue(String v) {
        for (InternalReleaseCause c: InternalReleaseCause.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
