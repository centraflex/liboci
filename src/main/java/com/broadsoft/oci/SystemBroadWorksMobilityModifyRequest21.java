//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the BroadWorks Mobility system parameters.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobilityModifyRequest21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobilityModifyRequest21">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enableLocationServices" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableMSRNLookup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableMobileStateChecking" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="denyCallOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="denyCallTerminations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="imrnTimeoutMilliseconds" type="{}IMRNTimeoutMilliseconds" minOccurs="0"/>
 *         <element name="enableInternalCLIDDelivery" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeRedirectForMobilityTermination" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableInternalCLIDDeliveryAccessLocations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableEnhancedUnreachableStateChecking" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableNetworkCallBarringStatusCheck" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="networkTranslationIndex" type="{}NetworkTranslationIndex" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobilityModifyRequest21", propOrder = {
    "enableLocationServices",
    "enableMSRNLookup",
    "enableMobileStateChecking",
    "denyCallOriginations",
    "denyCallTerminations",
    "imrnTimeoutMilliseconds",
    "enableInternalCLIDDelivery",
    "includeRedirectForMobilityTermination",
    "enableInternalCLIDDeliveryAccessLocations",
    "enableEnhancedUnreachableStateChecking",
    "enableNetworkCallBarringStatusCheck",
    "networkTranslationIndex"
})
public class SystemBroadWorksMobilityModifyRequest21
    extends OCIRequest
{

    protected Boolean enableLocationServices;
    protected Boolean enableMSRNLookup;
    protected Boolean enableMobileStateChecking;
    protected Boolean denyCallOriginations;
    protected Boolean denyCallTerminations;
    protected Integer imrnTimeoutMilliseconds;
    protected Boolean enableInternalCLIDDelivery;
    protected Boolean includeRedirectForMobilityTermination;
    protected Boolean enableInternalCLIDDeliveryAccessLocations;
    protected Boolean enableEnhancedUnreachableStateChecking;
    protected Boolean enableNetworkCallBarringStatusCheck;
    @XmlElementRef(name = "networkTranslationIndex", type = JAXBElement.class, required = false)
    protected JAXBElement<String> networkTranslationIndex;

    /**
     * Ruft den Wert der enableLocationServices-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableLocationServices() {
        return enableLocationServices;
    }

    /**
     * Legt den Wert der enableLocationServices-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableLocationServices(Boolean value) {
        this.enableLocationServices = value;
    }

    /**
     * Ruft den Wert der enableMSRNLookup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMSRNLookup() {
        return enableMSRNLookup;
    }

    /**
     * Legt den Wert der enableMSRNLookup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMSRNLookup(Boolean value) {
        this.enableMSRNLookup = value;
    }

    /**
     * Ruft den Wert der enableMobileStateChecking-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMobileStateChecking() {
        return enableMobileStateChecking;
    }

    /**
     * Legt den Wert der enableMobileStateChecking-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMobileStateChecking(Boolean value) {
        this.enableMobileStateChecking = value;
    }

    /**
     * Ruft den Wert der denyCallOriginations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDenyCallOriginations() {
        return denyCallOriginations;
    }

    /**
     * Legt den Wert der denyCallOriginations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDenyCallOriginations(Boolean value) {
        this.denyCallOriginations = value;
    }

    /**
     * Ruft den Wert der denyCallTerminations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDenyCallTerminations() {
        return denyCallTerminations;
    }

    /**
     * Legt den Wert der denyCallTerminations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDenyCallTerminations(Boolean value) {
        this.denyCallTerminations = value;
    }

    /**
     * Ruft den Wert der imrnTimeoutMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getImrnTimeoutMilliseconds() {
        return imrnTimeoutMilliseconds;
    }

    /**
     * Legt den Wert der imrnTimeoutMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setImrnTimeoutMilliseconds(Integer value) {
        this.imrnTimeoutMilliseconds = value;
    }

    /**
     * Ruft den Wert der enableInternalCLIDDelivery-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableInternalCLIDDelivery() {
        return enableInternalCLIDDelivery;
    }

    /**
     * Legt den Wert der enableInternalCLIDDelivery-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableInternalCLIDDelivery(Boolean value) {
        this.enableInternalCLIDDelivery = value;
    }

    /**
     * Ruft den Wert der includeRedirectForMobilityTermination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeRedirectForMobilityTermination() {
        return includeRedirectForMobilityTermination;
    }

    /**
     * Legt den Wert der includeRedirectForMobilityTermination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeRedirectForMobilityTermination(Boolean value) {
        this.includeRedirectForMobilityTermination = value;
    }

    /**
     * Ruft den Wert der enableInternalCLIDDeliveryAccessLocations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableInternalCLIDDeliveryAccessLocations() {
        return enableInternalCLIDDeliveryAccessLocations;
    }

    /**
     * Legt den Wert der enableInternalCLIDDeliveryAccessLocations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableInternalCLIDDeliveryAccessLocations(Boolean value) {
        this.enableInternalCLIDDeliveryAccessLocations = value;
    }

    /**
     * Ruft den Wert der enableEnhancedUnreachableStateChecking-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableEnhancedUnreachableStateChecking() {
        return enableEnhancedUnreachableStateChecking;
    }

    /**
     * Legt den Wert der enableEnhancedUnreachableStateChecking-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableEnhancedUnreachableStateChecking(Boolean value) {
        this.enableEnhancedUnreachableStateChecking = value;
    }

    /**
     * Ruft den Wert der enableNetworkCallBarringStatusCheck-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableNetworkCallBarringStatusCheck() {
        return enableNetworkCallBarringStatusCheck;
    }

    /**
     * Legt den Wert der enableNetworkCallBarringStatusCheck-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableNetworkCallBarringStatusCheck(Boolean value) {
        this.enableNetworkCallBarringStatusCheck = value;
    }

    /**
     * Ruft den Wert der networkTranslationIndex-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNetworkTranslationIndex() {
        return networkTranslationIndex;
    }

    /**
     * Legt den Wert der networkTranslationIndex-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNetworkTranslationIndex(JAXBElement<String> value) {
        this.networkTranslationIndex = value;
    }

}
