//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Indicates whether redirecting calls using specified digit patterns are permitted.
 *       
 * 
 * <p>Java-Klasse für OutgoingCallingPlanDigitPatternRedirectingPermission complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OutgoingCallingPlanDigitPatternRedirectingPermission">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="digitPatternName" type="{}CallingPlanDigitPatternName"/>
 *         <element name="permission" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingCallingPlanDigitPatternRedirectingPermission", propOrder = {
    "digitPatternName",
    "permission"
})
public class OutgoingCallingPlanDigitPatternRedirectingPermission {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String digitPatternName;
    protected boolean permission;

    /**
     * Ruft den Wert der digitPatternName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDigitPatternName() {
        return digitPatternName;
    }

    /**
     * Legt den Wert der digitPatternName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDigitPatternName(String value) {
        this.digitPatternName = value;
    }

    /**
     * Ruft den Wert der permission-Eigenschaft ab.
     * 
     */
    public boolean isPermission() {
        return permission;
    }

    /**
     * Legt den Wert der permission-Eigenschaft fest.
     * 
     */
    public void setPermission(boolean value) {
        this.permission = value;
    }

}
