//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 * 				Get an enterprise's common phone list for a user.
 * 				The response is either a UserEnterpriseCommonPhoneListGetPagedSortedListResponse
 * 				or an ErrorResponse.
 * 				The search can be done using multiple criterion.
 * 				If the searchCriteriaModeOr is present, any result matching any one
 * 				criteria is included in the results.
 * 				Otherwise, only results matching all the search criterion are included in the
 * 				results.
 * 				If no search criteria is specified, all results are returned.
 * 				Specifying searchCriteriaModeOr without any search criteria results
 * 				in an ErrorResponse.
 * 				The sort can be done on the name or the number in the common phone list.
 * 				The following elements are only used in AS data mode and ignored in XS data  
 * 				mode:
 * 					searchCriteriaEnterpriseCommonMultiPartPhoneListName
 * 			
 * 
 * <p>Java-Klasse für UserEnterpriseCommonPhoneListGetPagedSortedListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserEnterpriseCommonPhoneListGetPagedSortedListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl"/>
 *         <choice>
 *           <element name="sortByEnterpriseCommonPhoneListNumber" type="{}SortByEnterpriseCommonPhoneListNumber"/>
 *           <element name="sortByEnterpriseCommonPhoneListName" type="{}SortByEnterpriseCommonPhoneListName"/>
 *         </choice>
 *         <element name="searchCriteriaModeOr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="searchCriteriaEnterpriseCommonPhoneListName" type="{}SearchCriteriaEnterpriseCommonPhoneListName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaEnterpriseCommonPhoneListNumber" type="{}SearchCriteriaEnterpriseCommonPhoneListNumber" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaEnterpriseCommonMultiPartPhoneListName" type="{}SearchCriteriaEnterpriseCommonMultiPartPhoneListName" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserEnterpriseCommonPhoneListGetPagedSortedListRequest", propOrder = {
    "userId",
    "responsePagingControl",
    "sortByEnterpriseCommonPhoneListNumber",
    "sortByEnterpriseCommonPhoneListName",
    "searchCriteriaModeOr",
    "searchCriteriaEnterpriseCommonPhoneListName",
    "searchCriteriaEnterpriseCommonPhoneListNumber",
    "searchCriteriaEnterpriseCommonMultiPartPhoneListName"
})
public class UserEnterpriseCommonPhoneListGetPagedSortedListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    protected ResponsePagingControl responsePagingControl;
    protected SortByEnterpriseCommonPhoneListNumber sortByEnterpriseCommonPhoneListNumber;
    protected SortByEnterpriseCommonPhoneListName sortByEnterpriseCommonPhoneListName;
    protected Boolean searchCriteriaModeOr;
    protected List<SearchCriteriaEnterpriseCommonPhoneListName> searchCriteriaEnterpriseCommonPhoneListName;
    protected List<SearchCriteriaEnterpriseCommonPhoneListNumber> searchCriteriaEnterpriseCommonPhoneListNumber;
    protected List<SearchCriteriaEnterpriseCommonMultiPartPhoneListName> searchCriteriaEnterpriseCommonMultiPartPhoneListName;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Ruft den Wert der sortByEnterpriseCommonPhoneListNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByEnterpriseCommonPhoneListNumber }
     *     
     */
    public SortByEnterpriseCommonPhoneListNumber getSortByEnterpriseCommonPhoneListNumber() {
        return sortByEnterpriseCommonPhoneListNumber;
    }

    /**
     * Legt den Wert der sortByEnterpriseCommonPhoneListNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByEnterpriseCommonPhoneListNumber }
     *     
     */
    public void setSortByEnterpriseCommonPhoneListNumber(SortByEnterpriseCommonPhoneListNumber value) {
        this.sortByEnterpriseCommonPhoneListNumber = value;
    }

    /**
     * Ruft den Wert der sortByEnterpriseCommonPhoneListName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByEnterpriseCommonPhoneListName }
     *     
     */
    public SortByEnterpriseCommonPhoneListName getSortByEnterpriseCommonPhoneListName() {
        return sortByEnterpriseCommonPhoneListName;
    }

    /**
     * Legt den Wert der sortByEnterpriseCommonPhoneListName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByEnterpriseCommonPhoneListName }
     *     
     */
    public void setSortByEnterpriseCommonPhoneListName(SortByEnterpriseCommonPhoneListName value) {
        this.sortByEnterpriseCommonPhoneListName = value;
    }

    /**
     * Ruft den Wert der searchCriteriaModeOr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchCriteriaModeOr() {
        return searchCriteriaModeOr;
    }

    /**
     * Legt den Wert der searchCriteriaModeOr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchCriteriaModeOr(Boolean value) {
        this.searchCriteriaModeOr = value;
    }

    /**
     * Gets the value of the searchCriteriaEnterpriseCommonPhoneListName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaEnterpriseCommonPhoneListName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaEnterpriseCommonPhoneListName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaEnterpriseCommonPhoneListName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaEnterpriseCommonPhoneListName property.
     */
    public List<SearchCriteriaEnterpriseCommonPhoneListName> getSearchCriteriaEnterpriseCommonPhoneListName() {
        if (searchCriteriaEnterpriseCommonPhoneListName == null) {
            searchCriteriaEnterpriseCommonPhoneListName = new ArrayList<>();
        }
        return this.searchCriteriaEnterpriseCommonPhoneListName;
    }

    /**
     * Gets the value of the searchCriteriaEnterpriseCommonPhoneListNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaEnterpriseCommonPhoneListNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaEnterpriseCommonPhoneListNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaEnterpriseCommonPhoneListNumber }
     * 
     * 
     * @return
     *     The value of the searchCriteriaEnterpriseCommonPhoneListNumber property.
     */
    public List<SearchCriteriaEnterpriseCommonPhoneListNumber> getSearchCriteriaEnterpriseCommonPhoneListNumber() {
        if (searchCriteriaEnterpriseCommonPhoneListNumber == null) {
            searchCriteriaEnterpriseCommonPhoneListNumber = new ArrayList<>();
        }
        return this.searchCriteriaEnterpriseCommonPhoneListNumber;
    }

    /**
     * Gets the value of the searchCriteriaEnterpriseCommonMultiPartPhoneListName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaEnterpriseCommonMultiPartPhoneListName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaEnterpriseCommonMultiPartPhoneListName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaEnterpriseCommonMultiPartPhoneListName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaEnterpriseCommonMultiPartPhoneListName property.
     */
    public List<SearchCriteriaEnterpriseCommonMultiPartPhoneListName> getSearchCriteriaEnterpriseCommonMultiPartPhoneListName() {
        if (searchCriteriaEnterpriseCommonMultiPartPhoneListName == null) {
            searchCriteriaEnterpriseCommonMultiPartPhoneListName = new ArrayList<>();
        }
        return this.searchCriteriaEnterpriseCommonMultiPartPhoneListName;
    }

}
