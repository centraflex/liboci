//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify group FAC code level and the list of feature access codes for a group.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         Note: choice element is only valid when useFeatureAccessCodeLevel is set to "Group", otherwise an ErrorResponse will be returned.
 *         
 *         Replaced by: GroupFeatureAccessCodeModifyRequest21 in AS data mode
 *       
 * 
 * <p>Java-Klasse für GroupFeatureAccessCodeModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupFeatureAccessCodeModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="useFeatureAccessCodeLevel" type="{}GroupFeatureAccessCodeLevel" minOccurs="0"/>
 *         <choice>
 *           <element name="restoreDefaultCodes" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *           <element name="featureAccessCode" type="{}FeatureAccessCodeEntry" maxOccurs="unbounded" minOccurs="0"/>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupFeatureAccessCodeModifyRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "useFeatureAccessCodeLevel",
    "restoreDefaultCodes",
    "featureAccessCode"
})
public class GroupFeatureAccessCodeModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlSchemaType(name = "token")
    protected GroupFeatureAccessCodeLevel useFeatureAccessCodeLevel;
    protected Boolean restoreDefaultCodes;
    protected List<FeatureAccessCodeEntry> featureAccessCode;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der useFeatureAccessCodeLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupFeatureAccessCodeLevel }
     *     
     */
    public GroupFeatureAccessCodeLevel getUseFeatureAccessCodeLevel() {
        return useFeatureAccessCodeLevel;
    }

    /**
     * Legt den Wert der useFeatureAccessCodeLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupFeatureAccessCodeLevel }
     *     
     */
    public void setUseFeatureAccessCodeLevel(GroupFeatureAccessCodeLevel value) {
        this.useFeatureAccessCodeLevel = value;
    }

    /**
     * Ruft den Wert der restoreDefaultCodes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestoreDefaultCodes() {
        return restoreDefaultCodes;
    }

    /**
     * Legt den Wert der restoreDefaultCodes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestoreDefaultCodes(Boolean value) {
        this.restoreDefaultCodes = value;
    }

    /**
     * Gets the value of the featureAccessCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the featureAccessCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeatureAccessCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeatureAccessCodeEntry }
     * 
     * 
     * @return
     *     The value of the featureAccessCode property.
     */
    public List<FeatureAccessCodeEntry> getFeatureAccessCode() {
        if (featureAccessCode == null) {
            featureAccessCode = new ArrayList<>();
        }
        return this.featureAccessCode;
    }

}
