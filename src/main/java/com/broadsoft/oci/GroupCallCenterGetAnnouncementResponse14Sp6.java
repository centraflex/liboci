//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupCallCenterGetAnnouncementRequest14sp6.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterGetAnnouncementResponse14sp6 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterGetAnnouncementResponse14sp6">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="entranceMessageSelection" type="{}CallCenterAnnouncementSelection"/>
 *         <element name="entranceMessageAudioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="entranceMessageVideoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="periodicComfortMessageSelection" type="{}CallCenterAnnouncementSelection"/>
 *         <element name="periodicComfortMessageAudioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="periodicComfortMessageVideoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="onHoldSource" type="{}CallCenterMusicOnHoldSourceRead"/>
 *         <element name="onHoldUseAlternateSourceForInternalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="onHoldInternalSource" type="{}CallCenterMusicOnHoldSourceRead" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterGetAnnouncementResponse14sp6", propOrder = {
    "entranceMessageSelection",
    "entranceMessageAudioFileDescription",
    "entranceMessageVideoFileDescription",
    "periodicComfortMessageSelection",
    "periodicComfortMessageAudioFileDescription",
    "periodicComfortMessageVideoFileDescription",
    "onHoldSource",
    "onHoldUseAlternateSourceForInternalCalls",
    "onHoldInternalSource"
})
public class GroupCallCenterGetAnnouncementResponse14Sp6
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterAnnouncementSelection entranceMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String entranceMessageAudioFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String entranceMessageVideoFileDescription;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterAnnouncementSelection periodicComfortMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String periodicComfortMessageAudioFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String periodicComfortMessageVideoFileDescription;
    @XmlElement(required = true)
    protected CallCenterMusicOnHoldSourceRead onHoldSource;
    protected boolean onHoldUseAlternateSourceForInternalCalls;
    protected CallCenterMusicOnHoldSourceRead onHoldInternalSource;

    /**
     * Ruft den Wert der entranceMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public CallCenterAnnouncementSelection getEntranceMessageSelection() {
        return entranceMessageSelection;
    }

    /**
     * Legt den Wert der entranceMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public void setEntranceMessageSelection(CallCenterAnnouncementSelection value) {
        this.entranceMessageSelection = value;
    }

    /**
     * Ruft den Wert der entranceMessageAudioFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntranceMessageAudioFileDescription() {
        return entranceMessageAudioFileDescription;
    }

    /**
     * Legt den Wert der entranceMessageAudioFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntranceMessageAudioFileDescription(String value) {
        this.entranceMessageAudioFileDescription = value;
    }

    /**
     * Ruft den Wert der entranceMessageVideoFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntranceMessageVideoFileDescription() {
        return entranceMessageVideoFileDescription;
    }

    /**
     * Legt den Wert der entranceMessageVideoFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntranceMessageVideoFileDescription(String value) {
        this.entranceMessageVideoFileDescription = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public CallCenterAnnouncementSelection getPeriodicComfortMessageSelection() {
        return periodicComfortMessageSelection;
    }

    /**
     * Legt den Wert der periodicComfortMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public void setPeriodicComfortMessageSelection(CallCenterAnnouncementSelection value) {
        this.periodicComfortMessageSelection = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageAudioFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodicComfortMessageAudioFileDescription() {
        return periodicComfortMessageAudioFileDescription;
    }

    /**
     * Legt den Wert der periodicComfortMessageAudioFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodicComfortMessageAudioFileDescription(String value) {
        this.periodicComfortMessageAudioFileDescription = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageVideoFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodicComfortMessageVideoFileDescription() {
        return periodicComfortMessageVideoFileDescription;
    }

    /**
     * Legt den Wert der periodicComfortMessageVideoFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodicComfortMessageVideoFileDescription(String value) {
        this.periodicComfortMessageVideoFileDescription = value;
    }

    /**
     * Ruft den Wert der onHoldSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMusicOnHoldSourceRead }
     *     
     */
    public CallCenterMusicOnHoldSourceRead getOnHoldSource() {
        return onHoldSource;
    }

    /**
     * Legt den Wert der onHoldSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMusicOnHoldSourceRead }
     *     
     */
    public void setOnHoldSource(CallCenterMusicOnHoldSourceRead value) {
        this.onHoldSource = value;
    }

    /**
     * Ruft den Wert der onHoldUseAlternateSourceForInternalCalls-Eigenschaft ab.
     * 
     */
    public boolean isOnHoldUseAlternateSourceForInternalCalls() {
        return onHoldUseAlternateSourceForInternalCalls;
    }

    /**
     * Legt den Wert der onHoldUseAlternateSourceForInternalCalls-Eigenschaft fest.
     * 
     */
    public void setOnHoldUseAlternateSourceForInternalCalls(boolean value) {
        this.onHoldUseAlternateSourceForInternalCalls = value;
    }

    /**
     * Ruft den Wert der onHoldInternalSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMusicOnHoldSourceRead }
     *     
     */
    public CallCenterMusicOnHoldSourceRead getOnHoldInternalSource() {
        return onHoldInternalSource;
    }

    /**
     * Legt den Wert der onHoldInternalSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMusicOnHoldSourceRead }
     *     
     */
    public void setOnHoldInternalSource(CallCenterMusicOnHoldSourceRead value) {
        this.onHoldInternalSource = value;
    }

}
