//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify the Executive system parameters.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemExecutiveModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemExecutiveModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="treatVirtualOnNetCallsAsInternal" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemExecutiveModifyRequest", propOrder = {
    "treatVirtualOnNetCallsAsInternal"
})
public class SystemExecutiveModifyRequest
    extends OCIRequest
{

    protected Boolean treatVirtualOnNetCallsAsInternal;

    /**
     * Ruft den Wert der treatVirtualOnNetCallsAsInternal-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTreatVirtualOnNetCallsAsInternal() {
        return treatVirtualOnNetCallsAsInternal;
    }

    /**
     * Legt den Wert der treatVirtualOnNetCallsAsInternal-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTreatVirtualOnNetCallsAsInternal(Boolean value) {
        this.treatVirtualOnNetCallsAsInternal = value;
    }

}
