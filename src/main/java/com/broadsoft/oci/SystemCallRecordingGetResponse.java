//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemCallRecordingGetRequest.
 *         
 *         Replaced By: SystemCallRecordingGetResponse22
 *       
 * 
 * <p>Java-Klasse für SystemCallRecordingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallRecordingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="continueCallAfterRecordingFailure" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="refreshPeriodSeconds" type="{}RecordingRefreshPeriodSeconds"/>
 *         <element name="maxConsecutiveFailures" type="{}RecordingMaxConsecutiveFailures"/>
 *         <element name="maxResponseWaitTimeMilliseconds" type="{}RecordingMaxResponseWaitTimeMilliseconds"/>
 *         <element name="continueCallAfterVideoRecordingFailure" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallRecordingGetResponse", propOrder = {
    "continueCallAfterRecordingFailure",
    "refreshPeriodSeconds",
    "maxConsecutiveFailures",
    "maxResponseWaitTimeMilliseconds",
    "continueCallAfterVideoRecordingFailure"
})
public class SystemCallRecordingGetResponse
    extends OCIDataResponse
{

    protected boolean continueCallAfterRecordingFailure;
    protected int refreshPeriodSeconds;
    protected int maxConsecutiveFailures;
    protected int maxResponseWaitTimeMilliseconds;
    protected boolean continueCallAfterVideoRecordingFailure;

    /**
     * Ruft den Wert der continueCallAfterRecordingFailure-Eigenschaft ab.
     * 
     */
    public boolean isContinueCallAfterRecordingFailure() {
        return continueCallAfterRecordingFailure;
    }

    /**
     * Legt den Wert der continueCallAfterRecordingFailure-Eigenschaft fest.
     * 
     */
    public void setContinueCallAfterRecordingFailure(boolean value) {
        this.continueCallAfterRecordingFailure = value;
    }

    /**
     * Ruft den Wert der refreshPeriodSeconds-Eigenschaft ab.
     * 
     */
    public int getRefreshPeriodSeconds() {
        return refreshPeriodSeconds;
    }

    /**
     * Legt den Wert der refreshPeriodSeconds-Eigenschaft fest.
     * 
     */
    public void setRefreshPeriodSeconds(int value) {
        this.refreshPeriodSeconds = value;
    }

    /**
     * Ruft den Wert der maxConsecutiveFailures-Eigenschaft ab.
     * 
     */
    public int getMaxConsecutiveFailures() {
        return maxConsecutiveFailures;
    }

    /**
     * Legt den Wert der maxConsecutiveFailures-Eigenschaft fest.
     * 
     */
    public void setMaxConsecutiveFailures(int value) {
        this.maxConsecutiveFailures = value;
    }

    /**
     * Ruft den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft ab.
     * 
     */
    public int getMaxResponseWaitTimeMilliseconds() {
        return maxResponseWaitTimeMilliseconds;
    }

    /**
     * Legt den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft fest.
     * 
     */
    public void setMaxResponseWaitTimeMilliseconds(int value) {
        this.maxResponseWaitTimeMilliseconds = value;
    }

    /**
     * Ruft den Wert der continueCallAfterVideoRecordingFailure-Eigenschaft ab.
     * 
     */
    public boolean isContinueCallAfterVideoRecordingFailure() {
        return continueCallAfterVideoRecordingFailure;
    }

    /**
     * Legt den Wert der continueCallAfterVideoRecordingFailure-Eigenschaft fest.
     * 
     */
    public void setContinueCallAfterVideoRecordingFailure(boolean value) {
        this.continueCallAfterVideoRecordingFailure = value;
    }

}
