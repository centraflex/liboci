//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the user level data associated with Call Policies.
 *         The following elements are only used in AS data mode:
 *           callingLineIdentityForRedirectedCalls
 *           
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserCallPoliciesModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallPoliciesModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="redirectedCallsCOLPPrivacy" type="{}ConnectedLineIdentificationPrivacyOnRedirectedCalls" minOccurs="0"/>
 *         <element name="callBeingForwardedResponseCallType" type="{}CallBeingForwardedResponseCallType" minOccurs="0"/>
 *         <element name="callingLineIdentityForRedirectedCalls" type="{}CallingLineIdentityForRedirectedCalls" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallPoliciesModifyRequest", propOrder = {
    "userId",
    "redirectedCallsCOLPPrivacy",
    "callBeingForwardedResponseCallType",
    "callingLineIdentityForRedirectedCalls"
})
public class UserCallPoliciesModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlSchemaType(name = "token")
    protected ConnectedLineIdentificationPrivacyOnRedirectedCalls redirectedCallsCOLPPrivacy;
    @XmlSchemaType(name = "token")
    protected CallBeingForwardedResponseCallType callBeingForwardedResponseCallType;
    @XmlSchemaType(name = "token")
    protected CallingLineIdentityForRedirectedCalls callingLineIdentityForRedirectedCalls;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der redirectedCallsCOLPPrivacy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ConnectedLineIdentificationPrivacyOnRedirectedCalls }
     *     
     */
    public ConnectedLineIdentificationPrivacyOnRedirectedCalls getRedirectedCallsCOLPPrivacy() {
        return redirectedCallsCOLPPrivacy;
    }

    /**
     * Legt den Wert der redirectedCallsCOLPPrivacy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConnectedLineIdentificationPrivacyOnRedirectedCalls }
     *     
     */
    public void setRedirectedCallsCOLPPrivacy(ConnectedLineIdentificationPrivacyOnRedirectedCalls value) {
        this.redirectedCallsCOLPPrivacy = value;
    }

    /**
     * Ruft den Wert der callBeingForwardedResponseCallType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallBeingForwardedResponseCallType }
     *     
     */
    public CallBeingForwardedResponseCallType getCallBeingForwardedResponseCallType() {
        return callBeingForwardedResponseCallType;
    }

    /**
     * Legt den Wert der callBeingForwardedResponseCallType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallBeingForwardedResponseCallType }
     *     
     */
    public void setCallBeingForwardedResponseCallType(CallBeingForwardedResponseCallType value) {
        this.callBeingForwardedResponseCallType = value;
    }

    /**
     * Ruft den Wert der callingLineIdentityForRedirectedCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallingLineIdentityForRedirectedCalls }
     *     
     */
    public CallingLineIdentityForRedirectedCalls getCallingLineIdentityForRedirectedCalls() {
        return callingLineIdentityForRedirectedCalls;
    }

    /**
     * Legt den Wert der callingLineIdentityForRedirectedCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallingLineIdentityForRedirectedCalls }
     *     
     */
    public void setCallingLineIdentityForRedirectedCalls(CallingLineIdentityForRedirectedCalls value) {
        this.callingLineIdentityForRedirectedCalls = value;
    }

}
