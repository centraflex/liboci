//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCallCenterEventRecordingFileParametersGetRequest.
 *         Contains a list of system Call Center Event Recording File parameters.
 *       
 * 
 * <p>Java-Klasse für SystemCallCenterEventRecordingFileParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallCenterEventRecordingFileParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="fileRetentionTimeDays" type="{}CallCenterEventRecordingFileRetentionTimeDays"/>
 *         <element name="fileRotationPeriodMinutes" type="{}CallCenterEventRecordingFileRotationPeriodMinutes"/>
 *         <element name="fileRotationOffsetMinutes" type="{}CallCenterEventRecordingFileRotationOffsetMinutes"/>
 *         <element name="remoteUrl" type="{}URL" minOccurs="0"/>
 *         <element name="remoteUserId" type="{}CallCenterEventRecordingFileSFTPUserId" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallCenterEventRecordingFileParametersGetResponse", propOrder = {
    "fileRetentionTimeDays",
    "fileRotationPeriodMinutes",
    "fileRotationOffsetMinutes",
    "remoteUrl",
    "remoteUserId"
})
public class SystemCallCenterEventRecordingFileParametersGetResponse
    extends OCIDataResponse
{

    protected int fileRetentionTimeDays;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String fileRotationPeriodMinutes;
    protected int fileRotationOffsetMinutes;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String remoteUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String remoteUserId;

    /**
     * Ruft den Wert der fileRetentionTimeDays-Eigenschaft ab.
     * 
     */
    public int getFileRetentionTimeDays() {
        return fileRetentionTimeDays;
    }

    /**
     * Legt den Wert der fileRetentionTimeDays-Eigenschaft fest.
     * 
     */
    public void setFileRetentionTimeDays(int value) {
        this.fileRetentionTimeDays = value;
    }

    /**
     * Ruft den Wert der fileRotationPeriodMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileRotationPeriodMinutes() {
        return fileRotationPeriodMinutes;
    }

    /**
     * Legt den Wert der fileRotationPeriodMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileRotationPeriodMinutes(String value) {
        this.fileRotationPeriodMinutes = value;
    }

    /**
     * Ruft den Wert der fileRotationOffsetMinutes-Eigenschaft ab.
     * 
     */
    public int getFileRotationOffsetMinutes() {
        return fileRotationOffsetMinutes;
    }

    /**
     * Legt den Wert der fileRotationOffsetMinutes-Eigenschaft fest.
     * 
     */
    public void setFileRotationOffsetMinutes(int value) {
        this.fileRotationOffsetMinutes = value;
    }

    /**
     * Ruft den Wert der remoteUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteUrl() {
        return remoteUrl;
    }

    /**
     * Legt den Wert der remoteUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteUrl(String value) {
        this.remoteUrl = value;
    }

    /**
     * Ruft den Wert der remoteUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteUserId() {
        return remoteUserId;
    }

    /**
     * Legt den Wert der remoteUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteUserId(String value) {
        this.remoteUserId = value;
    }

}
