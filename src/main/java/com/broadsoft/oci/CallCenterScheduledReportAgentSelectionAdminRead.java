//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *          Either all agents or 2 list of agents: one for current and one for past (deleted) agents. 
 *          This is used when an admin reads a Scheduled Report.
 *          Each agent table has the following column headings:
 *          "User Id", "Last Name", "First Name", "Hiragana Last Name" and "Hiragana First Name".
 *        
 * 
 * <p>Java-Klasse für CallCenterScheduledReportAgentSelectionAdminRead complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterScheduledReportAgentSelectionAdminRead">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <choice>
 *         <element name="allAgent" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <sequence>
 *           <element name="currentAgentTable" type="{C}OCITable"/>
 *           <element name="pastAgentTable" type="{C}OCITable"/>
 *         </sequence>
 *       </choice>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterScheduledReportAgentSelectionAdminRead", propOrder = {
    "allAgent",
    "currentAgentTable",
    "pastAgentTable"
})
public class CallCenterScheduledReportAgentSelectionAdminRead {

    protected Boolean allAgent;
    protected OCITable currentAgentTable;
    protected OCITable pastAgentTable;

    /**
     * Ruft den Wert der allAgent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllAgent() {
        return allAgent;
    }

    /**
     * Legt den Wert der allAgent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllAgent(Boolean value) {
        this.allAgent = value;
    }

    /**
     * Ruft den Wert der currentAgentTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getCurrentAgentTable() {
        return currentAgentTable;
    }

    /**
     * Legt den Wert der currentAgentTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setCurrentAgentTable(OCITable value) {
        this.currentAgentTable = value;
    }

    /**
     * Ruft den Wert der pastAgentTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getPastAgentTable() {
        return pastAgentTable;
    }

    /**
     * Legt den Wert der pastAgentTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setPastAgentTable(OCITable value) {
        this.pastAgentTable = value;
    }

}
