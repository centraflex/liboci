//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AutomaticCallbackReleaseCause.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AutomaticCallbackReleaseCause">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Busy"/>
 *     <enumeration value="Forbidden"/>
 *     <enumeration value="Global Failure"/>
 *     <enumeration value="Request Failure"/>
 *     <enumeration value="Server Failure"/>
 *     <enumeration value="Translation Failure"/>
 *     <enumeration value="Temporarily Unavailable"/>
 *     <enumeration value="User Not Found"/>
 *     <enumeration value="Request Timeout"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AutomaticCallbackReleaseCause")
@XmlEnum
public enum AutomaticCallbackReleaseCause {

    @XmlEnumValue("Busy")
    BUSY("Busy"),
    @XmlEnumValue("Forbidden")
    FORBIDDEN("Forbidden"),
    @XmlEnumValue("Global Failure")
    GLOBAL_FAILURE("Global Failure"),
    @XmlEnumValue("Request Failure")
    REQUEST_FAILURE("Request Failure"),
    @XmlEnumValue("Server Failure")
    SERVER_FAILURE("Server Failure"),
    @XmlEnumValue("Translation Failure")
    TRANSLATION_FAILURE("Translation Failure"),
    @XmlEnumValue("Temporarily Unavailable")
    TEMPORARILY_UNAVAILABLE("Temporarily Unavailable"),
    @XmlEnumValue("User Not Found")
    USER_NOT_FOUND("User Not Found"),
    @XmlEnumValue("Request Timeout")
    REQUEST_TIMEOUT("Request Timeout");
    private final String value;

    AutomaticCallbackReleaseCause(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AutomaticCallbackReleaseCause fromValue(String v) {
        for (AutomaticCallbackReleaseCause c: AutomaticCallbackReleaseCause.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
