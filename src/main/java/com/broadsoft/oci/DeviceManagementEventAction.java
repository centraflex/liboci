//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeviceManagementEventAction.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DeviceManagementEventAction">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Delete"/>
 *     <enumeration value="Download"/>
 *     <enumeration value="Rebuild"/>
 *     <enumeration value="Reset"/>
 *     <enumeration value="Upload"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DeviceManagementEventAction")
@XmlEnum
public enum DeviceManagementEventAction {

    @XmlEnumValue("Delete")
    DELETE("Delete"),
    @XmlEnumValue("Download")
    DOWNLOAD("Download"),
    @XmlEnumValue("Rebuild")
    REBUILD("Rebuild"),
    @XmlEnumValue("Reset")
    RESET("Reset"),
    @XmlEnumValue("Upload")
    UPLOAD("Upload");
    private final String value;

    DeviceManagementEventAction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeviceManagementEventAction fromValue(String v) {
        for (DeviceManagementEventAction c: DeviceManagementEventAction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
