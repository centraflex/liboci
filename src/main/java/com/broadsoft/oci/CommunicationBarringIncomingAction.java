//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CommunicationBarringIncomingAction.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CommunicationBarringIncomingAction">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Allow"/>
 *     <enumeration value="Allow Timed"/>
 *     <enumeration value="Block"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CommunicationBarringIncomingAction")
@XmlEnum
public enum CommunicationBarringIncomingAction {

    @XmlEnumValue("Allow")
    ALLOW("Allow"),
    @XmlEnumValue("Allow Timed")
    ALLOW_TIMED("Allow Timed"),
    @XmlEnumValue("Block")
    BLOCK("Block");
    private final String value;

    CommunicationBarringIncomingAction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommunicationBarringIncomingAction fromValue(String v) {
        for (CommunicationBarringIncomingAction c: CommunicationBarringIncomingAction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
