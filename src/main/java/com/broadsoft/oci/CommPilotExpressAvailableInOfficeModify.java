//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         CommPilot Express Available In Office Settings.
 *       
 * 
 * <p>Java-Klasse für CommPilotExpressAvailableInOfficeModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CommPilotExpressAvailableInOfficeModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="additionalPhoneNumberToRing" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="busySetting" type="{}CommPilotExpressRedirectionModify" minOccurs="0"/>
 *         <element name="noAnswerSetting" type="{}CommPilotExpressRedirectionModify" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommPilotExpressAvailableInOfficeModify", propOrder = {
    "additionalPhoneNumberToRing",
    "busySetting",
    "noAnswerSetting"
})
public class CommPilotExpressAvailableInOfficeModify {

    @XmlElementRef(name = "additionalPhoneNumberToRing", type = JAXBElement.class, required = false)
    protected JAXBElement<String> additionalPhoneNumberToRing;
    protected CommPilotExpressRedirectionModify busySetting;
    protected CommPilotExpressRedirectionModify noAnswerSetting;

    /**
     * Ruft den Wert der additionalPhoneNumberToRing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAdditionalPhoneNumberToRing() {
        return additionalPhoneNumberToRing;
    }

    /**
     * Legt den Wert der additionalPhoneNumberToRing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAdditionalPhoneNumberToRing(JAXBElement<String> value) {
        this.additionalPhoneNumberToRing = value;
    }

    /**
     * Ruft den Wert der busySetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressRedirectionModify }
     *     
     */
    public CommPilotExpressRedirectionModify getBusySetting() {
        return busySetting;
    }

    /**
     * Legt den Wert der busySetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressRedirectionModify }
     *     
     */
    public void setBusySetting(CommPilotExpressRedirectionModify value) {
        this.busySetting = value;
    }

    /**
     * Ruft den Wert der noAnswerSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressRedirectionModify }
     *     
     */
    public CommPilotExpressRedirectionModify getNoAnswerSetting() {
        return noAnswerSetting;
    }

    /**
     * Legt den Wert der noAnswerSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressRedirectionModify }
     *     
     */
    public void setNoAnswerSetting(CommPilotExpressRedirectionModify value) {
        this.noAnswerSetting = value;
    }

}
