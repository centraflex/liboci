//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the Third Party Voice Mail Support settings for a user.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserThirdPartyVoiceMailSupportModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserThirdPartyVoiceMailSupportModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="busyRedirectToVoiceMail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="noAnswerRedirectToVoiceMail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="serverSelection" type="{}ThirdPartyVoiceMailSupportServerSelection" minOccurs="0"/>
 *         <element name="userServer" type="{}ThirdPartyVoiceMailSupportMailServer" minOccurs="0"/>
 *         <element name="mailboxIdType" type="{}ThirdPartyVoiceMailSupportMailboxIdType" minOccurs="0"/>
 *         <element name="mailboxURL" type="{}SIPURI" minOccurs="0"/>
 *         <element name="noAnswerNumberOfRings" type="{}ThirdPartyVoiceMailSupportNumberOfRings" minOccurs="0"/>
 *         <element name="alwaysRedirectToVoiceMail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="outOfPrimaryZoneRedirectToVoiceMail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserThirdPartyVoiceMailSupportModifyRequest", propOrder = {
    "userId",
    "isActive",
    "busyRedirectToVoiceMail",
    "noAnswerRedirectToVoiceMail",
    "serverSelection",
    "userServer",
    "mailboxIdType",
    "mailboxURL",
    "noAnswerNumberOfRings",
    "alwaysRedirectToVoiceMail",
    "outOfPrimaryZoneRedirectToVoiceMail"
})
public class UserThirdPartyVoiceMailSupportModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean isActive;
    protected Boolean busyRedirectToVoiceMail;
    protected Boolean noAnswerRedirectToVoiceMail;
    @XmlSchemaType(name = "token")
    protected ThirdPartyVoiceMailSupportServerSelection serverSelection;
    @XmlElementRef(name = "userServer", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userServer;
    @XmlSchemaType(name = "token")
    protected ThirdPartyVoiceMailSupportMailboxIdType mailboxIdType;
    @XmlElementRef(name = "mailboxURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mailboxURL;
    protected Integer noAnswerNumberOfRings;
    protected Boolean alwaysRedirectToVoiceMail;
    protected Boolean outOfPrimaryZoneRedirectToVoiceMail;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der busyRedirectToVoiceMail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBusyRedirectToVoiceMail() {
        return busyRedirectToVoiceMail;
    }

    /**
     * Legt den Wert der busyRedirectToVoiceMail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBusyRedirectToVoiceMail(Boolean value) {
        this.busyRedirectToVoiceMail = value;
    }

    /**
     * Ruft den Wert der noAnswerRedirectToVoiceMail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNoAnswerRedirectToVoiceMail() {
        return noAnswerRedirectToVoiceMail;
    }

    /**
     * Legt den Wert der noAnswerRedirectToVoiceMail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoAnswerRedirectToVoiceMail(Boolean value) {
        this.noAnswerRedirectToVoiceMail = value;
    }

    /**
     * Ruft den Wert der serverSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ThirdPartyVoiceMailSupportServerSelection }
     *     
     */
    public ThirdPartyVoiceMailSupportServerSelection getServerSelection() {
        return serverSelection;
    }

    /**
     * Legt den Wert der serverSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ThirdPartyVoiceMailSupportServerSelection }
     *     
     */
    public void setServerSelection(ThirdPartyVoiceMailSupportServerSelection value) {
        this.serverSelection = value;
    }

    /**
     * Ruft den Wert der userServer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserServer() {
        return userServer;
    }

    /**
     * Legt den Wert der userServer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserServer(JAXBElement<String> value) {
        this.userServer = value;
    }

    /**
     * Ruft den Wert der mailboxIdType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ThirdPartyVoiceMailSupportMailboxIdType }
     *     
     */
    public ThirdPartyVoiceMailSupportMailboxIdType getMailboxIdType() {
        return mailboxIdType;
    }

    /**
     * Legt den Wert der mailboxIdType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ThirdPartyVoiceMailSupportMailboxIdType }
     *     
     */
    public void setMailboxIdType(ThirdPartyVoiceMailSupportMailboxIdType value) {
        this.mailboxIdType = value;
    }

    /**
     * Ruft den Wert der mailboxURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMailboxURL() {
        return mailboxURL;
    }

    /**
     * Legt den Wert der mailboxURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMailboxURL(JAXBElement<String> value) {
        this.mailboxURL = value;
    }

    /**
     * Ruft den Wert der noAnswerNumberOfRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNoAnswerNumberOfRings() {
        return noAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der noAnswerNumberOfRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNoAnswerNumberOfRings(Integer value) {
        this.noAnswerNumberOfRings = value;
    }

    /**
     * Ruft den Wert der alwaysRedirectToVoiceMail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlwaysRedirectToVoiceMail() {
        return alwaysRedirectToVoiceMail;
    }

    /**
     * Legt den Wert der alwaysRedirectToVoiceMail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlwaysRedirectToVoiceMail(Boolean value) {
        this.alwaysRedirectToVoiceMail = value;
    }

    /**
     * Ruft den Wert der outOfPrimaryZoneRedirectToVoiceMail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOutOfPrimaryZoneRedirectToVoiceMail() {
        return outOfPrimaryZoneRedirectToVoiceMail;
    }

    /**
     * Legt den Wert der outOfPrimaryZoneRedirectToVoiceMail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOutOfPrimaryZoneRedirectToVoiceMail(Boolean value) {
        this.outOfPrimaryZoneRedirectToVoiceMail = value;
    }

}
