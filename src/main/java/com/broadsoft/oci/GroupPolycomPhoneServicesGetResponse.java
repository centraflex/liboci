//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupPolycomPhoneServicesGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupPolycomPhoneServicesGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupPolycomPhoneServicesGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="includeGroupCommonPhoneListInDirectory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeGroupCustomContactDirectoryInDirectory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="groupCustomContactDirectory" type="{}CustomContactDirectoryName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupPolycomPhoneServicesGetResponse", propOrder = {
    "includeGroupCommonPhoneListInDirectory",
    "includeGroupCustomContactDirectoryInDirectory",
    "groupCustomContactDirectory"
})
public class GroupPolycomPhoneServicesGetResponse
    extends OCIDataResponse
{

    protected boolean includeGroupCommonPhoneListInDirectory;
    protected boolean includeGroupCustomContactDirectoryInDirectory;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupCustomContactDirectory;

    /**
     * Ruft den Wert der includeGroupCommonPhoneListInDirectory-Eigenschaft ab.
     * 
     */
    public boolean isIncludeGroupCommonPhoneListInDirectory() {
        return includeGroupCommonPhoneListInDirectory;
    }

    /**
     * Legt den Wert der includeGroupCommonPhoneListInDirectory-Eigenschaft fest.
     * 
     */
    public void setIncludeGroupCommonPhoneListInDirectory(boolean value) {
        this.includeGroupCommonPhoneListInDirectory = value;
    }

    /**
     * Ruft den Wert der includeGroupCustomContactDirectoryInDirectory-Eigenschaft ab.
     * 
     */
    public boolean isIncludeGroupCustomContactDirectoryInDirectory() {
        return includeGroupCustomContactDirectoryInDirectory;
    }

    /**
     * Legt den Wert der includeGroupCustomContactDirectoryInDirectory-Eigenschaft fest.
     * 
     */
    public void setIncludeGroupCustomContactDirectoryInDirectory(boolean value) {
        this.includeGroupCustomContactDirectoryInDirectory = value;
    }

    /**
     * Ruft den Wert der groupCustomContactDirectory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupCustomContactDirectory() {
        return groupCustomContactDirectory;
    }

    /**
     * Legt den Wert der groupCustomContactDirectory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupCustomContactDirectory(String value) {
        this.groupCustomContactDirectory = value;
    }

}
