//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserResourcePriorityGetRequest.
 *         Contains the Resource Priority settings of a user.
 *         If useDefaultResourcePriority is true, parameter resourcePriority will not be returned. 
 *       
 * 
 * <p>Java-Klasse für UserResourcePriorityGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserResourcePriorityGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useDefaultResourcePriority" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="defaultResourcePriority" type="{}ResourcePriorityValue"/>
 *         <element name="userResourcePriority" type="{}ResourcePriorityValue"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserResourcePriorityGetResponse", propOrder = {
    "useDefaultResourcePriority",
    "defaultResourcePriority",
    "userResourcePriority"
})
public class UserResourcePriorityGetResponse
    extends OCIDataResponse
{

    protected boolean useDefaultResourcePriority;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ResourcePriorityValue defaultResourcePriority;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ResourcePriorityValue userResourcePriority;

    /**
     * Ruft den Wert der useDefaultResourcePriority-Eigenschaft ab.
     * 
     */
    public boolean isUseDefaultResourcePriority() {
        return useDefaultResourcePriority;
    }

    /**
     * Legt den Wert der useDefaultResourcePriority-Eigenschaft fest.
     * 
     */
    public void setUseDefaultResourcePriority(boolean value) {
        this.useDefaultResourcePriority = value;
    }

    /**
     * Ruft den Wert der defaultResourcePriority-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResourcePriorityValue }
     *     
     */
    public ResourcePriorityValue getDefaultResourcePriority() {
        return defaultResourcePriority;
    }

    /**
     * Legt den Wert der defaultResourcePriority-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResourcePriorityValue }
     *     
     */
    public void setDefaultResourcePriority(ResourcePriorityValue value) {
        this.defaultResourcePriority = value;
    }

    /**
     * Ruft den Wert der userResourcePriority-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResourcePriorityValue }
     *     
     */
    public ResourcePriorityValue getUserResourcePriority() {
        return userResourcePriority;
    }

    /**
     * Legt den Wert der userResourcePriority-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResourcePriorityValue }
     *     
     */
    public void setUserResourcePriority(ResourcePriorityValue value) {
        this.userResourcePriority = value;
    }

}
