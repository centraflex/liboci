//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Get list of Call Recording platforms.
 *         The response is either SystemCallRecordingGetPlatformListResponse or ErrorResponse.
 *         
 *         The possible values for "Support Video Rec" can be either true or false.  
 *         Schema version values include: 1.0, 2.0, 3.0
 *         
 *         Replaced by: SystemCallRecordingGetPlatformListRequest20sp1 in AS data mode
 *       
 * 
 * <p>Java-Klasse für SystemCallRecordingGetPlatformListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallRecordingGetPlatformListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallRecordingGetPlatformListRequest")
public class SystemCallRecordingGetPlatformListRequest
    extends OCIRequest
{


}
