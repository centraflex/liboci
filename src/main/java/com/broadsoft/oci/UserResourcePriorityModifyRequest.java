//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the Resource Priority settings of a user.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserResourcePriorityModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserResourcePriorityModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="useDefaultResourcePriority" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="userResourcePriority" type="{}ResourcePriorityValue" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserResourcePriorityModifyRequest", propOrder = {
    "userId",
    "useDefaultResourcePriority",
    "userResourcePriority"
})
public class UserResourcePriorityModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean useDefaultResourcePriority;
    @XmlSchemaType(name = "token")
    protected ResourcePriorityValue userResourcePriority;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der useDefaultResourcePriority-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseDefaultResourcePriority() {
        return useDefaultResourcePriority;
    }

    /**
     * Legt den Wert der useDefaultResourcePriority-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseDefaultResourcePriority(Boolean value) {
        this.useDefaultResourcePriority = value;
    }

    /**
     * Ruft den Wert der userResourcePriority-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResourcePriorityValue }
     *     
     */
    public ResourcePriorityValue getUserResourcePriority() {
        return userResourcePriority;
    }

    /**
     * Legt den Wert der userResourcePriority-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResourcePriorityValue }
     *     
     */
    public void setUserResourcePriority(ResourcePriorityValue value) {
        this.userResourcePriority = value;
    }

}
