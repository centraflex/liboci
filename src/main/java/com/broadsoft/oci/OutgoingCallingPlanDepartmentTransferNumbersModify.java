//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Outgoing Calling Plan transfer numbers for a department.
 *       
 * 
 * <p>Java-Klasse für OutgoingCallingPlanDepartmentTransferNumbersModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OutgoingCallingPlanDepartmentTransferNumbersModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="departmentKey" type="{}DepartmentKey"/>
 *         <element name="transferNumbers" type="{}OutgoingCallingPlanTransferNumbersModify" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingCallingPlanDepartmentTransferNumbersModify", propOrder = {
    "departmentKey",
    "transferNumbers"
})
public class OutgoingCallingPlanDepartmentTransferNumbersModify {

    @XmlElement(required = true)
    protected DepartmentKey departmentKey;
    protected OutgoingCallingPlanTransferNumbersModify transferNumbers;

    /**
     * Ruft den Wert der departmentKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DepartmentKey }
     *     
     */
    public DepartmentKey getDepartmentKey() {
        return departmentKey;
    }

    /**
     * Legt den Wert der departmentKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DepartmentKey }
     *     
     */
    public void setDepartmentKey(DepartmentKey value) {
        this.departmentKey = value;
    }

    /**
     * Ruft den Wert der transferNumbers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanTransferNumbersModify }
     *     
     */
    public OutgoingCallingPlanTransferNumbersModify getTransferNumbers() {
        return transferNumbers;
    }

    /**
     * Legt den Wert der transferNumbers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanTransferNumbersModify }
     *     
     */
    public void setTransferNumbers(OutgoingCallingPlanTransferNumbersModify value) {
        this.transferNumbers = value;
    }

}
