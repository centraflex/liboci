//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeviceManagementEventStatusInProgressOrPending.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DeviceManagementEventStatusInProgressOrPending">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Pending"/>
 *     <enumeration value="Queued"/>
 *     <enumeration value="In Progress"/>
 *     <enumeration value="Process On Other Host"/>
 *     <enumeration value="Stale"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DeviceManagementEventStatusInProgressOrPending")
@XmlEnum
public enum DeviceManagementEventStatusInProgressOrPending {

    @XmlEnumValue("Pending")
    PENDING("Pending"),
    @XmlEnumValue("Queued")
    QUEUED("Queued"),
    @XmlEnumValue("In Progress")
    IN_PROGRESS("In Progress"),
    @XmlEnumValue("Process On Other Host")
    PROCESS_ON_OTHER_HOST("Process On Other Host"),
    @XmlEnumValue("Stale")
    STALE("Stale");
    private final String value;

    DeviceManagementEventStatusInProgressOrPending(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeviceManagementEventStatusInProgressOrPending fromValue(String v) {
        for (DeviceManagementEventStatusInProgressOrPending c: DeviceManagementEventStatusInProgressOrPending.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
