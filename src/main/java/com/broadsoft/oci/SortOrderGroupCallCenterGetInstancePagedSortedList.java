//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Used to sort the GroupCallCenterGetInstancePagedSortedListRequest request.
 *       
 * 
 * <p>Java-Klasse für SortOrderGroupCallCenterGetInstancePagedSortedList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SortOrderGroupCallCenterGetInstancePagedSortedList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <choice>
 *         <element name="sortByUserId" type="{}SortByUserId"/>
 *         <element name="sortByCallCenterName" type="{}SortByCallCenterName"/>
 *         <element name="sortByDn" type="{}SortByDn"/>
 *         <element name="sortByExtension" type="{}SortByExtension"/>
 *         <element name="sortByDepartmentName" type="{}SortByDepartmentName"/>
 *         <element name="sortByHuntPolicy" type="{}SortByHuntPolicy"/>
 *         <element name="sortByCallCenterType" type="{}SortByCallCenterType"/>
 *       </choice>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SortOrderGroupCallCenterGetInstancePagedSortedList", propOrder = {
    "sortByUserId",
    "sortByCallCenterName",
    "sortByDn",
    "sortByExtension",
    "sortByDepartmentName",
    "sortByHuntPolicy",
    "sortByCallCenterType"
})
public class SortOrderGroupCallCenterGetInstancePagedSortedList {

    protected SortByUserId sortByUserId;
    protected SortByCallCenterName sortByCallCenterName;
    protected SortByDn sortByDn;
    protected SortByExtension sortByExtension;
    protected SortByDepartmentName sortByDepartmentName;
    protected SortByHuntPolicy sortByHuntPolicy;
    protected SortByCallCenterType sortByCallCenterType;

    /**
     * Ruft den Wert der sortByUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByUserId }
     *     
     */
    public SortByUserId getSortByUserId() {
        return sortByUserId;
    }

    /**
     * Legt den Wert der sortByUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByUserId }
     *     
     */
    public void setSortByUserId(SortByUserId value) {
        this.sortByUserId = value;
    }

    /**
     * Ruft den Wert der sortByCallCenterName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByCallCenterName }
     *     
     */
    public SortByCallCenterName getSortByCallCenterName() {
        return sortByCallCenterName;
    }

    /**
     * Legt den Wert der sortByCallCenterName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByCallCenterName }
     *     
     */
    public void setSortByCallCenterName(SortByCallCenterName value) {
        this.sortByCallCenterName = value;
    }

    /**
     * Ruft den Wert der sortByDn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByDn }
     *     
     */
    public SortByDn getSortByDn() {
        return sortByDn;
    }

    /**
     * Legt den Wert der sortByDn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByDn }
     *     
     */
    public void setSortByDn(SortByDn value) {
        this.sortByDn = value;
    }

    /**
     * Ruft den Wert der sortByExtension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByExtension }
     *     
     */
    public SortByExtension getSortByExtension() {
        return sortByExtension;
    }

    /**
     * Legt den Wert der sortByExtension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByExtension }
     *     
     */
    public void setSortByExtension(SortByExtension value) {
        this.sortByExtension = value;
    }

    /**
     * Ruft den Wert der sortByDepartmentName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByDepartmentName }
     *     
     */
    public SortByDepartmentName getSortByDepartmentName() {
        return sortByDepartmentName;
    }

    /**
     * Legt den Wert der sortByDepartmentName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByDepartmentName }
     *     
     */
    public void setSortByDepartmentName(SortByDepartmentName value) {
        this.sortByDepartmentName = value;
    }

    /**
     * Ruft den Wert der sortByHuntPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByHuntPolicy }
     *     
     */
    public SortByHuntPolicy getSortByHuntPolicy() {
        return sortByHuntPolicy;
    }

    /**
     * Legt den Wert der sortByHuntPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByHuntPolicy }
     *     
     */
    public void setSortByHuntPolicy(SortByHuntPolicy value) {
        this.sortByHuntPolicy = value;
    }

    /**
     * Ruft den Wert der sortByCallCenterType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByCallCenterType }
     *     
     */
    public SortByCallCenterType getSortByCallCenterType() {
        return sortByCallCenterType;
    }

    /**
     * Legt den Wert der sortByCallCenterType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByCallCenterType }
     *     
     */
    public void setSortByCallCenterType(SortByCallCenterType value) {
        this.sortByCallCenterType = value;
    }

}
