//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify an enterprise level call center reporting scheduled report.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         The startDate element is adjusted to the first occurrence of the recurrent schedule that comes at or after startDate.
 *       
 * 
 * <p>Java-Klasse für EnterpriseCallCenterEnhancedReportingScheduledReportModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseCallCenterEnhancedReportingScheduledReportModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="name" type="{}CallCenterScheduledReportName"/>
 *         <element name="newName" type="{}CallCenterScheduledReportName" minOccurs="0"/>
 *         <element name="description" type="{}CallCenterScheduledReportDescription" minOccurs="0"/>
 *         <element name="schedule" type="{}CallCenterReportSchedule" minOccurs="0"/>
 *         <element name="samplingPeriod" type="{}CallCenterReportSamplingPeriod" minOccurs="0"/>
 *         <element name="startDayOfWeek" type="{}DayOfWeek" minOccurs="0"/>
 *         <element name="reportTimeZone" type="{}TimeZone" minOccurs="0"/>
 *         <element name="reportDateFormat" type="{}CallCenterReportDateFormat" minOccurs="0"/>
 *         <element name="reportTimeFormat" type="{}CallCenterReportTimeFormat" minOccurs="0"/>
 *         <element name="reportInterval" type="{}CallCenterReportInterval" minOccurs="0"/>
 *         <element name="reportFormat" type="{}CallCenterReportFileFormat" minOccurs="0"/>
 *         <element name="agent" type="{}CallCenterScheduledReportAgentSelection" minOccurs="0"/>
 *         <choice minOccurs="0">
 *           <element name="callCenter" type="{}CallCenterScheduledReportCallCenterSelection"/>
 *           <element name="dnis" type="{}CallCenterScheduledReportDNISSelection"/>
 *         </choice>
 *         <element name="callCompletionThresholdSeconds" type="{}CallCenterReportThresholdSeconds" minOccurs="0"/>
 *         <element name="shortDurationThresholdSeconds" type="{}CallCenterReportThresholdSeconds" minOccurs="0"/>
 *         <element name="serviceLevelThresholdSeconds" type="{}CallCenterReportServiceLevelThresholdReplacementList" minOccurs="0"/>
 *         <element name="serviceLevelInclusions" type="{}CallCenterScheduledReportServiceLevelInclusionsModify" minOccurs="0"/>
 *         <element name="serviceLevelObjectivePercentage" type="{}CallCenterReportServiceLevelObjective" minOccurs="0"/>
 *         <element name="abandonedCallThresholdSeconds" type="{}CallCenterReportAbandonedCallThresholdReplacementList" minOccurs="0"/>
 *         <element name="emailAddress" type="{}CallCenterReportReplacementEmailList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseCallCenterEnhancedReportingScheduledReportModifyRequest", propOrder = {
    "serviceProviderId",
    "name",
    "newName",
    "description",
    "schedule",
    "samplingPeriod",
    "startDayOfWeek",
    "reportTimeZone",
    "reportDateFormat",
    "reportTimeFormat",
    "reportInterval",
    "reportFormat",
    "agent",
    "callCenter",
    "dnis",
    "callCompletionThresholdSeconds",
    "shortDurationThresholdSeconds",
    "serviceLevelThresholdSeconds",
    "serviceLevelInclusions",
    "serviceLevelObjectivePercentage",
    "abandonedCallThresholdSeconds",
    "emailAddress"
})
public class EnterpriseCallCenterEnhancedReportingScheduledReportModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newName;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    protected CallCenterReportSchedule schedule;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String samplingPeriod;
    @XmlSchemaType(name = "NMTOKEN")
    protected DayOfWeek startDayOfWeek;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String reportTimeZone;
    @XmlSchemaType(name = "token")
    protected CallCenterReportDateFormat reportDateFormat;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String reportTimeFormat;
    protected CallCenterReportInterval reportInterval;
    @XmlSchemaType(name = "token")
    protected CallCenterReportFileFormat reportFormat;
    protected CallCenterScheduledReportAgentSelection agent;
    protected CallCenterScheduledReportCallCenterSelection callCenter;
    protected CallCenterScheduledReportDNISSelection dnis;
    protected Integer callCompletionThresholdSeconds;
    protected Integer shortDurationThresholdSeconds;
    protected CallCenterReportServiceLevelThresholdReplacementList serviceLevelThresholdSeconds;
    protected CallCenterScheduledReportServiceLevelInclusionsModify serviceLevelInclusions;
    @XmlElementRef(name = "serviceLevelObjectivePercentage", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> serviceLevelObjectivePercentage;
    protected CallCenterReportAbandonedCallThresholdReplacementList abandonedCallThresholdSeconds;
    protected CallCenterReportReplacementEmailList emailAddress;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der newName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewName() {
        return newName;
    }

    /**
     * Legt den Wert der newName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewName(String value) {
        this.newName = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der schedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportSchedule }
     *     
     */
    public CallCenterReportSchedule getSchedule() {
        return schedule;
    }

    /**
     * Legt den Wert der schedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportSchedule }
     *     
     */
    public void setSchedule(CallCenterReportSchedule value) {
        this.schedule = value;
    }

    /**
     * Ruft den Wert der samplingPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSamplingPeriod() {
        return samplingPeriod;
    }

    /**
     * Legt den Wert der samplingPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSamplingPeriod(String value) {
        this.samplingPeriod = value;
    }

    /**
     * Ruft den Wert der startDayOfWeek-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DayOfWeek }
     *     
     */
    public DayOfWeek getStartDayOfWeek() {
        return startDayOfWeek;
    }

    /**
     * Legt den Wert der startDayOfWeek-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DayOfWeek }
     *     
     */
    public void setStartDayOfWeek(DayOfWeek value) {
        this.startDayOfWeek = value;
    }

    /**
     * Ruft den Wert der reportTimeZone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportTimeZone() {
        return reportTimeZone;
    }

    /**
     * Legt den Wert der reportTimeZone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportTimeZone(String value) {
        this.reportTimeZone = value;
    }

    /**
     * Ruft den Wert der reportDateFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportDateFormat }
     *     
     */
    public CallCenterReportDateFormat getReportDateFormat() {
        return reportDateFormat;
    }

    /**
     * Legt den Wert der reportDateFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportDateFormat }
     *     
     */
    public void setReportDateFormat(CallCenterReportDateFormat value) {
        this.reportDateFormat = value;
    }

    /**
     * Ruft den Wert der reportTimeFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportTimeFormat() {
        return reportTimeFormat;
    }

    /**
     * Legt den Wert der reportTimeFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportTimeFormat(String value) {
        this.reportTimeFormat = value;
    }

    /**
     * Ruft den Wert der reportInterval-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportInterval }
     *     
     */
    public CallCenterReportInterval getReportInterval() {
        return reportInterval;
    }

    /**
     * Legt den Wert der reportInterval-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportInterval }
     *     
     */
    public void setReportInterval(CallCenterReportInterval value) {
        this.reportInterval = value;
    }

    /**
     * Ruft den Wert der reportFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportFileFormat }
     *     
     */
    public CallCenterReportFileFormat getReportFormat() {
        return reportFormat;
    }

    /**
     * Legt den Wert der reportFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportFileFormat }
     *     
     */
    public void setReportFormat(CallCenterReportFileFormat value) {
        this.reportFormat = value;
    }

    /**
     * Ruft den Wert der agent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterScheduledReportAgentSelection }
     *     
     */
    public CallCenterScheduledReportAgentSelection getAgent() {
        return agent;
    }

    /**
     * Legt den Wert der agent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterScheduledReportAgentSelection }
     *     
     */
    public void setAgent(CallCenterScheduledReportAgentSelection value) {
        this.agent = value;
    }

    /**
     * Ruft den Wert der callCenter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterScheduledReportCallCenterSelection }
     *     
     */
    public CallCenterScheduledReportCallCenterSelection getCallCenter() {
        return callCenter;
    }

    /**
     * Legt den Wert der callCenter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterScheduledReportCallCenterSelection }
     *     
     */
    public void setCallCenter(CallCenterScheduledReportCallCenterSelection value) {
        this.callCenter = value;
    }

    /**
     * Ruft den Wert der dnis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterScheduledReportDNISSelection }
     *     
     */
    public CallCenterScheduledReportDNISSelection getDnis() {
        return dnis;
    }

    /**
     * Legt den Wert der dnis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterScheduledReportDNISSelection }
     *     
     */
    public void setDnis(CallCenterScheduledReportDNISSelection value) {
        this.dnis = value;
    }

    /**
     * Ruft den Wert der callCompletionThresholdSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCallCompletionThresholdSeconds() {
        return callCompletionThresholdSeconds;
    }

    /**
     * Legt den Wert der callCompletionThresholdSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCallCompletionThresholdSeconds(Integer value) {
        this.callCompletionThresholdSeconds = value;
    }

    /**
     * Ruft den Wert der shortDurationThresholdSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getShortDurationThresholdSeconds() {
        return shortDurationThresholdSeconds;
    }

    /**
     * Legt den Wert der shortDurationThresholdSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setShortDurationThresholdSeconds(Integer value) {
        this.shortDurationThresholdSeconds = value;
    }

    /**
     * Ruft den Wert der serviceLevelThresholdSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportServiceLevelThresholdReplacementList }
     *     
     */
    public CallCenterReportServiceLevelThresholdReplacementList getServiceLevelThresholdSeconds() {
        return serviceLevelThresholdSeconds;
    }

    /**
     * Legt den Wert der serviceLevelThresholdSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportServiceLevelThresholdReplacementList }
     *     
     */
    public void setServiceLevelThresholdSeconds(CallCenterReportServiceLevelThresholdReplacementList value) {
        this.serviceLevelThresholdSeconds = value;
    }

    /**
     * Ruft den Wert der serviceLevelInclusions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterScheduledReportServiceLevelInclusionsModify }
     *     
     */
    public CallCenterScheduledReportServiceLevelInclusionsModify getServiceLevelInclusions() {
        return serviceLevelInclusions;
    }

    /**
     * Legt den Wert der serviceLevelInclusions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterScheduledReportServiceLevelInclusionsModify }
     *     
     */
    public void setServiceLevelInclusions(CallCenterScheduledReportServiceLevelInclusionsModify value) {
        this.serviceLevelInclusions = value;
    }

    /**
     * Ruft den Wert der serviceLevelObjectivePercentage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getServiceLevelObjectivePercentage() {
        return serviceLevelObjectivePercentage;
    }

    /**
     * Legt den Wert der serviceLevelObjectivePercentage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setServiceLevelObjectivePercentage(JAXBElement<Integer> value) {
        this.serviceLevelObjectivePercentage = value;
    }

    /**
     * Ruft den Wert der abandonedCallThresholdSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportAbandonedCallThresholdReplacementList }
     *     
     */
    public CallCenterReportAbandonedCallThresholdReplacementList getAbandonedCallThresholdSeconds() {
        return abandonedCallThresholdSeconds;
    }

    /**
     * Legt den Wert der abandonedCallThresholdSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportAbandonedCallThresholdReplacementList }
     *     
     */
    public void setAbandonedCallThresholdSeconds(CallCenterReportAbandonedCallThresholdReplacementList value) {
        this.abandonedCallThresholdSeconds = value;
    }

    /**
     * Ruft den Wert der emailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportReplacementEmailList }
     *     
     */
    public CallCenterReportReplacementEmailList getEmailAddress() {
        return emailAddress;
    }

    /**
     * Legt den Wert der emailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportReplacementEmailList }
     *     
     */
    public void setEmailAddress(CallCenterReportReplacementEmailList value) {
        this.emailAddress = value;
    }

}
