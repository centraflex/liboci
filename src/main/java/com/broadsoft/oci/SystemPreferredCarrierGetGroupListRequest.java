//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Requests a list of groups using the specified carrier.  It is possible to restrict the
 *         number of rows returned by specifying various search criteria. Multiple search criteria
 *         are logically ANDed together.
 *         The response is either a SystemPreferredCarrierGetGroupListResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemPreferredCarrierGetGroupListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPreferredCarrierGetGroupListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="carrier" type="{}PreferredCarrierName"/>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaGroupId" type="{}SearchCriteriaGroupId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaGroupName" type="{}SearchCriteriaGroupName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactServiceProvider" type="{}SearchCriteriaExactServiceProvider" minOccurs="0"/>
 *         <element name="searchCriteriaServiceProviderId" type="{}SearchCriteriaServiceProviderId" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPreferredCarrierGetGroupListRequest", propOrder = {
    "carrier",
    "responseSizeLimit",
    "searchCriteriaGroupId",
    "searchCriteriaGroupName",
    "searchCriteriaExactServiceProvider",
    "searchCriteriaServiceProviderId"
})
public class SystemPreferredCarrierGetGroupListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String carrier;
    protected Integer responseSizeLimit;
    protected List<SearchCriteriaGroupId> searchCriteriaGroupId;
    protected List<SearchCriteriaGroupName> searchCriteriaGroupName;
    protected SearchCriteriaExactServiceProvider searchCriteriaExactServiceProvider;
    protected List<SearchCriteriaServiceProviderId> searchCriteriaServiceProviderId;

    /**
     * Ruft den Wert der carrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     * Legt den Wert der carrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrier(String value) {
        this.carrier = value;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaGroupId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaGroupId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaGroupId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaGroupId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaGroupId property.
     */
    public List<SearchCriteriaGroupId> getSearchCriteriaGroupId() {
        if (searchCriteriaGroupId == null) {
            searchCriteriaGroupId = new ArrayList<>();
        }
        return this.searchCriteriaGroupId;
    }

    /**
     * Gets the value of the searchCriteriaGroupName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaGroupName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaGroupName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaGroupName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaGroupName property.
     */
    public List<SearchCriteriaGroupName> getSearchCriteriaGroupName() {
        if (searchCriteriaGroupName == null) {
            searchCriteriaGroupName = new ArrayList<>();
        }
        return this.searchCriteriaGroupName;
    }

    /**
     * Ruft den Wert der searchCriteriaExactServiceProvider-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactServiceProvider }
     *     
     */
    public SearchCriteriaExactServiceProvider getSearchCriteriaExactServiceProvider() {
        return searchCriteriaExactServiceProvider;
    }

    /**
     * Legt den Wert der searchCriteriaExactServiceProvider-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactServiceProvider }
     *     
     */
    public void setSearchCriteriaExactServiceProvider(SearchCriteriaExactServiceProvider value) {
        this.searchCriteriaExactServiceProvider = value;
    }

    /**
     * Gets the value of the searchCriteriaServiceProviderId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaServiceProviderId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaServiceProviderId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaServiceProviderId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaServiceProviderId property.
     */
    public List<SearchCriteriaServiceProviderId> getSearchCriteriaServiceProviderId() {
        if (searchCriteriaServiceProviderId == null) {
            searchCriteriaServiceProviderId = new ArrayList<>();
        }
        return this.searchCriteriaServiceProviderId;
    }

}
