//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für NumberActivationMode.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="NumberActivationMode">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Off"/>
 *     <enumeration value="User Activation Enabled"/>
 *     <enumeration value="Group And User Activation Enabled"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "NumberActivationMode")
@XmlEnum
public enum NumberActivationMode {

    @XmlEnumValue("Off")
    OFF("Off"),
    @XmlEnumValue("User Activation Enabled")
    USER_ACTIVATION_ENABLED("User Activation Enabled"),
    @XmlEnumValue("Group And User Activation Enabled")
    GROUP_AND_USER_ACTIVATION_ENABLED("Group And User Activation Enabled");
    private final String value;

    NumberActivationMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NumberActivationMode fromValue(String v) {
        for (NumberActivationMode c: NumberActivationMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
