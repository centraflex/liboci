//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request the group's inventory report.
 *         The response is either a GroupInventoryReportGetResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupInventoryReportGetRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupInventoryReportGetRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="includeUsers" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeServices" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeDns" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeAccessDevices" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeDepartments" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="reportDeliveryEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupInventoryReportGetRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "includeUsers",
    "includeServices",
    "includeDns",
    "includeAccessDevices",
    "includeDepartments",
    "reportDeliveryEmailAddress"
})
public class GroupInventoryReportGetRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected boolean includeUsers;
    protected boolean includeServices;
    protected boolean includeDns;
    protected boolean includeAccessDevices;
    protected boolean includeDepartments;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String reportDeliveryEmailAddress;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der includeUsers-Eigenschaft ab.
     * 
     */
    public boolean isIncludeUsers() {
        return includeUsers;
    }

    /**
     * Legt den Wert der includeUsers-Eigenschaft fest.
     * 
     */
    public void setIncludeUsers(boolean value) {
        this.includeUsers = value;
    }

    /**
     * Ruft den Wert der includeServices-Eigenschaft ab.
     * 
     */
    public boolean isIncludeServices() {
        return includeServices;
    }

    /**
     * Legt den Wert der includeServices-Eigenschaft fest.
     * 
     */
    public void setIncludeServices(boolean value) {
        this.includeServices = value;
    }

    /**
     * Ruft den Wert der includeDns-Eigenschaft ab.
     * 
     */
    public boolean isIncludeDns() {
        return includeDns;
    }

    /**
     * Legt den Wert der includeDns-Eigenschaft fest.
     * 
     */
    public void setIncludeDns(boolean value) {
        this.includeDns = value;
    }

    /**
     * Ruft den Wert der includeAccessDevices-Eigenschaft ab.
     * 
     */
    public boolean isIncludeAccessDevices() {
        return includeAccessDevices;
    }

    /**
     * Legt den Wert der includeAccessDevices-Eigenschaft fest.
     * 
     */
    public void setIncludeAccessDevices(boolean value) {
        this.includeAccessDevices = value;
    }

    /**
     * Ruft den Wert der includeDepartments-Eigenschaft ab.
     * 
     */
    public boolean isIncludeDepartments() {
        return includeDepartments;
    }

    /**
     * Legt den Wert der includeDepartments-Eigenschaft fest.
     * 
     */
    public void setIncludeDepartments(boolean value) {
        this.includeDepartments = value;
    }

    /**
     * Ruft den Wert der reportDeliveryEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportDeliveryEmailAddress() {
        return reportDeliveryEmailAddress;
    }

    /**
     * Legt den Wert der reportDeliveryEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportDeliveryEmailAddress(String value) {
        this.reportDeliveryEmailAddress = value;
    }

}
