//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Delete a system GETS AVP Code map.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemGETSAvpCodeMapDeleteRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGETSAvpCodeMapDeleteRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="avpCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="vendorId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGETSAvpCodeMapDeleteRequest", propOrder = {
    "avpCode",
    "vendorId"
})
public class SystemGETSAvpCodeMapDeleteRequest
    extends OCIRequest
{

    protected int avpCode;
    protected int vendorId;

    /**
     * Ruft den Wert der avpCode-Eigenschaft ab.
     * 
     */
    public int getAvpCode() {
        return avpCode;
    }

    /**
     * Legt den Wert der avpCode-Eigenschaft fest.
     * 
     */
    public void setAvpCode(int value) {
        this.avpCode = value;
    }

    /**
     * Ruft den Wert der vendorId-Eigenschaft ab.
     * 
     */
    public int getVendorId() {
        return vendorId;
    }

    /**
     * Legt den Wert der vendorId-Eigenschaft fest.
     * 
     */
    public void setVendorId(int value) {
        this.vendorId = value;
    }

}
