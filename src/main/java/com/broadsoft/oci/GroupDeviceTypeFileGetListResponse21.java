//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupDeviceTypeFileGetListRequest21.
 *         Contains a table of device type files managed by the Device Management System, on a per-group basis.
 *         The column headings are: "File Format", "Is Authenticated", "Access URL", "Repository URL", "Template URL".
 *       
 * 
 * <p>Java-Klasse für GroupDeviceTypeFileGetListResponse21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDeviceTypeFileGetListResponse21">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="groupDeviceTypeFilesTable" type="{C}OCITable"/>
 *         <element name="groupHasCustomizableDynamicFiles" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDeviceTypeFileGetListResponse21", propOrder = {
    "groupDeviceTypeFilesTable",
    "groupHasCustomizableDynamicFiles"
})
public class GroupDeviceTypeFileGetListResponse21
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable groupDeviceTypeFilesTable;
    protected boolean groupHasCustomizableDynamicFiles;

    /**
     * Ruft den Wert der groupDeviceTypeFilesTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getGroupDeviceTypeFilesTable() {
        return groupDeviceTypeFilesTable;
    }

    /**
     * Legt den Wert der groupDeviceTypeFilesTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setGroupDeviceTypeFilesTable(OCITable value) {
        this.groupDeviceTypeFilesTable = value;
    }

    /**
     * Ruft den Wert der groupHasCustomizableDynamicFiles-Eigenschaft ab.
     * 
     */
    public boolean isGroupHasCustomizableDynamicFiles() {
        return groupHasCustomizableDynamicFiles;
    }

    /**
     * Legt den Wert der groupHasCustomizableDynamicFiles-Eigenschaft fest.
     * 
     */
    public void setGroupHasCustomizableDynamicFiles(boolean value) {
        this.groupHasCustomizableDynamicFiles = value;
    }

}
