//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupMusicOnHoldGetInstanceRequest14sp4.
 *       
 * 
 * <p>Java-Klasse für GroupMusicOnHoldGetInstanceResponse14sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupMusicOnHoldGetInstanceResponse14sp4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="isActiveDuringCallHold" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isActiveDuringCallPark" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isActiveDuringBusyCampOn" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="messageSelection" type="{}MusicOnHoldMessageSelection"/>
 *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointRead14" minOccurs="0"/>
 *         <element name="enableVideo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="audioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="videoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupMusicOnHoldGetInstanceResponse14sp4", propOrder = {
    "serviceUserId",
    "isActiveDuringCallHold",
    "isActiveDuringCallPark",
    "isActiveDuringBusyCampOn",
    "messageSelection",
    "accessDeviceEndpoint",
    "enableVideo",
    "audioFileDescription",
    "videoFileDescription"
})
public class GroupMusicOnHoldGetInstanceResponse14Sp4
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected boolean isActiveDuringCallHold;
    protected boolean isActiveDuringCallPark;
    protected boolean isActiveDuringBusyCampOn;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MusicOnHoldMessageSelection messageSelection;
    protected AccessDeviceEndpointRead14 accessDeviceEndpoint;
    protected boolean enableVideo;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String videoFileDescription;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der isActiveDuringCallHold-Eigenschaft ab.
     * 
     */
    public boolean isIsActiveDuringCallHold() {
        return isActiveDuringCallHold;
    }

    /**
     * Legt den Wert der isActiveDuringCallHold-Eigenschaft fest.
     * 
     */
    public void setIsActiveDuringCallHold(boolean value) {
        this.isActiveDuringCallHold = value;
    }

    /**
     * Ruft den Wert der isActiveDuringCallPark-Eigenschaft ab.
     * 
     */
    public boolean isIsActiveDuringCallPark() {
        return isActiveDuringCallPark;
    }

    /**
     * Legt den Wert der isActiveDuringCallPark-Eigenschaft fest.
     * 
     */
    public void setIsActiveDuringCallPark(boolean value) {
        this.isActiveDuringCallPark = value;
    }

    /**
     * Ruft den Wert der isActiveDuringBusyCampOn-Eigenschaft ab.
     * 
     */
    public boolean isIsActiveDuringBusyCampOn() {
        return isActiveDuringBusyCampOn;
    }

    /**
     * Legt den Wert der isActiveDuringBusyCampOn-Eigenschaft fest.
     * 
     */
    public void setIsActiveDuringBusyCampOn(boolean value) {
        this.isActiveDuringBusyCampOn = value;
    }

    /**
     * Ruft den Wert der messageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public MusicOnHoldMessageSelection getMessageSelection() {
        return messageSelection;
    }

    /**
     * Legt den Wert der messageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public void setMessageSelection(MusicOnHoldMessageSelection value) {
        this.messageSelection = value;
    }

    /**
     * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEndpointRead14 }
     *     
     */
    public AccessDeviceEndpointRead14 getAccessDeviceEndpoint() {
        return accessDeviceEndpoint;
    }

    /**
     * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEndpointRead14 }
     *     
     */
    public void setAccessDeviceEndpoint(AccessDeviceEndpointRead14 value) {
        this.accessDeviceEndpoint = value;
    }

    /**
     * Ruft den Wert der enableVideo-Eigenschaft ab.
     * 
     */
    public boolean isEnableVideo() {
        return enableVideo;
    }

    /**
     * Legt den Wert der enableVideo-Eigenschaft fest.
     * 
     */
    public void setEnableVideo(boolean value) {
        this.enableVideo = value;
    }

    /**
     * Ruft den Wert der audioFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioFileDescription() {
        return audioFileDescription;
    }

    /**
     * Legt den Wert der audioFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioFileDescription(String value) {
        this.audioFileDescription = value;
    }

    /**
     * Ruft den Wert der videoFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVideoFileDescription() {
        return videoFileDescription;
    }

    /**
     * Legt den Wert der videoFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVideoFileDescription(String value) {
        this.videoFileDescription = value;
    }

}
