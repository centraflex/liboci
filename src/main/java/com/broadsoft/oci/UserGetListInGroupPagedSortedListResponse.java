//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserGetListInGroupPagedSortedListRequest.
 *         Contains a table with column headings : "User Id",
 *         "Last Name", "First Name", "Department", "Department Type", 
 *         "Parent Department","Parent Department Type", "Phone Number", 
 *         "Phone Number Activated", "Email Address",  "Hiragana Last Name", 
 *         "Hiragana First Name", "In Trunk Group", "Extension", "Country Code", 
 *         "National Prefix", "User External Id" in a row for each user.
 *                 
 *         The "Department Type" and "Parent Department Type" columns
 *         will contain the values "Enterprise" or "Group".
 *         
 *         The following columns are only populated in AS data mode:
 *           "Country Code", "National Prefix", "User External Id"
 *           
 *       
 * 
 * <p>Java-Klasse für UserGetListInGroupPagedSortedListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserGetListInGroupPagedSortedListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="userTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserGetListInGroupPagedSortedListResponse", propOrder = {
    "userTable"
})
public class UserGetListInGroupPagedSortedListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable userTable;

    /**
     * Ruft den Wert der userTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserTable() {
        return userTable;
    }

    /**
     * Legt den Wert der userTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserTable(OCITable value) {
        this.userTable = value;
    }

}
