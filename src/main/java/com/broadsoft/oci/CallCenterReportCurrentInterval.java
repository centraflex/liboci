//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The call center enhanced reporting report current interval, for example, current week.
 *       
 * 
 * <p>Java-Klasse für CallCenterReportCurrentInterval complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterReportCurrentInterval">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="timeUnit" type="{}CallCenterReportIntervalTimeUnit"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterReportCurrentInterval", propOrder = {
    "timeUnit"
})
public class CallCenterReportCurrentInterval {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportIntervalTimeUnit timeUnit;

    /**
     * Ruft den Wert der timeUnit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportIntervalTimeUnit }
     *     
     */
    public CallCenterReportIntervalTimeUnit getTimeUnit() {
        return timeUnit;
    }

    /**
     * Legt den Wert der timeUnit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportIntervalTimeUnit }
     *     
     */
    public void setTimeUnit(CallCenterReportIntervalTimeUnit value) {
        this.timeUnit = value;
    }

}
