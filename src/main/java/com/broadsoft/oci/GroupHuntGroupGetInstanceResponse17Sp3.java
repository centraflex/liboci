//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupHuntGroupGetInstanceRequest17sp3.
 *         Contains the service profile information and a table of assigned users.
 *         The table has column headings: "User Id", "Last Name", "First Name",
 *         "Hiragana Last Name", "Hiragana First Name", "Weight".
 *         The following elements are only used in AS data mode:
 *            useSystemHuntGroupCLIDSetting
 *      includeHuntGroupNameInCLID
 *       
 * 
 * <p>Java-Klasse für GroupHuntGroupGetInstanceResponse17sp3 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupHuntGroupGetInstanceResponse17sp3">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceReadProfile17"/>
 *         <element name="policy" type="{}HuntPolicy"/>
 *         <element name="huntAfterNoAnswer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="noAnswerNumberOfRings" type="{}HuntNoAnswerRings"/>
 *         <element name="forwardAfterTimeout" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forwardTimeoutSeconds" type="{}HuntForwardTimeoutSeconds"/>
 *         <element name="forwardToPhoneNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="agentUserTable" type="{C}OCITable"/>
 *         <element name="allowCallWaitingForAgents" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useSystemHuntGroupCLIDSetting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeHuntGroupNameInCLID" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupHuntGroupGetInstanceResponse17sp3", propOrder = {
    "serviceInstanceProfile",
    "policy",
    "huntAfterNoAnswer",
    "noAnswerNumberOfRings",
    "forwardAfterTimeout",
    "forwardTimeoutSeconds",
    "forwardToPhoneNumber",
    "agentUserTable",
    "allowCallWaitingForAgents",
    "useSystemHuntGroupCLIDSetting",
    "includeHuntGroupNameInCLID"
})
public class GroupHuntGroupGetInstanceResponse17Sp3
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected ServiceInstanceReadProfile17 serviceInstanceProfile;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected HuntPolicy policy;
    protected boolean huntAfterNoAnswer;
    protected int noAnswerNumberOfRings;
    protected boolean forwardAfterTimeout;
    protected int forwardTimeoutSeconds;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String forwardToPhoneNumber;
    @XmlElement(required = true)
    protected OCITable agentUserTable;
    protected boolean allowCallWaitingForAgents;
    protected boolean useSystemHuntGroupCLIDSetting;
    protected boolean includeHuntGroupNameInCLID;

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceReadProfile17 }
     *     
     */
    public ServiceInstanceReadProfile17 getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceReadProfile17 }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceReadProfile17 value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der policy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HuntPolicy }
     *     
     */
    public HuntPolicy getPolicy() {
        return policy;
    }

    /**
     * Legt den Wert der policy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HuntPolicy }
     *     
     */
    public void setPolicy(HuntPolicy value) {
        this.policy = value;
    }

    /**
     * Ruft den Wert der huntAfterNoAnswer-Eigenschaft ab.
     * 
     */
    public boolean isHuntAfterNoAnswer() {
        return huntAfterNoAnswer;
    }

    /**
     * Legt den Wert der huntAfterNoAnswer-Eigenschaft fest.
     * 
     */
    public void setHuntAfterNoAnswer(boolean value) {
        this.huntAfterNoAnswer = value;
    }

    /**
     * Ruft den Wert der noAnswerNumberOfRings-Eigenschaft ab.
     * 
     */
    public int getNoAnswerNumberOfRings() {
        return noAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der noAnswerNumberOfRings-Eigenschaft fest.
     * 
     */
    public void setNoAnswerNumberOfRings(int value) {
        this.noAnswerNumberOfRings = value;
    }

    /**
     * Ruft den Wert der forwardAfterTimeout-Eigenschaft ab.
     * 
     */
    public boolean isForwardAfterTimeout() {
        return forwardAfterTimeout;
    }

    /**
     * Legt den Wert der forwardAfterTimeout-Eigenschaft fest.
     * 
     */
    public void setForwardAfterTimeout(boolean value) {
        this.forwardAfterTimeout = value;
    }

    /**
     * Ruft den Wert der forwardTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getForwardTimeoutSeconds() {
        return forwardTimeoutSeconds;
    }

    /**
     * Legt den Wert der forwardTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setForwardTimeoutSeconds(int value) {
        this.forwardTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der forwardToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForwardToPhoneNumber() {
        return forwardToPhoneNumber;
    }

    /**
     * Legt den Wert der forwardToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForwardToPhoneNumber(String value) {
        this.forwardToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der agentUserTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAgentUserTable() {
        return agentUserTable;
    }

    /**
     * Legt den Wert der agentUserTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAgentUserTable(OCITable value) {
        this.agentUserTable = value;
    }

    /**
     * Ruft den Wert der allowCallWaitingForAgents-Eigenschaft ab.
     * 
     */
    public boolean isAllowCallWaitingForAgents() {
        return allowCallWaitingForAgents;
    }

    /**
     * Legt den Wert der allowCallWaitingForAgents-Eigenschaft fest.
     * 
     */
    public void setAllowCallWaitingForAgents(boolean value) {
        this.allowCallWaitingForAgents = value;
    }

    /**
     * Ruft den Wert der useSystemHuntGroupCLIDSetting-Eigenschaft ab.
     * 
     */
    public boolean isUseSystemHuntGroupCLIDSetting() {
        return useSystemHuntGroupCLIDSetting;
    }

    /**
     * Legt den Wert der useSystemHuntGroupCLIDSetting-Eigenschaft fest.
     * 
     */
    public void setUseSystemHuntGroupCLIDSetting(boolean value) {
        this.useSystemHuntGroupCLIDSetting = value;
    }

    /**
     * Ruft den Wert der includeHuntGroupNameInCLID-Eigenschaft ab.
     * 
     */
    public boolean isIncludeHuntGroupNameInCLID() {
        return includeHuntGroupNameInCLID;
    }

    /**
     * Legt den Wert der includeHuntGroupNameInCLID-Eigenschaft fest.
     * 
     */
    public void setIncludeHuntGroupNameInCLID(boolean value) {
        this.includeHuntGroupNameInCLID = value;
    }

}
