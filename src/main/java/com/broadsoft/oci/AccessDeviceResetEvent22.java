//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AccessDeviceResetEvent22.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AccessDeviceResetEvent22">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Resync"/>
 *     <enumeration value="CheckSync"/>
 *     <enumeration value="ResetString"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AccessDeviceResetEvent22")
@XmlEnum
public enum AccessDeviceResetEvent22 {

    @XmlEnumValue("Resync")
    RESYNC("Resync"),
    @XmlEnumValue("CheckSync")
    CHECK_SYNC("CheckSync"),
    @XmlEnumValue("ResetString")
    RESET_STRING("ResetString");
    private final String value;

    AccessDeviceResetEvent22(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AccessDeviceResetEvent22 fromValue(String v) {
        for (AccessDeviceResetEvent22 c: AccessDeviceResetEvent22 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
