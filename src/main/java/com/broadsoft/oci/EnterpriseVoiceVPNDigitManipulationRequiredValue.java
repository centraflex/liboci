//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Enterprise Voice VPN Digit Manipulation Entry that has a value.
 *       
 * 
 * <p>Java-Klasse für EnterpriseVoiceVPNDigitManipulationRequiredValue complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseVoiceVPNDigitManipulationRequiredValue">
 *   <complexContent>
 *     <extension base="{}EnterpriseVoiceVPNDigitManipulation">
 *       <sequence>
 *         <element name="operation" type="{}EnterpriseVoiceVPNDigitManipulationOperationRequiredValue"/>
 *         <element name="value" type="{}EnterpriseVoiceVPNDigitManipulationValue"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseVoiceVPNDigitManipulationRequiredValue", propOrder = {
    "operation",
    "value"
})
public class EnterpriseVoiceVPNDigitManipulationRequiredValue
    extends EnterpriseVoiceVPNDigitManipulation
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseVoiceVPNDigitManipulationOperationRequiredValue operation;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String value;

    /**
     * Ruft den Wert der operation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseVoiceVPNDigitManipulationOperationRequiredValue }
     *     
     */
    public EnterpriseVoiceVPNDigitManipulationOperationRequiredValue getOperation() {
        return operation;
    }

    /**
     * Legt den Wert der operation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseVoiceVPNDigitManipulationOperationRequiredValue }
     *     
     */
    public void setOperation(EnterpriseVoiceVPNDigitManipulationOperationRequiredValue value) {
        this.operation = value;
    }

    /**
     * Ruft den Wert der value-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Legt den Wert der value-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

}
