//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemPerformanceMeasurementReportingGetFileServerListRequest22. The table columns are:
 *         "Repository URL", "User Id", "Passive".
 *       
 * 
 * <p>Java-Klasse für SystemPerformanceMeasurementReportingGetFileServerListResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPerformanceMeasurementReportingGetFileServerListResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="fileServerTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPerformanceMeasurementReportingGetFileServerListResponse22", propOrder = {
    "fileServerTable"
})
public class SystemPerformanceMeasurementReportingGetFileServerListResponse22
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable fileServerTable;

    /**
     * Ruft den Wert der fileServerTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getFileServerTable() {
        return fileServerTable;
    }

    /**
     * Legt den Wert der fileServerTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setFileServerTable(OCITable value) {
        this.fileServerTable = value;
    }

}
