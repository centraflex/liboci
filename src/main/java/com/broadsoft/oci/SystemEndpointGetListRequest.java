//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to search endpoints in the system.
 *         If reseller administrator sends the request, searchCriteriaResellerId is ignored. All endpoints 
 *         in the administrator's reseller meeting the search criteria are returned.        
 *         
 *         The response is either SystemEndpointGetListResponse or ErrorResponse.
 * 
 *         The following data elements are only used in AS data mode:
 *           searchCriteriaResellerId        
 *       
 * 
 * <p>Java-Klasse für SystemEndpointGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemEndpointGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaServiceProviderId" type="{}SearchCriteriaServiceProviderId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactOrganizationType" type="{}SearchCriteriaExactOrganizationType" minOccurs="0"/>
 *         <element name="searchCriteriaGroupId" type="{}SearchCriteriaGroupId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaLinePortUserPart" type="{}SearchCriteriaLinePortUserPart" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaLinePortDomain" type="{}SearchCriteriaLinePortDomain" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaUserLastName" type="{}SearchCriteriaUserLastName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaUserFirstName" type="{}SearchCriteriaUserFirstName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaUserId" type="{}SearchCriteriaUserId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDn" type="{}SearchCriteriaDn" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExtension" type="{}SearchCriteriaExtension" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDeviceType" type="{}SearchCriteriaDeviceType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDeviceName" type="{}SearchCriteriaDeviceName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDeviceMACAddress" type="{}SearchCriteriaDeviceMACAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDeviceNetAddress" type="{}SearchCriteriaDeviceNetAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaResellerId" type="{}SearchCriteriaResellerId" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemEndpointGetListRequest", propOrder = {
    "responseSizeLimit",
    "searchCriteriaServiceProviderId",
    "searchCriteriaExactOrganizationType",
    "searchCriteriaGroupId",
    "searchCriteriaLinePortUserPart",
    "searchCriteriaLinePortDomain",
    "searchCriteriaUserLastName",
    "searchCriteriaUserFirstName",
    "searchCriteriaUserId",
    "searchCriteriaDn",
    "searchCriteriaExtension",
    "searchCriteriaDeviceType",
    "searchCriteriaDeviceName",
    "searchCriteriaDeviceMACAddress",
    "searchCriteriaDeviceNetAddress",
    "searchCriteriaResellerId"
})
public class SystemEndpointGetListRequest
    extends OCIRequest
{

    protected Integer responseSizeLimit;
    protected List<SearchCriteriaServiceProviderId> searchCriteriaServiceProviderId;
    protected SearchCriteriaExactOrganizationType searchCriteriaExactOrganizationType;
    protected List<SearchCriteriaGroupId> searchCriteriaGroupId;
    protected List<SearchCriteriaLinePortUserPart> searchCriteriaLinePortUserPart;
    protected List<SearchCriteriaLinePortDomain> searchCriteriaLinePortDomain;
    protected List<SearchCriteriaUserLastName> searchCriteriaUserLastName;
    protected List<SearchCriteriaUserFirstName> searchCriteriaUserFirstName;
    protected List<SearchCriteriaUserId> searchCriteriaUserId;
    protected List<SearchCriteriaDn> searchCriteriaDn;
    protected List<SearchCriteriaExtension> searchCriteriaExtension;
    protected List<SearchCriteriaDeviceType> searchCriteriaDeviceType;
    protected List<SearchCriteriaDeviceName> searchCriteriaDeviceName;
    protected List<SearchCriteriaDeviceMACAddress> searchCriteriaDeviceMACAddress;
    protected List<SearchCriteriaDeviceNetAddress> searchCriteriaDeviceNetAddress;
    protected List<SearchCriteriaResellerId> searchCriteriaResellerId;

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaServiceProviderId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaServiceProviderId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaServiceProviderId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaServiceProviderId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaServiceProviderId property.
     */
    public List<SearchCriteriaServiceProviderId> getSearchCriteriaServiceProviderId() {
        if (searchCriteriaServiceProviderId == null) {
            searchCriteriaServiceProviderId = new ArrayList<>();
        }
        return this.searchCriteriaServiceProviderId;
    }

    /**
     * Ruft den Wert der searchCriteriaExactOrganizationType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactOrganizationType }
     *     
     */
    public SearchCriteriaExactOrganizationType getSearchCriteriaExactOrganizationType() {
        return searchCriteriaExactOrganizationType;
    }

    /**
     * Legt den Wert der searchCriteriaExactOrganizationType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactOrganizationType }
     *     
     */
    public void setSearchCriteriaExactOrganizationType(SearchCriteriaExactOrganizationType value) {
        this.searchCriteriaExactOrganizationType = value;
    }

    /**
     * Gets the value of the searchCriteriaGroupId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaGroupId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaGroupId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaGroupId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaGroupId property.
     */
    public List<SearchCriteriaGroupId> getSearchCriteriaGroupId() {
        if (searchCriteriaGroupId == null) {
            searchCriteriaGroupId = new ArrayList<>();
        }
        return this.searchCriteriaGroupId;
    }

    /**
     * Gets the value of the searchCriteriaLinePortUserPart property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaLinePortUserPart property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaLinePortUserPart().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaLinePortUserPart }
     * 
     * 
     * @return
     *     The value of the searchCriteriaLinePortUserPart property.
     */
    public List<SearchCriteriaLinePortUserPart> getSearchCriteriaLinePortUserPart() {
        if (searchCriteriaLinePortUserPart == null) {
            searchCriteriaLinePortUserPart = new ArrayList<>();
        }
        return this.searchCriteriaLinePortUserPart;
    }

    /**
     * Gets the value of the searchCriteriaLinePortDomain property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaLinePortDomain property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaLinePortDomain().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaLinePortDomain }
     * 
     * 
     * @return
     *     The value of the searchCriteriaLinePortDomain property.
     */
    public List<SearchCriteriaLinePortDomain> getSearchCriteriaLinePortDomain() {
        if (searchCriteriaLinePortDomain == null) {
            searchCriteriaLinePortDomain = new ArrayList<>();
        }
        return this.searchCriteriaLinePortDomain;
    }

    /**
     * Gets the value of the searchCriteriaUserLastName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserLastName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserLastName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserLastName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserLastName property.
     */
    public List<SearchCriteriaUserLastName> getSearchCriteriaUserLastName() {
        if (searchCriteriaUserLastName == null) {
            searchCriteriaUserLastName = new ArrayList<>();
        }
        return this.searchCriteriaUserLastName;
    }

    /**
     * Gets the value of the searchCriteriaUserFirstName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserFirstName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserFirstName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserFirstName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserFirstName property.
     */
    public List<SearchCriteriaUserFirstName> getSearchCriteriaUserFirstName() {
        if (searchCriteriaUserFirstName == null) {
            searchCriteriaUserFirstName = new ArrayList<>();
        }
        return this.searchCriteriaUserFirstName;
    }

    /**
     * Gets the value of the searchCriteriaUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserId property.
     */
    public List<SearchCriteriaUserId> getSearchCriteriaUserId() {
        if (searchCriteriaUserId == null) {
            searchCriteriaUserId = new ArrayList<>();
        }
        return this.searchCriteriaUserId;
    }

    /**
     * Gets the value of the searchCriteriaDn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDn }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDn property.
     */
    public List<SearchCriteriaDn> getSearchCriteriaDn() {
        if (searchCriteriaDn == null) {
            searchCriteriaDn = new ArrayList<>();
        }
        return this.searchCriteriaDn;
    }

    /**
     * Gets the value of the searchCriteriaExtension property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaExtension property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaExtension().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaExtension }
     * 
     * 
     * @return
     *     The value of the searchCriteriaExtension property.
     */
    public List<SearchCriteriaExtension> getSearchCriteriaExtension() {
        if (searchCriteriaExtension == null) {
            searchCriteriaExtension = new ArrayList<>();
        }
        return this.searchCriteriaExtension;
    }

    /**
     * Gets the value of the searchCriteriaDeviceType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDeviceType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDeviceType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDeviceType }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDeviceType property.
     */
    public List<SearchCriteriaDeviceType> getSearchCriteriaDeviceType() {
        if (searchCriteriaDeviceType == null) {
            searchCriteriaDeviceType = new ArrayList<>();
        }
        return this.searchCriteriaDeviceType;
    }

    /**
     * Gets the value of the searchCriteriaDeviceName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDeviceName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDeviceName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDeviceName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDeviceName property.
     */
    public List<SearchCriteriaDeviceName> getSearchCriteriaDeviceName() {
        if (searchCriteriaDeviceName == null) {
            searchCriteriaDeviceName = new ArrayList<>();
        }
        return this.searchCriteriaDeviceName;
    }

    /**
     * Gets the value of the searchCriteriaDeviceMACAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDeviceMACAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDeviceMACAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDeviceMACAddress }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDeviceMACAddress property.
     */
    public List<SearchCriteriaDeviceMACAddress> getSearchCriteriaDeviceMACAddress() {
        if (searchCriteriaDeviceMACAddress == null) {
            searchCriteriaDeviceMACAddress = new ArrayList<>();
        }
        return this.searchCriteriaDeviceMACAddress;
    }

    /**
     * Gets the value of the searchCriteriaDeviceNetAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDeviceNetAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDeviceNetAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDeviceNetAddress }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDeviceNetAddress property.
     */
    public List<SearchCriteriaDeviceNetAddress> getSearchCriteriaDeviceNetAddress() {
        if (searchCriteriaDeviceNetAddress == null) {
            searchCriteriaDeviceNetAddress = new ArrayList<>();
        }
        return this.searchCriteriaDeviceNetAddress;
    }

    /**
     * Gets the value of the searchCriteriaResellerId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaResellerId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaResellerId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaResellerId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaResellerId property.
     */
    public List<SearchCriteriaResellerId> getSearchCriteriaResellerId() {
        if (searchCriteriaResellerId == null) {
            searchCriteriaResellerId = new ArrayList<>();
        }
        return this.searchCriteriaResellerId;
    }

}
