//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemDeviceFamilyGetListRequest.
 *         The response includes a table of device family defined in the system.
 *         Column headings are: "Device Family Name", "Reseller Id".
 * 
 *         The following columns are only returned in AS data mode:       
 *           "Reseller Id"
 *       
 * 
 * <p>Java-Klasse für SystemDeviceFamilyGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceFamilyGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="deviceFamilyTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceFamilyGetListResponse", propOrder = {
    "deviceFamilyTable"
})
public class SystemDeviceFamilyGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable deviceFamilyTable;

    /**
     * Ruft den Wert der deviceFamilyTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDeviceFamilyTable() {
        return deviceFamilyTable;
    }

    /**
     * Legt den Wert der deviceFamilyTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDeviceFamilyTable(OCITable value) {
        this.deviceFamilyTable = value;
    }

}
