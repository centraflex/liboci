//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AutoAttendantDialingScope.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AutoAttendantDialingScope">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Enterprise"/>
 *     <enumeration value="Group"/>
 *     <enumeration value="Department"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AutoAttendantDialingScope")
@XmlEnum
public enum AutoAttendantDialingScope {

    @XmlEnumValue("Enterprise")
    ENTERPRISE("Enterprise"),
    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("Department")
    DEPARTMENT("Department");
    private final String value;

    AutoAttendantDialingScope(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AutoAttendantDialingScope fromValue(String v) {
        for (AutoAttendantDialingScope c: AutoAttendantDialingScope.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
