//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemAccountingGetChargingFunctionElementServerListRequest. The accounting charging function element Server table column
 *         headings are: "Address", "Extended Net Address", "Type", "Description".
 *       
 * 
 * <p>Java-Klasse für SystemAccountingGetChargingFunctionElementServerListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAccountingGetChargingFunctionElementServerListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="chargingFunctionElementServerTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAccountingGetChargingFunctionElementServerListResponse", propOrder = {
    "chargingFunctionElementServerTable"
})
public class SystemAccountingGetChargingFunctionElementServerListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable chargingFunctionElementServerTable;

    /**
     * Ruft den Wert der chargingFunctionElementServerTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getChargingFunctionElementServerTable() {
        return chargingFunctionElementServerTable;
    }

    /**
     * Legt den Wert der chargingFunctionElementServerTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setChargingFunctionElementServerTable(OCITable value) {
        this.chargingFunctionElementServerTable = value;
    }

}
