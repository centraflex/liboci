//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *        Request to modify a user.
 * 
 *         If deleteExistingDevices is set to true, when the devices for the main endpoint or SCA service are changed, devices with no more endpoint will be deleted if the command is executed with the correct privilege.
 *         Group administrator or above running this command can delete any group level devices. 
 *         Service provider administrator or above can delete any service provider and group devices. 
 *         Provisioning administrator or above can delete any devices. 
 *         An ErrorResponse will be returned if any device cannot be deleted because of insufficient privilege.
 *       
 *         When phone numbers are un-assigned from the user, the unused numbers may be un-assigned from the group and service provider. If UnassignPhoneNumbersLevel is set to 'Group', the user's primary phone number, fax number and any alternate numbers, will be un-assigned from the group if the command is executed by a service provider administrator or above.
 *         When set to 'Service Provider', they will be un-assigned from the group and service provider if the command is executed by a provisioning administrator or above.
 *         When omitted, the number(s) will be left assigned to the group.
 *         An ErrorResponse will be returned if any number cannot be unassigned because of insufficient privilege. 
 * 
 *        If the phoneNumber has not been assigned to the group and addPhoneNumberToGroup is set to true, it will be added to group if needed if the command is executed by a service provider administrator and above. The command will fail otherwise.
 *       
 *         Alternate user ids can be added by a group level administrator or above.
 *       
 *         The password is not required if external authentication is enabled.
 *         When sharedCallAppearanceAccessDeviceEndpoint element is included and the Shared Call Appearance service is not assigned after this request, the request will fail.
 *       
 *         The userService/servicePackwill be authorized to the group if it has not been authorized to the group and the command is excuted by a service provider administrator. The request will fail otherwise. 
 *         If not present, the authorizedQuantity will be set to unlimited if allowed.
 *       
 *         If any of the third party voice mail elements are included but the service is not assigned after this request, the request will fail.
 *       
 *          If the sip authentication elements are included but the SPI Authentication service is not assigned after this request, the request will fail.
 * 
 *          If the impPassword element is included but the Integrated IMP service is not assigned after this request, the request will fail.
 * 
 * 
 *          The following elements are ignored in XS data mode:
 *           nameDialingName 
 *           alternateUserIdList
 *           passcode
 *           trunkAddressing
 *           thirdPartyVoiceMailServerSelection
 *           thirdPartyVoiceMailServerUserServer
 *           thirdPartyVoiceMailServerMailboxIdType
 *           thirdPartyVoiceMailMailboxURL
 *           sipAuthenticationData
 *           newUserExternalId
 * 
 *          The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserConsolidatedModifyRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserConsolidatedModifyRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="deleteExistingDevices" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="unassignPhoneNumbers" type="{}UnassignPhoneNumbersLevel" minOccurs="0"/>
 *         <element name="addPhoneNumberToGroup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="newUserId" type="{}UserId" minOccurs="0"/>
 *         <element name="lastName" type="{}LastName" minOccurs="0"/>
 *         <element name="firstName" type="{}FirstName" minOccurs="0"/>
 *         <element name="callingLineIdLastName" type="{}CallingLineIdLastName" minOccurs="0"/>
 *         <element name="callingLineIdFirstName" type="{}CallingLineIdFirstName" minOccurs="0"/>
 *         <element name="nameDialingName" type="{}NameDialingName" minOccurs="0"/>
 *         <element name="hiraganaLastName" type="{}HiraganaLastName" minOccurs="0"/>
 *         <element name="hiraganaFirstName" type="{}HiraganaFirstName" minOccurs="0"/>
 *         <element name="phoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="alternateUserIdList" type="{}ReplacementAlternateUserIdEntryList" minOccurs="0"/>
 *         <element name="extension" type="{}Extension17" minOccurs="0"/>
 *         <element name="callingLineIdPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="oldPassword" type="{}Password" minOccurs="0"/>
 *         <element name="newPassword" type="{}Password" minOccurs="0"/>
 *         <element name="department" type="{}DepartmentKey" minOccurs="0"/>
 *         <element name="language" type="{}Language" minOccurs="0"/>
 *         <element name="timeZone" type="{}TimeZone" minOccurs="0"/>
 *         <element name="sipAliasList" type="{}ReplacementSIPAliasList" minOccurs="0"/>
 *         <element name="endpoint" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <choice>
 *                   <element name="accessDeviceEndpoint" type="{}ConsolidatedAccessDeviceMultipleIdentityEndpointAndContactModify"/>
 *                   <element name="trunkAddressing" type="{}TrunkAddressingMultipleContactModify"/>
 *                 </choice>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="sharedCallAppearanceAccessDeviceEndpointList" type="{}ReplacementConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpointList22" minOccurs="0"/>
 *         <element name="title" type="{}Title" minOccurs="0"/>
 *         <element name="pagerPhoneNumber" type="{}InformationalDN" minOccurs="0"/>
 *         <element name="mobilePhoneNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="emailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="yahooId" type="{}YahooId" minOccurs="0"/>
 *         <element name="addressLocation" type="{}AddressLocation" minOccurs="0"/>
 *         <element name="address" type="{}StreetAddress" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="userServiceList" type="{}ReplacementConsolidatedUserServiceAssignmentList" minOccurs="0"/>
 *         <element name="servicePackList" type="{}ReplacementConsolidatedServicePackAssignmentList" minOccurs="0"/>
 *         <element name="thirdPartyVoiceMailServerSelection" type="{}ThirdPartyVoiceMailSupportServerSelection" minOccurs="0"/>
 *         <element name="thirdPartyVoiceMailServerUserServer" type="{}ThirdPartyVoiceMailSupportMailServer" minOccurs="0"/>
 *         <element name="thirdPartyVoiceMailServerMailboxIdType" type="{}ThirdPartyVoiceMailSupportMailboxIdType" minOccurs="0"/>
 *         <element name="thirdPartyVoiceMailMailboxURL" type="{}SIPURI" minOccurs="0"/>
 *         <element name="sipAuthenticationUserName" type="{}SIPAuthenticationUserName" minOccurs="0"/>
 *         <element name="newSipAuthenticationPassword" type="{}Password" minOccurs="0"/>
 *         <element name="OldSipAuthenticationPassword" type="{}Password" minOccurs="0"/>
 *         <element name="newPasscode" type="{}Passcode" minOccurs="0"/>
 *         <element name="oldPasscode" type="{}Passcode" minOccurs="0"/>
 *         <element name="impPassword" type="{}Password" minOccurs="0"/>
 *         <element name="newUserExternalId" type="{}ExternalId" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserConsolidatedModifyRequest22", propOrder = {
    "userId",
    "deleteExistingDevices",
    "unassignPhoneNumbers",
    "addPhoneNumberToGroup",
    "newUserId",
    "lastName",
    "firstName",
    "callingLineIdLastName",
    "callingLineIdFirstName",
    "nameDialingName",
    "hiraganaLastName",
    "hiraganaFirstName",
    "phoneNumber",
    "alternateUserIdList",
    "extension",
    "callingLineIdPhoneNumber",
    "oldPassword",
    "newPassword",
    "department",
    "language",
    "timeZone",
    "sipAliasList",
    "endpoint",
    "sharedCallAppearanceAccessDeviceEndpointList",
    "title",
    "pagerPhoneNumber",
    "mobilePhoneNumber",
    "emailAddress",
    "yahooId",
    "addressLocation",
    "address",
    "networkClassOfService",
    "userServiceList",
    "servicePackList",
    "thirdPartyVoiceMailServerSelection",
    "thirdPartyVoiceMailServerUserServer",
    "thirdPartyVoiceMailServerMailboxIdType",
    "thirdPartyVoiceMailMailboxURL",
    "sipAuthenticationUserName",
    "newSipAuthenticationPassword",
    "oldSipAuthenticationPassword",
    "newPasscode",
    "oldPasscode",
    "impPassword",
    "newUserExternalId"
})
public class UserConsolidatedModifyRequest22
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean deleteExistingDevices;
    @XmlSchemaType(name = "token")
    protected UnassignPhoneNumbersLevel unassignPhoneNumbers;
    protected Boolean addPhoneNumberToGroup;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newUserId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String lastName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String firstName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdLastName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdFirstName;
    @XmlElementRef(name = "nameDialingName", type = JAXBElement.class, required = false)
    protected JAXBElement<NameDialingName> nameDialingName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String hiraganaLastName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String hiraganaFirstName;
    @XmlElementRef(name = "phoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> phoneNumber;
    @XmlElementRef(name = "alternateUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementAlternateUserIdEntryList> alternateUserIdList;
    @XmlElementRef(name = "extension", type = JAXBElement.class, required = false)
    protected JAXBElement<String> extension;
    @XmlElementRef(name = "callingLineIdPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callingLineIdPhoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String oldPassword;
    @XmlElementRef(name = "newPassword", type = JAXBElement.class, required = false)
    protected JAXBElement<String> newPassword;
    @XmlElementRef(name = "department", type = JAXBElement.class, required = false)
    protected JAXBElement<DepartmentKey> department;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String language;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeZone;
    @XmlElementRef(name = "sipAliasList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementSIPAliasList> sipAliasList;
    @XmlElementRef(name = "endpoint", type = JAXBElement.class, required = false)
    protected JAXBElement<UserConsolidatedModifyRequest22 .Endpoint> endpoint;
    @XmlElementRef(name = "sharedCallAppearanceAccessDeviceEndpointList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpointList22> sharedCallAppearanceAccessDeviceEndpointList;
    @XmlElementRef(name = "title", type = JAXBElement.class, required = false)
    protected JAXBElement<String> title;
    @XmlElementRef(name = "pagerPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pagerPhoneNumber;
    @XmlElementRef(name = "mobilePhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mobilePhoneNumber;
    @XmlElementRef(name = "emailAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emailAddress;
    @XmlElementRef(name = "yahooId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> yahooId;
    @XmlElementRef(name = "addressLocation", type = JAXBElement.class, required = false)
    protected JAXBElement<String> addressLocation;
    protected StreetAddress address;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    @XmlElementRef(name = "userServiceList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementConsolidatedUserServiceAssignmentList> userServiceList;
    @XmlElementRef(name = "servicePackList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementConsolidatedServicePackAssignmentList> servicePackList;
    @XmlSchemaType(name = "token")
    protected ThirdPartyVoiceMailSupportServerSelection thirdPartyVoiceMailServerSelection;
    @XmlElementRef(name = "thirdPartyVoiceMailServerUserServer", type = JAXBElement.class, required = false)
    protected JAXBElement<String> thirdPartyVoiceMailServerUserServer;
    @XmlSchemaType(name = "token")
    protected ThirdPartyVoiceMailSupportMailboxIdType thirdPartyVoiceMailServerMailboxIdType;
    @XmlElementRef(name = "thirdPartyVoiceMailMailboxURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> thirdPartyVoiceMailMailboxURL;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sipAuthenticationUserName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newSipAuthenticationPassword;
    @XmlElement(name = "OldSipAuthenticationPassword")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String oldSipAuthenticationPassword;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newPasscode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String oldPasscode;
    @XmlElementRef(name = "impPassword", type = JAXBElement.class, required = false)
    protected JAXBElement<String> impPassword;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newUserExternalId;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der deleteExistingDevices-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeleteExistingDevices() {
        return deleteExistingDevices;
    }

    /**
     * Legt den Wert der deleteExistingDevices-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeleteExistingDevices(Boolean value) {
        this.deleteExistingDevices = value;
    }

    /**
     * Ruft den Wert der unassignPhoneNumbers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnassignPhoneNumbersLevel }
     *     
     */
    public UnassignPhoneNumbersLevel getUnassignPhoneNumbers() {
        return unassignPhoneNumbers;
    }

    /**
     * Legt den Wert der unassignPhoneNumbers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnassignPhoneNumbersLevel }
     *     
     */
    public void setUnassignPhoneNumbers(UnassignPhoneNumbersLevel value) {
        this.unassignPhoneNumbers = value;
    }

    /**
     * Ruft den Wert der addPhoneNumberToGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAddPhoneNumberToGroup() {
        return addPhoneNumberToGroup;
    }

    /**
     * Legt den Wert der addPhoneNumberToGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAddPhoneNumberToGroup(Boolean value) {
        this.addPhoneNumberToGroup = value;
    }

    /**
     * Ruft den Wert der newUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewUserId() {
        return newUserId;
    }

    /**
     * Legt den Wert der newUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewUserId(String value) {
        this.newUserId = value;
    }

    /**
     * Ruft den Wert der lastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Legt den Wert der lastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Ruft den Wert der firstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Legt den Wert der firstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Ruft den Wert der callingLineIdLastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdLastName() {
        return callingLineIdLastName;
    }

    /**
     * Legt den Wert der callingLineIdLastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdLastName(String value) {
        this.callingLineIdLastName = value;
    }

    /**
     * Ruft den Wert der callingLineIdFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdFirstName() {
        return callingLineIdFirstName;
    }

    /**
     * Legt den Wert der callingLineIdFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdFirstName(String value) {
        this.callingLineIdFirstName = value;
    }

    /**
     * Ruft den Wert der nameDialingName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link NameDialingName }{@code >}
     *     
     */
    public JAXBElement<NameDialingName> getNameDialingName() {
        return nameDialingName;
    }

    /**
     * Legt den Wert der nameDialingName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link NameDialingName }{@code >}
     *     
     */
    public void setNameDialingName(JAXBElement<NameDialingName> value) {
        this.nameDialingName = value;
    }

    /**
     * Ruft den Wert der hiraganaLastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHiraganaLastName() {
        return hiraganaLastName;
    }

    /**
     * Legt den Wert der hiraganaLastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHiraganaLastName(String value) {
        this.hiraganaLastName = value;
    }

    /**
     * Ruft den Wert der hiraganaFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHiraganaFirstName() {
        return hiraganaFirstName;
    }

    /**
     * Legt den Wert der hiraganaFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHiraganaFirstName(String value) {
        this.hiraganaFirstName = value;
    }

    /**
     * Ruft den Wert der phoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Legt den Wert der phoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPhoneNumber(JAXBElement<String> value) {
        this.phoneNumber = value;
    }

    /**
     * Ruft den Wert der alternateUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementAlternateUserIdEntryList }{@code >}
     *     
     */
    public JAXBElement<ReplacementAlternateUserIdEntryList> getAlternateUserIdList() {
        return alternateUserIdList;
    }

    /**
     * Legt den Wert der alternateUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementAlternateUserIdEntryList }{@code >}
     *     
     */
    public void setAlternateUserIdList(JAXBElement<ReplacementAlternateUserIdEntryList> value) {
        this.alternateUserIdList = value;
    }

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExtension(JAXBElement<String> value) {
        this.extension = value;
    }

    /**
     * Ruft den Wert der callingLineIdPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallingLineIdPhoneNumber() {
        return callingLineIdPhoneNumber;
    }

    /**
     * Legt den Wert der callingLineIdPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallingLineIdPhoneNumber(JAXBElement<String> value) {
        this.callingLineIdPhoneNumber = value;
    }

    /**
     * Ruft den Wert der oldPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldPassword() {
        return oldPassword;
    }

    /**
     * Legt den Wert der oldPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldPassword(String value) {
        this.oldPassword = value;
    }

    /**
     * Ruft den Wert der newPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNewPassword() {
        return newPassword;
    }

    /**
     * Legt den Wert der newPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNewPassword(JAXBElement<String> value) {
        this.newPassword = value;
    }

    /**
     * Ruft den Wert der department-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DepartmentKey }{@code >}
     *     
     */
    public JAXBElement<DepartmentKey> getDepartment() {
        return department;
    }

    /**
     * Legt den Wert der department-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DepartmentKey }{@code >}
     *     
     */
    public void setDepartment(JAXBElement<DepartmentKey> value) {
        this.department = value;
    }

    /**
     * Ruft den Wert der language-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Legt den Wert der language-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Ruft den Wert der timeZone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * Legt den Wert der timeZone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeZone(String value) {
        this.timeZone = value;
    }

    /**
     * Ruft den Wert der sipAliasList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementSIPAliasList }{@code >}
     *     
     */
    public JAXBElement<ReplacementSIPAliasList> getSipAliasList() {
        return sipAliasList;
    }

    /**
     * Legt den Wert der sipAliasList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementSIPAliasList }{@code >}
     *     
     */
    public void setSipAliasList(JAXBElement<ReplacementSIPAliasList> value) {
        this.sipAliasList = value;
    }

    /**
     * Ruft den Wert der endpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link UserConsolidatedModifyRequest22 .Endpoint }{@code >}
     *     
     */
    public JAXBElement<UserConsolidatedModifyRequest22 .Endpoint> getEndpoint() {
        return endpoint;
    }

    /**
     * Legt den Wert der endpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link UserConsolidatedModifyRequest22 .Endpoint }{@code >}
     *     
     */
    public void setEndpoint(JAXBElement<UserConsolidatedModifyRequest22 .Endpoint> value) {
        this.endpoint = value;
    }

    /**
     * Ruft den Wert der sharedCallAppearanceAccessDeviceEndpointList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpointList22 }{@code >}
     *     
     */
    public JAXBElement<ReplacementConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpointList22> getSharedCallAppearanceAccessDeviceEndpointList() {
        return sharedCallAppearanceAccessDeviceEndpointList;
    }

    /**
     * Legt den Wert der sharedCallAppearanceAccessDeviceEndpointList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpointList22 }{@code >}
     *     
     */
    public void setSharedCallAppearanceAccessDeviceEndpointList(JAXBElement<ReplacementConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpointList22> value) {
        this.sharedCallAppearanceAccessDeviceEndpointList = value;
    }

    /**
     * Ruft den Wert der title-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTitle() {
        return title;
    }

    /**
     * Legt den Wert der title-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTitle(JAXBElement<String> value) {
        this.title = value;
    }

    /**
     * Ruft den Wert der pagerPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPagerPhoneNumber() {
        return pagerPhoneNumber;
    }

    /**
     * Legt den Wert der pagerPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPagerPhoneNumber(JAXBElement<String> value) {
        this.pagerPhoneNumber = value;
    }

    /**
     * Ruft den Wert der mobilePhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    /**
     * Legt den Wert der mobilePhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMobilePhoneNumber(JAXBElement<String> value) {
        this.mobilePhoneNumber = value;
    }

    /**
     * Ruft den Wert der emailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmailAddress() {
        return emailAddress;
    }

    /**
     * Legt den Wert der emailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmailAddress(JAXBElement<String> value) {
        this.emailAddress = value;
    }

    /**
     * Ruft den Wert der yahooId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getYahooId() {
        return yahooId;
    }

    /**
     * Legt den Wert der yahooId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setYahooId(JAXBElement<String> value) {
        this.yahooId = value;
    }

    /**
     * Ruft den Wert der addressLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAddressLocation() {
        return addressLocation;
    }

    /**
     * Legt den Wert der addressLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAddressLocation(JAXBElement<String> value) {
        this.addressLocation = value;
    }

    /**
     * Ruft den Wert der address-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StreetAddress }
     *     
     */
    public StreetAddress getAddress() {
        return address;
    }

    /**
     * Legt den Wert der address-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetAddress }
     *     
     */
    public void setAddress(StreetAddress value) {
        this.address = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Ruft den Wert der userServiceList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementConsolidatedUserServiceAssignmentList }{@code >}
     *     
     */
    public JAXBElement<ReplacementConsolidatedUserServiceAssignmentList> getUserServiceList() {
        return userServiceList;
    }

    /**
     * Legt den Wert der userServiceList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementConsolidatedUserServiceAssignmentList }{@code >}
     *     
     */
    public void setUserServiceList(JAXBElement<ReplacementConsolidatedUserServiceAssignmentList> value) {
        this.userServiceList = value;
    }

    /**
     * Ruft den Wert der servicePackList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementConsolidatedServicePackAssignmentList }{@code >}
     *     
     */
    public JAXBElement<ReplacementConsolidatedServicePackAssignmentList> getServicePackList() {
        return servicePackList;
    }

    /**
     * Legt den Wert der servicePackList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementConsolidatedServicePackAssignmentList }{@code >}
     *     
     */
    public void setServicePackList(JAXBElement<ReplacementConsolidatedServicePackAssignmentList> value) {
        this.servicePackList = value;
    }

    /**
     * Ruft den Wert der thirdPartyVoiceMailServerSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ThirdPartyVoiceMailSupportServerSelection }
     *     
     */
    public ThirdPartyVoiceMailSupportServerSelection getThirdPartyVoiceMailServerSelection() {
        return thirdPartyVoiceMailServerSelection;
    }

    /**
     * Legt den Wert der thirdPartyVoiceMailServerSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ThirdPartyVoiceMailSupportServerSelection }
     *     
     */
    public void setThirdPartyVoiceMailServerSelection(ThirdPartyVoiceMailSupportServerSelection value) {
        this.thirdPartyVoiceMailServerSelection = value;
    }

    /**
     * Ruft den Wert der thirdPartyVoiceMailServerUserServer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getThirdPartyVoiceMailServerUserServer() {
        return thirdPartyVoiceMailServerUserServer;
    }

    /**
     * Legt den Wert der thirdPartyVoiceMailServerUserServer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setThirdPartyVoiceMailServerUserServer(JAXBElement<String> value) {
        this.thirdPartyVoiceMailServerUserServer = value;
    }

    /**
     * Ruft den Wert der thirdPartyVoiceMailServerMailboxIdType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ThirdPartyVoiceMailSupportMailboxIdType }
     *     
     */
    public ThirdPartyVoiceMailSupportMailboxIdType getThirdPartyVoiceMailServerMailboxIdType() {
        return thirdPartyVoiceMailServerMailboxIdType;
    }

    /**
     * Legt den Wert der thirdPartyVoiceMailServerMailboxIdType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ThirdPartyVoiceMailSupportMailboxIdType }
     *     
     */
    public void setThirdPartyVoiceMailServerMailboxIdType(ThirdPartyVoiceMailSupportMailboxIdType value) {
        this.thirdPartyVoiceMailServerMailboxIdType = value;
    }

    /**
     * Ruft den Wert der thirdPartyVoiceMailMailboxURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getThirdPartyVoiceMailMailboxURL() {
        return thirdPartyVoiceMailMailboxURL;
    }

    /**
     * Legt den Wert der thirdPartyVoiceMailMailboxURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setThirdPartyVoiceMailMailboxURL(JAXBElement<String> value) {
        this.thirdPartyVoiceMailMailboxURL = value;
    }

    /**
     * Ruft den Wert der sipAuthenticationUserName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSipAuthenticationUserName() {
        return sipAuthenticationUserName;
    }

    /**
     * Legt den Wert der sipAuthenticationUserName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSipAuthenticationUserName(String value) {
        this.sipAuthenticationUserName = value;
    }

    /**
     * Ruft den Wert der newSipAuthenticationPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewSipAuthenticationPassword() {
        return newSipAuthenticationPassword;
    }

    /**
     * Legt den Wert der newSipAuthenticationPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewSipAuthenticationPassword(String value) {
        this.newSipAuthenticationPassword = value;
    }

    /**
     * Ruft den Wert der oldSipAuthenticationPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldSipAuthenticationPassword() {
        return oldSipAuthenticationPassword;
    }

    /**
     * Legt den Wert der oldSipAuthenticationPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldSipAuthenticationPassword(String value) {
        this.oldSipAuthenticationPassword = value;
    }

    /**
     * Ruft den Wert der newPasscode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewPasscode() {
        return newPasscode;
    }

    /**
     * Legt den Wert der newPasscode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewPasscode(String value) {
        this.newPasscode = value;
    }

    /**
     * Ruft den Wert der oldPasscode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldPasscode() {
        return oldPasscode;
    }

    /**
     * Legt den Wert der oldPasscode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldPasscode(String value) {
        this.oldPasscode = value;
    }

    /**
     * Ruft den Wert der impPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getImpPassword() {
        return impPassword;
    }

    /**
     * Legt den Wert der impPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setImpPassword(JAXBElement<String> value) {
        this.impPassword = value;
    }

    /**
     * Ruft den Wert der newUserExternalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewUserExternalId() {
        return newUserExternalId;
    }

    /**
     * Legt den Wert der newUserExternalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewUserExternalId(String value) {
        this.newUserExternalId = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <choice>
     *         <element name="accessDeviceEndpoint" type="{}ConsolidatedAccessDeviceMultipleIdentityEndpointAndContactModify"/>
     *         <element name="trunkAddressing" type="{}TrunkAddressingMultipleContactModify"/>
     *       </choice>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accessDeviceEndpoint",
        "trunkAddressing"
    })
    public static class Endpoint {

        protected ConsolidatedAccessDeviceMultipleIdentityEndpointAndContactModify accessDeviceEndpoint;
        protected TrunkAddressingMultipleContactModify trunkAddressing;

        /**
         * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link ConsolidatedAccessDeviceMultipleIdentityEndpointAndContactModify }
         *     
         */
        public ConsolidatedAccessDeviceMultipleIdentityEndpointAndContactModify getAccessDeviceEndpoint() {
            return accessDeviceEndpoint;
        }

        /**
         * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link ConsolidatedAccessDeviceMultipleIdentityEndpointAndContactModify }
         *     
         */
        public void setAccessDeviceEndpoint(ConsolidatedAccessDeviceMultipleIdentityEndpointAndContactModify value) {
            this.accessDeviceEndpoint = value;
        }

        /**
         * Ruft den Wert der trunkAddressing-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link TrunkAddressingMultipleContactModify }
         *     
         */
        public TrunkAddressingMultipleContactModify getTrunkAddressing() {
            return trunkAddressing;
        }

        /**
         * Legt den Wert der trunkAddressing-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link TrunkAddressingMultipleContactModify }
         *     
         */
        public void setTrunkAddressing(TrunkAddressingMultipleContactModify value) {
            this.trunkAddressing = value;
        }

    }

}
