//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The call blocking service map entry.
 *       
 * 
 * <p>Java-Klasse für CallBlockingServiceMapEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallBlockingServiceMapEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="callBlockingService" type="{}CallBlockingService23V3"/>
 *         <element name="treatmentId" type="{}TreatmentId" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallBlockingServiceMapEntry", propOrder = {
    "callBlockingService",
    "treatmentId"
})
public class CallBlockingServiceMapEntry {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallBlockingService23V3 callBlockingService;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String treatmentId;

    /**
     * Ruft den Wert der callBlockingService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallBlockingService23V3 }
     *     
     */
    public CallBlockingService23V3 getCallBlockingService() {
        return callBlockingService;
    }

    /**
     * Legt den Wert der callBlockingService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallBlockingService23V3 }
     *     
     */
    public void setCallBlockingService(CallBlockingService23V3 value) {
        this.callBlockingService = value;
    }

    /**
     * Ruft den Wert der treatmentId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTreatmentId() {
        return treatmentId;
    }

    /**
     * Legt den Wert der treatmentId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTreatmentId(String value) {
        this.treatmentId = value;
    }

}
