//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify Outgoing Pinhole Digit Plan originating call permissions for specified digit patterns.
 *       
 * 
 * <p>Java-Klasse für OutgoingPinholeDigitPlanDigitPatternOriginatingDepartmentPermissionsModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OutgoingPinholeDigitPlanDigitPatternOriginatingDepartmentPermissionsModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="departmentKey" type="{}DepartmentKey"/>
 *         <element name="digitPatternPermissions" type="{}OutgoingPinholeDigitPlanDigitPatternOriginatingPermissions"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingPinholeDigitPlanDigitPatternOriginatingDepartmentPermissionsModify", propOrder = {
    "departmentKey",
    "digitPatternPermissions"
})
public class OutgoingPinholeDigitPlanDigitPatternOriginatingDepartmentPermissionsModify {

    @XmlElement(required = true)
    protected DepartmentKey departmentKey;
    @XmlElement(required = true)
    protected OutgoingPinholeDigitPlanDigitPatternOriginatingPermissions digitPatternPermissions;

    /**
     * Ruft den Wert der departmentKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DepartmentKey }
     *     
     */
    public DepartmentKey getDepartmentKey() {
        return departmentKey;
    }

    /**
     * Legt den Wert der departmentKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DepartmentKey }
     *     
     */
    public void setDepartmentKey(DepartmentKey value) {
        this.departmentKey = value;
    }

    /**
     * Ruft den Wert der digitPatternPermissions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingPinholeDigitPlanDigitPatternOriginatingPermissions }
     *     
     */
    public OutgoingPinholeDigitPlanDigitPatternOriginatingPermissions getDigitPatternPermissions() {
        return digitPatternPermissions;
    }

    /**
     * Legt den Wert der digitPatternPermissions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingPinholeDigitPlanDigitPatternOriginatingPermissions }
     *     
     */
    public void setDigitPatternPermissions(OutgoingPinholeDigitPlanDigitPatternOriginatingPermissions value) {
        this.digitPatternPermissions = value;
    }

}
