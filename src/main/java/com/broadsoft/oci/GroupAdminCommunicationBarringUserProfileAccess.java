//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GroupAdminCommunicationBarringUserProfileAccess.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="GroupAdminCommunicationBarringUserProfileAccess">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Full"/>
 *     <enumeration value="None"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "GroupAdminCommunicationBarringUserProfileAccess")
@XmlEnum
public enum GroupAdminCommunicationBarringUserProfileAccess {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("None")
    NONE("None");
    private final String value;

    GroupAdminCommunicationBarringUserProfileAccess(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupAdminCommunicationBarringUserProfileAccess fromValue(String v) {
        for (GroupAdminCommunicationBarringUserProfileAccess c: GroupAdminCommunicationBarringUserProfileAccess.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
