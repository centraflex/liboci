//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PresentationIndicator.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="PresentationIndicator">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Anonymous"/>
 *     <enumeration value="Anonymous Name"/>
 *     <enumeration value="Anonymous URI"/>
 *     <enumeration value="Anonymous Unavailable"/>
 *     <enumeration value="Public"/>
 *     <enumeration value="Unavailable"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "PresentationIndicator")
@XmlEnum
public enum PresentationIndicator {

    @XmlEnumValue("Anonymous")
    ANONYMOUS("Anonymous"),
    @XmlEnumValue("Anonymous Name")
    ANONYMOUS_NAME("Anonymous Name"),
    @XmlEnumValue("Anonymous URI")
    ANONYMOUS_URI("Anonymous URI"),
    @XmlEnumValue("Anonymous Unavailable")
    ANONYMOUS_UNAVAILABLE("Anonymous Unavailable"),
    @XmlEnumValue("Public")
    PUBLIC("Public"),
    @XmlEnumValue("Unavailable")
    UNAVAILABLE("Unavailable");
    private final String value;

    PresentationIndicator(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PresentationIndicator fromValue(String v) {
        for (PresentationIndicator c: PresentationIndicator.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
