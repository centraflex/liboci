//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request device management's events for a specific queue.
 *         It is possible to restrict the number of rows returned using
 *         responseSizeLimit.
 *         If eventQueueType is not specified, the events from all the
 *         event queues are returned.
 *         The response is either a SystemDeviceManagementEventGetListResponse22 or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemDeviceManagementEventGetListRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceManagementEventGetListRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="eventQueueType" type="{}DeviceManagementEventQueueType" minOccurs="0"/>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaExactDeviceManagementEventStatusInProgressOrPending" type="{}SearchCriteriaExactDeviceManagementEventStatusInProgressOrPending" minOccurs="0"/>
 *         <element name="searchCriteriaExactDeviceManagementEventStatusCompleted" type="{}SearchCriteriaExactDeviceManagementEventStatusCompleted" minOccurs="0"/>
 *         <element name="searchCriteriaExactDeviceManagementEventAction" type="{}SearchCriteriaExactDeviceManagementEventAction" minOccurs="0"/>
 *         <element name="searchCriteriaExactDeviceManagementEventLevel" type="{}SearchCriteriaExactDeviceManagementEventLevel" minOccurs="0"/>
 *         <element name="searchCriteriaExactDeviceManagementEventType" type="{}SearchCriteriaExactDeviceManagementEventType" minOccurs="0"/>
 *         <element name="searchCriteriaDeviceManagementEventAdditionalInfo" type="{}SearchCriteriaDeviceManagementEventAdditionalInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDeviceManagementEventLoginId" type="{}SearchCriteriaDeviceManagementEventLoginId" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceManagementEventGetListRequest22", propOrder = {
    "eventQueueType",
    "responseSizeLimit",
    "searchCriteriaExactDeviceManagementEventStatusInProgressOrPending",
    "searchCriteriaExactDeviceManagementEventStatusCompleted",
    "searchCriteriaExactDeviceManagementEventAction",
    "searchCriteriaExactDeviceManagementEventLevel",
    "searchCriteriaExactDeviceManagementEventType",
    "searchCriteriaDeviceManagementEventAdditionalInfo",
    "searchCriteriaDeviceManagementEventLoginId"
})
public class SystemDeviceManagementEventGetListRequest22
    extends OCIRequest
{

    @XmlSchemaType(name = "token")
    protected DeviceManagementEventQueueType eventQueueType;
    protected Integer responseSizeLimit;
    protected SearchCriteriaExactDeviceManagementEventStatusInProgressOrPending searchCriteriaExactDeviceManagementEventStatusInProgressOrPending;
    protected SearchCriteriaExactDeviceManagementEventStatusCompleted searchCriteriaExactDeviceManagementEventStatusCompleted;
    protected SearchCriteriaExactDeviceManagementEventAction searchCriteriaExactDeviceManagementEventAction;
    protected SearchCriteriaExactDeviceManagementEventLevel searchCriteriaExactDeviceManagementEventLevel;
    protected SearchCriteriaExactDeviceManagementEventType searchCriteriaExactDeviceManagementEventType;
    protected List<SearchCriteriaDeviceManagementEventAdditionalInfo> searchCriteriaDeviceManagementEventAdditionalInfo;
    protected List<SearchCriteriaDeviceManagementEventLoginId> searchCriteriaDeviceManagementEventLoginId;

    /**
     * Ruft den Wert der eventQueueType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementEventQueueType }
     *     
     */
    public DeviceManagementEventQueueType getEventQueueType() {
        return eventQueueType;
    }

    /**
     * Legt den Wert der eventQueueType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementEventQueueType }
     *     
     */
    public void setEventQueueType(DeviceManagementEventQueueType value) {
        this.eventQueueType = value;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Ruft den Wert der searchCriteriaExactDeviceManagementEventStatusInProgressOrPending-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactDeviceManagementEventStatusInProgressOrPending }
     *     
     */
    public SearchCriteriaExactDeviceManagementEventStatusInProgressOrPending getSearchCriteriaExactDeviceManagementEventStatusInProgressOrPending() {
        return searchCriteriaExactDeviceManagementEventStatusInProgressOrPending;
    }

    /**
     * Legt den Wert der searchCriteriaExactDeviceManagementEventStatusInProgressOrPending-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactDeviceManagementEventStatusInProgressOrPending }
     *     
     */
    public void setSearchCriteriaExactDeviceManagementEventStatusInProgressOrPending(SearchCriteriaExactDeviceManagementEventStatusInProgressOrPending value) {
        this.searchCriteriaExactDeviceManagementEventStatusInProgressOrPending = value;
    }

    /**
     * Ruft den Wert der searchCriteriaExactDeviceManagementEventStatusCompleted-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactDeviceManagementEventStatusCompleted }
     *     
     */
    public SearchCriteriaExactDeviceManagementEventStatusCompleted getSearchCriteriaExactDeviceManagementEventStatusCompleted() {
        return searchCriteriaExactDeviceManagementEventStatusCompleted;
    }

    /**
     * Legt den Wert der searchCriteriaExactDeviceManagementEventStatusCompleted-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactDeviceManagementEventStatusCompleted }
     *     
     */
    public void setSearchCriteriaExactDeviceManagementEventStatusCompleted(SearchCriteriaExactDeviceManagementEventStatusCompleted value) {
        this.searchCriteriaExactDeviceManagementEventStatusCompleted = value;
    }

    /**
     * Ruft den Wert der searchCriteriaExactDeviceManagementEventAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactDeviceManagementEventAction }
     *     
     */
    public SearchCriteriaExactDeviceManagementEventAction getSearchCriteriaExactDeviceManagementEventAction() {
        return searchCriteriaExactDeviceManagementEventAction;
    }

    /**
     * Legt den Wert der searchCriteriaExactDeviceManagementEventAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactDeviceManagementEventAction }
     *     
     */
    public void setSearchCriteriaExactDeviceManagementEventAction(SearchCriteriaExactDeviceManagementEventAction value) {
        this.searchCriteriaExactDeviceManagementEventAction = value;
    }

    /**
     * Ruft den Wert der searchCriteriaExactDeviceManagementEventLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactDeviceManagementEventLevel }
     *     
     */
    public SearchCriteriaExactDeviceManagementEventLevel getSearchCriteriaExactDeviceManagementEventLevel() {
        return searchCriteriaExactDeviceManagementEventLevel;
    }

    /**
     * Legt den Wert der searchCriteriaExactDeviceManagementEventLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactDeviceManagementEventLevel }
     *     
     */
    public void setSearchCriteriaExactDeviceManagementEventLevel(SearchCriteriaExactDeviceManagementEventLevel value) {
        this.searchCriteriaExactDeviceManagementEventLevel = value;
    }

    /**
     * Ruft den Wert der searchCriteriaExactDeviceManagementEventType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactDeviceManagementEventType }
     *     
     */
    public SearchCriteriaExactDeviceManagementEventType getSearchCriteriaExactDeviceManagementEventType() {
        return searchCriteriaExactDeviceManagementEventType;
    }

    /**
     * Legt den Wert der searchCriteriaExactDeviceManagementEventType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactDeviceManagementEventType }
     *     
     */
    public void setSearchCriteriaExactDeviceManagementEventType(SearchCriteriaExactDeviceManagementEventType value) {
        this.searchCriteriaExactDeviceManagementEventType = value;
    }

    /**
     * Gets the value of the searchCriteriaDeviceManagementEventAdditionalInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDeviceManagementEventAdditionalInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDeviceManagementEventAdditionalInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDeviceManagementEventAdditionalInfo }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDeviceManagementEventAdditionalInfo property.
     */
    public List<SearchCriteriaDeviceManagementEventAdditionalInfo> getSearchCriteriaDeviceManagementEventAdditionalInfo() {
        if (searchCriteriaDeviceManagementEventAdditionalInfo == null) {
            searchCriteriaDeviceManagementEventAdditionalInfo = new ArrayList<>();
        }
        return this.searchCriteriaDeviceManagementEventAdditionalInfo;
    }

    /**
     * Gets the value of the searchCriteriaDeviceManagementEventLoginId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDeviceManagementEventLoginId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDeviceManagementEventLoginId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDeviceManagementEventLoginId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDeviceManagementEventLoginId property.
     */
    public List<SearchCriteriaDeviceManagementEventLoginId> getSearchCriteriaDeviceManagementEventLoginId() {
        if (searchCriteriaDeviceManagementEventLoginId == null) {
            searchCriteriaDeviceManagementEventLoginId = new ArrayList<>();
        }
        return this.searchCriteriaDeviceManagementEventLoginId;
    }

}
