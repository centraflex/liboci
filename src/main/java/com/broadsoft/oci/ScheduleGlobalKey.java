//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Uniquely identifies Holiday and Time Schedules throughout all System, Service Provider, Group and User level.
 *       
 * 
 * <p>Java-Klasse für ScheduleGlobalKey complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ScheduleGlobalKey">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="scheduleKey" type="{}ScheduleKey"/>
 *         <element name="scheduleLevel" type="{}ScheduleLevel"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScheduleGlobalKey", propOrder = {
    "scheduleKey",
    "scheduleLevel"
})
public class ScheduleGlobalKey {

    @XmlElement(required = true)
    protected ScheduleKey scheduleKey;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ScheduleLevel scheduleLevel;

    /**
     * Ruft den Wert der scheduleKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ScheduleKey }
     *     
     */
    public ScheduleKey getScheduleKey() {
        return scheduleKey;
    }

    /**
     * Legt den Wert der scheduleKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ScheduleKey }
     *     
     */
    public void setScheduleKey(ScheduleKey value) {
        this.scheduleKey = value;
    }

    /**
     * Ruft den Wert der scheduleLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ScheduleLevel }
     *     
     */
    public ScheduleLevel getScheduleLevel() {
        return scheduleLevel;
    }

    /**
     * Legt den Wert der scheduleLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ScheduleLevel }
     *     
     */
    public void setScheduleLevel(ScheduleLevel value) {
        this.scheduleLevel = value;
    }

}
