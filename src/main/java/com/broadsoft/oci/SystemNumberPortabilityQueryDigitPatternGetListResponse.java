//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemNumberPortabilityQueryDigitPatternGetListRequest.
 *         Contains a table with column headings:
 *         "Digit Pattern", "Status".
 *       
 * 
 * <p>Java-Klasse für SystemNumberPortabilityQueryDigitPatternGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNumberPortabilityQueryDigitPatternGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="statusDigitPatternTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNumberPortabilityQueryDigitPatternGetListResponse", propOrder = {
    "statusDigitPatternTable"
})
public class SystemNumberPortabilityQueryDigitPatternGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable statusDigitPatternTable;

    /**
     * Ruft den Wert der statusDigitPatternTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getStatusDigitPatternTable() {
        return statusDigitPatternTable;
    }

    /**
     * Legt den Wert der statusDigitPatternTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setStatusDigitPatternTable(OCITable value) {
        this.statusDigitPatternTable = value;
    }

}
