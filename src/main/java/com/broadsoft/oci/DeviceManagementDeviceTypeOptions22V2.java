//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Device Management System device type options.
 *         
 *         Note: For the elements listed below, when device configuration is set to deviceManagement, those elements apply to the creation of the Polycom Phone Services directory file only.
 *               For all other files, they are not used. Those elements are instead configured on a per-file basis at the Device Type File level.
 *               When device configuration is set to legacy, those elements apply to all configuration files.
 *         
 *               useHttpDigestAuthentication
 *               macBasedFileAuthentication
 *               userNamePasswordFileAuthentication
 *               macInNonRequestURI
 *               macInCert
 *               macFormatInNonRequestURI
 *       
 * 
 * <p>Java-Klasse für DeviceManagementDeviceTypeOptions22V2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="DeviceManagementDeviceTypeOptions22V2">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="deviceAccessProtocol" type="{}DeviceAccessProtocol22"/>
 *         <element name="tagMode" type="{}DeviceManagementTagMode"/>
 *         <element name="tagSet" type="{}DeviceManagementTagSet" minOccurs="0"/>
 *         <element name="allowDeviceProfileCustomTagSet" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowGroupCustomTagSet" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowSpCustomTagSet" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sendEmailUponResetFailure" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="deviceAccessNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="deviceAccessPort" type="{}Port" minOccurs="0"/>
 *         <element name="deviceAccessContext" type="{}DeviceAccessContext" minOccurs="0"/>
 *         <element name="deviceAccessURI" type="{}DeviceManagementAccessURI" minOccurs="0"/>
 *         <element name="defaultDeviceLanguage" type="{}DeviceLanguage" minOccurs="0"/>
 *         <element name="defaultDeviceEncoding" type="{}Encoding" minOccurs="0"/>
 *         <element name="accessDeviceCredentials" type="{}DeviceManagementUserNamePassword16" minOccurs="0"/>
 *         <element name="useHttpDigestAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macBasedFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="userNamePasswordFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macInNonRequestURI" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macInCert" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macFormatInNonRequestURI" type="{}DeviceManagementAccessURI" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeviceManagementDeviceTypeOptions22V2", propOrder = {
    "deviceAccessProtocol",
    "tagMode",
    "tagSet",
    "allowDeviceProfileCustomTagSet",
    "allowGroupCustomTagSet",
    "allowSpCustomTagSet",
    "sendEmailUponResetFailure",
    "deviceAccessNetAddress",
    "deviceAccessPort",
    "deviceAccessContext",
    "deviceAccessURI",
    "defaultDeviceLanguage",
    "defaultDeviceEncoding",
    "accessDeviceCredentials",
    "useHttpDigestAuthentication",
    "macBasedFileAuthentication",
    "userNamePasswordFileAuthentication",
    "macInNonRequestURI",
    "macInCert",
    "macFormatInNonRequestURI"
})
public class DeviceManagementDeviceTypeOptions22V2 {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceAccessProtocol22 deviceAccessProtocol;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceManagementTagMode tagMode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String tagSet;
    protected boolean allowDeviceProfileCustomTagSet;
    protected boolean allowGroupCustomTagSet;
    protected boolean allowSpCustomTagSet;
    protected boolean sendEmailUponResetFailure;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceAccessNetAddress;
    protected Integer deviceAccessPort;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceAccessContext;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceAccessURI;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultDeviceLanguage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultDeviceEncoding;
    protected DeviceManagementUserNamePassword16 accessDeviceCredentials;
    protected boolean useHttpDigestAuthentication;
    protected boolean macBasedFileAuthentication;
    protected boolean userNamePasswordFileAuthentication;
    protected boolean macInNonRequestURI;
    protected boolean macInCert;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String macFormatInNonRequestURI;

    /**
     * Ruft den Wert der deviceAccessProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceAccessProtocol22 }
     *     
     */
    public DeviceAccessProtocol22 getDeviceAccessProtocol() {
        return deviceAccessProtocol;
    }

    /**
     * Legt den Wert der deviceAccessProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceAccessProtocol22 }
     *     
     */
    public void setDeviceAccessProtocol(DeviceAccessProtocol22 value) {
        this.deviceAccessProtocol = value;
    }

    /**
     * Ruft den Wert der tagMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementTagMode }
     *     
     */
    public DeviceManagementTagMode getTagMode() {
        return tagMode;
    }

    /**
     * Legt den Wert der tagMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementTagMode }
     *     
     */
    public void setTagMode(DeviceManagementTagMode value) {
        this.tagMode = value;
    }

    /**
     * Ruft den Wert der tagSet-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTagSet() {
        return tagSet;
    }

    /**
     * Legt den Wert der tagSet-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTagSet(String value) {
        this.tagSet = value;
    }

    /**
     * Ruft den Wert der allowDeviceProfileCustomTagSet-Eigenschaft ab.
     * 
     */
    public boolean isAllowDeviceProfileCustomTagSet() {
        return allowDeviceProfileCustomTagSet;
    }

    /**
     * Legt den Wert der allowDeviceProfileCustomTagSet-Eigenschaft fest.
     * 
     */
    public void setAllowDeviceProfileCustomTagSet(boolean value) {
        this.allowDeviceProfileCustomTagSet = value;
    }

    /**
     * Ruft den Wert der allowGroupCustomTagSet-Eigenschaft ab.
     * 
     */
    public boolean isAllowGroupCustomTagSet() {
        return allowGroupCustomTagSet;
    }

    /**
     * Legt den Wert der allowGroupCustomTagSet-Eigenschaft fest.
     * 
     */
    public void setAllowGroupCustomTagSet(boolean value) {
        this.allowGroupCustomTagSet = value;
    }

    /**
     * Ruft den Wert der allowSpCustomTagSet-Eigenschaft ab.
     * 
     */
    public boolean isAllowSpCustomTagSet() {
        return allowSpCustomTagSet;
    }

    /**
     * Legt den Wert der allowSpCustomTagSet-Eigenschaft fest.
     * 
     */
    public void setAllowSpCustomTagSet(boolean value) {
        this.allowSpCustomTagSet = value;
    }

    /**
     * Ruft den Wert der sendEmailUponResetFailure-Eigenschaft ab.
     * 
     */
    public boolean isSendEmailUponResetFailure() {
        return sendEmailUponResetFailure;
    }

    /**
     * Legt den Wert der sendEmailUponResetFailure-Eigenschaft fest.
     * 
     */
    public void setSendEmailUponResetFailure(boolean value) {
        this.sendEmailUponResetFailure = value;
    }

    /**
     * Ruft den Wert der deviceAccessNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceAccessNetAddress() {
        return deviceAccessNetAddress;
    }

    /**
     * Legt den Wert der deviceAccessNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceAccessNetAddress(String value) {
        this.deviceAccessNetAddress = value;
    }

    /**
     * Ruft den Wert der deviceAccessPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDeviceAccessPort() {
        return deviceAccessPort;
    }

    /**
     * Legt den Wert der deviceAccessPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDeviceAccessPort(Integer value) {
        this.deviceAccessPort = value;
    }

    /**
     * Ruft den Wert der deviceAccessContext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceAccessContext() {
        return deviceAccessContext;
    }

    /**
     * Legt den Wert der deviceAccessContext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceAccessContext(String value) {
        this.deviceAccessContext = value;
    }

    /**
     * Ruft den Wert der deviceAccessURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceAccessURI() {
        return deviceAccessURI;
    }

    /**
     * Legt den Wert der deviceAccessURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceAccessURI(String value) {
        this.deviceAccessURI = value;
    }

    /**
     * Ruft den Wert der defaultDeviceLanguage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultDeviceLanguage() {
        return defaultDeviceLanguage;
    }

    /**
     * Legt den Wert der defaultDeviceLanguage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultDeviceLanguage(String value) {
        this.defaultDeviceLanguage = value;
    }

    /**
     * Ruft den Wert der defaultDeviceEncoding-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultDeviceEncoding() {
        return defaultDeviceEncoding;
    }

    /**
     * Legt den Wert der defaultDeviceEncoding-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultDeviceEncoding(String value) {
        this.defaultDeviceEncoding = value;
    }

    /**
     * Ruft den Wert der accessDeviceCredentials-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementUserNamePassword16 }
     *     
     */
    public DeviceManagementUserNamePassword16 getAccessDeviceCredentials() {
        return accessDeviceCredentials;
    }

    /**
     * Legt den Wert der accessDeviceCredentials-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementUserNamePassword16 }
     *     
     */
    public void setAccessDeviceCredentials(DeviceManagementUserNamePassword16 value) {
        this.accessDeviceCredentials = value;
    }

    /**
     * Ruft den Wert der useHttpDigestAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isUseHttpDigestAuthentication() {
        return useHttpDigestAuthentication;
    }

    /**
     * Legt den Wert der useHttpDigestAuthentication-Eigenschaft fest.
     * 
     */
    public void setUseHttpDigestAuthentication(boolean value) {
        this.useHttpDigestAuthentication = value;
    }

    /**
     * Ruft den Wert der macBasedFileAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isMacBasedFileAuthentication() {
        return macBasedFileAuthentication;
    }

    /**
     * Legt den Wert der macBasedFileAuthentication-Eigenschaft fest.
     * 
     */
    public void setMacBasedFileAuthentication(boolean value) {
        this.macBasedFileAuthentication = value;
    }

    /**
     * Ruft den Wert der userNamePasswordFileAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isUserNamePasswordFileAuthentication() {
        return userNamePasswordFileAuthentication;
    }

    /**
     * Legt den Wert der userNamePasswordFileAuthentication-Eigenschaft fest.
     * 
     */
    public void setUserNamePasswordFileAuthentication(boolean value) {
        this.userNamePasswordFileAuthentication = value;
    }

    /**
     * Ruft den Wert der macInNonRequestURI-Eigenschaft ab.
     * 
     */
    public boolean isMacInNonRequestURI() {
        return macInNonRequestURI;
    }

    /**
     * Legt den Wert der macInNonRequestURI-Eigenschaft fest.
     * 
     */
    public void setMacInNonRequestURI(boolean value) {
        this.macInNonRequestURI = value;
    }

    /**
     * Ruft den Wert der macInCert-Eigenschaft ab.
     * 
     */
    public boolean isMacInCert() {
        return macInCert;
    }

    /**
     * Legt den Wert der macInCert-Eigenschaft fest.
     * 
     */
    public void setMacInCert(boolean value) {
        this.macInCert = value;
    }

    /**
     * Ruft den Wert der macFormatInNonRequestURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacFormatInNonRequestURI() {
        return macFormatInNonRequestURI;
    }

    /**
     * Legt den Wert der macFormatInNonRequestURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacFormatInNonRequestURI(String value) {
        this.macFormatInNonRequestURI = value;
    }

}
