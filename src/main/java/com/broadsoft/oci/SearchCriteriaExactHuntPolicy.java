//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for a particular fully specified hunt policy.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactHuntPolicy complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactHuntPolicy">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="huntPolicy" type="{}HuntPolicy"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactHuntPolicy", propOrder = {
    "huntPolicy"
})
public class SearchCriteriaExactHuntPolicy
    extends SearchCriteria
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected HuntPolicy huntPolicy;

    /**
     * Ruft den Wert der huntPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HuntPolicy }
     *     
     */
    public HuntPolicy getHuntPolicy() {
        return huntPolicy;
    }

    /**
     * Legt den Wert der huntPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HuntPolicy }
     *     
     */
    public void setHuntPolicy(HuntPolicy value) {
        this.huntPolicy = value;
    }

}
