//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TrunkGroupUserCreationUserIdFormat.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="TrunkGroupUserCreationUserIdFormat">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Extension"/>
 *     <enumeration value="National DN"/>
 *     <enumeration value="E164 Format No Plus"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TrunkGroupUserCreationUserIdFormat")
@XmlEnum
public enum TrunkGroupUserCreationUserIdFormat {

    @XmlEnumValue("Extension")
    EXTENSION("Extension"),
    @XmlEnumValue("National DN")
    NATIONAL_DN("National DN"),
    @XmlEnumValue("E164 Format No Plus")
    E_164_FORMAT_NO_PLUS("E164 Format No Plus");
    private final String value;

    TrunkGroupUserCreationUserIdFormat(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrunkGroupUserCreationUserIdFormat fromValue(String v) {
        for (TrunkGroupUserCreationUserIdFormat c: TrunkGroupUserCreationUserIdFormat.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
