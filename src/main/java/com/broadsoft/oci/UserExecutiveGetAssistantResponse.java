//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserExecutiveGetAssistantsRequest.
 *         Contains the assistant setting and a table of assigned assistants.
 *         The table has column headings: "User Id", "Last Name", "First Name", "Hiragana Last Name", 
 *         "Hiragana First Name", "Phone Number", "Extension", "Department", "Email Address" and "Opt-in".
 *         The possible values for "Opt-in" column are "true" and "false".
 *       
 * 
 * <p>Java-Klasse für UserExecutiveGetAssistantResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserExecutiveGetAssistantResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="allowOptInOut" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="assistantUserTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserExecutiveGetAssistantResponse", propOrder = {
    "allowOptInOut",
    "assistantUserTable"
})
public class UserExecutiveGetAssistantResponse
    extends OCIDataResponse
{

    protected boolean allowOptInOut;
    @XmlElement(required = true)
    protected OCITable assistantUserTable;

    /**
     * Ruft den Wert der allowOptInOut-Eigenschaft ab.
     * 
     */
    public boolean isAllowOptInOut() {
        return allowOptInOut;
    }

    /**
     * Legt den Wert der allowOptInOut-Eigenschaft fest.
     * 
     */
    public void setAllowOptInOut(boolean value) {
        this.allowOptInOut = value;
    }

    /**
     * Ruft den Wert der assistantUserTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAssistantUserTable() {
        return assistantUserTable;
    }

    /**
     * Legt den Wert der assistantUserTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAssistantUserTable(OCITable value) {
        this.assistantUserTable = value;
    }

}
