//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a new file repository user.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemFileRepositoryDeviceUserAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemFileRepositoryDeviceUserAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="fileRepositoryName" type="{}FileRepositoryName"/>
 *         <element name="userName" type="{}FileRepositoryUserName"/>
 *         <element name="password" type="{}FileRepositoryUserPassword"/>
 *         <element name="allowPut" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowDelete" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowGet" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemFileRepositoryDeviceUserAddRequest", propOrder = {
    "fileRepositoryName",
    "userName",
    "password",
    "allowPut",
    "allowDelete",
    "allowGet"
})
public class SystemFileRepositoryDeviceUserAddRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String fileRepositoryName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String password;
    protected boolean allowPut;
    protected boolean allowDelete;
    protected boolean allowGet;

    /**
     * Ruft den Wert der fileRepositoryName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileRepositoryName() {
        return fileRepositoryName;
    }

    /**
     * Legt den Wert der fileRepositoryName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileRepositoryName(String value) {
        this.fileRepositoryName = value;
    }

    /**
     * Ruft den Wert der userName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Legt den Wert der userName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Ruft den Wert der password-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Legt den Wert der password-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Ruft den Wert der allowPut-Eigenschaft ab.
     * 
     */
    public boolean isAllowPut() {
        return allowPut;
    }

    /**
     * Legt den Wert der allowPut-Eigenschaft fest.
     * 
     */
    public void setAllowPut(boolean value) {
        this.allowPut = value;
    }

    /**
     * Ruft den Wert der allowDelete-Eigenschaft ab.
     * 
     */
    public boolean isAllowDelete() {
        return allowDelete;
    }

    /**
     * Legt den Wert der allowDelete-Eigenschaft fest.
     * 
     */
    public void setAllowDelete(boolean value) {
        this.allowDelete = value;
    }

    /**
     * Ruft den Wert der allowGet-Eigenschaft ab.
     * 
     */
    public boolean isAllowGet() {
        return allowGet;
    }

    /**
     * Legt den Wert der allowGet-Eigenschaft fest.
     * 
     */
    public void setAllowGet(boolean value) {
        this.allowGet = value;
    }

}
