//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserVoiceMessagingUserGetVoicePortalPasswordInfoRequest.
 *         Replaced By: UserPortalPasscodeGetInfoResponse
 *       
 * 
 * <p>Java-Klasse für UserVoiceMessagingUserGetVoicePortalPasswordInfoResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserVoiceMessagingUserGetVoicePortalPasswordInfoResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isLoginDisabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <choice>
 *           <element name="expirationDays" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *           <element name="doesNotExpire" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         </choice>
 *         <element name="password" type="{}Password"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserVoiceMessagingUserGetVoicePortalPasswordInfoResponse", propOrder = {
    "isLoginDisabled",
    "expirationDays",
    "doesNotExpire",
    "password"
})
public class UserVoiceMessagingUserGetVoicePortalPasswordInfoResponse
    extends OCIDataResponse
{

    protected boolean isLoginDisabled;
    protected Integer expirationDays;
    protected Boolean doesNotExpire;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String password;

    /**
     * Ruft den Wert der isLoginDisabled-Eigenschaft ab.
     * 
     */
    public boolean isIsLoginDisabled() {
        return isLoginDisabled;
    }

    /**
     * Legt den Wert der isLoginDisabled-Eigenschaft fest.
     * 
     */
    public void setIsLoginDisabled(boolean value) {
        this.isLoginDisabled = value;
    }

    /**
     * Ruft den Wert der expirationDays-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExpirationDays() {
        return expirationDays;
    }

    /**
     * Legt den Wert der expirationDays-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExpirationDays(Integer value) {
        this.expirationDays = value;
    }

    /**
     * Ruft den Wert der doesNotExpire-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDoesNotExpire() {
        return doesNotExpire;
    }

    /**
     * Legt den Wert der doesNotExpire-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDoesNotExpire(Boolean value) {
        this.doesNotExpire = value;
    }

    /**
     * Ruft den Wert der password-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Legt den Wert der password-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

}
