//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupCallCenterEnhancedReportingGetRequest.
 *         
 *         Replaced by GroupCallCenterEnhancedReportingGetResponse19
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterEnhancedReportingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterEnhancedReportingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="reportingServer" type="{}CallCenterReportServerChoice"/>
 *         <element name="webStatisticSource" type="{}CallCenterReportWebStatisticsSource"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterEnhancedReportingGetResponse", propOrder = {
    "reportingServer",
    "webStatisticSource"
})
public class GroupCallCenterEnhancedReportingGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportServerChoice reportingServer;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportWebStatisticsSource webStatisticSource;

    /**
     * Ruft den Wert der reportingServer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportServerChoice }
     *     
     */
    public CallCenterReportServerChoice getReportingServer() {
        return reportingServer;
    }

    /**
     * Legt den Wert der reportingServer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportServerChoice }
     *     
     */
    public void setReportingServer(CallCenterReportServerChoice value) {
        this.reportingServer = value;
    }

    /**
     * Ruft den Wert der webStatisticSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportWebStatisticsSource }
     *     
     */
    public CallCenterReportWebStatisticsSource getWebStatisticSource() {
        return webStatisticSource;
    }

    /**
     * Legt den Wert der webStatisticSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportWebStatisticsSource }
     *     
     */
    public void setWebStatisticSource(CallCenterReportWebStatisticsSource value) {
        this.webStatisticSource = value;
    }

}
