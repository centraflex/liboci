//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserAnnouncementFileGetListRequest.
 *         When requested, the response contains a table with columns: "Name", 
 *         "Media Type", "File Size", "Announcement File External Id".
 *         The "Name" column contains the name of the announcement file.
 *         The "Media Type" column contains the media type of the announcement file with the possible values:
 *                 WMA - Windows Media Audio file
 *                 WAV - A WAV file
 *                 3GP - A 3GP file
 *                 MOV - A MOV file using a H.263 or H.264 codec.
 *         The "File Size" column contains the file size in kB of the announcement file.
 *         The "Announcement File External Id" column contains the external ID of the announcement file.
 *  
 *         The following columns are populated in AS data mode only:       
 *           "Announcement File External Id"
 *           
 *         The response also contains the current total file size (KB) for the user across
 *         all media types and the maximum total file size (MB) allowed for the user.
 *       
 * 
 * <p>Java-Klasse für UserAnnouncementFileGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserAnnouncementFileGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="announcementTable" type="{C}OCITable" minOccurs="0"/>
 *         <element name="totalFileSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="maxFileSize" type="{}RepositoryTotalFileSize"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserAnnouncementFileGetListResponse", propOrder = {
    "announcementTable",
    "totalFileSize",
    "maxFileSize"
})
public class UserAnnouncementFileGetListResponse
    extends OCIDataResponse
{

    protected OCITable announcementTable;
    protected int totalFileSize;
    protected int maxFileSize;

    /**
     * Ruft den Wert der announcementTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAnnouncementTable() {
        return announcementTable;
    }

    /**
     * Legt den Wert der announcementTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAnnouncementTable(OCITable value) {
        this.announcementTable = value;
    }

    /**
     * Ruft den Wert der totalFileSize-Eigenschaft ab.
     * 
     */
    public int getTotalFileSize() {
        return totalFileSize;
    }

    /**
     * Legt den Wert der totalFileSize-Eigenschaft fest.
     * 
     */
    public void setTotalFileSize(int value) {
        this.totalFileSize = value;
    }

    /**
     * Ruft den Wert der maxFileSize-Eigenschaft ab.
     * 
     */
    public int getMaxFileSize() {
        return maxFileSize;
    }

    /**
     * Legt den Wert der maxFileSize-Eigenschaft fest.
     * 
     */
    public void setMaxFileSize(int value) {
        this.maxFileSize = value;
    }

}
