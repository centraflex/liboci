//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallingPartyCategorySelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallingPartyCategorySelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Special"/>
 *     <enumeration value="Hospital"/>
 *     <enumeration value="Hotel"/>
 *     <enumeration value="Prison"/>
 *     <enumeration value="Payphone"/>
 *     <enumeration value="Ordinary"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallingPartyCategorySelection")
@XmlEnum
public enum CallingPartyCategorySelection {

    @XmlEnumValue("Special")
    SPECIAL("Special"),
    @XmlEnumValue("Hospital")
    HOSPITAL("Hospital"),
    @XmlEnumValue("Hotel")
    HOTEL("Hotel"),
    @XmlEnumValue("Prison")
    PRISON("Prison"),
    @XmlEnumValue("Payphone")
    PAYPHONE("Payphone"),
    @XmlEnumValue("Ordinary")
    ORDINARY("Ordinary");
    private final String value;

    CallingPartyCategorySelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallingPartyCategorySelection fromValue(String v) {
        for (CallingPartyCategorySelection c: CallingPartyCategorySelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
