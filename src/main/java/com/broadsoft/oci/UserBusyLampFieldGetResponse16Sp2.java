//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserBusyLampFieldGetRequest16sp2.
 *         The table has column headings:
 *           "User Id", "Last Name", "First Name", "Hiragana Last Name", "Hiragana First Name",
 *           "Phone Number", "Extension", "Department", "Email Address", "IMP Id".
 *       
 * 
 * <p>Java-Klasse für UserBusyLampFieldGetResponse16sp2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBusyLampFieldGetResponse16sp2">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="listURI" type="{}SIPURI" minOccurs="0"/>
 *         <element name="enableCallParkNotification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="monitoredUserTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBusyLampFieldGetResponse16sp2", propOrder = {
    "listURI",
    "enableCallParkNotification",
    "monitoredUserTable"
})
public class UserBusyLampFieldGetResponse16Sp2
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String listURI;
    protected boolean enableCallParkNotification;
    @XmlElement(required = true)
    protected OCITable monitoredUserTable;

    /**
     * Ruft den Wert der listURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListURI() {
        return listURI;
    }

    /**
     * Legt den Wert der listURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListURI(String value) {
        this.listURI = value;
    }

    /**
     * Ruft den Wert der enableCallParkNotification-Eigenschaft ab.
     * 
     */
    public boolean isEnableCallParkNotification() {
        return enableCallParkNotification;
    }

    /**
     * Legt den Wert der enableCallParkNotification-Eigenschaft fest.
     * 
     */
    public void setEnableCallParkNotification(boolean value) {
        this.enableCallParkNotification = value;
    }

    /**
     * Ruft den Wert der monitoredUserTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getMonitoredUserTable() {
        return monitoredUserTable;
    }

    /**
     * Legt den Wert der monitoredUserTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setMonitoredUserTable(OCITable value) {
        this.monitoredUserTable = value;
    }

}
