//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the service provider administrator's policy settings.
 *         The response is either SuccessResponse or ErrorResponse.
 *         The networkPolicyAccess is only modified for the enterprise administrator.
 *         The following elements are only used in AS data mode:
 *             dialableCallerIDAccess
 *             verifyTranslationAndroutingAccess
 *       
 * 
 * <p>Java-Klasse für ServiceProviderAdminModifyPolicyRequest14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderAdminModifyPolicyRequest14">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="profileAccess" type="{}ServiceProviderAdminProfileAccess" minOccurs="0"/>
 *         <element name="groupAccess" type="{}ServiceProviderAdminGroupAccess" minOccurs="0"/>
 *         <element name="userAccess" type="{}ServiceProviderAdminUserAccess" minOccurs="0"/>
 *         <element name="adminAccess" type="{}ServiceProviderAdminAdminAccess" minOccurs="0"/>
 *         <element name="departmentAccess" type="{}ServiceProviderAdminDepartmentAccess" minOccurs="0"/>
 *         <element name="accessDeviceAccess" type="{}ServiceProviderAdminAccessDeviceAccess" minOccurs="0"/>
 *         <element name="phoneNumberExtensionAccess" type="{}ServiceProviderAdminPhoneNumberExtensionAccess" minOccurs="0"/>
 *         <element name="callingLineIdNumberAccess" type="{}ServiceProviderAdminCallingLineIdNumberAccess" minOccurs="0"/>
 *         <element name="serviceAccess" type="{}ServiceProviderAdminServiceAccess" minOccurs="0"/>
 *         <element name="servicePackAccess" type="{}ServiceProviderAdminServicePackAccess" minOccurs="0"/>
 *         <element name="sessionAdmissionControlAccess" type="{}ServiceProviderAdminSessionAdmissionControlAccess" minOccurs="0"/>
 *         <element name="webBrandingAccess" type="{}ServiceProviderAdminWebBrandingAccess" minOccurs="0"/>
 *         <element name="officeZoneAccess" type="{}ServiceProviderAdminOfficeZoneAccess" minOccurs="0"/>
 *         <element name="communicationBarringAccess" type="{}ServiceProviderAdminCommunicationBarringAccess" minOccurs="0"/>
 *         <element name="networkPolicyAccess" type="{}EnterpriseAdminNetworkPolicyAccess" minOccurs="0"/>
 *         <element name="numberActivationAccess" type="{}EnterpriseAdminNumberActivationAccess" minOccurs="0"/>
 *         <element name="dialableCallerIDAccess" type="{}ServiceProviderAdminDialableCallerIDAccess" minOccurs="0"/>
 *         <element name="verifyTranslationAndRoutingAccess" type="{}ServiceProviderAdminVerifyTranslationAndRoutingAccess" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderAdminModifyPolicyRequest14", propOrder = {
    "userId",
    "profileAccess",
    "groupAccess",
    "userAccess",
    "adminAccess",
    "departmentAccess",
    "accessDeviceAccess",
    "phoneNumberExtensionAccess",
    "callingLineIdNumberAccess",
    "serviceAccess",
    "servicePackAccess",
    "sessionAdmissionControlAccess",
    "webBrandingAccess",
    "officeZoneAccess",
    "communicationBarringAccess",
    "networkPolicyAccess",
    "numberActivationAccess",
    "dialableCallerIDAccess",
    "verifyTranslationAndRoutingAccess"
})
public class ServiceProviderAdminModifyPolicyRequest14
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminProfileAccess profileAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminGroupAccess groupAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminUserAccess userAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminAdminAccess adminAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminDepartmentAccess departmentAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminAccessDeviceAccess accessDeviceAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminPhoneNumberExtensionAccess phoneNumberExtensionAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminCallingLineIdNumberAccess callingLineIdNumberAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminServiceAccess serviceAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminServicePackAccess servicePackAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminSessionAdmissionControlAccess sessionAdmissionControlAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminWebBrandingAccess webBrandingAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminOfficeZoneAccess officeZoneAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminCommunicationBarringAccess communicationBarringAccess;
    @XmlSchemaType(name = "token")
    protected EnterpriseAdminNetworkPolicyAccess networkPolicyAccess;
    @XmlSchemaType(name = "token")
    protected EnterpriseAdminNumberActivationAccess numberActivationAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminDialableCallerIDAccess dialableCallerIDAccess;
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminVerifyTranslationAndRoutingAccess verifyTranslationAndRoutingAccess;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der profileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminProfileAccess }
     *     
     */
    public ServiceProviderAdminProfileAccess getProfileAccess() {
        return profileAccess;
    }

    /**
     * Legt den Wert der profileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminProfileAccess }
     *     
     */
    public void setProfileAccess(ServiceProviderAdminProfileAccess value) {
        this.profileAccess = value;
    }

    /**
     * Ruft den Wert der groupAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminGroupAccess }
     *     
     */
    public ServiceProviderAdminGroupAccess getGroupAccess() {
        return groupAccess;
    }

    /**
     * Legt den Wert der groupAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminGroupAccess }
     *     
     */
    public void setGroupAccess(ServiceProviderAdminGroupAccess value) {
        this.groupAccess = value;
    }

    /**
     * Ruft den Wert der userAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminUserAccess }
     *     
     */
    public ServiceProviderAdminUserAccess getUserAccess() {
        return userAccess;
    }

    /**
     * Legt den Wert der userAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminUserAccess }
     *     
     */
    public void setUserAccess(ServiceProviderAdminUserAccess value) {
        this.userAccess = value;
    }

    /**
     * Ruft den Wert der adminAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminAdminAccess }
     *     
     */
    public ServiceProviderAdminAdminAccess getAdminAccess() {
        return adminAccess;
    }

    /**
     * Legt den Wert der adminAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminAdminAccess }
     *     
     */
    public void setAdminAccess(ServiceProviderAdminAdminAccess value) {
        this.adminAccess = value;
    }

    /**
     * Ruft den Wert der departmentAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminDepartmentAccess }
     *     
     */
    public ServiceProviderAdminDepartmentAccess getDepartmentAccess() {
        return departmentAccess;
    }

    /**
     * Legt den Wert der departmentAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminDepartmentAccess }
     *     
     */
    public void setDepartmentAccess(ServiceProviderAdminDepartmentAccess value) {
        this.departmentAccess = value;
    }

    /**
     * Ruft den Wert der accessDeviceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminAccessDeviceAccess }
     *     
     */
    public ServiceProviderAdminAccessDeviceAccess getAccessDeviceAccess() {
        return accessDeviceAccess;
    }

    /**
     * Legt den Wert der accessDeviceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminAccessDeviceAccess }
     *     
     */
    public void setAccessDeviceAccess(ServiceProviderAdminAccessDeviceAccess value) {
        this.accessDeviceAccess = value;
    }

    /**
     * Ruft den Wert der phoneNumberExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminPhoneNumberExtensionAccess }
     *     
     */
    public ServiceProviderAdminPhoneNumberExtensionAccess getPhoneNumberExtensionAccess() {
        return phoneNumberExtensionAccess;
    }

    /**
     * Legt den Wert der phoneNumberExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminPhoneNumberExtensionAccess }
     *     
     */
    public void setPhoneNumberExtensionAccess(ServiceProviderAdminPhoneNumberExtensionAccess value) {
        this.phoneNumberExtensionAccess = value;
    }

    /**
     * Ruft den Wert der callingLineIdNumberAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminCallingLineIdNumberAccess }
     *     
     */
    public ServiceProviderAdminCallingLineIdNumberAccess getCallingLineIdNumberAccess() {
        return callingLineIdNumberAccess;
    }

    /**
     * Legt den Wert der callingLineIdNumberAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminCallingLineIdNumberAccess }
     *     
     */
    public void setCallingLineIdNumberAccess(ServiceProviderAdminCallingLineIdNumberAccess value) {
        this.callingLineIdNumberAccess = value;
    }

    /**
     * Ruft den Wert der serviceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminServiceAccess }
     *     
     */
    public ServiceProviderAdminServiceAccess getServiceAccess() {
        return serviceAccess;
    }

    /**
     * Legt den Wert der serviceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminServiceAccess }
     *     
     */
    public void setServiceAccess(ServiceProviderAdminServiceAccess value) {
        this.serviceAccess = value;
    }

    /**
     * Ruft den Wert der servicePackAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminServicePackAccess }
     *     
     */
    public ServiceProviderAdminServicePackAccess getServicePackAccess() {
        return servicePackAccess;
    }

    /**
     * Legt den Wert der servicePackAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminServicePackAccess }
     *     
     */
    public void setServicePackAccess(ServiceProviderAdminServicePackAccess value) {
        this.servicePackAccess = value;
    }

    /**
     * Ruft den Wert der sessionAdmissionControlAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminSessionAdmissionControlAccess }
     *     
     */
    public ServiceProviderAdminSessionAdmissionControlAccess getSessionAdmissionControlAccess() {
        return sessionAdmissionControlAccess;
    }

    /**
     * Legt den Wert der sessionAdmissionControlAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminSessionAdmissionControlAccess }
     *     
     */
    public void setSessionAdmissionControlAccess(ServiceProviderAdminSessionAdmissionControlAccess value) {
        this.sessionAdmissionControlAccess = value;
    }

    /**
     * Ruft den Wert der webBrandingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminWebBrandingAccess }
     *     
     */
    public ServiceProviderAdminWebBrandingAccess getWebBrandingAccess() {
        return webBrandingAccess;
    }

    /**
     * Legt den Wert der webBrandingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminWebBrandingAccess }
     *     
     */
    public void setWebBrandingAccess(ServiceProviderAdminWebBrandingAccess value) {
        this.webBrandingAccess = value;
    }

    /**
     * Ruft den Wert der officeZoneAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminOfficeZoneAccess }
     *     
     */
    public ServiceProviderAdminOfficeZoneAccess getOfficeZoneAccess() {
        return officeZoneAccess;
    }

    /**
     * Legt den Wert der officeZoneAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminOfficeZoneAccess }
     *     
     */
    public void setOfficeZoneAccess(ServiceProviderAdminOfficeZoneAccess value) {
        this.officeZoneAccess = value;
    }

    /**
     * Ruft den Wert der communicationBarringAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminCommunicationBarringAccess }
     *     
     */
    public ServiceProviderAdminCommunicationBarringAccess getCommunicationBarringAccess() {
        return communicationBarringAccess;
    }

    /**
     * Legt den Wert der communicationBarringAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminCommunicationBarringAccess }
     *     
     */
    public void setCommunicationBarringAccess(ServiceProviderAdminCommunicationBarringAccess value) {
        this.communicationBarringAccess = value;
    }

    /**
     * Ruft den Wert der networkPolicyAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseAdminNetworkPolicyAccess }
     *     
     */
    public EnterpriseAdminNetworkPolicyAccess getNetworkPolicyAccess() {
        return networkPolicyAccess;
    }

    /**
     * Legt den Wert der networkPolicyAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseAdminNetworkPolicyAccess }
     *     
     */
    public void setNetworkPolicyAccess(EnterpriseAdminNetworkPolicyAccess value) {
        this.networkPolicyAccess = value;
    }

    /**
     * Ruft den Wert der numberActivationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseAdminNumberActivationAccess }
     *     
     */
    public EnterpriseAdminNumberActivationAccess getNumberActivationAccess() {
        return numberActivationAccess;
    }

    /**
     * Legt den Wert der numberActivationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseAdminNumberActivationAccess }
     *     
     */
    public void setNumberActivationAccess(EnterpriseAdminNumberActivationAccess value) {
        this.numberActivationAccess = value;
    }

    /**
     * Ruft den Wert der dialableCallerIDAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminDialableCallerIDAccess }
     *     
     */
    public ServiceProviderAdminDialableCallerIDAccess getDialableCallerIDAccess() {
        return dialableCallerIDAccess;
    }

    /**
     * Legt den Wert der dialableCallerIDAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminDialableCallerIDAccess }
     *     
     */
    public void setDialableCallerIDAccess(ServiceProviderAdminDialableCallerIDAccess value) {
        this.dialableCallerIDAccess = value;
    }

    /**
     * Ruft den Wert der verifyTranslationAndRoutingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminVerifyTranslationAndRoutingAccess }
     *     
     */
    public ServiceProviderAdminVerifyTranslationAndRoutingAccess getVerifyTranslationAndRoutingAccess() {
        return verifyTranslationAndRoutingAccess;
    }

    /**
     * Legt den Wert der verifyTranslationAndRoutingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminVerifyTranslationAndRoutingAccess }
     *     
     */
    public void setVerifyTranslationAndRoutingAccess(ServiceProviderAdminVerifyTranslationAndRoutingAccess value) {
        this.verifyTranslationAndRoutingAccess = value;
    }

}
