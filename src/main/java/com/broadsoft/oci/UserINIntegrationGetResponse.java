//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *          Response to UserINIntegrationGetRequest
 *       
 * 
 * <p>Java-Klasse für UserINIntegrationGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserINIntegrationGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="originatingServiceKey" type="{}MobilityManagerServiceKey" minOccurs="0"/>
 *         <element name="terminatingServiceKey" type="{}MobilityManagerServiceKey" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserINIntegrationGetResponse", propOrder = {
    "originatingServiceKey",
    "terminatingServiceKey"
})
public class UserINIntegrationGetResponse
    extends OCIDataResponse
{

    protected Integer originatingServiceKey;
    protected Integer terminatingServiceKey;

    /**
     * Ruft den Wert der originatingServiceKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOriginatingServiceKey() {
        return originatingServiceKey;
    }

    /**
     * Legt den Wert der originatingServiceKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOriginatingServiceKey(Integer value) {
        this.originatingServiceKey = value;
    }

    /**
     * Ruft den Wert der terminatingServiceKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTerminatingServiceKey() {
        return terminatingServiceKey;
    }

    /**
     * Legt den Wert der terminatingServiceKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTerminatingServiceKey(Integer value) {
        this.terminatingServiceKey = value;
    }

}
