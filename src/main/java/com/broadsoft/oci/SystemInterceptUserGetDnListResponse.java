//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *           Response to SystemInterceptUserGetDnListRequest.
 *           The Intercept User DN List.
 *         
 * 
 * <p>Java-Klasse für SystemInterceptUserGetDnListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemInterceptUserGetDnListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="interceptUserList" type="{}InterceptDNListEntry" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemInterceptUserGetDnListResponse", propOrder = {
    "interceptUserList"
})
public class SystemInterceptUserGetDnListResponse
    extends OCIDataResponse
{

    protected List<InterceptDNListEntry> interceptUserList;

    /**
     * Gets the value of the interceptUserList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the interceptUserList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInterceptUserList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InterceptDNListEntry }
     * 
     * 
     * @return
     *     The value of the interceptUserList property.
     */
    public List<InterceptDNListEntry> getInterceptUserList() {
        if (interceptUserList == null) {
            interceptUserList = new ArrayList<>();
        }
        return this.interceptUserList;
    }

}
