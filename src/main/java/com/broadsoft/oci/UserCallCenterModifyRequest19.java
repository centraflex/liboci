//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 *  
 *         Modifies the agents call center settings and the availability for an agent in one or more Call Centers.
 *         Contains a list specifying the desired availability status of one or more call centers.
 *         A default unavailable code will be used if the parameter agentUnavailableCode is not active, included or is invalid.
 *         Changing the agentACDState from unavailable to any other state will automatically clear the unavailable code.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           agentACDState
 *           agentThresholdProfileName
 *           agentUnavailableCode
 *           useSystemDefaultUnavailableSettings
 *           forceAgentUnavailableOnDNDActivation
 *           forceAgentUnavailableOnPersonalCalls
 *           forceAgentUnavailableOnBouncedCallLimit
 *           numberConsecutiveBouncedCallsToForceAgentUnavailable
 *           forceAgentUnavailableOnNotReachable
 *           makeOutgoingCallsAsCallCenter
 *           outgoingCallDNIS
 *           useSystemDefaultWrapUpDestination
 *           wrapUpDestination
 *       
 * 
 * <p>Java-Klasse für UserCallCenterModifyRequest19 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallCenterModifyRequest19">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="agentACDState" type="{}AgentACDState" minOccurs="0"/>
 *         <element name="agentThresholdProfileName" type="{}CallCenterAgentThresholdProfileName" minOccurs="0"/>
 *         <element name="agentUnavailableCode" type="{}CallCenterAgentUnavailableCode" minOccurs="0"/>
 *         <element name="useDefaultGuardTimer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableGuardTimer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="guardTimerSeconds" type="{}CallCenterGuardTimerSeconds" minOccurs="0"/>
 *         <element name="useSystemDefaultUnavailableSettings" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forceAgentUnavailableOnDNDActivation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forceAgentUnavailableOnPersonalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forceAgentUnavailableOnBouncedCallLimit" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="numberConsecutiveBouncedCallsToForceAgentUnavailable" type="{}CallCenterConsecutiveBouncedCallsToForceAgentUnavailable" minOccurs="0"/>
 *         <element name="forceAgentUnavailableOnNotReachable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="makeOutgoingCallsAsCallCenter" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="outgoingCallDNIS" type="{}DNISKey" minOccurs="0"/>
 *         <element name="callCenterAgentSettings" type="{}CallCenterAgentSettings" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="useSystemDefaultWrapUpDestination" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="wrapUpDestination" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallCenterModifyRequest19", propOrder = {
    "userId",
    "agentACDState",
    "agentThresholdProfileName",
    "agentUnavailableCode",
    "useDefaultGuardTimer",
    "enableGuardTimer",
    "guardTimerSeconds",
    "useSystemDefaultUnavailableSettings",
    "forceAgentUnavailableOnDNDActivation",
    "forceAgentUnavailableOnPersonalCalls",
    "forceAgentUnavailableOnBouncedCallLimit",
    "numberConsecutiveBouncedCallsToForceAgentUnavailable",
    "forceAgentUnavailableOnNotReachable",
    "makeOutgoingCallsAsCallCenter",
    "outgoingCallDNIS",
    "callCenterAgentSettings",
    "useSystemDefaultWrapUpDestination",
    "wrapUpDestination"
})
public class UserCallCenterModifyRequest19
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlSchemaType(name = "token")
    protected AgentACDState agentACDState;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String agentThresholdProfileName;
    @XmlElementRef(name = "agentUnavailableCode", type = JAXBElement.class, required = false)
    protected JAXBElement<String> agentUnavailableCode;
    protected Boolean useDefaultGuardTimer;
    protected Boolean enableGuardTimer;
    protected Integer guardTimerSeconds;
    protected Boolean useSystemDefaultUnavailableSettings;
    protected Boolean forceAgentUnavailableOnDNDActivation;
    protected Boolean forceAgentUnavailableOnPersonalCalls;
    protected Boolean forceAgentUnavailableOnBouncedCallLimit;
    protected Integer numberConsecutiveBouncedCallsToForceAgentUnavailable;
    protected Boolean forceAgentUnavailableOnNotReachable;
    protected Boolean makeOutgoingCallsAsCallCenter;
    @XmlElementRef(name = "outgoingCallDNIS", type = JAXBElement.class, required = false)
    protected JAXBElement<DNISKey> outgoingCallDNIS;
    protected List<CallCenterAgentSettings> callCenterAgentSettings;
    protected Boolean useSystemDefaultWrapUpDestination;
    @XmlElementRef(name = "wrapUpDestination", type = JAXBElement.class, required = false)
    protected JAXBElement<String> wrapUpDestination;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der agentACDState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AgentACDState }
     *     
     */
    public AgentACDState getAgentACDState() {
        return agentACDState;
    }

    /**
     * Legt den Wert der agentACDState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentACDState }
     *     
     */
    public void setAgentACDState(AgentACDState value) {
        this.agentACDState = value;
    }

    /**
     * Ruft den Wert der agentThresholdProfileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentThresholdProfileName() {
        return agentThresholdProfileName;
    }

    /**
     * Legt den Wert der agentThresholdProfileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentThresholdProfileName(String value) {
        this.agentThresholdProfileName = value;
    }

    /**
     * Ruft den Wert der agentUnavailableCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAgentUnavailableCode() {
        return agentUnavailableCode;
    }

    /**
     * Legt den Wert der agentUnavailableCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAgentUnavailableCode(JAXBElement<String> value) {
        this.agentUnavailableCode = value;
    }

    /**
     * Ruft den Wert der useDefaultGuardTimer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseDefaultGuardTimer() {
        return useDefaultGuardTimer;
    }

    /**
     * Legt den Wert der useDefaultGuardTimer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseDefaultGuardTimer(Boolean value) {
        this.useDefaultGuardTimer = value;
    }

    /**
     * Ruft den Wert der enableGuardTimer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableGuardTimer() {
        return enableGuardTimer;
    }

    /**
     * Legt den Wert der enableGuardTimer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableGuardTimer(Boolean value) {
        this.enableGuardTimer = value;
    }

    /**
     * Ruft den Wert der guardTimerSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGuardTimerSeconds() {
        return guardTimerSeconds;
    }

    /**
     * Legt den Wert der guardTimerSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGuardTimerSeconds(Integer value) {
        this.guardTimerSeconds = value;
    }

    /**
     * Ruft den Wert der useSystemDefaultUnavailableSettings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseSystemDefaultUnavailableSettings() {
        return useSystemDefaultUnavailableSettings;
    }

    /**
     * Legt den Wert der useSystemDefaultUnavailableSettings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSystemDefaultUnavailableSettings(Boolean value) {
        this.useSystemDefaultUnavailableSettings = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnDNDActivation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceAgentUnavailableOnDNDActivation() {
        return forceAgentUnavailableOnDNDActivation;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnDNDActivation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceAgentUnavailableOnDNDActivation(Boolean value) {
        this.forceAgentUnavailableOnDNDActivation = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnPersonalCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceAgentUnavailableOnPersonalCalls() {
        return forceAgentUnavailableOnPersonalCalls;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnPersonalCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceAgentUnavailableOnPersonalCalls(Boolean value) {
        this.forceAgentUnavailableOnPersonalCalls = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnBouncedCallLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceAgentUnavailableOnBouncedCallLimit() {
        return forceAgentUnavailableOnBouncedCallLimit;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnBouncedCallLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceAgentUnavailableOnBouncedCallLimit(Boolean value) {
        this.forceAgentUnavailableOnBouncedCallLimit = value;
    }

    /**
     * Ruft den Wert der numberConsecutiveBouncedCallsToForceAgentUnavailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberConsecutiveBouncedCallsToForceAgentUnavailable() {
        return numberConsecutiveBouncedCallsToForceAgentUnavailable;
    }

    /**
     * Legt den Wert der numberConsecutiveBouncedCallsToForceAgentUnavailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberConsecutiveBouncedCallsToForceAgentUnavailable(Integer value) {
        this.numberConsecutiveBouncedCallsToForceAgentUnavailable = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnNotReachable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceAgentUnavailableOnNotReachable() {
        return forceAgentUnavailableOnNotReachable;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnNotReachable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceAgentUnavailableOnNotReachable(Boolean value) {
        this.forceAgentUnavailableOnNotReachable = value;
    }

    /**
     * Ruft den Wert der makeOutgoingCallsAsCallCenter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMakeOutgoingCallsAsCallCenter() {
        return makeOutgoingCallsAsCallCenter;
    }

    /**
     * Legt den Wert der makeOutgoingCallsAsCallCenter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMakeOutgoingCallsAsCallCenter(Boolean value) {
        this.makeOutgoingCallsAsCallCenter = value;
    }

    /**
     * Ruft den Wert der outgoingCallDNIS-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DNISKey }{@code >}
     *     
     */
    public JAXBElement<DNISKey> getOutgoingCallDNIS() {
        return outgoingCallDNIS;
    }

    /**
     * Legt den Wert der outgoingCallDNIS-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DNISKey }{@code >}
     *     
     */
    public void setOutgoingCallDNIS(JAXBElement<DNISKey> value) {
        this.outgoingCallDNIS = value;
    }

    /**
     * Gets the value of the callCenterAgentSettings property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the callCenterAgentSettings property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCallCenterAgentSettings().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallCenterAgentSettings }
     * 
     * 
     * @return
     *     The value of the callCenterAgentSettings property.
     */
    public List<CallCenterAgentSettings> getCallCenterAgentSettings() {
        if (callCenterAgentSettings == null) {
            callCenterAgentSettings = new ArrayList<>();
        }
        return this.callCenterAgentSettings;
    }

    /**
     * Ruft den Wert der useSystemDefaultWrapUpDestination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseSystemDefaultWrapUpDestination() {
        return useSystemDefaultWrapUpDestination;
    }

    /**
     * Legt den Wert der useSystemDefaultWrapUpDestination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSystemDefaultWrapUpDestination(Boolean value) {
        this.useSystemDefaultWrapUpDestination = value;
    }

    /**
     * Ruft den Wert der wrapUpDestination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWrapUpDestination() {
        return wrapUpDestination;
    }

    /**
     * Legt den Wert der wrapUpDestination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWrapUpDestination(JAXBElement<String> value) {
        this.wrapUpDestination = value;
    }

}
