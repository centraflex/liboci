//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ConnectedLineIdentificationPrivacyOnRedirectedCalls.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ConnectedLineIdentificationPrivacyOnRedirectedCalls">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="No Privacy"/>
 *     <enumeration value="Privacy For External Calls"/>
 *     <enumeration value="Privacy For All Calls"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ConnectedLineIdentificationPrivacyOnRedirectedCalls")
@XmlEnum
public enum ConnectedLineIdentificationPrivacyOnRedirectedCalls {

    @XmlEnumValue("No Privacy")
    NO_PRIVACY("No Privacy"),
    @XmlEnumValue("Privacy For External Calls")
    PRIVACY_FOR_EXTERNAL_CALLS("Privacy For External Calls"),
    @XmlEnumValue("Privacy For All Calls")
    PRIVACY_FOR_ALL_CALLS("Privacy For All Calls");
    private final String value;

    ConnectedLineIdentificationPrivacyOnRedirectedCalls(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ConnectedLineIdentificationPrivacyOnRedirectedCalls fromValue(String v) {
        for (ConnectedLineIdentificationPrivacyOnRedirectedCalls c: ConnectedLineIdentificationPrivacyOnRedirectedCalls.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
