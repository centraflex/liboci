//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AuthenticationLockoutWaitAlgorithmType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AuthenticationLockoutWaitAlgorithmType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Double"/>
 *     <enumeration value="Fixed"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AuthenticationLockoutWaitAlgorithmType")
@XmlEnum
public enum AuthenticationLockoutWaitAlgorithmType {

    @XmlEnumValue("Double")
    DOUBLE("Double"),
    @XmlEnumValue("Fixed")
    FIXED("Fixed");
    private final String value;

    AuthenticationLockoutWaitAlgorithmType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AuthenticationLockoutWaitAlgorithmType fromValue(String v) {
        for (AuthenticationLockoutWaitAlgorithmType c: AuthenticationLockoutWaitAlgorithmType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
