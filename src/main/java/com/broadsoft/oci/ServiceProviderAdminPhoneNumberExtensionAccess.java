//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ServiceProviderAdminPhoneNumberExtensionAccess.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ServiceProviderAdminPhoneNumberExtensionAccess">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Full"/>
 *     <enumeration value="Assign To Services and Users"/>
 *     <enumeration value="Read-Only"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ServiceProviderAdminPhoneNumberExtensionAccess")
@XmlEnum
public enum ServiceProviderAdminPhoneNumberExtensionAccess {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("Assign To Services and Users")
    ASSIGN_TO_SERVICES_AND_USERS("Assign To Services and Users"),
    @XmlEnumValue("Read-Only")
    READ_ONLY("Read-Only");
    private final String value;

    ServiceProviderAdminPhoneNumberExtensionAccess(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ServiceProviderAdminPhoneNumberExtensionAccess fromValue(String v) {
        for (ServiceProviderAdminPhoneNumberExtensionAccess c: ServiceProviderAdminPhoneNumberExtensionAccess.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
