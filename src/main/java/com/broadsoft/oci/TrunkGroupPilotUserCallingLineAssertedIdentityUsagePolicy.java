//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="All Originating Calls"/>
 *     <enumeration value="Unscreened Originating Calls"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy")
@XmlEnum
public enum TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy {

    @XmlEnumValue("All Originating Calls")
    ALL_ORIGINATING_CALLS("All Originating Calls"),
    @XmlEnumValue("Unscreened Originating Calls")
    UNSCREENED_ORIGINATING_CALLS("Unscreened Originating Calls");
    private final String value;

    TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy fromValue(String v) {
        for (TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy c: TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
