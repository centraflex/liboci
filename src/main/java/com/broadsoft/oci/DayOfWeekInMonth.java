//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DayOfWeekInMonth.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DayOfWeekInMonth">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="First"/>
 *     <enumeration value="Second"/>
 *     <enumeration value="Third"/>
 *     <enumeration value="Fourth"/>
 *     <enumeration value="Last"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DayOfWeekInMonth")
@XmlEnum
public enum DayOfWeekInMonth {

    @XmlEnumValue("First")
    FIRST("First"),
    @XmlEnumValue("Second")
    SECOND("Second"),
    @XmlEnumValue("Third")
    THIRD("Third"),
    @XmlEnumValue("Fourth")
    FOURTH("Fourth"),
    @XmlEnumValue("Last")
    LAST("Last");
    private final String value;

    DayOfWeekInMonth(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DayOfWeekInMonth fromValue(String v) {
        for (DayOfWeekInMonth c: DayOfWeekInMonth.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
