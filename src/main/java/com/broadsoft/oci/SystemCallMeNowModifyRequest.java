//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with Call me now service.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCallMeNowModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallMeNowModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="passcodeLength" type="{}CallMeNowPasscodeLength" minOccurs="0"/>
 *         <element name="passcodeTimeoutSeconds" type="{}CallMeNowPasscodeTimeoutSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallMeNowModifyRequest", propOrder = {
    "passcodeLength",
    "passcodeTimeoutSeconds"
})
public class SystemCallMeNowModifyRequest
    extends OCIRequest
{

    protected Integer passcodeLength;
    protected Integer passcodeTimeoutSeconds;

    /**
     * Ruft den Wert der passcodeLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPasscodeLength() {
        return passcodeLength;
    }

    /**
     * Legt den Wert der passcodeLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPasscodeLength(Integer value) {
        this.passcodeLength = value;
    }

    /**
     * Ruft den Wert der passcodeTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPasscodeTimeoutSeconds() {
        return passcodeTimeoutSeconds;
    }

    /**
     * Legt den Wert der passcodeTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPasscodeTimeoutSeconds(Integer value) {
        this.passcodeTimeoutSeconds = value;
    }

}
