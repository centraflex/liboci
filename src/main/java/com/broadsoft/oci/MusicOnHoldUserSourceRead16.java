//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Contains the music on hold user source configuration.
 *       
 * 
 * <p>Java-Klasse für MusicOnHoldUserSourceRead16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="MusicOnHoldUserSourceRead16">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="messageSourceSelection" type="{}MusicOnHoldUserMessageSelection"/>
 *         <element name="customSource" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="audioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *                   <element name="audioMediaType" type="{}MediaFileType" minOccurs="0"/>
 *                   <element name="videoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *                   <element name="videoMediaType" type="{}MediaFileType" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MusicOnHoldUserSourceRead16", propOrder = {
    "messageSourceSelection",
    "customSource"
})
public class MusicOnHoldUserSourceRead16 {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MusicOnHoldUserMessageSelection messageSourceSelection;
    protected MusicOnHoldUserSourceRead16 .CustomSource customSource;

    /**
     * Ruft den Wert der messageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldUserMessageSelection }
     *     
     */
    public MusicOnHoldUserMessageSelection getMessageSourceSelection() {
        return messageSourceSelection;
    }

    /**
     * Legt den Wert der messageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldUserMessageSelection }
     *     
     */
    public void setMessageSourceSelection(MusicOnHoldUserMessageSelection value) {
        this.messageSourceSelection = value;
    }

    /**
     * Ruft den Wert der customSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldUserSourceRead16 .CustomSource }
     *     
     */
    public MusicOnHoldUserSourceRead16 .CustomSource getCustomSource() {
        return customSource;
    }

    /**
     * Legt den Wert der customSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldUserSourceRead16 .CustomSource }
     *     
     */
    public void setCustomSource(MusicOnHoldUserSourceRead16 .CustomSource value) {
        this.customSource = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="audioFileDescription" type="{}FileDescription" minOccurs="0"/>
     *         <element name="audioMediaType" type="{}MediaFileType" minOccurs="0"/>
     *         <element name="videoFileDescription" type="{}FileDescription" minOccurs="0"/>
     *         <element name="videoMediaType" type="{}MediaFileType" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "audioFileDescription",
        "audioMediaType",
        "videoFileDescription",
        "videoMediaType"
    })
    public static class CustomSource {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String audioFileDescription;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String audioMediaType;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String videoFileDescription;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String videoMediaType;

        /**
         * Ruft den Wert der audioFileDescription-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAudioFileDescription() {
            return audioFileDescription;
        }

        /**
         * Legt den Wert der audioFileDescription-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAudioFileDescription(String value) {
            this.audioFileDescription = value;
        }

        /**
         * Ruft den Wert der audioMediaType-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAudioMediaType() {
            return audioMediaType;
        }

        /**
         * Legt den Wert der audioMediaType-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAudioMediaType(String value) {
            this.audioMediaType = value;
        }

        /**
         * Ruft den Wert der videoFileDescription-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVideoFileDescription() {
            return videoFileDescription;
        }

        /**
         * Legt den Wert der videoFileDescription-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVideoFileDescription(String value) {
            this.videoFileDescription = value;
        }

        /**
         * Ruft den Wert der videoMediaType-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVideoMediaType() {
            return videoMediaType;
        }

        /**
         * Legt den Wert der videoMediaType-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVideoMediaType(String value) {
            this.videoMediaType = value;
        }

    }

}
