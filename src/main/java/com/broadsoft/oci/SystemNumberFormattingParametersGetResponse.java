//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemNumberFormattingParametersGetRequest.
 *         Contains the system number formatting parameter.
 *       
 * 
 * <p>Java-Klasse für SystemNumberFormattingParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNumberFormattingParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="applyFormattingToE164Numbers" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNumberFormattingParametersGetResponse", propOrder = {
    "applyFormattingToE164Numbers"
})
public class SystemNumberFormattingParametersGetResponse
    extends OCIDataResponse
{

    protected boolean applyFormattingToE164Numbers;

    /**
     * Ruft den Wert der applyFormattingToE164Numbers-Eigenschaft ab.
     * 
     */
    public boolean isApplyFormattingToE164Numbers() {
        return applyFormattingToE164Numbers;
    }

    /**
     * Legt den Wert der applyFormattingToE164Numbers-Eigenschaft fest.
     * 
     */
    public void setApplyFormattingToE164Numbers(boolean value) {
        this.applyFormattingToE164Numbers = value;
    }

}
