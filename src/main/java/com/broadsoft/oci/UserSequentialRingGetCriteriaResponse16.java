//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserSequentialRingGetCriteriaRequest16.
 *       
 * 
 * <p>Java-Klasse für UserSequentialRingGetCriteriaResponse16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserSequentialRingGetCriteriaResponse16">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="timeSchedule" type="{}TimeSchedule" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}HolidaySchedule" minOccurs="0"/>
 *         <element name="blacklisted" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="fromDnCriteria" type="{}CriteriaFromDn"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserSequentialRingGetCriteriaResponse16", propOrder = {
    "timeSchedule",
    "holidaySchedule",
    "blacklisted",
    "fromDnCriteria"
})
public class UserSequentialRingGetCriteriaResponse16
    extends OCIDataResponse
{

    protected TimeSchedule timeSchedule;
    protected HolidaySchedule holidaySchedule;
    protected boolean blacklisted;
    @XmlElement(required = true)
    protected CriteriaFromDn fromDnCriteria;

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeSchedule }
     *     
     */
    public TimeSchedule getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeSchedule }
     *     
     */
    public void setTimeSchedule(TimeSchedule value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HolidaySchedule }
     *     
     */
    public HolidaySchedule getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HolidaySchedule }
     *     
     */
    public void setHolidaySchedule(HolidaySchedule value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der blacklisted-Eigenschaft ab.
     * 
     */
    public boolean isBlacklisted() {
        return blacklisted;
    }

    /**
     * Legt den Wert der blacklisted-Eigenschaft fest.
     * 
     */
    public void setBlacklisted(boolean value) {
        this.blacklisted = value;
    }

    /**
     * Ruft den Wert der fromDnCriteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaFromDn }
     *     
     */
    public CriteriaFromDn getFromDnCriteria() {
        return fromDnCriteria;
    }

    /**
     * Legt den Wert der fromDnCriteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaFromDn }
     *     
     */
    public void setFromDnCriteria(CriteriaFromDn value) {
        this.fromDnCriteria = value;
    }

}
