//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupDigitCollectionGetRequest13mp4.
 *       
 * 
 * <p>Java-Klasse für GroupDigitCollectionGetResponse13mp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDigitCollectionGetResponse13mp4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useSetting" type="{}GroupDigitCollectionSettingLevel"/>
 *         <element name="accessCode" type="{}AccessCode" minOccurs="0"/>
 *         <element name="publicDigitMap" type="{}DigitMap" minOccurs="0"/>
 *         <element name="privateDigitMap" type="{}DigitMap" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDigitCollectionGetResponse13mp4", propOrder = {
    "useSetting",
    "accessCode",
    "publicDigitMap",
    "privateDigitMap"
})
public class GroupDigitCollectionGetResponse13Mp4
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupDigitCollectionSettingLevel useSetting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accessCode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String publicDigitMap;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String privateDigitMap;

    /**
     * Ruft den Wert der useSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDigitCollectionSettingLevel }
     *     
     */
    public GroupDigitCollectionSettingLevel getUseSetting() {
        return useSetting;
    }

    /**
     * Legt den Wert der useSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDigitCollectionSettingLevel }
     *     
     */
    public void setUseSetting(GroupDigitCollectionSettingLevel value) {
        this.useSetting = value;
    }

    /**
     * Ruft den Wert der accessCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessCode() {
        return accessCode;
    }

    /**
     * Legt den Wert der accessCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessCode(String value) {
        this.accessCode = value;
    }

    /**
     * Ruft den Wert der publicDigitMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPublicDigitMap() {
        return publicDigitMap;
    }

    /**
     * Legt den Wert der publicDigitMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPublicDigitMap(String value) {
        this.publicDigitMap = value;
    }

    /**
     * Ruft den Wert der privateDigitMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrivateDigitMap() {
        return privateDigitMap;
    }

    /**
     * Legt den Wert der privateDigitMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrivateDigitMap(String value) {
        this.privateDigitMap = value;
    }

}
