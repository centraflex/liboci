//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The voice portal greeting only forwarding destination menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für GreetingOnlyForwardingDestinationMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GreetingOnlyForwardingDestinationMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="greetingOnlyForwardingDestination" type="{}DigitStarPound" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GreetingOnlyForwardingDestinationMenuKeysModifyEntry", propOrder = {
    "greetingOnlyForwardingDestination"
})
public class GreetingOnlyForwardingDestinationMenuKeysModifyEntry {

    @XmlElementRef(name = "greetingOnlyForwardingDestination", type = JAXBElement.class, required = false)
    protected JAXBElement<String> greetingOnlyForwardingDestination;

    /**
     * Ruft den Wert der greetingOnlyForwardingDestination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGreetingOnlyForwardingDestination() {
        return greetingOnlyForwardingDestination;
    }

    /**
     * Legt den Wert der greetingOnlyForwardingDestination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGreetingOnlyForwardingDestination(JAXBElement<String> value) {
        this.greetingOnlyForwardingDestination = value;
    }

}
