//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemOutgoingCallingPlanCallTypeGetListRequest.
 *       
 * 
 * <p>Java-Klasse für SystemOutgoingCallingPlanCallTypeGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemOutgoingCallingPlanCallTypeGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="callType" type="{}OutgoingCallingPlanCallType" maxOccurs="11" minOccurs="11"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemOutgoingCallingPlanCallTypeGetListResponse", propOrder = {
    "callType"
})
public class SystemOutgoingCallingPlanCallTypeGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected List<OutgoingCallingPlanCallType> callType;

    /**
     * Gets the value of the callType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the callType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCallType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OutgoingCallingPlanCallType }
     * 
     * 
     * @return
     *     The value of the callType property.
     */
    public List<OutgoingCallingPlanCallType> getCallType() {
        if (callType == null) {
            callType = new ArrayList<>();
        }
        return this.callType;
    }

}
