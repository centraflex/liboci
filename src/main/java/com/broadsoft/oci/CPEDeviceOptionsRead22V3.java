//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         CPE device's options.
 *       
 * 
 * <p>Java-Klasse für CPEDeviceOptionsRead22V3 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CPEDeviceOptionsRead22V3">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="enableMonitoring" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="configType" type="{}AccessDeviceEnhancedConfigurationType14" minOccurs="0"/>
 *         <element name="systemFileName" type="{}CPESystemFileName" minOccurs="0"/>
 *         <element name="deviceFileFormat" type="{}CPEDeviceFileFormat" minOccurs="0"/>
 *         <element name="deviceManagementDeviceTypeOptions" type="{}DeviceManagementDeviceTypeOptionsRead22V3" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CPEDeviceOptionsRead22V3", propOrder = {
    "enableMonitoring",
    "configType",
    "systemFileName",
    "deviceFileFormat",
    "deviceManagementDeviceTypeOptions"
})
public class CPEDeviceOptionsRead22V3 {

    protected boolean enableMonitoring;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String configType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String systemFileName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceFileFormat;
    protected DeviceManagementDeviceTypeOptionsRead22V3 deviceManagementDeviceTypeOptions;

    /**
     * Ruft den Wert der enableMonitoring-Eigenschaft ab.
     * 
     */
    public boolean isEnableMonitoring() {
        return enableMonitoring;
    }

    /**
     * Legt den Wert der enableMonitoring-Eigenschaft fest.
     * 
     */
    public void setEnableMonitoring(boolean value) {
        this.enableMonitoring = value;
    }

    /**
     * Ruft den Wert der configType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfigType() {
        return configType;
    }

    /**
     * Legt den Wert der configType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfigType(String value) {
        this.configType = value;
    }

    /**
     * Ruft den Wert der systemFileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemFileName() {
        return systemFileName;
    }

    /**
     * Legt den Wert der systemFileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemFileName(String value) {
        this.systemFileName = value;
    }

    /**
     * Ruft den Wert der deviceFileFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceFileFormat() {
        return deviceFileFormat;
    }

    /**
     * Legt den Wert der deviceFileFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceFileFormat(String value) {
        this.deviceFileFormat = value;
    }

    /**
     * Ruft den Wert der deviceManagementDeviceTypeOptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementDeviceTypeOptionsRead22V3 }
     *     
     */
    public DeviceManagementDeviceTypeOptionsRead22V3 getDeviceManagementDeviceTypeOptions() {
        return deviceManagementDeviceTypeOptions;
    }

    /**
     * Legt den Wert der deviceManagementDeviceTypeOptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementDeviceTypeOptionsRead22V3 }
     *     
     */
    public void setDeviceManagementDeviceTypeOptions(DeviceManagementDeviceTypeOptionsRead22V3 value) {
        this.deviceManagementDeviceTypeOptions = value;
    }

}
