//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für OCIReportingLoginType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="OCIReportingLoginType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="User"/>
 *     <enumeration value="Group Department"/>
 *     <enumeration value="Group"/>
 *     <enumeration value="Service Provider"/>
 *     <enumeration value="Reseller"/>
 *     <enumeration value="Provisioning"/>
 *     <enumeration value="System"/>
 *     <enumeration value="Not Logged In"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "OCIReportingLoginType")
@XmlEnum
public enum OCIReportingLoginType {

    @XmlEnumValue("User")
    USER("User"),
    @XmlEnumValue("Group Department")
    GROUP_DEPARTMENT("Group Department"),
    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("Service Provider")
    SERVICE_PROVIDER("Service Provider"),
    @XmlEnumValue("Reseller")
    RESELLER("Reseller"),
    @XmlEnumValue("Provisioning")
    PROVISIONING("Provisioning"),
    @XmlEnumValue("System")
    SYSTEM("System"),
    @XmlEnumValue("Not Logged In")
    NOT_LOGGED_IN("Not Logged In");
    private final String value;

    OCIReportingLoginType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OCIReportingLoginType fromValue(String v) {
        for (OCIReportingLoginType c: OCIReportingLoginType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
