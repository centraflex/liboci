//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a department of an enterprise.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für EnterpriseDepartmentModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseDepartmentModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enterpriseId" type="{}ServiceProviderId"/>
 *         <element name="departmentName" type="{}DepartmentName"/>
 *         <element name="newDepartmentName" type="{}DepartmentName" minOccurs="0"/>
 *         <element name="newParentDepartmentKey" type="{}EnterpriseDepartmentKey" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseDepartmentModifyRequest", propOrder = {
    "enterpriseId",
    "departmentName",
    "newDepartmentName",
    "newParentDepartmentKey"
})
public class EnterpriseDepartmentModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String enterpriseId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String departmentName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newDepartmentName;
    @XmlElementRef(name = "newParentDepartmentKey", type = JAXBElement.class, required = false)
    protected JAXBElement<EnterpriseDepartmentKey> newParentDepartmentKey;

    /**
     * Ruft den Wert der enterpriseId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnterpriseId() {
        return enterpriseId;
    }

    /**
     * Legt den Wert der enterpriseId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnterpriseId(String value) {
        this.enterpriseId = value;
    }

    /**
     * Ruft den Wert der departmentName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Legt den Wert der departmentName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartmentName(String value) {
        this.departmentName = value;
    }

    /**
     * Ruft den Wert der newDepartmentName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewDepartmentName() {
        return newDepartmentName;
    }

    /**
     * Legt den Wert der newDepartmentName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewDepartmentName(String value) {
        this.newDepartmentName = value;
    }

    /**
     * Ruft den Wert der newParentDepartmentKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link EnterpriseDepartmentKey }{@code >}
     *     
     */
    public JAXBElement<EnterpriseDepartmentKey> getNewParentDepartmentKey() {
        return newParentDepartmentKey;
    }

    /**
     * Legt den Wert der newParentDepartmentKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link EnterpriseDepartmentKey }{@code >}
     *     
     */
    public void setNewParentDepartmentKey(JAXBElement<EnterpriseDepartmentKey> value) {
        this.newParentDepartmentKey = value;
    }

}
