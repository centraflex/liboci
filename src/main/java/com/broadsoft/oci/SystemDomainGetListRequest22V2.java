//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Requests the list of all matching system-level domains and all matching reseller level domains. 
 *         If excludeReseller is specified, returns all matching system-level domain names only. 
 *         If resellerId is specified, returns all matching system-level domain names and the given reseller's domains. 
 *         If reseller administrator sends the request and resellerId is not specified, the administrator's resellerId is used.
 *         
 *         The response is either SystemDomainGetListResponse22V2 or ErrorResponse.
 *         
 *         The following elements are only used in AS data mode and will be ingored in XS data mode:
 *           includeReseller
 *           resellerId
 *           responseSizeLimit
 *           searchCriteriaDomainName
 *           searchCriteriaResellerId
 *           searchCriteriaExactDomainLevel
 *       
 * 
 * <p>Java-Klasse für SystemDomainGetListRequest22V2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDomainGetListRequest22V2">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <choice>
 *           <element name="excludeReseller" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *           <element name="resellerId" type="{}ResellerId22" minOccurs="0"/>
 *         </choice>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaDomainName" type="{}SearchCriteriaDomainName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaResellerId" type="{}SearchCriteriaResellerId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactDomainLevel" type="{}SearchCriteriaExactDomainLevel" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDomainGetListRequest22V2", propOrder = {
    "excludeReseller",
    "resellerId",
    "responseSizeLimit",
    "searchCriteriaDomainName",
    "searchCriteriaResellerId",
    "searchCriteriaExactDomainLevel"
})
public class SystemDomainGetListRequest22V2
    extends OCIRequest
{

    protected Boolean excludeReseller;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    protected Integer responseSizeLimit;
    protected List<SearchCriteriaDomainName> searchCriteriaDomainName;
    protected List<SearchCriteriaResellerId> searchCriteriaResellerId;
    protected SearchCriteriaExactDomainLevel searchCriteriaExactDomainLevel;

    /**
     * Ruft den Wert der excludeReseller-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExcludeReseller() {
        return excludeReseller;
    }

    /**
     * Legt den Wert der excludeReseller-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludeReseller(Boolean value) {
        this.excludeReseller = value;
    }

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaDomainName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDomainName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDomainName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDomainName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDomainName property.
     */
    public List<SearchCriteriaDomainName> getSearchCriteriaDomainName() {
        if (searchCriteriaDomainName == null) {
            searchCriteriaDomainName = new ArrayList<>();
        }
        return this.searchCriteriaDomainName;
    }

    /**
     * Gets the value of the searchCriteriaResellerId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaResellerId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaResellerId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaResellerId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaResellerId property.
     */
    public List<SearchCriteriaResellerId> getSearchCriteriaResellerId() {
        if (searchCriteriaResellerId == null) {
            searchCriteriaResellerId = new ArrayList<>();
        }
        return this.searchCriteriaResellerId;
    }

    /**
     * Ruft den Wert der searchCriteriaExactDomainLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactDomainLevel }
     *     
     */
    public SearchCriteriaExactDomainLevel getSearchCriteriaExactDomainLevel() {
        return searchCriteriaExactDomainLevel;
    }

    /**
     * Legt den Wert der searchCriteriaExactDomainLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactDomainLevel }
     *     
     */
    public void setSearchCriteriaExactDomainLevel(SearchCriteriaExactDomainLevel value) {
        this.searchCriteriaExactDomainLevel = value;
    }

}
