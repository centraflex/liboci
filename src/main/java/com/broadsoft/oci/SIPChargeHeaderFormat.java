//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SIPChargeHeaderFormat.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SIPChargeHeaderFormat">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="ChargeHeaderSip"/>
 *     <enumeration value="ChargeHeaderTel"/>
 *     <enumeration value="PChargeInfoSip"/>
 *     <enumeration value="PChargeInfoTel"/>
 *     <enumeration value="PAITelURI"/>
 *     <enumeration value="DiversionSip"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SIPChargeHeaderFormat")
@XmlEnum
public enum SIPChargeHeaderFormat {

    @XmlEnumValue("ChargeHeaderSip")
    CHARGE_HEADER_SIP("ChargeHeaderSip"),
    @XmlEnumValue("ChargeHeaderTel")
    CHARGE_HEADER_TEL("ChargeHeaderTel"),
    @XmlEnumValue("PChargeInfoSip")
    P_CHARGE_INFO_SIP("PChargeInfoSip"),
    @XmlEnumValue("PChargeInfoTel")
    P_CHARGE_INFO_TEL("PChargeInfoTel"),
    @XmlEnumValue("PAITelURI")
    PAI_TEL_URI("PAITelURI"),
    @XmlEnumValue("DiversionSip")
    DIVERSION_SIP("DiversionSip");
    private final String value;

    SIPChargeHeaderFormat(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SIPChargeHeaderFormat fromValue(String v) {
        for (SIPChargeHeaderFormat c: SIPChargeHeaderFormat.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
