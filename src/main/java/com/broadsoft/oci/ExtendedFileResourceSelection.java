//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ExtendedFileResourceSelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ExtendedFileResourceSelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="File"/>
 *     <enumeration value="URL"/>
 *     <enumeration value="Default"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ExtendedFileResourceSelection")
@XmlEnum
public enum ExtendedFileResourceSelection {

    @XmlEnumValue("File")
    FILE("File"),
    URL("URL"),
    @XmlEnumValue("Default")
    DEFAULT("Default");
    private final String value;

    ExtendedFileResourceSelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExtendedFileResourceSelection fromValue(String v) {
        for (ExtendedFileResourceSelection c: ExtendedFileResourceSelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
