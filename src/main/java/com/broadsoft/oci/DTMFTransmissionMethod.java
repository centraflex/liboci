//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DTMFTransmissionMethod.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DTMFTransmissionMethod">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Signaling"/>
 *     <enumeration value="RTP"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DTMFTransmissionMethod")
@XmlEnum
public enum DTMFTransmissionMethod {

    @XmlEnumValue("Signaling")
    SIGNALING("Signaling"),
    RTP("RTP");
    private final String value;

    DTMFTransmissionMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DTMFTransmissionMethod fromValue(String v) {
        for (DTMFTransmissionMethod c: DTMFTransmissionMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
