//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the ServiceProviderMaliciousCallTraceGetRequest.
 *         The response contains the service provider Malicious Call Trace 
 *         settings. 
 *       
 * 
 * <p>Java-Klasse für ServiceProviderMaliciousCallTraceGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderMaliciousCallTraceGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useSystemPlayMCTWarningAnnouncement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="playMCTWarningAnnouncement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderMaliciousCallTraceGetResponse", propOrder = {
    "useSystemPlayMCTWarningAnnouncement",
    "playMCTWarningAnnouncement"
})
public class ServiceProviderMaliciousCallTraceGetResponse
    extends OCIDataResponse
{

    protected boolean useSystemPlayMCTWarningAnnouncement;
    protected boolean playMCTWarningAnnouncement;

    /**
     * Ruft den Wert der useSystemPlayMCTWarningAnnouncement-Eigenschaft ab.
     * 
     */
    public boolean isUseSystemPlayMCTWarningAnnouncement() {
        return useSystemPlayMCTWarningAnnouncement;
    }

    /**
     * Legt den Wert der useSystemPlayMCTWarningAnnouncement-Eigenschaft fest.
     * 
     */
    public void setUseSystemPlayMCTWarningAnnouncement(boolean value) {
        this.useSystemPlayMCTWarningAnnouncement = value;
    }

    /**
     * Ruft den Wert der playMCTWarningAnnouncement-Eigenschaft ab.
     * 
     */
    public boolean isPlayMCTWarningAnnouncement() {
        return playMCTWarningAnnouncement;
    }

    /**
     * Legt den Wert der playMCTWarningAnnouncement-Eigenschaft fest.
     * 
     */
    public void setPlayMCTWarningAnnouncement(boolean value) {
        this.playMCTWarningAnnouncement = value;
    }

}
