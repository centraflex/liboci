//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *           Request to add a sip device type file.
 *           The response is either SuccessResponse or ErrorResponse.
 *         
 * 
 * <p>Java-Klasse für SystemSIPDeviceTypeFileAddRequest14sp8 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPDeviceTypeFileAddRequest14sp8">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="deviceType" type="{}AccessDeviceType"/>
 *         <element name="fileFormat" type="{}DeviceManagementFileFormat"/>
 *         <element name="remoteFileFormat" type="{}DeviceManagementFileFormat"/>
 *         <element name="fileCategory" type="{}DeviceManagementFileCategory"/>
 *         <element name="allowFileCustomization" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="fileSource" type="{}DeviceTypeFileEnhancedConfigurationMode"/>
 *         <element name="uploadFile" type="{}FileResource" minOccurs="0"/>
 *         <element name="useHttpDigestAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macBasedFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="userNamePasswordFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macInNonRequestURI" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macFormatInNonRequestURI" type="{}DeviceManagementAccessURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPDeviceTypeFileAddRequest14sp8", propOrder = {
    "deviceType",
    "fileFormat",
    "remoteFileFormat",
    "fileCategory",
    "allowFileCustomization",
    "fileSource",
    "uploadFile",
    "useHttpDigestAuthentication",
    "macBasedFileAuthentication",
    "userNamePasswordFileAuthentication",
    "macInNonRequestURI",
    "macFormatInNonRequestURI"
})
public class SystemSIPDeviceTypeFileAddRequest14Sp8
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceType;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String fileFormat;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String remoteFileFormat;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceManagementFileCategory fileCategory;
    protected boolean allowFileCustomization;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceTypeFileEnhancedConfigurationMode fileSource;
    protected FileResource uploadFile;
    protected boolean useHttpDigestAuthentication;
    protected boolean macBasedFileAuthentication;
    protected boolean userNamePasswordFileAuthentication;
    protected boolean macInNonRequestURI;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String macFormatInNonRequestURI;

    /**
     * Ruft den Wert der deviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Legt den Wert der deviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceType(String value) {
        this.deviceType = value;
    }

    /**
     * Ruft den Wert der fileFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileFormat() {
        return fileFormat;
    }

    /**
     * Legt den Wert der fileFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileFormat(String value) {
        this.fileFormat = value;
    }

    /**
     * Ruft den Wert der remoteFileFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteFileFormat() {
        return remoteFileFormat;
    }

    /**
     * Legt den Wert der remoteFileFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteFileFormat(String value) {
        this.remoteFileFormat = value;
    }

    /**
     * Ruft den Wert der fileCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementFileCategory }
     *     
     */
    public DeviceManagementFileCategory getFileCategory() {
        return fileCategory;
    }

    /**
     * Legt den Wert der fileCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementFileCategory }
     *     
     */
    public void setFileCategory(DeviceManagementFileCategory value) {
        this.fileCategory = value;
    }

    /**
     * Ruft den Wert der allowFileCustomization-Eigenschaft ab.
     * 
     */
    public boolean isAllowFileCustomization() {
        return allowFileCustomization;
    }

    /**
     * Legt den Wert der allowFileCustomization-Eigenschaft fest.
     * 
     */
    public void setAllowFileCustomization(boolean value) {
        this.allowFileCustomization = value;
    }

    /**
     * Ruft den Wert der fileSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceTypeFileEnhancedConfigurationMode }
     *     
     */
    public DeviceTypeFileEnhancedConfigurationMode getFileSource() {
        return fileSource;
    }

    /**
     * Legt den Wert der fileSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceTypeFileEnhancedConfigurationMode }
     *     
     */
    public void setFileSource(DeviceTypeFileEnhancedConfigurationMode value) {
        this.fileSource = value;
    }

    /**
     * Ruft den Wert der uploadFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FileResource }
     *     
     */
    public FileResource getUploadFile() {
        return uploadFile;
    }

    /**
     * Legt den Wert der uploadFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FileResource }
     *     
     */
    public void setUploadFile(FileResource value) {
        this.uploadFile = value;
    }

    /**
     * Ruft den Wert der useHttpDigestAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isUseHttpDigestAuthentication() {
        return useHttpDigestAuthentication;
    }

    /**
     * Legt den Wert der useHttpDigestAuthentication-Eigenschaft fest.
     * 
     */
    public void setUseHttpDigestAuthentication(boolean value) {
        this.useHttpDigestAuthentication = value;
    }

    /**
     * Ruft den Wert der macBasedFileAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isMacBasedFileAuthentication() {
        return macBasedFileAuthentication;
    }

    /**
     * Legt den Wert der macBasedFileAuthentication-Eigenschaft fest.
     * 
     */
    public void setMacBasedFileAuthentication(boolean value) {
        this.macBasedFileAuthentication = value;
    }

    /**
     * Ruft den Wert der userNamePasswordFileAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isUserNamePasswordFileAuthentication() {
        return userNamePasswordFileAuthentication;
    }

    /**
     * Legt den Wert der userNamePasswordFileAuthentication-Eigenschaft fest.
     * 
     */
    public void setUserNamePasswordFileAuthentication(boolean value) {
        this.userNamePasswordFileAuthentication = value;
    }

    /**
     * Ruft den Wert der macInNonRequestURI-Eigenschaft ab.
     * 
     */
    public boolean isMacInNonRequestURI() {
        return macInNonRequestURI;
    }

    /**
     * Legt den Wert der macInNonRequestURI-Eigenschaft fest.
     * 
     */
    public void setMacInNonRequestURI(boolean value) {
        this.macInNonRequestURI = value;
    }

    /**
     * Ruft den Wert der macFormatInNonRequestURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacFormatInNonRequestURI() {
        return macFormatInNonRequestURI;
    }

    /**
     * Legt den Wert der macFormatInNonRequestURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacFormatInNonRequestURI(String value) {
        this.macFormatInNonRequestURI = value;
    }

}
