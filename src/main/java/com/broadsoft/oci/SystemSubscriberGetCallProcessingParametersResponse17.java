//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemSubscriberGetCallProcessingParametersRequest17.
 *       
 * 
 * <p>Java-Klasse für SystemSubscriberGetCallProcessingParametersResponse17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSubscriberGetCallProcessingParametersResponse17">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isExtendedCallingLineIdActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isRingTimeOutActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="ringTimeoutSeconds" type="{}SystemUserRingTimeoutSeconds"/>
 *         <element name="allowEmergencyRemoteOfficeOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxNoAnswerNumberOfRings" type="{}MaxNoAnswerNumberOfRings"/>
 *         <element name="incomingCallToUserAliasMode" type="{}IncomingCallToUserAliasMode"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSubscriberGetCallProcessingParametersResponse17", propOrder = {
    "isExtendedCallingLineIdActive",
    "isRingTimeOutActive",
    "ringTimeoutSeconds",
    "allowEmergencyRemoteOfficeOriginations",
    "maxNoAnswerNumberOfRings",
    "incomingCallToUserAliasMode"
})
public class SystemSubscriberGetCallProcessingParametersResponse17
    extends OCIDataResponse
{

    protected boolean isExtendedCallingLineIdActive;
    protected boolean isRingTimeOutActive;
    protected int ringTimeoutSeconds;
    protected boolean allowEmergencyRemoteOfficeOriginations;
    protected int maxNoAnswerNumberOfRings;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected IncomingCallToUserAliasMode incomingCallToUserAliasMode;

    /**
     * Ruft den Wert der isExtendedCallingLineIdActive-Eigenschaft ab.
     * 
     */
    public boolean isIsExtendedCallingLineIdActive() {
        return isExtendedCallingLineIdActive;
    }

    /**
     * Legt den Wert der isExtendedCallingLineIdActive-Eigenschaft fest.
     * 
     */
    public void setIsExtendedCallingLineIdActive(boolean value) {
        this.isExtendedCallingLineIdActive = value;
    }

    /**
     * Ruft den Wert der isRingTimeOutActive-Eigenschaft ab.
     * 
     */
    public boolean isIsRingTimeOutActive() {
        return isRingTimeOutActive;
    }

    /**
     * Legt den Wert der isRingTimeOutActive-Eigenschaft fest.
     * 
     */
    public void setIsRingTimeOutActive(boolean value) {
        this.isRingTimeOutActive = value;
    }

    /**
     * Ruft den Wert der ringTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getRingTimeoutSeconds() {
        return ringTimeoutSeconds;
    }

    /**
     * Legt den Wert der ringTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setRingTimeoutSeconds(int value) {
        this.ringTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der allowEmergencyRemoteOfficeOriginations-Eigenschaft ab.
     * 
     */
    public boolean isAllowEmergencyRemoteOfficeOriginations() {
        return allowEmergencyRemoteOfficeOriginations;
    }

    /**
     * Legt den Wert der allowEmergencyRemoteOfficeOriginations-Eigenschaft fest.
     * 
     */
    public void setAllowEmergencyRemoteOfficeOriginations(boolean value) {
        this.allowEmergencyRemoteOfficeOriginations = value;
    }

    /**
     * Ruft den Wert der maxNoAnswerNumberOfRings-Eigenschaft ab.
     * 
     */
    public int getMaxNoAnswerNumberOfRings() {
        return maxNoAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der maxNoAnswerNumberOfRings-Eigenschaft fest.
     * 
     */
    public void setMaxNoAnswerNumberOfRings(int value) {
        this.maxNoAnswerNumberOfRings = value;
    }

    /**
     * Ruft den Wert der incomingCallToUserAliasMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IncomingCallToUserAliasMode }
     *     
     */
    public IncomingCallToUserAliasMode getIncomingCallToUserAliasMode() {
        return incomingCallToUserAliasMode;
    }

    /**
     * Legt den Wert der incomingCallToUserAliasMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IncomingCallToUserAliasMode }
     *     
     */
    public void setIncomingCallToUserAliasMode(IncomingCallToUserAliasMode value) {
        this.incomingCallToUserAliasMode = value;
    }

}
