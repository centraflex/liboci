//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *       Response to the SystemEmergencyCallDDoSProtectionGetRequest.
 *     
 * 
 * <p>Java-Klasse für SystemEmergencyCallDDoSProtectionGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemEmergencyCallDDoSProtectionGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sampleIntervalSeconds" type="{}SampleIntervalInSeconds"/>
 *         <element name="protectionRate" type="{}ProtectionRate" minOccurs="0"/>
 *         <element name="protectionAction" type="{}ProtectionAction"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemEmergencyCallDDoSProtectionGetResponse", propOrder = {
    "enabled",
    "sampleIntervalSeconds",
    "protectionRate",
    "protectionAction"
})
public class SystemEmergencyCallDDoSProtectionGetResponse
    extends OCIDataResponse
{

    protected boolean enabled;
    protected int sampleIntervalSeconds;
    protected Integer protectionRate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ProtectionAction protectionAction;

    /**
     * Ruft den Wert der enabled-Eigenschaft ab.
     * 
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Legt den Wert der enabled-Eigenschaft fest.
     * 
     */
    public void setEnabled(boolean value) {
        this.enabled = value;
    }

    /**
     * Ruft den Wert der sampleIntervalSeconds-Eigenschaft ab.
     * 
     */
    public int getSampleIntervalSeconds() {
        return sampleIntervalSeconds;
    }

    /**
     * Legt den Wert der sampleIntervalSeconds-Eigenschaft fest.
     * 
     */
    public void setSampleIntervalSeconds(int value) {
        this.sampleIntervalSeconds = value;
    }

    /**
     * Ruft den Wert der protectionRate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getProtectionRate() {
        return protectionRate;
    }

    /**
     * Legt den Wert der protectionRate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setProtectionRate(Integer value) {
        this.protectionRate = value;
    }

    /**
     * Ruft den Wert der protectionAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProtectionAction }
     *     
     */
    public ProtectionAction getProtectionAction() {
        return protectionAction;
    }

    /**
     * Legt den Wert der protectionAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtectionAction }
     *     
     */
    public void setProtectionAction(ProtectionAction value) {
        this.protectionAction = value;
    }

}
