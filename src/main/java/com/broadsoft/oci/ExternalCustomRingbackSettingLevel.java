//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ExternalCustomRingbackSettingLevel.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ExternalCustomRingbackSettingLevel">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Service Provider"/>
 *     <enumeration value="User"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ExternalCustomRingbackSettingLevel")
@XmlEnum
public enum ExternalCustomRingbackSettingLevel {

    @XmlEnumValue("Service Provider")
    SERVICE_PROVIDER("Service Provider"),
    @XmlEnumValue("User")
    USER("User");
    private final String value;

    ExternalCustomRingbackSettingLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExternalCustomRingbackSettingLevel fromValue(String v) {
        for (ExternalCustomRingbackSettingLevel c: ExternalCustomRingbackSettingLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
