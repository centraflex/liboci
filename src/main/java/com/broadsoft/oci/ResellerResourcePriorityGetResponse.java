//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the ResellerResourcePriorityGetRequest.
 *         The response contains the reseller Resource Priority service attributes.
 *         If useSystemSettings is true, the parameters sendResourcePriorityToNetwork and 
 *         resourcePriority will not be returned in the response.
 *       
 * 
 * <p>Java-Klasse für ResellerResourcePriorityGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerResourcePriorityGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useSystemSettings" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sendResourcePriorityToNetwork" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="resourcePriority" type="{}ResourcePriorityValue"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerResourcePriorityGetResponse", propOrder = {
    "useSystemSettings",
    "sendResourcePriorityToNetwork",
    "resourcePriority"
})
public class ResellerResourcePriorityGetResponse
    extends OCIDataResponse
{

    protected boolean useSystemSettings;
    protected boolean sendResourcePriorityToNetwork;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ResourcePriorityValue resourcePriority;

    /**
     * Ruft den Wert der useSystemSettings-Eigenschaft ab.
     * 
     */
    public boolean isUseSystemSettings() {
        return useSystemSettings;
    }

    /**
     * Legt den Wert der useSystemSettings-Eigenschaft fest.
     * 
     */
    public void setUseSystemSettings(boolean value) {
        this.useSystemSettings = value;
    }

    /**
     * Ruft den Wert der sendResourcePriorityToNetwork-Eigenschaft ab.
     * 
     */
    public boolean isSendResourcePriorityToNetwork() {
        return sendResourcePriorityToNetwork;
    }

    /**
     * Legt den Wert der sendResourcePriorityToNetwork-Eigenschaft fest.
     * 
     */
    public void setSendResourcePriorityToNetwork(boolean value) {
        this.sendResourcePriorityToNetwork = value;
    }

    /**
     * Ruft den Wert der resourcePriority-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResourcePriorityValue }
     *     
     */
    public ResourcePriorityValue getResourcePriority() {
        return resourcePriority;
    }

    /**
     * Legt den Wert der resourcePriority-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResourcePriorityValue }
     *     
     */
    public void setResourcePriority(ResourcePriorityValue value) {
        this.resourcePriority = value;
    }

}
