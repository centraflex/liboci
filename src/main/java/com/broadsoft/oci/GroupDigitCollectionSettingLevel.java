//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GroupDigitCollectionSettingLevel.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="GroupDigitCollectionSettingLevel">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="System"/>
 *     <enumeration value="Service Provider"/>
 *     <enumeration value="Group"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "GroupDigitCollectionSettingLevel")
@XmlEnum
public enum GroupDigitCollectionSettingLevel {

    @XmlEnumValue("System")
    SYSTEM("System"),
    @XmlEnumValue("Service Provider")
    SERVICE_PROVIDER("Service Provider"),
    @XmlEnumValue("Group")
    GROUP("Group");
    private final String value;

    GroupDigitCollectionSettingLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupDigitCollectionSettingLevel fromValue(String v) {
        for (GroupDigitCollectionSettingLevel c: GroupDigitCollectionSettingLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
