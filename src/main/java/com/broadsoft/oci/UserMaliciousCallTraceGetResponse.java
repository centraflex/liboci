//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserMaliciousCallTraceGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserMaliciousCallTraceGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserMaliciousCallTraceGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="traceTypeSelection" type="{}MaliciousCallTraceCallTypeSelection"/>
 *         <element name="traceForTimePeriod" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="traceTimePeriod" type="{}MaliciousCallTraceTimePeriod" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserMaliciousCallTraceGetResponse", propOrder = {
    "isActive",
    "traceTypeSelection",
    "traceForTimePeriod",
    "traceTimePeriod"
})
public class UserMaliciousCallTraceGetResponse
    extends OCIDataResponse
{

    protected boolean isActive;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MaliciousCallTraceCallTypeSelection traceTypeSelection;
    protected boolean traceForTimePeriod;
    protected MaliciousCallTraceTimePeriod traceTimePeriod;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der traceTypeSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MaliciousCallTraceCallTypeSelection }
     *     
     */
    public MaliciousCallTraceCallTypeSelection getTraceTypeSelection() {
        return traceTypeSelection;
    }

    /**
     * Legt den Wert der traceTypeSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MaliciousCallTraceCallTypeSelection }
     *     
     */
    public void setTraceTypeSelection(MaliciousCallTraceCallTypeSelection value) {
        this.traceTypeSelection = value;
    }

    /**
     * Ruft den Wert der traceForTimePeriod-Eigenschaft ab.
     * 
     */
    public boolean isTraceForTimePeriod() {
        return traceForTimePeriod;
    }

    /**
     * Legt den Wert der traceForTimePeriod-Eigenschaft fest.
     * 
     */
    public void setTraceForTimePeriod(boolean value) {
        this.traceForTimePeriod = value;
    }

    /**
     * Ruft den Wert der traceTimePeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MaliciousCallTraceTimePeriod }
     *     
     */
    public MaliciousCallTraceTimePeriod getTraceTimePeriod() {
        return traceTimePeriod;
    }

    /**
     * Legt den Wert der traceTimePeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MaliciousCallTraceTimePeriod }
     *     
     */
    public void setTraceTimePeriod(MaliciousCallTraceTimePeriod value) {
        this.traceTimePeriod = value;
    }

}
