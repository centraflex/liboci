//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The read configuration of a key for Auto
 *         Attendant.
 *       
 * 
 * <p>Java-Klasse für AutoAttendantKeyReadConfiguration19 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="AutoAttendantKeyReadConfiguration19">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="key" type="{}AutoAttendantMenuKey"/>
 *         <element name="entry" type="{}AutoAttendantKeyConfigurationReadEntry19"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutoAttendantKeyReadConfiguration19", propOrder = {
    "key",
    "entry"
})
public class AutoAttendantKeyReadConfiguration19 {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String key;
    @XmlElement(required = true)
    protected AutoAttendantKeyConfigurationReadEntry19 entry;

    /**
     * Ruft den Wert der key-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Legt den Wert der key-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Ruft den Wert der entry-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantKeyConfigurationReadEntry19 }
     *     
     */
    public AutoAttendantKeyConfigurationReadEntry19 getEntry() {
        return entry;
    }

    /**
     * Legt den Wert der entry-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantKeyConfigurationReadEntry19 }
     *     
     */
    public void setEntry(AutoAttendantKeyConfigurationReadEntry19 value) {
        this.entry = value;
    }

}
