//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserCommunicationBarringGetRequest.
 *         This command only applies to users in an Enterprise.
 *       
 * 
 * <p>Java-Klasse für UserCommunicationBarringGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCommunicationBarringGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useGroupSetting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="profileName" type="{}CommunicationBarringProfileName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCommunicationBarringGetResponse", propOrder = {
    "useGroupSetting",
    "profileName"
})
public class UserCommunicationBarringGetResponse
    extends OCIDataResponse
{

    protected boolean useGroupSetting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String profileName;

    /**
     * Ruft den Wert der useGroupSetting-Eigenschaft ab.
     * 
     */
    public boolean isUseGroupSetting() {
        return useGroupSetting;
    }

    /**
     * Legt den Wert der useGroupSetting-Eigenschaft fest.
     * 
     */
    public void setUseGroupSetting(boolean value) {
        this.useGroupSetting = value;
    }

    /**
     * Ruft den Wert der profileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileName() {
        return profileName;
    }

    /**
     * Legt den Wert der profileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileName(String value) {
        this.profileName = value;
    }

}
