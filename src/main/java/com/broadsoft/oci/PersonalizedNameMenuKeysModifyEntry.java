//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal personalized name menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für PersonalizedNameMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="PersonalizedNameMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="recordNewPersonalizedName" type="{}DigitAny" minOccurs="0"/>
 *         <element name="listenToCurrentPersonalizedName" type="{}DigitAny" minOccurs="0"/>
 *         <element name="deletePersonalizedName" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonalizedNameMenuKeysModifyEntry", propOrder = {
    "recordNewPersonalizedName",
    "listenToCurrentPersonalizedName",
    "deletePersonalizedName",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class PersonalizedNameMenuKeysModifyEntry {

    @XmlElementRef(name = "recordNewPersonalizedName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recordNewPersonalizedName;
    @XmlElementRef(name = "listenToCurrentPersonalizedName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> listenToCurrentPersonalizedName;
    @XmlElementRef(name = "deletePersonalizedName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deletePersonalizedName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repeatMenu;

    /**
     * Ruft den Wert der recordNewPersonalizedName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecordNewPersonalizedName() {
        return recordNewPersonalizedName;
    }

    /**
     * Legt den Wert der recordNewPersonalizedName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecordNewPersonalizedName(JAXBElement<String> value) {
        this.recordNewPersonalizedName = value;
    }

    /**
     * Ruft den Wert der listenToCurrentPersonalizedName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getListenToCurrentPersonalizedName() {
        return listenToCurrentPersonalizedName;
    }

    /**
     * Legt den Wert der listenToCurrentPersonalizedName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setListenToCurrentPersonalizedName(JAXBElement<String> value) {
        this.listenToCurrentPersonalizedName = value;
    }

    /**
     * Ruft den Wert der deletePersonalizedName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeletePersonalizedName() {
        return deletePersonalizedName;
    }

    /**
     * Legt den Wert der deletePersonalizedName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeletePersonalizedName(JAXBElement<String> value) {
        this.deletePersonalizedName = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepeatMenu(JAXBElement<String> value) {
        this.repeatMenu = value;
    }

}
