//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderMWIDeliveryToMobileEndpointGetRequest.
 *         
 *         The templateActivationTable contains the list of templates defined for the service provider.
 *         The column headings are "Enable", "Language", "Type".
 *       
 * 
 * <p>Java-Klasse für ServiceProviderMWIDeliveryToMobileEndpointGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderMWIDeliveryToMobileEndpointGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="templateActivationTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderMWIDeliveryToMobileEndpointGetResponse", propOrder = {
    "templateActivationTable"
})
public class ServiceProviderMWIDeliveryToMobileEndpointGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable templateActivationTable;

    /**
     * Ruft den Wert der templateActivationTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getTemplateActivationTable() {
        return templateActivationTable;
    }

    /**
     * Legt den Wert der templateActivationTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setTemplateActivationTable(OCITable value) {
        this.templateActivationTable = value;
    }

}
