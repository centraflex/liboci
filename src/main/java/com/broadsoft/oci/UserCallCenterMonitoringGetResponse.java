//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserCallCenterMonitoringGetRequest.
 *         Replaced by UserCallCenterMonitoringGetResponse23
 *       
 * 
 * <p>Java-Klasse für UserCallCenterMonitoringGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallCenterMonitoringGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="playToneToAgentForSilentMonitoring" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallCenterMonitoringGetResponse", propOrder = {
    "playToneToAgentForSilentMonitoring"
})
public class UserCallCenterMonitoringGetResponse
    extends OCIDataResponse
{

    protected boolean playToneToAgentForSilentMonitoring;

    /**
     * Ruft den Wert der playToneToAgentForSilentMonitoring-Eigenschaft ab.
     * 
     */
    public boolean isPlayToneToAgentForSilentMonitoring() {
        return playToneToAgentForSilentMonitoring;
    }

    /**
     * Legt den Wert der playToneToAgentForSilentMonitoring-Eigenschaft fest.
     * 
     */
    public void setPlayToneToAgentForSilentMonitoring(boolean value) {
        this.playToneToAgentForSilentMonitoring = value;
    }

}
