//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The voice portal voice portal login menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für VoicePortalLoginMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="VoicePortalLoginMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="accessUsingOtherMailboxId" type="{}VoicePortalDigitSequence" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoicePortalLoginMenuKeysModifyEntry", propOrder = {
    "accessUsingOtherMailboxId"
})
public class VoicePortalLoginMenuKeysModifyEntry {

    @XmlElementRef(name = "accessUsingOtherMailboxId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> accessUsingOtherMailboxId;

    /**
     * Ruft den Wert der accessUsingOtherMailboxId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAccessUsingOtherMailboxId() {
        return accessUsingOtherMailboxId;
    }

    /**
     * Legt den Wert der accessUsingOtherMailboxId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAccessUsingOtherMailboxId(JAXBElement<String> value) {
        this.accessUsingOtherMailboxId = value;
    }

}
