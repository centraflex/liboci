//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupCallCenterComfortMessageBypassGetRequest20.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterComfortMessageBypassGetResponse20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterComfortMessageBypassGetResponse20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="callWaitingAgeThresholdSeconds" type="{}CallCenterComfortMessageBypassThresholdSeconds"/>
 *         <element name="playAnnouncementAfterRinging" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="ringTimeBeforePlayingAnnouncementSeconds" type="{}CallCenterRingTimeBeforePlayingComfortMessageBypassAnnouncementSeconds"/>
 *         <element name="audioMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="audioUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="audioFileList" type="{}CallCenterAnnouncementFileListRead20" minOccurs="0"/>
 *         <element name="videoMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="videoUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="videoFileList" type="{}CallCenterAnnouncementFileListRead20" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterComfortMessageBypassGetResponse20", propOrder = {
    "isActive",
    "callWaitingAgeThresholdSeconds",
    "playAnnouncementAfterRinging",
    "ringTimeBeforePlayingAnnouncementSeconds",
    "audioMessageSelection",
    "audioUrlList",
    "audioFileList",
    "videoMessageSelection",
    "videoUrlList",
    "videoFileList"
})
public class GroupCallCenterComfortMessageBypassGetResponse20
    extends OCIDataResponse
{

    protected boolean isActive;
    protected int callWaitingAgeThresholdSeconds;
    protected boolean playAnnouncementAfterRinging;
    protected int ringTimeBeforePlayingAnnouncementSeconds;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection audioMessageSelection;
    protected CallCenterAnnouncementURLList audioUrlList;
    protected CallCenterAnnouncementFileListRead20 audioFileList;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection videoMessageSelection;
    protected CallCenterAnnouncementURLList videoUrlList;
    protected CallCenterAnnouncementFileListRead20 videoFileList;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der callWaitingAgeThresholdSeconds-Eigenschaft ab.
     * 
     */
    public int getCallWaitingAgeThresholdSeconds() {
        return callWaitingAgeThresholdSeconds;
    }

    /**
     * Legt den Wert der callWaitingAgeThresholdSeconds-Eigenschaft fest.
     * 
     */
    public void setCallWaitingAgeThresholdSeconds(int value) {
        this.callWaitingAgeThresholdSeconds = value;
    }

    /**
     * Ruft den Wert der playAnnouncementAfterRinging-Eigenschaft ab.
     * 
     */
    public boolean isPlayAnnouncementAfterRinging() {
        return playAnnouncementAfterRinging;
    }

    /**
     * Legt den Wert der playAnnouncementAfterRinging-Eigenschaft fest.
     * 
     */
    public void setPlayAnnouncementAfterRinging(boolean value) {
        this.playAnnouncementAfterRinging = value;
    }

    /**
     * Ruft den Wert der ringTimeBeforePlayingAnnouncementSeconds-Eigenschaft ab.
     * 
     */
    public int getRingTimeBeforePlayingAnnouncementSeconds() {
        return ringTimeBeforePlayingAnnouncementSeconds;
    }

    /**
     * Legt den Wert der ringTimeBeforePlayingAnnouncementSeconds-Eigenschaft fest.
     * 
     */
    public void setRingTimeBeforePlayingAnnouncementSeconds(int value) {
        this.ringTimeBeforePlayingAnnouncementSeconds = value;
    }

    /**
     * Ruft den Wert der audioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getAudioMessageSelection() {
        return audioMessageSelection;
    }

    /**
     * Legt den Wert der audioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.audioMessageSelection = value;
    }

    /**
     * Ruft den Wert der audioUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getAudioUrlList() {
        return audioUrlList;
    }

    /**
     * Legt den Wert der audioUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setAudioUrlList(CallCenterAnnouncementURLList value) {
        this.audioUrlList = value;
    }

    /**
     * Ruft den Wert der audioFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListRead20 }
     *     
     */
    public CallCenterAnnouncementFileListRead20 getAudioFileList() {
        return audioFileList;
    }

    /**
     * Legt den Wert der audioFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListRead20 }
     *     
     */
    public void setAudioFileList(CallCenterAnnouncementFileListRead20 value) {
        this.audioFileList = value;
    }

    /**
     * Ruft den Wert der videoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getVideoMessageSelection() {
        return videoMessageSelection;
    }

    /**
     * Legt den Wert der videoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.videoMessageSelection = value;
    }

    /**
     * Ruft den Wert der videoUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getVideoUrlList() {
        return videoUrlList;
    }

    /**
     * Legt den Wert der videoUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setVideoUrlList(CallCenterAnnouncementURLList value) {
        this.videoUrlList = value;
    }

    /**
     * Ruft den Wert der videoFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListRead20 }
     *     
     */
    public CallCenterAnnouncementFileListRead20 getVideoFileList() {
        return videoFileList;
    }

    /**
     * Legt den Wert der videoFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListRead20 }
     *     
     */
    public void setVideoFileList(CallCenterAnnouncementFileListRead20 value) {
        this.videoFileList = value;
    }

}
