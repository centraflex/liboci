//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserPhoneDirectoryGetPagedListRequest.
 *         Returns the number of entries that would be returned if the response 
 *         Was not page size restricted.
 *         The "My Room Room Id" and "My Room Bridge Id" are only populated for
 * 	      users assigned the "Collaborate-Audio" service.
 *         Contains a table with a row for each user and column headings:
 *         "User Id", "First Name", "Last Name", "First Name Unicode", 
 *         "Last Name Unicode", "Hiragana First Name", "Hiragana Last Name",
 *         "Title", "Phone Number", "Extension", "Mobile", "Pager", 
 *         "Email Address", "Yahoo Id", "Department", "Group Id", "Location",
 *         "Address Line 1", "Address Line 2", "City", "State", "Zip", "Country",
 *         "IMP Id", "Location Code", "My Room Room Id", "My Room Bridge Id", "Service Name",
 *         and "Receptionist Note".
 *         The Service Name represents the localized service name for service instances. The localized values are taken from the BroadworksLabel.properties file.
 *         Service Name is currently supporting:
 *         AutoAttendant, AutoAttendantStandard, AutoAttendantVideo, CallCenter, CallCenterStandard, CallCenterPremium
 *         HuntGroup, InstantGroupCall, VoiceMessagingGroup, RoutePoint, BroadWorksAnywhere, GroupPaging, FindmeFollowme, 
 *         VoiceXML, FlexibleSeatingGuest, CollaborateAudio, MeetMeConferencing.
 *         For a Regular User or a Virtual On Network Enterprise Extensions, the Service Name is empty.
 *         The response entries are sorted by "Last Name Unicode" first and
 *         "First Name unicode" secondarily.
 *         
 *         The following columns are only returned in AS data mode:       
 *         "Location Code", "My Room Room Id", "My Room Bridge Id", "Service Name", "Receptionist Note"
 *         The Receptionist Note column is only populated, if the user sending 
 *         the request is the owner of the Receptionist Note and a Note exists.
 *       
 * 
 * <p>Java-Klasse für UserPhoneDirectoryGetPagedListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPhoneDirectoryGetPagedListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="totalNumberOfRows" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="directoryTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPhoneDirectoryGetPagedListResponse", propOrder = {
    "totalNumberOfRows",
    "directoryTable"
})
public class UserPhoneDirectoryGetPagedListResponse
    extends OCIDataResponse
{

    protected int totalNumberOfRows;
    @XmlElement(required = true)
    protected OCITable directoryTable;

    /**
     * Ruft den Wert der totalNumberOfRows-Eigenschaft ab.
     * 
     */
    public int getTotalNumberOfRows() {
        return totalNumberOfRows;
    }

    /**
     * Legt den Wert der totalNumberOfRows-Eigenschaft fest.
     * 
     */
    public void setTotalNumberOfRows(int value) {
        this.totalNumberOfRows = value;
    }

    /**
     * Ruft den Wert der directoryTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDirectoryTable() {
        return directoryTable;
    }

    /**
     * Legt den Wert der directoryTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDirectoryTable(OCITable value) {
        this.directoryTable = value;
    }

}
