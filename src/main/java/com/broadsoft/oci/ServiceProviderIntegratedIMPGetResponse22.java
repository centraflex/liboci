//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the ServiceProviderIntegratedIMPGetRequest22.
 *         The response contains the service provider Integrated IMP service attributes.
 *         If the service provider is within a reseller, useSystemServiceDomain means using reseller level service 
 *         domain setting. And useSystemMessagingServer means using the reseller level messaging server setting.
 *         The element useResellerIMPIdSetting means using the reseller level IMP User ID setting.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderIntegratedIMPGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderIntegratedIMPGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useSystemServiceDomain" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="serviceDomain" type="{}DomainName" minOccurs="0"/>
 *         <element name="servicePort" type="{}Port" minOccurs="0"/>
 *         <element name="useSystemMessagingServer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="provisioningUrl" type="{}URL" minOccurs="0"/>
 *         <element name="provisioningUserId" type="{}ProvisioningBroadCloudAuthenticationUserName" minOccurs="0"/>
 *         <element name="boshURL" type="{}URL" minOccurs="0"/>
 *         <choice>
 *           <element name="defaultImpIdType" type="{}IntegratedIMPUserIDType"/>
 *           <element name="useResellerIMPIdSetting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderIntegratedIMPGetResponse22", propOrder = {
    "useSystemServiceDomain",
    "serviceDomain",
    "servicePort",
    "useSystemMessagingServer",
    "provisioningUrl",
    "provisioningUserId",
    "boshURL",
    "defaultImpIdType",
    "useResellerIMPIdSetting"
})
public class ServiceProviderIntegratedIMPGetResponse22
    extends OCIDataResponse
{

    protected boolean useSystemServiceDomain;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceDomain;
    protected Integer servicePort;
    protected boolean useSystemMessagingServer;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String provisioningUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String provisioningUserId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String boshURL;
    @XmlSchemaType(name = "token")
    protected IntegratedIMPUserIDType defaultImpIdType;
    protected Boolean useResellerIMPIdSetting;

    /**
     * Ruft den Wert der useSystemServiceDomain-Eigenschaft ab.
     * 
     */
    public boolean isUseSystemServiceDomain() {
        return useSystemServiceDomain;
    }

    /**
     * Legt den Wert der useSystemServiceDomain-Eigenschaft fest.
     * 
     */
    public void setUseSystemServiceDomain(boolean value) {
        this.useSystemServiceDomain = value;
    }

    /**
     * Ruft den Wert der serviceDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceDomain() {
        return serviceDomain;
    }

    /**
     * Legt den Wert der serviceDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceDomain(String value) {
        this.serviceDomain = value;
    }

    /**
     * Ruft den Wert der servicePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServicePort() {
        return servicePort;
    }

    /**
     * Legt den Wert der servicePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServicePort(Integer value) {
        this.servicePort = value;
    }

    /**
     * Ruft den Wert der useSystemMessagingServer-Eigenschaft ab.
     * 
     */
    public boolean isUseSystemMessagingServer() {
        return useSystemMessagingServer;
    }

    /**
     * Legt den Wert der useSystemMessagingServer-Eigenschaft fest.
     * 
     */
    public void setUseSystemMessagingServer(boolean value) {
        this.useSystemMessagingServer = value;
    }

    /**
     * Ruft den Wert der provisioningUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvisioningUrl() {
        return provisioningUrl;
    }

    /**
     * Legt den Wert der provisioningUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvisioningUrl(String value) {
        this.provisioningUrl = value;
    }

    /**
     * Ruft den Wert der provisioningUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvisioningUserId() {
        return provisioningUserId;
    }

    /**
     * Legt den Wert der provisioningUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvisioningUserId(String value) {
        this.provisioningUserId = value;
    }

    /**
     * Ruft den Wert der boshURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoshURL() {
        return boshURL;
    }

    /**
     * Legt den Wert der boshURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoshURL(String value) {
        this.boshURL = value;
    }

    /**
     * Ruft den Wert der defaultImpIdType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IntegratedIMPUserIDType }
     *     
     */
    public IntegratedIMPUserIDType getDefaultImpIdType() {
        return defaultImpIdType;
    }

    /**
     * Legt den Wert der defaultImpIdType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IntegratedIMPUserIDType }
     *     
     */
    public void setDefaultImpIdType(IntegratedIMPUserIDType value) {
        this.defaultImpIdType = value;
    }

    /**
     * Ruft den Wert der useResellerIMPIdSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseResellerIMPIdSetting() {
        return useResellerIMPIdSetting;
    }

    /**
     * Legt den Wert der useResellerIMPIdSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseResellerIMPIdSetting(Boolean value) {
        this.useResellerIMPIdSetting = value;
    }

}
