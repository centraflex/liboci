//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeviceManagementFileCategory.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DeviceManagementFileCategory">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Static"/>
 *     <enumeration value="Dynamic Group"/>
 *     <enumeration value="Dynamic Profile"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DeviceManagementFileCategory")
@XmlEnum
public enum DeviceManagementFileCategory {

    @XmlEnumValue("Static")
    STATIC("Static"),
    @XmlEnumValue("Dynamic Group")
    DYNAMIC_GROUP("Dynamic Group"),
    @XmlEnumValue("Dynamic Profile")
    DYNAMIC_PROFILE("Dynamic Profile");
    private final String value;

    DeviceManagementFileCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeviceManagementFileCategory fromValue(String v) {
        for (DeviceManagementFileCategory c: DeviceManagementFileCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
