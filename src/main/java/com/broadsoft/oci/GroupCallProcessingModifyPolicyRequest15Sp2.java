//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the group level data associated with Call Processing Policy.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         The useGroupCLIDSetting attribute controls the CLID settings 
 *         (clidPolicy,enterpriseCallsCLIDPolicy, groupCallsCLIDPolicy,  emergencyClidPolicy, allowAlternateNumbersForRedirectingIdentity, useGroupName, allowConfigurableCLIDForRedirectingIdentity, allowDepartmentCLIDNameOverride
 *          useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable, useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailabl)
 * 
 *         The allowDepartmentCLID attribute is automatically set to false when the useGroupName attribute is set to false.
 * 
 *         The useGroupMediaSetting attribute controls the Media settings 
 *         (medisPolicySelection, supportedMediaSetName)
 * 
 *         The useGroupCallLimitsSetting attribute controls the Call Limits settings 
 *         (useMaxSimultaneousCalls, maxSimultaneousCalls, useMaxSimultaneousVideoCalls, maxSimultaneousVideoCalls, 
 *         useMaxCallTimeForAnsweredCalls, maxCallTimeForAnsweredCallsMinutes, useMaxCallTimeForUnansweredCalls, 
 *         maxCallTimeForUnansweredCallsMinutes, useMaxConcurrentRedirectedCalls, useMaxFindMeFollowMeDepth, 
 *         maxRedirectionDepth, useMaxConcurrentFindMeFollowMeInvocations, maxConcurrentFindMeFollowMeInvocations, 
 *         useMaxConcurrentTerminatingAlertingRequests, maxConcurrentTerminatingAlertingRequests,
 *         includeRedirectionsInMaximumNumberOfConcurrentCalls)
 * 
 *         The useGroupTranslationRoutingSetting attribute controls the routing and translation settings (networkUsageSelection, enforceGroupCallingLineIdentityRestriction, allowEnterpriseGroupCallTypingForPrivateDialingPlan, allowEnterpriseGroupCallTypingForPublicDialingPlan, overrideCLIDRestrictionForExternalCallsViaPrivateDialingPlan, enableEnterpriseExtensionDialing, routeOverrideDomain, routeOverridePrefix)
 *         The useGroupDCLIDSetting controls the Dialable Caller ID settings (enableDialableCallerID)
 * 
 *         The useGroupPhoneListLookupSetting controls whether or not to use the enterprise/service provider setting for the Phone List Lookup policy (enablePhoneListLookup)
 *         
 *         The following elements are only used in AS data mode:
 *            useGroupDCLIDSetting
 *            enableDialableCallerID
 *            allowConfigurableCLIDForRedirectingIdentity
 *            allowDepartmentCLIDNameOverride
 *            allowConfigurableCLIDForRedirectingIdentity
 *            enterpriseCallsCLIDPolicy
 *            groupCallsCLIDPolicy
 *            
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *            useGroupPhoneListLookupSetting
 *            enablePhoneListLookup
 *            useMaxConcurrentTerminatingAlertingRequests
 *            maxConcurrentTerminatingAlertingRequests
 *            includeRedirectionsInMaximumNumberOfConcurrentCalls
 *            useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable
 *            useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable
 *            allowMobileDNForRedirectingIdentity
 *            enableGatewayRoutePolicy
 *            networkCallsGatewayRouteIdentity
 *            networkURLCallsGatewayRouteIdentity
 *            emergencyCallsGatewayRouteIdentity
 *            repairCallsGatewayRouteIdentity
 *            callTypingErrorsGatewayRouteIdentity
 *            
 *         The following elements are only used in XS data mode and ignored in AS data mode:
 *            routeOverrideDomain
 *            routeOverridePrefix
 *       
 * 
 * <p>Java-Klasse für GroupCallProcessingModifyPolicyRequest15sp2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallProcessingModifyPolicyRequest15sp2">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="useGroupCLIDSetting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useGroupMediaSetting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useGroupCallLimitsSetting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useGroupTranslationRoutingSetting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useGroupDCLIDSetting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useMaxSimultaneousCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxSimultaneousCalls" type="{}CallProcessingMaxSimultaneousCalls19sp1" minOccurs="0"/>
 *         <element name="useMaxSimultaneousVideoCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxSimultaneousVideoCalls" type="{}CallProcessingMaxSimultaneousCalls19sp1" minOccurs="0"/>
 *         <element name="useMaxCallTimeForAnsweredCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxCallTimeForAnsweredCallsMinutes" type="{}CallProcessingMaxCallTimeForAnsweredCallsMinutes16" minOccurs="0"/>
 *         <element name="useMaxCallTimeForUnansweredCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxCallTimeForUnansweredCallsMinutes" type="{}CallProcessingMaxCallTimeForUnansweredCallsMinutes19sp1" minOccurs="0"/>
 *         <element name="mediaPolicySelection" type="{}MediaPolicySelection" minOccurs="0"/>
 *         <element name="supportedMediaSetName" type="{}MediaSetName" minOccurs="0"/>
 *         <element name="networkUsageSelection" type="{}NetworkUsageSelection" minOccurs="0"/>
 *         <element name="enforceGroupCallingLineIdentityRestriction" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowEnterpriseGroupCallTypingForPrivateDialingPlan" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowEnterpriseGroupCallTypingForPublicDialingPlan" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="overrideCLIDRestrictionForPrivateCallCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useEnterpriseCLIDForPrivateCallCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableEnterpriseExtensionDialing" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useMaxConcurrentRedirectedCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxConcurrentRedirectedCalls" type="{}CallProcessingMaxConcurrentRedirectedCalls19sp1" minOccurs="0"/>
 *         <element name="useMaxFindMeFollowMeDepth" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxFindMeFollowMeDepth" type="{}CallProcessingMaxFindMeFollowMeDepth19sp1" minOccurs="0"/>
 *         <element name="maxRedirectionDepth" type="{}CallProcessingMaxRedirectionDepth19sp1" minOccurs="0"/>
 *         <element name="useMaxConcurrentFindMeFollowMeInvocations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxConcurrentFindMeFollowMeInvocations" type="{}CallProcessingMaxConcurrentFindMeFollowMeInvocations19sp1" minOccurs="0"/>
 *         <element name="clidPolicy" type="{}GroupCLIDPolicy" minOccurs="0"/>
 *         <element name="emergencyClidPolicy" type="{}GroupCLIDPolicy" minOccurs="0"/>
 *         <element name="allowAlternateNumbersForRedirectingIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useGroupName" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="blockCallingNameForExternalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableDialableCallerID" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowConfigurableCLIDForRedirectingIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowDepartmentCLIDNameOverride" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enterpriseCallsCLIDPolicy" type="{}EnterpriseInternalCallsCLIDPolicy" minOccurs="0"/>
 *         <element name="groupCallsCLIDPolicy" type="{}EnterpriseInternalCallsCLIDPolicy" minOccurs="0"/>
 *         <element name="useGroupPhoneListLookupSetting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enablePhoneListLookup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useMaxConcurrentTerminatingAlertingRequests" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxConcurrentTerminatingAlertingRequests" type="{}CallProcessingMaxConcurrentTerminatingAlertingRequests" minOccurs="0"/>
 *         <element name="includeRedirectionsInMaximumNumberOfConcurrentCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="routeOverrideDomain" type="{}NetAddress" minOccurs="0"/>
 *         <element name="routeOverridePrefix" type="{}RouteOverridePrefix" minOccurs="0"/>
 *         <element name="allowMobileDNForRedirectingIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableGatewayRoutePolicy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="networkCallsGatewayRouteIdentity" type="{}TrunkGroupName" minOccurs="0"/>
 *         <element name="networkURLCallsGatewayRouteIdentity" type="{}TrunkGroupName" minOccurs="0"/>
 *         <element name="emergencyCallsGatewayRouteIdentity" type="{}TrunkGroupName" minOccurs="0"/>
 *         <element name="repairCallsGatewayRouteIdentity" type="{}TrunkGroupName" minOccurs="0"/>
 *         <element name="callTypingErrorsGatewayRouteIdentity" type="{}TrunkGroupName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallProcessingModifyPolicyRequest15sp2", propOrder = {
    "serviceProviderId",
    "groupId",
    "useGroupCLIDSetting",
    "useGroupMediaSetting",
    "useGroupCallLimitsSetting",
    "useGroupTranslationRoutingSetting",
    "useGroupDCLIDSetting",
    "useMaxSimultaneousCalls",
    "maxSimultaneousCalls",
    "useMaxSimultaneousVideoCalls",
    "maxSimultaneousVideoCalls",
    "useMaxCallTimeForAnsweredCalls",
    "maxCallTimeForAnsweredCallsMinutes",
    "useMaxCallTimeForUnansweredCalls",
    "maxCallTimeForUnansweredCallsMinutes",
    "mediaPolicySelection",
    "supportedMediaSetName",
    "networkUsageSelection",
    "enforceGroupCallingLineIdentityRestriction",
    "allowEnterpriseGroupCallTypingForPrivateDialingPlan",
    "allowEnterpriseGroupCallTypingForPublicDialingPlan",
    "overrideCLIDRestrictionForPrivateCallCategory",
    "useEnterpriseCLIDForPrivateCallCategory",
    "enableEnterpriseExtensionDialing",
    "useMaxConcurrentRedirectedCalls",
    "maxConcurrentRedirectedCalls",
    "useMaxFindMeFollowMeDepth",
    "maxFindMeFollowMeDepth",
    "maxRedirectionDepth",
    "useMaxConcurrentFindMeFollowMeInvocations",
    "maxConcurrentFindMeFollowMeInvocations",
    "clidPolicy",
    "emergencyClidPolicy",
    "allowAlternateNumbersForRedirectingIdentity",
    "useGroupName",
    "blockCallingNameForExternalCalls",
    "enableDialableCallerID",
    "allowConfigurableCLIDForRedirectingIdentity",
    "allowDepartmentCLIDNameOverride",
    "enterpriseCallsCLIDPolicy",
    "groupCallsCLIDPolicy",
    "useGroupPhoneListLookupSetting",
    "enablePhoneListLookup",
    "useMaxConcurrentTerminatingAlertingRequests",
    "maxConcurrentTerminatingAlertingRequests",
    "includeRedirectionsInMaximumNumberOfConcurrentCalls",
    "useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable",
    "useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable",
    "routeOverrideDomain",
    "routeOverridePrefix",
    "allowMobileDNForRedirectingIdentity",
    "enableGatewayRoutePolicy",
    "networkCallsGatewayRouteIdentity",
    "networkURLCallsGatewayRouteIdentity",
    "emergencyCallsGatewayRouteIdentity",
    "repairCallsGatewayRouteIdentity",
    "callTypingErrorsGatewayRouteIdentity"
})
public class GroupCallProcessingModifyPolicyRequest15Sp2
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected Boolean useGroupCLIDSetting;
    protected Boolean useGroupMediaSetting;
    protected Boolean useGroupCallLimitsSetting;
    protected Boolean useGroupTranslationRoutingSetting;
    protected Boolean useGroupDCLIDSetting;
    protected Boolean useMaxSimultaneousCalls;
    protected Integer maxSimultaneousCalls;
    protected Boolean useMaxSimultaneousVideoCalls;
    protected Integer maxSimultaneousVideoCalls;
    protected Boolean useMaxCallTimeForAnsweredCalls;
    protected Integer maxCallTimeForAnsweredCallsMinutes;
    protected Boolean useMaxCallTimeForUnansweredCalls;
    protected Integer maxCallTimeForUnansweredCallsMinutes;
    @XmlSchemaType(name = "token")
    protected MediaPolicySelection mediaPolicySelection;
    @XmlElementRef(name = "supportedMediaSetName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> supportedMediaSetName;
    @XmlSchemaType(name = "token")
    protected NetworkUsageSelection networkUsageSelection;
    protected Boolean enforceGroupCallingLineIdentityRestriction;
    protected Boolean allowEnterpriseGroupCallTypingForPrivateDialingPlan;
    protected Boolean allowEnterpriseGroupCallTypingForPublicDialingPlan;
    protected Boolean overrideCLIDRestrictionForPrivateCallCategory;
    protected Boolean useEnterpriseCLIDForPrivateCallCategory;
    protected Boolean enableEnterpriseExtensionDialing;
    protected Boolean useMaxConcurrentRedirectedCalls;
    protected Integer maxConcurrentRedirectedCalls;
    protected Boolean useMaxFindMeFollowMeDepth;
    protected Integer maxFindMeFollowMeDepth;
    protected Integer maxRedirectionDepth;
    protected Boolean useMaxConcurrentFindMeFollowMeInvocations;
    protected Integer maxConcurrentFindMeFollowMeInvocations;
    @XmlSchemaType(name = "token")
    protected GroupCLIDPolicy clidPolicy;
    @XmlSchemaType(name = "token")
    protected GroupCLIDPolicy emergencyClidPolicy;
    protected Boolean allowAlternateNumbersForRedirectingIdentity;
    protected Boolean useGroupName;
    protected Boolean blockCallingNameForExternalCalls;
    protected Boolean enableDialableCallerID;
    protected Boolean allowConfigurableCLIDForRedirectingIdentity;
    protected Boolean allowDepartmentCLIDNameOverride;
    @XmlSchemaType(name = "token")
    protected EnterpriseInternalCallsCLIDPolicy enterpriseCallsCLIDPolicy;
    @XmlSchemaType(name = "token")
    protected EnterpriseInternalCallsCLIDPolicy groupCallsCLIDPolicy;
    protected Boolean useGroupPhoneListLookupSetting;
    protected Boolean enablePhoneListLookup;
    protected Boolean useMaxConcurrentTerminatingAlertingRequests;
    protected Integer maxConcurrentTerminatingAlertingRequests;
    protected Boolean includeRedirectionsInMaximumNumberOfConcurrentCalls;
    protected Boolean useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable;
    protected Boolean useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable;
    @XmlElementRef(name = "routeOverrideDomain", type = JAXBElement.class, required = false)
    protected JAXBElement<String> routeOverrideDomain;
    @XmlElementRef(name = "routeOverridePrefix", type = JAXBElement.class, required = false)
    protected JAXBElement<String> routeOverridePrefix;
    protected Boolean allowMobileDNForRedirectingIdentity;
    protected Boolean enableGatewayRoutePolicy;
    @XmlElementRef(name = "networkCallsGatewayRouteIdentity", type = JAXBElement.class, required = false)
    protected JAXBElement<String> networkCallsGatewayRouteIdentity;
    @XmlElementRef(name = "networkURLCallsGatewayRouteIdentity", type = JAXBElement.class, required = false)
    protected JAXBElement<String> networkURLCallsGatewayRouteIdentity;
    @XmlElementRef(name = "emergencyCallsGatewayRouteIdentity", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emergencyCallsGatewayRouteIdentity;
    @XmlElementRef(name = "repairCallsGatewayRouteIdentity", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repairCallsGatewayRouteIdentity;
    @XmlElementRef(name = "callTypingErrorsGatewayRouteIdentity", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callTypingErrorsGatewayRouteIdentity;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der useGroupCLIDSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseGroupCLIDSetting() {
        return useGroupCLIDSetting;
    }

    /**
     * Legt den Wert der useGroupCLIDSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseGroupCLIDSetting(Boolean value) {
        this.useGroupCLIDSetting = value;
    }

    /**
     * Ruft den Wert der useGroupMediaSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseGroupMediaSetting() {
        return useGroupMediaSetting;
    }

    /**
     * Legt den Wert der useGroupMediaSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseGroupMediaSetting(Boolean value) {
        this.useGroupMediaSetting = value;
    }

    /**
     * Ruft den Wert der useGroupCallLimitsSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseGroupCallLimitsSetting() {
        return useGroupCallLimitsSetting;
    }

    /**
     * Legt den Wert der useGroupCallLimitsSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseGroupCallLimitsSetting(Boolean value) {
        this.useGroupCallLimitsSetting = value;
    }

    /**
     * Ruft den Wert der useGroupTranslationRoutingSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseGroupTranslationRoutingSetting() {
        return useGroupTranslationRoutingSetting;
    }

    /**
     * Legt den Wert der useGroupTranslationRoutingSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseGroupTranslationRoutingSetting(Boolean value) {
        this.useGroupTranslationRoutingSetting = value;
    }

    /**
     * Ruft den Wert der useGroupDCLIDSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseGroupDCLIDSetting() {
        return useGroupDCLIDSetting;
    }

    /**
     * Legt den Wert der useGroupDCLIDSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseGroupDCLIDSetting(Boolean value) {
        this.useGroupDCLIDSetting = value;
    }

    /**
     * Ruft den Wert der useMaxSimultaneousCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxSimultaneousCalls() {
        return useMaxSimultaneousCalls;
    }

    /**
     * Legt den Wert der useMaxSimultaneousCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxSimultaneousCalls(Boolean value) {
        this.useMaxSimultaneousCalls = value;
    }

    /**
     * Ruft den Wert der maxSimultaneousCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxSimultaneousCalls() {
        return maxSimultaneousCalls;
    }

    /**
     * Legt den Wert der maxSimultaneousCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxSimultaneousCalls(Integer value) {
        this.maxSimultaneousCalls = value;
    }

    /**
     * Ruft den Wert der useMaxSimultaneousVideoCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxSimultaneousVideoCalls() {
        return useMaxSimultaneousVideoCalls;
    }

    /**
     * Legt den Wert der useMaxSimultaneousVideoCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxSimultaneousVideoCalls(Boolean value) {
        this.useMaxSimultaneousVideoCalls = value;
    }

    /**
     * Ruft den Wert der maxSimultaneousVideoCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxSimultaneousVideoCalls() {
        return maxSimultaneousVideoCalls;
    }

    /**
     * Legt den Wert der maxSimultaneousVideoCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxSimultaneousVideoCalls(Integer value) {
        this.maxSimultaneousVideoCalls = value;
    }

    /**
     * Ruft den Wert der useMaxCallTimeForAnsweredCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxCallTimeForAnsweredCalls() {
        return useMaxCallTimeForAnsweredCalls;
    }

    /**
     * Legt den Wert der useMaxCallTimeForAnsweredCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxCallTimeForAnsweredCalls(Boolean value) {
        this.useMaxCallTimeForAnsweredCalls = value;
    }

    /**
     * Ruft den Wert der maxCallTimeForAnsweredCallsMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCallTimeForAnsweredCallsMinutes() {
        return maxCallTimeForAnsweredCallsMinutes;
    }

    /**
     * Legt den Wert der maxCallTimeForAnsweredCallsMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCallTimeForAnsweredCallsMinutes(Integer value) {
        this.maxCallTimeForAnsweredCallsMinutes = value;
    }

    /**
     * Ruft den Wert der useMaxCallTimeForUnansweredCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxCallTimeForUnansweredCalls() {
        return useMaxCallTimeForUnansweredCalls;
    }

    /**
     * Legt den Wert der useMaxCallTimeForUnansweredCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxCallTimeForUnansweredCalls(Boolean value) {
        this.useMaxCallTimeForUnansweredCalls = value;
    }

    /**
     * Ruft den Wert der maxCallTimeForUnansweredCallsMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCallTimeForUnansweredCallsMinutes() {
        return maxCallTimeForUnansweredCallsMinutes;
    }

    /**
     * Legt den Wert der maxCallTimeForUnansweredCallsMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCallTimeForUnansweredCallsMinutes(Integer value) {
        this.maxCallTimeForUnansweredCallsMinutes = value;
    }

    /**
     * Ruft den Wert der mediaPolicySelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MediaPolicySelection }
     *     
     */
    public MediaPolicySelection getMediaPolicySelection() {
        return mediaPolicySelection;
    }

    /**
     * Legt den Wert der mediaPolicySelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MediaPolicySelection }
     *     
     */
    public void setMediaPolicySelection(MediaPolicySelection value) {
        this.mediaPolicySelection = value;
    }

    /**
     * Ruft den Wert der supportedMediaSetName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSupportedMediaSetName() {
        return supportedMediaSetName;
    }

    /**
     * Legt den Wert der supportedMediaSetName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSupportedMediaSetName(JAXBElement<String> value) {
        this.supportedMediaSetName = value;
    }

    /**
     * Ruft den Wert der networkUsageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkUsageSelection }
     *     
     */
    public NetworkUsageSelection getNetworkUsageSelection() {
        return networkUsageSelection;
    }

    /**
     * Legt den Wert der networkUsageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkUsageSelection }
     *     
     */
    public void setNetworkUsageSelection(NetworkUsageSelection value) {
        this.networkUsageSelection = value;
    }

    /**
     * Ruft den Wert der enforceGroupCallingLineIdentityRestriction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnforceGroupCallingLineIdentityRestriction() {
        return enforceGroupCallingLineIdentityRestriction;
    }

    /**
     * Legt den Wert der enforceGroupCallingLineIdentityRestriction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnforceGroupCallingLineIdentityRestriction(Boolean value) {
        this.enforceGroupCallingLineIdentityRestriction = value;
    }

    /**
     * Ruft den Wert der allowEnterpriseGroupCallTypingForPrivateDialingPlan-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowEnterpriseGroupCallTypingForPrivateDialingPlan() {
        return allowEnterpriseGroupCallTypingForPrivateDialingPlan;
    }

    /**
     * Legt den Wert der allowEnterpriseGroupCallTypingForPrivateDialingPlan-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowEnterpriseGroupCallTypingForPrivateDialingPlan(Boolean value) {
        this.allowEnterpriseGroupCallTypingForPrivateDialingPlan = value;
    }

    /**
     * Ruft den Wert der allowEnterpriseGroupCallTypingForPublicDialingPlan-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowEnterpriseGroupCallTypingForPublicDialingPlan() {
        return allowEnterpriseGroupCallTypingForPublicDialingPlan;
    }

    /**
     * Legt den Wert der allowEnterpriseGroupCallTypingForPublicDialingPlan-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowEnterpriseGroupCallTypingForPublicDialingPlan(Boolean value) {
        this.allowEnterpriseGroupCallTypingForPublicDialingPlan = value;
    }

    /**
     * Ruft den Wert der overrideCLIDRestrictionForPrivateCallCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOverrideCLIDRestrictionForPrivateCallCategory() {
        return overrideCLIDRestrictionForPrivateCallCategory;
    }

    /**
     * Legt den Wert der overrideCLIDRestrictionForPrivateCallCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverrideCLIDRestrictionForPrivateCallCategory(Boolean value) {
        this.overrideCLIDRestrictionForPrivateCallCategory = value;
    }

    /**
     * Ruft den Wert der useEnterpriseCLIDForPrivateCallCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseEnterpriseCLIDForPrivateCallCategory() {
        return useEnterpriseCLIDForPrivateCallCategory;
    }

    /**
     * Legt den Wert der useEnterpriseCLIDForPrivateCallCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseEnterpriseCLIDForPrivateCallCategory(Boolean value) {
        this.useEnterpriseCLIDForPrivateCallCategory = value;
    }

    /**
     * Ruft den Wert der enableEnterpriseExtensionDialing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableEnterpriseExtensionDialing() {
        return enableEnterpriseExtensionDialing;
    }

    /**
     * Legt den Wert der enableEnterpriseExtensionDialing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableEnterpriseExtensionDialing(Boolean value) {
        this.enableEnterpriseExtensionDialing = value;
    }

    /**
     * Ruft den Wert der useMaxConcurrentRedirectedCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxConcurrentRedirectedCalls() {
        return useMaxConcurrentRedirectedCalls;
    }

    /**
     * Legt den Wert der useMaxConcurrentRedirectedCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxConcurrentRedirectedCalls(Boolean value) {
        this.useMaxConcurrentRedirectedCalls = value;
    }

    /**
     * Ruft den Wert der maxConcurrentRedirectedCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxConcurrentRedirectedCalls() {
        return maxConcurrentRedirectedCalls;
    }

    /**
     * Legt den Wert der maxConcurrentRedirectedCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxConcurrentRedirectedCalls(Integer value) {
        this.maxConcurrentRedirectedCalls = value;
    }

    /**
     * Ruft den Wert der useMaxFindMeFollowMeDepth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxFindMeFollowMeDepth() {
        return useMaxFindMeFollowMeDepth;
    }

    /**
     * Legt den Wert der useMaxFindMeFollowMeDepth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxFindMeFollowMeDepth(Boolean value) {
        this.useMaxFindMeFollowMeDepth = value;
    }

    /**
     * Ruft den Wert der maxFindMeFollowMeDepth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxFindMeFollowMeDepth() {
        return maxFindMeFollowMeDepth;
    }

    /**
     * Legt den Wert der maxFindMeFollowMeDepth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxFindMeFollowMeDepth(Integer value) {
        this.maxFindMeFollowMeDepth = value;
    }

    /**
     * Ruft den Wert der maxRedirectionDepth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxRedirectionDepth() {
        return maxRedirectionDepth;
    }

    /**
     * Legt den Wert der maxRedirectionDepth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxRedirectionDepth(Integer value) {
        this.maxRedirectionDepth = value;
    }

    /**
     * Ruft den Wert der useMaxConcurrentFindMeFollowMeInvocations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxConcurrentFindMeFollowMeInvocations() {
        return useMaxConcurrentFindMeFollowMeInvocations;
    }

    /**
     * Legt den Wert der useMaxConcurrentFindMeFollowMeInvocations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxConcurrentFindMeFollowMeInvocations(Boolean value) {
        this.useMaxConcurrentFindMeFollowMeInvocations = value;
    }

    /**
     * Ruft den Wert der maxConcurrentFindMeFollowMeInvocations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxConcurrentFindMeFollowMeInvocations() {
        return maxConcurrentFindMeFollowMeInvocations;
    }

    /**
     * Legt den Wert der maxConcurrentFindMeFollowMeInvocations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxConcurrentFindMeFollowMeInvocations(Integer value) {
        this.maxConcurrentFindMeFollowMeInvocations = value;
    }

    /**
     * Ruft den Wert der clidPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public GroupCLIDPolicy getClidPolicy() {
        return clidPolicy;
    }

    /**
     * Legt den Wert der clidPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public void setClidPolicy(GroupCLIDPolicy value) {
        this.clidPolicy = value;
    }

    /**
     * Ruft den Wert der emergencyClidPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public GroupCLIDPolicy getEmergencyClidPolicy() {
        return emergencyClidPolicy;
    }

    /**
     * Legt den Wert der emergencyClidPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public void setEmergencyClidPolicy(GroupCLIDPolicy value) {
        this.emergencyClidPolicy = value;
    }

    /**
     * Ruft den Wert der allowAlternateNumbersForRedirectingIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowAlternateNumbersForRedirectingIdentity() {
        return allowAlternateNumbersForRedirectingIdentity;
    }

    /**
     * Legt den Wert der allowAlternateNumbersForRedirectingIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowAlternateNumbersForRedirectingIdentity(Boolean value) {
        this.allowAlternateNumbersForRedirectingIdentity = value;
    }

    /**
     * Ruft den Wert der useGroupName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseGroupName() {
        return useGroupName;
    }

    /**
     * Legt den Wert der useGroupName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseGroupName(Boolean value) {
        this.useGroupName = value;
    }

    /**
     * Ruft den Wert der blockCallingNameForExternalCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBlockCallingNameForExternalCalls() {
        return blockCallingNameForExternalCalls;
    }

    /**
     * Legt den Wert der blockCallingNameForExternalCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBlockCallingNameForExternalCalls(Boolean value) {
        this.blockCallingNameForExternalCalls = value;
    }

    /**
     * Ruft den Wert der enableDialableCallerID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableDialableCallerID() {
        return enableDialableCallerID;
    }

    /**
     * Legt den Wert der enableDialableCallerID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableDialableCallerID(Boolean value) {
        this.enableDialableCallerID = value;
    }

    /**
     * Ruft den Wert der allowConfigurableCLIDForRedirectingIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowConfigurableCLIDForRedirectingIdentity() {
        return allowConfigurableCLIDForRedirectingIdentity;
    }

    /**
     * Legt den Wert der allowConfigurableCLIDForRedirectingIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowConfigurableCLIDForRedirectingIdentity(Boolean value) {
        this.allowConfigurableCLIDForRedirectingIdentity = value;
    }

    /**
     * Ruft den Wert der allowDepartmentCLIDNameOverride-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowDepartmentCLIDNameOverride() {
        return allowDepartmentCLIDNameOverride;
    }

    /**
     * Legt den Wert der allowDepartmentCLIDNameOverride-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowDepartmentCLIDNameOverride(Boolean value) {
        this.allowDepartmentCLIDNameOverride = value;
    }

    /**
     * Ruft den Wert der enterpriseCallsCLIDPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public EnterpriseInternalCallsCLIDPolicy getEnterpriseCallsCLIDPolicy() {
        return enterpriseCallsCLIDPolicy;
    }

    /**
     * Legt den Wert der enterpriseCallsCLIDPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public void setEnterpriseCallsCLIDPolicy(EnterpriseInternalCallsCLIDPolicy value) {
        this.enterpriseCallsCLIDPolicy = value;
    }

    /**
     * Ruft den Wert der groupCallsCLIDPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public EnterpriseInternalCallsCLIDPolicy getGroupCallsCLIDPolicy() {
        return groupCallsCLIDPolicy;
    }

    /**
     * Legt den Wert der groupCallsCLIDPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public void setGroupCallsCLIDPolicy(EnterpriseInternalCallsCLIDPolicy value) {
        this.groupCallsCLIDPolicy = value;
    }

    /**
     * Ruft den Wert der useGroupPhoneListLookupSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseGroupPhoneListLookupSetting() {
        return useGroupPhoneListLookupSetting;
    }

    /**
     * Legt den Wert der useGroupPhoneListLookupSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseGroupPhoneListLookupSetting(Boolean value) {
        this.useGroupPhoneListLookupSetting = value;
    }

    /**
     * Ruft den Wert der enablePhoneListLookup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnablePhoneListLookup() {
        return enablePhoneListLookup;
    }

    /**
     * Legt den Wert der enablePhoneListLookup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnablePhoneListLookup(Boolean value) {
        this.enablePhoneListLookup = value;
    }

    /**
     * Ruft den Wert der useMaxConcurrentTerminatingAlertingRequests-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxConcurrentTerminatingAlertingRequests() {
        return useMaxConcurrentTerminatingAlertingRequests;
    }

    /**
     * Legt den Wert der useMaxConcurrentTerminatingAlertingRequests-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxConcurrentTerminatingAlertingRequests(Boolean value) {
        this.useMaxConcurrentTerminatingAlertingRequests = value;
    }

    /**
     * Ruft den Wert der maxConcurrentTerminatingAlertingRequests-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxConcurrentTerminatingAlertingRequests() {
        return maxConcurrentTerminatingAlertingRequests;
    }

    /**
     * Legt den Wert der maxConcurrentTerminatingAlertingRequests-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxConcurrentTerminatingAlertingRequests(Integer value) {
        this.maxConcurrentTerminatingAlertingRequests = value;
    }

    /**
     * Ruft den Wert der includeRedirectionsInMaximumNumberOfConcurrentCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeRedirectionsInMaximumNumberOfConcurrentCalls() {
        return includeRedirectionsInMaximumNumberOfConcurrentCalls;
    }

    /**
     * Legt den Wert der includeRedirectionsInMaximumNumberOfConcurrentCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeRedirectionsInMaximumNumberOfConcurrentCalls(Boolean value) {
        this.includeRedirectionsInMaximumNumberOfConcurrentCalls = value;
    }

    /**
     * Ruft den Wert der useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable() {
        return useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable;
    }

    /**
     * Legt den Wert der useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable(Boolean value) {
        this.useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable = value;
    }

    /**
     * Ruft den Wert der useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable() {
        return useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable;
    }

    /**
     * Legt den Wert der useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable(Boolean value) {
        this.useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable = value;
    }

    /**
     * Ruft den Wert der routeOverrideDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRouteOverrideDomain() {
        return routeOverrideDomain;
    }

    /**
     * Legt den Wert der routeOverrideDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRouteOverrideDomain(JAXBElement<String> value) {
        this.routeOverrideDomain = value;
    }

    /**
     * Ruft den Wert der routeOverridePrefix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRouteOverridePrefix() {
        return routeOverridePrefix;
    }

    /**
     * Legt den Wert der routeOverridePrefix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRouteOverridePrefix(JAXBElement<String> value) {
        this.routeOverridePrefix = value;
    }

    /**
     * Ruft den Wert der allowMobileDNForRedirectingIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowMobileDNForRedirectingIdentity() {
        return allowMobileDNForRedirectingIdentity;
    }

    /**
     * Legt den Wert der allowMobileDNForRedirectingIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowMobileDNForRedirectingIdentity(Boolean value) {
        this.allowMobileDNForRedirectingIdentity = value;
    }

    /**
     * Ruft den Wert der enableGatewayRoutePolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableGatewayRoutePolicy() {
        return enableGatewayRoutePolicy;
    }

    /**
     * Legt den Wert der enableGatewayRoutePolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableGatewayRoutePolicy(Boolean value) {
        this.enableGatewayRoutePolicy = value;
    }

    /**
     * Ruft den Wert der networkCallsGatewayRouteIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNetworkCallsGatewayRouteIdentity() {
        return networkCallsGatewayRouteIdentity;
    }

    /**
     * Legt den Wert der networkCallsGatewayRouteIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNetworkCallsGatewayRouteIdentity(JAXBElement<String> value) {
        this.networkCallsGatewayRouteIdentity = value;
    }

    /**
     * Ruft den Wert der networkURLCallsGatewayRouteIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNetworkURLCallsGatewayRouteIdentity() {
        return networkURLCallsGatewayRouteIdentity;
    }

    /**
     * Legt den Wert der networkURLCallsGatewayRouteIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNetworkURLCallsGatewayRouteIdentity(JAXBElement<String> value) {
        this.networkURLCallsGatewayRouteIdentity = value;
    }

    /**
     * Ruft den Wert der emergencyCallsGatewayRouteIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmergencyCallsGatewayRouteIdentity() {
        return emergencyCallsGatewayRouteIdentity;
    }

    /**
     * Legt den Wert der emergencyCallsGatewayRouteIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmergencyCallsGatewayRouteIdentity(JAXBElement<String> value) {
        this.emergencyCallsGatewayRouteIdentity = value;
    }

    /**
     * Ruft den Wert der repairCallsGatewayRouteIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepairCallsGatewayRouteIdentity() {
        return repairCallsGatewayRouteIdentity;
    }

    /**
     * Legt den Wert der repairCallsGatewayRouteIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepairCallsGatewayRouteIdentity(JAXBElement<String> value) {
        this.repairCallsGatewayRouteIdentity = value;
    }

    /**
     * Ruft den Wert der callTypingErrorsGatewayRouteIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallTypingErrorsGatewayRouteIdentity() {
        return callTypingErrorsGatewayRouteIdentity;
    }

    /**
     * Legt den Wert der callTypingErrorsGatewayRouteIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallTypingErrorsGatewayRouteIdentity(JAXBElement<String> value) {
        this.callTypingErrorsGatewayRouteIdentity = value;
    }

}
