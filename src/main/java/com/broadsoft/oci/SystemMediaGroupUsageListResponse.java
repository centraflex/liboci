//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemMediaGroupUsageListRequest.
 *         Contains a table of SAC groups associated with the media group.
 *         The column headings are: "SAC Group Name", "Organization Id", "Organization Type", and "Group Id".
 *         The "Group Id" will be empty for enterprise SAC groups. 
 *       
 * 
 * <p>Java-Klasse für SystemMediaGroupUsageListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemMediaGroupUsageListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="usageTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemMediaGroupUsageListResponse", propOrder = {
    "usageTable"
})
public class SystemMediaGroupUsageListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable usageTable;

    /**
     * Ruft den Wert der usageTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUsageTable() {
        return usageTable;
    }

    /**
     * Legt den Wert der usageTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUsageTable(OCITable value) {
        this.usageTable = value;
    }

}
