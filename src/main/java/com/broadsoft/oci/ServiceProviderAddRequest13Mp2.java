//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a service provider or enterprise.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         The following elements are only used in Amplify data mode and ignored in AS and XS data mode:
 *         servicePolicy,
 *         callProcessingSliceId,
 *         provisioningSliceId,
 *         subscriberPartition.
 *         When the callProcessingSliceId or provisioningSliceId is not specified in the AmplifyDataMode, 
 *         the default slice Id is assigned to the service provider.
 *         Only Provisioning admin and above can change the callProcessingSliceId,  provisioningSliceId, andsubscriberPartition.
 *         
 *         The following elements are only used in Amplify and XS data mode and ignored in AS data mode:
 *         preferredDataCenter.
 *         Only Provisioning admin and above can change the preferredDataCenter.     
 *         
 *         The following data elements are only used in AS data mode:
 *           resellerId        
 *         If reseller administrator sends the request, resellerId is ignored. The reseller administrator's reseller id is used.
 *         Replaced by: ServiceProviderConsolidatedAddRequest in AS data mode.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderAddRequest13mp2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderAddRequest13mp2">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <choice>
 *           <element name="isEnterprise" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *           <element name="useCustomRoutingProfile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         </choice>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="defaultDomain" type="{}NetAddress"/>
 *         <element name="serviceProviderName" type="{}ServiceProviderName" minOccurs="0"/>
 *         <element name="supportEmail" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="contact" type="{}Contact" minOccurs="0"/>
 *         <element name="address" type="{}StreetAddress" minOccurs="0"/>
 *         <element name="servicePolicy" type="{}ServicePolicyName" minOccurs="0"/>
 *         <element name="callProcessingSliceId" type="{}SliceId" minOccurs="0"/>
 *         <element name="provisioningSliceId" type="{}SliceId" minOccurs="0"/>
 *         <element name="subscriberPartition" type="{}SubscriberPartition" minOccurs="0"/>
 *         <element name="preferredDataCenter" type="{}DataCenter" minOccurs="0"/>
 *         <element name="resellerId" type="{}ResellerId22" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderAddRequest13mp2", propOrder = {
    "isEnterprise",
    "useCustomRoutingProfile",
    "serviceProviderId",
    "defaultDomain",
    "serviceProviderName",
    "supportEmail",
    "contact",
    "address",
    "servicePolicy",
    "callProcessingSliceId",
    "provisioningSliceId",
    "subscriberPartition",
    "preferredDataCenter",
    "resellerId"
})
public class ServiceProviderAddRequest13Mp2
    extends OCIRequest
{

    protected Boolean isEnterprise;
    protected Boolean useCustomRoutingProfile;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultDomain;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String supportEmail;
    protected Contact contact;
    protected StreetAddress address;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String servicePolicy;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callProcessingSliceId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String provisioningSliceId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String subscriberPartition;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String preferredDataCenter;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;

    /**
     * Ruft den Wert der isEnterprise-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsEnterprise() {
        return isEnterprise;
    }

    /**
     * Legt den Wert der isEnterprise-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsEnterprise(Boolean value) {
        this.isEnterprise = value;
    }

    /**
     * Ruft den Wert der useCustomRoutingProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseCustomRoutingProfile() {
        return useCustomRoutingProfile;
    }

    /**
     * Legt den Wert der useCustomRoutingProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseCustomRoutingProfile(Boolean value) {
        this.useCustomRoutingProfile = value;
    }

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der defaultDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultDomain() {
        return defaultDomain;
    }

    /**
     * Legt den Wert der defaultDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultDomain(String value) {
        this.defaultDomain = value;
    }

    /**
     * Ruft den Wert der serviceProviderName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderName() {
        return serviceProviderName;
    }

    /**
     * Legt den Wert der serviceProviderName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderName(String value) {
        this.serviceProviderName = value;
    }

    /**
     * Ruft den Wert der supportEmail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupportEmail() {
        return supportEmail;
    }

    /**
     * Legt den Wert der supportEmail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupportEmail(String value) {
        this.supportEmail = value;
    }

    /**
     * Ruft den Wert der contact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Contact }
     *     
     */
    public Contact getContact() {
        return contact;
    }

    /**
     * Legt den Wert der contact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Contact }
     *     
     */
    public void setContact(Contact value) {
        this.contact = value;
    }

    /**
     * Ruft den Wert der address-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StreetAddress }
     *     
     */
    public StreetAddress getAddress() {
        return address;
    }

    /**
     * Legt den Wert der address-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetAddress }
     *     
     */
    public void setAddress(StreetAddress value) {
        this.address = value;
    }

    /**
     * Ruft den Wert der servicePolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicePolicy() {
        return servicePolicy;
    }

    /**
     * Legt den Wert der servicePolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicePolicy(String value) {
        this.servicePolicy = value;
    }

    /**
     * Ruft den Wert der callProcessingSliceId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallProcessingSliceId() {
        return callProcessingSliceId;
    }

    /**
     * Legt den Wert der callProcessingSliceId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallProcessingSliceId(String value) {
        this.callProcessingSliceId = value;
    }

    /**
     * Ruft den Wert der provisioningSliceId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvisioningSliceId() {
        return provisioningSliceId;
    }

    /**
     * Legt den Wert der provisioningSliceId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvisioningSliceId(String value) {
        this.provisioningSliceId = value;
    }

    /**
     * Ruft den Wert der subscriberPartition-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberPartition() {
        return subscriberPartition;
    }

    /**
     * Legt den Wert der subscriberPartition-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberPartition(String value) {
        this.subscriberPartition = value;
    }

    /**
     * Ruft den Wert der preferredDataCenter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredDataCenter() {
        return preferredDataCenter;
    }

    /**
     * Legt den Wert der preferredDataCenter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredDataCenter(String value) {
        this.preferredDataCenter = value;
    }

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

}
