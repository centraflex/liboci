//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains the music on hold user source configuration.
 *       
 * 
 * <p>Java-Klasse für MusicOnHoldUserSourceModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="MusicOnHoldUserSourceModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="messageSourceSelection" type="{}MusicOnHoldUserMessageSelection" minOccurs="0"/>
 *         <element name="customSource" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="audioFile" type="{}LabeledFileResource" minOccurs="0"/>
 *                   <element name="videoFile" type="{}LabeledFileResource" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MusicOnHoldUserSourceModify", propOrder = {
    "messageSourceSelection",
    "customSource"
})
public class MusicOnHoldUserSourceModify {

    @XmlSchemaType(name = "token")
    protected MusicOnHoldUserMessageSelection messageSourceSelection;
    protected MusicOnHoldUserSourceModify.CustomSource customSource;

    /**
     * Ruft den Wert der messageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldUserMessageSelection }
     *     
     */
    public MusicOnHoldUserMessageSelection getMessageSourceSelection() {
        return messageSourceSelection;
    }

    /**
     * Legt den Wert der messageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldUserMessageSelection }
     *     
     */
    public void setMessageSourceSelection(MusicOnHoldUserMessageSelection value) {
        this.messageSourceSelection = value;
    }

    /**
     * Ruft den Wert der customSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldUserSourceModify.CustomSource }
     *     
     */
    public MusicOnHoldUserSourceModify.CustomSource getCustomSource() {
        return customSource;
    }

    /**
     * Legt den Wert der customSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldUserSourceModify.CustomSource }
     *     
     */
    public void setCustomSource(MusicOnHoldUserSourceModify.CustomSource value) {
        this.customSource = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="audioFile" type="{}LabeledFileResource" minOccurs="0"/>
     *         <element name="videoFile" type="{}LabeledFileResource" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "audioFile",
        "videoFile"
    })
    public static class CustomSource {

        protected LabeledFileResource audioFile;
        protected LabeledFileResource videoFile;

        /**
         * Ruft den Wert der audioFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link LabeledFileResource }
         *     
         */
        public LabeledFileResource getAudioFile() {
            return audioFile;
        }

        /**
         * Legt den Wert der audioFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link LabeledFileResource }
         *     
         */
        public void setAudioFile(LabeledFileResource value) {
            this.audioFile = value;
        }

        /**
         * Ruft den Wert der videoFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link LabeledFileResource }
         *     
         */
        public LabeledFileResource getVideoFile() {
            return videoFile;
        }

        /**
         * Legt den Wert der videoFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link LabeledFileResource }
         *     
         */
        public void setVideoFile(LabeledFileResource value) {
            this.videoFile = value;
        }

    }

}
