//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserNetworkConferencingGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserNetworkConferencingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserNetworkConferencingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="conferenceURI" type="{}SIPURI" minOccurs="0"/>
 *         <element name="maxConferenceParties" type="{}CallProcessingMaxConferenceParties"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserNetworkConferencingGetResponse", propOrder = {
    "conferenceURI",
    "maxConferenceParties"
})
public class UserNetworkConferencingGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String conferenceURI;
    protected int maxConferenceParties;

    /**
     * Ruft den Wert der conferenceURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConferenceURI() {
        return conferenceURI;
    }

    /**
     * Legt den Wert der conferenceURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConferenceURI(String value) {
        this.conferenceURI = value;
    }

    /**
     * Ruft den Wert der maxConferenceParties-Eigenschaft ab.
     * 
     */
    public int getMaxConferenceParties() {
        return maxConferenceParties;
    }

    /**
     * Legt den Wert der maxConferenceParties-Eigenschaft fest.
     * 
     */
    public void setMaxConferenceParties(int value) {
        this.maxConferenceParties = value;
    }

}
