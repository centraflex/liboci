//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Communication Barring Originating Rule
 *       
 * 
 * <p>Java-Klasse für CommunicationBarringOriginatingRule complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CommunicationBarringOriginatingRule">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="criteria" type="{}CommunicationBarringCriteriaName"/>
 *         <element name="action" type="{}CommunicationBarringOriginatingAction"/>
 *         <element name="treatmentId" type="{}TreatmentId" minOccurs="0"/>
 *         <element name="transferNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="callTimeoutSeconds" type="{}CommunicationBarringTimeoutSeconds" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommunicationBarringOriginatingRule", propOrder = {
    "criteria",
    "action",
    "treatmentId",
    "transferNumber",
    "callTimeoutSeconds"
})
public class CommunicationBarringOriginatingRule {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String criteria;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CommunicationBarringOriginatingAction action;
    @XmlElementRef(name = "treatmentId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> treatmentId;
    @XmlElementRef(name = "transferNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transferNumber;
    @XmlElementRef(name = "callTimeoutSeconds", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> callTimeoutSeconds;

    /**
     * Ruft den Wert der criteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCriteria() {
        return criteria;
    }

    /**
     * Legt den Wert der criteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCriteria(String value) {
        this.criteria = value;
    }

    /**
     * Ruft den Wert der action-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringOriginatingAction }
     *     
     */
    public CommunicationBarringOriginatingAction getAction() {
        return action;
    }

    /**
     * Legt den Wert der action-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringOriginatingAction }
     *     
     */
    public void setAction(CommunicationBarringOriginatingAction value) {
        this.action = value;
    }

    /**
     * Ruft den Wert der treatmentId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTreatmentId() {
        return treatmentId;
    }

    /**
     * Legt den Wert der treatmentId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTreatmentId(JAXBElement<String> value) {
        this.treatmentId = value;
    }

    /**
     * Ruft den Wert der transferNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransferNumber() {
        return transferNumber;
    }

    /**
     * Legt den Wert der transferNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransferNumber(JAXBElement<String> value) {
        this.transferNumber = value;
    }

    /**
     * Ruft den Wert der callTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCallTimeoutSeconds() {
        return callTimeoutSeconds;
    }

    /**
     * Legt den Wert der callTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCallTimeoutSeconds(JAXBElement<Integer> value) {
        this.callTimeoutSeconds = value;
    }

}
