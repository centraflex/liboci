//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify data for a group or department Music On Hold Instance.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupMusicOnHoldModifyInstanceRequest14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupMusicOnHoldModifyInstanceRequest14">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="department" type="{}DepartmentKey" minOccurs="0"/>
 *         <element name="isActiveDuringCallHold" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="isActiveDuringCallPark" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="isActiveDuringBusyCampOn" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="messageSelection" type="{}MusicOnHoldMessageSelection" minOccurs="0"/>
 *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointModify" minOccurs="0"/>
 *         <element name="audioFile" type="{}LabeledFileResource" minOccurs="0"/>
 *         <element name="videoFile" type="{}LabeledFileResource" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupMusicOnHoldModifyInstanceRequest14", propOrder = {
    "serviceProviderId",
    "groupId",
    "department",
    "isActiveDuringCallHold",
    "isActiveDuringCallPark",
    "isActiveDuringBusyCampOn",
    "messageSelection",
    "accessDeviceEndpoint",
    "audioFile",
    "videoFile"
})
public class GroupMusicOnHoldModifyInstanceRequest14
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected DepartmentKey department;
    protected Boolean isActiveDuringCallHold;
    protected Boolean isActiveDuringCallPark;
    protected Boolean isActiveDuringBusyCampOn;
    @XmlSchemaType(name = "token")
    protected MusicOnHoldMessageSelection messageSelection;
    @XmlElementRef(name = "accessDeviceEndpoint", type = JAXBElement.class, required = false)
    protected JAXBElement<AccessDeviceEndpointModify> accessDeviceEndpoint;
    protected LabeledFileResource audioFile;
    protected LabeledFileResource videoFile;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der department-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DepartmentKey }
     *     
     */
    public DepartmentKey getDepartment() {
        return department;
    }

    /**
     * Legt den Wert der department-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DepartmentKey }
     *     
     */
    public void setDepartment(DepartmentKey value) {
        this.department = value;
    }

    /**
     * Ruft den Wert der isActiveDuringCallHold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActiveDuringCallHold() {
        return isActiveDuringCallHold;
    }

    /**
     * Legt den Wert der isActiveDuringCallHold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActiveDuringCallHold(Boolean value) {
        this.isActiveDuringCallHold = value;
    }

    /**
     * Ruft den Wert der isActiveDuringCallPark-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActiveDuringCallPark() {
        return isActiveDuringCallPark;
    }

    /**
     * Legt den Wert der isActiveDuringCallPark-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActiveDuringCallPark(Boolean value) {
        this.isActiveDuringCallPark = value;
    }

    /**
     * Ruft den Wert der isActiveDuringBusyCampOn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActiveDuringBusyCampOn() {
        return isActiveDuringBusyCampOn;
    }

    /**
     * Legt den Wert der isActiveDuringBusyCampOn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActiveDuringBusyCampOn(Boolean value) {
        this.isActiveDuringBusyCampOn = value;
    }

    /**
     * Ruft den Wert der messageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public MusicOnHoldMessageSelection getMessageSelection() {
        return messageSelection;
    }

    /**
     * Legt den Wert der messageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public void setMessageSelection(MusicOnHoldMessageSelection value) {
        this.messageSelection = value;
    }

    /**
     * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceEndpointModify }{@code >}
     *     
     */
    public JAXBElement<AccessDeviceEndpointModify> getAccessDeviceEndpoint() {
        return accessDeviceEndpoint;
    }

    /**
     * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceEndpointModify }{@code >}
     *     
     */
    public void setAccessDeviceEndpoint(JAXBElement<AccessDeviceEndpointModify> value) {
        this.accessDeviceEndpoint = value;
    }

    /**
     * Ruft den Wert der audioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledFileResource }
     *     
     */
    public LabeledFileResource getAudioFile() {
        return audioFile;
    }

    /**
     * Legt den Wert der audioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledFileResource }
     *     
     */
    public void setAudioFile(LabeledFileResource value) {
        this.audioFile = value;
    }

    /**
     * Ruft den Wert der videoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledFileResource }
     *     
     */
    public LabeledFileResource getVideoFile() {
        return videoFile;
    }

    /**
     * Legt den Wert der videoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledFileResource }
     *     
     */
    public void setVideoFile(LabeledFileResource value) {
        this.videoFile = value;
    }

}
