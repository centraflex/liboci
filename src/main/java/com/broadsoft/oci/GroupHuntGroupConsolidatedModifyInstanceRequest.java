//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a Hunt Group instance.
 *         
 *         When phone numbers are un-assigned from the user, the unused numbers may be un-assigned from the group and service provider. If UnassignPhoneNumbersLevel is set to 'Group', the user's primary phone number, fax number and any alternate numbers, will be un-assigned from the group if the command is executed by a service provider administrator or above.
 *         When set to 'Service Provider', they will be un-assigned from the group and service provider if the command is executed by a provisioning administrator or above.
 *         When omitted, the number(s) will be left assigned to the group.
 *         An ErrorResponse will be returned if any number cannot be unassigned because of insufficient privilege.
 *         
 *         If the phoneNumber has not been assigned to the group and addPhoneNumberToGroup is set to true, it will be added to group if needed if the command is executed by a service provider administrator and above. The command will fail otherwise.
 *         
 *         The response is either SuccessResponse or ErrorResponse.
 *         The following elements are only used in AS data mode:
 *            useSystemHuntGroupCLIDSetting
 *            includeHuntGroupNameInCLID
 *       
 * 
 * <p>Java-Klasse für GroupHuntGroupConsolidatedModifyInstanceRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupHuntGroupConsolidatedModifyInstanceRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="unassignPhoneNumbers" type="{}UnassignPhoneNumbersLevel" minOccurs="0"/>
 *         <element name="addPhoneNumberToGroup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceModifyProfile" minOccurs="0"/>
 *         <element name="policy" type="{}HuntPolicy" minOccurs="0"/>
 *         <element name="huntAfterNoAnswer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="noAnswerNumberOfRings" type="{}HuntNoAnswerRings" minOccurs="0"/>
 *         <element name="forwardAfterTimeout" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forwardTimeoutSeconds" type="{}HuntForwardTimeoutSeconds" minOccurs="0"/>
 *         <element name="forwardToPhoneNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <choice>
 *           <element name="agentUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *           <element name="agentWeightList" type="{}ReplacementAgentWeightList" minOccurs="0"/>
 *         </choice>
 *         <element name="allowCallWaitingForAgents" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useSystemHuntGroupCLIDSetting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeHuntGroupNameInCLID" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableNotReachableForwarding" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="notReachableForwardToPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="makeBusyWhenNotReachable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowMembersToControlGroupBusy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableGroupBusy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="applyGroupBusyWhenTerminatingToAgent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="serviceList" type="{}ReplacementConsolidatedUserServiceAssignmentList" minOccurs="0"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="directoryNumberHuntingAgentUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *         <element name="directoryNumberHuntingUseTerminateCallToAgentFirst" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="directoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupHuntGroupConsolidatedModifyInstanceRequest", propOrder = {
    "serviceUserId",
    "unassignPhoneNumbers",
    "addPhoneNumberToGroup",
    "serviceInstanceProfile",
    "policy",
    "huntAfterNoAnswer",
    "noAnswerNumberOfRings",
    "forwardAfterTimeout",
    "forwardTimeoutSeconds",
    "forwardToPhoneNumber",
    "agentUserIdList",
    "agentWeightList",
    "allowCallWaitingForAgents",
    "useSystemHuntGroupCLIDSetting",
    "includeHuntGroupNameInCLID",
    "enableNotReachableForwarding",
    "notReachableForwardToPhoneNumber",
    "makeBusyWhenNotReachable",
    "allowMembersToControlGroupBusy",
    "enableGroupBusy",
    "applyGroupBusyWhenTerminatingToAgent",
    "networkClassOfService",
    "serviceList",
    "isActive",
    "directoryNumberHuntingAgentUserIdList",
    "directoryNumberHuntingUseTerminateCallToAgentFirst",
    "directoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls"
})
public class GroupHuntGroupConsolidatedModifyInstanceRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlSchemaType(name = "token")
    protected UnassignPhoneNumbersLevel unassignPhoneNumbers;
    protected Boolean addPhoneNumberToGroup;
    protected ServiceInstanceModifyProfile serviceInstanceProfile;
    @XmlSchemaType(name = "token")
    protected HuntPolicy policy;
    protected Boolean huntAfterNoAnswer;
    protected Integer noAnswerNumberOfRings;
    protected Boolean forwardAfterTimeout;
    protected Integer forwardTimeoutSeconds;
    @XmlElementRef(name = "forwardToPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forwardToPhoneNumber;
    @XmlElementRef(name = "agentUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> agentUserIdList;
    @XmlElementRef(name = "agentWeightList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementAgentWeightList> agentWeightList;
    protected Boolean allowCallWaitingForAgents;
    protected Boolean useSystemHuntGroupCLIDSetting;
    protected Boolean includeHuntGroupNameInCLID;
    protected Boolean enableNotReachableForwarding;
    @XmlElementRef(name = "notReachableForwardToPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> notReachableForwardToPhoneNumber;
    protected Boolean makeBusyWhenNotReachable;
    protected Boolean allowMembersToControlGroupBusy;
    protected Boolean enableGroupBusy;
    protected Boolean applyGroupBusyWhenTerminatingToAgent;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    @XmlElementRef(name = "serviceList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementConsolidatedUserServiceAssignmentList> serviceList;
    protected Boolean isActive;
    @XmlElementRef(name = "directoryNumberHuntingAgentUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> directoryNumberHuntingAgentUserIdList;
    protected Boolean directoryNumberHuntingUseTerminateCallToAgentFirst;
    protected Boolean directoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der unassignPhoneNumbers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnassignPhoneNumbersLevel }
     *     
     */
    public UnassignPhoneNumbersLevel getUnassignPhoneNumbers() {
        return unassignPhoneNumbers;
    }

    /**
     * Legt den Wert der unassignPhoneNumbers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnassignPhoneNumbersLevel }
     *     
     */
    public void setUnassignPhoneNumbers(UnassignPhoneNumbersLevel value) {
        this.unassignPhoneNumbers = value;
    }

    /**
     * Ruft den Wert der addPhoneNumberToGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAddPhoneNumberToGroup() {
        return addPhoneNumberToGroup;
    }

    /**
     * Legt den Wert der addPhoneNumberToGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAddPhoneNumberToGroup(Boolean value) {
        this.addPhoneNumberToGroup = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public ServiceInstanceModifyProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceModifyProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der policy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HuntPolicy }
     *     
     */
    public HuntPolicy getPolicy() {
        return policy;
    }

    /**
     * Legt den Wert der policy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HuntPolicy }
     *     
     */
    public void setPolicy(HuntPolicy value) {
        this.policy = value;
    }

    /**
     * Ruft den Wert der huntAfterNoAnswer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHuntAfterNoAnswer() {
        return huntAfterNoAnswer;
    }

    /**
     * Legt den Wert der huntAfterNoAnswer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHuntAfterNoAnswer(Boolean value) {
        this.huntAfterNoAnswer = value;
    }

    /**
     * Ruft den Wert der noAnswerNumberOfRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNoAnswerNumberOfRings() {
        return noAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der noAnswerNumberOfRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNoAnswerNumberOfRings(Integer value) {
        this.noAnswerNumberOfRings = value;
    }

    /**
     * Ruft den Wert der forwardAfterTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForwardAfterTimeout() {
        return forwardAfterTimeout;
    }

    /**
     * Legt den Wert der forwardAfterTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForwardAfterTimeout(Boolean value) {
        this.forwardAfterTimeout = value;
    }

    /**
     * Ruft den Wert der forwardTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getForwardTimeoutSeconds() {
        return forwardTimeoutSeconds;
    }

    /**
     * Legt den Wert der forwardTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setForwardTimeoutSeconds(Integer value) {
        this.forwardTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der forwardToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForwardToPhoneNumber() {
        return forwardToPhoneNumber;
    }

    /**
     * Legt den Wert der forwardToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForwardToPhoneNumber(JAXBElement<String> value) {
        this.forwardToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der agentUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getAgentUserIdList() {
        return agentUserIdList;
    }

    /**
     * Legt den Wert der agentUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setAgentUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.agentUserIdList = value;
    }

    /**
     * Ruft den Wert der agentWeightList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementAgentWeightList }{@code >}
     *     
     */
    public JAXBElement<ReplacementAgentWeightList> getAgentWeightList() {
        return agentWeightList;
    }

    /**
     * Legt den Wert der agentWeightList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementAgentWeightList }{@code >}
     *     
     */
    public void setAgentWeightList(JAXBElement<ReplacementAgentWeightList> value) {
        this.agentWeightList = value;
    }

    /**
     * Ruft den Wert der allowCallWaitingForAgents-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowCallWaitingForAgents() {
        return allowCallWaitingForAgents;
    }

    /**
     * Legt den Wert der allowCallWaitingForAgents-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowCallWaitingForAgents(Boolean value) {
        this.allowCallWaitingForAgents = value;
    }

    /**
     * Ruft den Wert der useSystemHuntGroupCLIDSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseSystemHuntGroupCLIDSetting() {
        return useSystemHuntGroupCLIDSetting;
    }

    /**
     * Legt den Wert der useSystemHuntGroupCLIDSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSystemHuntGroupCLIDSetting(Boolean value) {
        this.useSystemHuntGroupCLIDSetting = value;
    }

    /**
     * Ruft den Wert der includeHuntGroupNameInCLID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeHuntGroupNameInCLID() {
        return includeHuntGroupNameInCLID;
    }

    /**
     * Legt den Wert der includeHuntGroupNameInCLID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeHuntGroupNameInCLID(Boolean value) {
        this.includeHuntGroupNameInCLID = value;
    }

    /**
     * Ruft den Wert der enableNotReachableForwarding-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableNotReachableForwarding() {
        return enableNotReachableForwarding;
    }

    /**
     * Legt den Wert der enableNotReachableForwarding-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableNotReachableForwarding(Boolean value) {
        this.enableNotReachableForwarding = value;
    }

    /**
     * Ruft den Wert der notReachableForwardToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNotReachableForwardToPhoneNumber() {
        return notReachableForwardToPhoneNumber;
    }

    /**
     * Legt den Wert der notReachableForwardToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNotReachableForwardToPhoneNumber(JAXBElement<String> value) {
        this.notReachableForwardToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der makeBusyWhenNotReachable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMakeBusyWhenNotReachable() {
        return makeBusyWhenNotReachable;
    }

    /**
     * Legt den Wert der makeBusyWhenNotReachable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMakeBusyWhenNotReachable(Boolean value) {
        this.makeBusyWhenNotReachable = value;
    }

    /**
     * Ruft den Wert der allowMembersToControlGroupBusy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowMembersToControlGroupBusy() {
        return allowMembersToControlGroupBusy;
    }

    /**
     * Legt den Wert der allowMembersToControlGroupBusy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowMembersToControlGroupBusy(Boolean value) {
        this.allowMembersToControlGroupBusy = value;
    }

    /**
     * Ruft den Wert der enableGroupBusy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableGroupBusy() {
        return enableGroupBusy;
    }

    /**
     * Legt den Wert der enableGroupBusy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableGroupBusy(Boolean value) {
        this.enableGroupBusy = value;
    }

    /**
     * Ruft den Wert der applyGroupBusyWhenTerminatingToAgent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isApplyGroupBusyWhenTerminatingToAgent() {
        return applyGroupBusyWhenTerminatingToAgent;
    }

    /**
     * Legt den Wert der applyGroupBusyWhenTerminatingToAgent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setApplyGroupBusyWhenTerminatingToAgent(Boolean value) {
        this.applyGroupBusyWhenTerminatingToAgent = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Ruft den Wert der serviceList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementConsolidatedUserServiceAssignmentList }{@code >}
     *     
     */
    public JAXBElement<ReplacementConsolidatedUserServiceAssignmentList> getServiceList() {
        return serviceList;
    }

    /**
     * Legt den Wert der serviceList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementConsolidatedUserServiceAssignmentList }{@code >}
     *     
     */
    public void setServiceList(JAXBElement<ReplacementConsolidatedUserServiceAssignmentList> value) {
        this.serviceList = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der directoryNumberHuntingAgentUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getDirectoryNumberHuntingAgentUserIdList() {
        return directoryNumberHuntingAgentUserIdList;
    }

    /**
     * Legt den Wert der directoryNumberHuntingAgentUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setDirectoryNumberHuntingAgentUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.directoryNumberHuntingAgentUserIdList = value;
    }

    /**
     * Ruft den Wert der directoryNumberHuntingUseTerminateCallToAgentFirst-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDirectoryNumberHuntingUseTerminateCallToAgentFirst() {
        return directoryNumberHuntingUseTerminateCallToAgentFirst;
    }

    /**
     * Legt den Wert der directoryNumberHuntingUseTerminateCallToAgentFirst-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDirectoryNumberHuntingUseTerminateCallToAgentFirst(Boolean value) {
        this.directoryNumberHuntingUseTerminateCallToAgentFirst = value;
    }

    /**
     * Ruft den Wert der directoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDirectoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls() {
        return directoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls;
    }

    /**
     * Legt den Wert der directoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDirectoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls(Boolean value) {
        this.directoryNumberHuntingUseOriginalAgentServicesForBusyAndNoAnswerCalls = value;
    }

}
