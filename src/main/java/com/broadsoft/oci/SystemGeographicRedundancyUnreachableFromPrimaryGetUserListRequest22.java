//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Get the list of users that are unreachable from the primary application server.
 *         The response is a SystemGeographicRedundancyUnreachableFromPrimaryGetUserListResponse22 or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemGeographicRedundancyUnreachableFromPrimaryGetUserListRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGeographicRedundancyUnreachableFromPrimaryGetUserListRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userListSizeLimit" type="{}MaximumReturnSize" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGeographicRedundancyUnreachableFromPrimaryGetUserListRequest22", propOrder = {
    "userListSizeLimit"
})
public class SystemGeographicRedundancyUnreachableFromPrimaryGetUserListRequest22
    extends OCIRequest
{

    protected Integer userListSizeLimit;

    /**
     * Ruft den Wert der userListSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUserListSizeLimit() {
        return userListSizeLimit;
    }

    /**
     * Legt den Wert der userListSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUserListSizeLimit(Integer value) {
        this.userListSizeLimit = value;
    }

}
