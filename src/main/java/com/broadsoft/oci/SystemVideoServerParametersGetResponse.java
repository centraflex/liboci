//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemVideoServerParametersGetRequest.
 *         Contains a list of system video server parameters.
 *       
 * 
 * <p>Java-Klasse für SystemVideoServerParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemVideoServerParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="videoServerResponseTimerMilliseconds" type="{}VideoServerResponseTimerMilliseconds"/>
 *         <element name="videoServerSelectionRouteTimerMilliseconds" type="{}VideoServerSelectionRouteTimerMilliseconds"/>
 *         <element name="useStaticVideoServerDevice" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemVideoServerParametersGetResponse", propOrder = {
    "videoServerResponseTimerMilliseconds",
    "videoServerSelectionRouteTimerMilliseconds",
    "useStaticVideoServerDevice"
})
public class SystemVideoServerParametersGetResponse
    extends OCIDataResponse
{

    protected int videoServerResponseTimerMilliseconds;
    protected int videoServerSelectionRouteTimerMilliseconds;
    protected boolean useStaticVideoServerDevice;

    /**
     * Ruft den Wert der videoServerResponseTimerMilliseconds-Eigenschaft ab.
     * 
     */
    public int getVideoServerResponseTimerMilliseconds() {
        return videoServerResponseTimerMilliseconds;
    }

    /**
     * Legt den Wert der videoServerResponseTimerMilliseconds-Eigenschaft fest.
     * 
     */
    public void setVideoServerResponseTimerMilliseconds(int value) {
        this.videoServerResponseTimerMilliseconds = value;
    }

    /**
     * Ruft den Wert der videoServerSelectionRouteTimerMilliseconds-Eigenschaft ab.
     * 
     */
    public int getVideoServerSelectionRouteTimerMilliseconds() {
        return videoServerSelectionRouteTimerMilliseconds;
    }

    /**
     * Legt den Wert der videoServerSelectionRouteTimerMilliseconds-Eigenschaft fest.
     * 
     */
    public void setVideoServerSelectionRouteTimerMilliseconds(int value) {
        this.videoServerSelectionRouteTimerMilliseconds = value;
    }

    /**
     * Ruft den Wert der useStaticVideoServerDevice-Eigenschaft ab.
     * 
     */
    public boolean isUseStaticVideoServerDevice() {
        return useStaticVideoServerDevice;
    }

    /**
     * Legt den Wert der useStaticVideoServerDevice-Eigenschaft fest.
     * 
     */
    public void setUseStaticVideoServerDevice(boolean value) {
        this.useStaticVideoServerDevice = value;
    }

}
