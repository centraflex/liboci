//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Authorize and assign a group service.
 * 
 *         The authorizedQuantity will be used at the group level if provided; otherwise, the service quantity will be set to unlimited. 
 *         The command will fail if the authorized quantity set at the service provider level is insufficient.
 *       
 * 
 * <p>Java-Klasse für ConsolidatedGroupServiceAssignment complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ConsolidatedGroupServiceAssignment">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="groupServiceName" type="{}GroupService"/>
 *         <element name="authorizedQuantity" type="{}UnboundedPositiveInt" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsolidatedGroupServiceAssignment", propOrder = {
    "groupServiceName",
    "authorizedQuantity"
})
public class ConsolidatedGroupServiceAssignment {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupService groupServiceName;
    protected UnboundedPositiveInt authorizedQuantity;

    /**
     * Ruft den Wert der groupServiceName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupService }
     *     
     */
    public GroupService getGroupServiceName() {
        return groupServiceName;
    }

    /**
     * Legt den Wert der groupServiceName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupService }
     *     
     */
    public void setGroupServiceName(GroupService value) {
        this.groupServiceName = value;
    }

    /**
     * Ruft den Wert der authorizedQuantity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public UnboundedPositiveInt getAuthorizedQuantity() {
        return authorizedQuantity;
    }

    /**
     * Legt den Wert der authorizedQuantity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public void setAuthorizedQuantity(UnboundedPositiveInt value) {
        this.authorizedQuantity = value;
    }

}
