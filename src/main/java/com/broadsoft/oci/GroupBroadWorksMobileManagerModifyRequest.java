//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the BroadWorks Mobile Manager settings.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupBroadWorksMobileManagerModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupBroadWorksMobileManagerModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="informationFile" type="{}BroadWorksMobileManagerInformationFile" minOccurs="0"/>
 *         <element name="certificateFile" type="{}BroadWorksMobileManagerCertificateFile" minOccurs="0"/>
 *         <element name="localToCarrier" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxTxPerSecondEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxTxPerSecond" type="{}BroadWorksMobileManagerMaxTxPerSecond" minOccurs="0"/>
 *         <element name="tldnEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="genericNumberEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="mobileStateCheckEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="locationBasedServicesEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupBroadWorksMobileManagerModifyRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "informationFile",
    "certificateFile",
    "localToCarrier",
    "maxTxPerSecondEnabled",
    "maxTxPerSecond",
    "tldnEnabled",
    "genericNumberEnabled",
    "mobileStateCheckEnabled",
    "locationBasedServicesEnabled"
})
public class GroupBroadWorksMobileManagerModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected byte[] informationFile;
    protected byte[] certificateFile;
    protected Boolean localToCarrier;
    protected Boolean maxTxPerSecondEnabled;
    protected Integer maxTxPerSecond;
    protected Boolean tldnEnabled;
    protected Boolean genericNumberEnabled;
    protected Boolean mobileStateCheckEnabled;
    protected Boolean locationBasedServicesEnabled;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der informationFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getInformationFile() {
        return informationFile;
    }

    /**
     * Legt den Wert der informationFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setInformationFile(byte[] value) {
        this.informationFile = value;
    }

    /**
     * Ruft den Wert der certificateFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getCertificateFile() {
        return certificateFile;
    }

    /**
     * Legt den Wert der certificateFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setCertificateFile(byte[] value) {
        this.certificateFile = value;
    }

    /**
     * Ruft den Wert der localToCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLocalToCarrier() {
        return localToCarrier;
    }

    /**
     * Legt den Wert der localToCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLocalToCarrier(Boolean value) {
        this.localToCarrier = value;
    }

    /**
     * Ruft den Wert der maxTxPerSecondEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMaxTxPerSecondEnabled() {
        return maxTxPerSecondEnabled;
    }

    /**
     * Legt den Wert der maxTxPerSecondEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMaxTxPerSecondEnabled(Boolean value) {
        this.maxTxPerSecondEnabled = value;
    }

    /**
     * Ruft den Wert der maxTxPerSecond-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxTxPerSecond() {
        return maxTxPerSecond;
    }

    /**
     * Legt den Wert der maxTxPerSecond-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxTxPerSecond(Integer value) {
        this.maxTxPerSecond = value;
    }

    /**
     * Ruft den Wert der tldnEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTldnEnabled() {
        return tldnEnabled;
    }

    /**
     * Legt den Wert der tldnEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTldnEnabled(Boolean value) {
        this.tldnEnabled = value;
    }

    /**
     * Ruft den Wert der genericNumberEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGenericNumberEnabled() {
        return genericNumberEnabled;
    }

    /**
     * Legt den Wert der genericNumberEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGenericNumberEnabled(Boolean value) {
        this.genericNumberEnabled = value;
    }

    /**
     * Ruft den Wert der mobileStateCheckEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMobileStateCheckEnabled() {
        return mobileStateCheckEnabled;
    }

    /**
     * Legt den Wert der mobileStateCheckEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMobileStateCheckEnabled(Boolean value) {
        this.mobileStateCheckEnabled = value;
    }

    /**
     * Ruft den Wert der locationBasedServicesEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLocationBasedServicesEnabled() {
        return locationBasedServicesEnabled;
    }

    /**
     * Legt den Wert der locationBasedServicesEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLocationBasedServicesEnabled(Boolean value) {
        this.locationBasedServicesEnabled = value;
    }

}
