//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal review selected distribution list menu keys.
 *       
 * 
 * <p>Java-Klasse für ReviewSelectedDistributionListMenuKeysReadEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ReviewSelectedDistributionListMenuKeysReadEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="interruptPlaybackAndReturnToPreviousMenu" type="{}DigitAny"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReviewSelectedDistributionListMenuKeysReadEntry", propOrder = {
    "interruptPlaybackAndReturnToPreviousMenu"
})
public class ReviewSelectedDistributionListMenuKeysReadEntry {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String interruptPlaybackAndReturnToPreviousMenu;

    /**
     * Ruft den Wert der interruptPlaybackAndReturnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterruptPlaybackAndReturnToPreviousMenu() {
        return interruptPlaybackAndReturnToPreviousMenu;
    }

    /**
     * Legt den Wert der interruptPlaybackAndReturnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterruptPlaybackAndReturnToPreviousMenu(String value) {
        this.interruptPlaybackAndReturnToPreviousMenu = value;
    }

}
