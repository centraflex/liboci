//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify the Group Night Forwarding system parameters.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemGroupNightForwardingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGroupNightForwardingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="nightForwardGroupCallsWithinEnterprise" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGroupNightForwardingModifyRequest", propOrder = {
    "nightForwardGroupCallsWithinEnterprise"
})
public class SystemGroupNightForwardingModifyRequest
    extends OCIRequest
{

    protected Boolean nightForwardGroupCallsWithinEnterprise;

    /**
     * Ruft den Wert der nightForwardGroupCallsWithinEnterprise-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNightForwardGroupCallsWithinEnterprise() {
        return nightForwardGroupCallsWithinEnterprise;
    }

    /**
     * Legt den Wert der nightForwardGroupCallsWithinEnterprise-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNightForwardGroupCallsWithinEnterprise(Boolean value) {
        this.nightForwardGroupCallsWithinEnterprise = value;
    }

}
