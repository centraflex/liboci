//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Add a GETS Session Priority map.  It maps a priority level with a session priority AVP value.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemGETSSessionPriorityMapAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGETSSessionPriorityMapAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="priorityLevel" type="{}GETSPriorityLevel"/>
 *         <element name="sessionPriority" type="{}GETSSessionPriority"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGETSSessionPriorityMapAddRequest", propOrder = {
    "priorityLevel",
    "sessionPriority"
})
public class SystemGETSSessionPriorityMapAddRequest
    extends OCIRequest
{

    protected int priorityLevel;
    protected int sessionPriority;

    /**
     * Ruft den Wert der priorityLevel-Eigenschaft ab.
     * 
     */
    public int getPriorityLevel() {
        return priorityLevel;
    }

    /**
     * Legt den Wert der priorityLevel-Eigenschaft fest.
     * 
     */
    public void setPriorityLevel(int value) {
        this.priorityLevel = value;
    }

    /**
     * Ruft den Wert der sessionPriority-Eigenschaft ab.
     * 
     */
    public int getSessionPriority() {
        return sessionPriority;
    }

    /**
     * Legt den Wert der sessionPriority-Eigenschaft fest.
     * 
     */
    public void setSessionPriority(int value) {
        this.sessionPriority = value;
    }

}
