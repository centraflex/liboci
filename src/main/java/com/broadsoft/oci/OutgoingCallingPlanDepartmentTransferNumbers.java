//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Outgoing Calling Plan transfer numbers for a department.
 *       
 * 
 * <p>Java-Klasse für OutgoingCallingPlanDepartmentTransferNumbers complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OutgoingCallingPlanDepartmentTransferNumbers">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="departmentKey" type="{}DepartmentKey"/>
 *         <element name="departmentName" type="{}DepartmentFullPathName"/>
 *         <element name="transferNumbers" type="{}OutgoingCallingPlanTransferNumbers"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingCallingPlanDepartmentTransferNumbers", propOrder = {
    "departmentKey",
    "departmentName",
    "transferNumbers"
})
public class OutgoingCallingPlanDepartmentTransferNumbers {

    @XmlElement(required = true)
    protected DepartmentKey departmentKey;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String departmentName;
    @XmlElement(required = true)
    protected OutgoingCallingPlanTransferNumbers transferNumbers;

    /**
     * Ruft den Wert der departmentKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DepartmentKey }
     *     
     */
    public DepartmentKey getDepartmentKey() {
        return departmentKey;
    }

    /**
     * Legt den Wert der departmentKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DepartmentKey }
     *     
     */
    public void setDepartmentKey(DepartmentKey value) {
        this.departmentKey = value;
    }

    /**
     * Ruft den Wert der departmentName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Legt den Wert der departmentName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartmentName(String value) {
        this.departmentName = value;
    }

    /**
     * Ruft den Wert der transferNumbers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanTransferNumbers }
     *     
     */
    public OutgoingCallingPlanTransferNumbers getTransferNumbers() {
        return transferNumbers;
    }

    /**
     * Legt den Wert der transferNumbers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanTransferNumbers }
     *     
     */
    public void setTransferNumbers(OutgoingCallingPlanTransferNumbers value) {
        this.transferNumbers = value;
    }

}
