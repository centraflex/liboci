//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to a GroupPreferredCarrierGroupGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupPreferredCarrierGroupGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupPreferredCarrierGroupGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="intraLataCarrier" type="{}GroupPreferredCarrierName"/>
 *         <element name="interLataCarrier" type="{}GroupPreferredCarrierName"/>
 *         <element name="internationalCarrier" type="{}GroupPreferredCarrierName"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupPreferredCarrierGroupGetResponse", propOrder = {
    "intraLataCarrier",
    "interLataCarrier",
    "internationalCarrier"
})
public class GroupPreferredCarrierGroupGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected GroupPreferredCarrierName intraLataCarrier;
    @XmlElement(required = true)
    protected GroupPreferredCarrierName interLataCarrier;
    @XmlElement(required = true)
    protected GroupPreferredCarrierName internationalCarrier;

    /**
     * Ruft den Wert der intraLataCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupPreferredCarrierName }
     *     
     */
    public GroupPreferredCarrierName getIntraLataCarrier() {
        return intraLataCarrier;
    }

    /**
     * Legt den Wert der intraLataCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupPreferredCarrierName }
     *     
     */
    public void setIntraLataCarrier(GroupPreferredCarrierName value) {
        this.intraLataCarrier = value;
    }

    /**
     * Ruft den Wert der interLataCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupPreferredCarrierName }
     *     
     */
    public GroupPreferredCarrierName getInterLataCarrier() {
        return interLataCarrier;
    }

    /**
     * Legt den Wert der interLataCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupPreferredCarrierName }
     *     
     */
    public void setInterLataCarrier(GroupPreferredCarrierName value) {
        this.interLataCarrier = value;
    }

    /**
     * Ruft den Wert der internationalCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupPreferredCarrierName }
     *     
     */
    public GroupPreferredCarrierName getInternationalCarrier() {
        return internationalCarrier;
    }

    /**
     * Legt den Wert der internationalCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupPreferredCarrierName }
     *     
     */
    public void setInternationalCarrier(GroupPreferredCarrierName value) {
        this.internationalCarrier = value;
    }

}
