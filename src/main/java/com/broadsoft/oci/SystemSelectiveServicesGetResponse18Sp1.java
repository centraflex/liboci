//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemSelectiveServicesGetRequest18sp1.
 *       
 * 
 * <p>Java-Klasse für SystemSelectiveServicesGetResponse18sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSelectiveServicesGetResponse18sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="scheduleCombination" type="{}ScheduleCombinationType"/>
 *         <element name="screenPrivateNumber" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSelectiveServicesGetResponse18sp1", propOrder = {
    "scheduleCombination",
    "screenPrivateNumber"
})
public class SystemSelectiveServicesGetResponse18Sp1
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ScheduleCombinationType scheduleCombination;
    protected boolean screenPrivateNumber;

    /**
     * Ruft den Wert der scheduleCombination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ScheduleCombinationType }
     *     
     */
    public ScheduleCombinationType getScheduleCombination() {
        return scheduleCombination;
    }

    /**
     * Legt den Wert der scheduleCombination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ScheduleCombinationType }
     *     
     */
    public void setScheduleCombination(ScheduleCombinationType value) {
        this.scheduleCombination = value;
    }

    /**
     * Ruft den Wert der screenPrivateNumber-Eigenschaft ab.
     * 
     */
    public boolean isScreenPrivateNumber() {
        return screenPrivateNumber;
    }

    /**
     * Legt den Wert der screenPrivateNumber-Eigenschaft fest.
     * 
     */
    public void setScreenPrivateNumber(boolean value) {
        this.screenPrivateNumber = value;
    }

}
