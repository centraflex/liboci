//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserPushNotificationRegistrationGetListRequest21sp1.
 *         
 *         A registration has more than one row in the response when the registration includes more than one token
 *         and/or one or more event.  There can be one more tokens per registration ID and there can be one or more
 *         events per token.
 *         
 *         Registration Date uses the format "yyyy-MM-dd'T'HH:mm:ss.SSSZ" in the time zone of the requested user.
 *         Example: 2010-10-01T09:30:00:000-0400.
 *       
 * 
 * <p>Java-Klasse für UserPushNotificationRegistrationGetListResponse21sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPushNotificationRegistrationGetListResponse21sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="pushNotificationRegistrationData" type="{}PushNotificationRegistrationData" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPushNotificationRegistrationGetListResponse21sp1", propOrder = {
    "userId",
    "pushNotificationRegistrationData"
})
public class UserPushNotificationRegistrationGetListResponse21Sp1
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected List<PushNotificationRegistrationData> pushNotificationRegistrationData;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the pushNotificationRegistrationData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the pushNotificationRegistrationData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPushNotificationRegistrationData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PushNotificationRegistrationData }
     * 
     * 
     * @return
     *     The value of the pushNotificationRegistrationData property.
     */
    public List<PushNotificationRegistrationData> getPushNotificationRegistrationData() {
        if (pushNotificationRegistrationData == null) {
            pushNotificationRegistrationData = new ArrayList<>();
        }
        return this.pushNotificationRegistrationData;
    }

}
