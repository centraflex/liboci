//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für IncomingCallToUserAliasMode.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="IncomingCallToUserAliasMode">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Enabled"/>
 *     <enumeration value="ExplicitAlias"/>
 *     <enumeration value="Disabled"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "IncomingCallToUserAliasMode")
@XmlEnum
public enum IncomingCallToUserAliasMode {

    @XmlEnumValue("Enabled")
    ENABLED("Enabled"),
    @XmlEnumValue("ExplicitAlias")
    EXPLICIT_ALIAS("ExplicitAlias"),
    @XmlEnumValue("Disabled")
    DISABLED("Disabled");
    private final String value;

    IncomingCallToUserAliasMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static IncomingCallToUserAliasMode fromValue(String v) {
        for (IncomingCallToUserAliasMode c: IncomingCallToUserAliasMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
