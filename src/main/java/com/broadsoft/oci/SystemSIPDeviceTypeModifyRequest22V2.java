//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a sip device type.
 *         The response is either SuccessResponse or ErrorResponse.
 *         The following elements are not changeable:
 *           numberOfPorts
 *           SignalingAddressType
 *           isConferenceDevice
 *           isMusicOnHoldDevice
 *           isMobilityManagerDevice
 *           deviceTypeConfigurationOption
 *           staticLineOrdering
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           supportClientSessionInfo
 *           supportCallInfoConferenceSubscriptionURI
 *           supportRemotePartyInfo
 *           supportVisualDeviceManagement
 *           bypassMediaTreatment
 *           supportCauseParameter
 *           supportCallingPartyCategoryInOutboundFromHeader
 *         The following elements are only used in XS data mode and ignored in AS mode:
 *           enhancedForICS
 *           supports3G4GContinuity
 *           publishesOwnPresence
 *           locationNetwork
 *           allowTerminationBasedOnICSI
 *           roamingMode
 *         The following logic applies to these elements:
 *           macInCert
 *           macInNonRequestURI
 *           The two elements are mutually exclusive.
 *           When both are set to true, the command fails.
 *           When macInCert is set to true, macInNonRequestURI will be reset to false.
 *           When macInNonRequestURI is set to true, macInCert will be reset to false.
 *         Replaced by: SystemSIPDeviceTypeModifyRequest22V3 in AS data mode.
 *       
 * 
 * <p>Java-Klasse für SystemSIPDeviceTypeModifyRequest22V2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPDeviceTypeModifyRequest22V2">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="deviceType" type="{}AccessDeviceType"/>
 *         <element name="isObsolete" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="registrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="holdNormalization" type="{}HoldNormalizationMode" minOccurs="0"/>
 *         <element name="holdAnnouncementMethod" type="{}HoldAnnouncementMethodMode" minOccurs="0"/>
 *         <element name="isTrusted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="E164Capable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="routeAdvance" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forwardingOverride" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="wirelessIntegration" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="webBasedConfigURL" type="{}WebBasedConfigURL" minOccurs="0"/>
 *         <element name="isVideoCapable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="PBXIntegration" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="staticRegistrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="cpeDeviceOptions" type="{}CPEDeviceModifyOptions22" minOccurs="0"/>
 *         <element name="earlyMediaSupport" type="{}EarlyMediaSupportType" minOccurs="0"/>
 *         <element name="authenticateRefer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="autoConfigSoftClient" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="authenticationMode" type="{}AuthenticationMode22" minOccurs="0"/>
 *         <element name="requiresBroadWorksDigitCollection" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="requiresBroadWorksCallWaitingTone" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="requiresMWISubscription" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useHistoryInfoHeaderOnAccessSide" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="adviceOfChargeCapable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="resetEvent" type="{}AccessDeviceResetEvent" minOccurs="0"/>
 *         <element name="supportCallCenterMIMEType" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="trunkMode" type="{}TrunkMode" minOccurs="0"/>
 *         <element name="addPCalledPartyId" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="supportIdentityInUpdateAndReInvite" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="unscreenedPresentationIdentityPolicy" type="{}UnscreenedPresentationIdentityPolicy" minOccurs="0"/>
 *         <element name="enhancedForICS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="supportEmergencyDisconnectControl" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="supportRFC3398" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="supportClientSessionInfo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="supportCallInfoConferenceSubscriptionURI" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="supportRemotePartyInfo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="supportVisualDeviceManagement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="bypassMediaTreatment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="supports3G4GContinuity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="publishesOwnPresence" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="supportCauseParameter" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="locationNetwork" type="{}LocationNetworkType" minOccurs="0"/>
 *         <element name="allowTerminationBasedOnICSI" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="roamingMode" type="{}RoamingMode" minOccurs="0"/>
 *         <element name="supportCallingPartyCategoryInOutboundFromHeader" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPDeviceTypeModifyRequest22V2", propOrder = {
    "deviceType",
    "isObsolete",
    "registrationCapable",
    "holdNormalization",
    "holdAnnouncementMethod",
    "isTrusted",
    "e164Capable",
    "routeAdvance",
    "forwardingOverride",
    "wirelessIntegration",
    "webBasedConfigURL",
    "isVideoCapable",
    "pbxIntegration",
    "staticRegistrationCapable",
    "cpeDeviceOptions",
    "earlyMediaSupport",
    "authenticateRefer",
    "autoConfigSoftClient",
    "authenticationMode",
    "requiresBroadWorksDigitCollection",
    "requiresBroadWorksCallWaitingTone",
    "requiresMWISubscription",
    "useHistoryInfoHeaderOnAccessSide",
    "adviceOfChargeCapable",
    "resetEvent",
    "supportCallCenterMIMEType",
    "trunkMode",
    "addPCalledPartyId",
    "supportIdentityInUpdateAndReInvite",
    "unscreenedPresentationIdentityPolicy",
    "enhancedForICS",
    "supportEmergencyDisconnectControl",
    "supportRFC3398",
    "supportClientSessionInfo",
    "supportCallInfoConferenceSubscriptionURI",
    "supportRemotePartyInfo",
    "supportVisualDeviceManagement",
    "bypassMediaTreatment",
    "supports3G4GContinuity",
    "publishesOwnPresence",
    "supportCauseParameter",
    "locationNetwork",
    "allowTerminationBasedOnICSI",
    "roamingMode",
    "supportCallingPartyCategoryInOutboundFromHeader"
})
public class SystemSIPDeviceTypeModifyRequest22V2
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceType;
    protected Boolean isObsolete;
    protected Boolean registrationCapable;
    @XmlSchemaType(name = "token")
    protected HoldNormalizationMode holdNormalization;
    @XmlSchemaType(name = "token")
    protected HoldAnnouncementMethodMode holdAnnouncementMethod;
    protected Boolean isTrusted;
    @XmlElement(name = "E164Capable")
    protected Boolean e164Capable;
    protected Boolean routeAdvance;
    protected Boolean forwardingOverride;
    protected Boolean wirelessIntegration;
    @XmlElementRef(name = "webBasedConfigURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> webBasedConfigURL;
    protected Boolean isVideoCapable;
    @XmlElement(name = "PBXIntegration")
    protected Boolean pbxIntegration;
    protected Boolean staticRegistrationCapable;
    protected CPEDeviceModifyOptions22 cpeDeviceOptions;
    @XmlSchemaType(name = "token")
    protected EarlyMediaSupportType earlyMediaSupport;
    protected Boolean authenticateRefer;
    protected Boolean autoConfigSoftClient;
    @XmlSchemaType(name = "token")
    protected AuthenticationMode22 authenticationMode;
    protected Boolean requiresBroadWorksDigitCollection;
    protected Boolean requiresBroadWorksCallWaitingTone;
    protected Boolean requiresMWISubscription;
    protected Boolean useHistoryInfoHeaderOnAccessSide;
    protected Boolean adviceOfChargeCapable;
    @XmlElementRef(name = "resetEvent", type = JAXBElement.class, required = false)
    protected JAXBElement<AccessDeviceResetEvent> resetEvent;
    protected Boolean supportCallCenterMIMEType;
    @XmlSchemaType(name = "token")
    protected TrunkMode trunkMode;
    protected Boolean addPCalledPartyId;
    protected Boolean supportIdentityInUpdateAndReInvite;
    @XmlSchemaType(name = "token")
    protected UnscreenedPresentationIdentityPolicy unscreenedPresentationIdentityPolicy;
    protected Boolean enhancedForICS;
    protected Boolean supportEmergencyDisconnectControl;
    protected Boolean supportRFC3398;
    protected Boolean supportClientSessionInfo;
    protected Boolean supportCallInfoConferenceSubscriptionURI;
    protected Boolean supportRemotePartyInfo;
    protected Boolean supportVisualDeviceManagement;
    protected Boolean bypassMediaTreatment;
    protected Boolean supports3G4GContinuity;
    protected Boolean publishesOwnPresence;
    protected Boolean supportCauseParameter;
    @XmlSchemaType(name = "token")
    protected LocationNetworkType locationNetwork;
    protected Boolean allowTerminationBasedOnICSI;
    @XmlSchemaType(name = "token")
    protected RoamingMode roamingMode;
    protected Boolean supportCallingPartyCategoryInOutboundFromHeader;

    /**
     * Ruft den Wert der deviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Legt den Wert der deviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceType(String value) {
        this.deviceType = value;
    }

    /**
     * Ruft den Wert der isObsolete-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsObsolete() {
        return isObsolete;
    }

    /**
     * Legt den Wert der isObsolete-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsObsolete(Boolean value) {
        this.isObsolete = value;
    }

    /**
     * Ruft den Wert der registrationCapable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRegistrationCapable() {
        return registrationCapable;
    }

    /**
     * Legt den Wert der registrationCapable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRegistrationCapable(Boolean value) {
        this.registrationCapable = value;
    }

    /**
     * Ruft den Wert der holdNormalization-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HoldNormalizationMode }
     *     
     */
    public HoldNormalizationMode getHoldNormalization() {
        return holdNormalization;
    }

    /**
     * Legt den Wert der holdNormalization-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HoldNormalizationMode }
     *     
     */
    public void setHoldNormalization(HoldNormalizationMode value) {
        this.holdNormalization = value;
    }

    /**
     * Ruft den Wert der holdAnnouncementMethod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HoldAnnouncementMethodMode }
     *     
     */
    public HoldAnnouncementMethodMode getHoldAnnouncementMethod() {
        return holdAnnouncementMethod;
    }

    /**
     * Legt den Wert der holdAnnouncementMethod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HoldAnnouncementMethodMode }
     *     
     */
    public void setHoldAnnouncementMethod(HoldAnnouncementMethodMode value) {
        this.holdAnnouncementMethod = value;
    }

    /**
     * Ruft den Wert der isTrusted-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsTrusted() {
        return isTrusted;
    }

    /**
     * Legt den Wert der isTrusted-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsTrusted(Boolean value) {
        this.isTrusted = value;
    }

    /**
     * Ruft den Wert der e164Capable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isE164Capable() {
        return e164Capable;
    }

    /**
     * Legt den Wert der e164Capable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setE164Capable(Boolean value) {
        this.e164Capable = value;
    }

    /**
     * Ruft den Wert der routeAdvance-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRouteAdvance() {
        return routeAdvance;
    }

    /**
     * Legt den Wert der routeAdvance-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRouteAdvance(Boolean value) {
        this.routeAdvance = value;
    }

    /**
     * Ruft den Wert der forwardingOverride-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForwardingOverride() {
        return forwardingOverride;
    }

    /**
     * Legt den Wert der forwardingOverride-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForwardingOverride(Boolean value) {
        this.forwardingOverride = value;
    }

    /**
     * Ruft den Wert der wirelessIntegration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWirelessIntegration() {
        return wirelessIntegration;
    }

    /**
     * Legt den Wert der wirelessIntegration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWirelessIntegration(Boolean value) {
        this.wirelessIntegration = value;
    }

    /**
     * Ruft den Wert der webBasedConfigURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWebBasedConfigURL() {
        return webBasedConfigURL;
    }

    /**
     * Legt den Wert der webBasedConfigURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWebBasedConfigURL(JAXBElement<String> value) {
        this.webBasedConfigURL = value;
    }

    /**
     * Ruft den Wert der isVideoCapable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsVideoCapable() {
        return isVideoCapable;
    }

    /**
     * Legt den Wert der isVideoCapable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsVideoCapable(Boolean value) {
        this.isVideoCapable = value;
    }

    /**
     * Ruft den Wert der pbxIntegration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPBXIntegration() {
        return pbxIntegration;
    }

    /**
     * Legt den Wert der pbxIntegration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPBXIntegration(Boolean value) {
        this.pbxIntegration = value;
    }

    /**
     * Ruft den Wert der staticRegistrationCapable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStaticRegistrationCapable() {
        return staticRegistrationCapable;
    }

    /**
     * Legt den Wert der staticRegistrationCapable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStaticRegistrationCapable(Boolean value) {
        this.staticRegistrationCapable = value;
    }

    /**
     * Ruft den Wert der cpeDeviceOptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CPEDeviceModifyOptions22 }
     *     
     */
    public CPEDeviceModifyOptions22 getCpeDeviceOptions() {
        return cpeDeviceOptions;
    }

    /**
     * Legt den Wert der cpeDeviceOptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CPEDeviceModifyOptions22 }
     *     
     */
    public void setCpeDeviceOptions(CPEDeviceModifyOptions22 value) {
        this.cpeDeviceOptions = value;
    }

    /**
     * Ruft den Wert der earlyMediaSupport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EarlyMediaSupportType }
     *     
     */
    public EarlyMediaSupportType getEarlyMediaSupport() {
        return earlyMediaSupport;
    }

    /**
     * Legt den Wert der earlyMediaSupport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EarlyMediaSupportType }
     *     
     */
    public void setEarlyMediaSupport(EarlyMediaSupportType value) {
        this.earlyMediaSupport = value;
    }

    /**
     * Ruft den Wert der authenticateRefer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAuthenticateRefer() {
        return authenticateRefer;
    }

    /**
     * Legt den Wert der authenticateRefer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAuthenticateRefer(Boolean value) {
        this.authenticateRefer = value;
    }

    /**
     * Ruft den Wert der autoConfigSoftClient-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutoConfigSoftClient() {
        return autoConfigSoftClient;
    }

    /**
     * Legt den Wert der autoConfigSoftClient-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoConfigSoftClient(Boolean value) {
        this.autoConfigSoftClient = value;
    }

    /**
     * Ruft den Wert der authenticationMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationMode22 }
     *     
     */
    public AuthenticationMode22 getAuthenticationMode() {
        return authenticationMode;
    }

    /**
     * Legt den Wert der authenticationMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationMode22 }
     *     
     */
    public void setAuthenticationMode(AuthenticationMode22 value) {
        this.authenticationMode = value;
    }

    /**
     * Ruft den Wert der requiresBroadWorksDigitCollection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequiresBroadWorksDigitCollection() {
        return requiresBroadWorksDigitCollection;
    }

    /**
     * Legt den Wert der requiresBroadWorksDigitCollection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequiresBroadWorksDigitCollection(Boolean value) {
        this.requiresBroadWorksDigitCollection = value;
    }

    /**
     * Ruft den Wert der requiresBroadWorksCallWaitingTone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequiresBroadWorksCallWaitingTone() {
        return requiresBroadWorksCallWaitingTone;
    }

    /**
     * Legt den Wert der requiresBroadWorksCallWaitingTone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequiresBroadWorksCallWaitingTone(Boolean value) {
        this.requiresBroadWorksCallWaitingTone = value;
    }

    /**
     * Ruft den Wert der requiresMWISubscription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequiresMWISubscription() {
        return requiresMWISubscription;
    }

    /**
     * Legt den Wert der requiresMWISubscription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequiresMWISubscription(Boolean value) {
        this.requiresMWISubscription = value;
    }

    /**
     * Ruft den Wert der useHistoryInfoHeaderOnAccessSide-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseHistoryInfoHeaderOnAccessSide() {
        return useHistoryInfoHeaderOnAccessSide;
    }

    /**
     * Legt den Wert der useHistoryInfoHeaderOnAccessSide-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseHistoryInfoHeaderOnAccessSide(Boolean value) {
        this.useHistoryInfoHeaderOnAccessSide = value;
    }

    /**
     * Ruft den Wert der adviceOfChargeCapable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAdviceOfChargeCapable() {
        return adviceOfChargeCapable;
    }

    /**
     * Legt den Wert der adviceOfChargeCapable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAdviceOfChargeCapable(Boolean value) {
        this.adviceOfChargeCapable = value;
    }

    /**
     * Ruft den Wert der resetEvent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceResetEvent }{@code >}
     *     
     */
    public JAXBElement<AccessDeviceResetEvent> getResetEvent() {
        return resetEvent;
    }

    /**
     * Legt den Wert der resetEvent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceResetEvent }{@code >}
     *     
     */
    public void setResetEvent(JAXBElement<AccessDeviceResetEvent> value) {
        this.resetEvent = value;
    }

    /**
     * Ruft den Wert der supportCallCenterMIMEType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportCallCenterMIMEType() {
        return supportCallCenterMIMEType;
    }

    /**
     * Legt den Wert der supportCallCenterMIMEType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportCallCenterMIMEType(Boolean value) {
        this.supportCallCenterMIMEType = value;
    }

    /**
     * Ruft den Wert der trunkMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkMode }
     *     
     */
    public TrunkMode getTrunkMode() {
        return trunkMode;
    }

    /**
     * Legt den Wert der trunkMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkMode }
     *     
     */
    public void setTrunkMode(TrunkMode value) {
        this.trunkMode = value;
    }

    /**
     * Ruft den Wert der addPCalledPartyId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAddPCalledPartyId() {
        return addPCalledPartyId;
    }

    /**
     * Legt den Wert der addPCalledPartyId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAddPCalledPartyId(Boolean value) {
        this.addPCalledPartyId = value;
    }

    /**
     * Ruft den Wert der supportIdentityInUpdateAndReInvite-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportIdentityInUpdateAndReInvite() {
        return supportIdentityInUpdateAndReInvite;
    }

    /**
     * Legt den Wert der supportIdentityInUpdateAndReInvite-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportIdentityInUpdateAndReInvite(Boolean value) {
        this.supportIdentityInUpdateAndReInvite = value;
    }

    /**
     * Ruft den Wert der unscreenedPresentationIdentityPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnscreenedPresentationIdentityPolicy }
     *     
     */
    public UnscreenedPresentationIdentityPolicy getUnscreenedPresentationIdentityPolicy() {
        return unscreenedPresentationIdentityPolicy;
    }

    /**
     * Legt den Wert der unscreenedPresentationIdentityPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnscreenedPresentationIdentityPolicy }
     *     
     */
    public void setUnscreenedPresentationIdentityPolicy(UnscreenedPresentationIdentityPolicy value) {
        this.unscreenedPresentationIdentityPolicy = value;
    }

    /**
     * Ruft den Wert der enhancedForICS-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnhancedForICS() {
        return enhancedForICS;
    }

    /**
     * Legt den Wert der enhancedForICS-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnhancedForICS(Boolean value) {
        this.enhancedForICS = value;
    }

    /**
     * Ruft den Wert der supportEmergencyDisconnectControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportEmergencyDisconnectControl() {
        return supportEmergencyDisconnectControl;
    }

    /**
     * Legt den Wert der supportEmergencyDisconnectControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportEmergencyDisconnectControl(Boolean value) {
        this.supportEmergencyDisconnectControl = value;
    }

    /**
     * Ruft den Wert der supportRFC3398-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportRFC3398() {
        return supportRFC3398;
    }

    /**
     * Legt den Wert der supportRFC3398-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportRFC3398(Boolean value) {
        this.supportRFC3398 = value;
    }

    /**
     * Ruft den Wert der supportClientSessionInfo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportClientSessionInfo() {
        return supportClientSessionInfo;
    }

    /**
     * Legt den Wert der supportClientSessionInfo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportClientSessionInfo(Boolean value) {
        this.supportClientSessionInfo = value;
    }

    /**
     * Ruft den Wert der supportCallInfoConferenceSubscriptionURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportCallInfoConferenceSubscriptionURI() {
        return supportCallInfoConferenceSubscriptionURI;
    }

    /**
     * Legt den Wert der supportCallInfoConferenceSubscriptionURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportCallInfoConferenceSubscriptionURI(Boolean value) {
        this.supportCallInfoConferenceSubscriptionURI = value;
    }

    /**
     * Ruft den Wert der supportRemotePartyInfo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportRemotePartyInfo() {
        return supportRemotePartyInfo;
    }

    /**
     * Legt den Wert der supportRemotePartyInfo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportRemotePartyInfo(Boolean value) {
        this.supportRemotePartyInfo = value;
    }

    /**
     * Ruft den Wert der supportVisualDeviceManagement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportVisualDeviceManagement() {
        return supportVisualDeviceManagement;
    }

    /**
     * Legt den Wert der supportVisualDeviceManagement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportVisualDeviceManagement(Boolean value) {
        this.supportVisualDeviceManagement = value;
    }

    /**
     * Ruft den Wert der bypassMediaTreatment-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBypassMediaTreatment() {
        return bypassMediaTreatment;
    }

    /**
     * Legt den Wert der bypassMediaTreatment-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBypassMediaTreatment(Boolean value) {
        this.bypassMediaTreatment = value;
    }

    /**
     * Ruft den Wert der supports3G4GContinuity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupports3G4GContinuity() {
        return supports3G4GContinuity;
    }

    /**
     * Legt den Wert der supports3G4GContinuity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupports3G4GContinuity(Boolean value) {
        this.supports3G4GContinuity = value;
    }

    /**
     * Ruft den Wert der publishesOwnPresence-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPublishesOwnPresence() {
        return publishesOwnPresence;
    }

    /**
     * Legt den Wert der publishesOwnPresence-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPublishesOwnPresence(Boolean value) {
        this.publishesOwnPresence = value;
    }

    /**
     * Ruft den Wert der supportCauseParameter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportCauseParameter() {
        return supportCauseParameter;
    }

    /**
     * Legt den Wert der supportCauseParameter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportCauseParameter(Boolean value) {
        this.supportCauseParameter = value;
    }

    /**
     * Ruft den Wert der locationNetwork-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocationNetworkType }
     *     
     */
    public LocationNetworkType getLocationNetwork() {
        return locationNetwork;
    }

    /**
     * Legt den Wert der locationNetwork-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationNetworkType }
     *     
     */
    public void setLocationNetwork(LocationNetworkType value) {
        this.locationNetwork = value;
    }

    /**
     * Ruft den Wert der allowTerminationBasedOnICSI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowTerminationBasedOnICSI() {
        return allowTerminationBasedOnICSI;
    }

    /**
     * Legt den Wert der allowTerminationBasedOnICSI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowTerminationBasedOnICSI(Boolean value) {
        this.allowTerminationBasedOnICSI = value;
    }

    /**
     * Ruft den Wert der roamingMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RoamingMode }
     *     
     */
    public RoamingMode getRoamingMode() {
        return roamingMode;
    }

    /**
     * Legt den Wert der roamingMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RoamingMode }
     *     
     */
    public void setRoamingMode(RoamingMode value) {
        this.roamingMode = value;
    }

    /**
     * Ruft den Wert der supportCallingPartyCategoryInOutboundFromHeader-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportCallingPartyCategoryInOutboundFromHeader() {
        return supportCallingPartyCategoryInOutboundFromHeader;
    }

    /**
     * Legt den Wert der supportCallingPartyCategoryInOutboundFromHeader-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportCallingPartyCategoryInOutboundFromHeader(Boolean value) {
        this.supportCallingPartyCategoryInOutboundFromHeader = value;
    }

}
