//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AuthenticationMode22V2.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AuthenticationMode22V2">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="SIP Authentication"/>
 *     <enumeration value="Certificate-based Authentication"/>
 *     <enumeration value="Disabled"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AuthenticationMode22V2")
@XmlEnum
public enum AuthenticationMode22V2 {

    @XmlEnumValue("SIP Authentication")
    SIP_AUTHENTICATION("SIP Authentication"),
    @XmlEnumValue("Certificate-based Authentication")
    CERTIFICATE_BASED_AUTHENTICATION("Certificate-based Authentication"),
    @XmlEnumValue("Disabled")
    DISABLED("Disabled");
    private final String value;

    AuthenticationMode22V2(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AuthenticationMode22V2 fromValue(String v) {
        for (AuthenticationMode22V2 c: AuthenticationMode22V2 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
