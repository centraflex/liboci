//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify an Auto Attendant instance.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupAutoAttendantModifyInstanceRequest16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAutoAttendantModifyInstanceRequest16">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceModifyProfile" minOccurs="0"/>
 *         <element name="enableVideo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="businessHours" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="holidayScheduleName" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="extensionDialingScope" type="{}AutoAttendantDialingScope" minOccurs="0"/>
 *         <element name="nameDialingScope" type="{}AutoAttendantDialingScope" minOccurs="0"/>
 *         <element name="nameDialingEntries" type="{}AutoAttendantNameDialingEntry" minOccurs="0"/>
 *         <element name="businessHoursMenu" type="{}AutoAttendantModifyMenu16" minOccurs="0"/>
 *         <element name="afterHoursMenu" type="{}AutoAttendantModifyMenu16" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAutoAttendantModifyInstanceRequest16", propOrder = {
    "serviceUserId",
    "serviceInstanceProfile",
    "enableVideo",
    "businessHours",
    "holidayScheduleName",
    "extensionDialingScope",
    "nameDialingScope",
    "nameDialingEntries",
    "businessHoursMenu",
    "afterHoursMenu"
})
public class GroupAutoAttendantModifyInstanceRequest16
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected ServiceInstanceModifyProfile serviceInstanceProfile;
    protected Boolean enableVideo;
    @XmlElementRef(name = "businessHours", type = JAXBElement.class, required = false)
    protected JAXBElement<String> businessHours;
    @XmlElementRef(name = "holidayScheduleName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> holidayScheduleName;
    @XmlSchemaType(name = "token")
    protected AutoAttendantDialingScope extensionDialingScope;
    @XmlSchemaType(name = "token")
    protected AutoAttendantDialingScope nameDialingScope;
    @XmlSchemaType(name = "token")
    protected AutoAttendantNameDialingEntry nameDialingEntries;
    protected AutoAttendantModifyMenu16 businessHoursMenu;
    protected AutoAttendantModifyMenu16 afterHoursMenu;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public ServiceInstanceModifyProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceModifyProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der enableVideo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableVideo() {
        return enableVideo;
    }

    /**
     * Legt den Wert der enableVideo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableVideo(Boolean value) {
        this.enableVideo = value;
    }

    /**
     * Ruft den Wert der businessHours-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBusinessHours() {
        return businessHours;
    }

    /**
     * Legt den Wert der businessHours-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBusinessHours(JAXBElement<String> value) {
        this.businessHours = value;
    }

    /**
     * Ruft den Wert der holidayScheduleName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHolidayScheduleName() {
        return holidayScheduleName;
    }

    /**
     * Legt den Wert der holidayScheduleName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHolidayScheduleName(JAXBElement<String> value) {
        this.holidayScheduleName = value;
    }

    /**
     * Ruft den Wert der extensionDialingScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public AutoAttendantDialingScope getExtensionDialingScope() {
        return extensionDialingScope;
    }

    /**
     * Legt den Wert der extensionDialingScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public void setExtensionDialingScope(AutoAttendantDialingScope value) {
        this.extensionDialingScope = value;
    }

    /**
     * Ruft den Wert der nameDialingScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public AutoAttendantDialingScope getNameDialingScope() {
        return nameDialingScope;
    }

    /**
     * Legt den Wert der nameDialingScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public void setNameDialingScope(AutoAttendantDialingScope value) {
        this.nameDialingScope = value;
    }

    /**
     * Ruft den Wert der nameDialingEntries-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantNameDialingEntry }
     *     
     */
    public AutoAttendantNameDialingEntry getNameDialingEntries() {
        return nameDialingEntries;
    }

    /**
     * Legt den Wert der nameDialingEntries-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantNameDialingEntry }
     *     
     */
    public void setNameDialingEntries(AutoAttendantNameDialingEntry value) {
        this.nameDialingEntries = value;
    }

    /**
     * Ruft den Wert der businessHoursMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantModifyMenu16 }
     *     
     */
    public AutoAttendantModifyMenu16 getBusinessHoursMenu() {
        return businessHoursMenu;
    }

    /**
     * Legt den Wert der businessHoursMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantModifyMenu16 }
     *     
     */
    public void setBusinessHoursMenu(AutoAttendantModifyMenu16 value) {
        this.businessHoursMenu = value;
    }

    /**
     * Ruft den Wert der afterHoursMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantModifyMenu16 }
     *     
     */
    public AutoAttendantModifyMenu16 getAfterHoursMenu() {
        return afterHoursMenu;
    }

    /**
     * Legt den Wert der afterHoursMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantModifyMenu16 }
     *     
     */
    public void setAfterHoursMenu(AutoAttendantModifyMenu16 value) {
        this.afterHoursMenu = value;
    }

}
