//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Get the system Third-Party IMP service attributes.
 *         The response is either SystemThirdPartyIMPGetResponse or ErrorResponse.
 *         
 *         Replaced by SystemThirdPartyIMPGetRequest19.
 *       
 * 
 * <p>Java-Klasse für SystemThirdPartyIMPGetRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemThirdPartyIMPGetRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemThirdPartyIMPGetRequest")
public class SystemThirdPartyIMPGetRequest
    extends OCIRequest
{


}
