//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EnhancedCallLogsSubscriberType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="EnhancedCallLogsSubscriberType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Auto Attendant"/>
 *     <enumeration value="BroadWorks Anywhere Portal"/>
 *     <enumeration value="Call Center"/>
 *     <enumeration value="Hunt Group"/>
 *     <enumeration value="Route Point"/>
 *     <enumeration value="User"/>
 *     <enumeration value="Find-me/Follow-me"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "EnhancedCallLogsSubscriberType")
@XmlEnum
public enum EnhancedCallLogsSubscriberType {

    @XmlEnumValue("Auto Attendant")
    AUTO_ATTENDANT("Auto Attendant"),
    @XmlEnumValue("BroadWorks Anywhere Portal")
    BROAD_WORKS_ANYWHERE_PORTAL("BroadWorks Anywhere Portal"),
    @XmlEnumValue("Call Center")
    CALL_CENTER("Call Center"),
    @XmlEnumValue("Hunt Group")
    HUNT_GROUP("Hunt Group"),
    @XmlEnumValue("Route Point")
    ROUTE_POINT("Route Point"),
    @XmlEnumValue("User")
    USER("User"),
    @XmlEnumValue("Find-me/Follow-me")
    FIND_ME_FOLLOW_ME("Find-me/Follow-me");
    private final String value;

    EnhancedCallLogsSubscriberType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnhancedCallLogsSubscriberType fromValue(String v) {
        for (EnhancedCallLogsSubscriberType c: EnhancedCallLogsSubscriberType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
