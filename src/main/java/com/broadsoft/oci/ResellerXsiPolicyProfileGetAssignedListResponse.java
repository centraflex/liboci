//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ResellerXsiPolicyProfileGetAssignedListRequest.
 *         Contains a table of all Xsi Policy Profile assigned to.
 *         The column headings are: "Name", "Level", "Description" and "Default".
 *       
 * 
 * <p>Java-Klasse für ResellerXsiPolicyProfileGetAssignedListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerXsiPolicyProfileGetAssignedListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="assignedTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerXsiPolicyProfileGetAssignedListResponse", propOrder = {
    "assignedTable"
})
public class ResellerXsiPolicyProfileGetAssignedListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable assignedTable;

    /**
     * Ruft den Wert der assignedTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAssignedTable() {
        return assignedTable;
    }

    /**
     * Legt den Wert der assignedTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAssignedTable(OCITable value) {
        this.assignedTable = value;
    }

}
