//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with Call Return.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCallReturnModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallReturnModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="twoLevelActivation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="provideDate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="lastUnansweredCallOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="confirmationKey" type="{}DigitAny" minOccurs="0"/>
 *         <element name="allowRestrictedNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="deleteNumberAfterAnsweredCallReturn" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallReturnModifyRequest", propOrder = {
    "twoLevelActivation",
    "provideDate",
    "lastUnansweredCallOnly",
    "confirmationKey",
    "allowRestrictedNumber",
    "deleteNumberAfterAnsweredCallReturn"
})
public class SystemCallReturnModifyRequest
    extends OCIRequest
{

    protected Boolean twoLevelActivation;
    protected Boolean provideDate;
    protected Boolean lastUnansweredCallOnly;
    @XmlElementRef(name = "confirmationKey", type = JAXBElement.class, required = false)
    protected JAXBElement<String> confirmationKey;
    protected Boolean allowRestrictedNumber;
    protected Boolean deleteNumberAfterAnsweredCallReturn;

    /**
     * Ruft den Wert der twoLevelActivation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTwoLevelActivation() {
        return twoLevelActivation;
    }

    /**
     * Legt den Wert der twoLevelActivation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTwoLevelActivation(Boolean value) {
        this.twoLevelActivation = value;
    }

    /**
     * Ruft den Wert der provideDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProvideDate() {
        return provideDate;
    }

    /**
     * Legt den Wert der provideDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProvideDate(Boolean value) {
        this.provideDate = value;
    }

    /**
     * Ruft den Wert der lastUnansweredCallOnly-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLastUnansweredCallOnly() {
        return lastUnansweredCallOnly;
    }

    /**
     * Legt den Wert der lastUnansweredCallOnly-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLastUnansweredCallOnly(Boolean value) {
        this.lastUnansweredCallOnly = value;
    }

    /**
     * Ruft den Wert der confirmationKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getConfirmationKey() {
        return confirmationKey;
    }

    /**
     * Legt den Wert der confirmationKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setConfirmationKey(JAXBElement<String> value) {
        this.confirmationKey = value;
    }

    /**
     * Ruft den Wert der allowRestrictedNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowRestrictedNumber() {
        return allowRestrictedNumber;
    }

    /**
     * Legt den Wert der allowRestrictedNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowRestrictedNumber(Boolean value) {
        this.allowRestrictedNumber = value;
    }

    /**
     * Ruft den Wert der deleteNumberAfterAnsweredCallReturn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeleteNumberAfterAnsweredCallReturn() {
        return deleteNumberAfterAnsweredCallReturn;
    }

    /**
     * Legt den Wert der deleteNumberAfterAnsweredCallReturn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeleteNumberAfterAnsweredCallReturn(Boolean value) {
        this.deleteNumberAfterAnsweredCallReturn = value;
    }

}
