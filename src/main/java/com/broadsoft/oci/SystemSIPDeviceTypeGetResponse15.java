//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *             Response to SystemSIPDeviceTypeGetRequest15.
 *             Replaced By : SystemSIPDeviceTypeAddResponse16
 *          
 * 
 * <p>Java-Klasse für SystemSIPDeviceTypeGetResponse15 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPDeviceTypeGetResponse15">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isObsolete" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="numberOfPorts" type="{}UnboundedPositiveInt"/>
 *         <element name="profile" type="{}SignalingAddressType"/>
 *         <element name="registrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isConferenceDevice" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isMobilityManagerDevice" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isMusicOnHoldDevice" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="RFC3264Hold" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isTrusted" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="E164Capable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="routeAdvance" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forwardingOverride" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="wirelessIntegration" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="webBasedConfigURL" type="{}WebBasedConfigURL" minOccurs="0"/>
 *         <element name="isVideoCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="PBXIntegration" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useBusinessTrunkingContact" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="staticRegistrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="cpeDeviceOptions" type="{}CPEDeviceOptionsRead" minOccurs="0"/>
 *         <element name="protocolChoice" type="{}AccessDeviceProtocol" maxOccurs="unbounded"/>
 *         <element name="earlyMediaSupport" type="{}EarlyMediaSupportType"/>
 *         <element name="authenticateRefer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="autoConfigSoftClient" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="authenticationMode" type="{}AuthenticationMode"/>
 *         <element name="tdmOverlay" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="requiresBroadWorksCallWaitingTone" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="requiresMWISubscription" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useHistoryInfoHeaderOnAccessSide" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPDeviceTypeGetResponse15", propOrder = {
    "isObsolete",
    "numberOfPorts",
    "profile",
    "registrationCapable",
    "isConferenceDevice",
    "isMobilityManagerDevice",
    "isMusicOnHoldDevice",
    "rfc3264Hold",
    "isTrusted",
    "e164Capable",
    "routeAdvance",
    "forwardingOverride",
    "wirelessIntegration",
    "webBasedConfigURL",
    "isVideoCapable",
    "pbxIntegration",
    "useBusinessTrunkingContact",
    "staticRegistrationCapable",
    "cpeDeviceOptions",
    "protocolChoice",
    "earlyMediaSupport",
    "authenticateRefer",
    "autoConfigSoftClient",
    "authenticationMode",
    "tdmOverlay",
    "requiresBroadWorksCallWaitingTone",
    "requiresMWISubscription",
    "useHistoryInfoHeaderOnAccessSide"
})
public class SystemSIPDeviceTypeGetResponse15
    extends OCIDataResponse
{

    protected boolean isObsolete;
    @XmlElement(required = true)
    protected UnboundedPositiveInt numberOfPorts;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SignalingAddressType profile;
    protected boolean registrationCapable;
    protected boolean isConferenceDevice;
    protected boolean isMobilityManagerDevice;
    protected boolean isMusicOnHoldDevice;
    @XmlElement(name = "RFC3264Hold")
    protected boolean rfc3264Hold;
    protected boolean isTrusted;
    @XmlElement(name = "E164Capable")
    protected boolean e164Capable;
    protected boolean routeAdvance;
    protected boolean forwardingOverride;
    protected boolean wirelessIntegration;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String webBasedConfigURL;
    protected boolean isVideoCapable;
    @XmlElement(name = "PBXIntegration")
    protected boolean pbxIntegration;
    protected boolean useBusinessTrunkingContact;
    protected boolean staticRegistrationCapable;
    protected CPEDeviceOptionsRead cpeDeviceOptions;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> protocolChoice;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EarlyMediaSupportType earlyMediaSupport;
    protected boolean authenticateRefer;
    protected boolean autoConfigSoftClient;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AuthenticationMode authenticationMode;
    protected boolean tdmOverlay;
    protected boolean requiresBroadWorksCallWaitingTone;
    protected boolean requiresMWISubscription;
    protected boolean useHistoryInfoHeaderOnAccessSide;

    /**
     * Ruft den Wert der isObsolete-Eigenschaft ab.
     * 
     */
    public boolean isIsObsolete() {
        return isObsolete;
    }

    /**
     * Legt den Wert der isObsolete-Eigenschaft fest.
     * 
     */
    public void setIsObsolete(boolean value) {
        this.isObsolete = value;
    }

    /**
     * Ruft den Wert der numberOfPorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public UnboundedPositiveInt getNumberOfPorts() {
        return numberOfPorts;
    }

    /**
     * Legt den Wert der numberOfPorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public void setNumberOfPorts(UnboundedPositiveInt value) {
        this.numberOfPorts = value;
    }

    /**
     * Ruft den Wert der profile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SignalingAddressType }
     *     
     */
    public SignalingAddressType getProfile() {
        return profile;
    }

    /**
     * Legt den Wert der profile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SignalingAddressType }
     *     
     */
    public void setProfile(SignalingAddressType value) {
        this.profile = value;
    }

    /**
     * Ruft den Wert der registrationCapable-Eigenschaft ab.
     * 
     */
    public boolean isRegistrationCapable() {
        return registrationCapable;
    }

    /**
     * Legt den Wert der registrationCapable-Eigenschaft fest.
     * 
     */
    public void setRegistrationCapable(boolean value) {
        this.registrationCapable = value;
    }

    /**
     * Ruft den Wert der isConferenceDevice-Eigenschaft ab.
     * 
     */
    public boolean isIsConferenceDevice() {
        return isConferenceDevice;
    }

    /**
     * Legt den Wert der isConferenceDevice-Eigenschaft fest.
     * 
     */
    public void setIsConferenceDevice(boolean value) {
        this.isConferenceDevice = value;
    }

    /**
     * Ruft den Wert der isMobilityManagerDevice-Eigenschaft ab.
     * 
     */
    public boolean isIsMobilityManagerDevice() {
        return isMobilityManagerDevice;
    }

    /**
     * Legt den Wert der isMobilityManagerDevice-Eigenschaft fest.
     * 
     */
    public void setIsMobilityManagerDevice(boolean value) {
        this.isMobilityManagerDevice = value;
    }

    /**
     * Ruft den Wert der isMusicOnHoldDevice-Eigenschaft ab.
     * 
     */
    public boolean isIsMusicOnHoldDevice() {
        return isMusicOnHoldDevice;
    }

    /**
     * Legt den Wert der isMusicOnHoldDevice-Eigenschaft fest.
     * 
     */
    public void setIsMusicOnHoldDevice(boolean value) {
        this.isMusicOnHoldDevice = value;
    }

    /**
     * Ruft den Wert der rfc3264Hold-Eigenschaft ab.
     * 
     */
    public boolean isRFC3264Hold() {
        return rfc3264Hold;
    }

    /**
     * Legt den Wert der rfc3264Hold-Eigenschaft fest.
     * 
     */
    public void setRFC3264Hold(boolean value) {
        this.rfc3264Hold = value;
    }

    /**
     * Ruft den Wert der isTrusted-Eigenschaft ab.
     * 
     */
    public boolean isIsTrusted() {
        return isTrusted;
    }

    /**
     * Legt den Wert der isTrusted-Eigenschaft fest.
     * 
     */
    public void setIsTrusted(boolean value) {
        this.isTrusted = value;
    }

    /**
     * Ruft den Wert der e164Capable-Eigenschaft ab.
     * 
     */
    public boolean isE164Capable() {
        return e164Capable;
    }

    /**
     * Legt den Wert der e164Capable-Eigenschaft fest.
     * 
     */
    public void setE164Capable(boolean value) {
        this.e164Capable = value;
    }

    /**
     * Ruft den Wert der routeAdvance-Eigenschaft ab.
     * 
     */
    public boolean isRouteAdvance() {
        return routeAdvance;
    }

    /**
     * Legt den Wert der routeAdvance-Eigenschaft fest.
     * 
     */
    public void setRouteAdvance(boolean value) {
        this.routeAdvance = value;
    }

    /**
     * Ruft den Wert der forwardingOverride-Eigenschaft ab.
     * 
     */
    public boolean isForwardingOverride() {
        return forwardingOverride;
    }

    /**
     * Legt den Wert der forwardingOverride-Eigenschaft fest.
     * 
     */
    public void setForwardingOverride(boolean value) {
        this.forwardingOverride = value;
    }

    /**
     * Ruft den Wert der wirelessIntegration-Eigenschaft ab.
     * 
     */
    public boolean isWirelessIntegration() {
        return wirelessIntegration;
    }

    /**
     * Legt den Wert der wirelessIntegration-Eigenschaft fest.
     * 
     */
    public void setWirelessIntegration(boolean value) {
        this.wirelessIntegration = value;
    }

    /**
     * Ruft den Wert der webBasedConfigURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebBasedConfigURL() {
        return webBasedConfigURL;
    }

    /**
     * Legt den Wert der webBasedConfigURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebBasedConfigURL(String value) {
        this.webBasedConfigURL = value;
    }

    /**
     * Ruft den Wert der isVideoCapable-Eigenschaft ab.
     * 
     */
    public boolean isIsVideoCapable() {
        return isVideoCapable;
    }

    /**
     * Legt den Wert der isVideoCapable-Eigenschaft fest.
     * 
     */
    public void setIsVideoCapable(boolean value) {
        this.isVideoCapable = value;
    }

    /**
     * Ruft den Wert der pbxIntegration-Eigenschaft ab.
     * 
     */
    public boolean isPBXIntegration() {
        return pbxIntegration;
    }

    /**
     * Legt den Wert der pbxIntegration-Eigenschaft fest.
     * 
     */
    public void setPBXIntegration(boolean value) {
        this.pbxIntegration = value;
    }

    /**
     * Ruft den Wert der useBusinessTrunkingContact-Eigenschaft ab.
     * 
     */
    public boolean isUseBusinessTrunkingContact() {
        return useBusinessTrunkingContact;
    }

    /**
     * Legt den Wert der useBusinessTrunkingContact-Eigenschaft fest.
     * 
     */
    public void setUseBusinessTrunkingContact(boolean value) {
        this.useBusinessTrunkingContact = value;
    }

    /**
     * Ruft den Wert der staticRegistrationCapable-Eigenschaft ab.
     * 
     */
    public boolean isStaticRegistrationCapable() {
        return staticRegistrationCapable;
    }

    /**
     * Legt den Wert der staticRegistrationCapable-Eigenschaft fest.
     * 
     */
    public void setStaticRegistrationCapable(boolean value) {
        this.staticRegistrationCapable = value;
    }

    /**
     * Ruft den Wert der cpeDeviceOptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CPEDeviceOptionsRead }
     *     
     */
    public CPEDeviceOptionsRead getCpeDeviceOptions() {
        return cpeDeviceOptions;
    }

    /**
     * Legt den Wert der cpeDeviceOptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CPEDeviceOptionsRead }
     *     
     */
    public void setCpeDeviceOptions(CPEDeviceOptionsRead value) {
        this.cpeDeviceOptions = value;
    }

    /**
     * Gets the value of the protocolChoice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the protocolChoice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProtocolChoice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the protocolChoice property.
     */
    public List<String> getProtocolChoice() {
        if (protocolChoice == null) {
            protocolChoice = new ArrayList<>();
        }
        return this.protocolChoice;
    }

    /**
     * Ruft den Wert der earlyMediaSupport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EarlyMediaSupportType }
     *     
     */
    public EarlyMediaSupportType getEarlyMediaSupport() {
        return earlyMediaSupport;
    }

    /**
     * Legt den Wert der earlyMediaSupport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EarlyMediaSupportType }
     *     
     */
    public void setEarlyMediaSupport(EarlyMediaSupportType value) {
        this.earlyMediaSupport = value;
    }

    /**
     * Ruft den Wert der authenticateRefer-Eigenschaft ab.
     * 
     */
    public boolean isAuthenticateRefer() {
        return authenticateRefer;
    }

    /**
     * Legt den Wert der authenticateRefer-Eigenschaft fest.
     * 
     */
    public void setAuthenticateRefer(boolean value) {
        this.authenticateRefer = value;
    }

    /**
     * Ruft den Wert der autoConfigSoftClient-Eigenschaft ab.
     * 
     */
    public boolean isAutoConfigSoftClient() {
        return autoConfigSoftClient;
    }

    /**
     * Legt den Wert der autoConfigSoftClient-Eigenschaft fest.
     * 
     */
    public void setAutoConfigSoftClient(boolean value) {
        this.autoConfigSoftClient = value;
    }

    /**
     * Ruft den Wert der authenticationMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationMode }
     *     
     */
    public AuthenticationMode getAuthenticationMode() {
        return authenticationMode;
    }

    /**
     * Legt den Wert der authenticationMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationMode }
     *     
     */
    public void setAuthenticationMode(AuthenticationMode value) {
        this.authenticationMode = value;
    }

    /**
     * Ruft den Wert der tdmOverlay-Eigenschaft ab.
     * 
     */
    public boolean isTdmOverlay() {
        return tdmOverlay;
    }

    /**
     * Legt den Wert der tdmOverlay-Eigenschaft fest.
     * 
     */
    public void setTdmOverlay(boolean value) {
        this.tdmOverlay = value;
    }

    /**
     * Ruft den Wert der requiresBroadWorksCallWaitingTone-Eigenschaft ab.
     * 
     */
    public boolean isRequiresBroadWorksCallWaitingTone() {
        return requiresBroadWorksCallWaitingTone;
    }

    /**
     * Legt den Wert der requiresBroadWorksCallWaitingTone-Eigenschaft fest.
     * 
     */
    public void setRequiresBroadWorksCallWaitingTone(boolean value) {
        this.requiresBroadWorksCallWaitingTone = value;
    }

    /**
     * Ruft den Wert der requiresMWISubscription-Eigenschaft ab.
     * 
     */
    public boolean isRequiresMWISubscription() {
        return requiresMWISubscription;
    }

    /**
     * Legt den Wert der requiresMWISubscription-Eigenschaft fest.
     * 
     */
    public void setRequiresMWISubscription(boolean value) {
        this.requiresMWISubscription = value;
    }

    /**
     * Ruft den Wert der useHistoryInfoHeaderOnAccessSide-Eigenschaft ab.
     * 
     */
    public boolean isUseHistoryInfoHeaderOnAccessSide() {
        return useHistoryInfoHeaderOnAccessSide;
    }

    /**
     * Legt den Wert der useHistoryInfoHeaderOnAccessSide-Eigenschaft fest.
     * 
     */
    public void setUseHistoryInfoHeaderOnAccessSide(boolean value) {
        this.useHistoryInfoHeaderOnAccessSide = value;
    }

}
