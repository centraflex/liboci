//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         MWI Delivery To Mobile Endpoint enabled status indicator
 *       
 * 
 * <p>Java-Klasse für MWIDeliveryToMobileEndpointTemplateActivation complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="MWIDeliveryToMobileEndpointTemplateActivation">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="language" type="{}Language"/>
 *         <element name="type" type="{}MWIDeliveryToMobileEndpointTemplateType"/>
 *         <element name="isEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MWIDeliveryToMobileEndpointTemplateActivation", propOrder = {
    "language",
    "type",
    "isEnabled"
})
public class MWIDeliveryToMobileEndpointTemplateActivation {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String language;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MWIDeliveryToMobileEndpointTemplateType type;
    protected boolean isEnabled;

    /**
     * Ruft den Wert der language-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Legt den Wert der language-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MWIDeliveryToMobileEndpointTemplateType }
     *     
     */
    public MWIDeliveryToMobileEndpointTemplateType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MWIDeliveryToMobileEndpointTemplateType }
     *     
     */
    public void setType(MWIDeliveryToMobileEndpointTemplateType value) {
        this.type = value;
    }

    /**
     * Ruft den Wert der isEnabled-Eigenschaft ab.
     * 
     */
    public boolean isIsEnabled() {
        return isEnabled;
    }

    /**
     * Legt den Wert der isEnabled-Eigenschaft fest.
     * 
     */
    public void setIsEnabled(boolean value) {
        this.isEnabled = value;
    }

}
