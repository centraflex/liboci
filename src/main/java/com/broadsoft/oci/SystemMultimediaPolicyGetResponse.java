//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemMultimediaPolicyGetRequest
 *       
 * 
 * <p>Java-Klasse für SystemMultimediaPolicyGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemMultimediaPolicyGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="restrictNonAudioVideoMediaTypes" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemMultimediaPolicyGetResponse", propOrder = {
    "restrictNonAudioVideoMediaTypes"
})
public class SystemMultimediaPolicyGetResponse
    extends OCIDataResponse
{

    protected boolean restrictNonAudioVideoMediaTypes;

    /**
     * Ruft den Wert der restrictNonAudioVideoMediaTypes-Eigenschaft ab.
     * 
     */
    public boolean isRestrictNonAudioVideoMediaTypes() {
        return restrictNonAudioVideoMediaTypes;
    }

    /**
     * Legt den Wert der restrictNonAudioVideoMediaTypes-Eigenschaft fest.
     * 
     */
    public void setRestrictNonAudioVideoMediaTypes(boolean value) {
        this.restrictNonAudioVideoMediaTypes = value;
    }

}
