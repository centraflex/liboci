//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserNumberPortabilityAnnouncementGetRequest.
 *         The response contains the user Number Portability attributes.
 *       
 * 
 * <p>Java-Klasse für UserNumberPortabilityAnnouncementGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserNumberPortabilityAnnouncementGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserNumberPortabilityAnnouncementGetResponse", propOrder = {
    "enable"
})
public class UserNumberPortabilityAnnouncementGetResponse
    extends OCIDataResponse
{

    protected boolean enable;

    /**
     * Ruft den Wert der enable-Eigenschaft ab.
     * 
     */
    public boolean isEnable() {
        return enable;
    }

    /**
     * Legt den Wert der enable-Eigenschaft fest.
     * 
     */
    public void setEnable(boolean value) {
        this.enable = value;
    }

}
