//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request the Receptionist notes for the specified Receptionist and Contact User IDs.  
 *         The response is either a UserBroadWorksReceptionistEnterpriseNoteGetResponse or an
 *         ErrorResponse.  If the user sending the request is the not the owner of the 
 *         Receptionist Note, then an ErrorResponse will be returned.
 *       
 * 
 * <p>Java-Klasse für UserBroadWorksReceptionistEnterpriseNoteGetRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksReceptionistEnterpriseNoteGetRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="receptionistUserId" type="{}UserId"/>
 *         <choice>
 *           <element name="contactUserId" type="{}UserId"/>
 *           <element name="vonUser" type="{}VirtualOnNetUserKey"/>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksReceptionistEnterpriseNoteGetRequest", propOrder = {
    "receptionistUserId",
    "contactUserId",
    "vonUser"
})
public class UserBroadWorksReceptionistEnterpriseNoteGetRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String receptionistUserId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String contactUserId;
    protected VirtualOnNetUserKey vonUser;

    /**
     * Ruft den Wert der receptionistUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceptionistUserId() {
        return receptionistUserId;
    }

    /**
     * Legt den Wert der receptionistUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceptionistUserId(String value) {
        this.receptionistUserId = value;
    }

    /**
     * Ruft den Wert der contactUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactUserId() {
        return contactUserId;
    }

    /**
     * Legt den Wert der contactUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactUserId(String value) {
        this.contactUserId = value;
    }

    /**
     * Ruft den Wert der vonUser-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VirtualOnNetUserKey }
     *     
     */
    public VirtualOnNetUserKey getVonUser() {
        return vonUser;
    }

    /**
     * Legt den Wert der vonUser-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VirtualOnNetUserKey }
     *     
     */
    public void setVonUser(VirtualOnNetUserKey value) {
        this.vonUser = value;
    }

}
