//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CommPilotExpressRedirectionAction.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CommPilotExpressRedirectionAction">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Transfer To Voice Mail"/>
 *     <enumeration value="Forward"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CommPilotExpressRedirectionAction")
@XmlEnum
public enum CommPilotExpressRedirectionAction {

    @XmlEnumValue("Transfer To Voice Mail")
    TRANSFER_TO_VOICE_MAIL("Transfer To Voice Mail"),
    @XmlEnumValue("Forward")
    FORWARD("Forward");
    private final String value;

    CommPilotExpressRedirectionAction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommPilotExpressRedirectionAction fromValue(String v) {
        for (CommPilotExpressRedirectionAction c: CommPilotExpressRedirectionAction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
