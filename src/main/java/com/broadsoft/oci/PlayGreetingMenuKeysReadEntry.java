//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal play greeting menu keys.
 *       
 * 
 * <p>Java-Klasse für PlayGreetingMenuKeysReadEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="PlayGreetingMenuKeysReadEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="skipBackward" type="{}DigitAny" minOccurs="0"/>
 *         <element name="pauseOrResume" type="{}DigitAny" minOccurs="0"/>
 *         <element name="skipForward" type="{}DigitAny" minOccurs="0"/>
 *         <element name="jumpToBegin" type="{}DigitAny" minOccurs="0"/>
 *         <element name="jumpToEnd" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlayGreetingMenuKeysReadEntry", propOrder = {
    "skipBackward",
    "pauseOrResume",
    "skipForward",
    "jumpToBegin",
    "jumpToEnd"
})
public class PlayGreetingMenuKeysReadEntry {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String skipBackward;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String pauseOrResume;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String skipForward;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String jumpToBegin;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String jumpToEnd;

    /**
     * Ruft den Wert der skipBackward-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkipBackward() {
        return skipBackward;
    }

    /**
     * Legt den Wert der skipBackward-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkipBackward(String value) {
        this.skipBackward = value;
    }

    /**
     * Ruft den Wert der pauseOrResume-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPauseOrResume() {
        return pauseOrResume;
    }

    /**
     * Legt den Wert der pauseOrResume-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPauseOrResume(String value) {
        this.pauseOrResume = value;
    }

    /**
     * Ruft den Wert der skipForward-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkipForward() {
        return skipForward;
    }

    /**
     * Legt den Wert der skipForward-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkipForward(String value) {
        this.skipForward = value;
    }

    /**
     * Ruft den Wert der jumpToBegin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJumpToBegin() {
        return jumpToBegin;
    }

    /**
     * Legt den Wert der jumpToBegin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJumpToBegin(String value) {
        this.jumpToBegin = value;
    }

    /**
     * Ruft den Wert der jumpToEnd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJumpToEnd() {
        return jumpToEnd;
    }

    /**
     * Legt den Wert der jumpToEnd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJumpToEnd(String value) {
        this.jumpToEnd = value;
    }

}
