//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *           Criteria for searching for users with/without Route List feature assigned.
 *         
 * 
 * <p>Java-Klasse für SearchCriteriaExactUserRouteListAssigned complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactUserRouteListAssigned">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="routeListAssigned" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactUserRouteListAssigned", propOrder = {
    "routeListAssigned"
})
public class SearchCriteriaExactUserRouteListAssigned
    extends SearchCriteria
{

    protected boolean routeListAssigned;

    /**
     * Ruft den Wert der routeListAssigned-Eigenschaft ab.
     * 
     */
    public boolean isRouteListAssigned() {
        return routeListAssigned;
    }

    /**
     * Legt den Wert der routeListAssigned-Eigenschaft fest.
     * 
     */
    public void setRouteListAssigned(boolean value) {
        this.routeListAssigned = value;
    }

}
