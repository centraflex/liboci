//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCategory.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCategory">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Local"/>
 *     <enumeration value="National"/>
 *     <enumeration value="Interlata"/>
 *     <enumeration value="Intralata"/>
 *     <enumeration value="International"/>
 *     <enumeration value="Private"/>
 *     <enumeration value="Emergency"/>
 *     <enumeration value="Other"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCategory")
@XmlEnum
public enum CallCategory {

    @XmlEnumValue("Local")
    LOCAL("Local"),
    @XmlEnumValue("National")
    NATIONAL("National"),
    @XmlEnumValue("Interlata")
    INTERLATA("Interlata"),
    @XmlEnumValue("Intralata")
    INTRALATA("Intralata"),
    @XmlEnumValue("International")
    INTERNATIONAL("International"),
    @XmlEnumValue("Private")
    PRIVATE("Private"),
    @XmlEnumValue("Emergency")
    EMERGENCY("Emergency"),
    @XmlEnumValue("Other")
    OTHER("Other");
    private final String value;

    CallCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCategory fromValue(String v) {
        for (CallCategory c: CallCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
