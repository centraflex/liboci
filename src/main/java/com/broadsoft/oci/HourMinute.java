//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Represents a specific time with hour and minute granularity
 *       
 * 
 * <p>Java-Klasse für HourMinute complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="HourMinute">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="hour" type="{}Hour"/>
 *         <element name="minute" type="{}Minute"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HourMinute", propOrder = {
    "hour",
    "minute"
})
public class HourMinute {

    protected int hour;
    protected int minute;

    /**
     * Ruft den Wert der hour-Eigenschaft ab.
     * 
     */
    public int getHour() {
        return hour;
    }

    /**
     * Legt den Wert der hour-Eigenschaft fest.
     * 
     */
    public void setHour(int value) {
        this.hour = value;
    }

    /**
     * Ruft den Wert der minute-Eigenschaft ab.
     * 
     */
    public int getMinute() {
        return minute;
    }

    /**
     * Legt den Wert der minute-Eigenschaft fest.
     * 
     */
    public void setMinute(int value) {
        this.minute = value;
    }

}
