//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemCallRecordingGetRequest23.
 *       
 * 
 * <p>Java-Klasse für SystemCallRecordingGetResponse23 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallRecordingGetResponse23">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="continueCallAfterRecordingFailure" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxResponseWaitTimeMilliseconds" type="{}RecordingMaxResponseWaitTimeMilliseconds"/>
 *         <element name="continueCallAfterVideoRecordingFailure" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useContinueCallAfterRecordingFailureForOnDemandMode" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="restrictCallRecordingProvisioningAccess" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallRecordingGetResponse23", propOrder = {
    "continueCallAfterRecordingFailure",
    "maxResponseWaitTimeMilliseconds",
    "continueCallAfterVideoRecordingFailure",
    "useContinueCallAfterRecordingFailureForOnDemandMode",
    "useContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode",
    "restrictCallRecordingProvisioningAccess"
})
public class SystemCallRecordingGetResponse23
    extends OCIDataResponse
{

    protected boolean continueCallAfterRecordingFailure;
    protected int maxResponseWaitTimeMilliseconds;
    protected boolean continueCallAfterVideoRecordingFailure;
    protected boolean useContinueCallAfterRecordingFailureForOnDemandMode;
    protected boolean useContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode;
    protected boolean restrictCallRecordingProvisioningAccess;

    /**
     * Ruft den Wert der continueCallAfterRecordingFailure-Eigenschaft ab.
     * 
     */
    public boolean isContinueCallAfterRecordingFailure() {
        return continueCallAfterRecordingFailure;
    }

    /**
     * Legt den Wert der continueCallAfterRecordingFailure-Eigenschaft fest.
     * 
     */
    public void setContinueCallAfterRecordingFailure(boolean value) {
        this.continueCallAfterRecordingFailure = value;
    }

    /**
     * Ruft den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft ab.
     * 
     */
    public int getMaxResponseWaitTimeMilliseconds() {
        return maxResponseWaitTimeMilliseconds;
    }

    /**
     * Legt den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft fest.
     * 
     */
    public void setMaxResponseWaitTimeMilliseconds(int value) {
        this.maxResponseWaitTimeMilliseconds = value;
    }

    /**
     * Ruft den Wert der continueCallAfterVideoRecordingFailure-Eigenschaft ab.
     * 
     */
    public boolean isContinueCallAfterVideoRecordingFailure() {
        return continueCallAfterVideoRecordingFailure;
    }

    /**
     * Legt den Wert der continueCallAfterVideoRecordingFailure-Eigenschaft fest.
     * 
     */
    public void setContinueCallAfterVideoRecordingFailure(boolean value) {
        this.continueCallAfterVideoRecordingFailure = value;
    }

    /**
     * Ruft den Wert der useContinueCallAfterRecordingFailureForOnDemandMode-Eigenschaft ab.
     * 
     */
    public boolean isUseContinueCallAfterRecordingFailureForOnDemandMode() {
        return useContinueCallAfterRecordingFailureForOnDemandMode;
    }

    /**
     * Legt den Wert der useContinueCallAfterRecordingFailureForOnDemandMode-Eigenschaft fest.
     * 
     */
    public void setUseContinueCallAfterRecordingFailureForOnDemandMode(boolean value) {
        this.useContinueCallAfterRecordingFailureForOnDemandMode = value;
    }

    /**
     * Ruft den Wert der useContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode-Eigenschaft ab.
     * 
     */
    public boolean isUseContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode() {
        return useContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode;
    }

    /**
     * Legt den Wert der useContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode-Eigenschaft fest.
     * 
     */
    public void setUseContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode(boolean value) {
        this.useContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode = value;
    }

    /**
     * Ruft den Wert der restrictCallRecordingProvisioningAccess-Eigenschaft ab.
     * 
     */
    public boolean isRestrictCallRecordingProvisioningAccess() {
        return restrictCallRecordingProvisioningAccess;
    }

    /**
     * Legt den Wert der restrictCallRecordingProvisioningAccess-Eigenschaft fest.
     * 
     */
    public void setRestrictCallRecordingProvisioningAccess(boolean value) {
        this.restrictCallRecordingProvisioningAccess = value;
    }

}
