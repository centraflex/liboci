//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserDirectedCallPickupWithBargeInGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserDirectedCallPickupWithBargeInGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserDirectedCallPickupWithBargeInGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableBargeInWarningTone" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDirectedCallPickupWithBargeInGetResponse", propOrder = {
    "enableBargeInWarningTone"
})
public class UserDirectedCallPickupWithBargeInGetResponse
    extends OCIDataResponse
{

    protected boolean enableBargeInWarningTone;

    /**
     * Ruft den Wert der enableBargeInWarningTone-Eigenschaft ab.
     * 
     */
    public boolean isEnableBargeInWarningTone() {
        return enableBargeInWarningTone;
    }

    /**
     * Legt den Wert der enableBargeInWarningTone-Eigenschaft fest.
     * 
     */
    public void setEnableBargeInWarningTone(boolean value) {
        this.enableBargeInWarningTone = value;
    }

}
