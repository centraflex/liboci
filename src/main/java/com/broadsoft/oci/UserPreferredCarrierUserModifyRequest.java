//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modifies the currently configured preferred carriers for a user.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserPreferredCarrierUserModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPreferredCarrierUserModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="intraLataCarrier" type="{}UserPreferredCarrierNameModify" minOccurs="0"/>
 *         <element name="interLataCarrier" type="{}UserPreferredCarrierNameModify" minOccurs="0"/>
 *         <element name="internationalCarrier" type="{}UserPreferredCarrierNameModify" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPreferredCarrierUserModifyRequest", propOrder = {
    "userId",
    "intraLataCarrier",
    "interLataCarrier",
    "internationalCarrier"
})
public class UserPreferredCarrierUserModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected UserPreferredCarrierNameModify intraLataCarrier;
    protected UserPreferredCarrierNameModify interLataCarrier;
    protected UserPreferredCarrierNameModify internationalCarrier;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der intraLataCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserPreferredCarrierNameModify }
     *     
     */
    public UserPreferredCarrierNameModify getIntraLataCarrier() {
        return intraLataCarrier;
    }

    /**
     * Legt den Wert der intraLataCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserPreferredCarrierNameModify }
     *     
     */
    public void setIntraLataCarrier(UserPreferredCarrierNameModify value) {
        this.intraLataCarrier = value;
    }

    /**
     * Ruft den Wert der interLataCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserPreferredCarrierNameModify }
     *     
     */
    public UserPreferredCarrierNameModify getInterLataCarrier() {
        return interLataCarrier;
    }

    /**
     * Legt den Wert der interLataCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserPreferredCarrierNameModify }
     *     
     */
    public void setInterLataCarrier(UserPreferredCarrierNameModify value) {
        this.interLataCarrier = value;
    }

    /**
     * Ruft den Wert der internationalCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserPreferredCarrierNameModify }
     *     
     */
    public UserPreferredCarrierNameModify getInternationalCarrier() {
        return internationalCarrier;
    }

    /**
     * Legt den Wert der internationalCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserPreferredCarrierNameModify }
     *     
     */
    public void setInternationalCarrier(UserPreferredCarrierNameModify value) {
        this.internationalCarrier = value;
    }

}
