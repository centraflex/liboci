//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a Find-me/Follow-me alerting group.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupFindMeFollowMeModifyAlertingGroupRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupFindMeFollowMeModifyAlertingGroupRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="alertingGroupName" type="{}FindMeFollowMeAlertingGroupName"/>
 *         <element name="newAlertingGroupName" type="{}FindMeFollowMeAlertingGroupName" minOccurs="0"/>
 *         <element name="alertingGroupDescription" type="{}FindMeFollowMeAlertingGroupDescription" minOccurs="0"/>
 *         <element name="useDiversionInhibitor" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="answerConfirmationRequired" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="numberOfRings" type="{}FindMeFollowMeAlertingGroupNumberOfRings" minOccurs="0"/>
 *         <element name="phoneNumberOrUserList" type="{}FindMeFollowMeAlertingGroupReplacementOutgoingDNSIPURIorUserIdList" minOccurs="0"/>
 *         <element name="criteriaActivation" type="{}CriteriaActivation" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupFindMeFollowMeModifyAlertingGroupRequest", propOrder = {
    "serviceUserId",
    "alertingGroupName",
    "newAlertingGroupName",
    "alertingGroupDescription",
    "useDiversionInhibitor",
    "answerConfirmationRequired",
    "numberOfRings",
    "phoneNumberOrUserList",
    "criteriaActivation"
})
public class GroupFindMeFollowMeModifyAlertingGroupRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String alertingGroupName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newAlertingGroupName;
    @XmlElementRef(name = "alertingGroupDescription", type = JAXBElement.class, required = false)
    protected JAXBElement<String> alertingGroupDescription;
    protected Boolean useDiversionInhibitor;
    protected Boolean answerConfirmationRequired;
    protected Integer numberOfRings;
    @XmlElementRef(name = "phoneNumberOrUserList", type = JAXBElement.class, required = false)
    protected JAXBElement<FindMeFollowMeAlertingGroupReplacementOutgoingDNSIPURIorUserIdList> phoneNumberOrUserList;
    protected List<CriteriaActivation> criteriaActivation;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der alertingGroupName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlertingGroupName() {
        return alertingGroupName;
    }

    /**
     * Legt den Wert der alertingGroupName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlertingGroupName(String value) {
        this.alertingGroupName = value;
    }

    /**
     * Ruft den Wert der newAlertingGroupName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewAlertingGroupName() {
        return newAlertingGroupName;
    }

    /**
     * Legt den Wert der newAlertingGroupName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewAlertingGroupName(String value) {
        this.newAlertingGroupName = value;
    }

    /**
     * Ruft den Wert der alertingGroupDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAlertingGroupDescription() {
        return alertingGroupDescription;
    }

    /**
     * Legt den Wert der alertingGroupDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAlertingGroupDescription(JAXBElement<String> value) {
        this.alertingGroupDescription = value;
    }

    /**
     * Ruft den Wert der useDiversionInhibitor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseDiversionInhibitor() {
        return useDiversionInhibitor;
    }

    /**
     * Legt den Wert der useDiversionInhibitor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseDiversionInhibitor(Boolean value) {
        this.useDiversionInhibitor = value;
    }

    /**
     * Ruft den Wert der answerConfirmationRequired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAnswerConfirmationRequired() {
        return answerConfirmationRequired;
    }

    /**
     * Legt den Wert der answerConfirmationRequired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAnswerConfirmationRequired(Boolean value) {
        this.answerConfirmationRequired = value;
    }

    /**
     * Ruft den Wert der numberOfRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfRings() {
        return numberOfRings;
    }

    /**
     * Legt den Wert der numberOfRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfRings(Integer value) {
        this.numberOfRings = value;
    }

    /**
     * Ruft den Wert der phoneNumberOrUserList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FindMeFollowMeAlertingGroupReplacementOutgoingDNSIPURIorUserIdList }{@code >}
     *     
     */
    public JAXBElement<FindMeFollowMeAlertingGroupReplacementOutgoingDNSIPURIorUserIdList> getPhoneNumberOrUserList() {
        return phoneNumberOrUserList;
    }

    /**
     * Legt den Wert der phoneNumberOrUserList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FindMeFollowMeAlertingGroupReplacementOutgoingDNSIPURIorUserIdList }{@code >}
     *     
     */
    public void setPhoneNumberOrUserList(JAXBElement<FindMeFollowMeAlertingGroupReplacementOutgoingDNSIPURIorUserIdList> value) {
        this.phoneNumberOrUserList = value;
    }

    /**
     * Gets the value of the criteriaActivation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the criteriaActivation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCriteriaActivation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CriteriaActivation }
     * 
     * 
     * @return
     *     The value of the criteriaActivation property.
     */
    public List<CriteriaActivation> getCriteriaActivation() {
        if (criteriaActivation == null) {
            criteriaActivation = new ArrayList<>();
        }
        return this.criteriaActivation;
    }

}
