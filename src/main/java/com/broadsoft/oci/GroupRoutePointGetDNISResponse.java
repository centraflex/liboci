//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupRoutePointGetDNISRequest.
 *       
 * 
 * <p>Java-Klasse für GroupRoutePointGetDNISResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupRoutePointGetDNISResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="dnisPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="extension" type="{}Extension17" minOccurs="0"/>
 *         <element name="useCustomCLIDSettings" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="callingLineIdPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="callingLineIdLastName" type="{}CallingLineIdLastName" minOccurs="0"/>
 *         <element name="callingLineIdFirstName" type="{}CallingLineIdFirstName" minOccurs="0"/>
 *         <element name="useCustomDnisAnnouncementSettings" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowOutgoingACDCall" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupRoutePointGetDNISResponse", propOrder = {
    "dnisPhoneNumber",
    "extension",
    "useCustomCLIDSettings",
    "callingLineIdPhoneNumber",
    "callingLineIdLastName",
    "callingLineIdFirstName",
    "useCustomDnisAnnouncementSettings",
    "allowOutgoingACDCall"
})
public class GroupRoutePointGetDNISResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String dnisPhoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String extension;
    protected boolean useCustomCLIDSettings;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdPhoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdLastName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdFirstName;
    protected boolean useCustomDnisAnnouncementSettings;
    protected boolean allowOutgoingACDCall;

    /**
     * Ruft den Wert der dnisPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDnisPhoneNumber() {
        return dnisPhoneNumber;
    }

    /**
     * Legt den Wert der dnisPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDnisPhoneNumber(String value) {
        this.dnisPhoneNumber = value;
    }

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtension(String value) {
        this.extension = value;
    }

    /**
     * Ruft den Wert der useCustomCLIDSettings-Eigenschaft ab.
     * 
     */
    public boolean isUseCustomCLIDSettings() {
        return useCustomCLIDSettings;
    }

    /**
     * Legt den Wert der useCustomCLIDSettings-Eigenschaft fest.
     * 
     */
    public void setUseCustomCLIDSettings(boolean value) {
        this.useCustomCLIDSettings = value;
    }

    /**
     * Ruft den Wert der callingLineIdPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdPhoneNumber() {
        return callingLineIdPhoneNumber;
    }

    /**
     * Legt den Wert der callingLineIdPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdPhoneNumber(String value) {
        this.callingLineIdPhoneNumber = value;
    }

    /**
     * Ruft den Wert der callingLineIdLastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdLastName() {
        return callingLineIdLastName;
    }

    /**
     * Legt den Wert der callingLineIdLastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdLastName(String value) {
        this.callingLineIdLastName = value;
    }

    /**
     * Ruft den Wert der callingLineIdFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdFirstName() {
        return callingLineIdFirstName;
    }

    /**
     * Legt den Wert der callingLineIdFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdFirstName(String value) {
        this.callingLineIdFirstName = value;
    }

    /**
     * Ruft den Wert der useCustomDnisAnnouncementSettings-Eigenschaft ab.
     * 
     */
    public boolean isUseCustomDnisAnnouncementSettings() {
        return useCustomDnisAnnouncementSettings;
    }

    /**
     * Legt den Wert der useCustomDnisAnnouncementSettings-Eigenschaft fest.
     * 
     */
    public void setUseCustomDnisAnnouncementSettings(boolean value) {
        this.useCustomDnisAnnouncementSettings = value;
    }

    /**
     * Ruft den Wert der allowOutgoingACDCall-Eigenschaft ab.
     * 
     */
    public boolean isAllowOutgoingACDCall() {
        return allowOutgoingACDCall;
    }

    /**
     * Legt den Wert der allowOutgoingACDCall-Eigenschaft fest.
     * 
     */
    public void setAllowOutgoingACDCall(boolean value) {
        this.allowOutgoingACDCall = value;
    }

}
