//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserScheduleGetPagedSortedListRequest.
 *         Contains a 3 column table with column headings: "Name", "Type", "Level"
 *         and a row for each schedule.
 *       
 * 
 * <p>Java-Klasse für UserScheduleGetPagedSortedListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserScheduleGetPagedSortedListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="scheduleTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserScheduleGetPagedSortedListResponse", propOrder = {
    "scheduleTable"
})
public class UserScheduleGetPagedSortedListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable scheduleTable;

    /**
     * Ruft den Wert der scheduleTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getScheduleTable() {
        return scheduleTable;
    }

    /**
     * Legt den Wert der scheduleTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setScheduleTable(OCITable value) {
        this.scheduleTable = value;
    }

}
