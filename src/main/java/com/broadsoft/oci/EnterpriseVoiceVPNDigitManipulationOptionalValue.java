//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Enterprise Voice VPN Digit Manipulation Entry that optionally has a value.
 *       
 * 
 * <p>Java-Klasse für EnterpriseVoiceVPNDigitManipulationOptionalValue complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseVoiceVPNDigitManipulationOptionalValue">
 *   <complexContent>
 *     <extension base="{}EnterpriseVoiceVPNDigitManipulation">
 *       <sequence>
 *         <element name="operation" type="{}EnterpriseVoiceVPNDigitManipulationOperationOptionalValue"/>
 *         <element name="value" type="{}EnterpriseVoiceVPNDigitManipulationValue" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseVoiceVPNDigitManipulationOptionalValue", propOrder = {
    "operation",
    "value"
})
public class EnterpriseVoiceVPNDigitManipulationOptionalValue
    extends EnterpriseVoiceVPNDigitManipulation
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseVoiceVPNDigitManipulationOperationOptionalValue operation;
    @XmlElementRef(name = "value", type = JAXBElement.class, required = false)
    protected JAXBElement<String> value;

    /**
     * Ruft den Wert der operation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseVoiceVPNDigitManipulationOperationOptionalValue }
     *     
     */
    public EnterpriseVoiceVPNDigitManipulationOperationOptionalValue getOperation() {
        return operation;
    }

    /**
     * Legt den Wert der operation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseVoiceVPNDigitManipulationOperationOptionalValue }
     *     
     */
    public void setOperation(EnterpriseVoiceVPNDigitManipulationOperationOptionalValue value) {
        this.operation = value;
    }

    /**
     * Ruft den Wert der value-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getValue() {
        return value;
    }

    /**
     * Legt den Wert der value-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setValue(JAXBElement<String> value) {
        this.value = value;
    }

}
