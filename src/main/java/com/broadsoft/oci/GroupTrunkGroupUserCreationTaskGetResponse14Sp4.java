//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to a GroupTrunkGroupUserCreationTaskGetRequest14sp4.
 *       
 * 
 * <p>Java-Klasse für GroupTrunkGroupUserCreationTaskGetResponse14sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupTrunkGroupUserCreationTaskGetResponse14sp4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="userIdFormat" type="{}TrunkGroupUserCreationUserIdFormat"/>
 *         <element name="userIdDomain" type="{}NetAddress"/>
 *         <element name="populateExtension" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="linePortFormat" type="{}TrunkGroupUserCreationSIPURIFormat"/>
 *         <element name="linePortDomain" type="{}NetAddress"/>
 *         <element name="populateContact" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="contactFormat" type="{}TrunkGroupUserCreationSIPURIFormat" minOccurs="0"/>
 *         <element name="contactDomain" type="{}NetAddress" minOccurs="0"/>
 *         <element name="usersCreated" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="totalUsersToCreate" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="errorCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="servicePackName" type="{}ServicePackName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="userServiceName" type="{}UserService" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="status" type="{}TrunkGroupUserCreationTaskStatus"/>
 *         <element name="userCreationMode" type="{}TrunkGroupUserCreationMode"/>
 *         <element name="taskSummary" type="{http://www.w3.org/2001/XMLSchema}token"/>
 *         <element name="reportFileKey" type="{http://www.w3.org/2001/XMLSchema}token"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupTrunkGroupUserCreationTaskGetResponse14sp4", propOrder = {
    "userIdFormat",
    "userIdDomain",
    "populateExtension",
    "linePortFormat",
    "linePortDomain",
    "populateContact",
    "contactFormat",
    "contactDomain",
    "usersCreated",
    "totalUsersToCreate",
    "errorCount",
    "servicePackName",
    "userServiceName",
    "status",
    "userCreationMode",
    "taskSummary",
    "reportFileKey"
})
public class GroupTrunkGroupUserCreationTaskGetResponse14Sp4
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupUserCreationUserIdFormat userIdFormat;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userIdDomain;
    protected boolean populateExtension;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupUserCreationSIPURIFormat linePortFormat;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String linePortDomain;
    protected boolean populateContact;
    @XmlSchemaType(name = "token")
    protected TrunkGroupUserCreationSIPURIFormat contactFormat;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String contactDomain;
    protected int usersCreated;
    protected int totalUsersToCreate;
    protected int errorCount;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> servicePackName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> userServiceName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupUserCreationTaskStatus status;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupUserCreationMode userCreationMode;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String taskSummary;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String reportFileKey;

    /**
     * Ruft den Wert der userIdFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupUserCreationUserIdFormat }
     *     
     */
    public TrunkGroupUserCreationUserIdFormat getUserIdFormat() {
        return userIdFormat;
    }

    /**
     * Legt den Wert der userIdFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupUserCreationUserIdFormat }
     *     
     */
    public void setUserIdFormat(TrunkGroupUserCreationUserIdFormat value) {
        this.userIdFormat = value;
    }

    /**
     * Ruft den Wert der userIdDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserIdDomain() {
        return userIdDomain;
    }

    /**
     * Legt den Wert der userIdDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserIdDomain(String value) {
        this.userIdDomain = value;
    }

    /**
     * Ruft den Wert der populateExtension-Eigenschaft ab.
     * 
     */
    public boolean isPopulateExtension() {
        return populateExtension;
    }

    /**
     * Legt den Wert der populateExtension-Eigenschaft fest.
     * 
     */
    public void setPopulateExtension(boolean value) {
        this.populateExtension = value;
    }

    /**
     * Ruft den Wert der linePortFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupUserCreationSIPURIFormat }
     *     
     */
    public TrunkGroupUserCreationSIPURIFormat getLinePortFormat() {
        return linePortFormat;
    }

    /**
     * Legt den Wert der linePortFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupUserCreationSIPURIFormat }
     *     
     */
    public void setLinePortFormat(TrunkGroupUserCreationSIPURIFormat value) {
        this.linePortFormat = value;
    }

    /**
     * Ruft den Wert der linePortDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinePortDomain() {
        return linePortDomain;
    }

    /**
     * Legt den Wert der linePortDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinePortDomain(String value) {
        this.linePortDomain = value;
    }

    /**
     * Ruft den Wert der populateContact-Eigenschaft ab.
     * 
     */
    public boolean isPopulateContact() {
        return populateContact;
    }

    /**
     * Legt den Wert der populateContact-Eigenschaft fest.
     * 
     */
    public void setPopulateContact(boolean value) {
        this.populateContact = value;
    }

    /**
     * Ruft den Wert der contactFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupUserCreationSIPURIFormat }
     *     
     */
    public TrunkGroupUserCreationSIPURIFormat getContactFormat() {
        return contactFormat;
    }

    /**
     * Legt den Wert der contactFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupUserCreationSIPURIFormat }
     *     
     */
    public void setContactFormat(TrunkGroupUserCreationSIPURIFormat value) {
        this.contactFormat = value;
    }

    /**
     * Ruft den Wert der contactDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactDomain() {
        return contactDomain;
    }

    /**
     * Legt den Wert der contactDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactDomain(String value) {
        this.contactDomain = value;
    }

    /**
     * Ruft den Wert der usersCreated-Eigenschaft ab.
     * 
     */
    public int getUsersCreated() {
        return usersCreated;
    }

    /**
     * Legt den Wert der usersCreated-Eigenschaft fest.
     * 
     */
    public void setUsersCreated(int value) {
        this.usersCreated = value;
    }

    /**
     * Ruft den Wert der totalUsersToCreate-Eigenschaft ab.
     * 
     */
    public int getTotalUsersToCreate() {
        return totalUsersToCreate;
    }

    /**
     * Legt den Wert der totalUsersToCreate-Eigenschaft fest.
     * 
     */
    public void setTotalUsersToCreate(int value) {
        this.totalUsersToCreate = value;
    }

    /**
     * Ruft den Wert der errorCount-Eigenschaft ab.
     * 
     */
    public int getErrorCount() {
        return errorCount;
    }

    /**
     * Legt den Wert der errorCount-Eigenschaft fest.
     * 
     */
    public void setErrorCount(int value) {
        this.errorCount = value;
    }

    /**
     * Gets the value of the servicePackName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the servicePackName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicePackName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the servicePackName property.
     */
    public List<String> getServicePackName() {
        if (servicePackName == null) {
            servicePackName = new ArrayList<>();
        }
        return this.servicePackName;
    }

    /**
     * Gets the value of the userServiceName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the userServiceName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserServiceName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the userServiceName property.
     */
    public List<String> getUserServiceName() {
        if (userServiceName == null) {
            userServiceName = new ArrayList<>();
        }
        return this.userServiceName;
    }

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupUserCreationTaskStatus }
     *     
     */
    public TrunkGroupUserCreationTaskStatus getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupUserCreationTaskStatus }
     *     
     */
    public void setStatus(TrunkGroupUserCreationTaskStatus value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der userCreationMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupUserCreationMode }
     *     
     */
    public TrunkGroupUserCreationMode getUserCreationMode() {
        return userCreationMode;
    }

    /**
     * Legt den Wert der userCreationMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupUserCreationMode }
     *     
     */
    public void setUserCreationMode(TrunkGroupUserCreationMode value) {
        this.userCreationMode = value;
    }

    /**
     * Ruft den Wert der taskSummary-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskSummary() {
        return taskSummary;
    }

    /**
     * Legt den Wert der taskSummary-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskSummary(String value) {
        this.taskSummary = value;
    }

    /**
     * Ruft den Wert der reportFileKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportFileKey() {
        return reportFileKey;
    }

    /**
     * Legt den Wert der reportFileKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportFileKey(String value) {
        this.reportFileKey = value;
    }

}
