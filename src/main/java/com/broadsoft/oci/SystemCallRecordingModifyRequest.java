//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with Call Recording.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         Replaced By: SystemCallRecordingModifyRequest22
 *       
 * 
 * <p>Java-Klasse für SystemCallRecordingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallRecordingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="continueCallAfterRecordingFailure" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="refreshPeriodSeconds" type="{}RecordingRefreshPeriodSeconds" minOccurs="0"/>
 *         <element name="maxConsecutiveFailures" type="{}RecordingMaxConsecutiveFailures" minOccurs="0"/>
 *         <element name="maxResponseWaitTimeMilliseconds" type="{}RecordingMaxResponseWaitTimeMilliseconds" minOccurs="0"/>
 *         <element name="continueCallAfterVideoRecordingFailure" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallRecordingModifyRequest", propOrder = {
    "continueCallAfterRecordingFailure",
    "refreshPeriodSeconds",
    "maxConsecutiveFailures",
    "maxResponseWaitTimeMilliseconds",
    "continueCallAfterVideoRecordingFailure"
})
public class SystemCallRecordingModifyRequest
    extends OCIRequest
{

    protected Boolean continueCallAfterRecordingFailure;
    protected Integer refreshPeriodSeconds;
    protected Integer maxConsecutiveFailures;
    protected Integer maxResponseWaitTimeMilliseconds;
    protected Boolean continueCallAfterVideoRecordingFailure;

    /**
     * Ruft den Wert der continueCallAfterRecordingFailure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isContinueCallAfterRecordingFailure() {
        return continueCallAfterRecordingFailure;
    }

    /**
     * Legt den Wert der continueCallAfterRecordingFailure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContinueCallAfterRecordingFailure(Boolean value) {
        this.continueCallAfterRecordingFailure = value;
    }

    /**
     * Ruft den Wert der refreshPeriodSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRefreshPeriodSeconds() {
        return refreshPeriodSeconds;
    }

    /**
     * Legt den Wert der refreshPeriodSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRefreshPeriodSeconds(Integer value) {
        this.refreshPeriodSeconds = value;
    }

    /**
     * Ruft den Wert der maxConsecutiveFailures-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxConsecutiveFailures() {
        return maxConsecutiveFailures;
    }

    /**
     * Legt den Wert der maxConsecutiveFailures-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxConsecutiveFailures(Integer value) {
        this.maxConsecutiveFailures = value;
    }

    /**
     * Ruft den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxResponseWaitTimeMilliseconds() {
        return maxResponseWaitTimeMilliseconds;
    }

    /**
     * Legt den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxResponseWaitTimeMilliseconds(Integer value) {
        this.maxResponseWaitTimeMilliseconds = value;
    }

    /**
     * Ruft den Wert der continueCallAfterVideoRecordingFailure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isContinueCallAfterVideoRecordingFailure() {
        return continueCallAfterVideoRecordingFailure;
    }

    /**
     * Legt den Wert der continueCallAfterVideoRecordingFailure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContinueCallAfterVideoRecordingFailure(Boolean value) {
        this.continueCallAfterVideoRecordingFailure = value;
    }

}
