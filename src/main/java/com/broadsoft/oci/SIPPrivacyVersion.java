//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SIPPrivacyVersion.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SIPPrivacyVersion">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="RFC3323"/>
 *     <enumeration value="RFC3323 Japan"/>
 *     <enumeration value="Privacy 03"/>
 *     <enumeration value="Privacy 00"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SIPPrivacyVersion")
@XmlEnum
public enum SIPPrivacyVersion {

    @XmlEnumValue("RFC3323")
    RFC_3323("RFC3323"),
    @XmlEnumValue("RFC3323 Japan")
    RFC_3323_JAPAN("RFC3323 Japan"),
    @XmlEnumValue("Privacy 03")
    PRIVACY_03("Privacy 03"),
    @XmlEnumValue("Privacy 00")
    PRIVACY_00("Privacy 00");
    private final String value;

    SIPPrivacyVersion(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SIPPrivacyVersion fromValue(String v) {
        for (SIPPrivacyVersion c: SIPPrivacyVersion.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
