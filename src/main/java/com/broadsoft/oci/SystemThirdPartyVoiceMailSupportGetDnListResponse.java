//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *        Response to SystemThirdPartyVoiceMailSupportGetDnListRequest.
 *        Contains a table with a row for each ThirdPartyVoiceMailSupport User DN and column headings:
 *        "Phone Number", "Description"
 *      
 * 
 * <p>Java-Klasse für SystemThirdPartyVoiceMailSupportGetDnListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemThirdPartyVoiceMailSupportGetDnListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="thirdPartyVoiceMailSupportTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemThirdPartyVoiceMailSupportGetDnListResponse", propOrder = {
    "thirdPartyVoiceMailSupportTable"
})
public class SystemThirdPartyVoiceMailSupportGetDnListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable thirdPartyVoiceMailSupportTable;

    /**
     * Ruft den Wert der thirdPartyVoiceMailSupportTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getThirdPartyVoiceMailSupportTable() {
        return thirdPartyVoiceMailSupportTable;
    }

    /**
     * Legt den Wert der thirdPartyVoiceMailSupportTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setThirdPartyVoiceMailSupportTable(OCITable value) {
        this.thirdPartyVoiceMailSupportTable = value;
    }

}
