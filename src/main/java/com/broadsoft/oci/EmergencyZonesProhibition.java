//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EmergencyZonesProhibition.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="EmergencyZonesProhibition">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Prohibit all registrations and call originations"/>
 *     <enumeration value="Prohibit emergency call originations"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "EmergencyZonesProhibition")
@XmlEnum
public enum EmergencyZonesProhibition {

    @XmlEnumValue("Prohibit all registrations and call originations")
    PROHIBIT_ALL_REGISTRATIONS_AND_CALL_ORIGINATIONS("Prohibit all registrations and call originations"),
    @XmlEnumValue("Prohibit emergency call originations")
    PROHIBIT_EMERGENCY_CALL_ORIGINATIONS("Prohibit emergency call originations");
    private final String value;

    EmergencyZonesProhibition(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EmergencyZonesProhibition fromValue(String v) {
        for (EmergencyZonesProhibition c: EmergencyZonesProhibition.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
