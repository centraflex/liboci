//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Communication Barring Incoming Rule
 *       
 * 
 * <p>Java-Klasse für CommunicationBarringIncomingRule19sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CommunicationBarringIncomingRule19sp1">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <choice>
 *           <element name="digitPatternCriteria" type="{}DigitPatternCriteriaName"/>
 *           <element name="incomingCriteria" type="{}CommunicationBarringCriteriaName"/>
 *         </choice>
 *         <element name="action" type="{}CommunicationBarringIncomingAction"/>
 *         <element name="callTimeoutSeconds" type="{}CommunicationBarringTimeoutSeconds" minOccurs="0"/>
 *         <element name="timeSchedule" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="priority" type="{}CommunicationBarringCriteriaPriority"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommunicationBarringIncomingRule19sp1", propOrder = {
    "digitPatternCriteria",
    "incomingCriteria",
    "action",
    "callTimeoutSeconds",
    "timeSchedule",
    "holidaySchedule",
    "priority"
})
public class CommunicationBarringIncomingRule19Sp1 {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String digitPatternCriteria;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String incomingCriteria;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CommunicationBarringIncomingAction action;
    protected Integer callTimeoutSeconds;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeSchedule;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String holidaySchedule;
    protected float priority;

    /**
     * Ruft den Wert der digitPatternCriteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDigitPatternCriteria() {
        return digitPatternCriteria;
    }

    /**
     * Legt den Wert der digitPatternCriteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDigitPatternCriteria(String value) {
        this.digitPatternCriteria = value;
    }

    /**
     * Ruft den Wert der incomingCriteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncomingCriteria() {
        return incomingCriteria;
    }

    /**
     * Legt den Wert der incomingCriteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncomingCriteria(String value) {
        this.incomingCriteria = value;
    }

    /**
     * Ruft den Wert der action-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringIncomingAction }
     *     
     */
    public CommunicationBarringIncomingAction getAction() {
        return action;
    }

    /**
     * Legt den Wert der action-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringIncomingAction }
     *     
     */
    public void setAction(CommunicationBarringIncomingAction value) {
        this.action = value;
    }

    /**
     * Ruft den Wert der callTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCallTimeoutSeconds() {
        return callTimeoutSeconds;
    }

    /**
     * Legt den Wert der callTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCallTimeoutSeconds(Integer value) {
        this.callTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeSchedule(String value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHolidaySchedule(String value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der priority-Eigenschaft ab.
     * 
     */
    public float getPriority() {
        return priority;
    }

    /**
     * Legt den Wert der priority-Eigenschaft fest.
     * 
     */
    public void setPriority(float value) {
        this.priority = value;
    }

}
