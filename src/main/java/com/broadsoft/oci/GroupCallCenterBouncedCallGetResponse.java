//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupCallCenterBouncedCallGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterBouncedCallGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterBouncedCallGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="numberOfRingsBeforeBouncingCall" type="{}HuntNoAnswerRings"/>
 *         <element name="enableTransfer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="transferPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="bounceCallWhenAgentUnavailable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterBouncedCallGetResponse", propOrder = {
    "isActive",
    "numberOfRingsBeforeBouncingCall",
    "enableTransfer",
    "transferPhoneNumber",
    "bounceCallWhenAgentUnavailable"
})
public class GroupCallCenterBouncedCallGetResponse
    extends OCIDataResponse
{

    protected boolean isActive;
    protected int numberOfRingsBeforeBouncingCall;
    protected Boolean enableTransfer;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String transferPhoneNumber;
    protected boolean bounceCallWhenAgentUnavailable;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der numberOfRingsBeforeBouncingCall-Eigenschaft ab.
     * 
     */
    public int getNumberOfRingsBeforeBouncingCall() {
        return numberOfRingsBeforeBouncingCall;
    }

    /**
     * Legt den Wert der numberOfRingsBeforeBouncingCall-Eigenschaft fest.
     * 
     */
    public void setNumberOfRingsBeforeBouncingCall(int value) {
        this.numberOfRingsBeforeBouncingCall = value;
    }

    /**
     * Ruft den Wert der enableTransfer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableTransfer() {
        return enableTransfer;
    }

    /**
     * Legt den Wert der enableTransfer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableTransfer(Boolean value) {
        this.enableTransfer = value;
    }

    /**
     * Ruft den Wert der transferPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferPhoneNumber() {
        return transferPhoneNumber;
    }

    /**
     * Legt den Wert der transferPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferPhoneNumber(String value) {
        this.transferPhoneNumber = value;
    }

    /**
     * Ruft den Wert der bounceCallWhenAgentUnavailable-Eigenschaft ab.
     * 
     */
    public boolean isBounceCallWhenAgentUnavailable() {
        return bounceCallWhenAgentUnavailable;
    }

    /**
     * Legt den Wert der bounceCallWhenAgentUnavailable-Eigenschaft fest.
     * 
     */
    public void setBounceCallWhenAgentUnavailable(boolean value) {
        this.bounceCallWhenAgentUnavailable = value;
    }

}
