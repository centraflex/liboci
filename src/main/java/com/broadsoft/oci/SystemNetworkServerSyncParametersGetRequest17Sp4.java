//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to get the list of Network Server Sync system parameters.
 *         The response is either SystemNetworkServerSyncParametersGetResponse17sp4 or ErrorResponse.
 *         
 *         Replaced by: SystemNetworkServerSyncParametersGetRequest24
 *       
 * 
 * <p>Java-Klasse für SystemNetworkServerSyncParametersGetRequest17sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNetworkServerSyncParametersGetRequest17sp4">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNetworkServerSyncParametersGetRequest17sp4")
public class SystemNetworkServerSyncParametersGetRequest17Sp4
    extends OCIRequest
{


}
