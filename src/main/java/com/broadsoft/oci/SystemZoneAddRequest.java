//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a Zone and optional Net Addresses and Physical Locations.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemZoneAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemZoneAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="zoneName" type="{}ZoneName"/>
 *         <element name="netAddress" type="{}IPAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="netAddressRange" type="{}IPAddressRange" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="locationBasedPhysicalLocation" type="{}PhysicalLocation" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="callingZonePhysicalLocation" type="{}PhysicalLocation" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemZoneAddRequest", propOrder = {
    "zoneName",
    "netAddress",
    "netAddressRange",
    "locationBasedPhysicalLocation",
    "callingZonePhysicalLocation"
})
public class SystemZoneAddRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String zoneName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> netAddress;
    protected List<IPAddressRange> netAddressRange;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> locationBasedPhysicalLocation;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingZonePhysicalLocation;

    /**
     * Ruft den Wert der zoneName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZoneName() {
        return zoneName;
    }

    /**
     * Legt den Wert der zoneName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZoneName(String value) {
        this.zoneName = value;
    }

    /**
     * Gets the value of the netAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the netAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNetAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the netAddress property.
     */
    public List<String> getNetAddress() {
        if (netAddress == null) {
            netAddress = new ArrayList<>();
        }
        return this.netAddress;
    }

    /**
     * Gets the value of the netAddressRange property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the netAddressRange property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNetAddressRange().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IPAddressRange }
     * 
     * 
     * @return
     *     The value of the netAddressRange property.
     */
    public List<IPAddressRange> getNetAddressRange() {
        if (netAddressRange == null) {
            netAddressRange = new ArrayList<>();
        }
        return this.netAddressRange;
    }

    /**
     * Gets the value of the locationBasedPhysicalLocation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the locationBasedPhysicalLocation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationBasedPhysicalLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the locationBasedPhysicalLocation property.
     */
    public List<String> getLocationBasedPhysicalLocation() {
        if (locationBasedPhysicalLocation == null) {
            locationBasedPhysicalLocation = new ArrayList<>();
        }
        return this.locationBasedPhysicalLocation;
    }

    /**
     * Ruft den Wert der callingZonePhysicalLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingZonePhysicalLocation() {
        return callingZonePhysicalLocation;
    }

    /**
     * Legt den Wert der callingZonePhysicalLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingZonePhysicalLocation(String value) {
        this.callingZonePhysicalLocation = value;
    }

}
