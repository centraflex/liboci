//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für NoChargeTreatmentHandling.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="NoChargeTreatmentHandling">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Answer"/>
 *     <enumeration value="Early Media"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "NoChargeTreatmentHandling")
@XmlEnum
public enum NoChargeTreatmentHandling {

    @XmlEnumValue("Answer")
    ANSWER("Answer"),
    @XmlEnumValue("Early Media")
    EARLY_MEDIA("Early Media");
    private final String value;

    NoChargeTreatmentHandling(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NoChargeTreatmentHandling fromValue(String v) {
        for (NoChargeTreatmentHandling c: NoChargeTreatmentHandling.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
