//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Requests the system voice portal passcode rules setting.
 *         The response is either SystemVoiceMessagingGroupGetPasscodeRulesResponse14 or ErrorResponse.
 *         Replaced By: SystemPortalPasscodeRulesGetRequest
 *       
 * 
 * <p>Java-Klasse für SystemVoiceMessagingGroupGetPasscodeRulesRequest14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemVoiceMessagingGroupGetPasscodeRulesRequest14">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemVoiceMessagingGroupGetPasscodeRulesRequest14")
public class SystemVoiceMessagingGroupGetPasscodeRulesRequest14
    extends OCIRequest
{


}
