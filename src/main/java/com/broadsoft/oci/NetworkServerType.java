//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für NetworkServerType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="NetworkServerType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Lookup"/>
 *     <enumeration value="Update"/>
 *     <enumeration value="Both"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "NetworkServerType")
@XmlEnum
public enum NetworkServerType {

    @XmlEnumValue("Lookup")
    LOOKUP("Lookup"),
    @XmlEnumValue("Update")
    UPDATE("Update"),
    @XmlEnumValue("Both")
    BOTH("Both");
    private final String value;

    NetworkServerType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NetworkServerType fromValue(String v) {
        for (NetworkServerType c: NetworkServerType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
