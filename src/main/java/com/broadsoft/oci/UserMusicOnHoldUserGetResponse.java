//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserMusicOnHoldUserGetRequest.
 *         Replaced By: UserMusicOnHoldUserGetResponse16                                         
 *       
 * 
 * <p>Java-Klasse für UserMusicOnHoldUserGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserMusicOnHoldUserGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableVideo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="source" type="{}MusicOnHoldUserSourceRead"/>
 *         <element name="useAlternateSourceForInternalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="internalSource" type="{}MusicOnHoldUserSourceRead" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserMusicOnHoldUserGetResponse", propOrder = {
    "enableVideo",
    "source",
    "useAlternateSourceForInternalCalls",
    "internalSource"
})
public class UserMusicOnHoldUserGetResponse
    extends OCIDataResponse
{

    protected boolean enableVideo;
    @XmlElement(required = true)
    protected MusicOnHoldUserSourceRead source;
    protected boolean useAlternateSourceForInternalCalls;
    protected MusicOnHoldUserSourceRead internalSource;

    /**
     * Ruft den Wert der enableVideo-Eigenschaft ab.
     * 
     */
    public boolean isEnableVideo() {
        return enableVideo;
    }

    /**
     * Legt den Wert der enableVideo-Eigenschaft fest.
     * 
     */
    public void setEnableVideo(boolean value) {
        this.enableVideo = value;
    }

    /**
     * Ruft den Wert der source-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldUserSourceRead }
     *     
     */
    public MusicOnHoldUserSourceRead getSource() {
        return source;
    }

    /**
     * Legt den Wert der source-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldUserSourceRead }
     *     
     */
    public void setSource(MusicOnHoldUserSourceRead value) {
        this.source = value;
    }

    /**
     * Ruft den Wert der useAlternateSourceForInternalCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseAlternateSourceForInternalCalls() {
        return useAlternateSourceForInternalCalls;
    }

    /**
     * Legt den Wert der useAlternateSourceForInternalCalls-Eigenschaft fest.
     * 
     */
    public void setUseAlternateSourceForInternalCalls(boolean value) {
        this.useAlternateSourceForInternalCalls = value;
    }

    /**
     * Ruft den Wert der internalSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldUserSourceRead }
     *     
     */
    public MusicOnHoldUserSourceRead getInternalSource() {
        return internalSource;
    }

    /**
     * Legt den Wert der internalSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldUserSourceRead }
     *     
     */
    public void setInternalSource(MusicOnHoldUserSourceRead value) {
        this.internalSource = value;
    }

}
