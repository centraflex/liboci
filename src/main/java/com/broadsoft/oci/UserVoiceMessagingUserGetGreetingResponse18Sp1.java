//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserVoiceMessagingUserGetGreetingRequest18sp1.
 *         Contains the greeting configuration for a user's voice messaging.
 *         The following elements are only used in AS data mode:
 *           disableMessageDeposit, value "false" is returned in XS data mode
 *           disableMessageDepositAction, value "Disconnect" is returned in XS data mode
 *           extendedAwayEnabled, value "false" is returned in XS data mode
 *           extendedAwayDisableMessageDeposit, value "true" is returned in XS data mode
 *           
 *         The following elements are only used in AS data mode and not returned in XS data mode:
 *           greetingOnlyForwardDestination
 *           extendedAwayAudioFile
 *           extendedAwayAudioMediaType
 *           extendedAwayVideoFile
 *           extendedAwayVideoMediaType
 *           
 *           Replaced by: UserVoiceMessagingUserGetGreetingResponse20 in AS data mode
 *       
 * 
 * <p>Java-Klasse für UserVoiceMessagingUserGetGreetingResponse18sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserVoiceMessagingUserGetGreetingResponse18sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="busyAnnouncementSelection" type="{}AnnouncementSelection"/>
 *         <element name="busyPersonalAudioFile" type="{}FileDescription" minOccurs="0"/>
 *         <element name="busyPersonalAudioMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="busyPersonalVideoFile" type="{}FileDescription" minOccurs="0"/>
 *         <element name="busyPersonalVideoMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="noAnswerAnnouncementSelection" type="{}VoiceMessagingNoAnswerGreetingSelection"/>
 *         <element name="noAnswerPersonalAudioFile" type="{}FileDescription" minOccurs="0"/>
 *         <element name="noAnswerPersonalAudioMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="noAnswerPersonalVideoFile" type="{}FileDescription" minOccurs="0"/>
 *         <element name="noAnswerPersonalVideoMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="extendedAwayEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="extendedAwayDisableMessageDeposit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="extendedAwayAudioFile" type="{}FileDescription" minOccurs="0"/>
 *         <element name="extendedAwayAudioMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="extendedAwayVideoFile" type="{}FileDescription" minOccurs="0"/>
 *         <element name="extendedAwayVideoMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="noAnswerAlternateGreeting01" type="{}VoiceMessagingAlternateNoAnswerGreetingRead16" minOccurs="0"/>
 *         <element name="noAnswerAlternateGreeting02" type="{}VoiceMessagingAlternateNoAnswerGreetingRead16" minOccurs="0"/>
 *         <element name="noAnswerAlternateGreeting03" type="{}VoiceMessagingAlternateNoAnswerGreetingRead16" minOccurs="0"/>
 *         <element name="noAnswerNumberOfRings" type="{}VoiceMessagingNumberOfRings"/>
 *         <element name="disableMessageDeposit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disableMessageDepositAction" type="{}VoiceMessagingDisableMessageDepositSelection"/>
 *         <element name="greetingOnlyForwardDestination" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserVoiceMessagingUserGetGreetingResponse18sp1", propOrder = {
    "busyAnnouncementSelection",
    "busyPersonalAudioFile",
    "busyPersonalAudioMediaType",
    "busyPersonalVideoFile",
    "busyPersonalVideoMediaType",
    "noAnswerAnnouncementSelection",
    "noAnswerPersonalAudioFile",
    "noAnswerPersonalAudioMediaType",
    "noAnswerPersonalVideoFile",
    "noAnswerPersonalVideoMediaType",
    "extendedAwayEnabled",
    "extendedAwayDisableMessageDeposit",
    "extendedAwayAudioFile",
    "extendedAwayAudioMediaType",
    "extendedAwayVideoFile",
    "extendedAwayVideoMediaType",
    "noAnswerAlternateGreeting01",
    "noAnswerAlternateGreeting02",
    "noAnswerAlternateGreeting03",
    "noAnswerNumberOfRings",
    "disableMessageDeposit",
    "disableMessageDepositAction",
    "greetingOnlyForwardDestination"
})
public class UserVoiceMessagingUserGetGreetingResponse18Sp1
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AnnouncementSelection busyAnnouncementSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String busyPersonalAudioFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String busyPersonalAudioMediaType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String busyPersonalVideoFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String busyPersonalVideoMediaType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected VoiceMessagingNoAnswerGreetingSelection noAnswerAnnouncementSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String noAnswerPersonalAudioFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String noAnswerPersonalAudioMediaType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String noAnswerPersonalVideoFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String noAnswerPersonalVideoMediaType;
    protected boolean extendedAwayEnabled;
    protected boolean extendedAwayDisableMessageDeposit;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String extendedAwayAudioFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String extendedAwayAudioMediaType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String extendedAwayVideoFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String extendedAwayVideoMediaType;
    protected VoiceMessagingAlternateNoAnswerGreetingRead16 noAnswerAlternateGreeting01;
    protected VoiceMessagingAlternateNoAnswerGreetingRead16 noAnswerAlternateGreeting02;
    protected VoiceMessagingAlternateNoAnswerGreetingRead16 noAnswerAlternateGreeting03;
    protected int noAnswerNumberOfRings;
    protected boolean disableMessageDeposit;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected VoiceMessagingDisableMessageDepositSelection disableMessageDepositAction;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String greetingOnlyForwardDestination;

    /**
     * Ruft den Wert der busyAnnouncementSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementSelection }
     *     
     */
    public AnnouncementSelection getBusyAnnouncementSelection() {
        return busyAnnouncementSelection;
    }

    /**
     * Legt den Wert der busyAnnouncementSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementSelection }
     *     
     */
    public void setBusyAnnouncementSelection(AnnouncementSelection value) {
        this.busyAnnouncementSelection = value;
    }

    /**
     * Ruft den Wert der busyPersonalAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusyPersonalAudioFile() {
        return busyPersonalAudioFile;
    }

    /**
     * Legt den Wert der busyPersonalAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusyPersonalAudioFile(String value) {
        this.busyPersonalAudioFile = value;
    }

    /**
     * Ruft den Wert der busyPersonalAudioMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusyPersonalAudioMediaType() {
        return busyPersonalAudioMediaType;
    }

    /**
     * Legt den Wert der busyPersonalAudioMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusyPersonalAudioMediaType(String value) {
        this.busyPersonalAudioMediaType = value;
    }

    /**
     * Ruft den Wert der busyPersonalVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusyPersonalVideoFile() {
        return busyPersonalVideoFile;
    }

    /**
     * Legt den Wert der busyPersonalVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusyPersonalVideoFile(String value) {
        this.busyPersonalVideoFile = value;
    }

    /**
     * Ruft den Wert der busyPersonalVideoMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusyPersonalVideoMediaType() {
        return busyPersonalVideoMediaType;
    }

    /**
     * Legt den Wert der busyPersonalVideoMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusyPersonalVideoMediaType(String value) {
        this.busyPersonalVideoMediaType = value;
    }

    /**
     * Ruft den Wert der noAnswerAnnouncementSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingNoAnswerGreetingSelection }
     *     
     */
    public VoiceMessagingNoAnswerGreetingSelection getNoAnswerAnnouncementSelection() {
        return noAnswerAnnouncementSelection;
    }

    /**
     * Legt den Wert der noAnswerAnnouncementSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingNoAnswerGreetingSelection }
     *     
     */
    public void setNoAnswerAnnouncementSelection(VoiceMessagingNoAnswerGreetingSelection value) {
        this.noAnswerAnnouncementSelection = value;
    }

    /**
     * Ruft den Wert der noAnswerPersonalAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoAnswerPersonalAudioFile() {
        return noAnswerPersonalAudioFile;
    }

    /**
     * Legt den Wert der noAnswerPersonalAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoAnswerPersonalAudioFile(String value) {
        this.noAnswerPersonalAudioFile = value;
    }

    /**
     * Ruft den Wert der noAnswerPersonalAudioMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoAnswerPersonalAudioMediaType() {
        return noAnswerPersonalAudioMediaType;
    }

    /**
     * Legt den Wert der noAnswerPersonalAudioMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoAnswerPersonalAudioMediaType(String value) {
        this.noAnswerPersonalAudioMediaType = value;
    }

    /**
     * Ruft den Wert der noAnswerPersonalVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoAnswerPersonalVideoFile() {
        return noAnswerPersonalVideoFile;
    }

    /**
     * Legt den Wert der noAnswerPersonalVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoAnswerPersonalVideoFile(String value) {
        this.noAnswerPersonalVideoFile = value;
    }

    /**
     * Ruft den Wert der noAnswerPersonalVideoMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoAnswerPersonalVideoMediaType() {
        return noAnswerPersonalVideoMediaType;
    }

    /**
     * Legt den Wert der noAnswerPersonalVideoMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoAnswerPersonalVideoMediaType(String value) {
        this.noAnswerPersonalVideoMediaType = value;
    }

    /**
     * Ruft den Wert der extendedAwayEnabled-Eigenschaft ab.
     * 
     */
    public boolean isExtendedAwayEnabled() {
        return extendedAwayEnabled;
    }

    /**
     * Legt den Wert der extendedAwayEnabled-Eigenschaft fest.
     * 
     */
    public void setExtendedAwayEnabled(boolean value) {
        this.extendedAwayEnabled = value;
    }

    /**
     * Ruft den Wert der extendedAwayDisableMessageDeposit-Eigenschaft ab.
     * 
     */
    public boolean isExtendedAwayDisableMessageDeposit() {
        return extendedAwayDisableMessageDeposit;
    }

    /**
     * Legt den Wert der extendedAwayDisableMessageDeposit-Eigenschaft fest.
     * 
     */
    public void setExtendedAwayDisableMessageDeposit(boolean value) {
        this.extendedAwayDisableMessageDeposit = value;
    }

    /**
     * Ruft den Wert der extendedAwayAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtendedAwayAudioFile() {
        return extendedAwayAudioFile;
    }

    /**
     * Legt den Wert der extendedAwayAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtendedAwayAudioFile(String value) {
        this.extendedAwayAudioFile = value;
    }

    /**
     * Ruft den Wert der extendedAwayAudioMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtendedAwayAudioMediaType() {
        return extendedAwayAudioMediaType;
    }

    /**
     * Legt den Wert der extendedAwayAudioMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtendedAwayAudioMediaType(String value) {
        this.extendedAwayAudioMediaType = value;
    }

    /**
     * Ruft den Wert der extendedAwayVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtendedAwayVideoFile() {
        return extendedAwayVideoFile;
    }

    /**
     * Legt den Wert der extendedAwayVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtendedAwayVideoFile(String value) {
        this.extendedAwayVideoFile = value;
    }

    /**
     * Ruft den Wert der extendedAwayVideoMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtendedAwayVideoMediaType() {
        return extendedAwayVideoMediaType;
    }

    /**
     * Legt den Wert der extendedAwayVideoMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtendedAwayVideoMediaType(String value) {
        this.extendedAwayVideoMediaType = value;
    }

    /**
     * Ruft den Wert der noAnswerAlternateGreeting01-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingRead16 }
     *     
     */
    public VoiceMessagingAlternateNoAnswerGreetingRead16 getNoAnswerAlternateGreeting01() {
        return noAnswerAlternateGreeting01;
    }

    /**
     * Legt den Wert der noAnswerAlternateGreeting01-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingRead16 }
     *     
     */
    public void setNoAnswerAlternateGreeting01(VoiceMessagingAlternateNoAnswerGreetingRead16 value) {
        this.noAnswerAlternateGreeting01 = value;
    }

    /**
     * Ruft den Wert der noAnswerAlternateGreeting02-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingRead16 }
     *     
     */
    public VoiceMessagingAlternateNoAnswerGreetingRead16 getNoAnswerAlternateGreeting02() {
        return noAnswerAlternateGreeting02;
    }

    /**
     * Legt den Wert der noAnswerAlternateGreeting02-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingRead16 }
     *     
     */
    public void setNoAnswerAlternateGreeting02(VoiceMessagingAlternateNoAnswerGreetingRead16 value) {
        this.noAnswerAlternateGreeting02 = value;
    }

    /**
     * Ruft den Wert der noAnswerAlternateGreeting03-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingRead16 }
     *     
     */
    public VoiceMessagingAlternateNoAnswerGreetingRead16 getNoAnswerAlternateGreeting03() {
        return noAnswerAlternateGreeting03;
    }

    /**
     * Legt den Wert der noAnswerAlternateGreeting03-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingRead16 }
     *     
     */
    public void setNoAnswerAlternateGreeting03(VoiceMessagingAlternateNoAnswerGreetingRead16 value) {
        this.noAnswerAlternateGreeting03 = value;
    }

    /**
     * Ruft den Wert der noAnswerNumberOfRings-Eigenschaft ab.
     * 
     */
    public int getNoAnswerNumberOfRings() {
        return noAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der noAnswerNumberOfRings-Eigenschaft fest.
     * 
     */
    public void setNoAnswerNumberOfRings(int value) {
        this.noAnswerNumberOfRings = value;
    }

    /**
     * Ruft den Wert der disableMessageDeposit-Eigenschaft ab.
     * 
     */
    public boolean isDisableMessageDeposit() {
        return disableMessageDeposit;
    }

    /**
     * Legt den Wert der disableMessageDeposit-Eigenschaft fest.
     * 
     */
    public void setDisableMessageDeposit(boolean value) {
        this.disableMessageDeposit = value;
    }

    /**
     * Ruft den Wert der disableMessageDepositAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingDisableMessageDepositSelection }
     *     
     */
    public VoiceMessagingDisableMessageDepositSelection getDisableMessageDepositAction() {
        return disableMessageDepositAction;
    }

    /**
     * Legt den Wert der disableMessageDepositAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingDisableMessageDepositSelection }
     *     
     */
    public void setDisableMessageDepositAction(VoiceMessagingDisableMessageDepositSelection value) {
        this.disableMessageDepositAction = value;
    }

    /**
     * Ruft den Wert der greetingOnlyForwardDestination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGreetingOnlyForwardDestination() {
        return greetingOnlyForwardDestination;
    }

    /**
     * Legt den Wert der greetingOnlyForwardDestination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGreetingOnlyForwardDestination(String value) {
        this.greetingOnlyForwardDestination = value;
    }

}
