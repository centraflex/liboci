//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request subscribers be exported to files on the AS filesystem.
 *         The response is either a SuccessResponse or an
 *         ErrorResponse.
 * 
 *         ***** Warning *****:
 *         This activity should only be done during non-busy hours on the secondary
 *         provisioning server because this may cause large amounts of data to be
 *         dumped to disk and it may take some time to execute.
 *       
 * 
 * <p>Java-Klasse für SystemExportSubscriberRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemExportSubscriberRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemExportSubscriberRequest")
public class SystemExportSubscriberRequest
    extends OCIRequest
{


}
