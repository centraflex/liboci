//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal call forwarding option menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für CallForwardingOptionsMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallForwardingOptionsMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="activateCallForwarding" type="{}DigitAny" minOccurs="0"/>
 *         <element name="deactivateCallForwarding" type="{}DigitAny" minOccurs="0"/>
 *         <element name="changeCallForwardingDestination" type="{}DigitAny" minOccurs="0"/>
 *         <element name="listenToCallForwardingStatus" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallForwardingOptionsMenuKeysModifyEntry", propOrder = {
    "activateCallForwarding",
    "deactivateCallForwarding",
    "changeCallForwardingDestination",
    "listenToCallForwardingStatus",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class CallForwardingOptionsMenuKeysModifyEntry {

    @XmlElementRef(name = "activateCallForwarding", type = JAXBElement.class, required = false)
    protected JAXBElement<String> activateCallForwarding;
    @XmlElementRef(name = "deactivateCallForwarding", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deactivateCallForwarding;
    @XmlElementRef(name = "changeCallForwardingDestination", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changeCallForwardingDestination;
    @XmlElementRef(name = "listenToCallForwardingStatus", type = JAXBElement.class, required = false)
    protected JAXBElement<String> listenToCallForwardingStatus;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repeatMenu;

    /**
     * Ruft den Wert der activateCallForwarding-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActivateCallForwarding() {
        return activateCallForwarding;
    }

    /**
     * Legt den Wert der activateCallForwarding-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActivateCallForwarding(JAXBElement<String> value) {
        this.activateCallForwarding = value;
    }

    /**
     * Ruft den Wert der deactivateCallForwarding-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeactivateCallForwarding() {
        return deactivateCallForwarding;
    }

    /**
     * Legt den Wert der deactivateCallForwarding-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeactivateCallForwarding(JAXBElement<String> value) {
        this.deactivateCallForwarding = value;
    }

    /**
     * Ruft den Wert der changeCallForwardingDestination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangeCallForwardingDestination() {
        return changeCallForwardingDestination;
    }

    /**
     * Legt den Wert der changeCallForwardingDestination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangeCallForwardingDestination(JAXBElement<String> value) {
        this.changeCallForwardingDestination = value;
    }

    /**
     * Ruft den Wert der listenToCallForwardingStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getListenToCallForwardingStatus() {
        return listenToCallForwardingStatus;
    }

    /**
     * Legt den Wert der listenToCallForwardingStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setListenToCallForwardingStatus(JAXBElement<String> value) {
        this.listenToCallForwardingStatus = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepeatMenu(JAXBElement<String> value) {
        this.repeatMenu = value;
    }

}
