//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ServicePackMigrationTaskUserSelectionType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ServicePackMigrationTaskUserSelectionType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     <enumeration value="All"/>
 *     <enumeration value="Any"/>
 *     <enumeration value="None"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ServicePackMigrationTaskUserSelectionType")
@XmlEnum
public enum ServicePackMigrationTaskUserSelectionType {

    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("Any")
    ANY("Any"),
    @XmlEnumValue("None")
    NONE("None");
    private final String value;

    ServicePackMigrationTaskUserSelectionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ServicePackMigrationTaskUserSelectionType fromValue(String v) {
        for (ServicePackMigrationTaskUserSelectionType c: ServicePackMigrationTaskUserSelectionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
