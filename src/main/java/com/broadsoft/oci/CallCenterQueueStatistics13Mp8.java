//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.math.BigDecimal;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains Call Center Queue statistics.
 *       
 * 
 * <p>Java-Klasse für CallCenterQueueStatistics13mp8 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterQueueStatistics13mp8">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="numberOfIncomingCalls" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="numberOfCallsQueued" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="numberOfBusyOverflows" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="numberOfCallsAnswered" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="averageTimeWithAgentSeconds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="averageTimeInQueueSeconds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="averageNumberOfAgentsBusy" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         <element name="averageNumberOfAgentsLoggedOff" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         <element name="averageHoldTimeBeforeCallLossSeconds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterQueueStatistics13mp8", propOrder = {
    "numberOfIncomingCalls",
    "numberOfCallsQueued",
    "numberOfBusyOverflows",
    "numberOfCallsAnswered",
    "averageTimeWithAgentSeconds",
    "averageTimeInQueueSeconds",
    "averageNumberOfAgentsBusy",
    "averageNumberOfAgentsLoggedOff",
    "averageHoldTimeBeforeCallLossSeconds"
})
public class CallCenterQueueStatistics13Mp8 {

    protected int numberOfIncomingCalls;
    protected int numberOfCallsQueued;
    protected int numberOfBusyOverflows;
    protected int numberOfCallsAnswered;
    protected int averageTimeWithAgentSeconds;
    protected int averageTimeInQueueSeconds;
    @XmlElement(required = true)
    protected BigDecimal averageNumberOfAgentsBusy;
    @XmlElement(required = true)
    protected BigDecimal averageNumberOfAgentsLoggedOff;
    protected int averageHoldTimeBeforeCallLossSeconds;

    /**
     * Ruft den Wert der numberOfIncomingCalls-Eigenschaft ab.
     * 
     */
    public int getNumberOfIncomingCalls() {
        return numberOfIncomingCalls;
    }

    /**
     * Legt den Wert der numberOfIncomingCalls-Eigenschaft fest.
     * 
     */
    public void setNumberOfIncomingCalls(int value) {
        this.numberOfIncomingCalls = value;
    }

    /**
     * Ruft den Wert der numberOfCallsQueued-Eigenschaft ab.
     * 
     */
    public int getNumberOfCallsQueued() {
        return numberOfCallsQueued;
    }

    /**
     * Legt den Wert der numberOfCallsQueued-Eigenschaft fest.
     * 
     */
    public void setNumberOfCallsQueued(int value) {
        this.numberOfCallsQueued = value;
    }

    /**
     * Ruft den Wert der numberOfBusyOverflows-Eigenschaft ab.
     * 
     */
    public int getNumberOfBusyOverflows() {
        return numberOfBusyOverflows;
    }

    /**
     * Legt den Wert der numberOfBusyOverflows-Eigenschaft fest.
     * 
     */
    public void setNumberOfBusyOverflows(int value) {
        this.numberOfBusyOverflows = value;
    }

    /**
     * Ruft den Wert der numberOfCallsAnswered-Eigenschaft ab.
     * 
     */
    public int getNumberOfCallsAnswered() {
        return numberOfCallsAnswered;
    }

    /**
     * Legt den Wert der numberOfCallsAnswered-Eigenschaft fest.
     * 
     */
    public void setNumberOfCallsAnswered(int value) {
        this.numberOfCallsAnswered = value;
    }

    /**
     * Ruft den Wert der averageTimeWithAgentSeconds-Eigenschaft ab.
     * 
     */
    public int getAverageTimeWithAgentSeconds() {
        return averageTimeWithAgentSeconds;
    }

    /**
     * Legt den Wert der averageTimeWithAgentSeconds-Eigenschaft fest.
     * 
     */
    public void setAverageTimeWithAgentSeconds(int value) {
        this.averageTimeWithAgentSeconds = value;
    }

    /**
     * Ruft den Wert der averageTimeInQueueSeconds-Eigenschaft ab.
     * 
     */
    public int getAverageTimeInQueueSeconds() {
        return averageTimeInQueueSeconds;
    }

    /**
     * Legt den Wert der averageTimeInQueueSeconds-Eigenschaft fest.
     * 
     */
    public void setAverageTimeInQueueSeconds(int value) {
        this.averageTimeInQueueSeconds = value;
    }

    /**
     * Ruft den Wert der averageNumberOfAgentsBusy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAverageNumberOfAgentsBusy() {
        return averageNumberOfAgentsBusy;
    }

    /**
     * Legt den Wert der averageNumberOfAgentsBusy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAverageNumberOfAgentsBusy(BigDecimal value) {
        this.averageNumberOfAgentsBusy = value;
    }

    /**
     * Ruft den Wert der averageNumberOfAgentsLoggedOff-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAverageNumberOfAgentsLoggedOff() {
        return averageNumberOfAgentsLoggedOff;
    }

    /**
     * Legt den Wert der averageNumberOfAgentsLoggedOff-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAverageNumberOfAgentsLoggedOff(BigDecimal value) {
        this.averageNumberOfAgentsLoggedOff = value;
    }

    /**
     * Ruft den Wert der averageHoldTimeBeforeCallLossSeconds-Eigenschaft ab.
     * 
     */
    public int getAverageHoldTimeBeforeCallLossSeconds() {
        return averageHoldTimeBeforeCallLossSeconds;
    }

    /**
     * Legt den Wert der averageHoldTimeBeforeCallLossSeconds-Eigenschaft fest.
     * 
     */
    public void setAverageHoldTimeBeforeCallLossSeconds(int value) {
        this.averageHoldTimeBeforeCallLossSeconds = value;
    }

}
