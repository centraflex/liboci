//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Trunk group endpoint that can have multiple contacts.
 *         alternateTrunkIdentityDomain is only used in XS mode and the AS when deployed in IMS mode.  
 *         Both alternateTrunkIdentity and AlternateTrunkIdentityDomain should be set at the same time if one is set in XS mode.
 *         The following elements are only used in AS data mode and are ignored in XS data mode:        
 *          physicalLocation
 *       
 * 
 * <p>Java-Klasse für TrunkAddressingMultipleContactAdd complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="TrunkAddressingMultipleContactAdd">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="trunkGroupDeviceEndpoint" type="{}TrunkGroupDeviceMultipleContactEndpointAdd" minOccurs="0"/>
 *         <element name="enterpriseTrunkName" type="{}EnterpriseTrunkName" minOccurs="0"/>
 *         <element name="alternateTrunkIdentity" type="{}AlternateTrunkIdentity" minOccurs="0"/>
 *         <element name="alternateTrunkIdentityDomain" type="{}DomainName" minOccurs="0"/>
 *         <element name="physicalLocation" type="{}AccessDevicePhysicalLocation" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrunkAddressingMultipleContactAdd", propOrder = {
    "trunkGroupDeviceEndpoint",
    "enterpriseTrunkName",
    "alternateTrunkIdentity",
    "alternateTrunkIdentityDomain",
    "physicalLocation"
})
public class TrunkAddressingMultipleContactAdd {

    protected TrunkGroupDeviceMultipleContactEndpointAdd trunkGroupDeviceEndpoint;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String enterpriseTrunkName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String alternateTrunkIdentity;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String alternateTrunkIdentityDomain;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String physicalLocation;

    /**
     * Ruft den Wert der trunkGroupDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupDeviceMultipleContactEndpointAdd }
     *     
     */
    public TrunkGroupDeviceMultipleContactEndpointAdd getTrunkGroupDeviceEndpoint() {
        return trunkGroupDeviceEndpoint;
    }

    /**
     * Legt den Wert der trunkGroupDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupDeviceMultipleContactEndpointAdd }
     *     
     */
    public void setTrunkGroupDeviceEndpoint(TrunkGroupDeviceMultipleContactEndpointAdd value) {
        this.trunkGroupDeviceEndpoint = value;
    }

    /**
     * Ruft den Wert der enterpriseTrunkName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnterpriseTrunkName() {
        return enterpriseTrunkName;
    }

    /**
     * Legt den Wert der enterpriseTrunkName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnterpriseTrunkName(String value) {
        this.enterpriseTrunkName = value;
    }

    /**
     * Ruft den Wert der alternateTrunkIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateTrunkIdentity() {
        return alternateTrunkIdentity;
    }

    /**
     * Legt den Wert der alternateTrunkIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateTrunkIdentity(String value) {
        this.alternateTrunkIdentity = value;
    }

    /**
     * Ruft den Wert der alternateTrunkIdentityDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateTrunkIdentityDomain() {
        return alternateTrunkIdentityDomain;
    }

    /**
     * Legt den Wert der alternateTrunkIdentityDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateTrunkIdentityDomain(String value) {
        this.alternateTrunkIdentityDomain = value;
    }

    /**
     * Ruft den Wert der physicalLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhysicalLocation() {
        return physicalLocation;
    }

    /**
     * Legt den Wert der physicalLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhysicalLocation(String value) {
        this.physicalLocation = value;
    }

}
