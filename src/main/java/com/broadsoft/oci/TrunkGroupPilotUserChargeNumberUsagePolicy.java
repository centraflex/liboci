//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TrunkGroupPilotUserChargeNumberUsagePolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="TrunkGroupPilotUserChargeNumberUsagePolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="All Originating Calls"/>
 *     <enumeration value="Unscreened Originating Calls"/>
 *     <enumeration value="No Calls"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TrunkGroupPilotUserChargeNumberUsagePolicy")
@XmlEnum
public enum TrunkGroupPilotUserChargeNumberUsagePolicy {

    @XmlEnumValue("All Originating Calls")
    ALL_ORIGINATING_CALLS("All Originating Calls"),
    @XmlEnumValue("Unscreened Originating Calls")
    UNSCREENED_ORIGINATING_CALLS("Unscreened Originating Calls"),
    @XmlEnumValue("No Calls")
    NO_CALLS("No Calls");
    private final String value;

    TrunkGroupPilotUserChargeNumberUsagePolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrunkGroupPilotUserChargeNumberUsagePolicy fromValue(String v) {
        for (TrunkGroupPilotUserChargeNumberUsagePolicy c: TrunkGroupPilotUserChargeNumberUsagePolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
