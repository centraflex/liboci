//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EstimatedWaitMessageOperatingMode.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="EstimatedWaitMessageOperatingMode">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Position"/>
 *     <enumeration value="Time"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "EstimatedWaitMessageOperatingMode")
@XmlEnum
public enum EstimatedWaitMessageOperatingMode {

    @XmlEnumValue("Position")
    POSITION("Position"),
    @XmlEnumValue("Time")
    TIME("Time");
    private final String value;

    EstimatedWaitMessageOperatingMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EstimatedWaitMessageOperatingMode fromValue(String v) {
        for (EstimatedWaitMessageOperatingMode c: EstimatedWaitMessageOperatingMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
