//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemDeviceFamilyGetRequest.
 *         The response includes the tag sets and device types associated to a device family defined in the system.
 *         Column headings for deviceTypeTable are : Device Type(s)
 *         Column headings for tagSetTable are :Tag Set(s)
 *       
 * 
 * <p>Java-Klasse für SystemDeviceFamilyGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceFamilyGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="deviceTypeTable" type="{C}OCITable"/>
 *         <element name="tagSetTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceFamilyGetResponse", propOrder = {
    "deviceTypeTable",
    "tagSetTable"
})
public class SystemDeviceFamilyGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable deviceTypeTable;
    @XmlElement(required = true)
    protected OCITable tagSetTable;

    /**
     * Ruft den Wert der deviceTypeTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDeviceTypeTable() {
        return deviceTypeTable;
    }

    /**
     * Legt den Wert der deviceTypeTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDeviceTypeTable(OCITable value) {
        this.deviceTypeTable = value;
    }

    /**
     * Ruft den Wert der tagSetTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getTagSetTable() {
        return tagSetTable;
    }

    /**
     * Legt den Wert der tagSetTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setTagSetTable(OCITable value) {
        this.tagSetTable = value;
    }

}
