//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserCallProcessingGetPolicyRequest19sp1.
 *      The useUserCLIDSetting attribute controls the CLID settings 
 *      (clidPolicy, emergencyClidPolicy, allowAlternateNumbersForRedirectingIdentity, useGroupName, allowConfigurableCLIDForRedirectingIdentity, allowDepartmentCLIDNameOverride)
 *      
 *         The useUserMediaSetting attribute controls the Media settings 
 *         (medisPolicySelection, supportedMediaSetName)
 *         
 *         The useUserCallLimitsSetting attribute controls the Call Limits setting 
 *         (useMaxSimultaneousCalls, maxSimultaneousCalls, useMaxSimultaneousVideoCalls, maxSimultaneousVideoCalls, useMaxCallTimeForAnsweredCalls, maxCallTimeForAnsweredCallsMinutes, useMaxCallTimeForUnansweredCalls, maxCallTimeForUnansweredCallsMinutes, useMaxConcurrentRedirectedCalls, useMaxFindMeFollowMeDepth, maxRedirectionDepth, useMaxConcurrentFindMeFollowMeInvocations, maxConcurrentFindMeFollowMeInvocations)
 *         The following elements are only used in AS data mode :
 *          useUserDCLIDSetting
 *          enableDialableCallerID
 *          allowConfigurableCLIDForRedirectingIdentity
 *          allowDepartmentCLIDNameOverride
 *         
 *         Replaced by: UserCallProcessingGetPolicyResponse21sp1
 *        
 * 
 * <p>Java-Klasse für UserCallProcessingGetPolicyResponse19sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallProcessingGetPolicyResponse19sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useUserCLIDSetting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useUserMediaSetting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useUserCallLimitsSetting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useUserDCLIDSetting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useMaxSimultaneousCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxSimultaneousCalls" type="{}CallProcessingMaxSimultaneousCalls19sp1"/>
 *         <element name="useMaxSimultaneousVideoCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxSimultaneousVideoCalls" type="{}CallProcessingMaxSimultaneousCalls19sp1"/>
 *         <element name="useMaxCallTimeForAnsweredCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxCallTimeForAnsweredCallsMinutes" type="{}CallProcessingMaxCallTimeForAnsweredCallsMinutes16"/>
 *         <element name="useMaxCallTimeForUnansweredCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxCallTimeForUnansweredCallsMinutes" type="{}CallProcessingMaxCallTimeForUnansweredCallsMinutes19sp1"/>
 *         <element name="mediaPolicySelection" type="{}MediaPolicySelection"/>
 *         <element name="supportedMediaSetName" type="{}MediaSetName" minOccurs="0"/>
 *         <element name="useMaxConcurrentRedirectedCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxConcurrentRedirectedCalls" type="{}CallProcessingMaxConcurrentRedirectedCalls19sp1"/>
 *         <element name="useMaxFindMeFollowMeDepth" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxFindMeFollowMeDepth" type="{}CallProcessingMaxFindMeFollowMeDepth19sp1"/>
 *         <element name="maxRedirectionDepth" type="{}CallProcessingMaxRedirectionDepth19sp1"/>
 *         <element name="useMaxConcurrentFindMeFollowMeInvocations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxConcurrentFindMeFollowMeInvocations" type="{}CallProcessingMaxConcurrentFindMeFollowMeInvocations19sp1"/>
 *         <element name="clidPolicy" type="{}GroupCLIDPolicy"/>
 *         <element name="emergencyClidPolicy" type="{}GroupCLIDPolicy"/>
 *         <element name="allowAlternateNumbersForRedirectingIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useGroupName" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="blockCallingNameForExternalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableDialableCallerID" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowConfigurableCLIDForRedirectingIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowDepartmentCLIDNameOverride" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallProcessingGetPolicyResponse19sp1", propOrder = {
    "useUserCLIDSetting",
    "useUserMediaSetting",
    "useUserCallLimitsSetting",
    "useUserDCLIDSetting",
    "useMaxSimultaneousCalls",
    "maxSimultaneousCalls",
    "useMaxSimultaneousVideoCalls",
    "maxSimultaneousVideoCalls",
    "useMaxCallTimeForAnsweredCalls",
    "maxCallTimeForAnsweredCallsMinutes",
    "useMaxCallTimeForUnansweredCalls",
    "maxCallTimeForUnansweredCallsMinutes",
    "mediaPolicySelection",
    "supportedMediaSetName",
    "useMaxConcurrentRedirectedCalls",
    "maxConcurrentRedirectedCalls",
    "useMaxFindMeFollowMeDepth",
    "maxFindMeFollowMeDepth",
    "maxRedirectionDepth",
    "useMaxConcurrentFindMeFollowMeInvocations",
    "maxConcurrentFindMeFollowMeInvocations",
    "clidPolicy",
    "emergencyClidPolicy",
    "allowAlternateNumbersForRedirectingIdentity",
    "useGroupName",
    "blockCallingNameForExternalCalls",
    "enableDialableCallerID",
    "allowConfigurableCLIDForRedirectingIdentity",
    "allowDepartmentCLIDNameOverride"
})
public class UserCallProcessingGetPolicyResponse19Sp1
    extends OCIDataResponse
{

    protected boolean useUserCLIDSetting;
    protected boolean useUserMediaSetting;
    protected boolean useUserCallLimitsSetting;
    protected boolean useUserDCLIDSetting;
    protected boolean useMaxSimultaneousCalls;
    protected int maxSimultaneousCalls;
    protected boolean useMaxSimultaneousVideoCalls;
    protected int maxSimultaneousVideoCalls;
    protected boolean useMaxCallTimeForAnsweredCalls;
    protected int maxCallTimeForAnsweredCallsMinutes;
    protected boolean useMaxCallTimeForUnansweredCalls;
    protected int maxCallTimeForUnansweredCallsMinutes;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MediaPolicySelection mediaPolicySelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String supportedMediaSetName;
    protected boolean useMaxConcurrentRedirectedCalls;
    protected int maxConcurrentRedirectedCalls;
    protected boolean useMaxFindMeFollowMeDepth;
    protected int maxFindMeFollowMeDepth;
    protected int maxRedirectionDepth;
    protected boolean useMaxConcurrentFindMeFollowMeInvocations;
    protected int maxConcurrentFindMeFollowMeInvocations;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupCLIDPolicy clidPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupCLIDPolicy emergencyClidPolicy;
    protected boolean allowAlternateNumbersForRedirectingIdentity;
    protected boolean useGroupName;
    protected boolean blockCallingNameForExternalCalls;
    protected boolean enableDialableCallerID;
    protected boolean allowConfigurableCLIDForRedirectingIdentity;
    protected boolean allowDepartmentCLIDNameOverride;

    /**
     * Ruft den Wert der useUserCLIDSetting-Eigenschaft ab.
     * 
     */
    public boolean isUseUserCLIDSetting() {
        return useUserCLIDSetting;
    }

    /**
     * Legt den Wert der useUserCLIDSetting-Eigenschaft fest.
     * 
     */
    public void setUseUserCLIDSetting(boolean value) {
        this.useUserCLIDSetting = value;
    }

    /**
     * Ruft den Wert der useUserMediaSetting-Eigenschaft ab.
     * 
     */
    public boolean isUseUserMediaSetting() {
        return useUserMediaSetting;
    }

    /**
     * Legt den Wert der useUserMediaSetting-Eigenschaft fest.
     * 
     */
    public void setUseUserMediaSetting(boolean value) {
        this.useUserMediaSetting = value;
    }

    /**
     * Ruft den Wert der useUserCallLimitsSetting-Eigenschaft ab.
     * 
     */
    public boolean isUseUserCallLimitsSetting() {
        return useUserCallLimitsSetting;
    }

    /**
     * Legt den Wert der useUserCallLimitsSetting-Eigenschaft fest.
     * 
     */
    public void setUseUserCallLimitsSetting(boolean value) {
        this.useUserCallLimitsSetting = value;
    }

    /**
     * Ruft den Wert der useUserDCLIDSetting-Eigenschaft ab.
     * 
     */
    public boolean isUseUserDCLIDSetting() {
        return useUserDCLIDSetting;
    }

    /**
     * Legt den Wert der useUserDCLIDSetting-Eigenschaft fest.
     * 
     */
    public void setUseUserDCLIDSetting(boolean value) {
        this.useUserDCLIDSetting = value;
    }

    /**
     * Ruft den Wert der useMaxSimultaneousCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxSimultaneousCalls() {
        return useMaxSimultaneousCalls;
    }

    /**
     * Legt den Wert der useMaxSimultaneousCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxSimultaneousCalls(boolean value) {
        this.useMaxSimultaneousCalls = value;
    }

    /**
     * Ruft den Wert der maxSimultaneousCalls-Eigenschaft ab.
     * 
     */
    public int getMaxSimultaneousCalls() {
        return maxSimultaneousCalls;
    }

    /**
     * Legt den Wert der maxSimultaneousCalls-Eigenschaft fest.
     * 
     */
    public void setMaxSimultaneousCalls(int value) {
        this.maxSimultaneousCalls = value;
    }

    /**
     * Ruft den Wert der useMaxSimultaneousVideoCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxSimultaneousVideoCalls() {
        return useMaxSimultaneousVideoCalls;
    }

    /**
     * Legt den Wert der useMaxSimultaneousVideoCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxSimultaneousVideoCalls(boolean value) {
        this.useMaxSimultaneousVideoCalls = value;
    }

    /**
     * Ruft den Wert der maxSimultaneousVideoCalls-Eigenschaft ab.
     * 
     */
    public int getMaxSimultaneousVideoCalls() {
        return maxSimultaneousVideoCalls;
    }

    /**
     * Legt den Wert der maxSimultaneousVideoCalls-Eigenschaft fest.
     * 
     */
    public void setMaxSimultaneousVideoCalls(int value) {
        this.maxSimultaneousVideoCalls = value;
    }

    /**
     * Ruft den Wert der useMaxCallTimeForAnsweredCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxCallTimeForAnsweredCalls() {
        return useMaxCallTimeForAnsweredCalls;
    }

    /**
     * Legt den Wert der useMaxCallTimeForAnsweredCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxCallTimeForAnsweredCalls(boolean value) {
        this.useMaxCallTimeForAnsweredCalls = value;
    }

    /**
     * Ruft den Wert der maxCallTimeForAnsweredCallsMinutes-Eigenschaft ab.
     * 
     */
    public int getMaxCallTimeForAnsweredCallsMinutes() {
        return maxCallTimeForAnsweredCallsMinutes;
    }

    /**
     * Legt den Wert der maxCallTimeForAnsweredCallsMinutes-Eigenschaft fest.
     * 
     */
    public void setMaxCallTimeForAnsweredCallsMinutes(int value) {
        this.maxCallTimeForAnsweredCallsMinutes = value;
    }

    /**
     * Ruft den Wert der useMaxCallTimeForUnansweredCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxCallTimeForUnansweredCalls() {
        return useMaxCallTimeForUnansweredCalls;
    }

    /**
     * Legt den Wert der useMaxCallTimeForUnansweredCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxCallTimeForUnansweredCalls(boolean value) {
        this.useMaxCallTimeForUnansweredCalls = value;
    }

    /**
     * Ruft den Wert der maxCallTimeForUnansweredCallsMinutes-Eigenschaft ab.
     * 
     */
    public int getMaxCallTimeForUnansweredCallsMinutes() {
        return maxCallTimeForUnansweredCallsMinutes;
    }

    /**
     * Legt den Wert der maxCallTimeForUnansweredCallsMinutes-Eigenschaft fest.
     * 
     */
    public void setMaxCallTimeForUnansweredCallsMinutes(int value) {
        this.maxCallTimeForUnansweredCallsMinutes = value;
    }

    /**
     * Ruft den Wert der mediaPolicySelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MediaPolicySelection }
     *     
     */
    public MediaPolicySelection getMediaPolicySelection() {
        return mediaPolicySelection;
    }

    /**
     * Legt den Wert der mediaPolicySelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MediaPolicySelection }
     *     
     */
    public void setMediaPolicySelection(MediaPolicySelection value) {
        this.mediaPolicySelection = value;
    }

    /**
     * Ruft den Wert der supportedMediaSetName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupportedMediaSetName() {
        return supportedMediaSetName;
    }

    /**
     * Legt den Wert der supportedMediaSetName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupportedMediaSetName(String value) {
        this.supportedMediaSetName = value;
    }

    /**
     * Ruft den Wert der useMaxConcurrentRedirectedCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxConcurrentRedirectedCalls() {
        return useMaxConcurrentRedirectedCalls;
    }

    /**
     * Legt den Wert der useMaxConcurrentRedirectedCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxConcurrentRedirectedCalls(boolean value) {
        this.useMaxConcurrentRedirectedCalls = value;
    }

    /**
     * Ruft den Wert der maxConcurrentRedirectedCalls-Eigenschaft ab.
     * 
     */
    public int getMaxConcurrentRedirectedCalls() {
        return maxConcurrentRedirectedCalls;
    }

    /**
     * Legt den Wert der maxConcurrentRedirectedCalls-Eigenschaft fest.
     * 
     */
    public void setMaxConcurrentRedirectedCalls(int value) {
        this.maxConcurrentRedirectedCalls = value;
    }

    /**
     * Ruft den Wert der useMaxFindMeFollowMeDepth-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxFindMeFollowMeDepth() {
        return useMaxFindMeFollowMeDepth;
    }

    /**
     * Legt den Wert der useMaxFindMeFollowMeDepth-Eigenschaft fest.
     * 
     */
    public void setUseMaxFindMeFollowMeDepth(boolean value) {
        this.useMaxFindMeFollowMeDepth = value;
    }

    /**
     * Ruft den Wert der maxFindMeFollowMeDepth-Eigenschaft ab.
     * 
     */
    public int getMaxFindMeFollowMeDepth() {
        return maxFindMeFollowMeDepth;
    }

    /**
     * Legt den Wert der maxFindMeFollowMeDepth-Eigenschaft fest.
     * 
     */
    public void setMaxFindMeFollowMeDepth(int value) {
        this.maxFindMeFollowMeDepth = value;
    }

    /**
     * Ruft den Wert der maxRedirectionDepth-Eigenschaft ab.
     * 
     */
    public int getMaxRedirectionDepth() {
        return maxRedirectionDepth;
    }

    /**
     * Legt den Wert der maxRedirectionDepth-Eigenschaft fest.
     * 
     */
    public void setMaxRedirectionDepth(int value) {
        this.maxRedirectionDepth = value;
    }

    /**
     * Ruft den Wert der useMaxConcurrentFindMeFollowMeInvocations-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxConcurrentFindMeFollowMeInvocations() {
        return useMaxConcurrentFindMeFollowMeInvocations;
    }

    /**
     * Legt den Wert der useMaxConcurrentFindMeFollowMeInvocations-Eigenschaft fest.
     * 
     */
    public void setUseMaxConcurrentFindMeFollowMeInvocations(boolean value) {
        this.useMaxConcurrentFindMeFollowMeInvocations = value;
    }

    /**
     * Ruft den Wert der maxConcurrentFindMeFollowMeInvocations-Eigenschaft ab.
     * 
     */
    public int getMaxConcurrentFindMeFollowMeInvocations() {
        return maxConcurrentFindMeFollowMeInvocations;
    }

    /**
     * Legt den Wert der maxConcurrentFindMeFollowMeInvocations-Eigenschaft fest.
     * 
     */
    public void setMaxConcurrentFindMeFollowMeInvocations(int value) {
        this.maxConcurrentFindMeFollowMeInvocations = value;
    }

    /**
     * Ruft den Wert der clidPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public GroupCLIDPolicy getClidPolicy() {
        return clidPolicy;
    }

    /**
     * Legt den Wert der clidPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public void setClidPolicy(GroupCLIDPolicy value) {
        this.clidPolicy = value;
    }

    /**
     * Ruft den Wert der emergencyClidPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public GroupCLIDPolicy getEmergencyClidPolicy() {
        return emergencyClidPolicy;
    }

    /**
     * Legt den Wert der emergencyClidPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public void setEmergencyClidPolicy(GroupCLIDPolicy value) {
        this.emergencyClidPolicy = value;
    }

    /**
     * Ruft den Wert der allowAlternateNumbersForRedirectingIdentity-Eigenschaft ab.
     * 
     */
    public boolean isAllowAlternateNumbersForRedirectingIdentity() {
        return allowAlternateNumbersForRedirectingIdentity;
    }

    /**
     * Legt den Wert der allowAlternateNumbersForRedirectingIdentity-Eigenschaft fest.
     * 
     */
    public void setAllowAlternateNumbersForRedirectingIdentity(boolean value) {
        this.allowAlternateNumbersForRedirectingIdentity = value;
    }

    /**
     * Ruft den Wert der useGroupName-Eigenschaft ab.
     * 
     */
    public boolean isUseGroupName() {
        return useGroupName;
    }

    /**
     * Legt den Wert der useGroupName-Eigenschaft fest.
     * 
     */
    public void setUseGroupName(boolean value) {
        this.useGroupName = value;
    }

    /**
     * Ruft den Wert der blockCallingNameForExternalCalls-Eigenschaft ab.
     * 
     */
    public boolean isBlockCallingNameForExternalCalls() {
        return blockCallingNameForExternalCalls;
    }

    /**
     * Legt den Wert der blockCallingNameForExternalCalls-Eigenschaft fest.
     * 
     */
    public void setBlockCallingNameForExternalCalls(boolean value) {
        this.blockCallingNameForExternalCalls = value;
    }

    /**
     * Ruft den Wert der enableDialableCallerID-Eigenschaft ab.
     * 
     */
    public boolean isEnableDialableCallerID() {
        return enableDialableCallerID;
    }

    /**
     * Legt den Wert der enableDialableCallerID-Eigenschaft fest.
     * 
     */
    public void setEnableDialableCallerID(boolean value) {
        this.enableDialableCallerID = value;
    }

    /**
     * Ruft den Wert der allowConfigurableCLIDForRedirectingIdentity-Eigenschaft ab.
     * 
     */
    public boolean isAllowConfigurableCLIDForRedirectingIdentity() {
        return allowConfigurableCLIDForRedirectingIdentity;
    }

    /**
     * Legt den Wert der allowConfigurableCLIDForRedirectingIdentity-Eigenschaft fest.
     * 
     */
    public void setAllowConfigurableCLIDForRedirectingIdentity(boolean value) {
        this.allowConfigurableCLIDForRedirectingIdentity = value;
    }

    /**
     * Ruft den Wert der allowDepartmentCLIDNameOverride-Eigenschaft ab.
     * 
     */
    public boolean isAllowDepartmentCLIDNameOverride() {
        return allowDepartmentCLIDNameOverride;
    }

    /**
     * Legt den Wert der allowDepartmentCLIDNameOverride-Eigenschaft fest.
     * 
     */
    public void setAllowDepartmentCLIDNameOverride(boolean value) {
        this.allowDepartmentCLIDNameOverride = value;
    }

}
