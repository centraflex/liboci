//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Requests a table of all the existing Call Blocking Service Mappings in the system.
 *         The response is either a SystemTreatmentMappingCallBlockingServicesGetListResponse22
 *         or an ErrorResponse.
 *         Replaced by: SystemTreatmentMappingCallBlockingServiceGetListRequest23
 *       
 * 
 * <p>Java-Klasse für SystemTreatmentMappingCallBlockingServiceGetListRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemTreatmentMappingCallBlockingServiceGetListRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemTreatmentMappingCallBlockingServiceGetListRequest22")
public class SystemTreatmentMappingCallBlockingServiceGetListRequest22
    extends OCIRequest
{


}
