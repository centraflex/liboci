//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Get an existing Digit Pattern Criteria.
 *         The response is either a SystemCommunicationBarringDigitPatternCriteriaGetPatternListResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCommunicationBarringDigitPatternCriteriaGetPatternListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCommunicationBarringDigitPatternCriteriaGetPatternListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="name" type="{}DigitPatternCriteriaName"/>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaDigitPattern" type="{}SearchCriteriaDigitPattern" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCommunicationBarringDigitPatternCriteriaGetPatternListRequest", propOrder = {
    "name",
    "responseSizeLimit",
    "searchCriteriaDigitPattern"
})
public class SystemCommunicationBarringDigitPatternCriteriaGetPatternListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    protected Integer responseSizeLimit;
    protected List<SearchCriteriaDigitPattern> searchCriteriaDigitPattern;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaDigitPattern property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDigitPattern property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDigitPattern().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDigitPattern }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDigitPattern property.
     */
    public List<SearchCriteriaDigitPattern> getSearchCriteriaDigitPattern() {
        if (searchCriteriaDigitPattern == null) {
            searchCriteriaDigitPattern = new ArrayList<>();
        }
        return this.searchCriteriaDigitPattern;
    }

}
