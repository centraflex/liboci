//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupRoutePointQueueCallDispositionCodeSettingsGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupRoutePointQueueCallDispositionCodeSettingsGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupRoutePointQueueCallDispositionCodeSettingsGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableCallDispositionCodes" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeOrganizationCodes" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forceUseOfCallDispositionCodes" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="defaultCallDispositionCode" type="{}CallDispositionCodeWithLevel" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupRoutePointQueueCallDispositionCodeSettingsGetResponse", propOrder = {
    "enableCallDispositionCodes",
    "includeOrganizationCodes",
    "forceUseOfCallDispositionCodes",
    "defaultCallDispositionCode"
})
public class GroupRoutePointQueueCallDispositionCodeSettingsGetResponse
    extends OCIDataResponse
{

    protected boolean enableCallDispositionCodes;
    protected boolean includeOrganizationCodes;
    protected boolean forceUseOfCallDispositionCodes;
    protected CallDispositionCodeWithLevel defaultCallDispositionCode;

    /**
     * Ruft den Wert der enableCallDispositionCodes-Eigenschaft ab.
     * 
     */
    public boolean isEnableCallDispositionCodes() {
        return enableCallDispositionCodes;
    }

    /**
     * Legt den Wert der enableCallDispositionCodes-Eigenschaft fest.
     * 
     */
    public void setEnableCallDispositionCodes(boolean value) {
        this.enableCallDispositionCodes = value;
    }

    /**
     * Ruft den Wert der includeOrganizationCodes-Eigenschaft ab.
     * 
     */
    public boolean isIncludeOrganizationCodes() {
        return includeOrganizationCodes;
    }

    /**
     * Legt den Wert der includeOrganizationCodes-Eigenschaft fest.
     * 
     */
    public void setIncludeOrganizationCodes(boolean value) {
        this.includeOrganizationCodes = value;
    }

    /**
     * Ruft den Wert der forceUseOfCallDispositionCodes-Eigenschaft ab.
     * 
     */
    public boolean isForceUseOfCallDispositionCodes() {
        return forceUseOfCallDispositionCodes;
    }

    /**
     * Legt den Wert der forceUseOfCallDispositionCodes-Eigenschaft fest.
     * 
     */
    public void setForceUseOfCallDispositionCodes(boolean value) {
        this.forceUseOfCallDispositionCodes = value;
    }

    /**
     * Ruft den Wert der defaultCallDispositionCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallDispositionCodeWithLevel }
     *     
     */
    public CallDispositionCodeWithLevel getDefaultCallDispositionCode() {
        return defaultCallDispositionCode;
    }

    /**
     * Legt den Wert der defaultCallDispositionCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallDispositionCodeWithLevel }
     *     
     */
    public void setDefaultCallDispositionCode(CallDispositionCodeWithLevel value) {
        this.defaultCallDispositionCode = value;
    }

}
