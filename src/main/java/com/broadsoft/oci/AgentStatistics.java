//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains Call Center Agent statistics for a given time frame.
 *       
 * 
 * <p>Java-Klasse für AgentStatistics complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="AgentStatistics">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="numberOfCallsHandled" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="numberOfCallsUnanswered" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="averageCallSeconds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="totalTalkSeconds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="totalStaffedSeconds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgentStatistics", propOrder = {
    "numberOfCallsHandled",
    "numberOfCallsUnanswered",
    "averageCallSeconds",
    "totalTalkSeconds",
    "totalStaffedSeconds"
})
public class AgentStatistics {

    protected int numberOfCallsHandled;
    protected int numberOfCallsUnanswered;
    protected int averageCallSeconds;
    protected int totalTalkSeconds;
    protected int totalStaffedSeconds;

    /**
     * Ruft den Wert der numberOfCallsHandled-Eigenschaft ab.
     * 
     */
    public int getNumberOfCallsHandled() {
        return numberOfCallsHandled;
    }

    /**
     * Legt den Wert der numberOfCallsHandled-Eigenschaft fest.
     * 
     */
    public void setNumberOfCallsHandled(int value) {
        this.numberOfCallsHandled = value;
    }

    /**
     * Ruft den Wert der numberOfCallsUnanswered-Eigenschaft ab.
     * 
     */
    public int getNumberOfCallsUnanswered() {
        return numberOfCallsUnanswered;
    }

    /**
     * Legt den Wert der numberOfCallsUnanswered-Eigenschaft fest.
     * 
     */
    public void setNumberOfCallsUnanswered(int value) {
        this.numberOfCallsUnanswered = value;
    }

    /**
     * Ruft den Wert der averageCallSeconds-Eigenschaft ab.
     * 
     */
    public int getAverageCallSeconds() {
        return averageCallSeconds;
    }

    /**
     * Legt den Wert der averageCallSeconds-Eigenschaft fest.
     * 
     */
    public void setAverageCallSeconds(int value) {
        this.averageCallSeconds = value;
    }

    /**
     * Ruft den Wert der totalTalkSeconds-Eigenschaft ab.
     * 
     */
    public int getTotalTalkSeconds() {
        return totalTalkSeconds;
    }

    /**
     * Legt den Wert der totalTalkSeconds-Eigenschaft fest.
     * 
     */
    public void setTotalTalkSeconds(int value) {
        this.totalTalkSeconds = value;
    }

    /**
     * Ruft den Wert der totalStaffedSeconds-Eigenschaft ab.
     * 
     */
    public int getTotalStaffedSeconds() {
        return totalStaffedSeconds;
    }

    /**
     * Legt den Wert der totalStaffedSeconds-Eigenschaft fest.
     * 
     */
    public void setTotalStaffedSeconds(int value) {
        this.totalStaffedSeconds = value;
    }

}
