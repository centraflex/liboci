//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemBusyLampFieldGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemBusyLampFieldGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBusyLampFieldGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="displayLocalUserIdentityLastNameFirst" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBusyLampFieldGetResponse", propOrder = {
    "displayLocalUserIdentityLastNameFirst"
})
public class SystemBusyLampFieldGetResponse
    extends OCIDataResponse
{

    protected boolean displayLocalUserIdentityLastNameFirst;

    /**
     * Ruft den Wert der displayLocalUserIdentityLastNameFirst-Eigenschaft ab.
     * 
     */
    public boolean isDisplayLocalUserIdentityLastNameFirst() {
        return displayLocalUserIdentityLastNameFirst;
    }

    /**
     * Legt den Wert der displayLocalUserIdentityLastNameFirst-Eigenschaft fest.
     * 
     */
    public void setDisplayLocalUserIdentityLastNameFirst(boolean value) {
        this.displayLocalUserIdentityLastNameFirst = value;
    }

}
