//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Enterprise Voice VPN Digit Manipulation Entry that has no value.
 *       
 * 
 * <p>Java-Klasse für EnterpriseVoiceVPNDigitManipulationNoValue complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseVoiceVPNDigitManipulationNoValue">
 *   <complexContent>
 *     <extension base="{}EnterpriseVoiceVPNDigitManipulation">
 *       <sequence>
 *         <element name="operation" type="{}EnterpriseVoiceVPNDigitManipulationOperationNoValue"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseVoiceVPNDigitManipulationNoValue", propOrder = {
    "operation"
})
public class EnterpriseVoiceVPNDigitManipulationNoValue
    extends EnterpriseVoiceVPNDigitManipulation
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseVoiceVPNDigitManipulationOperationNoValue operation;

    /**
     * Ruft den Wert der operation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseVoiceVPNDigitManipulationOperationNoValue }
     *     
     */
    public EnterpriseVoiceVPNDigitManipulationOperationNoValue getOperation() {
        return operation;
    }

    /**
     * Legt den Wert der operation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseVoiceVPNDigitManipulationOperationNoValue }
     *     
     */
    public void setOperation(EnterpriseVoiceVPNDigitManipulationOperationNoValue value) {
        this.operation = value;
    }

}
