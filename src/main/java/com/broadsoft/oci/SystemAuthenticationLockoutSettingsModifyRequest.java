//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify the system level device password lockout settings
 *         The response is either SuccessResponse or ErrorResponse.
 *      
 * 
 * <p>Java-Klasse für SystemAuthenticationLockoutSettingsModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAuthenticationLockoutSettingsModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="counterResetIntervalDays" type="{}CounterResetIntervalDays" minOccurs="0"/>
 *         <element name="counterResetHour" type="{}CounterResetHour" minOccurs="0"/>
 *         <element name="counterResetMinute" type="{}CounterResetMinute" minOccurs="0"/>
 *         <element name="emergencySIPBypassAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAuthenticationLockoutSettingsModifyRequest", propOrder = {
    "counterResetIntervalDays",
    "counterResetHour",
    "counterResetMinute",
    "emergencySIPBypassAllowed"
})
public class SystemAuthenticationLockoutSettingsModifyRequest
    extends OCIRequest
{

    protected Integer counterResetIntervalDays;
    protected Integer counterResetHour;
    protected Integer counterResetMinute;
    protected Boolean emergencySIPBypassAllowed;

    /**
     * Ruft den Wert der counterResetIntervalDays-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCounterResetIntervalDays() {
        return counterResetIntervalDays;
    }

    /**
     * Legt den Wert der counterResetIntervalDays-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCounterResetIntervalDays(Integer value) {
        this.counterResetIntervalDays = value;
    }

    /**
     * Ruft den Wert der counterResetHour-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCounterResetHour() {
        return counterResetHour;
    }

    /**
     * Legt den Wert der counterResetHour-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCounterResetHour(Integer value) {
        this.counterResetHour = value;
    }

    /**
     * Ruft den Wert der counterResetMinute-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCounterResetMinute() {
        return counterResetMinute;
    }

    /**
     * Legt den Wert der counterResetMinute-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCounterResetMinute(Integer value) {
        this.counterResetMinute = value;
    }

    /**
     * Ruft den Wert der emergencySIPBypassAllowed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEmergencySIPBypassAllowed() {
        return emergencySIPBypassAllowed;
    }

    /**
     * Legt den Wert der emergencySIPBypassAllowed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEmergencySIPBypassAllowed(Boolean value) {
        this.emergencySIPBypassAllowed = value;
    }

}
