//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to ServiceProviderDialPlanPolicyGetRequest
 *         
 *         Replaced by: ServiceProviderDialPlanPolicyGetResponse17
 *       
 * 
 * <p>Java-Klasse für ServiceProviderDialPlanPolicyGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderDialPlanPolicyGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="requiresAccessCodeForPublicCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowE164PublicCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="publicDigitMap" type="{}DigitMap" minOccurs="0"/>
 *         <element name="privateDigitMap" type="{}DigitMap" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderDialPlanPolicyGetResponse", propOrder = {
    "requiresAccessCodeForPublicCalls",
    "allowE164PublicCalls",
    "publicDigitMap",
    "privateDigitMap"
})
public class ServiceProviderDialPlanPolicyGetResponse
    extends OCIDataResponse
{

    protected boolean requiresAccessCodeForPublicCalls;
    protected boolean allowE164PublicCalls;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String publicDigitMap;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String privateDigitMap;

    /**
     * Ruft den Wert der requiresAccessCodeForPublicCalls-Eigenschaft ab.
     * 
     */
    public boolean isRequiresAccessCodeForPublicCalls() {
        return requiresAccessCodeForPublicCalls;
    }

    /**
     * Legt den Wert der requiresAccessCodeForPublicCalls-Eigenschaft fest.
     * 
     */
    public void setRequiresAccessCodeForPublicCalls(boolean value) {
        this.requiresAccessCodeForPublicCalls = value;
    }

    /**
     * Ruft den Wert der allowE164PublicCalls-Eigenschaft ab.
     * 
     */
    public boolean isAllowE164PublicCalls() {
        return allowE164PublicCalls;
    }

    /**
     * Legt den Wert der allowE164PublicCalls-Eigenschaft fest.
     * 
     */
    public void setAllowE164PublicCalls(boolean value) {
        this.allowE164PublicCalls = value;
    }

    /**
     * Ruft den Wert der publicDigitMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPublicDigitMap() {
        return publicDigitMap;
    }

    /**
     * Legt den Wert der publicDigitMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPublicDigitMap(String value) {
        this.publicDigitMap = value;
    }

    /**
     * Ruft den Wert der privateDigitMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrivateDigitMap() {
        return privateDigitMap;
    }

    /**
     * Legt den Wert der privateDigitMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrivateDigitMap(String value) {
        this.privateDigitMap = value;
    }

}
