//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemCommunicationBarringUserControlGetRequest.
 *         Contains the settings to whole system for Communication Barring User-Control
 *       
 * 
 * <p>Java-Klasse für SystemCommunicationBarringUserControlGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCommunicationBarringUserControlGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableLockout" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxNumberOfFailedAttempts" type="{}CommunicationBarringUserControlNumberOfAttempts"/>
 *         <element name="lockoutMinutes" type="{}CommunicationBarringUserControlLockoutMinutes"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCommunicationBarringUserControlGetResponse", propOrder = {
    "enableLockout",
    "maxNumberOfFailedAttempts",
    "lockoutMinutes"
})
public class SystemCommunicationBarringUserControlGetResponse
    extends OCIDataResponse
{

    protected boolean enableLockout;
    protected int maxNumberOfFailedAttempts;
    protected int lockoutMinutes;

    /**
     * Ruft den Wert der enableLockout-Eigenschaft ab.
     * 
     */
    public boolean isEnableLockout() {
        return enableLockout;
    }

    /**
     * Legt den Wert der enableLockout-Eigenschaft fest.
     * 
     */
    public void setEnableLockout(boolean value) {
        this.enableLockout = value;
    }

    /**
     * Ruft den Wert der maxNumberOfFailedAttempts-Eigenschaft ab.
     * 
     */
    public int getMaxNumberOfFailedAttempts() {
        return maxNumberOfFailedAttempts;
    }

    /**
     * Legt den Wert der maxNumberOfFailedAttempts-Eigenschaft fest.
     * 
     */
    public void setMaxNumberOfFailedAttempts(int value) {
        this.maxNumberOfFailedAttempts = value;
    }

    /**
     * Ruft den Wert der lockoutMinutes-Eigenschaft ab.
     * 
     */
    public int getLockoutMinutes() {
        return lockoutMinutes;
    }

    /**
     * Legt den Wert der lockoutMinutes-Eigenschaft fest.
     * 
     */
    public void setLockoutMinutes(int value) {
        this.lockoutMinutes = value;
    }

}
