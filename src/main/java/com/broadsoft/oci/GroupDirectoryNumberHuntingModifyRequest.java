//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Replaces a list of users as agents for a directory number hunting group.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupDirectoryNumberHuntingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDirectoryNumberHuntingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="agentUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *         <element name="useTerminateCallToAgentFirst" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useOriginalAgentServicesForBusyAndNoAnswerCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDirectoryNumberHuntingModifyRequest", propOrder = {
    "serviceUserId",
    "agentUserIdList",
    "useTerminateCallToAgentFirst",
    "useOriginalAgentServicesForBusyAndNoAnswerCalls"
})
public class GroupDirectoryNumberHuntingModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlElementRef(name = "agentUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> agentUserIdList;
    protected Boolean useTerminateCallToAgentFirst;
    protected Boolean useOriginalAgentServicesForBusyAndNoAnswerCalls;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der agentUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getAgentUserIdList() {
        return agentUserIdList;
    }

    /**
     * Legt den Wert der agentUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setAgentUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.agentUserIdList = value;
    }

    /**
     * Ruft den Wert der useTerminateCallToAgentFirst-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseTerminateCallToAgentFirst() {
        return useTerminateCallToAgentFirst;
    }

    /**
     * Legt den Wert der useTerminateCallToAgentFirst-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseTerminateCallToAgentFirst(Boolean value) {
        this.useTerminateCallToAgentFirst = value;
    }

    /**
     * Ruft den Wert der useOriginalAgentServicesForBusyAndNoAnswerCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseOriginalAgentServicesForBusyAndNoAnswerCalls() {
        return useOriginalAgentServicesForBusyAndNoAnswerCalls;
    }

    /**
     * Legt den Wert der useOriginalAgentServicesForBusyAndNoAnswerCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseOriginalAgentServicesForBusyAndNoAnswerCalls(Boolean value) {
        this.useOriginalAgentServicesForBusyAndNoAnswerCalls = value;
    }

}
