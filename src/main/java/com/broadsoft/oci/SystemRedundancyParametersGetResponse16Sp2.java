//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemRedundancyParametersGetRequest16sp2.
 *         Contains a list of system Redundancy parameters.
 *       
 * 
 * <p>Java-Klasse für SystemRedundancyParametersGetResponse16sp2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemRedundancyParametersGetResponse16sp2">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="rollBackTimerMinutes" type="{}RedundancyRollBackTimerMinutes"/>
 *         <element name="sendSipOptionMessageUponMigration" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemRedundancyParametersGetResponse16sp2", propOrder = {
    "rollBackTimerMinutes",
    "sendSipOptionMessageUponMigration"
})
public class SystemRedundancyParametersGetResponse16Sp2
    extends OCIDataResponse
{

    protected int rollBackTimerMinutes;
    protected boolean sendSipOptionMessageUponMigration;

    /**
     * Ruft den Wert der rollBackTimerMinutes-Eigenschaft ab.
     * 
     */
    public int getRollBackTimerMinutes() {
        return rollBackTimerMinutes;
    }

    /**
     * Legt den Wert der rollBackTimerMinutes-Eigenschaft fest.
     * 
     */
    public void setRollBackTimerMinutes(int value) {
        this.rollBackTimerMinutes = value;
    }

    /**
     * Ruft den Wert der sendSipOptionMessageUponMigration-Eigenschaft ab.
     * 
     */
    public boolean isSendSipOptionMessageUponMigration() {
        return sendSipOptionMessageUponMigration;
    }

    /**
     * Legt den Wert der sendSipOptionMessageUponMigration-Eigenschaft fest.
     * 
     */
    public void setSendSipOptionMessageUponMigration(boolean value) {
        this.sendSipOptionMessageUponMigration = value;
    }

}
