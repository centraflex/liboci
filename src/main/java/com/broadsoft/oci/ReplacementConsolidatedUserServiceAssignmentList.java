//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         A list of user services that replaces existing user services assigned to the user.
 *         If a service is not authorized to the group, the service will be authorized. The authorizedQuantity will be used if provided; otherwise, the service quantity will be set to unlimited. The command will fail if the authorized Quantity set at the service provider is insufficient. 
 *         If a service is already authorized to the group, the service quantity will be ignored if included.
 *       
 * 
 * <p>Java-Klasse für ReplacementConsolidatedUserServiceAssignmentList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ReplacementConsolidatedUserServiceAssignmentList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="userServiceServiceName" type="{}ConsolidatedUserServiceAssignment" maxOccurs="unbounded"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReplacementConsolidatedUserServiceAssignmentList", propOrder = {
    "userServiceServiceName"
})
public class ReplacementConsolidatedUserServiceAssignmentList {

    @XmlElement(required = true)
    protected List<ConsolidatedUserServiceAssignment> userServiceServiceName;

    /**
     * Gets the value of the userServiceServiceName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the userServiceServiceName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserServiceServiceName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConsolidatedUserServiceAssignment }
     * 
     * 
     * @return
     *     The value of the userServiceServiceName property.
     */
    public List<ConsolidatedUserServiceAssignment> getUserServiceServiceName() {
        if (userServiceServiceName == null) {
            userServiceServiceName = new ArrayList<>();
        }
        return this.userServiceServiceName;
    }

}
