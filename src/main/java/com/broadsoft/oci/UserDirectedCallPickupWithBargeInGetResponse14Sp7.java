//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserDirectedCallPickupWithBargeInGetRequest14sp7.
 *       
 * 
 * <p>Java-Klasse für UserDirectedCallPickupWithBargeInGetResponse14sp7 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserDirectedCallPickupWithBargeInGetResponse14sp7">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableBargeInWarningTone" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableAutomaticTargetSelection" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDirectedCallPickupWithBargeInGetResponse14sp7", propOrder = {
    "enableBargeInWarningTone",
    "enableAutomaticTargetSelection"
})
public class UserDirectedCallPickupWithBargeInGetResponse14Sp7
    extends OCIDataResponse
{

    protected boolean enableBargeInWarningTone;
    protected boolean enableAutomaticTargetSelection;

    /**
     * Ruft den Wert der enableBargeInWarningTone-Eigenschaft ab.
     * 
     */
    public boolean isEnableBargeInWarningTone() {
        return enableBargeInWarningTone;
    }

    /**
     * Legt den Wert der enableBargeInWarningTone-Eigenschaft fest.
     * 
     */
    public void setEnableBargeInWarningTone(boolean value) {
        this.enableBargeInWarningTone = value;
    }

    /**
     * Ruft den Wert der enableAutomaticTargetSelection-Eigenschaft ab.
     * 
     */
    public boolean isEnableAutomaticTargetSelection() {
        return enableAutomaticTargetSelection;
    }

    /**
     * Legt den Wert der enableAutomaticTargetSelection-Eigenschaft fest.
     * 
     */
    public void setEnableAutomaticTargetSelection(boolean value) {
        this.enableAutomaticTargetSelection = value;
    }

}
