//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupDeviceTypeFileGetListRequest14sp8.
 *         Contains a table of device type files managed by the Device Management System, on a per-group basis.
 *         The column headings are: "File Format", "Is Authenticated", "Access URL", "Repository URL", "Template URL".
 *         
 *         Replaced by: GroupDeviceTypeFileGetListResponse21
 *       
 * 
 * <p>Java-Klasse für GroupDeviceTypeFileGetListResponse14sp8 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDeviceTypeFileGetListResponse14sp8">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="groupDeviceTypeFilesTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDeviceTypeFileGetListResponse14sp8", propOrder = {
    "groupDeviceTypeFilesTable"
})
public class GroupDeviceTypeFileGetListResponse14Sp8
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable groupDeviceTypeFilesTable;

    /**
     * Ruft den Wert der groupDeviceTypeFilesTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getGroupDeviceTypeFilesTable() {
        return groupDeviceTypeFilesTable;
    }

    /**
     * Legt den Wert der groupDeviceTypeFilesTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setGroupDeviceTypeFilesTable(OCITable value) {
        this.groupDeviceTypeFilesTable = value;
    }

}
