//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal play message menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für PlayMessagesMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="PlayMessagesMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="saveMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="deleteMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="playMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="previousMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
 *         <element name="nextMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="callbackCaller" type="{}DigitAny" minOccurs="0"/>
 *         <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="replyMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="forwardMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="additionalMessageOptions" type="{}DigitAny" minOccurs="0"/>
 *         <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
 *         <element name="passcode" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlayMessagesMenuKeysModifyEntry", propOrder = {
    "saveMessage",
    "deleteMessage",
    "playMessage",
    "previousMessage",
    "playEnvelope",
    "nextMessage",
    "callbackCaller",
    "composeMessage",
    "replyMessage",
    "forwardMessage",
    "additionalMessageOptions",
    "personalizedName",
    "passcode",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class PlayMessagesMenuKeysModifyEntry {

    @XmlElementRef(name = "saveMessage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> saveMessage;
    @XmlElementRef(name = "deleteMessage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deleteMessage;
    @XmlElementRef(name = "playMessage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> playMessage;
    @XmlElementRef(name = "previousMessage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> previousMessage;
    @XmlElementRef(name = "playEnvelope", type = JAXBElement.class, required = false)
    protected JAXBElement<String> playEnvelope;
    @XmlElementRef(name = "nextMessage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nextMessage;
    @XmlElementRef(name = "callbackCaller", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callbackCaller;
    @XmlElementRef(name = "composeMessage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> composeMessage;
    @XmlElementRef(name = "replyMessage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> replyMessage;
    @XmlElementRef(name = "forwardMessage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forwardMessage;
    @XmlElementRef(name = "additionalMessageOptions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> additionalMessageOptions;
    @XmlElementRef(name = "personalizedName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> personalizedName;
    @XmlElementRef(name = "passcode", type = JAXBElement.class, required = false)
    protected JAXBElement<String> passcode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repeatMenu;

    /**
     * Ruft den Wert der saveMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSaveMessage() {
        return saveMessage;
    }

    /**
     * Legt den Wert der saveMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSaveMessage(JAXBElement<String> value) {
        this.saveMessage = value;
    }

    /**
     * Ruft den Wert der deleteMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeleteMessage() {
        return deleteMessage;
    }

    /**
     * Legt den Wert der deleteMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeleteMessage(JAXBElement<String> value) {
        this.deleteMessage = value;
    }

    /**
     * Ruft den Wert der playMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlayMessage() {
        return playMessage;
    }

    /**
     * Legt den Wert der playMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlayMessage(JAXBElement<String> value) {
        this.playMessage = value;
    }

    /**
     * Ruft den Wert der previousMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPreviousMessage() {
        return previousMessage;
    }

    /**
     * Legt den Wert der previousMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPreviousMessage(JAXBElement<String> value) {
        this.previousMessage = value;
    }

    /**
     * Ruft den Wert der playEnvelope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlayEnvelope() {
        return playEnvelope;
    }

    /**
     * Legt den Wert der playEnvelope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlayEnvelope(JAXBElement<String> value) {
        this.playEnvelope = value;
    }

    /**
     * Ruft den Wert der nextMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNextMessage() {
        return nextMessage;
    }

    /**
     * Legt den Wert der nextMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNextMessage(JAXBElement<String> value) {
        this.nextMessage = value;
    }

    /**
     * Ruft den Wert der callbackCaller-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallbackCaller() {
        return callbackCaller;
    }

    /**
     * Legt den Wert der callbackCaller-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallbackCaller(JAXBElement<String> value) {
        this.callbackCaller = value;
    }

    /**
     * Ruft den Wert der composeMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getComposeMessage() {
        return composeMessage;
    }

    /**
     * Legt den Wert der composeMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setComposeMessage(JAXBElement<String> value) {
        this.composeMessage = value;
    }

    /**
     * Ruft den Wert der replyMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReplyMessage() {
        return replyMessage;
    }

    /**
     * Legt den Wert der replyMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReplyMessage(JAXBElement<String> value) {
        this.replyMessage = value;
    }

    /**
     * Ruft den Wert der forwardMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForwardMessage() {
        return forwardMessage;
    }

    /**
     * Legt den Wert der forwardMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForwardMessage(JAXBElement<String> value) {
        this.forwardMessage = value;
    }

    /**
     * Ruft den Wert der additionalMessageOptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAdditionalMessageOptions() {
        return additionalMessageOptions;
    }

    /**
     * Legt den Wert der additionalMessageOptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAdditionalMessageOptions(JAXBElement<String> value) {
        this.additionalMessageOptions = value;
    }

    /**
     * Ruft den Wert der personalizedName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPersonalizedName() {
        return personalizedName;
    }

    /**
     * Legt den Wert der personalizedName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPersonalizedName(JAXBElement<String> value) {
        this.personalizedName = value;
    }

    /**
     * Ruft den Wert der passcode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPasscode() {
        return passcode;
    }

    /**
     * Legt den Wert der passcode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPasscode(JAXBElement<String> value) {
        this.passcode = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepeatMenu(JAXBElement<String> value) {
        this.repeatMenu = value;
    }

}
