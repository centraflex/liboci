//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PhysicalLocationIndicator.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="PhysicalLocationIndicator">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="CI"/>
 *     <enumeration value="LAC-CI"/>
 *     <enumeration value="PANI"/>
 *     <enumeration value="Disregard Zones"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "PhysicalLocationIndicator")
@XmlEnum
public enum PhysicalLocationIndicator {

    CI("CI"),
    @XmlEnumValue("LAC-CI")
    LAC_CI("LAC-CI"),
    PANI("PANI"),
    @XmlEnumValue("Disregard Zones")
    DISREGARD_ZONES("Disregard Zones");
    private final String value;

    PhysicalLocationIndicator(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PhysicalLocationIndicator fromValue(String v) {
        for (PhysicalLocationIndicator c: PhysicalLocationIndicator.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
