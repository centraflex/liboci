//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Information related to a tree device.
 *         A tree device is a device associated with a device type that has the option 
 *         supportLinks set to "Support Links from Devices".  Many leaf devices can link to it.
 *         When a tree device is created, it is assigned a system-wide unique linkId.        
 *       
 * 
 * <p>Java-Klasse für TreeDeviceInfo complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="TreeDeviceInfo">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="treeDeviceKey" type="{}AccessDeviceKey"/>
 *         <element name="linkId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TreeDeviceInfo", propOrder = {
    "treeDeviceKey",
    "linkId"
})
public class TreeDeviceInfo {

    @XmlElement(required = true)
    protected AccessDeviceKey treeDeviceKey;
    @XmlElement(required = true)
    protected String linkId;

    /**
     * Ruft den Wert der treeDeviceKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceKey }
     *     
     */
    public AccessDeviceKey getTreeDeviceKey() {
        return treeDeviceKey;
    }

    /**
     * Legt den Wert der treeDeviceKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceKey }
     *     
     */
    public void setTreeDeviceKey(AccessDeviceKey value) {
        this.treeDeviceKey = value;
    }

    /**
     * Ruft den Wert der linkId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinkId() {
        return linkId;
    }

    /**
     * Legt den Wert der linkId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinkId(String value) {
        this.linkId = value;
    }

}
