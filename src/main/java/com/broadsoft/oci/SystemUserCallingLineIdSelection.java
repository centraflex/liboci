//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SystemUserCallingLineIdSelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SystemUserCallingLineIdSelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Disable All"/>
 *     <enumeration value="Enable All"/>
 *     <enumeration value="Enable All Except Emergency"/>
 *     <enumeration value="Enable Emergency Only"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SystemUserCallingLineIdSelection")
@XmlEnum
public enum SystemUserCallingLineIdSelection {

    @XmlEnumValue("Disable All")
    DISABLE_ALL("Disable All"),
    @XmlEnumValue("Enable All")
    ENABLE_ALL("Enable All"),
    @XmlEnumValue("Enable All Except Emergency")
    ENABLE_ALL_EXCEPT_EMERGENCY("Enable All Except Emergency"),
    @XmlEnumValue("Enable Emergency Only")
    ENABLE_EMERGENCY_ONLY("Enable Emergency Only");
    private final String value;

    SystemUserCallingLineIdSelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SystemUserCallingLineIdSelection fromValue(String v) {
        for (SystemUserCallingLineIdSelection c: SystemUserCallingLineIdSelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
