//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the SystemIntegratedIMPGetRequest.
 *         The response contains the system Integrated IMP service attributes.
 *         
 *         Replaced by SystemIntegratedIMPGetResponse19.
 *       
 * 
 * <p>Java-Klasse für SystemIntegratedIMPGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemIntegratedIMPGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceDomain" type="{}DomainName" minOccurs="0"/>
 *         <element name="servicePort" type="{}Port" minOccurs="0"/>
 *         <element name="addServiceProviderInIMPUserId" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemIntegratedIMPGetResponse", propOrder = {
    "serviceDomain",
    "servicePort",
    "addServiceProviderInIMPUserId"
})
public class SystemIntegratedIMPGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceDomain;
    protected Integer servicePort;
    protected boolean addServiceProviderInIMPUserId;

    /**
     * Ruft den Wert der serviceDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceDomain() {
        return serviceDomain;
    }

    /**
     * Legt den Wert der serviceDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceDomain(String value) {
        this.serviceDomain = value;
    }

    /**
     * Ruft den Wert der servicePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServicePort() {
        return servicePort;
    }

    /**
     * Legt den Wert der servicePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServicePort(Integer value) {
        this.servicePort = value;
    }

    /**
     * Ruft den Wert der addServiceProviderInIMPUserId-Eigenschaft ab.
     * 
     */
    public boolean isAddServiceProviderInIMPUserId() {
        return addServiceProviderInIMPUserId;
    }

    /**
     * Legt den Wert der addServiceProviderInIMPUserId-Eigenschaft fest.
     * 
     */
    public void setAddServiceProviderInIMPUserId(boolean value) {
        this.addServiceProviderInIMPUserId = value;
    }

}
