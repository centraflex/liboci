//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Set the status configuration for a given call center.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterQueueStatusNotificationModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterQueueStatusNotificationModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="enableQueueStatusNotification" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableQueueDepthThreshold" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableWaitingTimeThreshold" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="numberOfCallsThreshold" type="{}CallCenterQueueDepthNotificationThreshold" minOccurs="0"/>
 *         <element name="waitingTimeOfCallsThreshold" type="{}CallCenterWaitingTimeNotificationThresholdSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterQueueStatusNotificationModifyRequest", propOrder = {
    "serviceUserId",
    "enableQueueStatusNotification",
    "enableQueueDepthThreshold",
    "enableWaitingTimeThreshold",
    "numberOfCallsThreshold",
    "waitingTimeOfCallsThreshold"
})
public class GroupCallCenterQueueStatusNotificationModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected Boolean enableQueueStatusNotification;
    protected Boolean enableQueueDepthThreshold;
    protected Boolean enableWaitingTimeThreshold;
    protected Integer numberOfCallsThreshold;
    protected Integer waitingTimeOfCallsThreshold;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der enableQueueStatusNotification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableQueueStatusNotification() {
        return enableQueueStatusNotification;
    }

    /**
     * Legt den Wert der enableQueueStatusNotification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableQueueStatusNotification(Boolean value) {
        this.enableQueueStatusNotification = value;
    }

    /**
     * Ruft den Wert der enableQueueDepthThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableQueueDepthThreshold() {
        return enableQueueDepthThreshold;
    }

    /**
     * Legt den Wert der enableQueueDepthThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableQueueDepthThreshold(Boolean value) {
        this.enableQueueDepthThreshold = value;
    }

    /**
     * Ruft den Wert der enableWaitingTimeThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableWaitingTimeThreshold() {
        return enableWaitingTimeThreshold;
    }

    /**
     * Legt den Wert der enableWaitingTimeThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableWaitingTimeThreshold(Boolean value) {
        this.enableWaitingTimeThreshold = value;
    }

    /**
     * Ruft den Wert der numberOfCallsThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfCallsThreshold() {
        return numberOfCallsThreshold;
    }

    /**
     * Legt den Wert der numberOfCallsThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfCallsThreshold(Integer value) {
        this.numberOfCallsThreshold = value;
    }

    /**
     * Ruft den Wert der waitingTimeOfCallsThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWaitingTimeOfCallsThreshold() {
        return waitingTimeOfCallsThreshold;
    }

    /**
     * Legt den Wert der waitingTimeOfCallsThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWaitingTimeOfCallsThreshold(Integer value) {
        this.waitingTimeOfCallsThreshold = value;
    }

}
