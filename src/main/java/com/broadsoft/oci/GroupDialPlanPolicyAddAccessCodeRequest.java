//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to add a Group level Dial Plan Access Code and its all attribues.
 *         The response is either SuccessResponse or ErrorResponse.  
 *       
 * 
 * <p>Java-Klasse für GroupDialPlanPolicyAddAccessCodeRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDialPlanPolicyAddAccessCodeRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="accessCode" type="{}DialPlanAccessCode"/>
 *         <element name="includeCodeForNetworkTranslationsAndRouting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeCodeForScreeningServices" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableSecondaryDialTone" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="description" type="{}DialPlanAccessCodeDescription" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDialPlanPolicyAddAccessCodeRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "accessCode",
    "includeCodeForNetworkTranslationsAndRouting",
    "includeCodeForScreeningServices",
    "enableSecondaryDialTone",
    "description"
})
public class GroupDialPlanPolicyAddAccessCodeRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accessCode;
    protected boolean includeCodeForNetworkTranslationsAndRouting;
    protected boolean includeCodeForScreeningServices;
    protected boolean enableSecondaryDialTone;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der accessCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessCode() {
        return accessCode;
    }

    /**
     * Legt den Wert der accessCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessCode(String value) {
        this.accessCode = value;
    }

    /**
     * Ruft den Wert der includeCodeForNetworkTranslationsAndRouting-Eigenschaft ab.
     * 
     */
    public boolean isIncludeCodeForNetworkTranslationsAndRouting() {
        return includeCodeForNetworkTranslationsAndRouting;
    }

    /**
     * Legt den Wert der includeCodeForNetworkTranslationsAndRouting-Eigenschaft fest.
     * 
     */
    public void setIncludeCodeForNetworkTranslationsAndRouting(boolean value) {
        this.includeCodeForNetworkTranslationsAndRouting = value;
    }

    /**
     * Ruft den Wert der includeCodeForScreeningServices-Eigenschaft ab.
     * 
     */
    public boolean isIncludeCodeForScreeningServices() {
        return includeCodeForScreeningServices;
    }

    /**
     * Legt den Wert der includeCodeForScreeningServices-Eigenschaft fest.
     * 
     */
    public void setIncludeCodeForScreeningServices(boolean value) {
        this.includeCodeForScreeningServices = value;
    }

    /**
     * Ruft den Wert der enableSecondaryDialTone-Eigenschaft ab.
     * 
     */
    public boolean isEnableSecondaryDialTone() {
        return enableSecondaryDialTone;
    }

    /**
     * Legt den Wert der enableSecondaryDialTone-Eigenschaft fest.
     * 
     */
    public void setEnableSecondaryDialTone(boolean value) {
        this.enableSecondaryDialTone = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

}
