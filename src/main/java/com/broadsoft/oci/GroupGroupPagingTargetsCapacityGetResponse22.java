//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupGroupPagingTargetsCapacityGetRequest22.
 *       
 * 
 * <p>Java-Klasse für GroupGroupPagingTargetsCapacityGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupGroupPagingTargetsCapacityGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maximumTargetUsersFromServiceProvider" type="{}GroupPagingMaxTargetCapacity22"/>
 *         <element name="maximumTargetUsers" type="{}GroupPagingMaxTargetCapacity22"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupGroupPagingTargetsCapacityGetResponse22", propOrder = {
    "maximumTargetUsersFromServiceProvider",
    "maximumTargetUsers"
})
public class GroupGroupPagingTargetsCapacityGetResponse22
    extends OCIDataResponse
{

    protected int maximumTargetUsersFromServiceProvider;
    protected int maximumTargetUsers;

    /**
     * Ruft den Wert der maximumTargetUsersFromServiceProvider-Eigenschaft ab.
     * 
     */
    public int getMaximumTargetUsersFromServiceProvider() {
        return maximumTargetUsersFromServiceProvider;
    }

    /**
     * Legt den Wert der maximumTargetUsersFromServiceProvider-Eigenschaft fest.
     * 
     */
    public void setMaximumTargetUsersFromServiceProvider(int value) {
        this.maximumTargetUsersFromServiceProvider = value;
    }

    /**
     * Ruft den Wert der maximumTargetUsers-Eigenschaft ab.
     * 
     */
    public int getMaximumTargetUsers() {
        return maximumTargetUsers;
    }

    /**
     * Legt den Wert der maximumTargetUsers-Eigenschaft fest.
     * 
     */
    public void setMaximumTargetUsers(int value) {
        this.maximumTargetUsers = value;
    }

}
