//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CLIDPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CLIDPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Use DN"/>
 *     <enumeration value="Use Configurable CLID"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CLIDPolicy")
@XmlEnum
public enum CLIDPolicy {

    @XmlEnumValue("Use DN")
    USE_DN("Use DN"),
    @XmlEnumValue("Use Configurable CLID")
    USE_CONFIGURABLE_CLID("Use Configurable CLID");
    private final String value;

    CLIDPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CLIDPolicy fromValue(String v) {
        for (CLIDPolicy c: CLIDPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
