//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to ResellerVoiceMessagingGroupGetRequest.
 *       
 * 
 * <p>Java-Klasse für ResellerVoiceMessagingGroupGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerVoiceMessagingGroupGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="deliveryFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="notificationFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="voicePortalLockoutFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerVoiceMessagingGroupGetResponse", propOrder = {
    "deliveryFromAddress",
    "notificationFromAddress",
    "voicePortalLockoutFromAddress"
})
public class ResellerVoiceMessagingGroupGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deliveryFromAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String notificationFromAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String voicePortalLockoutFromAddress;

    /**
     * Ruft den Wert der deliveryFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryFromAddress() {
        return deliveryFromAddress;
    }

    /**
     * Legt den Wert der deliveryFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryFromAddress(String value) {
        this.deliveryFromAddress = value;
    }

    /**
     * Ruft den Wert der notificationFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotificationFromAddress() {
        return notificationFromAddress;
    }

    /**
     * Legt den Wert der notificationFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotificationFromAddress(String value) {
        this.notificationFromAddress = value;
    }

    /**
     * Ruft den Wert der voicePortalLockoutFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoicePortalLockoutFromAddress() {
        return voicePortalLockoutFromAddress;
    }

    /**
     * Legt den Wert der voicePortalLockoutFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoicePortalLockoutFromAddress(String value) {
        this.voicePortalLockoutFromAddress = value;
    }

}
