//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         A list of CallProcessingPolicyProfileSubscriberType21. The list replaces a previously configured list.
 *       
 * 
 * <p>Java-Klasse für ReplacementCallProcessingPolicyProfileSubscriberTypeList21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ReplacementCallProcessingPolicyProfileSubscriberTypeList21">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="subscriberType" type="{}CallProcessingPolicyProfileSubscriberType21" maxOccurs="unbounded"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReplacementCallProcessingPolicyProfileSubscriberTypeList21", propOrder = {
    "subscriberType"
})
public class ReplacementCallProcessingPolicyProfileSubscriberTypeList21 {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected List<CallProcessingPolicyProfileSubscriberType21> subscriberType;

    /**
     * Gets the value of the subscriberType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the subscriberType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubscriberType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallProcessingPolicyProfileSubscriberType21 }
     * 
     * 
     * @return
     *     The value of the subscriberType property.
     */
    public List<CallProcessingPolicyProfileSubscriberType21> getSubscriberType() {
        if (subscriberType == null) {
            subscriberType = new ArrayList<>();
        }
        return this.subscriberType;
    }

}
