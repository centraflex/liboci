//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modifies a Call Capacity Management group. Replaces the entire list of users in the group.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCallCapacityManagementModifyInstanceRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCapacityManagementModifyInstanceRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="name" type="{}ServiceInstanceName"/>
 *         <element name="newName" type="{}ServiceInstanceName" minOccurs="0"/>
 *         <element name="maxActiveCallsAllowed" type="{}CallCapacityCallLimit" minOccurs="0"/>
 *         <element name="maxIncomingActiveCallsAllowed" type="{}CallCapacityCallLimit" minOccurs="0"/>
 *         <element name="maxOutgoingActiveCallsAllowed" type="{}CallCapacityCallLimit" minOccurs="0"/>
 *         <element name="becomeDefaultGroupForNewUsers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="userIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCapacityManagementModifyInstanceRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "name",
    "newName",
    "maxActiveCallsAllowed",
    "maxIncomingActiveCallsAllowed",
    "maxOutgoingActiveCallsAllowed",
    "becomeDefaultGroupForNewUsers",
    "userIdList"
})
public class GroupCallCapacityManagementModifyInstanceRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newName;
    protected Integer maxActiveCallsAllowed;
    @XmlElementRef(name = "maxIncomingActiveCallsAllowed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxIncomingActiveCallsAllowed;
    @XmlElementRef(name = "maxOutgoingActiveCallsAllowed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxOutgoingActiveCallsAllowed;
    protected Boolean becomeDefaultGroupForNewUsers;
    @XmlElementRef(name = "userIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> userIdList;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der newName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewName() {
        return newName;
    }

    /**
     * Legt den Wert der newName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewName(String value) {
        this.newName = value;
    }

    /**
     * Ruft den Wert der maxActiveCallsAllowed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxActiveCallsAllowed() {
        return maxActiveCallsAllowed;
    }

    /**
     * Legt den Wert der maxActiveCallsAllowed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxActiveCallsAllowed(Integer value) {
        this.maxActiveCallsAllowed = value;
    }

    /**
     * Ruft den Wert der maxIncomingActiveCallsAllowed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxIncomingActiveCallsAllowed() {
        return maxIncomingActiveCallsAllowed;
    }

    /**
     * Legt den Wert der maxIncomingActiveCallsAllowed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxIncomingActiveCallsAllowed(JAXBElement<Integer> value) {
        this.maxIncomingActiveCallsAllowed = value;
    }

    /**
     * Ruft den Wert der maxOutgoingActiveCallsAllowed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxOutgoingActiveCallsAllowed() {
        return maxOutgoingActiveCallsAllowed;
    }

    /**
     * Legt den Wert der maxOutgoingActiveCallsAllowed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxOutgoingActiveCallsAllowed(JAXBElement<Integer> value) {
        this.maxOutgoingActiveCallsAllowed = value;
    }

    /**
     * Ruft den Wert der becomeDefaultGroupForNewUsers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBecomeDefaultGroupForNewUsers() {
        return becomeDefaultGroupForNewUsers;
    }

    /**
     * Legt den Wert der becomeDefaultGroupForNewUsers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBecomeDefaultGroupForNewUsers(Boolean value) {
        this.becomeDefaultGroupForNewUsers = value;
    }

    /**
     * Ruft den Wert der userIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getUserIdList() {
        return userIdList;
    }

    /**
     * Legt den Wert der userIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.userIdList = value;
    }

}
