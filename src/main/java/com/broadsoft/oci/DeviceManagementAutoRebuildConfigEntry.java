//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Contains one automatic rebuild configuration list entry.
 *       
 * 
 * <p>Java-Klasse für DeviceManagementAutoRebuildConfigEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="DeviceManagementAutoRebuildConfigEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="ociRequestPrefix" type="{}DeviceManagementAutoRebuildConfigOciRequestPrefix"/>
 *         <element name="autoRebuildEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeviceManagementAutoRebuildConfigEntry", propOrder = {
    "ociRequestPrefix",
    "autoRebuildEnabled"
})
public class DeviceManagementAutoRebuildConfigEntry {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String ociRequestPrefix;
    protected Boolean autoRebuildEnabled;

    /**
     * Ruft den Wert der ociRequestPrefix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOciRequestPrefix() {
        return ociRequestPrefix;
    }

    /**
     * Legt den Wert der ociRequestPrefix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOciRequestPrefix(String value) {
        this.ociRequestPrefix = value;
    }

    /**
     * Ruft den Wert der autoRebuildEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutoRebuildEnabled() {
        return autoRebuildEnabled;
    }

    /**
     * Legt den Wert der autoRebuildEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoRebuildEnabled(Boolean value) {
        this.autoRebuildEnabled = value;
    }

}
