//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemSessionAdmissionControlGetRequest21sp1.
 *         The response contains the session admission control settings for the system.
 *         
 *         Replaced by: SystemSessionAdmissionControlGetResponse22.
 *       
 * 
 * <p>Java-Klasse für SystemSessionAdmissionControlGetResponse21sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSessionAdmissionControlGetResponse21sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="countLongConnectionsToMediaServer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sacHandlingForMoH" type="{}SessionAdmissionControlForMusicOnHoldType"/>
 *         <element name="blockVMDepositDueToSACLimits" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sacCodecSelectionPolicy" type="{}SessionAdmissionControlCodecSelectionPolicyType"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSessionAdmissionControlGetResponse21sp1", propOrder = {
    "countLongConnectionsToMediaServer",
    "sacHandlingForMoH",
    "blockVMDepositDueToSACLimits",
    "sacCodecSelectionPolicy"
})
public class SystemSessionAdmissionControlGetResponse21Sp1
    extends OCIDataResponse
{

    protected boolean countLongConnectionsToMediaServer;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SessionAdmissionControlForMusicOnHoldType sacHandlingForMoH;
    protected boolean blockVMDepositDueToSACLimits;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SessionAdmissionControlCodecSelectionPolicyType sacCodecSelectionPolicy;

    /**
     * Ruft den Wert der countLongConnectionsToMediaServer-Eigenschaft ab.
     * 
     */
    public boolean isCountLongConnectionsToMediaServer() {
        return countLongConnectionsToMediaServer;
    }

    /**
     * Legt den Wert der countLongConnectionsToMediaServer-Eigenschaft fest.
     * 
     */
    public void setCountLongConnectionsToMediaServer(boolean value) {
        this.countLongConnectionsToMediaServer = value;
    }

    /**
     * Ruft den Wert der sacHandlingForMoH-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SessionAdmissionControlForMusicOnHoldType }
     *     
     */
    public SessionAdmissionControlForMusicOnHoldType getSacHandlingForMoH() {
        return sacHandlingForMoH;
    }

    /**
     * Legt den Wert der sacHandlingForMoH-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionAdmissionControlForMusicOnHoldType }
     *     
     */
    public void setSacHandlingForMoH(SessionAdmissionControlForMusicOnHoldType value) {
        this.sacHandlingForMoH = value;
    }

    /**
     * Ruft den Wert der blockVMDepositDueToSACLimits-Eigenschaft ab.
     * 
     */
    public boolean isBlockVMDepositDueToSACLimits() {
        return blockVMDepositDueToSACLimits;
    }

    /**
     * Legt den Wert der blockVMDepositDueToSACLimits-Eigenschaft fest.
     * 
     */
    public void setBlockVMDepositDueToSACLimits(boolean value) {
        this.blockVMDepositDueToSACLimits = value;
    }

    /**
     * Ruft den Wert der sacCodecSelectionPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SessionAdmissionControlCodecSelectionPolicyType }
     *     
     */
    public SessionAdmissionControlCodecSelectionPolicyType getSacCodecSelectionPolicy() {
        return sacCodecSelectionPolicy;
    }

    /**
     * Legt den Wert der sacCodecSelectionPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionAdmissionControlCodecSelectionPolicyType }
     *     
     */
    public void setSacCodecSelectionPolicy(SessionAdmissionControlCodecSelectionPolicyType value) {
        this.sacCodecSelectionPolicy = value;
    }

}
