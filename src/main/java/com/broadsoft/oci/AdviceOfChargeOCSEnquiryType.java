//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AdviceOfChargeOCSEnquiryType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AdviceOfChargeOCSEnquiryType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Service Price"/>
 *     <enumeration value="Advice Of Charge"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AdviceOfChargeOCSEnquiryType")
@XmlEnum
public enum AdviceOfChargeOCSEnquiryType {

    @XmlEnumValue("Service Price")
    SERVICE_PRICE("Service Price"),
    @XmlEnumValue("Advice Of Charge")
    ADVICE_OF_CHARGE("Advice Of Charge");
    private final String value;

    AdviceOfChargeOCSEnquiryType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AdviceOfChargeOCSEnquiryType fromValue(String v) {
        for (AdviceOfChargeOCSEnquiryType c: AdviceOfChargeOCSEnquiryType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
