//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the user's intercept user service settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         Replaced by: UserInterceptUserModifyResponse21sp1 in AS data mode 
 *       
 * 
 * <p>Java-Klasse für UserInterceptUserModifyRequest16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserInterceptUserModifyRequest16">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="announcementSelection" type="{}AnnouncementSelection" minOccurs="0"/>
 *         <element name="audioFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *         <element name="videoFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *         <element name="playNewPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="newPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="transferOnZeroToPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="transferPhoneNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="rerouteOutboundCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="outboundReroutePhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="allowOutboundLocalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="inboundCallMode" type="{}InterceptInboundCall" minOccurs="0"/>
 *         <element name="alternateBlockingAnnouncement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="routeToVoiceMail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserInterceptUserModifyRequest16", propOrder = {
    "userId",
    "isActive",
    "announcementSelection",
    "audioFile",
    "videoFile",
    "playNewPhoneNumber",
    "newPhoneNumber",
    "transferOnZeroToPhoneNumber",
    "transferPhoneNumber",
    "rerouteOutboundCalls",
    "outboundReroutePhoneNumber",
    "allowOutboundLocalCalls",
    "inboundCallMode",
    "alternateBlockingAnnouncement",
    "routeToVoiceMail"
})
public class UserInterceptUserModifyRequest16
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean isActive;
    @XmlSchemaType(name = "token")
    protected AnnouncementSelection announcementSelection;
    protected LabeledMediaFileResource audioFile;
    protected LabeledMediaFileResource videoFile;
    protected Boolean playNewPhoneNumber;
    @XmlElementRef(name = "newPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> newPhoneNumber;
    protected Boolean transferOnZeroToPhoneNumber;
    @XmlElementRef(name = "transferPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transferPhoneNumber;
    protected Boolean rerouteOutboundCalls;
    @XmlElementRef(name = "outboundReroutePhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> outboundReroutePhoneNumber;
    protected Boolean allowOutboundLocalCalls;
    @XmlSchemaType(name = "token")
    protected InterceptInboundCall inboundCallMode;
    protected Boolean alternateBlockingAnnouncement;
    protected Boolean routeToVoiceMail;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der announcementSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementSelection }
     *     
     */
    public AnnouncementSelection getAnnouncementSelection() {
        return announcementSelection;
    }

    /**
     * Legt den Wert der announcementSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementSelection }
     *     
     */
    public void setAnnouncementSelection(AnnouncementSelection value) {
        this.announcementSelection = value;
    }

    /**
     * Ruft den Wert der audioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public LabeledMediaFileResource getAudioFile() {
        return audioFile;
    }

    /**
     * Legt den Wert der audioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public void setAudioFile(LabeledMediaFileResource value) {
        this.audioFile = value;
    }

    /**
     * Ruft den Wert der videoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public LabeledMediaFileResource getVideoFile() {
        return videoFile;
    }

    /**
     * Legt den Wert der videoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public void setVideoFile(LabeledMediaFileResource value) {
        this.videoFile = value;
    }

    /**
     * Ruft den Wert der playNewPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayNewPhoneNumber() {
        return playNewPhoneNumber;
    }

    /**
     * Legt den Wert der playNewPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayNewPhoneNumber(Boolean value) {
        this.playNewPhoneNumber = value;
    }

    /**
     * Ruft den Wert der newPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNewPhoneNumber() {
        return newPhoneNumber;
    }

    /**
     * Legt den Wert der newPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNewPhoneNumber(JAXBElement<String> value) {
        this.newPhoneNumber = value;
    }

    /**
     * Ruft den Wert der transferOnZeroToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTransferOnZeroToPhoneNumber() {
        return transferOnZeroToPhoneNumber;
    }

    /**
     * Legt den Wert der transferOnZeroToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTransferOnZeroToPhoneNumber(Boolean value) {
        this.transferOnZeroToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der transferPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransferPhoneNumber() {
        return transferPhoneNumber;
    }

    /**
     * Legt den Wert der transferPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransferPhoneNumber(JAXBElement<String> value) {
        this.transferPhoneNumber = value;
    }

    /**
     * Ruft den Wert der rerouteOutboundCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRerouteOutboundCalls() {
        return rerouteOutboundCalls;
    }

    /**
     * Legt den Wert der rerouteOutboundCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRerouteOutboundCalls(Boolean value) {
        this.rerouteOutboundCalls = value;
    }

    /**
     * Ruft den Wert der outboundReroutePhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOutboundReroutePhoneNumber() {
        return outboundReroutePhoneNumber;
    }

    /**
     * Legt den Wert der outboundReroutePhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOutboundReroutePhoneNumber(JAXBElement<String> value) {
        this.outboundReroutePhoneNumber = value;
    }

    /**
     * Ruft den Wert der allowOutboundLocalCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowOutboundLocalCalls() {
        return allowOutboundLocalCalls;
    }

    /**
     * Legt den Wert der allowOutboundLocalCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowOutboundLocalCalls(Boolean value) {
        this.allowOutboundLocalCalls = value;
    }

    /**
     * Ruft den Wert der inboundCallMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InterceptInboundCall }
     *     
     */
    public InterceptInboundCall getInboundCallMode() {
        return inboundCallMode;
    }

    /**
     * Legt den Wert der inboundCallMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InterceptInboundCall }
     *     
     */
    public void setInboundCallMode(InterceptInboundCall value) {
        this.inboundCallMode = value;
    }

    /**
     * Ruft den Wert der alternateBlockingAnnouncement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlternateBlockingAnnouncement() {
        return alternateBlockingAnnouncement;
    }

    /**
     * Legt den Wert der alternateBlockingAnnouncement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlternateBlockingAnnouncement(Boolean value) {
        this.alternateBlockingAnnouncement = value;
    }

    /**
     * Ruft den Wert der routeToVoiceMail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRouteToVoiceMail() {
        return routeToVoiceMail;
    }

    /**
     * Legt den Wert der routeToVoiceMail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRouteToVoiceMail(Boolean value) {
        this.routeToVoiceMail = value;
    }

}
