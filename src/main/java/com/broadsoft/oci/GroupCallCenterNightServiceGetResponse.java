//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupCallCenterNightServiceGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterNightServiceGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterNightServiceGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="action" type="{}CallCenterScheduledServiceAction"/>
 *         <element name="businessHours" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="forceNightService" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowManualOverrideViaFAC" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="transferPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="playAnnouncementBeforeAction" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="audioMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="audioFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="audioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="audioMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="videoMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="videoFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="videoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="videoMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="manualAnnouncementMode" type="{}CallCenterManualNightServiceAnnouncementMode"/>
 *         <element name="manualAudioMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="manualAudioFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="manualAudioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="manualAudioMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="manualVideoMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="manualVideoFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="manualVideoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="manualVideoMediaType" type="{}MediaFileType" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterNightServiceGetResponse", propOrder = {
    "action",
    "businessHours",
    "forceNightService",
    "allowManualOverrideViaFAC",
    "transferPhoneNumber",
    "playAnnouncementBeforeAction",
    "audioMessageSelection",
    "audioFileUrl",
    "audioFileDescription",
    "audioMediaType",
    "videoMessageSelection",
    "videoFileUrl",
    "videoFileDescription",
    "videoMediaType",
    "manualAnnouncementMode",
    "manualAudioMessageSelection",
    "manualAudioFileUrl",
    "manualAudioFileDescription",
    "manualAudioMediaType",
    "manualVideoMessageSelection",
    "manualVideoFileUrl",
    "manualVideoFileDescription",
    "manualVideoMediaType"
})
public class GroupCallCenterNightServiceGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterScheduledServiceAction action;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String businessHours;
    protected boolean forceNightService;
    protected boolean allowManualOverrideViaFAC;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String transferPhoneNumber;
    protected boolean playAnnouncementBeforeAction;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection audioMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioFileUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioMediaType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection videoMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String videoFileUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String videoFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String videoMediaType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterManualNightServiceAnnouncementMode manualAnnouncementMode;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection manualAudioMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String manualAudioFileUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String manualAudioFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String manualAudioMediaType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection manualVideoMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String manualVideoFileUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String manualVideoFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String manualVideoMediaType;

    /**
     * Ruft den Wert der action-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterScheduledServiceAction }
     *     
     */
    public CallCenterScheduledServiceAction getAction() {
        return action;
    }

    /**
     * Legt den Wert der action-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterScheduledServiceAction }
     *     
     */
    public void setAction(CallCenterScheduledServiceAction value) {
        this.action = value;
    }

    /**
     * Ruft den Wert der businessHours-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessHours() {
        return businessHours;
    }

    /**
     * Legt den Wert der businessHours-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessHours(String value) {
        this.businessHours = value;
    }

    /**
     * Ruft den Wert der forceNightService-Eigenschaft ab.
     * 
     */
    public boolean isForceNightService() {
        return forceNightService;
    }

    /**
     * Legt den Wert der forceNightService-Eigenschaft fest.
     * 
     */
    public void setForceNightService(boolean value) {
        this.forceNightService = value;
    }

    /**
     * Ruft den Wert der allowManualOverrideViaFAC-Eigenschaft ab.
     * 
     */
    public boolean isAllowManualOverrideViaFAC() {
        return allowManualOverrideViaFAC;
    }

    /**
     * Legt den Wert der allowManualOverrideViaFAC-Eigenschaft fest.
     * 
     */
    public void setAllowManualOverrideViaFAC(boolean value) {
        this.allowManualOverrideViaFAC = value;
    }

    /**
     * Ruft den Wert der transferPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferPhoneNumber() {
        return transferPhoneNumber;
    }

    /**
     * Legt den Wert der transferPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferPhoneNumber(String value) {
        this.transferPhoneNumber = value;
    }

    /**
     * Ruft den Wert der playAnnouncementBeforeAction-Eigenschaft ab.
     * 
     */
    public boolean isPlayAnnouncementBeforeAction() {
        return playAnnouncementBeforeAction;
    }

    /**
     * Legt den Wert der playAnnouncementBeforeAction-Eigenschaft fest.
     * 
     */
    public void setPlayAnnouncementBeforeAction(boolean value) {
        this.playAnnouncementBeforeAction = value;
    }

    /**
     * Ruft den Wert der audioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getAudioMessageSelection() {
        return audioMessageSelection;
    }

    /**
     * Legt den Wert der audioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.audioMessageSelection = value;
    }

    /**
     * Ruft den Wert der audioFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioFileUrl() {
        return audioFileUrl;
    }

    /**
     * Legt den Wert der audioFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioFileUrl(String value) {
        this.audioFileUrl = value;
    }

    /**
     * Ruft den Wert der audioFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioFileDescription() {
        return audioFileDescription;
    }

    /**
     * Legt den Wert der audioFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioFileDescription(String value) {
        this.audioFileDescription = value;
    }

    /**
     * Ruft den Wert der audioMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioMediaType() {
        return audioMediaType;
    }

    /**
     * Legt den Wert der audioMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioMediaType(String value) {
        this.audioMediaType = value;
    }

    /**
     * Ruft den Wert der videoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getVideoMessageSelection() {
        return videoMessageSelection;
    }

    /**
     * Legt den Wert der videoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.videoMessageSelection = value;
    }

    /**
     * Ruft den Wert der videoFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVideoFileUrl() {
        return videoFileUrl;
    }

    /**
     * Legt den Wert der videoFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVideoFileUrl(String value) {
        this.videoFileUrl = value;
    }

    /**
     * Ruft den Wert der videoFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVideoFileDescription() {
        return videoFileDescription;
    }

    /**
     * Legt den Wert der videoFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVideoFileDescription(String value) {
        this.videoFileDescription = value;
    }

    /**
     * Ruft den Wert der videoMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVideoMediaType() {
        return videoMediaType;
    }

    /**
     * Legt den Wert der videoMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVideoMediaType(String value) {
        this.videoMediaType = value;
    }

    /**
     * Ruft den Wert der manualAnnouncementMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterManualNightServiceAnnouncementMode }
     *     
     */
    public CallCenterManualNightServiceAnnouncementMode getManualAnnouncementMode() {
        return manualAnnouncementMode;
    }

    /**
     * Legt den Wert der manualAnnouncementMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterManualNightServiceAnnouncementMode }
     *     
     */
    public void setManualAnnouncementMode(CallCenterManualNightServiceAnnouncementMode value) {
        this.manualAnnouncementMode = value;
    }

    /**
     * Ruft den Wert der manualAudioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getManualAudioMessageSelection() {
        return manualAudioMessageSelection;
    }

    /**
     * Legt den Wert der manualAudioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setManualAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.manualAudioMessageSelection = value;
    }

    /**
     * Ruft den Wert der manualAudioFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManualAudioFileUrl() {
        return manualAudioFileUrl;
    }

    /**
     * Legt den Wert der manualAudioFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManualAudioFileUrl(String value) {
        this.manualAudioFileUrl = value;
    }

    /**
     * Ruft den Wert der manualAudioFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManualAudioFileDescription() {
        return manualAudioFileDescription;
    }

    /**
     * Legt den Wert der manualAudioFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManualAudioFileDescription(String value) {
        this.manualAudioFileDescription = value;
    }

    /**
     * Ruft den Wert der manualAudioMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManualAudioMediaType() {
        return manualAudioMediaType;
    }

    /**
     * Legt den Wert der manualAudioMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManualAudioMediaType(String value) {
        this.manualAudioMediaType = value;
    }

    /**
     * Ruft den Wert der manualVideoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getManualVideoMessageSelection() {
        return manualVideoMessageSelection;
    }

    /**
     * Legt den Wert der manualVideoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setManualVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.manualVideoMessageSelection = value;
    }

    /**
     * Ruft den Wert der manualVideoFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManualVideoFileUrl() {
        return manualVideoFileUrl;
    }

    /**
     * Legt den Wert der manualVideoFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManualVideoFileUrl(String value) {
        this.manualVideoFileUrl = value;
    }

    /**
     * Ruft den Wert der manualVideoFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManualVideoFileDescription() {
        return manualVideoFileDescription;
    }

    /**
     * Legt den Wert der manualVideoFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManualVideoFileDescription(String value) {
        this.manualVideoFileDescription = value;
    }

    /**
     * Ruft den Wert der manualVideoMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManualVideoMediaType() {
        return manualVideoMediaType;
    }

    /**
     * Legt den Wert der manualVideoMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManualVideoMediaType(String value) {
        this.manualVideoMediaType = value;
    }

}
