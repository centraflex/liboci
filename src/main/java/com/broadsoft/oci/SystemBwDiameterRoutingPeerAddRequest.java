//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a Diameter routing peer.  The realm must refer to a Diameter routing realm whose action is relay.  The destinationPeerIdentity must refer to an existing Diameter peer whose mode is active.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemBwDiameterRoutingPeerAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBwDiameterRoutingPeerAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="instance" type="{}BwDiameterPeerInstance"/>
 *         <element name="realm" type="{}DomainName"/>
 *         <element name="applicationId" type="{}BwDiameterApplicationId"/>
 *         <element name="identity" type="{}DomainName"/>
 *         <element name="priority" type="{}BwDiameterPriority"/>
 *         <element name="weight" type="{}BwDiameterWeight"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBwDiameterRoutingPeerAddRequest", propOrder = {
    "instance",
    "realm",
    "applicationId",
    "identity",
    "priority",
    "weight"
})
public class SystemBwDiameterRoutingPeerAddRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BwDiameterPeerInstance instance;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String realm;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BwDiameterApplicationId applicationId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String identity;
    protected int priority;
    protected int weight;

    /**
     * Ruft den Wert der instance-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BwDiameterPeerInstance }
     *     
     */
    public BwDiameterPeerInstance getInstance() {
        return instance;
    }

    /**
     * Legt den Wert der instance-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BwDiameterPeerInstance }
     *     
     */
    public void setInstance(BwDiameterPeerInstance value) {
        this.instance = value;
    }

    /**
     * Ruft den Wert der realm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRealm() {
        return realm;
    }

    /**
     * Legt den Wert der realm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRealm(String value) {
        this.realm = value;
    }

    /**
     * Ruft den Wert der applicationId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BwDiameterApplicationId }
     *     
     */
    public BwDiameterApplicationId getApplicationId() {
        return applicationId;
    }

    /**
     * Legt den Wert der applicationId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BwDiameterApplicationId }
     *     
     */
    public void setApplicationId(BwDiameterApplicationId value) {
        this.applicationId = value;
    }

    /**
     * Ruft den Wert der identity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentity() {
        return identity;
    }

    /**
     * Legt den Wert der identity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentity(String value) {
        this.identity = value;
    }

    /**
     * Ruft den Wert der priority-Eigenschaft ab.
     * 
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Legt den Wert der priority-Eigenschaft fest.
     * 
     */
    public void setPriority(int value) {
        this.priority = value;
    }

    /**
     * Ruft den Wert der weight-Eigenschaft ab.
     * 
     */
    public int getWeight() {
        return weight;
    }

    /**
     * Legt den Wert der weight-Eigenschaft fest.
     * 
     */
    public void setWeight(int value) {
        this.weight = value;
    }

}
