//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the ServiceProviderCommunicationBarringProfileGetRequest17sp3.
 *         The response contains the Communication Barring Profile information.
 *         The incoming, originating, redirecting and call me now rules are returned in ascending priority order.
 *         The following elements are only used in AS data mode:
 *            callMeNowDefaultAction,  value "Blocked" is returned in XS data mode
 *         The following elements are only used in AS data mode and not returned in XS data mode:
 *            callMeNowDefaultCallTimeout
 *            callMeNowRule  
 *            
 *         Replaced By : ServiceProviderCommunicationBarringProfileGetResponse19sp1V2 in AS data mode  
 *       
 * 
 * <p>Java-Klasse für ServiceProviderCommunicationBarringProfileGetResponse17sp3 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderCommunicationBarringProfileGetResponse17sp3">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="description" type="{}CommunicationBarringProfileDescription" minOccurs="0"/>
 *         <element name="originatingDefaultAction" type="{}CommunicationBarringOriginatingAction"/>
 *         <element name="originatingDefaultTreatmentId" type="{}TreatmentId" minOccurs="0"/>
 *         <element name="originatingDefaultTransferNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="originatingDefaultCallTimeout" type="{}CommunicationBarringTimeoutSeconds" minOccurs="0"/>
 *         <element name="originatingRule" type="{}ServiceProviderCommunicationBarringHierarchicalOriginatingRule" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="redirectingDefaultAction" type="{}CommunicationBarringRedirectingAction"/>
 *         <element name="redirectingDefaultCallTimeout" type="{}CommunicationBarringTimeoutSeconds" minOccurs="0"/>
 *         <element name="redirectingRule" type="{}ServiceProviderCommunicationBarringHierarchicalRedirectingRule" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="callMeNowDefaultAction" type="{}CommunicationBarringCallMeNowAction"/>
 *         <element name="callMeNowDefaultCallTimeout" type="{}CommunicationBarringTimeoutSeconds" minOccurs="0"/>
 *         <element name="callMeNowRule" type="{}ServiceProviderCommunicationBarringHierarchicalCallMeNowRule" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="incomingDefaultAction" type="{}CommunicationBarringIncomingAction"/>
 *         <element name="incomingDefaultCallTimeout" type="{}CommunicationBarringTimeoutSeconds" minOccurs="0"/>
 *         <element name="incomingRule" type="{}CommunicationBarringIncomingRule" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="isDefault" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderCommunicationBarringProfileGetResponse17sp3", propOrder = {
    "description",
    "originatingDefaultAction",
    "originatingDefaultTreatmentId",
    "originatingDefaultTransferNumber",
    "originatingDefaultCallTimeout",
    "originatingRule",
    "redirectingDefaultAction",
    "redirectingDefaultCallTimeout",
    "redirectingRule",
    "callMeNowDefaultAction",
    "callMeNowDefaultCallTimeout",
    "callMeNowRule",
    "incomingDefaultAction",
    "incomingDefaultCallTimeout",
    "incomingRule",
    "isDefault"
})
public class ServiceProviderCommunicationBarringProfileGetResponse17Sp3
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CommunicationBarringOriginatingAction originatingDefaultAction;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String originatingDefaultTreatmentId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String originatingDefaultTransferNumber;
    protected Integer originatingDefaultCallTimeout;
    protected List<ServiceProviderCommunicationBarringHierarchicalOriginatingRule> originatingRule;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CommunicationBarringRedirectingAction redirectingDefaultAction;
    protected Integer redirectingDefaultCallTimeout;
    protected List<ServiceProviderCommunicationBarringHierarchicalRedirectingRule> redirectingRule;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CommunicationBarringCallMeNowAction callMeNowDefaultAction;
    protected Integer callMeNowDefaultCallTimeout;
    protected List<ServiceProviderCommunicationBarringHierarchicalCallMeNowRule> callMeNowRule;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CommunicationBarringIncomingAction incomingDefaultAction;
    protected Integer incomingDefaultCallTimeout;
    protected List<CommunicationBarringIncomingRule> incomingRule;
    protected boolean isDefault;

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der originatingDefaultAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringOriginatingAction }
     *     
     */
    public CommunicationBarringOriginatingAction getOriginatingDefaultAction() {
        return originatingDefaultAction;
    }

    /**
     * Legt den Wert der originatingDefaultAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringOriginatingAction }
     *     
     */
    public void setOriginatingDefaultAction(CommunicationBarringOriginatingAction value) {
        this.originatingDefaultAction = value;
    }

    /**
     * Ruft den Wert der originatingDefaultTreatmentId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatingDefaultTreatmentId() {
        return originatingDefaultTreatmentId;
    }

    /**
     * Legt den Wert der originatingDefaultTreatmentId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatingDefaultTreatmentId(String value) {
        this.originatingDefaultTreatmentId = value;
    }

    /**
     * Ruft den Wert der originatingDefaultTransferNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatingDefaultTransferNumber() {
        return originatingDefaultTransferNumber;
    }

    /**
     * Legt den Wert der originatingDefaultTransferNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatingDefaultTransferNumber(String value) {
        this.originatingDefaultTransferNumber = value;
    }

    /**
     * Ruft den Wert der originatingDefaultCallTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOriginatingDefaultCallTimeout() {
        return originatingDefaultCallTimeout;
    }

    /**
     * Legt den Wert der originatingDefaultCallTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOriginatingDefaultCallTimeout(Integer value) {
        this.originatingDefaultCallTimeout = value;
    }

    /**
     * Gets the value of the originatingRule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the originatingRule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOriginatingRule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceProviderCommunicationBarringHierarchicalOriginatingRule }
     * 
     * 
     * @return
     *     The value of the originatingRule property.
     */
    public List<ServiceProviderCommunicationBarringHierarchicalOriginatingRule> getOriginatingRule() {
        if (originatingRule == null) {
            originatingRule = new ArrayList<>();
        }
        return this.originatingRule;
    }

    /**
     * Ruft den Wert der redirectingDefaultAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringRedirectingAction }
     *     
     */
    public CommunicationBarringRedirectingAction getRedirectingDefaultAction() {
        return redirectingDefaultAction;
    }

    /**
     * Legt den Wert der redirectingDefaultAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringRedirectingAction }
     *     
     */
    public void setRedirectingDefaultAction(CommunicationBarringRedirectingAction value) {
        this.redirectingDefaultAction = value;
    }

    /**
     * Ruft den Wert der redirectingDefaultCallTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRedirectingDefaultCallTimeout() {
        return redirectingDefaultCallTimeout;
    }

    /**
     * Legt den Wert der redirectingDefaultCallTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRedirectingDefaultCallTimeout(Integer value) {
        this.redirectingDefaultCallTimeout = value;
    }

    /**
     * Gets the value of the redirectingRule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the redirectingRule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRedirectingRule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceProviderCommunicationBarringHierarchicalRedirectingRule }
     * 
     * 
     * @return
     *     The value of the redirectingRule property.
     */
    public List<ServiceProviderCommunicationBarringHierarchicalRedirectingRule> getRedirectingRule() {
        if (redirectingRule == null) {
            redirectingRule = new ArrayList<>();
        }
        return this.redirectingRule;
    }

    /**
     * Ruft den Wert der callMeNowDefaultAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringCallMeNowAction }
     *     
     */
    public CommunicationBarringCallMeNowAction getCallMeNowDefaultAction() {
        return callMeNowDefaultAction;
    }

    /**
     * Legt den Wert der callMeNowDefaultAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringCallMeNowAction }
     *     
     */
    public void setCallMeNowDefaultAction(CommunicationBarringCallMeNowAction value) {
        this.callMeNowDefaultAction = value;
    }

    /**
     * Ruft den Wert der callMeNowDefaultCallTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCallMeNowDefaultCallTimeout() {
        return callMeNowDefaultCallTimeout;
    }

    /**
     * Legt den Wert der callMeNowDefaultCallTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCallMeNowDefaultCallTimeout(Integer value) {
        this.callMeNowDefaultCallTimeout = value;
    }

    /**
     * Gets the value of the callMeNowRule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the callMeNowRule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCallMeNowRule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceProviderCommunicationBarringHierarchicalCallMeNowRule }
     * 
     * 
     * @return
     *     The value of the callMeNowRule property.
     */
    public List<ServiceProviderCommunicationBarringHierarchicalCallMeNowRule> getCallMeNowRule() {
        if (callMeNowRule == null) {
            callMeNowRule = new ArrayList<>();
        }
        return this.callMeNowRule;
    }

    /**
     * Ruft den Wert der incomingDefaultAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringIncomingAction }
     *     
     */
    public CommunicationBarringIncomingAction getIncomingDefaultAction() {
        return incomingDefaultAction;
    }

    /**
     * Legt den Wert der incomingDefaultAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringIncomingAction }
     *     
     */
    public void setIncomingDefaultAction(CommunicationBarringIncomingAction value) {
        this.incomingDefaultAction = value;
    }

    /**
     * Ruft den Wert der incomingDefaultCallTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIncomingDefaultCallTimeout() {
        return incomingDefaultCallTimeout;
    }

    /**
     * Legt den Wert der incomingDefaultCallTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIncomingDefaultCallTimeout(Integer value) {
        this.incomingDefaultCallTimeout = value;
    }

    /**
     * Gets the value of the incomingRule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the incomingRule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncomingRule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationBarringIncomingRule }
     * 
     * 
     * @return
     *     The value of the incomingRule property.
     */
    public List<CommunicationBarringIncomingRule> getIncomingRule() {
        if (incomingRule == null) {
            incomingRule = new ArrayList<>();
        }
        return this.incomingRule;
    }

    /**
     * Ruft den Wert der isDefault-Eigenschaft ab.
     * 
     */
    public boolean isIsDefault() {
        return isDefault;
    }

    /**
     * Legt den Wert der isDefault-Eigenschaft fest.
     * 
     */
    public void setIsDefault(boolean value) {
        this.isDefault = value;
    }

}
