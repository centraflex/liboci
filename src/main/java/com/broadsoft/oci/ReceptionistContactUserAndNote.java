//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The Receptionist User (or VON User) and Receptionist Notes.
 *       
 * 
 * <p>Java-Klasse für ReceptionistContactUserAndNote complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ReceptionistContactUserAndNote">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <choice>
 *           <element name="contactUserId" type="{}UserId"/>
 *           <element name="vonUser" type="{}VirtualOnNetUserKey"/>
 *         </choice>
 *         <element name="note" type="{}ReceptionistNote"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceptionistContactUserAndNote", propOrder = {
    "contactUserId",
    "vonUser",
    "note"
})
public class ReceptionistContactUserAndNote {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String contactUserId;
    protected VirtualOnNetUserKey vonUser;
    @XmlElement(required = true, nillable = true)
    protected String note;

    /**
     * Ruft den Wert der contactUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactUserId() {
        return contactUserId;
    }

    /**
     * Legt den Wert der contactUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactUserId(String value) {
        this.contactUserId = value;
    }

    /**
     * Ruft den Wert der vonUser-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VirtualOnNetUserKey }
     *     
     */
    public VirtualOnNetUserKey getVonUser() {
        return vonUser;
    }

    /**
     * Legt den Wert der vonUser-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VirtualOnNetUserKey }
     *     
     */
    public void setVonUser(VirtualOnNetUserKey value) {
        this.vonUser = value;
    }

    /**
     * Ruft den Wert der note-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Legt den Wert der note-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

}
