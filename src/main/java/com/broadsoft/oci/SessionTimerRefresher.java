//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SessionTimerRefresher.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SessionTimerRefresher">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Local"/>
 *     <enumeration value="Remote"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SessionTimerRefresher")
@XmlEnum
public enum SessionTimerRefresher {

    @XmlEnumValue("Local")
    LOCAL("Local"),
    @XmlEnumValue("Remote")
    REMOTE("Remote");
    private final String value;

    SessionTimerRefresher(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SessionTimerRefresher fromValue(String v) {
        for (SessionTimerRefresher c: SessionTimerRefresher.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
