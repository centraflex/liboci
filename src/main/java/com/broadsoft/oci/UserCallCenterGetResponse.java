//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserCallCenterGetRequest.
 *         Contains the user's ACD state
 *         Indicates whether the agent is current available (logged in) to each call center in the list.
 *         Contains a table with column headings: "Service User Id", "Phone Number", "Extension", "Available", "Logoff Allowed".
 *       
 * 
 * <p>Java-Klasse für UserCallCenterGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallCenterGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="agentACDState" type="{}AgentACDState" minOccurs="0"/>
 *         <element name="userTable" type="{C}OCITable" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallCenterGetResponse", propOrder = {
    "agentACDState",
    "userTable"
})
public class UserCallCenterGetResponse
    extends OCIDataResponse
{

    @XmlSchemaType(name = "token")
    protected AgentACDState agentACDState;
    protected OCITable userTable;

    /**
     * Ruft den Wert der agentACDState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AgentACDState }
     *     
     */
    public AgentACDState getAgentACDState() {
        return agentACDState;
    }

    /**
     * Legt den Wert der agentACDState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentACDState }
     *     
     */
    public void setAgentACDState(AgentACDState value) {
        this.agentACDState = value;
    }

    /**
     * Ruft den Wert der userTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserTable() {
        return userTable;
    }

    /**
     * Legt den Wert der userTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserTable(OCITable value) {
        this.userTable = value;
    }

}
