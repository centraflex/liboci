//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the user level data associated with Malicious Call Trace.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserMaliciousCallTraceModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserMaliciousCallTraceModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="traceTypeSelection" type="{}MaliciousCallTraceCallTypeSelection" minOccurs="0"/>
 *         <element name="traceForTimePeriod" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="traceTimePeriod" type="{}MaliciousCallTraceTimePeriod" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserMaliciousCallTraceModifyRequest", propOrder = {
    "userId",
    "isActive",
    "traceTypeSelection",
    "traceForTimePeriod",
    "traceTimePeriod"
})
public class UserMaliciousCallTraceModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean isActive;
    @XmlSchemaType(name = "token")
    protected MaliciousCallTraceCallTypeSelection traceTypeSelection;
    protected Boolean traceForTimePeriod;
    @XmlElementRef(name = "traceTimePeriod", type = JAXBElement.class, required = false)
    protected JAXBElement<MaliciousCallTraceTimePeriod> traceTimePeriod;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der traceTypeSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MaliciousCallTraceCallTypeSelection }
     *     
     */
    public MaliciousCallTraceCallTypeSelection getTraceTypeSelection() {
        return traceTypeSelection;
    }

    /**
     * Legt den Wert der traceTypeSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MaliciousCallTraceCallTypeSelection }
     *     
     */
    public void setTraceTypeSelection(MaliciousCallTraceCallTypeSelection value) {
        this.traceTypeSelection = value;
    }

    /**
     * Ruft den Wert der traceForTimePeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTraceForTimePeriod() {
        return traceForTimePeriod;
    }

    /**
     * Legt den Wert der traceForTimePeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTraceForTimePeriod(Boolean value) {
        this.traceForTimePeriod = value;
    }

    /**
     * Ruft den Wert der traceTimePeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link MaliciousCallTraceTimePeriod }{@code >}
     *     
     */
    public JAXBElement<MaliciousCallTraceTimePeriod> getTraceTimePeriod() {
        return traceTimePeriod;
    }

    /**
     * Legt den Wert der traceTimePeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link MaliciousCallTraceTimePeriod }{@code >}
     *     
     */
    public void setTraceTimePeriod(JAXBElement<MaliciousCallTraceTimePeriod> value) {
        this.traceTimePeriod = value;
    }

}
