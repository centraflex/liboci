//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Trunk group endpoint.
 *       
 * 
 * <p>Java-Klasse für TrunkAddressingModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="TrunkAddressingModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="trunkGroupDeviceEndpoint" type="{}TrunkGroupDeviceEndpointModify" minOccurs="0"/>
 *         <element name="enterpriseTrunkName" type="{}EnterpriseTrunkName" minOccurs="0"/>
 *         <element name="alternateTrunkIdentity" type="{}AlternateTrunkIdentity" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrunkAddressingModify", propOrder = {
    "trunkGroupDeviceEndpoint",
    "enterpriseTrunkName",
    "alternateTrunkIdentity"
})
public class TrunkAddressingModify {

    @XmlElementRef(name = "trunkGroupDeviceEndpoint", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupDeviceEndpointModify> trunkGroupDeviceEndpoint;
    @XmlElementRef(name = "enterpriseTrunkName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> enterpriseTrunkName;
    @XmlElementRef(name = "alternateTrunkIdentity", type = JAXBElement.class, required = false)
    protected JAXBElement<String> alternateTrunkIdentity;

    /**
     * Ruft den Wert der trunkGroupDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupDeviceEndpointModify }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupDeviceEndpointModify> getTrunkGroupDeviceEndpoint() {
        return trunkGroupDeviceEndpoint;
    }

    /**
     * Legt den Wert der trunkGroupDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupDeviceEndpointModify }{@code >}
     *     
     */
    public void setTrunkGroupDeviceEndpoint(JAXBElement<TrunkGroupDeviceEndpointModify> value) {
        this.trunkGroupDeviceEndpoint = value;
    }

    /**
     * Ruft den Wert der enterpriseTrunkName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEnterpriseTrunkName() {
        return enterpriseTrunkName;
    }

    /**
     * Legt den Wert der enterpriseTrunkName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEnterpriseTrunkName(JAXBElement<String> value) {
        this.enterpriseTrunkName = value;
    }

    /**
     * Ruft den Wert der alternateTrunkIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAlternateTrunkIdentity() {
        return alternateTrunkIdentity;
    }

    /**
     * Legt den Wert der alternateTrunkIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAlternateTrunkIdentity(JAXBElement<String> value) {
        this.alternateTrunkIdentity = value;
    }

}
