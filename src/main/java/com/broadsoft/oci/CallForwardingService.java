//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallForwardingService.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallForwardingService">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Call Forwarding Always"/>
 *     <enumeration value="Call Forwarding Always Secondary"/>
 *     <enumeration value="Call Forwarding Busy"/>
 *     <enumeration value="Call Forwarding No Answer"/>
 *     <enumeration value="Call Forwarding Not Reachable"/>
 *     <enumeration value="Call Forwarding Selective"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallForwardingService")
@XmlEnum
public enum CallForwardingService {

    @XmlEnumValue("Call Forwarding Always")
    CALL_FORWARDING_ALWAYS("Call Forwarding Always"),
    @XmlEnumValue("Call Forwarding Always Secondary")
    CALL_FORWARDING_ALWAYS_SECONDARY("Call Forwarding Always Secondary"),
    @XmlEnumValue("Call Forwarding Busy")
    CALL_FORWARDING_BUSY("Call Forwarding Busy"),
    @XmlEnumValue("Call Forwarding No Answer")
    CALL_FORWARDING_NO_ANSWER("Call Forwarding No Answer"),
    @XmlEnumValue("Call Forwarding Not Reachable")
    CALL_FORWARDING_NOT_REACHABLE("Call Forwarding Not Reachable"),
    @XmlEnumValue("Call Forwarding Selective")
    CALL_FORWARDING_SELECTIVE("Call Forwarding Selective");
    private final String value;

    CallForwardingService(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallForwardingService fromValue(String v) {
        for (CallForwardingService c: CallForwardingService.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
