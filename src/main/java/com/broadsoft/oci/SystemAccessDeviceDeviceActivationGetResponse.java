//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemAccessDeviceDeviceActivationGetRequest.
 *         The response contains the activation code (if available), the expiry time (if available) and the activation state.
 *         The expiryTime is represented as a timestamp, i.e. the number of milliseconds
 *         since January 1, 1970, 00:00:00 GMT.
 *       
 * 
 * <p>Java-Klasse für SystemAccessDeviceDeviceActivationGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAccessDeviceDeviceActivationGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="activationCode" type="{}DeviceActivationCode" minOccurs="0"/>
 *         <element name="expiryTime" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         <element name="activationState" type="{}DeviceActivationState"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAccessDeviceDeviceActivationGetResponse", propOrder = {
    "activationCode",
    "expiryTime",
    "activationState"
})
public class SystemAccessDeviceDeviceActivationGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String activationCode;
    protected Long expiryTime;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceActivationState activationState;

    /**
     * Ruft den Wert der activationCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationCode() {
        return activationCode;
    }

    /**
     * Legt den Wert der activationCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationCode(String value) {
        this.activationCode = value;
    }

    /**
     * Ruft den Wert der expiryTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getExpiryTime() {
        return expiryTime;
    }

    /**
     * Legt den Wert der expiryTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setExpiryTime(Long value) {
        this.expiryTime = value;
    }

    /**
     * Ruft den Wert der activationState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceActivationState }
     *     
     */
    public DeviceActivationState getActivationState() {
        return activationState;
    }

    /**
     * Legt den Wert der activationState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceActivationState }
     *     
     */
    public void setActivationState(DeviceActivationState value) {
        this.activationState = value;
    }

}
