//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the user's simultaneous ring personal service setting.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         Replaced By: UserSimultaneousRingPersonalModifyRequest14sp4
 *       
 * 
 * <p>Java-Klasse für UserSimultaneousRingPersonalModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserSimultaneousRingPersonalModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="incomingCalls" type="{}SimultaneousRingSelection" minOccurs="0"/>
 *         <element name="simRingPhoneNumberList" type="{}ReplacementOutgoingDNorSIPURIList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserSimultaneousRingPersonalModifyRequest", propOrder = {
    "userId",
    "isActive",
    "incomingCalls",
    "simRingPhoneNumberList"
})
public class UserSimultaneousRingPersonalModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean isActive;
    @XmlSchemaType(name = "token")
    protected SimultaneousRingSelection incomingCalls;
    @XmlElementRef(name = "simRingPhoneNumberList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementOutgoingDNorSIPURIList> simRingPhoneNumberList;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der incomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SimultaneousRingSelection }
     *     
     */
    public SimultaneousRingSelection getIncomingCalls() {
        return incomingCalls;
    }

    /**
     * Legt den Wert der incomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SimultaneousRingSelection }
     *     
     */
    public void setIncomingCalls(SimultaneousRingSelection value) {
        this.incomingCalls = value;
    }

    /**
     * Ruft den Wert der simRingPhoneNumberList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementOutgoingDNorSIPURIList }{@code >}
     *     
     */
    public JAXBElement<ReplacementOutgoingDNorSIPURIList> getSimRingPhoneNumberList() {
        return simRingPhoneNumberList;
    }

    /**
     * Legt den Wert der simRingPhoneNumberList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementOutgoingDNorSIPURIList }{@code >}
     *     
     */
    public void setSimRingPhoneNumberList(JAXBElement<ReplacementOutgoingDNorSIPURIList> value) {
        this.simRingPhoneNumberList = value;
    }

}
