//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the existing conference delegates list.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserMeetMeConferencingModifyConferenceDelegateListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserMeetMeConferencingModifyConferenceDelegateListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="conferenceKey" type="{}MeetMeConferencingConferenceKey"/>
 *         <element name="conferenceDelegateUserList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserMeetMeConferencingModifyConferenceDelegateListRequest", propOrder = {
    "userId",
    "conferenceKey",
    "conferenceDelegateUserList"
})
public class UserMeetMeConferencingModifyConferenceDelegateListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    protected MeetMeConferencingConferenceKey conferenceKey;
    @XmlElementRef(name = "conferenceDelegateUserList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> conferenceDelegateUserList;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der conferenceKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferenceKey }
     *     
     */
    public MeetMeConferencingConferenceKey getConferenceKey() {
        return conferenceKey;
    }

    /**
     * Legt den Wert der conferenceKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferenceKey }
     *     
     */
    public void setConferenceKey(MeetMeConferencingConferenceKey value) {
        this.conferenceKey = value;
    }

    /**
     * Ruft den Wert der conferenceDelegateUserList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getConferenceDelegateUserList() {
        return conferenceDelegateUserList;
    }

    /**
     * Legt den Wert der conferenceDelegateUserList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setConferenceDelegateUserList(JAXBElement<ReplacementUserIdList> value) {
        this.conferenceDelegateUserList = value;
    }

}
