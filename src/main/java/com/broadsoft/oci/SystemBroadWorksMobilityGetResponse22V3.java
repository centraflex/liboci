//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The response to a SystemBroadWorksMobilityGetRequest22V3. 
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobilityGetResponse22V3 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobilityGetResponse22V3">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableLocationServices" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableMSRNLookup" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableMobileStateChecking" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="denyCallOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="denyCallTerminations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="imrnTimeoutMillisecnds" type="{}IMRNTimeoutMilliseconds"/>
 *         <element name="enableInternalCLIDDelivery" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeRedirectForMobilityTermination" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableInternalCLIDDeliveryAccessLocations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableEnhancedUnreachableStateChecking" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableNetworkCallBarringStatusCheck" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="networkTranslationIndex" type="{}NetworkTranslationIndex" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobilityGetResponse22V3", propOrder = {
    "enableLocationServices",
    "enableMSRNLookup",
    "enableMobileStateChecking",
    "denyCallOriginations",
    "denyCallTerminations",
    "imrnTimeoutMillisecnds",
    "enableInternalCLIDDelivery",
    "includeRedirectForMobilityTermination",
    "enableInternalCLIDDeliveryAccessLocations",
    "enableEnhancedUnreachableStateChecking",
    "enableNetworkCallBarringStatusCheck",
    "networkTranslationIndex"
})
public class SystemBroadWorksMobilityGetResponse22V3
    extends OCIDataResponse
{

    protected boolean enableLocationServices;
    protected boolean enableMSRNLookup;
    protected boolean enableMobileStateChecking;
    protected boolean denyCallOriginations;
    protected boolean denyCallTerminations;
    protected int imrnTimeoutMillisecnds;
    protected boolean enableInternalCLIDDelivery;
    protected boolean includeRedirectForMobilityTermination;
    protected boolean enableInternalCLIDDeliveryAccessLocations;
    protected boolean enableEnhancedUnreachableStateChecking;
    protected boolean enableNetworkCallBarringStatusCheck;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkTranslationIndex;

    /**
     * Ruft den Wert der enableLocationServices-Eigenschaft ab.
     * 
     */
    public boolean isEnableLocationServices() {
        return enableLocationServices;
    }

    /**
     * Legt den Wert der enableLocationServices-Eigenschaft fest.
     * 
     */
    public void setEnableLocationServices(boolean value) {
        this.enableLocationServices = value;
    }

    /**
     * Ruft den Wert der enableMSRNLookup-Eigenschaft ab.
     * 
     */
    public boolean isEnableMSRNLookup() {
        return enableMSRNLookup;
    }

    /**
     * Legt den Wert der enableMSRNLookup-Eigenschaft fest.
     * 
     */
    public void setEnableMSRNLookup(boolean value) {
        this.enableMSRNLookup = value;
    }

    /**
     * Ruft den Wert der enableMobileStateChecking-Eigenschaft ab.
     * 
     */
    public boolean isEnableMobileStateChecking() {
        return enableMobileStateChecking;
    }

    /**
     * Legt den Wert der enableMobileStateChecking-Eigenschaft fest.
     * 
     */
    public void setEnableMobileStateChecking(boolean value) {
        this.enableMobileStateChecking = value;
    }

    /**
     * Ruft den Wert der denyCallOriginations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallOriginations() {
        return denyCallOriginations;
    }

    /**
     * Legt den Wert der denyCallOriginations-Eigenschaft fest.
     * 
     */
    public void setDenyCallOriginations(boolean value) {
        this.denyCallOriginations = value;
    }

    /**
     * Ruft den Wert der denyCallTerminations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallTerminations() {
        return denyCallTerminations;
    }

    /**
     * Legt den Wert der denyCallTerminations-Eigenschaft fest.
     * 
     */
    public void setDenyCallTerminations(boolean value) {
        this.denyCallTerminations = value;
    }

    /**
     * Ruft den Wert der imrnTimeoutMillisecnds-Eigenschaft ab.
     * 
     */
    public int getImrnTimeoutMillisecnds() {
        return imrnTimeoutMillisecnds;
    }

    /**
     * Legt den Wert der imrnTimeoutMillisecnds-Eigenschaft fest.
     * 
     */
    public void setImrnTimeoutMillisecnds(int value) {
        this.imrnTimeoutMillisecnds = value;
    }

    /**
     * Ruft den Wert der enableInternalCLIDDelivery-Eigenschaft ab.
     * 
     */
    public boolean isEnableInternalCLIDDelivery() {
        return enableInternalCLIDDelivery;
    }

    /**
     * Legt den Wert der enableInternalCLIDDelivery-Eigenschaft fest.
     * 
     */
    public void setEnableInternalCLIDDelivery(boolean value) {
        this.enableInternalCLIDDelivery = value;
    }

    /**
     * Ruft den Wert der includeRedirectForMobilityTermination-Eigenschaft ab.
     * 
     */
    public boolean isIncludeRedirectForMobilityTermination() {
        return includeRedirectForMobilityTermination;
    }

    /**
     * Legt den Wert der includeRedirectForMobilityTermination-Eigenschaft fest.
     * 
     */
    public void setIncludeRedirectForMobilityTermination(boolean value) {
        this.includeRedirectForMobilityTermination = value;
    }

    /**
     * Ruft den Wert der enableInternalCLIDDeliveryAccessLocations-Eigenschaft ab.
     * 
     */
    public boolean isEnableInternalCLIDDeliveryAccessLocations() {
        return enableInternalCLIDDeliveryAccessLocations;
    }

    /**
     * Legt den Wert der enableInternalCLIDDeliveryAccessLocations-Eigenschaft fest.
     * 
     */
    public void setEnableInternalCLIDDeliveryAccessLocations(boolean value) {
        this.enableInternalCLIDDeliveryAccessLocations = value;
    }

    /**
     * Ruft den Wert der enableEnhancedUnreachableStateChecking-Eigenschaft ab.
     * 
     */
    public boolean isEnableEnhancedUnreachableStateChecking() {
        return enableEnhancedUnreachableStateChecking;
    }

    /**
     * Legt den Wert der enableEnhancedUnreachableStateChecking-Eigenschaft fest.
     * 
     */
    public void setEnableEnhancedUnreachableStateChecking(boolean value) {
        this.enableEnhancedUnreachableStateChecking = value;
    }

    /**
     * Ruft den Wert der enableNetworkCallBarringStatusCheck-Eigenschaft ab.
     * 
     */
    public boolean isEnableNetworkCallBarringStatusCheck() {
        return enableNetworkCallBarringStatusCheck;
    }

    /**
     * Legt den Wert der enableNetworkCallBarringStatusCheck-Eigenschaft fest.
     * 
     */
    public void setEnableNetworkCallBarringStatusCheck(boolean value) {
        this.enableNetworkCallBarringStatusCheck = value;
    }

    /**
     * Ruft den Wert der networkTranslationIndex-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkTranslationIndex() {
        return networkTranslationIndex;
    }

    /**
     * Legt den Wert der networkTranslationIndex-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkTranslationIndex(String value) {
        this.networkTranslationIndex = value;
    }

}
