//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Change the push to talk service settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserPushToTalkModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPushToTalkModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="allowAutoAnswer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="outgoingConnectionSelection" type="{}PushToTalkOutgoingConnectionSelection" minOccurs="0"/>
 *         <element name="accessListSelection" type="{}PushToTalkAccessListSelection" minOccurs="0"/>
 *         <element name="selectedUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPushToTalkModifyRequest", propOrder = {
    "userId",
    "allowAutoAnswer",
    "outgoingConnectionSelection",
    "accessListSelection",
    "selectedUserIdList"
})
public class UserPushToTalkModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean allowAutoAnswer;
    @XmlSchemaType(name = "token")
    protected PushToTalkOutgoingConnectionSelection outgoingConnectionSelection;
    @XmlSchemaType(name = "token")
    protected PushToTalkAccessListSelection accessListSelection;
    @XmlElementRef(name = "selectedUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> selectedUserIdList;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der allowAutoAnswer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowAutoAnswer() {
        return allowAutoAnswer;
    }

    /**
     * Legt den Wert der allowAutoAnswer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowAutoAnswer(Boolean value) {
        this.allowAutoAnswer = value;
    }

    /**
     * Ruft den Wert der outgoingConnectionSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PushToTalkOutgoingConnectionSelection }
     *     
     */
    public PushToTalkOutgoingConnectionSelection getOutgoingConnectionSelection() {
        return outgoingConnectionSelection;
    }

    /**
     * Legt den Wert der outgoingConnectionSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PushToTalkOutgoingConnectionSelection }
     *     
     */
    public void setOutgoingConnectionSelection(PushToTalkOutgoingConnectionSelection value) {
        this.outgoingConnectionSelection = value;
    }

    /**
     * Ruft den Wert der accessListSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PushToTalkAccessListSelection }
     *     
     */
    public PushToTalkAccessListSelection getAccessListSelection() {
        return accessListSelection;
    }

    /**
     * Legt den Wert der accessListSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PushToTalkAccessListSelection }
     *     
     */
    public void setAccessListSelection(PushToTalkAccessListSelection value) {
        this.accessListSelection = value;
    }

    /**
     * Ruft den Wert der selectedUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getSelectedUserIdList() {
        return selectedUserIdList;
    }

    /**
     * Legt den Wert der selectedUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setSelectedUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.selectedUserIdList = value;
    }

}
