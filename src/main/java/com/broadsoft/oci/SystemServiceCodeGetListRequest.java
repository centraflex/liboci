//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to get all service codes that have been defined in the system.
 *         It is possible to search by various criteria to restrict the number of rows returned.
 *         Multiple search criteria are logically ANDed together.
 *         The response is either SystemServiceCodeGetListResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemServiceCodeGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemServiceCodeGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaServiceCode" type="{}SearchCriteriaServiceCode" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaServiceCodeDescription" type="{}SearchCriteriaServiceCodeDescription" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemServiceCodeGetListRequest", propOrder = {
    "responseSizeLimit",
    "searchCriteriaServiceCode",
    "searchCriteriaServiceCodeDescription"
})
public class SystemServiceCodeGetListRequest
    extends OCIRequest
{

    protected Integer responseSizeLimit;
    protected List<SearchCriteriaServiceCode> searchCriteriaServiceCode;
    protected List<SearchCriteriaServiceCodeDescription> searchCriteriaServiceCodeDescription;

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaServiceCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaServiceCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaServiceCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaServiceCode }
     * 
     * 
     * @return
     *     The value of the searchCriteriaServiceCode property.
     */
    public List<SearchCriteriaServiceCode> getSearchCriteriaServiceCode() {
        if (searchCriteriaServiceCode == null) {
            searchCriteriaServiceCode = new ArrayList<>();
        }
        return this.searchCriteriaServiceCode;
    }

    /**
     * Gets the value of the searchCriteriaServiceCodeDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaServiceCodeDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaServiceCodeDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaServiceCodeDescription }
     * 
     * 
     * @return
     *     The value of the searchCriteriaServiceCodeDescription property.
     */
    public List<SearchCriteriaServiceCodeDescription> getSearchCriteriaServiceCodeDescription() {
        if (searchCriteriaServiceCodeDescription == null) {
            searchCriteriaServiceCodeDescription = new ArrayList<>();
        }
        return this.searchCriteriaServiceCodeDescription;
    }

}
