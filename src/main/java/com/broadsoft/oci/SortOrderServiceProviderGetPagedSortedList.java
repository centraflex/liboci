//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Used to sort the ServiceProviderGetPagedSortedListRequest request.
 *       
 * 
 * <p>Java-Klasse für SortOrderServiceProviderGetPagedSortedList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SortOrderServiceProviderGetPagedSortedList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <choice>
 *         <element name="sortByServiceProviderId" type="{}SortByServiceProviderId"/>
 *         <element name="sortByServiceProviderName" type="{}SortByServiceProviderName"/>
 *       </choice>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SortOrderServiceProviderGetPagedSortedList", propOrder = {
    "sortByServiceProviderId",
    "sortByServiceProviderName"
})
public class SortOrderServiceProviderGetPagedSortedList {

    protected SortByServiceProviderId sortByServiceProviderId;
    protected SortByServiceProviderName sortByServiceProviderName;

    /**
     * Ruft den Wert der sortByServiceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByServiceProviderId }
     *     
     */
    public SortByServiceProviderId getSortByServiceProviderId() {
        return sortByServiceProviderId;
    }

    /**
     * Legt den Wert der sortByServiceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByServiceProviderId }
     *     
     */
    public void setSortByServiceProviderId(SortByServiceProviderId value) {
        this.sortByServiceProviderId = value;
    }

    /**
     * Ruft den Wert der sortByServiceProviderName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByServiceProviderName }
     *     
     */
    public SortByServiceProviderName getSortByServiceProviderName() {
        return sortByServiceProviderName;
    }

    /**
     * Legt den Wert der sortByServiceProviderName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByServiceProviderName }
     *     
     */
    public void setSortByServiceProviderName(SortByServiceProviderName value) {
        this.sortByServiceProviderName = value;
    }

}
