//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The default Network Class of Service to set during assignation/unassignation.
 *       
 * 
 * <p>Java-Klasse für DefaultNetworkClassOfService complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="DefaultNetworkClassOfService">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <choice>
 *         <element name="useExisting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="networkClassOfServiceName" type="{}NetworkClassOfServiceName"/>
 *       </choice>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DefaultNetworkClassOfService", propOrder = {
    "useExisting",
    "networkClassOfServiceName"
})
public class DefaultNetworkClassOfService {

    protected Boolean useExisting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfServiceName;

    /**
     * Ruft den Wert der useExisting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseExisting() {
        return useExisting;
    }

    /**
     * Legt den Wert der useExisting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseExisting(Boolean value) {
        this.useExisting = value;
    }

    /**
     * Ruft den Wert der networkClassOfServiceName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfServiceName() {
        return networkClassOfServiceName;
    }

    /**
     * Legt den Wert der networkClassOfServiceName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfServiceName(String value) {
        this.networkClassOfServiceName = value;
    }

}
