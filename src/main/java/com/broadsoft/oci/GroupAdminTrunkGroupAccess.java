//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GroupAdminTrunkGroupAccess.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="GroupAdminTrunkGroupAccess">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Full"/>
 *     <enumeration value="Full Resources"/>
 *     <enumeration value="Read-Only Resources"/>
 *     <enumeration value="None"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "GroupAdminTrunkGroupAccess")
@XmlEnum
public enum GroupAdminTrunkGroupAccess {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("Full Resources")
    FULL_RESOURCES("Full Resources"),
    @XmlEnumValue("Read-Only Resources")
    READ_ONLY_RESOURCES("Read-Only Resources"),
    @XmlEnumValue("None")
    NONE("None");
    private final String value;

    GroupAdminTrunkGroupAccess(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupAdminTrunkGroupAccess fromValue(String v) {
        for (GroupAdminTrunkGroupAccess c: GroupAdminTrunkGroupAccess.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
