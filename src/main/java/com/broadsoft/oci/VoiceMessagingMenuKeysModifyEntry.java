//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal voice messaging menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für VoiceMessagingMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="VoiceMessagingMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="playMessages" type="{}DigitAny" minOccurs="0"/>
 *         <element name="changeBusyGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="changeNoAnswerGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="changeExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="deleteAllMessages" type="{}DigitAny" minOccurs="0"/>
 *         <element name="passcode" type="{}DigitAny" minOccurs="0"/>
 *         <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
 *         <element name="messageDeposit" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoiceMessagingMenuKeysModifyEntry", propOrder = {
    "playMessages",
    "changeBusyGreeting",
    "changeNoAnswerGreeting",
    "changeExtendedAwayGreeting",
    "composeMessage",
    "deleteAllMessages",
    "passcode",
    "personalizedName",
    "messageDeposit",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class VoiceMessagingMenuKeysModifyEntry {

    @XmlElementRef(name = "playMessages", type = JAXBElement.class, required = false)
    protected JAXBElement<String> playMessages;
    @XmlElementRef(name = "changeBusyGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changeBusyGreeting;
    @XmlElementRef(name = "changeNoAnswerGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changeNoAnswerGreeting;
    @XmlElementRef(name = "changeExtendedAwayGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changeExtendedAwayGreeting;
    @XmlElementRef(name = "composeMessage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> composeMessage;
    @XmlElementRef(name = "deleteAllMessages", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deleteAllMessages;
    @XmlElementRef(name = "passcode", type = JAXBElement.class, required = false)
    protected JAXBElement<String> passcode;
    @XmlElementRef(name = "personalizedName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> personalizedName;
    @XmlElementRef(name = "messageDeposit", type = JAXBElement.class, required = false)
    protected JAXBElement<String> messageDeposit;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repeatMenu;

    /**
     * Ruft den Wert der playMessages-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlayMessages() {
        return playMessages;
    }

    /**
     * Legt den Wert der playMessages-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlayMessages(JAXBElement<String> value) {
        this.playMessages = value;
    }

    /**
     * Ruft den Wert der changeBusyGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangeBusyGreeting() {
        return changeBusyGreeting;
    }

    /**
     * Legt den Wert der changeBusyGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangeBusyGreeting(JAXBElement<String> value) {
        this.changeBusyGreeting = value;
    }

    /**
     * Ruft den Wert der changeNoAnswerGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangeNoAnswerGreeting() {
        return changeNoAnswerGreeting;
    }

    /**
     * Legt den Wert der changeNoAnswerGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangeNoAnswerGreeting(JAXBElement<String> value) {
        this.changeNoAnswerGreeting = value;
    }

    /**
     * Ruft den Wert der changeExtendedAwayGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangeExtendedAwayGreeting() {
        return changeExtendedAwayGreeting;
    }

    /**
     * Legt den Wert der changeExtendedAwayGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangeExtendedAwayGreeting(JAXBElement<String> value) {
        this.changeExtendedAwayGreeting = value;
    }

    /**
     * Ruft den Wert der composeMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getComposeMessage() {
        return composeMessage;
    }

    /**
     * Legt den Wert der composeMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setComposeMessage(JAXBElement<String> value) {
        this.composeMessage = value;
    }

    /**
     * Ruft den Wert der deleteAllMessages-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeleteAllMessages() {
        return deleteAllMessages;
    }

    /**
     * Legt den Wert der deleteAllMessages-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeleteAllMessages(JAXBElement<String> value) {
        this.deleteAllMessages = value;
    }

    /**
     * Ruft den Wert der passcode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPasscode() {
        return passcode;
    }

    /**
     * Legt den Wert der passcode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPasscode(JAXBElement<String> value) {
        this.passcode = value;
    }

    /**
     * Ruft den Wert der personalizedName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPersonalizedName() {
        return personalizedName;
    }

    /**
     * Legt den Wert der personalizedName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPersonalizedName(JAXBElement<String> value) {
        this.personalizedName = value;
    }

    /**
     * Ruft den Wert der messageDeposit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMessageDeposit() {
        return messageDeposit;
    }

    /**
     * Legt den Wert der messageDeposit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMessageDeposit(JAXBElement<String> value) {
        this.messageDeposit = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepeatMenu(JAXBElement<String> value) {
        this.repeatMenu = value;
    }

}
