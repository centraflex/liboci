//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Communication Barring Call Me Now Rule
 *       
 * 
 * <p>Java-Klasse für CommunicationBarringCallMeNowRule complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CommunicationBarringCallMeNowRule">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="criteria" type="{}CommunicationBarringCriteriaName"/>
 *         <element name="action" type="{}CommunicationBarringCallMeNowAction"/>
 *         <element name="callTimeoutSeconds" type="{}CommunicationBarringTimeoutSeconds" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommunicationBarringCallMeNowRule", propOrder = {
    "criteria",
    "action",
    "callTimeoutSeconds"
})
public class CommunicationBarringCallMeNowRule {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String criteria;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CommunicationBarringCallMeNowAction action;
    @XmlElementRef(name = "callTimeoutSeconds", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> callTimeoutSeconds;

    /**
     * Ruft den Wert der criteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCriteria() {
        return criteria;
    }

    /**
     * Legt den Wert der criteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCriteria(String value) {
        this.criteria = value;
    }

    /**
     * Ruft den Wert der action-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringCallMeNowAction }
     *     
     */
    public CommunicationBarringCallMeNowAction getAction() {
        return action;
    }

    /**
     * Legt den Wert der action-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringCallMeNowAction }
     *     
     */
    public void setAction(CommunicationBarringCallMeNowAction value) {
        this.action = value;
    }

    /**
     * Ruft den Wert der callTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCallTimeoutSeconds() {
        return callTimeoutSeconds;
    }

    /**
     * Legt den Wert der callTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCallTimeoutSeconds(JAXBElement<Integer> value) {
        this.callTimeoutSeconds = value;
    }

}
