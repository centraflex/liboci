//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         	This is the configuration parameters for Simultaneous Ring Personal  service
 *         	
 *         	Contains a criteria table with column heading: "Is Active", "Criteria Name", "Time Schedule", "Holiday Schedule", "Calls From" and "Blacklisted".
 *         	
 *         	The "Calls From" column is a string containing call numbers
 *         	
 * 
 * <p>Java-Klasse für ProfileAndServiceSimultaneousRingPersonalInfo complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ProfileAndServiceSimultaneousRingPersonalInfo">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="doNotRingIfOnCall" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="simultaneousRingNumber" type="{}SimultaneousRingNumber" maxOccurs="10" minOccurs="0"/>
 *         <element name="criteriaTable" type="{C}OCITable"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfileAndServiceSimultaneousRingPersonalInfo", propOrder = {
    "isActive",
    "doNotRingIfOnCall",
    "simultaneousRingNumber",
    "criteriaTable"
})
public class ProfileAndServiceSimultaneousRingPersonalInfo {

    protected boolean isActive;
    protected boolean doNotRingIfOnCall;
    protected List<SimultaneousRingNumber> simultaneousRingNumber;
    @XmlElement(required = true)
    protected OCITable criteriaTable;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der doNotRingIfOnCall-Eigenschaft ab.
     * 
     */
    public boolean isDoNotRingIfOnCall() {
        return doNotRingIfOnCall;
    }

    /**
     * Legt den Wert der doNotRingIfOnCall-Eigenschaft fest.
     * 
     */
    public void setDoNotRingIfOnCall(boolean value) {
        this.doNotRingIfOnCall = value;
    }

    /**
     * Gets the value of the simultaneousRingNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the simultaneousRingNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSimultaneousRingNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimultaneousRingNumber }
     * 
     * 
     * @return
     *     The value of the simultaneousRingNumber property.
     */
    public List<SimultaneousRingNumber> getSimultaneousRingNumber() {
        if (simultaneousRingNumber == null) {
            simultaneousRingNumber = new ArrayList<>();
        }
        return this.simultaneousRingNumber;
    }

    /**
     * Ruft den Wert der criteriaTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getCriteriaTable() {
        return criteriaTable;
    }

    /**
     * Legt den Wert der criteriaTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setCriteriaTable(OCITable value) {
        this.criteriaTable = value;
    }

}
