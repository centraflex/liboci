//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The voice portal main menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für VoicePortalMainMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="VoicePortalMainMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="voiceMessaging" type="{}DigitAny" minOccurs="0"/>
 *         <element name="commPilotExpressProfile" type="{}DigitAny" minOccurs="0"/>
 *         <element name="greetings" type="{}DigitAny" minOccurs="0"/>
 *         <element name="callForwardingOptions" type="{}DigitAny" minOccurs="0"/>
 *         <element name="voicePortalCalling" type="{}DigitAny" minOccurs="0"/>
 *         <element name="hoteling" type="{}DigitAny" minOccurs="0"/>
 *         <element name="passcode" type="{}DigitAny" minOccurs="0"/>
 *         <element name="exitVoicePortal" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="externalRouting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="announcement" type="{}DigitAny" minOccurs="0"/>
 *         <element name="personalAssistant" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoicePortalMainMenuKeysModifyEntry", propOrder = {
    "voiceMessaging",
    "commPilotExpressProfile",
    "greetings",
    "callForwardingOptions",
    "voicePortalCalling",
    "hoteling",
    "passcode",
    "exitVoicePortal",
    "repeatMenu",
    "externalRouting",
    "announcement",
    "personalAssistant"
})
public class VoicePortalMainMenuKeysModifyEntry {

    @XmlElementRef(name = "voiceMessaging", type = JAXBElement.class, required = false)
    protected JAXBElement<String> voiceMessaging;
    @XmlElementRef(name = "commPilotExpressProfile", type = JAXBElement.class, required = false)
    protected JAXBElement<String> commPilotExpressProfile;
    @XmlElementRef(name = "greetings", type = JAXBElement.class, required = false)
    protected JAXBElement<String> greetings;
    @XmlElementRef(name = "callForwardingOptions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callForwardingOptions;
    @XmlElementRef(name = "voicePortalCalling", type = JAXBElement.class, required = false)
    protected JAXBElement<String> voicePortalCalling;
    @XmlElementRef(name = "hoteling", type = JAXBElement.class, required = false)
    protected JAXBElement<String> hoteling;
    @XmlElementRef(name = "passcode", type = JAXBElement.class, required = false)
    protected JAXBElement<String> passcode;
    @XmlElementRef(name = "exitVoicePortal", type = JAXBElement.class, required = false)
    protected JAXBElement<String> exitVoicePortal;
    @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repeatMenu;
    @XmlElementRef(name = "externalRouting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externalRouting;
    @XmlElementRef(name = "announcement", type = JAXBElement.class, required = false)
    protected JAXBElement<String> announcement;
    @XmlElementRef(name = "personalAssistant", type = JAXBElement.class, required = false)
    protected JAXBElement<String> personalAssistant;

    /**
     * Ruft den Wert der voiceMessaging-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVoiceMessaging() {
        return voiceMessaging;
    }

    /**
     * Legt den Wert der voiceMessaging-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVoiceMessaging(JAXBElement<String> value) {
        this.voiceMessaging = value;
    }

    /**
     * Ruft den Wert der commPilotExpressProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCommPilotExpressProfile() {
        return commPilotExpressProfile;
    }

    /**
     * Legt den Wert der commPilotExpressProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCommPilotExpressProfile(JAXBElement<String> value) {
        this.commPilotExpressProfile = value;
    }

    /**
     * Ruft den Wert der greetings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGreetings() {
        return greetings;
    }

    /**
     * Legt den Wert der greetings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGreetings(JAXBElement<String> value) {
        this.greetings = value;
    }

    /**
     * Ruft den Wert der callForwardingOptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallForwardingOptions() {
        return callForwardingOptions;
    }

    /**
     * Legt den Wert der callForwardingOptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallForwardingOptions(JAXBElement<String> value) {
        this.callForwardingOptions = value;
    }

    /**
     * Ruft den Wert der voicePortalCalling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVoicePortalCalling() {
        return voicePortalCalling;
    }

    /**
     * Legt den Wert der voicePortalCalling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVoicePortalCalling(JAXBElement<String> value) {
        this.voicePortalCalling = value;
    }

    /**
     * Ruft den Wert der hoteling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHoteling() {
        return hoteling;
    }

    /**
     * Legt den Wert der hoteling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHoteling(JAXBElement<String> value) {
        this.hoteling = value;
    }

    /**
     * Ruft den Wert der passcode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPasscode() {
        return passcode;
    }

    /**
     * Legt den Wert der passcode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPasscode(JAXBElement<String> value) {
        this.passcode = value;
    }

    /**
     * Ruft den Wert der exitVoicePortal-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExitVoicePortal() {
        return exitVoicePortal;
    }

    /**
     * Legt den Wert der exitVoicePortal-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExitVoicePortal(JAXBElement<String> value) {
        this.exitVoicePortal = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepeatMenu(JAXBElement<String> value) {
        this.repeatMenu = value;
    }

    /**
     * Ruft den Wert der externalRouting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternalRouting() {
        return externalRouting;
    }

    /**
     * Legt den Wert der externalRouting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternalRouting(JAXBElement<String> value) {
        this.externalRouting = value;
    }

    /**
     * Ruft den Wert der announcement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAnnouncement() {
        return announcement;
    }

    /**
     * Legt den Wert der announcement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAnnouncement(JAXBElement<String> value) {
        this.announcement = value;
    }

    /**
     * Ruft den Wert der personalAssistant-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPersonalAssistant() {
        return personalAssistant;
    }

    /**
     * Legt den Wert der personalAssistant-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPersonalAssistant(JAXBElement<String> value) {
        this.personalAssistant = value;
    }

}
