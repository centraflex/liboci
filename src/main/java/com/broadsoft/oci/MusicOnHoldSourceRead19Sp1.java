//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Contains the music on hold source configuration.
 *       
 * 
 * <p>Java-Klasse für MusicOnHoldSourceRead19sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="MusicOnHoldSourceRead19sp1">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="audioFilePreferredCodec" type="{}AudioFileCodecExtended"/>
 *         <element name="messageSourceSelection" type="{}MusicOnHoldMessageSelection"/>
 *         <element name="customSource" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="audioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *                   <element name="audioMediaType" type="{}MediaFileType" minOccurs="0"/>
 *                   <element name="videoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *                   <element name="videoMediaType" type="{}MediaFileType" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="externalSource" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointWithPortNumberRead"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MusicOnHoldSourceRead19sp1", propOrder = {
    "audioFilePreferredCodec",
    "messageSourceSelection",
    "customSource",
    "externalSource"
})
public class MusicOnHoldSourceRead19Sp1 {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AudioFileCodecExtended audioFilePreferredCodec;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MusicOnHoldMessageSelection messageSourceSelection;
    protected MusicOnHoldSourceRead19Sp1 .CustomSource customSource;
    protected MusicOnHoldSourceRead19Sp1 .ExternalSource externalSource;

    /**
     * Ruft den Wert der audioFilePreferredCodec-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AudioFileCodecExtended }
     *     
     */
    public AudioFileCodecExtended getAudioFilePreferredCodec() {
        return audioFilePreferredCodec;
    }

    /**
     * Legt den Wert der audioFilePreferredCodec-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AudioFileCodecExtended }
     *     
     */
    public void setAudioFilePreferredCodec(AudioFileCodecExtended value) {
        this.audioFilePreferredCodec = value;
    }

    /**
     * Ruft den Wert der messageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public MusicOnHoldMessageSelection getMessageSourceSelection() {
        return messageSourceSelection;
    }

    /**
     * Legt den Wert der messageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public void setMessageSourceSelection(MusicOnHoldMessageSelection value) {
        this.messageSourceSelection = value;
    }

    /**
     * Ruft den Wert der customSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceRead19Sp1 .CustomSource }
     *     
     */
    public MusicOnHoldSourceRead19Sp1 .CustomSource getCustomSource() {
        return customSource;
    }

    /**
     * Legt den Wert der customSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceRead19Sp1 .CustomSource }
     *     
     */
    public void setCustomSource(MusicOnHoldSourceRead19Sp1 .CustomSource value) {
        this.customSource = value;
    }

    /**
     * Ruft den Wert der externalSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceRead19Sp1 .ExternalSource }
     *     
     */
    public MusicOnHoldSourceRead19Sp1 .ExternalSource getExternalSource() {
        return externalSource;
    }

    /**
     * Legt den Wert der externalSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceRead19Sp1 .ExternalSource }
     *     
     */
    public void setExternalSource(MusicOnHoldSourceRead19Sp1 .ExternalSource value) {
        this.externalSource = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="audioFileDescription" type="{}FileDescription" minOccurs="0"/>
     *         <element name="audioMediaType" type="{}MediaFileType" minOccurs="0"/>
     *         <element name="videoFileDescription" type="{}FileDescription" minOccurs="0"/>
     *         <element name="videoMediaType" type="{}MediaFileType" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "audioFileDescription",
        "audioMediaType",
        "videoFileDescription",
        "videoMediaType"
    })
    public static class CustomSource {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String audioFileDescription;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String audioMediaType;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String videoFileDescription;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String videoMediaType;

        /**
         * Ruft den Wert der audioFileDescription-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAudioFileDescription() {
            return audioFileDescription;
        }

        /**
         * Legt den Wert der audioFileDescription-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAudioFileDescription(String value) {
            this.audioFileDescription = value;
        }

        /**
         * Ruft den Wert der audioMediaType-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAudioMediaType() {
            return audioMediaType;
        }

        /**
         * Legt den Wert der audioMediaType-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAudioMediaType(String value) {
            this.audioMediaType = value;
        }

        /**
         * Ruft den Wert der videoFileDescription-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVideoFileDescription() {
            return videoFileDescription;
        }

        /**
         * Legt den Wert der videoFileDescription-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVideoFileDescription(String value) {
            this.videoFileDescription = value;
        }

        /**
         * Ruft den Wert der videoMediaType-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVideoMediaType() {
            return videoMediaType;
        }

        /**
         * Legt den Wert der videoMediaType-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVideoMediaType(String value) {
            this.videoMediaType = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointWithPortNumberRead"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accessDeviceEndpoint"
    })
    public static class ExternalSource {

        @XmlElement(required = true)
        protected AccessDeviceEndpointWithPortNumberRead accessDeviceEndpoint;

        /**
         * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AccessDeviceEndpointWithPortNumberRead }
         *     
         */
        public AccessDeviceEndpointWithPortNumberRead getAccessDeviceEndpoint() {
            return accessDeviceEndpoint;
        }

        /**
         * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AccessDeviceEndpointWithPortNumberRead }
         *     
         */
        public void setAccessDeviceEndpoint(AccessDeviceEndpointWithPortNumberRead value) {
            this.accessDeviceEndpoint = value;
        }

    }

}
