//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemClientIdentityGetListRequest.
 *         Returns a table with column headings:
 *          "Client Identity".
 *       
 * 
 * <p>Java-Klasse für SystemClientIdentityGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemClientIdentityGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="clientIdentityTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemClientIdentityGetListResponse", propOrder = {
    "clientIdentityTable"
})
public class SystemClientIdentityGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable clientIdentityTable;

    /**
     * Ruft den Wert der clientIdentityTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getClientIdentityTable() {
        return clientIdentityTable;
    }

    /**
     * Legt den Wert der clientIdentityTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setClientIdentityTable(OCITable value) {
        this.clientIdentityTable = value;
    }

}
