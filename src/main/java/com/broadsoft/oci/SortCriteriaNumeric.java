//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The sort criteria specifies whether sort is
 *         ascending or descending.
 *         Sort order defaults to ascending.
 *       
 * 
 * <p>Java-Klasse für SortCriteriaNumeric complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SortCriteriaNumeric">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="isAscending" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SortCriteriaNumeric", propOrder = {
    "isAscending"
})
@XmlSeeAlso({
    SortByAnnouncementFileSize.class
})
public abstract class SortCriteriaNumeric {

    @XmlElement(defaultValue = "true")
    protected boolean isAscending;

    /**
     * Ruft den Wert der isAscending-Eigenschaft ab.
     * 
     */
    public boolean isIsAscending() {
        return isAscending;
    }

    /**
     * Legt den Wert der isAscending-Eigenschaft fest.
     * 
     */
    public void setIsAscending(boolean value) {
        this.isAscending = value;
    }

}
