//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserVoiceMessagingUserGetGreetingRequest20.
 *         Contains the greeting configuration for a user's voice messaging.
 *         The following elements are only used in AS data mode:
 *           disableMessageDeposit
 *           disableMessageDepositAction
 *           greetingOnlyForwardDestination
 *           extendedAwayEnabled
 *           extendedAwayDisableMessageDeposit
 *           extendedAwayAudioFile
 *           extendedAwayAudioMediaType
 *           extendedAwayVideoFile
 *           extendedAwayVideoMediaTyp
 *       
 * 
 * <p>Java-Klasse für UserVoiceMessagingUserGetGreetingResponse20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserVoiceMessagingUserGetGreetingResponse20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="busyAnnouncementSelection" type="{}AnnouncementSelection"/>
 *         <element name="busyPersonalAudioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="busyPersonalVideoFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="noAnswerAnnouncementSelection" type="{}VoiceMessagingNoAnswerGreetingSelection"/>
 *         <element name="noAnswerPersonalAudioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="noAnswerPersonalVideoFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="noAnswerAlternateGreeting01" type="{}VoiceMessagingAlternateNoAnswerGreetingRead20" minOccurs="0"/>
 *         <element name="noAnswerAlternateGreeting02" type="{}VoiceMessagingAlternateNoAnswerGreetingRead20" minOccurs="0"/>
 *         <element name="noAnswerAlternateGreeting03" type="{}VoiceMessagingAlternateNoAnswerGreetingRead20" minOccurs="0"/>
 *         <element name="extendedAwayEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="extendedAwayDisableMessageDeposit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="extendedAwayAudioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="extendedAwayVideoFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="noAnswerNumberOfRings" type="{}VoiceMessagingNumberOfRings"/>
 *         <element name="disableMessageDeposit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disableMessageDepositAction" type="{}VoiceMessagingDisableMessageDepositSelection"/>
 *         <element name="greetingOnlyForwardDestination" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserVoiceMessagingUserGetGreetingResponse20", propOrder = {
    "busyAnnouncementSelection",
    "busyPersonalAudioFile",
    "busyPersonalVideoFile",
    "noAnswerAnnouncementSelection",
    "noAnswerPersonalAudioFile",
    "noAnswerPersonalVideoFile",
    "noAnswerAlternateGreeting01",
    "noAnswerAlternateGreeting02",
    "noAnswerAlternateGreeting03",
    "extendedAwayEnabled",
    "extendedAwayDisableMessageDeposit",
    "extendedAwayAudioFile",
    "extendedAwayVideoFile",
    "noAnswerNumberOfRings",
    "disableMessageDeposit",
    "disableMessageDepositAction",
    "greetingOnlyForwardDestination"
})
public class UserVoiceMessagingUserGetGreetingResponse20
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AnnouncementSelection busyAnnouncementSelection;
    protected AnnouncementFileLevelKey busyPersonalAudioFile;
    protected AnnouncementFileLevelKey busyPersonalVideoFile;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected VoiceMessagingNoAnswerGreetingSelection noAnswerAnnouncementSelection;
    protected AnnouncementFileLevelKey noAnswerPersonalAudioFile;
    protected AnnouncementFileLevelKey noAnswerPersonalVideoFile;
    protected VoiceMessagingAlternateNoAnswerGreetingRead20 noAnswerAlternateGreeting01;
    protected VoiceMessagingAlternateNoAnswerGreetingRead20 noAnswerAlternateGreeting02;
    protected VoiceMessagingAlternateNoAnswerGreetingRead20 noAnswerAlternateGreeting03;
    protected boolean extendedAwayEnabled;
    protected boolean extendedAwayDisableMessageDeposit;
    protected AnnouncementFileLevelKey extendedAwayAudioFile;
    protected AnnouncementFileLevelKey extendedAwayVideoFile;
    protected int noAnswerNumberOfRings;
    protected boolean disableMessageDeposit;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected VoiceMessagingDisableMessageDepositSelection disableMessageDepositAction;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String greetingOnlyForwardDestination;

    /**
     * Ruft den Wert der busyAnnouncementSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementSelection }
     *     
     */
    public AnnouncementSelection getBusyAnnouncementSelection() {
        return busyAnnouncementSelection;
    }

    /**
     * Legt den Wert der busyAnnouncementSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementSelection }
     *     
     */
    public void setBusyAnnouncementSelection(AnnouncementSelection value) {
        this.busyAnnouncementSelection = value;
    }

    /**
     * Ruft den Wert der busyPersonalAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getBusyPersonalAudioFile() {
        return busyPersonalAudioFile;
    }

    /**
     * Legt den Wert der busyPersonalAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setBusyPersonalAudioFile(AnnouncementFileLevelKey value) {
        this.busyPersonalAudioFile = value;
    }

    /**
     * Ruft den Wert der busyPersonalVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getBusyPersonalVideoFile() {
        return busyPersonalVideoFile;
    }

    /**
     * Legt den Wert der busyPersonalVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setBusyPersonalVideoFile(AnnouncementFileLevelKey value) {
        this.busyPersonalVideoFile = value;
    }

    /**
     * Ruft den Wert der noAnswerAnnouncementSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingNoAnswerGreetingSelection }
     *     
     */
    public VoiceMessagingNoAnswerGreetingSelection getNoAnswerAnnouncementSelection() {
        return noAnswerAnnouncementSelection;
    }

    /**
     * Legt den Wert der noAnswerAnnouncementSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingNoAnswerGreetingSelection }
     *     
     */
    public void setNoAnswerAnnouncementSelection(VoiceMessagingNoAnswerGreetingSelection value) {
        this.noAnswerAnnouncementSelection = value;
    }

    /**
     * Ruft den Wert der noAnswerPersonalAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getNoAnswerPersonalAudioFile() {
        return noAnswerPersonalAudioFile;
    }

    /**
     * Legt den Wert der noAnswerPersonalAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setNoAnswerPersonalAudioFile(AnnouncementFileLevelKey value) {
        this.noAnswerPersonalAudioFile = value;
    }

    /**
     * Ruft den Wert der noAnswerPersonalVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getNoAnswerPersonalVideoFile() {
        return noAnswerPersonalVideoFile;
    }

    /**
     * Legt den Wert der noAnswerPersonalVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setNoAnswerPersonalVideoFile(AnnouncementFileLevelKey value) {
        this.noAnswerPersonalVideoFile = value;
    }

    /**
     * Ruft den Wert der noAnswerAlternateGreeting01-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingRead20 }
     *     
     */
    public VoiceMessagingAlternateNoAnswerGreetingRead20 getNoAnswerAlternateGreeting01() {
        return noAnswerAlternateGreeting01;
    }

    /**
     * Legt den Wert der noAnswerAlternateGreeting01-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingRead20 }
     *     
     */
    public void setNoAnswerAlternateGreeting01(VoiceMessagingAlternateNoAnswerGreetingRead20 value) {
        this.noAnswerAlternateGreeting01 = value;
    }

    /**
     * Ruft den Wert der noAnswerAlternateGreeting02-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingRead20 }
     *     
     */
    public VoiceMessagingAlternateNoAnswerGreetingRead20 getNoAnswerAlternateGreeting02() {
        return noAnswerAlternateGreeting02;
    }

    /**
     * Legt den Wert der noAnswerAlternateGreeting02-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingRead20 }
     *     
     */
    public void setNoAnswerAlternateGreeting02(VoiceMessagingAlternateNoAnswerGreetingRead20 value) {
        this.noAnswerAlternateGreeting02 = value;
    }

    /**
     * Ruft den Wert der noAnswerAlternateGreeting03-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingRead20 }
     *     
     */
    public VoiceMessagingAlternateNoAnswerGreetingRead20 getNoAnswerAlternateGreeting03() {
        return noAnswerAlternateGreeting03;
    }

    /**
     * Legt den Wert der noAnswerAlternateGreeting03-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingRead20 }
     *     
     */
    public void setNoAnswerAlternateGreeting03(VoiceMessagingAlternateNoAnswerGreetingRead20 value) {
        this.noAnswerAlternateGreeting03 = value;
    }

    /**
     * Ruft den Wert der extendedAwayEnabled-Eigenschaft ab.
     * 
     */
    public boolean isExtendedAwayEnabled() {
        return extendedAwayEnabled;
    }

    /**
     * Legt den Wert der extendedAwayEnabled-Eigenschaft fest.
     * 
     */
    public void setExtendedAwayEnabled(boolean value) {
        this.extendedAwayEnabled = value;
    }

    /**
     * Ruft den Wert der extendedAwayDisableMessageDeposit-Eigenschaft ab.
     * 
     */
    public boolean isExtendedAwayDisableMessageDeposit() {
        return extendedAwayDisableMessageDeposit;
    }

    /**
     * Legt den Wert der extendedAwayDisableMessageDeposit-Eigenschaft fest.
     * 
     */
    public void setExtendedAwayDisableMessageDeposit(boolean value) {
        this.extendedAwayDisableMessageDeposit = value;
    }

    /**
     * Ruft den Wert der extendedAwayAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getExtendedAwayAudioFile() {
        return extendedAwayAudioFile;
    }

    /**
     * Legt den Wert der extendedAwayAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setExtendedAwayAudioFile(AnnouncementFileLevelKey value) {
        this.extendedAwayAudioFile = value;
    }

    /**
     * Ruft den Wert der extendedAwayVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getExtendedAwayVideoFile() {
        return extendedAwayVideoFile;
    }

    /**
     * Legt den Wert der extendedAwayVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setExtendedAwayVideoFile(AnnouncementFileLevelKey value) {
        this.extendedAwayVideoFile = value;
    }

    /**
     * Ruft den Wert der noAnswerNumberOfRings-Eigenschaft ab.
     * 
     */
    public int getNoAnswerNumberOfRings() {
        return noAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der noAnswerNumberOfRings-Eigenschaft fest.
     * 
     */
    public void setNoAnswerNumberOfRings(int value) {
        this.noAnswerNumberOfRings = value;
    }

    /**
     * Ruft den Wert der disableMessageDeposit-Eigenschaft ab.
     * 
     */
    public boolean isDisableMessageDeposit() {
        return disableMessageDeposit;
    }

    /**
     * Legt den Wert der disableMessageDeposit-Eigenschaft fest.
     * 
     */
    public void setDisableMessageDeposit(boolean value) {
        this.disableMessageDeposit = value;
    }

    /**
     * Ruft den Wert der disableMessageDepositAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingDisableMessageDepositSelection }
     *     
     */
    public VoiceMessagingDisableMessageDepositSelection getDisableMessageDepositAction() {
        return disableMessageDepositAction;
    }

    /**
     * Legt den Wert der disableMessageDepositAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingDisableMessageDepositSelection }
     *     
     */
    public void setDisableMessageDepositAction(VoiceMessagingDisableMessageDepositSelection value) {
        this.disableMessageDepositAction = value;
    }

    /**
     * Ruft den Wert der greetingOnlyForwardDestination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGreetingOnlyForwardDestination() {
        return greetingOnlyForwardDestination;
    }

    /**
     * Legt den Wert der greetingOnlyForwardDestination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGreetingOnlyForwardDestination(String value) {
        this.greetingOnlyForwardDestination = value;
    }

}
