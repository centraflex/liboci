//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the system level device profile authentication password rule settings.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemDeviceProfileAuthenticationPasswordRulesModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceProfileAuthenticationPasswordRulesModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="disallowAuthenticationName" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disallowOldPassword" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disallowReversedOldPassword" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="restrictMinDigits" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="minDigits" type="{}PasswordMinDigits" minOccurs="0"/>
 *         <element name="restrictMinUpperCaseLetters" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="minUpperCaseLetters" type="{}PasswordMinUpperCaseLetters" minOccurs="0"/>
 *         <element name="restrictMinLowerCaseLetters" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="minLowerCaseLetters" type="{}PasswordMinLowerCaseLetters" minOccurs="0"/>
 *         <element name="restrictMinNonAlphanumericCharacters" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="minNonAlphanumericCharacters" type="{}PasswordMinNonAlphanumericCharacters" minOccurs="0"/>
 *         <element name="minLength" type="{}PasswordMinLength" minOccurs="0"/>
 *         <element name="sendPermanentLockoutNotification" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="permanentLockoutNotifyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="deviceProfileAuthenticationLockoutType" type="{}AuthenticationLockoutType" minOccurs="0"/>
 *         <element name="deviceProfileTemporaryLockoutThreshold" type="{}AuthenticationTemporaryLockoutThreshold" minOccurs="0"/>
 *         <element name="deviceProfileWaitAlgorithm" type="{}AuthenticationLockoutWaitAlgorithmType" minOccurs="0"/>
 *         <element name="deviceProfileLockoutFixedMinutes" type="{}AuthenticationLockoutFixedWaitTimeMinutes" minOccurs="0"/>
 *         <element name="deviceProfilePermanentLockoutThreshold" type="{}AuthenticationPermanentLockoutThreshold" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceProfileAuthenticationPasswordRulesModifyRequest", propOrder = {
    "disallowAuthenticationName",
    "disallowOldPassword",
    "disallowReversedOldPassword",
    "restrictMinDigits",
    "minDigits",
    "restrictMinUpperCaseLetters",
    "minUpperCaseLetters",
    "restrictMinLowerCaseLetters",
    "minLowerCaseLetters",
    "restrictMinNonAlphanumericCharacters",
    "minNonAlphanumericCharacters",
    "minLength",
    "sendPermanentLockoutNotification",
    "permanentLockoutNotifyEmailAddress",
    "deviceProfileAuthenticationLockoutType",
    "deviceProfileTemporaryLockoutThreshold",
    "deviceProfileWaitAlgorithm",
    "deviceProfileLockoutFixedMinutes",
    "deviceProfilePermanentLockoutThreshold"
})
public class SystemDeviceProfileAuthenticationPasswordRulesModifyRequest
    extends OCIRequest
{

    protected Boolean disallowAuthenticationName;
    protected Boolean disallowOldPassword;
    protected Boolean disallowReversedOldPassword;
    protected Boolean restrictMinDigits;
    protected Integer minDigits;
    protected Boolean restrictMinUpperCaseLetters;
    protected Integer minUpperCaseLetters;
    protected Boolean restrictMinLowerCaseLetters;
    protected Integer minLowerCaseLetters;
    protected Boolean restrictMinNonAlphanumericCharacters;
    protected Integer minNonAlphanumericCharacters;
    protected Integer minLength;
    protected Boolean sendPermanentLockoutNotification;
    @XmlElementRef(name = "permanentLockoutNotifyEmailAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> permanentLockoutNotifyEmailAddress;
    @XmlSchemaType(name = "token")
    protected AuthenticationLockoutType deviceProfileAuthenticationLockoutType;
    protected Integer deviceProfileTemporaryLockoutThreshold;
    @XmlSchemaType(name = "token")
    protected AuthenticationLockoutWaitAlgorithmType deviceProfileWaitAlgorithm;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceProfileLockoutFixedMinutes;
    protected Integer deviceProfilePermanentLockoutThreshold;

    /**
     * Ruft den Wert der disallowAuthenticationName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowAuthenticationName() {
        return disallowAuthenticationName;
    }

    /**
     * Legt den Wert der disallowAuthenticationName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowAuthenticationName(Boolean value) {
        this.disallowAuthenticationName = value;
    }

    /**
     * Ruft den Wert der disallowOldPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowOldPassword() {
        return disallowOldPassword;
    }

    /**
     * Legt den Wert der disallowOldPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowOldPassword(Boolean value) {
        this.disallowOldPassword = value;
    }

    /**
     * Ruft den Wert der disallowReversedOldPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowReversedOldPassword() {
        return disallowReversedOldPassword;
    }

    /**
     * Legt den Wert der disallowReversedOldPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowReversedOldPassword(Boolean value) {
        this.disallowReversedOldPassword = value;
    }

    /**
     * Ruft den Wert der restrictMinDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictMinDigits() {
        return restrictMinDigits;
    }

    /**
     * Legt den Wert der restrictMinDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictMinDigits(Boolean value) {
        this.restrictMinDigits = value;
    }

    /**
     * Ruft den Wert der minDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinDigits() {
        return minDigits;
    }

    /**
     * Legt den Wert der minDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinDigits(Integer value) {
        this.minDigits = value;
    }

    /**
     * Ruft den Wert der restrictMinUpperCaseLetters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictMinUpperCaseLetters() {
        return restrictMinUpperCaseLetters;
    }

    /**
     * Legt den Wert der restrictMinUpperCaseLetters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictMinUpperCaseLetters(Boolean value) {
        this.restrictMinUpperCaseLetters = value;
    }

    /**
     * Ruft den Wert der minUpperCaseLetters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinUpperCaseLetters() {
        return minUpperCaseLetters;
    }

    /**
     * Legt den Wert der minUpperCaseLetters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinUpperCaseLetters(Integer value) {
        this.minUpperCaseLetters = value;
    }

    /**
     * Ruft den Wert der restrictMinLowerCaseLetters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictMinLowerCaseLetters() {
        return restrictMinLowerCaseLetters;
    }

    /**
     * Legt den Wert der restrictMinLowerCaseLetters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictMinLowerCaseLetters(Boolean value) {
        this.restrictMinLowerCaseLetters = value;
    }

    /**
     * Ruft den Wert der minLowerCaseLetters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinLowerCaseLetters() {
        return minLowerCaseLetters;
    }

    /**
     * Legt den Wert der minLowerCaseLetters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinLowerCaseLetters(Integer value) {
        this.minLowerCaseLetters = value;
    }

    /**
     * Ruft den Wert der restrictMinNonAlphanumericCharacters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictMinNonAlphanumericCharacters() {
        return restrictMinNonAlphanumericCharacters;
    }

    /**
     * Legt den Wert der restrictMinNonAlphanumericCharacters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictMinNonAlphanumericCharacters(Boolean value) {
        this.restrictMinNonAlphanumericCharacters = value;
    }

    /**
     * Ruft den Wert der minNonAlphanumericCharacters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinNonAlphanumericCharacters() {
        return minNonAlphanumericCharacters;
    }

    /**
     * Legt den Wert der minNonAlphanumericCharacters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinNonAlphanumericCharacters(Integer value) {
        this.minNonAlphanumericCharacters = value;
    }

    /**
     * Ruft den Wert der minLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinLength() {
        return minLength;
    }

    /**
     * Legt den Wert der minLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinLength(Integer value) {
        this.minLength = value;
    }

    /**
     * Ruft den Wert der sendPermanentLockoutNotification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendPermanentLockoutNotification() {
        return sendPermanentLockoutNotification;
    }

    /**
     * Legt den Wert der sendPermanentLockoutNotification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendPermanentLockoutNotification(Boolean value) {
        this.sendPermanentLockoutNotification = value;
    }

    /**
     * Ruft den Wert der permanentLockoutNotifyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPermanentLockoutNotifyEmailAddress() {
        return permanentLockoutNotifyEmailAddress;
    }

    /**
     * Legt den Wert der permanentLockoutNotifyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPermanentLockoutNotifyEmailAddress(JAXBElement<String> value) {
        this.permanentLockoutNotifyEmailAddress = value;
    }

    /**
     * Ruft den Wert der deviceProfileAuthenticationLockoutType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationLockoutType }
     *     
     */
    public AuthenticationLockoutType getDeviceProfileAuthenticationLockoutType() {
        return deviceProfileAuthenticationLockoutType;
    }

    /**
     * Legt den Wert der deviceProfileAuthenticationLockoutType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationLockoutType }
     *     
     */
    public void setDeviceProfileAuthenticationLockoutType(AuthenticationLockoutType value) {
        this.deviceProfileAuthenticationLockoutType = value;
    }

    /**
     * Ruft den Wert der deviceProfileTemporaryLockoutThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDeviceProfileTemporaryLockoutThreshold() {
        return deviceProfileTemporaryLockoutThreshold;
    }

    /**
     * Legt den Wert der deviceProfileTemporaryLockoutThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDeviceProfileTemporaryLockoutThreshold(Integer value) {
        this.deviceProfileTemporaryLockoutThreshold = value;
    }

    /**
     * Ruft den Wert der deviceProfileWaitAlgorithm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationLockoutWaitAlgorithmType }
     *     
     */
    public AuthenticationLockoutWaitAlgorithmType getDeviceProfileWaitAlgorithm() {
        return deviceProfileWaitAlgorithm;
    }

    /**
     * Legt den Wert der deviceProfileWaitAlgorithm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationLockoutWaitAlgorithmType }
     *     
     */
    public void setDeviceProfileWaitAlgorithm(AuthenticationLockoutWaitAlgorithmType value) {
        this.deviceProfileWaitAlgorithm = value;
    }

    /**
     * Ruft den Wert der deviceProfileLockoutFixedMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceProfileLockoutFixedMinutes() {
        return deviceProfileLockoutFixedMinutes;
    }

    /**
     * Legt den Wert der deviceProfileLockoutFixedMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceProfileLockoutFixedMinutes(String value) {
        this.deviceProfileLockoutFixedMinutes = value;
    }

    /**
     * Ruft den Wert der deviceProfilePermanentLockoutThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDeviceProfilePermanentLockoutThreshold() {
        return deviceProfilePermanentLockoutThreshold;
    }

    /**
     * Legt den Wert der deviceProfilePermanentLockoutThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDeviceProfilePermanentLockoutThreshold(Integer value) {
        this.deviceProfilePermanentLockoutThreshold = value;
    }

}
