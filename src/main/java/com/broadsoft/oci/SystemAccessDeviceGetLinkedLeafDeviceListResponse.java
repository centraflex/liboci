//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemAccessDeviceGetLinkedLeafDeviceListRequest.
 *       
 * 
 * <p>Java-Klasse für SystemAccessDeviceGetLinkedLeafDeviceListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAccessDeviceGetLinkedLeafDeviceListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="treeDeviceLinkId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         <element name="leafDeviceKey" type="{}AccessDeviceKey" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAccessDeviceGetLinkedLeafDeviceListResponse", propOrder = {
    "treeDeviceLinkId",
    "leafDeviceKey"
})
public class SystemAccessDeviceGetLinkedLeafDeviceListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected String treeDeviceLinkId;
    protected List<AccessDeviceKey> leafDeviceKey;

    /**
     * Ruft den Wert der treeDeviceLinkId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTreeDeviceLinkId() {
        return treeDeviceLinkId;
    }

    /**
     * Legt den Wert der treeDeviceLinkId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTreeDeviceLinkId(String value) {
        this.treeDeviceLinkId = value;
    }

    /**
     * Gets the value of the leafDeviceKey property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the leafDeviceKey property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLeafDeviceKey().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccessDeviceKey }
     * 
     * 
     * @return
     *     The value of the leafDeviceKey property.
     */
    public List<AccessDeviceKey> getLeafDeviceKey() {
        if (leafDeviceKey == null) {
            leafDeviceKey = new ArrayList<>();
        }
        return this.leafDeviceKey;
    }

}
