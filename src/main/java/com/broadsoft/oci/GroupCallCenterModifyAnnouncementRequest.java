//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a call center's announcement settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterModifyAnnouncementRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterModifyAnnouncementRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="entranceMessageSelection" type="{}CallCenterAnnouncementSelection" minOccurs="0"/>
 *         <element name="entranceMessageAudioFile" type="{}LabeledFileResource" minOccurs="0"/>
 *         <element name="entranceMessageVideoFile" type="{}LabeledFileResource" minOccurs="0"/>
 *         <element name="periodicComfortMessageSelection" type="{}CallCenterAnnouncementSelection" minOccurs="0"/>
 *         <element name="periodicComfortMessageAudioFile" type="{}LabeledFileResource" minOccurs="0"/>
 *         <element name="periodicComfortMessageVideoFile" type="{}LabeledFileResource" minOccurs="0"/>
 *         <element name="onHoldMessageSelection" type="{}CallCenterAnnouncementSelection" minOccurs="0"/>
 *         <element name="onHoldMessageAudioFile" type="{}LabeledFileResource" minOccurs="0"/>
 *         <element name="onHoldMessageVideoFile" type="{}LabeledFileResource" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterModifyAnnouncementRequest", propOrder = {
    "serviceUserId",
    "entranceMessageSelection",
    "entranceMessageAudioFile",
    "entranceMessageVideoFile",
    "periodicComfortMessageSelection",
    "periodicComfortMessageAudioFile",
    "periodicComfortMessageVideoFile",
    "onHoldMessageSelection",
    "onHoldMessageAudioFile",
    "onHoldMessageVideoFile"
})
public class GroupCallCenterModifyAnnouncementRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlSchemaType(name = "token")
    protected CallCenterAnnouncementSelection entranceMessageSelection;
    protected LabeledFileResource entranceMessageAudioFile;
    protected LabeledFileResource entranceMessageVideoFile;
    @XmlSchemaType(name = "token")
    protected CallCenterAnnouncementSelection periodicComfortMessageSelection;
    protected LabeledFileResource periodicComfortMessageAudioFile;
    protected LabeledFileResource periodicComfortMessageVideoFile;
    @XmlSchemaType(name = "token")
    protected CallCenterAnnouncementSelection onHoldMessageSelection;
    protected LabeledFileResource onHoldMessageAudioFile;
    protected LabeledFileResource onHoldMessageVideoFile;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der entranceMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public CallCenterAnnouncementSelection getEntranceMessageSelection() {
        return entranceMessageSelection;
    }

    /**
     * Legt den Wert der entranceMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public void setEntranceMessageSelection(CallCenterAnnouncementSelection value) {
        this.entranceMessageSelection = value;
    }

    /**
     * Ruft den Wert der entranceMessageAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledFileResource }
     *     
     */
    public LabeledFileResource getEntranceMessageAudioFile() {
        return entranceMessageAudioFile;
    }

    /**
     * Legt den Wert der entranceMessageAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledFileResource }
     *     
     */
    public void setEntranceMessageAudioFile(LabeledFileResource value) {
        this.entranceMessageAudioFile = value;
    }

    /**
     * Ruft den Wert der entranceMessageVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledFileResource }
     *     
     */
    public LabeledFileResource getEntranceMessageVideoFile() {
        return entranceMessageVideoFile;
    }

    /**
     * Legt den Wert der entranceMessageVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledFileResource }
     *     
     */
    public void setEntranceMessageVideoFile(LabeledFileResource value) {
        this.entranceMessageVideoFile = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public CallCenterAnnouncementSelection getPeriodicComfortMessageSelection() {
        return periodicComfortMessageSelection;
    }

    /**
     * Legt den Wert der periodicComfortMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public void setPeriodicComfortMessageSelection(CallCenterAnnouncementSelection value) {
        this.periodicComfortMessageSelection = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledFileResource }
     *     
     */
    public LabeledFileResource getPeriodicComfortMessageAudioFile() {
        return periodicComfortMessageAudioFile;
    }

    /**
     * Legt den Wert der periodicComfortMessageAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledFileResource }
     *     
     */
    public void setPeriodicComfortMessageAudioFile(LabeledFileResource value) {
        this.periodicComfortMessageAudioFile = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledFileResource }
     *     
     */
    public LabeledFileResource getPeriodicComfortMessageVideoFile() {
        return periodicComfortMessageVideoFile;
    }

    /**
     * Legt den Wert der periodicComfortMessageVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledFileResource }
     *     
     */
    public void setPeriodicComfortMessageVideoFile(LabeledFileResource value) {
        this.periodicComfortMessageVideoFile = value;
    }

    /**
     * Ruft den Wert der onHoldMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public CallCenterAnnouncementSelection getOnHoldMessageSelection() {
        return onHoldMessageSelection;
    }

    /**
     * Legt den Wert der onHoldMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public void setOnHoldMessageSelection(CallCenterAnnouncementSelection value) {
        this.onHoldMessageSelection = value;
    }

    /**
     * Ruft den Wert der onHoldMessageAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledFileResource }
     *     
     */
    public LabeledFileResource getOnHoldMessageAudioFile() {
        return onHoldMessageAudioFile;
    }

    /**
     * Legt den Wert der onHoldMessageAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledFileResource }
     *     
     */
    public void setOnHoldMessageAudioFile(LabeledFileResource value) {
        this.onHoldMessageAudioFile = value;
    }

    /**
     * Ruft den Wert der onHoldMessageVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledFileResource }
     *     
     */
    public LabeledFileResource getOnHoldMessageVideoFile() {
        return onHoldMessageVideoFile;
    }

    /**
     * Legt den Wert der onHoldMessageVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledFileResource }
     *     
     */
    public void setOnHoldMessageVideoFile(LabeledFileResource value) {
        this.onHoldMessageVideoFile = value;
    }

}
