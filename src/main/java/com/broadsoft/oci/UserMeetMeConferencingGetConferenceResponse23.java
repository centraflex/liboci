//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserMeetMeConferencingGetConferenceRequest23.
 *         Contains the information of a conference.
 *       
 * 
 * <p>Java-Klasse für UserMeetMeConferencingGetConferenceResponse23 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserMeetMeConferencingGetConferenceResponse23">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="title" type="{}MeetMeConferencingConferenceTitle"/>
 *         <element name="estimatedParticipants" type="{}MeetMeConferencingNumberOfParticipants" minOccurs="0"/>
 *         <choice>
 *           <element name="restrictParticipants" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *           <element name="maxParticipants" type="{}MeetMeConferencingNumberOfParticipants"/>
 *         </choice>
 *         <element name="accountCode" type="{}MeetMeConferencingConferenceAccountCode" minOccurs="0"/>
 *         <element name="muteAllAttendeesOnEntry" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="endConferenceOnModeratorExit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="moderatorRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="requireSecurityPin" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="securityPin" type="{}MeetMeConferencingConferenceSecurityPin" minOccurs="0"/>
 *         <element name="allowUniqueIdentifier" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="attendeeNotification" type="{}MeetMeConferencingConferenceAttendeeNotification"/>
 *         <element name="conferenceSchedule" type="{}MeetMeConferencingConferenceSchedule"/>
 *         <element name="moderatorPin" type="{}MeetMeConferencingConferencePassCode"/>
 *         <element name="hostTimeZone" type="{}TimeZone"/>
 *         <element name="allowParticipantUnmuteInAutoLectureMode" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserMeetMeConferencingGetConferenceResponse23", propOrder = {
    "title",
    "estimatedParticipants",
    "restrictParticipants",
    "maxParticipants",
    "accountCode",
    "muteAllAttendeesOnEntry",
    "endConferenceOnModeratorExit",
    "moderatorRequired",
    "requireSecurityPin",
    "securityPin",
    "allowUniqueIdentifier",
    "attendeeNotification",
    "conferenceSchedule",
    "moderatorPin",
    "hostTimeZone",
    "allowParticipantUnmuteInAutoLectureMode"
})
public class UserMeetMeConferencingGetConferenceResponse23
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String title;
    protected Integer estimatedParticipants;
    protected Boolean restrictParticipants;
    protected Integer maxParticipants;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accountCode;
    protected boolean muteAllAttendeesOnEntry;
    protected boolean endConferenceOnModeratorExit;
    protected boolean moderatorRequired;
    protected boolean requireSecurityPin;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String securityPin;
    protected boolean allowUniqueIdentifier;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MeetMeConferencingConferenceAttendeeNotification attendeeNotification;
    @XmlElement(required = true)
    protected MeetMeConferencingConferenceSchedule conferenceSchedule;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String moderatorPin;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String hostTimeZone;
    protected boolean allowParticipantUnmuteInAutoLectureMode;

    /**
     * Ruft den Wert der title-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Legt den Wert der title-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Ruft den Wert der estimatedParticipants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEstimatedParticipants() {
        return estimatedParticipants;
    }

    /**
     * Legt den Wert der estimatedParticipants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEstimatedParticipants(Integer value) {
        this.estimatedParticipants = value;
    }

    /**
     * Ruft den Wert der restrictParticipants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictParticipants() {
        return restrictParticipants;
    }

    /**
     * Legt den Wert der restrictParticipants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictParticipants(Boolean value) {
        this.restrictParticipants = value;
    }

    /**
     * Ruft den Wert der maxParticipants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxParticipants() {
        return maxParticipants;
    }

    /**
     * Legt den Wert der maxParticipants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxParticipants(Integer value) {
        this.maxParticipants = value;
    }

    /**
     * Ruft den Wert der accountCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCode() {
        return accountCode;
    }

    /**
     * Legt den Wert der accountCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCode(String value) {
        this.accountCode = value;
    }

    /**
     * Ruft den Wert der muteAllAttendeesOnEntry-Eigenschaft ab.
     * 
     */
    public boolean isMuteAllAttendeesOnEntry() {
        return muteAllAttendeesOnEntry;
    }

    /**
     * Legt den Wert der muteAllAttendeesOnEntry-Eigenschaft fest.
     * 
     */
    public void setMuteAllAttendeesOnEntry(boolean value) {
        this.muteAllAttendeesOnEntry = value;
    }

    /**
     * Ruft den Wert der endConferenceOnModeratorExit-Eigenschaft ab.
     * 
     */
    public boolean isEndConferenceOnModeratorExit() {
        return endConferenceOnModeratorExit;
    }

    /**
     * Legt den Wert der endConferenceOnModeratorExit-Eigenschaft fest.
     * 
     */
    public void setEndConferenceOnModeratorExit(boolean value) {
        this.endConferenceOnModeratorExit = value;
    }

    /**
     * Ruft den Wert der moderatorRequired-Eigenschaft ab.
     * 
     */
    public boolean isModeratorRequired() {
        return moderatorRequired;
    }

    /**
     * Legt den Wert der moderatorRequired-Eigenschaft fest.
     * 
     */
    public void setModeratorRequired(boolean value) {
        this.moderatorRequired = value;
    }

    /**
     * Ruft den Wert der requireSecurityPin-Eigenschaft ab.
     * 
     */
    public boolean isRequireSecurityPin() {
        return requireSecurityPin;
    }

    /**
     * Legt den Wert der requireSecurityPin-Eigenschaft fest.
     * 
     */
    public void setRequireSecurityPin(boolean value) {
        this.requireSecurityPin = value;
    }

    /**
     * Ruft den Wert der securityPin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecurityPin() {
        return securityPin;
    }

    /**
     * Legt den Wert der securityPin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecurityPin(String value) {
        this.securityPin = value;
    }

    /**
     * Ruft den Wert der allowUniqueIdentifier-Eigenschaft ab.
     * 
     */
    public boolean isAllowUniqueIdentifier() {
        return allowUniqueIdentifier;
    }

    /**
     * Legt den Wert der allowUniqueIdentifier-Eigenschaft fest.
     * 
     */
    public void setAllowUniqueIdentifier(boolean value) {
        this.allowUniqueIdentifier = value;
    }

    /**
     * Ruft den Wert der attendeeNotification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferenceAttendeeNotification }
     *     
     */
    public MeetMeConferencingConferenceAttendeeNotification getAttendeeNotification() {
        return attendeeNotification;
    }

    /**
     * Legt den Wert der attendeeNotification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferenceAttendeeNotification }
     *     
     */
    public void setAttendeeNotification(MeetMeConferencingConferenceAttendeeNotification value) {
        this.attendeeNotification = value;
    }

    /**
     * Ruft den Wert der conferenceSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferenceSchedule }
     *     
     */
    public MeetMeConferencingConferenceSchedule getConferenceSchedule() {
        return conferenceSchedule;
    }

    /**
     * Legt den Wert der conferenceSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferenceSchedule }
     *     
     */
    public void setConferenceSchedule(MeetMeConferencingConferenceSchedule value) {
        this.conferenceSchedule = value;
    }

    /**
     * Ruft den Wert der moderatorPin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModeratorPin() {
        return moderatorPin;
    }

    /**
     * Legt den Wert der moderatorPin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModeratorPin(String value) {
        this.moderatorPin = value;
    }

    /**
     * Ruft den Wert der hostTimeZone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHostTimeZone() {
        return hostTimeZone;
    }

    /**
     * Legt den Wert der hostTimeZone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHostTimeZone(String value) {
        this.hostTimeZone = value;
    }

    /**
     * Ruft den Wert der allowParticipantUnmuteInAutoLectureMode-Eigenschaft ab.
     * 
     */
    public boolean isAllowParticipantUnmuteInAutoLectureMode() {
        return allowParticipantUnmuteInAutoLectureMode;
    }

    /**
     * Legt den Wert der allowParticipantUnmuteInAutoLectureMode-Eigenschaft fest.
     * 
     */
    public void setAllowParticipantUnmuteInAutoLectureMode(boolean value) {
        this.allowParticipantUnmuteInAutoLectureMode = value;
    }

}
