//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ThirdPartyVoiceMailSupportMailboxIdType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ThirdPartyVoiceMailSupportMailboxIdType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="User Or Group Phone Number"/>
 *     <enumeration value="URL"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ThirdPartyVoiceMailSupportMailboxIdType")
@XmlEnum
public enum ThirdPartyVoiceMailSupportMailboxIdType {

    @XmlEnumValue("User Or Group Phone Number")
    USER_OR_GROUP_PHONE_NUMBER("User Or Group Phone Number"),
    URL("URL");
    private final String value;

    ThirdPartyVoiceMailSupportMailboxIdType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ThirdPartyVoiceMailSupportMailboxIdType fromValue(String v) {
        for (ThirdPartyVoiceMailSupportMailboxIdType c: ThirdPartyVoiceMailSupportMailboxIdType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
