//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Get the third-party emergency call service settings for the system.
 *         The response is either a SystemThirdPartyEmergencyCallingGetResponse or an ErrorResponse.
 *         
 *         Replaced by: SystemThirdPartyEmergencyCallingGetRequest24
 *       
 * 
 * <p>Java-Klasse für SystemThirdPartyEmergencyCallingGetRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemThirdPartyEmergencyCallingGetRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemThirdPartyEmergencyCallingGetRequest")
public class SystemThirdPartyEmergencyCallingGetRequest
    extends OCIRequest
{


}
