//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a performance measurements reporting ftp server.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemPerformanceMeasurementReportingAddFileServerRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPerformanceMeasurementReportingAddFileServerRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="ftpHostNetAddress" type="{}NetAddress"/>
 *         <element name="ftpUserId" type="{}FTPUserId"/>
 *         <element name="ftpUserPassword" type="{}FTPUserPassword"/>
 *         <element name="passiveFTP" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPerformanceMeasurementReportingAddFileServerRequest", propOrder = {
    "ftpHostNetAddress",
    "ftpUserId",
    "ftpUserPassword",
    "passiveFTP"
})
public class SystemPerformanceMeasurementReportingAddFileServerRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String ftpHostNetAddress;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String ftpUserId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String ftpUserPassword;
    protected Boolean passiveFTP;

    /**
     * Ruft den Wert der ftpHostNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFtpHostNetAddress() {
        return ftpHostNetAddress;
    }

    /**
     * Legt den Wert der ftpHostNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFtpHostNetAddress(String value) {
        this.ftpHostNetAddress = value;
    }

    /**
     * Ruft den Wert der ftpUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFtpUserId() {
        return ftpUserId;
    }

    /**
     * Legt den Wert der ftpUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFtpUserId(String value) {
        this.ftpUserId = value;
    }

    /**
     * Ruft den Wert der ftpUserPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFtpUserPassword() {
        return ftpUserPassword;
    }

    /**
     * Legt den Wert der ftpUserPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFtpUserPassword(String value) {
        this.ftpUserPassword = value;
    }

    /**
     * Ruft den Wert der passiveFTP-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPassiveFTP() {
        return passiveFTP;
    }

    /**
     * Legt den Wert der passiveFTP-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPassiveFTP(Boolean value) {
        this.passiveFTP = value;
    }

}
