//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         ShInterface User Id Data Entry.
 *       
 * 
 * <p>Java-Klasse für ShInterfaceUserIdDataEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ShInterfaceUserIdDataEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="userType" type="{}UserType"/>
 *         <element name="publicUserIdentity" type="{}PublicUserIdentity"/>
 *         <element name="endpointType" type="{}EndpointType"/>
 *         <element name="SCSCFName" type="{}SIPURI" minOccurs="0"/>
 *         <element name="IMSUserState" type="{}IMSUserState"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShInterfaceUserIdDataEntry", propOrder = {
    "userType",
    "publicUserIdentity",
    "endpointType",
    "scscfName",
    "imsUserState"
})
public class ShInterfaceUserIdDataEntry {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected UserType userType;
    @XmlElement(required = true)
    protected PublicUserIdentity publicUserIdentity;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EndpointType endpointType;
    @XmlElement(name = "SCSCFName")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String scscfName;
    @XmlElement(name = "IMSUserState", required = true)
    @XmlSchemaType(name = "token")
    protected IMSUserState imsUserState;

    /**
     * Ruft den Wert der userType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserType }
     *     
     */
    public UserType getUserType() {
        return userType;
    }

    /**
     * Legt den Wert der userType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserType }
     *     
     */
    public void setUserType(UserType value) {
        this.userType = value;
    }

    /**
     * Ruft den Wert der publicUserIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PublicUserIdentity }
     *     
     */
    public PublicUserIdentity getPublicUserIdentity() {
        return publicUserIdentity;
    }

    /**
     * Legt den Wert der publicUserIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PublicUserIdentity }
     *     
     */
    public void setPublicUserIdentity(PublicUserIdentity value) {
        this.publicUserIdentity = value;
    }

    /**
     * Ruft den Wert der endpointType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EndpointType }
     *     
     */
    public EndpointType getEndpointType() {
        return endpointType;
    }

    /**
     * Legt den Wert der endpointType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EndpointType }
     *     
     */
    public void setEndpointType(EndpointType value) {
        this.endpointType = value;
    }

    /**
     * Ruft den Wert der scscfName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSCSCFName() {
        return scscfName;
    }

    /**
     * Legt den Wert der scscfName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSCSCFName(String value) {
        this.scscfName = value;
    }

    /**
     * Ruft den Wert der imsUserState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IMSUserState }
     *     
     */
    public IMSUserState getIMSUserState() {
        return imsUserState;
    }

    /**
     * Legt den Wert der imsUserState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IMSUserState }
     *     
     */
    public void setIMSUserState(IMSUserState value) {
        this.imsUserState = value;
    }

}
