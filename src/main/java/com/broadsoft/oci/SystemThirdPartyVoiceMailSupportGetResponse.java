//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemThirdPartyVoiceMailSupportGetRequest.
 *         
 *         Replaced by: SystemThirdPartyVoiceMailSupportGetResponse17sp4
 *       
 * 
 * <p>Java-Klasse für SystemThirdPartyVoiceMailSupportGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemThirdPartyVoiceMailSupportGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="overrideAltCallerIdForVMRetrieval" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemThirdPartyVoiceMailSupportGetResponse", propOrder = {
    "overrideAltCallerIdForVMRetrieval"
})
public class SystemThirdPartyVoiceMailSupportGetResponse
    extends OCIDataResponse
{

    protected boolean overrideAltCallerIdForVMRetrieval;

    /**
     * Ruft den Wert der overrideAltCallerIdForVMRetrieval-Eigenschaft ab.
     * 
     */
    public boolean isOverrideAltCallerIdForVMRetrieval() {
        return overrideAltCallerIdForVMRetrieval;
    }

    /**
     * Legt den Wert der overrideAltCallerIdForVMRetrieval-Eigenschaft fest.
     * 
     */
    public void setOverrideAltCallerIdForVMRetrieval(boolean value) {
        this.overrideAltCallerIdForVMRetrieval = value;
    }

}
