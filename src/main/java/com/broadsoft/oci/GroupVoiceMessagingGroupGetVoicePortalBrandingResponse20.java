//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupVoiceMessagingGroupGetVoicePortalBrandingRequest20.
 *       
 * 
 * <p>Java-Klasse für GroupVoiceMessagingGroupGetVoicePortalBrandingResponse20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupVoiceMessagingGroupGetVoicePortalBrandingResponse20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="voicePortalGreetingSelection" type="{}VoiceMessagingBrandingSelection"/>
 *         <element name="voicePortalGreetingFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *         <element name="voiceMessagingGreetingSelection" type="{}VoiceMessagingBrandingSelection"/>
 *         <element name="voiceMessagingGreetingFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupVoiceMessagingGroupGetVoicePortalBrandingResponse20", propOrder = {
    "voicePortalGreetingSelection",
    "voicePortalGreetingFile",
    "voiceMessagingGreetingSelection",
    "voiceMessagingGreetingFile"
})
public class GroupVoiceMessagingGroupGetVoicePortalBrandingResponse20
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected VoiceMessagingBrandingSelection voicePortalGreetingSelection;
    protected AnnouncementFileKey voicePortalGreetingFile;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected VoiceMessagingBrandingSelection voiceMessagingGreetingSelection;
    protected AnnouncementFileKey voiceMessagingGreetingFile;

    /**
     * Ruft den Wert der voicePortalGreetingSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public VoiceMessagingBrandingSelection getVoicePortalGreetingSelection() {
        return voicePortalGreetingSelection;
    }

    /**
     * Legt den Wert der voicePortalGreetingSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public void setVoicePortalGreetingSelection(VoiceMessagingBrandingSelection value) {
        this.voicePortalGreetingSelection = value;
    }

    /**
     * Ruft den Wert der voicePortalGreetingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileKey }
     *     
     */
    public AnnouncementFileKey getVoicePortalGreetingFile() {
        return voicePortalGreetingFile;
    }

    /**
     * Legt den Wert der voicePortalGreetingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileKey }
     *     
     */
    public void setVoicePortalGreetingFile(AnnouncementFileKey value) {
        this.voicePortalGreetingFile = value;
    }

    /**
     * Ruft den Wert der voiceMessagingGreetingSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public VoiceMessagingBrandingSelection getVoiceMessagingGreetingSelection() {
        return voiceMessagingGreetingSelection;
    }

    /**
     * Legt den Wert der voiceMessagingGreetingSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public void setVoiceMessagingGreetingSelection(VoiceMessagingBrandingSelection value) {
        this.voiceMessagingGreetingSelection = value;
    }

    /**
     * Ruft den Wert der voiceMessagingGreetingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileKey }
     *     
     */
    public AnnouncementFileKey getVoiceMessagingGreetingFile() {
        return voiceMessagingGreetingFile;
    }

    /**
     * Legt den Wert der voiceMessagingGreetingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileKey }
     *     
     */
    public void setVoiceMessagingGreetingFile(AnnouncementFileKey value) {
        this.voiceMessagingGreetingFile = value;
    }

}
