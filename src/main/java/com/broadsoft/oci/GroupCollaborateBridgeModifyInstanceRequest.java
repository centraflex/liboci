//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a Collaborate bridge.
 *         The request fails when the collaborateOwnerUserIdList is included in the request for the default collaborate bridge.
 *         The request fails when the supportOutdial is included in the request and the system-level collaborate supportOutdial setting is disabled.
 *         The response is either SuccessResponse or ErrorResponse.
 *         
 *         Replaced by: GroupCollaborateBridgeModifyInstanceRequest20sp1
 *       
 * 
 * <p>Java-Klasse für GroupCollaborateBridgeModifyInstanceRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCollaborateBridgeModifyInstanceRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceModifyProfile" minOccurs="0"/>
 *         <element name="maximumBridgeParticipants" type="{}CollaborateBridgeMaximumParticipants" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="maxCollaborateRoomParticipants" type="{}CollaborateRoomMaximumParticipants" minOccurs="0"/>
 *         <element name="supportOutdial" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="collaborateOwnerUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCollaborateBridgeModifyInstanceRequest", propOrder = {
    "serviceUserId",
    "serviceInstanceProfile",
    "maximumBridgeParticipants",
    "networkClassOfService",
    "maxCollaborateRoomParticipants",
    "supportOutdial",
    "collaborateOwnerUserIdList"
})
public class GroupCollaborateBridgeModifyInstanceRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected ServiceInstanceModifyProfile serviceInstanceProfile;
    protected CollaborateBridgeMaximumParticipants maximumBridgeParticipants;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    protected Integer maxCollaborateRoomParticipants;
    protected Boolean supportOutdial;
    @XmlElementRef(name = "collaborateOwnerUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> collaborateOwnerUserIdList;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public ServiceInstanceModifyProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceModifyProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der maximumBridgeParticipants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CollaborateBridgeMaximumParticipants }
     *     
     */
    public CollaborateBridgeMaximumParticipants getMaximumBridgeParticipants() {
        return maximumBridgeParticipants;
    }

    /**
     * Legt den Wert der maximumBridgeParticipants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CollaborateBridgeMaximumParticipants }
     *     
     */
    public void setMaximumBridgeParticipants(CollaborateBridgeMaximumParticipants value) {
        this.maximumBridgeParticipants = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Ruft den Wert der maxCollaborateRoomParticipants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCollaborateRoomParticipants() {
        return maxCollaborateRoomParticipants;
    }

    /**
     * Legt den Wert der maxCollaborateRoomParticipants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCollaborateRoomParticipants(Integer value) {
        this.maxCollaborateRoomParticipants = value;
    }

    /**
     * Ruft den Wert der supportOutdial-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportOutdial() {
        return supportOutdial;
    }

    /**
     * Legt den Wert der supportOutdial-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportOutdial(Boolean value) {
        this.supportOutdial = value;
    }

    /**
     * Ruft den Wert der collaborateOwnerUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getCollaborateOwnerUserIdList() {
        return collaborateOwnerUserIdList;
    }

    /**
     * Legt den Wert der collaborateOwnerUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setCollaborateOwnerUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.collaborateOwnerUserIdList = value;
    }

}
