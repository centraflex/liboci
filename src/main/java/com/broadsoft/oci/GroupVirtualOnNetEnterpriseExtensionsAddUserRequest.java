//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Adds Virtual On-Net users to a Group. It is possible to add 
 *         either: a single user,  or a list of users, or a range of users, 
 *         or any combination thereof.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupVirtualOnNetEnterpriseExtensionsAddUserRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupVirtualOnNetEnterpriseExtensionsAddUserRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="virtualOnNetUser" type="{}VirtualOnNetUser" maxOccurs="100" minOccurs="0"/>
 *         <element name="virtualOnNetUserRange" type="{}VirtualOnNetUserRange" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupVirtualOnNetEnterpriseExtensionsAddUserRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "virtualOnNetUser",
    "virtualOnNetUserRange"
})
public class GroupVirtualOnNetEnterpriseExtensionsAddUserRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected List<VirtualOnNetUser> virtualOnNetUser;
    protected VirtualOnNetUserRange virtualOnNetUserRange;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Gets the value of the virtualOnNetUser property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the virtualOnNetUser property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVirtualOnNetUser().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VirtualOnNetUser }
     * 
     * 
     * @return
     *     The value of the virtualOnNetUser property.
     */
    public List<VirtualOnNetUser> getVirtualOnNetUser() {
        if (virtualOnNetUser == null) {
            virtualOnNetUser = new ArrayList<>();
        }
        return this.virtualOnNetUser;
    }

    /**
     * Ruft den Wert der virtualOnNetUserRange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VirtualOnNetUserRange }
     *     
     */
    public VirtualOnNetUserRange getVirtualOnNetUserRange() {
        return virtualOnNetUserRange;
    }

    /**
     * Legt den Wert der virtualOnNetUserRange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VirtualOnNetUserRange }
     *     
     */
    public void setVirtualOnNetUserRange(VirtualOnNetUserRange value) {
        this.virtualOnNetUserRange = value;
    }

}
