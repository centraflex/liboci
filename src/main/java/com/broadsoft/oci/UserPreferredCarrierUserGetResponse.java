//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to a UserPreferredCarrierUserGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserPreferredCarrierUserGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPreferredCarrierUserGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="intraLataCarrier" type="{}UserPreferredCarrierName"/>
 *         <element name="interLataCarrier" type="{}UserPreferredCarrierName"/>
 *         <element name="internationalCarrier" type="{}UserPreferredCarrierName"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPreferredCarrierUserGetResponse", propOrder = {
    "intraLataCarrier",
    "interLataCarrier",
    "internationalCarrier"
})
public class UserPreferredCarrierUserGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected UserPreferredCarrierName intraLataCarrier;
    @XmlElement(required = true)
    protected UserPreferredCarrierName interLataCarrier;
    @XmlElement(required = true)
    protected UserPreferredCarrierName internationalCarrier;

    /**
     * Ruft den Wert der intraLataCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserPreferredCarrierName }
     *     
     */
    public UserPreferredCarrierName getIntraLataCarrier() {
        return intraLataCarrier;
    }

    /**
     * Legt den Wert der intraLataCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserPreferredCarrierName }
     *     
     */
    public void setIntraLataCarrier(UserPreferredCarrierName value) {
        this.intraLataCarrier = value;
    }

    /**
     * Ruft den Wert der interLataCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserPreferredCarrierName }
     *     
     */
    public UserPreferredCarrierName getInterLataCarrier() {
        return interLataCarrier;
    }

    /**
     * Legt den Wert der interLataCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserPreferredCarrierName }
     *     
     */
    public void setInterLataCarrier(UserPreferredCarrierName value) {
        this.interLataCarrier = value;
    }

    /**
     * Ruft den Wert der internationalCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserPreferredCarrierName }
     *     
     */
    public UserPreferredCarrierName getInternationalCarrier() {
        return internationalCarrier;
    }

    /**
     * Legt den Wert der internationalCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserPreferredCarrierName }
     *     
     */
    public void setInternationalCarrier(UserPreferredCarrierName value) {
        this.internationalCarrier = value;
    }

}
