//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserAlternateNumbersGetRequest21.
 *       
 * 
 * <p>Java-Klasse für UserAlternateNumbersGetResponse21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserAlternateNumbersGetResponse21">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="distinctiveRing" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="alternateEntry01" type="{}AlternateNumberEntry21" minOccurs="0"/>
 *         <element name="alternateEntry02" type="{}AlternateNumberEntry21" minOccurs="0"/>
 *         <element name="alternateEntry03" type="{}AlternateNumberEntry21" minOccurs="0"/>
 *         <element name="alternateEntry04" type="{}AlternateNumberEntry21" minOccurs="0"/>
 *         <element name="alternateEntry05" type="{}AlternateNumberEntry21" minOccurs="0"/>
 *         <element name="alternateEntry06" type="{}AlternateNumberEntry21" minOccurs="0"/>
 *         <element name="alternateEntry07" type="{}AlternateNumberEntry21" minOccurs="0"/>
 *         <element name="alternateEntry08" type="{}AlternateNumberEntry21" minOccurs="0"/>
 *         <element name="alternateEntry09" type="{}AlternateNumberEntry21" minOccurs="0"/>
 *         <element name="alternateEntry10" type="{}AlternateNumberEntry21" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserAlternateNumbersGetResponse21", propOrder = {
    "distinctiveRing",
    "alternateEntry01",
    "alternateEntry02",
    "alternateEntry03",
    "alternateEntry04",
    "alternateEntry05",
    "alternateEntry06",
    "alternateEntry07",
    "alternateEntry08",
    "alternateEntry09",
    "alternateEntry10"
})
public class UserAlternateNumbersGetResponse21
    extends OCIDataResponse
{

    protected boolean distinctiveRing;
    protected AlternateNumberEntry21 alternateEntry01;
    protected AlternateNumberEntry21 alternateEntry02;
    protected AlternateNumberEntry21 alternateEntry03;
    protected AlternateNumberEntry21 alternateEntry04;
    protected AlternateNumberEntry21 alternateEntry05;
    protected AlternateNumberEntry21 alternateEntry06;
    protected AlternateNumberEntry21 alternateEntry07;
    protected AlternateNumberEntry21 alternateEntry08;
    protected AlternateNumberEntry21 alternateEntry09;
    protected AlternateNumberEntry21 alternateEntry10;

    /**
     * Ruft den Wert der distinctiveRing-Eigenschaft ab.
     * 
     */
    public boolean isDistinctiveRing() {
        return distinctiveRing;
    }

    /**
     * Legt den Wert der distinctiveRing-Eigenschaft fest.
     * 
     */
    public void setDistinctiveRing(boolean value) {
        this.distinctiveRing = value;
    }

    /**
     * Ruft den Wert der alternateEntry01-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public AlternateNumberEntry21 getAlternateEntry01() {
        return alternateEntry01;
    }

    /**
     * Legt den Wert der alternateEntry01-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public void setAlternateEntry01(AlternateNumberEntry21 value) {
        this.alternateEntry01 = value;
    }

    /**
     * Ruft den Wert der alternateEntry02-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public AlternateNumberEntry21 getAlternateEntry02() {
        return alternateEntry02;
    }

    /**
     * Legt den Wert der alternateEntry02-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public void setAlternateEntry02(AlternateNumberEntry21 value) {
        this.alternateEntry02 = value;
    }

    /**
     * Ruft den Wert der alternateEntry03-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public AlternateNumberEntry21 getAlternateEntry03() {
        return alternateEntry03;
    }

    /**
     * Legt den Wert der alternateEntry03-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public void setAlternateEntry03(AlternateNumberEntry21 value) {
        this.alternateEntry03 = value;
    }

    /**
     * Ruft den Wert der alternateEntry04-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public AlternateNumberEntry21 getAlternateEntry04() {
        return alternateEntry04;
    }

    /**
     * Legt den Wert der alternateEntry04-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public void setAlternateEntry04(AlternateNumberEntry21 value) {
        this.alternateEntry04 = value;
    }

    /**
     * Ruft den Wert der alternateEntry05-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public AlternateNumberEntry21 getAlternateEntry05() {
        return alternateEntry05;
    }

    /**
     * Legt den Wert der alternateEntry05-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public void setAlternateEntry05(AlternateNumberEntry21 value) {
        this.alternateEntry05 = value;
    }

    /**
     * Ruft den Wert der alternateEntry06-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public AlternateNumberEntry21 getAlternateEntry06() {
        return alternateEntry06;
    }

    /**
     * Legt den Wert der alternateEntry06-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public void setAlternateEntry06(AlternateNumberEntry21 value) {
        this.alternateEntry06 = value;
    }

    /**
     * Ruft den Wert der alternateEntry07-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public AlternateNumberEntry21 getAlternateEntry07() {
        return alternateEntry07;
    }

    /**
     * Legt den Wert der alternateEntry07-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public void setAlternateEntry07(AlternateNumberEntry21 value) {
        this.alternateEntry07 = value;
    }

    /**
     * Ruft den Wert der alternateEntry08-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public AlternateNumberEntry21 getAlternateEntry08() {
        return alternateEntry08;
    }

    /**
     * Legt den Wert der alternateEntry08-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public void setAlternateEntry08(AlternateNumberEntry21 value) {
        this.alternateEntry08 = value;
    }

    /**
     * Ruft den Wert der alternateEntry09-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public AlternateNumberEntry21 getAlternateEntry09() {
        return alternateEntry09;
    }

    /**
     * Legt den Wert der alternateEntry09-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public void setAlternateEntry09(AlternateNumberEntry21 value) {
        this.alternateEntry09 = value;
    }

    /**
     * Ruft den Wert der alternateEntry10-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public AlternateNumberEntry21 getAlternateEntry10() {
        return alternateEntry10;
    }

    /**
     * Legt den Wert der alternateEntry10-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlternateNumberEntry21 }
     *     
     */
    public void setAlternateEntry10(AlternateNumberEntry21 value) {
        this.alternateEntry10 = value;
    }

}
