//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the user's voice messaging greeting.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         Engineering Note: This command is used internally by Call Processing.
 *         The following elements are only used in AS data mode:
 *           disableMessageDeposit
 *           disableMessageDepositAction
 *           greetingOnlyForwardDestination              
 *       
 * 
 * <p>Java-Klasse für UserVoiceMessagingUserModifyGreetingRequest20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserVoiceMessagingUserModifyGreetingRequest20">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="busyAnnouncementSelection" type="{}AnnouncementSelection" minOccurs="0"/>
 *         <element name="busyPersonalAudioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="busyPersonalVideoFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="noAnswerAnnouncementSelection" type="{}VoiceMessagingNoAnswerGreetingSelection" minOccurs="0"/>
 *         <element name="noAnswerPersonalAudioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="noAnswerPersonalVideoFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="noAnswerAlternateGreeting01" type="{}VoiceMessagingAlternateNoAnswerGreetingModify20" minOccurs="0"/>
 *         <element name="noAnswerAlternateGreeting02" type="{}VoiceMessagingAlternateNoAnswerGreetingModify20" minOccurs="0"/>
 *         <element name="noAnswerAlternateGreeting03" type="{}VoiceMessagingAlternateNoAnswerGreetingModify20" minOccurs="0"/>
 *         <element name="extendedAwayEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="extendedAwayDisableMessageDeposit" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="extendedAwayAudioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="extendedAwayVideoFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="noAnswerNumberOfRings" type="{}VoiceMessagingNumberOfRings" minOccurs="0"/>
 *         <element name="disableMessageDeposit" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disableMessageDepositAction" type="{}VoiceMessagingDisableMessageDepositSelection" minOccurs="0"/>
 *         <element name="greetingOnlyForwardDestination" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserVoiceMessagingUserModifyGreetingRequest20", propOrder = {
    "userId",
    "busyAnnouncementSelection",
    "busyPersonalAudioFile",
    "busyPersonalVideoFile",
    "noAnswerAnnouncementSelection",
    "noAnswerPersonalAudioFile",
    "noAnswerPersonalVideoFile",
    "noAnswerAlternateGreeting01",
    "noAnswerAlternateGreeting02",
    "noAnswerAlternateGreeting03",
    "extendedAwayEnabled",
    "extendedAwayDisableMessageDeposit",
    "extendedAwayAudioFile",
    "extendedAwayVideoFile",
    "noAnswerNumberOfRings",
    "disableMessageDeposit",
    "disableMessageDepositAction",
    "greetingOnlyForwardDestination"
})
public class UserVoiceMessagingUserModifyGreetingRequest20
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlSchemaType(name = "token")
    protected AnnouncementSelection busyAnnouncementSelection;
    @XmlElementRef(name = "busyPersonalAudioFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> busyPersonalAudioFile;
    @XmlElementRef(name = "busyPersonalVideoFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> busyPersonalVideoFile;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingNoAnswerGreetingSelection noAnswerAnnouncementSelection;
    @XmlElementRef(name = "noAnswerPersonalAudioFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> noAnswerPersonalAudioFile;
    @XmlElementRef(name = "noAnswerPersonalVideoFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> noAnswerPersonalVideoFile;
    protected VoiceMessagingAlternateNoAnswerGreetingModify20 noAnswerAlternateGreeting01;
    protected VoiceMessagingAlternateNoAnswerGreetingModify20 noAnswerAlternateGreeting02;
    protected VoiceMessagingAlternateNoAnswerGreetingModify20 noAnswerAlternateGreeting03;
    protected Boolean extendedAwayEnabled;
    protected Boolean extendedAwayDisableMessageDeposit;
    @XmlElementRef(name = "extendedAwayAudioFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> extendedAwayAudioFile;
    @XmlElementRef(name = "extendedAwayVideoFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> extendedAwayVideoFile;
    protected Integer noAnswerNumberOfRings;
    protected Boolean disableMessageDeposit;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingDisableMessageDepositSelection disableMessageDepositAction;
    @XmlElementRef(name = "greetingOnlyForwardDestination", type = JAXBElement.class, required = false)
    protected JAXBElement<String> greetingOnlyForwardDestination;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der busyAnnouncementSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementSelection }
     *     
     */
    public AnnouncementSelection getBusyAnnouncementSelection() {
        return busyAnnouncementSelection;
    }

    /**
     * Legt den Wert der busyAnnouncementSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementSelection }
     *     
     */
    public void setBusyAnnouncementSelection(AnnouncementSelection value) {
        this.busyAnnouncementSelection = value;
    }

    /**
     * Ruft den Wert der busyPersonalAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getBusyPersonalAudioFile() {
        return busyPersonalAudioFile;
    }

    /**
     * Legt den Wert der busyPersonalAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setBusyPersonalAudioFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.busyPersonalAudioFile = value;
    }

    /**
     * Ruft den Wert der busyPersonalVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getBusyPersonalVideoFile() {
        return busyPersonalVideoFile;
    }

    /**
     * Legt den Wert der busyPersonalVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setBusyPersonalVideoFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.busyPersonalVideoFile = value;
    }

    /**
     * Ruft den Wert der noAnswerAnnouncementSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingNoAnswerGreetingSelection }
     *     
     */
    public VoiceMessagingNoAnswerGreetingSelection getNoAnswerAnnouncementSelection() {
        return noAnswerAnnouncementSelection;
    }

    /**
     * Legt den Wert der noAnswerAnnouncementSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingNoAnswerGreetingSelection }
     *     
     */
    public void setNoAnswerAnnouncementSelection(VoiceMessagingNoAnswerGreetingSelection value) {
        this.noAnswerAnnouncementSelection = value;
    }

    /**
     * Ruft den Wert der noAnswerPersonalAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getNoAnswerPersonalAudioFile() {
        return noAnswerPersonalAudioFile;
    }

    /**
     * Legt den Wert der noAnswerPersonalAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setNoAnswerPersonalAudioFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.noAnswerPersonalAudioFile = value;
    }

    /**
     * Ruft den Wert der noAnswerPersonalVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getNoAnswerPersonalVideoFile() {
        return noAnswerPersonalVideoFile;
    }

    /**
     * Legt den Wert der noAnswerPersonalVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setNoAnswerPersonalVideoFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.noAnswerPersonalVideoFile = value;
    }

    /**
     * Ruft den Wert der noAnswerAlternateGreeting01-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingModify20 }
     *     
     */
    public VoiceMessagingAlternateNoAnswerGreetingModify20 getNoAnswerAlternateGreeting01() {
        return noAnswerAlternateGreeting01;
    }

    /**
     * Legt den Wert der noAnswerAlternateGreeting01-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingModify20 }
     *     
     */
    public void setNoAnswerAlternateGreeting01(VoiceMessagingAlternateNoAnswerGreetingModify20 value) {
        this.noAnswerAlternateGreeting01 = value;
    }

    /**
     * Ruft den Wert der noAnswerAlternateGreeting02-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingModify20 }
     *     
     */
    public VoiceMessagingAlternateNoAnswerGreetingModify20 getNoAnswerAlternateGreeting02() {
        return noAnswerAlternateGreeting02;
    }

    /**
     * Legt den Wert der noAnswerAlternateGreeting02-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingModify20 }
     *     
     */
    public void setNoAnswerAlternateGreeting02(VoiceMessagingAlternateNoAnswerGreetingModify20 value) {
        this.noAnswerAlternateGreeting02 = value;
    }

    /**
     * Ruft den Wert der noAnswerAlternateGreeting03-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingModify20 }
     *     
     */
    public VoiceMessagingAlternateNoAnswerGreetingModify20 getNoAnswerAlternateGreeting03() {
        return noAnswerAlternateGreeting03;
    }

    /**
     * Legt den Wert der noAnswerAlternateGreeting03-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingModify20 }
     *     
     */
    public void setNoAnswerAlternateGreeting03(VoiceMessagingAlternateNoAnswerGreetingModify20 value) {
        this.noAnswerAlternateGreeting03 = value;
    }

    /**
     * Ruft den Wert der extendedAwayEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExtendedAwayEnabled() {
        return extendedAwayEnabled;
    }

    /**
     * Legt den Wert der extendedAwayEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExtendedAwayEnabled(Boolean value) {
        this.extendedAwayEnabled = value;
    }

    /**
     * Ruft den Wert der extendedAwayDisableMessageDeposit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExtendedAwayDisableMessageDeposit() {
        return extendedAwayDisableMessageDeposit;
    }

    /**
     * Legt den Wert der extendedAwayDisableMessageDeposit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExtendedAwayDisableMessageDeposit(Boolean value) {
        this.extendedAwayDisableMessageDeposit = value;
    }

    /**
     * Ruft den Wert der extendedAwayAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getExtendedAwayAudioFile() {
        return extendedAwayAudioFile;
    }

    /**
     * Legt den Wert der extendedAwayAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setExtendedAwayAudioFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.extendedAwayAudioFile = value;
    }

    /**
     * Ruft den Wert der extendedAwayVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getExtendedAwayVideoFile() {
        return extendedAwayVideoFile;
    }

    /**
     * Legt den Wert der extendedAwayVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setExtendedAwayVideoFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.extendedAwayVideoFile = value;
    }

    /**
     * Ruft den Wert der noAnswerNumberOfRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNoAnswerNumberOfRings() {
        return noAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der noAnswerNumberOfRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNoAnswerNumberOfRings(Integer value) {
        this.noAnswerNumberOfRings = value;
    }

    /**
     * Ruft den Wert der disableMessageDeposit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisableMessageDeposit() {
        return disableMessageDeposit;
    }

    /**
     * Legt den Wert der disableMessageDeposit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisableMessageDeposit(Boolean value) {
        this.disableMessageDeposit = value;
    }

    /**
     * Ruft den Wert der disableMessageDepositAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingDisableMessageDepositSelection }
     *     
     */
    public VoiceMessagingDisableMessageDepositSelection getDisableMessageDepositAction() {
        return disableMessageDepositAction;
    }

    /**
     * Legt den Wert der disableMessageDepositAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingDisableMessageDepositSelection }
     *     
     */
    public void setDisableMessageDepositAction(VoiceMessagingDisableMessageDepositSelection value) {
        this.disableMessageDepositAction = value;
    }

    /**
     * Ruft den Wert der greetingOnlyForwardDestination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGreetingOnlyForwardDestination() {
        return greetingOnlyForwardDestination;
    }

    /**
     * Legt den Wert der greetingOnlyForwardDestination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGreetingOnlyForwardDestination(JAXBElement<String> value) {
        this.greetingOnlyForwardDestination = value;
    }

}
