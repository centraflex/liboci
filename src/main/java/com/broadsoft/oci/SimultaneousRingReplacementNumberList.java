//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         A list of Simultaneous Ring numbers that replaces a previously configured list.
 *         By convention, an element of this type may be set nill to clear the list.
 *       
 * 
 * <p>Java-Klasse für SimultaneousRingReplacementNumberList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SimultaneousRingReplacementNumberList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="simultaneousRingNumber" type="{}SimultaneousRingNumber" maxOccurs="10"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimultaneousRingReplacementNumberList", propOrder = {
    "simultaneousRingNumber"
})
public class SimultaneousRingReplacementNumberList {

    @XmlElement(required = true)
    protected List<SimultaneousRingNumber> simultaneousRingNumber;

    /**
     * Gets the value of the simultaneousRingNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the simultaneousRingNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSimultaneousRingNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimultaneousRingNumber }
     * 
     * 
     * @return
     *     The value of the simultaneousRingNumber property.
     */
    public List<SimultaneousRingNumber> getSimultaneousRingNumber() {
        if (simultaneousRingNumber == null) {
            simultaneousRingNumber = new ArrayList<>();
        }
        return this.simultaneousRingNumber;
    }

}
