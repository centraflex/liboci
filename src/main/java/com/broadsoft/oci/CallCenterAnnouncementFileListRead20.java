//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains a list of announcement repository files
 *         
 * 
 * <p>Java-Klasse für CallCenterAnnouncementFileListRead20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterAnnouncementFileListRead20">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="file1" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="file2" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="file3" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="file4" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterAnnouncementFileListRead20", propOrder = {
    "file1",
    "file2",
    "file3",
    "file4"
})
public class CallCenterAnnouncementFileListRead20 {

    protected AnnouncementFileLevelKey file1;
    protected AnnouncementFileLevelKey file2;
    protected AnnouncementFileLevelKey file3;
    protected AnnouncementFileLevelKey file4;

    /**
     * Ruft den Wert der file1-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getFile1() {
        return file1;
    }

    /**
     * Legt den Wert der file1-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setFile1(AnnouncementFileLevelKey value) {
        this.file1 = value;
    }

    /**
     * Ruft den Wert der file2-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getFile2() {
        return file2;
    }

    /**
     * Legt den Wert der file2-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setFile2(AnnouncementFileLevelKey value) {
        this.file2 = value;
    }

    /**
     * Ruft den Wert der file3-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getFile3() {
        return file3;
    }

    /**
     * Legt den Wert der file3-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setFile3(AnnouncementFileLevelKey value) {
        this.file3 = value;
    }

    /**
     * Ruft den Wert der file4-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getFile4() {
        return file4;
    }

    /**
     * Legt den Wert der file4-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setFile4(AnnouncementFileLevelKey value) {
        this.file4 = value;
    }

}
