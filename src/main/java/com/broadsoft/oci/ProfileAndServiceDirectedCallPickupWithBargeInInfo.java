//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         	This is the configuration parameters for Directed Call Pickup With Barge In service
 *         	
 * 
 * <p>Java-Klasse für ProfileAndServiceDirectedCallPickupWithBargeInInfo complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ProfileAndServiceDirectedCallPickupWithBargeInInfo">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="enableBargeInWarningTone" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableAutomaticTargetSelection" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfileAndServiceDirectedCallPickupWithBargeInInfo", propOrder = {
    "enableBargeInWarningTone",
    "enableAutomaticTargetSelection"
})
public class ProfileAndServiceDirectedCallPickupWithBargeInInfo {

    protected boolean enableBargeInWarningTone;
    protected boolean enableAutomaticTargetSelection;

    /**
     * Ruft den Wert der enableBargeInWarningTone-Eigenschaft ab.
     * 
     */
    public boolean isEnableBargeInWarningTone() {
        return enableBargeInWarningTone;
    }

    /**
     * Legt den Wert der enableBargeInWarningTone-Eigenschaft fest.
     * 
     */
    public void setEnableBargeInWarningTone(boolean value) {
        this.enableBargeInWarningTone = value;
    }

    /**
     * Ruft den Wert der enableAutomaticTargetSelection-Eigenschaft ab.
     * 
     */
    public boolean isEnableAutomaticTargetSelection() {
        return enableAutomaticTargetSelection;
    }

    /**
     * Legt den Wert der enableAutomaticTargetSelection-Eigenschaft fest.
     * 
     */
    public void setEnableAutomaticTargetSelection(boolean value) {
        this.enableAutomaticTargetSelection = value;
    }

}
