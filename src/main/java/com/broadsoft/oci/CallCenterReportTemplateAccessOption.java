//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterReportTemplateAccessOption.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterReportTemplateAccessOption">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Supervisor Only"/>
 *     <enumeration value="Supervisor and Agent"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterReportTemplateAccessOption")
@XmlEnum
public enum CallCenterReportTemplateAccessOption {

    @XmlEnumValue("Supervisor Only")
    SUPERVISOR_ONLY("Supervisor Only"),
    @XmlEnumValue("Supervisor and Agent")
    SUPERVISOR_AND_AGENT("Supervisor and Agent");
    private final String value;

    CallCenterReportTemplateAccessOption(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterReportTemplateAccessOption fromValue(String v) {
        for (CallCenterReportTemplateAccessOption c: CallCenterReportTemplateAccessOption.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
