//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains Call Center queue status and a table with column headings: 
 *         "User Id", "First Name", "Last Name", "Phone Number", "Extension", "Department", "Email Address".
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterGetInstanceQueueStatusResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterGetInstanceQueueStatusResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="numberOfCallsQueuedNow" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="agentsCurrentlyStaffed" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterGetInstanceQueueStatusResponse", propOrder = {
    "numberOfCallsQueuedNow",
    "agentsCurrentlyStaffed"
})
public class GroupCallCenterGetInstanceQueueStatusResponse
    extends OCIDataResponse
{

    protected int numberOfCallsQueuedNow;
    @XmlElement(required = true)
    protected OCITable agentsCurrentlyStaffed;

    /**
     * Ruft den Wert der numberOfCallsQueuedNow-Eigenschaft ab.
     * 
     */
    public int getNumberOfCallsQueuedNow() {
        return numberOfCallsQueuedNow;
    }

    /**
     * Legt den Wert der numberOfCallsQueuedNow-Eigenschaft fest.
     * 
     */
    public void setNumberOfCallsQueuedNow(int value) {
        this.numberOfCallsQueuedNow = value;
    }

    /**
     * Ruft den Wert der agentsCurrentlyStaffed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAgentsCurrentlyStaffed() {
        return agentsCurrentlyStaffed;
    }

    /**
     * Legt den Wert der agentsCurrentlyStaffed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAgentsCurrentlyStaffed(OCITable value) {
        this.agentsCurrentlyStaffed = value;
    }

}
