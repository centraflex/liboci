//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the filtering setting for an executive user.
 *         Both executive and the executive assistant can run this command.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserExecutiveModifyFilteringRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserExecutiveModifyFilteringRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="enableFiltering" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="filteringMode" type="{}ExecutiveCallFilteringMode" minOccurs="0"/>
 *         <element name="simpleFilterType" type="{}ExecutiveCallFilteringSimpleFilterType" minOccurs="0"/>
 *         <element name="criteriaActivation" type="{}CriteriaActivation" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserExecutiveModifyFilteringRequest", propOrder = {
    "userId",
    "enableFiltering",
    "filteringMode",
    "simpleFilterType",
    "criteriaActivation"
})
public class UserExecutiveModifyFilteringRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean enableFiltering;
    @XmlSchemaType(name = "token")
    protected ExecutiveCallFilteringMode filteringMode;
    @XmlSchemaType(name = "token")
    protected ExecutiveCallFilteringSimpleFilterType simpleFilterType;
    protected List<CriteriaActivation> criteriaActivation;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der enableFiltering-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableFiltering() {
        return enableFiltering;
    }

    /**
     * Legt den Wert der enableFiltering-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableFiltering(Boolean value) {
        this.enableFiltering = value;
    }

    /**
     * Ruft den Wert der filteringMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveCallFilteringMode }
     *     
     */
    public ExecutiveCallFilteringMode getFilteringMode() {
        return filteringMode;
    }

    /**
     * Legt den Wert der filteringMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveCallFilteringMode }
     *     
     */
    public void setFilteringMode(ExecutiveCallFilteringMode value) {
        this.filteringMode = value;
    }

    /**
     * Ruft den Wert der simpleFilterType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveCallFilteringSimpleFilterType }
     *     
     */
    public ExecutiveCallFilteringSimpleFilterType getSimpleFilterType() {
        return simpleFilterType;
    }

    /**
     * Legt den Wert der simpleFilterType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveCallFilteringSimpleFilterType }
     *     
     */
    public void setSimpleFilterType(ExecutiveCallFilteringSimpleFilterType value) {
        this.simpleFilterType = value;
    }

    /**
     * Gets the value of the criteriaActivation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the criteriaActivation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCriteriaActivation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CriteriaActivation }
     * 
     * 
     * @return
     *     The value of the criteriaActivation property.
     */
    public List<CriteriaActivation> getCriteriaActivation() {
        if (criteriaActivation == null) {
            criteriaActivation = new ArrayList<>();
        }
        return this.criteriaActivation;
    }

}
