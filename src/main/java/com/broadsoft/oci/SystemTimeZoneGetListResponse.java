//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemTimeZoneGetListRequest.
 *         Contains a 2 column table with column headings 'Key' and 'Display Name' and a row
 *         for each time zone.
 *         
 *         Replaced by: SystemTimeZoneGetListResponse20 in AS data mode
 *       
 * 
 * <p>Java-Klasse für SystemTimeZoneGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemTimeZoneGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="timeZoneTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemTimeZoneGetListResponse", propOrder = {
    "timeZoneTable"
})
public class SystemTimeZoneGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable timeZoneTable;

    /**
     * Ruft den Wert der timeZoneTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getTimeZoneTable() {
        return timeZoneTable;
    }

    /**
     * Legt den Wert der timeZoneTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setTimeZoneTable(OCITable value) {
        this.timeZoneTable = value;
    }

}
