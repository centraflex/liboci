//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderVoiceMessagingGroupGetVoicePortalRequest.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderVoiceMessagingGroupGetVoicePortalResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderVoiceMessagingGroupGetVoicePortalResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="voicePortalScope" type="{}ServiceProviderVoicePortalScope"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderVoiceMessagingGroupGetVoicePortalResponse", propOrder = {
    "voicePortalScope"
})
public class ServiceProviderVoiceMessagingGroupGetVoicePortalResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderVoicePortalScope voicePortalScope;

    /**
     * Ruft den Wert der voicePortalScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderVoicePortalScope }
     *     
     */
    public ServiceProviderVoicePortalScope getVoicePortalScope() {
        return voicePortalScope;
    }

    /**
     * Legt den Wert der voicePortalScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderVoicePortalScope }
     *     
     */
    public void setVoicePortalScope(ServiceProviderVoicePortalScope value) {
        this.voicePortalScope = value;
    }

}
