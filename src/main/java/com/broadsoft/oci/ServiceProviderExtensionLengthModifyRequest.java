//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the service provider's extension length range.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderExtensionLengthModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderExtensionLengthModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="defaultExtensionLength" type="{}ExtensionLength" minOccurs="0"/>
 *         <element name="locationRoutingPrefixDigit" type="{}LocationRoutingPrefixDigit" minOccurs="0"/>
 *         <element name="locationCodeLength" type="{}EnterpriseLocationDialingCodeLength" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderExtensionLengthModifyRequest", propOrder = {
    "serviceProviderId",
    "defaultExtensionLength",
    "locationRoutingPrefixDigit",
    "locationCodeLength"
})
public class ServiceProviderExtensionLengthModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElementRef(name = "defaultExtensionLength", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> defaultExtensionLength;
    @XmlElementRef(name = "locationRoutingPrefixDigit", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> locationRoutingPrefixDigit;
    @XmlElementRef(name = "locationCodeLength", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> locationCodeLength;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der defaultExtensionLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getDefaultExtensionLength() {
        return defaultExtensionLength;
    }

    /**
     * Legt den Wert der defaultExtensionLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setDefaultExtensionLength(JAXBElement<Integer> value) {
        this.defaultExtensionLength = value;
    }

    /**
     * Ruft den Wert der locationRoutingPrefixDigit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getLocationRoutingPrefixDigit() {
        return locationRoutingPrefixDigit;
    }

    /**
     * Legt den Wert der locationRoutingPrefixDigit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setLocationRoutingPrefixDigit(JAXBElement<Integer> value) {
        this.locationRoutingPrefixDigit = value;
    }

    /**
     * Ruft den Wert der locationCodeLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getLocationCodeLength() {
        return locationCodeLength;
    }

    /**
     * Legt den Wert der locationCodeLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setLocationCodeLength(JAXBElement<Integer> value) {
        this.locationCodeLength = value;
    }

}
