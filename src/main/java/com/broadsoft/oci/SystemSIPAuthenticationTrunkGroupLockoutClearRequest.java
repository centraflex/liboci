//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *          Request to clear sip authentication trunk group lockouts in the system.
 *          The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemSIPAuthenticationTrunkGroupLockoutClearRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPAuthenticationTrunkGroupLockoutClearRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="trunkGroupkey" type="{}TrunkGroupKey" maxOccurs="unbounded"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPAuthenticationTrunkGroupLockoutClearRequest", propOrder = {
    "trunkGroupkey"
})
public class SystemSIPAuthenticationTrunkGroupLockoutClearRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    protected List<TrunkGroupKey> trunkGroupkey;

    /**
     * Gets the value of the trunkGroupkey property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the trunkGroupkey property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTrunkGroupkey().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TrunkGroupKey }
     * 
     * 
     * @return
     *     The value of the trunkGroupkey property.
     */
    public List<TrunkGroupKey> getTrunkGroupkey() {
        if (trunkGroupkey == null) {
            trunkGroupkey = new ArrayList<>();
        }
        return this.trunkGroupkey;
    }

}
