//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *          Modify outgoing Calling Plan for Call Me Now call permissions.
 *        
 * 
 * <p>Java-Klasse für OutgoingCallingPlanCallMeNowPermissionsModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OutgoingCallingPlanCallMeNowPermissionsModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="group" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="local" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="tollFree" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="toll" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="international" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="operatorAssisted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="chargeableDirectoryAssisted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="specialServicesI" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="specialServicesII" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="premiumServicesI" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="premiumServicesII" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="casual" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="urlDialing" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="unknown" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingCallingPlanCallMeNowPermissionsModify", propOrder = {
    "group",
    "local",
    "tollFree",
    "toll",
    "international",
    "operatorAssisted",
    "chargeableDirectoryAssisted",
    "specialServicesI",
    "specialServicesII",
    "premiumServicesI",
    "premiumServicesII",
    "casual",
    "urlDialing",
    "unknown"
})
public class OutgoingCallingPlanCallMeNowPermissionsModify {

    protected Boolean group;
    protected Boolean local;
    protected Boolean tollFree;
    protected Boolean toll;
    protected Boolean international;
    protected Boolean operatorAssisted;
    protected Boolean chargeableDirectoryAssisted;
    protected Boolean specialServicesI;
    protected Boolean specialServicesII;
    protected Boolean premiumServicesI;
    protected Boolean premiumServicesII;
    protected Boolean casual;
    protected Boolean urlDialing;
    protected Boolean unknown;

    /**
     * Ruft den Wert der group-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGroup() {
        return group;
    }

    /**
     * Legt den Wert der group-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGroup(Boolean value) {
        this.group = value;
    }

    /**
     * Ruft den Wert der local-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLocal() {
        return local;
    }

    /**
     * Legt den Wert der local-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLocal(Boolean value) {
        this.local = value;
    }

    /**
     * Ruft den Wert der tollFree-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTollFree() {
        return tollFree;
    }

    /**
     * Legt den Wert der tollFree-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTollFree(Boolean value) {
        this.tollFree = value;
    }

    /**
     * Ruft den Wert der toll-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isToll() {
        return toll;
    }

    /**
     * Legt den Wert der toll-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setToll(Boolean value) {
        this.toll = value;
    }

    /**
     * Ruft den Wert der international-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInternational() {
        return international;
    }

    /**
     * Legt den Wert der international-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInternational(Boolean value) {
        this.international = value;
    }

    /**
     * Ruft den Wert der operatorAssisted-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOperatorAssisted() {
        return operatorAssisted;
    }

    /**
     * Legt den Wert der operatorAssisted-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOperatorAssisted(Boolean value) {
        this.operatorAssisted = value;
    }

    /**
     * Ruft den Wert der chargeableDirectoryAssisted-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isChargeableDirectoryAssisted() {
        return chargeableDirectoryAssisted;
    }

    /**
     * Legt den Wert der chargeableDirectoryAssisted-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChargeableDirectoryAssisted(Boolean value) {
        this.chargeableDirectoryAssisted = value;
    }

    /**
     * Ruft den Wert der specialServicesI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSpecialServicesI() {
        return specialServicesI;
    }

    /**
     * Legt den Wert der specialServicesI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSpecialServicesI(Boolean value) {
        this.specialServicesI = value;
    }

    /**
     * Ruft den Wert der specialServicesII-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSpecialServicesII() {
        return specialServicesII;
    }

    /**
     * Legt den Wert der specialServicesII-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSpecialServicesII(Boolean value) {
        this.specialServicesII = value;
    }

    /**
     * Ruft den Wert der premiumServicesI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPremiumServicesI() {
        return premiumServicesI;
    }

    /**
     * Legt den Wert der premiumServicesI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPremiumServicesI(Boolean value) {
        this.premiumServicesI = value;
    }

    /**
     * Ruft den Wert der premiumServicesII-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPremiumServicesII() {
        return premiumServicesII;
    }

    /**
     * Legt den Wert der premiumServicesII-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPremiumServicesII(Boolean value) {
        this.premiumServicesII = value;
    }

    /**
     * Ruft den Wert der casual-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCasual() {
        return casual;
    }

    /**
     * Legt den Wert der casual-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCasual(Boolean value) {
        this.casual = value;
    }

    /**
     * Ruft den Wert der urlDialing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUrlDialing() {
        return urlDialing;
    }

    /**
     * Legt den Wert der urlDialing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUrlDialing(Boolean value) {
        this.urlDialing = value;
    }

    /**
     * Ruft den Wert der unknown-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUnknown() {
        return unknown;
    }

    /**
     * Legt den Wert der unknown-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnknown(Boolean value) {
        this.unknown = value;
    }

}
