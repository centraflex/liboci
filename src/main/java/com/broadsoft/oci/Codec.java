//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für Codec.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="Codec">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Clear"/>
 *     <enumeration value="G711alaw"/>
 *     <enumeration value="G711ulaw"/>
 *     <enumeration value="G722"/>
 *     <enumeration value="G723-53"/>
 *     <enumeration value="G723-63"/>
 *     <enumeration value="G723A-53"/>
 *     <enumeration value="G723A-63"/>
 *     <enumeration value="G726-16"/>
 *     <enumeration value="G726-24"/>
 *     <enumeration value="G726-32"/>
 *     <enumeration value="G726-40"/>
 *     <enumeration value="G728"/>
 *     <enumeration value="G729-8"/>
 *     <enumeration value="G729B-8"/>
 *     <enumeration value="GSMEFR"/>
 *     <enumeration value="GSMFR"/>
 *     <enumeration value="AMR-WB"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "Codec")
@XmlEnum
public enum Codec {

    @XmlEnumValue("Clear")
    CLEAR("Clear"),
    @XmlEnumValue("G711alaw")
    G_711_ALAW("G711alaw"),
    @XmlEnumValue("G711ulaw")
    G_711_ULAW("G711ulaw"),
    @XmlEnumValue("G722")
    G_722("G722"),
    @XmlEnumValue("G723-53")
    G_723_53("G723-53"),
    @XmlEnumValue("G723-63")
    G_723_63("G723-63"),
    @XmlEnumValue("G723A-53")
    G_723_A_53("G723A-53"),
    @XmlEnumValue("G723A-63")
    G_723_A_63("G723A-63"),
    @XmlEnumValue("G726-16")
    G_726_16("G726-16"),
    @XmlEnumValue("G726-24")
    G_726_24("G726-24"),
    @XmlEnumValue("G726-32")
    G_726_32("G726-32"),
    @XmlEnumValue("G726-40")
    G_726_40("G726-40"),
    @XmlEnumValue("G728")
    G_728("G728"),
    @XmlEnumValue("G729-8")
    G_729_8("G729-8"),
    @XmlEnumValue("G729B-8")
    G_729_B_8("G729B-8"),
    GSMEFR("GSMEFR"),
    GSMFR("GSMFR"),
    @XmlEnumValue("AMR-WB")
    AMR_WB("AMR-WB");
    private final String value;

    Codec(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Codec fromValue(String v) {
        for (Codec c: Codec.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
