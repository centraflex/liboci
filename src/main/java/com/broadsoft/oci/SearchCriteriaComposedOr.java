//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Abstract base type for specifying search criteria where the search criteria params are OR'ed. 
 *         A search criteria is an optional element
 *         used to restrict the number of rows returned when requesting a potentially large set of
 *         data from the provisioning server.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaComposedOr complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaComposedOr">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaComposedOr")
@XmlSeeAlso({
    SearchCriteriaComposedOrDnExtension.class,
    SearchCriteriaComposedOrUserName.class
})
public abstract class SearchCriteriaComposedOr {


}
