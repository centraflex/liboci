//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         User can either use it's group's preferred carrier or use it's own.
 *         The user carrier name is exposed if it was previously configured.
 *       
 * 
 * <p>Java-Klasse für UserPreferredCarrierName complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPreferredCarrierName">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="useGroupPreferredCarrier" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="carrier" type="{}PreferredCarrierName" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPreferredCarrierName", propOrder = {
    "useGroupPreferredCarrier",
    "carrier"
})
public class UserPreferredCarrierName {

    protected boolean useGroupPreferredCarrier;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String carrier;

    /**
     * Ruft den Wert der useGroupPreferredCarrier-Eigenschaft ab.
     * 
     */
    public boolean isUseGroupPreferredCarrier() {
        return useGroupPreferredCarrier;
    }

    /**
     * Legt den Wert der useGroupPreferredCarrier-Eigenschaft fest.
     * 
     */
    public void setUseGroupPreferredCarrier(boolean value) {
        this.useGroupPreferredCarrier = value;
    }

    /**
     * Ruft den Wert der carrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     * Legt den Wert der carrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrier(String value) {
        this.carrier = value;
    }

}
