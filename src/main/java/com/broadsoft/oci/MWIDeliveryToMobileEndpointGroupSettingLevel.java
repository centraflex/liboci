//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für MWIDeliveryToMobileEndpointGroupSettingLevel.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="MWIDeliveryToMobileEndpointGroupSettingLevel">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Service Provider"/>
 *     <enumeration value="Group"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "MWIDeliveryToMobileEndpointGroupSettingLevel")
@XmlEnum
public enum MWIDeliveryToMobileEndpointGroupSettingLevel {

    @XmlEnumValue("Service Provider")
    SERVICE_PROVIDER("Service Provider"),
    @XmlEnumValue("Group")
    GROUP("Group");
    private final String value;

    MWIDeliveryToMobileEndpointGroupSettingLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MWIDeliveryToMobileEndpointGroupSettingLevel fromValue(String v) {
        for (MWIDeliveryToMobileEndpointGroupSettingLevel c: MWIDeliveryToMobileEndpointGroupSettingLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
