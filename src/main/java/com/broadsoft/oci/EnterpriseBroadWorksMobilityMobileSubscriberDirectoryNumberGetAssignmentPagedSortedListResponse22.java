//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentPagedSortedListRequest22.
 *         The response contains the number of entries that would be returned if the response was not page size restricted.
 *         Contains a table with columns: "Mobile Number", "User Id", "Last Name", "First Name", "Phone Number", "Extension", 
 *         "Group Id", "Department", "Department Type", "Parent Department", "Parent Department Type", "Mobile Network", "Country Code", 
 *         "National Prefix", "Available", "Mobile Country Code", "Mobile National Prefix".
 *         The "Mobile Number" column contains a single DN.
 *         The "User Id", "Last Name" and "First Name" columns contains the corresponding attributes of the user possessing the DN(s).
 *         The "Phone Number" column contains a single DN.
 *         The "Group Id" column contains the Group Id of the user.
 *         The "Group Name" column contains the Group Name of the user.
 *         The "Department" column contains the department of the user if it is part of a department.
 *         The "Parent Department" column contains the parent department of the user if it is part of a department.
 *         The "Department Type" and "Parent Department Type" columns will contain the values "Enterprise" or "Group".
 *         The "Mobile Network" column contains the Mobile Network the number belongs to.
 *         The "Country Code" column indicates the dialing prefix for the phone number.
 *         The "National Prefix" column indicates the digit sequence to be dialed before the telephone number.
 *         The "Available" column indicates if the Mobile Number is available.
 *         The "Mobile Country Code" column indicates the dialing prefix for the mobile number.
 *         The "Mobile National Prefix" column indicates the digit sequence to be dialed before the mobile number.
 *       
 * 
 * <p>Java-Klasse für EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentPagedSortedListResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentPagedSortedListResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="totalNumberOfRows" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         <element name="mobileSubscriberDirectoryNumbersAssignmentTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentPagedSortedListResponse22", propOrder = {
    "totalNumberOfRows",
    "mobileSubscriberDirectoryNumbersAssignmentTable"
})
public class EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentPagedSortedListResponse22
    extends OCIDataResponse
{

    protected Integer totalNumberOfRows;
    @XmlElement(required = true)
    protected OCITable mobileSubscriberDirectoryNumbersAssignmentTable;

    /**
     * Ruft den Wert der totalNumberOfRows-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalNumberOfRows() {
        return totalNumberOfRows;
    }

    /**
     * Legt den Wert der totalNumberOfRows-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalNumberOfRows(Integer value) {
        this.totalNumberOfRows = value;
    }

    /**
     * Ruft den Wert der mobileSubscriberDirectoryNumbersAssignmentTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getMobileSubscriberDirectoryNumbersAssignmentTable() {
        return mobileSubscriberDirectoryNumbersAssignmentTable;
    }

    /**
     * Legt den Wert der mobileSubscriberDirectoryNumbersAssignmentTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setMobileSubscriberDirectoryNumbersAssignmentTable(OCITable value) {
        this.mobileSubscriberDirectoryNumbersAssignmentTable = value;
    }

}
