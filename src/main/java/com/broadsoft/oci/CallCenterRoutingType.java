//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterRoutingType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterRoutingType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Priority Based"/>
 *     <enumeration value="Skill Based"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterRoutingType")
@XmlEnum
public enum CallCenterRoutingType {

    @XmlEnumValue("Priority Based")
    PRIORITY_BASED("Priority Based"),
    @XmlEnumValue("Skill Based")
    SKILL_BASED("Skill Based");
    private final String value;

    CallCenterRoutingType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterRoutingType fromValue(String v) {
        for (CallCenterRoutingType c: CallCenterRoutingType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
