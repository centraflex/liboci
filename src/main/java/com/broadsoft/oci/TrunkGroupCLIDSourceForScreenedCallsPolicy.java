//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TrunkGroupCLIDSourceForScreenedCallsPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="TrunkGroupCLIDSourceForScreenedCallsPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Profile Name Profile Number"/>
 *     <enumeration value="Received Name Profile Number"/>
 *     <enumeration value="Received Name Received Number"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TrunkGroupCLIDSourceForScreenedCallsPolicy")
@XmlEnum
public enum TrunkGroupCLIDSourceForScreenedCallsPolicy {

    @XmlEnumValue("Profile Name Profile Number")
    PROFILE_NAME_PROFILE_NUMBER("Profile Name Profile Number"),
    @XmlEnumValue("Received Name Profile Number")
    RECEIVED_NAME_PROFILE_NUMBER("Received Name Profile Number"),
    @XmlEnumValue("Received Name Received Number")
    RECEIVED_NAME_RECEIVED_NUMBER("Received Name Received Number");
    private final String value;

    TrunkGroupCLIDSourceForScreenedCallsPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrunkGroupCLIDSourceForScreenedCallsPolicy fromValue(String v) {
        for (TrunkGroupCLIDSourceForScreenedCallsPolicy c: TrunkGroupCLIDSourceForScreenedCallsPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
