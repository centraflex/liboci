//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *        	Response to SystemDeviceManagementTagSetRegionTagGetListRequest.
 *         The column headings for the tagsTable are: "Tag Name", "Tag Value".
 *       
 * 
 * <p>Java-Klasse für SystemDeviceManagementTagSetRegionTagGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceManagementTagSetRegionTagGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="tagsTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceManagementTagSetRegionTagGetListResponse", propOrder = {
    "tagsTable"
})
public class SystemDeviceManagementTagSetRegionTagGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable tagsTable;

    /**
     * Ruft den Wert der tagsTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getTagsTable() {
        return tagsTable;
    }

    /**
     * Legt den Wert der tagsTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setTagsTable(OCITable value) {
        this.tagsTable = value;
    }

}
