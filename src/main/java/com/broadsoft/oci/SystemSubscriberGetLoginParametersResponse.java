//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemSubscriberGetLoginParametersRequest.
 *       
 * 
 * <p>Java-Klasse für SystemSubscriberGetLoginParametersResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSubscriberGetLoginParametersResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maxFailedLoginAttempts" type="{}SystemMaxLoginAttempts"/>
 *         <element name="minLoginIdLength" type="{}SystemMinLoginIdLength"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSubscriberGetLoginParametersResponse", propOrder = {
    "maxFailedLoginAttempts",
    "minLoginIdLength"
})
public class SystemSubscriberGetLoginParametersResponse
    extends OCIDataResponse
{

    protected int maxFailedLoginAttempts;
    protected int minLoginIdLength;

    /**
     * Ruft den Wert der maxFailedLoginAttempts-Eigenschaft ab.
     * 
     */
    public int getMaxFailedLoginAttempts() {
        return maxFailedLoginAttempts;
    }

    /**
     * Legt den Wert der maxFailedLoginAttempts-Eigenschaft fest.
     * 
     */
    public void setMaxFailedLoginAttempts(int value) {
        this.maxFailedLoginAttempts = value;
    }

    /**
     * Ruft den Wert der minLoginIdLength-Eigenschaft ab.
     * 
     */
    public int getMinLoginIdLength() {
        return minLoginIdLength;
    }

    /**
     * Legt den Wert der minLoginIdLength-Eigenschaft fest.
     * 
     */
    public void setMinLoginIdLength(int value) {
        this.minLoginIdLength = value;
    }

}
