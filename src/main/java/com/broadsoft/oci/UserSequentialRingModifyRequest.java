//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the user's sequential ring service setting.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserSequentialRingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserSequentialRingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="ringBaseLocationFirst" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="baseLocationNumberOfRings" type="{}SequentialRingNumberOfRings" minOccurs="0"/>
 *         <element name="continueIfBaseLocationIsBusy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="callerMayStopSearch" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="Location01" type="{}SequentialRingLocationModify" minOccurs="0"/>
 *         <element name="Location02" type="{}SequentialRingLocationModify" minOccurs="0"/>
 *         <element name="Location03" type="{}SequentialRingLocationModify" minOccurs="0"/>
 *         <element name="Location04" type="{}SequentialRingLocationModify" minOccurs="0"/>
 *         <element name="Location05" type="{}SequentialRingLocationModify" minOccurs="0"/>
 *         <element name="criteriaActivation" type="{}CriteriaActivation" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserSequentialRingModifyRequest", propOrder = {
    "userId",
    "ringBaseLocationFirst",
    "baseLocationNumberOfRings",
    "continueIfBaseLocationIsBusy",
    "callerMayStopSearch",
    "location01",
    "location02",
    "location03",
    "location04",
    "location05",
    "criteriaActivation"
})
public class UserSequentialRingModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean ringBaseLocationFirst;
    protected Integer baseLocationNumberOfRings;
    protected Boolean continueIfBaseLocationIsBusy;
    protected Boolean callerMayStopSearch;
    @XmlElement(name = "Location01")
    protected SequentialRingLocationModify location01;
    @XmlElement(name = "Location02")
    protected SequentialRingLocationModify location02;
    @XmlElement(name = "Location03")
    protected SequentialRingLocationModify location03;
    @XmlElement(name = "Location04")
    protected SequentialRingLocationModify location04;
    @XmlElement(name = "Location05")
    protected SequentialRingLocationModify location05;
    protected List<CriteriaActivation> criteriaActivation;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der ringBaseLocationFirst-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRingBaseLocationFirst() {
        return ringBaseLocationFirst;
    }

    /**
     * Legt den Wert der ringBaseLocationFirst-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRingBaseLocationFirst(Boolean value) {
        this.ringBaseLocationFirst = value;
    }

    /**
     * Ruft den Wert der baseLocationNumberOfRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBaseLocationNumberOfRings() {
        return baseLocationNumberOfRings;
    }

    /**
     * Legt den Wert der baseLocationNumberOfRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBaseLocationNumberOfRings(Integer value) {
        this.baseLocationNumberOfRings = value;
    }

    /**
     * Ruft den Wert der continueIfBaseLocationIsBusy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isContinueIfBaseLocationIsBusy() {
        return continueIfBaseLocationIsBusy;
    }

    /**
     * Legt den Wert der continueIfBaseLocationIsBusy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContinueIfBaseLocationIsBusy(Boolean value) {
        this.continueIfBaseLocationIsBusy = value;
    }

    /**
     * Ruft den Wert der callerMayStopSearch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCallerMayStopSearch() {
        return callerMayStopSearch;
    }

    /**
     * Legt den Wert der callerMayStopSearch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCallerMayStopSearch(Boolean value) {
        this.callerMayStopSearch = value;
    }

    /**
     * Ruft den Wert der location01-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SequentialRingLocationModify }
     *     
     */
    public SequentialRingLocationModify getLocation01() {
        return location01;
    }

    /**
     * Legt den Wert der location01-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SequentialRingLocationModify }
     *     
     */
    public void setLocation01(SequentialRingLocationModify value) {
        this.location01 = value;
    }

    /**
     * Ruft den Wert der location02-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SequentialRingLocationModify }
     *     
     */
    public SequentialRingLocationModify getLocation02() {
        return location02;
    }

    /**
     * Legt den Wert der location02-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SequentialRingLocationModify }
     *     
     */
    public void setLocation02(SequentialRingLocationModify value) {
        this.location02 = value;
    }

    /**
     * Ruft den Wert der location03-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SequentialRingLocationModify }
     *     
     */
    public SequentialRingLocationModify getLocation03() {
        return location03;
    }

    /**
     * Legt den Wert der location03-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SequentialRingLocationModify }
     *     
     */
    public void setLocation03(SequentialRingLocationModify value) {
        this.location03 = value;
    }

    /**
     * Ruft den Wert der location04-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SequentialRingLocationModify }
     *     
     */
    public SequentialRingLocationModify getLocation04() {
        return location04;
    }

    /**
     * Legt den Wert der location04-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SequentialRingLocationModify }
     *     
     */
    public void setLocation04(SequentialRingLocationModify value) {
        this.location04 = value;
    }

    /**
     * Ruft den Wert der location05-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SequentialRingLocationModify }
     *     
     */
    public SequentialRingLocationModify getLocation05() {
        return location05;
    }

    /**
     * Legt den Wert der location05-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SequentialRingLocationModify }
     *     
     */
    public void setLocation05(SequentialRingLocationModify value) {
        this.location05 = value;
    }

    /**
     * Gets the value of the criteriaActivation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the criteriaActivation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCriteriaActivation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CriteriaActivation }
     * 
     * 
     * @return
     *     The value of the criteriaActivation property.
     */
    public List<CriteriaActivation> getCriteriaActivation() {
        if (criteriaActivation == null) {
            criteriaActivation = new ArrayList<>();
        }
        return this.criteriaActivation;
    }

}
