//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to EnterpriseCallCenterAgentUnavailableCodeSettingsGetRequest17sp4.
 *       
 * 
 * <p>Java-Klasse für EnterpriseCallCenterAgentUnavailableCodeSettingsGetResponse17sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseCallCenterAgentUnavailableCodeSettingsGetResponse17sp4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableAgentUnavailableCodes" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="defaultAgentUnavailableCodeOnDND" type="{}CallCenterAgentUnavailableCode" minOccurs="0"/>
 *         <element name="defaultAgentUnavailableCodeOnPersonalCalls" type="{}CallCenterAgentUnavailableCode" minOccurs="0"/>
 *         <element name="defaultAgentUnavailableCodeOnConsecutiveBounces" type="{}CallCenterAgentUnavailableCode" minOccurs="0"/>
 *         <element name="defaultAgentUnavailableCodeOnNotReachable" type="{}CallCenterAgentUnavailableCode" minOccurs="0"/>
 *         <element name="forceUseOfAgentUnavailableCodes" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="defaultAgentUnavailableCode" type="{}CallCenterAgentUnavailableCode" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseCallCenterAgentUnavailableCodeSettingsGetResponse17sp4", propOrder = {
    "enableAgentUnavailableCodes",
    "defaultAgentUnavailableCodeOnDND",
    "defaultAgentUnavailableCodeOnPersonalCalls",
    "defaultAgentUnavailableCodeOnConsecutiveBounces",
    "defaultAgentUnavailableCodeOnNotReachable",
    "forceUseOfAgentUnavailableCodes",
    "defaultAgentUnavailableCode"
})
public class EnterpriseCallCenterAgentUnavailableCodeSettingsGetResponse17Sp4
    extends OCIDataResponse
{

    protected boolean enableAgentUnavailableCodes;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultAgentUnavailableCodeOnDND;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultAgentUnavailableCodeOnPersonalCalls;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultAgentUnavailableCodeOnConsecutiveBounces;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultAgentUnavailableCodeOnNotReachable;
    protected boolean forceUseOfAgentUnavailableCodes;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultAgentUnavailableCode;

    /**
     * Ruft den Wert der enableAgentUnavailableCodes-Eigenschaft ab.
     * 
     */
    public boolean isEnableAgentUnavailableCodes() {
        return enableAgentUnavailableCodes;
    }

    /**
     * Legt den Wert der enableAgentUnavailableCodes-Eigenschaft fest.
     * 
     */
    public void setEnableAgentUnavailableCodes(boolean value) {
        this.enableAgentUnavailableCodes = value;
    }

    /**
     * Ruft den Wert der defaultAgentUnavailableCodeOnDND-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultAgentUnavailableCodeOnDND() {
        return defaultAgentUnavailableCodeOnDND;
    }

    /**
     * Legt den Wert der defaultAgentUnavailableCodeOnDND-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultAgentUnavailableCodeOnDND(String value) {
        this.defaultAgentUnavailableCodeOnDND = value;
    }

    /**
     * Ruft den Wert der defaultAgentUnavailableCodeOnPersonalCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultAgentUnavailableCodeOnPersonalCalls() {
        return defaultAgentUnavailableCodeOnPersonalCalls;
    }

    /**
     * Legt den Wert der defaultAgentUnavailableCodeOnPersonalCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultAgentUnavailableCodeOnPersonalCalls(String value) {
        this.defaultAgentUnavailableCodeOnPersonalCalls = value;
    }

    /**
     * Ruft den Wert der defaultAgentUnavailableCodeOnConsecutiveBounces-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultAgentUnavailableCodeOnConsecutiveBounces() {
        return defaultAgentUnavailableCodeOnConsecutiveBounces;
    }

    /**
     * Legt den Wert der defaultAgentUnavailableCodeOnConsecutiveBounces-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultAgentUnavailableCodeOnConsecutiveBounces(String value) {
        this.defaultAgentUnavailableCodeOnConsecutiveBounces = value;
    }

    /**
     * Ruft den Wert der defaultAgentUnavailableCodeOnNotReachable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultAgentUnavailableCodeOnNotReachable() {
        return defaultAgentUnavailableCodeOnNotReachable;
    }

    /**
     * Legt den Wert der defaultAgentUnavailableCodeOnNotReachable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultAgentUnavailableCodeOnNotReachable(String value) {
        this.defaultAgentUnavailableCodeOnNotReachable = value;
    }

    /**
     * Ruft den Wert der forceUseOfAgentUnavailableCodes-Eigenschaft ab.
     * 
     */
    public boolean isForceUseOfAgentUnavailableCodes() {
        return forceUseOfAgentUnavailableCodes;
    }

    /**
     * Legt den Wert der forceUseOfAgentUnavailableCodes-Eigenschaft fest.
     * 
     */
    public void setForceUseOfAgentUnavailableCodes(boolean value) {
        this.forceUseOfAgentUnavailableCodes = value;
    }

    /**
     * Ruft den Wert der defaultAgentUnavailableCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultAgentUnavailableCode() {
        return defaultAgentUnavailableCode;
    }

    /**
     * Legt den Wert der defaultAgentUnavailableCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultAgentUnavailableCode(String value) {
        this.defaultAgentUnavailableCode = value;
    }

}
