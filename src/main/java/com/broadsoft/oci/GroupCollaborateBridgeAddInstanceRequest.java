//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a collaborate bridge to a group.
 *         The domain is required in the serviceUserId.
 *         The request fails when supportOutdial is enabled and the system-level collaborate supportOutdial setting is disabled.
 *         The response is either SuccessResponse or ErrorResponse.
 *         
 *         Replaced by GroupCollaborateBridgeAddInstanceRequest20sp1
 *         
 * 
 * <p>Java-Klasse für GroupCollaborateBridgeAddInstanceRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCollaborateBridgeAddInstanceRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceAddProfile"/>
 *         <element name="maximumBridgeParticipants" type="{}CollaborateBridgeMaximumParticipants"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="maxCollaborateRoomParticipants" type="{}CollaborateRoomMaximumParticipants"/>
 *         <element name="supportOutdial" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="collaborateOwnerUserId" type="{}UserId" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCollaborateBridgeAddInstanceRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "serviceUserId",
    "serviceInstanceProfile",
    "maximumBridgeParticipants",
    "networkClassOfService",
    "maxCollaborateRoomParticipants",
    "supportOutdial",
    "collaborateOwnerUserId"
})
public class GroupCollaborateBridgeAddInstanceRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlElement(required = true)
    protected ServiceInstanceAddProfile serviceInstanceProfile;
    @XmlElement(required = true)
    protected CollaborateBridgeMaximumParticipants maximumBridgeParticipants;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    protected int maxCollaborateRoomParticipants;
    protected boolean supportOutdial;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> collaborateOwnerUserId;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceAddProfile }
     *     
     */
    public ServiceInstanceAddProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceAddProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceAddProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der maximumBridgeParticipants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CollaborateBridgeMaximumParticipants }
     *     
     */
    public CollaborateBridgeMaximumParticipants getMaximumBridgeParticipants() {
        return maximumBridgeParticipants;
    }

    /**
     * Legt den Wert der maximumBridgeParticipants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CollaborateBridgeMaximumParticipants }
     *     
     */
    public void setMaximumBridgeParticipants(CollaborateBridgeMaximumParticipants value) {
        this.maximumBridgeParticipants = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Ruft den Wert der maxCollaborateRoomParticipants-Eigenschaft ab.
     * 
     */
    public int getMaxCollaborateRoomParticipants() {
        return maxCollaborateRoomParticipants;
    }

    /**
     * Legt den Wert der maxCollaborateRoomParticipants-Eigenschaft fest.
     * 
     */
    public void setMaxCollaborateRoomParticipants(int value) {
        this.maxCollaborateRoomParticipants = value;
    }

    /**
     * Ruft den Wert der supportOutdial-Eigenschaft ab.
     * 
     */
    public boolean isSupportOutdial() {
        return supportOutdial;
    }

    /**
     * Legt den Wert der supportOutdial-Eigenschaft fest.
     * 
     */
    public void setSupportOutdial(boolean value) {
        this.supportOutdial = value;
    }

    /**
     * Gets the value of the collaborateOwnerUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the collaborateOwnerUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCollaborateOwnerUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the collaborateOwnerUserId property.
     */
    public List<String> getCollaborateOwnerUserId() {
        if (collaborateOwnerUserId == null) {
            collaborateOwnerUserId = new ArrayList<>();
        }
        return this.collaborateOwnerUserId;
    }

}
