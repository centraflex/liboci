//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCollaborateGetRequest20sp1.
 *         
 *         Replaced by: SystemCollaborateGetResponse20sp1V2
 *       
 * 
 * <p>Java-Klasse für SystemCollaborateGetResponse20sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCollaborateGetResponse20sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="collaborateRoomIdLength" type="{}CollaboratePassCodeLength"/>
 *         <element name="instantRoomIdleTimeoutSeconds" type="{}CollaborateInstantRoomIdleTimeoutSeconds20sp1"/>
 *         <element name="collaborateRoomMaximumDurationMinutes" type="{}CollaborateRoomMaximumDurationMinutes"/>
 *         <element name="supportOutdial" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxCollaborateRoomParticipants" type="{}CollaborateRoomMaximumParticipants"/>
 *         <element name="collaborateActiveTalkerRefreshIntervalSeconds" type="{}CollaborateActiveTalkerRefreshIntervalSeconds"/>
 *         <element name="terminateCollaborateAfterGracePeriod" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="collaborateGracePeriod" type="{}CollaborateGracePeriodDuration"/>
 *         <element name="enableActiveCollaborateNotification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="collaborateFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCollaborateGetResponse20sp1", propOrder = {
    "collaborateRoomIdLength",
    "instantRoomIdleTimeoutSeconds",
    "collaborateRoomMaximumDurationMinutes",
    "supportOutdial",
    "maxCollaborateRoomParticipants",
    "collaborateActiveTalkerRefreshIntervalSeconds",
    "terminateCollaborateAfterGracePeriod",
    "collaborateGracePeriod",
    "enableActiveCollaborateNotification",
    "collaborateFromAddress"
})
public class SystemCollaborateGetResponse20Sp1
    extends OCIDataResponse
{

    protected int collaborateRoomIdLength;
    protected int instantRoomIdleTimeoutSeconds;
    protected int collaborateRoomMaximumDurationMinutes;
    protected boolean supportOutdial;
    protected int maxCollaborateRoomParticipants;
    protected int collaborateActiveTalkerRefreshIntervalSeconds;
    protected boolean terminateCollaborateAfterGracePeriod;
    @XmlElement(required = true)
    protected CollaborateGracePeriodDuration collaborateGracePeriod;
    protected boolean enableActiveCollaborateNotification;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String collaborateFromAddress;

    /**
     * Ruft den Wert der collaborateRoomIdLength-Eigenschaft ab.
     * 
     */
    public int getCollaborateRoomIdLength() {
        return collaborateRoomIdLength;
    }

    /**
     * Legt den Wert der collaborateRoomIdLength-Eigenschaft fest.
     * 
     */
    public void setCollaborateRoomIdLength(int value) {
        this.collaborateRoomIdLength = value;
    }

    /**
     * Ruft den Wert der instantRoomIdleTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getInstantRoomIdleTimeoutSeconds() {
        return instantRoomIdleTimeoutSeconds;
    }

    /**
     * Legt den Wert der instantRoomIdleTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setInstantRoomIdleTimeoutSeconds(int value) {
        this.instantRoomIdleTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der collaborateRoomMaximumDurationMinutes-Eigenschaft ab.
     * 
     */
    public int getCollaborateRoomMaximumDurationMinutes() {
        return collaborateRoomMaximumDurationMinutes;
    }

    /**
     * Legt den Wert der collaborateRoomMaximumDurationMinutes-Eigenschaft fest.
     * 
     */
    public void setCollaborateRoomMaximumDurationMinutes(int value) {
        this.collaborateRoomMaximumDurationMinutes = value;
    }

    /**
     * Ruft den Wert der supportOutdial-Eigenschaft ab.
     * 
     */
    public boolean isSupportOutdial() {
        return supportOutdial;
    }

    /**
     * Legt den Wert der supportOutdial-Eigenschaft fest.
     * 
     */
    public void setSupportOutdial(boolean value) {
        this.supportOutdial = value;
    }

    /**
     * Ruft den Wert der maxCollaborateRoomParticipants-Eigenschaft ab.
     * 
     */
    public int getMaxCollaborateRoomParticipants() {
        return maxCollaborateRoomParticipants;
    }

    /**
     * Legt den Wert der maxCollaborateRoomParticipants-Eigenschaft fest.
     * 
     */
    public void setMaxCollaborateRoomParticipants(int value) {
        this.maxCollaborateRoomParticipants = value;
    }

    /**
     * Ruft den Wert der collaborateActiveTalkerRefreshIntervalSeconds-Eigenschaft ab.
     * 
     */
    public int getCollaborateActiveTalkerRefreshIntervalSeconds() {
        return collaborateActiveTalkerRefreshIntervalSeconds;
    }

    /**
     * Legt den Wert der collaborateActiveTalkerRefreshIntervalSeconds-Eigenschaft fest.
     * 
     */
    public void setCollaborateActiveTalkerRefreshIntervalSeconds(int value) {
        this.collaborateActiveTalkerRefreshIntervalSeconds = value;
    }

    /**
     * Ruft den Wert der terminateCollaborateAfterGracePeriod-Eigenschaft ab.
     * 
     */
    public boolean isTerminateCollaborateAfterGracePeriod() {
        return terminateCollaborateAfterGracePeriod;
    }

    /**
     * Legt den Wert der terminateCollaborateAfterGracePeriod-Eigenschaft fest.
     * 
     */
    public void setTerminateCollaborateAfterGracePeriod(boolean value) {
        this.terminateCollaborateAfterGracePeriod = value;
    }

    /**
     * Ruft den Wert der collaborateGracePeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CollaborateGracePeriodDuration }
     *     
     */
    public CollaborateGracePeriodDuration getCollaborateGracePeriod() {
        return collaborateGracePeriod;
    }

    /**
     * Legt den Wert der collaborateGracePeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CollaborateGracePeriodDuration }
     *     
     */
    public void setCollaborateGracePeriod(CollaborateGracePeriodDuration value) {
        this.collaborateGracePeriod = value;
    }

    /**
     * Ruft den Wert der enableActiveCollaborateNotification-Eigenschaft ab.
     * 
     */
    public boolean isEnableActiveCollaborateNotification() {
        return enableActiveCollaborateNotification;
    }

    /**
     * Legt den Wert der enableActiveCollaborateNotification-Eigenschaft fest.
     * 
     */
    public void setEnableActiveCollaborateNotification(boolean value) {
        this.enableActiveCollaborateNotification = value;
    }

    /**
     * Ruft den Wert der collaborateFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollaborateFromAddress() {
        return collaborateFromAddress;
    }

    /**
     * Legt den Wert der collaborateFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollaborateFromAddress(String value) {
        this.collaborateFromAddress = value;
    }

}
