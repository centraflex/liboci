//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupVoiceMessagingGroupGetVoicePortalRequest17sp4.
 *         
 *         The following elements are only used in XS data mode:
 *         enableExtendedScope
 *         
 *         Replaced by: GroupVoiceMessagingGroupGetVoicePortalResponse19sp1 in AS data mode
 *       
 * 
 * <p>Java-Klasse für GroupVoiceMessagingGroupGetVoicePortalResponse17sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupVoiceMessagingGroupGetVoicePortalResponse17sp4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceReadProfile17"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableExtendedScope" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useVoicePortalWizard" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="voicePortalExternalRoutingScope" type="{}VoicePortalExternalRoutingScope"/>
 *         <element name="useExternalRouting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="externalRoutingAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="homeZoneName" type="{}ZoneName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupVoiceMessagingGroupGetVoicePortalResponse17sp4", propOrder = {
    "serviceUserId",
    "serviceInstanceProfile",
    "isActive",
    "enableExtendedScope",
    "allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin",
    "useVoicePortalWizard",
    "voicePortalExternalRoutingScope",
    "useExternalRouting",
    "externalRoutingAddress",
    "homeZoneName"
})
public class GroupVoiceMessagingGroupGetVoicePortalResponse17Sp4
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlElement(required = true)
    protected ServiceInstanceReadProfile17 serviceInstanceProfile;
    protected boolean isActive;
    protected boolean enableExtendedScope;
    protected boolean allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin;
    protected boolean useVoicePortalWizard;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected VoicePortalExternalRoutingScope voicePortalExternalRoutingScope;
    protected boolean useExternalRouting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String externalRoutingAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String homeZoneName;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceReadProfile17 }
     *     
     */
    public ServiceInstanceReadProfile17 getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceReadProfile17 }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceReadProfile17 value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der enableExtendedScope-Eigenschaft ab.
     * 
     */
    public boolean isEnableExtendedScope() {
        return enableExtendedScope;
    }

    /**
     * Legt den Wert der enableExtendedScope-Eigenschaft fest.
     * 
     */
    public void setEnableExtendedScope(boolean value) {
        this.enableExtendedScope = value;
    }

    /**
     * Ruft den Wert der allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin-Eigenschaft ab.
     * 
     */
    public boolean isAllowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin() {
        return allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin;
    }

    /**
     * Legt den Wert der allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin-Eigenschaft fest.
     * 
     */
    public void setAllowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin(boolean value) {
        this.allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin = value;
    }

    /**
     * Ruft den Wert der useVoicePortalWizard-Eigenschaft ab.
     * 
     */
    public boolean isUseVoicePortalWizard() {
        return useVoicePortalWizard;
    }

    /**
     * Legt den Wert der useVoicePortalWizard-Eigenschaft fest.
     * 
     */
    public void setUseVoicePortalWizard(boolean value) {
        this.useVoicePortalWizard = value;
    }

    /**
     * Ruft den Wert der voicePortalExternalRoutingScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoicePortalExternalRoutingScope }
     *     
     */
    public VoicePortalExternalRoutingScope getVoicePortalExternalRoutingScope() {
        return voicePortalExternalRoutingScope;
    }

    /**
     * Legt den Wert der voicePortalExternalRoutingScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoicePortalExternalRoutingScope }
     *     
     */
    public void setVoicePortalExternalRoutingScope(VoicePortalExternalRoutingScope value) {
        this.voicePortalExternalRoutingScope = value;
    }

    /**
     * Ruft den Wert der useExternalRouting-Eigenschaft ab.
     * 
     */
    public boolean isUseExternalRouting() {
        return useExternalRouting;
    }

    /**
     * Legt den Wert der useExternalRouting-Eigenschaft fest.
     * 
     */
    public void setUseExternalRouting(boolean value) {
        this.useExternalRouting = value;
    }

    /**
     * Ruft den Wert der externalRoutingAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalRoutingAddress() {
        return externalRoutingAddress;
    }

    /**
     * Legt den Wert der externalRoutingAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalRoutingAddress(String value) {
        this.externalRoutingAddress = value;
    }

    /**
     * Ruft den Wert der homeZoneName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHomeZoneName() {
        return homeZoneName;
    }

    /**
     * Legt den Wert der homeZoneName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHomeZoneName(String value) {
        this.homeZoneName = value;
    }

}
