//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserBroadWorksReceptionistEnterpriseNoteGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserBroadWorksReceptionistEnterpriseNoteGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksReceptionistEnterpriseNoteGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="receptionistNote" type="{}ReceptionistNote" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksReceptionistEnterpriseNoteGetResponse", propOrder = {
    "receptionistNote"
})
public class UserBroadWorksReceptionistEnterpriseNoteGetResponse
    extends OCIDataResponse
{

    protected String receptionistNote;

    /**
     * Ruft den Wert der receptionistNote-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceptionistNote() {
        return receptionistNote;
    }

    /**
     * Legt den Wert der receptionistNote-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceptionistNote(String value) {
        this.receptionistNote = value;
    }

}
