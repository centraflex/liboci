//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the UserInterceptUserGetRequest21sp1.
 *         
 *         The following elements are only used in AS data mode:
 *           exemptInboundMobilityCalls
 *           exemptOutboundMobilityCalls
 *           disableParallelRingingToNetworkLocations
 *       
 * 
 * <p>Java-Klasse für UserInterceptUserGetResponse21sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserInterceptUserGetResponse21sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="announcementSelection" type="{}AnnouncementSelection"/>
 *         <element name="audioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="audioMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="videoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="videoMediaType" type="{}MediaFileType" minOccurs="0"/>
 *         <element name="inboundCallMode" type="{}InterceptInboundCall"/>
 *         <element name="alternateBlockingAnnouncement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="exemptInboundMobilityCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disableParallelRingingToNetworkLocations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="routeToVoiceMail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="playNewPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="newPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="transferOnZeroToPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="transferPhoneNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="outboundCallMode" type="{}InterceptOutboundCall"/>
 *         <element name="exemptOutboundMobilityCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="rerouteOutboundCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="outboundReroutePhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserInterceptUserGetResponse21sp1", propOrder = {
    "isActive",
    "announcementSelection",
    "audioFileDescription",
    "audioMediaType",
    "videoFileDescription",
    "videoMediaType",
    "inboundCallMode",
    "alternateBlockingAnnouncement",
    "exemptInboundMobilityCalls",
    "disableParallelRingingToNetworkLocations",
    "routeToVoiceMail",
    "playNewPhoneNumber",
    "newPhoneNumber",
    "transferOnZeroToPhoneNumber",
    "transferPhoneNumber",
    "outboundCallMode",
    "exemptOutboundMobilityCalls",
    "rerouteOutboundCalls",
    "outboundReroutePhoneNumber"
})
public class UserInterceptUserGetResponse21Sp1
    extends OCIDataResponse
{

    protected boolean isActive;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AnnouncementSelection announcementSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioMediaType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String videoFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String videoMediaType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected InterceptInboundCall inboundCallMode;
    protected boolean alternateBlockingAnnouncement;
    protected boolean exemptInboundMobilityCalls;
    protected boolean disableParallelRingingToNetworkLocations;
    protected boolean routeToVoiceMail;
    protected boolean playNewPhoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newPhoneNumber;
    protected boolean transferOnZeroToPhoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String transferPhoneNumber;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected InterceptOutboundCall outboundCallMode;
    protected boolean exemptOutboundMobilityCalls;
    protected boolean rerouteOutboundCalls;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String outboundReroutePhoneNumber;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der announcementSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementSelection }
     *     
     */
    public AnnouncementSelection getAnnouncementSelection() {
        return announcementSelection;
    }

    /**
     * Legt den Wert der announcementSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementSelection }
     *     
     */
    public void setAnnouncementSelection(AnnouncementSelection value) {
        this.announcementSelection = value;
    }

    /**
     * Ruft den Wert der audioFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioFileDescription() {
        return audioFileDescription;
    }

    /**
     * Legt den Wert der audioFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioFileDescription(String value) {
        this.audioFileDescription = value;
    }

    /**
     * Ruft den Wert der audioMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioMediaType() {
        return audioMediaType;
    }

    /**
     * Legt den Wert der audioMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioMediaType(String value) {
        this.audioMediaType = value;
    }

    /**
     * Ruft den Wert der videoFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVideoFileDescription() {
        return videoFileDescription;
    }

    /**
     * Legt den Wert der videoFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVideoFileDescription(String value) {
        this.videoFileDescription = value;
    }

    /**
     * Ruft den Wert der videoMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVideoMediaType() {
        return videoMediaType;
    }

    /**
     * Legt den Wert der videoMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVideoMediaType(String value) {
        this.videoMediaType = value;
    }

    /**
     * Ruft den Wert der inboundCallMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InterceptInboundCall }
     *     
     */
    public InterceptInboundCall getInboundCallMode() {
        return inboundCallMode;
    }

    /**
     * Legt den Wert der inboundCallMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InterceptInboundCall }
     *     
     */
    public void setInboundCallMode(InterceptInboundCall value) {
        this.inboundCallMode = value;
    }

    /**
     * Ruft den Wert der alternateBlockingAnnouncement-Eigenschaft ab.
     * 
     */
    public boolean isAlternateBlockingAnnouncement() {
        return alternateBlockingAnnouncement;
    }

    /**
     * Legt den Wert der alternateBlockingAnnouncement-Eigenschaft fest.
     * 
     */
    public void setAlternateBlockingAnnouncement(boolean value) {
        this.alternateBlockingAnnouncement = value;
    }

    /**
     * Ruft den Wert der exemptInboundMobilityCalls-Eigenschaft ab.
     * 
     */
    public boolean isExemptInboundMobilityCalls() {
        return exemptInboundMobilityCalls;
    }

    /**
     * Legt den Wert der exemptInboundMobilityCalls-Eigenschaft fest.
     * 
     */
    public void setExemptInboundMobilityCalls(boolean value) {
        this.exemptInboundMobilityCalls = value;
    }

    /**
     * Ruft den Wert der disableParallelRingingToNetworkLocations-Eigenschaft ab.
     * 
     */
    public boolean isDisableParallelRingingToNetworkLocations() {
        return disableParallelRingingToNetworkLocations;
    }

    /**
     * Legt den Wert der disableParallelRingingToNetworkLocations-Eigenschaft fest.
     * 
     */
    public void setDisableParallelRingingToNetworkLocations(boolean value) {
        this.disableParallelRingingToNetworkLocations = value;
    }

    /**
     * Ruft den Wert der routeToVoiceMail-Eigenschaft ab.
     * 
     */
    public boolean isRouteToVoiceMail() {
        return routeToVoiceMail;
    }

    /**
     * Legt den Wert der routeToVoiceMail-Eigenschaft fest.
     * 
     */
    public void setRouteToVoiceMail(boolean value) {
        this.routeToVoiceMail = value;
    }

    /**
     * Ruft den Wert der playNewPhoneNumber-Eigenschaft ab.
     * 
     */
    public boolean isPlayNewPhoneNumber() {
        return playNewPhoneNumber;
    }

    /**
     * Legt den Wert der playNewPhoneNumber-Eigenschaft fest.
     * 
     */
    public void setPlayNewPhoneNumber(boolean value) {
        this.playNewPhoneNumber = value;
    }

    /**
     * Ruft den Wert der newPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewPhoneNumber() {
        return newPhoneNumber;
    }

    /**
     * Legt den Wert der newPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewPhoneNumber(String value) {
        this.newPhoneNumber = value;
    }

    /**
     * Ruft den Wert der transferOnZeroToPhoneNumber-Eigenschaft ab.
     * 
     */
    public boolean isTransferOnZeroToPhoneNumber() {
        return transferOnZeroToPhoneNumber;
    }

    /**
     * Legt den Wert der transferOnZeroToPhoneNumber-Eigenschaft fest.
     * 
     */
    public void setTransferOnZeroToPhoneNumber(boolean value) {
        this.transferOnZeroToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der transferPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferPhoneNumber() {
        return transferPhoneNumber;
    }

    /**
     * Legt den Wert der transferPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferPhoneNumber(String value) {
        this.transferPhoneNumber = value;
    }

    /**
     * Ruft den Wert der outboundCallMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InterceptOutboundCall }
     *     
     */
    public InterceptOutboundCall getOutboundCallMode() {
        return outboundCallMode;
    }

    /**
     * Legt den Wert der outboundCallMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InterceptOutboundCall }
     *     
     */
    public void setOutboundCallMode(InterceptOutboundCall value) {
        this.outboundCallMode = value;
    }

    /**
     * Ruft den Wert der exemptOutboundMobilityCalls-Eigenschaft ab.
     * 
     */
    public boolean isExemptOutboundMobilityCalls() {
        return exemptOutboundMobilityCalls;
    }

    /**
     * Legt den Wert der exemptOutboundMobilityCalls-Eigenschaft fest.
     * 
     */
    public void setExemptOutboundMobilityCalls(boolean value) {
        this.exemptOutboundMobilityCalls = value;
    }

    /**
     * Ruft den Wert der rerouteOutboundCalls-Eigenschaft ab.
     * 
     */
    public boolean isRerouteOutboundCalls() {
        return rerouteOutboundCalls;
    }

    /**
     * Legt den Wert der rerouteOutboundCalls-Eigenschaft fest.
     * 
     */
    public void setRerouteOutboundCalls(boolean value) {
        this.rerouteOutboundCalls = value;
    }

    /**
     * Ruft den Wert der outboundReroutePhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutboundReroutePhoneNumber() {
        return outboundReroutePhoneNumber;
    }

    /**
     * Legt den Wert der outboundReroutePhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutboundReroutePhoneNumber(String value) {
        this.outboundReroutePhoneNumber = value;
    }

}
