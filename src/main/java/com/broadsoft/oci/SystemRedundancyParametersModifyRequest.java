//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify Redundancy system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemRedundancyParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemRedundancyParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="rollBackTimerMinutes" type="{}RedundancyRollBackTimerMinutes" minOccurs="0"/>
 *         <element name="sendSipOptionMessageUponMigration" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemRedundancyParametersModifyRequest", propOrder = {
    "rollBackTimerMinutes",
    "sendSipOptionMessageUponMigration"
})
public class SystemRedundancyParametersModifyRequest
    extends OCIRequest
{

    protected Integer rollBackTimerMinutes;
    protected Boolean sendSipOptionMessageUponMigration;

    /**
     * Ruft den Wert der rollBackTimerMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRollBackTimerMinutes() {
        return rollBackTimerMinutes;
    }

    /**
     * Legt den Wert der rollBackTimerMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRollBackTimerMinutes(Integer value) {
        this.rollBackTimerMinutes = value;
    }

    /**
     * Ruft den Wert der sendSipOptionMessageUponMigration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendSipOptionMessageUponMigration() {
        return sendSipOptionMessageUponMigration;
    }

    /**
     * Legt den Wert der sendSipOptionMessageUponMigration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendSipOptionMessageUponMigration(Boolean value) {
        this.sendSipOptionMessageUponMigration = value;
    }

}
