//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Unassign a list of Xsi policy profile.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ResellerXsiPolicyProfileUnassignListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerXsiPolicyProfileUnassignListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="resellerId" type="{}ResellerId22"/>
 *         <element name="spXsiPolicyProfile" type="{}XsiPolicyProfileUnassignEntry" minOccurs="0"/>
 *         <element name="groupXsiPolicyProfile" type="{}XsiPolicyProfileUnassignEntry" minOccurs="0"/>
 *         <element name="userXsiPolicyProfile" type="{}XsiPolicyProfileUnassignEntry" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerXsiPolicyProfileUnassignListRequest", propOrder = {
    "resellerId",
    "spXsiPolicyProfile",
    "groupXsiPolicyProfile",
    "userXsiPolicyProfile"
})
public class ResellerXsiPolicyProfileUnassignListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    protected XsiPolicyProfileUnassignEntry spXsiPolicyProfile;
    protected XsiPolicyProfileUnassignEntry groupXsiPolicyProfile;
    protected XsiPolicyProfileUnassignEntry userXsiPolicyProfile;

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Ruft den Wert der spXsiPolicyProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XsiPolicyProfileUnassignEntry }
     *     
     */
    public XsiPolicyProfileUnassignEntry getSpXsiPolicyProfile() {
        return spXsiPolicyProfile;
    }

    /**
     * Legt den Wert der spXsiPolicyProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XsiPolicyProfileUnassignEntry }
     *     
     */
    public void setSpXsiPolicyProfile(XsiPolicyProfileUnassignEntry value) {
        this.spXsiPolicyProfile = value;
    }

    /**
     * Ruft den Wert der groupXsiPolicyProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XsiPolicyProfileUnassignEntry }
     *     
     */
    public XsiPolicyProfileUnassignEntry getGroupXsiPolicyProfile() {
        return groupXsiPolicyProfile;
    }

    /**
     * Legt den Wert der groupXsiPolicyProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XsiPolicyProfileUnassignEntry }
     *     
     */
    public void setGroupXsiPolicyProfile(XsiPolicyProfileUnassignEntry value) {
        this.groupXsiPolicyProfile = value;
    }

    /**
     * Ruft den Wert der userXsiPolicyProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XsiPolicyProfileUnassignEntry }
     *     
     */
    public XsiPolicyProfileUnassignEntry getUserXsiPolicyProfile() {
        return userXsiPolicyProfile;
    }

    /**
     * Legt den Wert der userXsiPolicyProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XsiPolicyProfileUnassignEntry }
     *     
     */
    public void setUserXsiPolicyProfile(XsiPolicyProfileUnassignEntry value) {
        this.userXsiPolicyProfile = value;
    }

}
