//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallParkRecallTo.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallParkRecallTo">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Parking User Only"/>
 *     <enumeration value="Parking User Then Alternate User"/>
 *     <enumeration value="Alternate User Only"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallParkRecallTo")
@XmlEnum
public enum CallParkRecallTo {

    @XmlEnumValue("Parking User Only")
    PARKING_USER_ONLY("Parking User Only"),
    @XmlEnumValue("Parking User Then Alternate User")
    PARKING_USER_THEN_ALTERNATE_USER("Parking User Then Alternate User"),
    @XmlEnumValue("Alternate User Only")
    ALTERNATE_USER_ONLY("Alternate User Only");
    private final String value;

    CallParkRecallTo(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallParkRecallTo fromValue(String v) {
        for (CallParkRecallTo c: CallParkRecallTo.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
