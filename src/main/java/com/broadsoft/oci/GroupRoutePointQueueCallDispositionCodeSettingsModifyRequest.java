//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the queue level data associated with Route Point Agents Unavailable Code Settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupRoutePointQueueCallDispositionCodeSettingsModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupRoutePointQueueCallDispositionCodeSettingsModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="enableCallDispositionCodes" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeOrganizationCodes" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forceUseOfCallDispositionCodes" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="defaultCallDispositionCode" type="{}CallDispositionCodeWithLevel" minOccurs="0"/>
 *         <element name="callDispositionCodeActivation" type="{}CallDispositionCodeActivation" maxOccurs="1000" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupRoutePointQueueCallDispositionCodeSettingsModifyRequest", propOrder = {
    "serviceUserId",
    "enableCallDispositionCodes",
    "includeOrganizationCodes",
    "forceUseOfCallDispositionCodes",
    "defaultCallDispositionCode",
    "callDispositionCodeActivation"
})
public class GroupRoutePointQueueCallDispositionCodeSettingsModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected Boolean enableCallDispositionCodes;
    protected Boolean includeOrganizationCodes;
    protected Boolean forceUseOfCallDispositionCodes;
    @XmlElementRef(name = "defaultCallDispositionCode", type = JAXBElement.class, required = false)
    protected JAXBElement<CallDispositionCodeWithLevel> defaultCallDispositionCode;
    protected List<CallDispositionCodeActivation> callDispositionCodeActivation;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der enableCallDispositionCodes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCallDispositionCodes() {
        return enableCallDispositionCodes;
    }

    /**
     * Legt den Wert der enableCallDispositionCodes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCallDispositionCodes(Boolean value) {
        this.enableCallDispositionCodes = value;
    }

    /**
     * Ruft den Wert der includeOrganizationCodes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeOrganizationCodes() {
        return includeOrganizationCodes;
    }

    /**
     * Legt den Wert der includeOrganizationCodes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeOrganizationCodes(Boolean value) {
        this.includeOrganizationCodes = value;
    }

    /**
     * Ruft den Wert der forceUseOfCallDispositionCodes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceUseOfCallDispositionCodes() {
        return forceUseOfCallDispositionCodes;
    }

    /**
     * Legt den Wert der forceUseOfCallDispositionCodes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceUseOfCallDispositionCodes(Boolean value) {
        this.forceUseOfCallDispositionCodes = value;
    }

    /**
     * Ruft den Wert der defaultCallDispositionCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CallDispositionCodeWithLevel }{@code >}
     *     
     */
    public JAXBElement<CallDispositionCodeWithLevel> getDefaultCallDispositionCode() {
        return defaultCallDispositionCode;
    }

    /**
     * Legt den Wert der defaultCallDispositionCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CallDispositionCodeWithLevel }{@code >}
     *     
     */
    public void setDefaultCallDispositionCode(JAXBElement<CallDispositionCodeWithLevel> value) {
        this.defaultCallDispositionCode = value;
    }

    /**
     * Gets the value of the callDispositionCodeActivation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the callDispositionCodeActivation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCallDispositionCodeActivation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallDispositionCodeActivation }
     * 
     * 
     * @return
     *     The value of the callDispositionCodeActivation property.
     */
    public List<CallDispositionCodeActivation> getCallDispositionCodeActivation() {
        if (callDispositionCodeActivation == null) {
            callDispositionCodeActivation = new ArrayList<>();
        }
        return this.callDispositionCodeActivation;
    }

}
