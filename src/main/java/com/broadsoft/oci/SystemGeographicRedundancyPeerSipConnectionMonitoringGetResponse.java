//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemGeographicRedundancyPeerSipConnectionMonitoringGetRequest.
 *         Contains a list of Peer SIP Connection Monitoring system parameters.
 *       
 * 
 * <p>Java-Klasse für SystemGeographicRedundancyPeerSipConnectionMonitoringGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGeographicRedundancyPeerSipConnectionMonitoringGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="heartbeatInterval" type="{}PeerSipConnectionMonitoringHeartbeatIntervalMilliseconds"/>
 *         <element name="heartbeatTimeout" type="{}PeerSipConnectionMonitoringHeartbeatTimeoutMilliseconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGeographicRedundancyPeerSipConnectionMonitoringGetResponse", propOrder = {
    "enabled",
    "heartbeatInterval",
    "heartbeatTimeout"
})
public class SystemGeographicRedundancyPeerSipConnectionMonitoringGetResponse
    extends OCIDataResponse
{

    protected boolean enabled;
    protected int heartbeatInterval;
    protected int heartbeatTimeout;

    /**
     * Ruft den Wert der enabled-Eigenschaft ab.
     * 
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Legt den Wert der enabled-Eigenschaft fest.
     * 
     */
    public void setEnabled(boolean value) {
        this.enabled = value;
    }

    /**
     * Ruft den Wert der heartbeatInterval-Eigenschaft ab.
     * 
     */
    public int getHeartbeatInterval() {
        return heartbeatInterval;
    }

    /**
     * Legt den Wert der heartbeatInterval-Eigenschaft fest.
     * 
     */
    public void setHeartbeatInterval(int value) {
        this.heartbeatInterval = value;
    }

    /**
     * Ruft den Wert der heartbeatTimeout-Eigenschaft ab.
     * 
     */
    public int getHeartbeatTimeout() {
        return heartbeatTimeout;
    }

    /**
     * Legt den Wert der heartbeatTimeout-Eigenschaft fest.
     * 
     */
    public void setHeartbeatTimeout(int value) {
        this.heartbeatTimeout = value;
    }

}
