//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für VoicePortalExternalRoutingScope.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="VoicePortalExternalRoutingScope">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="System"/>
 *     <enumeration value="Group"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "VoicePortalExternalRoutingScope")
@XmlEnum
public enum VoicePortalExternalRoutingScope {

    @XmlEnumValue("System")
    SYSTEM("System"),
    @XmlEnumValue("Group")
    GROUP("Group");
    private final String value;

    VoicePortalExternalRoutingScope(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VoicePortalExternalRoutingScope fromValue(String v) {
        for (VoicePortalExternalRoutingScope c: VoicePortalExternalRoutingScope.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
