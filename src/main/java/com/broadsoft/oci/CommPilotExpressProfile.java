//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CommPilotExpressProfile.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CommPilotExpressProfile">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Available In Office"/>
 *     <enumeration value="Available Out Of Office"/>
 *     <enumeration value="Busy"/>
 *     <enumeration value="Unavailable"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CommPilotExpressProfile")
@XmlEnum
public enum CommPilotExpressProfile {

    @XmlEnumValue("Available In Office")
    AVAILABLE_IN_OFFICE("Available In Office"),
    @XmlEnumValue("Available Out Of Office")
    AVAILABLE_OUT_OF_OFFICE("Available Out Of Office"),
    @XmlEnumValue("Busy")
    BUSY("Busy"),
    @XmlEnumValue("Unavailable")
    UNAVAILABLE("Unavailable");
    private final String value;

    CommPilotExpressProfile(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommPilotExpressProfile fromValue(String v) {
        for (CommPilotExpressProfile c: CommPilotExpressProfile.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
