//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the EnterpriseCallCenterCurrentAndPastAgentGetListRequest.
 *         Contains a table with column headings: "User Id", "Last Name", "First Name", "Hiragana Last Name", "Hiragana First Name",
 *         "Phone Number", "Extension", "Department", "Email Address".
 *       
 * 
 * <p>Java-Klasse für EnterpriseCallCenterCurrentAndPastAgentGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseCallCenterCurrentAndPastAgentGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="agentUserTable" type="{C}OCITable"/>
 *         <element name="deletedAgentUserTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseCallCenterCurrentAndPastAgentGetListResponse", propOrder = {
    "agentUserTable",
    "deletedAgentUserTable"
})
public class EnterpriseCallCenterCurrentAndPastAgentGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable agentUserTable;
    @XmlElement(required = true)
    protected OCITable deletedAgentUserTable;

    /**
     * Ruft den Wert der agentUserTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAgentUserTable() {
        return agentUserTable;
    }

    /**
     * Legt den Wert der agentUserTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAgentUserTable(OCITable value) {
        this.agentUserTable = value;
    }

    /**
     * Ruft den Wert der deletedAgentUserTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDeletedAgentUserTable() {
        return deletedAgentUserTable;
    }

    /**
     * Legt den Wert der deletedAgentUserTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDeletedAgentUserTable(OCITable value) {
        this.deletedAgentUserTable = value;
    }

}
