//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Create a service pack migration task.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderServicePackMigrationTaskAddRequest21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderServicePackMigrationTaskAddRequest21">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="taskName" type="{}ServicePackMigrationTaskName"/>
 *         <element name="startTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         <element name="expireAfterNumHours" type="{}ServicePackMigrationExpireAfterNumberOfHours"/>
 *         <element name="maxDurationHours" type="{}ServicePackMigrationMaxDurationHours"/>
 *         <element name="sendReportEmail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="reportDeliveryEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="abortOnError" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="abortErrorThreshold" type="{}ServicePackMigrationAbortErrorThreshold" minOccurs="0"/>
 *         <element name="reportAllUsers" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="automaticallyIncrementServiceQuantity" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderServicePackMigrationTaskAddRequest21", propOrder = {
    "serviceProviderId",
    "taskName",
    "startTimestamp",
    "expireAfterNumHours",
    "maxDurationHours",
    "sendReportEmail",
    "reportDeliveryEmailAddress",
    "abortOnError",
    "abortErrorThreshold",
    "reportAllUsers",
    "automaticallyIncrementServiceQuantity"
})
public class ServiceProviderServicePackMigrationTaskAddRequest21
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String taskName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startTimestamp;
    protected int expireAfterNumHours;
    protected int maxDurationHours;
    protected boolean sendReportEmail;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String reportDeliveryEmailAddress;
    protected boolean abortOnError;
    protected Integer abortErrorThreshold;
    protected boolean reportAllUsers;
    protected boolean automaticallyIncrementServiceQuantity;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der taskName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * Legt den Wert der taskName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskName(String value) {
        this.taskName = value;
    }

    /**
     * Ruft den Wert der startTimestamp-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartTimestamp() {
        return startTimestamp;
    }

    /**
     * Legt den Wert der startTimestamp-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartTimestamp(XMLGregorianCalendar value) {
        this.startTimestamp = value;
    }

    /**
     * Ruft den Wert der expireAfterNumHours-Eigenschaft ab.
     * 
     */
    public int getExpireAfterNumHours() {
        return expireAfterNumHours;
    }

    /**
     * Legt den Wert der expireAfterNumHours-Eigenschaft fest.
     * 
     */
    public void setExpireAfterNumHours(int value) {
        this.expireAfterNumHours = value;
    }

    /**
     * Ruft den Wert der maxDurationHours-Eigenschaft ab.
     * 
     */
    public int getMaxDurationHours() {
        return maxDurationHours;
    }

    /**
     * Legt den Wert der maxDurationHours-Eigenschaft fest.
     * 
     */
    public void setMaxDurationHours(int value) {
        this.maxDurationHours = value;
    }

    /**
     * Ruft den Wert der sendReportEmail-Eigenschaft ab.
     * 
     */
    public boolean isSendReportEmail() {
        return sendReportEmail;
    }

    /**
     * Legt den Wert der sendReportEmail-Eigenschaft fest.
     * 
     */
    public void setSendReportEmail(boolean value) {
        this.sendReportEmail = value;
    }

    /**
     * Ruft den Wert der reportDeliveryEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportDeliveryEmailAddress() {
        return reportDeliveryEmailAddress;
    }

    /**
     * Legt den Wert der reportDeliveryEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportDeliveryEmailAddress(String value) {
        this.reportDeliveryEmailAddress = value;
    }

    /**
     * Ruft den Wert der abortOnError-Eigenschaft ab.
     * 
     */
    public boolean isAbortOnError() {
        return abortOnError;
    }

    /**
     * Legt den Wert der abortOnError-Eigenschaft fest.
     * 
     */
    public void setAbortOnError(boolean value) {
        this.abortOnError = value;
    }

    /**
     * Ruft den Wert der abortErrorThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAbortErrorThreshold() {
        return abortErrorThreshold;
    }

    /**
     * Legt den Wert der abortErrorThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAbortErrorThreshold(Integer value) {
        this.abortErrorThreshold = value;
    }

    /**
     * Ruft den Wert der reportAllUsers-Eigenschaft ab.
     * 
     */
    public boolean isReportAllUsers() {
        return reportAllUsers;
    }

    /**
     * Legt den Wert der reportAllUsers-Eigenschaft fest.
     * 
     */
    public void setReportAllUsers(boolean value) {
        this.reportAllUsers = value;
    }

    /**
     * Ruft den Wert der automaticallyIncrementServiceQuantity-Eigenschaft ab.
     * 
     */
    public boolean isAutomaticallyIncrementServiceQuantity() {
        return automaticallyIncrementServiceQuantity;
    }

    /**
     * Legt den Wert der automaticallyIncrementServiceQuantity-Eigenschaft fest.
     * 
     */
    public void setAutomaticallyIncrementServiceQuantity(boolean value) {
        this.automaticallyIncrementServiceQuantity = value;
    }

}
