//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemNumberPortabilityQueryGetRequest.
 *         Returns system Number Portability Query Parameters.
 *       
 * 
 * <p>Java-Klasse für SystemNumberPortabilityQueryGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNumberPortabilityQueryGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="continueCallAsDialedOnTimeoutOrError" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="numberPortabilityNameLookupTimeoutMilliseconds" type="{}NumberPortabilityNameLookupTimeoutMilliseconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNumberPortabilityQueryGetResponse", propOrder = {
    "continueCallAsDialedOnTimeoutOrError",
    "numberPortabilityNameLookupTimeoutMilliseconds"
})
public class SystemNumberPortabilityQueryGetResponse
    extends OCIDataResponse
{

    protected boolean continueCallAsDialedOnTimeoutOrError;
    protected int numberPortabilityNameLookupTimeoutMilliseconds;

    /**
     * Ruft den Wert der continueCallAsDialedOnTimeoutOrError-Eigenschaft ab.
     * 
     */
    public boolean isContinueCallAsDialedOnTimeoutOrError() {
        return continueCallAsDialedOnTimeoutOrError;
    }

    /**
     * Legt den Wert der continueCallAsDialedOnTimeoutOrError-Eigenschaft fest.
     * 
     */
    public void setContinueCallAsDialedOnTimeoutOrError(boolean value) {
        this.continueCallAsDialedOnTimeoutOrError = value;
    }

    /**
     * Ruft den Wert der numberPortabilityNameLookupTimeoutMilliseconds-Eigenschaft ab.
     * 
     */
    public int getNumberPortabilityNameLookupTimeoutMilliseconds() {
        return numberPortabilityNameLookupTimeoutMilliseconds;
    }

    /**
     * Legt den Wert der numberPortabilityNameLookupTimeoutMilliseconds-Eigenschaft fest.
     * 
     */
    public void setNumberPortabilityNameLookupTimeoutMilliseconds(int value) {
        this.numberPortabilityNameLookupTimeoutMilliseconds = value;
    }

}
