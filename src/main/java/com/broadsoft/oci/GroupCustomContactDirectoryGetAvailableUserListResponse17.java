//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the 
 *         GroupCustomContactDirectoryGetAvailableUserListRequest17.
 *         Contains a table with column headings: "User Id", "Last Name", 
 *         "First Name", "Hiragana Last Name", "Hiragana First Name", 
 *         "Virtual On-Net Phone Number", "Group Id", "Is Virtual On-Net User", 
 *         "Phone Number", "Extension", "Department", "Email Address".
 * 
 *         If the entry represents a Virtual On-Net user then "User Id" is blank,  
 *         the "Virtual On-Net Phone Number" contains the phone Number of the 
 *         Virtual On-Net user, the "Group Id" contains the Virtual On-Net user's 
 *         group and the "Is Virtual On-Net User" contains true.
 * 
 *         If the entry represents a BroadWorks user then the "User Id" contains 
 *         his BroadWorks userId, the "Virtual On-Net Phone Number" and 
 *         "Group Id" fields are blank and the "Is Virtual On-Net User" 
 *         contains false.
 *       
 * 
 * <p>Java-Klasse für GroupCustomContactDirectoryGetAvailableUserListResponse17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCustomContactDirectoryGetAvailableUserListResponse17">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="userTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCustomContactDirectoryGetAvailableUserListResponse17", propOrder = {
    "userTable"
})
public class GroupCustomContactDirectoryGetAvailableUserListResponse17
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable userTable;

    /**
     * Ruft den Wert der userTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserTable() {
        return userTable;
    }

    /**
     * Legt den Wert der userTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserTable(OCITable value) {
        this.userTable = value;
    }

}
