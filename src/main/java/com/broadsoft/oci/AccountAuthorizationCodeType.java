//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AccountAuthorizationCodeType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AccountAuthorizationCodeType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Account Code"/>
 *     <enumeration value="Authorization Code"/>
 *     <enumeration value="Deactivated"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AccountAuthorizationCodeType")
@XmlEnum
public enum AccountAuthorizationCodeType {

    @XmlEnumValue("Account Code")
    ACCOUNT_CODE("Account Code"),
    @XmlEnumValue("Authorization Code")
    AUTHORIZATION_CODE("Authorization Code"),
    @XmlEnumValue("Deactivated")
    DEACTIVATED("Deactivated");
    private final String value;

    AccountAuthorizationCodeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AccountAuthorizationCodeType fromValue(String v) {
        for (AccountAuthorizationCodeType c: AccountAuthorizationCodeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
