//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to EnterpriseCallCenterMonitoringGetRequest23.
 *       
 * 
 * <p>Java-Klasse für EnterpriseCallCenterMonitoringGetResponse23 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseCallCenterMonitoringGetResponse23">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableSupervisorCoaching" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="bypassEnforcementOfSupervisorAgentRelationship" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseCallCenterMonitoringGetResponse23", propOrder = {
    "enableSupervisorCoaching",
    "bypassEnforcementOfSupervisorAgentRelationship"
})
public class EnterpriseCallCenterMonitoringGetResponse23
    extends OCIDataResponse
{

    protected boolean enableSupervisorCoaching;
    protected Boolean bypassEnforcementOfSupervisorAgentRelationship;

    /**
     * Ruft den Wert der enableSupervisorCoaching-Eigenschaft ab.
     * 
     */
    public boolean isEnableSupervisorCoaching() {
        return enableSupervisorCoaching;
    }

    /**
     * Legt den Wert der enableSupervisorCoaching-Eigenschaft fest.
     * 
     */
    public void setEnableSupervisorCoaching(boolean value) {
        this.enableSupervisorCoaching = value;
    }

    /**
     * Ruft den Wert der bypassEnforcementOfSupervisorAgentRelationship-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBypassEnforcementOfSupervisorAgentRelationship() {
        return bypassEnforcementOfSupervisorAgentRelationship;
    }

    /**
     * Legt den Wert der bypassEnforcementOfSupervisorAgentRelationship-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBypassEnforcementOfSupervisorAgentRelationship(Boolean value) {
        this.bypassEnforcementOfSupervisorAgentRelationship = value;
    }

}
