//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Dial Plan Access Code attributes.
 *       
 * 
 * <p>Java-Klasse für DialPlanPolicy complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="DialPlanPolicy">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="useSetting" type="{}GroupDialPlanPolicySettingLevel" minOccurs="0"/>
 *         <element name="requiresAccessCodeForPublicCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowE164PublicCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="preferE164NumberFormatForCallbackServices" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="publicDigitMap" type="{}DigitMap" minOccurs="0"/>
 *         <element name="privateDigitMap" type="{}DigitMap" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DialPlanPolicy", propOrder = {
    "useSetting",
    "requiresAccessCodeForPublicCalls",
    "allowE164PublicCalls",
    "preferE164NumberFormatForCallbackServices",
    "publicDigitMap",
    "privateDigitMap"
})
public class DialPlanPolicy {

    @XmlSchemaType(name = "token")
    protected GroupDialPlanPolicySettingLevel useSetting;
    protected Boolean requiresAccessCodeForPublicCalls;
    protected Boolean allowE164PublicCalls;
    protected Boolean preferE164NumberFormatForCallbackServices;
    @XmlElementRef(name = "publicDigitMap", type = JAXBElement.class, required = false)
    protected JAXBElement<String> publicDigitMap;
    @XmlElementRef(name = "privateDigitMap", type = JAXBElement.class, required = false)
    protected JAXBElement<String> privateDigitMap;

    /**
     * Ruft den Wert der useSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDialPlanPolicySettingLevel }
     *     
     */
    public GroupDialPlanPolicySettingLevel getUseSetting() {
        return useSetting;
    }

    /**
     * Legt den Wert der useSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDialPlanPolicySettingLevel }
     *     
     */
    public void setUseSetting(GroupDialPlanPolicySettingLevel value) {
        this.useSetting = value;
    }

    /**
     * Ruft den Wert der requiresAccessCodeForPublicCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequiresAccessCodeForPublicCalls() {
        return requiresAccessCodeForPublicCalls;
    }

    /**
     * Legt den Wert der requiresAccessCodeForPublicCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequiresAccessCodeForPublicCalls(Boolean value) {
        this.requiresAccessCodeForPublicCalls = value;
    }

    /**
     * Ruft den Wert der allowE164PublicCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowE164PublicCalls() {
        return allowE164PublicCalls;
    }

    /**
     * Legt den Wert der allowE164PublicCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowE164PublicCalls(Boolean value) {
        this.allowE164PublicCalls = value;
    }

    /**
     * Ruft den Wert der preferE164NumberFormatForCallbackServices-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPreferE164NumberFormatForCallbackServices() {
        return preferE164NumberFormatForCallbackServices;
    }

    /**
     * Legt den Wert der preferE164NumberFormatForCallbackServices-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreferE164NumberFormatForCallbackServices(Boolean value) {
        this.preferE164NumberFormatForCallbackServices = value;
    }

    /**
     * Ruft den Wert der publicDigitMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPublicDigitMap() {
        return publicDigitMap;
    }

    /**
     * Legt den Wert der publicDigitMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPublicDigitMap(JAXBElement<String> value) {
        this.publicDigitMap = value;
    }

    /**
     * Ruft den Wert der privateDigitMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrivateDigitMap() {
        return privateDigitMap;
    }

    /**
     * Legt den Wert der privateDigitMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrivateDigitMap(JAXBElement<String> value) {
        this.privateDigitMap = value;
    }

}
