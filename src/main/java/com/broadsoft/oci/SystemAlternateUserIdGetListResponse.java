//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemAlternateUserIdGetListRequest.
 *         The "User Type" column contains the corresponding enumerated UserType value.
 *         Contains a table of alternate user ids, the column headings are: 
 *           "User Id", "Alternate User Id", "Group Id", "Organization Id", "Reseller Id" and "User Type".
 *           
 *         The following columns are only returned in AS data mode:       
 *           "Reseller Id"          
 *       
 * 
 * <p>Java-Klasse für SystemAlternateUserIdGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAlternateUserIdGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="alternateUserIdTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAlternateUserIdGetListResponse", propOrder = {
    "alternateUserIdTable"
})
public class SystemAlternateUserIdGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable alternateUserIdTable;

    /**
     * Ruft den Wert der alternateUserIdTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAlternateUserIdTable() {
        return alternateUserIdTable;
    }

    /**
     * Legt den Wert der alternateUserIdTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAlternateUserIdTable(OCITable value) {
        this.alternateUserIdTable = value;
    }

}
