//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupCallParkGetRequest.
 *         Contains the settings that apply to the whole group for Call Park.
 *       
 * 
 * <p>Java-Klasse für GroupCallParkGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallParkGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="recallTimerSeconds" type="{}CallParkRecallTimerSeconds"/>
 *         <element name="displayTimerSeconds" type="{}CallParkDisplayTimerSeconds"/>
 *         <element name="enableDestinationAnnouncement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallParkGetResponse", propOrder = {
    "recallTimerSeconds",
    "displayTimerSeconds",
    "enableDestinationAnnouncement"
})
public class GroupCallParkGetResponse
    extends OCIDataResponse
{

    protected int recallTimerSeconds;
    protected int displayTimerSeconds;
    protected boolean enableDestinationAnnouncement;

    /**
     * Ruft den Wert der recallTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getRecallTimerSeconds() {
        return recallTimerSeconds;
    }

    /**
     * Legt den Wert der recallTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setRecallTimerSeconds(int value) {
        this.recallTimerSeconds = value;
    }

    /**
     * Ruft den Wert der displayTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getDisplayTimerSeconds() {
        return displayTimerSeconds;
    }

    /**
     * Legt den Wert der displayTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setDisplayTimerSeconds(int value) {
        this.displayTimerSeconds = value;
    }

    /**
     * Ruft den Wert der enableDestinationAnnouncement-Eigenschaft ab.
     * 
     */
    public boolean isEnableDestinationAnnouncement() {
        return enableDestinationAnnouncement;
    }

    /**
     * Legt den Wert der enableDestinationAnnouncement-Eigenschaft fest.
     * 
     */
    public void setEnableDestinationAnnouncement(boolean value) {
        this.enableDestinationAnnouncement = value;
    }

}
