//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserGroupNightForwardingGetRequest.
 *         businessHours and holidaySchedule are returned in the response only when groupNightForwarding is ‘Auto On’.
 *       
 * 
 * <p>Java-Klasse für UserGroupNightForwardingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserGroupNightForwardingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="nightForwarding" type="{}GroupNightForwardingUserServiceActivationMode"/>
 *         <element name="groupNightForwarding" type="{}GroupNightForwardingGroupServiceActivationMode"/>
 *         <element name="businessHours" type="{}TimeSchedule" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}HolidaySchedule" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserGroupNightForwardingGetResponse", propOrder = {
    "nightForwarding",
    "groupNightForwarding",
    "businessHours",
    "holidaySchedule"
})
public class UserGroupNightForwardingGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupNightForwardingUserServiceActivationMode nightForwarding;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupNightForwardingGroupServiceActivationMode groupNightForwarding;
    protected TimeSchedule businessHours;
    protected HolidaySchedule holidaySchedule;

    /**
     * Ruft den Wert der nightForwarding-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupNightForwardingUserServiceActivationMode }
     *     
     */
    public GroupNightForwardingUserServiceActivationMode getNightForwarding() {
        return nightForwarding;
    }

    /**
     * Legt den Wert der nightForwarding-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupNightForwardingUserServiceActivationMode }
     *     
     */
    public void setNightForwarding(GroupNightForwardingUserServiceActivationMode value) {
        this.nightForwarding = value;
    }

    /**
     * Ruft den Wert der groupNightForwarding-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupNightForwardingGroupServiceActivationMode }
     *     
     */
    public GroupNightForwardingGroupServiceActivationMode getGroupNightForwarding() {
        return groupNightForwarding;
    }

    /**
     * Legt den Wert der groupNightForwarding-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupNightForwardingGroupServiceActivationMode }
     *     
     */
    public void setGroupNightForwarding(GroupNightForwardingGroupServiceActivationMode value) {
        this.groupNightForwarding = value;
    }

    /**
     * Ruft den Wert der businessHours-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeSchedule }
     *     
     */
    public TimeSchedule getBusinessHours() {
        return businessHours;
    }

    /**
     * Legt den Wert der businessHours-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeSchedule }
     *     
     */
    public void setBusinessHours(TimeSchedule value) {
        this.businessHours = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HolidaySchedule }
     *     
     */
    public HolidaySchedule getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HolidaySchedule }
     *     
     */
    public void setHolidaySchedule(HolidaySchedule value) {
        this.holidaySchedule = value;
    }

}
