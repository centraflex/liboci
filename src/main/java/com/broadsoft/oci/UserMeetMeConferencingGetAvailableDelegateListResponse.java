//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserMeetMeConferencingGetAvailableDelegateListRequest.
 *         Contains all hosts assigned on a bridge.
 *         The table has column headings: "User Id", "Last Name", "First Name", "Hiragana Last Name", "Hiragana First Name", "Phone Number", "Extension", "Department" and "Email Address".
 *       
 * 
 * <p>Java-Klasse für UserMeetMeConferencingGetAvailableDelegateListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserMeetMeConferencingGetAvailableDelegateListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="conferenceDelegateUserTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserMeetMeConferencingGetAvailableDelegateListResponse", propOrder = {
    "conferenceDelegateUserTable"
})
public class UserMeetMeConferencingGetAvailableDelegateListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable conferenceDelegateUserTable;

    /**
     * Ruft den Wert der conferenceDelegateUserTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getConferenceDelegateUserTable() {
        return conferenceDelegateUserTable;
    }

    /**
     * Legt den Wert der conferenceDelegateUserTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setConferenceDelegateUserTable(OCITable value) {
        this.conferenceDelegateUserTable = value;
    }

}
