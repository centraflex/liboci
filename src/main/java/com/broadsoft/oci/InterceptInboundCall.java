//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für InterceptInboundCall.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="InterceptInboundCall">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Intercept All"/>
 *     <enumeration value="Allow All"/>
 *     <enumeration value="Allow System Dns"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "InterceptInboundCall")
@XmlEnum
public enum InterceptInboundCall {

    @XmlEnumValue("Intercept All")
    INTERCEPT_ALL("Intercept All"),
    @XmlEnumValue("Allow All")
    ALLOW_ALL("Allow All"),
    @XmlEnumValue("Allow System Dns")
    ALLOW_SYSTEM_DNS("Allow System Dns");
    private final String value;

    InterceptInboundCall(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InterceptInboundCall fromValue(String v) {
        for (InterceptInboundCall c: InterceptInboundCall.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
