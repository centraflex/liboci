//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SIPCallingPartyCategoryFormat.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SIPCallingPartyCategoryFormat">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="CPC"/>
 *     <enumeration value="ISUP OLI"/>
 *     <enumeration value="CPC GTD"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SIPCallingPartyCategoryFormat")
@XmlEnum
public enum SIPCallingPartyCategoryFormat {

    CPC("CPC"),
    @XmlEnumValue("ISUP OLI")
    ISUP_OLI("ISUP OLI"),
    @XmlEnumValue("CPC GTD")
    CPC_GTD("CPC GTD");
    private final String value;

    SIPCallingPartyCategoryFormat(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SIPCallingPartyCategoryFormat fromValue(String v) {
        for (SIPCallingPartyCategoryFormat c: SIPCallingPartyCategoryFormat.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
