//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *          The response is either SuccessResponse or ErrorResponse.
 *          
 *          In AS and Amplify  Data Mode:
 *            The group user limit will be increased by one if needed.
 *            
 *            The group will be added if it does not exist and if the command is executed by a Service 
 *            Provider level administrator or above. If the group needs to be created, the 
 *            groupProperties element must be set or the request will fail.
 *            The groupProperties element will be ignored if the group already exists.
 *            
 *            If the domain has not been assigned to the group, it will be added to group if the 
 *            command is executed by a Service provider level administrator or above.
 *            If the domain has not been assigned to the service provider, it will be added to the 
 *            service provider if the command is executed by a Provisioning level administrator or 
 *            above. The command will fail otherwise.
 *            
 *            If the phoneNumber has not been assigned to the group and addPhoneNumberToGroup is set 
 *            to true, it will be added to group if the command is executed by a service provider 
 *            administrator or above and the number is already assigned to the service provider. The 
 *            command will fail otherwise.
 *            
 *            The password is not required if external authentication is enabled.
 *            Alternate user ids can be added by a group level administrator or above.
 *            
 *            When sharedCallAppearanceAccessDeviceEndpoint element is included and the Shared Call 
 *            Appearance is not included in the service/service pack of the request or in the 
 *            "new user template", the request will fail.
 *            
 *            The userService/servicePack will be authorized to the group if it has not been 
 *            authorized to the group if the command is executed by a Service Provider level 
 *            administrator or above. The command will fail otherwise.
 *            The authorizedQuantity will be set to unlimited if not present.
 *            
 *            When thirdPartyVoiceMail elements are included and the Third Party Voice Mail Support 
 *            service is not included in the service/service pack of the request or in the 
 *            "new user template", the request will fail.
 *            
 *            When sipAuthenticationData element is included and the Authentication service is not 
 *            included in the service/service pack of the request or in the "new user template", 
 *            the request will fail.
 *            
 *            
 *          In XS data mode:
 *            only the System level administrator has the authorization level to execute the command.
 *            The group will be added if it does not exist. If the group needs to be created, the 
 *            groupProperties element must be set or the request will fail.
 *            The groupProperties element will be ignored if the group already exists. 
 *            
 *            If the phoneNumber has not been assigned to the group, it will be added to group and 
 *            service provider if needed.
 *            
 *            When sharedCallAppearanceAccessDeviceEndpoint element is included and the Shared 
 *            Call Appearance is not included in the service/service pack of the request, the request 
 *            will fail.
 *            
 *          The following elements are ignored in XS data mode:
 *            addPhoneNumberToGroup
 *            nameDialingName 
 *            alternateUserId
 *            passcode
 *            trunkAddressing
 *            thirdPartyVoiceMailServerSelection
 *            thirdPartyVoiceMailServerUserServer
 *            thirdPartyVoiceMailServerMailboxIdType
 *            thirdPartyVoiceMailMailboxURL
 *            sipAuthenticationData 
 *          Replaced by: UserConsolidatedAddRequest22 in AS data mode.
 *       
 * 
 * <p>Java-Klasse für UserConsolidatedAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserConsolidatedAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <choice>
 *           <sequence>
 *             <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *             <element name="groupId" type="{}GroupId"/>
 *           </sequence>
 *           <element name="groupExternalId" type="{}ExternalId"/>
 *         </choice>
 *         <element name="groupProperties" type="{}ConsolidatedGroupProperties" minOccurs="0"/>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="userExternalId" type="{}ExternalId" minOccurs="0"/>
 *         <element name="addPhoneNumberToGroup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="lastName" type="{}LastName"/>
 *         <element name="firstName" type="{}FirstName"/>
 *         <element name="callingLineIdLastName" type="{}CallingLineIdLastName"/>
 *         <element name="callingLineIdFirstName" type="{}CallingLineIdFirstName"/>
 *         <element name="nameDialingName" type="{}NameDialingName" minOccurs="0"/>
 *         <element name="hiraganaLastName" type="{}HiraganaLastName" minOccurs="0"/>
 *         <element name="hiraganaFirstName" type="{}HiraganaFirstName" minOccurs="0"/>
 *         <element name="phoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="alternateUserId" type="{}AlternateUserIdEntry" maxOccurs="4" minOccurs="0"/>
 *         <element name="extension" type="{}Extension17" minOccurs="0"/>
 *         <element name="callingLineIdPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="password" type="{}Password" minOccurs="0"/>
 *         <element name="passcode" type="{}Passcode" minOccurs="0"/>
 *         <element name="department" type="{}DepartmentKey" minOccurs="0"/>
 *         <element name="language" type="{}Language" minOccurs="0"/>
 *         <element name="timeZone" type="{}TimeZone" minOccurs="0"/>
 *         <element name="alias" type="{}SIPURI" maxOccurs="3" minOccurs="0"/>
 *         <choice>
 *           <element name="accessDeviceEndpoint" type="{}ConsolidatedAccessDeviceMultipleIdentityEndpointAndContactAdd" minOccurs="0"/>
 *           <element name="trunkAddressing" type="{}TrunkAddressingMultipleContactAdd" minOccurs="0"/>
 *         </choice>
 *         <element name="sharedCallAppearanceAccessDeviceEndpoint" type="{}ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint" maxOccurs="35" minOccurs="0"/>
 *         <element name="title" type="{}Title" minOccurs="0"/>
 *         <element name="pagerPhoneNumber" type="{}InformationalDN" minOccurs="0"/>
 *         <element name="mobilePhoneNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="emailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="yahooId" type="{}YahooId" minOccurs="0"/>
 *         <element name="addressLocation" type="{}AddressLocation" minOccurs="0"/>
 *         <element name="address" type="{}StreetAddress" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="userService" type="{}ConsolidatedUserServiceAssignment" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="servicePack" type="{}ConsolidatedServicePackAssignment" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="thirdPartyVoiceMailServerSelection" type="{}ThirdPartyVoiceMailSupportServerSelection" minOccurs="0"/>
 *         <element name="thirdPartyVoiceMailServerUserServer" type="{}ThirdPartyVoiceMailSupportMailServer" minOccurs="0"/>
 *         <element name="thirdPartyVoiceMailServerMailboxIdType" type="{}ThirdPartyVoiceMailSupportMailboxIdType" minOccurs="0"/>
 *         <element name="thirdPartyVoiceMailMailboxURL" type="{}SIPURI" minOccurs="0"/>
 *         <element name="sipAuthenticationData" type="{}SIPAuthenticationUserNamePassword" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserConsolidatedAddRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "groupExternalId",
    "groupProperties",
    "userId",
    "userExternalId",
    "addPhoneNumberToGroup",
    "lastName",
    "firstName",
    "callingLineIdLastName",
    "callingLineIdFirstName",
    "nameDialingName",
    "hiraganaLastName",
    "hiraganaFirstName",
    "phoneNumber",
    "alternateUserId",
    "extension",
    "callingLineIdPhoneNumber",
    "password",
    "passcode",
    "department",
    "language",
    "timeZone",
    "alias",
    "accessDeviceEndpoint",
    "trunkAddressing",
    "sharedCallAppearanceAccessDeviceEndpoint",
    "title",
    "pagerPhoneNumber",
    "mobilePhoneNumber",
    "emailAddress",
    "yahooId",
    "addressLocation",
    "address",
    "networkClassOfService",
    "userService",
    "servicePack",
    "thirdPartyVoiceMailServerSelection",
    "thirdPartyVoiceMailServerUserServer",
    "thirdPartyVoiceMailServerMailboxIdType",
    "thirdPartyVoiceMailMailboxURL",
    "sipAuthenticationData"
})
public class UserConsolidatedAddRequest
    extends OCIRequest
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupExternalId;
    protected ConsolidatedGroupProperties groupProperties;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userExternalId;
    protected Boolean addPhoneNumberToGroup;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String lastName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String firstName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdLastName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdFirstName;
    protected NameDialingName nameDialingName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String hiraganaLastName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String hiraganaFirstName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String phoneNumber;
    protected List<AlternateUserIdEntry> alternateUserId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String extension;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdPhoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String password;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String passcode;
    protected DepartmentKey department;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String language;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeZone;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> alias;
    protected ConsolidatedAccessDeviceMultipleIdentityEndpointAndContactAdd accessDeviceEndpoint;
    protected TrunkAddressingMultipleContactAdd trunkAddressing;
    protected List<ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint> sharedCallAppearanceAccessDeviceEndpoint;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String title;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String pagerPhoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mobilePhoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String emailAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String yahooId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String addressLocation;
    protected StreetAddress address;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    protected List<ConsolidatedUserServiceAssignment> userService;
    protected List<ConsolidatedServicePackAssignment> servicePack;
    @XmlSchemaType(name = "token")
    protected ThirdPartyVoiceMailSupportServerSelection thirdPartyVoiceMailServerSelection;
    @XmlElementRef(name = "thirdPartyVoiceMailServerUserServer", type = JAXBElement.class, required = false)
    protected JAXBElement<String> thirdPartyVoiceMailServerUserServer;
    @XmlSchemaType(name = "token")
    protected ThirdPartyVoiceMailSupportMailboxIdType thirdPartyVoiceMailServerMailboxIdType;
    @XmlElementRef(name = "thirdPartyVoiceMailMailboxURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> thirdPartyVoiceMailMailboxURL;
    protected SIPAuthenticationUserNamePassword sipAuthenticationData;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der groupExternalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupExternalId() {
        return groupExternalId;
    }

    /**
     * Legt den Wert der groupExternalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupExternalId(String value) {
        this.groupExternalId = value;
    }

    /**
     * Ruft den Wert der groupProperties-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ConsolidatedGroupProperties }
     *     
     */
    public ConsolidatedGroupProperties getGroupProperties() {
        return groupProperties;
    }

    /**
     * Legt den Wert der groupProperties-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsolidatedGroupProperties }
     *     
     */
    public void setGroupProperties(ConsolidatedGroupProperties value) {
        this.groupProperties = value;
    }

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der userExternalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserExternalId() {
        return userExternalId;
    }

    /**
     * Legt den Wert der userExternalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserExternalId(String value) {
        this.userExternalId = value;
    }

    /**
     * Ruft den Wert der addPhoneNumberToGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAddPhoneNumberToGroup() {
        return addPhoneNumberToGroup;
    }

    /**
     * Legt den Wert der addPhoneNumberToGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAddPhoneNumberToGroup(Boolean value) {
        this.addPhoneNumberToGroup = value;
    }

    /**
     * Ruft den Wert der lastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Legt den Wert der lastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Ruft den Wert der firstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Legt den Wert der firstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Ruft den Wert der callingLineIdLastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdLastName() {
        return callingLineIdLastName;
    }

    /**
     * Legt den Wert der callingLineIdLastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdLastName(String value) {
        this.callingLineIdLastName = value;
    }

    /**
     * Ruft den Wert der callingLineIdFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdFirstName() {
        return callingLineIdFirstName;
    }

    /**
     * Legt den Wert der callingLineIdFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdFirstName(String value) {
        this.callingLineIdFirstName = value;
    }

    /**
     * Ruft den Wert der nameDialingName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NameDialingName }
     *     
     */
    public NameDialingName getNameDialingName() {
        return nameDialingName;
    }

    /**
     * Legt den Wert der nameDialingName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NameDialingName }
     *     
     */
    public void setNameDialingName(NameDialingName value) {
        this.nameDialingName = value;
    }

    /**
     * Ruft den Wert der hiraganaLastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHiraganaLastName() {
        return hiraganaLastName;
    }

    /**
     * Legt den Wert der hiraganaLastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHiraganaLastName(String value) {
        this.hiraganaLastName = value;
    }

    /**
     * Ruft den Wert der hiraganaFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHiraganaFirstName() {
        return hiraganaFirstName;
    }

    /**
     * Legt den Wert der hiraganaFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHiraganaFirstName(String value) {
        this.hiraganaFirstName = value;
    }

    /**
     * Ruft den Wert der phoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Legt den Wert der phoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber(String value) {
        this.phoneNumber = value;
    }

    /**
     * Gets the value of the alternateUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the alternateUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateUserIdEntry }
     * 
     * 
     * @return
     *     The value of the alternateUserId property.
     */
    public List<AlternateUserIdEntry> getAlternateUserId() {
        if (alternateUserId == null) {
            alternateUserId = new ArrayList<>();
        }
        return this.alternateUserId;
    }

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtension(String value) {
        this.extension = value;
    }

    /**
     * Ruft den Wert der callingLineIdPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdPhoneNumber() {
        return callingLineIdPhoneNumber;
    }

    /**
     * Legt den Wert der callingLineIdPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdPhoneNumber(String value) {
        this.callingLineIdPhoneNumber = value;
    }

    /**
     * Ruft den Wert der password-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Legt den Wert der password-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Ruft den Wert der passcode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasscode() {
        return passcode;
    }

    /**
     * Legt den Wert der passcode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasscode(String value) {
        this.passcode = value;
    }

    /**
     * Ruft den Wert der department-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DepartmentKey }
     *     
     */
    public DepartmentKey getDepartment() {
        return department;
    }

    /**
     * Legt den Wert der department-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DepartmentKey }
     *     
     */
    public void setDepartment(DepartmentKey value) {
        this.department = value;
    }

    /**
     * Ruft den Wert der language-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Legt den Wert der language-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Ruft den Wert der timeZone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * Legt den Wert der timeZone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeZone(String value) {
        this.timeZone = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the alias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the alias property.
     */
    public List<String> getAlias() {
        if (alias == null) {
            alias = new ArrayList<>();
        }
        return this.alias;
    }

    /**
     * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ConsolidatedAccessDeviceMultipleIdentityEndpointAndContactAdd }
     *     
     */
    public ConsolidatedAccessDeviceMultipleIdentityEndpointAndContactAdd getAccessDeviceEndpoint() {
        return accessDeviceEndpoint;
    }

    /**
     * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsolidatedAccessDeviceMultipleIdentityEndpointAndContactAdd }
     *     
     */
    public void setAccessDeviceEndpoint(ConsolidatedAccessDeviceMultipleIdentityEndpointAndContactAdd value) {
        this.accessDeviceEndpoint = value;
    }

    /**
     * Ruft den Wert der trunkAddressing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkAddressingMultipleContactAdd }
     *     
     */
    public TrunkAddressingMultipleContactAdd getTrunkAddressing() {
        return trunkAddressing;
    }

    /**
     * Legt den Wert der trunkAddressing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkAddressingMultipleContactAdd }
     *     
     */
    public void setTrunkAddressing(TrunkAddressingMultipleContactAdd value) {
        this.trunkAddressing = value;
    }

    /**
     * Gets the value of the sharedCallAppearanceAccessDeviceEndpoint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the sharedCallAppearanceAccessDeviceEndpoint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSharedCallAppearanceAccessDeviceEndpoint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint }
     * 
     * 
     * @return
     *     The value of the sharedCallAppearanceAccessDeviceEndpoint property.
     */
    public List<ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint> getSharedCallAppearanceAccessDeviceEndpoint() {
        if (sharedCallAppearanceAccessDeviceEndpoint == null) {
            sharedCallAppearanceAccessDeviceEndpoint = new ArrayList<>();
        }
        return this.sharedCallAppearanceAccessDeviceEndpoint;
    }

    /**
     * Ruft den Wert der title-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Legt den Wert der title-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Ruft den Wert der pagerPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPagerPhoneNumber() {
        return pagerPhoneNumber;
    }

    /**
     * Legt den Wert der pagerPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPagerPhoneNumber(String value) {
        this.pagerPhoneNumber = value;
    }

    /**
     * Ruft den Wert der mobilePhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    /**
     * Legt den Wert der mobilePhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobilePhoneNumber(String value) {
        this.mobilePhoneNumber = value;
    }

    /**
     * Ruft den Wert der emailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Legt den Wert der emailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Ruft den Wert der yahooId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYahooId() {
        return yahooId;
    }

    /**
     * Legt den Wert der yahooId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYahooId(String value) {
        this.yahooId = value;
    }

    /**
     * Ruft den Wert der addressLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressLocation() {
        return addressLocation;
    }

    /**
     * Legt den Wert der addressLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressLocation(String value) {
        this.addressLocation = value;
    }

    /**
     * Ruft den Wert der address-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StreetAddress }
     *     
     */
    public StreetAddress getAddress() {
        return address;
    }

    /**
     * Legt den Wert der address-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetAddress }
     *     
     */
    public void setAddress(StreetAddress value) {
        this.address = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Gets the value of the userService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the userService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConsolidatedUserServiceAssignment }
     * 
     * 
     * @return
     *     The value of the userService property.
     */
    public List<ConsolidatedUserServiceAssignment> getUserService() {
        if (userService == null) {
            userService = new ArrayList<>();
        }
        return this.userService;
    }

    /**
     * Gets the value of the servicePack property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the servicePack property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicePack().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConsolidatedServicePackAssignment }
     * 
     * 
     * @return
     *     The value of the servicePack property.
     */
    public List<ConsolidatedServicePackAssignment> getServicePack() {
        if (servicePack == null) {
            servicePack = new ArrayList<>();
        }
        return this.servicePack;
    }

    /**
     * Ruft den Wert der thirdPartyVoiceMailServerSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ThirdPartyVoiceMailSupportServerSelection }
     *     
     */
    public ThirdPartyVoiceMailSupportServerSelection getThirdPartyVoiceMailServerSelection() {
        return thirdPartyVoiceMailServerSelection;
    }

    /**
     * Legt den Wert der thirdPartyVoiceMailServerSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ThirdPartyVoiceMailSupportServerSelection }
     *     
     */
    public void setThirdPartyVoiceMailServerSelection(ThirdPartyVoiceMailSupportServerSelection value) {
        this.thirdPartyVoiceMailServerSelection = value;
    }

    /**
     * Ruft den Wert der thirdPartyVoiceMailServerUserServer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getThirdPartyVoiceMailServerUserServer() {
        return thirdPartyVoiceMailServerUserServer;
    }

    /**
     * Legt den Wert der thirdPartyVoiceMailServerUserServer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setThirdPartyVoiceMailServerUserServer(JAXBElement<String> value) {
        this.thirdPartyVoiceMailServerUserServer = value;
    }

    /**
     * Ruft den Wert der thirdPartyVoiceMailServerMailboxIdType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ThirdPartyVoiceMailSupportMailboxIdType }
     *     
     */
    public ThirdPartyVoiceMailSupportMailboxIdType getThirdPartyVoiceMailServerMailboxIdType() {
        return thirdPartyVoiceMailServerMailboxIdType;
    }

    /**
     * Legt den Wert der thirdPartyVoiceMailServerMailboxIdType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ThirdPartyVoiceMailSupportMailboxIdType }
     *     
     */
    public void setThirdPartyVoiceMailServerMailboxIdType(ThirdPartyVoiceMailSupportMailboxIdType value) {
        this.thirdPartyVoiceMailServerMailboxIdType = value;
    }

    /**
     * Ruft den Wert der thirdPartyVoiceMailMailboxURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getThirdPartyVoiceMailMailboxURL() {
        return thirdPartyVoiceMailMailboxURL;
    }

    /**
     * Legt den Wert der thirdPartyVoiceMailMailboxURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setThirdPartyVoiceMailMailboxURL(JAXBElement<String> value) {
        this.thirdPartyVoiceMailMailboxURL = value;
    }

    /**
     * Ruft den Wert der sipAuthenticationData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SIPAuthenticationUserNamePassword }
     *     
     */
    public SIPAuthenticationUserNamePassword getSipAuthenticationData() {
        return sipAuthenticationData;
    }

    /**
     * Legt den Wert der sipAuthenticationData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SIPAuthenticationUserNamePassword }
     *     
     */
    public void setSipAuthenticationData(SIPAuthenticationUserNamePassword value) {
        this.sipAuthenticationData = value;
    }

}
