//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Virtual On-Net User Range.
 *       
 * 
 * <p>Java-Klasse für VirtualOnNetUserRange complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="VirtualOnNetUserRange">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="dnRange" type="{}DNRange"/>
 *         <element name="extensionRange" type="{}ExtensionRange17"/>
 *         <element name="firstName" type="{}FirstName"/>
 *         <element name="lastName" type="{}LastName"/>
 *         <element name="callingLineIdFirstName" type="{}CallingLineIdFirstName"/>
 *         <element name="callingLineIdLastName" type="{}CallingLineIdLastName"/>
 *         <element name="virtualOnNetCallTypeName" type="{}VirtualOnNetCallTypeName"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VirtualOnNetUserRange", propOrder = {
    "dnRange",
    "extensionRange",
    "firstName",
    "lastName",
    "callingLineIdFirstName",
    "callingLineIdLastName",
    "virtualOnNetCallTypeName"
})
public class VirtualOnNetUserRange {

    @XmlElement(required = true)
    protected DNRange dnRange;
    @XmlElement(required = true)
    protected ExtensionRange17 extensionRange;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String firstName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String lastName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdFirstName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdLastName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String virtualOnNetCallTypeName;

    /**
     * Ruft den Wert der dnRange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DNRange }
     *     
     */
    public DNRange getDnRange() {
        return dnRange;
    }

    /**
     * Legt den Wert der dnRange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DNRange }
     *     
     */
    public void setDnRange(DNRange value) {
        this.dnRange = value;
    }

    /**
     * Ruft den Wert der extensionRange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtensionRange17 }
     *     
     */
    public ExtensionRange17 getExtensionRange() {
        return extensionRange;
    }

    /**
     * Legt den Wert der extensionRange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtensionRange17 }
     *     
     */
    public void setExtensionRange(ExtensionRange17 value) {
        this.extensionRange = value;
    }

    /**
     * Ruft den Wert der firstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Legt den Wert der firstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Ruft den Wert der lastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Legt den Wert der lastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Ruft den Wert der callingLineIdFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdFirstName() {
        return callingLineIdFirstName;
    }

    /**
     * Legt den Wert der callingLineIdFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdFirstName(String value) {
        this.callingLineIdFirstName = value;
    }

    /**
     * Ruft den Wert der callingLineIdLastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdLastName() {
        return callingLineIdLastName;
    }

    /**
     * Legt den Wert der callingLineIdLastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdLastName(String value) {
        this.callingLineIdLastName = value;
    }

    /**
     * Ruft den Wert der virtualOnNetCallTypeName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVirtualOnNetCallTypeName() {
        return virtualOnNetCallTypeName;
    }

    /**
     * Legt den Wert der virtualOnNetCallTypeName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVirtualOnNetCallTypeName(String value) {
        this.virtualOnNetCallTypeName = value;
    }

}
