//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with Voice Messaging.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemVoiceMessageSummaryUpdateModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemVoiceMessageSummaryUpdateModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="sendSavedAndUrgentMWIOnNotification" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="sendMessageSummaryUpdateOnRegister" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="minTimeBetweenMWIOnRegister" type="{}VoiceMessageSummaryUpdateSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemVoiceMessageSummaryUpdateModifyRequest", propOrder = {
    "sendSavedAndUrgentMWIOnNotification",
    "sendMessageSummaryUpdateOnRegister",
    "minTimeBetweenMWIOnRegister"
})
public class SystemVoiceMessageSummaryUpdateModifyRequest
    extends OCIRequest
{

    protected Boolean sendSavedAndUrgentMWIOnNotification;
    protected Boolean sendMessageSummaryUpdateOnRegister;
    protected Integer minTimeBetweenMWIOnRegister;

    /**
     * Ruft den Wert der sendSavedAndUrgentMWIOnNotification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendSavedAndUrgentMWIOnNotification() {
        return sendSavedAndUrgentMWIOnNotification;
    }

    /**
     * Legt den Wert der sendSavedAndUrgentMWIOnNotification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendSavedAndUrgentMWIOnNotification(Boolean value) {
        this.sendSavedAndUrgentMWIOnNotification = value;
    }

    /**
     * Ruft den Wert der sendMessageSummaryUpdateOnRegister-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendMessageSummaryUpdateOnRegister() {
        return sendMessageSummaryUpdateOnRegister;
    }

    /**
     * Legt den Wert der sendMessageSummaryUpdateOnRegister-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendMessageSummaryUpdateOnRegister(Boolean value) {
        this.sendMessageSummaryUpdateOnRegister = value;
    }

    /**
     * Ruft den Wert der minTimeBetweenMWIOnRegister-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinTimeBetweenMWIOnRegister() {
        return minTimeBetweenMWIOnRegister;
    }

    /**
     * Legt den Wert der minTimeBetweenMWIOnRegister-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinTimeBetweenMWIOnRegister(Integer value) {
        this.minTimeBetweenMWIOnRegister = value;
    }

}
