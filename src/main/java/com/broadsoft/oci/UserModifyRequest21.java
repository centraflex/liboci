//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a user. 
 *         When oldPassword is specified, all password rule applies. If oldPassword in not specified,
 *         any password rule related to old password does not apply.         
 *         The request will fail if officeZoneName or primaryZoneName is present but the Location-Based Calling Restrictions service is not assigned to the user.
 *         The response is either SuccessResponse or ErrorResponse.
 *         The following data elements are only used in AS data mode:
 *           contact[2]-contact[5]
 *           nameDialingName
 *           alternateUserIdList
 *         The following elements are only used in AS data mode and will fail in XS data mode:
 *           trunkAddressing
 *         The following elements are only used in XS data mode and ignored in AS data mode:
 *           allowVideo
 *         
 *         The allowVideo element can only be used by a system administrator.
 *         The impId and impPassword are accepted when the Third-Party IMP service is assigned to the user; 
 *         when Integrated IMP service is assigned to the user and active, only the impPassword is accepted; 
 *         all other cases, the request fails if either field is changed.
 *         
 *         Replaced by : UserAddRequest22 in AS mode
 *       
 * 
 * <p>Java-Klasse für UserModifyRequest21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserModifyRequest21">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="lastName" type="{}LastName" minOccurs="0"/>
 *         <element name="firstName" type="{}FirstName" minOccurs="0"/>
 *         <element name="callingLineIdLastName" type="{}CallingLineIdLastName" minOccurs="0"/>
 *         <element name="callingLineIdFirstName" type="{}CallingLineIdFirstName" minOccurs="0"/>
 *         <element name="nameDialingName" type="{}NameDialingName" minOccurs="0"/>
 *         <element name="hiraganaLastName" type="{}HiraganaLastName" minOccurs="0"/>
 *         <element name="hiraganaFirstName" type="{}HiraganaFirstName" minOccurs="0"/>
 *         <element name="phoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="extension" type="{}Extension17" minOccurs="0"/>
 *         <element name="callingLineIdPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="oldPassword" type="{}Password" minOccurs="0"/>
 *         <element name="newPassword" type="{}Password" minOccurs="0"/>
 *         <element name="department" type="{}DepartmentKey" minOccurs="0"/>
 *         <element name="language" type="{}Language" minOccurs="0"/>
 *         <element name="timeZone" type="{}TimeZone" minOccurs="0"/>
 *         <element name="sipAliasList" type="{}ReplacementSIPAliasList" minOccurs="0"/>
 *         <element name="endpoint" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <choice>
 *                   <element name="accessDeviceEndpoint" type="{}AccessDeviceMultipleIdentityAndContactEndpointModify"/>
 *                   <element name="trunkAddressing" type="{}TrunkAddressingMultipleContactModify"/>
 *                 </choice>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="title" type="{}Title" minOccurs="0"/>
 *         <element name="pagerPhoneNumber" type="{}InformationalDN" minOccurs="0"/>
 *         <element name="mobilePhoneNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="emailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="yahooId" type="{}YahooId" minOccurs="0"/>
 *         <element name="addressLocation" type="{}AddressLocation" minOccurs="0"/>
 *         <element name="address" type="{}StreetAddress" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="officeZoneName" type="{}OfficeZoneName" minOccurs="0"/>
 *         <element name="primaryZoneName" type="{}ZoneName" minOccurs="0"/>
 *         <element name="impId" type="{}IMPUserId" minOccurs="0"/>
 *         <element name="impPassword" type="{}Password" minOccurs="0"/>
 *         <element name="alternateUserIdList" type="{}ReplacementAlternateUserIdEntryList" minOccurs="0"/>
 *         <element name="allowVideo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserModifyRequest21", propOrder = {
    "userId",
    "lastName",
    "firstName",
    "callingLineIdLastName",
    "callingLineIdFirstName",
    "nameDialingName",
    "hiraganaLastName",
    "hiraganaFirstName",
    "phoneNumber",
    "extension",
    "callingLineIdPhoneNumber",
    "oldPassword",
    "newPassword",
    "department",
    "language",
    "timeZone",
    "sipAliasList",
    "endpoint",
    "title",
    "pagerPhoneNumber",
    "mobilePhoneNumber",
    "emailAddress",
    "yahooId",
    "addressLocation",
    "address",
    "networkClassOfService",
    "officeZoneName",
    "primaryZoneName",
    "impId",
    "impPassword",
    "alternateUserIdList",
    "allowVideo"
})
public class UserModifyRequest21
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String lastName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String firstName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdLastName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdFirstName;
    @XmlElementRef(name = "nameDialingName", type = JAXBElement.class, required = false)
    protected JAXBElement<NameDialingName> nameDialingName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String hiraganaLastName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String hiraganaFirstName;
    @XmlElementRef(name = "phoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> phoneNumber;
    @XmlElementRef(name = "extension", type = JAXBElement.class, required = false)
    protected JAXBElement<String> extension;
    @XmlElementRef(name = "callingLineIdPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> callingLineIdPhoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String oldPassword;
    @XmlElementRef(name = "newPassword", type = JAXBElement.class, required = false)
    protected JAXBElement<String> newPassword;
    @XmlElementRef(name = "department", type = JAXBElement.class, required = false)
    protected JAXBElement<DepartmentKey> department;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String language;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeZone;
    @XmlElementRef(name = "sipAliasList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementSIPAliasList> sipAliasList;
    @XmlElementRef(name = "endpoint", type = JAXBElement.class, required = false)
    protected JAXBElement<UserModifyRequest21 .Endpoint> endpoint;
    @XmlElementRef(name = "title", type = JAXBElement.class, required = false)
    protected JAXBElement<String> title;
    @XmlElementRef(name = "pagerPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pagerPhoneNumber;
    @XmlElementRef(name = "mobilePhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mobilePhoneNumber;
    @XmlElementRef(name = "emailAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emailAddress;
    @XmlElementRef(name = "yahooId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> yahooId;
    @XmlElementRef(name = "addressLocation", type = JAXBElement.class, required = false)
    protected JAXBElement<String> addressLocation;
    protected StreetAddress address;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String officeZoneName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String primaryZoneName;
    @XmlElementRef(name = "impId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> impId;
    @XmlElementRef(name = "impPassword", type = JAXBElement.class, required = false)
    protected JAXBElement<String> impPassword;
    @XmlElementRef(name = "alternateUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementAlternateUserIdEntryList> alternateUserIdList;
    protected Boolean allowVideo;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der lastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Legt den Wert der lastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Ruft den Wert der firstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Legt den Wert der firstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Ruft den Wert der callingLineIdLastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdLastName() {
        return callingLineIdLastName;
    }

    /**
     * Legt den Wert der callingLineIdLastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdLastName(String value) {
        this.callingLineIdLastName = value;
    }

    /**
     * Ruft den Wert der callingLineIdFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdFirstName() {
        return callingLineIdFirstName;
    }

    /**
     * Legt den Wert der callingLineIdFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdFirstName(String value) {
        this.callingLineIdFirstName = value;
    }

    /**
     * Ruft den Wert der nameDialingName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link NameDialingName }{@code >}
     *     
     */
    public JAXBElement<NameDialingName> getNameDialingName() {
        return nameDialingName;
    }

    /**
     * Legt den Wert der nameDialingName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link NameDialingName }{@code >}
     *     
     */
    public void setNameDialingName(JAXBElement<NameDialingName> value) {
        this.nameDialingName = value;
    }

    /**
     * Ruft den Wert der hiraganaLastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHiraganaLastName() {
        return hiraganaLastName;
    }

    /**
     * Legt den Wert der hiraganaLastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHiraganaLastName(String value) {
        this.hiraganaLastName = value;
    }

    /**
     * Ruft den Wert der hiraganaFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHiraganaFirstName() {
        return hiraganaFirstName;
    }

    /**
     * Legt den Wert der hiraganaFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHiraganaFirstName(String value) {
        this.hiraganaFirstName = value;
    }

    /**
     * Ruft den Wert der phoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Legt den Wert der phoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPhoneNumber(JAXBElement<String> value) {
        this.phoneNumber = value;
    }

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExtension(JAXBElement<String> value) {
        this.extension = value;
    }

    /**
     * Ruft den Wert der callingLineIdPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallingLineIdPhoneNumber() {
        return callingLineIdPhoneNumber;
    }

    /**
     * Legt den Wert der callingLineIdPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallingLineIdPhoneNumber(JAXBElement<String> value) {
        this.callingLineIdPhoneNumber = value;
    }

    /**
     * Ruft den Wert der oldPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldPassword() {
        return oldPassword;
    }

    /**
     * Legt den Wert der oldPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldPassword(String value) {
        this.oldPassword = value;
    }

    /**
     * Ruft den Wert der newPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNewPassword() {
        return newPassword;
    }

    /**
     * Legt den Wert der newPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNewPassword(JAXBElement<String> value) {
        this.newPassword = value;
    }

    /**
     * Ruft den Wert der department-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DepartmentKey }{@code >}
     *     
     */
    public JAXBElement<DepartmentKey> getDepartment() {
        return department;
    }

    /**
     * Legt den Wert der department-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DepartmentKey }{@code >}
     *     
     */
    public void setDepartment(JAXBElement<DepartmentKey> value) {
        this.department = value;
    }

    /**
     * Ruft den Wert der language-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Legt den Wert der language-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Ruft den Wert der timeZone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * Legt den Wert der timeZone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeZone(String value) {
        this.timeZone = value;
    }

    /**
     * Ruft den Wert der sipAliasList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementSIPAliasList }{@code >}
     *     
     */
    public JAXBElement<ReplacementSIPAliasList> getSipAliasList() {
        return sipAliasList;
    }

    /**
     * Legt den Wert der sipAliasList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementSIPAliasList }{@code >}
     *     
     */
    public void setSipAliasList(JAXBElement<ReplacementSIPAliasList> value) {
        this.sipAliasList = value;
    }

    /**
     * Ruft den Wert der endpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link UserModifyRequest21 .Endpoint }{@code >}
     *     
     */
    public JAXBElement<UserModifyRequest21 .Endpoint> getEndpoint() {
        return endpoint;
    }

    /**
     * Legt den Wert der endpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link UserModifyRequest21 .Endpoint }{@code >}
     *     
     */
    public void setEndpoint(JAXBElement<UserModifyRequest21 .Endpoint> value) {
        this.endpoint = value;
    }

    /**
     * Ruft den Wert der title-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTitle() {
        return title;
    }

    /**
     * Legt den Wert der title-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTitle(JAXBElement<String> value) {
        this.title = value;
    }

    /**
     * Ruft den Wert der pagerPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPagerPhoneNumber() {
        return pagerPhoneNumber;
    }

    /**
     * Legt den Wert der pagerPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPagerPhoneNumber(JAXBElement<String> value) {
        this.pagerPhoneNumber = value;
    }

    /**
     * Ruft den Wert der mobilePhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    /**
     * Legt den Wert der mobilePhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMobilePhoneNumber(JAXBElement<String> value) {
        this.mobilePhoneNumber = value;
    }

    /**
     * Ruft den Wert der emailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmailAddress() {
        return emailAddress;
    }

    /**
     * Legt den Wert der emailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmailAddress(JAXBElement<String> value) {
        this.emailAddress = value;
    }

    /**
     * Ruft den Wert der yahooId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getYahooId() {
        return yahooId;
    }

    /**
     * Legt den Wert der yahooId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setYahooId(JAXBElement<String> value) {
        this.yahooId = value;
    }

    /**
     * Ruft den Wert der addressLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAddressLocation() {
        return addressLocation;
    }

    /**
     * Legt den Wert der addressLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAddressLocation(JAXBElement<String> value) {
        this.addressLocation = value;
    }

    /**
     * Ruft den Wert der address-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StreetAddress }
     *     
     */
    public StreetAddress getAddress() {
        return address;
    }

    /**
     * Legt den Wert der address-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetAddress }
     *     
     */
    public void setAddress(StreetAddress value) {
        this.address = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Ruft den Wert der officeZoneName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficeZoneName() {
        return officeZoneName;
    }

    /**
     * Legt den Wert der officeZoneName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficeZoneName(String value) {
        this.officeZoneName = value;
    }

    /**
     * Ruft den Wert der primaryZoneName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryZoneName() {
        return primaryZoneName;
    }

    /**
     * Legt den Wert der primaryZoneName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryZoneName(String value) {
        this.primaryZoneName = value;
    }

    /**
     * Ruft den Wert der impId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getImpId() {
        return impId;
    }

    /**
     * Legt den Wert der impId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setImpId(JAXBElement<String> value) {
        this.impId = value;
    }

    /**
     * Ruft den Wert der impPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getImpPassword() {
        return impPassword;
    }

    /**
     * Legt den Wert der impPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setImpPassword(JAXBElement<String> value) {
        this.impPassword = value;
    }

    /**
     * Ruft den Wert der alternateUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementAlternateUserIdEntryList }{@code >}
     *     
     */
    public JAXBElement<ReplacementAlternateUserIdEntryList> getAlternateUserIdList() {
        return alternateUserIdList;
    }

    /**
     * Legt den Wert der alternateUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementAlternateUserIdEntryList }{@code >}
     *     
     */
    public void setAlternateUserIdList(JAXBElement<ReplacementAlternateUserIdEntryList> value) {
        this.alternateUserIdList = value;
    }

    /**
     * Ruft den Wert der allowVideo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowVideo() {
        return allowVideo;
    }

    /**
     * Legt den Wert der allowVideo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowVideo(Boolean value) {
        this.allowVideo = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <choice>
     *         <element name="accessDeviceEndpoint" type="{}AccessDeviceMultipleIdentityAndContactEndpointModify"/>
     *         <element name="trunkAddressing" type="{}TrunkAddressingMultipleContactModify"/>
     *       </choice>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accessDeviceEndpoint",
        "trunkAddressing"
    })
    public static class Endpoint {

        protected AccessDeviceMultipleIdentityAndContactEndpointModify accessDeviceEndpoint;
        protected TrunkAddressingMultipleContactModify trunkAddressing;

        /**
         * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AccessDeviceMultipleIdentityAndContactEndpointModify }
         *     
         */
        public AccessDeviceMultipleIdentityAndContactEndpointModify getAccessDeviceEndpoint() {
            return accessDeviceEndpoint;
        }

        /**
         * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AccessDeviceMultipleIdentityAndContactEndpointModify }
         *     
         */
        public void setAccessDeviceEndpoint(AccessDeviceMultipleIdentityAndContactEndpointModify value) {
            this.accessDeviceEndpoint = value;
        }

        /**
         * Ruft den Wert der trunkAddressing-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link TrunkAddressingMultipleContactModify }
         *     
         */
        public TrunkAddressingMultipleContactModify getTrunkAddressing() {
            return trunkAddressing;
        }

        /**
         * Legt den Wert der trunkAddressing-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link TrunkAddressingMultipleContactModify }
         *     
         */
        public void setTrunkAddressing(TrunkAddressingMultipleContactModify value) {
            this.trunkAddressing = value;
        }

    }

}
