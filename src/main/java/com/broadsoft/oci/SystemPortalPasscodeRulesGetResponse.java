//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemPortalPasscodeRulesGetRequest.
 *         Contains the system passcode rules setting.
 *         
 *         Replaced by: SystemPortalPasscodeRulesGetResponse19
 *       
 * 
 * <p>Java-Klasse für SystemPortalPasscodeRulesGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPortalPasscodeRulesGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="disallowRepeatedDigits" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disallowUserNumber" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disallowReversedUserNumber" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disallowOldPasscode" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disallowReversedOldPasscode" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minCodeLength" type="{}PasscodeMinLength"/>
 *         <element name="maxCodeLength" type="{}PasscodeMaxLength"/>
 *         <element name="disableLoginAfterMaxFailedLoginAttempts" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxFailedLoginAttempts" type="{}PortalMaxFailedLoginAttempts" minOccurs="0"/>
 *         <element name="expirePassword" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="passcodeExpiresDays" type="{}PasscodeExpiresDays" minOccurs="0"/>
 *         <element name="sendLoginDisabledNotifyEmail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="loginDisabledNotifyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="defaultPassword" type="{}Password"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPortalPasscodeRulesGetResponse", propOrder = {
    "disallowRepeatedDigits",
    "disallowUserNumber",
    "disallowReversedUserNumber",
    "disallowOldPasscode",
    "disallowReversedOldPasscode",
    "minCodeLength",
    "maxCodeLength",
    "disableLoginAfterMaxFailedLoginAttempts",
    "maxFailedLoginAttempts",
    "expirePassword",
    "passcodeExpiresDays",
    "sendLoginDisabledNotifyEmail",
    "loginDisabledNotifyEmailAddress",
    "defaultPassword"
})
public class SystemPortalPasscodeRulesGetResponse
    extends OCIDataResponse
{

    protected boolean disallowRepeatedDigits;
    protected boolean disallowUserNumber;
    protected boolean disallowReversedUserNumber;
    protected boolean disallowOldPasscode;
    protected boolean disallowReversedOldPasscode;
    protected int minCodeLength;
    protected int maxCodeLength;
    protected boolean disableLoginAfterMaxFailedLoginAttempts;
    protected Integer maxFailedLoginAttempts;
    protected boolean expirePassword;
    protected Integer passcodeExpiresDays;
    protected boolean sendLoginDisabledNotifyEmail;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String loginDisabledNotifyEmailAddress;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultPassword;

    /**
     * Ruft den Wert der disallowRepeatedDigits-Eigenschaft ab.
     * 
     */
    public boolean isDisallowRepeatedDigits() {
        return disallowRepeatedDigits;
    }

    /**
     * Legt den Wert der disallowRepeatedDigits-Eigenschaft fest.
     * 
     */
    public void setDisallowRepeatedDigits(boolean value) {
        this.disallowRepeatedDigits = value;
    }

    /**
     * Ruft den Wert der disallowUserNumber-Eigenschaft ab.
     * 
     */
    public boolean isDisallowUserNumber() {
        return disallowUserNumber;
    }

    /**
     * Legt den Wert der disallowUserNumber-Eigenschaft fest.
     * 
     */
    public void setDisallowUserNumber(boolean value) {
        this.disallowUserNumber = value;
    }

    /**
     * Ruft den Wert der disallowReversedUserNumber-Eigenschaft ab.
     * 
     */
    public boolean isDisallowReversedUserNumber() {
        return disallowReversedUserNumber;
    }

    /**
     * Legt den Wert der disallowReversedUserNumber-Eigenschaft fest.
     * 
     */
    public void setDisallowReversedUserNumber(boolean value) {
        this.disallowReversedUserNumber = value;
    }

    /**
     * Ruft den Wert der disallowOldPasscode-Eigenschaft ab.
     * 
     */
    public boolean isDisallowOldPasscode() {
        return disallowOldPasscode;
    }

    /**
     * Legt den Wert der disallowOldPasscode-Eigenschaft fest.
     * 
     */
    public void setDisallowOldPasscode(boolean value) {
        this.disallowOldPasscode = value;
    }

    /**
     * Ruft den Wert der disallowReversedOldPasscode-Eigenschaft ab.
     * 
     */
    public boolean isDisallowReversedOldPasscode() {
        return disallowReversedOldPasscode;
    }

    /**
     * Legt den Wert der disallowReversedOldPasscode-Eigenschaft fest.
     * 
     */
    public void setDisallowReversedOldPasscode(boolean value) {
        this.disallowReversedOldPasscode = value;
    }

    /**
     * Ruft den Wert der minCodeLength-Eigenschaft ab.
     * 
     */
    public int getMinCodeLength() {
        return minCodeLength;
    }

    /**
     * Legt den Wert der minCodeLength-Eigenschaft fest.
     * 
     */
    public void setMinCodeLength(int value) {
        this.minCodeLength = value;
    }

    /**
     * Ruft den Wert der maxCodeLength-Eigenschaft ab.
     * 
     */
    public int getMaxCodeLength() {
        return maxCodeLength;
    }

    /**
     * Legt den Wert der maxCodeLength-Eigenschaft fest.
     * 
     */
    public void setMaxCodeLength(int value) {
        this.maxCodeLength = value;
    }

    /**
     * Ruft den Wert der disableLoginAfterMaxFailedLoginAttempts-Eigenschaft ab.
     * 
     */
    public boolean isDisableLoginAfterMaxFailedLoginAttempts() {
        return disableLoginAfterMaxFailedLoginAttempts;
    }

    /**
     * Legt den Wert der disableLoginAfterMaxFailedLoginAttempts-Eigenschaft fest.
     * 
     */
    public void setDisableLoginAfterMaxFailedLoginAttempts(boolean value) {
        this.disableLoginAfterMaxFailedLoginAttempts = value;
    }

    /**
     * Ruft den Wert der maxFailedLoginAttempts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxFailedLoginAttempts() {
        return maxFailedLoginAttempts;
    }

    /**
     * Legt den Wert der maxFailedLoginAttempts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxFailedLoginAttempts(Integer value) {
        this.maxFailedLoginAttempts = value;
    }

    /**
     * Ruft den Wert der expirePassword-Eigenschaft ab.
     * 
     */
    public boolean isExpirePassword() {
        return expirePassword;
    }

    /**
     * Legt den Wert der expirePassword-Eigenschaft fest.
     * 
     */
    public void setExpirePassword(boolean value) {
        this.expirePassword = value;
    }

    /**
     * Ruft den Wert der passcodeExpiresDays-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPasscodeExpiresDays() {
        return passcodeExpiresDays;
    }

    /**
     * Legt den Wert der passcodeExpiresDays-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPasscodeExpiresDays(Integer value) {
        this.passcodeExpiresDays = value;
    }

    /**
     * Ruft den Wert der sendLoginDisabledNotifyEmail-Eigenschaft ab.
     * 
     */
    public boolean isSendLoginDisabledNotifyEmail() {
        return sendLoginDisabledNotifyEmail;
    }

    /**
     * Legt den Wert der sendLoginDisabledNotifyEmail-Eigenschaft fest.
     * 
     */
    public void setSendLoginDisabledNotifyEmail(boolean value) {
        this.sendLoginDisabledNotifyEmail = value;
    }

    /**
     * Ruft den Wert der loginDisabledNotifyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoginDisabledNotifyEmailAddress() {
        return loginDisabledNotifyEmailAddress;
    }

    /**
     * Legt den Wert der loginDisabledNotifyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoginDisabledNotifyEmailAddress(String value) {
        this.loginDisabledNotifyEmailAddress = value;
    }

    /**
     * Ruft den Wert der defaultPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultPassword() {
        return defaultPassword;
    }

    /**
     * Legt den Wert der defaultPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultPassword(String value) {
        this.defaultPassword = value;
    }

}
