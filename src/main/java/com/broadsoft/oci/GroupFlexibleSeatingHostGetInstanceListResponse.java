//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupFlexibleSeatingHostGetInstanceListRequest.
 *         Contains a table with column headings:
 *         "Service User Id", "Name", "Phone Number", "Extension", "Department", "Is Active".
 *         The column value for "Is Active" can either be true, or false.
 *       
 * 
 * <p>Java-Klasse für GroupFlexibleSeatingHostGetInstanceListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupFlexibleSeatingHostGetInstanceListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="flexibleSeatingHostTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupFlexibleSeatingHostGetInstanceListResponse", propOrder = {
    "flexibleSeatingHostTable"
})
public class GroupFlexibleSeatingHostGetInstanceListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable flexibleSeatingHostTable;

    /**
     * Ruft den Wert der flexibleSeatingHostTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getFlexibleSeatingHostTable() {
        return flexibleSeatingHostTable;
    }

    /**
     * Legt den Wert der flexibleSeatingHostTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setFlexibleSeatingHostTable(OCITable value) {
        this.flexibleSeatingHostTable = value;
    }

}
