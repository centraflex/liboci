//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserCallCenterGetSupervisorCallCenterListRequest.
 *         Contains a table with column headings: "Service User Id", "Phone Number", 
 *         "Extension", "Type" and "Priority".
 *       
 * 
 * <p>Java-Klasse für UserCallCenterGetSupervisorCallCenterListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallCenterGetSupervisorCallCenterListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="callCenterTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallCenterGetSupervisorCallCenterListResponse", propOrder = {
    "callCenterTable"
})
public class UserCallCenterGetSupervisorCallCenterListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable callCenterTable;

    /**
     * Ruft den Wert der callCenterTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getCallCenterTable() {
        return callCenterTable;
    }

    /**
     * Legt den Wert der callCenterTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setCallCenterTable(OCITable value) {
        this.callCenterTable = value;
    }

}
