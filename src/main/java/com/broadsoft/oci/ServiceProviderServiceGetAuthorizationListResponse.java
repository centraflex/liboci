//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderServiceGetAuthorizationListRequest.
 *         Contains two tables, one for the group services and one for the user services.
 *         The Group Service table has column headings:
 *         "Service Name", "Authorized", "Assigned", "Limited", "Quantity", "Allocated", "Licensed", "Service Pack Allocation"
 *         The User Service table has column headings:
 *         "Service Name", "Authorized", "Assigned", "Limited", "Quantity", "Allocated", "Licensed", "Service Pack Allocation", "User Assignable", "Service Pack Assignable".
 *       
 * 
 * <p>Java-Klasse für ServiceProviderServiceGetAuthorizationListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderServiceGetAuthorizationListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="groupServicesAuthorizationTable" type="{C}OCITable"/>
 *         <element name="userServicesAuthorizationTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderServiceGetAuthorizationListResponse", propOrder = {
    "groupServicesAuthorizationTable",
    "userServicesAuthorizationTable"
})
public class ServiceProviderServiceGetAuthorizationListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable groupServicesAuthorizationTable;
    @XmlElement(required = true)
    protected OCITable userServicesAuthorizationTable;

    /**
     * Ruft den Wert der groupServicesAuthorizationTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getGroupServicesAuthorizationTable() {
        return groupServicesAuthorizationTable;
    }

    /**
     * Legt den Wert der groupServicesAuthorizationTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setGroupServicesAuthorizationTable(OCITable value) {
        this.groupServicesAuthorizationTable = value;
    }

    /**
     * Ruft den Wert der userServicesAuthorizationTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserServicesAuthorizationTable() {
        return userServicesAuthorizationTable;
    }

    /**
     * Legt den Wert der userServicesAuthorizationTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserServicesAuthorizationTable(OCITable value) {
        this.userServicesAuthorizationTable = value;
    }

}
