//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modifies the system level Location Based Calling Restriction attributes.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemLocationBasedCallingRestrictionsModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemLocationBasedCallingRestrictionsModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="physicalLocationIndicator" type="{}PhysicalLocationIndicator" minOccurs="0"/>
 *         <element name="enforceMscValidation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableOfficeZoneAnnouncement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enhanceOfficeZone" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemLocationBasedCallingRestrictionsModifyRequest", propOrder = {
    "physicalLocationIndicator",
    "enforceMscValidation",
    "enableOfficeZoneAnnouncement",
    "enhanceOfficeZone"
})
public class SystemLocationBasedCallingRestrictionsModifyRequest
    extends OCIRequest
{

    @XmlSchemaType(name = "token")
    protected PhysicalLocationIndicator physicalLocationIndicator;
    protected Boolean enforceMscValidation;
    protected Boolean enableOfficeZoneAnnouncement;
    protected Boolean enhanceOfficeZone;

    /**
     * Ruft den Wert der physicalLocationIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PhysicalLocationIndicator }
     *     
     */
    public PhysicalLocationIndicator getPhysicalLocationIndicator() {
        return physicalLocationIndicator;
    }

    /**
     * Legt den Wert der physicalLocationIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PhysicalLocationIndicator }
     *     
     */
    public void setPhysicalLocationIndicator(PhysicalLocationIndicator value) {
        this.physicalLocationIndicator = value;
    }

    /**
     * Ruft den Wert der enforceMscValidation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnforceMscValidation() {
        return enforceMscValidation;
    }

    /**
     * Legt den Wert der enforceMscValidation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnforceMscValidation(Boolean value) {
        this.enforceMscValidation = value;
    }

    /**
     * Ruft den Wert der enableOfficeZoneAnnouncement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableOfficeZoneAnnouncement() {
        return enableOfficeZoneAnnouncement;
    }

    /**
     * Legt den Wert der enableOfficeZoneAnnouncement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableOfficeZoneAnnouncement(Boolean value) {
        this.enableOfficeZoneAnnouncement = value;
    }

    /**
     * Ruft den Wert der enhanceOfficeZone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnhanceOfficeZone() {
        return enhanceOfficeZone;
    }

    /**
     * Legt den Wert der enhanceOfficeZone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnhanceOfficeZone(Boolean value) {
        this.enhanceOfficeZone = value;
    }

}
