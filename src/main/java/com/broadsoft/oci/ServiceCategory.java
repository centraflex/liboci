//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ServiceCategory.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ServiceCategory">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Basic Call Logs"/>
 *     <enumeration value="Call Transfer"/>
 *     <enumeration value="Call Waiting"/>
 *     <enumeration value="Calling Name Retrieval"/>
 *     <enumeration value="Charge Number"/>
 *     <enumeration value="External Calling Line ID Delivery"/>
 *     <enumeration value="Group Night Forwarding"/>
 *     <enumeration value="Integrated IMP"/>
 *     <enumeration value="Intercept Group"/>
 *     <enumeration value="Internal Calling Line ID Delivery"/>
 *     <enumeration value="Music On Hold"/>
 *     <enumeration value="OMA Presence"/>
 *     <enumeration value="Prepaid"/>
 *     <enumeration value="Push to Talk"/>
 *     <enumeration value="Shared Call Appearance"/>
 *     <enumeration value="Third-Party Voice Mail Support"/>
 *     <enumeration value="User"/>
 *     <enumeration value="Voice Portal Calling"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ServiceCategory")
@XmlEnum
public enum ServiceCategory {

    @XmlEnumValue("Basic Call Logs")
    BASIC_CALL_LOGS("Basic Call Logs"),
    @XmlEnumValue("Call Transfer")
    CALL_TRANSFER("Call Transfer"),
    @XmlEnumValue("Call Waiting")
    CALL_WAITING("Call Waiting"),
    @XmlEnumValue("Calling Name Retrieval")
    CALLING_NAME_RETRIEVAL("Calling Name Retrieval"),
    @XmlEnumValue("Charge Number")
    CHARGE_NUMBER("Charge Number"),
    @XmlEnumValue("External Calling Line ID Delivery")
    EXTERNAL_CALLING_LINE_ID_DELIVERY("External Calling Line ID Delivery"),
    @XmlEnumValue("Group Night Forwarding")
    GROUP_NIGHT_FORWARDING("Group Night Forwarding"),
    @XmlEnumValue("Integrated IMP")
    INTEGRATED_IMP("Integrated IMP"),
    @XmlEnumValue("Intercept Group")
    INTERCEPT_GROUP("Intercept Group"),
    @XmlEnumValue("Internal Calling Line ID Delivery")
    INTERNAL_CALLING_LINE_ID_DELIVERY("Internal Calling Line ID Delivery"),
    @XmlEnumValue("Music On Hold")
    MUSIC_ON_HOLD("Music On Hold"),
    @XmlEnumValue("OMA Presence")
    OMA_PRESENCE("OMA Presence"),
    @XmlEnumValue("Prepaid")
    PREPAID("Prepaid"),
    @XmlEnumValue("Push to Talk")
    PUSH_TO_TALK("Push to Talk"),
    @XmlEnumValue("Shared Call Appearance")
    SHARED_CALL_APPEARANCE("Shared Call Appearance"),
    @XmlEnumValue("Third-Party Voice Mail Support")
    THIRD_PARTY_VOICE_MAIL_SUPPORT("Third-Party Voice Mail Support"),
    @XmlEnumValue("User")
    USER("User"),
    @XmlEnumValue("Voice Portal Calling")
    VOICE_PORTAL_CALLING("Voice Portal Calling");
    private final String value;

    ServiceCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ServiceCategory fromValue(String v) {
        for (ServiceCategory c: ServiceCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
