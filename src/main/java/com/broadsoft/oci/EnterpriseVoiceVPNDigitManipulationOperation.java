//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EnterpriseVoiceVPNDigitManipulationOperation.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="EnterpriseVoiceVPNDigitManipulationOperation">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Prepend"/>
 *     <enumeration value="End"/>
 *     <enumeration value="Overwrite"/>
 *     <enumeration value="Right Trim"/>
 *     <enumeration value="Replace All"/>
 *     <enumeration value="Left Trim"/>
 *     <enumeration value="Append"/>
 *     <enumeration value="Position"/>
 *     <enumeration value="Insert"/>
 *     <enumeration value="Trim"/>
 *     <enumeration value="Delete"/>
 *     <enumeration value="Move"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "EnterpriseVoiceVPNDigitManipulationOperation")
@XmlEnum
public enum EnterpriseVoiceVPNDigitManipulationOperation {

    @XmlEnumValue("Prepend")
    PREPEND("Prepend"),
    @XmlEnumValue("End")
    END("End"),
    @XmlEnumValue("Overwrite")
    OVERWRITE("Overwrite"),
    @XmlEnumValue("Right Trim")
    RIGHT_TRIM("Right Trim"),
    @XmlEnumValue("Replace All")
    REPLACE_ALL("Replace All"),
    @XmlEnumValue("Left Trim")
    LEFT_TRIM("Left Trim"),
    @XmlEnumValue("Append")
    APPEND("Append"),
    @XmlEnumValue("Position")
    POSITION("Position"),
    @XmlEnumValue("Insert")
    INSERT("Insert"),
    @XmlEnumValue("Trim")
    TRIM("Trim"),
    @XmlEnumValue("Delete")
    DELETE("Delete"),
    @XmlEnumValue("Move")
    MOVE("Move");
    private final String value;

    EnterpriseVoiceVPNDigitManipulationOperation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnterpriseVoiceVPNDigitManipulationOperation fromValue(String v) {
        for (EnterpriseVoiceVPNDigitManipulationOperation c: EnterpriseVoiceVPNDigitManipulationOperation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
