//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Attributes of the WebDav protocol when the file repository interface is using WebDav.
 *       
 * 
 * <p>Java-Klasse für FileRepositoryProtocolWebDAV20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="FileRepositoryProtocolWebDAV20">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="secure" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="netAddress" type="{}NetAddress"/>
 *         <element name="extendedFileCaptureSupport" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FileRepositoryProtocolWebDAV20", propOrder = {
    "secure",
    "netAddress",
    "extendedFileCaptureSupport"
})
public class FileRepositoryProtocolWebDAV20 {

    protected boolean secure;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String netAddress;
    protected boolean extendedFileCaptureSupport;

    /**
     * Ruft den Wert der secure-Eigenschaft ab.
     * 
     */
    public boolean isSecure() {
        return secure;
    }

    /**
     * Legt den Wert der secure-Eigenschaft fest.
     * 
     */
    public void setSecure(boolean value) {
        this.secure = value;
    }

    /**
     * Ruft den Wert der netAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetAddress() {
        return netAddress;
    }

    /**
     * Legt den Wert der netAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetAddress(String value) {
        this.netAddress = value;
    }

    /**
     * Ruft den Wert der extendedFileCaptureSupport-Eigenschaft ab.
     * 
     */
    public boolean isExtendedFileCaptureSupport() {
        return extendedFileCaptureSupport;
    }

    /**
     * Legt den Wert der extendedFileCaptureSupport-Eigenschaft fest.
     * 
     */
    public void setExtendedFileCaptureSupport(boolean value) {
        this.extendedFileCaptureSupport = value;
    }

}
