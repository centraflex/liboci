//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserBroadWorksAnywhereGetPhoneNumberPagedSortedListRequest.
 *         The phoneNumberTable contains columns: "Phone Number", "Description", "Activated"
 *       
 * 
 * <p>Java-Klasse für UserBroadWorksAnywhereGetPhoneNumberPagedSortedListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksAnywhereGetPhoneNumberPagedSortedListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="phoneNumberTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksAnywhereGetPhoneNumberPagedSortedListResponse", propOrder = {
    "phoneNumberTable"
})
public class UserBroadWorksAnywhereGetPhoneNumberPagedSortedListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable phoneNumberTable;

    /**
     * Ruft den Wert der phoneNumberTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getPhoneNumberTable() {
        return phoneNumberTable;
    }

    /**
     * Legt den Wert der phoneNumberTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setPhoneNumberTable(OCITable value) {
        this.phoneNumberTable = value;
    }

}
