//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify data for Music On Hold User.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         Replaced by: UserMusicOnHoldUserModifyRequest20 in AS data mode
 *       
 * 
 * <p>Java-Klasse für UserMusicOnHoldUserModifyRequest16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserMusicOnHoldUserModifyRequest16">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="source" type="{}MusicOnHoldUserSourceModify16" minOccurs="0"/>
 *         <element name="useAlternateSourceForInternalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="internalSource" type="{}MusicOnHoldUserSourceModify16" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserMusicOnHoldUserModifyRequest16", propOrder = {
    "userId",
    "source",
    "useAlternateSourceForInternalCalls",
    "internalSource"
})
public class UserMusicOnHoldUserModifyRequest16
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected MusicOnHoldUserSourceModify16 source;
    protected Boolean useAlternateSourceForInternalCalls;
    protected MusicOnHoldUserSourceModify16 internalSource;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der source-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldUserSourceModify16 }
     *     
     */
    public MusicOnHoldUserSourceModify16 getSource() {
        return source;
    }

    /**
     * Legt den Wert der source-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldUserSourceModify16 }
     *     
     */
    public void setSource(MusicOnHoldUserSourceModify16 value) {
        this.source = value;
    }

    /**
     * Ruft den Wert der useAlternateSourceForInternalCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseAlternateSourceForInternalCalls() {
        return useAlternateSourceForInternalCalls;
    }

    /**
     * Legt den Wert der useAlternateSourceForInternalCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseAlternateSourceForInternalCalls(Boolean value) {
        this.useAlternateSourceForInternalCalls = value;
    }

    /**
     * Ruft den Wert der internalSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldUserSourceModify16 }
     *     
     */
    public MusicOnHoldUserSourceModify16 getInternalSource() {
        return internalSource;
    }

    /**
     * Legt den Wert der internalSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldUserSourceModify16 }
     *     
     */
    public void setInternalSource(MusicOnHoldUserSourceModify16 value) {
        this.internalSource = value;
    }

}
