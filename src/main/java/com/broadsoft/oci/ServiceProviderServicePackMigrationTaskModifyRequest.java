//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the properties of a specified service pack migration task.
 *         Modification is only allowed prior to task execution.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderServicePackMigrationTaskModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderServicePackMigrationTaskModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="taskName" type="{}ServicePackMigrationTaskName"/>
 *         <element name="newTaskName" type="{}ServicePackMigrationTaskName" minOccurs="0"/>
 *         <element name="startTimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         <element name="expireAfterNumHours" type="{}ServicePackMigrationExpireAfterNumberOfHours" minOccurs="0"/>
 *         <element name="maxDurationHours" type="{}ServicePackMigrationMaxDurationHours" minOccurs="0"/>
 *         <element name="sendReportEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="reportDeliveryEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="abortOnError" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="abortErrorThreshold" type="{}ServicePackMigrationAbortErrorThreshold" minOccurs="0"/>
 *         <element name="reportAllUsers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="automaticallyIncrementServiceQuantity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderServicePackMigrationTaskModifyRequest", propOrder = {
    "serviceProviderId",
    "taskName",
    "newTaskName",
    "startTimestamp",
    "expireAfterNumHours",
    "maxDurationHours",
    "sendReportEmail",
    "reportDeliveryEmailAddress",
    "abortOnError",
    "abortErrorThreshold",
    "reportAllUsers",
    "automaticallyIncrementServiceQuantity"
})
public class ServiceProviderServicePackMigrationTaskModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String taskName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newTaskName;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startTimestamp;
    protected Integer expireAfterNumHours;
    protected Integer maxDurationHours;
    protected Boolean sendReportEmail;
    @XmlElementRef(name = "reportDeliveryEmailAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> reportDeliveryEmailAddress;
    protected Boolean abortOnError;
    @XmlElementRef(name = "abortErrorThreshold", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> abortErrorThreshold;
    protected Boolean reportAllUsers;
    protected Boolean automaticallyIncrementServiceQuantity;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der taskName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * Legt den Wert der taskName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskName(String value) {
        this.taskName = value;
    }

    /**
     * Ruft den Wert der newTaskName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewTaskName() {
        return newTaskName;
    }

    /**
     * Legt den Wert der newTaskName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewTaskName(String value) {
        this.newTaskName = value;
    }

    /**
     * Ruft den Wert der startTimestamp-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartTimestamp() {
        return startTimestamp;
    }

    /**
     * Legt den Wert der startTimestamp-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartTimestamp(XMLGregorianCalendar value) {
        this.startTimestamp = value;
    }

    /**
     * Ruft den Wert der expireAfterNumHours-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExpireAfterNumHours() {
        return expireAfterNumHours;
    }

    /**
     * Legt den Wert der expireAfterNumHours-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExpireAfterNumHours(Integer value) {
        this.expireAfterNumHours = value;
    }

    /**
     * Ruft den Wert der maxDurationHours-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxDurationHours() {
        return maxDurationHours;
    }

    /**
     * Legt den Wert der maxDurationHours-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxDurationHours(Integer value) {
        this.maxDurationHours = value;
    }

    /**
     * Ruft den Wert der sendReportEmail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendReportEmail() {
        return sendReportEmail;
    }

    /**
     * Legt den Wert der sendReportEmail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendReportEmail(Boolean value) {
        this.sendReportEmail = value;
    }

    /**
     * Ruft den Wert der reportDeliveryEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReportDeliveryEmailAddress() {
        return reportDeliveryEmailAddress;
    }

    /**
     * Legt den Wert der reportDeliveryEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReportDeliveryEmailAddress(JAXBElement<String> value) {
        this.reportDeliveryEmailAddress = value;
    }

    /**
     * Ruft den Wert der abortOnError-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAbortOnError() {
        return abortOnError;
    }

    /**
     * Legt den Wert der abortOnError-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAbortOnError(Boolean value) {
        this.abortOnError = value;
    }

    /**
     * Ruft den Wert der abortErrorThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getAbortErrorThreshold() {
        return abortErrorThreshold;
    }

    /**
     * Legt den Wert der abortErrorThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setAbortErrorThreshold(JAXBElement<Integer> value) {
        this.abortErrorThreshold = value;
    }

    /**
     * Ruft den Wert der reportAllUsers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReportAllUsers() {
        return reportAllUsers;
    }

    /**
     * Legt den Wert der reportAllUsers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReportAllUsers(Boolean value) {
        this.reportAllUsers = value;
    }

    /**
     * Ruft den Wert der automaticallyIncrementServiceQuantity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutomaticallyIncrementServiceQuantity() {
        return automaticallyIncrementServiceQuantity;
    }

    /**
     * Legt den Wert der automaticallyIncrementServiceQuantity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutomaticallyIncrementServiceQuantity(Boolean value) {
        this.automaticallyIncrementServiceQuantity = value;
    }

}
