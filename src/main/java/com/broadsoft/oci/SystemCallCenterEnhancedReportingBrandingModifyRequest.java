//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify the system branding configuration.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCallCenterEnhancedReportingBrandingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallCenterEnhancedReportingBrandingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="brandingChoice" type="{}CallCenterEnhancedReportingSystemBrandingChoice" minOccurs="0"/>
 *         <element name="customBrandingFile" type="{}LabeledFileResource" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallCenterEnhancedReportingBrandingModifyRequest", propOrder = {
    "brandingChoice",
    "customBrandingFile"
})
public class SystemCallCenterEnhancedReportingBrandingModifyRequest
    extends OCIRequest
{

    @XmlSchemaType(name = "token")
    protected CallCenterEnhancedReportingSystemBrandingChoice brandingChoice;
    protected LabeledFileResource customBrandingFile;

    /**
     * Ruft den Wert der brandingChoice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterEnhancedReportingSystemBrandingChoice }
     *     
     */
    public CallCenterEnhancedReportingSystemBrandingChoice getBrandingChoice() {
        return brandingChoice;
    }

    /**
     * Legt den Wert der brandingChoice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterEnhancedReportingSystemBrandingChoice }
     *     
     */
    public void setBrandingChoice(CallCenterEnhancedReportingSystemBrandingChoice value) {
        this.brandingChoice = value;
    }

    /**
     * Ruft den Wert der customBrandingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledFileResource }
     *     
     */
    public LabeledFileResource getCustomBrandingFile() {
        return customBrandingFile;
    }

    /**
     * Legt den Wert der customBrandingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledFileResource }
     *     
     */
    public void setCustomBrandingFile(LabeledFileResource value) {
        this.customBrandingFile = value;
    }

}
