//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DiameterPeerMode.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DiameterPeerMode">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Active"/>
 *     <enumeration value="Standby"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DiameterPeerMode")
@XmlEnum
public enum DiameterPeerMode {

    @XmlEnumValue("Active")
    ACTIVE("Active"),
    @XmlEnumValue("Standby")
    STANDBY("Standby");
    private final String value;

    DiameterPeerMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DiameterPeerMode fromValue(String v) {
        for (DiameterPeerMode c: DiameterPeerMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
