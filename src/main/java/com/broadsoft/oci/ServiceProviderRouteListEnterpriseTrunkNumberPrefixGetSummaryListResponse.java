//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderRouteListEnterpriseTrunkPrefixGetSummaryListRequest.
 *         The column headings are "Number Prefix", "Group Id" ","Is Active", “Extension Range Start” and “Extension Range End”.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderRouteListEnterpriseTrunkNumberPrefixGetSummaryListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderRouteListEnterpriseTrunkNumberPrefixGetSummaryListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="prefixSummaryTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderRouteListEnterpriseTrunkNumberPrefixGetSummaryListResponse", propOrder = {
    "prefixSummaryTable"
})
public class ServiceProviderRouteListEnterpriseTrunkNumberPrefixGetSummaryListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable prefixSummaryTable;

    /**
     * Ruft den Wert der prefixSummaryTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getPrefixSummaryTable() {
        return prefixSummaryTable;
    }

    /**
     * Legt den Wert der prefixSummaryTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setPrefixSummaryTable(OCITable value) {
        this.prefixSummaryTable = value;
    }

}
