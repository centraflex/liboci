//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Add an Attribute Value Pair (AVP) combination to disable. The AVP
 *         combination includes a diameter AVP and a vendor ID. The response is either
 *         a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemAccountingInhibitedAttributeValuePairCodeAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAccountingInhibitedAttributeValuePairCodeAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="attributeValuePairCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="vendorId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAccountingInhibitedAttributeValuePairCodeAddRequest", propOrder = {
    "attributeValuePairCode",
    "vendorId"
})
public class SystemAccountingInhibitedAttributeValuePairCodeAddRequest
    extends OCIRequest
{

    protected int attributeValuePairCode;
    protected int vendorId;

    /**
     * Ruft den Wert der attributeValuePairCode-Eigenschaft ab.
     * 
     */
    public int getAttributeValuePairCode() {
        return attributeValuePairCode;
    }

    /**
     * Legt den Wert der attributeValuePairCode-Eigenschaft fest.
     * 
     */
    public void setAttributeValuePairCode(int value) {
        this.attributeValuePairCode = value;
    }

    /**
     * Ruft den Wert der vendorId-Eigenschaft ab.
     * 
     */
    public int getVendorId() {
        return vendorId;
    }

    /**
     * Legt den Wert der vendorId-Eigenschaft fest.
     * 
     */
    public void setVendorId(int value) {
        this.vendorId = value;
    }

}
