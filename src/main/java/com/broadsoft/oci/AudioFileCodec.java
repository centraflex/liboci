//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AudioFileCodec.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AudioFileCodec">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="None"/>
 *     <enumeration value="G711"/>
 *     <enumeration value="G722"/>
 *     <enumeration value="G729"/>
 *     <enumeration value="G726"/>
 *     <enumeration value="AMR"/>
 *     <enumeration value="AMR-WB"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AudioFileCodec")
@XmlEnum
public enum AudioFileCodec {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("G711")
    G_711("G711"),
    @XmlEnumValue("G722")
    G_722("G722"),
    @XmlEnumValue("G729")
    G_729("G729"),
    @XmlEnumValue("G726")
    G_726("G726"),
    AMR("AMR"),
    @XmlEnumValue("AMR-WB")
    AMR_WB("AMR-WB");
    private final String value;

    AudioFileCodec(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AudioFileCodec fromValue(String v) {
        for (AudioFileCodec c: AudioFileCodec.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
