//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ChargeIndicator.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ChargeIndicator">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Charge"/>
 *     <enumeration value="No Charge"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ChargeIndicator")
@XmlEnum
public enum ChargeIndicator {

    @XmlEnumValue("Charge")
    CHARGE("Charge"),
    @XmlEnumValue("No Charge")
    NO_CHARGE("No Charge");
    private final String value;

    ChargeIndicator(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ChargeIndicator fromValue(String v) {
        for (ChargeIndicator c: ChargeIndicator.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
