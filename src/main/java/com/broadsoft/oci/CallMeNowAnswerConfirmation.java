//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallMeNowAnswerConfirmation.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallMeNowAnswerConfirmation">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="None"/>
 *     <enumeration value="Any Key"/>
 *     <enumeration value="Passcode"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallMeNowAnswerConfirmation")
@XmlEnum
public enum CallMeNowAnswerConfirmation {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Any Key")
    ANY_KEY("Any Key"),
    @XmlEnumValue("Passcode")
    PASSCODE("Passcode");
    private final String value;

    CallMeNowAnswerConfirmation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallMeNowAnswerConfirmation fromValue(String v) {
        for (CallMeNowAnswerConfirmation c: CallMeNowAnswerConfirmation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
