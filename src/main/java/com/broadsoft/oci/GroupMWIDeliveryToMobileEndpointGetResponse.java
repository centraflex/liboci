//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupMWIDeliveryToMobileEndpointGetRequest.
 *         
 *         The templateActivationTable contains the list of templates defined for the group.
 *         The column headings are "Enable", "Language", "Type".
 *         
 *       
 * 
 * <p>Java-Klasse für GroupMWIDeliveryToMobileEndpointGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupMWIDeliveryToMobileEndpointGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useSettingLevel" type="{}MWIDeliveryToMobileEndpointGroupSettingLevel"/>
 *         <element name="templateActivationTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupMWIDeliveryToMobileEndpointGetResponse", propOrder = {
    "useSettingLevel",
    "templateActivationTable"
})
public class GroupMWIDeliveryToMobileEndpointGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MWIDeliveryToMobileEndpointGroupSettingLevel useSettingLevel;
    @XmlElement(required = true)
    protected OCITable templateActivationTable;

    /**
     * Ruft den Wert der useSettingLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MWIDeliveryToMobileEndpointGroupSettingLevel }
     *     
     */
    public MWIDeliveryToMobileEndpointGroupSettingLevel getUseSettingLevel() {
        return useSettingLevel;
    }

    /**
     * Legt den Wert der useSettingLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MWIDeliveryToMobileEndpointGroupSettingLevel }
     *     
     */
    public void setUseSettingLevel(MWIDeliveryToMobileEndpointGroupSettingLevel value) {
        this.useSettingLevel = value;
    }

    /**
     * Ruft den Wert der templateActivationTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getTemplateActivationTable() {
        return templateActivationTable;
    }

    /**
     * Legt den Wert der templateActivationTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setTemplateActivationTable(OCITable value) {
        this.templateActivationTable = value;
    }

}
