//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCallCenterEnhancedReportingGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemCallCenterEnhancedReportingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallCenterEnhancedReportingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="archiveReports" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="reportApplicationURL" type="{}URL" minOccurs="0"/>
 *         <element name="repositoryApplicationURL" type="{}URL" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallCenterEnhancedReportingGetResponse", propOrder = {
    "archiveReports",
    "reportApplicationURL",
    "repositoryApplicationURL"
})
public class SystemCallCenterEnhancedReportingGetResponse
    extends OCIDataResponse
{

    protected boolean archiveReports;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String reportApplicationURL;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String repositoryApplicationURL;

    /**
     * Ruft den Wert der archiveReports-Eigenschaft ab.
     * 
     */
    public boolean isArchiveReports() {
        return archiveReports;
    }

    /**
     * Legt den Wert der archiveReports-Eigenschaft fest.
     * 
     */
    public void setArchiveReports(boolean value) {
        this.archiveReports = value;
    }

    /**
     * Ruft den Wert der reportApplicationURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportApplicationURL() {
        return reportApplicationURL;
    }

    /**
     * Legt den Wert der reportApplicationURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportApplicationURL(String value) {
        this.reportApplicationURL = value;
    }

    /**
     * Ruft den Wert der repositoryApplicationURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepositoryApplicationURL() {
        return repositoryApplicationURL;
    }

    /**
     * Legt den Wert der repositoryApplicationURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepositoryApplicationURL(String value) {
        this.repositoryApplicationURL = value;
    }

}
