//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupServiceInstancePrivacyGetRequest17sp4.
 *       
 * 
 * <p>Java-Klasse für GroupServiceInstancePrivacyGetResponse17sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupServiceInstancePrivacyGetResponse17sp4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableDirectoryPrivacy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableAutoAttendantExtensionDialingPrivacy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableAutoAttendantNameDialingPrivacy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupServiceInstancePrivacyGetResponse17sp4", propOrder = {
    "enableDirectoryPrivacy",
    "enableAutoAttendantExtensionDialingPrivacy",
    "enableAutoAttendantNameDialingPrivacy"
})
public class GroupServiceInstancePrivacyGetResponse17Sp4
    extends OCIDataResponse
{

    protected boolean enableDirectoryPrivacy;
    protected boolean enableAutoAttendantExtensionDialingPrivacy;
    protected boolean enableAutoAttendantNameDialingPrivacy;

    /**
     * Ruft den Wert der enableDirectoryPrivacy-Eigenschaft ab.
     * 
     */
    public boolean isEnableDirectoryPrivacy() {
        return enableDirectoryPrivacy;
    }

    /**
     * Legt den Wert der enableDirectoryPrivacy-Eigenschaft fest.
     * 
     */
    public void setEnableDirectoryPrivacy(boolean value) {
        this.enableDirectoryPrivacy = value;
    }

    /**
     * Ruft den Wert der enableAutoAttendantExtensionDialingPrivacy-Eigenschaft ab.
     * 
     */
    public boolean isEnableAutoAttendantExtensionDialingPrivacy() {
        return enableAutoAttendantExtensionDialingPrivacy;
    }

    /**
     * Legt den Wert der enableAutoAttendantExtensionDialingPrivacy-Eigenschaft fest.
     * 
     */
    public void setEnableAutoAttendantExtensionDialingPrivacy(boolean value) {
        this.enableAutoAttendantExtensionDialingPrivacy = value;
    }

    /**
     * Ruft den Wert der enableAutoAttendantNameDialingPrivacy-Eigenschaft ab.
     * 
     */
    public boolean isEnableAutoAttendantNameDialingPrivacy() {
        return enableAutoAttendantNameDialingPrivacy;
    }

    /**
     * Legt den Wert der enableAutoAttendantNameDialingPrivacy-Eigenschaft fest.
     * 
     */
    public void setEnableAutoAttendantNameDialingPrivacy(boolean value) {
        this.enableAutoAttendantNameDialingPrivacy = value;
    }

}
