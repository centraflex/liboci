//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *          Response to SystemCallRecordingGetPlatformListRequest22V2.
 *          Contains the default system Call Recording platform, default reseller Call Recording Platform (when applicable) and a table with columns headings    
 * 		 "Name", "Net Address", "Port", "Transport Type", "Media Stream", "Description", "Schema Version", "Support Video Rec", "Reseller Id", "Route".
 *          The system default recording platform also appears in the table with the other platforms.
 *          
 *          The port can be empty if it is not defined in the recording platform.
 *          The possible values for "Support Video Rec" can be either true or false.
 *          Schema version values include: 1.0, 2.0, 3.0
 *       
 * 
 * <p>Java-Klasse für SystemCallRecordingGetPlatformListResponse22V2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallRecordingGetPlatformListResponse22V2">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="systemDefault" type="{}CallRecordingPlatformName" minOccurs="0"/>
 *         <element name="resellerDefault" type="{}CallRecordingPlatformName" minOccurs="0"/>
 *         <element name="callRecordingPlatformTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallRecordingGetPlatformListResponse22V2", propOrder = {
    "systemDefault",
    "resellerDefault",
    "callRecordingPlatformTable"
})
public class SystemCallRecordingGetPlatformListResponse22V2
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String systemDefault;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerDefault;
    @XmlElement(required = true)
    protected OCITable callRecordingPlatformTable;

    /**
     * Ruft den Wert der systemDefault-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemDefault() {
        return systemDefault;
    }

    /**
     * Legt den Wert der systemDefault-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemDefault(String value) {
        this.systemDefault = value;
    }

    /**
     * Ruft den Wert der resellerDefault-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerDefault() {
        return resellerDefault;
    }

    /**
     * Legt den Wert der resellerDefault-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerDefault(String value) {
        this.resellerDefault = value;
    }

    /**
     * Ruft den Wert der callRecordingPlatformTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getCallRecordingPlatformTable() {
        return callRecordingPlatformTable;
    }

    /**
     * Legt den Wert der callRecordingPlatformTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setCallRecordingPlatformTable(OCITable value) {
        this.callRecordingPlatformTable = value;
    }

}
