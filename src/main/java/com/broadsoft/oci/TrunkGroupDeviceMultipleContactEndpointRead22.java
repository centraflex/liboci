//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Trunk group device endpoint that can have multiple contacts.
 *       
 * 
 * <p>Java-Klasse für TrunkGroupDeviceMultipleContactEndpointRead22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="TrunkGroupDeviceMultipleContactEndpointRead22">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="name" type="{}TrunkGroupName"/>
 *         <element name="linePort" type="{}AccessDeviceEndpointLinePort"/>
 *         <element name="contact" type="{}SIPContactInfo" maxOccurs="5" minOccurs="0"/>
 *         <element name="staticRegistrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useDomain" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isPilotUser" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrunkGroupDeviceMultipleContactEndpointRead22", propOrder = {
    "name",
    "linePort",
    "contact",
    "staticRegistrationCapable",
    "useDomain",
    "isPilotUser"
})
public class TrunkGroupDeviceMultipleContactEndpointRead22 {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String linePort;
    protected List<SIPContactInfo> contact;
    protected boolean staticRegistrationCapable;
    protected boolean useDomain;
    protected boolean isPilotUser;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der linePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinePort() {
        return linePort;
    }

    /**
     * Legt den Wert der linePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinePort(String value) {
        this.linePort = value;
    }

    /**
     * Gets the value of the contact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the contact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SIPContactInfo }
     * 
     * 
     * @return
     *     The value of the contact property.
     */
    public List<SIPContactInfo> getContact() {
        if (contact == null) {
            contact = new ArrayList<>();
        }
        return this.contact;
    }

    /**
     * Ruft den Wert der staticRegistrationCapable-Eigenschaft ab.
     * 
     */
    public boolean isStaticRegistrationCapable() {
        return staticRegistrationCapable;
    }

    /**
     * Legt den Wert der staticRegistrationCapable-Eigenschaft fest.
     * 
     */
    public void setStaticRegistrationCapable(boolean value) {
        this.staticRegistrationCapable = value;
    }

    /**
     * Ruft den Wert der useDomain-Eigenschaft ab.
     * 
     */
    public boolean isUseDomain() {
        return useDomain;
    }

    /**
     * Legt den Wert der useDomain-Eigenschaft fest.
     * 
     */
    public void setUseDomain(boolean value) {
        this.useDomain = value;
    }

    /**
     * Ruft den Wert der isPilotUser-Eigenschaft ab.
     * 
     */
    public boolean isIsPilotUser() {
        return isPilotUser;
    }

    /**
     * Legt den Wert der isPilotUser-Eigenschaft fest.
     * 
     */
    public void setIsPilotUser(boolean value) {
        this.isPilotUser = value;
    }

}
