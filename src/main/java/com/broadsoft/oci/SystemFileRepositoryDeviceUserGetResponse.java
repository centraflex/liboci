//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemFileRepositoryDeviceUserGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemFileRepositoryDeviceUserGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemFileRepositoryDeviceUserGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="allowPut" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowDelete" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowGet" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemFileRepositoryDeviceUserGetResponse", propOrder = {
    "allowPut",
    "allowDelete",
    "allowGet"
})
public class SystemFileRepositoryDeviceUserGetResponse
    extends OCIDataResponse
{

    protected boolean allowPut;
    protected boolean allowDelete;
    protected boolean allowGet;

    /**
     * Ruft den Wert der allowPut-Eigenschaft ab.
     * 
     */
    public boolean isAllowPut() {
        return allowPut;
    }

    /**
     * Legt den Wert der allowPut-Eigenschaft fest.
     * 
     */
    public void setAllowPut(boolean value) {
        this.allowPut = value;
    }

    /**
     * Ruft den Wert der allowDelete-Eigenschaft ab.
     * 
     */
    public boolean isAllowDelete() {
        return allowDelete;
    }

    /**
     * Legt den Wert der allowDelete-Eigenschaft fest.
     * 
     */
    public void setAllowDelete(boolean value) {
        this.allowDelete = value;
    }

    /**
     * Ruft den Wert der allowGet-Eigenschaft ab.
     * 
     */
    public boolean isAllowGet() {
        return allowGet;
    }

    /**
     * Legt den Wert der allowGet-Eigenschaft fest.
     * 
     */
    public void setAllowGet(boolean value) {
        this.allowGet = value;
    }

}
