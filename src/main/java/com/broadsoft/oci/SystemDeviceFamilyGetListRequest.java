//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to get the list of device family managed by the Device Management System. 
 *         If includeSystemLevel is specified, all system level device families and the reseller device families matching search criteria 
 *         are returned even when searchCriteriaResellerId is specified.        
 *         If reseller administrator sends the request, searchCriteriaResellerId is ignored and automatically set to the administrator's reseller. 
 *         
 *         The response is either SystemDeviceFamilyGetListResponse or ErrorResponse.       
 *       
 * 
 * <p>Java-Klasse für SystemDeviceFamilyGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceFamilyGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="includeSystemLevel" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="searchCriteriaResellerId" type="{}SearchCriteriaResellerId" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceFamilyGetListRequest", propOrder = {
    "includeSystemLevel",
    "searchCriteriaResellerId"
})
public class SystemDeviceFamilyGetListRequest
    extends OCIRequest
{

    protected Boolean includeSystemLevel;
    protected List<SearchCriteriaResellerId> searchCriteriaResellerId;

    /**
     * Ruft den Wert der includeSystemLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeSystemLevel() {
        return includeSystemLevel;
    }

    /**
     * Legt den Wert der includeSystemLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeSystemLevel(Boolean value) {
        this.includeSystemLevel = value;
    }

    /**
     * Gets the value of the searchCriteriaResellerId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaResellerId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaResellerId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaResellerId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaResellerId property.
     */
    public List<SearchCriteriaResellerId> getSearchCriteriaResellerId() {
        if (searchCriteriaResellerId == null) {
            searchCriteriaResellerId = new ArrayList<>();
        }
        return this.searchCriteriaResellerId;
    }

}
