//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Get a list of local gateways defined within an enterprise. Only the local gateways having device associated are returned.
 * 
 *         If the responsePagingControl element is not provided, the paging startIndex will be set to 1 
 *         by default, and the responsePageSize will be set to the maximum ResponsePageSize by default.
 *         If no sortOrder is included the response is sorted by Trunk group name  ascending by default.
 *         Multiple search criteria are logically ANDed together unless the searchCriteriaModeOr option is included.
 *         Then the search criteria are logically ORed together.
 *  
 *         The response is either a EnterpriseLocalGatewayGetPagedSortedListResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für EnterpriseLocalGatewayGetPagedSortedListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseLocalGatewayGetPagedSortedListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl" minOccurs="0"/>
 *         <element name="sortByTrunkGroupName" type="{}SortByTrunkGroupName" minOccurs="0"/>
 *         <element name="searchCriteriaTrunkGroupName" type="{}SearchCriteriaTrunkGroupName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaModeOr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseLocalGatewayGetPagedSortedListRequest", propOrder = {
    "serviceProviderId",
    "responsePagingControl",
    "sortByTrunkGroupName",
    "searchCriteriaTrunkGroupName",
    "searchCriteriaModeOr"
})
public class EnterpriseLocalGatewayGetPagedSortedListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected ResponsePagingControl responsePagingControl;
    protected SortByTrunkGroupName sortByTrunkGroupName;
    protected List<SearchCriteriaTrunkGroupName> searchCriteriaTrunkGroupName;
    protected Boolean searchCriteriaModeOr;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Ruft den Wert der sortByTrunkGroupName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByTrunkGroupName }
     *     
     */
    public SortByTrunkGroupName getSortByTrunkGroupName() {
        return sortByTrunkGroupName;
    }

    /**
     * Legt den Wert der sortByTrunkGroupName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByTrunkGroupName }
     *     
     */
    public void setSortByTrunkGroupName(SortByTrunkGroupName value) {
        this.sortByTrunkGroupName = value;
    }

    /**
     * Gets the value of the searchCriteriaTrunkGroupName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaTrunkGroupName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaTrunkGroupName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaTrunkGroupName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaTrunkGroupName property.
     */
    public List<SearchCriteriaTrunkGroupName> getSearchCriteriaTrunkGroupName() {
        if (searchCriteriaTrunkGroupName == null) {
            searchCriteriaTrunkGroupName = new ArrayList<>();
        }
        return this.searchCriteriaTrunkGroupName;
    }

    /**
     * Ruft den Wert der searchCriteriaModeOr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchCriteriaModeOr() {
        return searchCriteriaModeOr;
    }

    /**
     * Legt den Wert der searchCriteriaModeOr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchCriteriaModeOr(Boolean value) {
        this.searchCriteriaModeOr = value;
    }

}
