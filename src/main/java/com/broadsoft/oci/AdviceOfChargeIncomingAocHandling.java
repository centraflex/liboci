//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AdviceOfChargeIncomingAocHandling.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AdviceOfChargeIncomingAocHandling">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Ignore"/>
 *     <enumeration value="Charge"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AdviceOfChargeIncomingAocHandling")
@XmlEnum
public enum AdviceOfChargeIncomingAocHandling {

    @XmlEnumValue("Ignore")
    IGNORE("Ignore"),
    @XmlEnumValue("Charge")
    CHARGE("Charge");
    private final String value;

    AdviceOfChargeIncomingAocHandling(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AdviceOfChargeIncomingAocHandling fromValue(String v) {
        for (AdviceOfChargeIncomingAocHandling c: AdviceOfChargeIncomingAocHandling.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
