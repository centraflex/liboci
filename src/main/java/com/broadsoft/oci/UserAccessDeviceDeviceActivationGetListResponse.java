//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserAccessDeviceDeviceActivationGetListRequest.
 *         Contains a table of devices supporting device activation associated to a user.
 *         The column headings are: "Device Name", "Device Level", "Device SP", "Device Group", "Endpoint Type", "Line Port", "Activation Status", "Activation Code", "Expiration Time" and "MAC Address".
 *       
 * 
 * <p>Java-Klasse für UserAccessDeviceDeviceActivationGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserAccessDeviceDeviceActivationGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="accessDeviceTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserAccessDeviceDeviceActivationGetListResponse", propOrder = {
    "accessDeviceTable"
})
public class UserAccessDeviceDeviceActivationGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable accessDeviceTable;

    /**
     * Ruft den Wert der accessDeviceTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAccessDeviceTable() {
        return accessDeviceTable;
    }

    /**
     * Legt den Wert der accessDeviceTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAccessDeviceTable(OCITable value) {
        this.accessDeviceTable = value;
    }

}
