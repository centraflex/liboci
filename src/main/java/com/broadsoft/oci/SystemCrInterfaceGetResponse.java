//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemCrInterfaceGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemCrInterfaceGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCrInterfaceGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="crAuditEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="crAuditIntervalMilliseconds" type="{}CrAuditIntervalMilliseconds"/>
 *         <element name="crAuditTimeoutMilliseconds" type="{}CrAuditTimeoutMilliseconds"/>
 *         <element name="crConnectionEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="crConnectionTimeoutMilliseconds" type="{}CrConnectionTimeoutMilliseconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCrInterfaceGetResponse", propOrder = {
    "crAuditEnabled",
    "crAuditIntervalMilliseconds",
    "crAuditTimeoutMilliseconds",
    "crConnectionEnabled",
    "crConnectionTimeoutMilliseconds"
})
public class SystemCrInterfaceGetResponse
    extends OCIDataResponse
{

    protected boolean crAuditEnabled;
    protected int crAuditIntervalMilliseconds;
    protected int crAuditTimeoutMilliseconds;
    protected boolean crConnectionEnabled;
    protected int crConnectionTimeoutMilliseconds;

    /**
     * Ruft den Wert der crAuditEnabled-Eigenschaft ab.
     * 
     */
    public boolean isCrAuditEnabled() {
        return crAuditEnabled;
    }

    /**
     * Legt den Wert der crAuditEnabled-Eigenschaft fest.
     * 
     */
    public void setCrAuditEnabled(boolean value) {
        this.crAuditEnabled = value;
    }

    /**
     * Ruft den Wert der crAuditIntervalMilliseconds-Eigenschaft ab.
     * 
     */
    public int getCrAuditIntervalMilliseconds() {
        return crAuditIntervalMilliseconds;
    }

    /**
     * Legt den Wert der crAuditIntervalMilliseconds-Eigenschaft fest.
     * 
     */
    public void setCrAuditIntervalMilliseconds(int value) {
        this.crAuditIntervalMilliseconds = value;
    }

    /**
     * Ruft den Wert der crAuditTimeoutMilliseconds-Eigenschaft ab.
     * 
     */
    public int getCrAuditTimeoutMilliseconds() {
        return crAuditTimeoutMilliseconds;
    }

    /**
     * Legt den Wert der crAuditTimeoutMilliseconds-Eigenschaft fest.
     * 
     */
    public void setCrAuditTimeoutMilliseconds(int value) {
        this.crAuditTimeoutMilliseconds = value;
    }

    /**
     * Ruft den Wert der crConnectionEnabled-Eigenschaft ab.
     * 
     */
    public boolean isCrConnectionEnabled() {
        return crConnectionEnabled;
    }

    /**
     * Legt den Wert der crConnectionEnabled-Eigenschaft fest.
     * 
     */
    public void setCrConnectionEnabled(boolean value) {
        this.crConnectionEnabled = value;
    }

    /**
     * Ruft den Wert der crConnectionTimeoutMilliseconds-Eigenschaft ab.
     * 
     */
    public int getCrConnectionTimeoutMilliseconds() {
        return crConnectionTimeoutMilliseconds;
    }

    /**
     * Legt den Wert der crConnectionTimeoutMilliseconds-Eigenschaft fest.
     * 
     */
    public void setCrConnectionTimeoutMilliseconds(int value) {
        this.crConnectionTimeoutMilliseconds = value;
    }

}
