//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the service provider's passcode rules setting.
 *         The response is either SuccessResponse or ErrorResponse.
 *         
 *         The following elements are only used in AS data mode:
 *           numberOfRepeatedDigits
 *           disallowRepeatedPatterns
 *           disallowContiguousSequences
 *           numberOfAscendingDigits
 *           numberOfDescendingDigits
 *           numberOfPreviousPasscodes
 *           
 *           The following elements are only used in AS data mode and ignored in XS data mode:
 *           enableDefaultPasscode
 *           defaultPasscode
 *       
 * 
 * <p>Java-Klasse für ServiceProviderPortalPasscodeRulesModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderPortalPasscodeRulesModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="disallowRepeatedDigits" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="numberOfRepeatedDigits" type="{}PasscodeMaxRepeatedDigits" minOccurs="0"/>
 *         <element name="disallowRepeatedPatterns" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disallowContiguousSequences" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="numberOfAscendingDigits" type="{}PasscodeMaxContiguousDigits" minOccurs="0"/>
 *         <element name="numberOfDescendingDigits" type="{}PasscodeMaxContiguousDigits" minOccurs="0"/>
 *         <element name="disallowUserNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disallowReversedUserNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disallowOldPasscode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="numberOfPreviousPasscodes" type="{}PasscodeHistoryCount" minOccurs="0"/>
 *         <element name="disallowReversedOldPasscode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="minCodeLength" type="{}PasscodeMinLength" minOccurs="0"/>
 *         <element name="maxCodeLength" type="{}PasscodeMaxLength" minOccurs="0"/>
 *         <element name="disableLoginAfterMaxFailedLoginAttempts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxFailedLoginAttempts" type="{}PortalMaxFailedLoginAttempts" minOccurs="0"/>
 *         <element name="expirePassword" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="passcodeExpiresDays" type="{}PasscodeExpiresDays" minOccurs="0"/>
 *         <element name="sendLoginDisabledNotifyEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="loginDisabledNotifyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="enableDefaultPasscode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="defaultPasscode" type="{}Passcode" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderPortalPasscodeRulesModifyRequest", propOrder = {
    "serviceProviderId",
    "disallowRepeatedDigits",
    "numberOfRepeatedDigits",
    "disallowRepeatedPatterns",
    "disallowContiguousSequences",
    "numberOfAscendingDigits",
    "numberOfDescendingDigits",
    "disallowUserNumber",
    "disallowReversedUserNumber",
    "disallowOldPasscode",
    "numberOfPreviousPasscodes",
    "disallowReversedOldPasscode",
    "minCodeLength",
    "maxCodeLength",
    "disableLoginAfterMaxFailedLoginAttempts",
    "maxFailedLoginAttempts",
    "expirePassword",
    "passcodeExpiresDays",
    "sendLoginDisabledNotifyEmail",
    "loginDisabledNotifyEmailAddress",
    "enableDefaultPasscode",
    "defaultPasscode"
})
public class ServiceProviderPortalPasscodeRulesModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected Boolean disallowRepeatedDigits;
    protected Integer numberOfRepeatedDigits;
    protected Boolean disallowRepeatedPatterns;
    protected Boolean disallowContiguousSequences;
    protected Integer numberOfAscendingDigits;
    protected Integer numberOfDescendingDigits;
    protected Boolean disallowUserNumber;
    protected Boolean disallowReversedUserNumber;
    protected Boolean disallowOldPasscode;
    protected Integer numberOfPreviousPasscodes;
    protected Boolean disallowReversedOldPasscode;
    protected Integer minCodeLength;
    protected Integer maxCodeLength;
    protected Boolean disableLoginAfterMaxFailedLoginAttempts;
    protected Integer maxFailedLoginAttempts;
    protected Boolean expirePassword;
    protected Integer passcodeExpiresDays;
    protected Boolean sendLoginDisabledNotifyEmail;
    @XmlElementRef(name = "loginDisabledNotifyEmailAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> loginDisabledNotifyEmailAddress;
    protected Boolean enableDefaultPasscode;
    @XmlElementRef(name = "defaultPasscode", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultPasscode;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der disallowRepeatedDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowRepeatedDigits() {
        return disallowRepeatedDigits;
    }

    /**
     * Legt den Wert der disallowRepeatedDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowRepeatedDigits(Boolean value) {
        this.disallowRepeatedDigits = value;
    }

    /**
     * Ruft den Wert der numberOfRepeatedDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfRepeatedDigits() {
        return numberOfRepeatedDigits;
    }

    /**
     * Legt den Wert der numberOfRepeatedDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfRepeatedDigits(Integer value) {
        this.numberOfRepeatedDigits = value;
    }

    /**
     * Ruft den Wert der disallowRepeatedPatterns-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowRepeatedPatterns() {
        return disallowRepeatedPatterns;
    }

    /**
     * Legt den Wert der disallowRepeatedPatterns-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowRepeatedPatterns(Boolean value) {
        this.disallowRepeatedPatterns = value;
    }

    /**
     * Ruft den Wert der disallowContiguousSequences-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowContiguousSequences() {
        return disallowContiguousSequences;
    }

    /**
     * Legt den Wert der disallowContiguousSequences-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowContiguousSequences(Boolean value) {
        this.disallowContiguousSequences = value;
    }

    /**
     * Ruft den Wert der numberOfAscendingDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfAscendingDigits() {
        return numberOfAscendingDigits;
    }

    /**
     * Legt den Wert der numberOfAscendingDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfAscendingDigits(Integer value) {
        this.numberOfAscendingDigits = value;
    }

    /**
     * Ruft den Wert der numberOfDescendingDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfDescendingDigits() {
        return numberOfDescendingDigits;
    }

    /**
     * Legt den Wert der numberOfDescendingDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfDescendingDigits(Integer value) {
        this.numberOfDescendingDigits = value;
    }

    /**
     * Ruft den Wert der disallowUserNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowUserNumber() {
        return disallowUserNumber;
    }

    /**
     * Legt den Wert der disallowUserNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowUserNumber(Boolean value) {
        this.disallowUserNumber = value;
    }

    /**
     * Ruft den Wert der disallowReversedUserNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowReversedUserNumber() {
        return disallowReversedUserNumber;
    }

    /**
     * Legt den Wert der disallowReversedUserNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowReversedUserNumber(Boolean value) {
        this.disallowReversedUserNumber = value;
    }

    /**
     * Ruft den Wert der disallowOldPasscode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowOldPasscode() {
        return disallowOldPasscode;
    }

    /**
     * Legt den Wert der disallowOldPasscode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowOldPasscode(Boolean value) {
        this.disallowOldPasscode = value;
    }

    /**
     * Ruft den Wert der numberOfPreviousPasscodes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfPreviousPasscodes() {
        return numberOfPreviousPasscodes;
    }

    /**
     * Legt den Wert der numberOfPreviousPasscodes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfPreviousPasscodes(Integer value) {
        this.numberOfPreviousPasscodes = value;
    }

    /**
     * Ruft den Wert der disallowReversedOldPasscode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowReversedOldPasscode() {
        return disallowReversedOldPasscode;
    }

    /**
     * Legt den Wert der disallowReversedOldPasscode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowReversedOldPasscode(Boolean value) {
        this.disallowReversedOldPasscode = value;
    }

    /**
     * Ruft den Wert der minCodeLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinCodeLength() {
        return minCodeLength;
    }

    /**
     * Legt den Wert der minCodeLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinCodeLength(Integer value) {
        this.minCodeLength = value;
    }

    /**
     * Ruft den Wert der maxCodeLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCodeLength() {
        return maxCodeLength;
    }

    /**
     * Legt den Wert der maxCodeLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCodeLength(Integer value) {
        this.maxCodeLength = value;
    }

    /**
     * Ruft den Wert der disableLoginAfterMaxFailedLoginAttempts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisableLoginAfterMaxFailedLoginAttempts() {
        return disableLoginAfterMaxFailedLoginAttempts;
    }

    /**
     * Legt den Wert der disableLoginAfterMaxFailedLoginAttempts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisableLoginAfterMaxFailedLoginAttempts(Boolean value) {
        this.disableLoginAfterMaxFailedLoginAttempts = value;
    }

    /**
     * Ruft den Wert der maxFailedLoginAttempts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxFailedLoginAttempts() {
        return maxFailedLoginAttempts;
    }

    /**
     * Legt den Wert der maxFailedLoginAttempts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxFailedLoginAttempts(Integer value) {
        this.maxFailedLoginAttempts = value;
    }

    /**
     * Ruft den Wert der expirePassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExpirePassword() {
        return expirePassword;
    }

    /**
     * Legt den Wert der expirePassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExpirePassword(Boolean value) {
        this.expirePassword = value;
    }

    /**
     * Ruft den Wert der passcodeExpiresDays-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPasscodeExpiresDays() {
        return passcodeExpiresDays;
    }

    /**
     * Legt den Wert der passcodeExpiresDays-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPasscodeExpiresDays(Integer value) {
        this.passcodeExpiresDays = value;
    }

    /**
     * Ruft den Wert der sendLoginDisabledNotifyEmail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendLoginDisabledNotifyEmail() {
        return sendLoginDisabledNotifyEmail;
    }

    /**
     * Legt den Wert der sendLoginDisabledNotifyEmail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendLoginDisabledNotifyEmail(Boolean value) {
        this.sendLoginDisabledNotifyEmail = value;
    }

    /**
     * Ruft den Wert der loginDisabledNotifyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLoginDisabledNotifyEmailAddress() {
        return loginDisabledNotifyEmailAddress;
    }

    /**
     * Legt den Wert der loginDisabledNotifyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLoginDisabledNotifyEmailAddress(JAXBElement<String> value) {
        this.loginDisabledNotifyEmailAddress = value;
    }

    /**
     * Ruft den Wert der enableDefaultPasscode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableDefaultPasscode() {
        return enableDefaultPasscode;
    }

    /**
     * Legt den Wert der enableDefaultPasscode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableDefaultPasscode(Boolean value) {
        this.enableDefaultPasscode = value;
    }

    /**
     * Ruft den Wert der defaultPasscode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultPasscode() {
        return defaultPasscode;
    }

    /**
     * Legt den Wert der defaultPasscode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultPasscode(JAXBElement<String> value) {
        this.defaultPasscode = value;
    }

}
