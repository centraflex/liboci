//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemLicensingGetRequest. The license table columns are: "Name", "Licensed", "Used",
 *         "Available" and "Expiration Date".
 *         Replaced By: SystemLicensingGetResponse14sp3
 *       
 * 
 * <p>Java-Klasse für SystemLicensingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemLicensingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="licenseStrictness" type="{}LicenseStrictness"/>
 *         <element name="groupUserlimit" type="{}GroupUserLicenseLimit"/>
 *         <element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         <element name="hostId" type="{}ServerHostId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="licenseName" type="{}LicenseName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="licenseTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemLicensingGetResponse", propOrder = {
    "licenseStrictness",
    "groupUserlimit",
    "expirationDate",
    "hostId",
    "licenseName",
    "licenseTable"
})
public class SystemLicensingGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected LicenseStrictness licenseStrictness;
    protected int groupUserlimit;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDate;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> hostId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> licenseName;
    @XmlElement(required = true)
    protected OCITable licenseTable;

    /**
     * Ruft den Wert der licenseStrictness-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LicenseStrictness }
     *     
     */
    public LicenseStrictness getLicenseStrictness() {
        return licenseStrictness;
    }

    /**
     * Legt den Wert der licenseStrictness-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseStrictness }
     *     
     */
    public void setLicenseStrictness(LicenseStrictness value) {
        this.licenseStrictness = value;
    }

    /**
     * Ruft den Wert der groupUserlimit-Eigenschaft ab.
     * 
     */
    public int getGroupUserlimit() {
        return groupUserlimit;
    }

    /**
     * Legt den Wert der groupUserlimit-Eigenschaft fest.
     * 
     */
    public void setGroupUserlimit(int value) {
        this.groupUserlimit = value;
    }

    /**
     * Ruft den Wert der expirationDate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Legt den Wert der expirationDate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the hostId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the hostId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHostId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the hostId property.
     */
    public List<String> getHostId() {
        if (hostId == null) {
            hostId = new ArrayList<>();
        }
        return this.hostId;
    }

    /**
     * Gets the value of the licenseName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the licenseName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLicenseName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the licenseName property.
     */
    public List<String> getLicenseName() {
        if (licenseName == null) {
            licenseName = new ArrayList<>();
        }
        return this.licenseName;
    }

    /**
     * Ruft den Wert der licenseTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getLicenseTable() {
        return licenseTable;
    }

    /**
     * Legt den Wert der licenseTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setLicenseTable(OCITable value) {
        this.licenseTable = value;
    }

}
