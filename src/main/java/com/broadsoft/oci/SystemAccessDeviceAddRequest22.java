//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to add a system-level access device.
 *         The response is either SuccessResponse or ErrorResponse.
 *         The following elements are only used in AS data mode and ignored in XS data mode: 
 *           useHotline
 *           hotlineContact
 *           deviceExternalId
 *           
 *         Replaced by: SystemAccessDeviceAddRequest22V2
 *       
 * 
 * <p>Java-Klasse für SystemAccessDeviceAddRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAccessDeviceAddRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <choice>
 *           <element name="deviceName" type="{}AccessDeviceName"/>
 *           <element name="deviceExternalId" type="{}ExternalId"/>
 *         </choice>
 *         <element name="deviceType" type="{}AccessDeviceType"/>
 *         <element name="protocol" type="{}AccessDeviceProtocol" minOccurs="0"/>
 *         <element name="netAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="port" type="{}Port1025" minOccurs="0"/>
 *         <element name="outboundProxyServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="stunServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="macAddress" type="{}AccessDeviceMACAddress" minOccurs="0"/>
 *         <element name="serialNumber" type="{}AccessDeviceSerialNumber" minOccurs="0"/>
 *         <element name="description" type="{}AccessDeviceDescription" minOccurs="0"/>
 *         <element name="physicalLocation" type="{}AccessDevicePhysicalLocation" minOccurs="0"/>
 *         <element name="transportProtocol" type="{}ExtendedTransportProtocol" minOccurs="0"/>
 *         <element name="mobilityManagerProvisioningURL" type="{}URL" minOccurs="0"/>
 *         <element name="mobilityManagerProvisioningUserName" type="{}MobilityManagerProvisioningUserName" minOccurs="0"/>
 *         <element name="mobilityManagerProvisioningPassword" type="{}MobilityManagerProvisioningPassword" minOccurs="0"/>
 *         <element name="mobilityManagerDefaultOriginatingServiceKey" type="{}MobilityManagerServiceKey" minOccurs="0"/>
 *         <element name="mobilityManagerDefaultTerminatingServiceKey" type="{}MobilityManagerServiceKey" minOccurs="0"/>
 *         <element name="useCustomUserNamePassword" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="accessDeviceCredentials" type="{}DeviceManagementUserNamePassword16" minOccurs="0"/>
 *         <element name="useHotline" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="hotlineContact" type="{}SIPURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAccessDeviceAddRequest22", propOrder = {
    "deviceName",
    "deviceExternalId",
    "deviceType",
    "protocol",
    "netAddress",
    "port",
    "outboundProxyServerNetAddress",
    "stunServerNetAddress",
    "macAddress",
    "serialNumber",
    "description",
    "physicalLocation",
    "transportProtocol",
    "mobilityManagerProvisioningURL",
    "mobilityManagerProvisioningUserName",
    "mobilityManagerProvisioningPassword",
    "mobilityManagerDefaultOriginatingServiceKey",
    "mobilityManagerDefaultTerminatingServiceKey",
    "useCustomUserNamePassword",
    "accessDeviceCredentials",
    "useHotline",
    "hotlineContact"
})
public class SystemAccessDeviceAddRequest22
    extends OCIRequest
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceExternalId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String protocol;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String netAddress;
    protected Integer port;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String outboundProxyServerNetAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String stunServerNetAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String macAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serialNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String physicalLocation;
    @XmlSchemaType(name = "token")
    protected ExtendedTransportProtocol transportProtocol;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mobilityManagerProvisioningURL;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mobilityManagerProvisioningUserName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mobilityManagerProvisioningPassword;
    protected Integer mobilityManagerDefaultOriginatingServiceKey;
    protected Integer mobilityManagerDefaultTerminatingServiceKey;
    protected Boolean useCustomUserNamePassword;
    protected DeviceManagementUserNamePassword16 accessDeviceCredentials;
    protected boolean useHotline;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String hotlineContact;

    /**
     * Ruft den Wert der deviceName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * Legt den Wert der deviceName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceName(String value) {
        this.deviceName = value;
    }

    /**
     * Ruft den Wert der deviceExternalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceExternalId() {
        return deviceExternalId;
    }

    /**
     * Legt den Wert der deviceExternalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceExternalId(String value) {
        this.deviceExternalId = value;
    }

    /**
     * Ruft den Wert der deviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Legt den Wert der deviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceType(String value) {
        this.deviceType = value;
    }

    /**
     * Ruft den Wert der protocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocol() {
        return protocol;
    }

    /**
     * Legt den Wert der protocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocol(String value) {
        this.protocol = value;
    }

    /**
     * Ruft den Wert der netAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetAddress() {
        return netAddress;
    }

    /**
     * Legt den Wert der netAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetAddress(String value) {
        this.netAddress = value;
    }

    /**
     * Ruft den Wert der port-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPort() {
        return port;
    }

    /**
     * Legt den Wert der port-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPort(Integer value) {
        this.port = value;
    }

    /**
     * Ruft den Wert der outboundProxyServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutboundProxyServerNetAddress() {
        return outboundProxyServerNetAddress;
    }

    /**
     * Legt den Wert der outboundProxyServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutboundProxyServerNetAddress(String value) {
        this.outboundProxyServerNetAddress = value;
    }

    /**
     * Ruft den Wert der stunServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStunServerNetAddress() {
        return stunServerNetAddress;
    }

    /**
     * Legt den Wert der stunServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStunServerNetAddress(String value) {
        this.stunServerNetAddress = value;
    }

    /**
     * Ruft den Wert der macAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacAddress() {
        return macAddress;
    }

    /**
     * Legt den Wert der macAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacAddress(String value) {
        this.macAddress = value;
    }

    /**
     * Ruft den Wert der serialNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Legt den Wert der serialNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNumber(String value) {
        this.serialNumber = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der physicalLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhysicalLocation() {
        return physicalLocation;
    }

    /**
     * Legt den Wert der physicalLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhysicalLocation(String value) {
        this.physicalLocation = value;
    }

    /**
     * Ruft den Wert der transportProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedTransportProtocol }
     *     
     */
    public ExtendedTransportProtocol getTransportProtocol() {
        return transportProtocol;
    }

    /**
     * Legt den Wert der transportProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedTransportProtocol }
     *     
     */
    public void setTransportProtocol(ExtendedTransportProtocol value) {
        this.transportProtocol = value;
    }

    /**
     * Ruft den Wert der mobilityManagerProvisioningURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobilityManagerProvisioningURL() {
        return mobilityManagerProvisioningURL;
    }

    /**
     * Legt den Wert der mobilityManagerProvisioningURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobilityManagerProvisioningURL(String value) {
        this.mobilityManagerProvisioningURL = value;
    }

    /**
     * Ruft den Wert der mobilityManagerProvisioningUserName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobilityManagerProvisioningUserName() {
        return mobilityManagerProvisioningUserName;
    }

    /**
     * Legt den Wert der mobilityManagerProvisioningUserName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobilityManagerProvisioningUserName(String value) {
        this.mobilityManagerProvisioningUserName = value;
    }

    /**
     * Ruft den Wert der mobilityManagerProvisioningPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobilityManagerProvisioningPassword() {
        return mobilityManagerProvisioningPassword;
    }

    /**
     * Legt den Wert der mobilityManagerProvisioningPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobilityManagerProvisioningPassword(String value) {
        this.mobilityManagerProvisioningPassword = value;
    }

    /**
     * Ruft den Wert der mobilityManagerDefaultOriginatingServiceKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMobilityManagerDefaultOriginatingServiceKey() {
        return mobilityManagerDefaultOriginatingServiceKey;
    }

    /**
     * Legt den Wert der mobilityManagerDefaultOriginatingServiceKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMobilityManagerDefaultOriginatingServiceKey(Integer value) {
        this.mobilityManagerDefaultOriginatingServiceKey = value;
    }

    /**
     * Ruft den Wert der mobilityManagerDefaultTerminatingServiceKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMobilityManagerDefaultTerminatingServiceKey() {
        return mobilityManagerDefaultTerminatingServiceKey;
    }

    /**
     * Legt den Wert der mobilityManagerDefaultTerminatingServiceKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMobilityManagerDefaultTerminatingServiceKey(Integer value) {
        this.mobilityManagerDefaultTerminatingServiceKey = value;
    }

    /**
     * Ruft den Wert der useCustomUserNamePassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseCustomUserNamePassword() {
        return useCustomUserNamePassword;
    }

    /**
     * Legt den Wert der useCustomUserNamePassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseCustomUserNamePassword(Boolean value) {
        this.useCustomUserNamePassword = value;
    }

    /**
     * Ruft den Wert der accessDeviceCredentials-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementUserNamePassword16 }
     *     
     */
    public DeviceManagementUserNamePassword16 getAccessDeviceCredentials() {
        return accessDeviceCredentials;
    }

    /**
     * Legt den Wert der accessDeviceCredentials-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementUserNamePassword16 }
     *     
     */
    public void setAccessDeviceCredentials(DeviceManagementUserNamePassword16 value) {
        this.accessDeviceCredentials = value;
    }

    /**
     * Ruft den Wert der useHotline-Eigenschaft ab.
     * 
     */
    public boolean isUseHotline() {
        return useHotline;
    }

    /**
     * Legt den Wert der useHotline-Eigenschaft fest.
     * 
     */
    public void setUseHotline(boolean value) {
        this.useHotline = value;
    }

    /**
     * Ruft den Wert der hotlineContact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotlineContact() {
        return hotlineContact;
    }

    /**
     * Legt den Wert der hotlineContact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotlineContact(String value) {
        this.hotlineContact = value;
    }

}
