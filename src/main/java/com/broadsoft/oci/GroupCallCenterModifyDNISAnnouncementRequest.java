//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify a call center DNIS announcement settings.
 *         The response is either a SuccessResponse or an
 *         ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterModifyDNISAnnouncementRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterModifyDNISAnnouncementRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="dnisKey" type="{}DNISKey"/>
 *         <element name="playEntranceMessage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="mandatoryEntranceMessage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="entranceAudioMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="entranceMessageAudioUrlList" type="{}CallCenterAnnouncementURLListModify" minOccurs="0"/>
 *         <element name="entranceMessageAudioFileList" type="{}CallCenterAnnouncementFileListModify" minOccurs="0"/>
 *         <element name="entranceVideoMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="entranceMessageVideoUrlList" type="{}CallCenterAnnouncementURLListModify" minOccurs="0"/>
 *         <element name="entranceMessageVideoFileList" type="{}CallCenterAnnouncementFileListModify" minOccurs="0"/>
 *         <element name="playPeriodicComfortMessage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="timeBetweenComfortMessagesSeconds" type="{}CallCenterTimeBetweenComfortMessagesSeconds" minOccurs="0"/>
 *         <element name="periodicComfortAudioMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="periodicComfortMessageAudioUrlList" type="{}CallCenterAnnouncementURLListModify" minOccurs="0"/>
 *         <element name="periodicComfortMessageAudioFileList" type="{}CallCenterAnnouncementFileListModify" minOccurs="0"/>
 *         <element name="periodicComfortVideoMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="periodicComfortMessageVideoUrlList" type="{}CallCenterAnnouncementURLListModify" minOccurs="0"/>
 *         <element name="periodicComfortMessageVideoFileList" type="{}CallCenterAnnouncementFileListModify" minOccurs="0"/>
 *         <element name="enableMediaOnHoldForQueuedCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="mediaOnHoldSource" type="{}CallCenterMediaOnHoldSourceModify17" minOccurs="0"/>
 *         <element name="playWhisperMessage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="whisperAudioMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="whisperMessageAudioUrlList" type="{}CallCenterAnnouncementURLListModify" minOccurs="0"/>
 *         <element name="whisperMessageAudioFileList" type="{}CallCenterAnnouncementFileListModify" minOccurs="0"/>
 *         <element name="whisperVideoMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="whisperMessageVideoUrlList" type="{}CallCenterAnnouncementURLListModify" minOccurs="0"/>
 *         <element name="whisperMessageVideoFileList" type="{}CallCenterAnnouncementFileListModify" minOccurs="0"/>
 *         <element name="estimatedWaitMessageOptionsModify" type="{}EstimatedWaitMessageOptionsModify" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterModifyDNISAnnouncementRequest", propOrder = {
    "dnisKey",
    "playEntranceMessage",
    "mandatoryEntranceMessage",
    "entranceAudioMessageSelection",
    "entranceMessageAudioUrlList",
    "entranceMessageAudioFileList",
    "entranceVideoMessageSelection",
    "entranceMessageVideoUrlList",
    "entranceMessageVideoFileList",
    "playPeriodicComfortMessage",
    "timeBetweenComfortMessagesSeconds",
    "periodicComfortAudioMessageSelection",
    "periodicComfortMessageAudioUrlList",
    "periodicComfortMessageAudioFileList",
    "periodicComfortVideoMessageSelection",
    "periodicComfortMessageVideoUrlList",
    "periodicComfortMessageVideoFileList",
    "enableMediaOnHoldForQueuedCalls",
    "mediaOnHoldSource",
    "playWhisperMessage",
    "whisperAudioMessageSelection",
    "whisperMessageAudioUrlList",
    "whisperMessageAudioFileList",
    "whisperVideoMessageSelection",
    "whisperMessageVideoUrlList",
    "whisperMessageVideoFileList",
    "estimatedWaitMessageOptionsModify"
})
public class GroupCallCenterModifyDNISAnnouncementRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    protected DNISKey dnisKey;
    protected Boolean playEntranceMessage;
    protected Boolean mandatoryEntranceMessage;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection entranceAudioMessageSelection;
    protected CallCenterAnnouncementURLListModify entranceMessageAudioUrlList;
    protected CallCenterAnnouncementFileListModify entranceMessageAudioFileList;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection entranceVideoMessageSelection;
    protected CallCenterAnnouncementURLListModify entranceMessageVideoUrlList;
    protected CallCenterAnnouncementFileListModify entranceMessageVideoFileList;
    protected Boolean playPeriodicComfortMessage;
    protected Integer timeBetweenComfortMessagesSeconds;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection periodicComfortAudioMessageSelection;
    protected CallCenterAnnouncementURLListModify periodicComfortMessageAudioUrlList;
    protected CallCenterAnnouncementFileListModify periodicComfortMessageAudioFileList;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection periodicComfortVideoMessageSelection;
    protected CallCenterAnnouncementURLListModify periodicComfortMessageVideoUrlList;
    protected CallCenterAnnouncementFileListModify periodicComfortMessageVideoFileList;
    protected Boolean enableMediaOnHoldForQueuedCalls;
    protected CallCenterMediaOnHoldSourceModify17 mediaOnHoldSource;
    protected Boolean playWhisperMessage;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection whisperAudioMessageSelection;
    protected CallCenterAnnouncementURLListModify whisperMessageAudioUrlList;
    protected CallCenterAnnouncementFileListModify whisperMessageAudioFileList;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection whisperVideoMessageSelection;
    protected CallCenterAnnouncementURLListModify whisperMessageVideoUrlList;
    protected CallCenterAnnouncementFileListModify whisperMessageVideoFileList;
    protected EstimatedWaitMessageOptionsModify estimatedWaitMessageOptionsModify;

    /**
     * Ruft den Wert der dnisKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DNISKey }
     *     
     */
    public DNISKey getDnisKey() {
        return dnisKey;
    }

    /**
     * Legt den Wert der dnisKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DNISKey }
     *     
     */
    public void setDnisKey(DNISKey value) {
        this.dnisKey = value;
    }

    /**
     * Ruft den Wert der playEntranceMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayEntranceMessage() {
        return playEntranceMessage;
    }

    /**
     * Legt den Wert der playEntranceMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayEntranceMessage(Boolean value) {
        this.playEntranceMessage = value;
    }

    /**
     * Ruft den Wert der mandatoryEntranceMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMandatoryEntranceMessage() {
        return mandatoryEntranceMessage;
    }

    /**
     * Legt den Wert der mandatoryEntranceMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMandatoryEntranceMessage(Boolean value) {
        this.mandatoryEntranceMessage = value;
    }

    /**
     * Ruft den Wert der entranceAudioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getEntranceAudioMessageSelection() {
        return entranceAudioMessageSelection;
    }

    /**
     * Legt den Wert der entranceAudioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setEntranceAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.entranceAudioMessageSelection = value;
    }

    /**
     * Ruft den Wert der entranceMessageAudioUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public CallCenterAnnouncementURLListModify getEntranceMessageAudioUrlList() {
        return entranceMessageAudioUrlList;
    }

    /**
     * Legt den Wert der entranceMessageAudioUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public void setEntranceMessageAudioUrlList(CallCenterAnnouncementURLListModify value) {
        this.entranceMessageAudioUrlList = value;
    }

    /**
     * Ruft den Wert der entranceMessageAudioFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListModify }
     *     
     */
    public CallCenterAnnouncementFileListModify getEntranceMessageAudioFileList() {
        return entranceMessageAudioFileList;
    }

    /**
     * Legt den Wert der entranceMessageAudioFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListModify }
     *     
     */
    public void setEntranceMessageAudioFileList(CallCenterAnnouncementFileListModify value) {
        this.entranceMessageAudioFileList = value;
    }

    /**
     * Ruft den Wert der entranceVideoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getEntranceVideoMessageSelection() {
        return entranceVideoMessageSelection;
    }

    /**
     * Legt den Wert der entranceVideoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setEntranceVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.entranceVideoMessageSelection = value;
    }

    /**
     * Ruft den Wert der entranceMessageVideoUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public CallCenterAnnouncementURLListModify getEntranceMessageVideoUrlList() {
        return entranceMessageVideoUrlList;
    }

    /**
     * Legt den Wert der entranceMessageVideoUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public void setEntranceMessageVideoUrlList(CallCenterAnnouncementURLListModify value) {
        this.entranceMessageVideoUrlList = value;
    }

    /**
     * Ruft den Wert der entranceMessageVideoFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListModify }
     *     
     */
    public CallCenterAnnouncementFileListModify getEntranceMessageVideoFileList() {
        return entranceMessageVideoFileList;
    }

    /**
     * Legt den Wert der entranceMessageVideoFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListModify }
     *     
     */
    public void setEntranceMessageVideoFileList(CallCenterAnnouncementFileListModify value) {
        this.entranceMessageVideoFileList = value;
    }

    /**
     * Ruft den Wert der playPeriodicComfortMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayPeriodicComfortMessage() {
        return playPeriodicComfortMessage;
    }

    /**
     * Legt den Wert der playPeriodicComfortMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayPeriodicComfortMessage(Boolean value) {
        this.playPeriodicComfortMessage = value;
    }

    /**
     * Ruft den Wert der timeBetweenComfortMessagesSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTimeBetweenComfortMessagesSeconds() {
        return timeBetweenComfortMessagesSeconds;
    }

    /**
     * Legt den Wert der timeBetweenComfortMessagesSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTimeBetweenComfortMessagesSeconds(Integer value) {
        this.timeBetweenComfortMessagesSeconds = value;
    }

    /**
     * Ruft den Wert der periodicComfortAudioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getPeriodicComfortAudioMessageSelection() {
        return periodicComfortAudioMessageSelection;
    }

    /**
     * Legt den Wert der periodicComfortAudioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setPeriodicComfortAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.periodicComfortAudioMessageSelection = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageAudioUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public CallCenterAnnouncementURLListModify getPeriodicComfortMessageAudioUrlList() {
        return periodicComfortMessageAudioUrlList;
    }

    /**
     * Legt den Wert der periodicComfortMessageAudioUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public void setPeriodicComfortMessageAudioUrlList(CallCenterAnnouncementURLListModify value) {
        this.periodicComfortMessageAudioUrlList = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageAudioFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListModify }
     *     
     */
    public CallCenterAnnouncementFileListModify getPeriodicComfortMessageAudioFileList() {
        return periodicComfortMessageAudioFileList;
    }

    /**
     * Legt den Wert der periodicComfortMessageAudioFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListModify }
     *     
     */
    public void setPeriodicComfortMessageAudioFileList(CallCenterAnnouncementFileListModify value) {
        this.periodicComfortMessageAudioFileList = value;
    }

    /**
     * Ruft den Wert der periodicComfortVideoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getPeriodicComfortVideoMessageSelection() {
        return periodicComfortVideoMessageSelection;
    }

    /**
     * Legt den Wert der periodicComfortVideoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setPeriodicComfortVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.periodicComfortVideoMessageSelection = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageVideoUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public CallCenterAnnouncementURLListModify getPeriodicComfortMessageVideoUrlList() {
        return periodicComfortMessageVideoUrlList;
    }

    /**
     * Legt den Wert der periodicComfortMessageVideoUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public void setPeriodicComfortMessageVideoUrlList(CallCenterAnnouncementURLListModify value) {
        this.periodicComfortMessageVideoUrlList = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageVideoFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListModify }
     *     
     */
    public CallCenterAnnouncementFileListModify getPeriodicComfortMessageVideoFileList() {
        return periodicComfortMessageVideoFileList;
    }

    /**
     * Legt den Wert der periodicComfortMessageVideoFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListModify }
     *     
     */
    public void setPeriodicComfortMessageVideoFileList(CallCenterAnnouncementFileListModify value) {
        this.periodicComfortMessageVideoFileList = value;
    }

    /**
     * Ruft den Wert der enableMediaOnHoldForQueuedCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMediaOnHoldForQueuedCalls() {
        return enableMediaOnHoldForQueuedCalls;
    }

    /**
     * Legt den Wert der enableMediaOnHoldForQueuedCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMediaOnHoldForQueuedCalls(Boolean value) {
        this.enableMediaOnHoldForQueuedCalls = value;
    }

    /**
     * Ruft den Wert der mediaOnHoldSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldSourceModify17 }
     *     
     */
    public CallCenterMediaOnHoldSourceModify17 getMediaOnHoldSource() {
        return mediaOnHoldSource;
    }

    /**
     * Legt den Wert der mediaOnHoldSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldSourceModify17 }
     *     
     */
    public void setMediaOnHoldSource(CallCenterMediaOnHoldSourceModify17 value) {
        this.mediaOnHoldSource = value;
    }

    /**
     * Ruft den Wert der playWhisperMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayWhisperMessage() {
        return playWhisperMessage;
    }

    /**
     * Legt den Wert der playWhisperMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayWhisperMessage(Boolean value) {
        this.playWhisperMessage = value;
    }

    /**
     * Ruft den Wert der whisperAudioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getWhisperAudioMessageSelection() {
        return whisperAudioMessageSelection;
    }

    /**
     * Legt den Wert der whisperAudioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setWhisperAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.whisperAudioMessageSelection = value;
    }

    /**
     * Ruft den Wert der whisperMessageAudioUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public CallCenterAnnouncementURLListModify getWhisperMessageAudioUrlList() {
        return whisperMessageAudioUrlList;
    }

    /**
     * Legt den Wert der whisperMessageAudioUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public void setWhisperMessageAudioUrlList(CallCenterAnnouncementURLListModify value) {
        this.whisperMessageAudioUrlList = value;
    }

    /**
     * Ruft den Wert der whisperMessageAudioFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListModify }
     *     
     */
    public CallCenterAnnouncementFileListModify getWhisperMessageAudioFileList() {
        return whisperMessageAudioFileList;
    }

    /**
     * Legt den Wert der whisperMessageAudioFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListModify }
     *     
     */
    public void setWhisperMessageAudioFileList(CallCenterAnnouncementFileListModify value) {
        this.whisperMessageAudioFileList = value;
    }

    /**
     * Ruft den Wert der whisperVideoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getWhisperVideoMessageSelection() {
        return whisperVideoMessageSelection;
    }

    /**
     * Legt den Wert der whisperVideoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setWhisperVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.whisperVideoMessageSelection = value;
    }

    /**
     * Ruft den Wert der whisperMessageVideoUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public CallCenterAnnouncementURLListModify getWhisperMessageVideoUrlList() {
        return whisperMessageVideoUrlList;
    }

    /**
     * Legt den Wert der whisperMessageVideoUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLListModify }
     *     
     */
    public void setWhisperMessageVideoUrlList(CallCenterAnnouncementURLListModify value) {
        this.whisperMessageVideoUrlList = value;
    }

    /**
     * Ruft den Wert der whisperMessageVideoFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListModify }
     *     
     */
    public CallCenterAnnouncementFileListModify getWhisperMessageVideoFileList() {
        return whisperMessageVideoFileList;
    }

    /**
     * Legt den Wert der whisperMessageVideoFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListModify }
     *     
     */
    public void setWhisperMessageVideoFileList(CallCenterAnnouncementFileListModify value) {
        this.whisperMessageVideoFileList = value;
    }

    /**
     * Ruft den Wert der estimatedWaitMessageOptionsModify-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EstimatedWaitMessageOptionsModify }
     *     
     */
    public EstimatedWaitMessageOptionsModify getEstimatedWaitMessageOptionsModify() {
        return estimatedWaitMessageOptionsModify;
    }

    /**
     * Legt den Wert der estimatedWaitMessageOptionsModify-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EstimatedWaitMessageOptionsModify }
     *     
     */
    public void setEstimatedWaitMessageOptionsModify(EstimatedWaitMessageOptionsModify value) {
        this.estimatedWaitMessageOptionsModify = value;
    }

}
