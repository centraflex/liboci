//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the service provider or enterprise level data associated with Emergency 
 *         Call Notification.  The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderEmergencyCallNotificationModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderEmergencyCallNotificationModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="sendEmergencyCallNotificationEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="emergencyCallNotifyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="allowGroupOverride" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderEmergencyCallNotificationModifyRequest", propOrder = {
    "serviceProviderId",
    "sendEmergencyCallNotificationEmail",
    "emergencyCallNotifyEmailAddress",
    "allowGroupOverride"
})
public class ServiceProviderEmergencyCallNotificationModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected Boolean sendEmergencyCallNotificationEmail;
    @XmlElementRef(name = "emergencyCallNotifyEmailAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emergencyCallNotifyEmailAddress;
    protected Boolean allowGroupOverride;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der sendEmergencyCallNotificationEmail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendEmergencyCallNotificationEmail() {
        return sendEmergencyCallNotificationEmail;
    }

    /**
     * Legt den Wert der sendEmergencyCallNotificationEmail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendEmergencyCallNotificationEmail(Boolean value) {
        this.sendEmergencyCallNotificationEmail = value;
    }

    /**
     * Ruft den Wert der emergencyCallNotifyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmergencyCallNotifyEmailAddress() {
        return emergencyCallNotifyEmailAddress;
    }

    /**
     * Legt den Wert der emergencyCallNotifyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmergencyCallNotifyEmailAddress(JAXBElement<String> value) {
        this.emergencyCallNotifyEmailAddress = value;
    }

    /**
     * Ruft den Wert der allowGroupOverride-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowGroupOverride() {
        return allowGroupOverride;
    }

    /**
     * Legt den Wert der allowGroupOverride-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowGroupOverride(Boolean value) {
        this.allowGroupOverride = value;
    }

}
