//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify CPE Config system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *         
 *         Replaced by: SystemCPEConfigParametersModifyRequest20.
 *       
 * 
 * <p>Java-Klasse für SystemCPEConfigParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCPEConfigParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enableIPDeviceManagement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="ftpConnectTimeoutSeconds" type="{}DeviceManagementFTPConnectTimeoutSeconds" minOccurs="0"/>
 *         <element name="ftpFileTransferTimeoutSeconds" type="{}DeviceManagementFTPFileTransferTimeoutSeconds" minOccurs="0"/>
 *         <element name="pauseBetweenFileRebuildMilliseconds" type="{}DeviceManagementPauseBetweenFileRebuildMilliseconds" minOccurs="0"/>
 *         <element name="maxBusyTimeMinutes" type="{}DeviceManagementMaxBusyTimeMinutes" minOccurs="0"/>
 *         <element name="deviceAccessAppServerClusterName" type="{}NetAddress" minOccurs="0"/>
 *         <choice minOccurs="0">
 *           <element name="fileRebuildImmediate" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *           <element name="fileRebuildDaily">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="startHour" type="{}Hour"/>
 *                     <element name="startMinute" type="{}Minute"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *           <element name="fileRebuildHourly">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="startMinute" type="{}Minute"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCPEConfigParametersModifyRequest", propOrder = {
    "enableIPDeviceManagement",
    "ftpConnectTimeoutSeconds",
    "ftpFileTransferTimeoutSeconds",
    "pauseBetweenFileRebuildMilliseconds",
    "maxBusyTimeMinutes",
    "deviceAccessAppServerClusterName",
    "fileRebuildImmediate",
    "fileRebuildDaily",
    "fileRebuildHourly"
})
public class SystemCPEConfigParametersModifyRequest
    extends OCIRequest
{

    protected Boolean enableIPDeviceManagement;
    protected Integer ftpConnectTimeoutSeconds;
    protected Integer ftpFileTransferTimeoutSeconds;
    protected Integer pauseBetweenFileRebuildMilliseconds;
    protected Integer maxBusyTimeMinutes;
    @XmlElementRef(name = "deviceAccessAppServerClusterName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deviceAccessAppServerClusterName;
    protected Object fileRebuildImmediate;
    protected SystemCPEConfigParametersModifyRequest.FileRebuildDaily fileRebuildDaily;
    protected SystemCPEConfigParametersModifyRequest.FileRebuildHourly fileRebuildHourly;

    /**
     * Ruft den Wert der enableIPDeviceManagement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableIPDeviceManagement() {
        return enableIPDeviceManagement;
    }

    /**
     * Legt den Wert der enableIPDeviceManagement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableIPDeviceManagement(Boolean value) {
        this.enableIPDeviceManagement = value;
    }

    /**
     * Ruft den Wert der ftpConnectTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFtpConnectTimeoutSeconds() {
        return ftpConnectTimeoutSeconds;
    }

    /**
     * Legt den Wert der ftpConnectTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFtpConnectTimeoutSeconds(Integer value) {
        this.ftpConnectTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der ftpFileTransferTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFtpFileTransferTimeoutSeconds() {
        return ftpFileTransferTimeoutSeconds;
    }

    /**
     * Legt den Wert der ftpFileTransferTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFtpFileTransferTimeoutSeconds(Integer value) {
        this.ftpFileTransferTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der pauseBetweenFileRebuildMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPauseBetweenFileRebuildMilliseconds() {
        return pauseBetweenFileRebuildMilliseconds;
    }

    /**
     * Legt den Wert der pauseBetweenFileRebuildMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPauseBetweenFileRebuildMilliseconds(Integer value) {
        this.pauseBetweenFileRebuildMilliseconds = value;
    }

    /**
     * Ruft den Wert der maxBusyTimeMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxBusyTimeMinutes() {
        return maxBusyTimeMinutes;
    }

    /**
     * Legt den Wert der maxBusyTimeMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxBusyTimeMinutes(Integer value) {
        this.maxBusyTimeMinutes = value;
    }

    /**
     * Ruft den Wert der deviceAccessAppServerClusterName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeviceAccessAppServerClusterName() {
        return deviceAccessAppServerClusterName;
    }

    /**
     * Legt den Wert der deviceAccessAppServerClusterName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeviceAccessAppServerClusterName(JAXBElement<String> value) {
        this.deviceAccessAppServerClusterName = value;
    }

    /**
     * Ruft den Wert der fileRebuildImmediate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getFileRebuildImmediate() {
        return fileRebuildImmediate;
    }

    /**
     * Legt den Wert der fileRebuildImmediate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setFileRebuildImmediate(Object value) {
        this.fileRebuildImmediate = value;
    }

    /**
     * Ruft den Wert der fileRebuildDaily-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemCPEConfigParametersModifyRequest.FileRebuildDaily }
     *     
     */
    public SystemCPEConfigParametersModifyRequest.FileRebuildDaily getFileRebuildDaily() {
        return fileRebuildDaily;
    }

    /**
     * Legt den Wert der fileRebuildDaily-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemCPEConfigParametersModifyRequest.FileRebuildDaily }
     *     
     */
    public void setFileRebuildDaily(SystemCPEConfigParametersModifyRequest.FileRebuildDaily value) {
        this.fileRebuildDaily = value;
    }

    /**
     * Ruft den Wert der fileRebuildHourly-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemCPEConfigParametersModifyRequest.FileRebuildHourly }
     *     
     */
    public SystemCPEConfigParametersModifyRequest.FileRebuildHourly getFileRebuildHourly() {
        return fileRebuildHourly;
    }

    /**
     * Legt den Wert der fileRebuildHourly-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemCPEConfigParametersModifyRequest.FileRebuildHourly }
     *     
     */
    public void setFileRebuildHourly(SystemCPEConfigParametersModifyRequest.FileRebuildHourly value) {
        this.fileRebuildHourly = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="startHour" type="{}Hour"/>
     *         <element name="startMinute" type="{}Minute"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "startHour",
        "startMinute"
    })
    public static class FileRebuildDaily {

        protected int startHour;
        protected int startMinute;

        /**
         * Ruft den Wert der startHour-Eigenschaft ab.
         * 
         */
        public int getStartHour() {
            return startHour;
        }

        /**
         * Legt den Wert der startHour-Eigenschaft fest.
         * 
         */
        public void setStartHour(int value) {
            this.startHour = value;
        }

        /**
         * Ruft den Wert der startMinute-Eigenschaft ab.
         * 
         */
        public int getStartMinute() {
            return startMinute;
        }

        /**
         * Legt den Wert der startMinute-Eigenschaft fest.
         * 
         */
        public void setStartMinute(int value) {
            this.startMinute = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="startMinute" type="{}Minute"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "startMinute"
    })
    public static class FileRebuildHourly {

        protected int startMinute;

        /**
         * Ruft den Wert der startMinute-Eigenschaft ab.
         * 
         */
        public int getStartMinute() {
            return startMinute;
        }

        /**
         * Legt den Wert der startMinute-Eigenschaft fest.
         * 
         */
        public void setStartMinute(int value) {
            this.startMinute = value;
        }

    }

}
