//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupExtensionLengthGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupExtensionLengthGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupExtensionLengthGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="extensionLength" type="{}GroupExtensionLength"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupExtensionLengthGetResponse", propOrder = {
    "extensionLength"
})
public class GroupExtensionLengthGetResponse
    extends OCIDataResponse
{

    protected int extensionLength;

    /**
     * Ruft den Wert der extensionLength-Eigenschaft ab.
     * 
     */
    public int getExtensionLength() {
        return extensionLength;
    }

    /**
     * Legt den Wert der extensionLength-Eigenschaft fest.
     * 
     */
    public void setExtensionLength(int value) {
        this.extensionLength = value;
    }

}
