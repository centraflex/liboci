//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the expensive call notification service parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemExpensiveCallNotificationModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemExpensiveCallNotificationModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enablePostAnnouncementDelayTimer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="postAnnouncementDelaySeconds" type="{}ExpensiveCallNotificationPostAnnouncementDelaySeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemExpensiveCallNotificationModifyRequest", propOrder = {
    "enablePostAnnouncementDelayTimer",
    "postAnnouncementDelaySeconds"
})
public class SystemExpensiveCallNotificationModifyRequest
    extends OCIRequest
{

    protected Boolean enablePostAnnouncementDelayTimer;
    protected Integer postAnnouncementDelaySeconds;

    /**
     * Ruft den Wert der enablePostAnnouncementDelayTimer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnablePostAnnouncementDelayTimer() {
        return enablePostAnnouncementDelayTimer;
    }

    /**
     * Legt den Wert der enablePostAnnouncementDelayTimer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnablePostAnnouncementDelayTimer(Boolean value) {
        this.enablePostAnnouncementDelayTimer = value;
    }

    /**
     * Ruft den Wert der postAnnouncementDelaySeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPostAnnouncementDelaySeconds() {
        return postAnnouncementDelaySeconds;
    }

    /**
     * Legt den Wert der postAnnouncementDelaySeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPostAnnouncementDelaySeconds(Integer value) {
        this.postAnnouncementDelaySeconds = value;
    }

}
