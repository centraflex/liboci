//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupDnGetAssignmentPagedSortedListRequest.
 *         
 *         The response contains a table with columns: "Phone Numbers", "Department", "Department Type", 
 *         "Parent Department", "Parent Department Type", "Activated", "Available", "User Id",
 *         "Last Name", "First Name", "Extension", "Email Address", "User Type", "Country Code", "National Prefix".
 *         The "Phone Numbers" column contains either a single DN or a range of DNs.
 *         The "User Id", "Last Name" and "First Name" columns contains the corresponding attributes of the user possessing the DN(s).
 *         For a service instance, "Last Name" contains the service instance name and "First Name" column contains the corresponding enumerated UserType value.
 *         The "Department" column contains the department of the DN, not the department of the user or service instance.
 *         The "Department Type" and "Parent Department Type" columns will contain the values "Enterprise" or "Group".
 *         The "Activated" column indicates if the DN or DN range has been activated.  Only has a value if the DN(s) is assigned to a user.
 *         The "User Type" column contains the corresponding enumerated UserType value.
 *         The "Country Code" column indicates the dialing prefix for the phone number.
 *         The "National Prefix" column indicates the digit sequence to be dialed before the telephone number.
 *         NOTE: the same phone number can show up in the list twice if the phone number is being used as the group calling line Id. 
 *       
 * 
 * <p>Java-Klasse für GroupDnGetAssignmentPagedSortedListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDnGetAssignmentPagedSortedListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="dnTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDnGetAssignmentPagedSortedListResponse", propOrder = {
    "dnTable"
})
public class GroupDnGetAssignmentPagedSortedListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable dnTable;

    /**
     * Ruft den Wert der dnTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDnTable() {
        return dnTable;
    }

    /**
     * Legt den Wert der dnTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDnTable(OCITable value) {
        this.dnTable = value;
    }

}
