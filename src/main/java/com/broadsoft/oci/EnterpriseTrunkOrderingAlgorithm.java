//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für EnterpriseTrunkOrderingAlgorithm.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="EnterpriseTrunkOrderingAlgorithm">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Ordered Load Balancing"/>
 *     <enumeration value="Overflow"/>
 *     <enumeration value="Most Idle"/>
 *     <enumeration value="Least Idle"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "EnterpriseTrunkOrderingAlgorithm")
@XmlEnum
public enum EnterpriseTrunkOrderingAlgorithm {

    @XmlEnumValue("Ordered Load Balancing")
    ORDERED_LOAD_BALANCING("Ordered Load Balancing"),
    @XmlEnumValue("Overflow")
    OVERFLOW("Overflow"),
    @XmlEnumValue("Most Idle")
    MOST_IDLE("Most Idle"),
    @XmlEnumValue("Least Idle")
    LEAST_IDLE("Least Idle");
    private final String value;

    EnterpriseTrunkOrderingAlgorithm(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnterpriseTrunkOrderingAlgorithm fromValue(String v) {
        for (EnterpriseTrunkOrderingAlgorithm c: EnterpriseTrunkOrderingAlgorithm.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
