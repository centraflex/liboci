//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemCallProcessingPolicyProfileVoicePortalProfileGetRequest22V2.
 *         The following elements are only used in AS data mode:
 *           useMaxCallsPerSecond, value "false" is returned in XS data mode.
 *           maxCallsPerSecond, value "1" is returned in XS data mode.
 *       
 * 
 * <p>Java-Klasse für SystemCallProcessingPolicyProfileVoicePortalProfileGetResponse22V2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallProcessingPolicyProfileVoicePortalProfileGetResponse22V2">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useCLIDPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="clidPolicy" type="{}GroupCLIDPolicy"/>
 *         <element name="useGroupName" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="blockCallingNameForExternalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowConfigurableCLIDForRedirectingIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowDepartmentCLIDNameOverride" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enterpriseCallsCLIDPolicy" type="{}EnterpriseInternalCallsCLIDPolicy"/>
 *         <element name="enterpriseGroupCallsCLIDPolicy" type="{}EnterpriseInternalCallsCLIDPolicy"/>
 *         <element name="serviceProviderGroupCallsCLIDPolicy" type="{}ServiceProviderInternalCallsCLIDPolicy"/>
 *         <element name="useCallLimitsPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useMaxSimultaneousCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxSimultaneousCalls" type="{}CallProcessingMaxSimultaneousCalls19sp1"/>
 *         <element name="useMaxSimultaneousVideoCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxSimultaneousVideoCalls" type="{}CallProcessingMaxSimultaneousCalls19sp1"/>
 *         <element name="useMaxCallTimeForAnsweredCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxCallTimeForAnsweredCallsMinutes" type="{}CallProcessingMaxCallTimeForAnsweredCallsMinutes16"/>
 *         <element name="maxRedirectionDepth" type="{}CallProcessingMaxRedirectionDepth19sp1"/>
 *         <element name="useTranslationRoutingPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="networkUsageSelection" type="{}NetworkUsageSelection"/>
 *         <element name="enableEnterpriseExtensionDialing" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowEnterpriseGroupCallTypingForPrivateDialingPlan" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowEnterpriseGroupCallTypingForPublicDialingPlan" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeRedirectionsInMaximumNumberOfConcurrentCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useMaxCallsPerSecond" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxCallsPerSecond" type="{}MaxCallsPerSecondLimit" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallProcessingPolicyProfileVoicePortalProfileGetResponse22V2", propOrder = {
    "useCLIDPolicy",
    "clidPolicy",
    "useGroupName",
    "blockCallingNameForExternalCalls",
    "allowConfigurableCLIDForRedirectingIdentity",
    "allowDepartmentCLIDNameOverride",
    "enterpriseCallsCLIDPolicy",
    "enterpriseGroupCallsCLIDPolicy",
    "serviceProviderGroupCallsCLIDPolicy",
    "useCallLimitsPolicy",
    "useMaxSimultaneousCalls",
    "maxSimultaneousCalls",
    "useMaxSimultaneousVideoCalls",
    "maxSimultaneousVideoCalls",
    "useMaxCallTimeForAnsweredCalls",
    "maxCallTimeForAnsweredCallsMinutes",
    "maxRedirectionDepth",
    "useTranslationRoutingPolicy",
    "networkUsageSelection",
    "enableEnterpriseExtensionDialing",
    "allowEnterpriseGroupCallTypingForPrivateDialingPlan",
    "allowEnterpriseGroupCallTypingForPublicDialingPlan",
    "includeRedirectionsInMaximumNumberOfConcurrentCalls",
    "useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable",
    "useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable",
    "useMaxCallsPerSecond",
    "maxCallsPerSecond"
})
public class SystemCallProcessingPolicyProfileVoicePortalProfileGetResponse22V2
    extends OCIDataResponse
{

    protected boolean useCLIDPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupCLIDPolicy clidPolicy;
    protected boolean useGroupName;
    protected boolean blockCallingNameForExternalCalls;
    protected boolean allowConfigurableCLIDForRedirectingIdentity;
    protected boolean allowDepartmentCLIDNameOverride;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseInternalCallsCLIDPolicy enterpriseCallsCLIDPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseInternalCallsCLIDPolicy enterpriseGroupCallsCLIDPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderInternalCallsCLIDPolicy serviceProviderGroupCallsCLIDPolicy;
    protected boolean useCallLimitsPolicy;
    protected boolean useMaxSimultaneousCalls;
    protected int maxSimultaneousCalls;
    protected boolean useMaxSimultaneousVideoCalls;
    protected int maxSimultaneousVideoCalls;
    protected boolean useMaxCallTimeForAnsweredCalls;
    protected int maxCallTimeForAnsweredCallsMinutes;
    protected int maxRedirectionDepth;
    protected boolean useTranslationRoutingPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected NetworkUsageSelection networkUsageSelection;
    protected boolean enableEnterpriseExtensionDialing;
    protected boolean allowEnterpriseGroupCallTypingForPrivateDialingPlan;
    protected boolean allowEnterpriseGroupCallTypingForPublicDialingPlan;
    protected boolean includeRedirectionsInMaximumNumberOfConcurrentCalls;
    protected boolean useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable;
    protected boolean useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable;
    protected Boolean useMaxCallsPerSecond;
    protected Integer maxCallsPerSecond;

    /**
     * Ruft den Wert der useCLIDPolicy-Eigenschaft ab.
     * 
     */
    public boolean isUseCLIDPolicy() {
        return useCLIDPolicy;
    }

    /**
     * Legt den Wert der useCLIDPolicy-Eigenschaft fest.
     * 
     */
    public void setUseCLIDPolicy(boolean value) {
        this.useCLIDPolicy = value;
    }

    /**
     * Ruft den Wert der clidPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public GroupCLIDPolicy getClidPolicy() {
        return clidPolicy;
    }

    /**
     * Legt den Wert der clidPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public void setClidPolicy(GroupCLIDPolicy value) {
        this.clidPolicy = value;
    }

    /**
     * Ruft den Wert der useGroupName-Eigenschaft ab.
     * 
     */
    public boolean isUseGroupName() {
        return useGroupName;
    }

    /**
     * Legt den Wert der useGroupName-Eigenschaft fest.
     * 
     */
    public void setUseGroupName(boolean value) {
        this.useGroupName = value;
    }

    /**
     * Ruft den Wert der blockCallingNameForExternalCalls-Eigenschaft ab.
     * 
     */
    public boolean isBlockCallingNameForExternalCalls() {
        return blockCallingNameForExternalCalls;
    }

    /**
     * Legt den Wert der blockCallingNameForExternalCalls-Eigenschaft fest.
     * 
     */
    public void setBlockCallingNameForExternalCalls(boolean value) {
        this.blockCallingNameForExternalCalls = value;
    }

    /**
     * Ruft den Wert der allowConfigurableCLIDForRedirectingIdentity-Eigenschaft ab.
     * 
     */
    public boolean isAllowConfigurableCLIDForRedirectingIdentity() {
        return allowConfigurableCLIDForRedirectingIdentity;
    }

    /**
     * Legt den Wert der allowConfigurableCLIDForRedirectingIdentity-Eigenschaft fest.
     * 
     */
    public void setAllowConfigurableCLIDForRedirectingIdentity(boolean value) {
        this.allowConfigurableCLIDForRedirectingIdentity = value;
    }

    /**
     * Ruft den Wert der allowDepartmentCLIDNameOverride-Eigenschaft ab.
     * 
     */
    public boolean isAllowDepartmentCLIDNameOverride() {
        return allowDepartmentCLIDNameOverride;
    }

    /**
     * Legt den Wert der allowDepartmentCLIDNameOverride-Eigenschaft fest.
     * 
     */
    public void setAllowDepartmentCLIDNameOverride(boolean value) {
        this.allowDepartmentCLIDNameOverride = value;
    }

    /**
     * Ruft den Wert der enterpriseCallsCLIDPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public EnterpriseInternalCallsCLIDPolicy getEnterpriseCallsCLIDPolicy() {
        return enterpriseCallsCLIDPolicy;
    }

    /**
     * Legt den Wert der enterpriseCallsCLIDPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public void setEnterpriseCallsCLIDPolicy(EnterpriseInternalCallsCLIDPolicy value) {
        this.enterpriseCallsCLIDPolicy = value;
    }

    /**
     * Ruft den Wert der enterpriseGroupCallsCLIDPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public EnterpriseInternalCallsCLIDPolicy getEnterpriseGroupCallsCLIDPolicy() {
        return enterpriseGroupCallsCLIDPolicy;
    }

    /**
     * Legt den Wert der enterpriseGroupCallsCLIDPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public void setEnterpriseGroupCallsCLIDPolicy(EnterpriseInternalCallsCLIDPolicy value) {
        this.enterpriseGroupCallsCLIDPolicy = value;
    }

    /**
     * Ruft den Wert der serviceProviderGroupCallsCLIDPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderInternalCallsCLIDPolicy }
     *     
     */
    public ServiceProviderInternalCallsCLIDPolicy getServiceProviderGroupCallsCLIDPolicy() {
        return serviceProviderGroupCallsCLIDPolicy;
    }

    /**
     * Legt den Wert der serviceProviderGroupCallsCLIDPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderInternalCallsCLIDPolicy }
     *     
     */
    public void setServiceProviderGroupCallsCLIDPolicy(ServiceProviderInternalCallsCLIDPolicy value) {
        this.serviceProviderGroupCallsCLIDPolicy = value;
    }

    /**
     * Ruft den Wert der useCallLimitsPolicy-Eigenschaft ab.
     * 
     */
    public boolean isUseCallLimitsPolicy() {
        return useCallLimitsPolicy;
    }

    /**
     * Legt den Wert der useCallLimitsPolicy-Eigenschaft fest.
     * 
     */
    public void setUseCallLimitsPolicy(boolean value) {
        this.useCallLimitsPolicy = value;
    }

    /**
     * Ruft den Wert der useMaxSimultaneousCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxSimultaneousCalls() {
        return useMaxSimultaneousCalls;
    }

    /**
     * Legt den Wert der useMaxSimultaneousCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxSimultaneousCalls(boolean value) {
        this.useMaxSimultaneousCalls = value;
    }

    /**
     * Ruft den Wert der maxSimultaneousCalls-Eigenschaft ab.
     * 
     */
    public int getMaxSimultaneousCalls() {
        return maxSimultaneousCalls;
    }

    /**
     * Legt den Wert der maxSimultaneousCalls-Eigenschaft fest.
     * 
     */
    public void setMaxSimultaneousCalls(int value) {
        this.maxSimultaneousCalls = value;
    }

    /**
     * Ruft den Wert der useMaxSimultaneousVideoCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxSimultaneousVideoCalls() {
        return useMaxSimultaneousVideoCalls;
    }

    /**
     * Legt den Wert der useMaxSimultaneousVideoCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxSimultaneousVideoCalls(boolean value) {
        this.useMaxSimultaneousVideoCalls = value;
    }

    /**
     * Ruft den Wert der maxSimultaneousVideoCalls-Eigenschaft ab.
     * 
     */
    public int getMaxSimultaneousVideoCalls() {
        return maxSimultaneousVideoCalls;
    }

    /**
     * Legt den Wert der maxSimultaneousVideoCalls-Eigenschaft fest.
     * 
     */
    public void setMaxSimultaneousVideoCalls(int value) {
        this.maxSimultaneousVideoCalls = value;
    }

    /**
     * Ruft den Wert der useMaxCallTimeForAnsweredCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxCallTimeForAnsweredCalls() {
        return useMaxCallTimeForAnsweredCalls;
    }

    /**
     * Legt den Wert der useMaxCallTimeForAnsweredCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxCallTimeForAnsweredCalls(boolean value) {
        this.useMaxCallTimeForAnsweredCalls = value;
    }

    /**
     * Ruft den Wert der maxCallTimeForAnsweredCallsMinutes-Eigenschaft ab.
     * 
     */
    public int getMaxCallTimeForAnsweredCallsMinutes() {
        return maxCallTimeForAnsweredCallsMinutes;
    }

    /**
     * Legt den Wert der maxCallTimeForAnsweredCallsMinutes-Eigenschaft fest.
     * 
     */
    public void setMaxCallTimeForAnsweredCallsMinutes(int value) {
        this.maxCallTimeForAnsweredCallsMinutes = value;
    }

    /**
     * Ruft den Wert der maxRedirectionDepth-Eigenschaft ab.
     * 
     */
    public int getMaxRedirectionDepth() {
        return maxRedirectionDepth;
    }

    /**
     * Legt den Wert der maxRedirectionDepth-Eigenschaft fest.
     * 
     */
    public void setMaxRedirectionDepth(int value) {
        this.maxRedirectionDepth = value;
    }

    /**
     * Ruft den Wert der useTranslationRoutingPolicy-Eigenschaft ab.
     * 
     */
    public boolean isUseTranslationRoutingPolicy() {
        return useTranslationRoutingPolicy;
    }

    /**
     * Legt den Wert der useTranslationRoutingPolicy-Eigenschaft fest.
     * 
     */
    public void setUseTranslationRoutingPolicy(boolean value) {
        this.useTranslationRoutingPolicy = value;
    }

    /**
     * Ruft den Wert der networkUsageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkUsageSelection }
     *     
     */
    public NetworkUsageSelection getNetworkUsageSelection() {
        return networkUsageSelection;
    }

    /**
     * Legt den Wert der networkUsageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkUsageSelection }
     *     
     */
    public void setNetworkUsageSelection(NetworkUsageSelection value) {
        this.networkUsageSelection = value;
    }

    /**
     * Ruft den Wert der enableEnterpriseExtensionDialing-Eigenschaft ab.
     * 
     */
    public boolean isEnableEnterpriseExtensionDialing() {
        return enableEnterpriseExtensionDialing;
    }

    /**
     * Legt den Wert der enableEnterpriseExtensionDialing-Eigenschaft fest.
     * 
     */
    public void setEnableEnterpriseExtensionDialing(boolean value) {
        this.enableEnterpriseExtensionDialing = value;
    }

    /**
     * Ruft den Wert der allowEnterpriseGroupCallTypingForPrivateDialingPlan-Eigenschaft ab.
     * 
     */
    public boolean isAllowEnterpriseGroupCallTypingForPrivateDialingPlan() {
        return allowEnterpriseGroupCallTypingForPrivateDialingPlan;
    }

    /**
     * Legt den Wert der allowEnterpriseGroupCallTypingForPrivateDialingPlan-Eigenschaft fest.
     * 
     */
    public void setAllowEnterpriseGroupCallTypingForPrivateDialingPlan(boolean value) {
        this.allowEnterpriseGroupCallTypingForPrivateDialingPlan = value;
    }

    /**
     * Ruft den Wert der allowEnterpriseGroupCallTypingForPublicDialingPlan-Eigenschaft ab.
     * 
     */
    public boolean isAllowEnterpriseGroupCallTypingForPublicDialingPlan() {
        return allowEnterpriseGroupCallTypingForPublicDialingPlan;
    }

    /**
     * Legt den Wert der allowEnterpriseGroupCallTypingForPublicDialingPlan-Eigenschaft fest.
     * 
     */
    public void setAllowEnterpriseGroupCallTypingForPublicDialingPlan(boolean value) {
        this.allowEnterpriseGroupCallTypingForPublicDialingPlan = value;
    }

    /**
     * Ruft den Wert der includeRedirectionsInMaximumNumberOfConcurrentCalls-Eigenschaft ab.
     * 
     */
    public boolean isIncludeRedirectionsInMaximumNumberOfConcurrentCalls() {
        return includeRedirectionsInMaximumNumberOfConcurrentCalls;
    }

    /**
     * Legt den Wert der includeRedirectionsInMaximumNumberOfConcurrentCalls-Eigenschaft fest.
     * 
     */
    public void setIncludeRedirectionsInMaximumNumberOfConcurrentCalls(boolean value) {
        this.includeRedirectionsInMaximumNumberOfConcurrentCalls = value;
    }

    /**
     * Ruft den Wert der useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable-Eigenschaft ab.
     * 
     */
    public boolean isUseUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable() {
        return useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable;
    }

    /**
     * Legt den Wert der useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable-Eigenschaft fest.
     * 
     */
    public void setUseUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable(boolean value) {
        this.useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable = value;
    }

    /**
     * Ruft den Wert der useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable-Eigenschaft ab.
     * 
     */
    public boolean isUseUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable() {
        return useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable;
    }

    /**
     * Legt den Wert der useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable-Eigenschaft fest.
     * 
     */
    public void setUseUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable(boolean value) {
        this.useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable = value;
    }

    /**
     * Ruft den Wert der useMaxCallsPerSecond-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxCallsPerSecond() {
        return useMaxCallsPerSecond;
    }

    /**
     * Legt den Wert der useMaxCallsPerSecond-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxCallsPerSecond(Boolean value) {
        this.useMaxCallsPerSecond = value;
    }

    /**
     * Ruft den Wert der maxCallsPerSecond-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCallsPerSecond() {
        return maxCallsPerSecond;
    }

    /**
     * Legt den Wert der maxCallsPerSecond-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCallsPerSecond(Integer value) {
        this.maxCallsPerSecond = value;
    }

}
