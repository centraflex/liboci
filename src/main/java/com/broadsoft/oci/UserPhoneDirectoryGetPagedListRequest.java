//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request a table containing the phone directory for a user.  
 *         Setting isEnterpriseRequested to true will return enterprise directory
 *         members in the response if the user is in an enterprise. Otherwise,
 *         just the group directory members for a user are returned in the response.
 *         The response is either UserPhoneDirectoryGetPagedListResponse or 
 *         ErrorResponse.
 *         The search can be done using multiple criteria.
 *         If the searchCriteriaModeOr is present, any result matching any one criteria is included in 
 *         the results. Otherwise, only results matching all the search criteria are included in the results.
 *         If no search criteria is specified, all results are returned.
 *         In all cases, if a responseSizeLimit is specified and the number of matching results is more than 
 *         this limit, then an ErrorResponse is returned.
 *         Specifying searchCriteriaModeOr without any search criteria results in an ErrorResponse.
 *         The boolean sortByFirstName is optional. If it is not specified, the response is 
 *         sorted by Last Name.  The Receptionist Note column is only populated, if the user sending 
 *         the request is the owner of the Receptionist Note and a Note exists.
 *       
 * 
 * <p>Java-Klasse für UserPhoneDirectoryGetPagedListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPhoneDirectoryGetPagedListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="isEnterpriseInfoRequested" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl"/>
 *         <element name="searchCriteriaModeOr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="searchCriteriaUserLastName" type="{}SearchCriteriaUserLastName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaUserFirstName" type="{}SearchCriteriaUserFirstName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDn" type="{}SearchCriteriaDn" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaGroupLocationCode" type="{}SearchCriteriaGroupLocationCode" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExtension" type="{}SearchCriteriaExtension" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaMobilePhoneNumber" type="{}SearchCriteriaMobilePhoneNumber" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaEmailAddress" type="{}SearchCriteriaEmailAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaYahooId" type="{}SearchCriteriaYahooId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactUserGroup" type="{}SearchCriteriaExactUserGroup" minOccurs="0"/>
 *         <element name="searchCriteriaExactUserDepartment" type="{}SearchCriteriaExactUserDepartment" minOccurs="0"/>
 *         <element name="searchCriteriaUserId" type="{}SearchCriteriaUserId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaImpId" type="{}SearchCriteriaImpId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="sortByFirstName" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="searchCriteriaTitle" type="{}SearchCriteriaTitle" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaReceptionistNote" type="{}SearchCriteriaReceptionistNote" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPhoneDirectoryGetPagedListRequest", propOrder = {
    "userId",
    "isEnterpriseInfoRequested",
    "responsePagingControl",
    "searchCriteriaModeOr",
    "searchCriteriaUserLastName",
    "searchCriteriaUserFirstName",
    "searchCriteriaDn",
    "searchCriteriaGroupLocationCode",
    "searchCriteriaExtension",
    "searchCriteriaMobilePhoneNumber",
    "searchCriteriaEmailAddress",
    "searchCriteriaYahooId",
    "searchCriteriaExactUserGroup",
    "searchCriteriaExactUserDepartment",
    "searchCriteriaUserId",
    "searchCriteriaImpId",
    "sortByFirstName",
    "searchCriteriaTitle",
    "searchCriteriaReceptionistNote"
})
public class UserPhoneDirectoryGetPagedListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected boolean isEnterpriseInfoRequested;
    @XmlElement(required = true)
    protected ResponsePagingControl responsePagingControl;
    protected Boolean searchCriteriaModeOr;
    protected List<SearchCriteriaUserLastName> searchCriteriaUserLastName;
    protected List<SearchCriteriaUserFirstName> searchCriteriaUserFirstName;
    protected List<SearchCriteriaDn> searchCriteriaDn;
    protected List<SearchCriteriaGroupLocationCode> searchCriteriaGroupLocationCode;
    protected List<SearchCriteriaExtension> searchCriteriaExtension;
    protected List<SearchCriteriaMobilePhoneNumber> searchCriteriaMobilePhoneNumber;
    protected List<SearchCriteriaEmailAddress> searchCriteriaEmailAddress;
    protected List<SearchCriteriaYahooId> searchCriteriaYahooId;
    protected SearchCriteriaExactUserGroup searchCriteriaExactUserGroup;
    protected SearchCriteriaExactUserDepartment searchCriteriaExactUserDepartment;
    protected List<SearchCriteriaUserId> searchCriteriaUserId;
    protected List<SearchCriteriaImpId> searchCriteriaImpId;
    protected Boolean sortByFirstName;
    protected List<SearchCriteriaTitle> searchCriteriaTitle;
    protected List<SearchCriteriaReceptionistNote> searchCriteriaReceptionistNote;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der isEnterpriseInfoRequested-Eigenschaft ab.
     * 
     */
    public boolean isIsEnterpriseInfoRequested() {
        return isEnterpriseInfoRequested;
    }

    /**
     * Legt den Wert der isEnterpriseInfoRequested-Eigenschaft fest.
     * 
     */
    public void setIsEnterpriseInfoRequested(boolean value) {
        this.isEnterpriseInfoRequested = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Ruft den Wert der searchCriteriaModeOr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchCriteriaModeOr() {
        return searchCriteriaModeOr;
    }

    /**
     * Legt den Wert der searchCriteriaModeOr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchCriteriaModeOr(Boolean value) {
        this.searchCriteriaModeOr = value;
    }

    /**
     * Gets the value of the searchCriteriaUserLastName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserLastName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserLastName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserLastName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserLastName property.
     */
    public List<SearchCriteriaUserLastName> getSearchCriteriaUserLastName() {
        if (searchCriteriaUserLastName == null) {
            searchCriteriaUserLastName = new ArrayList<>();
        }
        return this.searchCriteriaUserLastName;
    }

    /**
     * Gets the value of the searchCriteriaUserFirstName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserFirstName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserFirstName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserFirstName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserFirstName property.
     */
    public List<SearchCriteriaUserFirstName> getSearchCriteriaUserFirstName() {
        if (searchCriteriaUserFirstName == null) {
            searchCriteriaUserFirstName = new ArrayList<>();
        }
        return this.searchCriteriaUserFirstName;
    }

    /**
     * Gets the value of the searchCriteriaDn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDn }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDn property.
     */
    public List<SearchCriteriaDn> getSearchCriteriaDn() {
        if (searchCriteriaDn == null) {
            searchCriteriaDn = new ArrayList<>();
        }
        return this.searchCriteriaDn;
    }

    /**
     * Gets the value of the searchCriteriaGroupLocationCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaGroupLocationCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaGroupLocationCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaGroupLocationCode }
     * 
     * 
     * @return
     *     The value of the searchCriteriaGroupLocationCode property.
     */
    public List<SearchCriteriaGroupLocationCode> getSearchCriteriaGroupLocationCode() {
        if (searchCriteriaGroupLocationCode == null) {
            searchCriteriaGroupLocationCode = new ArrayList<>();
        }
        return this.searchCriteriaGroupLocationCode;
    }

    /**
     * Gets the value of the searchCriteriaExtension property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaExtension property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaExtension().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaExtension }
     * 
     * 
     * @return
     *     The value of the searchCriteriaExtension property.
     */
    public List<SearchCriteriaExtension> getSearchCriteriaExtension() {
        if (searchCriteriaExtension == null) {
            searchCriteriaExtension = new ArrayList<>();
        }
        return this.searchCriteriaExtension;
    }

    /**
     * Gets the value of the searchCriteriaMobilePhoneNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaMobilePhoneNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaMobilePhoneNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaMobilePhoneNumber }
     * 
     * 
     * @return
     *     The value of the searchCriteriaMobilePhoneNumber property.
     */
    public List<SearchCriteriaMobilePhoneNumber> getSearchCriteriaMobilePhoneNumber() {
        if (searchCriteriaMobilePhoneNumber == null) {
            searchCriteriaMobilePhoneNumber = new ArrayList<>();
        }
        return this.searchCriteriaMobilePhoneNumber;
    }

    /**
     * Gets the value of the searchCriteriaEmailAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaEmailAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaEmailAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaEmailAddress }
     * 
     * 
     * @return
     *     The value of the searchCriteriaEmailAddress property.
     */
    public List<SearchCriteriaEmailAddress> getSearchCriteriaEmailAddress() {
        if (searchCriteriaEmailAddress == null) {
            searchCriteriaEmailAddress = new ArrayList<>();
        }
        return this.searchCriteriaEmailAddress;
    }

    /**
     * Gets the value of the searchCriteriaYahooId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaYahooId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaYahooId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaYahooId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaYahooId property.
     */
    public List<SearchCriteriaYahooId> getSearchCriteriaYahooId() {
        if (searchCriteriaYahooId == null) {
            searchCriteriaYahooId = new ArrayList<>();
        }
        return this.searchCriteriaYahooId;
    }

    /**
     * Ruft den Wert der searchCriteriaExactUserGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactUserGroup }
     *     
     */
    public SearchCriteriaExactUserGroup getSearchCriteriaExactUserGroup() {
        return searchCriteriaExactUserGroup;
    }

    /**
     * Legt den Wert der searchCriteriaExactUserGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactUserGroup }
     *     
     */
    public void setSearchCriteriaExactUserGroup(SearchCriteriaExactUserGroup value) {
        this.searchCriteriaExactUserGroup = value;
    }

    /**
     * Ruft den Wert der searchCriteriaExactUserDepartment-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactUserDepartment }
     *     
     */
    public SearchCriteriaExactUserDepartment getSearchCriteriaExactUserDepartment() {
        return searchCriteriaExactUserDepartment;
    }

    /**
     * Legt den Wert der searchCriteriaExactUserDepartment-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactUserDepartment }
     *     
     */
    public void setSearchCriteriaExactUserDepartment(SearchCriteriaExactUserDepartment value) {
        this.searchCriteriaExactUserDepartment = value;
    }

    /**
     * Gets the value of the searchCriteriaUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserId property.
     */
    public List<SearchCriteriaUserId> getSearchCriteriaUserId() {
        if (searchCriteriaUserId == null) {
            searchCriteriaUserId = new ArrayList<>();
        }
        return this.searchCriteriaUserId;
    }

    /**
     * Gets the value of the searchCriteriaImpId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaImpId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaImpId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaImpId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaImpId property.
     */
    public List<SearchCriteriaImpId> getSearchCriteriaImpId() {
        if (searchCriteriaImpId == null) {
            searchCriteriaImpId = new ArrayList<>();
        }
        return this.searchCriteriaImpId;
    }

    /**
     * Ruft den Wert der sortByFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSortByFirstName() {
        return sortByFirstName;
    }

    /**
     * Legt den Wert der sortByFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSortByFirstName(Boolean value) {
        this.sortByFirstName = value;
    }

    /**
     * Gets the value of the searchCriteriaTitle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaTitle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaTitle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaTitle }
     * 
     * 
     * @return
     *     The value of the searchCriteriaTitle property.
     */
    public List<SearchCriteriaTitle> getSearchCriteriaTitle() {
        if (searchCriteriaTitle == null) {
            searchCriteriaTitle = new ArrayList<>();
        }
        return this.searchCriteriaTitle;
    }

    /**
     * Gets the value of the searchCriteriaReceptionistNote property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaReceptionistNote property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaReceptionistNote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaReceptionistNote }
     * 
     * 
     * @return
     *     The value of the searchCriteriaReceptionistNote property.
     */
    public List<SearchCriteriaReceptionistNote> getSearchCriteriaReceptionistNote() {
        if (searchCriteriaReceptionistNote == null) {
            searchCriteriaReceptionistNote = new ArrayList<>();
        }
        return this.searchCriteriaReceptionistNote;
    }

}
