//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterMediaOnHoldMessageSelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterMediaOnHoldMessageSelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Default"/>
 *     <enumeration value="URL"/>
 *     <enumeration value="Custom"/>
 *     <enumeration value="External"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterMediaOnHoldMessageSelection")
@XmlEnum
public enum CallCenterMediaOnHoldMessageSelection {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    URL("URL"),
    @XmlEnumValue("Custom")
    CUSTOM("Custom"),
    @XmlEnumValue("External")
    EXTERNAL("External");
    private final String value;

    CallCenterMediaOnHoldMessageSelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterMediaOnHoldMessageSelection fromValue(String v) {
        for (CallCenterMediaOnHoldMessageSelection c: CallCenterMediaOnHoldMessageSelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
