//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserPolycomPhoneServicesGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserPolycomPhoneServicesGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPolycomPhoneServicesGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="integratePhoneDirectoryWithBroadWorks" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeUserPersonalPhoneListInDirectory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeGroupCustomContactDirectoryInDirectory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="groupCustomContactDirectory" type="{}CustomContactDirectoryName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPolycomPhoneServicesGetResponse", propOrder = {
    "integratePhoneDirectoryWithBroadWorks",
    "includeUserPersonalPhoneListInDirectory",
    "includeGroupCustomContactDirectoryInDirectory",
    "groupCustomContactDirectory"
})
public class UserPolycomPhoneServicesGetResponse
    extends OCIDataResponse
{

    protected boolean integratePhoneDirectoryWithBroadWorks;
    protected boolean includeUserPersonalPhoneListInDirectory;
    protected boolean includeGroupCustomContactDirectoryInDirectory;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupCustomContactDirectory;

    /**
     * Ruft den Wert der integratePhoneDirectoryWithBroadWorks-Eigenschaft ab.
     * 
     */
    public boolean isIntegratePhoneDirectoryWithBroadWorks() {
        return integratePhoneDirectoryWithBroadWorks;
    }

    /**
     * Legt den Wert der integratePhoneDirectoryWithBroadWorks-Eigenschaft fest.
     * 
     */
    public void setIntegratePhoneDirectoryWithBroadWorks(boolean value) {
        this.integratePhoneDirectoryWithBroadWorks = value;
    }

    /**
     * Ruft den Wert der includeUserPersonalPhoneListInDirectory-Eigenschaft ab.
     * 
     */
    public boolean isIncludeUserPersonalPhoneListInDirectory() {
        return includeUserPersonalPhoneListInDirectory;
    }

    /**
     * Legt den Wert der includeUserPersonalPhoneListInDirectory-Eigenschaft fest.
     * 
     */
    public void setIncludeUserPersonalPhoneListInDirectory(boolean value) {
        this.includeUserPersonalPhoneListInDirectory = value;
    }

    /**
     * Ruft den Wert der includeGroupCustomContactDirectoryInDirectory-Eigenschaft ab.
     * 
     */
    public boolean isIncludeGroupCustomContactDirectoryInDirectory() {
        return includeGroupCustomContactDirectoryInDirectory;
    }

    /**
     * Legt den Wert der includeGroupCustomContactDirectoryInDirectory-Eigenschaft fest.
     * 
     */
    public void setIncludeGroupCustomContactDirectoryInDirectory(boolean value) {
        this.includeGroupCustomContactDirectoryInDirectory = value;
    }

    /**
     * Ruft den Wert der groupCustomContactDirectory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupCustomContactDirectory() {
        return groupCustomContactDirectory;
    }

    /**
     * Legt den Wert der groupCustomContactDirectory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupCustomContactDirectory(String value) {
        this.groupCustomContactDirectory = value;
    }

}
