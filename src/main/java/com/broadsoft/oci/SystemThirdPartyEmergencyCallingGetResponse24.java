//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the SystemThirdPartyEmergencyCallingGetRequest24.
 *         The response contains the third-party emergency call service settings for the system.
 *       
 * 
 * <p>Java-Klasse für SystemThirdPartyEmergencyCallingGetResponse24 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemThirdPartyEmergencyCallingGetResponse24">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="primaryHELDServerURL" type="{}URL" minOccurs="0"/>
 *         <element name="secondaryHELDServerURL" type="{}URL" minOccurs="0"/>
 *         <element name="emergencyRouteNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="emergencyRoutePort" type="{}Port1025" minOccurs="0"/>
 *         <element name="emergencyRouteTransport" type="{}ExtendedTransportProtocol" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemThirdPartyEmergencyCallingGetResponse24", propOrder = {
    "primaryHELDServerURL",
    "secondaryHELDServerURL",
    "emergencyRouteNetAddress",
    "emergencyRoutePort",
    "emergencyRouteTransport"
})
public class SystemThirdPartyEmergencyCallingGetResponse24
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String primaryHELDServerURL;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String secondaryHELDServerURL;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String emergencyRouteNetAddress;
    protected Integer emergencyRoutePort;
    @XmlSchemaType(name = "token")
    protected ExtendedTransportProtocol emergencyRouteTransport;

    /**
     * Ruft den Wert der primaryHELDServerURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryHELDServerURL() {
        return primaryHELDServerURL;
    }

    /**
     * Legt den Wert der primaryHELDServerURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryHELDServerURL(String value) {
        this.primaryHELDServerURL = value;
    }

    /**
     * Ruft den Wert der secondaryHELDServerURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryHELDServerURL() {
        return secondaryHELDServerURL;
    }

    /**
     * Legt den Wert der secondaryHELDServerURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryHELDServerURL(String value) {
        this.secondaryHELDServerURL = value;
    }

    /**
     * Ruft den Wert der emergencyRouteNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmergencyRouteNetAddress() {
        return emergencyRouteNetAddress;
    }

    /**
     * Legt den Wert der emergencyRouteNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmergencyRouteNetAddress(String value) {
        this.emergencyRouteNetAddress = value;
    }

    /**
     * Ruft den Wert der emergencyRoutePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEmergencyRoutePort() {
        return emergencyRoutePort;
    }

    /**
     * Legt den Wert der emergencyRoutePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEmergencyRoutePort(Integer value) {
        this.emergencyRoutePort = value;
    }

    /**
     * Ruft den Wert der emergencyRouteTransport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedTransportProtocol }
     *     
     */
    public ExtendedTransportProtocol getEmergencyRouteTransport() {
        return emergencyRouteTransport;
    }

    /**
     * Legt den Wert der emergencyRouteTransport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedTransportProtocol }
     *     
     */
    public void setEmergencyRouteTransport(ExtendedTransportProtocol value) {
        this.emergencyRouteTransport = value;
    }

}
