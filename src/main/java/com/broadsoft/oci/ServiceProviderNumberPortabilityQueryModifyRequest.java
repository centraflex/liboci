//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the Service Provider Number Portability Query information.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderNumberPortabilityQueryModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderNumberPortabilityQueryModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="enableNumberPortabilityQueryForOutgoingCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableNumberPortabilityQueryForIncomingCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableNumberPortabilityQueryForNetworkCallsOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="deleteDigitPattern" type="{}DigitPattern" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderNumberPortabilityQueryModifyRequest", propOrder = {
    "serviceProviderId",
    "enableNumberPortabilityQueryForOutgoingCalls",
    "enableNumberPortabilityQueryForIncomingCalls",
    "enableNumberPortabilityQueryForNetworkCallsOnly",
    "deleteDigitPattern"
})
public class ServiceProviderNumberPortabilityQueryModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected Boolean enableNumberPortabilityQueryForOutgoingCalls;
    protected Boolean enableNumberPortabilityQueryForIncomingCalls;
    protected Boolean enableNumberPortabilityQueryForNetworkCallsOnly;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> deleteDigitPattern;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der enableNumberPortabilityQueryForOutgoingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableNumberPortabilityQueryForOutgoingCalls() {
        return enableNumberPortabilityQueryForOutgoingCalls;
    }

    /**
     * Legt den Wert der enableNumberPortabilityQueryForOutgoingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableNumberPortabilityQueryForOutgoingCalls(Boolean value) {
        this.enableNumberPortabilityQueryForOutgoingCalls = value;
    }

    /**
     * Ruft den Wert der enableNumberPortabilityQueryForIncomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableNumberPortabilityQueryForIncomingCalls() {
        return enableNumberPortabilityQueryForIncomingCalls;
    }

    /**
     * Legt den Wert der enableNumberPortabilityQueryForIncomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableNumberPortabilityQueryForIncomingCalls(Boolean value) {
        this.enableNumberPortabilityQueryForIncomingCalls = value;
    }

    /**
     * Ruft den Wert der enableNumberPortabilityQueryForNetworkCallsOnly-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableNumberPortabilityQueryForNetworkCallsOnly() {
        return enableNumberPortabilityQueryForNetworkCallsOnly;
    }

    /**
     * Legt den Wert der enableNumberPortabilityQueryForNetworkCallsOnly-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableNumberPortabilityQueryForNetworkCallsOnly(Boolean value) {
        this.enableNumberPortabilityQueryForNetworkCallsOnly = value;
    }

    /**
     * Gets the value of the deleteDigitPattern property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the deleteDigitPattern property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeleteDigitPattern().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the deleteDigitPattern property.
     */
    public List<String> getDeleteDigitPattern() {
        if (deleteDigitPattern == null) {
            deleteDigitPattern = new ArrayList<>();
        }
        return this.deleteDigitPattern;
    }

}
