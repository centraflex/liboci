//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ExternalUserIdentityRole.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ExternalUserIdentityRole">
 *   <restriction base="{C}NonEmptyToken">
 *     <enumeration value="User"/>
 *     <enumeration value="Administrator"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ExternalUserIdentityRole", namespace = "C")
@XmlEnum
public enum ExternalUserIdentityRole {

    @XmlEnumValue("User")
    USER("User"),
    @XmlEnumValue("Administrator")
    ADMINISTRATOR("Administrator");
    private final String value;

    ExternalUserIdentityRole(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExternalUserIdentityRole fromValue(String v) {
        for (ExternalUserIdentityRole c: ExternalUserIdentityRole.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
