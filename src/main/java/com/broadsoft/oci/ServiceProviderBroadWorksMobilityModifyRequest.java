//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the service provider level BroadWorks Mobility service settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderBroadWorksMobilityModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderBroadWorksMobilityModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="useSettingLevel" type="{}BroadWorksMobilityServiceProviderSettingLevel" minOccurs="0"/>
 *         <element name="enableLocationServices" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableMSRNLookup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableMobileStateChecking" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="denyCallOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="denyCallTerminations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableAnnouncementSuppression" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableInternalCLIDDelivery" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableInternalCLIDDeliveryAccessLocations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableEnhancedUnreachableStateChecking" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableNetworkCallBarringStatusCheck" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="networkTranslationIndex" type="{}NetworkTranslationIndex" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderBroadWorksMobilityModifyRequest", propOrder = {
    "serviceProviderId",
    "useSettingLevel",
    "enableLocationServices",
    "enableMSRNLookup",
    "enableMobileStateChecking",
    "denyCallOriginations",
    "denyCallTerminations",
    "enableAnnouncementSuppression",
    "enableInternalCLIDDelivery",
    "enableInternalCLIDDeliveryAccessLocations",
    "enableEnhancedUnreachableStateChecking",
    "enableNetworkCallBarringStatusCheck",
    "networkTranslationIndex"
})
public class ServiceProviderBroadWorksMobilityModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlSchemaType(name = "token")
    protected BroadWorksMobilityServiceProviderSettingLevel useSettingLevel;
    protected Boolean enableLocationServices;
    protected Boolean enableMSRNLookup;
    protected Boolean enableMobileStateChecking;
    protected Boolean denyCallOriginations;
    protected Boolean denyCallTerminations;
    protected Boolean enableAnnouncementSuppression;
    protected Boolean enableInternalCLIDDelivery;
    protected Boolean enableInternalCLIDDeliveryAccessLocations;
    protected Boolean enableEnhancedUnreachableStateChecking;
    protected Boolean enableNetworkCallBarringStatusCheck;
    @XmlElementRef(name = "networkTranslationIndex", type = JAXBElement.class, required = false)
    protected JAXBElement<String> networkTranslationIndex;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der useSettingLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksMobilityServiceProviderSettingLevel }
     *     
     */
    public BroadWorksMobilityServiceProviderSettingLevel getUseSettingLevel() {
        return useSettingLevel;
    }

    /**
     * Legt den Wert der useSettingLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksMobilityServiceProviderSettingLevel }
     *     
     */
    public void setUseSettingLevel(BroadWorksMobilityServiceProviderSettingLevel value) {
        this.useSettingLevel = value;
    }

    /**
     * Ruft den Wert der enableLocationServices-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableLocationServices() {
        return enableLocationServices;
    }

    /**
     * Legt den Wert der enableLocationServices-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableLocationServices(Boolean value) {
        this.enableLocationServices = value;
    }

    /**
     * Ruft den Wert der enableMSRNLookup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMSRNLookup() {
        return enableMSRNLookup;
    }

    /**
     * Legt den Wert der enableMSRNLookup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMSRNLookup(Boolean value) {
        this.enableMSRNLookup = value;
    }

    /**
     * Ruft den Wert der enableMobileStateChecking-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMobileStateChecking() {
        return enableMobileStateChecking;
    }

    /**
     * Legt den Wert der enableMobileStateChecking-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMobileStateChecking(Boolean value) {
        this.enableMobileStateChecking = value;
    }

    /**
     * Ruft den Wert der denyCallOriginations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDenyCallOriginations() {
        return denyCallOriginations;
    }

    /**
     * Legt den Wert der denyCallOriginations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDenyCallOriginations(Boolean value) {
        this.denyCallOriginations = value;
    }

    /**
     * Ruft den Wert der denyCallTerminations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDenyCallTerminations() {
        return denyCallTerminations;
    }

    /**
     * Legt den Wert der denyCallTerminations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDenyCallTerminations(Boolean value) {
        this.denyCallTerminations = value;
    }

    /**
     * Ruft den Wert der enableAnnouncementSuppression-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableAnnouncementSuppression() {
        return enableAnnouncementSuppression;
    }

    /**
     * Legt den Wert der enableAnnouncementSuppression-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableAnnouncementSuppression(Boolean value) {
        this.enableAnnouncementSuppression = value;
    }

    /**
     * Ruft den Wert der enableInternalCLIDDelivery-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableInternalCLIDDelivery() {
        return enableInternalCLIDDelivery;
    }

    /**
     * Legt den Wert der enableInternalCLIDDelivery-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableInternalCLIDDelivery(Boolean value) {
        this.enableInternalCLIDDelivery = value;
    }

    /**
     * Ruft den Wert der enableInternalCLIDDeliveryAccessLocations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableInternalCLIDDeliveryAccessLocations() {
        return enableInternalCLIDDeliveryAccessLocations;
    }

    /**
     * Legt den Wert der enableInternalCLIDDeliveryAccessLocations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableInternalCLIDDeliveryAccessLocations(Boolean value) {
        this.enableInternalCLIDDeliveryAccessLocations = value;
    }

    /**
     * Ruft den Wert der enableEnhancedUnreachableStateChecking-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableEnhancedUnreachableStateChecking() {
        return enableEnhancedUnreachableStateChecking;
    }

    /**
     * Legt den Wert der enableEnhancedUnreachableStateChecking-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableEnhancedUnreachableStateChecking(Boolean value) {
        this.enableEnhancedUnreachableStateChecking = value;
    }

    /**
     * Ruft den Wert der enableNetworkCallBarringStatusCheck-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableNetworkCallBarringStatusCheck() {
        return enableNetworkCallBarringStatusCheck;
    }

    /**
     * Legt den Wert der enableNetworkCallBarringStatusCheck-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableNetworkCallBarringStatusCheck(Boolean value) {
        this.enableNetworkCallBarringStatusCheck = value;
    }

    /**
     * Ruft den Wert der networkTranslationIndex-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNetworkTranslationIndex() {
        return networkTranslationIndex;
    }

    /**
     * Legt den Wert der networkTranslationIndex-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNetworkTranslationIndex(JAXBElement<String> value) {
        this.networkTranslationIndex = value;
    }

}
