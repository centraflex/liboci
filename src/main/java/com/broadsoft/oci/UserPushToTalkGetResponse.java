//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserPushToTalkGetRequest.  It returns the service settings and a
 *         9 column selected user table with the following column headings:
 *           "User Id", "Last Name", "First Name", "Hiragana Last Name", "Hiragana First Name",
 *           "Phone Number", "Extension", "Department", "Email Address", "IMP Id".
 *       
 * 
 * <p>Java-Klasse für UserPushToTalkGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPushToTalkGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="allowAutoAnswer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="outgoingConnectionSelection" type="{}PushToTalkOutgoingConnectionSelection"/>
 *         <element name="accessListSelection" type="{}PushToTalkAccessListSelection"/>
 *         <element name="selectedUserTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPushToTalkGetResponse", propOrder = {
    "allowAutoAnswer",
    "outgoingConnectionSelection",
    "accessListSelection",
    "selectedUserTable"
})
public class UserPushToTalkGetResponse
    extends OCIDataResponse
{

    protected boolean allowAutoAnswer;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected PushToTalkOutgoingConnectionSelection outgoingConnectionSelection;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected PushToTalkAccessListSelection accessListSelection;
    @XmlElement(required = true)
    protected OCITable selectedUserTable;

    /**
     * Ruft den Wert der allowAutoAnswer-Eigenschaft ab.
     * 
     */
    public boolean isAllowAutoAnswer() {
        return allowAutoAnswer;
    }

    /**
     * Legt den Wert der allowAutoAnswer-Eigenschaft fest.
     * 
     */
    public void setAllowAutoAnswer(boolean value) {
        this.allowAutoAnswer = value;
    }

    /**
     * Ruft den Wert der outgoingConnectionSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PushToTalkOutgoingConnectionSelection }
     *     
     */
    public PushToTalkOutgoingConnectionSelection getOutgoingConnectionSelection() {
        return outgoingConnectionSelection;
    }

    /**
     * Legt den Wert der outgoingConnectionSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PushToTalkOutgoingConnectionSelection }
     *     
     */
    public void setOutgoingConnectionSelection(PushToTalkOutgoingConnectionSelection value) {
        this.outgoingConnectionSelection = value;
    }

    /**
     * Ruft den Wert der accessListSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PushToTalkAccessListSelection }
     *     
     */
    public PushToTalkAccessListSelection getAccessListSelection() {
        return accessListSelection;
    }

    /**
     * Legt den Wert der accessListSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PushToTalkAccessListSelection }
     *     
     */
    public void setAccessListSelection(PushToTalkAccessListSelection value) {
        this.accessListSelection = value;
    }

    /**
     * Ruft den Wert der selectedUserTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getSelectedUserTable() {
        return selectedUserTable;
    }

    /**
     * Legt den Wert der selectedUserTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setSelectedUserTable(OCITable value) {
        this.selectedUserTable = value;
    }

}
