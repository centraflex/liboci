//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ServiceInvocationDisposition20sp1.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ServiceInvocationDisposition20sp1">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Automatic Hold/Retrieve"/>
 *     <enumeration value="Call Forward Always"/>
 *     <enumeration value="Call Forward Always Secondary"/>
 *     <enumeration value="Call Forward Busy"/>
 *     <enumeration value="Call Forward No Answer"/>
 *     <enumeration value="Call Forward Not Reachable"/>
 *     <enumeration value="Call Forward Selective"/>
 *     <enumeration value="Call Park"/>
 *     <enumeration value="Call Pickup"/>
 *     <enumeration value="Deflection"/>
 *     <enumeration value="Directed Call Pickup"/>
 *     <enumeration value="Distribution from Call Center"/>
 *     <enumeration value="Distribution from Hunt Group"/>
 *     <enumeration value="Distribution from Route Point"/>
 *     <enumeration value="Do Not Disturb"/>
 *     <enumeration value="Fax Deposit"/>
 *     <enumeration value="Group Night Forwarding"/>
 *     <enumeration value="Sequential Ring"/>
 *     <enumeration value="Series Completion"/>
 *     <enumeration value="Simultaneous Ring"/>
 *     <enumeration value="Third Party Deflection"/>
 *     <enumeration value="Third Party Voice Mail Support"/>
 *     <enumeration value="Transfer Consult"/>
 *     <enumeration value="Trunk Group Forward Capacity Exceeded"/>
 *     <enumeration value="Trunk Group Forward Unreachable"/>
 *     <enumeration value="Trunk Group Forward Unconditional"/>
 *     <enumeration value="Voice Mail Transfer"/>
 *     <enumeration value="Voice Messaging"/>
 *     <enumeration value="Find-me/Follow-me"/>
 *     <enumeration value="Personal Assistant"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ServiceInvocationDisposition20sp1")
@XmlEnum
public enum ServiceInvocationDisposition20Sp1 {

    @XmlEnumValue("Automatic Hold/Retrieve")
    AUTOMATIC_HOLD_RETRIEVE("Automatic Hold/Retrieve"),
    @XmlEnumValue("Call Forward Always")
    CALL_FORWARD_ALWAYS("Call Forward Always"),
    @XmlEnumValue("Call Forward Always Secondary")
    CALL_FORWARD_ALWAYS_SECONDARY("Call Forward Always Secondary"),
    @XmlEnumValue("Call Forward Busy")
    CALL_FORWARD_BUSY("Call Forward Busy"),
    @XmlEnumValue("Call Forward No Answer")
    CALL_FORWARD_NO_ANSWER("Call Forward No Answer"),
    @XmlEnumValue("Call Forward Not Reachable")
    CALL_FORWARD_NOT_REACHABLE("Call Forward Not Reachable"),
    @XmlEnumValue("Call Forward Selective")
    CALL_FORWARD_SELECTIVE("Call Forward Selective"),
    @XmlEnumValue("Call Park")
    CALL_PARK("Call Park"),
    @XmlEnumValue("Call Pickup")
    CALL_PICKUP("Call Pickup"),
    @XmlEnumValue("Deflection")
    DEFLECTION("Deflection"),
    @XmlEnumValue("Directed Call Pickup")
    DIRECTED_CALL_PICKUP("Directed Call Pickup"),
    @XmlEnumValue("Distribution from Call Center")
    DISTRIBUTION_FROM_CALL_CENTER("Distribution from Call Center"),
    @XmlEnumValue("Distribution from Hunt Group")
    DISTRIBUTION_FROM_HUNT_GROUP("Distribution from Hunt Group"),
    @XmlEnumValue("Distribution from Route Point")
    DISTRIBUTION_FROM_ROUTE_POINT("Distribution from Route Point"),
    @XmlEnumValue("Do Not Disturb")
    DO_NOT_DISTURB("Do Not Disturb"),
    @XmlEnumValue("Fax Deposit")
    FAX_DEPOSIT("Fax Deposit"),
    @XmlEnumValue("Group Night Forwarding")
    GROUP_NIGHT_FORWARDING("Group Night Forwarding"),
    @XmlEnumValue("Sequential Ring")
    SEQUENTIAL_RING("Sequential Ring"),
    @XmlEnumValue("Series Completion")
    SERIES_COMPLETION("Series Completion"),
    @XmlEnumValue("Simultaneous Ring")
    SIMULTANEOUS_RING("Simultaneous Ring"),
    @XmlEnumValue("Third Party Deflection")
    THIRD_PARTY_DEFLECTION("Third Party Deflection"),
    @XmlEnumValue("Third Party Voice Mail Support")
    THIRD_PARTY_VOICE_MAIL_SUPPORT("Third Party Voice Mail Support"),
    @XmlEnumValue("Transfer Consult")
    TRANSFER_CONSULT("Transfer Consult"),
    @XmlEnumValue("Trunk Group Forward Capacity Exceeded")
    TRUNK_GROUP_FORWARD_CAPACITY_EXCEEDED("Trunk Group Forward Capacity Exceeded"),
    @XmlEnumValue("Trunk Group Forward Unreachable")
    TRUNK_GROUP_FORWARD_UNREACHABLE("Trunk Group Forward Unreachable"),
    @XmlEnumValue("Trunk Group Forward Unconditional")
    TRUNK_GROUP_FORWARD_UNCONDITIONAL("Trunk Group Forward Unconditional"),
    @XmlEnumValue("Voice Mail Transfer")
    VOICE_MAIL_TRANSFER("Voice Mail Transfer"),
    @XmlEnumValue("Voice Messaging")
    VOICE_MESSAGING("Voice Messaging"),
    @XmlEnumValue("Find-me/Follow-me")
    FIND_ME_FOLLOW_ME("Find-me/Follow-me"),
    @XmlEnumValue("Personal Assistant")
    PERSONAL_ASSISTANT("Personal Assistant");
    private final String value;

    ServiceInvocationDisposition20Sp1(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ServiceInvocationDisposition20Sp1 fromValue(String v) {
        for (ServiceInvocationDisposition20Sp1 c: ServiceInvocationDisposition20Sp1 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
