//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupGroupPagingGetInstanceRequest19sp1.
 *         Contains the service profile information.
 *         
 *         The following element is only used in AS data mode and not returned in XS data mode :
 *            networkClassOfService
 *         
 *       
 * 
 * <p>Java-Klasse für GroupGroupPagingGetInstanceResponse19sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupGroupPagingGetInstanceResponse19sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceReadProfile19sp1"/>
 *         <element name="confirmationToneTimeoutSeconds" type="{}GroupPagingConfirmationToneTimeoutSeconds"/>
 *         <element name="deliverOriginatorCLIDInstead" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="originatorCLIDPrefix" type="{}GroupPagingOriginatorCLIDPrefix" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupGroupPagingGetInstanceResponse19sp1", propOrder = {
    "serviceInstanceProfile",
    "confirmationToneTimeoutSeconds",
    "deliverOriginatorCLIDInstead",
    "originatorCLIDPrefix",
    "networkClassOfService"
})
public class GroupGroupPagingGetInstanceResponse19Sp1
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected ServiceInstanceReadProfile19Sp1 serviceInstanceProfile;
    protected int confirmationToneTimeoutSeconds;
    protected boolean deliverOriginatorCLIDInstead;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String originatorCLIDPrefix;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceReadProfile19Sp1 }
     *     
     */
    public ServiceInstanceReadProfile19Sp1 getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceReadProfile19Sp1 }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceReadProfile19Sp1 value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der confirmationToneTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getConfirmationToneTimeoutSeconds() {
        return confirmationToneTimeoutSeconds;
    }

    /**
     * Legt den Wert der confirmationToneTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setConfirmationToneTimeoutSeconds(int value) {
        this.confirmationToneTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der deliverOriginatorCLIDInstead-Eigenschaft ab.
     * 
     */
    public boolean isDeliverOriginatorCLIDInstead() {
        return deliverOriginatorCLIDInstead;
    }

    /**
     * Legt den Wert der deliverOriginatorCLIDInstead-Eigenschaft fest.
     * 
     */
    public void setDeliverOriginatorCLIDInstead(boolean value) {
        this.deliverOriginatorCLIDInstead = value;
    }

    /**
     * Ruft den Wert der originatorCLIDPrefix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatorCLIDPrefix() {
        return originatorCLIDPrefix;
    }

    /**
     * Legt den Wert der originatorCLIDPrefix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatorCLIDPrefix(String value) {
        this.originatorCLIDPrefix = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

}
