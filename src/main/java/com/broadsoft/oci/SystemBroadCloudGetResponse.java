//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the SystemBroadCloudGetRequest.
 *         The response contains the system interface attributes for Messaging Server/BroadCloud.
 *       
 * 
 * <p>Java-Klasse für SystemBroadCloudGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadCloudGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="provisioningUrl" type="{}URL" minOccurs="0"/>
 *         <element name="provisioningUserId" type="{}ProvisioningBroadCloudAuthenticationUserName" minOccurs="0"/>
 *         <element name="enableSynchronization" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadCloudGetResponse", propOrder = {
    "provisioningUrl",
    "provisioningUserId",
    "enableSynchronization"
})
public class SystemBroadCloudGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String provisioningUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String provisioningUserId;
    protected boolean enableSynchronization;

    /**
     * Ruft den Wert der provisioningUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvisioningUrl() {
        return provisioningUrl;
    }

    /**
     * Legt den Wert der provisioningUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvisioningUrl(String value) {
        this.provisioningUrl = value;
    }

    /**
     * Ruft den Wert der provisioningUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvisioningUserId() {
        return provisioningUserId;
    }

    /**
     * Legt den Wert der provisioningUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvisioningUserId(String value) {
        this.provisioningUserId = value;
    }

    /**
     * Ruft den Wert der enableSynchronization-Eigenschaft ab.
     * 
     */
    public boolean isEnableSynchronization() {
        return enableSynchronization;
    }

    /**
     * Legt den Wert der enableSynchronization-Eigenschaft fest.
     * 
     */
    public void setEnableSynchronization(boolean value) {
        this.enableSynchronization = value;
    }

}
