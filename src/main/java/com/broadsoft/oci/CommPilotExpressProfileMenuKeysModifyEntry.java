//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal commPilot express profile menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für CommPilotExpressProfileMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CommPilotExpressProfileMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="activateAvailableInOfficeProfile" type="{}DigitAny" minOccurs="0"/>
 *         <element name="activateAvailableOutOfOfficeProfile" type="{}DigitAny" minOccurs="0"/>
 *         <element name="activateBusyProfile" type="{}DigitAny" minOccurs="0"/>
 *         <element name="activateUnavailableProfile" type="{}DigitAny" minOccurs="0"/>
 *         <element name="noProfile" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommPilotExpressProfileMenuKeysModifyEntry", propOrder = {
    "activateAvailableInOfficeProfile",
    "activateAvailableOutOfOfficeProfile",
    "activateBusyProfile",
    "activateUnavailableProfile",
    "noProfile",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class CommPilotExpressProfileMenuKeysModifyEntry {

    @XmlElementRef(name = "activateAvailableInOfficeProfile", type = JAXBElement.class, required = false)
    protected JAXBElement<String> activateAvailableInOfficeProfile;
    @XmlElementRef(name = "activateAvailableOutOfOfficeProfile", type = JAXBElement.class, required = false)
    protected JAXBElement<String> activateAvailableOutOfOfficeProfile;
    @XmlElementRef(name = "activateBusyProfile", type = JAXBElement.class, required = false)
    protected JAXBElement<String> activateBusyProfile;
    @XmlElementRef(name = "activateUnavailableProfile", type = JAXBElement.class, required = false)
    protected JAXBElement<String> activateUnavailableProfile;
    @XmlElementRef(name = "noProfile", type = JAXBElement.class, required = false)
    protected JAXBElement<String> noProfile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repeatMenu;

    /**
     * Ruft den Wert der activateAvailableInOfficeProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActivateAvailableInOfficeProfile() {
        return activateAvailableInOfficeProfile;
    }

    /**
     * Legt den Wert der activateAvailableInOfficeProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActivateAvailableInOfficeProfile(JAXBElement<String> value) {
        this.activateAvailableInOfficeProfile = value;
    }

    /**
     * Ruft den Wert der activateAvailableOutOfOfficeProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActivateAvailableOutOfOfficeProfile() {
        return activateAvailableOutOfOfficeProfile;
    }

    /**
     * Legt den Wert der activateAvailableOutOfOfficeProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActivateAvailableOutOfOfficeProfile(JAXBElement<String> value) {
        this.activateAvailableOutOfOfficeProfile = value;
    }

    /**
     * Ruft den Wert der activateBusyProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActivateBusyProfile() {
        return activateBusyProfile;
    }

    /**
     * Legt den Wert der activateBusyProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActivateBusyProfile(JAXBElement<String> value) {
        this.activateBusyProfile = value;
    }

    /**
     * Ruft den Wert der activateUnavailableProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActivateUnavailableProfile() {
        return activateUnavailableProfile;
    }

    /**
     * Legt den Wert der activateUnavailableProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActivateUnavailableProfile(JAXBElement<String> value) {
        this.activateUnavailableProfile = value;
    }

    /**
     * Ruft den Wert der noProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNoProfile() {
        return noProfile;
    }

    /**
     * Legt den Wert der noProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNoProfile(JAXBElement<String> value) {
        this.noProfile = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepeatMenu(JAXBElement<String> value) {
        this.repeatMenu = value;
    }

}
