//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PushToTalkOutgoingConnectionSelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="PushToTalkOutgoingConnectionSelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="One Way"/>
 *     <enumeration value="Two Way"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "PushToTalkOutgoingConnectionSelection")
@XmlEnum
public enum PushToTalkOutgoingConnectionSelection {

    @XmlEnumValue("One Way")
    ONE_WAY("One Way"),
    @XmlEnumValue("Two Way")
    TWO_WAY("Two Way");
    private final String value;

    PushToTalkOutgoingConnectionSelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PushToTalkOutgoingConnectionSelection fromValue(String v) {
        for (PushToTalkOutgoingConnectionSelection c: PushToTalkOutgoingConnectionSelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
