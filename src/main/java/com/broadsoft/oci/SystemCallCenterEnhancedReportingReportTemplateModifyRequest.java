//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a system level call center report template.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCallCenterEnhancedReportingReportTemplateModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallCenterEnhancedReportingReportTemplateModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="name" type="{}CallCenterReportTemplateName"/>
 *         <element name="newName" type="{}CallCenterReportTemplateName" minOccurs="0"/>
 *         <element name="description" type="{}CallCenterReportTemplateDescription" minOccurs="0"/>
 *         <element name="xsltTemplate" type="{}LabeledFileResource" minOccurs="0"/>
 *         <element name="scope" type="{}CallCenterReportTemplateAccessOption" minOccurs="0"/>
 *         <element name="isEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="isRealtimeReport" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="filterNumber" type="{}CallCenterReportDataTemplateFilterNumber" minOccurs="0"/>
 *         <element name="filterValue" type="{}CallCenterReportDataTemplateQueryFilterValueReplacementList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallCenterEnhancedReportingReportTemplateModifyRequest", propOrder = {
    "name",
    "newName",
    "description",
    "xsltTemplate",
    "scope",
    "isEnabled",
    "isRealtimeReport",
    "filterNumber",
    "filterValue"
})
public class SystemCallCenterEnhancedReportingReportTemplateModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newName;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    protected LabeledFileResource xsltTemplate;
    @XmlSchemaType(name = "token")
    protected CallCenterReportTemplateAccessOption scope;
    protected Boolean isEnabled;
    protected Boolean isRealtimeReport;
    @XmlElementRef(name = "filterNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> filterNumber;
    @XmlElementRef(name = "filterValue", type = JAXBElement.class, required = false)
    protected JAXBElement<CallCenterReportDataTemplateQueryFilterValueReplacementList> filterValue;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der newName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewName() {
        return newName;
    }

    /**
     * Legt den Wert der newName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewName(String value) {
        this.newName = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der xsltTemplate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledFileResource }
     *     
     */
    public LabeledFileResource getXsltTemplate() {
        return xsltTemplate;
    }

    /**
     * Legt den Wert der xsltTemplate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledFileResource }
     *     
     */
    public void setXsltTemplate(LabeledFileResource value) {
        this.xsltTemplate = value;
    }

    /**
     * Ruft den Wert der scope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportTemplateAccessOption }
     *     
     */
    public CallCenterReportTemplateAccessOption getScope() {
        return scope;
    }

    /**
     * Legt den Wert der scope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportTemplateAccessOption }
     *     
     */
    public void setScope(CallCenterReportTemplateAccessOption value) {
        this.scope = value;
    }

    /**
     * Ruft den Wert der isEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsEnabled() {
        return isEnabled;
    }

    /**
     * Legt den Wert der isEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsEnabled(Boolean value) {
        this.isEnabled = value;
    }

    /**
     * Ruft den Wert der isRealtimeReport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRealtimeReport() {
        return isRealtimeReport;
    }

    /**
     * Legt den Wert der isRealtimeReport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRealtimeReport(Boolean value) {
        this.isRealtimeReport = value;
    }

    /**
     * Ruft den Wert der filterNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getFilterNumber() {
        return filterNumber;
    }

    /**
     * Legt den Wert der filterNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setFilterNumber(JAXBElement<Integer> value) {
        this.filterNumber = value;
    }

    /**
     * Ruft den Wert der filterValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CallCenterReportDataTemplateQueryFilterValueReplacementList }{@code >}
     *     
     */
    public JAXBElement<CallCenterReportDataTemplateQueryFilterValueReplacementList> getFilterValue() {
        return filterValue;
    }

    /**
     * Legt den Wert der filterValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CallCenterReportDataTemplateQueryFilterValueReplacementList }{@code >}
     *     
     */
    public void setFilterValue(JAXBElement<CallCenterReportDataTemplateQueryFilterValueReplacementList> value) {
        this.filterValue = value;
    }

}
