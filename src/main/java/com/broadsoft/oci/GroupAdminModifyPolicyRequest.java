//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the group administrator's policy settings.
 *         The response is either SuccessResponse or ErrorResponse.
 *         The following elements are only used in AS data mode:
 *             dialableCallerIDAccess
 *             verifyTranslationAndRoutingAccess
 *             communicationBarringUserProfileAccess (only applicable to groups in an Enterprise)            
 *       
 * 
 * <p>Java-Klasse für GroupAdminModifyPolicyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAdminModifyPolicyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="profileAccess" type="{}GroupAdminProfileAccess" minOccurs="0"/>
 *         <element name="userAccess" type="{}GroupAdminUserAccess" minOccurs="0"/>
 *         <element name="adminAccess" type="{}GroupAdminAdminAccess" minOccurs="0"/>
 *         <element name="departmentAccess" type="{}GroupAdminDepartmentAccess" minOccurs="0"/>
 *         <element name="accessDeviceAccess" type="{}GroupAdminAccessDeviceAccess" minOccurs="0"/>
 *         <element name="enhancedServiceInstanceAccess" type="{}GroupAdminEnhancedServiceInstanceAccess" minOccurs="0"/>
 *         <element name="featureAccessCodeAccess" type="{}GroupAdminFeatureAccessCodeAccess" minOccurs="0"/>
 *         <element name="phoneNumberExtensionAccess" type="{}GroupAdminPhoneNumberExtensionAccess" minOccurs="0"/>
 *         <element name="callingLineIdNumberAccess" type="{}GroupAdminCallingLineIdNumberAccess" minOccurs="0"/>
 *         <element name="serviceAccess" type="{}GroupAdminServiceAccess" minOccurs="0"/>
 *         <element name="trunkGroupAccess" type="{}GroupAdminTrunkGroupAccess" minOccurs="0"/>
 *         <element name="sessionAdmissionControlAccess" type="{}GroupAdminSessionAdmissionControlAccess" minOccurs="0"/>
 *         <element name="officeZoneAccess" type="{}GroupAdminOfficeZoneAccess" minOccurs="0"/>
 *         <element name="dialableCallerIDAccess" type="{}GroupAdminDialableCallerIDAccess" minOccurs="0"/>
 *         <element name="numberActivationAccess" type="{}GroupAdminNumberActivationAccess" minOccurs="0"/>
 *         <element name="verifyTranslationAndRoutingAccess" type="{}GroupAdminVerifyTranslationAndRoutingAccess" minOccurs="0"/>
 *         <element name="communicationBarringUserProfileAccess" type="{}GroupAdminCommunicationBarringUserProfileAccess" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAdminModifyPolicyRequest", propOrder = {
    "userId",
    "profileAccess",
    "userAccess",
    "adminAccess",
    "departmentAccess",
    "accessDeviceAccess",
    "enhancedServiceInstanceAccess",
    "featureAccessCodeAccess",
    "phoneNumberExtensionAccess",
    "callingLineIdNumberAccess",
    "serviceAccess",
    "trunkGroupAccess",
    "sessionAdmissionControlAccess",
    "officeZoneAccess",
    "dialableCallerIDAccess",
    "numberActivationAccess",
    "verifyTranslationAndRoutingAccess",
    "communicationBarringUserProfileAccess"
})
public class GroupAdminModifyPolicyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlSchemaType(name = "token")
    protected GroupAdminProfileAccess profileAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminUserAccess userAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminAdminAccess adminAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminDepartmentAccess departmentAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminAccessDeviceAccess accessDeviceAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminEnhancedServiceInstanceAccess enhancedServiceInstanceAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminFeatureAccessCodeAccess featureAccessCodeAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminPhoneNumberExtensionAccess phoneNumberExtensionAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminCallingLineIdNumberAccess callingLineIdNumberAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminServiceAccess serviceAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminTrunkGroupAccess trunkGroupAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminSessionAdmissionControlAccess sessionAdmissionControlAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminOfficeZoneAccess officeZoneAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminDialableCallerIDAccess dialableCallerIDAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminNumberActivationAccess numberActivationAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminVerifyTranslationAndRoutingAccess verifyTranslationAndRoutingAccess;
    @XmlSchemaType(name = "token")
    protected GroupAdminCommunicationBarringUserProfileAccess communicationBarringUserProfileAccess;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der profileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminProfileAccess }
     *     
     */
    public GroupAdminProfileAccess getProfileAccess() {
        return profileAccess;
    }

    /**
     * Legt den Wert der profileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminProfileAccess }
     *     
     */
    public void setProfileAccess(GroupAdminProfileAccess value) {
        this.profileAccess = value;
    }

    /**
     * Ruft den Wert der userAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminUserAccess }
     *     
     */
    public GroupAdminUserAccess getUserAccess() {
        return userAccess;
    }

    /**
     * Legt den Wert der userAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminUserAccess }
     *     
     */
    public void setUserAccess(GroupAdminUserAccess value) {
        this.userAccess = value;
    }

    /**
     * Ruft den Wert der adminAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminAdminAccess }
     *     
     */
    public GroupAdminAdminAccess getAdminAccess() {
        return adminAccess;
    }

    /**
     * Legt den Wert der adminAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminAdminAccess }
     *     
     */
    public void setAdminAccess(GroupAdminAdminAccess value) {
        this.adminAccess = value;
    }

    /**
     * Ruft den Wert der departmentAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminDepartmentAccess }
     *     
     */
    public GroupAdminDepartmentAccess getDepartmentAccess() {
        return departmentAccess;
    }

    /**
     * Legt den Wert der departmentAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminDepartmentAccess }
     *     
     */
    public void setDepartmentAccess(GroupAdminDepartmentAccess value) {
        this.departmentAccess = value;
    }

    /**
     * Ruft den Wert der accessDeviceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminAccessDeviceAccess }
     *     
     */
    public GroupAdminAccessDeviceAccess getAccessDeviceAccess() {
        return accessDeviceAccess;
    }

    /**
     * Legt den Wert der accessDeviceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminAccessDeviceAccess }
     *     
     */
    public void setAccessDeviceAccess(GroupAdminAccessDeviceAccess value) {
        this.accessDeviceAccess = value;
    }

    /**
     * Ruft den Wert der enhancedServiceInstanceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminEnhancedServiceInstanceAccess }
     *     
     */
    public GroupAdminEnhancedServiceInstanceAccess getEnhancedServiceInstanceAccess() {
        return enhancedServiceInstanceAccess;
    }

    /**
     * Legt den Wert der enhancedServiceInstanceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminEnhancedServiceInstanceAccess }
     *     
     */
    public void setEnhancedServiceInstanceAccess(GroupAdminEnhancedServiceInstanceAccess value) {
        this.enhancedServiceInstanceAccess = value;
    }

    /**
     * Ruft den Wert der featureAccessCodeAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminFeatureAccessCodeAccess }
     *     
     */
    public GroupAdminFeatureAccessCodeAccess getFeatureAccessCodeAccess() {
        return featureAccessCodeAccess;
    }

    /**
     * Legt den Wert der featureAccessCodeAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminFeatureAccessCodeAccess }
     *     
     */
    public void setFeatureAccessCodeAccess(GroupAdminFeatureAccessCodeAccess value) {
        this.featureAccessCodeAccess = value;
    }

    /**
     * Ruft den Wert der phoneNumberExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminPhoneNumberExtensionAccess }
     *     
     */
    public GroupAdminPhoneNumberExtensionAccess getPhoneNumberExtensionAccess() {
        return phoneNumberExtensionAccess;
    }

    /**
     * Legt den Wert der phoneNumberExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminPhoneNumberExtensionAccess }
     *     
     */
    public void setPhoneNumberExtensionAccess(GroupAdminPhoneNumberExtensionAccess value) {
        this.phoneNumberExtensionAccess = value;
    }

    /**
     * Ruft den Wert der callingLineIdNumberAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminCallingLineIdNumberAccess }
     *     
     */
    public GroupAdminCallingLineIdNumberAccess getCallingLineIdNumberAccess() {
        return callingLineIdNumberAccess;
    }

    /**
     * Legt den Wert der callingLineIdNumberAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminCallingLineIdNumberAccess }
     *     
     */
    public void setCallingLineIdNumberAccess(GroupAdminCallingLineIdNumberAccess value) {
        this.callingLineIdNumberAccess = value;
    }

    /**
     * Ruft den Wert der serviceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminServiceAccess }
     *     
     */
    public GroupAdminServiceAccess getServiceAccess() {
        return serviceAccess;
    }

    /**
     * Legt den Wert der serviceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminServiceAccess }
     *     
     */
    public void setServiceAccess(GroupAdminServiceAccess value) {
        this.serviceAccess = value;
    }

    /**
     * Ruft den Wert der trunkGroupAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminTrunkGroupAccess }
     *     
     */
    public GroupAdminTrunkGroupAccess getTrunkGroupAccess() {
        return trunkGroupAccess;
    }

    /**
     * Legt den Wert der trunkGroupAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminTrunkGroupAccess }
     *     
     */
    public void setTrunkGroupAccess(GroupAdminTrunkGroupAccess value) {
        this.trunkGroupAccess = value;
    }

    /**
     * Ruft den Wert der sessionAdmissionControlAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminSessionAdmissionControlAccess }
     *     
     */
    public GroupAdminSessionAdmissionControlAccess getSessionAdmissionControlAccess() {
        return sessionAdmissionControlAccess;
    }

    /**
     * Legt den Wert der sessionAdmissionControlAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminSessionAdmissionControlAccess }
     *     
     */
    public void setSessionAdmissionControlAccess(GroupAdminSessionAdmissionControlAccess value) {
        this.sessionAdmissionControlAccess = value;
    }

    /**
     * Ruft den Wert der officeZoneAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminOfficeZoneAccess }
     *     
     */
    public GroupAdminOfficeZoneAccess getOfficeZoneAccess() {
        return officeZoneAccess;
    }

    /**
     * Legt den Wert der officeZoneAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminOfficeZoneAccess }
     *     
     */
    public void setOfficeZoneAccess(GroupAdminOfficeZoneAccess value) {
        this.officeZoneAccess = value;
    }

    /**
     * Ruft den Wert der dialableCallerIDAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminDialableCallerIDAccess }
     *     
     */
    public GroupAdminDialableCallerIDAccess getDialableCallerIDAccess() {
        return dialableCallerIDAccess;
    }

    /**
     * Legt den Wert der dialableCallerIDAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminDialableCallerIDAccess }
     *     
     */
    public void setDialableCallerIDAccess(GroupAdminDialableCallerIDAccess value) {
        this.dialableCallerIDAccess = value;
    }

    /**
     * Ruft den Wert der numberActivationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminNumberActivationAccess }
     *     
     */
    public GroupAdminNumberActivationAccess getNumberActivationAccess() {
        return numberActivationAccess;
    }

    /**
     * Legt den Wert der numberActivationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminNumberActivationAccess }
     *     
     */
    public void setNumberActivationAccess(GroupAdminNumberActivationAccess value) {
        this.numberActivationAccess = value;
    }

    /**
     * Ruft den Wert der verifyTranslationAndRoutingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminVerifyTranslationAndRoutingAccess }
     *     
     */
    public GroupAdminVerifyTranslationAndRoutingAccess getVerifyTranslationAndRoutingAccess() {
        return verifyTranslationAndRoutingAccess;
    }

    /**
     * Legt den Wert der verifyTranslationAndRoutingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminVerifyTranslationAndRoutingAccess }
     *     
     */
    public void setVerifyTranslationAndRoutingAccess(GroupAdminVerifyTranslationAndRoutingAccess value) {
        this.verifyTranslationAndRoutingAccess = value;
    }

    /**
     * Ruft den Wert der communicationBarringUserProfileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminCommunicationBarringUserProfileAccess }
     *     
     */
    public GroupAdminCommunicationBarringUserProfileAccess getCommunicationBarringUserProfileAccess() {
        return communicationBarringUserProfileAccess;
    }

    /**
     * Legt den Wert der communicationBarringUserProfileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminCommunicationBarringUserProfileAccess }
     *     
     */
    public void setCommunicationBarringUserProfileAccess(GroupAdminCommunicationBarringUserProfileAccess value) {
        this.communicationBarringUserProfileAccess = value;
    }

}
