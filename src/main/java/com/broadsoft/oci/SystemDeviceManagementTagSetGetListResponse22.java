//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemDeviceManagementTagSetGetListRequest22.
 *         The response includes a table of tag set names defined in the system.
 *         Column headings are: "Tag Set Name", "Reseller Id".
 * 
 *         The following columns are only returned in AS data mode:       
 *           "Reseller Id"
 *         The system default tag set name is not part of this response.
 *       
 * 
 * <p>Java-Klasse für SystemDeviceManagementTagSetGetListResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceManagementTagSetGetListResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="tagSetTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceManagementTagSetGetListResponse22", propOrder = {
    "tagSetTable"
})
public class SystemDeviceManagementTagSetGetListResponse22
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable tagSetTable;

    /**
     * Ruft den Wert der tagSetTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getTagSetTable() {
        return tagSetTable;
    }

    /**
     * Legt den Wert der tagSetTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setTagSetTable(OCITable value) {
        this.tagSetTable = value;
    }

}
