//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemSIPDiversionReasonGetRequest.
 *         Contains a table containing a list of diversion reasons and associated cause values.
 *         The column headings are: "Diversion Reaon", "Cause Value".
 *       
 * 
 * <p>Java-Klasse für SystemSIPDiversionReasonGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPDiversionReasonGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="diversionReasonTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPDiversionReasonGetResponse", propOrder = {
    "diversionReasonTable"
})
public class SystemSIPDiversionReasonGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable diversionReasonTable;

    /**
     * Ruft den Wert der diversionReasonTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDiversionReasonTable() {
        return diversionReasonTable;
    }

    /**
     * Legt den Wert der diversionReasonTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDiversionReasonTable(OCITable value) {
        this.diversionReasonTable = value;
    }

}
