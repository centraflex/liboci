//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupServiceGetAuthorizationRequest.
 *         If the feature is not authorized, then "authorized" is false and the remaining elements are not returned.
 *         If the service pack is not available for use or is not authorized, then "authorized" is false and the remaining elements are not returned.        
 *         "authorizedQuantity" can be unlimited or a quantity. In the case of a service pack, "authorizedQuantity" is the service pack's quantity.
 *         "authorizable" is applicable for user services and group services; it is not returned for service packs.
 *       
 * 
 * <p>Java-Klasse für GroupServiceGetAuthorizationResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupServiceGetAuthorizationResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="authorized" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="authorizedQuantity" type="{}UnboundedNonNegativeInt" minOccurs="0"/>
 *         <element name="usedQuantity" type="{}UnboundedNonNegativeInt" minOccurs="0"/>
 *         <element name="authorizable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupServiceGetAuthorizationResponse", propOrder = {
    "authorized",
    "authorizedQuantity",
    "usedQuantity",
    "authorizable"
})
public class GroupServiceGetAuthorizationResponse
    extends OCIDataResponse
{

    protected boolean authorized;
    protected UnboundedNonNegativeInt authorizedQuantity;
    protected UnboundedNonNegativeInt usedQuantity;
    protected Boolean authorizable;

    /**
     * Ruft den Wert der authorized-Eigenschaft ab.
     * 
     */
    public boolean isAuthorized() {
        return authorized;
    }

    /**
     * Legt den Wert der authorized-Eigenschaft fest.
     * 
     */
    public void setAuthorized(boolean value) {
        this.authorized = value;
    }

    /**
     * Ruft den Wert der authorizedQuantity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public UnboundedNonNegativeInt getAuthorizedQuantity() {
        return authorizedQuantity;
    }

    /**
     * Legt den Wert der authorizedQuantity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public void setAuthorizedQuantity(UnboundedNonNegativeInt value) {
        this.authorizedQuantity = value;
    }

    /**
     * Ruft den Wert der usedQuantity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public UnboundedNonNegativeInt getUsedQuantity() {
        return usedQuantity;
    }

    /**
     * Legt den Wert der usedQuantity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public void setUsedQuantity(UnboundedNonNegativeInt value) {
        this.usedQuantity = value;
    }

    /**
     * Ruft den Wert der authorizable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAuthorizable() {
        return authorizable;
    }

    /**
     * Legt den Wert der authorizable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAuthorizable(Boolean value) {
        this.authorizable = value;
    }

}
