//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ServiceProviderPasswordRulesApplyTo.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ServiceProviderPasswordRulesApplyTo">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Administrator"/>
 *     <enumeration value="Administrator and User"/>
 *     <enumeration value="Group Administrator and User External Authentication"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ServiceProviderPasswordRulesApplyTo")
@XmlEnum
public enum ServiceProviderPasswordRulesApplyTo {

    @XmlEnumValue("Administrator")
    ADMINISTRATOR("Administrator"),
    @XmlEnumValue("Administrator and User")
    ADMINISTRATOR_AND_USER("Administrator and User"),
    @XmlEnumValue("Group Administrator and User External Authentication")
    GROUP_ADMINISTRATOR_AND_USER_EXTERNAL_AUTHENTICATION("Group Administrator and User External Authentication");
    private final String value;

    ServiceProviderPasswordRulesApplyTo(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ServiceProviderPasswordRulesApplyTo fromValue(String v) {
        for (ServiceProviderPasswordRulesApplyTo c: ServiceProviderPasswordRulesApplyTo.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
