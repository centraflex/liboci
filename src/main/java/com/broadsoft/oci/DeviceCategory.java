//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeviceCategory.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DeviceCategory">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Generic"/>
 *     <enumeration value="Hosted"/>
 *     <enumeration value="Client Applications"/>
 *     <enumeration value="SIP Trunking"/>
 *     <enumeration value="Local Gateway"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DeviceCategory")
@XmlEnum
public enum DeviceCategory {

    @XmlEnumValue("Generic")
    GENERIC("Generic"),
    @XmlEnumValue("Hosted")
    HOSTED("Hosted"),
    @XmlEnumValue("Client Applications")
    CLIENT_APPLICATIONS("Client Applications"),
    @XmlEnumValue("SIP Trunking")
    SIP_TRUNKING("SIP Trunking"),
    @XmlEnumValue("Local Gateway")
    LOCAL_GATEWAY("Local Gateway");
    private final String value;

    DeviceCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeviceCategory fromValue(String v) {
        for (DeviceCategory c: DeviceCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
