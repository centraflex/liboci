//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to a SystemTreatmentMappingInternalReleaseCauseGetListRequest. Contains a table with one row per mapping.
 *         The table columns are: "Internal Release Cause", "Treatment Id".
 *       
 * 
 * <p>Java-Klasse für SystemTreatmentMappingInternalReleaseCauseGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemTreatmentMappingInternalReleaseCauseGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="treatmentMappingTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemTreatmentMappingInternalReleaseCauseGetListResponse", propOrder = {
    "treatmentMappingTable"
})
public class SystemTreatmentMappingInternalReleaseCauseGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable treatmentMappingTable;

    /**
     * Ruft den Wert der treatmentMappingTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getTreatmentMappingTable() {
        return treatmentMappingTable;
    }

    /**
     * Legt den Wert der treatmentMappingTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setTreatmentMappingTable(OCITable value) {
        this.treatmentMappingTable = value;
    }

}
