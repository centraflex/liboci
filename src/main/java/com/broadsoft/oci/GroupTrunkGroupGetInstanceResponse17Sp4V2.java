//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupTrunkGroupGetInstanceRequest17sp4V2.
 *         Returns the profile information for the Trunk Group.
 *         Contains a hosted user table with column headings: "User Id", "Last Name",
 *         "First Name", "Hiragana Last Name", "Hiragana First Name".
 *     
 *         Replaced by: GroupTrunkGroupGetInstanceResponse19V2
 *       
 * 
 * <p>Java-Klasse für GroupTrunkGroupGetInstanceResponse17sp4V2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupTrunkGroupGetInstanceResponse17sp4V2">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="pilotUserId" type="{}UserId" minOccurs="0"/>
 *         <element name="department" type="{}DepartmentKey" minOccurs="0"/>
 *         <element name="accessDevice" type="{}AccessDevice" minOccurs="0"/>
 *         <element name="maxActiveCalls" type="{}MaxActiveCalls"/>
 *         <element name="maxIncomingCalls" type="{}MaxIncomingCalls" minOccurs="0"/>
 *         <element name="maxOutgoingCalls" type="{}MaxOutgoingCalls" minOccurs="0"/>
 *         <element name="enableBursting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="burstingMaxActiveCalls" type="{}BurstingMaxActiveCalls" minOccurs="0"/>
 *         <element name="burstingMaxIncomingCalls" type="{}BurstingMaxIncomingCalls" minOccurs="0"/>
 *         <element name="burstingMaxOutgoingCalls" type="{}BurstingMaxOutgoingCalls" minOccurs="0"/>
 *         <element name="capacityExceededAction" type="{}TrunkGroupCapacityExceededAction" minOccurs="0"/>
 *         <element name="capacityExceededForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="capacityExceededRerouteTrunkGroupKey" type="{}TrunkGroupKey" minOccurs="0"/>
 *         <element name="capacityExceededTrapInitialCalls" type="{}TrapInitialThreshold"/>
 *         <element name="capacityExceededTrapOffsetCalls" type="{}TrapOffsetThreshold"/>
 *         <element name="unreachableDestinationAction" type="{}TrunkGroupUnreachableDestinationAction" minOccurs="0"/>
 *         <element name="unreachableDestinationForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="unreachableDestinationRerouteTrunkGroupKey" type="{}TrunkGroupKey" minOccurs="0"/>
 *         <element name="invitationTimeout" type="{}TrunkGroupInvitationTimeoutSeconds"/>
 *         <element name="requireAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sipAuthenticationUserName" type="{}SIPAuthenticationUserName" minOccurs="0"/>
 *         <element name="hostedUserTable" type="{C}OCITable" minOccurs="0"/>
 *         <element name="trunkGroupIdentity" type="{}SIPURI" minOccurs="0"/>
 *         <element name="otgDtgIdentity" type="{}OtgDtgIdentity" minOccurs="0"/>
 *         <element name="allowTerminationToTrunkGroupIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowTerminationToDtgIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeTrunkGroupIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeDtgIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeTrunkGroupIdentityForNetworkCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeOtgIdentityForNetworkCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableNetworkAddressIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowUnscreenedCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowUnscreenedEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="pilotUserCallingLineIdentityPolicy" type="{}TrunkGroupPilotUserCallingLineIdentityUsagePolicy"/>
 *         <element name="pilotUserChargeNumberPolicy" type="{}TrunkGroupPilotUserChargeNumberUsagePolicy"/>
 *         <element name="callForwardingAlwaysAction" type="{}TrunkGroupCallForwardingAlwaysAction" minOccurs="0"/>
 *         <element name="callForwardingAlwaysForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="callForwardingAlwaysRerouteTrunkGroupKey" type="{}TrunkGroupKey" minOccurs="0"/>
 *         <element name="peeringDomain" type="{}DomainName" minOccurs="0"/>
 *         <element name="routeToPeeringDomain" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="prefixEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="prefix" type="{}TrunkGroupPrefix" minOccurs="0"/>
 *         <element name="statefulReroutingEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sendContinuousOptionsMessage" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="continuousOptionsSendingIntervalSeconds" type="{}TrunkGroupContinuousOptionsSendingIntervalSeconds"/>
 *         <element name="failureOptionsSendingIntervalSeconds" type="{}TrunkGroupFailureOptionsSendingIntervalSeconds"/>
 *         <element name="failureThresholdCounter" type="{}TrunkGroupThresholdCounter"/>
 *         <element name="successThresholdCounter" type="{}TrunkGroupThresholdCounter"/>
 *         <element name="inviteFailureThresholdCounter" type="{}TrunkGroupThresholdCounter"/>
 *         <element name="inviteFailureThresholdWindowSeconds" type="{}TrunkGroupFailureThresholdWindowSeconds"/>
 *         <element name="trunkGroupState" type="{}TrunkGroupState"/>
 *         <element name="pilotUserCallingLineAssertedIdentityPolicy" type="{}TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy"/>
 *         <element name="useSystemCallingLineAssertedIdentityPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupTrunkGroupGetInstanceResponse17sp4V2", propOrder = {
    "pilotUserId",
    "department",
    "accessDevice",
    "maxActiveCalls",
    "maxIncomingCalls",
    "maxOutgoingCalls",
    "enableBursting",
    "burstingMaxActiveCalls",
    "burstingMaxIncomingCalls",
    "burstingMaxOutgoingCalls",
    "capacityExceededAction",
    "capacityExceededForwardAddress",
    "capacityExceededRerouteTrunkGroupKey",
    "capacityExceededTrapInitialCalls",
    "capacityExceededTrapOffsetCalls",
    "unreachableDestinationAction",
    "unreachableDestinationForwardAddress",
    "unreachableDestinationRerouteTrunkGroupKey",
    "invitationTimeout",
    "requireAuthentication",
    "sipAuthenticationUserName",
    "hostedUserTable",
    "trunkGroupIdentity",
    "otgDtgIdentity",
    "allowTerminationToTrunkGroupIdentity",
    "allowTerminationToDtgIdentity",
    "includeTrunkGroupIdentity",
    "includeDtgIdentity",
    "includeTrunkGroupIdentityForNetworkCalls",
    "includeOtgIdentityForNetworkCalls",
    "enableNetworkAddressIdentity",
    "allowUnscreenedCalls",
    "allowUnscreenedEmergencyCalls",
    "pilotUserCallingLineIdentityPolicy",
    "pilotUserChargeNumberPolicy",
    "callForwardingAlwaysAction",
    "callForwardingAlwaysForwardAddress",
    "callForwardingAlwaysRerouteTrunkGroupKey",
    "peeringDomain",
    "routeToPeeringDomain",
    "prefixEnabled",
    "prefix",
    "statefulReroutingEnabled",
    "sendContinuousOptionsMessage",
    "continuousOptionsSendingIntervalSeconds",
    "failureOptionsSendingIntervalSeconds",
    "failureThresholdCounter",
    "successThresholdCounter",
    "inviteFailureThresholdCounter",
    "inviteFailureThresholdWindowSeconds",
    "trunkGroupState",
    "pilotUserCallingLineAssertedIdentityPolicy",
    "useSystemCallingLineAssertedIdentityPolicy"
})
public class GroupTrunkGroupGetInstanceResponse17Sp4V2
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String pilotUserId;
    protected DepartmentKey department;
    protected AccessDevice accessDevice;
    protected int maxActiveCalls;
    protected Integer maxIncomingCalls;
    protected Integer maxOutgoingCalls;
    protected boolean enableBursting;
    protected Integer burstingMaxActiveCalls;
    protected Integer burstingMaxIncomingCalls;
    protected Integer burstingMaxOutgoingCalls;
    @XmlSchemaType(name = "token")
    protected TrunkGroupCapacityExceededAction capacityExceededAction;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String capacityExceededForwardAddress;
    protected TrunkGroupKey capacityExceededRerouteTrunkGroupKey;
    protected int capacityExceededTrapInitialCalls;
    protected int capacityExceededTrapOffsetCalls;
    @XmlSchemaType(name = "token")
    protected TrunkGroupUnreachableDestinationAction unreachableDestinationAction;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String unreachableDestinationForwardAddress;
    protected TrunkGroupKey unreachableDestinationRerouteTrunkGroupKey;
    protected int invitationTimeout;
    protected boolean requireAuthentication;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sipAuthenticationUserName;
    protected OCITable hostedUserTable;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String trunkGroupIdentity;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String otgDtgIdentity;
    protected boolean allowTerminationToTrunkGroupIdentity;
    protected boolean allowTerminationToDtgIdentity;
    protected boolean includeTrunkGroupIdentity;
    protected boolean includeDtgIdentity;
    protected boolean includeTrunkGroupIdentityForNetworkCalls;
    protected boolean includeOtgIdentityForNetworkCalls;
    protected boolean enableNetworkAddressIdentity;
    protected boolean allowUnscreenedCalls;
    protected boolean allowUnscreenedEmergencyCalls;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupPilotUserCallingLineIdentityUsagePolicy pilotUserCallingLineIdentityPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupPilotUserChargeNumberUsagePolicy pilotUserChargeNumberPolicy;
    @XmlSchemaType(name = "token")
    protected TrunkGroupCallForwardingAlwaysAction callForwardingAlwaysAction;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callForwardingAlwaysForwardAddress;
    protected TrunkGroupKey callForwardingAlwaysRerouteTrunkGroupKey;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String peeringDomain;
    protected boolean routeToPeeringDomain;
    protected boolean prefixEnabled;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String prefix;
    protected boolean statefulReroutingEnabled;
    protected boolean sendContinuousOptionsMessage;
    protected int continuousOptionsSendingIntervalSeconds;
    protected int failureOptionsSendingIntervalSeconds;
    protected int failureThresholdCounter;
    protected int successThresholdCounter;
    protected int inviteFailureThresholdCounter;
    protected int inviteFailureThresholdWindowSeconds;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupState trunkGroupState;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy pilotUserCallingLineAssertedIdentityPolicy;
    protected boolean useSystemCallingLineAssertedIdentityPolicy;

    /**
     * Ruft den Wert der pilotUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPilotUserId() {
        return pilotUserId;
    }

    /**
     * Legt den Wert der pilotUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPilotUserId(String value) {
        this.pilotUserId = value;
    }

    /**
     * Ruft den Wert der department-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DepartmentKey }
     *     
     */
    public DepartmentKey getDepartment() {
        return department;
    }

    /**
     * Legt den Wert der department-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DepartmentKey }
     *     
     */
    public void setDepartment(DepartmentKey value) {
        this.department = value;
    }

    /**
     * Ruft den Wert der accessDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDevice }
     *     
     */
    public AccessDevice getAccessDevice() {
        return accessDevice;
    }

    /**
     * Legt den Wert der accessDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDevice }
     *     
     */
    public void setAccessDevice(AccessDevice value) {
        this.accessDevice = value;
    }

    /**
     * Ruft den Wert der maxActiveCalls-Eigenschaft ab.
     * 
     */
    public int getMaxActiveCalls() {
        return maxActiveCalls;
    }

    /**
     * Legt den Wert der maxActiveCalls-Eigenschaft fest.
     * 
     */
    public void setMaxActiveCalls(int value) {
        this.maxActiveCalls = value;
    }

    /**
     * Ruft den Wert der maxIncomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxIncomingCalls() {
        return maxIncomingCalls;
    }

    /**
     * Legt den Wert der maxIncomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxIncomingCalls(Integer value) {
        this.maxIncomingCalls = value;
    }

    /**
     * Ruft den Wert der maxOutgoingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxOutgoingCalls() {
        return maxOutgoingCalls;
    }

    /**
     * Legt den Wert der maxOutgoingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxOutgoingCalls(Integer value) {
        this.maxOutgoingCalls = value;
    }

    /**
     * Ruft den Wert der enableBursting-Eigenschaft ab.
     * 
     */
    public boolean isEnableBursting() {
        return enableBursting;
    }

    /**
     * Legt den Wert der enableBursting-Eigenschaft fest.
     * 
     */
    public void setEnableBursting(boolean value) {
        this.enableBursting = value;
    }

    /**
     * Ruft den Wert der burstingMaxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBurstingMaxActiveCalls() {
        return burstingMaxActiveCalls;
    }

    /**
     * Legt den Wert der burstingMaxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBurstingMaxActiveCalls(Integer value) {
        this.burstingMaxActiveCalls = value;
    }

    /**
     * Ruft den Wert der burstingMaxIncomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBurstingMaxIncomingCalls() {
        return burstingMaxIncomingCalls;
    }

    /**
     * Legt den Wert der burstingMaxIncomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBurstingMaxIncomingCalls(Integer value) {
        this.burstingMaxIncomingCalls = value;
    }

    /**
     * Ruft den Wert der burstingMaxOutgoingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBurstingMaxOutgoingCalls() {
        return burstingMaxOutgoingCalls;
    }

    /**
     * Legt den Wert der burstingMaxOutgoingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBurstingMaxOutgoingCalls(Integer value) {
        this.burstingMaxOutgoingCalls = value;
    }

    /**
     * Ruft den Wert der capacityExceededAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupCapacityExceededAction }
     *     
     */
    public TrunkGroupCapacityExceededAction getCapacityExceededAction() {
        return capacityExceededAction;
    }

    /**
     * Legt den Wert der capacityExceededAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupCapacityExceededAction }
     *     
     */
    public void setCapacityExceededAction(TrunkGroupCapacityExceededAction value) {
        this.capacityExceededAction = value;
    }

    /**
     * Ruft den Wert der capacityExceededForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCapacityExceededForwardAddress() {
        return capacityExceededForwardAddress;
    }

    /**
     * Legt den Wert der capacityExceededForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCapacityExceededForwardAddress(String value) {
        this.capacityExceededForwardAddress = value;
    }

    /**
     * Ruft den Wert der capacityExceededRerouteTrunkGroupKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupKey }
     *     
     */
    public TrunkGroupKey getCapacityExceededRerouteTrunkGroupKey() {
        return capacityExceededRerouteTrunkGroupKey;
    }

    /**
     * Legt den Wert der capacityExceededRerouteTrunkGroupKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupKey }
     *     
     */
    public void setCapacityExceededRerouteTrunkGroupKey(TrunkGroupKey value) {
        this.capacityExceededRerouteTrunkGroupKey = value;
    }

    /**
     * Ruft den Wert der capacityExceededTrapInitialCalls-Eigenschaft ab.
     * 
     */
    public int getCapacityExceededTrapInitialCalls() {
        return capacityExceededTrapInitialCalls;
    }

    /**
     * Legt den Wert der capacityExceededTrapInitialCalls-Eigenschaft fest.
     * 
     */
    public void setCapacityExceededTrapInitialCalls(int value) {
        this.capacityExceededTrapInitialCalls = value;
    }

    /**
     * Ruft den Wert der capacityExceededTrapOffsetCalls-Eigenschaft ab.
     * 
     */
    public int getCapacityExceededTrapOffsetCalls() {
        return capacityExceededTrapOffsetCalls;
    }

    /**
     * Legt den Wert der capacityExceededTrapOffsetCalls-Eigenschaft fest.
     * 
     */
    public void setCapacityExceededTrapOffsetCalls(int value) {
        this.capacityExceededTrapOffsetCalls = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupUnreachableDestinationAction }
     *     
     */
    public TrunkGroupUnreachableDestinationAction getUnreachableDestinationAction() {
        return unreachableDestinationAction;
    }

    /**
     * Legt den Wert der unreachableDestinationAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupUnreachableDestinationAction }
     *     
     */
    public void setUnreachableDestinationAction(TrunkGroupUnreachableDestinationAction value) {
        this.unreachableDestinationAction = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnreachableDestinationForwardAddress() {
        return unreachableDestinationForwardAddress;
    }

    /**
     * Legt den Wert der unreachableDestinationForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnreachableDestinationForwardAddress(String value) {
        this.unreachableDestinationForwardAddress = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationRerouteTrunkGroupKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupKey }
     *     
     */
    public TrunkGroupKey getUnreachableDestinationRerouteTrunkGroupKey() {
        return unreachableDestinationRerouteTrunkGroupKey;
    }

    /**
     * Legt den Wert der unreachableDestinationRerouteTrunkGroupKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupKey }
     *     
     */
    public void setUnreachableDestinationRerouteTrunkGroupKey(TrunkGroupKey value) {
        this.unreachableDestinationRerouteTrunkGroupKey = value;
    }

    /**
     * Ruft den Wert der invitationTimeout-Eigenschaft ab.
     * 
     */
    public int getInvitationTimeout() {
        return invitationTimeout;
    }

    /**
     * Legt den Wert der invitationTimeout-Eigenschaft fest.
     * 
     */
    public void setInvitationTimeout(int value) {
        this.invitationTimeout = value;
    }

    /**
     * Ruft den Wert der requireAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isRequireAuthentication() {
        return requireAuthentication;
    }

    /**
     * Legt den Wert der requireAuthentication-Eigenschaft fest.
     * 
     */
    public void setRequireAuthentication(boolean value) {
        this.requireAuthentication = value;
    }

    /**
     * Ruft den Wert der sipAuthenticationUserName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSipAuthenticationUserName() {
        return sipAuthenticationUserName;
    }

    /**
     * Legt den Wert der sipAuthenticationUserName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSipAuthenticationUserName(String value) {
        this.sipAuthenticationUserName = value;
    }

    /**
     * Ruft den Wert der hostedUserTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getHostedUserTable() {
        return hostedUserTable;
    }

    /**
     * Legt den Wert der hostedUserTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setHostedUserTable(OCITable value) {
        this.hostedUserTable = value;
    }

    /**
     * Ruft den Wert der trunkGroupIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrunkGroupIdentity() {
        return trunkGroupIdentity;
    }

    /**
     * Legt den Wert der trunkGroupIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrunkGroupIdentity(String value) {
        this.trunkGroupIdentity = value;
    }

    /**
     * Ruft den Wert der otgDtgIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtgDtgIdentity() {
        return otgDtgIdentity;
    }

    /**
     * Legt den Wert der otgDtgIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtgDtgIdentity(String value) {
        this.otgDtgIdentity = value;
    }

    /**
     * Ruft den Wert der allowTerminationToTrunkGroupIdentity-Eigenschaft ab.
     * 
     */
    public boolean isAllowTerminationToTrunkGroupIdentity() {
        return allowTerminationToTrunkGroupIdentity;
    }

    /**
     * Legt den Wert der allowTerminationToTrunkGroupIdentity-Eigenschaft fest.
     * 
     */
    public void setAllowTerminationToTrunkGroupIdentity(boolean value) {
        this.allowTerminationToTrunkGroupIdentity = value;
    }

    /**
     * Ruft den Wert der allowTerminationToDtgIdentity-Eigenschaft ab.
     * 
     */
    public boolean isAllowTerminationToDtgIdentity() {
        return allowTerminationToDtgIdentity;
    }

    /**
     * Legt den Wert der allowTerminationToDtgIdentity-Eigenschaft fest.
     * 
     */
    public void setAllowTerminationToDtgIdentity(boolean value) {
        this.allowTerminationToDtgIdentity = value;
    }

    /**
     * Ruft den Wert der includeTrunkGroupIdentity-Eigenschaft ab.
     * 
     */
    public boolean isIncludeTrunkGroupIdentity() {
        return includeTrunkGroupIdentity;
    }

    /**
     * Legt den Wert der includeTrunkGroupIdentity-Eigenschaft fest.
     * 
     */
    public void setIncludeTrunkGroupIdentity(boolean value) {
        this.includeTrunkGroupIdentity = value;
    }

    /**
     * Ruft den Wert der includeDtgIdentity-Eigenschaft ab.
     * 
     */
    public boolean isIncludeDtgIdentity() {
        return includeDtgIdentity;
    }

    /**
     * Legt den Wert der includeDtgIdentity-Eigenschaft fest.
     * 
     */
    public void setIncludeDtgIdentity(boolean value) {
        this.includeDtgIdentity = value;
    }

    /**
     * Ruft den Wert der includeTrunkGroupIdentityForNetworkCalls-Eigenschaft ab.
     * 
     */
    public boolean isIncludeTrunkGroupIdentityForNetworkCalls() {
        return includeTrunkGroupIdentityForNetworkCalls;
    }

    /**
     * Legt den Wert der includeTrunkGroupIdentityForNetworkCalls-Eigenschaft fest.
     * 
     */
    public void setIncludeTrunkGroupIdentityForNetworkCalls(boolean value) {
        this.includeTrunkGroupIdentityForNetworkCalls = value;
    }

    /**
     * Ruft den Wert der includeOtgIdentityForNetworkCalls-Eigenschaft ab.
     * 
     */
    public boolean isIncludeOtgIdentityForNetworkCalls() {
        return includeOtgIdentityForNetworkCalls;
    }

    /**
     * Legt den Wert der includeOtgIdentityForNetworkCalls-Eigenschaft fest.
     * 
     */
    public void setIncludeOtgIdentityForNetworkCalls(boolean value) {
        this.includeOtgIdentityForNetworkCalls = value;
    }

    /**
     * Ruft den Wert der enableNetworkAddressIdentity-Eigenschaft ab.
     * 
     */
    public boolean isEnableNetworkAddressIdentity() {
        return enableNetworkAddressIdentity;
    }

    /**
     * Legt den Wert der enableNetworkAddressIdentity-Eigenschaft fest.
     * 
     */
    public void setEnableNetworkAddressIdentity(boolean value) {
        this.enableNetworkAddressIdentity = value;
    }

    /**
     * Ruft den Wert der allowUnscreenedCalls-Eigenschaft ab.
     * 
     */
    public boolean isAllowUnscreenedCalls() {
        return allowUnscreenedCalls;
    }

    /**
     * Legt den Wert der allowUnscreenedCalls-Eigenschaft fest.
     * 
     */
    public void setAllowUnscreenedCalls(boolean value) {
        this.allowUnscreenedCalls = value;
    }

    /**
     * Ruft den Wert der allowUnscreenedEmergencyCalls-Eigenschaft ab.
     * 
     */
    public boolean isAllowUnscreenedEmergencyCalls() {
        return allowUnscreenedEmergencyCalls;
    }

    /**
     * Legt den Wert der allowUnscreenedEmergencyCalls-Eigenschaft fest.
     * 
     */
    public void setAllowUnscreenedEmergencyCalls(boolean value) {
        this.allowUnscreenedEmergencyCalls = value;
    }

    /**
     * Ruft den Wert der pilotUserCallingLineIdentityPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupPilotUserCallingLineIdentityUsagePolicy }
     *     
     */
    public TrunkGroupPilotUserCallingLineIdentityUsagePolicy getPilotUserCallingLineIdentityPolicy() {
        return pilotUserCallingLineIdentityPolicy;
    }

    /**
     * Legt den Wert der pilotUserCallingLineIdentityPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupPilotUserCallingLineIdentityUsagePolicy }
     *     
     */
    public void setPilotUserCallingLineIdentityPolicy(TrunkGroupPilotUserCallingLineIdentityUsagePolicy value) {
        this.pilotUserCallingLineIdentityPolicy = value;
    }

    /**
     * Ruft den Wert der pilotUserChargeNumberPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupPilotUserChargeNumberUsagePolicy }
     *     
     */
    public TrunkGroupPilotUserChargeNumberUsagePolicy getPilotUserChargeNumberPolicy() {
        return pilotUserChargeNumberPolicy;
    }

    /**
     * Legt den Wert der pilotUserChargeNumberPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupPilotUserChargeNumberUsagePolicy }
     *     
     */
    public void setPilotUserChargeNumberPolicy(TrunkGroupPilotUserChargeNumberUsagePolicy value) {
        this.pilotUserChargeNumberPolicy = value;
    }

    /**
     * Ruft den Wert der callForwardingAlwaysAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupCallForwardingAlwaysAction }
     *     
     */
    public TrunkGroupCallForwardingAlwaysAction getCallForwardingAlwaysAction() {
        return callForwardingAlwaysAction;
    }

    /**
     * Legt den Wert der callForwardingAlwaysAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupCallForwardingAlwaysAction }
     *     
     */
    public void setCallForwardingAlwaysAction(TrunkGroupCallForwardingAlwaysAction value) {
        this.callForwardingAlwaysAction = value;
    }

    /**
     * Ruft den Wert der callForwardingAlwaysForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallForwardingAlwaysForwardAddress() {
        return callForwardingAlwaysForwardAddress;
    }

    /**
     * Legt den Wert der callForwardingAlwaysForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallForwardingAlwaysForwardAddress(String value) {
        this.callForwardingAlwaysForwardAddress = value;
    }

    /**
     * Ruft den Wert der callForwardingAlwaysRerouteTrunkGroupKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupKey }
     *     
     */
    public TrunkGroupKey getCallForwardingAlwaysRerouteTrunkGroupKey() {
        return callForwardingAlwaysRerouteTrunkGroupKey;
    }

    /**
     * Legt den Wert der callForwardingAlwaysRerouteTrunkGroupKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupKey }
     *     
     */
    public void setCallForwardingAlwaysRerouteTrunkGroupKey(TrunkGroupKey value) {
        this.callForwardingAlwaysRerouteTrunkGroupKey = value;
    }

    /**
     * Ruft den Wert der peeringDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeeringDomain() {
        return peeringDomain;
    }

    /**
     * Legt den Wert der peeringDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeeringDomain(String value) {
        this.peeringDomain = value;
    }

    /**
     * Ruft den Wert der routeToPeeringDomain-Eigenschaft ab.
     * 
     */
    public boolean isRouteToPeeringDomain() {
        return routeToPeeringDomain;
    }

    /**
     * Legt den Wert der routeToPeeringDomain-Eigenschaft fest.
     * 
     */
    public void setRouteToPeeringDomain(boolean value) {
        this.routeToPeeringDomain = value;
    }

    /**
     * Ruft den Wert der prefixEnabled-Eigenschaft ab.
     * 
     */
    public boolean isPrefixEnabled() {
        return prefixEnabled;
    }

    /**
     * Legt den Wert der prefixEnabled-Eigenschaft fest.
     * 
     */
    public void setPrefixEnabled(boolean value) {
        this.prefixEnabled = value;
    }

    /**
     * Ruft den Wert der prefix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Legt den Wert der prefix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrefix(String value) {
        this.prefix = value;
    }

    /**
     * Ruft den Wert der statefulReroutingEnabled-Eigenschaft ab.
     * 
     */
    public boolean isStatefulReroutingEnabled() {
        return statefulReroutingEnabled;
    }

    /**
     * Legt den Wert der statefulReroutingEnabled-Eigenschaft fest.
     * 
     */
    public void setStatefulReroutingEnabled(boolean value) {
        this.statefulReroutingEnabled = value;
    }

    /**
     * Ruft den Wert der sendContinuousOptionsMessage-Eigenschaft ab.
     * 
     */
    public boolean isSendContinuousOptionsMessage() {
        return sendContinuousOptionsMessage;
    }

    /**
     * Legt den Wert der sendContinuousOptionsMessage-Eigenschaft fest.
     * 
     */
    public void setSendContinuousOptionsMessage(boolean value) {
        this.sendContinuousOptionsMessage = value;
    }

    /**
     * Ruft den Wert der continuousOptionsSendingIntervalSeconds-Eigenschaft ab.
     * 
     */
    public int getContinuousOptionsSendingIntervalSeconds() {
        return continuousOptionsSendingIntervalSeconds;
    }

    /**
     * Legt den Wert der continuousOptionsSendingIntervalSeconds-Eigenschaft fest.
     * 
     */
    public void setContinuousOptionsSendingIntervalSeconds(int value) {
        this.continuousOptionsSendingIntervalSeconds = value;
    }

    /**
     * Ruft den Wert der failureOptionsSendingIntervalSeconds-Eigenschaft ab.
     * 
     */
    public int getFailureOptionsSendingIntervalSeconds() {
        return failureOptionsSendingIntervalSeconds;
    }

    /**
     * Legt den Wert der failureOptionsSendingIntervalSeconds-Eigenschaft fest.
     * 
     */
    public void setFailureOptionsSendingIntervalSeconds(int value) {
        this.failureOptionsSendingIntervalSeconds = value;
    }

    /**
     * Ruft den Wert der failureThresholdCounter-Eigenschaft ab.
     * 
     */
    public int getFailureThresholdCounter() {
        return failureThresholdCounter;
    }

    /**
     * Legt den Wert der failureThresholdCounter-Eigenschaft fest.
     * 
     */
    public void setFailureThresholdCounter(int value) {
        this.failureThresholdCounter = value;
    }

    /**
     * Ruft den Wert der successThresholdCounter-Eigenschaft ab.
     * 
     */
    public int getSuccessThresholdCounter() {
        return successThresholdCounter;
    }

    /**
     * Legt den Wert der successThresholdCounter-Eigenschaft fest.
     * 
     */
    public void setSuccessThresholdCounter(int value) {
        this.successThresholdCounter = value;
    }

    /**
     * Ruft den Wert der inviteFailureThresholdCounter-Eigenschaft ab.
     * 
     */
    public int getInviteFailureThresholdCounter() {
        return inviteFailureThresholdCounter;
    }

    /**
     * Legt den Wert der inviteFailureThresholdCounter-Eigenschaft fest.
     * 
     */
    public void setInviteFailureThresholdCounter(int value) {
        this.inviteFailureThresholdCounter = value;
    }

    /**
     * Ruft den Wert der inviteFailureThresholdWindowSeconds-Eigenschaft ab.
     * 
     */
    public int getInviteFailureThresholdWindowSeconds() {
        return inviteFailureThresholdWindowSeconds;
    }

    /**
     * Legt den Wert der inviteFailureThresholdWindowSeconds-Eigenschaft fest.
     * 
     */
    public void setInviteFailureThresholdWindowSeconds(int value) {
        this.inviteFailureThresholdWindowSeconds = value;
    }

    /**
     * Ruft den Wert der trunkGroupState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupState }
     *     
     */
    public TrunkGroupState getTrunkGroupState() {
        return trunkGroupState;
    }

    /**
     * Legt den Wert der trunkGroupState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupState }
     *     
     */
    public void setTrunkGroupState(TrunkGroupState value) {
        this.trunkGroupState = value;
    }

    /**
     * Ruft den Wert der pilotUserCallingLineAssertedIdentityPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy }
     *     
     */
    public TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy getPilotUserCallingLineAssertedIdentityPolicy() {
        return pilotUserCallingLineAssertedIdentityPolicy;
    }

    /**
     * Legt den Wert der pilotUserCallingLineAssertedIdentityPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy }
     *     
     */
    public void setPilotUserCallingLineAssertedIdentityPolicy(TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy value) {
        this.pilotUserCallingLineAssertedIdentityPolicy = value;
    }

    /**
     * Ruft den Wert der useSystemCallingLineAssertedIdentityPolicy-Eigenschaft ab.
     * 
     */
    public boolean isUseSystemCallingLineAssertedIdentityPolicy() {
        return useSystemCallingLineAssertedIdentityPolicy;
    }

    /**
     * Legt den Wert der useSystemCallingLineAssertedIdentityPolicy-Eigenschaft fest.
     * 
     */
    public void setUseSystemCallingLineAssertedIdentityPolicy(boolean value) {
        this.useSystemCallingLineAssertedIdentityPolicy = value;
    }

}
