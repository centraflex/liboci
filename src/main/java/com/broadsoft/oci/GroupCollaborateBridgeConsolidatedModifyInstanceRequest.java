//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a Collaborate bridge.
 *         The request fails when the collaborateOwnerUserIdList is included in the request for the default collaborate bridge.
 *         The request fails when the supportOutdial is included in the request and the system-level collaborate supportOutdial setting is disabled.
 *         When phone numbers are un-assigned from the user, the unused numbers may be un-assigned from the group and service provider. If UnassignPhoneNumbersLevel is set to 'Group', the user's primary phone number, fax number and any alternate numbers, will be un-assigned from the group if the command is executed by a service provider administrator or above.
 *         When set to 'Service Provider', they will be un-assigned from the group and service provider if the command is executed by a provisioning administrator or above.
 *         When omitted, the number(s) will be left assigned to the group.
 *         An ErrorResponse will be returned if any number cannot be unassigned because of insufficient privilege.
 *         
 *         If the phoneNumber has not been assigned to the group and addPhoneNumberToGroup is set to true, it will be added to group if needed if the command is executed by a service provider administrator and above. The command will fail otherwise.
 *         
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCollaborateBridgeConsolidatedModifyInstanceRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCollaborateBridgeConsolidatedModifyInstanceRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="unassignPhoneNumbers" type="{}UnassignPhoneNumbersLevel" minOccurs="0"/>
 *         <element name="addPhoneNumberToGroup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceModifyProfile" minOccurs="0"/>
 *         <element name="maximumBridgeParticipants" type="{}CollaborateBridgeMaximumParticipants" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="maxCollaborateRoomParticipants" type="{}CollaborateRoomMaximumParticipants20sp1" minOccurs="0"/>
 *         <element name="supportOutdial" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="collaborateOwnerUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *         <element name="serviceList" type="{}ReplacementConsolidatedUserServiceAssignmentList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCollaborateBridgeConsolidatedModifyInstanceRequest", propOrder = {
    "serviceUserId",
    "unassignPhoneNumbers",
    "addPhoneNumberToGroup",
    "serviceInstanceProfile",
    "maximumBridgeParticipants",
    "networkClassOfService",
    "maxCollaborateRoomParticipants",
    "supportOutdial",
    "collaborateOwnerUserIdList",
    "serviceList"
})
public class GroupCollaborateBridgeConsolidatedModifyInstanceRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlSchemaType(name = "token")
    protected UnassignPhoneNumbersLevel unassignPhoneNumbers;
    protected Boolean addPhoneNumberToGroup;
    protected ServiceInstanceModifyProfile serviceInstanceProfile;
    protected CollaborateBridgeMaximumParticipants maximumBridgeParticipants;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    protected Integer maxCollaborateRoomParticipants;
    protected Boolean supportOutdial;
    @XmlElementRef(name = "collaborateOwnerUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> collaborateOwnerUserIdList;
    @XmlElementRef(name = "serviceList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementConsolidatedUserServiceAssignmentList> serviceList;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der unassignPhoneNumbers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnassignPhoneNumbersLevel }
     *     
     */
    public UnassignPhoneNumbersLevel getUnassignPhoneNumbers() {
        return unassignPhoneNumbers;
    }

    /**
     * Legt den Wert der unassignPhoneNumbers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnassignPhoneNumbersLevel }
     *     
     */
    public void setUnassignPhoneNumbers(UnassignPhoneNumbersLevel value) {
        this.unassignPhoneNumbers = value;
    }

    /**
     * Ruft den Wert der addPhoneNumberToGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAddPhoneNumberToGroup() {
        return addPhoneNumberToGroup;
    }

    /**
     * Legt den Wert der addPhoneNumberToGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAddPhoneNumberToGroup(Boolean value) {
        this.addPhoneNumberToGroup = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public ServiceInstanceModifyProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceModifyProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der maximumBridgeParticipants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CollaborateBridgeMaximumParticipants }
     *     
     */
    public CollaborateBridgeMaximumParticipants getMaximumBridgeParticipants() {
        return maximumBridgeParticipants;
    }

    /**
     * Legt den Wert der maximumBridgeParticipants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CollaborateBridgeMaximumParticipants }
     *     
     */
    public void setMaximumBridgeParticipants(CollaborateBridgeMaximumParticipants value) {
        this.maximumBridgeParticipants = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Ruft den Wert der maxCollaborateRoomParticipants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCollaborateRoomParticipants() {
        return maxCollaborateRoomParticipants;
    }

    /**
     * Legt den Wert der maxCollaborateRoomParticipants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCollaborateRoomParticipants(Integer value) {
        this.maxCollaborateRoomParticipants = value;
    }

    /**
     * Ruft den Wert der supportOutdial-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportOutdial() {
        return supportOutdial;
    }

    /**
     * Legt den Wert der supportOutdial-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportOutdial(Boolean value) {
        this.supportOutdial = value;
    }

    /**
     * Ruft den Wert der collaborateOwnerUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getCollaborateOwnerUserIdList() {
        return collaborateOwnerUserIdList;
    }

    /**
     * Legt den Wert der collaborateOwnerUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setCollaborateOwnerUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.collaborateOwnerUserIdList = value;
    }

    /**
     * Ruft den Wert der serviceList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementConsolidatedUserServiceAssignmentList }{@code >}
     *     
     */
    public JAXBElement<ReplacementConsolidatedUserServiceAssignmentList> getServiceList() {
        return serviceList;
    }

    /**
     * Legt den Wert der serviceList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementConsolidatedUserServiceAssignmentList }{@code >}
     *     
     */
    public void setServiceList(JAXBElement<ReplacementConsolidatedUserServiceAssignmentList> value) {
        this.serviceList = value;
    }

}
