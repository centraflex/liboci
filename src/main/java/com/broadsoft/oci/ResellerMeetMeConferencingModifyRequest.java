//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the reseller level data associated with Meet-Me Conferencing functions.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         The following data elements are only modified for System and Provisioning Administrators:        
 * 		maxAllocatedPorts.
 * 		The following data elements are only modified for System and Provisioning Administrators and AS Mode only:
 * 		disableUnlimitedMeetMePorts,
 * 		enableMaxAllocatedPorts.
 *       
 * 
 * <p>Java-Klasse für ResellerMeetMeConferencingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerMeetMeConferencingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="resellerId" type="{}ResellerId22"/>
 *         <element name="conferenceFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="maxAllocatedPorts" type="{}ResellerMeetMeConferencingConferencePorts" minOccurs="0"/>
 *         <element name="disableUnlimitedMeetMePorts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableMaxAllocatedPorts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerMeetMeConferencingModifyRequest", propOrder = {
    "resellerId",
    "conferenceFromAddress",
    "maxAllocatedPorts",
    "disableUnlimitedMeetMePorts",
    "enableMaxAllocatedPorts"
})
public class ResellerMeetMeConferencingModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    @XmlElementRef(name = "conferenceFromAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> conferenceFromAddress;
    protected Integer maxAllocatedPorts;
    protected Boolean disableUnlimitedMeetMePorts;
    protected Boolean enableMaxAllocatedPorts;

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Ruft den Wert der conferenceFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getConferenceFromAddress() {
        return conferenceFromAddress;
    }

    /**
     * Legt den Wert der conferenceFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setConferenceFromAddress(JAXBElement<String> value) {
        this.conferenceFromAddress = value;
    }

    /**
     * Ruft den Wert der maxAllocatedPorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxAllocatedPorts() {
        return maxAllocatedPorts;
    }

    /**
     * Legt den Wert der maxAllocatedPorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxAllocatedPorts(Integer value) {
        this.maxAllocatedPorts = value;
    }

    /**
     * Ruft den Wert der disableUnlimitedMeetMePorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisableUnlimitedMeetMePorts() {
        return disableUnlimitedMeetMePorts;
    }

    /**
     * Legt den Wert der disableUnlimitedMeetMePorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisableUnlimitedMeetMePorts(Boolean value) {
        this.disableUnlimitedMeetMePorts = value;
    }

    /**
     * Ruft den Wert der enableMaxAllocatedPorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMaxAllocatedPorts() {
        return enableMaxAllocatedPorts;
    }

    /**
     * Legt den Wert der enableMaxAllocatedPorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMaxAllocatedPorts(Boolean value) {
        this.enableMaxAllocatedPorts = value;
    }

}
