//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains the call center media on hold source configuration.
 *       
 * 
 * <p>Java-Klasse für CallCenterMediaOnHoldSourceRead22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterMediaOnHoldSourceRead22">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="audioMessageSourceSelection" type="{}CallCenterMediaOnHoldMessageSelection"/>
 *         <element name="audioUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="audioFileList" type="{}CallCenterAnnouncementFileListRead20" minOccurs="0"/>
 *         <element name="externalAudioSource" type="{}AccessDeviceEndpointWithPortNumberRead22" minOccurs="0"/>
 *         <element name="videoMessageSourceSelection" type="{}CallCenterMediaOnHoldMessageSelection" minOccurs="0"/>
 *         <element name="videoUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="videoFileList" type="{}CallCenterAnnouncementFileListRead20" minOccurs="0"/>
 *         <element name="externalVideoSource" type="{}AccessDeviceEndpointWithPortNumberRead22" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterMediaOnHoldSourceRead22", propOrder = {
    "audioMessageSourceSelection",
    "audioUrlList",
    "audioFileList",
    "externalAudioSource",
    "videoMessageSourceSelection",
    "videoUrlList",
    "videoFileList",
    "externalVideoSource"
})
public class CallCenterMediaOnHoldSourceRead22 {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterMediaOnHoldMessageSelection audioMessageSourceSelection;
    protected CallCenterAnnouncementURLList audioUrlList;
    protected CallCenterAnnouncementFileListRead20 audioFileList;
    protected AccessDeviceEndpointWithPortNumberRead22 externalAudioSource;
    @XmlSchemaType(name = "token")
    protected CallCenterMediaOnHoldMessageSelection videoMessageSourceSelection;
    protected CallCenterAnnouncementURLList videoUrlList;
    protected CallCenterAnnouncementFileListRead20 videoFileList;
    protected AccessDeviceEndpointWithPortNumberRead22 externalVideoSource;

    /**
     * Ruft den Wert der audioMessageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public CallCenterMediaOnHoldMessageSelection getAudioMessageSourceSelection() {
        return audioMessageSourceSelection;
    }

    /**
     * Legt den Wert der audioMessageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public void setAudioMessageSourceSelection(CallCenterMediaOnHoldMessageSelection value) {
        this.audioMessageSourceSelection = value;
    }

    /**
     * Ruft den Wert der audioUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getAudioUrlList() {
        return audioUrlList;
    }

    /**
     * Legt den Wert der audioUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setAudioUrlList(CallCenterAnnouncementURLList value) {
        this.audioUrlList = value;
    }

    /**
     * Ruft den Wert der audioFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListRead20 }
     *     
     */
    public CallCenterAnnouncementFileListRead20 getAudioFileList() {
        return audioFileList;
    }

    /**
     * Legt den Wert der audioFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListRead20 }
     *     
     */
    public void setAudioFileList(CallCenterAnnouncementFileListRead20 value) {
        this.audioFileList = value;
    }

    /**
     * Ruft den Wert der externalAudioSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEndpointWithPortNumberRead22 }
     *     
     */
    public AccessDeviceEndpointWithPortNumberRead22 getExternalAudioSource() {
        return externalAudioSource;
    }

    /**
     * Legt den Wert der externalAudioSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEndpointWithPortNumberRead22 }
     *     
     */
    public void setExternalAudioSource(AccessDeviceEndpointWithPortNumberRead22 value) {
        this.externalAudioSource = value;
    }

    /**
     * Ruft den Wert der videoMessageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public CallCenterMediaOnHoldMessageSelection getVideoMessageSourceSelection() {
        return videoMessageSourceSelection;
    }

    /**
     * Legt den Wert der videoMessageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public void setVideoMessageSourceSelection(CallCenterMediaOnHoldMessageSelection value) {
        this.videoMessageSourceSelection = value;
    }

    /**
     * Ruft den Wert der videoUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getVideoUrlList() {
        return videoUrlList;
    }

    /**
     * Legt den Wert der videoUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setVideoUrlList(CallCenterAnnouncementURLList value) {
        this.videoUrlList = value;
    }

    /**
     * Ruft den Wert der videoFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementFileListRead20 }
     *     
     */
    public CallCenterAnnouncementFileListRead20 getVideoFileList() {
        return videoFileList;
    }

    /**
     * Legt den Wert der videoFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementFileListRead20 }
     *     
     */
    public void setVideoFileList(CallCenterAnnouncementFileListRead20 value) {
        this.videoFileList = value;
    }

    /**
     * Ruft den Wert der externalVideoSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEndpointWithPortNumberRead22 }
     *     
     */
    public AccessDeviceEndpointWithPortNumberRead22 getExternalVideoSource() {
        return externalVideoSource;
    }

    /**
     * Legt den Wert der externalVideoSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEndpointWithPortNumberRead22 }
     *     
     */
    public void setExternalVideoSource(AccessDeviceEndpointWithPortNumberRead22 value) {
        this.externalVideoSource = value;
    }

}
