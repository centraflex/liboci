//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the user's Video Add-On service setting.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserVideoAddOnModifyRequest14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserVideoAddOnModifyRequest14">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxOriginatingCallDelaySeconds" type="{}VideoAddOnMaxOriginatingCallDelaySeconds" minOccurs="0"/>
 *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointModify" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserVideoAddOnModifyRequest14", propOrder = {
    "userId",
    "isActive",
    "maxOriginatingCallDelaySeconds",
    "accessDeviceEndpoint"
})
public class UserVideoAddOnModifyRequest14
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean isActive;
    protected Integer maxOriginatingCallDelaySeconds;
    @XmlElementRef(name = "accessDeviceEndpoint", type = JAXBElement.class, required = false)
    protected JAXBElement<AccessDeviceEndpointModify> accessDeviceEndpoint;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der maxOriginatingCallDelaySeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxOriginatingCallDelaySeconds() {
        return maxOriginatingCallDelaySeconds;
    }

    /**
     * Legt den Wert der maxOriginatingCallDelaySeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxOriginatingCallDelaySeconds(Integer value) {
        this.maxOriginatingCallDelaySeconds = value;
    }

    /**
     * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceEndpointModify }{@code >}
     *     
     */
    public JAXBElement<AccessDeviceEndpointModify> getAccessDeviceEndpoint() {
        return accessDeviceEndpoint;
    }

    /**
     * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceEndpointModify }{@code >}
     *     
     */
    public void setAccessDeviceEndpoint(JAXBElement<AccessDeviceEndpointModify> value) {
        this.accessDeviceEndpoint = value;
    }

}
