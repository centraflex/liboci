//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemDomainParametersGetRequest.
 *         Contains the system Domain parameters.
 *         
 *         The following elements are only used in AS data mode:
 *           useAliasForDomain, value "false" is returned in XS data mode.
 *           
 *         The following elements are only used in AS and XS data mode and not returned in Amplify data mode.
 *           defaultDomain
 *       
 * 
 * <p>Java-Klasse für SystemDomainParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDomainParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useAliasForDomain" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="defaultDomain" type="{}NetAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDomainParametersGetResponse", propOrder = {
    "useAliasForDomain",
    "defaultDomain"
})
public class SystemDomainParametersGetResponse
    extends OCIDataResponse
{

    protected boolean useAliasForDomain;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultDomain;

    /**
     * Ruft den Wert der useAliasForDomain-Eigenschaft ab.
     * 
     */
    public boolean isUseAliasForDomain() {
        return useAliasForDomain;
    }

    /**
     * Legt den Wert der useAliasForDomain-Eigenschaft fest.
     * 
     */
    public void setUseAliasForDomain(boolean value) {
        this.useAliasForDomain = value;
    }

    /**
     * Ruft den Wert der defaultDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultDomain() {
        return defaultDomain;
    }

    /**
     * Legt den Wert der defaultDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultDomain(String value) {
        this.defaultDomain = value;
    }

}
