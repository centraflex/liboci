//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to unlink one or more leaf access devices from a tree device.
 *         A leaf device is a device associated with a device type that has the option
 *         supportLinks set to "Support Link to Device". It can be linked to only one tree device.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupAccessDeviceUnlinkDeviceListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAccessDeviceUnlinkDeviceListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="leafDeviceKey" type="{}AccessDeviceKey" maxOccurs="unbounded"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAccessDeviceUnlinkDeviceListRequest", propOrder = {
    "leafDeviceKey"
})
public class GroupAccessDeviceUnlinkDeviceListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    protected List<AccessDeviceKey> leafDeviceKey;

    /**
     * Gets the value of the leafDeviceKey property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the leafDeviceKey property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLeafDeviceKey().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccessDeviceKey }
     * 
     * 
     * @return
     *     The value of the leafDeviceKey property.
     */
    public List<AccessDeviceKey> getLeafDeviceKey() {
        if (leafDeviceKey == null) {
            leafDeviceKey = new ArrayList<>();
        }
        return this.leafDeviceKey;
    }

}
