//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to EnterpriseCallCenterGetRoutingPolicyRequest.
 *         Contains a table with column headings: "Service User Id", "Name" and
 *         "Priority".
 *       
 * 
 * <p>Java-Klasse für EnterpriseCallCenterGetRoutingPolicyResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseCallCenterGetRoutingPolicyResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="routingPolicy" type="{}CallCenterRoutingPolicy"/>
 *         <element name="callCenterTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseCallCenterGetRoutingPolicyResponse", propOrder = {
    "routingPolicy",
    "callCenterTable"
})
public class EnterpriseCallCenterGetRoutingPolicyResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterRoutingPolicy routingPolicy;
    @XmlElement(required = true)
    protected OCITable callCenterTable;

    /**
     * Ruft den Wert der routingPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterRoutingPolicy }
     *     
     */
    public CallCenterRoutingPolicy getRoutingPolicy() {
        return routingPolicy;
    }

    /**
     * Legt den Wert der routingPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterRoutingPolicy }
     *     
     */
    public void setRoutingPolicy(CallCenterRoutingPolicy value) {
        this.routingPolicy = value;
    }

    /**
     * Ruft den Wert der callCenterTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getCallCenterTable() {
        return callCenterTable;
    }

    /**
     * Legt den Wert der callCenterTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setCallCenterTable(OCITable value) {
        this.callCenterTable = value;
    }

}
