//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeviceManagementFileAuthenticationStatus.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DeviceManagementFileAuthenticationStatus">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Challenge"/>
 *     <enumeration value="Need Digest Authentication"/>
 *     <enumeration value="Need User Password Authentication"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DeviceManagementFileAuthenticationStatus")
@XmlEnum
public enum DeviceManagementFileAuthenticationStatus {

    @XmlEnumValue("Challenge")
    CHALLENGE("Challenge"),
    @XmlEnumValue("Need Digest Authentication")
    NEED_DIGEST_AUTHENTICATION("Need Digest Authentication"),
    @XmlEnumValue("Need User Password Authentication")
    NEED_USER_PASSWORD_AUTHENTICATION("Need User Password Authentication");
    private final String value;

    DeviceManagementFileAuthenticationStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeviceManagementFileAuthenticationStatus fromValue(String v) {
        for (DeviceManagementFileAuthenticationStatus c: DeviceManagementFileAuthenticationStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
