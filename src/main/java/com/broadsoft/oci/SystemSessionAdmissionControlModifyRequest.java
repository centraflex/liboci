//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the session admission control settings for the system.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemSessionAdmissionControlModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSessionAdmissionControlModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="countLongConnectionsToMediaServer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="sacHandlingForMoH" type="{}SessionAdmissionControlForMusicOnHoldType" minOccurs="0"/>
 *         <element name="blockVMDepositDueToSACLimits" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="sacCodecSelectionPolicy" type="{}SessionAdmissionControlCodecSelectionPolicyType" minOccurs="0"/>
 *         <element name="countCallToMobileNumberForSACSubscriber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="countBWAnywhereForSACSubscriber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="countROForSACSubscriber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="excludeBWMobilityForSACSubscriber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableHoldoverOfHighwaterSessionCounts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="holdoverPeriodMinutes" type="{}SessionAdmissionControlHighwaterSessionCountHoldoverPeriodMinutes" minOccurs="0"/>
 *         <element name="timeZoneOffsetMinutes" type="{}SessionAdmissionControlTimeZoneOffsetMinutes" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSessionAdmissionControlModifyRequest", propOrder = {
    "countLongConnectionsToMediaServer",
    "sacHandlingForMoH",
    "blockVMDepositDueToSACLimits",
    "sacCodecSelectionPolicy",
    "countCallToMobileNumberForSACSubscriber",
    "countBWAnywhereForSACSubscriber",
    "countROForSACSubscriber",
    "excludeBWMobilityForSACSubscriber",
    "enableHoldoverOfHighwaterSessionCounts",
    "holdoverPeriodMinutes",
    "timeZoneOffsetMinutes"
})
public class SystemSessionAdmissionControlModifyRequest
    extends OCIRequest
{

    protected Boolean countLongConnectionsToMediaServer;
    @XmlSchemaType(name = "token")
    protected SessionAdmissionControlForMusicOnHoldType sacHandlingForMoH;
    protected Boolean blockVMDepositDueToSACLimits;
    @XmlSchemaType(name = "token")
    protected SessionAdmissionControlCodecSelectionPolicyType sacCodecSelectionPolicy;
    protected Boolean countCallToMobileNumberForSACSubscriber;
    protected Boolean countBWAnywhereForSACSubscriber;
    protected Boolean countROForSACSubscriber;
    protected Boolean excludeBWMobilityForSACSubscriber;
    protected Boolean enableHoldoverOfHighwaterSessionCounts;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String holdoverPeriodMinutes;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeZoneOffsetMinutes;

    /**
     * Ruft den Wert der countLongConnectionsToMediaServer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCountLongConnectionsToMediaServer() {
        return countLongConnectionsToMediaServer;
    }

    /**
     * Legt den Wert der countLongConnectionsToMediaServer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCountLongConnectionsToMediaServer(Boolean value) {
        this.countLongConnectionsToMediaServer = value;
    }

    /**
     * Ruft den Wert der sacHandlingForMoH-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SessionAdmissionControlForMusicOnHoldType }
     *     
     */
    public SessionAdmissionControlForMusicOnHoldType getSacHandlingForMoH() {
        return sacHandlingForMoH;
    }

    /**
     * Legt den Wert der sacHandlingForMoH-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionAdmissionControlForMusicOnHoldType }
     *     
     */
    public void setSacHandlingForMoH(SessionAdmissionControlForMusicOnHoldType value) {
        this.sacHandlingForMoH = value;
    }

    /**
     * Ruft den Wert der blockVMDepositDueToSACLimits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBlockVMDepositDueToSACLimits() {
        return blockVMDepositDueToSACLimits;
    }

    /**
     * Legt den Wert der blockVMDepositDueToSACLimits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBlockVMDepositDueToSACLimits(Boolean value) {
        this.blockVMDepositDueToSACLimits = value;
    }

    /**
     * Ruft den Wert der sacCodecSelectionPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SessionAdmissionControlCodecSelectionPolicyType }
     *     
     */
    public SessionAdmissionControlCodecSelectionPolicyType getSacCodecSelectionPolicy() {
        return sacCodecSelectionPolicy;
    }

    /**
     * Legt den Wert der sacCodecSelectionPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionAdmissionControlCodecSelectionPolicyType }
     *     
     */
    public void setSacCodecSelectionPolicy(SessionAdmissionControlCodecSelectionPolicyType value) {
        this.sacCodecSelectionPolicy = value;
    }

    /**
     * Ruft den Wert der countCallToMobileNumberForSACSubscriber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCountCallToMobileNumberForSACSubscriber() {
        return countCallToMobileNumberForSACSubscriber;
    }

    /**
     * Legt den Wert der countCallToMobileNumberForSACSubscriber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCountCallToMobileNumberForSACSubscriber(Boolean value) {
        this.countCallToMobileNumberForSACSubscriber = value;
    }

    /**
     * Ruft den Wert der countBWAnywhereForSACSubscriber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCountBWAnywhereForSACSubscriber() {
        return countBWAnywhereForSACSubscriber;
    }

    /**
     * Legt den Wert der countBWAnywhereForSACSubscriber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCountBWAnywhereForSACSubscriber(Boolean value) {
        this.countBWAnywhereForSACSubscriber = value;
    }

    /**
     * Ruft den Wert der countROForSACSubscriber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCountROForSACSubscriber() {
        return countROForSACSubscriber;
    }

    /**
     * Legt den Wert der countROForSACSubscriber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCountROForSACSubscriber(Boolean value) {
        this.countROForSACSubscriber = value;
    }

    /**
     * Ruft den Wert der excludeBWMobilityForSACSubscriber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExcludeBWMobilityForSACSubscriber() {
        return excludeBWMobilityForSACSubscriber;
    }

    /**
     * Legt den Wert der excludeBWMobilityForSACSubscriber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludeBWMobilityForSACSubscriber(Boolean value) {
        this.excludeBWMobilityForSACSubscriber = value;
    }

    /**
     * Ruft den Wert der enableHoldoverOfHighwaterSessionCounts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableHoldoverOfHighwaterSessionCounts() {
        return enableHoldoverOfHighwaterSessionCounts;
    }

    /**
     * Legt den Wert der enableHoldoverOfHighwaterSessionCounts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableHoldoverOfHighwaterSessionCounts(Boolean value) {
        this.enableHoldoverOfHighwaterSessionCounts = value;
    }

    /**
     * Ruft den Wert der holdoverPeriodMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoldoverPeriodMinutes() {
        return holdoverPeriodMinutes;
    }

    /**
     * Legt den Wert der holdoverPeriodMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoldoverPeriodMinutes(String value) {
        this.holdoverPeriodMinutes = value;
    }

    /**
     * Ruft den Wert der timeZoneOffsetMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeZoneOffsetMinutes() {
        return timeZoneOffsetMinutes;
    }

    /**
     * Legt den Wert der timeZoneOffsetMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeZoneOffsetMinutes(String value) {
        this.timeZoneOffsetMinutes = value;
    }

}
