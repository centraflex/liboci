//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to add an Auto Attendant submenu instance.
 *         The response is either SuccessResponse or ErrorResponse. 
 *         This request is only valid for Standard auto attendants.
 *       
 * 
 * <p>Java-Klasse für GroupAutoAttendantSubmenuAddRequest20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAutoAttendantSubmenuAddRequest20">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="submenuId" type="{}AutoAttendantSubmenuId"/>
 *         <element name="announcementSelection" type="{}AnnouncementSelection"/>
 *         <element name="audioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="videoFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="enableLevelExtensionDialing" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="keyConfiguration" type="{}AutoAttendantKeyConfiguration20" maxOccurs="12" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAutoAttendantSubmenuAddRequest20", propOrder = {
    "serviceUserId",
    "submenuId",
    "announcementSelection",
    "audioFile",
    "videoFile",
    "enableLevelExtensionDialing",
    "keyConfiguration"
})
public class GroupAutoAttendantSubmenuAddRequest20
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String submenuId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AnnouncementSelection announcementSelection;
    protected AnnouncementFileLevelKey audioFile;
    protected AnnouncementFileLevelKey videoFile;
    protected boolean enableLevelExtensionDialing;
    protected List<AutoAttendantKeyConfiguration20> keyConfiguration;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der submenuId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubmenuId() {
        return submenuId;
    }

    /**
     * Legt den Wert der submenuId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubmenuId(String value) {
        this.submenuId = value;
    }

    /**
     * Ruft den Wert der announcementSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementSelection }
     *     
     */
    public AnnouncementSelection getAnnouncementSelection() {
        return announcementSelection;
    }

    /**
     * Legt den Wert der announcementSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementSelection }
     *     
     */
    public void setAnnouncementSelection(AnnouncementSelection value) {
        this.announcementSelection = value;
    }

    /**
     * Ruft den Wert der audioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getAudioFile() {
        return audioFile;
    }

    /**
     * Legt den Wert der audioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setAudioFile(AnnouncementFileLevelKey value) {
        this.audioFile = value;
    }

    /**
     * Ruft den Wert der videoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public AnnouncementFileLevelKey getVideoFile() {
        return videoFile;
    }

    /**
     * Legt den Wert der videoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileLevelKey }
     *     
     */
    public void setVideoFile(AnnouncementFileLevelKey value) {
        this.videoFile = value;
    }

    /**
     * Ruft den Wert der enableLevelExtensionDialing-Eigenschaft ab.
     * 
     */
    public boolean isEnableLevelExtensionDialing() {
        return enableLevelExtensionDialing;
    }

    /**
     * Legt den Wert der enableLevelExtensionDialing-Eigenschaft fest.
     * 
     */
    public void setEnableLevelExtensionDialing(boolean value) {
        this.enableLevelExtensionDialing = value;
    }

    /**
     * Gets the value of the keyConfiguration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the keyConfiguration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKeyConfiguration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AutoAttendantKeyConfiguration20 }
     * 
     * 
     * @return
     *     The value of the keyConfiguration property.
     */
    public List<AutoAttendantKeyConfiguration20> getKeyConfiguration() {
        if (keyConfiguration == null) {
            keyConfiguration = new ArrayList<>();
        }
        return this.keyConfiguration;
    }

}
