//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Used to sort the GroupAdminGetPagedSortedListRequest request.
 *       
 * 
 * <p>Java-Klasse für SortOrderGroupAdminGetPagedSortedList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SortOrderGroupAdminGetPagedSortedList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <choice>
 *         <element name="sortByAdminId" type="{}SortByAdminId"/>
 *         <element name="sortByAdminLastName" type="{}SortByAdminLastName"/>
 *         <element name="sortByAdminFirstName" type="{}SortByAdminFirstName"/>
 *       </choice>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SortOrderGroupAdminGetPagedSortedList", propOrder = {
    "sortByAdminId",
    "sortByAdminLastName",
    "sortByAdminFirstName"
})
public class SortOrderGroupAdminGetPagedSortedList {

    protected SortByAdminId sortByAdminId;
    protected SortByAdminLastName sortByAdminLastName;
    protected SortByAdminFirstName sortByAdminFirstName;

    /**
     * Ruft den Wert der sortByAdminId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByAdminId }
     *     
     */
    public SortByAdminId getSortByAdminId() {
        return sortByAdminId;
    }

    /**
     * Legt den Wert der sortByAdminId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByAdminId }
     *     
     */
    public void setSortByAdminId(SortByAdminId value) {
        this.sortByAdminId = value;
    }

    /**
     * Ruft den Wert der sortByAdminLastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByAdminLastName }
     *     
     */
    public SortByAdminLastName getSortByAdminLastName() {
        return sortByAdminLastName;
    }

    /**
     * Legt den Wert der sortByAdminLastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByAdminLastName }
     *     
     */
    public void setSortByAdminLastName(SortByAdminLastName value) {
        this.sortByAdminLastName = value;
    }

    /**
     * Ruft den Wert der sortByAdminFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByAdminFirstName }
     *     
     */
    public SortByAdminFirstName getSortByAdminFirstName() {
        return sortByAdminFirstName;
    }

    /**
     * Legt den Wert der sortByAdminFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByAdminFirstName }
     *     
     */
    public void setSortByAdminFirstName(SortByAdminFirstName value) {
        this.sortByAdminFirstName = value;
    }

}
