//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the distinctive ringing configuration values for route point.
 *         
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupRoutePointDistinctiveRingingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupRoutePointDistinctiveRingingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="enableDistinctiveRinging" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="distinctiveRingingRingPattern" type="{}RingPattern" minOccurs="0"/>
 *         <element name="distinctiveRingingForceDeliveryRingPattern" type="{}RingPattern" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupRoutePointDistinctiveRingingModifyRequest", propOrder = {
    "serviceUserId",
    "enableDistinctiveRinging",
    "distinctiveRingingRingPattern",
    "distinctiveRingingForceDeliveryRingPattern"
})
public class GroupRoutePointDistinctiveRingingModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected Boolean enableDistinctiveRinging;
    @XmlSchemaType(name = "token")
    protected RingPattern distinctiveRingingRingPattern;
    @XmlSchemaType(name = "token")
    protected RingPattern distinctiveRingingForceDeliveryRingPattern;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der enableDistinctiveRinging-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableDistinctiveRinging() {
        return enableDistinctiveRinging;
    }

    /**
     * Legt den Wert der enableDistinctiveRinging-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableDistinctiveRinging(Boolean value) {
        this.enableDistinctiveRinging = value;
    }

    /**
     * Ruft den Wert der distinctiveRingingRingPattern-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RingPattern }
     *     
     */
    public RingPattern getDistinctiveRingingRingPattern() {
        return distinctiveRingingRingPattern;
    }

    /**
     * Legt den Wert der distinctiveRingingRingPattern-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RingPattern }
     *     
     */
    public void setDistinctiveRingingRingPattern(RingPattern value) {
        this.distinctiveRingingRingPattern = value;
    }

    /**
     * Ruft den Wert der distinctiveRingingForceDeliveryRingPattern-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RingPattern }
     *     
     */
    public RingPattern getDistinctiveRingingForceDeliveryRingPattern() {
        return distinctiveRingingForceDeliveryRingPattern;
    }

    /**
     * Legt den Wert der distinctiveRingingForceDeliveryRingPattern-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RingPattern }
     *     
     */
    public void setDistinctiveRingingForceDeliveryRingPattern(RingPattern value) {
        this.distinctiveRingingForceDeliveryRingPattern = value;
    }

}
