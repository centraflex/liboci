//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemAccessDeviceGetAvailableCustomTagListRequest.
 *         Contains a table of all available custom tags managed by the Device Management System on a per-device profile basis.
 *      
 *         In AS data mode, the column headings are: "Tag Name", "Tag Value", "Tag Level", "Tag Set Name", "Region Name".
 *         
 *         In XS data mode, the column headings are: "Tag Name", "Tag Value", "Tag Level", "Tag Set Name", "Is Encrypted".
 * 
 *         "Tag Level" can take the value: "System Default", "System", "Service Provider", "Group" or "Device Profile".
 *       
 * 
 * <p>Java-Klasse für SystemAccessDeviceGetAvailableCustomTagListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAccessDeviceGetAvailableCustomTagListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="deviceAvailableCustomTagsTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAccessDeviceGetAvailableCustomTagListResponse", propOrder = {
    "deviceAvailableCustomTagsTable"
})
public class SystemAccessDeviceGetAvailableCustomTagListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable deviceAvailableCustomTagsTable;

    /**
     * Ruft den Wert der deviceAvailableCustomTagsTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDeviceAvailableCustomTagsTable() {
        return deviceAvailableCustomTagsTable;
    }

    /**
     * Legt den Wert der deviceAvailableCustomTagsTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDeviceAvailableCustomTagsTable(OCITable value) {
        this.deviceAvailableCustomTagsTable = value;
    }

}
