//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal send message to selected distribution list menu keys.
 *       
 * 
 * <p>Java-Klasse für SendMessageToSelectedDistributionListMenuKeysReadEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SendMessageToSelectedDistributionListMenuKeysReadEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="confirmSendingToDistributionList" type="{}DigitStarPound" minOccurs="0"/>
 *         <element name="cancelSendingToDistributionList" type="{}DigitStarPound"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendMessageToSelectedDistributionListMenuKeysReadEntry", propOrder = {
    "confirmSendingToDistributionList",
    "cancelSendingToDistributionList"
})
public class SendMessageToSelectedDistributionListMenuKeysReadEntry {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String confirmSendingToDistributionList;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String cancelSendingToDistributionList;

    /**
     * Ruft den Wert der confirmSendingToDistributionList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmSendingToDistributionList() {
        return confirmSendingToDistributionList;
    }

    /**
     * Legt den Wert der confirmSendingToDistributionList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmSendingToDistributionList(String value) {
        this.confirmSendingToDistributionList = value;
    }

    /**
     * Ruft den Wert der cancelSendingToDistributionList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancelSendingToDistributionList() {
        return cancelSendingToDistributionList;
    }

    /**
     * Legt den Wert der cancelSendingToDistributionList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancelSendingToDistributionList(String value) {
        this.cancelSendingToDistributionList = value;
    }

}
