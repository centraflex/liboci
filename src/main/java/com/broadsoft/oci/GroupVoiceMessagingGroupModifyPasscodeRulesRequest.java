//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the group's voice portal passcode rules setting.
 *         The response is either SuccessResponse or ErrorResponse.
 *         Replaced By: GroupPortalPasscodeRulesModifyRequest
 *       
 * 
 * <p>Java-Klasse für GroupVoiceMessagingGroupModifyPasscodeRulesRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupVoiceMessagingGroupModifyPasscodeRulesRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="useRuleLevel" type="{}GroupPasscodeRulesLevel" minOccurs="0"/>
 *         <element name="disallowRepeatedDigits" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disallowUserNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disallowReversedUserNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disallowOldPasscode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disallowReversedOldPasscode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="minCodeLength" type="{}VoiceMessagingMinPasscodeCodeLength" minOccurs="0"/>
 *         <element name="maxCodeLength" type="{}VoiceMessagingMaxPasscodeCodeLength" minOccurs="0"/>
 *         <element name="disableLoginAfterMaxFailedLoginAttempts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxFailedLoginAttempts" type="{}VoiceMessagingMaxFailedLoginAttempts" minOccurs="0"/>
 *         <element name="expirePassword" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="passcodeExpiresDays" type="{}VoiceMessagingPasscodeExpiresDays" minOccurs="0"/>
 *         <element name="sendLoginDisabledNotifyEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="loginDisabledNotifyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupVoiceMessagingGroupModifyPasscodeRulesRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "useRuleLevel",
    "disallowRepeatedDigits",
    "disallowUserNumber",
    "disallowReversedUserNumber",
    "disallowOldPasscode",
    "disallowReversedOldPasscode",
    "minCodeLength",
    "maxCodeLength",
    "disableLoginAfterMaxFailedLoginAttempts",
    "maxFailedLoginAttempts",
    "expirePassword",
    "passcodeExpiresDays",
    "sendLoginDisabledNotifyEmail",
    "loginDisabledNotifyEmailAddress"
})
public class GroupVoiceMessagingGroupModifyPasscodeRulesRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlSchemaType(name = "token")
    protected GroupPasscodeRulesLevel useRuleLevel;
    protected Boolean disallowRepeatedDigits;
    protected Boolean disallowUserNumber;
    protected Boolean disallowReversedUserNumber;
    protected Boolean disallowOldPasscode;
    protected Boolean disallowReversedOldPasscode;
    protected Integer minCodeLength;
    protected Integer maxCodeLength;
    protected Boolean disableLoginAfterMaxFailedLoginAttempts;
    protected Integer maxFailedLoginAttempts;
    protected Boolean expirePassword;
    protected Integer passcodeExpiresDays;
    protected Boolean sendLoginDisabledNotifyEmail;
    @XmlElementRef(name = "loginDisabledNotifyEmailAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> loginDisabledNotifyEmailAddress;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der useRuleLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupPasscodeRulesLevel }
     *     
     */
    public GroupPasscodeRulesLevel getUseRuleLevel() {
        return useRuleLevel;
    }

    /**
     * Legt den Wert der useRuleLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupPasscodeRulesLevel }
     *     
     */
    public void setUseRuleLevel(GroupPasscodeRulesLevel value) {
        this.useRuleLevel = value;
    }

    /**
     * Ruft den Wert der disallowRepeatedDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowRepeatedDigits() {
        return disallowRepeatedDigits;
    }

    /**
     * Legt den Wert der disallowRepeatedDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowRepeatedDigits(Boolean value) {
        this.disallowRepeatedDigits = value;
    }

    /**
     * Ruft den Wert der disallowUserNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowUserNumber() {
        return disallowUserNumber;
    }

    /**
     * Legt den Wert der disallowUserNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowUserNumber(Boolean value) {
        this.disallowUserNumber = value;
    }

    /**
     * Ruft den Wert der disallowReversedUserNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowReversedUserNumber() {
        return disallowReversedUserNumber;
    }

    /**
     * Legt den Wert der disallowReversedUserNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowReversedUserNumber(Boolean value) {
        this.disallowReversedUserNumber = value;
    }

    /**
     * Ruft den Wert der disallowOldPasscode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowOldPasscode() {
        return disallowOldPasscode;
    }

    /**
     * Legt den Wert der disallowOldPasscode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowOldPasscode(Boolean value) {
        this.disallowOldPasscode = value;
    }

    /**
     * Ruft den Wert der disallowReversedOldPasscode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisallowReversedOldPasscode() {
        return disallowReversedOldPasscode;
    }

    /**
     * Legt den Wert der disallowReversedOldPasscode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisallowReversedOldPasscode(Boolean value) {
        this.disallowReversedOldPasscode = value;
    }

    /**
     * Ruft den Wert der minCodeLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinCodeLength() {
        return minCodeLength;
    }

    /**
     * Legt den Wert der minCodeLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinCodeLength(Integer value) {
        this.minCodeLength = value;
    }

    /**
     * Ruft den Wert der maxCodeLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCodeLength() {
        return maxCodeLength;
    }

    /**
     * Legt den Wert der maxCodeLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCodeLength(Integer value) {
        this.maxCodeLength = value;
    }

    /**
     * Ruft den Wert der disableLoginAfterMaxFailedLoginAttempts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisableLoginAfterMaxFailedLoginAttempts() {
        return disableLoginAfterMaxFailedLoginAttempts;
    }

    /**
     * Legt den Wert der disableLoginAfterMaxFailedLoginAttempts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisableLoginAfterMaxFailedLoginAttempts(Boolean value) {
        this.disableLoginAfterMaxFailedLoginAttempts = value;
    }

    /**
     * Ruft den Wert der maxFailedLoginAttempts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxFailedLoginAttempts() {
        return maxFailedLoginAttempts;
    }

    /**
     * Legt den Wert der maxFailedLoginAttempts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxFailedLoginAttempts(Integer value) {
        this.maxFailedLoginAttempts = value;
    }

    /**
     * Ruft den Wert der expirePassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExpirePassword() {
        return expirePassword;
    }

    /**
     * Legt den Wert der expirePassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExpirePassword(Boolean value) {
        this.expirePassword = value;
    }

    /**
     * Ruft den Wert der passcodeExpiresDays-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPasscodeExpiresDays() {
        return passcodeExpiresDays;
    }

    /**
     * Legt den Wert der passcodeExpiresDays-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPasscodeExpiresDays(Integer value) {
        this.passcodeExpiresDays = value;
    }

    /**
     * Ruft den Wert der sendLoginDisabledNotifyEmail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendLoginDisabledNotifyEmail() {
        return sendLoginDisabledNotifyEmail;
    }

    /**
     * Legt den Wert der sendLoginDisabledNotifyEmail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendLoginDisabledNotifyEmail(Boolean value) {
        this.sendLoginDisabledNotifyEmail = value;
    }

    /**
     * Ruft den Wert der loginDisabledNotifyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLoginDisabledNotifyEmailAddress() {
        return loginDisabledNotifyEmailAddress;
    }

    /**
     * Legt den Wert der loginDisabledNotifyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLoginDisabledNotifyEmailAddress(JAXBElement<String> value) {
        this.loginDisabledNotifyEmailAddress = value;
    }

}
