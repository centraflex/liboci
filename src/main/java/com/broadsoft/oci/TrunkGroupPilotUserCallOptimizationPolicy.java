//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TrunkGroupPilotUserCallOptimizationPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="TrunkGroupPilotUserCallOptimizationPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Optimize For User Services"/>
 *     <enumeration value="Optimize For High Call Volume"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TrunkGroupPilotUserCallOptimizationPolicy")
@XmlEnum
public enum TrunkGroupPilotUserCallOptimizationPolicy {

    @XmlEnumValue("Optimize For User Services")
    OPTIMIZE_FOR_USER_SERVICES("Optimize For User Services"),
    @XmlEnumValue("Optimize For High Call Volume")
    OPTIMIZE_FOR_HIGH_CALL_VOLUME("Optimize For High Call Volume");
    private final String value;

    TrunkGroupPilotUserCallOptimizationPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrunkGroupPilotUserCallOptimizationPolicy fromValue(String v) {
        for (TrunkGroupPilotUserCallOptimizationPolicy c: TrunkGroupPilotUserCallOptimizationPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
