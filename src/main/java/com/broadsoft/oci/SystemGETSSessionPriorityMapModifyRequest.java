//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify a GETS Session Priority Map.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemGETSSessionPriorityMapModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGETSSessionPriorityMapModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="priorityLevel" type="{}GETSPriorityLevel"/>
 *         <element name="sessionPriority" type="{}GETSSessionPriority" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGETSSessionPriorityMapModifyRequest", propOrder = {
    "priorityLevel",
    "sessionPriority"
})
public class SystemGETSSessionPriorityMapModifyRequest
    extends OCIRequest
{

    protected int priorityLevel;
    protected Integer sessionPriority;

    /**
     * Ruft den Wert der priorityLevel-Eigenschaft ab.
     * 
     */
    public int getPriorityLevel() {
        return priorityLevel;
    }

    /**
     * Legt den Wert der priorityLevel-Eigenschaft fest.
     * 
     */
    public void setPriorityLevel(int value) {
        this.priorityLevel = value;
    }

    /**
     * Ruft den Wert der sessionPriority-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSessionPriority() {
        return sessionPriority;
    }

    /**
     * Legt den Wert der sessionPriority-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSessionPriority(Integer value) {
        this.sessionPriority = value;
    }

}
