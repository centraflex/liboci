//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the ServiceProviderVoiceMessagingGroupGetVoicePortalBrandingRequest.
 *         Replaced By: ServiceProviderVoiceMessagingGroupGetVoicePortalBrandingResponse16        
 *       
 * 
 * <p>Java-Klasse für ServiceProviderVoiceMessagingGroupGetVoicePortalBrandingResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderVoiceMessagingGroupGetVoicePortalBrandingResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="voicePortalGreetingSelection" type="{}VoiceMessagingBrandingSelection"/>
 *         <element name="voicePortalGreetingFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="voiceMessagingGreetingSelection" type="{}VoiceMessagingBrandingSelection"/>
 *         <element name="voiceMessagingGreetingFileDescription" type="{}FileDescription" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderVoiceMessagingGroupGetVoicePortalBrandingResponse", propOrder = {
    "voicePortalGreetingSelection",
    "voicePortalGreetingFileDescription",
    "voiceMessagingGreetingSelection",
    "voiceMessagingGreetingFileDescription"
})
public class ServiceProviderVoiceMessagingGroupGetVoicePortalBrandingResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected VoiceMessagingBrandingSelection voicePortalGreetingSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String voicePortalGreetingFileDescription;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected VoiceMessagingBrandingSelection voiceMessagingGreetingSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String voiceMessagingGreetingFileDescription;

    /**
     * Ruft den Wert der voicePortalGreetingSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public VoiceMessagingBrandingSelection getVoicePortalGreetingSelection() {
        return voicePortalGreetingSelection;
    }

    /**
     * Legt den Wert der voicePortalGreetingSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public void setVoicePortalGreetingSelection(VoiceMessagingBrandingSelection value) {
        this.voicePortalGreetingSelection = value;
    }

    /**
     * Ruft den Wert der voicePortalGreetingFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoicePortalGreetingFileDescription() {
        return voicePortalGreetingFileDescription;
    }

    /**
     * Legt den Wert der voicePortalGreetingFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoicePortalGreetingFileDescription(String value) {
        this.voicePortalGreetingFileDescription = value;
    }

    /**
     * Ruft den Wert der voiceMessagingGreetingSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public VoiceMessagingBrandingSelection getVoiceMessagingGreetingSelection() {
        return voiceMessagingGreetingSelection;
    }

    /**
     * Legt den Wert der voiceMessagingGreetingSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public void setVoiceMessagingGreetingSelection(VoiceMessagingBrandingSelection value) {
        this.voiceMessagingGreetingSelection = value;
    }

    /**
     * Ruft den Wert der voiceMessagingGreetingFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoiceMessagingGreetingFileDescription() {
        return voiceMessagingGreetingFileDescription;
    }

    /**
     * Legt den Wert der voiceMessagingGreetingFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoiceMessagingGreetingFileDescription(String value) {
        this.voiceMessagingGreetingFileDescription = value;
    }

}
