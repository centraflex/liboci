//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für MusicOnHoldUserMessageSelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="MusicOnHoldUserMessageSelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Group"/>
 *     <enumeration value="Custom"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "MusicOnHoldUserMessageSelection")
@XmlEnum
public enum MusicOnHoldUserMessageSelection {

    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("Custom")
    CUSTOM("Custom");
    private final String value;

    MusicOnHoldUserMessageSelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MusicOnHoldUserMessageSelection fromValue(String v) {
        for (MusicOnHoldUserMessageSelection c: MusicOnHoldUserMessageSelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
