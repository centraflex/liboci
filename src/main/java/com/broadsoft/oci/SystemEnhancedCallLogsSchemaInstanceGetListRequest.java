//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *       Request the system level database schema instances defined for Enhanced Call Logs. 
 *       For each instance, the number of actual users (users that have the Enhanced Call Logs service 
 *       and are assigned to that schema instance) and the number of potential users are given 
 *       (users part of a group or service provider using that schema instance). 
 *       The response is either a SystemEnhancedCallLogsSchemaInstanceGetListResponse or an ErrorResponse.
 *     
 * 
 * <p>Java-Klasse für SystemEnhancedCallLogsSchemaInstanceGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemEnhancedCallLogsSchemaInstanceGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemEnhancedCallLogsSchemaInstanceGetListRequest")
public class SystemEnhancedCallLogsSchemaInstanceGetListRequest
    extends OCIRequest
{


}
