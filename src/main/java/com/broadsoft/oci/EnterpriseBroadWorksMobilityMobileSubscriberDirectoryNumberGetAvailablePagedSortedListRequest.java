//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request a list of available Mobile Subscriber Directory Numbers not yet assigned to any user.
 *         If the responsePagingControl element is not provided, the paging startIndex will be set to 1 
 *         by default, and the responsePageSize will be set to the maximum ResponsePageSize by default.
 *         If no sortOrder is included the response is sorted by Mobile Number ascending by default.
 *         Multiple search criteria are logically ANDed together unless the searchCriteriaModeOr option is included.
 *         Then the search criteria are logically ORed together.
 *         The response is either EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailablePagedSortedListResponse or ErrorResponse.
 *         Replaced by: EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailablePagedSortedListRequest22
 *       
 * 
 * <p>Java-Klasse für EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailablePagedSortedListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailablePagedSortedListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl" minOccurs="0"/>
 *         <element name="sortByMobileDirectoryNumber" type="{}SortByMobileDirectoryNumber" minOccurs="0"/>
 *         <element name="searchCriteriaMobileSubscriberDirectoryNumber" type="{}SearchCriteriaMobileSubscriberDirectoryNumber" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaModeOr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailablePagedSortedListRequest", propOrder = {
    "serviceProviderId",
    "responsePagingControl",
    "sortByMobileDirectoryNumber",
    "searchCriteriaMobileSubscriberDirectoryNumber",
    "searchCriteriaModeOr"
})
public class EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailablePagedSortedListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected ResponsePagingControl responsePagingControl;
    protected SortByMobileDirectoryNumber sortByMobileDirectoryNumber;
    protected List<SearchCriteriaMobileSubscriberDirectoryNumber> searchCriteriaMobileSubscriberDirectoryNumber;
    protected Boolean searchCriteriaModeOr;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Ruft den Wert der sortByMobileDirectoryNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByMobileDirectoryNumber }
     *     
     */
    public SortByMobileDirectoryNumber getSortByMobileDirectoryNumber() {
        return sortByMobileDirectoryNumber;
    }

    /**
     * Legt den Wert der sortByMobileDirectoryNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByMobileDirectoryNumber }
     *     
     */
    public void setSortByMobileDirectoryNumber(SortByMobileDirectoryNumber value) {
        this.sortByMobileDirectoryNumber = value;
    }

    /**
     * Gets the value of the searchCriteriaMobileSubscriberDirectoryNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaMobileSubscriberDirectoryNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaMobileSubscriberDirectoryNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaMobileSubscriberDirectoryNumber }
     * 
     * 
     * @return
     *     The value of the searchCriteriaMobileSubscriberDirectoryNumber property.
     */
    public List<SearchCriteriaMobileSubscriberDirectoryNumber> getSearchCriteriaMobileSubscriberDirectoryNumber() {
        if (searchCriteriaMobileSubscriberDirectoryNumber == null) {
            searchCriteriaMobileSubscriberDirectoryNumber = new ArrayList<>();
        }
        return this.searchCriteriaMobileSubscriberDirectoryNumber;
    }

    /**
     * Ruft den Wert der searchCriteriaModeOr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchCriteriaModeOr() {
        return searchCriteriaModeOr;
    }

    /**
     * Legt den Wert der searchCriteriaModeOr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchCriteriaModeOr(Boolean value) {
        this.searchCriteriaModeOr = value;
    }

}
