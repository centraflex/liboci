//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderGroupPagingTargetsCapacityGetRequest22.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderGroupPagingTargetsCapacityGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderGroupPagingTargetsCapacityGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maximumTargetUsers" type="{}GroupPagingMaxTargetCapacity22"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderGroupPagingTargetsCapacityGetResponse22", propOrder = {
    "maximumTargetUsers"
})
public class ServiceProviderGroupPagingTargetsCapacityGetResponse22
    extends OCIDataResponse
{

    protected int maximumTargetUsers;

    /**
     * Ruft den Wert der maximumTargetUsers-Eigenschaft ab.
     * 
     */
    public int getMaximumTargetUsers() {
        return maximumTargetUsers;
    }

    /**
     * Legt den Wert der maximumTargetUsers-Eigenschaft fest.
     * 
     */
    public void setMaximumTargetUsers(int value) {
        this.maximumTargetUsers = value;
    }

}
