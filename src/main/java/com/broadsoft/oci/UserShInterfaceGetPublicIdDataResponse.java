//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the UserShInterfaceGetPublicIdDataRequest.
 *         The response contains the Sh non-transparent data for the specified Public User Identity.
 *         The data also includes a userId, userType, and endpointType.
 *         
 *         Replaced by: UserShInterfaceGetPublicIdDataResponse21sp1.
 *       
 * 
 * <p>Java-Klasse für UserShInterfaceGetPublicIdDataResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserShInterfaceGetPublicIdDataResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="userType" type="{}UserType"/>
 *         <element name="endpointType" type="{}EndpointType"/>
 *         <element name="SCSCFName" type="{}SIPURI" minOccurs="0"/>
 *         <element name="IMSUserState" type="{}IMSUserState"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserShInterfaceGetPublicIdDataResponse", propOrder = {
    "userId",
    "userType",
    "endpointType",
    "scscfName",
    "imsUserState"
})
public class UserShInterfaceGetPublicIdDataResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected UserType userType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EndpointType endpointType;
    @XmlElement(name = "SCSCFName")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String scscfName;
    @XmlElement(name = "IMSUserState", required = true)
    @XmlSchemaType(name = "token")
    protected IMSUserState imsUserState;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der userType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserType }
     *     
     */
    public UserType getUserType() {
        return userType;
    }

    /**
     * Legt den Wert der userType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserType }
     *     
     */
    public void setUserType(UserType value) {
        this.userType = value;
    }

    /**
     * Ruft den Wert der endpointType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EndpointType }
     *     
     */
    public EndpointType getEndpointType() {
        return endpointType;
    }

    /**
     * Legt den Wert der endpointType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EndpointType }
     *     
     */
    public void setEndpointType(EndpointType value) {
        this.endpointType = value;
    }

    /**
     * Ruft den Wert der scscfName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSCSCFName() {
        return scscfName;
    }

    /**
     * Legt den Wert der scscfName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSCSCFName(String value) {
        this.scscfName = value;
    }

    /**
     * Ruft den Wert der imsUserState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IMSUserState }
     *     
     */
    public IMSUserState getIMSUserState() {
        return imsUserState;
    }

    /**
     * Legt den Wert der imsUserState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IMSUserState }
     *     
     */
    public void setIMSUserState(IMSUserState value) {
        this.imsUserState = value;
    }

}
