//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DtmfTransmissionSignalingContentType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DtmfTransmissionSignalingContentType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="application/dtmf"/>
 *     <enumeration value="application/dtmf-relay"/>
 *     <enumeration value="audio/telephone-event"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DtmfTransmissionSignalingContentType")
@XmlEnum
public enum DtmfTransmissionSignalingContentType {

    @XmlEnumValue("application/dtmf")
    APPLICATION_DTMF("application/dtmf"),
    @XmlEnumValue("application/dtmf-relay")
    APPLICATION_DTMF_RELAY("application/dtmf-relay"),
    @XmlEnumValue("audio/telephone-event")
    AUDIO_TELEPHONE_EVENT("audio/telephone-event");
    private final String value;

    DtmfTransmissionSignalingContentType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DtmfTransmissionSignalingContentType fromValue(String v) {
        for (DtmfTransmissionSignalingContentType c: DtmfTransmissionSignalingContentType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
