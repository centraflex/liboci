//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *          Associate an access device instance to the user's Shared Call Appearance.
 *          The response is either a SuccessResponse or an ErrorResponse.
 *          Replaced by: UserSharedCallAppearanceAddEndpointRequest21 in XS data mode   
 *          Replaced by: UserSharedCallAppearanceAddEndpointRequest22 in AS data mode   
 *        
 * 
 * <p>Java-Klasse für UserSharedCallAppearanceAddEndpointRequest14sp2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserSharedCallAppearanceAddEndpointRequest14sp2">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointAdd"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowOrigination" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowTermination" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserSharedCallAppearanceAddEndpointRequest14sp2", propOrder = {
    "userId",
    "accessDeviceEndpoint",
    "isActive",
    "allowOrigination",
    "allowTermination"
})
public class UserSharedCallAppearanceAddEndpointRequest14Sp2
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    protected AccessDeviceEndpointAdd accessDeviceEndpoint;
    protected boolean isActive;
    protected boolean allowOrigination;
    protected boolean allowTermination;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEndpointAdd }
     *     
     */
    public AccessDeviceEndpointAdd getAccessDeviceEndpoint() {
        return accessDeviceEndpoint;
    }

    /**
     * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEndpointAdd }
     *     
     */
    public void setAccessDeviceEndpoint(AccessDeviceEndpointAdd value) {
        this.accessDeviceEndpoint = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der allowOrigination-Eigenschaft ab.
     * 
     */
    public boolean isAllowOrigination() {
        return allowOrigination;
    }

    /**
     * Legt den Wert der allowOrigination-Eigenschaft fest.
     * 
     */
    public void setAllowOrigination(boolean value) {
        this.allowOrigination = value;
    }

    /**
     * Ruft den Wert der allowTermination-Eigenschaft ab.
     * 
     */
    public boolean isAllowTermination() {
        return allowTermination;
    }

    /**
     * Legt den Wert der allowTermination-Eigenschaft fest.
     * 
     */
    public void setAllowTermination(boolean value) {
        this.allowTermination = value;
    }

}
