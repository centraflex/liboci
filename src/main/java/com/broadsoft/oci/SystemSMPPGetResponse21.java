//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemSMPPGetRequest21.
 *         
 *         The following elements are only used in AS data mode:
 *           primarySMPPServerNetAddress, value "" is returned in Amplify data mode
 *           primarySMPPPort, value "2775" is returned in Amplify data mode
 *           secondarySMPPServerNetAddress, value "" is returned in Amplify mode
 *           secondarySMPPPort, value "2775" is returned in Amplify data mode
 *           systemId, value "" is returned in Amplify data mode
 *           password, value "" is returned in Amplify mode
 *           version, value "" is returned in Amplify data mode
 *           systemType, value "VMS" is returned in Amplify mode
 *           useGsmMwiUcs2Encoding, value "true" is returned in Amplify mode
 *       
 * 
 * <p>Java-Klasse für SystemSMPPGetResponse21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSMPPGetResponse21">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="primarySMPPServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="primarySMPPPort" type="{}Port"/>
 *         <element name="secondarySMPPServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="secondarySMPPPort" type="{}Port"/>
 *         <element name="systemId" type="{}SMPPSystemId" minOccurs="0"/>
 *         <element name="password" type="{}SMPPPassword" minOccurs="0"/>
 *         <element name="version" type="{}SMPPVersion"/>
 *         <element name="systemType" type="{}SMPPSystemType" minOccurs="0"/>
 *         <element name="enableMWICustomizedMessage" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="supportMessagePayload" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxShortMessageLength" type="{}SMPPMaxShortMessageLength"/>
 *         <element name="useGsmMwiUcs2Encoding" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSMPPGetResponse21", propOrder = {
    "primarySMPPServerNetAddress",
    "primarySMPPPort",
    "secondarySMPPServerNetAddress",
    "secondarySMPPPort",
    "systemId",
    "password",
    "version",
    "systemType",
    "enableMWICustomizedMessage",
    "supportMessagePayload",
    "maxShortMessageLength",
    "useGsmMwiUcs2Encoding"
})
public class SystemSMPPGetResponse21
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String primarySMPPServerNetAddress;
    protected int primarySMPPPort;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String secondarySMPPServerNetAddress;
    protected int secondarySMPPPort;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String systemId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String password;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String version;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String systemType;
    protected boolean enableMWICustomizedMessage;
    protected boolean supportMessagePayload;
    protected int maxShortMessageLength;
    protected boolean useGsmMwiUcs2Encoding;

    /**
     * Ruft den Wert der primarySMPPServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimarySMPPServerNetAddress() {
        return primarySMPPServerNetAddress;
    }

    /**
     * Legt den Wert der primarySMPPServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimarySMPPServerNetAddress(String value) {
        this.primarySMPPServerNetAddress = value;
    }

    /**
     * Ruft den Wert der primarySMPPPort-Eigenschaft ab.
     * 
     */
    public int getPrimarySMPPPort() {
        return primarySMPPPort;
    }

    /**
     * Legt den Wert der primarySMPPPort-Eigenschaft fest.
     * 
     */
    public void setPrimarySMPPPort(int value) {
        this.primarySMPPPort = value;
    }

    /**
     * Ruft den Wert der secondarySMPPServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondarySMPPServerNetAddress() {
        return secondarySMPPServerNetAddress;
    }

    /**
     * Legt den Wert der secondarySMPPServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondarySMPPServerNetAddress(String value) {
        this.secondarySMPPServerNetAddress = value;
    }

    /**
     * Ruft den Wert der secondarySMPPPort-Eigenschaft ab.
     * 
     */
    public int getSecondarySMPPPort() {
        return secondarySMPPPort;
    }

    /**
     * Legt den Wert der secondarySMPPPort-Eigenschaft fest.
     * 
     */
    public void setSecondarySMPPPort(int value) {
        this.secondarySMPPPort = value;
    }

    /**
     * Ruft den Wert der systemId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemId() {
        return systemId;
    }

    /**
     * Legt den Wert der systemId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemId(String value) {
        this.systemId = value;
    }

    /**
     * Ruft den Wert der password-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Legt den Wert der password-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Ruft den Wert der version-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Legt den Wert der version-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Ruft den Wert der systemType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemType() {
        return systemType;
    }

    /**
     * Legt den Wert der systemType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemType(String value) {
        this.systemType = value;
    }

    /**
     * Ruft den Wert der enableMWICustomizedMessage-Eigenschaft ab.
     * 
     */
    public boolean isEnableMWICustomizedMessage() {
        return enableMWICustomizedMessage;
    }

    /**
     * Legt den Wert der enableMWICustomizedMessage-Eigenschaft fest.
     * 
     */
    public void setEnableMWICustomizedMessage(boolean value) {
        this.enableMWICustomizedMessage = value;
    }

    /**
     * Ruft den Wert der supportMessagePayload-Eigenschaft ab.
     * 
     */
    public boolean isSupportMessagePayload() {
        return supportMessagePayload;
    }

    /**
     * Legt den Wert der supportMessagePayload-Eigenschaft fest.
     * 
     */
    public void setSupportMessagePayload(boolean value) {
        this.supportMessagePayload = value;
    }

    /**
     * Ruft den Wert der maxShortMessageLength-Eigenschaft ab.
     * 
     */
    public int getMaxShortMessageLength() {
        return maxShortMessageLength;
    }

    /**
     * Legt den Wert der maxShortMessageLength-Eigenschaft fest.
     * 
     */
    public void setMaxShortMessageLength(int value) {
        this.maxShortMessageLength = value;
    }

    /**
     * Ruft den Wert der useGsmMwiUcs2Encoding-Eigenschaft ab.
     * 
     */
    public boolean isUseGsmMwiUcs2Encoding() {
        return useGsmMwiUcs2Encoding;
    }

    /**
     * Legt den Wert der useGsmMwiUcs2Encoding-Eigenschaft fest.
     * 
     */
    public void setUseGsmMwiUcs2Encoding(boolean value) {
        this.useGsmMwiUcs2Encoding = value;
    }

}
