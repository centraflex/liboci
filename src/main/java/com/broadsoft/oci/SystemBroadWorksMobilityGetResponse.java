//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The response to a SystemBroadWorksMobilityGetRequest.
 *         
 *         Replaced by:  SystemBroadWorksMobilityGetResponse17sp4.
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobilityGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobilityGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableLocationServices" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableMSRNLookup" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableMobileStateChecking" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="denyCallOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="denyCallTerminations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="imrnTimeoutMillisecnds" type="{}IMRNTimeoutMilliseconds"/>
 *         <element name="scfSignalingIPAddress" type="{}IPAddress" minOccurs="0"/>
 *         <element name="scfSignalingPort" type="{}Port" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobilityGetResponse", propOrder = {
    "enableLocationServices",
    "enableMSRNLookup",
    "enableMobileStateChecking",
    "denyCallOriginations",
    "denyCallTerminations",
    "imrnTimeoutMillisecnds",
    "scfSignalingIPAddress",
    "scfSignalingPort"
})
public class SystemBroadWorksMobilityGetResponse
    extends OCIDataResponse
{

    protected boolean enableLocationServices;
    protected boolean enableMSRNLookup;
    protected boolean enableMobileStateChecking;
    protected boolean denyCallOriginations;
    protected boolean denyCallTerminations;
    protected int imrnTimeoutMillisecnds;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String scfSignalingIPAddress;
    protected Integer scfSignalingPort;

    /**
     * Ruft den Wert der enableLocationServices-Eigenschaft ab.
     * 
     */
    public boolean isEnableLocationServices() {
        return enableLocationServices;
    }

    /**
     * Legt den Wert der enableLocationServices-Eigenschaft fest.
     * 
     */
    public void setEnableLocationServices(boolean value) {
        this.enableLocationServices = value;
    }

    /**
     * Ruft den Wert der enableMSRNLookup-Eigenschaft ab.
     * 
     */
    public boolean isEnableMSRNLookup() {
        return enableMSRNLookup;
    }

    /**
     * Legt den Wert der enableMSRNLookup-Eigenschaft fest.
     * 
     */
    public void setEnableMSRNLookup(boolean value) {
        this.enableMSRNLookup = value;
    }

    /**
     * Ruft den Wert der enableMobileStateChecking-Eigenschaft ab.
     * 
     */
    public boolean isEnableMobileStateChecking() {
        return enableMobileStateChecking;
    }

    /**
     * Legt den Wert der enableMobileStateChecking-Eigenschaft fest.
     * 
     */
    public void setEnableMobileStateChecking(boolean value) {
        this.enableMobileStateChecking = value;
    }

    /**
     * Ruft den Wert der denyCallOriginations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallOriginations() {
        return denyCallOriginations;
    }

    /**
     * Legt den Wert der denyCallOriginations-Eigenschaft fest.
     * 
     */
    public void setDenyCallOriginations(boolean value) {
        this.denyCallOriginations = value;
    }

    /**
     * Ruft den Wert der denyCallTerminations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallTerminations() {
        return denyCallTerminations;
    }

    /**
     * Legt den Wert der denyCallTerminations-Eigenschaft fest.
     * 
     */
    public void setDenyCallTerminations(boolean value) {
        this.denyCallTerminations = value;
    }

    /**
     * Ruft den Wert der imrnTimeoutMillisecnds-Eigenschaft ab.
     * 
     */
    public int getImrnTimeoutMillisecnds() {
        return imrnTimeoutMillisecnds;
    }

    /**
     * Legt den Wert der imrnTimeoutMillisecnds-Eigenschaft fest.
     * 
     */
    public void setImrnTimeoutMillisecnds(int value) {
        this.imrnTimeoutMillisecnds = value;
    }

    /**
     * Ruft den Wert der scfSignalingIPAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScfSignalingIPAddress() {
        return scfSignalingIPAddress;
    }

    /**
     * Legt den Wert der scfSignalingIPAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScfSignalingIPAddress(String value) {
        this.scfSignalingIPAddress = value;
    }

    /**
     * Ruft den Wert der scfSignalingPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getScfSignalingPort() {
        return scfSignalingPort;
    }

    /**
     * Legt den Wert der scfSignalingPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setScfSignalingPort(Integer value) {
        this.scfSignalingPort = value;
    }

}
