//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Delete a user creation task for a trunk group.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupTrunkGroupUserCreationTaskDeleteRequest14sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupTrunkGroupUserCreationTaskDeleteRequest14sp4">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="trunkGroupKey" type="{}TrunkGroupKey"/>
 *         <element name="taskName" type="{}TrunkGroupUserCreationTaskName"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupTrunkGroupUserCreationTaskDeleteRequest14sp4", propOrder = {
    "trunkGroupKey",
    "taskName"
})
public class GroupTrunkGroupUserCreationTaskDeleteRequest14Sp4
    extends OCIRequest
{

    @XmlElement(required = true)
    protected TrunkGroupKey trunkGroupKey;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String taskName;

    /**
     * Ruft den Wert der trunkGroupKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupKey }
     *     
     */
    public TrunkGroupKey getTrunkGroupKey() {
        return trunkGroupKey;
    }

    /**
     * Legt den Wert der trunkGroupKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupKey }
     *     
     */
    public void setTrunkGroupKey(TrunkGroupKey value) {
        this.trunkGroupKey = value;
    }

    /**
     * Ruft den Wert der taskName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * Legt den Wert der taskName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskName(String value) {
        this.taskName = value;
    }

}
