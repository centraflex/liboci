//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemCallWaitingGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemCallWaitingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallWaitingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="playDistinctiveRingback" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallWaitingGetResponse", propOrder = {
    "playDistinctiveRingback"
})
public class SystemCallWaitingGetResponse
    extends OCIDataResponse
{

    protected boolean playDistinctiveRingback;

    /**
     * Ruft den Wert der playDistinctiveRingback-Eigenschaft ab.
     * 
     */
    public boolean isPlayDistinctiveRingback() {
        return playDistinctiveRingback;
    }

    /**
     * Legt den Wert der playDistinctiveRingback-Eigenschaft fest.
     * 
     */
    public void setPlayDistinctiveRingback(boolean value) {
        this.playDistinctiveRingback = value;
    }

}
