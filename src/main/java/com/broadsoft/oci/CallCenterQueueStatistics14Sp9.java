//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.math.BigDecimal;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains Call Center Queue statistics.
 *       
 * 
 * <p>Java-Klasse für CallCenterQueueStatistics14sp9 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterQueueStatistics14sp9">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="numberOfBusyOverflows" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="numberOfCallsAnswered" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="numberOfCallsAbandoned" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="numberOfCallsTransferred" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="numberOfCallsTimedout" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="averageNumberOfAgentsTalking" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         <element name="averageNumberOfAgentsStaffed" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         <element name="averageWaitSeconds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="averageAbandonmentSeconds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterQueueStatistics14sp9", propOrder = {
    "numberOfBusyOverflows",
    "numberOfCallsAnswered",
    "numberOfCallsAbandoned",
    "numberOfCallsTransferred",
    "numberOfCallsTimedout",
    "averageNumberOfAgentsTalking",
    "averageNumberOfAgentsStaffed",
    "averageWaitSeconds",
    "averageAbandonmentSeconds"
})
public class CallCenterQueueStatistics14Sp9 {

    protected int numberOfBusyOverflows;
    protected int numberOfCallsAnswered;
    protected int numberOfCallsAbandoned;
    protected int numberOfCallsTransferred;
    protected int numberOfCallsTimedout;
    @XmlElement(required = true)
    protected BigDecimal averageNumberOfAgentsTalking;
    @XmlElement(required = true)
    protected BigDecimal averageNumberOfAgentsStaffed;
    protected int averageWaitSeconds;
    protected int averageAbandonmentSeconds;

    /**
     * Ruft den Wert der numberOfBusyOverflows-Eigenschaft ab.
     * 
     */
    public int getNumberOfBusyOverflows() {
        return numberOfBusyOverflows;
    }

    /**
     * Legt den Wert der numberOfBusyOverflows-Eigenschaft fest.
     * 
     */
    public void setNumberOfBusyOverflows(int value) {
        this.numberOfBusyOverflows = value;
    }

    /**
     * Ruft den Wert der numberOfCallsAnswered-Eigenschaft ab.
     * 
     */
    public int getNumberOfCallsAnswered() {
        return numberOfCallsAnswered;
    }

    /**
     * Legt den Wert der numberOfCallsAnswered-Eigenschaft fest.
     * 
     */
    public void setNumberOfCallsAnswered(int value) {
        this.numberOfCallsAnswered = value;
    }

    /**
     * Ruft den Wert der numberOfCallsAbandoned-Eigenschaft ab.
     * 
     */
    public int getNumberOfCallsAbandoned() {
        return numberOfCallsAbandoned;
    }

    /**
     * Legt den Wert der numberOfCallsAbandoned-Eigenschaft fest.
     * 
     */
    public void setNumberOfCallsAbandoned(int value) {
        this.numberOfCallsAbandoned = value;
    }

    /**
     * Ruft den Wert der numberOfCallsTransferred-Eigenschaft ab.
     * 
     */
    public int getNumberOfCallsTransferred() {
        return numberOfCallsTransferred;
    }

    /**
     * Legt den Wert der numberOfCallsTransferred-Eigenschaft fest.
     * 
     */
    public void setNumberOfCallsTransferred(int value) {
        this.numberOfCallsTransferred = value;
    }

    /**
     * Ruft den Wert der numberOfCallsTimedout-Eigenschaft ab.
     * 
     */
    public int getNumberOfCallsTimedout() {
        return numberOfCallsTimedout;
    }

    /**
     * Legt den Wert der numberOfCallsTimedout-Eigenschaft fest.
     * 
     */
    public void setNumberOfCallsTimedout(int value) {
        this.numberOfCallsTimedout = value;
    }

    /**
     * Ruft den Wert der averageNumberOfAgentsTalking-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAverageNumberOfAgentsTalking() {
        return averageNumberOfAgentsTalking;
    }

    /**
     * Legt den Wert der averageNumberOfAgentsTalking-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAverageNumberOfAgentsTalking(BigDecimal value) {
        this.averageNumberOfAgentsTalking = value;
    }

    /**
     * Ruft den Wert der averageNumberOfAgentsStaffed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAverageNumberOfAgentsStaffed() {
        return averageNumberOfAgentsStaffed;
    }

    /**
     * Legt den Wert der averageNumberOfAgentsStaffed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAverageNumberOfAgentsStaffed(BigDecimal value) {
        this.averageNumberOfAgentsStaffed = value;
    }

    /**
     * Ruft den Wert der averageWaitSeconds-Eigenschaft ab.
     * 
     */
    public int getAverageWaitSeconds() {
        return averageWaitSeconds;
    }

    /**
     * Legt den Wert der averageWaitSeconds-Eigenschaft fest.
     * 
     */
    public void setAverageWaitSeconds(int value) {
        this.averageWaitSeconds = value;
    }

    /**
     * Ruft den Wert der averageAbandonmentSeconds-Eigenschaft ab.
     * 
     */
    public int getAverageAbandonmentSeconds() {
        return averageAbandonmentSeconds;
    }

    /**
     * Legt den Wert der averageAbandonmentSeconds-Eigenschaft fest.
     * 
     */
    public void setAverageAbandonmentSeconds(int value) {
        this.averageAbandonmentSeconds = value;
    }

}
