//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemMeetMeConferencingGetRequest.
 *         
 *         Replaced by: SystemMeetMeConferencingGetResponse17sp3
 *       
 * 
 * <p>Java-Klasse für SystemMeetMeConferencingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemMeetMeConferencingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="conferenceIdLength" type="{}MeetMeConferencingConferencePassCodeLength"/>
 *         <element name="moderatorPinLength" type="{}MeetMeConferencingConferencePassCodeLength"/>
 *         <element name="enableConferenceEndDateRestriction" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="conferenceEndDateRestrictionMonths" type="{}MeetMeConferencingConferenceEndDateRestrictionMonths"/>
 *         <element name="deleteExpiredConferencesAfterHoldPeriod" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="expiredConferenceHoldPeriodDays" type="{}MeetMeConferencingExpiredConferenceHoldPeriodDays"/>
 *         <element name="recordingWebAppURL" type="{}URL" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemMeetMeConferencingGetResponse", propOrder = {
    "conferenceIdLength",
    "moderatorPinLength",
    "enableConferenceEndDateRestriction",
    "conferenceEndDateRestrictionMonths",
    "deleteExpiredConferencesAfterHoldPeriod",
    "expiredConferenceHoldPeriodDays",
    "recordingWebAppURL"
})
public class SystemMeetMeConferencingGetResponse
    extends OCIDataResponse
{

    protected int conferenceIdLength;
    protected int moderatorPinLength;
    protected boolean enableConferenceEndDateRestriction;
    protected int conferenceEndDateRestrictionMonths;
    protected boolean deleteExpiredConferencesAfterHoldPeriod;
    protected int expiredConferenceHoldPeriodDays;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String recordingWebAppURL;

    /**
     * Ruft den Wert der conferenceIdLength-Eigenschaft ab.
     * 
     */
    public int getConferenceIdLength() {
        return conferenceIdLength;
    }

    /**
     * Legt den Wert der conferenceIdLength-Eigenschaft fest.
     * 
     */
    public void setConferenceIdLength(int value) {
        this.conferenceIdLength = value;
    }

    /**
     * Ruft den Wert der moderatorPinLength-Eigenschaft ab.
     * 
     */
    public int getModeratorPinLength() {
        return moderatorPinLength;
    }

    /**
     * Legt den Wert der moderatorPinLength-Eigenschaft fest.
     * 
     */
    public void setModeratorPinLength(int value) {
        this.moderatorPinLength = value;
    }

    /**
     * Ruft den Wert der enableConferenceEndDateRestriction-Eigenschaft ab.
     * 
     */
    public boolean isEnableConferenceEndDateRestriction() {
        return enableConferenceEndDateRestriction;
    }

    /**
     * Legt den Wert der enableConferenceEndDateRestriction-Eigenschaft fest.
     * 
     */
    public void setEnableConferenceEndDateRestriction(boolean value) {
        this.enableConferenceEndDateRestriction = value;
    }

    /**
     * Ruft den Wert der conferenceEndDateRestrictionMonths-Eigenschaft ab.
     * 
     */
    public int getConferenceEndDateRestrictionMonths() {
        return conferenceEndDateRestrictionMonths;
    }

    /**
     * Legt den Wert der conferenceEndDateRestrictionMonths-Eigenschaft fest.
     * 
     */
    public void setConferenceEndDateRestrictionMonths(int value) {
        this.conferenceEndDateRestrictionMonths = value;
    }

    /**
     * Ruft den Wert der deleteExpiredConferencesAfterHoldPeriod-Eigenschaft ab.
     * 
     */
    public boolean isDeleteExpiredConferencesAfterHoldPeriod() {
        return deleteExpiredConferencesAfterHoldPeriod;
    }

    /**
     * Legt den Wert der deleteExpiredConferencesAfterHoldPeriod-Eigenschaft fest.
     * 
     */
    public void setDeleteExpiredConferencesAfterHoldPeriod(boolean value) {
        this.deleteExpiredConferencesAfterHoldPeriod = value;
    }

    /**
     * Ruft den Wert der expiredConferenceHoldPeriodDays-Eigenschaft ab.
     * 
     */
    public int getExpiredConferenceHoldPeriodDays() {
        return expiredConferenceHoldPeriodDays;
    }

    /**
     * Legt den Wert der expiredConferenceHoldPeriodDays-Eigenschaft fest.
     * 
     */
    public void setExpiredConferenceHoldPeriodDays(int value) {
        this.expiredConferenceHoldPeriodDays = value;
    }

    /**
     * Ruft den Wert der recordingWebAppURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordingWebAppURL() {
        return recordingWebAppURL;
    }

    /**
     * Legt den Wert der recordingWebAppURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordingWebAppURL(String value) {
        this.recordingWebAppURL = value;
    }

}
