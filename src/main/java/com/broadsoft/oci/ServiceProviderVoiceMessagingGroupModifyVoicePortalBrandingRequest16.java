//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the service provider's voice portal branding settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderVoiceMessagingGroupModifyVoicePortalBrandingRequest16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderVoiceMessagingGroupModifyVoicePortalBrandingRequest16">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="voicePortalGreetingSelection" type="{}VoiceMessagingBrandingSelection" minOccurs="0"/>
 *         <element name="voicePortalGreetingFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *         <element name="voiceMessagingGreetingSelection" type="{}VoiceMessagingBrandingSelection" minOccurs="0"/>
 *         <element name="voiceMessagingGreetingFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderVoiceMessagingGroupModifyVoicePortalBrandingRequest16", propOrder = {
    "serviceProviderId",
    "voicePortalGreetingSelection",
    "voicePortalGreetingFile",
    "voiceMessagingGreetingSelection",
    "voiceMessagingGreetingFile"
})
public class ServiceProviderVoiceMessagingGroupModifyVoicePortalBrandingRequest16
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingBrandingSelection voicePortalGreetingSelection;
    protected LabeledMediaFileResource voicePortalGreetingFile;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingBrandingSelection voiceMessagingGreetingSelection;
    protected LabeledMediaFileResource voiceMessagingGreetingFile;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der voicePortalGreetingSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public VoiceMessagingBrandingSelection getVoicePortalGreetingSelection() {
        return voicePortalGreetingSelection;
    }

    /**
     * Legt den Wert der voicePortalGreetingSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public void setVoicePortalGreetingSelection(VoiceMessagingBrandingSelection value) {
        this.voicePortalGreetingSelection = value;
    }

    /**
     * Ruft den Wert der voicePortalGreetingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public LabeledMediaFileResource getVoicePortalGreetingFile() {
        return voicePortalGreetingFile;
    }

    /**
     * Legt den Wert der voicePortalGreetingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public void setVoicePortalGreetingFile(LabeledMediaFileResource value) {
        this.voicePortalGreetingFile = value;
    }

    /**
     * Ruft den Wert der voiceMessagingGreetingSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public VoiceMessagingBrandingSelection getVoiceMessagingGreetingSelection() {
        return voiceMessagingGreetingSelection;
    }

    /**
     * Legt den Wert der voiceMessagingGreetingSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingBrandingSelection }
     *     
     */
    public void setVoiceMessagingGreetingSelection(VoiceMessagingBrandingSelection value) {
        this.voiceMessagingGreetingSelection = value;
    }

    /**
     * Ruft den Wert der voiceMessagingGreetingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public LabeledMediaFileResource getVoiceMessagingGreetingFile() {
        return voiceMessagingGreetingFile;
    }

    /**
     * Legt den Wert der voiceMessagingGreetingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public void setVoiceMessagingGreetingFile(LabeledMediaFileResource value) {
        this.voiceMessagingGreetingFile = value;
    }

}
