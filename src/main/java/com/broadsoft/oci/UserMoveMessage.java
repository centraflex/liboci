//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The message describes the impact made when moving a user from one group to another group within the enterprise. The message could also contain the error condition that prevents the user move.
 *       
 * 
 * <p>Java-Klasse für UserMoveMessage complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserMoveMessage">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="messageCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="summary" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         <element name="summaryEnglish" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserMoveMessage", propOrder = {
    "messageCode",
    "summary",
    "summaryEnglish"
})
public class UserMoveMessage {

    protected int messageCode;
    @XmlElement(required = true)
    protected String summary;
    @XmlElement(required = true)
    protected String summaryEnglish;

    /**
     * Ruft den Wert der messageCode-Eigenschaft ab.
     * 
     */
    public int getMessageCode() {
        return messageCode;
    }

    /**
     * Legt den Wert der messageCode-Eigenschaft fest.
     * 
     */
    public void setMessageCode(int value) {
        this.messageCode = value;
    }

    /**
     * Ruft den Wert der summary-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummary() {
        return summary;
    }

    /**
     * Legt den Wert der summary-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummary(String value) {
        this.summary = value;
    }

    /**
     * Ruft den Wert der summaryEnglish-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummaryEnglish() {
        return summaryEnglish;
    }

    /**
     * Legt den Wert der summaryEnglish-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummaryEnglish(String value) {
        this.summaryEnglish = value;
    }

}
