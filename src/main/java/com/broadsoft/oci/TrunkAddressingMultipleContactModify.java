//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Trunk group endpoint that can have multiple contacts. 
 *         alternateTrunkIdentityDomain is only used in XS mode and the AS when deployed in IMS mode. 
 *         Setting alternateTrunkIdentity or alternateTrunkIdentityDomain to nil in XS mode, the other one paremter should be set to nil at the same time.
 *         The following elements are only used in AS data mode and are ignored in XS data mode:        
 *          physicalLocation
 *       
 * 
 * <p>Java-Klasse für TrunkAddressingMultipleContactModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="TrunkAddressingMultipleContactModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="trunkGroupDeviceEndpoint" type="{}TrunkGroupDeviceMultipleContactEndpointModify" minOccurs="0"/>
 *         <element name="enterpriseTrunkName" type="{}EnterpriseTrunkName" minOccurs="0"/>
 *         <element name="alternateTrunkIdentity" type="{}AlternateTrunkIdentity" minOccurs="0"/>
 *         <element name="alternateTrunkIdentityDomain" type="{}DomainName" minOccurs="0"/>
 *         <element name="physicalLocation" type="{}AccessDevicePhysicalLocation" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrunkAddressingMultipleContactModify", propOrder = {
    "trunkGroupDeviceEndpoint",
    "enterpriseTrunkName",
    "alternateTrunkIdentity",
    "alternateTrunkIdentityDomain",
    "physicalLocation"
})
public class TrunkAddressingMultipleContactModify {

    @XmlElementRef(name = "trunkGroupDeviceEndpoint", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupDeviceMultipleContactEndpointModify> trunkGroupDeviceEndpoint;
    @XmlElementRef(name = "enterpriseTrunkName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> enterpriseTrunkName;
    @XmlElementRef(name = "alternateTrunkIdentity", type = JAXBElement.class, required = false)
    protected JAXBElement<String> alternateTrunkIdentity;
    @XmlElementRef(name = "alternateTrunkIdentityDomain", type = JAXBElement.class, required = false)
    protected JAXBElement<String> alternateTrunkIdentityDomain;
    @XmlElementRef(name = "physicalLocation", type = JAXBElement.class, required = false)
    protected JAXBElement<String> physicalLocation;

    /**
     * Ruft den Wert der trunkGroupDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupDeviceMultipleContactEndpointModify }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupDeviceMultipleContactEndpointModify> getTrunkGroupDeviceEndpoint() {
        return trunkGroupDeviceEndpoint;
    }

    /**
     * Legt den Wert der trunkGroupDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupDeviceMultipleContactEndpointModify }{@code >}
     *     
     */
    public void setTrunkGroupDeviceEndpoint(JAXBElement<TrunkGroupDeviceMultipleContactEndpointModify> value) {
        this.trunkGroupDeviceEndpoint = value;
    }

    /**
     * Ruft den Wert der enterpriseTrunkName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEnterpriseTrunkName() {
        return enterpriseTrunkName;
    }

    /**
     * Legt den Wert der enterpriseTrunkName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEnterpriseTrunkName(JAXBElement<String> value) {
        this.enterpriseTrunkName = value;
    }

    /**
     * Ruft den Wert der alternateTrunkIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAlternateTrunkIdentity() {
        return alternateTrunkIdentity;
    }

    /**
     * Legt den Wert der alternateTrunkIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAlternateTrunkIdentity(JAXBElement<String> value) {
        this.alternateTrunkIdentity = value;
    }

    /**
     * Ruft den Wert der alternateTrunkIdentityDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAlternateTrunkIdentityDomain() {
        return alternateTrunkIdentityDomain;
    }

    /**
     * Legt den Wert der alternateTrunkIdentityDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAlternateTrunkIdentityDomain(JAXBElement<String> value) {
        this.alternateTrunkIdentityDomain = value;
    }

    /**
     * Ruft den Wert der physicalLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPhysicalLocation() {
        return physicalLocation;
    }

    /**
     * Legt den Wert der physicalLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPhysicalLocation(JAXBElement<String> value) {
        this.physicalLocation = value;
    }

}
