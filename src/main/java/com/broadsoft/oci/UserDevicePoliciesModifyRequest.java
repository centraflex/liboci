//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *           Modify the user level data associated with Device Policy. enableDeviceFeatureSynchronization and enableCallDecline can be 
 *           configured by the admin regardless of lineMode, but is ignored by the application server in Multiple User Shared mode. 
 *           
 *           enableCallDecline can be modified by the user when the admin has set the mode to ‘Single User Private and Shared Lines mode’. 
 *           This is the only element that the user can modify. In XS data mode,  the lineMode is ignored and enabledCallDecline is the only element that can be configured.
 *           
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           lineMode
 *           enableDeviceFeatureSynchronization
 *           enableDnd
 *           enableCallForwardingAlways
 *           enableCallForwardingBusy
 *           enableCallForwardingNoAnswer
 *           enableAcd
 *           enableExecutive
 *           enableExecutiveAssistant
 *           enableSecurityClassification
 *           enableCallRecording         
 *           
 *           The response is either a SuccessResponse or an ErrorResponse. 
 *         
 * 
 * <p>Java-Klasse für UserDevicePoliciesModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserDevicePoliciesModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="lineMode" type="{}UserDevicePolicyLineMode" minOccurs="0"/>
 *         <element name="enableDeviceFeatureSynchronization" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableDnd" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableCallForwardingAlways" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableCallForwardingBusy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableCallForwardingNoAnswer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableAcd" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableExecutive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableExecutiveAssistant" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableSecurityClassification" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableCallRecording" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableCallDecline" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDevicePoliciesModifyRequest", propOrder = {
    "userId",
    "lineMode",
    "enableDeviceFeatureSynchronization",
    "enableDnd",
    "enableCallForwardingAlways",
    "enableCallForwardingBusy",
    "enableCallForwardingNoAnswer",
    "enableAcd",
    "enableExecutive",
    "enableExecutiveAssistant",
    "enableSecurityClassification",
    "enableCallRecording",
    "enableCallDecline"
})
public class UserDevicePoliciesModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlSchemaType(name = "token")
    protected UserDevicePolicyLineMode lineMode;
    protected Boolean enableDeviceFeatureSynchronization;
    protected Boolean enableDnd;
    protected Boolean enableCallForwardingAlways;
    protected Boolean enableCallForwardingBusy;
    protected Boolean enableCallForwardingNoAnswer;
    protected Boolean enableAcd;
    protected Boolean enableExecutive;
    protected Boolean enableExecutiveAssistant;
    protected Boolean enableSecurityClassification;
    protected Boolean enableCallRecording;
    protected Boolean enableCallDecline;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der lineMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserDevicePolicyLineMode }
     *     
     */
    public UserDevicePolicyLineMode getLineMode() {
        return lineMode;
    }

    /**
     * Legt den Wert der lineMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDevicePolicyLineMode }
     *     
     */
    public void setLineMode(UserDevicePolicyLineMode value) {
        this.lineMode = value;
    }

    /**
     * Ruft den Wert der enableDeviceFeatureSynchronization-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableDeviceFeatureSynchronization() {
        return enableDeviceFeatureSynchronization;
    }

    /**
     * Legt den Wert der enableDeviceFeatureSynchronization-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableDeviceFeatureSynchronization(Boolean value) {
        this.enableDeviceFeatureSynchronization = value;
    }

    /**
     * Ruft den Wert der enableDnd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableDnd() {
        return enableDnd;
    }

    /**
     * Legt den Wert der enableDnd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableDnd(Boolean value) {
        this.enableDnd = value;
    }

    /**
     * Ruft den Wert der enableCallForwardingAlways-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCallForwardingAlways() {
        return enableCallForwardingAlways;
    }

    /**
     * Legt den Wert der enableCallForwardingAlways-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCallForwardingAlways(Boolean value) {
        this.enableCallForwardingAlways = value;
    }

    /**
     * Ruft den Wert der enableCallForwardingBusy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCallForwardingBusy() {
        return enableCallForwardingBusy;
    }

    /**
     * Legt den Wert der enableCallForwardingBusy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCallForwardingBusy(Boolean value) {
        this.enableCallForwardingBusy = value;
    }

    /**
     * Ruft den Wert der enableCallForwardingNoAnswer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCallForwardingNoAnswer() {
        return enableCallForwardingNoAnswer;
    }

    /**
     * Legt den Wert der enableCallForwardingNoAnswer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCallForwardingNoAnswer(Boolean value) {
        this.enableCallForwardingNoAnswer = value;
    }

    /**
     * Ruft den Wert der enableAcd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableAcd() {
        return enableAcd;
    }

    /**
     * Legt den Wert der enableAcd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableAcd(Boolean value) {
        this.enableAcd = value;
    }

    /**
     * Ruft den Wert der enableExecutive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableExecutive() {
        return enableExecutive;
    }

    /**
     * Legt den Wert der enableExecutive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableExecutive(Boolean value) {
        this.enableExecutive = value;
    }

    /**
     * Ruft den Wert der enableExecutiveAssistant-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableExecutiveAssistant() {
        return enableExecutiveAssistant;
    }

    /**
     * Legt den Wert der enableExecutiveAssistant-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableExecutiveAssistant(Boolean value) {
        this.enableExecutiveAssistant = value;
    }

    /**
     * Ruft den Wert der enableSecurityClassification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableSecurityClassification() {
        return enableSecurityClassification;
    }

    /**
     * Legt den Wert der enableSecurityClassification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSecurityClassification(Boolean value) {
        this.enableSecurityClassification = value;
    }

    /**
     * Ruft den Wert der enableCallRecording-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCallRecording() {
        return enableCallRecording;
    }

    /**
     * Legt den Wert der enableCallRecording-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCallRecording(Boolean value) {
        this.enableCallRecording = value;
    }

    /**
     * Ruft den Wert der enableCallDecline-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCallDecline() {
        return enableCallDecline;
    }

    /**
     * Legt den Wert der enableCallDecline-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCallDecline(Boolean value) {
        this.enableCallDecline = value;
    }

}
