//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains Call Center Agent statistics for one day.
 *       
 * 
 * <p>Java-Klasse für CallCenterAgentDailyStatistics13mp8 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterAgentDailyStatistics13mp8">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="numberOfCallsReceived" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="numberOfCallsNotAnswered" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="averageTimePerCallSeconds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="timeInCallsSeconds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="timeLoggedOffSeconds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="timeLoggedOnAndIdleSeconds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterAgentDailyStatistics13mp8", propOrder = {
    "numberOfCallsReceived",
    "numberOfCallsNotAnswered",
    "averageTimePerCallSeconds",
    "timeInCallsSeconds",
    "timeLoggedOffSeconds",
    "timeLoggedOnAndIdleSeconds"
})
public class CallCenterAgentDailyStatistics13Mp8 {

    protected int numberOfCallsReceived;
    protected int numberOfCallsNotAnswered;
    protected int averageTimePerCallSeconds;
    protected int timeInCallsSeconds;
    protected int timeLoggedOffSeconds;
    protected int timeLoggedOnAndIdleSeconds;

    /**
     * Ruft den Wert der numberOfCallsReceived-Eigenschaft ab.
     * 
     */
    public int getNumberOfCallsReceived() {
        return numberOfCallsReceived;
    }

    /**
     * Legt den Wert der numberOfCallsReceived-Eigenschaft fest.
     * 
     */
    public void setNumberOfCallsReceived(int value) {
        this.numberOfCallsReceived = value;
    }

    /**
     * Ruft den Wert der numberOfCallsNotAnswered-Eigenschaft ab.
     * 
     */
    public int getNumberOfCallsNotAnswered() {
        return numberOfCallsNotAnswered;
    }

    /**
     * Legt den Wert der numberOfCallsNotAnswered-Eigenschaft fest.
     * 
     */
    public void setNumberOfCallsNotAnswered(int value) {
        this.numberOfCallsNotAnswered = value;
    }

    /**
     * Ruft den Wert der averageTimePerCallSeconds-Eigenschaft ab.
     * 
     */
    public int getAverageTimePerCallSeconds() {
        return averageTimePerCallSeconds;
    }

    /**
     * Legt den Wert der averageTimePerCallSeconds-Eigenschaft fest.
     * 
     */
    public void setAverageTimePerCallSeconds(int value) {
        this.averageTimePerCallSeconds = value;
    }

    /**
     * Ruft den Wert der timeInCallsSeconds-Eigenschaft ab.
     * 
     */
    public int getTimeInCallsSeconds() {
        return timeInCallsSeconds;
    }

    /**
     * Legt den Wert der timeInCallsSeconds-Eigenschaft fest.
     * 
     */
    public void setTimeInCallsSeconds(int value) {
        this.timeInCallsSeconds = value;
    }

    /**
     * Ruft den Wert der timeLoggedOffSeconds-Eigenschaft ab.
     * 
     */
    public int getTimeLoggedOffSeconds() {
        return timeLoggedOffSeconds;
    }

    /**
     * Legt den Wert der timeLoggedOffSeconds-Eigenschaft fest.
     * 
     */
    public void setTimeLoggedOffSeconds(int value) {
        this.timeLoggedOffSeconds = value;
    }

    /**
     * Ruft den Wert der timeLoggedOnAndIdleSeconds-Eigenschaft ab.
     * 
     */
    public int getTimeLoggedOnAndIdleSeconds() {
        return timeLoggedOnAndIdleSeconds;
    }

    /**
     * Legt den Wert der timeLoggedOnAndIdleSeconds-Eigenschaft fest.
     * 
     */
    public void setTimeLoggedOnAndIdleSeconds(int value) {
        this.timeLoggedOnAndIdleSeconds = value;
    }

}
