//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the EnterpriseUserCallWaitingSettingsGetListRequest.
 *         Contains a table with column headings: "Group Id", "User Id", "Last Name", "First Name", 
 *         "Hiragana Last Name", and "Hiragana First Name", "Phone Number", "Extension", "Department", 
 *         "In Trunk Group", "Email Address", "Is Active".
 *         "Is Active" is "true" or "false".
 *         "Phone Number" is presented in the E164 format.
 *       
 * 
 * <p>Java-Klasse für EnterpriseUserCallWaitingSettingsGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseUserCallWaitingSettingsGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="userCallWaitingTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseUserCallWaitingSettingsGetListResponse", propOrder = {
    "userCallWaitingTable"
})
public class EnterpriseUserCallWaitingSettingsGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable userCallWaitingTable;

    /**
     * Ruft den Wert der userCallWaitingTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserCallWaitingTable() {
        return userCallWaitingTable;
    }

    /**
     * Legt den Wert der userCallWaitingTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserCallWaitingTable(OCITable value) {
        this.userCallWaitingTable = value;
    }

}
