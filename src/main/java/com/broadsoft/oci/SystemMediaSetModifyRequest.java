//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a Media Set.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemMediaSetModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemMediaSetModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="setName" type="{}MediaSetName"/>
 *         <element name="mediaNameList" type="{}ReplacementMediaNameList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemMediaSetModifyRequest", propOrder = {
    "setName",
    "mediaNameList"
})
public class SystemMediaSetModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String setName;
    protected ReplacementMediaNameList mediaNameList;

    /**
     * Ruft den Wert der setName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetName() {
        return setName;
    }

    /**
     * Legt den Wert der setName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetName(String value) {
        this.setName = value;
    }

    /**
     * Ruft den Wert der mediaNameList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ReplacementMediaNameList }
     *     
     */
    public ReplacementMediaNameList getMediaNameList() {
        return mediaNameList;
    }

    /**
     * Legt den Wert der mediaNameList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplacementMediaNameList }
     *     
     */
    public void setMediaNameList(ReplacementMediaNameList value) {
        this.mediaNameList = value;
    }

}
