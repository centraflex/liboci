//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the PasswordGenerateRequest.
 *         The response contains the requested passwords. 
 *       
 * 
 * <p>Java-Klasse für PasswordGenerateResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="PasswordGenerateResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="systemAdministratorPassword" type="{}Password" minOccurs="0"/>
 *         <element name="serviceProviderAdministratorPassword" type="{}Password" minOccurs="0"/>
 *         <element name="groupAdministratorPassword" type="{}Password" minOccurs="0"/>
 *         <element name="userPassword" type="{}Password" minOccurs="0"/>
 *         <element name="userPasscode" type="{}Passcode" minOccurs="0"/>
 *         <element name="userSIPAuthenticationPassword" type="{}Password" minOccurs="0"/>
 *         <element name="accessDeviceAuthenticationPassword" type="{}Password" minOccurs="0"/>
 *         <element name="trunkGroupAuthenticationPassword" type="{}Password" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PasswordGenerateResponse", propOrder = {
    "systemAdministratorPassword",
    "serviceProviderAdministratorPassword",
    "groupAdministratorPassword",
    "userPassword",
    "userPasscode",
    "userSIPAuthenticationPassword",
    "accessDeviceAuthenticationPassword",
    "trunkGroupAuthenticationPassword"
})
public class PasswordGenerateResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String systemAdministratorPassword;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderAdministratorPassword;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupAdministratorPassword;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userPassword;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userPasscode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userSIPAuthenticationPassword;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accessDeviceAuthenticationPassword;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String trunkGroupAuthenticationPassword;

    /**
     * Ruft den Wert der systemAdministratorPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemAdministratorPassword() {
        return systemAdministratorPassword;
    }

    /**
     * Legt den Wert der systemAdministratorPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemAdministratorPassword(String value) {
        this.systemAdministratorPassword = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdministratorPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderAdministratorPassword() {
        return serviceProviderAdministratorPassword;
    }

    /**
     * Legt den Wert der serviceProviderAdministratorPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderAdministratorPassword(String value) {
        this.serviceProviderAdministratorPassword = value;
    }

    /**
     * Ruft den Wert der groupAdministratorPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupAdministratorPassword() {
        return groupAdministratorPassword;
    }

    /**
     * Legt den Wert der groupAdministratorPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupAdministratorPassword(String value) {
        this.groupAdministratorPassword = value;
    }

    /**
     * Ruft den Wert der userPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * Legt den Wert der userPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserPassword(String value) {
        this.userPassword = value;
    }

    /**
     * Ruft den Wert der userPasscode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserPasscode() {
        return userPasscode;
    }

    /**
     * Legt den Wert der userPasscode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserPasscode(String value) {
        this.userPasscode = value;
    }

    /**
     * Ruft den Wert der userSIPAuthenticationPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserSIPAuthenticationPassword() {
        return userSIPAuthenticationPassword;
    }

    /**
     * Legt den Wert der userSIPAuthenticationPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserSIPAuthenticationPassword(String value) {
        this.userSIPAuthenticationPassword = value;
    }

    /**
     * Ruft den Wert der accessDeviceAuthenticationPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessDeviceAuthenticationPassword() {
        return accessDeviceAuthenticationPassword;
    }

    /**
     * Legt den Wert der accessDeviceAuthenticationPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessDeviceAuthenticationPassword(String value) {
        this.accessDeviceAuthenticationPassword = value;
    }

    /**
     * Ruft den Wert der trunkGroupAuthenticationPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrunkGroupAuthenticationPassword() {
        return trunkGroupAuthenticationPassword;
    }

    /**
     * Legt den Wert der trunkGroupAuthenticationPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrunkGroupAuthenticationPassword(String value) {
        this.trunkGroupAuthenticationPassword = value;
    }

}
