//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the UserExecutiveAssistantGetRequest.
 *         Contains the executive assistant setting and a table of executives this assistant has been assigned to.
 *         The criteria table's column headings are: "User Id", "Last Name", "First Name", ", "Hiragana Last Name", 
 *         "Hiragana First Name", "Phone Number", "Extension", "Department", "Email Address", 
 *         "Assistant Opt-in Status" and "Executive Allow Opt-in".
 *         The possible values for "Assistant Opt-in Status" and "Executive Allow Opt-in" columns are "true" and "false".
 *       
 * 
 * <p>Java-Klasse für UserExecutiveAssistantGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserExecutiveAssistantGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableDivert" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="divertToPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="executiveTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserExecutiveAssistantGetResponse", propOrder = {
    "enableDivert",
    "divertToPhoneNumber",
    "executiveTable"
})
public class UserExecutiveAssistantGetResponse
    extends OCIDataResponse
{

    protected boolean enableDivert;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String divertToPhoneNumber;
    @XmlElement(required = true)
    protected OCITable executiveTable;

    /**
     * Ruft den Wert der enableDivert-Eigenschaft ab.
     * 
     */
    public boolean isEnableDivert() {
        return enableDivert;
    }

    /**
     * Legt den Wert der enableDivert-Eigenschaft fest.
     * 
     */
    public void setEnableDivert(boolean value) {
        this.enableDivert = value;
    }

    /**
     * Ruft den Wert der divertToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDivertToPhoneNumber() {
        return divertToPhoneNumber;
    }

    /**
     * Legt den Wert der divertToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDivertToPhoneNumber(String value) {
        this.divertToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der executiveTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getExecutiveTable() {
        return executiveTable;
    }

    /**
     * Legt den Wert der executiveTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setExecutiveTable(OCITable value) {
        this.executiveTable = value;
    }

}
