//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a holiday schedule to a user.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserHolidayScheduleAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserHolidayScheduleAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="holidayScheduleName" type="{}ScheduleName"/>
 *         <element name="holiday01" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday02" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday03" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday04" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday05" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday06" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday07" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday08" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday09" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday10" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday11" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday12" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday13" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday14" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday15" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday16" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday17" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday18" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday19" type="{}Holiday" minOccurs="0"/>
 *         <element name="holiday20" type="{}Holiday" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserHolidayScheduleAddRequest", propOrder = {
    "userId",
    "holidayScheduleName",
    "holiday01",
    "holiday02",
    "holiday03",
    "holiday04",
    "holiday05",
    "holiday06",
    "holiday07",
    "holiday08",
    "holiday09",
    "holiday10",
    "holiday11",
    "holiday12",
    "holiday13",
    "holiday14",
    "holiday15",
    "holiday16",
    "holiday17",
    "holiday18",
    "holiday19",
    "holiday20"
})
public class UserHolidayScheduleAddRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String holidayScheduleName;
    protected Holiday holiday01;
    protected Holiday holiday02;
    protected Holiday holiday03;
    protected Holiday holiday04;
    protected Holiday holiday05;
    protected Holiday holiday06;
    protected Holiday holiday07;
    protected Holiday holiday08;
    protected Holiday holiday09;
    protected Holiday holiday10;
    protected Holiday holiday11;
    protected Holiday holiday12;
    protected Holiday holiday13;
    protected Holiday holiday14;
    protected Holiday holiday15;
    protected Holiday holiday16;
    protected Holiday holiday17;
    protected Holiday holiday18;
    protected Holiday holiday19;
    protected Holiday holiday20;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der holidayScheduleName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHolidayScheduleName() {
        return holidayScheduleName;
    }

    /**
     * Legt den Wert der holidayScheduleName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHolidayScheduleName(String value) {
        this.holidayScheduleName = value;
    }

    /**
     * Ruft den Wert der holiday01-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday01() {
        return holiday01;
    }

    /**
     * Legt den Wert der holiday01-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday01(Holiday value) {
        this.holiday01 = value;
    }

    /**
     * Ruft den Wert der holiday02-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday02() {
        return holiday02;
    }

    /**
     * Legt den Wert der holiday02-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday02(Holiday value) {
        this.holiday02 = value;
    }

    /**
     * Ruft den Wert der holiday03-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday03() {
        return holiday03;
    }

    /**
     * Legt den Wert der holiday03-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday03(Holiday value) {
        this.holiday03 = value;
    }

    /**
     * Ruft den Wert der holiday04-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday04() {
        return holiday04;
    }

    /**
     * Legt den Wert der holiday04-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday04(Holiday value) {
        this.holiday04 = value;
    }

    /**
     * Ruft den Wert der holiday05-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday05() {
        return holiday05;
    }

    /**
     * Legt den Wert der holiday05-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday05(Holiday value) {
        this.holiday05 = value;
    }

    /**
     * Ruft den Wert der holiday06-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday06() {
        return holiday06;
    }

    /**
     * Legt den Wert der holiday06-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday06(Holiday value) {
        this.holiday06 = value;
    }

    /**
     * Ruft den Wert der holiday07-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday07() {
        return holiday07;
    }

    /**
     * Legt den Wert der holiday07-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday07(Holiday value) {
        this.holiday07 = value;
    }

    /**
     * Ruft den Wert der holiday08-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday08() {
        return holiday08;
    }

    /**
     * Legt den Wert der holiday08-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday08(Holiday value) {
        this.holiday08 = value;
    }

    /**
     * Ruft den Wert der holiday09-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday09() {
        return holiday09;
    }

    /**
     * Legt den Wert der holiday09-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday09(Holiday value) {
        this.holiday09 = value;
    }

    /**
     * Ruft den Wert der holiday10-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday10() {
        return holiday10;
    }

    /**
     * Legt den Wert der holiday10-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday10(Holiday value) {
        this.holiday10 = value;
    }

    /**
     * Ruft den Wert der holiday11-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday11() {
        return holiday11;
    }

    /**
     * Legt den Wert der holiday11-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday11(Holiday value) {
        this.holiday11 = value;
    }

    /**
     * Ruft den Wert der holiday12-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday12() {
        return holiday12;
    }

    /**
     * Legt den Wert der holiday12-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday12(Holiday value) {
        this.holiday12 = value;
    }

    /**
     * Ruft den Wert der holiday13-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday13() {
        return holiday13;
    }

    /**
     * Legt den Wert der holiday13-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday13(Holiday value) {
        this.holiday13 = value;
    }

    /**
     * Ruft den Wert der holiday14-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday14() {
        return holiday14;
    }

    /**
     * Legt den Wert der holiday14-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday14(Holiday value) {
        this.holiday14 = value;
    }

    /**
     * Ruft den Wert der holiday15-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday15() {
        return holiday15;
    }

    /**
     * Legt den Wert der holiday15-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday15(Holiday value) {
        this.holiday15 = value;
    }

    /**
     * Ruft den Wert der holiday16-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday16() {
        return holiday16;
    }

    /**
     * Legt den Wert der holiday16-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday16(Holiday value) {
        this.holiday16 = value;
    }

    /**
     * Ruft den Wert der holiday17-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday17() {
        return holiday17;
    }

    /**
     * Legt den Wert der holiday17-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday17(Holiday value) {
        this.holiday17 = value;
    }

    /**
     * Ruft den Wert der holiday18-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday18() {
        return holiday18;
    }

    /**
     * Legt den Wert der holiday18-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday18(Holiday value) {
        this.holiday18 = value;
    }

    /**
     * Ruft den Wert der holiday19-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday19() {
        return holiday19;
    }

    /**
     * Legt den Wert der holiday19-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday19(Holiday value) {
        this.holiday19 = value;
    }

    /**
     * Ruft den Wert der holiday20-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Holiday }
     *     
     */
    public Holiday getHoliday20() {
        return holiday20;
    }

    /**
     * Legt den Wert der holiday20-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Holiday }
     *     
     */
    public void setHoliday20(Holiday value) {
        this.holiday20 = value;
    }

}
