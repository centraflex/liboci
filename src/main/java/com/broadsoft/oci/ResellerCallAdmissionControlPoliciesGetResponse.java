//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the ResellerCallAdmissionControlPoliciesGetRequest.
 *         The response contains the reseller call admission control policies information.
 *       
 * 
 * <p>Java-Klasse für ResellerCallAdmissionControlPoliciesGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerCallAdmissionControlPoliciesGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableCallAdmissionControl" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxConcurrentNetworkSessions" type="{}ConcurrentNetworkSessionsLimit"/>
 *         <element name="maxConcurrentNetworkSessionsThreshold" type="{}ConcurrentNetworkSessionsThresholdLimit" minOccurs="0"/>
 *         <element name="maxNetworkCallsPerSecond" type="{}NetworkCallsPerSecondLimit"/>
 *         <element name="maxNetworkCallsPerSecondThreshold" type="{}NetworkCallsPerSecondThresholdLimit" minOccurs="0"/>
 *         <element name="maxConcurrentExternalSIPRECSessions" type="{}ConcurrentExternalSIPRECSessionsLimit"/>
 *         <element name="maxConcurrentExternalSIPRECSessionsThreshold" type="{}ConcurrentExternalSIPRECSessionsThresholdLimit" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerCallAdmissionControlPoliciesGetResponse", propOrder = {
    "enableCallAdmissionControl",
    "maxConcurrentNetworkSessions",
    "maxConcurrentNetworkSessionsThreshold",
    "maxNetworkCallsPerSecond",
    "maxNetworkCallsPerSecondThreshold",
    "maxConcurrentExternalSIPRECSessions",
    "maxConcurrentExternalSIPRECSessionsThreshold"
})
public class ResellerCallAdmissionControlPoliciesGetResponse
    extends OCIDataResponse
{

    protected boolean enableCallAdmissionControl;
    protected int maxConcurrentNetworkSessions;
    protected Integer maxConcurrentNetworkSessionsThreshold;
    protected int maxNetworkCallsPerSecond;
    protected Integer maxNetworkCallsPerSecondThreshold;
    protected int maxConcurrentExternalSIPRECSessions;
    protected Integer maxConcurrentExternalSIPRECSessionsThreshold;

    /**
     * Ruft den Wert der enableCallAdmissionControl-Eigenschaft ab.
     * 
     */
    public boolean isEnableCallAdmissionControl() {
        return enableCallAdmissionControl;
    }

    /**
     * Legt den Wert der enableCallAdmissionControl-Eigenschaft fest.
     * 
     */
    public void setEnableCallAdmissionControl(boolean value) {
        this.enableCallAdmissionControl = value;
    }

    /**
     * Ruft den Wert der maxConcurrentNetworkSessions-Eigenschaft ab.
     * 
     */
    public int getMaxConcurrentNetworkSessions() {
        return maxConcurrentNetworkSessions;
    }

    /**
     * Legt den Wert der maxConcurrentNetworkSessions-Eigenschaft fest.
     * 
     */
    public void setMaxConcurrentNetworkSessions(int value) {
        this.maxConcurrentNetworkSessions = value;
    }

    /**
     * Ruft den Wert der maxConcurrentNetworkSessionsThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxConcurrentNetworkSessionsThreshold() {
        return maxConcurrentNetworkSessionsThreshold;
    }

    /**
     * Legt den Wert der maxConcurrentNetworkSessionsThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxConcurrentNetworkSessionsThreshold(Integer value) {
        this.maxConcurrentNetworkSessionsThreshold = value;
    }

    /**
     * Ruft den Wert der maxNetworkCallsPerSecond-Eigenschaft ab.
     * 
     */
    public int getMaxNetworkCallsPerSecond() {
        return maxNetworkCallsPerSecond;
    }

    /**
     * Legt den Wert der maxNetworkCallsPerSecond-Eigenschaft fest.
     * 
     */
    public void setMaxNetworkCallsPerSecond(int value) {
        this.maxNetworkCallsPerSecond = value;
    }

    /**
     * Ruft den Wert der maxNetworkCallsPerSecondThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxNetworkCallsPerSecondThreshold() {
        return maxNetworkCallsPerSecondThreshold;
    }

    /**
     * Legt den Wert der maxNetworkCallsPerSecondThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxNetworkCallsPerSecondThreshold(Integer value) {
        this.maxNetworkCallsPerSecondThreshold = value;
    }

    /**
     * Ruft den Wert der maxConcurrentExternalSIPRECSessions-Eigenschaft ab.
     * 
     */
    public int getMaxConcurrentExternalSIPRECSessions() {
        return maxConcurrentExternalSIPRECSessions;
    }

    /**
     * Legt den Wert der maxConcurrentExternalSIPRECSessions-Eigenschaft fest.
     * 
     */
    public void setMaxConcurrentExternalSIPRECSessions(int value) {
        this.maxConcurrentExternalSIPRECSessions = value;
    }

    /**
     * Ruft den Wert der maxConcurrentExternalSIPRECSessionsThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxConcurrentExternalSIPRECSessionsThreshold() {
        return maxConcurrentExternalSIPRECSessionsThreshold;
    }

    /**
     * Legt den Wert der maxConcurrentExternalSIPRECSessionsThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxConcurrentExternalSIPRECSessionsThreshold(Integer value) {
        this.maxConcurrentExternalSIPRECSessionsThreshold = value;
    }

}
