//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal play message menu keys.
 *       
 * 
 * <p>Java-Klasse für PlayMessagesMenuKeysReadEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="PlayMessagesMenuKeysReadEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="saveMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="deleteMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="playMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="previousMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
 *         <element name="nextMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="callbackCaller" type="{}DigitAny" minOccurs="0"/>
 *         <element name="composeMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="replyMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="forwardMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="additionalMessageOptions" type="{}DigitAny" minOccurs="0"/>
 *         <element name="personalizedName" type="{}DigitAny" minOccurs="0"/>
 *         <element name="passcode" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlayMessagesMenuKeysReadEntry", propOrder = {
    "saveMessage",
    "deleteMessage",
    "playMessage",
    "previousMessage",
    "playEnvelope",
    "nextMessage",
    "callbackCaller",
    "composeMessage",
    "replyMessage",
    "forwardMessage",
    "additionalMessageOptions",
    "personalizedName",
    "passcode",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class PlayMessagesMenuKeysReadEntry {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String saveMessage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deleteMessage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String playMessage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String previousMessage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String playEnvelope;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String nextMessage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callbackCaller;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String composeMessage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String replyMessage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String forwardMessage;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String additionalMessageOptions;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String personalizedName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String passcode;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String repeatMenu;

    /**
     * Ruft den Wert der saveMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaveMessage() {
        return saveMessage;
    }

    /**
     * Legt den Wert der saveMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaveMessage(String value) {
        this.saveMessage = value;
    }

    /**
     * Ruft den Wert der deleteMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeleteMessage() {
        return deleteMessage;
    }

    /**
     * Legt den Wert der deleteMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeleteMessage(String value) {
        this.deleteMessage = value;
    }

    /**
     * Ruft den Wert der playMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlayMessage() {
        return playMessage;
    }

    /**
     * Legt den Wert der playMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlayMessage(String value) {
        this.playMessage = value;
    }

    /**
     * Ruft den Wert der previousMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousMessage() {
        return previousMessage;
    }

    /**
     * Legt den Wert der previousMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousMessage(String value) {
        this.previousMessage = value;
    }

    /**
     * Ruft den Wert der playEnvelope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlayEnvelope() {
        return playEnvelope;
    }

    /**
     * Legt den Wert der playEnvelope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlayEnvelope(String value) {
        this.playEnvelope = value;
    }

    /**
     * Ruft den Wert der nextMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextMessage() {
        return nextMessage;
    }

    /**
     * Legt den Wert der nextMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextMessage(String value) {
        this.nextMessage = value;
    }

    /**
     * Ruft den Wert der callbackCaller-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallbackCaller() {
        return callbackCaller;
    }

    /**
     * Legt den Wert der callbackCaller-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallbackCaller(String value) {
        this.callbackCaller = value;
    }

    /**
     * Ruft den Wert der composeMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComposeMessage() {
        return composeMessage;
    }

    /**
     * Legt den Wert der composeMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComposeMessage(String value) {
        this.composeMessage = value;
    }

    /**
     * Ruft den Wert der replyMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReplyMessage() {
        return replyMessage;
    }

    /**
     * Legt den Wert der replyMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReplyMessage(String value) {
        this.replyMessage = value;
    }

    /**
     * Ruft den Wert der forwardMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForwardMessage() {
        return forwardMessage;
    }

    /**
     * Legt den Wert der forwardMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForwardMessage(String value) {
        this.forwardMessage = value;
    }

    /**
     * Ruft den Wert der additionalMessageOptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalMessageOptions() {
        return additionalMessageOptions;
    }

    /**
     * Legt den Wert der additionalMessageOptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalMessageOptions(String value) {
        this.additionalMessageOptions = value;
    }

    /**
     * Ruft den Wert der personalizedName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalizedName() {
        return personalizedName;
    }

    /**
     * Legt den Wert der personalizedName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalizedName(String value) {
        this.personalizedName = value;
    }

    /**
     * Ruft den Wert der passcode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasscode() {
        return passcode;
    }

    /**
     * Legt den Wert der passcode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasscode(String value) {
        this.passcode = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepeatMenu(String value) {
        this.repeatMenu = value;
    }

}
