//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the ServiceProviderMWIDeliveryToMobileEndpointCustomTemplateGetRequest.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderMWIDeliveryToMobileEndpointCustomTemplateGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderMWIDeliveryToMobileEndpointCustomTemplateGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="templateBody" type="{}MWIDeliveryToMobileEndpointTemplateBody"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderMWIDeliveryToMobileEndpointCustomTemplateGetResponse", propOrder = {
    "isEnabled",
    "templateBody"
})
public class ServiceProviderMWIDeliveryToMobileEndpointCustomTemplateGetResponse
    extends OCIDataResponse
{

    protected boolean isEnabled;
    @XmlElement(required = true)
    protected MWIDeliveryToMobileEndpointTemplateBody templateBody;

    /**
     * Ruft den Wert der isEnabled-Eigenschaft ab.
     * 
     */
    public boolean isIsEnabled() {
        return isEnabled;
    }

    /**
     * Legt den Wert der isEnabled-Eigenschaft fest.
     * 
     */
    public void setIsEnabled(boolean value) {
        this.isEnabled = value;
    }

    /**
     * Ruft den Wert der templateBody-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MWIDeliveryToMobileEndpointTemplateBody }
     *     
     */
    public MWIDeliveryToMobileEndpointTemplateBody getTemplateBody() {
        return templateBody;
    }

    /**
     * Legt den Wert der templateBody-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MWIDeliveryToMobileEndpointTemplateBody }
     *     
     */
    public void setTemplateBody(MWIDeliveryToMobileEndpointTemplateBody value) {
        this.templateBody = value;
    }

}
