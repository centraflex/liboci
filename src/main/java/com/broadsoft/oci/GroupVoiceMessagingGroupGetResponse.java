//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupVoiceMessagingGroupGetRequest.
 *         Contains the group's voice messaging settings.
 *       
 * 
 * <p>Java-Klasse für GroupVoiceMessagingGroupGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupVoiceMessagingGroupGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useMailServerSetting" type="{}VoiceMessagingGroupMailServerChoices"/>
 *         <element name="warnCallerBeforeRecordingVoiceMessage" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowUsersConfiguringAdvancedSettings" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowComposeOrForwardMessageToEntireGroup" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="mailServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="mailServerProtocol" type="{}VoiceMessagingMailServerProtocol"/>
 *         <element name="realDeleteForImap" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxMailboxLengthMinutes" type="{}VoiceMessagingMailboxLengthMinutes"/>
 *         <element name="doesMessageAge" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="holdPeriodDays" type="{}VoiceMessagingHoldPeriodDays"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupVoiceMessagingGroupGetResponse", propOrder = {
    "useMailServerSetting",
    "warnCallerBeforeRecordingVoiceMessage",
    "allowUsersConfiguringAdvancedSettings",
    "allowComposeOrForwardMessageToEntireGroup",
    "mailServerNetAddress",
    "mailServerProtocol",
    "realDeleteForImap",
    "maxMailboxLengthMinutes",
    "doesMessageAge",
    "holdPeriodDays"
})
public class GroupVoiceMessagingGroupGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected VoiceMessagingGroupMailServerChoices useMailServerSetting;
    protected boolean warnCallerBeforeRecordingVoiceMessage;
    protected boolean allowUsersConfiguringAdvancedSettings;
    protected boolean allowComposeOrForwardMessageToEntireGroup;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mailServerNetAddress;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected VoiceMessagingMailServerProtocol mailServerProtocol;
    protected boolean realDeleteForImap;
    protected int maxMailboxLengthMinutes;
    protected boolean doesMessageAge;
    protected int holdPeriodDays;

    /**
     * Ruft den Wert der useMailServerSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingGroupMailServerChoices }
     *     
     */
    public VoiceMessagingGroupMailServerChoices getUseMailServerSetting() {
        return useMailServerSetting;
    }

    /**
     * Legt den Wert der useMailServerSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingGroupMailServerChoices }
     *     
     */
    public void setUseMailServerSetting(VoiceMessagingGroupMailServerChoices value) {
        this.useMailServerSetting = value;
    }

    /**
     * Ruft den Wert der warnCallerBeforeRecordingVoiceMessage-Eigenschaft ab.
     * 
     */
    public boolean isWarnCallerBeforeRecordingVoiceMessage() {
        return warnCallerBeforeRecordingVoiceMessage;
    }

    /**
     * Legt den Wert der warnCallerBeforeRecordingVoiceMessage-Eigenschaft fest.
     * 
     */
    public void setWarnCallerBeforeRecordingVoiceMessage(boolean value) {
        this.warnCallerBeforeRecordingVoiceMessage = value;
    }

    /**
     * Ruft den Wert der allowUsersConfiguringAdvancedSettings-Eigenschaft ab.
     * 
     */
    public boolean isAllowUsersConfiguringAdvancedSettings() {
        return allowUsersConfiguringAdvancedSettings;
    }

    /**
     * Legt den Wert der allowUsersConfiguringAdvancedSettings-Eigenschaft fest.
     * 
     */
    public void setAllowUsersConfiguringAdvancedSettings(boolean value) {
        this.allowUsersConfiguringAdvancedSettings = value;
    }

    /**
     * Ruft den Wert der allowComposeOrForwardMessageToEntireGroup-Eigenschaft ab.
     * 
     */
    public boolean isAllowComposeOrForwardMessageToEntireGroup() {
        return allowComposeOrForwardMessageToEntireGroup;
    }

    /**
     * Legt den Wert der allowComposeOrForwardMessageToEntireGroup-Eigenschaft fest.
     * 
     */
    public void setAllowComposeOrForwardMessageToEntireGroup(boolean value) {
        this.allowComposeOrForwardMessageToEntireGroup = value;
    }

    /**
     * Ruft den Wert der mailServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailServerNetAddress() {
        return mailServerNetAddress;
    }

    /**
     * Legt den Wert der mailServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailServerNetAddress(String value) {
        this.mailServerNetAddress = value;
    }

    /**
     * Ruft den Wert der mailServerProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingMailServerProtocol }
     *     
     */
    public VoiceMessagingMailServerProtocol getMailServerProtocol() {
        return mailServerProtocol;
    }

    /**
     * Legt den Wert der mailServerProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingMailServerProtocol }
     *     
     */
    public void setMailServerProtocol(VoiceMessagingMailServerProtocol value) {
        this.mailServerProtocol = value;
    }

    /**
     * Ruft den Wert der realDeleteForImap-Eigenschaft ab.
     * 
     */
    public boolean isRealDeleteForImap() {
        return realDeleteForImap;
    }

    /**
     * Legt den Wert der realDeleteForImap-Eigenschaft fest.
     * 
     */
    public void setRealDeleteForImap(boolean value) {
        this.realDeleteForImap = value;
    }

    /**
     * Ruft den Wert der maxMailboxLengthMinutes-Eigenschaft ab.
     * 
     */
    public int getMaxMailboxLengthMinutes() {
        return maxMailboxLengthMinutes;
    }

    /**
     * Legt den Wert der maxMailboxLengthMinutes-Eigenschaft fest.
     * 
     */
    public void setMaxMailboxLengthMinutes(int value) {
        this.maxMailboxLengthMinutes = value;
    }

    /**
     * Ruft den Wert der doesMessageAge-Eigenschaft ab.
     * 
     */
    public boolean isDoesMessageAge() {
        return doesMessageAge;
    }

    /**
     * Legt den Wert der doesMessageAge-Eigenschaft fest.
     * 
     */
    public void setDoesMessageAge(boolean value) {
        this.doesMessageAge = value;
    }

    /**
     * Ruft den Wert der holdPeriodDays-Eigenschaft ab.
     * 
     */
    public int getHoldPeriodDays() {
        return holdPeriodDays;
    }

    /**
     * Legt den Wert der holdPeriodDays-Eigenschaft fest.
     * 
     */
    public void setHoldPeriodDays(int value) {
        this.holdPeriodDays = value;
    }

}
