//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify Client Session (web and CLI) system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemClientSessionParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemClientSessionParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enableInactivityTimeout" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="inactivityTimeoutMinutes" type="{}ClientSessionTimeoutMinutes" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemClientSessionParametersModifyRequest", propOrder = {
    "enableInactivityTimeout",
    "inactivityTimeoutMinutes"
})
public class SystemClientSessionParametersModifyRequest
    extends OCIRequest
{

    protected Boolean enableInactivityTimeout;
    protected Integer inactivityTimeoutMinutes;

    /**
     * Ruft den Wert der enableInactivityTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableInactivityTimeout() {
        return enableInactivityTimeout;
    }

    /**
     * Legt den Wert der enableInactivityTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableInactivityTimeout(Boolean value) {
        this.enableInactivityTimeout = value;
    }

    /**
     * Ruft den Wert der inactivityTimeoutMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInactivityTimeoutMinutes() {
        return inactivityTimeoutMinutes;
    }

    /**
     * Legt den Wert der inactivityTimeoutMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInactivityTimeoutMinutes(Integer value) {
        this.inactivityTimeoutMinutes = value;
    }

}
