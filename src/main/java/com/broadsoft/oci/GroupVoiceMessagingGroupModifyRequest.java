//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the group's voice messaging settings.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupVoiceMessagingGroupModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupVoiceMessagingGroupModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="useMailServerSetting" type="{}VoiceMessagingGroupMailServerChoices" minOccurs="0"/>
 *         <element name="warnCallerBeforeRecordingVoiceMessage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowUsersConfiguringAdvancedSettings" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowComposeOrForwardMessageToEntireGroup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="mailServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="mailServerProtocol" type="{}VoiceMessagingMailServerProtocol" minOccurs="0"/>
 *         <element name="realDeleteForImap" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxMailboxLengthMinutes" type="{}VoiceMessagingMailboxLengthMinutes" minOccurs="0"/>
 *         <element name="doesMessageAge" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="holdPeriodDays" type="{}VoiceMessagingHoldPeriodDays" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupVoiceMessagingGroupModifyRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "useMailServerSetting",
    "warnCallerBeforeRecordingVoiceMessage",
    "allowUsersConfiguringAdvancedSettings",
    "allowComposeOrForwardMessageToEntireGroup",
    "mailServerNetAddress",
    "mailServerProtocol",
    "realDeleteForImap",
    "maxMailboxLengthMinutes",
    "doesMessageAge",
    "holdPeriodDays"
})
public class GroupVoiceMessagingGroupModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingGroupMailServerChoices useMailServerSetting;
    protected Boolean warnCallerBeforeRecordingVoiceMessage;
    protected Boolean allowUsersConfiguringAdvancedSettings;
    protected Boolean allowComposeOrForwardMessageToEntireGroup;
    @XmlElementRef(name = "mailServerNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mailServerNetAddress;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingMailServerProtocol mailServerProtocol;
    protected Boolean realDeleteForImap;
    protected Integer maxMailboxLengthMinutes;
    protected Boolean doesMessageAge;
    protected Integer holdPeriodDays;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der useMailServerSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingGroupMailServerChoices }
     *     
     */
    public VoiceMessagingGroupMailServerChoices getUseMailServerSetting() {
        return useMailServerSetting;
    }

    /**
     * Legt den Wert der useMailServerSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingGroupMailServerChoices }
     *     
     */
    public void setUseMailServerSetting(VoiceMessagingGroupMailServerChoices value) {
        this.useMailServerSetting = value;
    }

    /**
     * Ruft den Wert der warnCallerBeforeRecordingVoiceMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWarnCallerBeforeRecordingVoiceMessage() {
        return warnCallerBeforeRecordingVoiceMessage;
    }

    /**
     * Legt den Wert der warnCallerBeforeRecordingVoiceMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWarnCallerBeforeRecordingVoiceMessage(Boolean value) {
        this.warnCallerBeforeRecordingVoiceMessage = value;
    }

    /**
     * Ruft den Wert der allowUsersConfiguringAdvancedSettings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowUsersConfiguringAdvancedSettings() {
        return allowUsersConfiguringAdvancedSettings;
    }

    /**
     * Legt den Wert der allowUsersConfiguringAdvancedSettings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowUsersConfiguringAdvancedSettings(Boolean value) {
        this.allowUsersConfiguringAdvancedSettings = value;
    }

    /**
     * Ruft den Wert der allowComposeOrForwardMessageToEntireGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowComposeOrForwardMessageToEntireGroup() {
        return allowComposeOrForwardMessageToEntireGroup;
    }

    /**
     * Legt den Wert der allowComposeOrForwardMessageToEntireGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowComposeOrForwardMessageToEntireGroup(Boolean value) {
        this.allowComposeOrForwardMessageToEntireGroup = value;
    }

    /**
     * Ruft den Wert der mailServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMailServerNetAddress() {
        return mailServerNetAddress;
    }

    /**
     * Legt den Wert der mailServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMailServerNetAddress(JAXBElement<String> value) {
        this.mailServerNetAddress = value;
    }

    /**
     * Ruft den Wert der mailServerProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingMailServerProtocol }
     *     
     */
    public VoiceMessagingMailServerProtocol getMailServerProtocol() {
        return mailServerProtocol;
    }

    /**
     * Legt den Wert der mailServerProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingMailServerProtocol }
     *     
     */
    public void setMailServerProtocol(VoiceMessagingMailServerProtocol value) {
        this.mailServerProtocol = value;
    }

    /**
     * Ruft den Wert der realDeleteForImap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRealDeleteForImap() {
        return realDeleteForImap;
    }

    /**
     * Legt den Wert der realDeleteForImap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRealDeleteForImap(Boolean value) {
        this.realDeleteForImap = value;
    }

    /**
     * Ruft den Wert der maxMailboxLengthMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxMailboxLengthMinutes() {
        return maxMailboxLengthMinutes;
    }

    /**
     * Legt den Wert der maxMailboxLengthMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxMailboxLengthMinutes(Integer value) {
        this.maxMailboxLengthMinutes = value;
    }

    /**
     * Ruft den Wert der doesMessageAge-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDoesMessageAge() {
        return doesMessageAge;
    }

    /**
     * Legt den Wert der doesMessageAge-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDoesMessageAge(Boolean value) {
        this.doesMessageAge = value;
    }

    /**
     * Ruft den Wert der holdPeriodDays-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHoldPeriodDays() {
        return holdPeriodDays;
    }

    /**
     * Legt den Wert der holdPeriodDays-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHoldPeriodDays(Integer value) {
        this.holdPeriodDays = value;
    }

}
