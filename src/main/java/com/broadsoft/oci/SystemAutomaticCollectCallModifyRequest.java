//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify system Automatic Collect Call service settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemAutomaticCollectCallModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAutomaticCollectCallModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enableAutomaticCollectCall" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableConnectTone" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeCountryCodeInCic" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAutomaticCollectCallModifyRequest", propOrder = {
    "enableAutomaticCollectCall",
    "enableConnectTone",
    "includeCountryCodeInCic"
})
public class SystemAutomaticCollectCallModifyRequest
    extends OCIRequest
{

    protected Boolean enableAutomaticCollectCall;
    protected Boolean enableConnectTone;
    protected Boolean includeCountryCodeInCic;

    /**
     * Ruft den Wert der enableAutomaticCollectCall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableAutomaticCollectCall() {
        return enableAutomaticCollectCall;
    }

    /**
     * Legt den Wert der enableAutomaticCollectCall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableAutomaticCollectCall(Boolean value) {
        this.enableAutomaticCollectCall = value;
    }

    /**
     * Ruft den Wert der enableConnectTone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableConnectTone() {
        return enableConnectTone;
    }

    /**
     * Legt den Wert der enableConnectTone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableConnectTone(Boolean value) {
        this.enableConnectTone = value;
    }

    /**
     * Ruft den Wert der includeCountryCodeInCic-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeCountryCodeInCic() {
        return includeCountryCodeInCic;
    }

    /**
     * Legt den Wert der includeCountryCodeInCic-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeCountryCodeInCic(Boolean value) {
        this.includeCountryCodeInCic = value;
    }

}
