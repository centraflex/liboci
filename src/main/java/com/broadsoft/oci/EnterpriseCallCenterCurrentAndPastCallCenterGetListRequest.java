//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to get all the current and past call centers for the enterprise.
 *         The searchCriteriaExactUserGroup does not apply to past call centers.
 *         The response is either EnterpriseCallCenterCurrentAndPastCallCenterGetListResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für EnterpriseCallCenterCurrentAndPastCallCenterGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseCallCenterCurrentAndPastCallCenterGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="isPremiumOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaExactUserGroup" type="{}SearchCriteriaExactUserGroup" minOccurs="0"/>
 *         <element name="searchCriteriaCallCenterName" type="{}SearchCriteriaCallCenterName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseCallCenterCurrentAndPastCallCenterGetListRequest", propOrder = {
    "serviceProviderId",
    "isPremiumOnly",
    "responseSizeLimit",
    "searchCriteriaExactUserGroup",
    "searchCriteriaCallCenterName"
})
public class EnterpriseCallCenterCurrentAndPastCallCenterGetListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected Boolean isPremiumOnly;
    protected Integer responseSizeLimit;
    protected SearchCriteriaExactUserGroup searchCriteriaExactUserGroup;
    protected SearchCriteriaCallCenterName searchCriteriaCallCenterName;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der isPremiumOnly-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPremiumOnly() {
        return isPremiumOnly;
    }

    /**
     * Legt den Wert der isPremiumOnly-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPremiumOnly(Boolean value) {
        this.isPremiumOnly = value;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Ruft den Wert der searchCriteriaExactUserGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactUserGroup }
     *     
     */
    public SearchCriteriaExactUserGroup getSearchCriteriaExactUserGroup() {
        return searchCriteriaExactUserGroup;
    }

    /**
     * Legt den Wert der searchCriteriaExactUserGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactUserGroup }
     *     
     */
    public void setSearchCriteriaExactUserGroup(SearchCriteriaExactUserGroup value) {
        this.searchCriteriaExactUserGroup = value;
    }

    /**
     * Ruft den Wert der searchCriteriaCallCenterName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaCallCenterName }
     *     
     */
    public SearchCriteriaCallCenterName getSearchCriteriaCallCenterName() {
        return searchCriteriaCallCenterName;
    }

    /**
     * Legt den Wert der searchCriteriaCallCenterName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaCallCenterName }
     *     
     */
    public void setSearchCriteriaCallCenterName(SearchCriteriaCallCenterName value) {
        this.searchCriteriaCallCenterName = value;
    }

}
