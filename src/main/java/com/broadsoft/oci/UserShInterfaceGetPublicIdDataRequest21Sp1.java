//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Returns the Sh non-transparent data stored against a Public User Identity (a SIP URI
 *         or TEL URI).
 *         The response is either a UserShInterfaceGetPublicIdDataResponse21sp1 or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserShInterfaceGetPublicIdDataRequest21sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserShInterfaceGetPublicIdDataRequest21sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="publicUserIdentity" type="{}PublicUserIdentity"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserShInterfaceGetPublicIdDataRequest21sp1", propOrder = {
    "publicUserIdentity"
})
public class UserShInterfaceGetPublicIdDataRequest21Sp1
    extends OCIRequest
{

    @XmlElement(required = true)
    protected PublicUserIdentity publicUserIdentity;

    /**
     * Ruft den Wert der publicUserIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PublicUserIdentity }
     *     
     */
    public PublicUserIdentity getPublicUserIdentity() {
        return publicUserIdentity;
    }

    /**
     * Legt den Wert der publicUserIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PublicUserIdentity }
     *     
     */
    public void setPublicUserIdentity(PublicUserIdentity value) {
        this.publicUserIdentity = value;
    }

}
