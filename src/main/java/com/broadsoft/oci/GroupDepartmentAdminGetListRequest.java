//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Get a list of department administrators for the department.
 *         The response is either a GroupDepartmentAdminGetListResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupDepartmentAdminGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDepartmentAdminGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="departmentKey" type="{}GroupDepartmentKey"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDepartmentAdminGetListRequest", propOrder = {
    "departmentKey"
})
public class GroupDepartmentAdminGetListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    protected GroupDepartmentKey departmentKey;

    /**
     * Ruft den Wert der departmentKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentKey }
     *     
     */
    public GroupDepartmentKey getDepartmentKey() {
        return departmentKey;
    }

    /**
     * Legt den Wert der departmentKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentKey }
     *     
     */
    public void setDepartmentKey(GroupDepartmentKey value) {
        this.departmentKey = value;
    }

}
