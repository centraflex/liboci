//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system call processing configuration for all subscribers.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         The following elements are only used in XS data mode and ignored in the AS data mode:
 *           minNoAnswerNumberOfRings
 * 
 *         The following elements are only used in AS data mode and ignored in the XS data mode:
 *           isExtendedCallingLineIdActive
 *           isRingTimeOutActive
 *           ringTimeoutSeconds
 *           allowEmergencyRemoteOfficeOriginations
 *           incomingCallToUserAliasMode
 *           bypassTerminationLoopDetection
 *           honorCLIDBlockingForEmergencyCalls
 *           useUnicodeIdentityName
 *       
 * 
 * <p>Java-Klasse für SystemSubscriberModifyCallProcessingParametersRequest14sp7 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSubscriberModifyCallProcessingParametersRequest14sp7">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="isExtendedCallingLineIdActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="isRingTimeOutActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="ringTimeoutSeconds" type="{}SystemUserRingTimeoutSeconds" minOccurs="0"/>
 *         <element name="allowEmergencyRemoteOfficeOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxNoAnswerNumberOfRings" type="{}MaxNoAnswerNumberOfRings" minOccurs="0"/>
 *         <element name="minNoAnswerNumberOfRings" type="{}MinNoAnswerNumberOfRings" minOccurs="0"/>
 *         <element name="incomingCallToUserAliasMode" type="{}IncomingCallToUserAliasMode" minOccurs="0"/>
 *         <element name="bypassTerminationLoopDetection" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="honorCLIDBlockingForEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useUnicodeIdentityName" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSubscriberModifyCallProcessingParametersRequest14sp7", propOrder = {
    "isExtendedCallingLineIdActive",
    "isRingTimeOutActive",
    "ringTimeoutSeconds",
    "allowEmergencyRemoteOfficeOriginations",
    "maxNoAnswerNumberOfRings",
    "minNoAnswerNumberOfRings",
    "incomingCallToUserAliasMode",
    "bypassTerminationLoopDetection",
    "honorCLIDBlockingForEmergencyCalls",
    "useUnicodeIdentityName"
})
public class SystemSubscriberModifyCallProcessingParametersRequest14Sp7
    extends OCIRequest
{

    protected Boolean isExtendedCallingLineIdActive;
    protected Boolean isRingTimeOutActive;
    protected Integer ringTimeoutSeconds;
    protected Boolean allowEmergencyRemoteOfficeOriginations;
    protected Integer maxNoAnswerNumberOfRings;
    protected Integer minNoAnswerNumberOfRings;
    @XmlSchemaType(name = "token")
    protected IncomingCallToUserAliasMode incomingCallToUserAliasMode;
    protected Boolean bypassTerminationLoopDetection;
    protected Boolean honorCLIDBlockingForEmergencyCalls;
    protected Boolean useUnicodeIdentityName;

    /**
     * Ruft den Wert der isExtendedCallingLineIdActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsExtendedCallingLineIdActive() {
        return isExtendedCallingLineIdActive;
    }

    /**
     * Legt den Wert der isExtendedCallingLineIdActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsExtendedCallingLineIdActive(Boolean value) {
        this.isExtendedCallingLineIdActive = value;
    }

    /**
     * Ruft den Wert der isRingTimeOutActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRingTimeOutActive() {
        return isRingTimeOutActive;
    }

    /**
     * Legt den Wert der isRingTimeOutActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRingTimeOutActive(Boolean value) {
        this.isRingTimeOutActive = value;
    }

    /**
     * Ruft den Wert der ringTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRingTimeoutSeconds() {
        return ringTimeoutSeconds;
    }

    /**
     * Legt den Wert der ringTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRingTimeoutSeconds(Integer value) {
        this.ringTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der allowEmergencyRemoteOfficeOriginations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowEmergencyRemoteOfficeOriginations() {
        return allowEmergencyRemoteOfficeOriginations;
    }

    /**
     * Legt den Wert der allowEmergencyRemoteOfficeOriginations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowEmergencyRemoteOfficeOriginations(Boolean value) {
        this.allowEmergencyRemoteOfficeOriginations = value;
    }

    /**
     * Ruft den Wert der maxNoAnswerNumberOfRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxNoAnswerNumberOfRings() {
        return maxNoAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der maxNoAnswerNumberOfRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxNoAnswerNumberOfRings(Integer value) {
        this.maxNoAnswerNumberOfRings = value;
    }

    /**
     * Ruft den Wert der minNoAnswerNumberOfRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinNoAnswerNumberOfRings() {
        return minNoAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der minNoAnswerNumberOfRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinNoAnswerNumberOfRings(Integer value) {
        this.minNoAnswerNumberOfRings = value;
    }

    /**
     * Ruft den Wert der incomingCallToUserAliasMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IncomingCallToUserAliasMode }
     *     
     */
    public IncomingCallToUserAliasMode getIncomingCallToUserAliasMode() {
        return incomingCallToUserAliasMode;
    }

    /**
     * Legt den Wert der incomingCallToUserAliasMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IncomingCallToUserAliasMode }
     *     
     */
    public void setIncomingCallToUserAliasMode(IncomingCallToUserAliasMode value) {
        this.incomingCallToUserAliasMode = value;
    }

    /**
     * Ruft den Wert der bypassTerminationLoopDetection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBypassTerminationLoopDetection() {
        return bypassTerminationLoopDetection;
    }

    /**
     * Legt den Wert der bypassTerminationLoopDetection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBypassTerminationLoopDetection(Boolean value) {
        this.bypassTerminationLoopDetection = value;
    }

    /**
     * Ruft den Wert der honorCLIDBlockingForEmergencyCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHonorCLIDBlockingForEmergencyCalls() {
        return honorCLIDBlockingForEmergencyCalls;
    }

    /**
     * Legt den Wert der honorCLIDBlockingForEmergencyCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHonorCLIDBlockingForEmergencyCalls(Boolean value) {
        this.honorCLIDBlockingForEmergencyCalls = value;
    }

    /**
     * Ruft den Wert der useUnicodeIdentityName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseUnicodeIdentityName() {
        return useUnicodeIdentityName;
    }

    /**
     * Legt den Wert der useUnicodeIdentityName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseUnicodeIdentityName(Boolean value) {
        this.useUnicodeIdentityName = value;
    }

}
