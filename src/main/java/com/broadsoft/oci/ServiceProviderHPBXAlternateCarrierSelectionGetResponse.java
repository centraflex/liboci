//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the ServiceProviderHPBXAlternateCarrierSelectionGetRequest.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderHPBXAlternateCarrierSelectionGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderHPBXAlternateCarrierSelectionGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="processCbcCarrierSelection" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="preselectedLocalCarrier" type="{}HPBXAlternateCarrierName" minOccurs="0"/>
 *         <element name="preselectedDistantCarrier" type="{}HPBXAlternateCarrierName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderHPBXAlternateCarrierSelectionGetResponse", propOrder = {
    "processCbcCarrierSelection",
    "preselectedLocalCarrier",
    "preselectedDistantCarrier"
})
public class ServiceProviderHPBXAlternateCarrierSelectionGetResponse
    extends OCIDataResponse
{

    protected boolean processCbcCarrierSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String preselectedLocalCarrier;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String preselectedDistantCarrier;

    /**
     * Ruft den Wert der processCbcCarrierSelection-Eigenschaft ab.
     * 
     */
    public boolean isProcessCbcCarrierSelection() {
        return processCbcCarrierSelection;
    }

    /**
     * Legt den Wert der processCbcCarrierSelection-Eigenschaft fest.
     * 
     */
    public void setProcessCbcCarrierSelection(boolean value) {
        this.processCbcCarrierSelection = value;
    }

    /**
     * Ruft den Wert der preselectedLocalCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreselectedLocalCarrier() {
        return preselectedLocalCarrier;
    }

    /**
     * Legt den Wert der preselectedLocalCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreselectedLocalCarrier(String value) {
        this.preselectedLocalCarrier = value;
    }

    /**
     * Ruft den Wert der preselectedDistantCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreselectedDistantCarrier() {
        return preselectedDistantCarrier;
    }

    /**
     * Legt den Wert der preselectedDistantCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreselectedDistantCarrier(String value) {
        this.preselectedDistantCarrier = value;
    }

}
