//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserCallPoliciesGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserCallPoliciesGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallPoliciesGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="redirectedCallsCOLPPrivacy" type="{}ConnectedLineIdentificationPrivacyOnRedirectedCalls"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallPoliciesGetResponse", propOrder = {
    "redirectedCallsCOLPPrivacy"
})
public class UserCallPoliciesGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ConnectedLineIdentificationPrivacyOnRedirectedCalls redirectedCallsCOLPPrivacy;

    /**
     * Ruft den Wert der redirectedCallsCOLPPrivacy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ConnectedLineIdentificationPrivacyOnRedirectedCalls }
     *     
     */
    public ConnectedLineIdentificationPrivacyOnRedirectedCalls getRedirectedCallsCOLPPrivacy() {
        return redirectedCallsCOLPPrivacy;
    }

    /**
     * Legt den Wert der redirectedCallsCOLPPrivacy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConnectedLineIdentificationPrivacyOnRedirectedCalls }
     *     
     */
    public void setRedirectedCallsCOLPPrivacy(ConnectedLineIdentificationPrivacyOnRedirectedCalls value) {
        this.redirectedCallsCOLPPrivacy = value;
    }

}
