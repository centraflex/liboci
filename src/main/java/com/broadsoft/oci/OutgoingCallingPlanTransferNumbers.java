//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Outgoing Calling Plan transfer numbers.
 *       
 * 
 * <p>Java-Klasse für OutgoingCallingPlanTransferNumbers complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OutgoingCallingPlanTransferNumbers">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="phoneNumber01" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="phoneNumber02" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="phoneNumber03" type="{}OutgoingDN" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingCallingPlanTransferNumbers", propOrder = {
    "phoneNumber01",
    "phoneNumber02",
    "phoneNumber03"
})
public class OutgoingCallingPlanTransferNumbers {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String phoneNumber01;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String phoneNumber02;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String phoneNumber03;

    /**
     * Ruft den Wert der phoneNumber01-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber01() {
        return phoneNumber01;
    }

    /**
     * Legt den Wert der phoneNumber01-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber01(String value) {
        this.phoneNumber01 = value;
    }

    /**
     * Ruft den Wert der phoneNumber02-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber02() {
        return phoneNumber02;
    }

    /**
     * Legt den Wert der phoneNumber02-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber02(String value) {
        this.phoneNumber02 = value;
    }

    /**
     * Ruft den Wert der phoneNumber03-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber03() {
        return phoneNumber03;
    }

    /**
     * Legt den Wert der phoneNumber03-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber03(String value) {
        this.phoneNumber03 = value;
    }

}
