//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ServiceType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ServiceType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Auto Attendant"/>
 *     <enumeration value="Auto Attendant - Standard"/>
 *     <enumeration value="BroadWorks Anywhere Portal"/>
 *     <enumeration value="Call Center"/>
 *     <enumeration value="Collaborate Bridge"/>
 *     <enumeration value="Find-me/Follow-me"/>
 *     <enumeration value="Flexible Seating Host"/>
 *     <enumeration value="Group Paging"/>
 *     <enumeration value="Hunt Group"/>
 *     <enumeration value="Instant Group Call"/>
 *     <enumeration value="Instant Conference Bridge"/>
 *     <enumeration value="Meet-Me Conference Bridge"/>
 *     <enumeration value="Route Point"/>
 *     <enumeration value="VoiceXML"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ServiceType")
@XmlEnum
public enum ServiceType {

    @XmlEnumValue("Auto Attendant")
    AUTO_ATTENDANT("Auto Attendant"),
    @XmlEnumValue("Auto Attendant - Standard")
    AUTO_ATTENDANT_STANDARD("Auto Attendant - Standard"),
    @XmlEnumValue("BroadWorks Anywhere Portal")
    BROAD_WORKS_ANYWHERE_PORTAL("BroadWorks Anywhere Portal"),
    @XmlEnumValue("Call Center")
    CALL_CENTER("Call Center"),
    @XmlEnumValue("Collaborate Bridge")
    COLLABORATE_BRIDGE("Collaborate Bridge"),
    @XmlEnumValue("Find-me/Follow-me")
    FIND_ME_FOLLOW_ME("Find-me/Follow-me"),
    @XmlEnumValue("Flexible Seating Host")
    FLEXIBLE_SEATING_HOST("Flexible Seating Host"),
    @XmlEnumValue("Group Paging")
    GROUP_PAGING("Group Paging"),
    @XmlEnumValue("Hunt Group")
    HUNT_GROUP("Hunt Group"),
    @XmlEnumValue("Instant Group Call")
    INSTANT_GROUP_CALL("Instant Group Call"),
    @XmlEnumValue("Instant Conference Bridge")
    INSTANT_CONFERENCE_BRIDGE("Instant Conference Bridge"),
    @XmlEnumValue("Meet-Me Conference Bridge")
    MEET_ME_CONFERENCE_BRIDGE("Meet-Me Conference Bridge"),
    @XmlEnumValue("Route Point")
    ROUTE_POINT("Route Point"),
    @XmlEnumValue("VoiceXML")
    VOICE_XML("VoiceXML");
    private final String value;

    ServiceType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ServiceType fromValue(String v) {
        for (ServiceType c: ServiceType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
