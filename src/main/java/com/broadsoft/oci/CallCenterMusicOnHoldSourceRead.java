//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Contains the music on hold source configuration.
 *       
 * 
 * <p>Java-Klasse für CallCenterMusicOnHoldSourceRead complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterMusicOnHoldSourceRead">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="audioFilePreferredCodec" type="{}AudioFileCodec"/>
 *         <element name="messageSourceSelection" type="{}CallCenterAnnouncementSelection"/>
 *         <element name="customSource" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="audioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *                   <element name="videoFileDescription" type="{}FileDescription" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterMusicOnHoldSourceRead", propOrder = {
    "audioFilePreferredCodec",
    "messageSourceSelection",
    "customSource"
})
public class CallCenterMusicOnHoldSourceRead {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AudioFileCodec audioFilePreferredCodec;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterAnnouncementSelection messageSourceSelection;
    protected CallCenterMusicOnHoldSourceRead.CustomSource customSource;

    /**
     * Ruft den Wert der audioFilePreferredCodec-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AudioFileCodec }
     *     
     */
    public AudioFileCodec getAudioFilePreferredCodec() {
        return audioFilePreferredCodec;
    }

    /**
     * Legt den Wert der audioFilePreferredCodec-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AudioFileCodec }
     *     
     */
    public void setAudioFilePreferredCodec(AudioFileCodec value) {
        this.audioFilePreferredCodec = value;
    }

    /**
     * Ruft den Wert der messageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public CallCenterAnnouncementSelection getMessageSourceSelection() {
        return messageSourceSelection;
    }

    /**
     * Legt den Wert der messageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementSelection }
     *     
     */
    public void setMessageSourceSelection(CallCenterAnnouncementSelection value) {
        this.messageSourceSelection = value;
    }

    /**
     * Ruft den Wert der customSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMusicOnHoldSourceRead.CustomSource }
     *     
     */
    public CallCenterMusicOnHoldSourceRead.CustomSource getCustomSource() {
        return customSource;
    }

    /**
     * Legt den Wert der customSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMusicOnHoldSourceRead.CustomSource }
     *     
     */
    public void setCustomSource(CallCenterMusicOnHoldSourceRead.CustomSource value) {
        this.customSource = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="audioFileDescription" type="{}FileDescription" minOccurs="0"/>
     *         <element name="videoFileDescription" type="{}FileDescription" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "audioFileDescription",
        "videoFileDescription"
    })
    public static class CustomSource {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String audioFileDescription;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String videoFileDescription;

        /**
         * Ruft den Wert der audioFileDescription-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAudioFileDescription() {
            return audioFileDescription;
        }

        /**
         * Legt den Wert der audioFileDescription-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAudioFileDescription(String value) {
            this.audioFileDescription = value;
        }

        /**
         * Ruft den Wert der videoFileDescription-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVideoFileDescription() {
            return videoFileDescription;
        }

        /**
         * Legt den Wert der videoFileDescription-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVideoFileDescription(String value) {
            this.videoFileDescription = value;
        }

    }

}
