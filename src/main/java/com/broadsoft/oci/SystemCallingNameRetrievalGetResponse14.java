//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCallingNameRetrievalGetRequest14.
 *       
 * 
 * <p>Java-Klasse für SystemCallingNameRetrievalGetResponse14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallingNameRetrievalGetResponse14">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="queryTimerMilliSeconds" type="{}CallingNameRetrievalQueryTimerMilliSeconds"/>
 *         <element name="serverNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="serverPort" type="{}Port1025" minOccurs="0"/>
 *         <element name="serverTransportProtocol" type="{}TransportProtocol"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallingNameRetrievalGetResponse14", propOrder = {
    "queryTimerMilliSeconds",
    "serverNetAddress",
    "serverPort",
    "serverTransportProtocol"
})
public class SystemCallingNameRetrievalGetResponse14
    extends OCIDataResponse
{

    protected int queryTimerMilliSeconds;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serverNetAddress;
    protected Integer serverPort;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TransportProtocol serverTransportProtocol;

    /**
     * Ruft den Wert der queryTimerMilliSeconds-Eigenschaft ab.
     * 
     */
    public int getQueryTimerMilliSeconds() {
        return queryTimerMilliSeconds;
    }

    /**
     * Legt den Wert der queryTimerMilliSeconds-Eigenschaft fest.
     * 
     */
    public void setQueryTimerMilliSeconds(int value) {
        this.queryTimerMilliSeconds = value;
    }

    /**
     * Ruft den Wert der serverNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServerNetAddress() {
        return serverNetAddress;
    }

    /**
     * Legt den Wert der serverNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServerNetAddress(String value) {
        this.serverNetAddress = value;
    }

    /**
     * Ruft den Wert der serverPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServerPort() {
        return serverPort;
    }

    /**
     * Legt den Wert der serverPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServerPort(Integer value) {
        this.serverPort = value;
    }

    /**
     * Ruft den Wert der serverTransportProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TransportProtocol }
     *     
     */
    public TransportProtocol getServerTransportProtocol() {
        return serverTransportProtocol;
    }

    /**
     * Legt den Wert der serverTransportProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TransportProtocol }
     *     
     */
    public void setServerTransportProtocol(TransportProtocol value) {
        this.serverTransportProtocol = value;
    }

}
