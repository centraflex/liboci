//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with Call Recording.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCallRecordingModifyRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallRecordingModifyRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="continueCallAfterRecordingFailure" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxResponseWaitTimeMilliseconds" type="{}RecordingMaxResponseWaitTimeMilliseconds" minOccurs="0"/>
 *         <element name="continueCallAfterVideoRecordingFailure" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useContinueCallAfterRecordingFailureForOnDemandMode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="restrictCallRecordingProvisioningAccess" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallRecordingModifyRequest22", propOrder = {
    "continueCallAfterRecordingFailure",
    "maxResponseWaitTimeMilliseconds",
    "continueCallAfterVideoRecordingFailure",
    "useContinueCallAfterRecordingFailureForOnDemandMode",
    "useContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode",
    "restrictCallRecordingProvisioningAccess"
})
public class SystemCallRecordingModifyRequest22
    extends OCIRequest
{

    protected Boolean continueCallAfterRecordingFailure;
    protected Integer maxResponseWaitTimeMilliseconds;
    protected Boolean continueCallAfterVideoRecordingFailure;
    protected Boolean useContinueCallAfterRecordingFailureForOnDemandMode;
    protected Boolean useContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode;
    protected Boolean restrictCallRecordingProvisioningAccess;

    /**
     * Ruft den Wert der continueCallAfterRecordingFailure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isContinueCallAfterRecordingFailure() {
        return continueCallAfterRecordingFailure;
    }

    /**
     * Legt den Wert der continueCallAfterRecordingFailure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContinueCallAfterRecordingFailure(Boolean value) {
        this.continueCallAfterRecordingFailure = value;
    }

    /**
     * Ruft den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxResponseWaitTimeMilliseconds() {
        return maxResponseWaitTimeMilliseconds;
    }

    /**
     * Legt den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxResponseWaitTimeMilliseconds(Integer value) {
        this.maxResponseWaitTimeMilliseconds = value;
    }

    /**
     * Ruft den Wert der continueCallAfterVideoRecordingFailure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isContinueCallAfterVideoRecordingFailure() {
        return continueCallAfterVideoRecordingFailure;
    }

    /**
     * Legt den Wert der continueCallAfterVideoRecordingFailure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContinueCallAfterVideoRecordingFailure(Boolean value) {
        this.continueCallAfterVideoRecordingFailure = value;
    }

    /**
     * Ruft den Wert der useContinueCallAfterRecordingFailureForOnDemandMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseContinueCallAfterRecordingFailureForOnDemandMode() {
        return useContinueCallAfterRecordingFailureForOnDemandMode;
    }

    /**
     * Legt den Wert der useContinueCallAfterRecordingFailureForOnDemandMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseContinueCallAfterRecordingFailureForOnDemandMode(Boolean value) {
        this.useContinueCallAfterRecordingFailureForOnDemandMode = value;
    }

    /**
     * Ruft den Wert der useContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode() {
        return useContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode;
    }

    /**
     * Legt den Wert der useContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode(Boolean value) {
        this.useContinueCallAfterRecordingFailureForOnDemandUserInitiatedStartMode = value;
    }

    /**
     * Ruft den Wert der restrictCallRecordingProvisioningAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictCallRecordingProvisioningAccess() {
        return restrictCallRecordingProvisioningAccess;
    }

    /**
     * Legt den Wert der restrictCallRecordingProvisioningAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictCallRecordingProvisioningAccess(Boolean value) {
        this.restrictCallRecordingProvisioningAccess = value;
    }

}
