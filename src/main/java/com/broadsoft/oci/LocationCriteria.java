//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für LocationCriteria.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="LocationCriteria">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="In Office Zone"/>
 *     <enumeration value="In Primary Zone"/>
 *     <enumeration value="In Office Zone, Outside of Primary Zone"/>
 *     <enumeration value="Outside of Office Zone"/>
 *     <enumeration value="Disregard Zones"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "LocationCriteria")
@XmlEnum
public enum LocationCriteria {

    @XmlEnumValue("In Office Zone")
    IN_OFFICE_ZONE("In Office Zone"),
    @XmlEnumValue("In Primary Zone")
    IN_PRIMARY_ZONE("In Primary Zone"),
    @XmlEnumValue("In Office Zone, Outside of Primary Zone")
    IN_OFFICE_ZONE_OUTSIDE_OF_PRIMARY_ZONE("In Office Zone, Outside of Primary Zone"),
    @XmlEnumValue("Outside of Office Zone")
    OUTSIDE_OF_OFFICE_ZONE("Outside of Office Zone"),
    @XmlEnumValue("Disregard Zones")
    DISREGARD_ZONES("Disregard Zones");
    private final String value;

    LocationCriteria(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LocationCriteria fromValue(String v) {
        for (LocationCriteria c: LocationCriteria.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
