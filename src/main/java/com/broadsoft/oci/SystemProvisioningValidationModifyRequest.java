//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify the system's provisioning validation attributes.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           denyMobilityNumberAsRedirectionDestination
 *           denyEnterpriseNumberAsNetworkLocationDestination
 *       
 * 
 * <p>Java-Klasse für SystemProvisioningValidationModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemProvisioningValidationModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="isNetworkServerQueryActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="timeoutSeconds" type="{}ProvisioningValidationTimeoutSeconds" minOccurs="0"/>
 *         <element name="denyMobilityNumberAsRedirectionDestination" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="denyEnterpriseNumberAsNetworkLocationDestination" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemProvisioningValidationModifyRequest", propOrder = {
    "isActive",
    "isNetworkServerQueryActive",
    "timeoutSeconds",
    "denyMobilityNumberAsRedirectionDestination",
    "denyEnterpriseNumberAsNetworkLocationDestination"
})
public class SystemProvisioningValidationModifyRequest
    extends OCIRequest
{

    protected Boolean isActive;
    protected Boolean isNetworkServerQueryActive;
    protected Integer timeoutSeconds;
    protected Boolean denyMobilityNumberAsRedirectionDestination;
    protected Boolean denyEnterpriseNumberAsNetworkLocationDestination;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der isNetworkServerQueryActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsNetworkServerQueryActive() {
        return isNetworkServerQueryActive;
    }

    /**
     * Legt den Wert der isNetworkServerQueryActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsNetworkServerQueryActive(Boolean value) {
        this.isNetworkServerQueryActive = value;
    }

    /**
     * Ruft den Wert der timeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTimeoutSeconds() {
        return timeoutSeconds;
    }

    /**
     * Legt den Wert der timeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTimeoutSeconds(Integer value) {
        this.timeoutSeconds = value;
    }

    /**
     * Ruft den Wert der denyMobilityNumberAsRedirectionDestination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDenyMobilityNumberAsRedirectionDestination() {
        return denyMobilityNumberAsRedirectionDestination;
    }

    /**
     * Legt den Wert der denyMobilityNumberAsRedirectionDestination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDenyMobilityNumberAsRedirectionDestination(Boolean value) {
        this.denyMobilityNumberAsRedirectionDestination = value;
    }

    /**
     * Ruft den Wert der denyEnterpriseNumberAsNetworkLocationDestination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDenyEnterpriseNumberAsNetworkLocationDestination() {
        return denyEnterpriseNumberAsNetworkLocationDestination;
    }

    /**
     * Legt den Wert der denyEnterpriseNumberAsNetworkLocationDestination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDenyEnterpriseNumberAsNetworkLocationDestination(Boolean value) {
        this.denyEnterpriseNumberAsNetworkLocationDestination = value;
    }

}
