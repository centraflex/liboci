//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system call processing configuration for all subscribers.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         The following elements are only used in AS data mode and ignored in the XS data mode:
 *           userCallingLineIdSelection
 *           isExtendedCallingLineIdActive
 *           isRingTimeOutActive
 *           ringTimeoutSeconds
 *           allowEmergencyRemoteOfficeOriginations
 *         Replaced By: SystemSubscriberModifyCallProcessingParametersRequest14sp7
 *       
 * 
 * <p>Java-Klasse für SystemSubscriberModifyCallProcessingParametersRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSubscriberModifyCallProcessingParametersRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userCallingLineIdSelection" type="{}SystemUserCallingLineIdSelection" minOccurs="0"/>
 *         <element name="isExtendedCallingLineIdActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="isRingTimeOutActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="ringTimeoutSeconds" type="{}SystemUserRingTimeoutSeconds" minOccurs="0"/>
 *         <element name="allowEmergencyRemoteOfficeOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxNoAnswerNumberOfRings" type="{}MaxNoAnswerNumberOfRings" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSubscriberModifyCallProcessingParametersRequest", propOrder = {
    "userCallingLineIdSelection",
    "isExtendedCallingLineIdActive",
    "isRingTimeOutActive",
    "ringTimeoutSeconds",
    "allowEmergencyRemoteOfficeOriginations",
    "maxNoAnswerNumberOfRings"
})
public class SystemSubscriberModifyCallProcessingParametersRequest
    extends OCIRequest
{

    @XmlSchemaType(name = "token")
    protected SystemUserCallingLineIdSelection userCallingLineIdSelection;
    protected Boolean isExtendedCallingLineIdActive;
    protected Boolean isRingTimeOutActive;
    protected Integer ringTimeoutSeconds;
    protected Boolean allowEmergencyRemoteOfficeOriginations;
    protected Integer maxNoAnswerNumberOfRings;

    /**
     * Ruft den Wert der userCallingLineIdSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemUserCallingLineIdSelection }
     *     
     */
    public SystemUserCallingLineIdSelection getUserCallingLineIdSelection() {
        return userCallingLineIdSelection;
    }

    /**
     * Legt den Wert der userCallingLineIdSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemUserCallingLineIdSelection }
     *     
     */
    public void setUserCallingLineIdSelection(SystemUserCallingLineIdSelection value) {
        this.userCallingLineIdSelection = value;
    }

    /**
     * Ruft den Wert der isExtendedCallingLineIdActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsExtendedCallingLineIdActive() {
        return isExtendedCallingLineIdActive;
    }

    /**
     * Legt den Wert der isExtendedCallingLineIdActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsExtendedCallingLineIdActive(Boolean value) {
        this.isExtendedCallingLineIdActive = value;
    }

    /**
     * Ruft den Wert der isRingTimeOutActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRingTimeOutActive() {
        return isRingTimeOutActive;
    }

    /**
     * Legt den Wert der isRingTimeOutActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRingTimeOutActive(Boolean value) {
        this.isRingTimeOutActive = value;
    }

    /**
     * Ruft den Wert der ringTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRingTimeoutSeconds() {
        return ringTimeoutSeconds;
    }

    /**
     * Legt den Wert der ringTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRingTimeoutSeconds(Integer value) {
        this.ringTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der allowEmergencyRemoteOfficeOriginations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowEmergencyRemoteOfficeOriginations() {
        return allowEmergencyRemoteOfficeOriginations;
    }

    /**
     * Legt den Wert der allowEmergencyRemoteOfficeOriginations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowEmergencyRemoteOfficeOriginations(Boolean value) {
        this.allowEmergencyRemoteOfficeOriginations = value;
    }

    /**
     * Ruft den Wert der maxNoAnswerNumberOfRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxNoAnswerNumberOfRings() {
        return maxNoAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der maxNoAnswerNumberOfRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxNoAnswerNumberOfRings(Integer value) {
        this.maxNoAnswerNumberOfRings = value;
    }

}
