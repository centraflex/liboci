//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Contains Call Center statistics.
 *         Replaced By: GroupCallCenterGetInstanceStatisticsResponse14sp9
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterGetInstanceStatisticsResponse13mp8 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterGetInstanceStatisticsResponse13mp8">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="numberOfCallsQueuedNow" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="generateDailyReport" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="collectionPeriodMinutes" type="{}CallCenterStatisticsCollectionPeriodMinutes"/>
 *         <element name="reportingEmailAddress1" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="reportingEmailAddress2" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="queueStatisticsYesterday" type="{}CallCenterQueueStatistics13mp8"/>
 *         <element name="queueStatisticsToday" type="{}CallCenterQueueStatistics13mp8"/>
 *         <element name="agentStatistics" type="{}CallCenterAgentStatistics13mp8" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterGetInstanceStatisticsResponse13mp8", propOrder = {
    "numberOfCallsQueuedNow",
    "generateDailyReport",
    "collectionPeriodMinutes",
    "reportingEmailAddress1",
    "reportingEmailAddress2",
    "queueStatisticsYesterday",
    "queueStatisticsToday",
    "agentStatistics"
})
public class GroupCallCenterGetInstanceStatisticsResponse13Mp8
    extends OCIDataResponse
{

    protected int numberOfCallsQueuedNow;
    protected boolean generateDailyReport;
    protected int collectionPeriodMinutes;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String reportingEmailAddress1;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String reportingEmailAddress2;
    @XmlElement(required = true)
    protected CallCenterQueueStatistics13Mp8 queueStatisticsYesterday;
    @XmlElement(required = true)
    protected CallCenterQueueStatistics13Mp8 queueStatisticsToday;
    protected List<CallCenterAgentStatistics13Mp8> agentStatistics;

    /**
     * Ruft den Wert der numberOfCallsQueuedNow-Eigenschaft ab.
     * 
     */
    public int getNumberOfCallsQueuedNow() {
        return numberOfCallsQueuedNow;
    }

    /**
     * Legt den Wert der numberOfCallsQueuedNow-Eigenschaft fest.
     * 
     */
    public void setNumberOfCallsQueuedNow(int value) {
        this.numberOfCallsQueuedNow = value;
    }

    /**
     * Ruft den Wert der generateDailyReport-Eigenschaft ab.
     * 
     */
    public boolean isGenerateDailyReport() {
        return generateDailyReport;
    }

    /**
     * Legt den Wert der generateDailyReport-Eigenschaft fest.
     * 
     */
    public void setGenerateDailyReport(boolean value) {
        this.generateDailyReport = value;
    }

    /**
     * Ruft den Wert der collectionPeriodMinutes-Eigenschaft ab.
     * 
     */
    public int getCollectionPeriodMinutes() {
        return collectionPeriodMinutes;
    }

    /**
     * Legt den Wert der collectionPeriodMinutes-Eigenschaft fest.
     * 
     */
    public void setCollectionPeriodMinutes(int value) {
        this.collectionPeriodMinutes = value;
    }

    /**
     * Ruft den Wert der reportingEmailAddress1-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingEmailAddress1() {
        return reportingEmailAddress1;
    }

    /**
     * Legt den Wert der reportingEmailAddress1-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingEmailAddress1(String value) {
        this.reportingEmailAddress1 = value;
    }

    /**
     * Ruft den Wert der reportingEmailAddress2-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingEmailAddress2() {
        return reportingEmailAddress2;
    }

    /**
     * Legt den Wert der reportingEmailAddress2-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingEmailAddress2(String value) {
        this.reportingEmailAddress2 = value;
    }

    /**
     * Ruft den Wert der queueStatisticsYesterday-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterQueueStatistics13Mp8 }
     *     
     */
    public CallCenterQueueStatistics13Mp8 getQueueStatisticsYesterday() {
        return queueStatisticsYesterday;
    }

    /**
     * Legt den Wert der queueStatisticsYesterday-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterQueueStatistics13Mp8 }
     *     
     */
    public void setQueueStatisticsYesterday(CallCenterQueueStatistics13Mp8 value) {
        this.queueStatisticsYesterday = value;
    }

    /**
     * Ruft den Wert der queueStatisticsToday-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterQueueStatistics13Mp8 }
     *     
     */
    public CallCenterQueueStatistics13Mp8 getQueueStatisticsToday() {
        return queueStatisticsToday;
    }

    /**
     * Legt den Wert der queueStatisticsToday-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterQueueStatistics13Mp8 }
     *     
     */
    public void setQueueStatisticsToday(CallCenterQueueStatistics13Mp8 value) {
        this.queueStatisticsToday = value;
    }

    /**
     * Gets the value of the agentStatistics property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the agentStatistics property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgentStatistics().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallCenterAgentStatistics13Mp8 }
     * 
     * 
     * @return
     *     The value of the agentStatistics property.
     */
    public List<CallCenterAgentStatistics13Mp8> getAgentStatistics() {
        if (agentStatistics == null) {
            agentStatistics = new ArrayList<>();
        }
        return this.agentStatistics;
    }

}
