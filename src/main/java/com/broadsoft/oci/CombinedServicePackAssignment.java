//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *       Assign a service pack to user. If the service pack has not been authorized to service provider or 
 *       group, it will be authorized.
 *             
 *       If the service pack needs to be authorized at group/service provider levels, the authorizedQuantity
 *       will be used. Otherwise, it will be ignored. If the authorizedQuantity is not included, the
 *       quantity will come from the group template for the service pack. If a template does 
 *       not exist, the service quantity will be set to unlimited.
 *     
 * 
 * <p>Java-Klasse für CombinedServicePackAssignment complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CombinedServicePackAssignment">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="servicePackName" type="{}ServicePackName"/>
 *         <element name="authorizedQuantity" type="{}UnboundedPositiveInt" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CombinedServicePackAssignment", propOrder = {
    "servicePackName",
    "authorizedQuantity"
})
public class CombinedServicePackAssignment {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String servicePackName;
    protected UnboundedPositiveInt authorizedQuantity;

    /**
     * Ruft den Wert der servicePackName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicePackName() {
        return servicePackName;
    }

    /**
     * Legt den Wert der servicePackName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicePackName(String value) {
        this.servicePackName = value;
    }

    /**
     * Ruft den Wert der authorizedQuantity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public UnboundedPositiveInt getAuthorizedQuantity() {
        return authorizedQuantity;
    }

    /**
     * Legt den Wert der authorizedQuantity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public void setAuthorizedQuantity(UnboundedPositiveInt value) {
        this.authorizedQuantity = value;
    }

}
