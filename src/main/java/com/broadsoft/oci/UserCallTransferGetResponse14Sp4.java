//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserCallTransferGetRequest14sp4.
 *       
 * 
 * <p>Java-Klasse für UserCallTransferGetResponse14sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallTransferGetResponse14sp4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isRecallActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="recallNumberOfRings" type="{}CallTransferRecallNumberOfRings"/>
 *         <element name="useDiversionInhibitorForBlindTransfer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useDiversionInhibitorForConsultativeCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableBusyCampOn" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="busyCampOnSeconds" type="{}CallTransferBusyCampOnSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallTransferGetResponse14sp4", propOrder = {
    "isRecallActive",
    "recallNumberOfRings",
    "useDiversionInhibitorForBlindTransfer",
    "useDiversionInhibitorForConsultativeCalls",
    "enableBusyCampOn",
    "busyCampOnSeconds"
})
public class UserCallTransferGetResponse14Sp4
    extends OCIDataResponse
{

    protected boolean isRecallActive;
    protected int recallNumberOfRings;
    protected boolean useDiversionInhibitorForBlindTransfer;
    protected boolean useDiversionInhibitorForConsultativeCalls;
    protected boolean enableBusyCampOn;
    protected int busyCampOnSeconds;

    /**
     * Ruft den Wert der isRecallActive-Eigenschaft ab.
     * 
     */
    public boolean isIsRecallActive() {
        return isRecallActive;
    }

    /**
     * Legt den Wert der isRecallActive-Eigenschaft fest.
     * 
     */
    public void setIsRecallActive(boolean value) {
        this.isRecallActive = value;
    }

    /**
     * Ruft den Wert der recallNumberOfRings-Eigenschaft ab.
     * 
     */
    public int getRecallNumberOfRings() {
        return recallNumberOfRings;
    }

    /**
     * Legt den Wert der recallNumberOfRings-Eigenschaft fest.
     * 
     */
    public void setRecallNumberOfRings(int value) {
        this.recallNumberOfRings = value;
    }

    /**
     * Ruft den Wert der useDiversionInhibitorForBlindTransfer-Eigenschaft ab.
     * 
     */
    public boolean isUseDiversionInhibitorForBlindTransfer() {
        return useDiversionInhibitorForBlindTransfer;
    }

    /**
     * Legt den Wert der useDiversionInhibitorForBlindTransfer-Eigenschaft fest.
     * 
     */
    public void setUseDiversionInhibitorForBlindTransfer(boolean value) {
        this.useDiversionInhibitorForBlindTransfer = value;
    }

    /**
     * Ruft den Wert der useDiversionInhibitorForConsultativeCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseDiversionInhibitorForConsultativeCalls() {
        return useDiversionInhibitorForConsultativeCalls;
    }

    /**
     * Legt den Wert der useDiversionInhibitorForConsultativeCalls-Eigenschaft fest.
     * 
     */
    public void setUseDiversionInhibitorForConsultativeCalls(boolean value) {
        this.useDiversionInhibitorForConsultativeCalls = value;
    }

    /**
     * Ruft den Wert der enableBusyCampOn-Eigenschaft ab.
     * 
     */
    public boolean isEnableBusyCampOn() {
        return enableBusyCampOn;
    }

    /**
     * Legt den Wert der enableBusyCampOn-Eigenschaft fest.
     * 
     */
    public void setEnableBusyCampOn(boolean value) {
        this.enableBusyCampOn = value;
    }

    /**
     * Ruft den Wert der busyCampOnSeconds-Eigenschaft ab.
     * 
     */
    public int getBusyCampOnSeconds() {
        return busyCampOnSeconds;
    }

    /**
     * Legt den Wert der busyCampOnSeconds-Eigenschaft fest.
     * 
     */
    public void setBusyCampOnSeconds(int value) {
        this.busyCampOnSeconds = value;
    }

}
