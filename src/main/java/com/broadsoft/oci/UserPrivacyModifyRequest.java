//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the user level data associated with Privacy.
 *         This command is supported for regular users only.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserPrivacyModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPrivacyModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="enableDirectoryPrivacy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableAutoAttendantExtensionDialingPrivacy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableAutoAttendantNameDialingPrivacy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enablePhoneStatusPrivacy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="permittedMonitorUserIdList" type="{}ReplacementUserIdList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPrivacyModifyRequest", propOrder = {
    "userId",
    "enableDirectoryPrivacy",
    "enableAutoAttendantExtensionDialingPrivacy",
    "enableAutoAttendantNameDialingPrivacy",
    "enablePhoneStatusPrivacy",
    "permittedMonitorUserIdList"
})
public class UserPrivacyModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean enableDirectoryPrivacy;
    protected Boolean enableAutoAttendantExtensionDialingPrivacy;
    protected Boolean enableAutoAttendantNameDialingPrivacy;
    protected Boolean enablePhoneStatusPrivacy;
    @XmlElementRef(name = "permittedMonitorUserIdList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserIdList> permittedMonitorUserIdList;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der enableDirectoryPrivacy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableDirectoryPrivacy() {
        return enableDirectoryPrivacy;
    }

    /**
     * Legt den Wert der enableDirectoryPrivacy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableDirectoryPrivacy(Boolean value) {
        this.enableDirectoryPrivacy = value;
    }

    /**
     * Ruft den Wert der enableAutoAttendantExtensionDialingPrivacy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableAutoAttendantExtensionDialingPrivacy() {
        return enableAutoAttendantExtensionDialingPrivacy;
    }

    /**
     * Legt den Wert der enableAutoAttendantExtensionDialingPrivacy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableAutoAttendantExtensionDialingPrivacy(Boolean value) {
        this.enableAutoAttendantExtensionDialingPrivacy = value;
    }

    /**
     * Ruft den Wert der enableAutoAttendantNameDialingPrivacy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableAutoAttendantNameDialingPrivacy() {
        return enableAutoAttendantNameDialingPrivacy;
    }

    /**
     * Legt den Wert der enableAutoAttendantNameDialingPrivacy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableAutoAttendantNameDialingPrivacy(Boolean value) {
        this.enableAutoAttendantNameDialingPrivacy = value;
    }

    /**
     * Ruft den Wert der enablePhoneStatusPrivacy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnablePhoneStatusPrivacy() {
        return enablePhoneStatusPrivacy;
    }

    /**
     * Legt den Wert der enablePhoneStatusPrivacy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnablePhoneStatusPrivacy(Boolean value) {
        this.enablePhoneStatusPrivacy = value;
    }

    /**
     * Ruft den Wert der permittedMonitorUserIdList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserIdList> getPermittedMonitorUserIdList() {
        return permittedMonitorUserIdList;
    }

    /**
     * Legt den Wert der permittedMonitorUserIdList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserIdList }{@code >}
     *     
     */
    public void setPermittedMonitorUserIdList(JAXBElement<ReplacementUserIdList> value) {
        this.permittedMonitorUserIdList = value;
    }

}
