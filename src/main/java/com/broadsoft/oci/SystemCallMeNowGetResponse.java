//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemCallMeNowGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemCallMeNowGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallMeNowGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="passcodeLength" type="{}CallMeNowPasscodeLength"/>
 *         <element name="passcodeTimeoutSeconds" type="{}CallMeNowPasscodeTimeoutSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallMeNowGetResponse", propOrder = {
    "passcodeLength",
    "passcodeTimeoutSeconds"
})
public class SystemCallMeNowGetResponse
    extends OCIDataResponse
{

    protected int passcodeLength;
    protected int passcodeTimeoutSeconds;

    /**
     * Ruft den Wert der passcodeLength-Eigenschaft ab.
     * 
     */
    public int getPasscodeLength() {
        return passcodeLength;
    }

    /**
     * Legt den Wert der passcodeLength-Eigenschaft fest.
     * 
     */
    public void setPasscodeLength(int value) {
        this.passcodeLength = value;
    }

    /**
     * Ruft den Wert der passcodeTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getPasscodeTimeoutSeconds() {
        return passcodeTimeoutSeconds;
    }

    /**
     * Legt den Wert der passcodeTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setPasscodeTimeoutSeconds(int value) {
        this.passcodeTimeoutSeconds = value;
    }

}
