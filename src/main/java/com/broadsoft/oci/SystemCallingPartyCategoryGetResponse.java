//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCallingPartyCategoryGetRequest.
 *         Contains information of a Calling Party Category defined in system.
 *       
 * 
 * <p>Java-Klasse für SystemCallingPartyCategoryGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallingPartyCategoryGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="cpcValue" type="{}CallingPartyCategoryValue" minOccurs="0"/>
 *         <element name="isupOliValue" type="{}ISDNUserPartOriginatingLineInformationValue" minOccurs="0"/>
 *         <element name="gtdOliValue" type="{}ISDNGenericTransparencyDescriptorOliValue" minOccurs="0"/>
 *         <element name="userCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="payPhone" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="operator" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="default" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="collectCall" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="webDisplayKey" type="{}WebDisplayKey" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallingPartyCategoryGetResponse", propOrder = {
    "cpcValue",
    "isupOliValue",
    "gtdOliValue",
    "userCategory",
    "payPhone",
    "operator",
    "_default",
    "collectCall",
    "webDisplayKey"
})
public class SystemCallingPartyCategoryGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String cpcValue;
    protected Integer isupOliValue;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String gtdOliValue;
    protected boolean userCategory;
    protected boolean payPhone;
    protected boolean operator;
    @XmlElement(name = "default")
    protected boolean _default;
    protected boolean collectCall;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String webDisplayKey;

    /**
     * Ruft den Wert der cpcValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpcValue() {
        return cpcValue;
    }

    /**
     * Legt den Wert der cpcValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpcValue(String value) {
        this.cpcValue = value;
    }

    /**
     * Ruft den Wert der isupOliValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsupOliValue() {
        return isupOliValue;
    }

    /**
     * Legt den Wert der isupOliValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsupOliValue(Integer value) {
        this.isupOliValue = value;
    }

    /**
     * Ruft den Wert der gtdOliValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtdOliValue() {
        return gtdOliValue;
    }

    /**
     * Legt den Wert der gtdOliValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtdOliValue(String value) {
        this.gtdOliValue = value;
    }

    /**
     * Ruft den Wert der userCategory-Eigenschaft ab.
     * 
     */
    public boolean isUserCategory() {
        return userCategory;
    }

    /**
     * Legt den Wert der userCategory-Eigenschaft fest.
     * 
     */
    public void setUserCategory(boolean value) {
        this.userCategory = value;
    }

    /**
     * Ruft den Wert der payPhone-Eigenschaft ab.
     * 
     */
    public boolean isPayPhone() {
        return payPhone;
    }

    /**
     * Legt den Wert der payPhone-Eigenschaft fest.
     * 
     */
    public void setPayPhone(boolean value) {
        this.payPhone = value;
    }

    /**
     * Ruft den Wert der operator-Eigenschaft ab.
     * 
     */
    public boolean isOperator() {
        return operator;
    }

    /**
     * Legt den Wert der operator-Eigenschaft fest.
     * 
     */
    public void setOperator(boolean value) {
        this.operator = value;
    }

    /**
     * Ruft den Wert der default-Eigenschaft ab.
     * 
     */
    public boolean isDefault() {
        return _default;
    }

    /**
     * Legt den Wert der default-Eigenschaft fest.
     * 
     */
    public void setDefault(boolean value) {
        this._default = value;
    }

    /**
     * Ruft den Wert der collectCall-Eigenschaft ab.
     * 
     */
    public boolean isCollectCall() {
        return collectCall;
    }

    /**
     * Legt den Wert der collectCall-Eigenschaft fest.
     * 
     */
    public void setCollectCall(boolean value) {
        this.collectCall = value;
    }

    /**
     * Ruft den Wert der webDisplayKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebDisplayKey() {
        return webDisplayKey;
    }

    /**
     * Legt den Wert der webDisplayKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebDisplayKey(String value) {
        this.webDisplayKey = value;
    }

}
