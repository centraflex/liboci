//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Allows or disallows various types of incoming calls for a specified department.
 *       
 * 
 * <p>Java-Klasse für IncomingCallingPlanDepartmentPermissions complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="IncomingCallingPlanDepartmentPermissions">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="departmentKey" type="{}DepartmentKey"/>
 *         <element name="departmentFullPathName" type="{}DepartmentFullPathName"/>
 *         <element name="allowFromWithinGroup" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowFromOutsideGroup" type="{}IncomingCallingPlanOutsideCallPermission"/>
 *         <element name="allowCollectCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="digitPatternPermission" type="{}IncomingCallingPlanDigitPatternPermission" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IncomingCallingPlanDepartmentPermissions", propOrder = {
    "departmentKey",
    "departmentFullPathName",
    "allowFromWithinGroup",
    "allowFromOutsideGroup",
    "allowCollectCalls",
    "digitPatternPermission"
})
public class IncomingCallingPlanDepartmentPermissions {

    @XmlElement(required = true)
    protected DepartmentKey departmentKey;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String departmentFullPathName;
    protected boolean allowFromWithinGroup;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected IncomingCallingPlanOutsideCallPermission allowFromOutsideGroup;
    protected boolean allowCollectCalls;
    protected List<IncomingCallingPlanDigitPatternPermission> digitPatternPermission;

    /**
     * Ruft den Wert der departmentKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DepartmentKey }
     *     
     */
    public DepartmentKey getDepartmentKey() {
        return departmentKey;
    }

    /**
     * Legt den Wert der departmentKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DepartmentKey }
     *     
     */
    public void setDepartmentKey(DepartmentKey value) {
        this.departmentKey = value;
    }

    /**
     * Ruft den Wert der departmentFullPathName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartmentFullPathName() {
        return departmentFullPathName;
    }

    /**
     * Legt den Wert der departmentFullPathName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartmentFullPathName(String value) {
        this.departmentFullPathName = value;
    }

    /**
     * Ruft den Wert der allowFromWithinGroup-Eigenschaft ab.
     * 
     */
    public boolean isAllowFromWithinGroup() {
        return allowFromWithinGroup;
    }

    /**
     * Legt den Wert der allowFromWithinGroup-Eigenschaft fest.
     * 
     */
    public void setAllowFromWithinGroup(boolean value) {
        this.allowFromWithinGroup = value;
    }

    /**
     * Ruft den Wert der allowFromOutsideGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IncomingCallingPlanOutsideCallPermission }
     *     
     */
    public IncomingCallingPlanOutsideCallPermission getAllowFromOutsideGroup() {
        return allowFromOutsideGroup;
    }

    /**
     * Legt den Wert der allowFromOutsideGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IncomingCallingPlanOutsideCallPermission }
     *     
     */
    public void setAllowFromOutsideGroup(IncomingCallingPlanOutsideCallPermission value) {
        this.allowFromOutsideGroup = value;
    }

    /**
     * Ruft den Wert der allowCollectCalls-Eigenschaft ab.
     * 
     */
    public boolean isAllowCollectCalls() {
        return allowCollectCalls;
    }

    /**
     * Legt den Wert der allowCollectCalls-Eigenschaft fest.
     * 
     */
    public void setAllowCollectCalls(boolean value) {
        this.allowCollectCalls = value;
    }

    /**
     * Gets the value of the digitPatternPermission property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the digitPatternPermission property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDigitPatternPermission().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IncomingCallingPlanDigitPatternPermission }
     * 
     * 
     * @return
     *     The value of the digitPatternPermission property.
     */
    public List<IncomingCallingPlanDigitPatternPermission> getDigitPatternPermission() {
        if (digitPatternPermission == null) {
            digitPatternPermission = new ArrayList<>();
        }
        return this.digitPatternPermission;
    }

}
