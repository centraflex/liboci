//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *           Response to SystemInCallServiceActivationGetRequest17.
 *         
 * 
 * <p>Java-Klasse für SystemInCallServiceActivationGetResponse17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemInCallServiceActivationGetResponse17">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="defaultFlashActivationDigits" type="{}InCallServiceActivationDigits"/>
 *         <element name="defaultCallTransferActivationDigits" type="{}InCallServiceActivationDigits"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemInCallServiceActivationGetResponse17", propOrder = {
    "defaultFlashActivationDigits",
    "defaultCallTransferActivationDigits"
})
public class SystemInCallServiceActivationGetResponse17
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultFlashActivationDigits;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultCallTransferActivationDigits;

    /**
     * Ruft den Wert der defaultFlashActivationDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultFlashActivationDigits() {
        return defaultFlashActivationDigits;
    }

    /**
     * Legt den Wert der defaultFlashActivationDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultFlashActivationDigits(String value) {
        this.defaultFlashActivationDigits = value;
    }

    /**
     * Ruft den Wert der defaultCallTransferActivationDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultCallTransferActivationDigits() {
        return defaultCallTransferActivationDigits;
    }

    /**
     * Legt den Wert der defaultCallTransferActivationDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultCallTransferActivationDigits(String value) {
        this.defaultCallTransferActivationDigits = value;
    }

}
