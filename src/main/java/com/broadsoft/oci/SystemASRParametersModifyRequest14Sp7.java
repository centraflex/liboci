//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify Application Server Registration system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *         
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           enableCustomMessageControl
 *           customNumberOfUsersPerMessage
 *           customMessageIntervalMilliseconds
 *       
 * 
 * <p>Java-Klasse für SystemASRParametersModifyRequest14sp7 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemASRParametersModifyRequest14sp7">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="maxTransmissions" type="{}ASRMaxTransmissions" minOccurs="0"/>
 *         <element name="retransmissionDelayMilliSeconds" type="{}ASRRetransmissionDelayMilliSeconds" minOccurs="0"/>
 *         <element name="listeningPort" type="{}Port1025" minOccurs="0"/>
 *         <element name="enableCustomMessageControl" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="customNumberOfUsersPerMessage" type="{}ASRNumberOfUsersPerMessage" minOccurs="0"/>
 *         <element name="customMessageIntervalMilliseconds" type="{}ASRMessageIntervalMilliseconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemASRParametersModifyRequest14sp7", propOrder = {
    "maxTransmissions",
    "retransmissionDelayMilliSeconds",
    "listeningPort",
    "enableCustomMessageControl",
    "customNumberOfUsersPerMessage",
    "customMessageIntervalMilliseconds"
})
public class SystemASRParametersModifyRequest14Sp7
    extends OCIRequest
{

    protected Integer maxTransmissions;
    protected Integer retransmissionDelayMilliSeconds;
    protected Integer listeningPort;
    protected Boolean enableCustomMessageControl;
    protected Integer customNumberOfUsersPerMessage;
    protected Integer customMessageIntervalMilliseconds;

    /**
     * Ruft den Wert der maxTransmissions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxTransmissions() {
        return maxTransmissions;
    }

    /**
     * Legt den Wert der maxTransmissions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxTransmissions(Integer value) {
        this.maxTransmissions = value;
    }

    /**
     * Ruft den Wert der retransmissionDelayMilliSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRetransmissionDelayMilliSeconds() {
        return retransmissionDelayMilliSeconds;
    }

    /**
     * Legt den Wert der retransmissionDelayMilliSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRetransmissionDelayMilliSeconds(Integer value) {
        this.retransmissionDelayMilliSeconds = value;
    }

    /**
     * Ruft den Wert der listeningPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getListeningPort() {
        return listeningPort;
    }

    /**
     * Legt den Wert der listeningPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setListeningPort(Integer value) {
        this.listeningPort = value;
    }

    /**
     * Ruft den Wert der enableCustomMessageControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCustomMessageControl() {
        return enableCustomMessageControl;
    }

    /**
     * Legt den Wert der enableCustomMessageControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCustomMessageControl(Boolean value) {
        this.enableCustomMessageControl = value;
    }

    /**
     * Ruft den Wert der customNumberOfUsersPerMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCustomNumberOfUsersPerMessage() {
        return customNumberOfUsersPerMessage;
    }

    /**
     * Legt den Wert der customNumberOfUsersPerMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCustomNumberOfUsersPerMessage(Integer value) {
        this.customNumberOfUsersPerMessage = value;
    }

    /**
     * Ruft den Wert der customMessageIntervalMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCustomMessageIntervalMilliseconds() {
        return customMessageIntervalMilliseconds;
    }

    /**
     * Legt den Wert der customMessageIntervalMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCustomMessageIntervalMilliseconds(Integer value) {
        this.customMessageIntervalMilliseconds = value;
    }

}
