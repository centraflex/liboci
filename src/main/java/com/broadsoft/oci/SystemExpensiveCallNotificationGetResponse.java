//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemExpensiveCallNotificationGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemExpensiveCallNotificationGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemExpensiveCallNotificationGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enablePostAnnouncementDelayTimer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="postAnnouncementDelaySeconds" type="{}ExpensiveCallNotificationPostAnnouncementDelaySeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemExpensiveCallNotificationGetResponse", propOrder = {
    "enablePostAnnouncementDelayTimer",
    "postAnnouncementDelaySeconds"
})
public class SystemExpensiveCallNotificationGetResponse
    extends OCIDataResponse
{

    protected boolean enablePostAnnouncementDelayTimer;
    protected int postAnnouncementDelaySeconds;

    /**
     * Ruft den Wert der enablePostAnnouncementDelayTimer-Eigenschaft ab.
     * 
     */
    public boolean isEnablePostAnnouncementDelayTimer() {
        return enablePostAnnouncementDelayTimer;
    }

    /**
     * Legt den Wert der enablePostAnnouncementDelayTimer-Eigenschaft fest.
     * 
     */
    public void setEnablePostAnnouncementDelayTimer(boolean value) {
        this.enablePostAnnouncementDelayTimer = value;
    }

    /**
     * Ruft den Wert der postAnnouncementDelaySeconds-Eigenschaft ab.
     * 
     */
    public int getPostAnnouncementDelaySeconds() {
        return postAnnouncementDelaySeconds;
    }

    /**
     * Legt den Wert der postAnnouncementDelaySeconds-Eigenschaft fest.
     * 
     */
    public void setPostAnnouncementDelaySeconds(int value) {
        this.postAnnouncementDelaySeconds = value;
    }

}
