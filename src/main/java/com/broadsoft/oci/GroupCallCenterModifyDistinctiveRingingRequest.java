//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the distinctive ringing configuration values for call center.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterModifyDistinctiveRingingRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterModifyDistinctiveRingingRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="distinctiveRingingCallCenterCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="distinctiveRingingRingPatternForCallCenter" type="{}RingPattern" minOccurs="0"/>
 *         <element name="distinctiveRingingForceDeliveryRingPattern" type="{}RingPattern" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterModifyDistinctiveRingingRequest", propOrder = {
    "serviceUserId",
    "distinctiveRingingCallCenterCalls",
    "distinctiveRingingRingPatternForCallCenter",
    "distinctiveRingingForceDeliveryRingPattern"
})
public class GroupCallCenterModifyDistinctiveRingingRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected Boolean distinctiveRingingCallCenterCalls;
    @XmlSchemaType(name = "token")
    protected RingPattern distinctiveRingingRingPatternForCallCenter;
    @XmlSchemaType(name = "token")
    protected RingPattern distinctiveRingingForceDeliveryRingPattern;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der distinctiveRingingCallCenterCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDistinctiveRingingCallCenterCalls() {
        return distinctiveRingingCallCenterCalls;
    }

    /**
     * Legt den Wert der distinctiveRingingCallCenterCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDistinctiveRingingCallCenterCalls(Boolean value) {
        this.distinctiveRingingCallCenterCalls = value;
    }

    /**
     * Ruft den Wert der distinctiveRingingRingPatternForCallCenter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RingPattern }
     *     
     */
    public RingPattern getDistinctiveRingingRingPatternForCallCenter() {
        return distinctiveRingingRingPatternForCallCenter;
    }

    /**
     * Legt den Wert der distinctiveRingingRingPatternForCallCenter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RingPattern }
     *     
     */
    public void setDistinctiveRingingRingPatternForCallCenter(RingPattern value) {
        this.distinctiveRingingRingPatternForCallCenter = value;
    }

    /**
     * Ruft den Wert der distinctiveRingingForceDeliveryRingPattern-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RingPattern }
     *     
     */
    public RingPattern getDistinctiveRingingForceDeliveryRingPattern() {
        return distinctiveRingingForceDeliveryRingPattern;
    }

    /**
     * Legt den Wert der distinctiveRingingForceDeliveryRingPattern-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RingPattern }
     *     
     */
    public void setDistinctiveRingingForceDeliveryRingPattern(RingPattern value) {
        this.distinctiveRingingForceDeliveryRingPattern = value;
    }

}
