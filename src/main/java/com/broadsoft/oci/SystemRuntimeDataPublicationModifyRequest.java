//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system call admission control parameters.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *         enableRuntimeDataSync,
 *         runtimeIntervalInMilliSeconds
 *       
 * 
 * <p>Java-Klasse für SystemRuntimeDataPublicationModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemRuntimeDataPublicationModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enableRuntimeDataSync" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="runtimeDataSyncIntervalInMilliSeconds" type="{}RuntimeDataSyncIntervalInMilliSeconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemRuntimeDataPublicationModifyRequest", propOrder = {
    "enableRuntimeDataSync",
    "runtimeDataSyncIntervalInMilliSeconds"
})
public class SystemRuntimeDataPublicationModifyRequest
    extends OCIRequest
{

    protected Boolean enableRuntimeDataSync;
    protected Integer runtimeDataSyncIntervalInMilliSeconds;

    /**
     * Ruft den Wert der enableRuntimeDataSync-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableRuntimeDataSync() {
        return enableRuntimeDataSync;
    }

    /**
     * Legt den Wert der enableRuntimeDataSync-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableRuntimeDataSync(Boolean value) {
        this.enableRuntimeDataSync = value;
    }

    /**
     * Ruft den Wert der runtimeDataSyncIntervalInMilliSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRuntimeDataSyncIntervalInMilliSeconds() {
        return runtimeDataSyncIntervalInMilliSeconds;
    }

    /**
     * Legt den Wert der runtimeDataSyncIntervalInMilliSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRuntimeDataSyncIntervalInMilliSeconds(Integer value) {
        this.runtimeDataSyncIntervalInMilliSeconds = value;
    }

}
