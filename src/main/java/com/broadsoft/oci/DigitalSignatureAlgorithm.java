//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DigitalSignatureAlgorithm.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DigitalSignatureAlgorithm">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     <enumeration value="MD5"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DigitalSignatureAlgorithm")
@XmlEnum
public enum DigitalSignatureAlgorithm {

    @XmlEnumValue("MD5")
    MD_5("MD5");
    private final String value;

    DigitalSignatureAlgorithm(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DigitalSignatureAlgorithm fromValue(String v) {
        for (DigitalSignatureAlgorithm c: DigitalSignatureAlgorithm.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
