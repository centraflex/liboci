//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemPerformanceMeasurementReportingGetRequest22.
 *         
 *         Replaced by: SystemPerformanceMeasurementReportingGetResponse24
 *       
 * 
 * <p>Java-Klasse für SystemPerformanceMeasurementReportingGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPerformanceMeasurementReportingGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="reportingInterval" type="{}PerformanceMeasurementReportingIntervalMinutes"/>
 *         <element name="resetMeasurementsAfterEachReport" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="reportEnterprise" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="reportServiceProvider" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="reportDevice" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="reportTable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPerformanceMeasurementReportingGetResponse22", propOrder = {
    "isActive",
    "reportingInterval",
    "resetMeasurementsAfterEachReport",
    "reportEnterprise",
    "reportServiceProvider",
    "reportDevice",
    "reportTable"
})
public class SystemPerformanceMeasurementReportingGetResponse22
    extends OCIDataResponse
{

    protected boolean isActive;
    protected int reportingInterval;
    protected boolean resetMeasurementsAfterEachReport;
    protected boolean reportEnterprise;
    protected boolean reportServiceProvider;
    protected boolean reportDevice;
    protected boolean reportTable;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der reportingInterval-Eigenschaft ab.
     * 
     */
    public int getReportingInterval() {
        return reportingInterval;
    }

    /**
     * Legt den Wert der reportingInterval-Eigenschaft fest.
     * 
     */
    public void setReportingInterval(int value) {
        this.reportingInterval = value;
    }

    /**
     * Ruft den Wert der resetMeasurementsAfterEachReport-Eigenschaft ab.
     * 
     */
    public boolean isResetMeasurementsAfterEachReport() {
        return resetMeasurementsAfterEachReport;
    }

    /**
     * Legt den Wert der resetMeasurementsAfterEachReport-Eigenschaft fest.
     * 
     */
    public void setResetMeasurementsAfterEachReport(boolean value) {
        this.resetMeasurementsAfterEachReport = value;
    }

    /**
     * Ruft den Wert der reportEnterprise-Eigenschaft ab.
     * 
     */
    public boolean isReportEnterprise() {
        return reportEnterprise;
    }

    /**
     * Legt den Wert der reportEnterprise-Eigenschaft fest.
     * 
     */
    public void setReportEnterprise(boolean value) {
        this.reportEnterprise = value;
    }

    /**
     * Ruft den Wert der reportServiceProvider-Eigenschaft ab.
     * 
     */
    public boolean isReportServiceProvider() {
        return reportServiceProvider;
    }

    /**
     * Legt den Wert der reportServiceProvider-Eigenschaft fest.
     * 
     */
    public void setReportServiceProvider(boolean value) {
        this.reportServiceProvider = value;
    }

    /**
     * Ruft den Wert der reportDevice-Eigenschaft ab.
     * 
     */
    public boolean isReportDevice() {
        return reportDevice;
    }

    /**
     * Legt den Wert der reportDevice-Eigenschaft fest.
     * 
     */
    public void setReportDevice(boolean value) {
        this.reportDevice = value;
    }

    /**
     * Ruft den Wert der reportTable-Eigenschaft ab.
     * 
     */
    public boolean isReportTable() {
        return reportTable;
    }

    /**
     * Legt den Wert der reportTable-Eigenschaft fest.
     * 
     */
    public void setReportTable(boolean value) {
        this.reportTable = value;
    }

}
