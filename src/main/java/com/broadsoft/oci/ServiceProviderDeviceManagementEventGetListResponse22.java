//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderDeviceManagementEventGetListRequest22.
 *         Contains a table with column headings: "Event Id", "Status", "Action",
 *         "Level", "Type", "Additional Info", "Is Local", "Completion %", 
 *         "Pushed/ Same Hash/ Not Pushed", "Login Id", "Start Time", 
 *         "Process Time", "Rx Time", "Total Time", "Request", "Priority".
 *         "Event Id" is a unique identifer for the event.
 *         "Status" can be: Pending, Queued, In Progress,
 *         Process On Other Host, Stale, Completed, Canceled.
 *         "Action" can be: Delete, Download, Rebuild, Reset, Upload.
 *         "Level" can be: Device, Device Type, Device Type Group, Group, User.
 *         "Type" can be: Automatic, Manual.
 *         "Additional Info" includes the affected device type, device or group.
 *         It depends on the level of the event:
 *           Device Profile: "Device Name" "Service Provider Id" "Group Id"
 *           Device Type: "Device Type Name"
 *           Device Type Service Provider: "Service Provider Id" "Device Type Name"
 *           Service Provider: "Service Provider Id"
 *           Device Type Group: "Service Provider Id" "Group Id" "Device Type Name"
 *           Group: "Service Provider Id" "Group Id"
 *           User: "User Id"
 *         "Is Local" is set to "yes" if the event is processed on the server 
 *         who received the request, "no" otherwise meaning that the event is
 *         processed on another server.
 *         "Completion %" provides an estimate of the completion of the event.
 *         A percentage is given, the current number of completed expanded event,
 *         and the total number of expanded event.
 *         "Pushed/ Same Hash/ Not Pushed" gives the total number of files that 
 *         were pushed, not pushed because of same hash, and not pushed when 
 *         processing the event.
 *         "LoginId" is the user or admin id who triggered the event.
 *         "Start Time" is the date when the event's processing started.  The
 *         display shows the month, day, hours, minutes, and seconds (MM-dd hh:mm:ss).
 *         "Process Time" is the time taken to process the event in hours,
 *         minutes, seconds, and milliseconds (hhhh:mm:ss.SSS).
 *         "Rx Time" is the date when the event was received via OCI-P and 
 *         stored in the system.  The display shows the month, day, hours, 
 *         minutes, and seconds (MM-dd hh:mm:ss).
 *         "Total Time" is the total time the event was in the system, from the
 *         moment it was received and stored until its processing ended, in 
 *         hours, minutes, seconds, and milliseconds (hhhh:mm:ss.SSS).
 *         "Request" is the name of the OCI-P request that triggered the event.
 *         "Priority" is the priority of the event.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderDeviceManagementEventGetListResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderDeviceManagementEventGetListResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="eventTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderDeviceManagementEventGetListResponse22", propOrder = {
    "eventTable"
})
public class ServiceProviderDeviceManagementEventGetListResponse22
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable eventTable;

    /**
     * Ruft den Wert der eventTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getEventTable() {
        return eventTable;
    }

    /**
     * Legt den Wert der eventTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setEventTable(OCITable value) {
        this.eventTable = value;
    }

}
