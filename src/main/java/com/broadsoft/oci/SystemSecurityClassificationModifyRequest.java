//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify security classification parameters.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         NOTE: The security classifications must be specified in order of priority. The command fails if all the security classifications defined for the system are not provided.
 *       
 * 
 * <p>Java-Klasse für SystemSecurityClassificationModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSecurityClassificationModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="meetMeAnncThreshold" type="{}SecurityClassificationMeetMeConferenceAnnouncementThresholdSeconds" minOccurs="0"/>
 *         <element name="playTrunkUserSecurityClassificationAnnouncement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="securityClassificationName" type="{}SecurityClassificationName" maxOccurs="20" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSecurityClassificationModifyRequest", propOrder = {
    "meetMeAnncThreshold",
    "playTrunkUserSecurityClassificationAnnouncement",
    "securityClassificationName"
})
public class SystemSecurityClassificationModifyRequest
    extends OCIRequest
{

    protected Integer meetMeAnncThreshold;
    protected Boolean playTrunkUserSecurityClassificationAnnouncement;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> securityClassificationName;

    /**
     * Ruft den Wert der meetMeAnncThreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMeetMeAnncThreshold() {
        return meetMeAnncThreshold;
    }

    /**
     * Legt den Wert der meetMeAnncThreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMeetMeAnncThreshold(Integer value) {
        this.meetMeAnncThreshold = value;
    }

    /**
     * Ruft den Wert der playTrunkUserSecurityClassificationAnnouncement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayTrunkUserSecurityClassificationAnnouncement() {
        return playTrunkUserSecurityClassificationAnnouncement;
    }

    /**
     * Legt den Wert der playTrunkUserSecurityClassificationAnnouncement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayTrunkUserSecurityClassificationAnnouncement(Boolean value) {
        this.playTrunkUserSecurityClassificationAnnouncement = value;
    }

    /**
     * Gets the value of the securityClassificationName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the securityClassificationName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSecurityClassificationName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the securityClassificationName property.
     */
    public List<String> getSecurityClassificationName() {
        if (securityClassificationName == null) {
            securityClassificationName = new ArrayList<>();
        }
        return this.securityClassificationName;
    }

}
