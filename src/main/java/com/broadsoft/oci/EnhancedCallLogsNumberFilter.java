//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Filter criteria based on the called number or number called.
 *       
 * 
 * <p>Java-Klasse für EnhancedCallLogsNumberFilter complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnhancedCallLogsNumberFilter">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <choice>
 *           <element name="includeBasicCallType" type="{}BasicCallType" maxOccurs="unbounded"/>
 *           <element name="includeCallCategory" type="{}CallCategory" maxOccurs="unbounded"/>
 *           <element name="includeConfigurableCallType" type="{}CommunicationBarringCallType" maxOccurs="unbounded"/>
 *           <element name="searchCriteriaDialedNumber" type="{}SearchCriteriaOutgoingDNorSIPURI" maxOccurs="unbounded"/>
 *           <element name="searchCriteriaCalledNumber" type="{}SearchCriteriaOutgoingDNorSIPURI" maxOccurs="unbounded"/>
 *           <element name="searchCriteriaNetworkTranslatedNumber" type="{}SearchCriteriaOutgoingDNorSIPURI" maxOccurs="unbounded"/>
 *           <element name="searchCriteriaCallingPresentationNumber" type="{}SearchCriteriaOutgoingDNorSIPURI" maxOccurs="unbounded"/>
 *         </choice>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnhancedCallLogsNumberFilter", propOrder = {
    "includeBasicCallType",
    "includeCallCategory",
    "includeConfigurableCallType",
    "searchCriteriaDialedNumber",
    "searchCriteriaCalledNumber",
    "searchCriteriaNetworkTranslatedNumber",
    "searchCriteriaCallingPresentationNumber"
})
public class EnhancedCallLogsNumberFilter {

    @XmlSchemaType(name = "token")
    protected List<BasicCallType> includeBasicCallType;
    @XmlSchemaType(name = "token")
    protected List<CallCategory> includeCallCategory;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> includeConfigurableCallType;
    protected List<SearchCriteriaOutgoingDNorSIPURI> searchCriteriaDialedNumber;
    protected List<SearchCriteriaOutgoingDNorSIPURI> searchCriteriaCalledNumber;
    protected List<SearchCriteriaOutgoingDNorSIPURI> searchCriteriaNetworkTranslatedNumber;
    protected List<SearchCriteriaOutgoingDNorSIPURI> searchCriteriaCallingPresentationNumber;

    /**
     * Gets the value of the includeBasicCallType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the includeBasicCallType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludeBasicCallType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BasicCallType }
     * 
     * 
     * @return
     *     The value of the includeBasicCallType property.
     */
    public List<BasicCallType> getIncludeBasicCallType() {
        if (includeBasicCallType == null) {
            includeBasicCallType = new ArrayList<>();
        }
        return this.includeBasicCallType;
    }

    /**
     * Gets the value of the includeCallCategory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the includeCallCategory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludeCallCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallCategory }
     * 
     * 
     * @return
     *     The value of the includeCallCategory property.
     */
    public List<CallCategory> getIncludeCallCategory() {
        if (includeCallCategory == null) {
            includeCallCategory = new ArrayList<>();
        }
        return this.includeCallCategory;
    }

    /**
     * Gets the value of the includeConfigurableCallType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the includeConfigurableCallType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludeConfigurableCallType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the includeConfigurableCallType property.
     */
    public List<String> getIncludeConfigurableCallType() {
        if (includeConfigurableCallType == null) {
            includeConfigurableCallType = new ArrayList<>();
        }
        return this.includeConfigurableCallType;
    }

    /**
     * Gets the value of the searchCriteriaDialedNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDialedNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDialedNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaOutgoingDNorSIPURI }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDialedNumber property.
     */
    public List<SearchCriteriaOutgoingDNorSIPURI> getSearchCriteriaDialedNumber() {
        if (searchCriteriaDialedNumber == null) {
            searchCriteriaDialedNumber = new ArrayList<>();
        }
        return this.searchCriteriaDialedNumber;
    }

    /**
     * Gets the value of the searchCriteriaCalledNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaCalledNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaCalledNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaOutgoingDNorSIPURI }
     * 
     * 
     * @return
     *     The value of the searchCriteriaCalledNumber property.
     */
    public List<SearchCriteriaOutgoingDNorSIPURI> getSearchCriteriaCalledNumber() {
        if (searchCriteriaCalledNumber == null) {
            searchCriteriaCalledNumber = new ArrayList<>();
        }
        return this.searchCriteriaCalledNumber;
    }

    /**
     * Gets the value of the searchCriteriaNetworkTranslatedNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaNetworkTranslatedNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaNetworkTranslatedNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaOutgoingDNorSIPURI }
     * 
     * 
     * @return
     *     The value of the searchCriteriaNetworkTranslatedNumber property.
     */
    public List<SearchCriteriaOutgoingDNorSIPURI> getSearchCriteriaNetworkTranslatedNumber() {
        if (searchCriteriaNetworkTranslatedNumber == null) {
            searchCriteriaNetworkTranslatedNumber = new ArrayList<>();
        }
        return this.searchCriteriaNetworkTranslatedNumber;
    }

    /**
     * Gets the value of the searchCriteriaCallingPresentationNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaCallingPresentationNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaCallingPresentationNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaOutgoingDNorSIPURI }
     * 
     * 
     * @return
     *     The value of the searchCriteriaCallingPresentationNumber property.
     */
    public List<SearchCriteriaOutgoingDNorSIPURI> getSearchCriteriaCallingPresentationNumber() {
        if (searchCriteriaCallingPresentationNumber == null) {
            searchCriteriaCallingPresentationNumber = new ArrayList<>();
        }
        return this.searchCriteriaCallingPresentationNumber;
    }

}
