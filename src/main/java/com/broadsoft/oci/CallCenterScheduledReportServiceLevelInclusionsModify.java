//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The call center enhanced reporting scheduled report modified inclusions related to the Service Level thresholds 
 *       
 * 
 * <p>Java-Klasse für CallCenterScheduledReportServiceLevelInclusionsModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterScheduledReportServiceLevelInclusionsModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="includeOverflowTimeTransferedInServiceLevel" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeOtherTransfersInServiceLevel" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="abandonedCallsInServiceLevel" type="{}CallCenterReportAbadonedCallsInServiceLevel" minOccurs="0"/>
 *         <element name="abandonedCallIntervalSeconds" type="{}CallCenterReportThresholdSeconds" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterScheduledReportServiceLevelInclusionsModify", propOrder = {
    "includeOverflowTimeTransferedInServiceLevel",
    "includeOtherTransfersInServiceLevel",
    "abandonedCallsInServiceLevel",
    "abandonedCallIntervalSeconds"
})
public class CallCenterScheduledReportServiceLevelInclusionsModify {

    protected Boolean includeOverflowTimeTransferedInServiceLevel;
    protected Boolean includeOtherTransfersInServiceLevel;
    @XmlSchemaType(name = "token")
    protected CallCenterReportAbadonedCallsInServiceLevel abandonedCallsInServiceLevel;
    @XmlElementRef(name = "abandonedCallIntervalSeconds", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> abandonedCallIntervalSeconds;

    /**
     * Ruft den Wert der includeOverflowTimeTransferedInServiceLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeOverflowTimeTransferedInServiceLevel() {
        return includeOverflowTimeTransferedInServiceLevel;
    }

    /**
     * Legt den Wert der includeOverflowTimeTransferedInServiceLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeOverflowTimeTransferedInServiceLevel(Boolean value) {
        this.includeOverflowTimeTransferedInServiceLevel = value;
    }

    /**
     * Ruft den Wert der includeOtherTransfersInServiceLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeOtherTransfersInServiceLevel() {
        return includeOtherTransfersInServiceLevel;
    }

    /**
     * Legt den Wert der includeOtherTransfersInServiceLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeOtherTransfersInServiceLevel(Boolean value) {
        this.includeOtherTransfersInServiceLevel = value;
    }

    /**
     * Ruft den Wert der abandonedCallsInServiceLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportAbadonedCallsInServiceLevel }
     *     
     */
    public CallCenterReportAbadonedCallsInServiceLevel getAbandonedCallsInServiceLevel() {
        return abandonedCallsInServiceLevel;
    }

    /**
     * Legt den Wert der abandonedCallsInServiceLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportAbadonedCallsInServiceLevel }
     *     
     */
    public void setAbandonedCallsInServiceLevel(CallCenterReportAbadonedCallsInServiceLevel value) {
        this.abandonedCallsInServiceLevel = value;
    }

    /**
     * Ruft den Wert der abandonedCallIntervalSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getAbandonedCallIntervalSeconds() {
        return abandonedCallIntervalSeconds;
    }

    /**
     * Legt den Wert der abandonedCallIntervalSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setAbandonedCallIntervalSeconds(JAXBElement<Integer> value) {
        this.abandonedCallIntervalSeconds = value;
    }

}
