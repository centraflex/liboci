//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemPasswordSecurityParametersGetResponse.
 *         The response contains the password security parameters for the system.
 *       
 * 
 * <p>Java-Klasse für SystemPasswordSecurityParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPasswordSecurityParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useExistingHashing" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enforcePasswordChangeOnExpiry" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPasswordSecurityParametersGetResponse", propOrder = {
    "useExistingHashing",
    "enforcePasswordChangeOnExpiry"
})
public class SystemPasswordSecurityParametersGetResponse
    extends OCIDataResponse
{

    protected boolean useExistingHashing;
    protected boolean enforcePasswordChangeOnExpiry;

    /**
     * Ruft den Wert der useExistingHashing-Eigenschaft ab.
     * 
     */
    public boolean isUseExistingHashing() {
        return useExistingHashing;
    }

    /**
     * Legt den Wert der useExistingHashing-Eigenschaft fest.
     * 
     */
    public void setUseExistingHashing(boolean value) {
        this.useExistingHashing = value;
    }

    /**
     * Ruft den Wert der enforcePasswordChangeOnExpiry-Eigenschaft ab.
     * 
     */
    public boolean isEnforcePasswordChangeOnExpiry() {
        return enforcePasswordChangeOnExpiry;
    }

    /**
     * Legt den Wert der enforcePasswordChangeOnExpiry-Eigenschaft fest.
     * 
     */
    public void setEnforcePasswordChangeOnExpiry(boolean value) {
        this.enforcePasswordChangeOnExpiry = value;
    }

}
