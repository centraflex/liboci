//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CPEDeviceConfigRebuildType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CPEDeviceConfigRebuildType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Device Type"/>
 *     <enumeration value="Device Profiles"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CPEDeviceConfigRebuildType")
@XmlEnum
public enum CPEDeviceConfigRebuildType {

    @XmlEnumValue("Device Type")
    DEVICE_TYPE("Device Type"),
    @XmlEnumValue("Device Profiles")
    DEVICE_PROFILES("Device Profiles");
    private final String value;

    CPEDeviceConfigRebuildType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CPEDeviceConfigRebuildType fromValue(String v) {
        for (CPEDeviceConfigRebuildType c: CPEDeviceConfigRebuildType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
