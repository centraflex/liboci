//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *           Request to modify a sip device type file.
 *           The response is either SuccessResponse or ErrorResponse.
 *         
 * 
 * <p>Java-Klasse für SystemSIPDeviceTypeFileModifyRequest14sp8 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPDeviceTypeFileModifyRequest14sp8">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="deviceType" type="{}AccessDeviceType"/>
 *         <element name="fileFormat" type="{}DeviceManagementFileFormat"/>
 *         <element name="allowFileCustomization" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="fileSource" type="{}DeviceTypeFileEnhancedConfigurationMode" minOccurs="0"/>
 *         <element name="uploadFile" type="{}FileResource" minOccurs="0"/>
 *         <element name="useHttpDigestAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="macBasedFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="userNamePasswordFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="macInNonRequestURI" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="macFormatInNonRequestURI" type="{}DeviceManagementAccessURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPDeviceTypeFileModifyRequest14sp8", propOrder = {
    "deviceType",
    "fileFormat",
    "allowFileCustomization",
    "fileSource",
    "uploadFile",
    "useHttpDigestAuthentication",
    "macBasedFileAuthentication",
    "userNamePasswordFileAuthentication",
    "macInNonRequestURI",
    "macFormatInNonRequestURI"
})
public class SystemSIPDeviceTypeFileModifyRequest14Sp8
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceType;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String fileFormat;
    protected Boolean allowFileCustomization;
    @XmlSchemaType(name = "token")
    protected DeviceTypeFileEnhancedConfigurationMode fileSource;
    protected FileResource uploadFile;
    protected Boolean useHttpDigestAuthentication;
    protected Boolean macBasedFileAuthentication;
    protected Boolean userNamePasswordFileAuthentication;
    protected Boolean macInNonRequestURI;
    @XmlElementRef(name = "macFormatInNonRequestURI", type = JAXBElement.class, required = false)
    protected JAXBElement<String> macFormatInNonRequestURI;

    /**
     * Ruft den Wert der deviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Legt den Wert der deviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceType(String value) {
        this.deviceType = value;
    }

    /**
     * Ruft den Wert der fileFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileFormat() {
        return fileFormat;
    }

    /**
     * Legt den Wert der fileFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileFormat(String value) {
        this.fileFormat = value;
    }

    /**
     * Ruft den Wert der allowFileCustomization-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowFileCustomization() {
        return allowFileCustomization;
    }

    /**
     * Legt den Wert der allowFileCustomization-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowFileCustomization(Boolean value) {
        this.allowFileCustomization = value;
    }

    /**
     * Ruft den Wert der fileSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceTypeFileEnhancedConfigurationMode }
     *     
     */
    public DeviceTypeFileEnhancedConfigurationMode getFileSource() {
        return fileSource;
    }

    /**
     * Legt den Wert der fileSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceTypeFileEnhancedConfigurationMode }
     *     
     */
    public void setFileSource(DeviceTypeFileEnhancedConfigurationMode value) {
        this.fileSource = value;
    }

    /**
     * Ruft den Wert der uploadFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link FileResource }
     *     
     */
    public FileResource getUploadFile() {
        return uploadFile;
    }

    /**
     * Legt den Wert der uploadFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link FileResource }
     *     
     */
    public void setUploadFile(FileResource value) {
        this.uploadFile = value;
    }

    /**
     * Ruft den Wert der useHttpDigestAuthentication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseHttpDigestAuthentication() {
        return useHttpDigestAuthentication;
    }

    /**
     * Legt den Wert der useHttpDigestAuthentication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseHttpDigestAuthentication(Boolean value) {
        this.useHttpDigestAuthentication = value;
    }

    /**
     * Ruft den Wert der macBasedFileAuthentication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMacBasedFileAuthentication() {
        return macBasedFileAuthentication;
    }

    /**
     * Legt den Wert der macBasedFileAuthentication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMacBasedFileAuthentication(Boolean value) {
        this.macBasedFileAuthentication = value;
    }

    /**
     * Ruft den Wert der userNamePasswordFileAuthentication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUserNamePasswordFileAuthentication() {
        return userNamePasswordFileAuthentication;
    }

    /**
     * Legt den Wert der userNamePasswordFileAuthentication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUserNamePasswordFileAuthentication(Boolean value) {
        this.userNamePasswordFileAuthentication = value;
    }

    /**
     * Ruft den Wert der macInNonRequestURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMacInNonRequestURI() {
        return macInNonRequestURI;
    }

    /**
     * Legt den Wert der macInNonRequestURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMacInNonRequestURI(Boolean value) {
        this.macInNonRequestURI = value;
    }

    /**
     * Ruft den Wert der macFormatInNonRequestURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMacFormatInNonRequestURI() {
        return macFormatInNonRequestURI;
    }

    /**
     * Legt den Wert der macFormatInNonRequestURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMacFormatInNonRequestURI(JAXBElement<String> value) {
        this.macFormatInNonRequestURI = value;
    }

}
