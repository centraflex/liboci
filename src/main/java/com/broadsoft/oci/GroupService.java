//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GroupService.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="GroupService">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Account/Authorization Codes"/>
 *     <enumeration value="Auto Attendant"/>
 *     <enumeration value="Auto Attendant - Video"/>
 *     <enumeration value="Auto Attendant - Standard"/>
 *     <enumeration value="Call Capacity Management"/>
 *     <enumeration value="Call Park"/>
 *     <enumeration value="Call Pickup"/>
 *     <enumeration value="City-Wide Centrex"/>
 *     <enumeration value="Custom Ringback Group"/>
 *     <enumeration value="Custom Ringback Group - Video"/>
 *     <enumeration value="Emergency Zones"/>
 *     <enumeration value="Enhanced Outgoing Calling Plan"/>
 *     <enumeration value="Find-me/Follow-me"/>
 *     <enumeration value="Group Paging"/>
 *     <enumeration value="Hunt Group"/>
 *     <enumeration value="Incoming Calling Plan"/>
 *     <enumeration value="Instant Group Call"/>
 *     <enumeration value="Intercept Group"/>
 *     <enumeration value="Inventory Report"/>
 *     <enumeration value="LDAP Integration"/>
 *     <enumeration value="Meet-Me Conferencing"/>
 *     <enumeration value="Music On Hold"/>
 *     <enumeration value="Music On Hold - Video"/>
 *     <enumeration value="Outgoing Calling Plan"/>
 *     <enumeration value="Preferred Carrier Group"/>
 *     <enumeration value="Route Point"/>
 *     <enumeration value="Series Completion"/>
 *     <enumeration value="Service Scripts Group"/>
 *     <enumeration value="Trunk Group"/>
 *     <enumeration value="Voice Messaging Group"/>
 *     <enumeration value="VoiceXML"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "GroupService")
@XmlEnum
public enum GroupService {

    @XmlEnumValue("Account/Authorization Codes")
    ACCOUNT_AUTHORIZATION_CODES("Account/Authorization Codes"),
    @XmlEnumValue("Auto Attendant")
    AUTO_ATTENDANT("Auto Attendant"),
    @XmlEnumValue("Auto Attendant - Video")
    AUTO_ATTENDANT_VIDEO("Auto Attendant - Video"),
    @XmlEnumValue("Auto Attendant - Standard")
    AUTO_ATTENDANT_STANDARD("Auto Attendant - Standard"),
    @XmlEnumValue("Call Capacity Management")
    CALL_CAPACITY_MANAGEMENT("Call Capacity Management"),
    @XmlEnumValue("Call Park")
    CALL_PARK("Call Park"),
    @XmlEnumValue("Call Pickup")
    CALL_PICKUP("Call Pickup"),
    @XmlEnumValue("City-Wide Centrex")
    CITY_WIDE_CENTREX("City-Wide Centrex"),
    @XmlEnumValue("Custom Ringback Group")
    CUSTOM_RINGBACK_GROUP("Custom Ringback Group"),
    @XmlEnumValue("Custom Ringback Group - Video")
    CUSTOM_RINGBACK_GROUP_VIDEO("Custom Ringback Group - Video"),
    @XmlEnumValue("Emergency Zones")
    EMERGENCY_ZONES("Emergency Zones"),
    @XmlEnumValue("Enhanced Outgoing Calling Plan")
    ENHANCED_OUTGOING_CALLING_PLAN("Enhanced Outgoing Calling Plan"),
    @XmlEnumValue("Find-me/Follow-me")
    FIND_ME_FOLLOW_ME("Find-me/Follow-me"),
    @XmlEnumValue("Group Paging")
    GROUP_PAGING("Group Paging"),
    @XmlEnumValue("Hunt Group")
    HUNT_GROUP("Hunt Group"),
    @XmlEnumValue("Incoming Calling Plan")
    INCOMING_CALLING_PLAN("Incoming Calling Plan"),
    @XmlEnumValue("Instant Group Call")
    INSTANT_GROUP_CALL("Instant Group Call"),
    @XmlEnumValue("Intercept Group")
    INTERCEPT_GROUP("Intercept Group"),
    @XmlEnumValue("Inventory Report")
    INVENTORY_REPORT("Inventory Report"),
    @XmlEnumValue("LDAP Integration")
    LDAP_INTEGRATION("LDAP Integration"),
    @XmlEnumValue("Meet-Me Conferencing")
    MEET_ME_CONFERENCING("Meet-Me Conferencing"),
    @XmlEnumValue("Music On Hold")
    MUSIC_ON_HOLD("Music On Hold"),
    @XmlEnumValue("Music On Hold - Video")
    MUSIC_ON_HOLD_VIDEO("Music On Hold - Video"),
    @XmlEnumValue("Outgoing Calling Plan")
    OUTGOING_CALLING_PLAN("Outgoing Calling Plan"),
    @XmlEnumValue("Preferred Carrier Group")
    PREFERRED_CARRIER_GROUP("Preferred Carrier Group"),
    @XmlEnumValue("Route Point")
    ROUTE_POINT("Route Point"),
    @XmlEnumValue("Series Completion")
    SERIES_COMPLETION("Series Completion"),
    @XmlEnumValue("Service Scripts Group")
    SERVICE_SCRIPTS_GROUP("Service Scripts Group"),
    @XmlEnumValue("Trunk Group")
    TRUNK_GROUP("Trunk Group"),
    @XmlEnumValue("Voice Messaging Group")
    VOICE_MESSAGING_GROUP("Voice Messaging Group"),
    @XmlEnumValue("VoiceXML")
    VOICE_XML("VoiceXML");
    private final String value;

    GroupService(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupService fromValue(String v) {
        for (GroupService c: GroupService.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
