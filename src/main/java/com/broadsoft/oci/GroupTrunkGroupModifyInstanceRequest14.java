//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a Trunk Group Instance in a group.
 *         The publicUserIdentity in the ServiceInstanceModifyProfile is not allowed for trunk groups.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupTrunkGroupModifyInstanceRequest14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupTrunkGroupModifyInstanceRequest14">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceModifyProfileTrunkGroup" minOccurs="0"/>
 *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointModify" minOccurs="0"/>
 *         <element name="maxActiveCalls" type="{}MaxActiveCalls" minOccurs="0"/>
 *         <element name="maxIncomingCalls" type="{}MaxIncomingCalls" minOccurs="0"/>
 *         <element name="maxOutgoingCalls" type="{}MaxOutgoingCalls" minOccurs="0"/>
 *         <element name="enableBursting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="burstingMaxActiveCalls" type="{}BurstingMaxActiveCalls" minOccurs="0"/>
 *         <element name="burstingMaxIncomingCalls" type="{}BurstingMaxIncomingCalls" minOccurs="0"/>
 *         <element name="burstingMaxOutgoingCalls" type="{}BurstingMaxOutgoingCalls" minOccurs="0"/>
 *         <element name="capacityExceededAction" type="{}TrunkGroupCapacityExceededAction" minOccurs="0"/>
 *         <element name="capacityExceededForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="capacityExceededRerouteTrunkGroupId" type="{}UserId" minOccurs="0"/>
 *         <element name="capacityExceededTrapInitialCalls" type="{}TrapInitialThreshold" minOccurs="0"/>
 *         <element name="capacityExceededTrapOffsetCalls" type="{}TrapOffsetThreshold" minOccurs="0"/>
 *         <element name="unreachableDestinationAction" type="{}TrunkGroupUnreachableDestinationAction" minOccurs="0"/>
 *         <element name="unreachableDestinationForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="unreachableDestinationRerouteTrunkGroupId" type="{}UserId" minOccurs="0"/>
 *         <element name="unreachableDestinationTrapInitialCalls" type="{}TrapInitialThreshold" minOccurs="0"/>
 *         <element name="unreachableDestinationTrapOffsetCalls" type="{}TrapOffsetThreshold" minOccurs="0"/>
 *         <element name="invitationTimeout" type="{}TrunkGroupInvitationTimeoutSeconds" minOccurs="0"/>
 *         <element name="requireAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="sipAuthenticationUserName" type="{}SIPAuthenticationUserName" minOccurs="0"/>
 *         <element name="sipAuthenticationPassword" type="{}SIPAuthenticationPassword" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupTrunkGroupModifyInstanceRequest14", propOrder = {
    "serviceUserId",
    "serviceInstanceProfile",
    "accessDeviceEndpoint",
    "maxActiveCalls",
    "maxIncomingCalls",
    "maxOutgoingCalls",
    "enableBursting",
    "burstingMaxActiveCalls",
    "burstingMaxIncomingCalls",
    "burstingMaxOutgoingCalls",
    "capacityExceededAction",
    "capacityExceededForwardAddress",
    "capacityExceededRerouteTrunkGroupId",
    "capacityExceededTrapInitialCalls",
    "capacityExceededTrapOffsetCalls",
    "unreachableDestinationAction",
    "unreachableDestinationForwardAddress",
    "unreachableDestinationRerouteTrunkGroupId",
    "unreachableDestinationTrapInitialCalls",
    "unreachableDestinationTrapOffsetCalls",
    "invitationTimeout",
    "requireAuthentication",
    "sipAuthenticationUserName",
    "sipAuthenticationPassword"
})
public class GroupTrunkGroupModifyInstanceRequest14
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected ServiceInstanceModifyProfileTrunkGroup serviceInstanceProfile;
    @XmlElementRef(name = "accessDeviceEndpoint", type = JAXBElement.class, required = false)
    protected JAXBElement<AccessDeviceEndpointModify> accessDeviceEndpoint;
    protected Integer maxActiveCalls;
    @XmlElementRef(name = "maxIncomingCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxIncomingCalls;
    @XmlElementRef(name = "maxOutgoingCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxOutgoingCalls;
    protected Boolean enableBursting;
    @XmlElementRef(name = "burstingMaxActiveCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> burstingMaxActiveCalls;
    @XmlElementRef(name = "burstingMaxIncomingCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> burstingMaxIncomingCalls;
    @XmlElementRef(name = "burstingMaxOutgoingCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> burstingMaxOutgoingCalls;
    @XmlElementRef(name = "capacityExceededAction", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupCapacityExceededAction> capacityExceededAction;
    @XmlElementRef(name = "capacityExceededForwardAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> capacityExceededForwardAddress;
    @XmlElementRef(name = "capacityExceededRerouteTrunkGroupId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> capacityExceededRerouteTrunkGroupId;
    protected Integer capacityExceededTrapInitialCalls;
    protected Integer capacityExceededTrapOffsetCalls;
    @XmlElementRef(name = "unreachableDestinationAction", type = JAXBElement.class, required = false)
    protected JAXBElement<TrunkGroupUnreachableDestinationAction> unreachableDestinationAction;
    @XmlElementRef(name = "unreachableDestinationForwardAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> unreachableDestinationForwardAddress;
    @XmlElementRef(name = "unreachableDestinationRerouteTrunkGroupId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> unreachableDestinationRerouteTrunkGroupId;
    protected Integer unreachableDestinationTrapInitialCalls;
    protected Integer unreachableDestinationTrapOffsetCalls;
    protected Integer invitationTimeout;
    protected Boolean requireAuthentication;
    @XmlElementRef(name = "sipAuthenticationUserName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sipAuthenticationUserName;
    @XmlElementRef(name = "sipAuthenticationPassword", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sipAuthenticationPassword;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceModifyProfileTrunkGroup }
     *     
     */
    public ServiceInstanceModifyProfileTrunkGroup getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceModifyProfileTrunkGroup }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceModifyProfileTrunkGroup value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceEndpointModify }{@code >}
     *     
     */
    public JAXBElement<AccessDeviceEndpointModify> getAccessDeviceEndpoint() {
        return accessDeviceEndpoint;
    }

    /**
     * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceEndpointModify }{@code >}
     *     
     */
    public void setAccessDeviceEndpoint(JAXBElement<AccessDeviceEndpointModify> value) {
        this.accessDeviceEndpoint = value;
    }

    /**
     * Ruft den Wert der maxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxActiveCalls() {
        return maxActiveCalls;
    }

    /**
     * Legt den Wert der maxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxActiveCalls(Integer value) {
        this.maxActiveCalls = value;
    }

    /**
     * Ruft den Wert der maxIncomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxIncomingCalls() {
        return maxIncomingCalls;
    }

    /**
     * Legt den Wert der maxIncomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxIncomingCalls(JAXBElement<Integer> value) {
        this.maxIncomingCalls = value;
    }

    /**
     * Ruft den Wert der maxOutgoingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxOutgoingCalls() {
        return maxOutgoingCalls;
    }

    /**
     * Legt den Wert der maxOutgoingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxOutgoingCalls(JAXBElement<Integer> value) {
        this.maxOutgoingCalls = value;
    }

    /**
     * Ruft den Wert der enableBursting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableBursting() {
        return enableBursting;
    }

    /**
     * Legt den Wert der enableBursting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableBursting(Boolean value) {
        this.enableBursting = value;
    }

    /**
     * Ruft den Wert der burstingMaxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getBurstingMaxActiveCalls() {
        return burstingMaxActiveCalls;
    }

    /**
     * Legt den Wert der burstingMaxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setBurstingMaxActiveCalls(JAXBElement<Integer> value) {
        this.burstingMaxActiveCalls = value;
    }

    /**
     * Ruft den Wert der burstingMaxIncomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getBurstingMaxIncomingCalls() {
        return burstingMaxIncomingCalls;
    }

    /**
     * Legt den Wert der burstingMaxIncomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setBurstingMaxIncomingCalls(JAXBElement<Integer> value) {
        this.burstingMaxIncomingCalls = value;
    }

    /**
     * Ruft den Wert der burstingMaxOutgoingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getBurstingMaxOutgoingCalls() {
        return burstingMaxOutgoingCalls;
    }

    /**
     * Legt den Wert der burstingMaxOutgoingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setBurstingMaxOutgoingCalls(JAXBElement<Integer> value) {
        this.burstingMaxOutgoingCalls = value;
    }

    /**
     * Ruft den Wert der capacityExceededAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupCapacityExceededAction }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupCapacityExceededAction> getCapacityExceededAction() {
        return capacityExceededAction;
    }

    /**
     * Legt den Wert der capacityExceededAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupCapacityExceededAction }{@code >}
     *     
     */
    public void setCapacityExceededAction(JAXBElement<TrunkGroupCapacityExceededAction> value) {
        this.capacityExceededAction = value;
    }

    /**
     * Ruft den Wert der capacityExceededForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCapacityExceededForwardAddress() {
        return capacityExceededForwardAddress;
    }

    /**
     * Legt den Wert der capacityExceededForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCapacityExceededForwardAddress(JAXBElement<String> value) {
        this.capacityExceededForwardAddress = value;
    }

    /**
     * Ruft den Wert der capacityExceededRerouteTrunkGroupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCapacityExceededRerouteTrunkGroupId() {
        return capacityExceededRerouteTrunkGroupId;
    }

    /**
     * Legt den Wert der capacityExceededRerouteTrunkGroupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCapacityExceededRerouteTrunkGroupId(JAXBElement<String> value) {
        this.capacityExceededRerouteTrunkGroupId = value;
    }

    /**
     * Ruft den Wert der capacityExceededTrapInitialCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCapacityExceededTrapInitialCalls() {
        return capacityExceededTrapInitialCalls;
    }

    /**
     * Legt den Wert der capacityExceededTrapInitialCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCapacityExceededTrapInitialCalls(Integer value) {
        this.capacityExceededTrapInitialCalls = value;
    }

    /**
     * Ruft den Wert der capacityExceededTrapOffsetCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCapacityExceededTrapOffsetCalls() {
        return capacityExceededTrapOffsetCalls;
    }

    /**
     * Legt den Wert der capacityExceededTrapOffsetCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCapacityExceededTrapOffsetCalls(Integer value) {
        this.capacityExceededTrapOffsetCalls = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupUnreachableDestinationAction }{@code >}
     *     
     */
    public JAXBElement<TrunkGroupUnreachableDestinationAction> getUnreachableDestinationAction() {
        return unreachableDestinationAction;
    }

    /**
     * Legt den Wert der unreachableDestinationAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TrunkGroupUnreachableDestinationAction }{@code >}
     *     
     */
    public void setUnreachableDestinationAction(JAXBElement<TrunkGroupUnreachableDestinationAction> value) {
        this.unreachableDestinationAction = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUnreachableDestinationForwardAddress() {
        return unreachableDestinationForwardAddress;
    }

    /**
     * Legt den Wert der unreachableDestinationForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUnreachableDestinationForwardAddress(JAXBElement<String> value) {
        this.unreachableDestinationForwardAddress = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationRerouteTrunkGroupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUnreachableDestinationRerouteTrunkGroupId() {
        return unreachableDestinationRerouteTrunkGroupId;
    }

    /**
     * Legt den Wert der unreachableDestinationRerouteTrunkGroupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUnreachableDestinationRerouteTrunkGroupId(JAXBElement<String> value) {
        this.unreachableDestinationRerouteTrunkGroupId = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationTrapInitialCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUnreachableDestinationTrapInitialCalls() {
        return unreachableDestinationTrapInitialCalls;
    }

    /**
     * Legt den Wert der unreachableDestinationTrapInitialCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUnreachableDestinationTrapInitialCalls(Integer value) {
        this.unreachableDestinationTrapInitialCalls = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationTrapOffsetCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUnreachableDestinationTrapOffsetCalls() {
        return unreachableDestinationTrapOffsetCalls;
    }

    /**
     * Legt den Wert der unreachableDestinationTrapOffsetCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUnreachableDestinationTrapOffsetCalls(Integer value) {
        this.unreachableDestinationTrapOffsetCalls = value;
    }

    /**
     * Ruft den Wert der invitationTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInvitationTimeout() {
        return invitationTimeout;
    }

    /**
     * Legt den Wert der invitationTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInvitationTimeout(Integer value) {
        this.invitationTimeout = value;
    }

    /**
     * Ruft den Wert der requireAuthentication-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequireAuthentication() {
        return requireAuthentication;
    }

    /**
     * Legt den Wert der requireAuthentication-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequireAuthentication(Boolean value) {
        this.requireAuthentication = value;
    }

    /**
     * Ruft den Wert der sipAuthenticationUserName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSipAuthenticationUserName() {
        return sipAuthenticationUserName;
    }

    /**
     * Legt den Wert der sipAuthenticationUserName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSipAuthenticationUserName(JAXBElement<String> value) {
        this.sipAuthenticationUserName = value;
    }

    /**
     * Ruft den Wert der sipAuthenticationPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSipAuthenticationPassword() {
        return sipAuthenticationPassword;
    }

    /**
     * Legt den Wert der sipAuthenticationPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSipAuthenticationPassword(JAXBElement<String> value) {
        this.sipAuthenticationPassword = value;
    }

}
