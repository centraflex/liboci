//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *          Response to a GroupPreAlertingAnnouncementGetResponse20.
 *       
 * 
 * <p>Java-Klasse für GroupPreAlertingAnnouncementGetResponse20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupPreAlertingAnnouncementGetResponse20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="announcementInterruption" type="{}PreAlertingAnnouncementInterrupt"/>
 *         <element name="interruptionDigitSequence" type="{}PreAlertingAnnouncementInterruptDigits" minOccurs="0"/>
 *         <element name="audioSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="audioFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *         <element name="audioFileUrl" type="{}URL" minOccurs="0"/>
 *         <element name="videoSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="videoFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *         <element name="videoFileUrl" type="{}URL" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupPreAlertingAnnouncementGetResponse20", propOrder = {
    "announcementInterruption",
    "interruptionDigitSequence",
    "audioSelection",
    "audioFile",
    "audioFileUrl",
    "videoSelection",
    "videoFile",
    "videoFileUrl"
})
public class GroupPreAlertingAnnouncementGetResponse20
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected PreAlertingAnnouncementInterrupt announcementInterruption;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String interruptionDigitSequence;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection audioSelection;
    protected AnnouncementFileKey audioFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String audioFileUrl;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection videoSelection;
    protected AnnouncementFileKey videoFile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String videoFileUrl;

    /**
     * Ruft den Wert der announcementInterruption-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PreAlertingAnnouncementInterrupt }
     *     
     */
    public PreAlertingAnnouncementInterrupt getAnnouncementInterruption() {
        return announcementInterruption;
    }

    /**
     * Legt den Wert der announcementInterruption-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PreAlertingAnnouncementInterrupt }
     *     
     */
    public void setAnnouncementInterruption(PreAlertingAnnouncementInterrupt value) {
        this.announcementInterruption = value;
    }

    /**
     * Ruft den Wert der interruptionDigitSequence-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterruptionDigitSequence() {
        return interruptionDigitSequence;
    }

    /**
     * Legt den Wert der interruptionDigitSequence-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterruptionDigitSequence(String value) {
        this.interruptionDigitSequence = value;
    }

    /**
     * Ruft den Wert der audioSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getAudioSelection() {
        return audioSelection;
    }

    /**
     * Legt den Wert der audioSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setAudioSelection(ExtendedFileResourceSelection value) {
        this.audioSelection = value;
    }

    /**
     * Ruft den Wert der audioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileKey }
     *     
     */
    public AnnouncementFileKey getAudioFile() {
        return audioFile;
    }

    /**
     * Legt den Wert der audioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileKey }
     *     
     */
    public void setAudioFile(AnnouncementFileKey value) {
        this.audioFile = value;
    }

    /**
     * Ruft den Wert der audioFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioFileUrl() {
        return audioFileUrl;
    }

    /**
     * Legt den Wert der audioFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioFileUrl(String value) {
        this.audioFileUrl = value;
    }

    /**
     * Ruft den Wert der videoSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getVideoSelection() {
        return videoSelection;
    }

    /**
     * Legt den Wert der videoSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setVideoSelection(ExtendedFileResourceSelection value) {
        this.videoSelection = value;
    }

    /**
     * Ruft den Wert der videoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileKey }
     *     
     */
    public AnnouncementFileKey getVideoFile() {
        return videoFile;
    }

    /**
     * Legt den Wert der videoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileKey }
     *     
     */
    public void setVideoFile(AnnouncementFileKey value) {
        this.videoFile = value;
    }

    /**
     * Ruft den Wert der videoFileUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVideoFileUrl() {
        return videoFileUrl;
    }

    /**
     * Legt den Wert der videoFileUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVideoFileUrl(String value) {
        this.videoFileUrl = value;
    }

}
