//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemSystemServiceDnGetUsageListRequest.
 *         The table columns are:  "Phone Number", "Id",
 *         "Name", and "System Service".
 *         The possible values for "System Service" is "System Voice Portal".
 *       
 * 
 * <p>Java-Klasse für SystemSystemServiceDnGetUsageListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSystemServiceDnGetUsageListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="dnUtilizationTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSystemServiceDnGetUsageListResponse", propOrder = {
    "dnUtilizationTable"
})
public class SystemSystemServiceDnGetUsageListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable dnUtilizationTable;

    /**
     * Ruft den Wert der dnUtilizationTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDnUtilizationTable() {
        return dnUtilizationTable;
    }

    /**
     * Legt den Wert der dnUtilizationTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDnUtilizationTable(OCITable value) {
        this.dnUtilizationTable = value;
    }

}
