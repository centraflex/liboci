//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für MediaBandwidthEnforcementType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="MediaBandwidthEnforcementType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Allow All"/>
 *     <enumeration value="Allow Unspecified"/>
 *     <enumeration value="Enforce All"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "MediaBandwidthEnforcementType")
@XmlEnum
public enum MediaBandwidthEnforcementType {

    @XmlEnumValue("Allow All")
    ALLOW_ALL("Allow All"),
    @XmlEnumValue("Allow Unspecified")
    ALLOW_UNSPECIFIED("Allow Unspecified"),
    @XmlEnumValue("Enforce All")
    ENFORCE_ALL("Enforce All");
    private final String value;

    MediaBandwidthEnforcementType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MediaBandwidthEnforcementType fromValue(String v) {
        for (MediaBandwidthEnforcementType c: MediaBandwidthEnforcementType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
