//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to get the System Number Portability Query Digit Pattern List information.
 *         The response is either a SystemNumberPortabilityQueryDigitPatternGetListResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemNumberPortabilityQueryDigitPatternGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNumberPortabilityQueryDigitPatternGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="searchCriteriaNumberPortabilityQueryDigitPattern" type="{}SearchCriteriaNumberPortabilityQueryDigitPattern" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaNumberPortabilityStatus" type="{}SearchCriteriaNumberPortabilityStatus" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNumberPortabilityQueryDigitPatternGetListRequest", propOrder = {
    "searchCriteriaNumberPortabilityQueryDigitPattern",
    "searchCriteriaNumberPortabilityStatus",
    "responseSizeLimit"
})
public class SystemNumberPortabilityQueryDigitPatternGetListRequest
    extends OCIRequest
{

    protected List<SearchCriteriaNumberPortabilityQueryDigitPattern> searchCriteriaNumberPortabilityQueryDigitPattern;
    protected List<SearchCriteriaNumberPortabilityStatus> searchCriteriaNumberPortabilityStatus;
    protected Integer responseSizeLimit;

    /**
     * Gets the value of the searchCriteriaNumberPortabilityQueryDigitPattern property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaNumberPortabilityQueryDigitPattern property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaNumberPortabilityQueryDigitPattern().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaNumberPortabilityQueryDigitPattern }
     * 
     * 
     * @return
     *     The value of the searchCriteriaNumberPortabilityQueryDigitPattern property.
     */
    public List<SearchCriteriaNumberPortabilityQueryDigitPattern> getSearchCriteriaNumberPortabilityQueryDigitPattern() {
        if (searchCriteriaNumberPortabilityQueryDigitPattern == null) {
            searchCriteriaNumberPortabilityQueryDigitPattern = new ArrayList<>();
        }
        return this.searchCriteriaNumberPortabilityQueryDigitPattern;
    }

    /**
     * Gets the value of the searchCriteriaNumberPortabilityStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaNumberPortabilityStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaNumberPortabilityStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaNumberPortabilityStatus }
     * 
     * 
     * @return
     *     The value of the searchCriteriaNumberPortabilityStatus property.
     */
    public List<SearchCriteriaNumberPortabilityStatus> getSearchCriteriaNumberPortabilityStatus() {
        if (searchCriteriaNumberPortabilityStatus == null) {
            searchCriteriaNumberPortabilityStatus = new ArrayList<>();
        }
        return this.searchCriteriaNumberPortabilityStatus;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

}
