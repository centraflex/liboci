//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to get the list of all active call center reporting scheduled reports in the system.
 *         The response is either a SystemCallCenterEnhancedReportingScheduledReportGetActiveListResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCallCenterEnhancedReportingScheduledReportGetActiveListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallCenterEnhancedReportingScheduledReportGetActiveListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaCallCenterScheduledReportName" type="{}SearchCriteriaCallCenterScheduledReportName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaGroupId" type="{}SearchCriteriaGroupId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactCallCenterScheduledReportServiceProvider" type="{}SearchCriteriaExactCallCenterScheduledReportServiceProvider" minOccurs="0"/>
 *         <element name="searchCriteriaServiceProviderId" type="{}SearchCriteriaServiceProviderId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactCallCenterScheduledReportCreatedBySupervisor" type="{}SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor" minOccurs="0"/>
 *         <element name="searchCriteriaCallCenterReportTemplateName" type="{}SearchCriteriaCallCenterReportTemplateName" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallCenterEnhancedReportingScheduledReportGetActiveListRequest", propOrder = {
    "responseSizeLimit",
    "searchCriteriaCallCenterScheduledReportName",
    "searchCriteriaGroupId",
    "searchCriteriaExactCallCenterScheduledReportServiceProvider",
    "searchCriteriaServiceProviderId",
    "searchCriteriaExactCallCenterScheduledReportCreatedBySupervisor",
    "searchCriteriaCallCenterReportTemplateName"
})
public class SystemCallCenterEnhancedReportingScheduledReportGetActiveListRequest
    extends OCIRequest
{

    protected Integer responseSizeLimit;
    protected List<SearchCriteriaCallCenterScheduledReportName> searchCriteriaCallCenterScheduledReportName;
    protected List<SearchCriteriaGroupId> searchCriteriaGroupId;
    protected SearchCriteriaExactCallCenterScheduledReportServiceProvider searchCriteriaExactCallCenterScheduledReportServiceProvider;
    protected List<SearchCriteriaServiceProviderId> searchCriteriaServiceProviderId;
    protected SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor searchCriteriaExactCallCenterScheduledReportCreatedBySupervisor;
    protected List<SearchCriteriaCallCenterReportTemplateName> searchCriteriaCallCenterReportTemplateName;

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaCallCenterScheduledReportName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaCallCenterScheduledReportName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaCallCenterScheduledReportName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaCallCenterScheduledReportName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaCallCenterScheduledReportName property.
     */
    public List<SearchCriteriaCallCenterScheduledReportName> getSearchCriteriaCallCenterScheduledReportName() {
        if (searchCriteriaCallCenterScheduledReportName == null) {
            searchCriteriaCallCenterScheduledReportName = new ArrayList<>();
        }
        return this.searchCriteriaCallCenterScheduledReportName;
    }

    /**
     * Gets the value of the searchCriteriaGroupId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaGroupId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaGroupId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaGroupId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaGroupId property.
     */
    public List<SearchCriteriaGroupId> getSearchCriteriaGroupId() {
        if (searchCriteriaGroupId == null) {
            searchCriteriaGroupId = new ArrayList<>();
        }
        return this.searchCriteriaGroupId;
    }

    /**
     * Ruft den Wert der searchCriteriaExactCallCenterScheduledReportServiceProvider-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactCallCenterScheduledReportServiceProvider }
     *     
     */
    public SearchCriteriaExactCallCenterScheduledReportServiceProvider getSearchCriteriaExactCallCenterScheduledReportServiceProvider() {
        return searchCriteriaExactCallCenterScheduledReportServiceProvider;
    }

    /**
     * Legt den Wert der searchCriteriaExactCallCenterScheduledReportServiceProvider-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactCallCenterScheduledReportServiceProvider }
     *     
     */
    public void setSearchCriteriaExactCallCenterScheduledReportServiceProvider(SearchCriteriaExactCallCenterScheduledReportServiceProvider value) {
        this.searchCriteriaExactCallCenterScheduledReportServiceProvider = value;
    }

    /**
     * Gets the value of the searchCriteriaServiceProviderId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaServiceProviderId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaServiceProviderId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaServiceProviderId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaServiceProviderId property.
     */
    public List<SearchCriteriaServiceProviderId> getSearchCriteriaServiceProviderId() {
        if (searchCriteriaServiceProviderId == null) {
            searchCriteriaServiceProviderId = new ArrayList<>();
        }
        return this.searchCriteriaServiceProviderId;
    }

    /**
     * Ruft den Wert der searchCriteriaExactCallCenterScheduledReportCreatedBySupervisor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor }
     *     
     */
    public SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor getSearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor() {
        return searchCriteriaExactCallCenterScheduledReportCreatedBySupervisor;
    }

    /**
     * Legt den Wert der searchCriteriaExactCallCenterScheduledReportCreatedBySupervisor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor }
     *     
     */
    public void setSearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor(SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor value) {
        this.searchCriteriaExactCallCenterScheduledReportCreatedBySupervisor = value;
    }

    /**
     * Gets the value of the searchCriteriaCallCenterReportTemplateName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaCallCenterReportTemplateName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaCallCenterReportTemplateName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaCallCenterReportTemplateName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaCallCenterReportTemplateName property.
     */
    public List<SearchCriteriaCallCenterReportTemplateName> getSearchCriteriaCallCenterReportTemplateName() {
        if (searchCriteriaCallCenterReportTemplateName == null) {
            searchCriteriaCallCenterReportTemplateName = new ArrayList<>();
        }
        return this.searchCriteriaCallCenterReportTemplateName;
    }

}
