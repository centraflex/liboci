//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *           Modifies the system's DTMF based in-call service activation trigger  attributes.The response is either a SuccessResponse or an ErrorResponse.
 *         
 * 
 * <p>Java-Klasse für SystemInCallServiceActivationModifyRequest17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemInCallServiceActivationModifyRequest17">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="defaultFlashActivationDigits" type="{}InCallServiceActivationDigits" minOccurs="0"/>
 *         <element name="defaultCallTransferActivationDigits" type="{}InCallServiceActivationDigits" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemInCallServiceActivationModifyRequest17", propOrder = {
    "defaultFlashActivationDigits",
    "defaultCallTransferActivationDigits"
})
public class SystemInCallServiceActivationModifyRequest17
    extends OCIRequest
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultFlashActivationDigits;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultCallTransferActivationDigits;

    /**
     * Ruft den Wert der defaultFlashActivationDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultFlashActivationDigits() {
        return defaultFlashActivationDigits;
    }

    /**
     * Legt den Wert der defaultFlashActivationDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultFlashActivationDigits(String value) {
        this.defaultFlashActivationDigits = value;
    }

    /**
     * Ruft den Wert der defaultCallTransferActivationDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultCallTransferActivationDigits() {
        return defaultCallTransferActivationDigits;
    }

    /**
     * Legt den Wert der defaultCallTransferActivationDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultCallTransferActivationDigits(String value) {
        this.defaultCallTransferActivationDigits = value;
    }

}
