//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TrunkGroupUnscreenedRedirectionHandling.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="TrunkGroupUnscreenedRedirectionHandling">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Reject"/>
 *     <enumeration value="Ignore"/>
 *     <enumeration value="Ignore If Unscreened Calls Disallowed"/>
 *     <enumeration value="Reject If Unscreened Calls Disallowed"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TrunkGroupUnscreenedRedirectionHandling")
@XmlEnum
public enum TrunkGroupUnscreenedRedirectionHandling {

    @XmlEnumValue("Reject")
    REJECT("Reject"),
    @XmlEnumValue("Ignore")
    IGNORE("Ignore"),
    @XmlEnumValue("Ignore If Unscreened Calls Disallowed")
    IGNORE_IF_UNSCREENED_CALLS_DISALLOWED("Ignore If Unscreened Calls Disallowed"),
    @XmlEnumValue("Reject If Unscreened Calls Disallowed")
    REJECT_IF_UNSCREENED_CALLS_DISALLOWED("Reject If Unscreened Calls Disallowed");
    private final String value;

    TrunkGroupUnscreenedRedirectionHandling(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrunkGroupUnscreenedRedirectionHandling fromValue(String v) {
        for (TrunkGroupUnscreenedRedirectionHandling c: TrunkGroupUnscreenedRedirectionHandling.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
