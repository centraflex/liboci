//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallToNumberType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallToNumberType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Primary"/>
 *     <enumeration value="Alternate1"/>
 *     <enumeration value="Alternate2"/>
 *     <enumeration value="Alternate3"/>
 *     <enumeration value="Alternate4"/>
 *     <enumeration value="Alternate5"/>
 *     <enumeration value="Alternate6"/>
 *     <enumeration value="Alternate7"/>
 *     <enumeration value="Alternate8"/>
 *     <enumeration value="Alternate9"/>
 *     <enumeration value="Alternate10"/>
 *     <enumeration value="BroadWorks Mobility"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallToNumberType")
@XmlEnum
public enum CallToNumberType {

    @XmlEnumValue("Primary")
    PRIMARY("Primary"),
    @XmlEnumValue("Alternate1")
    ALTERNATE_1("Alternate1"),
    @XmlEnumValue("Alternate2")
    ALTERNATE_2("Alternate2"),
    @XmlEnumValue("Alternate3")
    ALTERNATE_3("Alternate3"),
    @XmlEnumValue("Alternate4")
    ALTERNATE_4("Alternate4"),
    @XmlEnumValue("Alternate5")
    ALTERNATE_5("Alternate5"),
    @XmlEnumValue("Alternate6")
    ALTERNATE_6("Alternate6"),
    @XmlEnumValue("Alternate7")
    ALTERNATE_7("Alternate7"),
    @XmlEnumValue("Alternate8")
    ALTERNATE_8("Alternate8"),
    @XmlEnumValue("Alternate9")
    ALTERNATE_9("Alternate9"),
    @XmlEnumValue("Alternate10")
    ALTERNATE_10("Alternate10"),
    @XmlEnumValue("BroadWorks Mobility")
    BROAD_WORKS_MOBILITY("BroadWorks Mobility");
    private final String value;

    CallToNumberType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallToNumberType fromValue(String v) {
        for (CallToNumberType c: CallToNumberType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
