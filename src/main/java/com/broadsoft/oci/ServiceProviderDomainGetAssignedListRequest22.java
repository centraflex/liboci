//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Requests the list of all matching domains assigned to a service provider .
 *         The response is either ServiceProviderDomainGetAssignedListResponse22 or ErrorResponse.
 * 
 *         The following elements are only used in AS data mode and will be ignored in XS data mode:
 *           responseSizeLimit
 *           searchCriteriaDomainName
 *           searchCriteriaExactDomainLevel
 *       
 * 
 * <p>Java-Klasse für ServiceProviderDomainGetAssignedListRequest22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderDomainGetAssignedListRequest22">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaDomainName" type="{}SearchCriteriaDomainName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactDomainLevel" type="{}SearchCriteriaExactDomainLevel" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderDomainGetAssignedListRequest22", propOrder = {
    "serviceProviderId",
    "responseSizeLimit",
    "searchCriteriaDomainName",
    "searchCriteriaExactDomainLevel"
})
public class ServiceProviderDomainGetAssignedListRequest22
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected Integer responseSizeLimit;
    protected List<SearchCriteriaDomainName> searchCriteriaDomainName;
    protected SearchCriteriaExactDomainLevel searchCriteriaExactDomainLevel;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaDomainName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDomainName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDomainName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDomainName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDomainName property.
     */
    public List<SearchCriteriaDomainName> getSearchCriteriaDomainName() {
        if (searchCriteriaDomainName == null) {
            searchCriteriaDomainName = new ArrayList<>();
        }
        return this.searchCriteriaDomainName;
    }

    /**
     * Ruft den Wert der searchCriteriaExactDomainLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactDomainLevel }
     *     
     */
    public SearchCriteriaExactDomainLevel getSearchCriteriaExactDomainLevel() {
        return searchCriteriaExactDomainLevel;
    }

    /**
     * Legt den Wert der searchCriteriaExactDomainLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactDomainLevel }
     *     
     */
    public void setSearchCriteriaExactDomainLevel(SearchCriteriaExactDomainLevel value) {
        this.searchCriteriaExactDomainLevel = value;
    }

}
