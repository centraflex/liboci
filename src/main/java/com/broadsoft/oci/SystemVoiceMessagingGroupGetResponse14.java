//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemVoiceMessagingGroupGetRequest14.
 *         
 *         Replaced by: SystemVoiceMessagingGroupGetResponse16
 *       
 * 
 * <p>Java-Klasse für SystemVoiceMessagingGroupGetResponse14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemVoiceMessagingGroupGetResponse14">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="realDeleteForImap" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useDnInMailBody" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useShortSubjectLine" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxGreetingLengthMinutes" type="{}VoiceMessagingMaxGreetingLengthMinutes"/>
 *         <element name="maxMessageLengthMinutes" type="{}VoiceMessagingMaxMessageLengthMinutes"/>
 *         <element name="maxMailboxLengthMinutes" type="{}VoiceMessagingMailboxLengthMinutes"/>
 *         <element name="doesMessageAge" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="holdPeriodDays" type="{}VoiceMessagingHoldPeriodDays"/>
 *         <element name="mailServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="mailServerProtocol" type="{}VoiceMessagingMailServerProtocol"/>
 *         <element name="defaultDeliveryFromAddress" type="{}EmailAddress"/>
 *         <element name="defaultNotificationFromAddress" type="{}EmailAddress"/>
 *         <element name="defaultVoicePortalLockoutFromAddress" type="{}EmailAddress"/>
 *         <element name="useOutgoingMWIOnSMDI" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="mwiDelayInSeconds" type="{}VoiceMessagingMessageWaitingIndicatorDelayInSeconds"/>
 *         <element name="voicePortalScope" type="{}SystemVoicePortalScope"/>
 *         <element name="enterpriseVoicePortalLicensed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="networkWideMessaging" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useExternalRouting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="defaultExternalRoutingAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemVoiceMessagingGroupGetResponse14", propOrder = {
    "realDeleteForImap",
    "useDnInMailBody",
    "useShortSubjectLine",
    "maxGreetingLengthMinutes",
    "maxMessageLengthMinutes",
    "maxMailboxLengthMinutes",
    "doesMessageAge",
    "holdPeriodDays",
    "mailServerNetAddress",
    "mailServerProtocol",
    "defaultDeliveryFromAddress",
    "defaultNotificationFromAddress",
    "defaultVoicePortalLockoutFromAddress",
    "useOutgoingMWIOnSMDI",
    "mwiDelayInSeconds",
    "voicePortalScope",
    "enterpriseVoicePortalLicensed",
    "networkWideMessaging",
    "useExternalRouting",
    "defaultExternalRoutingAddress"
})
public class SystemVoiceMessagingGroupGetResponse14
    extends OCIDataResponse
{

    protected boolean realDeleteForImap;
    protected boolean useDnInMailBody;
    protected boolean useShortSubjectLine;
    protected int maxGreetingLengthMinutes;
    protected int maxMessageLengthMinutes;
    protected int maxMailboxLengthMinutes;
    protected boolean doesMessageAge;
    protected int holdPeriodDays;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mailServerNetAddress;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected VoiceMessagingMailServerProtocol mailServerProtocol;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultDeliveryFromAddress;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultNotificationFromAddress;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultVoicePortalLockoutFromAddress;
    protected boolean useOutgoingMWIOnSMDI;
    protected int mwiDelayInSeconds;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SystemVoicePortalScope voicePortalScope;
    protected boolean enterpriseVoicePortalLicensed;
    protected boolean networkWideMessaging;
    protected boolean useExternalRouting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultExternalRoutingAddress;

    /**
     * Ruft den Wert der realDeleteForImap-Eigenschaft ab.
     * 
     */
    public boolean isRealDeleteForImap() {
        return realDeleteForImap;
    }

    /**
     * Legt den Wert der realDeleteForImap-Eigenschaft fest.
     * 
     */
    public void setRealDeleteForImap(boolean value) {
        this.realDeleteForImap = value;
    }

    /**
     * Ruft den Wert der useDnInMailBody-Eigenschaft ab.
     * 
     */
    public boolean isUseDnInMailBody() {
        return useDnInMailBody;
    }

    /**
     * Legt den Wert der useDnInMailBody-Eigenschaft fest.
     * 
     */
    public void setUseDnInMailBody(boolean value) {
        this.useDnInMailBody = value;
    }

    /**
     * Ruft den Wert der useShortSubjectLine-Eigenschaft ab.
     * 
     */
    public boolean isUseShortSubjectLine() {
        return useShortSubjectLine;
    }

    /**
     * Legt den Wert der useShortSubjectLine-Eigenschaft fest.
     * 
     */
    public void setUseShortSubjectLine(boolean value) {
        this.useShortSubjectLine = value;
    }

    /**
     * Ruft den Wert der maxGreetingLengthMinutes-Eigenschaft ab.
     * 
     */
    public int getMaxGreetingLengthMinutes() {
        return maxGreetingLengthMinutes;
    }

    /**
     * Legt den Wert der maxGreetingLengthMinutes-Eigenschaft fest.
     * 
     */
    public void setMaxGreetingLengthMinutes(int value) {
        this.maxGreetingLengthMinutes = value;
    }

    /**
     * Ruft den Wert der maxMessageLengthMinutes-Eigenschaft ab.
     * 
     */
    public int getMaxMessageLengthMinutes() {
        return maxMessageLengthMinutes;
    }

    /**
     * Legt den Wert der maxMessageLengthMinutes-Eigenschaft fest.
     * 
     */
    public void setMaxMessageLengthMinutes(int value) {
        this.maxMessageLengthMinutes = value;
    }

    /**
     * Ruft den Wert der maxMailboxLengthMinutes-Eigenschaft ab.
     * 
     */
    public int getMaxMailboxLengthMinutes() {
        return maxMailboxLengthMinutes;
    }

    /**
     * Legt den Wert der maxMailboxLengthMinutes-Eigenschaft fest.
     * 
     */
    public void setMaxMailboxLengthMinutes(int value) {
        this.maxMailboxLengthMinutes = value;
    }

    /**
     * Ruft den Wert der doesMessageAge-Eigenschaft ab.
     * 
     */
    public boolean isDoesMessageAge() {
        return doesMessageAge;
    }

    /**
     * Legt den Wert der doesMessageAge-Eigenschaft fest.
     * 
     */
    public void setDoesMessageAge(boolean value) {
        this.doesMessageAge = value;
    }

    /**
     * Ruft den Wert der holdPeriodDays-Eigenschaft ab.
     * 
     */
    public int getHoldPeriodDays() {
        return holdPeriodDays;
    }

    /**
     * Legt den Wert der holdPeriodDays-Eigenschaft fest.
     * 
     */
    public void setHoldPeriodDays(int value) {
        this.holdPeriodDays = value;
    }

    /**
     * Ruft den Wert der mailServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailServerNetAddress() {
        return mailServerNetAddress;
    }

    /**
     * Legt den Wert der mailServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailServerNetAddress(String value) {
        this.mailServerNetAddress = value;
    }

    /**
     * Ruft den Wert der mailServerProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingMailServerProtocol }
     *     
     */
    public VoiceMessagingMailServerProtocol getMailServerProtocol() {
        return mailServerProtocol;
    }

    /**
     * Legt den Wert der mailServerProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingMailServerProtocol }
     *     
     */
    public void setMailServerProtocol(VoiceMessagingMailServerProtocol value) {
        this.mailServerProtocol = value;
    }

    /**
     * Ruft den Wert der defaultDeliveryFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultDeliveryFromAddress() {
        return defaultDeliveryFromAddress;
    }

    /**
     * Legt den Wert der defaultDeliveryFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultDeliveryFromAddress(String value) {
        this.defaultDeliveryFromAddress = value;
    }

    /**
     * Ruft den Wert der defaultNotificationFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultNotificationFromAddress() {
        return defaultNotificationFromAddress;
    }

    /**
     * Legt den Wert der defaultNotificationFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultNotificationFromAddress(String value) {
        this.defaultNotificationFromAddress = value;
    }

    /**
     * Ruft den Wert der defaultVoicePortalLockoutFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultVoicePortalLockoutFromAddress() {
        return defaultVoicePortalLockoutFromAddress;
    }

    /**
     * Legt den Wert der defaultVoicePortalLockoutFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultVoicePortalLockoutFromAddress(String value) {
        this.defaultVoicePortalLockoutFromAddress = value;
    }

    /**
     * Ruft den Wert der useOutgoingMWIOnSMDI-Eigenschaft ab.
     * 
     */
    public boolean isUseOutgoingMWIOnSMDI() {
        return useOutgoingMWIOnSMDI;
    }

    /**
     * Legt den Wert der useOutgoingMWIOnSMDI-Eigenschaft fest.
     * 
     */
    public void setUseOutgoingMWIOnSMDI(boolean value) {
        this.useOutgoingMWIOnSMDI = value;
    }

    /**
     * Ruft den Wert der mwiDelayInSeconds-Eigenschaft ab.
     * 
     */
    public int getMwiDelayInSeconds() {
        return mwiDelayInSeconds;
    }

    /**
     * Legt den Wert der mwiDelayInSeconds-Eigenschaft fest.
     * 
     */
    public void setMwiDelayInSeconds(int value) {
        this.mwiDelayInSeconds = value;
    }

    /**
     * Ruft den Wert der voicePortalScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoicePortalScope }
     *     
     */
    public SystemVoicePortalScope getVoicePortalScope() {
        return voicePortalScope;
    }

    /**
     * Legt den Wert der voicePortalScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoicePortalScope }
     *     
     */
    public void setVoicePortalScope(SystemVoicePortalScope value) {
        this.voicePortalScope = value;
    }

    /**
     * Ruft den Wert der enterpriseVoicePortalLicensed-Eigenschaft ab.
     * 
     */
    public boolean isEnterpriseVoicePortalLicensed() {
        return enterpriseVoicePortalLicensed;
    }

    /**
     * Legt den Wert der enterpriseVoicePortalLicensed-Eigenschaft fest.
     * 
     */
    public void setEnterpriseVoicePortalLicensed(boolean value) {
        this.enterpriseVoicePortalLicensed = value;
    }

    /**
     * Ruft den Wert der networkWideMessaging-Eigenschaft ab.
     * 
     */
    public boolean isNetworkWideMessaging() {
        return networkWideMessaging;
    }

    /**
     * Legt den Wert der networkWideMessaging-Eigenschaft fest.
     * 
     */
    public void setNetworkWideMessaging(boolean value) {
        this.networkWideMessaging = value;
    }

    /**
     * Ruft den Wert der useExternalRouting-Eigenschaft ab.
     * 
     */
    public boolean isUseExternalRouting() {
        return useExternalRouting;
    }

    /**
     * Legt den Wert der useExternalRouting-Eigenschaft fest.
     * 
     */
    public void setUseExternalRouting(boolean value) {
        this.useExternalRouting = value;
    }

    /**
     * Ruft den Wert der defaultExternalRoutingAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultExternalRoutingAddress() {
        return defaultExternalRoutingAddress;
    }

    /**
     * Legt den Wert der defaultExternalRoutingAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultExternalRoutingAddress(String value) {
        this.defaultExternalRoutingAddress = value;
    }

}
