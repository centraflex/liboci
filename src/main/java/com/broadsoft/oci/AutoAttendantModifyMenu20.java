//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The configuration of the automated receptionist greeting
 *         prompt and dialing menu to be used during business hours.
 *         It is used when modifying an Auto Attendant group.
 *       
 * 
 * <p>Java-Klasse für AutoAttendantModifyMenu20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="AutoAttendantModifyMenu20">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="announcementSelection" type="{}AnnouncementSelection" minOccurs="0"/>
 *         <element name="audioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="videoFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="enableFirstMenuLevelExtensionDialing" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="keyConfiguration" type="{}AutoAttendantKeyModifyConfiguration20" maxOccurs="12" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutoAttendantModifyMenu20", propOrder = {
    "announcementSelection",
    "audioFile",
    "videoFile",
    "enableFirstMenuLevelExtensionDialing",
    "keyConfiguration"
})
public class AutoAttendantModifyMenu20 {

    @XmlSchemaType(name = "token")
    protected AnnouncementSelection announcementSelection;
    @XmlElementRef(name = "audioFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> audioFile;
    @XmlElementRef(name = "videoFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> videoFile;
    protected Boolean enableFirstMenuLevelExtensionDialing;
    protected List<AutoAttendantKeyModifyConfiguration20> keyConfiguration;

    /**
     * Ruft den Wert der announcementSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementSelection }
     *     
     */
    public AnnouncementSelection getAnnouncementSelection() {
        return announcementSelection;
    }

    /**
     * Legt den Wert der announcementSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementSelection }
     *     
     */
    public void setAnnouncementSelection(AnnouncementSelection value) {
        this.announcementSelection = value;
    }

    /**
     * Ruft den Wert der audioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getAudioFile() {
        return audioFile;
    }

    /**
     * Legt den Wert der audioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setAudioFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.audioFile = value;
    }

    /**
     * Ruft den Wert der videoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getVideoFile() {
        return videoFile;
    }

    /**
     * Legt den Wert der videoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setVideoFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.videoFile = value;
    }

    /**
     * Ruft den Wert der enableFirstMenuLevelExtensionDialing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableFirstMenuLevelExtensionDialing() {
        return enableFirstMenuLevelExtensionDialing;
    }

    /**
     * Legt den Wert der enableFirstMenuLevelExtensionDialing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableFirstMenuLevelExtensionDialing(Boolean value) {
        this.enableFirstMenuLevelExtensionDialing = value;
    }

    /**
     * Gets the value of the keyConfiguration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the keyConfiguration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKeyConfiguration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AutoAttendantKeyModifyConfiguration20 }
     * 
     * 
     * @return
     *     The value of the keyConfiguration property.
     */
    public List<AutoAttendantKeyModifyConfiguration20> getKeyConfiguration() {
        if (keyConfiguration == null) {
            keyConfiguration = new ArrayList<>();
        }
        return this.keyConfiguration;
    }

}
