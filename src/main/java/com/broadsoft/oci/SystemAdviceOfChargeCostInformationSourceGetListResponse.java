//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemAdviceOfChargeCostInformationSourceGetListRequest.
 *         Contains a table with column headings: "PeerIdentity", "Priority"
 *       
 * 
 * <p>Java-Klasse für SystemAdviceOfChargeCostInformationSourceGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAdviceOfChargeCostInformationSourceGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="costInformationSourceTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAdviceOfChargeCostInformationSourceGetListResponse", propOrder = {
    "costInformationSourceTable"
})
public class SystemAdviceOfChargeCostInformationSourceGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable costInformationSourceTable;

    /**
     * Ruft den Wert der costInformationSourceTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getCostInformationSourceTable() {
        return costInformationSourceTable;
    }

    /**
     * Legt den Wert der costInformationSourceTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setCostInformationSourceTable(OCITable value) {
        this.costInformationSourceTable = value;
    }

}
