//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Either all agents or list of agents.
 *         The agent table has the following column headings:
 *         "User Id", "Last Name", "First Name", "Hiragana Last Name" and "Hiragana First Name".
 *       
 * 
 * <p>Java-Klasse für CallCenterScheduledReportAgentSelectionRead complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterScheduledReportAgentSelectionRead">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <choice>
 *         <element name="allAgent" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="agentTable" type="{C}OCITable"/>
 *       </choice>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterScheduledReportAgentSelectionRead", propOrder = {
    "allAgent",
    "agentTable"
})
public class CallCenterScheduledReportAgentSelectionRead {

    protected Boolean allAgent;
    protected OCITable agentTable;

    /**
     * Ruft den Wert der allAgent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllAgent() {
        return allAgent;
    }

    /**
     * Legt den Wert der allAgent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllAgent(Boolean value) {
        this.allAgent = value;
    }

    /**
     * Ruft den Wert der agentTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAgentTable() {
        return agentTable;
    }

    /**
     * Legt den Wert der agentTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAgentTable(OCITable value) {
        this.agentTable = value;
    }

}
