//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a Q850 Cause Value mapping.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemTreatmentMappingQ850CauseAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemTreatmentMappingQ850CauseAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="q850CauseValue" type="{}Q850CauseValue"/>
 *         <element name="treatmentId" type="{}TreatmentId"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemTreatmentMappingQ850CauseAddRequest", propOrder = {
    "q850CauseValue",
    "treatmentId"
})
public class SystemTreatmentMappingQ850CauseAddRequest
    extends OCIRequest
{

    protected int q850CauseValue;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String treatmentId;

    /**
     * Ruft den Wert der q850CauseValue-Eigenschaft ab.
     * 
     */
    public int getQ850CauseValue() {
        return q850CauseValue;
    }

    /**
     * Legt den Wert der q850CauseValue-Eigenschaft fest.
     * 
     */
    public void setQ850CauseValue(int value) {
        this.q850CauseValue = value;
    }

    /**
     * Ruft den Wert der treatmentId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTreatmentId() {
        return treatmentId;
    }

    /**
     * Legt den Wert der treatmentId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTreatmentId(String value) {
        this.treatmentId = value;
    }

}
