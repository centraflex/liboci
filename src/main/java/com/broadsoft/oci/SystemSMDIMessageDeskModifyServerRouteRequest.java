//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Replace the list of devices associated with a SMDI server route destination.
 *         There must be at least one device in the list.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemSMDIMessageDeskModifyServerRouteRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSMDIMessageDeskModifyServerRouteRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="routeDestination" type="{}SMDIServerRouteDestination"/>
 *         <element name="deviceNameList" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="deviceName" type="{}SMDIDeviceName" maxOccurs="unbounded"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSMDIMessageDeskModifyServerRouteRequest", propOrder = {
    "routeDestination",
    "deviceNameList"
})
public class SystemSMDIMessageDeskModifyServerRouteRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String routeDestination;
    protected SystemSMDIMessageDeskModifyServerRouteRequest.DeviceNameList deviceNameList;

    /**
     * Ruft den Wert der routeDestination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRouteDestination() {
        return routeDestination;
    }

    /**
     * Legt den Wert der routeDestination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRouteDestination(String value) {
        this.routeDestination = value;
    }

    /**
     * Ruft den Wert der deviceNameList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemSMDIMessageDeskModifyServerRouteRequest.DeviceNameList }
     *     
     */
    public SystemSMDIMessageDeskModifyServerRouteRequest.DeviceNameList getDeviceNameList() {
        return deviceNameList;
    }

    /**
     * Legt den Wert der deviceNameList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemSMDIMessageDeskModifyServerRouteRequest.DeviceNameList }
     *     
     */
    public void setDeviceNameList(SystemSMDIMessageDeskModifyServerRouteRequest.DeviceNameList value) {
        this.deviceNameList = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="deviceName" type="{}SMDIDeviceName" maxOccurs="unbounded"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "deviceName"
    })
    public static class DeviceNameList {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> deviceName;

        /**
         * Gets the value of the deviceName property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the deviceName property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDeviceName().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the deviceName property.
         */
        public List<String> getDeviceName() {
            if (deviceName == null) {
                deviceName = new ArrayList<>();
            }
            return this.deviceName;
        }

    }

}
