//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a criteria for the user's call forwarding selective service.
 *         The following elements are only used in AS data mode:
 *           callToNumber
 *           
 *         For the callToNumbers in the callToNumberList, the extension element is not used and the number element is only used when the type is BroadWorks Mobility.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserCallForwardingSelectiveModifyCriteriaRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallForwardingSelectiveModifyCriteriaRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="criteriaName" type="{}CriteriaName"/>
 *         <element name="newCriteriaName" type="{}CriteriaName" minOccurs="0"/>
 *         <element name="timeSchedule" type="{}TimeSchedule" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}HolidaySchedule" minOccurs="0"/>
 *         <element name="forwardToNumberSelection" type="{}CallForwardingSelectiveNumberSelection16" minOccurs="0"/>
 *         <element name="forwardToPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="fromDnCriteria" type="{}CriteriaFromDnModify" minOccurs="0"/>
 *         <element name="callToNumberList" type="{}ReplacementCallToNumberList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallForwardingSelectiveModifyCriteriaRequest", propOrder = {
    "userId",
    "criteriaName",
    "newCriteriaName",
    "timeSchedule",
    "holidaySchedule",
    "forwardToNumberSelection",
    "forwardToPhoneNumber",
    "fromDnCriteria",
    "callToNumberList"
})
public class UserCallForwardingSelectiveModifyCriteriaRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String criteriaName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newCriteriaName;
    @XmlElementRef(name = "timeSchedule", type = JAXBElement.class, required = false)
    protected JAXBElement<TimeSchedule> timeSchedule;
    @XmlElementRef(name = "holidaySchedule", type = JAXBElement.class, required = false)
    protected JAXBElement<HolidaySchedule> holidaySchedule;
    @XmlSchemaType(name = "token")
    protected CallForwardingSelectiveNumberSelection16 forwardToNumberSelection;
    @XmlElementRef(name = "forwardToPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forwardToPhoneNumber;
    protected CriteriaFromDnModify fromDnCriteria;
    @XmlElementRef(name = "callToNumberList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementCallToNumberList> callToNumberList;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der criteriaName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCriteriaName() {
        return criteriaName;
    }

    /**
     * Legt den Wert der criteriaName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCriteriaName(String value) {
        this.criteriaName = value;
    }

    /**
     * Ruft den Wert der newCriteriaName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewCriteriaName() {
        return newCriteriaName;
    }

    /**
     * Legt den Wert der newCriteriaName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewCriteriaName(String value) {
        this.newCriteriaName = value;
    }

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TimeSchedule }{@code >}
     *     
     */
    public JAXBElement<TimeSchedule> getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TimeSchedule }{@code >}
     *     
     */
    public void setTimeSchedule(JAXBElement<TimeSchedule> value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link HolidaySchedule }{@code >}
     *     
     */
    public JAXBElement<HolidaySchedule> getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link HolidaySchedule }{@code >}
     *     
     */
    public void setHolidaySchedule(JAXBElement<HolidaySchedule> value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der forwardToNumberSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallForwardingSelectiveNumberSelection16 }
     *     
     */
    public CallForwardingSelectiveNumberSelection16 getForwardToNumberSelection() {
        return forwardToNumberSelection;
    }

    /**
     * Legt den Wert der forwardToNumberSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallForwardingSelectiveNumberSelection16 }
     *     
     */
    public void setForwardToNumberSelection(CallForwardingSelectiveNumberSelection16 value) {
        this.forwardToNumberSelection = value;
    }

    /**
     * Ruft den Wert der forwardToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForwardToPhoneNumber() {
        return forwardToPhoneNumber;
    }

    /**
     * Legt den Wert der forwardToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForwardToPhoneNumber(JAXBElement<String> value) {
        this.forwardToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der fromDnCriteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaFromDnModify }
     *     
     */
    public CriteriaFromDnModify getFromDnCriteria() {
        return fromDnCriteria;
    }

    /**
     * Legt den Wert der fromDnCriteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaFromDnModify }
     *     
     */
    public void setFromDnCriteria(CriteriaFromDnModify value) {
        this.fromDnCriteria = value;
    }

    /**
     * Ruft den Wert der callToNumberList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCallToNumberList }{@code >}
     *     
     */
    public JAXBElement<ReplacementCallToNumberList> getCallToNumberList() {
        return callToNumberList;
    }

    /**
     * Legt den Wert der callToNumberList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCallToNumberList }{@code >}
     *     
     */
    public void setCallToNumberList(JAXBElement<ReplacementCallToNumberList> value) {
        this.callToNumberList = value;
    }

}
