//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to add an enterprise trunk in an enterprise.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         Replaced by: EnterpriseEnterpriseTrunkAddRequest21
 *       
 * 
 * <p>Java-Klasse für EnterpriseEnterpriseTrunkAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseEnterpriseTrunkAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="enterpriseTrunkName" type="{}EnterpriseTrunkName"/>
 *         <element name="maximumRerouteAttempts" type="{}EnterpriseTrunkMaximumRerouteAttempts"/>
 *         <element name="routeExhaustionAction" type="{}EnterpriseTrunkRouteExhaustionAction"/>
 *         <element name="routeExhaustionForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <choice>
 *           <element name="orderedRouting">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="trunkGroup" type="{}EnterpriseTrunkTrunkGroupKey" maxOccurs="10" minOccurs="0"/>
 *                     <element name="orderingAlgorithm" type="{}EnterpriseTrunkOrderingAlgorithm"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *           <element name="priorityWeightedRouting">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="maximumRerouteAttemptsWithinPriority" type="{}EnterpriseTrunkMaximumRerouteAttempts"/>
 *                     <element name="priorityWeightedTrunkGroup" type="{}EnterpriseEnterpriseTrunkPriorityWeightedTrunkGroup" maxOccurs="100" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseEnterpriseTrunkAddRequest", propOrder = {
    "serviceProviderId",
    "enterpriseTrunkName",
    "maximumRerouteAttempts",
    "routeExhaustionAction",
    "routeExhaustionForwardAddress",
    "orderedRouting",
    "priorityWeightedRouting"
})
public class EnterpriseEnterpriseTrunkAddRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String enterpriseTrunkName;
    protected int maximumRerouteAttempts;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseTrunkRouteExhaustionAction routeExhaustionAction;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String routeExhaustionForwardAddress;
    protected EnterpriseEnterpriseTrunkAddRequest.OrderedRouting orderedRouting;
    protected EnterpriseEnterpriseTrunkAddRequest.PriorityWeightedRouting priorityWeightedRouting;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der enterpriseTrunkName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnterpriseTrunkName() {
        return enterpriseTrunkName;
    }

    /**
     * Legt den Wert der enterpriseTrunkName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnterpriseTrunkName(String value) {
        this.enterpriseTrunkName = value;
    }

    /**
     * Ruft den Wert der maximumRerouteAttempts-Eigenschaft ab.
     * 
     */
    public int getMaximumRerouteAttempts() {
        return maximumRerouteAttempts;
    }

    /**
     * Legt den Wert der maximumRerouteAttempts-Eigenschaft fest.
     * 
     */
    public void setMaximumRerouteAttempts(int value) {
        this.maximumRerouteAttempts = value;
    }

    /**
     * Ruft den Wert der routeExhaustionAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseTrunkRouteExhaustionAction }
     *     
     */
    public EnterpriseTrunkRouteExhaustionAction getRouteExhaustionAction() {
        return routeExhaustionAction;
    }

    /**
     * Legt den Wert der routeExhaustionAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseTrunkRouteExhaustionAction }
     *     
     */
    public void setRouteExhaustionAction(EnterpriseTrunkRouteExhaustionAction value) {
        this.routeExhaustionAction = value;
    }

    /**
     * Ruft den Wert der routeExhaustionForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRouteExhaustionForwardAddress() {
        return routeExhaustionForwardAddress;
    }

    /**
     * Legt den Wert der routeExhaustionForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRouteExhaustionForwardAddress(String value) {
        this.routeExhaustionForwardAddress = value;
    }

    /**
     * Ruft den Wert der orderedRouting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseEnterpriseTrunkAddRequest.OrderedRouting }
     *     
     */
    public EnterpriseEnterpriseTrunkAddRequest.OrderedRouting getOrderedRouting() {
        return orderedRouting;
    }

    /**
     * Legt den Wert der orderedRouting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseEnterpriseTrunkAddRequest.OrderedRouting }
     *     
     */
    public void setOrderedRouting(EnterpriseEnterpriseTrunkAddRequest.OrderedRouting value) {
        this.orderedRouting = value;
    }

    /**
     * Ruft den Wert der priorityWeightedRouting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseEnterpriseTrunkAddRequest.PriorityWeightedRouting }
     *     
     */
    public EnterpriseEnterpriseTrunkAddRequest.PriorityWeightedRouting getPriorityWeightedRouting() {
        return priorityWeightedRouting;
    }

    /**
     * Legt den Wert der priorityWeightedRouting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseEnterpriseTrunkAddRequest.PriorityWeightedRouting }
     *     
     */
    public void setPriorityWeightedRouting(EnterpriseEnterpriseTrunkAddRequest.PriorityWeightedRouting value) {
        this.priorityWeightedRouting = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="trunkGroup" type="{}EnterpriseTrunkTrunkGroupKey" maxOccurs="10" minOccurs="0"/>
     *         <element name="orderingAlgorithm" type="{}EnterpriseTrunkOrderingAlgorithm"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "trunkGroup",
        "orderingAlgorithm"
    })
    public static class OrderedRouting {

        protected List<EnterpriseTrunkTrunkGroupKey> trunkGroup;
        @XmlElement(required = true)
        @XmlSchemaType(name = "token")
        protected EnterpriseTrunkOrderingAlgorithm orderingAlgorithm;

        /**
         * Gets the value of the trunkGroup property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the trunkGroup property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTrunkGroup().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link EnterpriseTrunkTrunkGroupKey }
         * 
         * 
         * @return
         *     The value of the trunkGroup property.
         */
        public List<EnterpriseTrunkTrunkGroupKey> getTrunkGroup() {
            if (trunkGroup == null) {
                trunkGroup = new ArrayList<>();
            }
            return this.trunkGroup;
        }

        /**
         * Ruft den Wert der orderingAlgorithm-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link EnterpriseTrunkOrderingAlgorithm }
         *     
         */
        public EnterpriseTrunkOrderingAlgorithm getOrderingAlgorithm() {
            return orderingAlgorithm;
        }

        /**
         * Legt den Wert der orderingAlgorithm-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link EnterpriseTrunkOrderingAlgorithm }
         *     
         */
        public void setOrderingAlgorithm(EnterpriseTrunkOrderingAlgorithm value) {
            this.orderingAlgorithm = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="maximumRerouteAttemptsWithinPriority" type="{}EnterpriseTrunkMaximumRerouteAttempts"/>
     *         <element name="priorityWeightedTrunkGroup" type="{}EnterpriseEnterpriseTrunkPriorityWeightedTrunkGroup" maxOccurs="100" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "maximumRerouteAttemptsWithinPriority",
        "priorityWeightedTrunkGroup"
    })
    public static class PriorityWeightedRouting {

        protected int maximumRerouteAttemptsWithinPriority;
        protected List<EnterpriseEnterpriseTrunkPriorityWeightedTrunkGroup> priorityWeightedTrunkGroup;

        /**
         * Ruft den Wert der maximumRerouteAttemptsWithinPriority-Eigenschaft ab.
         * 
         */
        public int getMaximumRerouteAttemptsWithinPriority() {
            return maximumRerouteAttemptsWithinPriority;
        }

        /**
         * Legt den Wert der maximumRerouteAttemptsWithinPriority-Eigenschaft fest.
         * 
         */
        public void setMaximumRerouteAttemptsWithinPriority(int value) {
            this.maximumRerouteAttemptsWithinPriority = value;
        }

        /**
         * Gets the value of the priorityWeightedTrunkGroup property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the priorityWeightedTrunkGroup property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPriorityWeightedTrunkGroup().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link EnterpriseEnterpriseTrunkPriorityWeightedTrunkGroup }
         * 
         * 
         * @return
         *     The value of the priorityWeightedTrunkGroup property.
         */
        public List<EnterpriseEnterpriseTrunkPriorityWeightedTrunkGroup> getPriorityWeightedTrunkGroup() {
            if (priorityWeightedTrunkGroup == null) {
                priorityWeightedTrunkGroup = new ArrayList<>();
            }
            return this.priorityWeightedTrunkGroup;
        }

    }

}
