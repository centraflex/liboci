//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TrunkGroupUserCreationSIPURIFormat.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="TrunkGroupUserCreationSIPURIFormat">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Extension"/>
 *     <enumeration value="National DN"/>
 *     <enumeration value="E164 Format No Plus"/>
 *     <enumeration value="E164 Format"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TrunkGroupUserCreationSIPURIFormat")
@XmlEnum
public enum TrunkGroupUserCreationSIPURIFormat {

    @XmlEnumValue("Extension")
    EXTENSION("Extension"),
    @XmlEnumValue("National DN")
    NATIONAL_DN("National DN"),
    @XmlEnumValue("E164 Format No Plus")
    E_164_FORMAT_NO_PLUS("E164 Format No Plus"),
    @XmlEnumValue("E164 Format")
    E_164_FORMAT("E164 Format");
    private final String value;

    TrunkGroupUserCreationSIPURIFormat(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrunkGroupUserCreationSIPURIFormat fromValue(String v) {
        for (TrunkGroupUserCreationSIPURIFormat c: TrunkGroupUserCreationSIPURIFormat.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
