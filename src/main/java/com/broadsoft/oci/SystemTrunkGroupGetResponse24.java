//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemTrunkGroupGetRequest24.
 *         Following attributes are only used in IMS mode:
 *           implicitRegistrationSetSupportPolicy
 *           sipIdentityForPilotAndProxyTrunkModesPolicy
 *           useMostRecentEntryOnDeflection
 *       
 * 
 * <p>Java-Klasse für SystemTrunkGroupGetResponse24 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemTrunkGroupGetResponse24">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enforceCLIDServiceAssignmentForPilotUser" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="terminateUnreachableTriggerDetectionOnReceiptOf18x" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="pilotUserCallingLineAssertedIdentityPolicy" type="{}TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy"/>
 *         <element name="enforceOutOfDialogPBXRedirectionPolicies" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="unscreenedRedirectionHandling" type="{}TrunkGroupUnscreenedRedirectionHandling"/>
 *         <element name="enableHoldoverOfHighwaterCallCounts" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="holdoverPeriod" type="{}TrunkGroupHighwaterCallCountHoldoverPeriodMinutes"/>
 *         <element name="timeZoneOffsetMinutes" type="{}TrunkGroupTimeZoneOffsetMinutes"/>
 *         <element name="clidSourceForScreenedCallsPolicy" type="{}TrunkGroupCLIDSourceForScreenedCallsPolicy"/>
 *         <element name="userLookupPolicy" type="{}TrunkGroupUserLookupPolicy"/>
 *         <element name="outOfDialogPBXRedirectionCLIDMapping" type="{}TrunkGroupOutOfDialogPBXRedirectionCLIDMapping"/>
 *         <element name="enforceOutOfDialogPBXRedirectionTrunkGroupCapacity" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="implicitRegistrationSetSupportPolicy" type="{}TrunkGroupImplicitRegistrationSetSupportPolicy"/>
 *         <element name="sipIdentityForPilotAndProxyTrunkModesPolicy" type="{}TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy"/>
 *         <element name="supportConnectedIdentityPolicy" type="{}TrunkGroupSupportConnectedIdentityPolicy"/>
 *         <element name="useUnmappedSessionsForTrunkUsers" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowPAILookupForOutOfDialogPBXRedirection" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="outOfDialogPBXRedirectionOriginatorLookupPolicy" type="{}TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy"/>
 *         <element name="allowTrunkIdentityForAllOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useMostRecentEntryOnDeflection" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemTrunkGroupGetResponse24", propOrder = {
    "enforceCLIDServiceAssignmentForPilotUser",
    "terminateUnreachableTriggerDetectionOnReceiptOf18X",
    "pilotUserCallingLineAssertedIdentityPolicy",
    "enforceOutOfDialogPBXRedirectionPolicies",
    "unscreenedRedirectionHandling",
    "enableHoldoverOfHighwaterCallCounts",
    "holdoverPeriod",
    "timeZoneOffsetMinutes",
    "clidSourceForScreenedCallsPolicy",
    "userLookupPolicy",
    "outOfDialogPBXRedirectionCLIDMapping",
    "enforceOutOfDialogPBXRedirectionTrunkGroupCapacity",
    "implicitRegistrationSetSupportPolicy",
    "sipIdentityForPilotAndProxyTrunkModesPolicy",
    "supportConnectedIdentityPolicy",
    "useUnmappedSessionsForTrunkUsers",
    "allowPAILookupForOutOfDialogPBXRedirection",
    "outOfDialogPBXRedirectionOriginatorLookupPolicy",
    "allowTrunkIdentityForAllOriginations",
    "useMostRecentEntryOnDeflection"
})
public class SystemTrunkGroupGetResponse24
    extends OCIDataResponse
{

    protected boolean enforceCLIDServiceAssignmentForPilotUser;
    @XmlElement(name = "terminateUnreachableTriggerDetectionOnReceiptOf18x")
    protected boolean terminateUnreachableTriggerDetectionOnReceiptOf18X;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy pilotUserCallingLineAssertedIdentityPolicy;
    protected boolean enforceOutOfDialogPBXRedirectionPolicies;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupUnscreenedRedirectionHandling unscreenedRedirectionHandling;
    protected boolean enableHoldoverOfHighwaterCallCounts;
    protected int holdoverPeriod;
    protected int timeZoneOffsetMinutes;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupCLIDSourceForScreenedCallsPolicy clidSourceForScreenedCallsPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupUserLookupPolicy userLookupPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupOutOfDialogPBXRedirectionCLIDMapping outOfDialogPBXRedirectionCLIDMapping;
    protected boolean enforceOutOfDialogPBXRedirectionTrunkGroupCapacity;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupImplicitRegistrationSetSupportPolicy implicitRegistrationSetSupportPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy sipIdentityForPilotAndProxyTrunkModesPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupSupportConnectedIdentityPolicy supportConnectedIdentityPolicy;
    protected boolean useUnmappedSessionsForTrunkUsers;
    protected boolean allowPAILookupForOutOfDialogPBXRedirection;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy outOfDialogPBXRedirectionOriginatorLookupPolicy;
    protected boolean allowTrunkIdentityForAllOriginations;
    protected boolean useMostRecentEntryOnDeflection;

    /**
     * Ruft den Wert der enforceCLIDServiceAssignmentForPilotUser-Eigenschaft ab.
     * 
     */
    public boolean isEnforceCLIDServiceAssignmentForPilotUser() {
        return enforceCLIDServiceAssignmentForPilotUser;
    }

    /**
     * Legt den Wert der enforceCLIDServiceAssignmentForPilotUser-Eigenschaft fest.
     * 
     */
    public void setEnforceCLIDServiceAssignmentForPilotUser(boolean value) {
        this.enforceCLIDServiceAssignmentForPilotUser = value;
    }

    /**
     * Ruft den Wert der terminateUnreachableTriggerDetectionOnReceiptOf18X-Eigenschaft ab.
     * 
     */
    public boolean isTerminateUnreachableTriggerDetectionOnReceiptOf18X() {
        return terminateUnreachableTriggerDetectionOnReceiptOf18X;
    }

    /**
     * Legt den Wert der terminateUnreachableTriggerDetectionOnReceiptOf18X-Eigenschaft fest.
     * 
     */
    public void setTerminateUnreachableTriggerDetectionOnReceiptOf18X(boolean value) {
        this.terminateUnreachableTriggerDetectionOnReceiptOf18X = value;
    }

    /**
     * Ruft den Wert der pilotUserCallingLineAssertedIdentityPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy }
     *     
     */
    public TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy getPilotUserCallingLineAssertedIdentityPolicy() {
        return pilotUserCallingLineAssertedIdentityPolicy;
    }

    /**
     * Legt den Wert der pilotUserCallingLineAssertedIdentityPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy }
     *     
     */
    public void setPilotUserCallingLineAssertedIdentityPolicy(TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy value) {
        this.pilotUserCallingLineAssertedIdentityPolicy = value;
    }

    /**
     * Ruft den Wert der enforceOutOfDialogPBXRedirectionPolicies-Eigenschaft ab.
     * 
     */
    public boolean isEnforceOutOfDialogPBXRedirectionPolicies() {
        return enforceOutOfDialogPBXRedirectionPolicies;
    }

    /**
     * Legt den Wert der enforceOutOfDialogPBXRedirectionPolicies-Eigenschaft fest.
     * 
     */
    public void setEnforceOutOfDialogPBXRedirectionPolicies(boolean value) {
        this.enforceOutOfDialogPBXRedirectionPolicies = value;
    }

    /**
     * Ruft den Wert der unscreenedRedirectionHandling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupUnscreenedRedirectionHandling }
     *     
     */
    public TrunkGroupUnscreenedRedirectionHandling getUnscreenedRedirectionHandling() {
        return unscreenedRedirectionHandling;
    }

    /**
     * Legt den Wert der unscreenedRedirectionHandling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupUnscreenedRedirectionHandling }
     *     
     */
    public void setUnscreenedRedirectionHandling(TrunkGroupUnscreenedRedirectionHandling value) {
        this.unscreenedRedirectionHandling = value;
    }

    /**
     * Ruft den Wert der enableHoldoverOfHighwaterCallCounts-Eigenschaft ab.
     * 
     */
    public boolean isEnableHoldoverOfHighwaterCallCounts() {
        return enableHoldoverOfHighwaterCallCounts;
    }

    /**
     * Legt den Wert der enableHoldoverOfHighwaterCallCounts-Eigenschaft fest.
     * 
     */
    public void setEnableHoldoverOfHighwaterCallCounts(boolean value) {
        this.enableHoldoverOfHighwaterCallCounts = value;
    }

    /**
     * Ruft den Wert der holdoverPeriod-Eigenschaft ab.
     * 
     */
    public int getHoldoverPeriod() {
        return holdoverPeriod;
    }

    /**
     * Legt den Wert der holdoverPeriod-Eigenschaft fest.
     * 
     */
    public void setHoldoverPeriod(int value) {
        this.holdoverPeriod = value;
    }

    /**
     * Ruft den Wert der timeZoneOffsetMinutes-Eigenschaft ab.
     * 
     */
    public int getTimeZoneOffsetMinutes() {
        return timeZoneOffsetMinutes;
    }

    /**
     * Legt den Wert der timeZoneOffsetMinutes-Eigenschaft fest.
     * 
     */
    public void setTimeZoneOffsetMinutes(int value) {
        this.timeZoneOffsetMinutes = value;
    }

    /**
     * Ruft den Wert der clidSourceForScreenedCallsPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupCLIDSourceForScreenedCallsPolicy }
     *     
     */
    public TrunkGroupCLIDSourceForScreenedCallsPolicy getClidSourceForScreenedCallsPolicy() {
        return clidSourceForScreenedCallsPolicy;
    }

    /**
     * Legt den Wert der clidSourceForScreenedCallsPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupCLIDSourceForScreenedCallsPolicy }
     *     
     */
    public void setClidSourceForScreenedCallsPolicy(TrunkGroupCLIDSourceForScreenedCallsPolicy value) {
        this.clidSourceForScreenedCallsPolicy = value;
    }

    /**
     * Ruft den Wert der userLookupPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupUserLookupPolicy }
     *     
     */
    public TrunkGroupUserLookupPolicy getUserLookupPolicy() {
        return userLookupPolicy;
    }

    /**
     * Legt den Wert der userLookupPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupUserLookupPolicy }
     *     
     */
    public void setUserLookupPolicy(TrunkGroupUserLookupPolicy value) {
        this.userLookupPolicy = value;
    }

    /**
     * Ruft den Wert der outOfDialogPBXRedirectionCLIDMapping-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupOutOfDialogPBXRedirectionCLIDMapping }
     *     
     */
    public TrunkGroupOutOfDialogPBXRedirectionCLIDMapping getOutOfDialogPBXRedirectionCLIDMapping() {
        return outOfDialogPBXRedirectionCLIDMapping;
    }

    /**
     * Legt den Wert der outOfDialogPBXRedirectionCLIDMapping-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupOutOfDialogPBXRedirectionCLIDMapping }
     *     
     */
    public void setOutOfDialogPBXRedirectionCLIDMapping(TrunkGroupOutOfDialogPBXRedirectionCLIDMapping value) {
        this.outOfDialogPBXRedirectionCLIDMapping = value;
    }

    /**
     * Ruft den Wert der enforceOutOfDialogPBXRedirectionTrunkGroupCapacity-Eigenschaft ab.
     * 
     */
    public boolean isEnforceOutOfDialogPBXRedirectionTrunkGroupCapacity() {
        return enforceOutOfDialogPBXRedirectionTrunkGroupCapacity;
    }

    /**
     * Legt den Wert der enforceOutOfDialogPBXRedirectionTrunkGroupCapacity-Eigenschaft fest.
     * 
     */
    public void setEnforceOutOfDialogPBXRedirectionTrunkGroupCapacity(boolean value) {
        this.enforceOutOfDialogPBXRedirectionTrunkGroupCapacity = value;
    }

    /**
     * Ruft den Wert der implicitRegistrationSetSupportPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupImplicitRegistrationSetSupportPolicy }
     *     
     */
    public TrunkGroupImplicitRegistrationSetSupportPolicy getImplicitRegistrationSetSupportPolicy() {
        return implicitRegistrationSetSupportPolicy;
    }

    /**
     * Legt den Wert der implicitRegistrationSetSupportPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupImplicitRegistrationSetSupportPolicy }
     *     
     */
    public void setImplicitRegistrationSetSupportPolicy(TrunkGroupImplicitRegistrationSetSupportPolicy value) {
        this.implicitRegistrationSetSupportPolicy = value;
    }

    /**
     * Ruft den Wert der sipIdentityForPilotAndProxyTrunkModesPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy }
     *     
     */
    public TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy getSipIdentityForPilotAndProxyTrunkModesPolicy() {
        return sipIdentityForPilotAndProxyTrunkModesPolicy;
    }

    /**
     * Legt den Wert der sipIdentityForPilotAndProxyTrunkModesPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy }
     *     
     */
    public void setSipIdentityForPilotAndProxyTrunkModesPolicy(TrunkGroupSIPIdentityForPilotAndProxyTrunkModesPolicy value) {
        this.sipIdentityForPilotAndProxyTrunkModesPolicy = value;
    }

    /**
     * Ruft den Wert der supportConnectedIdentityPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupSupportConnectedIdentityPolicy }
     *     
     */
    public TrunkGroupSupportConnectedIdentityPolicy getSupportConnectedIdentityPolicy() {
        return supportConnectedIdentityPolicy;
    }

    /**
     * Legt den Wert der supportConnectedIdentityPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupSupportConnectedIdentityPolicy }
     *     
     */
    public void setSupportConnectedIdentityPolicy(TrunkGroupSupportConnectedIdentityPolicy value) {
        this.supportConnectedIdentityPolicy = value;
    }

    /**
     * Ruft den Wert der useUnmappedSessionsForTrunkUsers-Eigenschaft ab.
     * 
     */
    public boolean isUseUnmappedSessionsForTrunkUsers() {
        return useUnmappedSessionsForTrunkUsers;
    }

    /**
     * Legt den Wert der useUnmappedSessionsForTrunkUsers-Eigenschaft fest.
     * 
     */
    public void setUseUnmappedSessionsForTrunkUsers(boolean value) {
        this.useUnmappedSessionsForTrunkUsers = value;
    }

    /**
     * Ruft den Wert der allowPAILookupForOutOfDialogPBXRedirection-Eigenschaft ab.
     * 
     */
    public boolean isAllowPAILookupForOutOfDialogPBXRedirection() {
        return allowPAILookupForOutOfDialogPBXRedirection;
    }

    /**
     * Legt den Wert der allowPAILookupForOutOfDialogPBXRedirection-Eigenschaft fest.
     * 
     */
    public void setAllowPAILookupForOutOfDialogPBXRedirection(boolean value) {
        this.allowPAILookupForOutOfDialogPBXRedirection = value;
    }

    /**
     * Ruft den Wert der outOfDialogPBXRedirectionOriginatorLookupPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy }
     *     
     */
    public TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy getOutOfDialogPBXRedirectionOriginatorLookupPolicy() {
        return outOfDialogPBXRedirectionOriginatorLookupPolicy;
    }

    /**
     * Legt den Wert der outOfDialogPBXRedirectionOriginatorLookupPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy }
     *     
     */
    public void setOutOfDialogPBXRedirectionOriginatorLookupPolicy(TrunkGroupOutOfDialogPBXRedirectionOriginatorLookupPolicy value) {
        this.outOfDialogPBXRedirectionOriginatorLookupPolicy = value;
    }

    /**
     * Ruft den Wert der allowTrunkIdentityForAllOriginations-Eigenschaft ab.
     * 
     */
    public boolean isAllowTrunkIdentityForAllOriginations() {
        return allowTrunkIdentityForAllOriginations;
    }

    /**
     * Legt den Wert der allowTrunkIdentityForAllOriginations-Eigenschaft fest.
     * 
     */
    public void setAllowTrunkIdentityForAllOriginations(boolean value) {
        this.allowTrunkIdentityForAllOriginations = value;
    }

    /**
     * Ruft den Wert der useMostRecentEntryOnDeflection-Eigenschaft ab.
     * 
     */
    public boolean isUseMostRecentEntryOnDeflection() {
        return useMostRecentEntryOnDeflection;
    }

    /**
     * Legt den Wert der useMostRecentEntryOnDeflection-Eigenschaft fest.
     * 
     */
    public void setUseMostRecentEntryOnDeflection(boolean value) {
        this.useMostRecentEntryOnDeflection = value;
    }

}
