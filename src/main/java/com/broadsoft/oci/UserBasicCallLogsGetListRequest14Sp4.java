//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request user's call logs.
 *         If the callLogType is not specified, all types of calls are returned.
 *         The response is either a UserBasicCallLogsGetListResponse14sp4 or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserBasicCallLogsGetListRequest14sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBasicCallLogsGetListRequest14sp4">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="callLogType" type="{}CallLogsType" minOccurs="0"/>
 *         <element name="formatNameAndNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBasicCallLogsGetListRequest14sp4", propOrder = {
    "userId",
    "callLogType",
    "formatNameAndNumber"
})
public class UserBasicCallLogsGetListRequest14Sp4
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlSchemaType(name = "token")
    protected CallLogsType callLogType;
    protected Boolean formatNameAndNumber;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der callLogType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallLogsType }
     *     
     */
    public CallLogsType getCallLogType() {
        return callLogType;
    }

    /**
     * Legt den Wert der callLogType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallLogsType }
     *     
     */
    public void setCallLogType(CallLogsType value) {
        this.callLogType = value;
    }

    /**
     * Ruft den Wert der formatNameAndNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFormatNameAndNumber() {
        return formatNameAndNumber;
    }

    /**
     * Legt den Wert der formatNameAndNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFormatNameAndNumber(Boolean value) {
        this.formatNameAndNumber = value;
    }

}
