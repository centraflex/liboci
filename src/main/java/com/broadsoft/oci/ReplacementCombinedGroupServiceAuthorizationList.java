//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *       A list of group services that replaces a previously authorized group services.
 *     
 * 
 * <p>Java-Klasse für ReplacementCombinedGroupServiceAuthorizationList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ReplacementCombinedGroupServiceAuthorizationList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="groupServiceAuthorization" type="{}CombinedGroupServiceAuthorization" maxOccurs="unbounded"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReplacementCombinedGroupServiceAuthorizationList", propOrder = {
    "groupServiceAuthorization"
})
public class ReplacementCombinedGroupServiceAuthorizationList {

    @XmlElement(required = true)
    protected List<CombinedGroupServiceAuthorization> groupServiceAuthorization;

    /**
     * Gets the value of the groupServiceAuthorization property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the groupServiceAuthorization property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupServiceAuthorization().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CombinedGroupServiceAuthorization }
     * 
     * 
     * @return
     *     The value of the groupServiceAuthorization property.
     */
    public List<CombinedGroupServiceAuthorization> getGroupServiceAuthorization() {
        if (groupServiceAuthorization == null) {
            groupServiceAuthorization = new ArrayList<>();
        }
        return this.groupServiceAuthorization;
    }

}
