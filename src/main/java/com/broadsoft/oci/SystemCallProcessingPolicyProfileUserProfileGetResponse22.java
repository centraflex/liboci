//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCallProcessingPolicyProfileUserProfileGetRequest22.
 *         
 *         Replaced by: SystemCallProcessingPolicyProfileUserProfileGetResponse22V2 in AS data mode.
 *       
 * 
 * <p>Java-Klasse für SystemCallProcessingPolicyProfileUserProfileGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallProcessingPolicyProfileUserProfileGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useCLIDPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="clidPolicy" type="{}GroupCLIDPolicy"/>
 *         <element name="emergencyClidPolicy" type="{}GroupCLIDPolicy"/>
 *         <element name="allowAlternateNumbersForRedirectingIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useGroupName" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="blockCallingNameForExternalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowConfigurableCLIDForRedirectingIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowDepartmentCLIDNameOverride" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enterpriseCallsCLIDPolicy" type="{}EnterpriseInternalCallsCLIDPolicy"/>
 *         <element name="enterpriseGroupCallsCLIDPolicy" type="{}EnterpriseInternalCallsCLIDPolicy"/>
 *         <element name="serviceProviderGroupCallsCLIDPolicy" type="{}ServiceProviderInternalCallsCLIDPolicy"/>
 *         <element name="useMediaPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="mediaPolicySelection" type="{}MediaPolicySelection"/>
 *         <element name="supportedMediaSetName" type="{}MediaSetName" minOccurs="0"/>
 *         <element name="useCallLimitsPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useMaxSimultaneousCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxSimultaneousCalls" type="{}CallProcessingMaxSimultaneousCalls19sp1"/>
 *         <element name="useMaxSimultaneousVideoCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxSimultaneousVideoCalls" type="{}CallProcessingMaxSimultaneousCalls19sp1"/>
 *         <element name="useMaxCallTimeForAnsweredCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxCallTimeForAnsweredCallsMinutes" type="{}CallProcessingMaxCallTimeForAnsweredCallsMinutes16"/>
 *         <element name="useMaxCallTimeForUnansweredCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxCallTimeForUnansweredCallsMinutes" type="{}CallProcessingMaxCallTimeForUnansweredCallsMinutes19sp1"/>
 *         <element name="useMaxConcurrentRedirectedCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxConcurrentRedirectedCalls" type="{}CallProcessingMaxConcurrentRedirectedCalls19sp1"/>
 *         <element name="useMaxConcurrentFindMeFollowMeInvocations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxConcurrentFindMeFollowMeInvocations" type="{}CallProcessingMaxConcurrentFindMeFollowMeInvocations19sp1"/>
 *         <element name="useMaxFindMeFollowMeDepth" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxFindMeFollowMeDepth" type="{}CallProcessingMaxFindMeFollowMeDepth19sp1"/>
 *         <element name="maxRedirectionDepth" type="{}CallProcessingMaxRedirectionDepth19sp1"/>
 *         <element name="useTranslationRoutingPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="networkUsageSelection" type="{}NetworkUsageSelection"/>
 *         <element name="enableEnterpriseExtensionDialing" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enforceGroupCallingLineIdentityRestriction" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enforceEnterpriseCallingLineIdentityRestriction" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowEnterpriseGroupCallTypingForPrivateDialingPlan" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowEnterpriseGroupCallTypingForPublicDialingPlan" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="overrideCLIDRestrictionForPrivateCallCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useEnterpriseCLIDForPrivateCallCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useIncomingCLIDPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableDialableCallerID" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="usePhoneListLookupPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enablePhoneListLookup" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useMaxConcurrentTerminatingAlertingRequests" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxConcurrentTerminatingAlertingRequests" type="{}CallProcessingMaxConcurrentTerminatingAlertingRequests"/>
 *         <element name="includeRedirectionsInMaximumNumberOfConcurrentCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallProcessingPolicyProfileUserProfileGetResponse22", propOrder = {
    "useCLIDPolicy",
    "clidPolicy",
    "emergencyClidPolicy",
    "allowAlternateNumbersForRedirectingIdentity",
    "useGroupName",
    "blockCallingNameForExternalCalls",
    "allowConfigurableCLIDForRedirectingIdentity",
    "allowDepartmentCLIDNameOverride",
    "enterpriseCallsCLIDPolicy",
    "enterpriseGroupCallsCLIDPolicy",
    "serviceProviderGroupCallsCLIDPolicy",
    "useMediaPolicy",
    "mediaPolicySelection",
    "supportedMediaSetName",
    "useCallLimitsPolicy",
    "useMaxSimultaneousCalls",
    "maxSimultaneousCalls",
    "useMaxSimultaneousVideoCalls",
    "maxSimultaneousVideoCalls",
    "useMaxCallTimeForAnsweredCalls",
    "maxCallTimeForAnsweredCallsMinutes",
    "useMaxCallTimeForUnansweredCalls",
    "maxCallTimeForUnansweredCallsMinutes",
    "useMaxConcurrentRedirectedCalls",
    "maxConcurrentRedirectedCalls",
    "useMaxConcurrentFindMeFollowMeInvocations",
    "maxConcurrentFindMeFollowMeInvocations",
    "useMaxFindMeFollowMeDepth",
    "maxFindMeFollowMeDepth",
    "maxRedirectionDepth",
    "useTranslationRoutingPolicy",
    "networkUsageSelection",
    "enableEnterpriseExtensionDialing",
    "enforceGroupCallingLineIdentityRestriction",
    "enforceEnterpriseCallingLineIdentityRestriction",
    "allowEnterpriseGroupCallTypingForPrivateDialingPlan",
    "allowEnterpriseGroupCallTypingForPublicDialingPlan",
    "overrideCLIDRestrictionForPrivateCallCategory",
    "useEnterpriseCLIDForPrivateCallCategory",
    "useIncomingCLIDPolicy",
    "enableDialableCallerID",
    "usePhoneListLookupPolicy",
    "enablePhoneListLookup",
    "useMaxConcurrentTerminatingAlertingRequests",
    "maxConcurrentTerminatingAlertingRequests",
    "includeRedirectionsInMaximumNumberOfConcurrentCalls",
    "useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable",
    "useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable"
})
public class SystemCallProcessingPolicyProfileUserProfileGetResponse22
    extends OCIDataResponse
{

    protected boolean useCLIDPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupCLIDPolicy clidPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupCLIDPolicy emergencyClidPolicy;
    protected boolean allowAlternateNumbersForRedirectingIdentity;
    protected boolean useGroupName;
    protected boolean blockCallingNameForExternalCalls;
    protected boolean allowConfigurableCLIDForRedirectingIdentity;
    protected boolean allowDepartmentCLIDNameOverride;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseInternalCallsCLIDPolicy enterpriseCallsCLIDPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseInternalCallsCLIDPolicy enterpriseGroupCallsCLIDPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderInternalCallsCLIDPolicy serviceProviderGroupCallsCLIDPolicy;
    protected boolean useMediaPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MediaPolicySelection mediaPolicySelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String supportedMediaSetName;
    protected boolean useCallLimitsPolicy;
    protected boolean useMaxSimultaneousCalls;
    protected int maxSimultaneousCalls;
    protected boolean useMaxSimultaneousVideoCalls;
    protected int maxSimultaneousVideoCalls;
    protected boolean useMaxCallTimeForAnsweredCalls;
    protected int maxCallTimeForAnsweredCallsMinutes;
    protected boolean useMaxCallTimeForUnansweredCalls;
    protected int maxCallTimeForUnansweredCallsMinutes;
    protected boolean useMaxConcurrentRedirectedCalls;
    protected int maxConcurrentRedirectedCalls;
    protected boolean useMaxConcurrentFindMeFollowMeInvocations;
    protected int maxConcurrentFindMeFollowMeInvocations;
    protected boolean useMaxFindMeFollowMeDepth;
    protected int maxFindMeFollowMeDepth;
    protected int maxRedirectionDepth;
    protected boolean useTranslationRoutingPolicy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected NetworkUsageSelection networkUsageSelection;
    protected boolean enableEnterpriseExtensionDialing;
    protected boolean enforceGroupCallingLineIdentityRestriction;
    protected boolean enforceEnterpriseCallingLineIdentityRestriction;
    protected boolean allowEnterpriseGroupCallTypingForPrivateDialingPlan;
    protected boolean allowEnterpriseGroupCallTypingForPublicDialingPlan;
    protected boolean overrideCLIDRestrictionForPrivateCallCategory;
    protected boolean useEnterpriseCLIDForPrivateCallCategory;
    protected boolean useIncomingCLIDPolicy;
    protected boolean enableDialableCallerID;
    protected boolean usePhoneListLookupPolicy;
    protected boolean enablePhoneListLookup;
    protected boolean useMaxConcurrentTerminatingAlertingRequests;
    protected int maxConcurrentTerminatingAlertingRequests;
    protected boolean includeRedirectionsInMaximumNumberOfConcurrentCalls;
    protected boolean useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable;
    protected boolean useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable;

    /**
     * Ruft den Wert der useCLIDPolicy-Eigenschaft ab.
     * 
     */
    public boolean isUseCLIDPolicy() {
        return useCLIDPolicy;
    }

    /**
     * Legt den Wert der useCLIDPolicy-Eigenschaft fest.
     * 
     */
    public void setUseCLIDPolicy(boolean value) {
        this.useCLIDPolicy = value;
    }

    /**
     * Ruft den Wert der clidPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public GroupCLIDPolicy getClidPolicy() {
        return clidPolicy;
    }

    /**
     * Legt den Wert der clidPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public void setClidPolicy(GroupCLIDPolicy value) {
        this.clidPolicy = value;
    }

    /**
     * Ruft den Wert der emergencyClidPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public GroupCLIDPolicy getEmergencyClidPolicy() {
        return emergencyClidPolicy;
    }

    /**
     * Legt den Wert der emergencyClidPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupCLIDPolicy }
     *     
     */
    public void setEmergencyClidPolicy(GroupCLIDPolicy value) {
        this.emergencyClidPolicy = value;
    }

    /**
     * Ruft den Wert der allowAlternateNumbersForRedirectingIdentity-Eigenschaft ab.
     * 
     */
    public boolean isAllowAlternateNumbersForRedirectingIdentity() {
        return allowAlternateNumbersForRedirectingIdentity;
    }

    /**
     * Legt den Wert der allowAlternateNumbersForRedirectingIdentity-Eigenschaft fest.
     * 
     */
    public void setAllowAlternateNumbersForRedirectingIdentity(boolean value) {
        this.allowAlternateNumbersForRedirectingIdentity = value;
    }

    /**
     * Ruft den Wert der useGroupName-Eigenschaft ab.
     * 
     */
    public boolean isUseGroupName() {
        return useGroupName;
    }

    /**
     * Legt den Wert der useGroupName-Eigenschaft fest.
     * 
     */
    public void setUseGroupName(boolean value) {
        this.useGroupName = value;
    }

    /**
     * Ruft den Wert der blockCallingNameForExternalCalls-Eigenschaft ab.
     * 
     */
    public boolean isBlockCallingNameForExternalCalls() {
        return blockCallingNameForExternalCalls;
    }

    /**
     * Legt den Wert der blockCallingNameForExternalCalls-Eigenschaft fest.
     * 
     */
    public void setBlockCallingNameForExternalCalls(boolean value) {
        this.blockCallingNameForExternalCalls = value;
    }

    /**
     * Ruft den Wert der allowConfigurableCLIDForRedirectingIdentity-Eigenschaft ab.
     * 
     */
    public boolean isAllowConfigurableCLIDForRedirectingIdentity() {
        return allowConfigurableCLIDForRedirectingIdentity;
    }

    /**
     * Legt den Wert der allowConfigurableCLIDForRedirectingIdentity-Eigenschaft fest.
     * 
     */
    public void setAllowConfigurableCLIDForRedirectingIdentity(boolean value) {
        this.allowConfigurableCLIDForRedirectingIdentity = value;
    }

    /**
     * Ruft den Wert der allowDepartmentCLIDNameOverride-Eigenschaft ab.
     * 
     */
    public boolean isAllowDepartmentCLIDNameOverride() {
        return allowDepartmentCLIDNameOverride;
    }

    /**
     * Legt den Wert der allowDepartmentCLIDNameOverride-Eigenschaft fest.
     * 
     */
    public void setAllowDepartmentCLIDNameOverride(boolean value) {
        this.allowDepartmentCLIDNameOverride = value;
    }

    /**
     * Ruft den Wert der enterpriseCallsCLIDPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public EnterpriseInternalCallsCLIDPolicy getEnterpriseCallsCLIDPolicy() {
        return enterpriseCallsCLIDPolicy;
    }

    /**
     * Legt den Wert der enterpriseCallsCLIDPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public void setEnterpriseCallsCLIDPolicy(EnterpriseInternalCallsCLIDPolicy value) {
        this.enterpriseCallsCLIDPolicy = value;
    }

    /**
     * Ruft den Wert der enterpriseGroupCallsCLIDPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public EnterpriseInternalCallsCLIDPolicy getEnterpriseGroupCallsCLIDPolicy() {
        return enterpriseGroupCallsCLIDPolicy;
    }

    /**
     * Legt den Wert der enterpriseGroupCallsCLIDPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseInternalCallsCLIDPolicy }
     *     
     */
    public void setEnterpriseGroupCallsCLIDPolicy(EnterpriseInternalCallsCLIDPolicy value) {
        this.enterpriseGroupCallsCLIDPolicy = value;
    }

    /**
     * Ruft den Wert der serviceProviderGroupCallsCLIDPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderInternalCallsCLIDPolicy }
     *     
     */
    public ServiceProviderInternalCallsCLIDPolicy getServiceProviderGroupCallsCLIDPolicy() {
        return serviceProviderGroupCallsCLIDPolicy;
    }

    /**
     * Legt den Wert der serviceProviderGroupCallsCLIDPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderInternalCallsCLIDPolicy }
     *     
     */
    public void setServiceProviderGroupCallsCLIDPolicy(ServiceProviderInternalCallsCLIDPolicy value) {
        this.serviceProviderGroupCallsCLIDPolicy = value;
    }

    /**
     * Ruft den Wert der useMediaPolicy-Eigenschaft ab.
     * 
     */
    public boolean isUseMediaPolicy() {
        return useMediaPolicy;
    }

    /**
     * Legt den Wert der useMediaPolicy-Eigenschaft fest.
     * 
     */
    public void setUseMediaPolicy(boolean value) {
        this.useMediaPolicy = value;
    }

    /**
     * Ruft den Wert der mediaPolicySelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MediaPolicySelection }
     *     
     */
    public MediaPolicySelection getMediaPolicySelection() {
        return mediaPolicySelection;
    }

    /**
     * Legt den Wert der mediaPolicySelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MediaPolicySelection }
     *     
     */
    public void setMediaPolicySelection(MediaPolicySelection value) {
        this.mediaPolicySelection = value;
    }

    /**
     * Ruft den Wert der supportedMediaSetName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupportedMediaSetName() {
        return supportedMediaSetName;
    }

    /**
     * Legt den Wert der supportedMediaSetName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupportedMediaSetName(String value) {
        this.supportedMediaSetName = value;
    }

    /**
     * Ruft den Wert der useCallLimitsPolicy-Eigenschaft ab.
     * 
     */
    public boolean isUseCallLimitsPolicy() {
        return useCallLimitsPolicy;
    }

    /**
     * Legt den Wert der useCallLimitsPolicy-Eigenschaft fest.
     * 
     */
    public void setUseCallLimitsPolicy(boolean value) {
        this.useCallLimitsPolicy = value;
    }

    /**
     * Ruft den Wert der useMaxSimultaneousCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxSimultaneousCalls() {
        return useMaxSimultaneousCalls;
    }

    /**
     * Legt den Wert der useMaxSimultaneousCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxSimultaneousCalls(boolean value) {
        this.useMaxSimultaneousCalls = value;
    }

    /**
     * Ruft den Wert der maxSimultaneousCalls-Eigenschaft ab.
     * 
     */
    public int getMaxSimultaneousCalls() {
        return maxSimultaneousCalls;
    }

    /**
     * Legt den Wert der maxSimultaneousCalls-Eigenschaft fest.
     * 
     */
    public void setMaxSimultaneousCalls(int value) {
        this.maxSimultaneousCalls = value;
    }

    /**
     * Ruft den Wert der useMaxSimultaneousVideoCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxSimultaneousVideoCalls() {
        return useMaxSimultaneousVideoCalls;
    }

    /**
     * Legt den Wert der useMaxSimultaneousVideoCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxSimultaneousVideoCalls(boolean value) {
        this.useMaxSimultaneousVideoCalls = value;
    }

    /**
     * Ruft den Wert der maxSimultaneousVideoCalls-Eigenschaft ab.
     * 
     */
    public int getMaxSimultaneousVideoCalls() {
        return maxSimultaneousVideoCalls;
    }

    /**
     * Legt den Wert der maxSimultaneousVideoCalls-Eigenschaft fest.
     * 
     */
    public void setMaxSimultaneousVideoCalls(int value) {
        this.maxSimultaneousVideoCalls = value;
    }

    /**
     * Ruft den Wert der useMaxCallTimeForAnsweredCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxCallTimeForAnsweredCalls() {
        return useMaxCallTimeForAnsweredCalls;
    }

    /**
     * Legt den Wert der useMaxCallTimeForAnsweredCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxCallTimeForAnsweredCalls(boolean value) {
        this.useMaxCallTimeForAnsweredCalls = value;
    }

    /**
     * Ruft den Wert der maxCallTimeForAnsweredCallsMinutes-Eigenschaft ab.
     * 
     */
    public int getMaxCallTimeForAnsweredCallsMinutes() {
        return maxCallTimeForAnsweredCallsMinutes;
    }

    /**
     * Legt den Wert der maxCallTimeForAnsweredCallsMinutes-Eigenschaft fest.
     * 
     */
    public void setMaxCallTimeForAnsweredCallsMinutes(int value) {
        this.maxCallTimeForAnsweredCallsMinutes = value;
    }

    /**
     * Ruft den Wert der useMaxCallTimeForUnansweredCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxCallTimeForUnansweredCalls() {
        return useMaxCallTimeForUnansweredCalls;
    }

    /**
     * Legt den Wert der useMaxCallTimeForUnansweredCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxCallTimeForUnansweredCalls(boolean value) {
        this.useMaxCallTimeForUnansweredCalls = value;
    }

    /**
     * Ruft den Wert der maxCallTimeForUnansweredCallsMinutes-Eigenschaft ab.
     * 
     */
    public int getMaxCallTimeForUnansweredCallsMinutes() {
        return maxCallTimeForUnansweredCallsMinutes;
    }

    /**
     * Legt den Wert der maxCallTimeForUnansweredCallsMinutes-Eigenschaft fest.
     * 
     */
    public void setMaxCallTimeForUnansweredCallsMinutes(int value) {
        this.maxCallTimeForUnansweredCallsMinutes = value;
    }

    /**
     * Ruft den Wert der useMaxConcurrentRedirectedCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxConcurrentRedirectedCalls() {
        return useMaxConcurrentRedirectedCalls;
    }

    /**
     * Legt den Wert der useMaxConcurrentRedirectedCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxConcurrentRedirectedCalls(boolean value) {
        this.useMaxConcurrentRedirectedCalls = value;
    }

    /**
     * Ruft den Wert der maxConcurrentRedirectedCalls-Eigenschaft ab.
     * 
     */
    public int getMaxConcurrentRedirectedCalls() {
        return maxConcurrentRedirectedCalls;
    }

    /**
     * Legt den Wert der maxConcurrentRedirectedCalls-Eigenschaft fest.
     * 
     */
    public void setMaxConcurrentRedirectedCalls(int value) {
        this.maxConcurrentRedirectedCalls = value;
    }

    /**
     * Ruft den Wert der useMaxConcurrentFindMeFollowMeInvocations-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxConcurrentFindMeFollowMeInvocations() {
        return useMaxConcurrentFindMeFollowMeInvocations;
    }

    /**
     * Legt den Wert der useMaxConcurrentFindMeFollowMeInvocations-Eigenschaft fest.
     * 
     */
    public void setUseMaxConcurrentFindMeFollowMeInvocations(boolean value) {
        this.useMaxConcurrentFindMeFollowMeInvocations = value;
    }

    /**
     * Ruft den Wert der maxConcurrentFindMeFollowMeInvocations-Eigenschaft ab.
     * 
     */
    public int getMaxConcurrentFindMeFollowMeInvocations() {
        return maxConcurrentFindMeFollowMeInvocations;
    }

    /**
     * Legt den Wert der maxConcurrentFindMeFollowMeInvocations-Eigenschaft fest.
     * 
     */
    public void setMaxConcurrentFindMeFollowMeInvocations(int value) {
        this.maxConcurrentFindMeFollowMeInvocations = value;
    }

    /**
     * Ruft den Wert der useMaxFindMeFollowMeDepth-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxFindMeFollowMeDepth() {
        return useMaxFindMeFollowMeDepth;
    }

    /**
     * Legt den Wert der useMaxFindMeFollowMeDepth-Eigenschaft fest.
     * 
     */
    public void setUseMaxFindMeFollowMeDepth(boolean value) {
        this.useMaxFindMeFollowMeDepth = value;
    }

    /**
     * Ruft den Wert der maxFindMeFollowMeDepth-Eigenschaft ab.
     * 
     */
    public int getMaxFindMeFollowMeDepth() {
        return maxFindMeFollowMeDepth;
    }

    /**
     * Legt den Wert der maxFindMeFollowMeDepth-Eigenschaft fest.
     * 
     */
    public void setMaxFindMeFollowMeDepth(int value) {
        this.maxFindMeFollowMeDepth = value;
    }

    /**
     * Ruft den Wert der maxRedirectionDepth-Eigenschaft ab.
     * 
     */
    public int getMaxRedirectionDepth() {
        return maxRedirectionDepth;
    }

    /**
     * Legt den Wert der maxRedirectionDepth-Eigenschaft fest.
     * 
     */
    public void setMaxRedirectionDepth(int value) {
        this.maxRedirectionDepth = value;
    }

    /**
     * Ruft den Wert der useTranslationRoutingPolicy-Eigenschaft ab.
     * 
     */
    public boolean isUseTranslationRoutingPolicy() {
        return useTranslationRoutingPolicy;
    }

    /**
     * Legt den Wert der useTranslationRoutingPolicy-Eigenschaft fest.
     * 
     */
    public void setUseTranslationRoutingPolicy(boolean value) {
        this.useTranslationRoutingPolicy = value;
    }

    /**
     * Ruft den Wert der networkUsageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkUsageSelection }
     *     
     */
    public NetworkUsageSelection getNetworkUsageSelection() {
        return networkUsageSelection;
    }

    /**
     * Legt den Wert der networkUsageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkUsageSelection }
     *     
     */
    public void setNetworkUsageSelection(NetworkUsageSelection value) {
        this.networkUsageSelection = value;
    }

    /**
     * Ruft den Wert der enableEnterpriseExtensionDialing-Eigenschaft ab.
     * 
     */
    public boolean isEnableEnterpriseExtensionDialing() {
        return enableEnterpriseExtensionDialing;
    }

    /**
     * Legt den Wert der enableEnterpriseExtensionDialing-Eigenschaft fest.
     * 
     */
    public void setEnableEnterpriseExtensionDialing(boolean value) {
        this.enableEnterpriseExtensionDialing = value;
    }

    /**
     * Ruft den Wert der enforceGroupCallingLineIdentityRestriction-Eigenschaft ab.
     * 
     */
    public boolean isEnforceGroupCallingLineIdentityRestriction() {
        return enforceGroupCallingLineIdentityRestriction;
    }

    /**
     * Legt den Wert der enforceGroupCallingLineIdentityRestriction-Eigenschaft fest.
     * 
     */
    public void setEnforceGroupCallingLineIdentityRestriction(boolean value) {
        this.enforceGroupCallingLineIdentityRestriction = value;
    }

    /**
     * Ruft den Wert der enforceEnterpriseCallingLineIdentityRestriction-Eigenschaft ab.
     * 
     */
    public boolean isEnforceEnterpriseCallingLineIdentityRestriction() {
        return enforceEnterpriseCallingLineIdentityRestriction;
    }

    /**
     * Legt den Wert der enforceEnterpriseCallingLineIdentityRestriction-Eigenschaft fest.
     * 
     */
    public void setEnforceEnterpriseCallingLineIdentityRestriction(boolean value) {
        this.enforceEnterpriseCallingLineIdentityRestriction = value;
    }

    /**
     * Ruft den Wert der allowEnterpriseGroupCallTypingForPrivateDialingPlan-Eigenschaft ab.
     * 
     */
    public boolean isAllowEnterpriseGroupCallTypingForPrivateDialingPlan() {
        return allowEnterpriseGroupCallTypingForPrivateDialingPlan;
    }

    /**
     * Legt den Wert der allowEnterpriseGroupCallTypingForPrivateDialingPlan-Eigenschaft fest.
     * 
     */
    public void setAllowEnterpriseGroupCallTypingForPrivateDialingPlan(boolean value) {
        this.allowEnterpriseGroupCallTypingForPrivateDialingPlan = value;
    }

    /**
     * Ruft den Wert der allowEnterpriseGroupCallTypingForPublicDialingPlan-Eigenschaft ab.
     * 
     */
    public boolean isAllowEnterpriseGroupCallTypingForPublicDialingPlan() {
        return allowEnterpriseGroupCallTypingForPublicDialingPlan;
    }

    /**
     * Legt den Wert der allowEnterpriseGroupCallTypingForPublicDialingPlan-Eigenschaft fest.
     * 
     */
    public void setAllowEnterpriseGroupCallTypingForPublicDialingPlan(boolean value) {
        this.allowEnterpriseGroupCallTypingForPublicDialingPlan = value;
    }

    /**
     * Ruft den Wert der overrideCLIDRestrictionForPrivateCallCategory-Eigenschaft ab.
     * 
     */
    public boolean isOverrideCLIDRestrictionForPrivateCallCategory() {
        return overrideCLIDRestrictionForPrivateCallCategory;
    }

    /**
     * Legt den Wert der overrideCLIDRestrictionForPrivateCallCategory-Eigenschaft fest.
     * 
     */
    public void setOverrideCLIDRestrictionForPrivateCallCategory(boolean value) {
        this.overrideCLIDRestrictionForPrivateCallCategory = value;
    }

    /**
     * Ruft den Wert der useEnterpriseCLIDForPrivateCallCategory-Eigenschaft ab.
     * 
     */
    public boolean isUseEnterpriseCLIDForPrivateCallCategory() {
        return useEnterpriseCLIDForPrivateCallCategory;
    }

    /**
     * Legt den Wert der useEnterpriseCLIDForPrivateCallCategory-Eigenschaft fest.
     * 
     */
    public void setUseEnterpriseCLIDForPrivateCallCategory(boolean value) {
        this.useEnterpriseCLIDForPrivateCallCategory = value;
    }

    /**
     * Ruft den Wert der useIncomingCLIDPolicy-Eigenschaft ab.
     * 
     */
    public boolean isUseIncomingCLIDPolicy() {
        return useIncomingCLIDPolicy;
    }

    /**
     * Legt den Wert der useIncomingCLIDPolicy-Eigenschaft fest.
     * 
     */
    public void setUseIncomingCLIDPolicy(boolean value) {
        this.useIncomingCLIDPolicy = value;
    }

    /**
     * Ruft den Wert der enableDialableCallerID-Eigenschaft ab.
     * 
     */
    public boolean isEnableDialableCallerID() {
        return enableDialableCallerID;
    }

    /**
     * Legt den Wert der enableDialableCallerID-Eigenschaft fest.
     * 
     */
    public void setEnableDialableCallerID(boolean value) {
        this.enableDialableCallerID = value;
    }

    /**
     * Ruft den Wert der usePhoneListLookupPolicy-Eigenschaft ab.
     * 
     */
    public boolean isUsePhoneListLookupPolicy() {
        return usePhoneListLookupPolicy;
    }

    /**
     * Legt den Wert der usePhoneListLookupPolicy-Eigenschaft fest.
     * 
     */
    public void setUsePhoneListLookupPolicy(boolean value) {
        this.usePhoneListLookupPolicy = value;
    }

    /**
     * Ruft den Wert der enablePhoneListLookup-Eigenschaft ab.
     * 
     */
    public boolean isEnablePhoneListLookup() {
        return enablePhoneListLookup;
    }

    /**
     * Legt den Wert der enablePhoneListLookup-Eigenschaft fest.
     * 
     */
    public void setEnablePhoneListLookup(boolean value) {
        this.enablePhoneListLookup = value;
    }

    /**
     * Ruft den Wert der useMaxConcurrentTerminatingAlertingRequests-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxConcurrentTerminatingAlertingRequests() {
        return useMaxConcurrentTerminatingAlertingRequests;
    }

    /**
     * Legt den Wert der useMaxConcurrentTerminatingAlertingRequests-Eigenschaft fest.
     * 
     */
    public void setUseMaxConcurrentTerminatingAlertingRequests(boolean value) {
        this.useMaxConcurrentTerminatingAlertingRequests = value;
    }

    /**
     * Ruft den Wert der maxConcurrentTerminatingAlertingRequests-Eigenschaft ab.
     * 
     */
    public int getMaxConcurrentTerminatingAlertingRequests() {
        return maxConcurrentTerminatingAlertingRequests;
    }

    /**
     * Legt den Wert der maxConcurrentTerminatingAlertingRequests-Eigenschaft fest.
     * 
     */
    public void setMaxConcurrentTerminatingAlertingRequests(int value) {
        this.maxConcurrentTerminatingAlertingRequests = value;
    }

    /**
     * Ruft den Wert der includeRedirectionsInMaximumNumberOfConcurrentCalls-Eigenschaft ab.
     * 
     */
    public boolean isIncludeRedirectionsInMaximumNumberOfConcurrentCalls() {
        return includeRedirectionsInMaximumNumberOfConcurrentCalls;
    }

    /**
     * Legt den Wert der includeRedirectionsInMaximumNumberOfConcurrentCalls-Eigenschaft fest.
     * 
     */
    public void setIncludeRedirectionsInMaximumNumberOfConcurrentCalls(boolean value) {
        this.includeRedirectionsInMaximumNumberOfConcurrentCalls = value;
    }

    /**
     * Ruft den Wert der useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable-Eigenschaft ab.
     * 
     */
    public boolean isUseUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable() {
        return useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable;
    }

    /**
     * Legt den Wert der useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable-Eigenschaft fest.
     * 
     */
    public void setUseUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable(boolean value) {
        this.useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable = value;
    }

    /**
     * Ruft den Wert der useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable-Eigenschaft ab.
     * 
     */
    public boolean isUseUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable() {
        return useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable;
    }

    /**
     * Legt den Wert der useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable-Eigenschaft fest.
     * 
     */
    public void setUseUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable(boolean value) {
        this.useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable = value;
    }

}
