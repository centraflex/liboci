//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal calling menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für VoicePortalCallingMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="VoicePortalCallingMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="endCurrentCallAndGoBackToPreviousMenu" type="{}VoicePortalDigitSequence" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoicePortalCallingMenuKeysModifyEntry", propOrder = {
    "endCurrentCallAndGoBackToPreviousMenu",
    "returnToPreviousMenu"
})
public class VoicePortalCallingMenuKeysModifyEntry {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String endCurrentCallAndGoBackToPreviousMenu;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;

    /**
     * Ruft den Wert der endCurrentCallAndGoBackToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndCurrentCallAndGoBackToPreviousMenu() {
        return endCurrentCallAndGoBackToPreviousMenu;
    }

    /**
     * Legt den Wert der endCurrentCallAndGoBackToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndCurrentCallAndGoBackToPreviousMenu(String value) {
        this.endCurrentCallAndGoBackToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

}
