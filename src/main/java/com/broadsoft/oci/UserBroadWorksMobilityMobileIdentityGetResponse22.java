//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *          Response to a UserBroadWorksMobilityMobileIdentityGetRequest22.
 *          
 *          Columns for the mobileNumberAlertedTable are as follows: "Mobile Number", "Country Code", "National Prefix"
 *         Replaced by UserBroadWorksMobilityMobileIdentityGetResponse22V2.
 *       
 * 
 * <p>Java-Klasse für UserBroadWorksMobilityMobileIdentityGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksMobilityMobileIdentityGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="description" type="{}BroadWorksMobilityUserMobileIdentityDescription" minOccurs="0"/>
 *         <element name="isPrimary" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableAlerting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="alertAgentCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="alertClickToDialCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="alertGroupPagingCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useMobilityCallingLineID" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableDiversionInhibitor" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="requireAnswerConfirmation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="broadworksCallControl" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useSettingLevel" type="{}BroadWorksMobilityUserSettingLevel"/>
 *         <element name="denyCallOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="denyCallTerminations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="effectiveEnableLocationServices" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="effectiveEnableMSRNLookup" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="effectiveEnableMobileStateChecking" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="effectiveEnableAnnouncementSuppression" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="effectiveDenyCallOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="effectiveDenyCallTerminations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="devicesToRing" type="{}BroadWorksMobilityPhoneToRing"/>
 *         <element name="includeSharedCallAppearance" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeBroadworksAnywhere" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeExecutiveAssistant" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableCallAnchoring" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="timeSchedule" type="{}ScheduleGlobalKey" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}ScheduleGlobalKey" minOccurs="0"/>
 *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointWithPortNumberRead22" minOccurs="0"/>
 *         <element name="outboundAlternateNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="enableDirectRouting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="markCDRAsEnterpriseGroupCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="mobileNumberAlertedTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksMobilityMobileIdentityGetResponse22", propOrder = {
    "description",
    "isPrimary",
    "enableAlerting",
    "alertAgentCalls",
    "alertClickToDialCalls",
    "alertGroupPagingCalls",
    "useMobilityCallingLineID",
    "enableDiversionInhibitor",
    "requireAnswerConfirmation",
    "broadworksCallControl",
    "useSettingLevel",
    "denyCallOriginations",
    "denyCallTerminations",
    "effectiveEnableLocationServices",
    "effectiveEnableMSRNLookup",
    "effectiveEnableMobileStateChecking",
    "effectiveEnableAnnouncementSuppression",
    "effectiveDenyCallOriginations",
    "effectiveDenyCallTerminations",
    "devicesToRing",
    "includeSharedCallAppearance",
    "includeBroadworksAnywhere",
    "includeExecutiveAssistant",
    "enableCallAnchoring",
    "timeSchedule",
    "holidaySchedule",
    "accessDeviceEndpoint",
    "outboundAlternateNumber",
    "enableDirectRouting",
    "markCDRAsEnterpriseGroupCalls",
    "mobileNumberAlertedTable"
})
public class UserBroadWorksMobilityMobileIdentityGetResponse22
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    protected boolean isPrimary;
    protected boolean enableAlerting;
    protected boolean alertAgentCalls;
    protected boolean alertClickToDialCalls;
    protected boolean alertGroupPagingCalls;
    protected boolean useMobilityCallingLineID;
    protected boolean enableDiversionInhibitor;
    protected boolean requireAnswerConfirmation;
    protected boolean broadworksCallControl;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BroadWorksMobilityUserSettingLevel useSettingLevel;
    protected boolean denyCallOriginations;
    protected boolean denyCallTerminations;
    protected boolean effectiveEnableLocationServices;
    protected boolean effectiveEnableMSRNLookup;
    protected boolean effectiveEnableMobileStateChecking;
    protected boolean effectiveEnableAnnouncementSuppression;
    protected boolean effectiveDenyCallOriginations;
    protected boolean effectiveDenyCallTerminations;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BroadWorksMobilityPhoneToRing devicesToRing;
    protected boolean includeSharedCallAppearance;
    protected boolean includeBroadworksAnywhere;
    protected boolean includeExecutiveAssistant;
    protected boolean enableCallAnchoring;
    protected ScheduleGlobalKey timeSchedule;
    protected ScheduleGlobalKey holidaySchedule;
    protected AccessDeviceEndpointWithPortNumberRead22 accessDeviceEndpoint;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String outboundAlternateNumber;
    protected boolean enableDirectRouting;
    protected boolean markCDRAsEnterpriseGroupCalls;
    @XmlElement(required = true)
    protected OCITable mobileNumberAlertedTable;

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der isPrimary-Eigenschaft ab.
     * 
     */
    public boolean isIsPrimary() {
        return isPrimary;
    }

    /**
     * Legt den Wert der isPrimary-Eigenschaft fest.
     * 
     */
    public void setIsPrimary(boolean value) {
        this.isPrimary = value;
    }

    /**
     * Ruft den Wert der enableAlerting-Eigenschaft ab.
     * 
     */
    public boolean isEnableAlerting() {
        return enableAlerting;
    }

    /**
     * Legt den Wert der enableAlerting-Eigenschaft fest.
     * 
     */
    public void setEnableAlerting(boolean value) {
        this.enableAlerting = value;
    }

    /**
     * Ruft den Wert der alertAgentCalls-Eigenschaft ab.
     * 
     */
    public boolean isAlertAgentCalls() {
        return alertAgentCalls;
    }

    /**
     * Legt den Wert der alertAgentCalls-Eigenschaft fest.
     * 
     */
    public void setAlertAgentCalls(boolean value) {
        this.alertAgentCalls = value;
    }

    /**
     * Ruft den Wert der alertClickToDialCalls-Eigenschaft ab.
     * 
     */
    public boolean isAlertClickToDialCalls() {
        return alertClickToDialCalls;
    }

    /**
     * Legt den Wert der alertClickToDialCalls-Eigenschaft fest.
     * 
     */
    public void setAlertClickToDialCalls(boolean value) {
        this.alertClickToDialCalls = value;
    }

    /**
     * Ruft den Wert der alertGroupPagingCalls-Eigenschaft ab.
     * 
     */
    public boolean isAlertGroupPagingCalls() {
        return alertGroupPagingCalls;
    }

    /**
     * Legt den Wert der alertGroupPagingCalls-Eigenschaft fest.
     * 
     */
    public void setAlertGroupPagingCalls(boolean value) {
        this.alertGroupPagingCalls = value;
    }

    /**
     * Ruft den Wert der useMobilityCallingLineID-Eigenschaft ab.
     * 
     */
    public boolean isUseMobilityCallingLineID() {
        return useMobilityCallingLineID;
    }

    /**
     * Legt den Wert der useMobilityCallingLineID-Eigenschaft fest.
     * 
     */
    public void setUseMobilityCallingLineID(boolean value) {
        this.useMobilityCallingLineID = value;
    }

    /**
     * Ruft den Wert der enableDiversionInhibitor-Eigenschaft ab.
     * 
     */
    public boolean isEnableDiversionInhibitor() {
        return enableDiversionInhibitor;
    }

    /**
     * Legt den Wert der enableDiversionInhibitor-Eigenschaft fest.
     * 
     */
    public void setEnableDiversionInhibitor(boolean value) {
        this.enableDiversionInhibitor = value;
    }

    /**
     * Ruft den Wert der requireAnswerConfirmation-Eigenschaft ab.
     * 
     */
    public boolean isRequireAnswerConfirmation() {
        return requireAnswerConfirmation;
    }

    /**
     * Legt den Wert der requireAnswerConfirmation-Eigenschaft fest.
     * 
     */
    public void setRequireAnswerConfirmation(boolean value) {
        this.requireAnswerConfirmation = value;
    }

    /**
     * Ruft den Wert der broadworksCallControl-Eigenschaft ab.
     * 
     */
    public boolean isBroadworksCallControl() {
        return broadworksCallControl;
    }

    /**
     * Legt den Wert der broadworksCallControl-Eigenschaft fest.
     * 
     */
    public void setBroadworksCallControl(boolean value) {
        this.broadworksCallControl = value;
    }

    /**
     * Ruft den Wert der useSettingLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksMobilityUserSettingLevel }
     *     
     */
    public BroadWorksMobilityUserSettingLevel getUseSettingLevel() {
        return useSettingLevel;
    }

    /**
     * Legt den Wert der useSettingLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksMobilityUserSettingLevel }
     *     
     */
    public void setUseSettingLevel(BroadWorksMobilityUserSettingLevel value) {
        this.useSettingLevel = value;
    }

    /**
     * Ruft den Wert der denyCallOriginations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallOriginations() {
        return denyCallOriginations;
    }

    /**
     * Legt den Wert der denyCallOriginations-Eigenschaft fest.
     * 
     */
    public void setDenyCallOriginations(boolean value) {
        this.denyCallOriginations = value;
    }

    /**
     * Ruft den Wert der denyCallTerminations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallTerminations() {
        return denyCallTerminations;
    }

    /**
     * Legt den Wert der denyCallTerminations-Eigenschaft fest.
     * 
     */
    public void setDenyCallTerminations(boolean value) {
        this.denyCallTerminations = value;
    }

    /**
     * Ruft den Wert der effectiveEnableLocationServices-Eigenschaft ab.
     * 
     */
    public boolean isEffectiveEnableLocationServices() {
        return effectiveEnableLocationServices;
    }

    /**
     * Legt den Wert der effectiveEnableLocationServices-Eigenschaft fest.
     * 
     */
    public void setEffectiveEnableLocationServices(boolean value) {
        this.effectiveEnableLocationServices = value;
    }

    /**
     * Ruft den Wert der effectiveEnableMSRNLookup-Eigenschaft ab.
     * 
     */
    public boolean isEffectiveEnableMSRNLookup() {
        return effectiveEnableMSRNLookup;
    }

    /**
     * Legt den Wert der effectiveEnableMSRNLookup-Eigenschaft fest.
     * 
     */
    public void setEffectiveEnableMSRNLookup(boolean value) {
        this.effectiveEnableMSRNLookup = value;
    }

    /**
     * Ruft den Wert der effectiveEnableMobileStateChecking-Eigenschaft ab.
     * 
     */
    public boolean isEffectiveEnableMobileStateChecking() {
        return effectiveEnableMobileStateChecking;
    }

    /**
     * Legt den Wert der effectiveEnableMobileStateChecking-Eigenschaft fest.
     * 
     */
    public void setEffectiveEnableMobileStateChecking(boolean value) {
        this.effectiveEnableMobileStateChecking = value;
    }

    /**
     * Ruft den Wert der effectiveEnableAnnouncementSuppression-Eigenschaft ab.
     * 
     */
    public boolean isEffectiveEnableAnnouncementSuppression() {
        return effectiveEnableAnnouncementSuppression;
    }

    /**
     * Legt den Wert der effectiveEnableAnnouncementSuppression-Eigenschaft fest.
     * 
     */
    public void setEffectiveEnableAnnouncementSuppression(boolean value) {
        this.effectiveEnableAnnouncementSuppression = value;
    }

    /**
     * Ruft den Wert der effectiveDenyCallOriginations-Eigenschaft ab.
     * 
     */
    public boolean isEffectiveDenyCallOriginations() {
        return effectiveDenyCallOriginations;
    }

    /**
     * Legt den Wert der effectiveDenyCallOriginations-Eigenschaft fest.
     * 
     */
    public void setEffectiveDenyCallOriginations(boolean value) {
        this.effectiveDenyCallOriginations = value;
    }

    /**
     * Ruft den Wert der effectiveDenyCallTerminations-Eigenschaft ab.
     * 
     */
    public boolean isEffectiveDenyCallTerminations() {
        return effectiveDenyCallTerminations;
    }

    /**
     * Legt den Wert der effectiveDenyCallTerminations-Eigenschaft fest.
     * 
     */
    public void setEffectiveDenyCallTerminations(boolean value) {
        this.effectiveDenyCallTerminations = value;
    }

    /**
     * Ruft den Wert der devicesToRing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksMobilityPhoneToRing }
     *     
     */
    public BroadWorksMobilityPhoneToRing getDevicesToRing() {
        return devicesToRing;
    }

    /**
     * Legt den Wert der devicesToRing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksMobilityPhoneToRing }
     *     
     */
    public void setDevicesToRing(BroadWorksMobilityPhoneToRing value) {
        this.devicesToRing = value;
    }

    /**
     * Ruft den Wert der includeSharedCallAppearance-Eigenschaft ab.
     * 
     */
    public boolean isIncludeSharedCallAppearance() {
        return includeSharedCallAppearance;
    }

    /**
     * Legt den Wert der includeSharedCallAppearance-Eigenschaft fest.
     * 
     */
    public void setIncludeSharedCallAppearance(boolean value) {
        this.includeSharedCallAppearance = value;
    }

    /**
     * Ruft den Wert der includeBroadworksAnywhere-Eigenschaft ab.
     * 
     */
    public boolean isIncludeBroadworksAnywhere() {
        return includeBroadworksAnywhere;
    }

    /**
     * Legt den Wert der includeBroadworksAnywhere-Eigenschaft fest.
     * 
     */
    public void setIncludeBroadworksAnywhere(boolean value) {
        this.includeBroadworksAnywhere = value;
    }

    /**
     * Ruft den Wert der includeExecutiveAssistant-Eigenschaft ab.
     * 
     */
    public boolean isIncludeExecutiveAssistant() {
        return includeExecutiveAssistant;
    }

    /**
     * Legt den Wert der includeExecutiveAssistant-Eigenschaft fest.
     * 
     */
    public void setIncludeExecutiveAssistant(boolean value) {
        this.includeExecutiveAssistant = value;
    }

    /**
     * Ruft den Wert der enableCallAnchoring-Eigenschaft ab.
     * 
     */
    public boolean isEnableCallAnchoring() {
        return enableCallAnchoring;
    }

    /**
     * Legt den Wert der enableCallAnchoring-Eigenschaft fest.
     * 
     */
    public void setEnableCallAnchoring(boolean value) {
        this.enableCallAnchoring = value;
    }

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ScheduleGlobalKey }
     *     
     */
    public ScheduleGlobalKey getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ScheduleGlobalKey }
     *     
     */
    public void setTimeSchedule(ScheduleGlobalKey value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ScheduleGlobalKey }
     *     
     */
    public ScheduleGlobalKey getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ScheduleGlobalKey }
     *     
     */
    public void setHolidaySchedule(ScheduleGlobalKey value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEndpointWithPortNumberRead22 }
     *     
     */
    public AccessDeviceEndpointWithPortNumberRead22 getAccessDeviceEndpoint() {
        return accessDeviceEndpoint;
    }

    /**
     * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEndpointWithPortNumberRead22 }
     *     
     */
    public void setAccessDeviceEndpoint(AccessDeviceEndpointWithPortNumberRead22 value) {
        this.accessDeviceEndpoint = value;
    }

    /**
     * Ruft den Wert der outboundAlternateNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutboundAlternateNumber() {
        return outboundAlternateNumber;
    }

    /**
     * Legt den Wert der outboundAlternateNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutboundAlternateNumber(String value) {
        this.outboundAlternateNumber = value;
    }

    /**
     * Ruft den Wert der enableDirectRouting-Eigenschaft ab.
     * 
     */
    public boolean isEnableDirectRouting() {
        return enableDirectRouting;
    }

    /**
     * Legt den Wert der enableDirectRouting-Eigenschaft fest.
     * 
     */
    public void setEnableDirectRouting(boolean value) {
        this.enableDirectRouting = value;
    }

    /**
     * Ruft den Wert der markCDRAsEnterpriseGroupCalls-Eigenschaft ab.
     * 
     */
    public boolean isMarkCDRAsEnterpriseGroupCalls() {
        return markCDRAsEnterpriseGroupCalls;
    }

    /**
     * Legt den Wert der markCDRAsEnterpriseGroupCalls-Eigenschaft fest.
     * 
     */
    public void setMarkCDRAsEnterpriseGroupCalls(boolean value) {
        this.markCDRAsEnterpriseGroupCalls = value;
    }

    /**
     * Ruft den Wert der mobileNumberAlertedTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getMobileNumberAlertedTable() {
        return mobileNumberAlertedTable;
    }

    /**
     * Legt den Wert der mobileNumberAlertedTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setMobileNumberAlertedTable(OCITable value) {
        this.mobileNumberAlertedTable = value;
    }

}
