//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderNetworkClassOfServiceGetAssignedGroupListRequest.
 *         Contains a table of groups that have the Network Class of Service 
 *         assigned. The column headings are: "Group Id" and "Group Name".
 *       
 * 
 * <p>Java-Klasse für ServiceProviderNetworkClassOfServiceGetAssignedGroupListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderNetworkClassOfServiceGetAssignedGroupListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="groupTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderNetworkClassOfServiceGetAssignedGroupListResponse", propOrder = {
    "groupTable"
})
public class ServiceProviderNetworkClassOfServiceGetAssignedGroupListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable groupTable;

    /**
     * Ruft den Wert der groupTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getGroupTable() {
        return groupTable;
    }

    /**
     * Legt den Wert der groupTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setGroupTable(OCITable value) {
        this.groupTable = value;
    }

}
