//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify CPE Config system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *         
 *         Replaced by: SystemCPEConfigParametersModifyRequest21       
 *       
 * 
 * <p>Java-Klasse für SystemCPEConfigParametersModifyRequest20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCPEConfigParametersModifyRequest20">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enableIPDeviceManagement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="ftpConnectTimeoutSeconds" type="{}DeviceManagementFTPConnectTimeoutSeconds" minOccurs="0"/>
 *         <element name="ftpFileTransferTimeoutSeconds" type="{}DeviceManagementFTPFileTransferTimeoutSeconds" minOccurs="0"/>
 *         <element name="pauseBetweenFileRebuildMilliseconds" type="{}DeviceManagementPauseBetweenFileRebuildMilliseconds" minOccurs="0"/>
 *         <element name="maxBusyTimeMinutes" type="{}DeviceManagementMaxBusyTimeMinutes" minOccurs="0"/>
 *         <element name="deviceAccessAppServerClusterName" type="{}NetAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCPEConfigParametersModifyRequest20", propOrder = {
    "enableIPDeviceManagement",
    "ftpConnectTimeoutSeconds",
    "ftpFileTransferTimeoutSeconds",
    "pauseBetweenFileRebuildMilliseconds",
    "maxBusyTimeMinutes",
    "deviceAccessAppServerClusterName"
})
public class SystemCPEConfigParametersModifyRequest20
    extends OCIRequest
{

    protected Boolean enableIPDeviceManagement;
    protected Integer ftpConnectTimeoutSeconds;
    protected Integer ftpFileTransferTimeoutSeconds;
    protected Integer pauseBetweenFileRebuildMilliseconds;
    protected Integer maxBusyTimeMinutes;
    @XmlElementRef(name = "deviceAccessAppServerClusterName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deviceAccessAppServerClusterName;

    /**
     * Ruft den Wert der enableIPDeviceManagement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableIPDeviceManagement() {
        return enableIPDeviceManagement;
    }

    /**
     * Legt den Wert der enableIPDeviceManagement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableIPDeviceManagement(Boolean value) {
        this.enableIPDeviceManagement = value;
    }

    /**
     * Ruft den Wert der ftpConnectTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFtpConnectTimeoutSeconds() {
        return ftpConnectTimeoutSeconds;
    }

    /**
     * Legt den Wert der ftpConnectTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFtpConnectTimeoutSeconds(Integer value) {
        this.ftpConnectTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der ftpFileTransferTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFtpFileTransferTimeoutSeconds() {
        return ftpFileTransferTimeoutSeconds;
    }

    /**
     * Legt den Wert der ftpFileTransferTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFtpFileTransferTimeoutSeconds(Integer value) {
        this.ftpFileTransferTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der pauseBetweenFileRebuildMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPauseBetweenFileRebuildMilliseconds() {
        return pauseBetweenFileRebuildMilliseconds;
    }

    /**
     * Legt den Wert der pauseBetweenFileRebuildMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPauseBetweenFileRebuildMilliseconds(Integer value) {
        this.pauseBetweenFileRebuildMilliseconds = value;
    }

    /**
     * Ruft den Wert der maxBusyTimeMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxBusyTimeMinutes() {
        return maxBusyTimeMinutes;
    }

    /**
     * Legt den Wert der maxBusyTimeMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxBusyTimeMinutes(Integer value) {
        this.maxBusyTimeMinutes = value;
    }

    /**
     * Ruft den Wert der deviceAccessAppServerClusterName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeviceAccessAppServerClusterName() {
        return deviceAccessAppServerClusterName;
    }

    /**
     * Legt den Wert der deviceAccessAppServerClusterName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeviceAccessAppServerClusterName(JAXBElement<String> value) {
        this.deviceAccessAppServerClusterName = value;
    }

}
