//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemConnectedLineIdentificationPresentationGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemConnectedLineIdentificationPresentationGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemConnectedLineIdentificationPresentationGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enforceUserServiceAssignment" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemConnectedLineIdentificationPresentationGetResponse", propOrder = {
    "enforceUserServiceAssignment"
})
public class SystemConnectedLineIdentificationPresentationGetResponse
    extends OCIDataResponse
{

    protected boolean enforceUserServiceAssignment;

    /**
     * Ruft den Wert der enforceUserServiceAssignment-Eigenschaft ab.
     * 
     */
    public boolean isEnforceUserServiceAssignment() {
        return enforceUserServiceAssignment;
    }

    /**
     * Legt den Wert der enforceUserServiceAssignment-Eigenschaft fest.
     * 
     */
    public void setEnforceUserServiceAssignment(boolean value) {
        this.enforceUserServiceAssignment = value;
    }

}
