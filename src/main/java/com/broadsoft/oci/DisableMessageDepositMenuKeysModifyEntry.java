//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal disable message deposit menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für DisableMessageDepositMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="DisableMessageDepositMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="disconnectAfterGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="forwardAfterGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="changeForwardingDestination" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DisableMessageDepositMenuKeysModifyEntry", propOrder = {
    "disconnectAfterGreeting",
    "forwardAfterGreeting",
    "changeForwardingDestination",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class DisableMessageDepositMenuKeysModifyEntry {

    @XmlElementRef(name = "disconnectAfterGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> disconnectAfterGreeting;
    @XmlElementRef(name = "forwardAfterGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forwardAfterGreeting;
    @XmlElementRef(name = "changeForwardingDestination", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changeForwardingDestination;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repeatMenu;

    /**
     * Ruft den Wert der disconnectAfterGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDisconnectAfterGreeting() {
        return disconnectAfterGreeting;
    }

    /**
     * Legt den Wert der disconnectAfterGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDisconnectAfterGreeting(JAXBElement<String> value) {
        this.disconnectAfterGreeting = value;
    }

    /**
     * Ruft den Wert der forwardAfterGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForwardAfterGreeting() {
        return forwardAfterGreeting;
    }

    /**
     * Legt den Wert der forwardAfterGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForwardAfterGreeting(JAXBElement<String> value) {
        this.forwardAfterGreeting = value;
    }

    /**
     * Ruft den Wert der changeForwardingDestination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangeForwardingDestination() {
        return changeForwardingDestination;
    }

    /**
     * Legt den Wert der changeForwardingDestination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangeForwardingDestination(JAXBElement<String> value) {
        this.changeForwardingDestination = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepeatMenu(JAXBElement<String> value) {
        this.repeatMenu = value;
    }

}
