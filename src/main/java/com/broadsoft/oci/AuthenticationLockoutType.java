//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AuthenticationLockoutType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AuthenticationLockoutType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="None"/>
 *     <enumeration value="Temporary"/>
 *     <enumeration value="Temporary Then Permanent"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AuthenticationLockoutType")
@XmlEnum
public enum AuthenticationLockoutType {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Temporary")
    TEMPORARY("Temporary"),
    @XmlEnumValue("Temporary Then Permanent")
    TEMPORARY_THEN_PERMANENT("Temporary Then Permanent");
    private final String value;

    AuthenticationLockoutType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AuthenticationLockoutType fromValue(String v) {
        for (AuthenticationLockoutType c: AuthenticationLockoutType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
