//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the third-party emergency call service settings for the system.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemThirdPartyEmergencyCallingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemThirdPartyEmergencyCallingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="primaryHELDServerURL" type="{}URL" minOccurs="0"/>
 *         <element name="secondaryHELDServerURL" type="{}URL" minOccurs="0"/>
 *         <element name="emergencyRouteNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="emergencyRoutePort" type="{}Port1025" minOccurs="0"/>
 *         <element name="emergencyRouteTransport" type="{}ExtendedTransportProtocol" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemThirdPartyEmergencyCallingModifyRequest", propOrder = {
    "primaryHELDServerURL",
    "secondaryHELDServerURL",
    "emergencyRouteNetAddress",
    "emergencyRoutePort",
    "emergencyRouteTransport"
})
public class SystemThirdPartyEmergencyCallingModifyRequest
    extends OCIRequest
{

    @XmlElementRef(name = "primaryHELDServerURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> primaryHELDServerURL;
    @XmlElementRef(name = "secondaryHELDServerURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> secondaryHELDServerURL;
    @XmlElementRef(name = "emergencyRouteNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emergencyRouteNetAddress;
    @XmlElementRef(name = "emergencyRoutePort", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> emergencyRoutePort;
    @XmlElementRef(name = "emergencyRouteTransport", type = JAXBElement.class, required = false)
    protected JAXBElement<ExtendedTransportProtocol> emergencyRouteTransport;

    /**
     * Ruft den Wert der primaryHELDServerURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrimaryHELDServerURL() {
        return primaryHELDServerURL;
    }

    /**
     * Legt den Wert der primaryHELDServerURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrimaryHELDServerURL(JAXBElement<String> value) {
        this.primaryHELDServerURL = value;
    }

    /**
     * Ruft den Wert der secondaryHELDServerURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSecondaryHELDServerURL() {
        return secondaryHELDServerURL;
    }

    /**
     * Legt den Wert der secondaryHELDServerURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSecondaryHELDServerURL(JAXBElement<String> value) {
        this.secondaryHELDServerURL = value;
    }

    /**
     * Ruft den Wert der emergencyRouteNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmergencyRouteNetAddress() {
        return emergencyRouteNetAddress;
    }

    /**
     * Legt den Wert der emergencyRouteNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmergencyRouteNetAddress(JAXBElement<String> value) {
        this.emergencyRouteNetAddress = value;
    }

    /**
     * Ruft den Wert der emergencyRoutePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getEmergencyRoutePort() {
        return emergencyRoutePort;
    }

    /**
     * Legt den Wert der emergencyRoutePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setEmergencyRoutePort(JAXBElement<Integer> value) {
        this.emergencyRoutePort = value;
    }

    /**
     * Ruft den Wert der emergencyRouteTransport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ExtendedTransportProtocol }{@code >}
     *     
     */
    public JAXBElement<ExtendedTransportProtocol> getEmergencyRouteTransport() {
        return emergencyRouteTransport;
    }

    /**
     * Legt den Wert der emergencyRouteTransport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ExtendedTransportProtocol }{@code >}
     *     
     */
    public void setEmergencyRouteTransport(JAXBElement<ExtendedTransportProtocol> value) {
        this.emergencyRouteTransport = value;
    }

}
