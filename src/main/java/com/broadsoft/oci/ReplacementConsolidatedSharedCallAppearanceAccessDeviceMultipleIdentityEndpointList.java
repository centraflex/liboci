//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         A list of shared call appearance endpoints that replaces existing endpoints.
 *       
 * 
 * <p>Java-Klasse für ReplacementConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpointList complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ReplacementConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpointList">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="sharedCallAppearanceAccessDeviceEndpoint" type="{}ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint" maxOccurs="35"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReplacementConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpointList", propOrder = {
    "sharedCallAppearanceAccessDeviceEndpoint"
})
public class ReplacementConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpointList {

    @XmlElement(required = true)
    protected List<ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint> sharedCallAppearanceAccessDeviceEndpoint;

    /**
     * Gets the value of the sharedCallAppearanceAccessDeviceEndpoint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the sharedCallAppearanceAccessDeviceEndpoint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSharedCallAppearanceAccessDeviceEndpoint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint }
     * 
     * 
     * @return
     *     The value of the sharedCallAppearanceAccessDeviceEndpoint property.
     */
    public List<ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint> getSharedCallAppearanceAccessDeviceEndpoint() {
        if (sharedCallAppearanceAccessDeviceEndpoint == null) {
            sharedCallAppearanceAccessDeviceEndpoint = new ArrayList<>();
        }
        return this.sharedCallAppearanceAccessDeviceEndpoint;
    }

}
