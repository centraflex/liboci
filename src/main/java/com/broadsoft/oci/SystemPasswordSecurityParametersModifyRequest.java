//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the password security settings for the system.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemPasswordSecurityParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPasswordSecurityParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="useExistingHashing" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enforcePasswordChangeOnExpiry" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPasswordSecurityParametersModifyRequest", propOrder = {
    "useExistingHashing",
    "enforcePasswordChangeOnExpiry"
})
public class SystemPasswordSecurityParametersModifyRequest
    extends OCIRequest
{

    protected Boolean useExistingHashing;
    protected Boolean enforcePasswordChangeOnExpiry;

    /**
     * Ruft den Wert der useExistingHashing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseExistingHashing() {
        return useExistingHashing;
    }

    /**
     * Legt den Wert der useExistingHashing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseExistingHashing(Boolean value) {
        this.useExistingHashing = value;
    }

    /**
     * Ruft den Wert der enforcePasswordChangeOnExpiry-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnforcePasswordChangeOnExpiry() {
        return enforcePasswordChangeOnExpiry;
    }

    /**
     * Legt den Wert der enforcePasswordChangeOnExpiry-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnforcePasswordChangeOnExpiry(Boolean value) {
        this.enforcePasswordChangeOnExpiry = value;
    }

}
