//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the system level data associated with Voice Messaging.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemVoiceMessagingGroupModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemVoiceMessagingGroupModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="realDeleteForImap" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useDnInMailBody" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useShortSubjectLine" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxGreetingLengthMinutes" type="{}VoiceMessagingMaxGreetingLengthMinutes" minOccurs="0"/>
 *         <element name="maxMessageLengthMinutes" type="{}VoiceMessagingMaxMessageLengthMinutes" minOccurs="0"/>
 *         <element name="maxMailboxLengthMinutes" type="{}VoiceMessagingMailboxLengthMinutes" minOccurs="0"/>
 *         <element name="doesMessageAge" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="holdPeriodDays" type="{}VoiceMessagingHoldPeriodDays" minOccurs="0"/>
 *         <element name="mailServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="mailServerProtocol" type="{}VoiceMessagingMailServerProtocol" minOccurs="0"/>
 *         <element name="defaultDeliveryFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="defaultNotificationFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="defaultVoicePortalLockoutFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="useOutgoingMWIOnSMDI" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="mwiDelayInSeconds" type="{}VoiceMessagingMessageWaitingIndicatorDelayInSeconds" minOccurs="0"/>
 *         <element name="voicePortalScope" type="{}SystemVoicePortalScope" minOccurs="0"/>
 *         <element name="networkWideMessaging" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useExternalRouting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="defaultExternalRoutingAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemVoiceMessagingGroupModifyRequest", propOrder = {
    "realDeleteForImap",
    "useDnInMailBody",
    "useShortSubjectLine",
    "maxGreetingLengthMinutes",
    "maxMessageLengthMinutes",
    "maxMailboxLengthMinutes",
    "doesMessageAge",
    "holdPeriodDays",
    "mailServerNetAddress",
    "mailServerProtocol",
    "defaultDeliveryFromAddress",
    "defaultNotificationFromAddress",
    "defaultVoicePortalLockoutFromAddress",
    "useOutgoingMWIOnSMDI",
    "mwiDelayInSeconds",
    "voicePortalScope",
    "networkWideMessaging",
    "useExternalRouting",
    "defaultExternalRoutingAddress"
})
public class SystemVoiceMessagingGroupModifyRequest
    extends OCIRequest
{

    protected Boolean realDeleteForImap;
    protected Boolean useDnInMailBody;
    protected Boolean useShortSubjectLine;
    protected Integer maxGreetingLengthMinutes;
    protected Integer maxMessageLengthMinutes;
    protected Integer maxMailboxLengthMinutes;
    protected Boolean doesMessageAge;
    protected Integer holdPeriodDays;
    @XmlElementRef(name = "mailServerNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mailServerNetAddress;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingMailServerProtocol mailServerProtocol;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultDeliveryFromAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultNotificationFromAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultVoicePortalLockoutFromAddress;
    protected Boolean useOutgoingMWIOnSMDI;
    protected Integer mwiDelayInSeconds;
    @XmlSchemaType(name = "token")
    protected SystemVoicePortalScope voicePortalScope;
    protected Boolean networkWideMessaging;
    protected Boolean useExternalRouting;
    @XmlElementRef(name = "defaultExternalRoutingAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultExternalRoutingAddress;

    /**
     * Ruft den Wert der realDeleteForImap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRealDeleteForImap() {
        return realDeleteForImap;
    }

    /**
     * Legt den Wert der realDeleteForImap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRealDeleteForImap(Boolean value) {
        this.realDeleteForImap = value;
    }

    /**
     * Ruft den Wert der useDnInMailBody-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseDnInMailBody() {
        return useDnInMailBody;
    }

    /**
     * Legt den Wert der useDnInMailBody-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseDnInMailBody(Boolean value) {
        this.useDnInMailBody = value;
    }

    /**
     * Ruft den Wert der useShortSubjectLine-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseShortSubjectLine() {
        return useShortSubjectLine;
    }

    /**
     * Legt den Wert der useShortSubjectLine-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseShortSubjectLine(Boolean value) {
        this.useShortSubjectLine = value;
    }

    /**
     * Ruft den Wert der maxGreetingLengthMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxGreetingLengthMinutes() {
        return maxGreetingLengthMinutes;
    }

    /**
     * Legt den Wert der maxGreetingLengthMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxGreetingLengthMinutes(Integer value) {
        this.maxGreetingLengthMinutes = value;
    }

    /**
     * Ruft den Wert der maxMessageLengthMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxMessageLengthMinutes() {
        return maxMessageLengthMinutes;
    }

    /**
     * Legt den Wert der maxMessageLengthMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxMessageLengthMinutes(Integer value) {
        this.maxMessageLengthMinutes = value;
    }

    /**
     * Ruft den Wert der maxMailboxLengthMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxMailboxLengthMinutes() {
        return maxMailboxLengthMinutes;
    }

    /**
     * Legt den Wert der maxMailboxLengthMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxMailboxLengthMinutes(Integer value) {
        this.maxMailboxLengthMinutes = value;
    }

    /**
     * Ruft den Wert der doesMessageAge-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDoesMessageAge() {
        return doesMessageAge;
    }

    /**
     * Legt den Wert der doesMessageAge-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDoesMessageAge(Boolean value) {
        this.doesMessageAge = value;
    }

    /**
     * Ruft den Wert der holdPeriodDays-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHoldPeriodDays() {
        return holdPeriodDays;
    }

    /**
     * Legt den Wert der holdPeriodDays-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHoldPeriodDays(Integer value) {
        this.holdPeriodDays = value;
    }

    /**
     * Ruft den Wert der mailServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMailServerNetAddress() {
        return mailServerNetAddress;
    }

    /**
     * Legt den Wert der mailServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMailServerNetAddress(JAXBElement<String> value) {
        this.mailServerNetAddress = value;
    }

    /**
     * Ruft den Wert der mailServerProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingMailServerProtocol }
     *     
     */
    public VoiceMessagingMailServerProtocol getMailServerProtocol() {
        return mailServerProtocol;
    }

    /**
     * Legt den Wert der mailServerProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingMailServerProtocol }
     *     
     */
    public void setMailServerProtocol(VoiceMessagingMailServerProtocol value) {
        this.mailServerProtocol = value;
    }

    /**
     * Ruft den Wert der defaultDeliveryFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultDeliveryFromAddress() {
        return defaultDeliveryFromAddress;
    }

    /**
     * Legt den Wert der defaultDeliveryFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultDeliveryFromAddress(String value) {
        this.defaultDeliveryFromAddress = value;
    }

    /**
     * Ruft den Wert der defaultNotificationFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultNotificationFromAddress() {
        return defaultNotificationFromAddress;
    }

    /**
     * Legt den Wert der defaultNotificationFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultNotificationFromAddress(String value) {
        this.defaultNotificationFromAddress = value;
    }

    /**
     * Ruft den Wert der defaultVoicePortalLockoutFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultVoicePortalLockoutFromAddress() {
        return defaultVoicePortalLockoutFromAddress;
    }

    /**
     * Legt den Wert der defaultVoicePortalLockoutFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultVoicePortalLockoutFromAddress(String value) {
        this.defaultVoicePortalLockoutFromAddress = value;
    }

    /**
     * Ruft den Wert der useOutgoingMWIOnSMDI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseOutgoingMWIOnSMDI() {
        return useOutgoingMWIOnSMDI;
    }

    /**
     * Legt den Wert der useOutgoingMWIOnSMDI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseOutgoingMWIOnSMDI(Boolean value) {
        this.useOutgoingMWIOnSMDI = value;
    }

    /**
     * Ruft den Wert der mwiDelayInSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMwiDelayInSeconds() {
        return mwiDelayInSeconds;
    }

    /**
     * Legt den Wert der mwiDelayInSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMwiDelayInSeconds(Integer value) {
        this.mwiDelayInSeconds = value;
    }

    /**
     * Ruft den Wert der voicePortalScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemVoicePortalScope }
     *     
     */
    public SystemVoicePortalScope getVoicePortalScope() {
        return voicePortalScope;
    }

    /**
     * Legt den Wert der voicePortalScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemVoicePortalScope }
     *     
     */
    public void setVoicePortalScope(SystemVoicePortalScope value) {
        this.voicePortalScope = value;
    }

    /**
     * Ruft den Wert der networkWideMessaging-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNetworkWideMessaging() {
        return networkWideMessaging;
    }

    /**
     * Legt den Wert der networkWideMessaging-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNetworkWideMessaging(Boolean value) {
        this.networkWideMessaging = value;
    }

    /**
     * Ruft den Wert der useExternalRouting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseExternalRouting() {
        return useExternalRouting;
    }

    /**
     * Legt den Wert der useExternalRouting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseExternalRouting(Boolean value) {
        this.useExternalRouting = value;
    }

    /**
     * Ruft den Wert der defaultExternalRoutingAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultExternalRoutingAddress() {
        return defaultExternalRoutingAddress;
    }

    /**
     * Legt den Wert der defaultExternalRoutingAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultExternalRoutingAddress(JAXBElement<String> value) {
        this.defaultExternalRoutingAddress = value;
    }

}
