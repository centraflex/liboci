//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the user's voice messaging advanced voice management service setting.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserVoiceMessagingUserModifyAdvancedVoiceManagementRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserVoiceMessagingUserModifyAdvancedVoiceManagementRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="mailServerSelection" type="{}VoiceMessagingUserMailServerSelection" minOccurs="0"/>
 *         <element name="groupMailServerEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="groupMailServerUserId" type="{}VoiceMessagingMailServerUserId" minOccurs="0"/>
 *         <element name="groupMailServerPassword" type="{}Password" minOccurs="0"/>
 *         <choice minOccurs="0">
 *           <element name="useGroupDefaultMailServerFullMailboxLimit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *           <element name="groupMailServerFullMailboxLimit" type="{}VoiceMessagingMailboxLengthMinutes"/>
 *         </choice>
 *         <element name="personalMailServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="personalMailServerProtocol" type="{}VoiceMessagingMailServerProtocol" minOccurs="0"/>
 *         <element name="personalMailServerRealDeleteForImap" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="personalMailServerEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="personalMailServerUserId" type="{}VoiceMessagingMailServerUserId" minOccurs="0"/>
 *         <element name="personalMailServerPassword" type="{}Password" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserVoiceMessagingUserModifyAdvancedVoiceManagementRequest", propOrder = {
    "userId",
    "mailServerSelection",
    "groupMailServerEmailAddress",
    "groupMailServerUserId",
    "groupMailServerPassword",
    "useGroupDefaultMailServerFullMailboxLimit",
    "groupMailServerFullMailboxLimit",
    "personalMailServerNetAddress",
    "personalMailServerProtocol",
    "personalMailServerRealDeleteForImap",
    "personalMailServerEmailAddress",
    "personalMailServerUserId",
    "personalMailServerPassword"
})
public class UserVoiceMessagingUserModifyAdvancedVoiceManagementRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingUserMailServerSelection mailServerSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupMailServerEmailAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupMailServerUserId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupMailServerPassword;
    protected Boolean useGroupDefaultMailServerFullMailboxLimit;
    protected Integer groupMailServerFullMailboxLimit;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String personalMailServerNetAddress;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingMailServerProtocol personalMailServerProtocol;
    protected Boolean personalMailServerRealDeleteForImap;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String personalMailServerEmailAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String personalMailServerUserId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String personalMailServerPassword;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der mailServerSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingUserMailServerSelection }
     *     
     */
    public VoiceMessagingUserMailServerSelection getMailServerSelection() {
        return mailServerSelection;
    }

    /**
     * Legt den Wert der mailServerSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingUserMailServerSelection }
     *     
     */
    public void setMailServerSelection(VoiceMessagingUserMailServerSelection value) {
        this.mailServerSelection = value;
    }

    /**
     * Ruft den Wert der groupMailServerEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupMailServerEmailAddress() {
        return groupMailServerEmailAddress;
    }

    /**
     * Legt den Wert der groupMailServerEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupMailServerEmailAddress(String value) {
        this.groupMailServerEmailAddress = value;
    }

    /**
     * Ruft den Wert der groupMailServerUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupMailServerUserId() {
        return groupMailServerUserId;
    }

    /**
     * Legt den Wert der groupMailServerUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupMailServerUserId(String value) {
        this.groupMailServerUserId = value;
    }

    /**
     * Ruft den Wert der groupMailServerPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupMailServerPassword() {
        return groupMailServerPassword;
    }

    /**
     * Legt den Wert der groupMailServerPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupMailServerPassword(String value) {
        this.groupMailServerPassword = value;
    }

    /**
     * Ruft den Wert der useGroupDefaultMailServerFullMailboxLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseGroupDefaultMailServerFullMailboxLimit() {
        return useGroupDefaultMailServerFullMailboxLimit;
    }

    /**
     * Legt den Wert der useGroupDefaultMailServerFullMailboxLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseGroupDefaultMailServerFullMailboxLimit(Boolean value) {
        this.useGroupDefaultMailServerFullMailboxLimit = value;
    }

    /**
     * Ruft den Wert der groupMailServerFullMailboxLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGroupMailServerFullMailboxLimit() {
        return groupMailServerFullMailboxLimit;
    }

    /**
     * Legt den Wert der groupMailServerFullMailboxLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGroupMailServerFullMailboxLimit(Integer value) {
        this.groupMailServerFullMailboxLimit = value;
    }

    /**
     * Ruft den Wert der personalMailServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalMailServerNetAddress() {
        return personalMailServerNetAddress;
    }

    /**
     * Legt den Wert der personalMailServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalMailServerNetAddress(String value) {
        this.personalMailServerNetAddress = value;
    }

    /**
     * Ruft den Wert der personalMailServerProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingMailServerProtocol }
     *     
     */
    public VoiceMessagingMailServerProtocol getPersonalMailServerProtocol() {
        return personalMailServerProtocol;
    }

    /**
     * Legt den Wert der personalMailServerProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingMailServerProtocol }
     *     
     */
    public void setPersonalMailServerProtocol(VoiceMessagingMailServerProtocol value) {
        this.personalMailServerProtocol = value;
    }

    /**
     * Ruft den Wert der personalMailServerRealDeleteForImap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPersonalMailServerRealDeleteForImap() {
        return personalMailServerRealDeleteForImap;
    }

    /**
     * Legt den Wert der personalMailServerRealDeleteForImap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPersonalMailServerRealDeleteForImap(Boolean value) {
        this.personalMailServerRealDeleteForImap = value;
    }

    /**
     * Ruft den Wert der personalMailServerEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalMailServerEmailAddress() {
        return personalMailServerEmailAddress;
    }

    /**
     * Legt den Wert der personalMailServerEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalMailServerEmailAddress(String value) {
        this.personalMailServerEmailAddress = value;
    }

    /**
     * Ruft den Wert der personalMailServerUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalMailServerUserId() {
        return personalMailServerUserId;
    }

    /**
     * Legt den Wert der personalMailServerUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalMailServerUserId(String value) {
        this.personalMailServerUserId = value;
    }

    /**
     * Ruft den Wert der personalMailServerPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalMailServerPassword() {
        return personalMailServerPassword;
    }

    /**
     * Legt den Wert der personalMailServerPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalMailServerPassword(String value) {
        this.personalMailServerPassword = value;
    }

}
