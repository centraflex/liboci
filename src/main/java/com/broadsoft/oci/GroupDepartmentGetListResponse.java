//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupDepartmentGetListRequest.
 *         The response includes two parallel arrays of department keys and department display names.
 *         
 *         Replaced by: GroupDepartmentGetListResponse18         
 *       
 * 
 * <p>Java-Klasse für GroupDepartmentGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDepartmentGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="departmentKey" type="{}DepartmentKey" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="fullPathName" type="{}DepartmentFullPathName" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDepartmentGetListResponse", propOrder = {
    "departmentKey",
    "fullPathName"
})
public class GroupDepartmentGetListResponse
    extends OCIDataResponse
{

    protected List<DepartmentKey> departmentKey;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> fullPathName;

    /**
     * Gets the value of the departmentKey property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the departmentKey property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDepartmentKey().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DepartmentKey }
     * 
     * 
     * @return
     *     The value of the departmentKey property.
     */
    public List<DepartmentKey> getDepartmentKey() {
        if (departmentKey == null) {
            departmentKey = new ArrayList<>();
        }
        return this.departmentKey;
    }

    /**
     * Gets the value of the fullPathName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the fullPathName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFullPathName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the fullPathName property.
     */
    public List<String> getFullPathName() {
        if (fullPathName == null) {
            fullPathName = new ArrayList<>();
        }
        return this.fullPathName;
    }

}
