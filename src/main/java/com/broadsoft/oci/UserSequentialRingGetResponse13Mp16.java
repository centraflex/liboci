//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserSequentialRingGetRequest13mp16. The criteria table's column headings are:
 *         "Is Active", "Criteria Name", "Time Schedule" and "Calls From".
 *         Replaced By: UserSequentialRingGetResponse14sp4
 *       
 * 
 * <p>Java-Klasse für UserSequentialRingGetResponse13mp16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserSequentialRingGetResponse13mp16">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="ringBaseLocationFirst" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="baseLocationNumberOfRings" type="{}SequentialRingNumberOfRings"/>
 *         <element name="continueIfBaseLocationIsBusy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="callerMayStopSearch" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="Location01" type="{}SequentialRingLocation"/>
 *         <element name="Location02" type="{}SequentialRingLocation"/>
 *         <element name="Location03" type="{}SequentialRingLocation"/>
 *         <element name="Location04" type="{}SequentialRingLocation"/>
 *         <element name="Location05" type="{}SequentialRingLocation"/>
 *         <element name="criteriaTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserSequentialRingGetResponse13mp16", propOrder = {
    "ringBaseLocationFirst",
    "baseLocationNumberOfRings",
    "continueIfBaseLocationIsBusy",
    "callerMayStopSearch",
    "location01",
    "location02",
    "location03",
    "location04",
    "location05",
    "criteriaTable"
})
public class UserSequentialRingGetResponse13Mp16
    extends OCIDataResponse
{

    protected boolean ringBaseLocationFirst;
    protected int baseLocationNumberOfRings;
    protected boolean continueIfBaseLocationIsBusy;
    protected boolean callerMayStopSearch;
    @XmlElement(name = "Location01", required = true)
    protected SequentialRingLocation location01;
    @XmlElement(name = "Location02", required = true)
    protected SequentialRingLocation location02;
    @XmlElement(name = "Location03", required = true)
    protected SequentialRingLocation location03;
    @XmlElement(name = "Location04", required = true)
    protected SequentialRingLocation location04;
    @XmlElement(name = "Location05", required = true)
    protected SequentialRingLocation location05;
    @XmlElement(required = true)
    protected OCITable criteriaTable;

    /**
     * Ruft den Wert der ringBaseLocationFirst-Eigenschaft ab.
     * 
     */
    public boolean isRingBaseLocationFirst() {
        return ringBaseLocationFirst;
    }

    /**
     * Legt den Wert der ringBaseLocationFirst-Eigenschaft fest.
     * 
     */
    public void setRingBaseLocationFirst(boolean value) {
        this.ringBaseLocationFirst = value;
    }

    /**
     * Ruft den Wert der baseLocationNumberOfRings-Eigenschaft ab.
     * 
     */
    public int getBaseLocationNumberOfRings() {
        return baseLocationNumberOfRings;
    }

    /**
     * Legt den Wert der baseLocationNumberOfRings-Eigenschaft fest.
     * 
     */
    public void setBaseLocationNumberOfRings(int value) {
        this.baseLocationNumberOfRings = value;
    }

    /**
     * Ruft den Wert der continueIfBaseLocationIsBusy-Eigenschaft ab.
     * 
     */
    public boolean isContinueIfBaseLocationIsBusy() {
        return continueIfBaseLocationIsBusy;
    }

    /**
     * Legt den Wert der continueIfBaseLocationIsBusy-Eigenschaft fest.
     * 
     */
    public void setContinueIfBaseLocationIsBusy(boolean value) {
        this.continueIfBaseLocationIsBusy = value;
    }

    /**
     * Ruft den Wert der callerMayStopSearch-Eigenschaft ab.
     * 
     */
    public boolean isCallerMayStopSearch() {
        return callerMayStopSearch;
    }

    /**
     * Legt den Wert der callerMayStopSearch-Eigenschaft fest.
     * 
     */
    public void setCallerMayStopSearch(boolean value) {
        this.callerMayStopSearch = value;
    }

    /**
     * Ruft den Wert der location01-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SequentialRingLocation }
     *     
     */
    public SequentialRingLocation getLocation01() {
        return location01;
    }

    /**
     * Legt den Wert der location01-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SequentialRingLocation }
     *     
     */
    public void setLocation01(SequentialRingLocation value) {
        this.location01 = value;
    }

    /**
     * Ruft den Wert der location02-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SequentialRingLocation }
     *     
     */
    public SequentialRingLocation getLocation02() {
        return location02;
    }

    /**
     * Legt den Wert der location02-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SequentialRingLocation }
     *     
     */
    public void setLocation02(SequentialRingLocation value) {
        this.location02 = value;
    }

    /**
     * Ruft den Wert der location03-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SequentialRingLocation }
     *     
     */
    public SequentialRingLocation getLocation03() {
        return location03;
    }

    /**
     * Legt den Wert der location03-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SequentialRingLocation }
     *     
     */
    public void setLocation03(SequentialRingLocation value) {
        this.location03 = value;
    }

    /**
     * Ruft den Wert der location04-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SequentialRingLocation }
     *     
     */
    public SequentialRingLocation getLocation04() {
        return location04;
    }

    /**
     * Legt den Wert der location04-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SequentialRingLocation }
     *     
     */
    public void setLocation04(SequentialRingLocation value) {
        this.location04 = value;
    }

    /**
     * Ruft den Wert der location05-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SequentialRingLocation }
     *     
     */
    public SequentialRingLocation getLocation05() {
        return location05;
    }

    /**
     * Legt den Wert der location05-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SequentialRingLocation }
     *     
     */
    public void setLocation05(SequentialRingLocation value) {
        this.location05 = value;
    }

    /**
     * Ruft den Wert der criteriaTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getCriteriaTable() {
        return criteriaTable;
    }

    /**
     * Legt den Wert der criteriaTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setCriteriaTable(OCITable value) {
        this.criteriaTable = value;
    }

}
