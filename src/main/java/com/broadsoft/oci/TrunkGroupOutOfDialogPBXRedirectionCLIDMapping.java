//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für TrunkGroupOutOfDialogPBXRedirectionCLIDMapping.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="TrunkGroupOutOfDialogPBXRedirectionCLIDMapping">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Disabled"/>
 *     <enumeration value="Enabled And Ignore Policies"/>
 *     <enumeration value="Enabled And Apply Policies"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "TrunkGroupOutOfDialogPBXRedirectionCLIDMapping")
@XmlEnum
public enum TrunkGroupOutOfDialogPBXRedirectionCLIDMapping {

    @XmlEnumValue("Disabled")
    DISABLED("Disabled"),
    @XmlEnumValue("Enabled And Ignore Policies")
    ENABLED_AND_IGNORE_POLICIES("Enabled And Ignore Policies"),
    @XmlEnumValue("Enabled And Apply Policies")
    ENABLED_AND_APPLY_POLICIES("Enabled And Apply Policies");
    private final String value;

    TrunkGroupOutOfDialogPBXRedirectionCLIDMapping(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TrunkGroupOutOfDialogPBXRedirectionCLIDMapping fromValue(String v) {
        for (TrunkGroupOutOfDialogPBXRedirectionCLIDMapping c: TrunkGroupOutOfDialogPBXRedirectionCLIDMapping.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
