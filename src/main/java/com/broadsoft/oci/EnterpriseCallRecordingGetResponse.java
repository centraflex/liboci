//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *          Response to the EnterpriseCallRecordingGetRequest.
 *          The response contains the enterprise's Call Recording attributes.
 *        
 * 
 * <p>Java-Klasse für EnterpriseCallRecordingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseCallRecordingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useCloudPBX" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useEnterpriseSetting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="FQDN" type="{}NetAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseCallRecordingGetResponse", propOrder = {
    "useCloudPBX",
    "useEnterpriseSetting",
    "fqdn"
})
public class EnterpriseCallRecordingGetResponse
    extends OCIDataResponse
{

    protected boolean useCloudPBX;
    protected Boolean useEnterpriseSetting;
    @XmlElement(name = "FQDN")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String fqdn;

    /**
     * Ruft den Wert der useCloudPBX-Eigenschaft ab.
     * 
     */
    public boolean isUseCloudPBX() {
        return useCloudPBX;
    }

    /**
     * Legt den Wert der useCloudPBX-Eigenschaft fest.
     * 
     */
    public void setUseCloudPBX(boolean value) {
        this.useCloudPBX = value;
    }

    /**
     * Ruft den Wert der useEnterpriseSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseEnterpriseSetting() {
        return useEnterpriseSetting;
    }

    /**
     * Legt den Wert der useEnterpriseSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseEnterpriseSetting(Boolean value) {
        this.useEnterpriseSetting = value;
    }

    /**
     * Ruft den Wert der fqdn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFQDN() {
        return fqdn;
    }

    /**
     * Legt den Wert der fqdn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFQDN(String value) {
        this.fqdn = value;
    }

}
