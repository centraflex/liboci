//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modifies the currently configured carriers for a group.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupPreferredCarrierGroupModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupPreferredCarrierGroupModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="intraLataCarrier" type="{}GroupPreferredCarrierNameModify" minOccurs="0"/>
 *         <element name="interLataCarrier" type="{}GroupPreferredCarrierNameModify" minOccurs="0"/>
 *         <element name="internationalCarrier" type="{}GroupPreferredCarrierNameModify" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupPreferredCarrierGroupModifyRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "intraLataCarrier",
    "interLataCarrier",
    "internationalCarrier"
})
public class GroupPreferredCarrierGroupModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected GroupPreferredCarrierNameModify intraLataCarrier;
    protected GroupPreferredCarrierNameModify interLataCarrier;
    protected GroupPreferredCarrierNameModify internationalCarrier;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der intraLataCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupPreferredCarrierNameModify }
     *     
     */
    public GroupPreferredCarrierNameModify getIntraLataCarrier() {
        return intraLataCarrier;
    }

    /**
     * Legt den Wert der intraLataCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupPreferredCarrierNameModify }
     *     
     */
    public void setIntraLataCarrier(GroupPreferredCarrierNameModify value) {
        this.intraLataCarrier = value;
    }

    /**
     * Ruft den Wert der interLataCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupPreferredCarrierNameModify }
     *     
     */
    public GroupPreferredCarrierNameModify getInterLataCarrier() {
        return interLataCarrier;
    }

    /**
     * Legt den Wert der interLataCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupPreferredCarrierNameModify }
     *     
     */
    public void setInterLataCarrier(GroupPreferredCarrierNameModify value) {
        this.interLataCarrier = value;
    }

    /**
     * Ruft den Wert der internationalCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupPreferredCarrierNameModify }
     *     
     */
    public GroupPreferredCarrierNameModify getInternationalCarrier() {
        return internationalCarrier;
    }

    /**
     * Legt den Wert der internationalCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupPreferredCarrierNameModify }
     *     
     */
    public void setInternationalCarrier(GroupPreferredCarrierNameModify value) {
        this.internationalCarrier = value;
    }

}
