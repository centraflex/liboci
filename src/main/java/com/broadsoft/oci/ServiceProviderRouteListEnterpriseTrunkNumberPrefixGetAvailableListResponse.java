//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderRouteListEnterpriseTrunkNumberPrefixGetAvailableListRequest. 
 *         Contains a list of available number prefixes not yet assigned to any group.
 *         The column headings are "Number Prefix"","Is Active", “Extension Range Start” and “Extension Range End”.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderRouteListEnterpriseTrunkNumberPrefixGetAvailableListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderRouteListEnterpriseTrunkNumberPrefixGetAvailableListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="availableNumberPrefixTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderRouteListEnterpriseTrunkNumberPrefixGetAvailableListResponse", propOrder = {
    "availableNumberPrefixTable"
})
public class ServiceProviderRouteListEnterpriseTrunkNumberPrefixGetAvailableListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable availableNumberPrefixTable;

    /**
     * Ruft den Wert der availableNumberPrefixTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAvailableNumberPrefixTable() {
        return availableNumberPrefixTable;
    }

    /**
     * Legt den Wert der availableNumberPrefixTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAvailableNumberPrefixTable(OCITable value) {
        this.availableNumberPrefixTable = value;
    }

}
