//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemMaliciousCallTraceGetRequest.
 *         The response contains the Malicious Call Trace system parameters and the list of users
 *         that use the Malicious Call Trace feature.
 * 
 *         The column headings are "Service Provider Id",
 *         "Group Id", "User Id", "Last Name", "First Name", "Phone Number", "Trace Type", "Status",
 *         "Hiragana Last Name" and "Hiragana First Name", "Extension", "Department", "Email Address".
 *       
 * 
 * <p>Java-Klasse für SystemMaliciousCallTraceGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemMaliciousCallTraceGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="playMCTWarningAnnouncement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="userTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemMaliciousCallTraceGetResponse", propOrder = {
    "playMCTWarningAnnouncement",
    "userTable"
})
public class SystemMaliciousCallTraceGetResponse
    extends OCIDataResponse
{

    protected boolean playMCTWarningAnnouncement;
    @XmlElement(required = true)
    protected OCITable userTable;

    /**
     * Ruft den Wert der playMCTWarningAnnouncement-Eigenschaft ab.
     * 
     */
    public boolean isPlayMCTWarningAnnouncement() {
        return playMCTWarningAnnouncement;
    }

    /**
     * Legt den Wert der playMCTWarningAnnouncement-Eigenschaft fest.
     * 
     */
    public void setPlayMCTWarningAnnouncement(boolean value) {
        this.playMCTWarningAnnouncement = value;
    }

    /**
     * Ruft den Wert der userTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserTable() {
        return userTable;
    }

    /**
     * Legt den Wert der userTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserTable(OCITable value) {
        this.userTable = value;
    }

}
