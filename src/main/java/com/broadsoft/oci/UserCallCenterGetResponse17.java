//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the UserCallCenterGetRequest17.
 *         Contains the user's call center settings.
 *         Indicates whether the agent is current available (logged in) to each call center in the list.
 *         Contains a table with column headings: "Service User Id", "Phone Number", "Extension", "Available", "Logoff Allowed", "Type", and "Priority".
 *         
 *         Replaced by UserCallCenterGetResponse17sp4.
 *       
 * 
 * <p>Java-Klasse für UserCallCenterGetResponse17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallCenterGetResponse17">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="agentACDState" type="{}AgentACDState" minOccurs="0"/>
 *         <element name="agentUnavailableCode" type="{}CallCenterAgentUnavailableCode" minOccurs="0"/>
 *         <element name="useDefaultGuardTimer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableGuardTimer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="guardTimerSeconds" type="{}CallCenterGuardTimerSeconds"/>
 *         <element name="useSystemDefaultUnavailableSettings" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forceAgentUnavailableOnDNDActivation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forceUnavailableOnPersonalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forceAgentUnavailableOnBouncedCallLimit" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="numberConsecutiveBouncedCallsToForceAgentUnavailable" type="{}CallCenterConsecutiveBouncedCallsToForceAgentUnavailable" minOccurs="0"/>
 *         <element name="makeOutgoingCallsAsCallCenter" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="outgoingCallDNIS" type="{}DNISKey" minOccurs="0"/>
 *         <element name="callCenterTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallCenterGetResponse17", propOrder = {
    "agentACDState",
    "agentUnavailableCode",
    "useDefaultGuardTimer",
    "enableGuardTimer",
    "guardTimerSeconds",
    "useSystemDefaultUnavailableSettings",
    "forceAgentUnavailableOnDNDActivation",
    "forceUnavailableOnPersonalCalls",
    "forceAgentUnavailableOnBouncedCallLimit",
    "numberConsecutiveBouncedCallsToForceAgentUnavailable",
    "makeOutgoingCallsAsCallCenter",
    "outgoingCallDNIS",
    "callCenterTable"
})
public class UserCallCenterGetResponse17
    extends OCIDataResponse
{

    @XmlSchemaType(name = "token")
    protected AgentACDState agentACDState;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String agentUnavailableCode;
    protected boolean useDefaultGuardTimer;
    protected boolean enableGuardTimer;
    protected int guardTimerSeconds;
    protected Boolean useSystemDefaultUnavailableSettings;
    protected Boolean forceAgentUnavailableOnDNDActivation;
    protected Boolean forceUnavailableOnPersonalCalls;
    protected Boolean forceAgentUnavailableOnBouncedCallLimit;
    protected Integer numberConsecutiveBouncedCallsToForceAgentUnavailable;
    protected Boolean makeOutgoingCallsAsCallCenter;
    protected DNISKey outgoingCallDNIS;
    @XmlElement(required = true)
    protected OCITable callCenterTable;

    /**
     * Ruft den Wert der agentACDState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AgentACDState }
     *     
     */
    public AgentACDState getAgentACDState() {
        return agentACDState;
    }

    /**
     * Legt den Wert der agentACDState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentACDState }
     *     
     */
    public void setAgentACDState(AgentACDState value) {
        this.agentACDState = value;
    }

    /**
     * Ruft den Wert der agentUnavailableCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentUnavailableCode() {
        return agentUnavailableCode;
    }

    /**
     * Legt den Wert der agentUnavailableCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentUnavailableCode(String value) {
        this.agentUnavailableCode = value;
    }

    /**
     * Ruft den Wert der useDefaultGuardTimer-Eigenschaft ab.
     * 
     */
    public boolean isUseDefaultGuardTimer() {
        return useDefaultGuardTimer;
    }

    /**
     * Legt den Wert der useDefaultGuardTimer-Eigenschaft fest.
     * 
     */
    public void setUseDefaultGuardTimer(boolean value) {
        this.useDefaultGuardTimer = value;
    }

    /**
     * Ruft den Wert der enableGuardTimer-Eigenschaft ab.
     * 
     */
    public boolean isEnableGuardTimer() {
        return enableGuardTimer;
    }

    /**
     * Legt den Wert der enableGuardTimer-Eigenschaft fest.
     * 
     */
    public void setEnableGuardTimer(boolean value) {
        this.enableGuardTimer = value;
    }

    /**
     * Ruft den Wert der guardTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getGuardTimerSeconds() {
        return guardTimerSeconds;
    }

    /**
     * Legt den Wert der guardTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setGuardTimerSeconds(int value) {
        this.guardTimerSeconds = value;
    }

    /**
     * Ruft den Wert der useSystemDefaultUnavailableSettings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseSystemDefaultUnavailableSettings() {
        return useSystemDefaultUnavailableSettings;
    }

    /**
     * Legt den Wert der useSystemDefaultUnavailableSettings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSystemDefaultUnavailableSettings(Boolean value) {
        this.useSystemDefaultUnavailableSettings = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnDNDActivation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceAgentUnavailableOnDNDActivation() {
        return forceAgentUnavailableOnDNDActivation;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnDNDActivation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceAgentUnavailableOnDNDActivation(Boolean value) {
        this.forceAgentUnavailableOnDNDActivation = value;
    }

    /**
     * Ruft den Wert der forceUnavailableOnPersonalCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceUnavailableOnPersonalCalls() {
        return forceUnavailableOnPersonalCalls;
    }

    /**
     * Legt den Wert der forceUnavailableOnPersonalCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceUnavailableOnPersonalCalls(Boolean value) {
        this.forceUnavailableOnPersonalCalls = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnBouncedCallLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceAgentUnavailableOnBouncedCallLimit() {
        return forceAgentUnavailableOnBouncedCallLimit;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnBouncedCallLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceAgentUnavailableOnBouncedCallLimit(Boolean value) {
        this.forceAgentUnavailableOnBouncedCallLimit = value;
    }

    /**
     * Ruft den Wert der numberConsecutiveBouncedCallsToForceAgentUnavailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberConsecutiveBouncedCallsToForceAgentUnavailable() {
        return numberConsecutiveBouncedCallsToForceAgentUnavailable;
    }

    /**
     * Legt den Wert der numberConsecutiveBouncedCallsToForceAgentUnavailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberConsecutiveBouncedCallsToForceAgentUnavailable(Integer value) {
        this.numberConsecutiveBouncedCallsToForceAgentUnavailable = value;
    }

    /**
     * Ruft den Wert der makeOutgoingCallsAsCallCenter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMakeOutgoingCallsAsCallCenter() {
        return makeOutgoingCallsAsCallCenter;
    }

    /**
     * Legt den Wert der makeOutgoingCallsAsCallCenter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMakeOutgoingCallsAsCallCenter(Boolean value) {
        this.makeOutgoingCallsAsCallCenter = value;
    }

    /**
     * Ruft den Wert der outgoingCallDNIS-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DNISKey }
     *     
     */
    public DNISKey getOutgoingCallDNIS() {
        return outgoingCallDNIS;
    }

    /**
     * Legt den Wert der outgoingCallDNIS-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DNISKey }
     *     
     */
    public void setOutgoingCallDNIS(DNISKey value) {
        this.outgoingCallDNIS = value;
    }

    /**
     * Ruft den Wert der callCenterTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getCallCenterTable() {
        return callCenterTable;
    }

    /**
     * Legt den Wert der callCenterTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setCallCenterTable(OCITable value) {
        this.callCenterTable = value;
    }

}
