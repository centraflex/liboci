//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Get the address and credentials of the File Repository hosting the requested access device file.
 *         Also get the file name and path on the File Repository.
 *         The response is either a DeviceManagementFileAuthLocationGetResponse18 or an ErrorResponse.
 *         Replaced by: DeviceManagementFileAuthLocationGetRequest21.
 *       
 * 
 * <p>Java-Klasse für DeviceManagementFileAuthLocationGetRequest18 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="DeviceManagementFileAuthLocationGetRequest18">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="deviceAccessProtocol" type="{}DeviceAccessProtocol16"/>
 *         <element name="deviceAccessMethod" type="{}FileRepositoryAccessType"/>
 *         <element name="deviceAccessURI" type="{}DeviceManagementAccessURI"/>
 *         <element name="accessDeviceUserName" type="{}UserId" minOccurs="0"/>
 *         <element name="accessDeviceUserPassword" type="{}Password" minOccurs="0"/>
 *         <element name="signedPassword" type="{}SignedPassword" minOccurs="0"/>
 *         <element name="macAddress" type="{}AccessDeviceMACAddress" minOccurs="0"/>
 *         <element name="realmName" type="{}RealmName" minOccurs="0"/>
 *         <element name="digestHa1Complement" type="{}DigestHa1Complement" minOccurs="0"/>
 *         <element name="digestResponse" type="{}Md5Hash" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeviceManagementFileAuthLocationGetRequest18", propOrder = {
    "deviceAccessProtocol",
    "deviceAccessMethod",
    "deviceAccessURI",
    "accessDeviceUserName",
    "accessDeviceUserPassword",
    "signedPassword",
    "macAddress",
    "realmName",
    "digestHa1Complement",
    "digestResponse"
})
public class DeviceManagementFileAuthLocationGetRequest18
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceAccessProtocol16 deviceAccessProtocol;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceAccessMethod;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceAccessURI;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accessDeviceUserName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accessDeviceUserPassword;
    protected String signedPassword;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String macAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String realmName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String digestHa1Complement;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String digestResponse;

    /**
     * Ruft den Wert der deviceAccessProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceAccessProtocol16 }
     *     
     */
    public DeviceAccessProtocol16 getDeviceAccessProtocol() {
        return deviceAccessProtocol;
    }

    /**
     * Legt den Wert der deviceAccessProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceAccessProtocol16 }
     *     
     */
    public void setDeviceAccessProtocol(DeviceAccessProtocol16 value) {
        this.deviceAccessProtocol = value;
    }

    /**
     * Ruft den Wert der deviceAccessMethod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceAccessMethod() {
        return deviceAccessMethod;
    }

    /**
     * Legt den Wert der deviceAccessMethod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceAccessMethod(String value) {
        this.deviceAccessMethod = value;
    }

    /**
     * Ruft den Wert der deviceAccessURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceAccessURI() {
        return deviceAccessURI;
    }

    /**
     * Legt den Wert der deviceAccessURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceAccessURI(String value) {
        this.deviceAccessURI = value;
    }

    /**
     * Ruft den Wert der accessDeviceUserName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessDeviceUserName() {
        return accessDeviceUserName;
    }

    /**
     * Legt den Wert der accessDeviceUserName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessDeviceUserName(String value) {
        this.accessDeviceUserName = value;
    }

    /**
     * Ruft den Wert der accessDeviceUserPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessDeviceUserPassword() {
        return accessDeviceUserPassword;
    }

    /**
     * Legt den Wert der accessDeviceUserPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessDeviceUserPassword(String value) {
        this.accessDeviceUserPassword = value;
    }

    /**
     * Ruft den Wert der signedPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSignedPassword() {
        return signedPassword;
    }

    /**
     * Legt den Wert der signedPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSignedPassword(String value) {
        this.signedPassword = value;
    }

    /**
     * Ruft den Wert der macAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacAddress() {
        return macAddress;
    }

    /**
     * Legt den Wert der macAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacAddress(String value) {
        this.macAddress = value;
    }

    /**
     * Ruft den Wert der realmName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRealmName() {
        return realmName;
    }

    /**
     * Legt den Wert der realmName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRealmName(String value) {
        this.realmName = value;
    }

    /**
     * Ruft den Wert der digestHa1Complement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDigestHa1Complement() {
        return digestHa1Complement;
    }

    /**
     * Legt den Wert der digestHa1Complement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDigestHa1Complement(String value) {
        this.digestHa1Complement = value;
    }

    /**
     * Ruft den Wert der digestResponse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDigestResponse() {
        return digestResponse;
    }

    /**
     * Legt den Wert der digestResponse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDigestResponse(String value) {
        this.digestResponse = value;
    }

}
