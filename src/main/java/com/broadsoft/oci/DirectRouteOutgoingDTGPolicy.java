//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DirectRouteOutgoingDTGPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DirectRouteOutgoingDTGPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Direct Route DTG"/>
 *     <enumeration value="Trunk Group DTG"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DirectRouteOutgoingDTGPolicy")
@XmlEnum
public enum DirectRouteOutgoingDTGPolicy {

    @XmlEnumValue("Direct Route DTG")
    DIRECT_ROUTE_DTG("Direct Route DTG"),
    @XmlEnumValue("Trunk Group DTG")
    TRUNK_GROUP_DTG("Trunk Group DTG");
    private final String value;

    DirectRouteOutgoingDTGPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DirectRouteOutgoingDTGPolicy fromValue(String v) {
        for (DirectRouteOutgoingDTGPolicy c: DirectRouteOutgoingDTGPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
