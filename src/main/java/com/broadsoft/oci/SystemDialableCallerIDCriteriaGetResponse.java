//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the SystemDialableCallerIDCriteriaGetRequest.
 *         The response contains the Dialable Caller ID Criteria information.
 *       
 * 
 * <p>Java-Klasse für SystemDialableCallerIDCriteriaGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDialableCallerIDCriteriaGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="description" type="{}DialableCallerIDCriteriaDescription" minOccurs="0"/>
 *         <element name="prefixDigits" type="{}DialableCallerIDPrefixDigits" minOccurs="0"/>
 *         <element name="matchCallType" type="{}CommunicationBarringCallType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="matchAlternateCallIndicator" type="{}CommunicationBarringAlternateCallIndicator" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="matchLocalCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchNationalCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchInterlataCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchIntralataCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchInternationalCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchPrivateCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchEmergencyCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="matchOtherCategory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDialableCallerIDCriteriaGetResponse", propOrder = {
    "description",
    "prefixDigits",
    "matchCallType",
    "matchAlternateCallIndicator",
    "matchLocalCategory",
    "matchNationalCategory",
    "matchInterlataCategory",
    "matchIntralataCategory",
    "matchInternationalCategory",
    "matchPrivateCategory",
    "matchEmergencyCategory",
    "matchOtherCategory"
})
public class SystemDialableCallerIDCriteriaGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String prefixDigits;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> matchCallType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> matchAlternateCallIndicator;
    protected boolean matchLocalCategory;
    protected boolean matchNationalCategory;
    protected boolean matchInterlataCategory;
    protected boolean matchIntralataCategory;
    protected boolean matchInternationalCategory;
    protected boolean matchPrivateCategory;
    protected boolean matchEmergencyCategory;
    protected boolean matchOtherCategory;

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der prefixDigits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrefixDigits() {
        return prefixDigits;
    }

    /**
     * Legt den Wert der prefixDigits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrefixDigits(String value) {
        this.prefixDigits = value;
    }

    /**
     * Gets the value of the matchCallType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the matchCallType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMatchCallType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the matchCallType property.
     */
    public List<String> getMatchCallType() {
        if (matchCallType == null) {
            matchCallType = new ArrayList<>();
        }
        return this.matchCallType;
    }

    /**
     * Gets the value of the matchAlternateCallIndicator property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the matchAlternateCallIndicator property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMatchAlternateCallIndicator().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the matchAlternateCallIndicator property.
     */
    public List<String> getMatchAlternateCallIndicator() {
        if (matchAlternateCallIndicator == null) {
            matchAlternateCallIndicator = new ArrayList<>();
        }
        return this.matchAlternateCallIndicator;
    }

    /**
     * Ruft den Wert der matchLocalCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchLocalCategory() {
        return matchLocalCategory;
    }

    /**
     * Legt den Wert der matchLocalCategory-Eigenschaft fest.
     * 
     */
    public void setMatchLocalCategory(boolean value) {
        this.matchLocalCategory = value;
    }

    /**
     * Ruft den Wert der matchNationalCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchNationalCategory() {
        return matchNationalCategory;
    }

    /**
     * Legt den Wert der matchNationalCategory-Eigenschaft fest.
     * 
     */
    public void setMatchNationalCategory(boolean value) {
        this.matchNationalCategory = value;
    }

    /**
     * Ruft den Wert der matchInterlataCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchInterlataCategory() {
        return matchInterlataCategory;
    }

    /**
     * Legt den Wert der matchInterlataCategory-Eigenschaft fest.
     * 
     */
    public void setMatchInterlataCategory(boolean value) {
        this.matchInterlataCategory = value;
    }

    /**
     * Ruft den Wert der matchIntralataCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchIntralataCategory() {
        return matchIntralataCategory;
    }

    /**
     * Legt den Wert der matchIntralataCategory-Eigenschaft fest.
     * 
     */
    public void setMatchIntralataCategory(boolean value) {
        this.matchIntralataCategory = value;
    }

    /**
     * Ruft den Wert der matchInternationalCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchInternationalCategory() {
        return matchInternationalCategory;
    }

    /**
     * Legt den Wert der matchInternationalCategory-Eigenschaft fest.
     * 
     */
    public void setMatchInternationalCategory(boolean value) {
        this.matchInternationalCategory = value;
    }

    /**
     * Ruft den Wert der matchPrivateCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchPrivateCategory() {
        return matchPrivateCategory;
    }

    /**
     * Legt den Wert der matchPrivateCategory-Eigenschaft fest.
     * 
     */
    public void setMatchPrivateCategory(boolean value) {
        this.matchPrivateCategory = value;
    }

    /**
     * Ruft den Wert der matchEmergencyCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchEmergencyCategory() {
        return matchEmergencyCategory;
    }

    /**
     * Legt den Wert der matchEmergencyCategory-Eigenschaft fest.
     * 
     */
    public void setMatchEmergencyCategory(boolean value) {
        this.matchEmergencyCategory = value;
    }

    /**
     * Ruft den Wert der matchOtherCategory-Eigenschaft ab.
     * 
     */
    public boolean isMatchOtherCategory() {
        return matchOtherCategory;
    }

    /**
     * Legt den Wert der matchOtherCategory-Eigenschaft fest.
     * 
     */
    public void setMatchOtherCategory(boolean value) {
        this.matchOtherCategory = value;
    }

}
