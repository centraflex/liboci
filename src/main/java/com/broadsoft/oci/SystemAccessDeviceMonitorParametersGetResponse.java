//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemAccessDeviceMonitorParametersGetListRequest.
 *         Contains a list of system Access Device Monitor parameters.
 *       
 * 
 * <p>Java-Klasse für SystemAccessDeviceMonitorParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAccessDeviceMonitorParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="pollingIntervalMinutes" type="{}AccessDeviceMonitorPollingIntervalMinutes"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAccessDeviceMonitorParametersGetResponse", propOrder = {
    "pollingIntervalMinutes"
})
public class SystemAccessDeviceMonitorParametersGetResponse
    extends OCIDataResponse
{

    protected int pollingIntervalMinutes;

    /**
     * Ruft den Wert der pollingIntervalMinutes-Eigenschaft ab.
     * 
     */
    public int getPollingIntervalMinutes() {
        return pollingIntervalMinutes;
    }

    /**
     * Legt den Wert der pollingIntervalMinutes-Eigenschaft fest.
     * 
     */
    public void setPollingIntervalMinutes(int value) {
        this.pollingIntervalMinutes = value;
    }

}
