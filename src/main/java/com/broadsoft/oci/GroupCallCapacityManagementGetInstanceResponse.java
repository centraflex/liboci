//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupCallCapacityManagementGetInstanceRequest.
 *         Contains a table with column headings: "User Id", "Last Name", "First Name", "Hiragana Last Name", "Hiragana First Name",
 *         "Phone Number", "Extension", "Department", "Email Address".
 *       
 * 
 * <p>Java-Klasse für GroupCallCapacityManagementGetInstanceResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCapacityManagementGetInstanceResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="name" type="{}ServiceInstanceName"/>
 *         <element name="maxActiveCallsAllowed" type="{}CallCapacityCallLimit"/>
 *         <element name="maxIncomingActiveCallsAllowed" type="{}CallCapacityCallLimit" minOccurs="0"/>
 *         <element name="maxOutgoingActiveCallsAllowed" type="{}CallCapacityCallLimit" minOccurs="0"/>
 *         <element name="defaultGroupForNewUsers" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="userTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCapacityManagementGetInstanceResponse", propOrder = {
    "name",
    "maxActiveCallsAllowed",
    "maxIncomingActiveCallsAllowed",
    "maxOutgoingActiveCallsAllowed",
    "defaultGroupForNewUsers",
    "userTable"
})
public class GroupCallCapacityManagementGetInstanceResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    protected int maxActiveCallsAllowed;
    protected Integer maxIncomingActiveCallsAllowed;
    protected Integer maxOutgoingActiveCallsAllowed;
    protected boolean defaultGroupForNewUsers;
    @XmlElement(required = true)
    protected OCITable userTable;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der maxActiveCallsAllowed-Eigenschaft ab.
     * 
     */
    public int getMaxActiveCallsAllowed() {
        return maxActiveCallsAllowed;
    }

    /**
     * Legt den Wert der maxActiveCallsAllowed-Eigenschaft fest.
     * 
     */
    public void setMaxActiveCallsAllowed(int value) {
        this.maxActiveCallsAllowed = value;
    }

    /**
     * Ruft den Wert der maxIncomingActiveCallsAllowed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxIncomingActiveCallsAllowed() {
        return maxIncomingActiveCallsAllowed;
    }

    /**
     * Legt den Wert der maxIncomingActiveCallsAllowed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxIncomingActiveCallsAllowed(Integer value) {
        this.maxIncomingActiveCallsAllowed = value;
    }

    /**
     * Ruft den Wert der maxOutgoingActiveCallsAllowed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxOutgoingActiveCallsAllowed() {
        return maxOutgoingActiveCallsAllowed;
    }

    /**
     * Legt den Wert der maxOutgoingActiveCallsAllowed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxOutgoingActiveCallsAllowed(Integer value) {
        this.maxOutgoingActiveCallsAllowed = value;
    }

    /**
     * Ruft den Wert der defaultGroupForNewUsers-Eigenschaft ab.
     * 
     */
    public boolean isDefaultGroupForNewUsers() {
        return defaultGroupForNewUsers;
    }

    /**
     * Legt den Wert der defaultGroupForNewUsers-Eigenschaft fest.
     * 
     */
    public void setDefaultGroupForNewUsers(boolean value) {
        this.defaultGroupForNewUsers = value;
    }

    /**
     * Ruft den Wert der userTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserTable() {
        return userTable;
    }

    /**
     * Legt den Wert der userTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserTable(OCITable value) {
        this.userTable = value;
    }

}
