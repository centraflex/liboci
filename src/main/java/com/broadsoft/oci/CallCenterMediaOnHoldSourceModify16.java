//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Contains the call center media on hold source configuration.
 *       
 * 
 * <p>Java-Klasse für CallCenterMediaOnHoldSourceModify16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterMediaOnHoldSourceModify16">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="audioMessageSourceSelection" type="{}CallCenterMediaOnHoldMessageSelection" minOccurs="0"/>
 *         <element name="audioFile" type="{}ExtendedMediaFileResource" minOccurs="0"/>
 *         <element name="externalAudioSource" type="{}AccessDeviceEndpointModify" minOccurs="0"/>
 *         <element name="videoMessageSourceSelection" type="{}CallCenterMediaOnHoldMessageSelection" minOccurs="0"/>
 *         <element name="videoFile" type="{}ExtendedMediaFileResource" minOccurs="0"/>
 *         <element name="externalVideoSource" type="{}AccessDeviceEndpointModify" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterMediaOnHoldSourceModify16", propOrder = {
    "audioMessageSourceSelection",
    "audioFile",
    "externalAudioSource",
    "videoMessageSourceSelection",
    "videoFile",
    "externalVideoSource"
})
public class CallCenterMediaOnHoldSourceModify16 {

    @XmlSchemaType(name = "token")
    protected CallCenterMediaOnHoldMessageSelection audioMessageSourceSelection;
    protected ExtendedMediaFileResource audioFile;
    @XmlElementRef(name = "externalAudioSource", type = JAXBElement.class, required = false)
    protected JAXBElement<AccessDeviceEndpointModify> externalAudioSource;
    @XmlSchemaType(name = "token")
    protected CallCenterMediaOnHoldMessageSelection videoMessageSourceSelection;
    protected ExtendedMediaFileResource videoFile;
    @XmlElementRef(name = "externalVideoSource", type = JAXBElement.class, required = false)
    protected JAXBElement<AccessDeviceEndpointModify> externalVideoSource;

    /**
     * Ruft den Wert der audioMessageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public CallCenterMediaOnHoldMessageSelection getAudioMessageSourceSelection() {
        return audioMessageSourceSelection;
    }

    /**
     * Legt den Wert der audioMessageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public void setAudioMessageSourceSelection(CallCenterMediaOnHoldMessageSelection value) {
        this.audioMessageSourceSelection = value;
    }

    /**
     * Ruft den Wert der audioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public ExtendedMediaFileResource getAudioFile() {
        return audioFile;
    }

    /**
     * Legt den Wert der audioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public void setAudioFile(ExtendedMediaFileResource value) {
        this.audioFile = value;
    }

    /**
     * Ruft den Wert der externalAudioSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceEndpointModify }{@code >}
     *     
     */
    public JAXBElement<AccessDeviceEndpointModify> getExternalAudioSource() {
        return externalAudioSource;
    }

    /**
     * Legt den Wert der externalAudioSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceEndpointModify }{@code >}
     *     
     */
    public void setExternalAudioSource(JAXBElement<AccessDeviceEndpointModify> value) {
        this.externalAudioSource = value;
    }

    /**
     * Ruft den Wert der videoMessageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public CallCenterMediaOnHoldMessageSelection getVideoMessageSourceSelection() {
        return videoMessageSourceSelection;
    }

    /**
     * Legt den Wert der videoMessageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldMessageSelection }
     *     
     */
    public void setVideoMessageSourceSelection(CallCenterMediaOnHoldMessageSelection value) {
        this.videoMessageSourceSelection = value;
    }

    /**
     * Ruft den Wert der videoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public ExtendedMediaFileResource getVideoFile() {
        return videoFile;
    }

    /**
     * Legt den Wert der videoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public void setVideoFile(ExtendedMediaFileResource value) {
        this.videoFile = value;
    }

    /**
     * Ruft den Wert der externalVideoSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceEndpointModify }{@code >}
     *     
     */
    public JAXBElement<AccessDeviceEndpointModify> getExternalVideoSource() {
        return externalVideoSource;
    }

    /**
     * Legt den Wert der externalVideoSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccessDeviceEndpointModify }{@code >}
     *     
     */
    public void setExternalVideoSource(JAXBElement<AccessDeviceEndpointModify> value) {
        this.externalVideoSource = value;
    }

}
