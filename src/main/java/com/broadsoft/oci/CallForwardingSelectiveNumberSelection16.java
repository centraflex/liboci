//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallForwardingSelectiveNumberSelection16.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallForwardingSelectiveNumberSelection16">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Forward To Default Number"/>
 *     <enumeration value="Forward To Specified Number"/>
 *     <enumeration value="Do not forward"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallForwardingSelectiveNumberSelection16")
@XmlEnum
public enum CallForwardingSelectiveNumberSelection16 {

    @XmlEnumValue("Forward To Default Number")
    FORWARD_TO_DEFAULT_NUMBER("Forward To Default Number"),
    @XmlEnumValue("Forward To Specified Number")
    FORWARD_TO_SPECIFIED_NUMBER("Forward To Specified Number"),
    @XmlEnumValue("Do not forward")
    DO_NOT_FORWARD("Do not forward");
    private final String value;

    CallForwardingSelectiveNumberSelection16(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallForwardingSelectiveNumberSelection16 fromValue(String v) {
        for (CallForwardingSelectiveNumberSelection16 c: CallForwardingSelectiveNumberSelection16 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
