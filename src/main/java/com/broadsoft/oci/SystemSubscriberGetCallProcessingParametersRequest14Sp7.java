//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Get the system call processing configuration for all subscribers
 *             The response is either a SystemSubscriberGetCallProcessingParametersResponse14sp7 or an ErrorResponse.
 *             Replaced By : SystemSubscriberGetCallProcessingParametersRequest15sp2
 *          
 * 
 * <p>Java-Klasse für SystemSubscriberGetCallProcessingParametersRequest14sp7 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSubscriberGetCallProcessingParametersRequest14sp7">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSubscriberGetCallProcessingParametersRequest14sp7")
public class SystemSubscriberGetCallProcessingParametersRequest14Sp7
    extends OCIRequest
{


}
