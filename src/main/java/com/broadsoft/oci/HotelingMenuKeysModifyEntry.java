//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal hoteling menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für HotelingMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="HotelingMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="checkHostStatus" type="{}DigitAny" minOccurs="0"/>
 *         <element name="associateWithHost" type="{}DigitAny" minOccurs="0"/>
 *         <element name="disassociateFromHost" type="{}DigitAny" minOccurs="0"/>
 *         <element name="disassociateFromRemoteHost" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HotelingMenuKeysModifyEntry", propOrder = {
    "checkHostStatus",
    "associateWithHost",
    "disassociateFromHost",
    "disassociateFromRemoteHost",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class HotelingMenuKeysModifyEntry {

    @XmlElementRef(name = "checkHostStatus", type = JAXBElement.class, required = false)
    protected JAXBElement<String> checkHostStatus;
    @XmlElementRef(name = "associateWithHost", type = JAXBElement.class, required = false)
    protected JAXBElement<String> associateWithHost;
    @XmlElementRef(name = "disassociateFromHost", type = JAXBElement.class, required = false)
    protected JAXBElement<String> disassociateFromHost;
    @XmlElementRef(name = "disassociateFromRemoteHost", type = JAXBElement.class, required = false)
    protected JAXBElement<String> disassociateFromRemoteHost;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repeatMenu;

    /**
     * Ruft den Wert der checkHostStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCheckHostStatus() {
        return checkHostStatus;
    }

    /**
     * Legt den Wert der checkHostStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCheckHostStatus(JAXBElement<String> value) {
        this.checkHostStatus = value;
    }

    /**
     * Ruft den Wert der associateWithHost-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAssociateWithHost() {
        return associateWithHost;
    }

    /**
     * Legt den Wert der associateWithHost-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAssociateWithHost(JAXBElement<String> value) {
        this.associateWithHost = value;
    }

    /**
     * Ruft den Wert der disassociateFromHost-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDisassociateFromHost() {
        return disassociateFromHost;
    }

    /**
     * Legt den Wert der disassociateFromHost-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDisassociateFromHost(JAXBElement<String> value) {
        this.disassociateFromHost = value;
    }

    /**
     * Ruft den Wert der disassociateFromRemoteHost-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDisassociateFromRemoteHost() {
        return disassociateFromRemoteHost;
    }

    /**
     * Legt den Wert der disassociateFromRemoteHost-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDisassociateFromRemoteHost(JAXBElement<String> value) {
        this.disassociateFromRemoteHost = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepeatMenu(JAXBElement<String> value) {
        this.repeatMenu = value;
    }

}
