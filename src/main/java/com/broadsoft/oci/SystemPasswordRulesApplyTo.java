//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SystemPasswordRulesApplyTo.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SystemPasswordRulesApplyTo">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="System, Provisioning Administrator"/>
 *     <enumeration value="System, Provisioning, Service Provider Administrator"/>
 *     <enumeration value="Administrator and User"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SystemPasswordRulesApplyTo")
@XmlEnum
public enum SystemPasswordRulesApplyTo {

    @XmlEnumValue("System, Provisioning Administrator")
    SYSTEM_PROVISIONING_ADMINISTRATOR("System, Provisioning Administrator"),
    @XmlEnumValue("System, Provisioning, Service Provider Administrator")
    SYSTEM_PROVISIONING_SERVICE_PROVIDER_ADMINISTRATOR("System, Provisioning, Service Provider Administrator"),
    @XmlEnumValue("Administrator and User")
    ADMINISTRATOR_AND_USER("Administrator and User");
    private final String value;

    SystemPasswordRulesApplyTo(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SystemPasswordRulesApplyTo fromValue(String v) {
        for (SystemPasswordRulesApplyTo c: SystemPasswordRulesApplyTo.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
