//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailablePagedSortedListRequest.
 *         The response contains a table with columns: "Mobile Number", "Mobile Network", "Mobile Country Code", 
 *         "Mobile National Prefix".
 *         The "Mobile Number" column contains a single DN.
 *         The "Mobile Network" column contains the Mobile Network the number belongs to.
 *         The "Mobile Country Code" column indicates the dialing prefix for the mobile number.
 *         The "Mobile National Prefix" column indicates the digit sequence to be dialed before the mobile number.
 *         Replaced by: EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailablePagedSortedListResponse22
 *       
 * 
 * <p>Java-Klasse für EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailablePagedSortedListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailablePagedSortedListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="availableMobileSubscriberDirectoryNumberTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailablePagedSortedListResponse", propOrder = {
    "availableMobileSubscriberDirectoryNumberTable"
})
public class EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAvailablePagedSortedListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable availableMobileSubscriberDirectoryNumberTable;

    /**
     * Ruft den Wert der availableMobileSubscriberDirectoryNumberTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAvailableMobileSubscriberDirectoryNumberTable() {
        return availableMobileSubscriberDirectoryNumberTable;
    }

    /**
     * Legt den Wert der availableMobileSubscriberDirectoryNumberTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAvailableMobileSubscriberDirectoryNumberTable(OCITable value) {
        this.availableMobileSubscriberDirectoryNumberTable = value;
    }

}
