//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für StirShakenVerificationErrorHandling.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="StirShakenVerificationErrorHandling">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Abort"/>
 *     <enumeration value="Proceed"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "StirShakenVerificationErrorHandling")
@XmlEnum
public enum StirShakenVerificationErrorHandling {

    @XmlEnumValue("Abort")
    ABORT("Abort"),
    @XmlEnumValue("Proceed")
    PROCEED("Proceed");
    private final String value;

    StirShakenVerificationErrorHandling(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StirShakenVerificationErrorHandling fromValue(String v) {
        for (StirShakenVerificationErrorHandling c: StirShakenVerificationErrorHandling.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
