//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Delete a Network SIP Status Code mapping.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemTreatmentMappingNetworkSIPStatusDeleteRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemTreatmentMappingNetworkSIPStatusDeleteRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="sipStatusCode" type="{}SIPFailureStatusCode"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemTreatmentMappingNetworkSIPStatusDeleteRequest", propOrder = {
    "sipStatusCode"
})
public class SystemTreatmentMappingNetworkSIPStatusDeleteRequest
    extends OCIRequest
{

    protected int sipStatusCode;

    /**
     * Ruft den Wert der sipStatusCode-Eigenschaft ab.
     * 
     */
    public int getSipStatusCode() {
        return sipStatusCode;
    }

    /**
     * Legt den Wert der sipStatusCode-Eigenschaft fest.
     * 
     */
    public void setSipStatusCode(int value) {
        this.sipStatusCode = value;
    }

}
