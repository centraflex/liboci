//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The response to a SystemBroadWorksMobilityMobileNetworkGetListRequest.
 *         Contains a table with column headings: "Name", "SCF Signaling Net Address", "SCF Signaling Port" 
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobilityMobileNetworkGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobilityMobileNetworkGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="mobileNetworkTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobilityMobileNetworkGetListResponse", propOrder = {
    "mobileNetworkTable"
})
public class SystemBroadWorksMobilityMobileNetworkGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable mobileNetworkTable;

    /**
     * Ruft den Wert der mobileNetworkTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getMobileNetworkTable() {
        return mobileNetworkTable;
    }

    /**
     * Legt den Wert der mobileNetworkTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setMobileNetworkTable(OCITable value) {
        this.mobileNetworkTable = value;
    }

}
