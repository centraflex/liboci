//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Filter criteria based on the transferred/forwarded number.
 *       
 * 
 * <p>Java-Klasse für EnhancedCallLogsRedirectedNumberFilter20sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnhancedCallLogsRedirectedNumberFilter20sp1">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <choice>
 *           <element name="redirectedCall" type="{}EnhancedCallLogsRedirectedCallSelection20sp1"/>
 *           <element name="includeServiceInvocationBasicCallType" type="{}BasicCallType" maxOccurs="unbounded"/>
 *           <element name="includeServiceInvocationCallCategory" type="{}CallCategory" maxOccurs="unbounded"/>
 *           <element name="includeServiceInvocationConfigurableCallType" type="{}CommunicationBarringCallType" maxOccurs="unbounded"/>
 *           <element name="searchCriteriaServiceInvocationDialedNumber" type="{}SearchCriteriaOutgoingDNorSIPURI" maxOccurs="unbounded"/>
 *           <element name="searchCriteriaServiceInvocationCalledNumber" type="{}SearchCriteriaOutgoingDNorSIPURI" maxOccurs="unbounded"/>
 *           <element name="searchCriteriaServiceInvocationNetworkTranslatedNumber" type="{}SearchCriteriaOutgoingDNorSIPURI" maxOccurs="unbounded"/>
 *         </choice>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnhancedCallLogsRedirectedNumberFilter20sp1", propOrder = {
    "redirectedCall",
    "includeServiceInvocationBasicCallType",
    "includeServiceInvocationCallCategory",
    "includeServiceInvocationConfigurableCallType",
    "searchCriteriaServiceInvocationDialedNumber",
    "searchCriteriaServiceInvocationCalledNumber",
    "searchCriteriaServiceInvocationNetworkTranslatedNumber"
})
public class EnhancedCallLogsRedirectedNumberFilter20Sp1 {

    protected EnhancedCallLogsRedirectedCallSelection20Sp1 redirectedCall;
    @XmlSchemaType(name = "token")
    protected List<BasicCallType> includeServiceInvocationBasicCallType;
    @XmlSchemaType(name = "token")
    protected List<CallCategory> includeServiceInvocationCallCategory;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> includeServiceInvocationConfigurableCallType;
    protected List<SearchCriteriaOutgoingDNorSIPURI> searchCriteriaServiceInvocationDialedNumber;
    protected List<SearchCriteriaOutgoingDNorSIPURI> searchCriteriaServiceInvocationCalledNumber;
    protected List<SearchCriteriaOutgoingDNorSIPURI> searchCriteriaServiceInvocationNetworkTranslatedNumber;

    /**
     * Ruft den Wert der redirectedCall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnhancedCallLogsRedirectedCallSelection20Sp1 }
     *     
     */
    public EnhancedCallLogsRedirectedCallSelection20Sp1 getRedirectedCall() {
        return redirectedCall;
    }

    /**
     * Legt den Wert der redirectedCall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnhancedCallLogsRedirectedCallSelection20Sp1 }
     *     
     */
    public void setRedirectedCall(EnhancedCallLogsRedirectedCallSelection20Sp1 value) {
        this.redirectedCall = value;
    }

    /**
     * Gets the value of the includeServiceInvocationBasicCallType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the includeServiceInvocationBasicCallType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludeServiceInvocationBasicCallType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BasicCallType }
     * 
     * 
     * @return
     *     The value of the includeServiceInvocationBasicCallType property.
     */
    public List<BasicCallType> getIncludeServiceInvocationBasicCallType() {
        if (includeServiceInvocationBasicCallType == null) {
            includeServiceInvocationBasicCallType = new ArrayList<>();
        }
        return this.includeServiceInvocationBasicCallType;
    }

    /**
     * Gets the value of the includeServiceInvocationCallCategory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the includeServiceInvocationCallCategory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludeServiceInvocationCallCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallCategory }
     * 
     * 
     * @return
     *     The value of the includeServiceInvocationCallCategory property.
     */
    public List<CallCategory> getIncludeServiceInvocationCallCategory() {
        if (includeServiceInvocationCallCategory == null) {
            includeServiceInvocationCallCategory = new ArrayList<>();
        }
        return this.includeServiceInvocationCallCategory;
    }

    /**
     * Gets the value of the includeServiceInvocationConfigurableCallType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the includeServiceInvocationConfigurableCallType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncludeServiceInvocationConfigurableCallType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the includeServiceInvocationConfigurableCallType property.
     */
    public List<String> getIncludeServiceInvocationConfigurableCallType() {
        if (includeServiceInvocationConfigurableCallType == null) {
            includeServiceInvocationConfigurableCallType = new ArrayList<>();
        }
        return this.includeServiceInvocationConfigurableCallType;
    }

    /**
     * Gets the value of the searchCriteriaServiceInvocationDialedNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaServiceInvocationDialedNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaServiceInvocationDialedNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaOutgoingDNorSIPURI }
     * 
     * 
     * @return
     *     The value of the searchCriteriaServiceInvocationDialedNumber property.
     */
    public List<SearchCriteriaOutgoingDNorSIPURI> getSearchCriteriaServiceInvocationDialedNumber() {
        if (searchCriteriaServiceInvocationDialedNumber == null) {
            searchCriteriaServiceInvocationDialedNumber = new ArrayList<>();
        }
        return this.searchCriteriaServiceInvocationDialedNumber;
    }

    /**
     * Gets the value of the searchCriteriaServiceInvocationCalledNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaServiceInvocationCalledNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaServiceInvocationCalledNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaOutgoingDNorSIPURI }
     * 
     * 
     * @return
     *     The value of the searchCriteriaServiceInvocationCalledNumber property.
     */
    public List<SearchCriteriaOutgoingDNorSIPURI> getSearchCriteriaServiceInvocationCalledNumber() {
        if (searchCriteriaServiceInvocationCalledNumber == null) {
            searchCriteriaServiceInvocationCalledNumber = new ArrayList<>();
        }
        return this.searchCriteriaServiceInvocationCalledNumber;
    }

    /**
     * Gets the value of the searchCriteriaServiceInvocationNetworkTranslatedNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaServiceInvocationNetworkTranslatedNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaServiceInvocationNetworkTranslatedNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaOutgoingDNorSIPURI }
     * 
     * 
     * @return
     *     The value of the searchCriteriaServiceInvocationNetworkTranslatedNumber property.
     */
    public List<SearchCriteriaOutgoingDNorSIPURI> getSearchCriteriaServiceInvocationNetworkTranslatedNumber() {
        if (searchCriteriaServiceInvocationNetworkTranslatedNumber == null) {
            searchCriteriaServiceInvocationNetworkTranslatedNumber = new ArrayList<>();
        }
        return this.searchCriteriaServiceInvocationNetworkTranslatedNumber;
    }

}
