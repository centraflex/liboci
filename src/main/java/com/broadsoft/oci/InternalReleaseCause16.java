//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für InternalReleaseCause16.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="InternalReleaseCause16">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Busy"/>
 *     <enumeration value="Forbidden"/>
 *     <enumeration value="Routing Failure"/>
 *     <enumeration value="Global Failure"/>
 *     <enumeration value="Request Failure"/>
 *     <enumeration value="Server Failure"/>
 *     <enumeration value="Translation Failure"/>
 *     <enumeration value="Temporarily Unavailable"/>
 *     <enumeration value="User Not Found"/>
 *     <enumeration value="Request Timeout"/>
 *     <enumeration value="Dial Tone Timeout"/>
 *     <enumeration value="Insufficient Credits"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "InternalReleaseCause16")
@XmlEnum
public enum InternalReleaseCause16 {

    @XmlEnumValue("Busy")
    BUSY("Busy"),
    @XmlEnumValue("Forbidden")
    FORBIDDEN("Forbidden"),
    @XmlEnumValue("Routing Failure")
    ROUTING_FAILURE("Routing Failure"),
    @XmlEnumValue("Global Failure")
    GLOBAL_FAILURE("Global Failure"),
    @XmlEnumValue("Request Failure")
    REQUEST_FAILURE("Request Failure"),
    @XmlEnumValue("Server Failure")
    SERVER_FAILURE("Server Failure"),
    @XmlEnumValue("Translation Failure")
    TRANSLATION_FAILURE("Translation Failure"),
    @XmlEnumValue("Temporarily Unavailable")
    TEMPORARILY_UNAVAILABLE("Temporarily Unavailable"),
    @XmlEnumValue("User Not Found")
    USER_NOT_FOUND("User Not Found"),
    @XmlEnumValue("Request Timeout")
    REQUEST_TIMEOUT("Request Timeout"),
    @XmlEnumValue("Dial Tone Timeout")
    DIAL_TONE_TIMEOUT("Dial Tone Timeout"),
    @XmlEnumValue("Insufficient Credits")
    INSUFFICIENT_CREDITS("Insufficient Credits");
    private final String value;

    InternalReleaseCause16(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InternalReleaseCause16 fromValue(String v) {
        for (InternalReleaseCause16 c: InternalReleaseCause16 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
