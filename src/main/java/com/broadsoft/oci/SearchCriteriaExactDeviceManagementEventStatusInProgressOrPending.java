//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for a particular fully specified Device Management event in progress or pending status.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactDeviceManagementEventStatusInProgressOrPending complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactDeviceManagementEventStatusInProgressOrPending">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="dmEventStatusInProgressOrPending" type="{}DeviceManagementEventStatusInProgressOrPending"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactDeviceManagementEventStatusInProgressOrPending", propOrder = {
    "dmEventStatusInProgressOrPending"
})
public class SearchCriteriaExactDeviceManagementEventStatusInProgressOrPending
    extends SearchCriteria
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceManagementEventStatusInProgressOrPending dmEventStatusInProgressOrPending;

    /**
     * Ruft den Wert der dmEventStatusInProgressOrPending-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementEventStatusInProgressOrPending }
     *     
     */
    public DeviceManagementEventStatusInProgressOrPending getDmEventStatusInProgressOrPending() {
        return dmEventStatusInProgressOrPending;
    }

    /**
     * Legt den Wert der dmEventStatusInProgressOrPending-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementEventStatusInProgressOrPending }
     *     
     */
    public void setDmEventStatusInProgressOrPending(DeviceManagementEventStatusInProgressOrPending value) {
        this.dmEventStatusInProgressOrPending = value;
    }

}
