//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderEnhancedCallLogsGetRequest.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderEnhancedCallLogsGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderEnhancedCallLogsGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maxLoggedCalls" type="{}EnhancedCallLogsMaxLoggedCalls"/>
 *         <element name="callExpirationDays" type="{}EnhancedCallLogsCallExpirationDays"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderEnhancedCallLogsGetResponse", propOrder = {
    "maxLoggedCalls",
    "callExpirationDays"
})
public class ServiceProviderEnhancedCallLogsGetResponse
    extends OCIDataResponse
{

    protected int maxLoggedCalls;
    protected int callExpirationDays;

    /**
     * Ruft den Wert der maxLoggedCalls-Eigenschaft ab.
     * 
     */
    public int getMaxLoggedCalls() {
        return maxLoggedCalls;
    }

    /**
     * Legt den Wert der maxLoggedCalls-Eigenschaft fest.
     * 
     */
    public void setMaxLoggedCalls(int value) {
        this.maxLoggedCalls = value;
    }

    /**
     * Ruft den Wert der callExpirationDays-Eigenschaft ab.
     * 
     */
    public int getCallExpirationDays() {
        return callExpirationDays;
    }

    /**
     * Legt den Wert der callExpirationDays-Eigenschaft fest.
     * 
     */
    public void setCallExpirationDays(int value) {
        this.callExpirationDays = value;
    }

}
