//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für BroadWorksMobilityUserSettingLevel.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="BroadWorksMobilityUserSettingLevel">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Group"/>
 *     <enumeration value="User"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "BroadWorksMobilityUserSettingLevel")
@XmlEnum
public enum BroadWorksMobilityUserSettingLevel {

    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("User")
    USER("User");
    private final String value;

    BroadWorksMobilityUserSettingLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BroadWorksMobilityUserSettingLevel fromValue(String v) {
        for (BroadWorksMobilityUserSettingLevel c: BroadWorksMobilityUserSettingLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
