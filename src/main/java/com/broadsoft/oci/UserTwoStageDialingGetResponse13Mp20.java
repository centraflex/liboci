//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *           Response to UserTwoStageDialingGetRequest13Mp20.
 *         
 * 
 * <p>Java-Klasse für UserTwoStageDialingGetResponse13Mp20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserTwoStageDialingGetResponse13Mp20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowActivationWithUserAddresses" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserTwoStageDialingGetResponse13Mp20", propOrder = {
    "isActive",
    "allowActivationWithUserAddresses"
})
public class UserTwoStageDialingGetResponse13Mp20
    extends OCIDataResponse
{

    protected boolean isActive;
    protected boolean allowActivationWithUserAddresses;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der allowActivationWithUserAddresses-Eigenschaft ab.
     * 
     */
    public boolean isAllowActivationWithUserAddresses() {
        return allowActivationWithUserAddresses;
    }

    /**
     * Legt den Wert der allowActivationWithUserAddresses-Eigenschaft fest.
     * 
     */
    public void setAllowActivationWithUserAddresses(boolean value) {
        this.allowActivationWithUserAddresses = value;
    }

}
