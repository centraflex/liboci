//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemMusicOnHoldGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemMusicOnHoldGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemMusicOnHoldGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="delayMilliseconds" type="{}MusicOnHoldDelayMilliseconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemMusicOnHoldGetResponse", propOrder = {
    "delayMilliseconds"
})
public class SystemMusicOnHoldGetResponse
    extends OCIDataResponse
{

    protected int delayMilliseconds;

    /**
     * Ruft den Wert der delayMilliseconds-Eigenschaft ab.
     * 
     */
    public int getDelayMilliseconds() {
        return delayMilliseconds;
    }

    /**
     * Legt den Wert der delayMilliseconds-Eigenschaft fest.
     * 
     */
    public void setDelayMilliseconds(int value) {
        this.delayMilliseconds = value;
    }

}
