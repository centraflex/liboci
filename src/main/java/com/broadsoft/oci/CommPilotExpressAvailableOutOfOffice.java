//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         CommPilot Express Available Out Of Office Configuration used in the context of a get.
 *       
 * 
 * <p>Java-Klasse für CommPilotExpressAvailableOutOfOffice complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CommPilotExpressAvailableOutOfOffice">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="incomingCalls" type="{}CommPilotExpressRedirection"/>
 *         <element name="incomingCallNotify" type="{}CommPilotExpressEmailNotify"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommPilotExpressAvailableOutOfOffice", propOrder = {
    "incomingCalls",
    "incomingCallNotify"
})
public class CommPilotExpressAvailableOutOfOffice {

    @XmlElement(required = true)
    protected CommPilotExpressRedirection incomingCalls;
    @XmlElement(required = true)
    protected CommPilotExpressEmailNotify incomingCallNotify;

    /**
     * Ruft den Wert der incomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressRedirection }
     *     
     */
    public CommPilotExpressRedirection getIncomingCalls() {
        return incomingCalls;
    }

    /**
     * Legt den Wert der incomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressRedirection }
     *     
     */
    public void setIncomingCalls(CommPilotExpressRedirection value) {
        this.incomingCalls = value;
    }

    /**
     * Ruft den Wert der incomingCallNotify-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressEmailNotify }
     *     
     */
    public CommPilotExpressEmailNotify getIncomingCallNotify() {
        return incomingCallNotify;
    }

    /**
     * Legt den Wert der incomingCallNotify-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressEmailNotify }
     *     
     */
    public void setIncomingCallNotify(CommPilotExpressEmailNotify value) {
        this.incomingCallNotify = value;
    }

}
