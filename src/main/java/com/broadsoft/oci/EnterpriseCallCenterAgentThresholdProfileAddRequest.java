//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a new Call Center Agent Threshold Profile in an Enterprise.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für EnterpriseCallCenterAgentThresholdProfileAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseCallCenterAgentThresholdProfileAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="profileName" type="{}CallCenterAgentThresholdProfileName"/>
 *         <element name="profileDescription" type="{}CallCenterAgentThresholdProfileDescription" minOccurs="0"/>
 *         <element name="thresholdCurrentCallStateIdleTimeYellow" type="{}CallCenterAgentThresholdCurrentCallStateIdleTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdCurrentCallStateIdleTimeRed" type="{}CallCenterAgentThresholdCurrentCallStateIdleTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdCurrentCallStateOnCallTimeYellow" type="{}CallCenterAgentThresholdCurrentCallStateOnCallTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdCurrentCallStateOnCallTimeRed" type="{}CallCenterAgentThresholdCurrentCallStateOnCallTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdCurrentAgentStateUnavailableTimeYellow" type="{}CallCenterAgentThresholdCurrentAgentStateUnavailableTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdCurrentAgentStateUnavailableTimeRed" type="{}CallCenterAgentThresholdCurrentAgentStateUnavailableTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageBusyInTimeYellow" type="{}CallCenterAgentThresholdAverageBusyInTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageBusyInTimeRed" type="{}CallCenterAgentThresholdAverageBusyInTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageBusyOutTimeYellow" type="{}CallCenterAgentThresholdAverageBusyOutTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageBusyOutTimeRed" type="{}CallCenterAgentThresholdAverageBusyOutTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageWrapUpTimeYellow" type="{}CallCenterAgentThresholdAverageWrapUpTimeSeconds" minOccurs="0"/>
 *         <element name="thresholdAverageWrapUpTimeRed" type="{}CallCenterAgentThresholdAverageWrapUpTimeSeconds" minOccurs="0"/>
 *         <element name="enableNotificationEmail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="notificationEmailAddress" type="{}EmailAddress" maxOccurs="8" minOccurs="0"/>
 *         <element name="agentUserId" type="{}UserId" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseCallCenterAgentThresholdProfileAddRequest", propOrder = {
    "serviceProviderId",
    "profileName",
    "profileDescription",
    "thresholdCurrentCallStateIdleTimeYellow",
    "thresholdCurrentCallStateIdleTimeRed",
    "thresholdCurrentCallStateOnCallTimeYellow",
    "thresholdCurrentCallStateOnCallTimeRed",
    "thresholdCurrentAgentStateUnavailableTimeYellow",
    "thresholdCurrentAgentStateUnavailableTimeRed",
    "thresholdAverageBusyInTimeYellow",
    "thresholdAverageBusyInTimeRed",
    "thresholdAverageBusyOutTimeYellow",
    "thresholdAverageBusyOutTimeRed",
    "thresholdAverageWrapUpTimeYellow",
    "thresholdAverageWrapUpTimeRed",
    "enableNotificationEmail",
    "notificationEmailAddress",
    "agentUserId"
})
public class EnterpriseCallCenterAgentThresholdProfileAddRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String profileName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String profileDescription;
    protected Integer thresholdCurrentCallStateIdleTimeYellow;
    protected Integer thresholdCurrentCallStateIdleTimeRed;
    protected Integer thresholdCurrentCallStateOnCallTimeYellow;
    protected Integer thresholdCurrentCallStateOnCallTimeRed;
    protected Integer thresholdCurrentAgentStateUnavailableTimeYellow;
    protected Integer thresholdCurrentAgentStateUnavailableTimeRed;
    protected Integer thresholdAverageBusyInTimeYellow;
    protected Integer thresholdAverageBusyInTimeRed;
    protected Integer thresholdAverageBusyOutTimeYellow;
    protected Integer thresholdAverageBusyOutTimeRed;
    protected Integer thresholdAverageWrapUpTimeYellow;
    protected Integer thresholdAverageWrapUpTimeRed;
    protected boolean enableNotificationEmail;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> notificationEmailAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> agentUserId;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der profileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileName() {
        return profileName;
    }

    /**
     * Legt den Wert der profileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileName(String value) {
        this.profileName = value;
    }

    /**
     * Ruft den Wert der profileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileDescription() {
        return profileDescription;
    }

    /**
     * Legt den Wert der profileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileDescription(String value) {
        this.profileDescription = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentCallStateIdleTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdCurrentCallStateIdleTimeYellow() {
        return thresholdCurrentCallStateIdleTimeYellow;
    }

    /**
     * Legt den Wert der thresholdCurrentCallStateIdleTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdCurrentCallStateIdleTimeYellow(Integer value) {
        this.thresholdCurrentCallStateIdleTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentCallStateIdleTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdCurrentCallStateIdleTimeRed() {
        return thresholdCurrentCallStateIdleTimeRed;
    }

    /**
     * Legt den Wert der thresholdCurrentCallStateIdleTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdCurrentCallStateIdleTimeRed(Integer value) {
        this.thresholdCurrentCallStateIdleTimeRed = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentCallStateOnCallTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdCurrentCallStateOnCallTimeYellow() {
        return thresholdCurrentCallStateOnCallTimeYellow;
    }

    /**
     * Legt den Wert der thresholdCurrentCallStateOnCallTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdCurrentCallStateOnCallTimeYellow(Integer value) {
        this.thresholdCurrentCallStateOnCallTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentCallStateOnCallTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdCurrentCallStateOnCallTimeRed() {
        return thresholdCurrentCallStateOnCallTimeRed;
    }

    /**
     * Legt den Wert der thresholdCurrentCallStateOnCallTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdCurrentCallStateOnCallTimeRed(Integer value) {
        this.thresholdCurrentCallStateOnCallTimeRed = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentAgentStateUnavailableTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdCurrentAgentStateUnavailableTimeYellow() {
        return thresholdCurrentAgentStateUnavailableTimeYellow;
    }

    /**
     * Legt den Wert der thresholdCurrentAgentStateUnavailableTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdCurrentAgentStateUnavailableTimeYellow(Integer value) {
        this.thresholdCurrentAgentStateUnavailableTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdCurrentAgentStateUnavailableTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdCurrentAgentStateUnavailableTimeRed() {
        return thresholdCurrentAgentStateUnavailableTimeRed;
    }

    /**
     * Legt den Wert der thresholdCurrentAgentStateUnavailableTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdCurrentAgentStateUnavailableTimeRed(Integer value) {
        this.thresholdCurrentAgentStateUnavailableTimeRed = value;
    }

    /**
     * Ruft den Wert der thresholdAverageBusyInTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdAverageBusyInTimeYellow() {
        return thresholdAverageBusyInTimeYellow;
    }

    /**
     * Legt den Wert der thresholdAverageBusyInTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdAverageBusyInTimeYellow(Integer value) {
        this.thresholdAverageBusyInTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdAverageBusyInTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdAverageBusyInTimeRed() {
        return thresholdAverageBusyInTimeRed;
    }

    /**
     * Legt den Wert der thresholdAverageBusyInTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdAverageBusyInTimeRed(Integer value) {
        this.thresholdAverageBusyInTimeRed = value;
    }

    /**
     * Ruft den Wert der thresholdAverageBusyOutTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdAverageBusyOutTimeYellow() {
        return thresholdAverageBusyOutTimeYellow;
    }

    /**
     * Legt den Wert der thresholdAverageBusyOutTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdAverageBusyOutTimeYellow(Integer value) {
        this.thresholdAverageBusyOutTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdAverageBusyOutTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdAverageBusyOutTimeRed() {
        return thresholdAverageBusyOutTimeRed;
    }

    /**
     * Legt den Wert der thresholdAverageBusyOutTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdAverageBusyOutTimeRed(Integer value) {
        this.thresholdAverageBusyOutTimeRed = value;
    }

    /**
     * Ruft den Wert der thresholdAverageWrapUpTimeYellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdAverageWrapUpTimeYellow() {
        return thresholdAverageWrapUpTimeYellow;
    }

    /**
     * Legt den Wert der thresholdAverageWrapUpTimeYellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdAverageWrapUpTimeYellow(Integer value) {
        this.thresholdAverageWrapUpTimeYellow = value;
    }

    /**
     * Ruft den Wert der thresholdAverageWrapUpTimeRed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getThresholdAverageWrapUpTimeRed() {
        return thresholdAverageWrapUpTimeRed;
    }

    /**
     * Legt den Wert der thresholdAverageWrapUpTimeRed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThresholdAverageWrapUpTimeRed(Integer value) {
        this.thresholdAverageWrapUpTimeRed = value;
    }

    /**
     * Ruft den Wert der enableNotificationEmail-Eigenschaft ab.
     * 
     */
    public boolean isEnableNotificationEmail() {
        return enableNotificationEmail;
    }

    /**
     * Legt den Wert der enableNotificationEmail-Eigenschaft fest.
     * 
     */
    public void setEnableNotificationEmail(boolean value) {
        this.enableNotificationEmail = value;
    }

    /**
     * Gets the value of the notificationEmailAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the notificationEmailAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNotificationEmailAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the notificationEmailAddress property.
     */
    public List<String> getNotificationEmailAddress() {
        if (notificationEmailAddress == null) {
            notificationEmailAddress = new ArrayList<>();
        }
        return this.notificationEmailAddress;
    }

    /**
     * Gets the value of the agentUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the agentUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgentUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the agentUserId property.
     */
    public List<String> getAgentUserId() {
        if (agentUserId == null) {
            agentUserId = new ArrayList<>();
        }
        return this.agentUserId;
    }

}
