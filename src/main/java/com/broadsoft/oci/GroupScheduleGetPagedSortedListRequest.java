//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Get the list of schedules viewable by a group.
 *         The response is either a GroupScheduleGetPagedSortedListResponse or an ErrorResponse.
 *         If no sortOrder is included, the response is sorted by Name ascending by default.
 *         If the responsePagingControl element is not provided, the paging startIndex will be 
 *         set to 1 by default, and the responsePageSize will be set to the maximum 
 *         responsePageSize by default.
 *         Multiple search criteria are logically ANDed together unless the searchCriteriaModeOr option 
 *         is included. Then the search criteria are logically ORed together.
 *       
 * 
 * <p>Java-Klasse für GroupScheduleGetPagedSortedListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupScheduleGetPagedSortedListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl" minOccurs="0"/>
 *         <element name="sortByScheduleName" type="{}SortByScheduleName" minOccurs="0"/>
 *         <element name="searchCriteriaScheduleName" type="{}SearchCriteriaScheduleName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactScheduleType" type="{}SearchCriteriaExactScheduleType" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactScheduleLevel" type="{}SearchCriteriaExactScheduleLevel" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaModeOr" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupScheduleGetPagedSortedListRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "responsePagingControl",
    "sortByScheduleName",
    "searchCriteriaScheduleName",
    "searchCriteriaExactScheduleType",
    "searchCriteriaExactScheduleLevel",
    "searchCriteriaModeOr"
})
public class GroupScheduleGetPagedSortedListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected ResponsePagingControl responsePagingControl;
    protected SortByScheduleName sortByScheduleName;
    protected List<SearchCriteriaScheduleName> searchCriteriaScheduleName;
    protected List<SearchCriteriaExactScheduleType> searchCriteriaExactScheduleType;
    protected List<SearchCriteriaExactScheduleLevel> searchCriteriaExactScheduleLevel;
    protected Boolean searchCriteriaModeOr;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Ruft den Wert der sortByScheduleName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByScheduleName }
     *     
     */
    public SortByScheduleName getSortByScheduleName() {
        return sortByScheduleName;
    }

    /**
     * Legt den Wert der sortByScheduleName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByScheduleName }
     *     
     */
    public void setSortByScheduleName(SortByScheduleName value) {
        this.sortByScheduleName = value;
    }

    /**
     * Gets the value of the searchCriteriaScheduleName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaScheduleName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaScheduleName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaScheduleName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaScheduleName property.
     */
    public List<SearchCriteriaScheduleName> getSearchCriteriaScheduleName() {
        if (searchCriteriaScheduleName == null) {
            searchCriteriaScheduleName = new ArrayList<>();
        }
        return this.searchCriteriaScheduleName;
    }

    /**
     * Gets the value of the searchCriteriaExactScheduleType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaExactScheduleType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaExactScheduleType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaExactScheduleType }
     * 
     * 
     * @return
     *     The value of the searchCriteriaExactScheduleType property.
     */
    public List<SearchCriteriaExactScheduleType> getSearchCriteriaExactScheduleType() {
        if (searchCriteriaExactScheduleType == null) {
            searchCriteriaExactScheduleType = new ArrayList<>();
        }
        return this.searchCriteriaExactScheduleType;
    }

    /**
     * Gets the value of the searchCriteriaExactScheduleLevel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaExactScheduleLevel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaExactScheduleLevel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaExactScheduleLevel }
     * 
     * 
     * @return
     *     The value of the searchCriteriaExactScheduleLevel property.
     */
    public List<SearchCriteriaExactScheduleLevel> getSearchCriteriaExactScheduleLevel() {
        if (searchCriteriaExactScheduleLevel == null) {
            searchCriteriaExactScheduleLevel = new ArrayList<>();
        }
        return this.searchCriteriaExactScheduleLevel;
    }

    /**
     * Ruft den Wert der searchCriteriaModeOr-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSearchCriteriaModeOr() {
        return searchCriteriaModeOr;
    }

    /**
     * Legt den Wert der searchCriteriaModeOr-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSearchCriteriaModeOr(Boolean value) {
        this.searchCriteriaModeOr = value;
    }

}
