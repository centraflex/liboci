//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to change the service provider's or enterprise's voice portal settings.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderVoiceMessagingGroupModifyVoicePortalRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderVoiceMessagingGroupModifyVoicePortalRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="voicePortalScope" type="{}ServiceProviderVoicePortalScope" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderVoiceMessagingGroupModifyVoicePortalRequest", propOrder = {
    "serviceProviderId",
    "voicePortalScope"
})
public class ServiceProviderVoiceMessagingGroupModifyVoicePortalRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlSchemaType(name = "token")
    protected ServiceProviderVoicePortalScope voicePortalScope;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der voicePortalScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderVoicePortalScope }
     *     
     */
    public ServiceProviderVoicePortalScope getVoicePortalScope() {
        return voicePortalScope;
    }

    /**
     * Legt den Wert der voicePortalScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderVoicePortalScope }
     *     
     */
    public void setVoicePortalScope(ServiceProviderVoicePortalScope value) {
        this.voicePortalScope = value;
    }

}
