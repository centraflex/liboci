//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify Network Device Polling system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemNetworkDeviceMonitorParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNetworkDeviceMonitorParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="pollingIntervalMinutes" type="{}NetworkDeviceMonitorPollingIntervalMinutes" minOccurs="0"/>
 *         <element name="failedPollingIntervalMinutes" type="{}NetworkDeviceMonitorFailedPollingIntervalMinutes" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNetworkDeviceMonitorParametersModifyRequest", propOrder = {
    "pollingIntervalMinutes",
    "failedPollingIntervalMinutes"
})
public class SystemNetworkDeviceMonitorParametersModifyRequest
    extends OCIRequest
{

    protected Integer pollingIntervalMinutes;
    protected Integer failedPollingIntervalMinutes;

    /**
     * Ruft den Wert der pollingIntervalMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPollingIntervalMinutes() {
        return pollingIntervalMinutes;
    }

    /**
     * Legt den Wert der pollingIntervalMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPollingIntervalMinutes(Integer value) {
        this.pollingIntervalMinutes = value;
    }

    /**
     * Ruft den Wert der failedPollingIntervalMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFailedPollingIntervalMinutes() {
        return failedPollingIntervalMinutes;
    }

    /**
     * Legt den Wert der failedPollingIntervalMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFailedPollingIntervalMinutes(Integer value) {
        this.failedPollingIntervalMinutes = value;
    }

}
