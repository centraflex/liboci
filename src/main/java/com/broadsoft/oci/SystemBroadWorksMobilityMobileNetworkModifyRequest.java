//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a Mobile Network.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobilityMobileNetworkModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobilityMobileNetworkModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="name" type="{}BroadWorksMobilityMobileNetworkName"/>
 *         <element name="newName" type="{}BroadWorksMobilityMobileNetworkName" minOccurs="0"/>
 *         <element name="scfSignalingNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="scfSignalingPort" type="{}Port" minOccurs="0"/>
 *         <element name="refreshPeriodSeconds" type="{}SCFRefreshPeriodSeconds" minOccurs="0"/>
 *         <element name="maxConsecutiveFailures" type="{}SCFMaxConsecutiveFailures" minOccurs="0"/>
 *         <element name="maxResponseWaitTimeMilliseconds" type="{}SCFMaxResponseWaitTimeMilliseconds" minOccurs="0"/>
 *         <element name="enableAnnouncementSuppression" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="serviceAccessCodeListName" type="{}ServiceAccessCodeListName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobilityMobileNetworkModifyRequest", propOrder = {
    "name",
    "newName",
    "scfSignalingNetAddress",
    "scfSignalingPort",
    "refreshPeriodSeconds",
    "maxConsecutiveFailures",
    "maxResponseWaitTimeMilliseconds",
    "enableAnnouncementSuppression",
    "serviceAccessCodeListName"
})
public class SystemBroadWorksMobilityMobileNetworkModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newName;
    @XmlElementRef(name = "scfSignalingNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> scfSignalingNetAddress;
    @XmlElementRef(name = "scfSignalingPort", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> scfSignalingPort;
    protected Integer refreshPeriodSeconds;
    protected Integer maxConsecutiveFailures;
    protected Integer maxResponseWaitTimeMilliseconds;
    protected Boolean enableAnnouncementSuppression;
    @XmlElementRef(name = "serviceAccessCodeListName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serviceAccessCodeListName;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der newName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewName() {
        return newName;
    }

    /**
     * Legt den Wert der newName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewName(String value) {
        this.newName = value;
    }

    /**
     * Ruft den Wert der scfSignalingNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getScfSignalingNetAddress() {
        return scfSignalingNetAddress;
    }

    /**
     * Legt den Wert der scfSignalingNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setScfSignalingNetAddress(JAXBElement<String> value) {
        this.scfSignalingNetAddress = value;
    }

    /**
     * Ruft den Wert der scfSignalingPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getScfSignalingPort() {
        return scfSignalingPort;
    }

    /**
     * Legt den Wert der scfSignalingPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setScfSignalingPort(JAXBElement<Integer> value) {
        this.scfSignalingPort = value;
    }

    /**
     * Ruft den Wert der refreshPeriodSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRefreshPeriodSeconds() {
        return refreshPeriodSeconds;
    }

    /**
     * Legt den Wert der refreshPeriodSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRefreshPeriodSeconds(Integer value) {
        this.refreshPeriodSeconds = value;
    }

    /**
     * Ruft den Wert der maxConsecutiveFailures-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxConsecutiveFailures() {
        return maxConsecutiveFailures;
    }

    /**
     * Legt den Wert der maxConsecutiveFailures-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxConsecutiveFailures(Integer value) {
        this.maxConsecutiveFailures = value;
    }

    /**
     * Ruft den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxResponseWaitTimeMilliseconds() {
        return maxResponseWaitTimeMilliseconds;
    }

    /**
     * Legt den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxResponseWaitTimeMilliseconds(Integer value) {
        this.maxResponseWaitTimeMilliseconds = value;
    }

    /**
     * Ruft den Wert der enableAnnouncementSuppression-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableAnnouncementSuppression() {
        return enableAnnouncementSuppression;
    }

    /**
     * Legt den Wert der enableAnnouncementSuppression-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableAnnouncementSuppression(Boolean value) {
        this.enableAnnouncementSuppression = value;
    }

    /**
     * Ruft den Wert der serviceAccessCodeListName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServiceAccessCodeListName() {
        return serviceAccessCodeListName;
    }

    /**
     * Legt den Wert der serviceAccessCodeListName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServiceAccessCodeListName(JAXBElement<String> value) {
        this.serviceAccessCodeListName = value;
    }

}
