//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify system level settings for Service Pack Migration.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemServicePackMigrationModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemServicePackMigrationModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="maxSimultaneousMigrationTasks" type="{}MaxSimultaneousMigrationTasks" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemServicePackMigrationModifyRequest", propOrder = {
    "maxSimultaneousMigrationTasks"
})
public class SystemServicePackMigrationModifyRequest
    extends OCIRequest
{

    protected Integer maxSimultaneousMigrationTasks;

    /**
     * Ruft den Wert der maxSimultaneousMigrationTasks-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxSimultaneousMigrationTasks() {
        return maxSimultaneousMigrationTasks;
    }

    /**
     * Legt den Wert der maxSimultaneousMigrationTasks-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxSimultaneousMigrationTasks(Integer value) {
        this.maxSimultaneousMigrationTasks = value;
    }

}
