//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemCommunicationBarringGetRequest.
 *         
 *         Replaced by: SystemCommunicationBarringGetResponse21sp1 in AS data mode.
 *       
 * 
 * <p>Java-Klasse für SystemCommunicationBarringGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCommunicationBarringGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="directTransferScreening" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCommunicationBarringGetResponse", propOrder = {
    "directTransferScreening"
})
public class SystemCommunicationBarringGetResponse
    extends OCIDataResponse
{

    protected boolean directTransferScreening;

    /**
     * Ruft den Wert der directTransferScreening-Eigenschaft ab.
     * 
     */
    public boolean isDirectTransferScreening() {
        return directTransferScreening;
    }

    /**
     * Legt den Wert der directTransferScreening-Eigenschaft fest.
     * 
     */
    public void setDirectTransferScreening(boolean value) {
        this.directTransferScreening = value;
    }

}
