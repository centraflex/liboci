//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupOutgoingCallingPlanAuthorizationCodeGetListRequest.
 *       
 * 
 * <p>Java-Klasse für GroupOutgoingCallingPlanAuthorizationCodeGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupOutgoingCallingPlanAuthorizationCodeGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="groupCodeList" type="{}OutgoingCallingPlanGroupAuthorizationCodes"/>
 *         <element name="departmentCodeList" type="{}OutgoingCallingPlanDepartmentAuthorizationCodes" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupOutgoingCallingPlanAuthorizationCodeGetListResponse", propOrder = {
    "groupCodeList",
    "departmentCodeList"
})
public class GroupOutgoingCallingPlanAuthorizationCodeGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OutgoingCallingPlanGroupAuthorizationCodes groupCodeList;
    protected List<OutgoingCallingPlanDepartmentAuthorizationCodes> departmentCodeList;

    /**
     * Ruft den Wert der groupCodeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanGroupAuthorizationCodes }
     *     
     */
    public OutgoingCallingPlanGroupAuthorizationCodes getGroupCodeList() {
        return groupCodeList;
    }

    /**
     * Legt den Wert der groupCodeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanGroupAuthorizationCodes }
     *     
     */
    public void setGroupCodeList(OutgoingCallingPlanGroupAuthorizationCodes value) {
        this.groupCodeList = value;
    }

    /**
     * Gets the value of the departmentCodeList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the departmentCodeList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDepartmentCodeList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OutgoingCallingPlanDepartmentAuthorizationCodes }
     * 
     * 
     * @return
     *     The value of the departmentCodeList property.
     */
    public List<OutgoingCallingPlanDepartmentAuthorizationCodes> getDepartmentCodeList() {
        if (departmentCodeList == null) {
            departmentCodeList = new ArrayList<>();
        }
        return this.departmentCodeList;
    }

}
