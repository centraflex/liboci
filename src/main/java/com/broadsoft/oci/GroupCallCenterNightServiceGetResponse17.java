//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupCallCenterNightServiceGetRequest17.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterNightServiceGetResponse17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterNightServiceGetResponse17">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="action" type="{}CallCenterScheduledServiceAction"/>
 *         <element name="businessHours" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="forceNightService" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowManualOverrideViaFAC" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="transferPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="playAnnouncementBeforeAction" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="audioMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="audioUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="audioFileList" type="{}CallCenterAnnouncementDescriptionList" minOccurs="0"/>
 *         <element name="audioMediaTypeList" type="{}CallCenterAnnouncementMediaFileTypeList" minOccurs="0"/>
 *         <element name="videoMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="videoUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="videoFileList" type="{}CallCenterAnnouncementDescriptionList" minOccurs="0"/>
 *         <element name="videoMediaTypeList" type="{}CallCenterAnnouncementMediaFileTypeList" minOccurs="0"/>
 *         <element name="manualAnnouncementMode" type="{}CallCenterManualNightServiceAnnouncementMode"/>
 *         <element name="manualAudioMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="manualAudioUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="manualAudioFileList" type="{}CallCenterAnnouncementDescriptionList" minOccurs="0"/>
 *         <element name="manualAudioMediaTypeList" type="{}CallCenterAnnouncementMediaFileTypeList" minOccurs="0"/>
 *         <element name="manualVideoMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="manualVideoUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="manualVideoFileList" type="{}CallCenterAnnouncementDescriptionList" minOccurs="0"/>
 *         <element name="manualVideoMediaTypeList" type="{}CallCenterAnnouncementMediaFileTypeList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterNightServiceGetResponse17", propOrder = {
    "action",
    "businessHours",
    "forceNightService",
    "allowManualOverrideViaFAC",
    "transferPhoneNumber",
    "playAnnouncementBeforeAction",
    "audioMessageSelection",
    "audioUrlList",
    "audioFileList",
    "audioMediaTypeList",
    "videoMessageSelection",
    "videoUrlList",
    "videoFileList",
    "videoMediaTypeList",
    "manualAnnouncementMode",
    "manualAudioMessageSelection",
    "manualAudioUrlList",
    "manualAudioFileList",
    "manualAudioMediaTypeList",
    "manualVideoMessageSelection",
    "manualVideoUrlList",
    "manualVideoFileList",
    "manualVideoMediaTypeList"
})
public class GroupCallCenterNightServiceGetResponse17
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterScheduledServiceAction action;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String businessHours;
    protected boolean forceNightService;
    protected boolean allowManualOverrideViaFAC;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String transferPhoneNumber;
    protected boolean playAnnouncementBeforeAction;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection audioMessageSelection;
    protected CallCenterAnnouncementURLList audioUrlList;
    protected CallCenterAnnouncementDescriptionList audioFileList;
    protected CallCenterAnnouncementMediaFileTypeList audioMediaTypeList;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection videoMessageSelection;
    protected CallCenterAnnouncementURLList videoUrlList;
    protected CallCenterAnnouncementDescriptionList videoFileList;
    protected CallCenterAnnouncementMediaFileTypeList videoMediaTypeList;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterManualNightServiceAnnouncementMode manualAnnouncementMode;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection manualAudioMessageSelection;
    protected CallCenterAnnouncementURLList manualAudioUrlList;
    protected CallCenterAnnouncementDescriptionList manualAudioFileList;
    protected CallCenterAnnouncementMediaFileTypeList manualAudioMediaTypeList;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection manualVideoMessageSelection;
    protected CallCenterAnnouncementURLList manualVideoUrlList;
    protected CallCenterAnnouncementDescriptionList manualVideoFileList;
    protected CallCenterAnnouncementMediaFileTypeList manualVideoMediaTypeList;

    /**
     * Ruft den Wert der action-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterScheduledServiceAction }
     *     
     */
    public CallCenterScheduledServiceAction getAction() {
        return action;
    }

    /**
     * Legt den Wert der action-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterScheduledServiceAction }
     *     
     */
    public void setAction(CallCenterScheduledServiceAction value) {
        this.action = value;
    }

    /**
     * Ruft den Wert der businessHours-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessHours() {
        return businessHours;
    }

    /**
     * Legt den Wert der businessHours-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessHours(String value) {
        this.businessHours = value;
    }

    /**
     * Ruft den Wert der forceNightService-Eigenschaft ab.
     * 
     */
    public boolean isForceNightService() {
        return forceNightService;
    }

    /**
     * Legt den Wert der forceNightService-Eigenschaft fest.
     * 
     */
    public void setForceNightService(boolean value) {
        this.forceNightService = value;
    }

    /**
     * Ruft den Wert der allowManualOverrideViaFAC-Eigenschaft ab.
     * 
     */
    public boolean isAllowManualOverrideViaFAC() {
        return allowManualOverrideViaFAC;
    }

    /**
     * Legt den Wert der allowManualOverrideViaFAC-Eigenschaft fest.
     * 
     */
    public void setAllowManualOverrideViaFAC(boolean value) {
        this.allowManualOverrideViaFAC = value;
    }

    /**
     * Ruft den Wert der transferPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferPhoneNumber() {
        return transferPhoneNumber;
    }

    /**
     * Legt den Wert der transferPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferPhoneNumber(String value) {
        this.transferPhoneNumber = value;
    }

    /**
     * Ruft den Wert der playAnnouncementBeforeAction-Eigenschaft ab.
     * 
     */
    public boolean isPlayAnnouncementBeforeAction() {
        return playAnnouncementBeforeAction;
    }

    /**
     * Legt den Wert der playAnnouncementBeforeAction-Eigenschaft fest.
     * 
     */
    public void setPlayAnnouncementBeforeAction(boolean value) {
        this.playAnnouncementBeforeAction = value;
    }

    /**
     * Ruft den Wert der audioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getAudioMessageSelection() {
        return audioMessageSelection;
    }

    /**
     * Legt den Wert der audioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.audioMessageSelection = value;
    }

    /**
     * Ruft den Wert der audioUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getAudioUrlList() {
        return audioUrlList;
    }

    /**
     * Legt den Wert der audioUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setAudioUrlList(CallCenterAnnouncementURLList value) {
        this.audioUrlList = value;
    }

    /**
     * Ruft den Wert der audioFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public CallCenterAnnouncementDescriptionList getAudioFileList() {
        return audioFileList;
    }

    /**
     * Legt den Wert der audioFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public void setAudioFileList(CallCenterAnnouncementDescriptionList value) {
        this.audioFileList = value;
    }

    /**
     * Ruft den Wert der audioMediaTypeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public CallCenterAnnouncementMediaFileTypeList getAudioMediaTypeList() {
        return audioMediaTypeList;
    }

    /**
     * Legt den Wert der audioMediaTypeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public void setAudioMediaTypeList(CallCenterAnnouncementMediaFileTypeList value) {
        this.audioMediaTypeList = value;
    }

    /**
     * Ruft den Wert der videoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getVideoMessageSelection() {
        return videoMessageSelection;
    }

    /**
     * Legt den Wert der videoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.videoMessageSelection = value;
    }

    /**
     * Ruft den Wert der videoUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getVideoUrlList() {
        return videoUrlList;
    }

    /**
     * Legt den Wert der videoUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setVideoUrlList(CallCenterAnnouncementURLList value) {
        this.videoUrlList = value;
    }

    /**
     * Ruft den Wert der videoFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public CallCenterAnnouncementDescriptionList getVideoFileList() {
        return videoFileList;
    }

    /**
     * Legt den Wert der videoFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public void setVideoFileList(CallCenterAnnouncementDescriptionList value) {
        this.videoFileList = value;
    }

    /**
     * Ruft den Wert der videoMediaTypeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public CallCenterAnnouncementMediaFileTypeList getVideoMediaTypeList() {
        return videoMediaTypeList;
    }

    /**
     * Legt den Wert der videoMediaTypeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public void setVideoMediaTypeList(CallCenterAnnouncementMediaFileTypeList value) {
        this.videoMediaTypeList = value;
    }

    /**
     * Ruft den Wert der manualAnnouncementMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterManualNightServiceAnnouncementMode }
     *     
     */
    public CallCenterManualNightServiceAnnouncementMode getManualAnnouncementMode() {
        return manualAnnouncementMode;
    }

    /**
     * Legt den Wert der manualAnnouncementMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterManualNightServiceAnnouncementMode }
     *     
     */
    public void setManualAnnouncementMode(CallCenterManualNightServiceAnnouncementMode value) {
        this.manualAnnouncementMode = value;
    }

    /**
     * Ruft den Wert der manualAudioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getManualAudioMessageSelection() {
        return manualAudioMessageSelection;
    }

    /**
     * Legt den Wert der manualAudioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setManualAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.manualAudioMessageSelection = value;
    }

    /**
     * Ruft den Wert der manualAudioUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getManualAudioUrlList() {
        return manualAudioUrlList;
    }

    /**
     * Legt den Wert der manualAudioUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setManualAudioUrlList(CallCenterAnnouncementURLList value) {
        this.manualAudioUrlList = value;
    }

    /**
     * Ruft den Wert der manualAudioFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public CallCenterAnnouncementDescriptionList getManualAudioFileList() {
        return manualAudioFileList;
    }

    /**
     * Legt den Wert der manualAudioFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public void setManualAudioFileList(CallCenterAnnouncementDescriptionList value) {
        this.manualAudioFileList = value;
    }

    /**
     * Ruft den Wert der manualAudioMediaTypeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public CallCenterAnnouncementMediaFileTypeList getManualAudioMediaTypeList() {
        return manualAudioMediaTypeList;
    }

    /**
     * Legt den Wert der manualAudioMediaTypeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public void setManualAudioMediaTypeList(CallCenterAnnouncementMediaFileTypeList value) {
        this.manualAudioMediaTypeList = value;
    }

    /**
     * Ruft den Wert der manualVideoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getManualVideoMessageSelection() {
        return manualVideoMessageSelection;
    }

    /**
     * Legt den Wert der manualVideoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setManualVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.manualVideoMessageSelection = value;
    }

    /**
     * Ruft den Wert der manualVideoUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getManualVideoUrlList() {
        return manualVideoUrlList;
    }

    /**
     * Legt den Wert der manualVideoUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setManualVideoUrlList(CallCenterAnnouncementURLList value) {
        this.manualVideoUrlList = value;
    }

    /**
     * Ruft den Wert der manualVideoFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public CallCenterAnnouncementDescriptionList getManualVideoFileList() {
        return manualVideoFileList;
    }

    /**
     * Legt den Wert der manualVideoFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public void setManualVideoFileList(CallCenterAnnouncementDescriptionList value) {
        this.manualVideoFileList = value;
    }

    /**
     * Ruft den Wert der manualVideoMediaTypeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public CallCenterAnnouncementMediaFileTypeList getManualVideoMediaTypeList() {
        return manualVideoMediaTypeList;
    }

    /**
     * Legt den Wert der manualVideoMediaTypeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public void setManualVideoMediaTypeList(CallCenterAnnouncementMediaFileTypeList value) {
        this.manualVideoMediaTypeList = value;
    }

}
