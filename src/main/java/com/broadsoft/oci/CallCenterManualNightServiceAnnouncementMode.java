//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterManualNightServiceAnnouncementMode.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterManualNightServiceAnnouncementMode">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Normal Announcement"/>
 *     <enumeration value="Manual Announcement"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterManualNightServiceAnnouncementMode")
@XmlEnum
public enum CallCenterManualNightServiceAnnouncementMode {

    @XmlEnumValue("Normal Announcement")
    NORMAL_ANNOUNCEMENT("Normal Announcement"),
    @XmlEnumValue("Manual Announcement")
    MANUAL_ANNOUNCEMENT("Manual Announcement");
    private final String value;

    CallCenterManualNightServiceAnnouncementMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterManualNightServiceAnnouncementMode fromValue(String v) {
        for (CallCenterManualNightServiceAnnouncementMode c: CallCenterManualNightServiceAnnouncementMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
