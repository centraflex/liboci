//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Enterprise Trunk Number Prefix
 *       
 * 
 * <p>Java-Klasse für EnterpriseTrunkNumberPrefix22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseTrunkNumberPrefix22">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="numberPrefix" type="{}EnterpriseTrunkNumberPrefix"/>
 *         <element name="extensionRange" type="{}ExtensionRange17" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseTrunkNumberPrefix22", propOrder = {
    "numberPrefix",
    "extensionRange"
})
public class EnterpriseTrunkNumberPrefix22 {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String numberPrefix;
    protected ExtensionRange17 extensionRange;

    /**
     * Ruft den Wert der numberPrefix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberPrefix() {
        return numberPrefix;
    }

    /**
     * Legt den Wert der numberPrefix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberPrefix(String value) {
        this.numberPrefix = value;
    }

    /**
     * Ruft den Wert der extensionRange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtensionRange17 }
     *     
     */
    public ExtensionRange17 getExtensionRange() {
        return extensionRange;
    }

    /**
     * Legt den Wert der extensionRange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtensionRange17 }
     *     
     */
    public void setExtensionRange(ExtensionRange17 value) {
        this.extensionRange = value;
    }

}
