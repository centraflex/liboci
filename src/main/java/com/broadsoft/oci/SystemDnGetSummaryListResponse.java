//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemDnGetSummaryListRequest.
 *         The column headings are "Phone Numbers, "Service Provider Id", "Is Enterprise" and "Reseller Id".
 *         
 *         The following columns are only returned in AS data mode:       
 *           "Reseller Id"       
 *       
 * 
 * <p>Java-Klasse für SystemDnGetSummaryListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDnGetSummaryListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="dnSummaryTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDnGetSummaryListResponse", propOrder = {
    "dnSummaryTable"
})
public class SystemDnGetSummaryListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable dnSummaryTable;

    /**
     * Ruft den Wert der dnSummaryTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDnSummaryTable() {
        return dnSummaryTable;
    }

    /**
     * Legt den Wert der dnSummaryTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDnSummaryTable(OCITable value) {
        this.dnSummaryTable = value;
    }

}
