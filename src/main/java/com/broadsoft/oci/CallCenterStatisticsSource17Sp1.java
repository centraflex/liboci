//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterStatisticsSource17sp1.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterStatisticsSource17sp1">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="None"/>
 *     <enumeration value="Application Server"/>
 *     <enumeration value="External Reporting Server"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterStatisticsSource17sp1")
@XmlEnum
public enum CallCenterStatisticsSource17Sp1 {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Application Server")
    APPLICATION_SERVER("Application Server"),
    @XmlEnumValue("External Reporting Server")
    EXTERNAL_REPORTING_SERVER("External Reporting Server");
    private final String value;

    CallCenterStatisticsSource17Sp1(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterStatisticsSource17Sp1 fromValue(String v) {
        for (CallCenterStatisticsSource17Sp1 c: CallCenterStatisticsSource17Sp1 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
