//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modifies the system's automatic callback attributes.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemAutomaticCallbackModifyRequest15 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAutomaticCallbackModifyRequest15">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="monitorMinutes" type="{}AutomaticCallbackMonitorMinutes" minOccurs="0"/>
 *         <element name="maxMonitorsPerOriginator" type="{}AutomaticCallbackMaxMonitorsPerOriginator" minOccurs="0"/>
 *         <element name="maxCallbackRings" type="{}AutomaticCallbackMaxCallbackRings" minOccurs="0"/>
 *         <element name="maxMonitorsPerTerminator" type="{}AutomaticCallbackMaxMonitorsPerTerminator" minOccurs="0"/>
 *         <element name="terminatorIdleGuardSeconds" type="{}AutomaticCallbackTerminatorIdleGuardSeconds" minOccurs="0"/>
 *         <element name="callbackMethod" type="{}AutomaticCallbackMethod" minOccurs="0"/>
 *         <element name="pollingIntervalSeconds" type="{}AutomaticCallbackPollingIntervalSeconds" minOccurs="0"/>
 *         <element name="activationDigit" type="{}AutomaticCallbackActivationDigit" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAutomaticCallbackModifyRequest15", propOrder = {
    "monitorMinutes",
    "maxMonitorsPerOriginator",
    "maxCallbackRings",
    "maxMonitorsPerTerminator",
    "terminatorIdleGuardSeconds",
    "callbackMethod",
    "pollingIntervalSeconds",
    "activationDigit"
})
public class SystemAutomaticCallbackModifyRequest15
    extends OCIRequest
{

    protected Integer monitorMinutes;
    protected Integer maxMonitorsPerOriginator;
    protected Integer maxCallbackRings;
    protected Integer maxMonitorsPerTerminator;
    protected Integer terminatorIdleGuardSeconds;
    @XmlSchemaType(name = "token")
    protected AutomaticCallbackMethod callbackMethod;
    protected Integer pollingIntervalSeconds;
    protected Integer activationDigit;

    /**
     * Ruft den Wert der monitorMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMonitorMinutes() {
        return monitorMinutes;
    }

    /**
     * Legt den Wert der monitorMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMonitorMinutes(Integer value) {
        this.monitorMinutes = value;
    }

    /**
     * Ruft den Wert der maxMonitorsPerOriginator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxMonitorsPerOriginator() {
        return maxMonitorsPerOriginator;
    }

    /**
     * Legt den Wert der maxMonitorsPerOriginator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxMonitorsPerOriginator(Integer value) {
        this.maxMonitorsPerOriginator = value;
    }

    /**
     * Ruft den Wert der maxCallbackRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCallbackRings() {
        return maxCallbackRings;
    }

    /**
     * Legt den Wert der maxCallbackRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCallbackRings(Integer value) {
        this.maxCallbackRings = value;
    }

    /**
     * Ruft den Wert der maxMonitorsPerTerminator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxMonitorsPerTerminator() {
        return maxMonitorsPerTerminator;
    }

    /**
     * Legt den Wert der maxMonitorsPerTerminator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxMonitorsPerTerminator(Integer value) {
        this.maxMonitorsPerTerminator = value;
    }

    /**
     * Ruft den Wert der terminatorIdleGuardSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTerminatorIdleGuardSeconds() {
        return terminatorIdleGuardSeconds;
    }

    /**
     * Legt den Wert der terminatorIdleGuardSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTerminatorIdleGuardSeconds(Integer value) {
        this.terminatorIdleGuardSeconds = value;
    }

    /**
     * Ruft den Wert der callbackMethod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutomaticCallbackMethod }
     *     
     */
    public AutomaticCallbackMethod getCallbackMethod() {
        return callbackMethod;
    }

    /**
     * Legt den Wert der callbackMethod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutomaticCallbackMethod }
     *     
     */
    public void setCallbackMethod(AutomaticCallbackMethod value) {
        this.callbackMethod = value;
    }

    /**
     * Ruft den Wert der pollingIntervalSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPollingIntervalSeconds() {
        return pollingIntervalSeconds;
    }

    /**
     * Legt den Wert der pollingIntervalSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPollingIntervalSeconds(Integer value) {
        this.pollingIntervalSeconds = value;
    }

    /**
     * Ruft den Wert der activationDigit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getActivationDigit() {
        return activationDigit;
    }

    /**
     * Legt den Wert der activationDigit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setActivationDigit(Integer value) {
        this.activationDigit = value;
    }

}
