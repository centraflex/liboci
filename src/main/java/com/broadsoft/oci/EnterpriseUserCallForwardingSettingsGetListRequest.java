//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Get a list of users and the service settings for a Call Forwarding service.
 *         The response is either EnterpriseUserCallForwardingSettingsGetListResponse or ErrorResponse.
 *         By default, virtual users and non-virtual users that match the selected search criteria are included in the response when "includeVirtualUsers" is present; otherwise, if "includeVirtualUsers" is absent, then only non-virtual users are returned. 
 *         The search can be performed using multiple criteria. If search criteria is specified, only the users matching all of the specified search criteria are included in the response. If no search criteria is specified, all results are returned.
 *       
 * 
 * <p>Java-Klasse für EnterpriseUserCallForwardingSettingsGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseUserCallForwardingSettingsGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="callForwardingService" type="{}CallForwardingService"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl"/>
 *         <choice>
 *           <element name="sortByUserLastName" type="{}SortByUserLastName"/>
 *           <element name="sortByUserFirstName" type="{}SortByUserFirstName"/>
 *           <element name="sortByUserDepartment" type="{}SortByUserDepartment"/>
 *           <element name="sortByDn" type="{}SortByDn"/>
 *           <element name="sortByExtension" type="{}SortByExtension"/>
 *           <element name="sortByGroupId" type="{}SortByGroupId"/>
 *           <element name="sortByForwardedToNumber" type="{}SortByForwardedToNumber"/>
 *         </choice>
 *         <element name="includeVirtualUsers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="searchCriteriaUserLastName" type="{}SearchCriteriaUserLastName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaUserFirstName" type="{}SearchCriteriaUserFirstName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaUserId" type="{}SearchCriteriaUserId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDn" type="{}SearchCriteriaDn" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExtension" type="{}SearchCriteriaExtension" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaEmailAddress" type="{}SearchCriteriaEmailAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactUserDepartment" type="{}SearchCriteriaExactUserDepartment" minOccurs="0"/>
 *         <element name="searchCriteriaExactUserInTrunkGroup" type="{}SearchCriteriaExactUserInTrunkGroup" minOccurs="0"/>
 *         <element name="searchCriteriaForwardedToNumber" type="{}SearchCriteriaForwardedToNumber" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseUserCallForwardingSettingsGetListRequest", propOrder = {
    "serviceProviderId",
    "callForwardingService",
    "responsePagingControl",
    "sortByUserLastName",
    "sortByUserFirstName",
    "sortByUserDepartment",
    "sortByDn",
    "sortByExtension",
    "sortByGroupId",
    "sortByForwardedToNumber",
    "includeVirtualUsers",
    "searchCriteriaUserLastName",
    "searchCriteriaUserFirstName",
    "searchCriteriaUserId",
    "searchCriteriaDn",
    "searchCriteriaExtension",
    "searchCriteriaEmailAddress",
    "searchCriteriaExactUserDepartment",
    "searchCriteriaExactUserInTrunkGroup",
    "searchCriteriaForwardedToNumber"
})
public class EnterpriseUserCallForwardingSettingsGetListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallForwardingService callForwardingService;
    @XmlElement(required = true)
    protected ResponsePagingControl responsePagingControl;
    protected SortByUserLastName sortByUserLastName;
    protected SortByUserFirstName sortByUserFirstName;
    protected SortByUserDepartment sortByUserDepartment;
    protected SortByDn sortByDn;
    protected SortByExtension sortByExtension;
    protected SortByGroupId sortByGroupId;
    protected SortByForwardedToNumber sortByForwardedToNumber;
    protected Boolean includeVirtualUsers;
    protected List<SearchCriteriaUserLastName> searchCriteriaUserLastName;
    protected List<SearchCriteriaUserFirstName> searchCriteriaUserFirstName;
    protected List<SearchCriteriaUserId> searchCriteriaUserId;
    protected List<SearchCriteriaDn> searchCriteriaDn;
    protected List<SearchCriteriaExtension> searchCriteriaExtension;
    protected List<SearchCriteriaEmailAddress> searchCriteriaEmailAddress;
    protected SearchCriteriaExactUserDepartment searchCriteriaExactUserDepartment;
    protected SearchCriteriaExactUserInTrunkGroup searchCriteriaExactUserInTrunkGroup;
    protected List<SearchCriteriaForwardedToNumber> searchCriteriaForwardedToNumber;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der callForwardingService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallForwardingService }
     *     
     */
    public CallForwardingService getCallForwardingService() {
        return callForwardingService;
    }

    /**
     * Legt den Wert der callForwardingService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallForwardingService }
     *     
     */
    public void setCallForwardingService(CallForwardingService value) {
        this.callForwardingService = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

    /**
     * Ruft den Wert der sortByUserLastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByUserLastName }
     *     
     */
    public SortByUserLastName getSortByUserLastName() {
        return sortByUserLastName;
    }

    /**
     * Legt den Wert der sortByUserLastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByUserLastName }
     *     
     */
    public void setSortByUserLastName(SortByUserLastName value) {
        this.sortByUserLastName = value;
    }

    /**
     * Ruft den Wert der sortByUserFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByUserFirstName }
     *     
     */
    public SortByUserFirstName getSortByUserFirstName() {
        return sortByUserFirstName;
    }

    /**
     * Legt den Wert der sortByUserFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByUserFirstName }
     *     
     */
    public void setSortByUserFirstName(SortByUserFirstName value) {
        this.sortByUserFirstName = value;
    }

    /**
     * Ruft den Wert der sortByUserDepartment-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByUserDepartment }
     *     
     */
    public SortByUserDepartment getSortByUserDepartment() {
        return sortByUserDepartment;
    }

    /**
     * Legt den Wert der sortByUserDepartment-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByUserDepartment }
     *     
     */
    public void setSortByUserDepartment(SortByUserDepartment value) {
        this.sortByUserDepartment = value;
    }

    /**
     * Ruft den Wert der sortByDn-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByDn }
     *     
     */
    public SortByDn getSortByDn() {
        return sortByDn;
    }

    /**
     * Legt den Wert der sortByDn-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByDn }
     *     
     */
    public void setSortByDn(SortByDn value) {
        this.sortByDn = value;
    }

    /**
     * Ruft den Wert der sortByExtension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByExtension }
     *     
     */
    public SortByExtension getSortByExtension() {
        return sortByExtension;
    }

    /**
     * Legt den Wert der sortByExtension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByExtension }
     *     
     */
    public void setSortByExtension(SortByExtension value) {
        this.sortByExtension = value;
    }

    /**
     * Ruft den Wert der sortByGroupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByGroupId }
     *     
     */
    public SortByGroupId getSortByGroupId() {
        return sortByGroupId;
    }

    /**
     * Legt den Wert der sortByGroupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByGroupId }
     *     
     */
    public void setSortByGroupId(SortByGroupId value) {
        this.sortByGroupId = value;
    }

    /**
     * Ruft den Wert der sortByForwardedToNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SortByForwardedToNumber }
     *     
     */
    public SortByForwardedToNumber getSortByForwardedToNumber() {
        return sortByForwardedToNumber;
    }

    /**
     * Legt den Wert der sortByForwardedToNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SortByForwardedToNumber }
     *     
     */
    public void setSortByForwardedToNumber(SortByForwardedToNumber value) {
        this.sortByForwardedToNumber = value;
    }

    /**
     * Ruft den Wert der includeVirtualUsers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeVirtualUsers() {
        return includeVirtualUsers;
    }

    /**
     * Legt den Wert der includeVirtualUsers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeVirtualUsers(Boolean value) {
        this.includeVirtualUsers = value;
    }

    /**
     * Gets the value of the searchCriteriaUserLastName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserLastName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserLastName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserLastName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserLastName property.
     */
    public List<SearchCriteriaUserLastName> getSearchCriteriaUserLastName() {
        if (searchCriteriaUserLastName == null) {
            searchCriteriaUserLastName = new ArrayList<>();
        }
        return this.searchCriteriaUserLastName;
    }

    /**
     * Gets the value of the searchCriteriaUserFirstName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserFirstName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserFirstName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserFirstName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserFirstName property.
     */
    public List<SearchCriteriaUserFirstName> getSearchCriteriaUserFirstName() {
        if (searchCriteriaUserFirstName == null) {
            searchCriteriaUserFirstName = new ArrayList<>();
        }
        return this.searchCriteriaUserFirstName;
    }

    /**
     * Gets the value of the searchCriteriaUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserId property.
     */
    public List<SearchCriteriaUserId> getSearchCriteriaUserId() {
        if (searchCriteriaUserId == null) {
            searchCriteriaUserId = new ArrayList<>();
        }
        return this.searchCriteriaUserId;
    }

    /**
     * Gets the value of the searchCriteriaDn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDn }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDn property.
     */
    public List<SearchCriteriaDn> getSearchCriteriaDn() {
        if (searchCriteriaDn == null) {
            searchCriteriaDn = new ArrayList<>();
        }
        return this.searchCriteriaDn;
    }

    /**
     * Gets the value of the searchCriteriaExtension property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaExtension property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaExtension().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaExtension }
     * 
     * 
     * @return
     *     The value of the searchCriteriaExtension property.
     */
    public List<SearchCriteriaExtension> getSearchCriteriaExtension() {
        if (searchCriteriaExtension == null) {
            searchCriteriaExtension = new ArrayList<>();
        }
        return this.searchCriteriaExtension;
    }

    /**
     * Gets the value of the searchCriteriaEmailAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaEmailAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaEmailAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaEmailAddress }
     * 
     * 
     * @return
     *     The value of the searchCriteriaEmailAddress property.
     */
    public List<SearchCriteriaEmailAddress> getSearchCriteriaEmailAddress() {
        if (searchCriteriaEmailAddress == null) {
            searchCriteriaEmailAddress = new ArrayList<>();
        }
        return this.searchCriteriaEmailAddress;
    }

    /**
     * Ruft den Wert der searchCriteriaExactUserDepartment-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactUserDepartment }
     *     
     */
    public SearchCriteriaExactUserDepartment getSearchCriteriaExactUserDepartment() {
        return searchCriteriaExactUserDepartment;
    }

    /**
     * Legt den Wert der searchCriteriaExactUserDepartment-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactUserDepartment }
     *     
     */
    public void setSearchCriteriaExactUserDepartment(SearchCriteriaExactUserDepartment value) {
        this.searchCriteriaExactUserDepartment = value;
    }

    /**
     * Ruft den Wert der searchCriteriaExactUserInTrunkGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactUserInTrunkGroup }
     *     
     */
    public SearchCriteriaExactUserInTrunkGroup getSearchCriteriaExactUserInTrunkGroup() {
        return searchCriteriaExactUserInTrunkGroup;
    }

    /**
     * Legt den Wert der searchCriteriaExactUserInTrunkGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactUserInTrunkGroup }
     *     
     */
    public void setSearchCriteriaExactUserInTrunkGroup(SearchCriteriaExactUserInTrunkGroup value) {
        this.searchCriteriaExactUserInTrunkGroup = value;
    }

    /**
     * Gets the value of the searchCriteriaForwardedToNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaForwardedToNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaForwardedToNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaForwardedToNumber }
     * 
     * 
     * @return
     *     The value of the searchCriteriaForwardedToNumber property.
     */
    public List<SearchCriteriaForwardedToNumber> getSearchCriteriaForwardedToNumber() {
        if (searchCriteriaForwardedToNumber == null) {
            searchCriteriaForwardedToNumber = new ArrayList<>();
        }
        return this.searchCriteriaForwardedToNumber;
    }

}
