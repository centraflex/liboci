//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Replaced by: GroupGroupPagingGetInstanceResponse17sp3
 *         Response to GroupGroupPagingGetInstanceRequest.
 *         Contains the service profile information.
 *       
 * 
 * <p>Java-Klasse für GroupGroupPagingGetInstanceResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupGroupPagingGetInstanceResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceReadProfile"/>
 *         <element name="confirmationToneTimeoutSeconds" type="{}GroupPagingConfirmationToneTimeoutSeconds"/>
 *         <element name="deliverOriginatorCLIDInstead" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="originatorCLIDPrefix" type="{}GroupPagingOriginatorCLIDPrefix" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupGroupPagingGetInstanceResponse", propOrder = {
    "serviceInstanceProfile",
    "confirmationToneTimeoutSeconds",
    "deliverOriginatorCLIDInstead",
    "originatorCLIDPrefix"
})
public class GroupGroupPagingGetInstanceResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected ServiceInstanceReadProfile serviceInstanceProfile;
    protected int confirmationToneTimeoutSeconds;
    protected boolean deliverOriginatorCLIDInstead;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String originatorCLIDPrefix;

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceReadProfile }
     *     
     */
    public ServiceInstanceReadProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceReadProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceReadProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der confirmationToneTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getConfirmationToneTimeoutSeconds() {
        return confirmationToneTimeoutSeconds;
    }

    /**
     * Legt den Wert der confirmationToneTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setConfirmationToneTimeoutSeconds(int value) {
        this.confirmationToneTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der deliverOriginatorCLIDInstead-Eigenschaft ab.
     * 
     */
    public boolean isDeliverOriginatorCLIDInstead() {
        return deliverOriginatorCLIDInstead;
    }

    /**
     * Legt den Wert der deliverOriginatorCLIDInstead-Eigenschaft fest.
     * 
     */
    public void setDeliverOriginatorCLIDInstead(boolean value) {
        this.deliverOriginatorCLIDInstead = value;
    }

    /**
     * Ruft den Wert der originatorCLIDPrefix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatorCLIDPrefix() {
        return originatorCLIDPrefix;
    }

    /**
     * Legt den Wert der originatorCLIDPrefix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatorCLIDPrefix(String value) {
        this.originatorCLIDPrefix = value;
    }

}
