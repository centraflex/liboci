//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SessionAdmissionControlForMusicOnHoldType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SessionAdmissionControlForMusicOnHoldType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Do Not Play"/>
 *     <enumeration value="Play And Count"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SessionAdmissionControlForMusicOnHoldType")
@XmlEnum
public enum SessionAdmissionControlForMusicOnHoldType {

    @XmlEnumValue("Do Not Play")
    DO_NOT_PLAY("Do Not Play"),
    @XmlEnumValue("Play And Count")
    PLAY_AND_COUNT("Play And Count");
    private final String value;

    SessionAdmissionControlForMusicOnHoldType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SessionAdmissionControlForMusicOnHoldType fromValue(String v) {
        for (SessionAdmissionControlForMusicOnHoldType c: SessionAdmissionControlForMusicOnHoldType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
