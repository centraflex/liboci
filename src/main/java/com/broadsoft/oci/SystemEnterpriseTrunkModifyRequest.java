//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the system level data associated with the Enterprise Trunk Service. 
 * 
 *         The response is either a SuccessResponse or an ErrorResponse. 
 *       
 * 
 * <p>Java-Klasse für SystemEnterpriseTrunkModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemEnterpriseTrunkModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enableHoldoverOfHighwaterCallCounts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="holdoverPeriod" type="{}EnterpriseTrunkHighwateCallCountHoldoverPeriodMinutes" minOccurs="0"/>
 *         <element name="timeZoneOffsetMinutes" type="{}EnterpriseTrunkTimeZoneOffsetMinutes" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemEnterpriseTrunkModifyRequest", propOrder = {
    "enableHoldoverOfHighwaterCallCounts",
    "holdoverPeriod",
    "timeZoneOffsetMinutes"
})
public class SystemEnterpriseTrunkModifyRequest
    extends OCIRequest
{

    protected Boolean enableHoldoverOfHighwaterCallCounts;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String holdoverPeriod;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeZoneOffsetMinutes;

    /**
     * Ruft den Wert der enableHoldoverOfHighwaterCallCounts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableHoldoverOfHighwaterCallCounts() {
        return enableHoldoverOfHighwaterCallCounts;
    }

    /**
     * Legt den Wert der enableHoldoverOfHighwaterCallCounts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableHoldoverOfHighwaterCallCounts(Boolean value) {
        this.enableHoldoverOfHighwaterCallCounts = value;
    }

    /**
     * Ruft den Wert der holdoverPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoldoverPeriod() {
        return holdoverPeriod;
    }

    /**
     * Legt den Wert der holdoverPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoldoverPeriod(String value) {
        this.holdoverPeriod = value;
    }

    /**
     * Ruft den Wert der timeZoneOffsetMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeZoneOffsetMinutes() {
        return timeZoneOffsetMinutes;
    }

    /**
     * Legt den Wert der timeZoneOffsetMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeZoneOffsetMinutes(String value) {
        this.timeZoneOffsetMinutes = value;
    }

}
