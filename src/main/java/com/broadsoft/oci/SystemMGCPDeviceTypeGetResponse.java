//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemMGCPDeviceTypeGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemMGCPDeviceTypeGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemMGCPDeviceTypeGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isObsolete" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="profile" type="{}SignalingAddressType"/>
 *         <element name="numberOfPorts" type="{}UnboundedPositiveInt"/>
 *         <element name="protocolChoice" type="{}AccessDeviceProtocol" maxOccurs="unbounded"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemMGCPDeviceTypeGetResponse", propOrder = {
    "isObsolete",
    "profile",
    "numberOfPorts",
    "protocolChoice"
})
public class SystemMGCPDeviceTypeGetResponse
    extends OCIDataResponse
{

    protected boolean isObsolete;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SignalingAddressType profile;
    @XmlElement(required = true)
    protected UnboundedPositiveInt numberOfPorts;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> protocolChoice;

    /**
     * Ruft den Wert der isObsolete-Eigenschaft ab.
     * 
     */
    public boolean isIsObsolete() {
        return isObsolete;
    }

    /**
     * Legt den Wert der isObsolete-Eigenschaft fest.
     * 
     */
    public void setIsObsolete(boolean value) {
        this.isObsolete = value;
    }

    /**
     * Ruft den Wert der profile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SignalingAddressType }
     *     
     */
    public SignalingAddressType getProfile() {
        return profile;
    }

    /**
     * Legt den Wert der profile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SignalingAddressType }
     *     
     */
    public void setProfile(SignalingAddressType value) {
        this.profile = value;
    }

    /**
     * Ruft den Wert der numberOfPorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public UnboundedPositiveInt getNumberOfPorts() {
        return numberOfPorts;
    }

    /**
     * Legt den Wert der numberOfPorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public void setNumberOfPorts(UnboundedPositiveInt value) {
        this.numberOfPorts = value;
    }

    /**
     * Gets the value of the protocolChoice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the protocolChoice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProtocolChoice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the protocolChoice property.
     */
    public List<String> getProtocolChoice() {
        if (protocolChoice == null) {
            protocolChoice = new ArrayList<>();
        }
        return this.protocolChoice;
    }

}
