//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemConfigurableFileSystemGetRequest23.
 *         Contains the File System parameters.
 *       
 * 
 * <p>Java-Klasse für SystemConfigurableFileSystemGetResponse23 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemConfigurableFileSystemGetResponse23">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="mediaDirectory" type="{}ConfigurableFileSystemDirectory"/>
 *         <choice>
 *           <element name="protocolFile">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="replicated" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *           <element name="protocolWebDAV">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="secure" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                     <element name="userName" type="{}WebDAVUserName" minOccurs="0"/>
 *                     <element name="fileServerFQDN" type="{}NetAddress"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemConfigurableFileSystemGetResponse23", propOrder = {
    "mediaDirectory",
    "protocolFile",
    "protocolWebDAV"
})
public class SystemConfigurableFileSystemGetResponse23
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mediaDirectory;
    protected SystemConfigurableFileSystemGetResponse23 .ProtocolFile protocolFile;
    protected SystemConfigurableFileSystemGetResponse23 .ProtocolWebDAV protocolWebDAV;

    /**
     * Ruft den Wert der mediaDirectory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMediaDirectory() {
        return mediaDirectory;
    }

    /**
     * Legt den Wert der mediaDirectory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMediaDirectory(String value) {
        this.mediaDirectory = value;
    }

    /**
     * Ruft den Wert der protocolFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemConfigurableFileSystemGetResponse23 .ProtocolFile }
     *     
     */
    public SystemConfigurableFileSystemGetResponse23 .ProtocolFile getProtocolFile() {
        return protocolFile;
    }

    /**
     * Legt den Wert der protocolFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemConfigurableFileSystemGetResponse23 .ProtocolFile }
     *     
     */
    public void setProtocolFile(SystemConfigurableFileSystemGetResponse23 .ProtocolFile value) {
        this.protocolFile = value;
    }

    /**
     * Ruft den Wert der protocolWebDAV-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemConfigurableFileSystemGetResponse23 .ProtocolWebDAV }
     *     
     */
    public SystemConfigurableFileSystemGetResponse23 .ProtocolWebDAV getProtocolWebDAV() {
        return protocolWebDAV;
    }

    /**
     * Legt den Wert der protocolWebDAV-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemConfigurableFileSystemGetResponse23 .ProtocolWebDAV }
     *     
     */
    public void setProtocolWebDAV(SystemConfigurableFileSystemGetResponse23 .ProtocolWebDAV value) {
        this.protocolWebDAV = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="replicated" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "replicated"
    })
    public static class ProtocolFile {

        protected boolean replicated;

        /**
         * Ruft den Wert der replicated-Eigenschaft ab.
         * 
         */
        public boolean isReplicated() {
            return replicated;
        }

        /**
         * Legt den Wert der replicated-Eigenschaft fest.
         * 
         */
        public void setReplicated(boolean value) {
            this.replicated = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="secure" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         <element name="userName" type="{}WebDAVUserName" minOccurs="0"/>
     *         <element name="fileServerFQDN" type="{}NetAddress"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "secure",
        "userName",
        "fileServerFQDN"
    })
    public static class ProtocolWebDAV {

        protected boolean secure;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String userName;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String fileServerFQDN;

        /**
         * Ruft den Wert der secure-Eigenschaft ab.
         * 
         */
        public boolean isSecure() {
            return secure;
        }

        /**
         * Legt den Wert der secure-Eigenschaft fest.
         * 
         */
        public void setSecure(boolean value) {
            this.secure = value;
        }

        /**
         * Ruft den Wert der userName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUserName() {
            return userName;
        }

        /**
         * Legt den Wert der userName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUserName(String value) {
            this.userName = value;
        }

        /**
         * Ruft den Wert der fileServerFQDN-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFileServerFQDN() {
            return fileServerFQDN;
        }

        /**
         * Legt den Wert der fileServerFQDN-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFileServerFQDN(String value) {
            this.fileServerFQDN = value;
        }

    }

}
