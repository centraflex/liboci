//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The system-level collaborate supportOutdial setting is returned in the response when the system-level collaborate supportOutdial setting is disabled.
 *         Response to UserCollaborateBridgeGetRequest20sp1.
 *       
 * 
 * <p>Java-Klasse für UserCollaborateBridgeGetResponse20sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCollaborateBridgeGetResponse20sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="bridgeId" type="{}UserId"/>
 *         <element name="bridgeName" type="{}CollaborateBridgeName"/>
 *         <element name="phoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="extension" type="{}Extension17" minOccurs="0"/>
 *         <element name="alternateNumberEntry" type="{}AlternateNumberEntry21" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="supportOutDial" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxCollaborateRoomParticipants" type="{}CollaborateRoomMaximumParticipants20sp1"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCollaborateBridgeGetResponse20sp1", propOrder = {
    "bridgeId",
    "bridgeName",
    "phoneNumber",
    "extension",
    "alternateNumberEntry",
    "supportOutDial",
    "maxCollaborateRoomParticipants"
})
public class UserCollaborateBridgeGetResponse20Sp1
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String bridgeId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String bridgeName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String phoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String extension;
    protected List<AlternateNumberEntry21> alternateNumberEntry;
    protected boolean supportOutDial;
    protected int maxCollaborateRoomParticipants;

    /**
     * Ruft den Wert der bridgeId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBridgeId() {
        return bridgeId;
    }

    /**
     * Legt den Wert der bridgeId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBridgeId(String value) {
        this.bridgeId = value;
    }

    /**
     * Ruft den Wert der bridgeName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBridgeName() {
        return bridgeName;
    }

    /**
     * Legt den Wert der bridgeName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBridgeName(String value) {
        this.bridgeName = value;
    }

    /**
     * Ruft den Wert der phoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Legt den Wert der phoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber(String value) {
        this.phoneNumber = value;
    }

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtension(String value) {
        this.extension = value;
    }

    /**
     * Gets the value of the alternateNumberEntry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the alternateNumberEntry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternateNumberEntry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternateNumberEntry21 }
     * 
     * 
     * @return
     *     The value of the alternateNumberEntry property.
     */
    public List<AlternateNumberEntry21> getAlternateNumberEntry() {
        if (alternateNumberEntry == null) {
            alternateNumberEntry = new ArrayList<>();
        }
        return this.alternateNumberEntry;
    }

    /**
     * Ruft den Wert der supportOutDial-Eigenschaft ab.
     * 
     */
    public boolean isSupportOutDial() {
        return supportOutDial;
    }

    /**
     * Legt den Wert der supportOutDial-Eigenschaft fest.
     * 
     */
    public void setSupportOutDial(boolean value) {
        this.supportOutDial = value;
    }

    /**
     * Ruft den Wert der maxCollaborateRoomParticipants-Eigenschaft ab.
     * 
     */
    public int getMaxCollaborateRoomParticipants() {
        return maxCollaborateRoomParticipants;
    }

    /**
     * Legt den Wert der maxCollaborateRoomParticipants-Eigenschaft fest.
     * 
     */
    public void setMaxCollaborateRoomParticipants(int value) {
        this.maxCollaborateRoomParticipants = value;
    }

}
