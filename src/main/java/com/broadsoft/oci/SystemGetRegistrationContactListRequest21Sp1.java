//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Get the list of registration contacts.
 *         This request handles all levels of administration privileges.  The content of the response will only contain 
 *         items within the scope of the requester's login id.  At the system level any of the choice parameters may be 
 *         specified to filter the registrations listed.  At the reseller level, the resellerId must be specified. 
 *         ResellerId is not valid at service provider, group or user level. At the service provider level the 
 *         serviceProviderId must be specified for the service provider and group options.  When using the userId or 
 *         linePort options the specified value must be valid for that service provider login.  At the group level 
 *         the servicProviderId and the groupId must be specified for the group option. When using the userId or linePort 
 *         options the specified value must be valid for that group login.  
 *         The serviceProviderId option is not valid at the group level.  At the user level when using the userId or 
 *         linePort options the specified value must be valid for that user login.  The serviceProviderId and groupId 
 *         options are not valid at the user level.
 *         The response is either SystemGetRegistrationContactListResponse21sp1 or ErrorResponse.
 *         The RegistrationEndpointType21sp1 is sent in response The Endpoint Type column contains one of the enumerated RegistrationEndpointType21sp1 values.
 *         The value Mobility in Endpoint Type column is only applicable in AS data mode.  
 *         
 *         The following elements are only used in AS data mode:
 *           resellerId
 *       
 * 
 * <p>Java-Klasse für SystemGetRegistrationContactListRequest21sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGetRegistrationContactListRequest21sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <choice minOccurs="0">
 *           <element name="resellerId" type="{}ResellerId22"/>
 *           <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *           <sequence>
 *             <element name="svcProviderId" type="{}ServiceProviderId"/>
 *             <element name="groupId" type="{}GroupId"/>
 *           </sequence>
 *           <element name="userId" type="{}UserId"/>
 *           <element name="linePort" type="{}AccessDeviceEndpointLinePort"/>
 *         </choice>
 *         <element name="deviceLevel" type="{}AccessDeviceLevel" minOccurs="0"/>
 *         <element name="deviceName" type="{}AccessDeviceName" minOccurs="0"/>
 *         <element name="deviceType" type="{}AccessDeviceType" minOccurs="0"/>
 *         <element name="searchCriteriaRegistrationURI" type="{}SearchCriteriaRegistrationURI" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaSIPContact" type="{}SearchCriteriaSIPContact" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="endpointType" type="{}RegistrationEndpointType21sp1" minOccurs="0"/>
 *         <element name="expired" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGetRegistrationContactListRequest21sp1", propOrder = {
    "resellerId",
    "serviceProviderId",
    "svcProviderId",
    "groupId",
    "userId",
    "linePort",
    "deviceLevel",
    "deviceName",
    "deviceType",
    "searchCriteriaRegistrationURI",
    "searchCriteriaSIPContact",
    "endpointType",
    "expired"
})
public class SystemGetRegistrationContactListRequest21Sp1
    extends OCIRequest
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String svcProviderId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String linePort;
    @XmlSchemaType(name = "token")
    protected AccessDeviceLevel deviceLevel;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceType;
    protected List<SearchCriteriaRegistrationURI> searchCriteriaRegistrationURI;
    protected List<SearchCriteriaSIPContact> searchCriteriaSIPContact;
    @XmlSchemaType(name = "token")
    protected RegistrationEndpointType21Sp1 endpointType;
    protected Boolean expired;

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der svcProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSvcProviderId() {
        return svcProviderId;
    }

    /**
     * Legt den Wert der svcProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSvcProviderId(String value) {
        this.svcProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der linePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinePort() {
        return linePort;
    }

    /**
     * Legt den Wert der linePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinePort(String value) {
        this.linePort = value;
    }

    /**
     * Ruft den Wert der deviceLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceLevel }
     *     
     */
    public AccessDeviceLevel getDeviceLevel() {
        return deviceLevel;
    }

    /**
     * Legt den Wert der deviceLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceLevel }
     *     
     */
    public void setDeviceLevel(AccessDeviceLevel value) {
        this.deviceLevel = value;
    }

    /**
     * Ruft den Wert der deviceName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * Legt den Wert der deviceName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceName(String value) {
        this.deviceName = value;
    }

    /**
     * Ruft den Wert der deviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Legt den Wert der deviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceType(String value) {
        this.deviceType = value;
    }

    /**
     * Gets the value of the searchCriteriaRegistrationURI property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaRegistrationURI property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaRegistrationURI().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaRegistrationURI }
     * 
     * 
     * @return
     *     The value of the searchCriteriaRegistrationURI property.
     */
    public List<SearchCriteriaRegistrationURI> getSearchCriteriaRegistrationURI() {
        if (searchCriteriaRegistrationURI == null) {
            searchCriteriaRegistrationURI = new ArrayList<>();
        }
        return this.searchCriteriaRegistrationURI;
    }

    /**
     * Gets the value of the searchCriteriaSIPContact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaSIPContact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaSIPContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaSIPContact }
     * 
     * 
     * @return
     *     The value of the searchCriteriaSIPContact property.
     */
    public List<SearchCriteriaSIPContact> getSearchCriteriaSIPContact() {
        if (searchCriteriaSIPContact == null) {
            searchCriteriaSIPContact = new ArrayList<>();
        }
        return this.searchCriteriaSIPContact;
    }

    /**
     * Ruft den Wert der endpointType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationEndpointType21Sp1 }
     *     
     */
    public RegistrationEndpointType21Sp1 getEndpointType() {
        return endpointType;
    }

    /**
     * Legt den Wert der endpointType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationEndpointType21Sp1 }
     *     
     */
    public void setEndpointType(RegistrationEndpointType21Sp1 value) {
        this.endpointType = value;
    }

    /**
     * Ruft den Wert der expired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExpired() {
        return expired;
    }

    /**
     * Legt den Wert der expired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExpired(Boolean value) {
        this.expired = value;
    }

}
