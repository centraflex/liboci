//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the route point failover policy. 
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupRoutePointModifyFailoverPolicyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupRoutePointModifyFailoverPolicyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="enableFailoverSupport" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="externalSystem" type="{}RoutePointExternalSystem" minOccurs="0"/>
 *         <element name="failoverPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="perCallEnableFailoverSupport" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="perCallCallFailureTimeoutSeconds" type="{}RoutePointCallFailureTimeout" minOccurs="0"/>
 *         <element name="perCallOutboundCallFailureTimeoutSeconds" type="{}RoutePointCallFailureTimeout" minOccurs="0"/>
 *         <element name="perCallFailoverPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupRoutePointModifyFailoverPolicyRequest", propOrder = {
    "serviceUserId",
    "enableFailoverSupport",
    "externalSystem",
    "failoverPhoneNumber",
    "perCallEnableFailoverSupport",
    "perCallCallFailureTimeoutSeconds",
    "perCallOutboundCallFailureTimeoutSeconds",
    "perCallFailoverPhoneNumber"
})
public class GroupRoutePointModifyFailoverPolicyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected Boolean enableFailoverSupport;
    @XmlElementRef(name = "externalSystem", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externalSystem;
    @XmlElementRef(name = "failoverPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> failoverPhoneNumber;
    protected Boolean perCallEnableFailoverSupport;
    protected Integer perCallCallFailureTimeoutSeconds;
    protected Integer perCallOutboundCallFailureTimeoutSeconds;
    @XmlElementRef(name = "perCallFailoverPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> perCallFailoverPhoneNumber;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der enableFailoverSupport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableFailoverSupport() {
        return enableFailoverSupport;
    }

    /**
     * Legt den Wert der enableFailoverSupport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableFailoverSupport(Boolean value) {
        this.enableFailoverSupport = value;
    }

    /**
     * Ruft den Wert der externalSystem-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternalSystem() {
        return externalSystem;
    }

    /**
     * Legt den Wert der externalSystem-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternalSystem(JAXBElement<String> value) {
        this.externalSystem = value;
    }

    /**
     * Ruft den Wert der failoverPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFailoverPhoneNumber() {
        return failoverPhoneNumber;
    }

    /**
     * Legt den Wert der failoverPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFailoverPhoneNumber(JAXBElement<String> value) {
        this.failoverPhoneNumber = value;
    }

    /**
     * Ruft den Wert der perCallEnableFailoverSupport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPerCallEnableFailoverSupport() {
        return perCallEnableFailoverSupport;
    }

    /**
     * Legt den Wert der perCallEnableFailoverSupport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPerCallEnableFailoverSupport(Boolean value) {
        this.perCallEnableFailoverSupport = value;
    }

    /**
     * Ruft den Wert der perCallCallFailureTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPerCallCallFailureTimeoutSeconds() {
        return perCallCallFailureTimeoutSeconds;
    }

    /**
     * Legt den Wert der perCallCallFailureTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPerCallCallFailureTimeoutSeconds(Integer value) {
        this.perCallCallFailureTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der perCallOutboundCallFailureTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPerCallOutboundCallFailureTimeoutSeconds() {
        return perCallOutboundCallFailureTimeoutSeconds;
    }

    /**
     * Legt den Wert der perCallOutboundCallFailureTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPerCallOutboundCallFailureTimeoutSeconds(Integer value) {
        this.perCallOutboundCallFailureTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der perCallFailoverPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPerCallFailoverPhoneNumber() {
        return perCallFailoverPhoneNumber;
    }

    /**
     * Legt den Wert der perCallFailoverPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPerCallFailoverPhoneNumber(JAXBElement<String> value) {
        this.perCallFailoverPhoneNumber = value;
    }

}
