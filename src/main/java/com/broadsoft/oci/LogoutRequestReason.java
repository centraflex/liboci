//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für LogoutRequestReason.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="LogoutRequestReason">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Client Logout"/>
 *     <enumeration value="Server Connection Failure"/>
 *     <enumeration value="Open Client Server Forced Logout"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "LogoutRequestReason")
@XmlEnum
public enum LogoutRequestReason {

    @XmlEnumValue("Client Logout")
    CLIENT_LOGOUT("Client Logout"),
    @XmlEnumValue("Server Connection Failure")
    SERVER_CONNECTION_FAILURE("Server Connection Failure"),
    @XmlEnumValue("Open Client Server Forced Logout")
    OPEN_CLIENT_SERVER_FORCED_LOGOUT("Open Client Server Forced Logout");
    private final String value;

    LogoutRequestReason(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LogoutRequestReason fromValue(String v) {
        for (LogoutRequestReason c: LogoutRequestReason.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
