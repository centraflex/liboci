//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify Outbound Proxy Cache system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *         
 *         Note that an error is returned if CloudPBX is not licensed.
 *       
 * 
 * <p>Java-Klasse für SystemOutboundProxyCacheParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemOutboundProxyCacheParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="evictionTimeoutMinutes" type="{}OutboundProxyCacheEvictionTimeoutMinutes" minOccurs="0"/>
 *         <element name="refreshTimeoutMinutes" type="{}OutboundProxyCacheRefreshTimeoutMinutes" minOccurs="0"/>
 *         <element name="auditIntervalMinutes" type="{}OutboundProxyCacheAuditIntervalMinutes" minOccurs="0"/>
 *         <element name="maximumCacheSize" type="{}NonNegativeInt" minOccurs="0"/>
 *         <element name="dnsTypeDefaultValue" type="{}OutboundProxyCacheDefaultValue" minOccurs="0"/>
 *         <element name="useDnsSrvDefaultValue" type="{}OutboundProxyCacheDefaultValue" minOccurs="0"/>
 *         <element name="srvPrefixDefaultValue" type="{}OutboundProxyCacheDefaultValue" minOccurs="0"/>
 *         <element name="outboundProxyDefaultValue" type="{}OutboundProxyCacheDefaultValue" minOccurs="0"/>
 *         <element name="transportTypeDefaultValue" type="{}OutboundProxyCacheDefaultValue" minOccurs="0"/>
 *         <element name="secureRtpDefaultValue" type="{}OutboundProxyCacheDefaultValue" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemOutboundProxyCacheParametersModifyRequest", propOrder = {
    "evictionTimeoutMinutes",
    "refreshTimeoutMinutes",
    "auditIntervalMinutes",
    "maximumCacheSize",
    "dnsTypeDefaultValue",
    "useDnsSrvDefaultValue",
    "srvPrefixDefaultValue",
    "outboundProxyDefaultValue",
    "transportTypeDefaultValue",
    "secureRtpDefaultValue"
})
public class SystemOutboundProxyCacheParametersModifyRequest
    extends OCIRequest
{

    protected Integer evictionTimeoutMinutes;
    protected Integer refreshTimeoutMinutes;
    protected Integer auditIntervalMinutes;
    protected Integer maximumCacheSize;
    @XmlElementRef(name = "dnsTypeDefaultValue", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dnsTypeDefaultValue;
    @XmlElementRef(name = "useDnsSrvDefaultValue", type = JAXBElement.class, required = false)
    protected JAXBElement<String> useDnsSrvDefaultValue;
    @XmlElementRef(name = "srvPrefixDefaultValue", type = JAXBElement.class, required = false)
    protected JAXBElement<String> srvPrefixDefaultValue;
    @XmlElementRef(name = "outboundProxyDefaultValue", type = JAXBElement.class, required = false)
    protected JAXBElement<String> outboundProxyDefaultValue;
    @XmlElementRef(name = "transportTypeDefaultValue", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transportTypeDefaultValue;
    @XmlElementRef(name = "secureRtpDefaultValue", type = JAXBElement.class, required = false)
    protected JAXBElement<String> secureRtpDefaultValue;

    /**
     * Ruft den Wert der evictionTimeoutMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEvictionTimeoutMinutes() {
        return evictionTimeoutMinutes;
    }

    /**
     * Legt den Wert der evictionTimeoutMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEvictionTimeoutMinutes(Integer value) {
        this.evictionTimeoutMinutes = value;
    }

    /**
     * Ruft den Wert der refreshTimeoutMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRefreshTimeoutMinutes() {
        return refreshTimeoutMinutes;
    }

    /**
     * Legt den Wert der refreshTimeoutMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRefreshTimeoutMinutes(Integer value) {
        this.refreshTimeoutMinutes = value;
    }

    /**
     * Ruft den Wert der auditIntervalMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAuditIntervalMinutes() {
        return auditIntervalMinutes;
    }

    /**
     * Legt den Wert der auditIntervalMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAuditIntervalMinutes(Integer value) {
        this.auditIntervalMinutes = value;
    }

    /**
     * Ruft den Wert der maximumCacheSize-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaximumCacheSize() {
        return maximumCacheSize;
    }

    /**
     * Legt den Wert der maximumCacheSize-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaximumCacheSize(Integer value) {
        this.maximumCacheSize = value;
    }

    /**
     * Ruft den Wert der dnsTypeDefaultValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDnsTypeDefaultValue() {
        return dnsTypeDefaultValue;
    }

    /**
     * Legt den Wert der dnsTypeDefaultValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDnsTypeDefaultValue(JAXBElement<String> value) {
        this.dnsTypeDefaultValue = value;
    }

    /**
     * Ruft den Wert der useDnsSrvDefaultValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUseDnsSrvDefaultValue() {
        return useDnsSrvDefaultValue;
    }

    /**
     * Legt den Wert der useDnsSrvDefaultValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUseDnsSrvDefaultValue(JAXBElement<String> value) {
        this.useDnsSrvDefaultValue = value;
    }

    /**
     * Ruft den Wert der srvPrefixDefaultValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSrvPrefixDefaultValue() {
        return srvPrefixDefaultValue;
    }

    /**
     * Legt den Wert der srvPrefixDefaultValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSrvPrefixDefaultValue(JAXBElement<String> value) {
        this.srvPrefixDefaultValue = value;
    }

    /**
     * Ruft den Wert der outboundProxyDefaultValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOutboundProxyDefaultValue() {
        return outboundProxyDefaultValue;
    }

    /**
     * Legt den Wert der outboundProxyDefaultValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOutboundProxyDefaultValue(JAXBElement<String> value) {
        this.outboundProxyDefaultValue = value;
    }

    /**
     * Ruft den Wert der transportTypeDefaultValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransportTypeDefaultValue() {
        return transportTypeDefaultValue;
    }

    /**
     * Legt den Wert der transportTypeDefaultValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransportTypeDefaultValue(JAXBElement<String> value) {
        this.transportTypeDefaultValue = value;
    }

    /**
     * Ruft den Wert der secureRtpDefaultValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSecureRtpDefaultValue() {
        return secureRtpDefaultValue;
    }

    /**
     * Legt den Wert der secureRtpDefaultValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSecureRtpDefaultValue(JAXBElement<String> value) {
        this.secureRtpDefaultValue = value;
    }

}
