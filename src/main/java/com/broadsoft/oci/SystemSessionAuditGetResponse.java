//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemSessionAuditGetRequest.
 *         Replaced By: SystemSessionAuditGetResponse14sp3
 *       
 * 
 * <p>Java-Klasse für SystemSessionAuditGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSessionAuditGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="intervalSeconds" type="{}SessionAuditIntervalSeconds"/>
 *         <element name="timeoutPeriodSeconds" type="{}SessionAuditTimeoutPeriodSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSessionAuditGetResponse", propOrder = {
    "isActive",
    "intervalSeconds",
    "timeoutPeriodSeconds"
})
public class SystemSessionAuditGetResponse
    extends OCIDataResponse
{

    protected boolean isActive;
    protected int intervalSeconds;
    protected int timeoutPeriodSeconds;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der intervalSeconds-Eigenschaft ab.
     * 
     */
    public int getIntervalSeconds() {
        return intervalSeconds;
    }

    /**
     * Legt den Wert der intervalSeconds-Eigenschaft fest.
     * 
     */
    public void setIntervalSeconds(int value) {
        this.intervalSeconds = value;
    }

    /**
     * Ruft den Wert der timeoutPeriodSeconds-Eigenschaft ab.
     * 
     */
    public int getTimeoutPeriodSeconds() {
        return timeoutPeriodSeconds;
    }

    /**
     * Legt den Wert der timeoutPeriodSeconds-Eigenschaft fest.
     * 
     */
    public void setTimeoutPeriodSeconds(int value) {
        this.timeoutPeriodSeconds = value;
    }

}
