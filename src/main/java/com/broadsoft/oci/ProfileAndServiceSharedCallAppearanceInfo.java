//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         This is the configuration parameters for shared call appearance service
 *         The endpointTable contains columns:
 *           "Device Level", "Device Name", "Device Type", "Line/Port", "SIP Contact", "Port Number". "Private Identity" .        
 *           
 *         The "Device Level" column contains one of the AccessDeviceLevel enumerated constants.
 *         Port numbers are only used by devices with static line ordering.       
 *       
 * 
 * <p>Java-Klasse für ProfileAndServiceSharedCallAppearanceInfo complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ProfileAndServiceSharedCallAppearanceInfo">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="alertAllAppearancesForClickToDialCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="alertAllAppearancesForGroupPagingCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxAppearances" type="{}SharedCallAppearanceMaximumAppearances"/>
 *         <element name="allowSCACallRetrieve" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableMultipleCallArrangement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="multipleCallArrangementIsActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="endpointTable" type="{C}OCITable"/>
 *         <element name="allowBridgingBetweenLocations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="bridgeWarningTone" type="{}SharedCallAppearanceBridgeWarningTone"/>
 *         <element name="enableCallParkNotification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useUserPrimaryWithAlternateCallsSetting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowSimultaneousPrimaryAndAlternate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="restrictCallRetrieveOfPrimary" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="restrictCallBridgingOfPrimary" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfileAndServiceSharedCallAppearanceInfo", propOrder = {
    "alertAllAppearancesForClickToDialCalls",
    "alertAllAppearancesForGroupPagingCalls",
    "maxAppearances",
    "allowSCACallRetrieve",
    "enableMultipleCallArrangement",
    "multipleCallArrangementIsActive",
    "endpointTable",
    "allowBridgingBetweenLocations",
    "bridgeWarningTone",
    "enableCallParkNotification",
    "useUserPrimaryWithAlternateCallsSetting",
    "allowSimultaneousPrimaryAndAlternate",
    "restrictCallRetrieveOfPrimary",
    "restrictCallBridgingOfPrimary"
})
public class ProfileAndServiceSharedCallAppearanceInfo {

    protected boolean alertAllAppearancesForClickToDialCalls;
    protected boolean alertAllAppearancesForGroupPagingCalls;
    protected int maxAppearances;
    protected boolean allowSCACallRetrieve;
    protected boolean enableMultipleCallArrangement;
    protected boolean multipleCallArrangementIsActive;
    @XmlElement(required = true)
    protected OCITable endpointTable;
    protected boolean allowBridgingBetweenLocations;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SharedCallAppearanceBridgeWarningTone bridgeWarningTone;
    protected boolean enableCallParkNotification;
    protected boolean useUserPrimaryWithAlternateCallsSetting;
    protected boolean allowSimultaneousPrimaryAndAlternate;
    protected boolean restrictCallRetrieveOfPrimary;
    protected boolean restrictCallBridgingOfPrimary;

    /**
     * Ruft den Wert der alertAllAppearancesForClickToDialCalls-Eigenschaft ab.
     * 
     */
    public boolean isAlertAllAppearancesForClickToDialCalls() {
        return alertAllAppearancesForClickToDialCalls;
    }

    /**
     * Legt den Wert der alertAllAppearancesForClickToDialCalls-Eigenschaft fest.
     * 
     */
    public void setAlertAllAppearancesForClickToDialCalls(boolean value) {
        this.alertAllAppearancesForClickToDialCalls = value;
    }

    /**
     * Ruft den Wert der alertAllAppearancesForGroupPagingCalls-Eigenschaft ab.
     * 
     */
    public boolean isAlertAllAppearancesForGroupPagingCalls() {
        return alertAllAppearancesForGroupPagingCalls;
    }

    /**
     * Legt den Wert der alertAllAppearancesForGroupPagingCalls-Eigenschaft fest.
     * 
     */
    public void setAlertAllAppearancesForGroupPagingCalls(boolean value) {
        this.alertAllAppearancesForGroupPagingCalls = value;
    }

    /**
     * Ruft den Wert der maxAppearances-Eigenschaft ab.
     * 
     */
    public int getMaxAppearances() {
        return maxAppearances;
    }

    /**
     * Legt den Wert der maxAppearances-Eigenschaft fest.
     * 
     */
    public void setMaxAppearances(int value) {
        this.maxAppearances = value;
    }

    /**
     * Ruft den Wert der allowSCACallRetrieve-Eigenschaft ab.
     * 
     */
    public boolean isAllowSCACallRetrieve() {
        return allowSCACallRetrieve;
    }

    /**
     * Legt den Wert der allowSCACallRetrieve-Eigenschaft fest.
     * 
     */
    public void setAllowSCACallRetrieve(boolean value) {
        this.allowSCACallRetrieve = value;
    }

    /**
     * Ruft den Wert der enableMultipleCallArrangement-Eigenschaft ab.
     * 
     */
    public boolean isEnableMultipleCallArrangement() {
        return enableMultipleCallArrangement;
    }

    /**
     * Legt den Wert der enableMultipleCallArrangement-Eigenschaft fest.
     * 
     */
    public void setEnableMultipleCallArrangement(boolean value) {
        this.enableMultipleCallArrangement = value;
    }

    /**
     * Ruft den Wert der multipleCallArrangementIsActive-Eigenschaft ab.
     * 
     */
    public boolean isMultipleCallArrangementIsActive() {
        return multipleCallArrangementIsActive;
    }

    /**
     * Legt den Wert der multipleCallArrangementIsActive-Eigenschaft fest.
     * 
     */
    public void setMultipleCallArrangementIsActive(boolean value) {
        this.multipleCallArrangementIsActive = value;
    }

    /**
     * Ruft den Wert der endpointTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getEndpointTable() {
        return endpointTable;
    }

    /**
     * Legt den Wert der endpointTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setEndpointTable(OCITable value) {
        this.endpointTable = value;
    }

    /**
     * Ruft den Wert der allowBridgingBetweenLocations-Eigenschaft ab.
     * 
     */
    public boolean isAllowBridgingBetweenLocations() {
        return allowBridgingBetweenLocations;
    }

    /**
     * Legt den Wert der allowBridgingBetweenLocations-Eigenschaft fest.
     * 
     */
    public void setAllowBridgingBetweenLocations(boolean value) {
        this.allowBridgingBetweenLocations = value;
    }

    /**
     * Ruft den Wert der bridgeWarningTone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SharedCallAppearanceBridgeWarningTone }
     *     
     */
    public SharedCallAppearanceBridgeWarningTone getBridgeWarningTone() {
        return bridgeWarningTone;
    }

    /**
     * Legt den Wert der bridgeWarningTone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SharedCallAppearanceBridgeWarningTone }
     *     
     */
    public void setBridgeWarningTone(SharedCallAppearanceBridgeWarningTone value) {
        this.bridgeWarningTone = value;
    }

    /**
     * Ruft den Wert der enableCallParkNotification-Eigenschaft ab.
     * 
     */
    public boolean isEnableCallParkNotification() {
        return enableCallParkNotification;
    }

    /**
     * Legt den Wert der enableCallParkNotification-Eigenschaft fest.
     * 
     */
    public void setEnableCallParkNotification(boolean value) {
        this.enableCallParkNotification = value;
    }

    /**
     * Ruft den Wert der useUserPrimaryWithAlternateCallsSetting-Eigenschaft ab.
     * 
     */
    public boolean isUseUserPrimaryWithAlternateCallsSetting() {
        return useUserPrimaryWithAlternateCallsSetting;
    }

    /**
     * Legt den Wert der useUserPrimaryWithAlternateCallsSetting-Eigenschaft fest.
     * 
     */
    public void setUseUserPrimaryWithAlternateCallsSetting(boolean value) {
        this.useUserPrimaryWithAlternateCallsSetting = value;
    }

    /**
     * Ruft den Wert der allowSimultaneousPrimaryAndAlternate-Eigenschaft ab.
     * 
     */
    public boolean isAllowSimultaneousPrimaryAndAlternate() {
        return allowSimultaneousPrimaryAndAlternate;
    }

    /**
     * Legt den Wert der allowSimultaneousPrimaryAndAlternate-Eigenschaft fest.
     * 
     */
    public void setAllowSimultaneousPrimaryAndAlternate(boolean value) {
        this.allowSimultaneousPrimaryAndAlternate = value;
    }

    /**
     * Ruft den Wert der restrictCallRetrieveOfPrimary-Eigenschaft ab.
     * 
     */
    public boolean isRestrictCallRetrieveOfPrimary() {
        return restrictCallRetrieveOfPrimary;
    }

    /**
     * Legt den Wert der restrictCallRetrieveOfPrimary-Eigenschaft fest.
     * 
     */
    public void setRestrictCallRetrieveOfPrimary(boolean value) {
        this.restrictCallRetrieveOfPrimary = value;
    }

    /**
     * Ruft den Wert der restrictCallBridgingOfPrimary-Eigenschaft ab.
     * 
     */
    public boolean isRestrictCallBridgingOfPrimary() {
        return restrictCallBridgingOfPrimary;
    }

    /**
     * Legt den Wert der restrictCallBridgingOfPrimary-Eigenschaft fest.
     * 
     */
    public void setRestrictCallBridgingOfPrimary(boolean value) {
        this.restrictCallBridgingOfPrimary = value;
    }

}
