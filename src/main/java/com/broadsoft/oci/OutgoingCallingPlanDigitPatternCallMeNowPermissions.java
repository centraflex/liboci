//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Outgoing Calling Plan Call Me Now call permissions for specified digit patterns.
 *       
 * 
 * <p>Java-Klasse für OutgoingCallingPlanDigitPatternCallMeNowPermissions complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OutgoingCallingPlanDigitPatternCallMeNowPermissions">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="digitPatternPermissions" type="{}OutgoingCallingPlanDigitPatternCallMeNowPermission" maxOccurs="unbounded"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingCallingPlanDigitPatternCallMeNowPermissions", propOrder = {
    "digitPatternPermissions"
})
public class OutgoingCallingPlanDigitPatternCallMeNowPermissions {

    @XmlElement(required = true)
    protected List<OutgoingCallingPlanDigitPatternCallMeNowPermission> digitPatternPermissions;

    /**
     * Gets the value of the digitPatternPermissions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the digitPatternPermissions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDigitPatternPermissions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OutgoingCallingPlanDigitPatternCallMeNowPermission }
     * 
     * 
     * @return
     *     The value of the digitPatternPermissions property.
     */
    public List<OutgoingCallingPlanDigitPatternCallMeNowPermission> getDigitPatternPermissions() {
        if (digitPatternPermissions == null) {
            digitPatternPermissions = new ArrayList<>();
        }
        return this.digitPatternPermissions;
    }

}
