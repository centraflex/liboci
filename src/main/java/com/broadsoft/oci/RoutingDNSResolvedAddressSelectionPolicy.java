//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für RoutingDNSResolvedAddressSelectionPolicy.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="RoutingDNSResolvedAddressSelectionPolicy">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Standard"/>
 *     <enumeration value="Load Balanced"/>
 *     <enumeration value="Stateful"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "RoutingDNSResolvedAddressSelectionPolicy")
@XmlEnum
public enum RoutingDNSResolvedAddressSelectionPolicy {

    @XmlEnumValue("Standard")
    STANDARD("Standard"),
    @XmlEnumValue("Load Balanced")
    LOAD_BALANCED("Load Balanced"),
    @XmlEnumValue("Stateful")
    STATEFUL("Stateful");
    private final String value;

    RoutingDNSResolvedAddressSelectionPolicy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RoutingDNSResolvedAddressSelectionPolicy fromValue(String v) {
        for (RoutingDNSResolvedAddressSelectionPolicy c: RoutingDNSResolvedAddressSelectionPolicy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
