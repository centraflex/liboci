//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal change extended away greeting menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für ChangeExtendedAwayGreetingMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ChangeExtendedAwayGreetingMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="activateExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="deactivateExtendedAwayGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="recordNewGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="listenToCurrentGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="enableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
 *         <element name="disableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeExtendedAwayGreetingMenuKeysModifyEntry", propOrder = {
    "activateExtendedAwayGreeting",
    "deactivateExtendedAwayGreeting",
    "recordNewGreeting",
    "listenToCurrentGreeting",
    "enableMessageDeposit",
    "disableMessageDeposit",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class ChangeExtendedAwayGreetingMenuKeysModifyEntry {

    @XmlElementRef(name = "activateExtendedAwayGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> activateExtendedAwayGreeting;
    @XmlElementRef(name = "deactivateExtendedAwayGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deactivateExtendedAwayGreeting;
    @XmlElementRef(name = "recordNewGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recordNewGreeting;
    @XmlElementRef(name = "listenToCurrentGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> listenToCurrentGreeting;
    @XmlElementRef(name = "enableMessageDeposit", type = JAXBElement.class, required = false)
    protected JAXBElement<String> enableMessageDeposit;
    @XmlElementRef(name = "disableMessageDeposit", type = JAXBElement.class, required = false)
    protected JAXBElement<String> disableMessageDeposit;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repeatMenu;

    /**
     * Ruft den Wert der activateExtendedAwayGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActivateExtendedAwayGreeting() {
        return activateExtendedAwayGreeting;
    }

    /**
     * Legt den Wert der activateExtendedAwayGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActivateExtendedAwayGreeting(JAXBElement<String> value) {
        this.activateExtendedAwayGreeting = value;
    }

    /**
     * Ruft den Wert der deactivateExtendedAwayGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeactivateExtendedAwayGreeting() {
        return deactivateExtendedAwayGreeting;
    }

    /**
     * Legt den Wert der deactivateExtendedAwayGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeactivateExtendedAwayGreeting(JAXBElement<String> value) {
        this.deactivateExtendedAwayGreeting = value;
    }

    /**
     * Ruft den Wert der recordNewGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecordNewGreeting() {
        return recordNewGreeting;
    }

    /**
     * Legt den Wert der recordNewGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecordNewGreeting(JAXBElement<String> value) {
        this.recordNewGreeting = value;
    }

    /**
     * Ruft den Wert der listenToCurrentGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getListenToCurrentGreeting() {
        return listenToCurrentGreeting;
    }

    /**
     * Legt den Wert der listenToCurrentGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setListenToCurrentGreeting(JAXBElement<String> value) {
        this.listenToCurrentGreeting = value;
    }

    /**
     * Ruft den Wert der enableMessageDeposit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEnableMessageDeposit() {
        return enableMessageDeposit;
    }

    /**
     * Legt den Wert der enableMessageDeposit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEnableMessageDeposit(JAXBElement<String> value) {
        this.enableMessageDeposit = value;
    }

    /**
     * Ruft den Wert der disableMessageDeposit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDisableMessageDeposit() {
        return disableMessageDeposit;
    }

    /**
     * Legt den Wert der disableMessageDeposit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDisableMessageDeposit(JAXBElement<String> value) {
        this.disableMessageDeposit = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepeatMenu(JAXBElement<String> value) {
        this.repeatMenu = value;
    }

}
