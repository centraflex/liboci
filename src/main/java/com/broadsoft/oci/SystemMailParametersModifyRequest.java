//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify Mail system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemMailParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemMailParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="primaryServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="secondaryServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="defaultFromAddress" type="{}SMTPFromAddress" minOccurs="0"/>
 *         <element name="defaultSubject" type="{}SMTPSubject" minOccurs="0"/>
 *         <element name="supportDNSSRVForMailServerAccess" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="secureMode" type="{}SMTPSecureMode" minOccurs="0"/>
 *         <element name="port" type="{}Port" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemMailParametersModifyRequest", propOrder = {
    "primaryServerNetAddress",
    "secondaryServerNetAddress",
    "defaultFromAddress",
    "defaultSubject",
    "supportDNSSRVForMailServerAccess",
    "secureMode",
    "port"
})
public class SystemMailParametersModifyRequest
    extends OCIRequest
{

    @XmlElementRef(name = "primaryServerNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> primaryServerNetAddress;
    @XmlElementRef(name = "secondaryServerNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> secondaryServerNetAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultFromAddress;
    @XmlElementRef(name = "defaultSubject", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultSubject;
    protected Boolean supportDNSSRVForMailServerAccess;
    @XmlSchemaType(name = "token")
    protected SMTPSecureMode secureMode;
    @XmlElementRef(name = "port", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> port;

    /**
     * Ruft den Wert der primaryServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrimaryServerNetAddress() {
        return primaryServerNetAddress;
    }

    /**
     * Legt den Wert der primaryServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrimaryServerNetAddress(JAXBElement<String> value) {
        this.primaryServerNetAddress = value;
    }

    /**
     * Ruft den Wert der secondaryServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSecondaryServerNetAddress() {
        return secondaryServerNetAddress;
    }

    /**
     * Legt den Wert der secondaryServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSecondaryServerNetAddress(JAXBElement<String> value) {
        this.secondaryServerNetAddress = value;
    }

    /**
     * Ruft den Wert der defaultFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultFromAddress() {
        return defaultFromAddress;
    }

    /**
     * Legt den Wert der defaultFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultFromAddress(String value) {
        this.defaultFromAddress = value;
    }

    /**
     * Ruft den Wert der defaultSubject-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultSubject() {
        return defaultSubject;
    }

    /**
     * Legt den Wert der defaultSubject-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultSubject(JAXBElement<String> value) {
        this.defaultSubject = value;
    }

    /**
     * Ruft den Wert der supportDNSSRVForMailServerAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportDNSSRVForMailServerAccess() {
        return supportDNSSRVForMailServerAccess;
    }

    /**
     * Legt den Wert der supportDNSSRVForMailServerAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportDNSSRVForMailServerAccess(Boolean value) {
        this.supportDNSSRVForMailServerAccess = value;
    }

    /**
     * Ruft den Wert der secureMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SMTPSecureMode }
     *     
     */
    public SMTPSecureMode getSecureMode() {
        return secureMode;
    }

    /**
     * Legt den Wert der secureMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SMTPSecureMode }
     *     
     */
    public void setSecureMode(SMTPSecureMode value) {
        this.secureMode = value;
    }

    /**
     * Ruft den Wert der port-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getPort() {
        return port;
    }

    /**
     * Legt den Wert der port-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setPort(JAXBElement<Integer> value) {
        this.port = value;
    }

}
