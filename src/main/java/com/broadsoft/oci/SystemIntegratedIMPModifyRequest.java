//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system Integrated IMP service attributes.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         The following elements are only used in AS data mode:
 *           boshURL
 *           allowImpPasswordRetrieval
 *       
 * 
 * <p>Java-Klasse für SystemIntegratedIMPModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemIntegratedIMPModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceDomain" type="{}DomainName" minOccurs="0"/>
 *         <element name="servicePort" type="{}Port" minOccurs="0"/>
 *         <element name="addServiceProviderInIMPUserId" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="boshURL" type="{}URL" minOccurs="0"/>
 *         <element name="allowImpPasswordRetrieval" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemIntegratedIMPModifyRequest", propOrder = {
    "serviceDomain",
    "servicePort",
    "addServiceProviderInIMPUserId",
    "boshURL",
    "allowImpPasswordRetrieval"
})
public class SystemIntegratedIMPModifyRequest
    extends OCIRequest
{

    @XmlElementRef(name = "serviceDomain", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serviceDomain;
    @XmlElementRef(name = "servicePort", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> servicePort;
    protected Boolean addServiceProviderInIMPUserId;
    @XmlElementRef(name = "boshURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> boshURL;
    protected Boolean allowImpPasswordRetrieval;

    /**
     * Ruft den Wert der serviceDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServiceDomain() {
        return serviceDomain;
    }

    /**
     * Legt den Wert der serviceDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServiceDomain(JAXBElement<String> value) {
        this.serviceDomain = value;
    }

    /**
     * Ruft den Wert der servicePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getServicePort() {
        return servicePort;
    }

    /**
     * Legt den Wert der servicePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setServicePort(JAXBElement<Integer> value) {
        this.servicePort = value;
    }

    /**
     * Ruft den Wert der addServiceProviderInIMPUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAddServiceProviderInIMPUserId() {
        return addServiceProviderInIMPUserId;
    }

    /**
     * Legt den Wert der addServiceProviderInIMPUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAddServiceProviderInIMPUserId(Boolean value) {
        this.addServiceProviderInIMPUserId = value;
    }

    /**
     * Ruft den Wert der boshURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBoshURL() {
        return boshURL;
    }

    /**
     * Legt den Wert der boshURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBoshURL(JAXBElement<String> value) {
        this.boshURL = value;
    }

    /**
     * Ruft den Wert der allowImpPasswordRetrieval-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowImpPasswordRetrieval() {
        return allowImpPasswordRetrieval;
    }

    /**
     * Legt den Wert der allowImpPasswordRetrieval-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowImpPasswordRetrieval(Boolean value) {
        this.allowImpPasswordRetrieval = value;
    }

}
