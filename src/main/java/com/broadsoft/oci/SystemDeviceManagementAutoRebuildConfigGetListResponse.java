//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemDeviceManagementAutoRebuildConfigGetListRequest.
 *         Contains a table with column headings: "OCI Request Prefix", "Auto Rebuild Enabled".
 *         "OCI Request Prefix" is the prefix of the OCI request name.  It does
 *         not include the request's version '17.sp4, 18...' since the disabled
 *         flag applies to the whole series of requests, independent of the
 *         version.
 *         "Auto Rebuild Enabled": 'True' if the request prefix triggers DM events
 *         automatically.  Otherwise, automatic DM events are not generated.
 *       
 * 
 * <p>Java-Klasse für SystemDeviceManagementAutoRebuildConfigGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceManagementAutoRebuildConfigGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="autoRebuildConfigTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceManagementAutoRebuildConfigGetListResponse", propOrder = {
    "autoRebuildConfigTable"
})
public class SystemDeviceManagementAutoRebuildConfigGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable autoRebuildConfigTable;

    /**
     * Ruft den Wert der autoRebuildConfigTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAutoRebuildConfigTable() {
        return autoRebuildConfigTable;
    }

    /**
     * Legt den Wert der autoRebuildConfigTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAutoRebuildConfigTable(OCITable value) {
        this.autoRebuildConfigTable = value;
    }

}
