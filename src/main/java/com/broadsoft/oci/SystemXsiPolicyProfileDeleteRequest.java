//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Delete an entry from system Xsi application Id list.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemXsiPolicyProfileDeleteRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemXsiPolicyProfileDeleteRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="xsiPolicyProfile" type="{}XsiPolicyProfileKey"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemXsiPolicyProfileDeleteRequest", propOrder = {
    "xsiPolicyProfile"
})
public class SystemXsiPolicyProfileDeleteRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    protected XsiPolicyProfileKey xsiPolicyProfile;

    /**
     * Ruft den Wert der xsiPolicyProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XsiPolicyProfileKey }
     *     
     */
    public XsiPolicyProfileKey getXsiPolicyProfile() {
        return xsiPolicyProfile;
    }

    /**
     * Legt den Wert der xsiPolicyProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XsiPolicyProfileKey }
     *     
     */
    public void setXsiPolicyProfile(XsiPolicyProfileKey value) {
        this.xsiPolicyProfile = value;
    }

}
