//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the UserBroadWorksAnywhereGetPhoneNumberRequest. 
 *         The criteria table's column headings are: "Is Active", "Criteria Name", "Time Schedule", 
 *         "Calls From", "Blacklisted", "Holiday Schedule", "Calls To Type", "Calls To Number" and "Calls To Extension".
 *         The following columns are only returned in AS data mode:       
 *           "Calls To Type", "Calls To Number" and "Calls To Extension"
 *           
 *         The possible values for the "Calls To Type" column are the following or a combination of them separated by comma:
 *           - Primary
 *           - Alternate X (where x is a number between 1 and 10)
 *           - Mobility        
 *         The possible values for the "Calls To Number" column are the following or a combination of them separated by comma:
 *           - The value of the phone number for the corresponding Calls To Type, when the number is available. i.e. Alternate 1 may have extension, but no number.
 *           - When no number is available a blank space is provided instead.
 *         The possible values for the "Calls To Extension" column are the following or a caombination of them separated by comma:
 *           - The value of the extension for the corresponding Calls To Type, when the extension is available. i.e. Primary may have number, but no extension.
 *           - For Mobility Calls To Type, this is always blank.
 *           - When no exension is available a blank space is provided instead.
 *       
 * 
 * <p>Java-Klasse für UserBroadWorksAnywhereGetPhoneNumberResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksAnywhereGetPhoneNumberResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="description" type="{}BroadWorksAnywherePhoneNumberDescription" minOccurs="0"/>
 *         <element name="outboundAlternateNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="broadworksCallControl" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useDiversionInhibitor" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="answerConfirmationRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="criteriaTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksAnywhereGetPhoneNumberResponse", propOrder = {
    "description",
    "outboundAlternateNumber",
    "isActive",
    "broadworksCallControl",
    "useDiversionInhibitor",
    "answerConfirmationRequired",
    "criteriaTable"
})
public class UserBroadWorksAnywhereGetPhoneNumberResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String outboundAlternateNumber;
    protected boolean isActive;
    protected boolean broadworksCallControl;
    protected boolean useDiversionInhibitor;
    protected boolean answerConfirmationRequired;
    @XmlElement(required = true)
    protected OCITable criteriaTable;

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der outboundAlternateNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutboundAlternateNumber() {
        return outboundAlternateNumber;
    }

    /**
     * Legt den Wert der outboundAlternateNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutboundAlternateNumber(String value) {
        this.outboundAlternateNumber = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der broadworksCallControl-Eigenschaft ab.
     * 
     */
    public boolean isBroadworksCallControl() {
        return broadworksCallControl;
    }

    /**
     * Legt den Wert der broadworksCallControl-Eigenschaft fest.
     * 
     */
    public void setBroadworksCallControl(boolean value) {
        this.broadworksCallControl = value;
    }

    /**
     * Ruft den Wert der useDiversionInhibitor-Eigenschaft ab.
     * 
     */
    public boolean isUseDiversionInhibitor() {
        return useDiversionInhibitor;
    }

    /**
     * Legt den Wert der useDiversionInhibitor-Eigenschaft fest.
     * 
     */
    public void setUseDiversionInhibitor(boolean value) {
        this.useDiversionInhibitor = value;
    }

    /**
     * Ruft den Wert der answerConfirmationRequired-Eigenschaft ab.
     * 
     */
    public boolean isAnswerConfirmationRequired() {
        return answerConfirmationRequired;
    }

    /**
     * Legt den Wert der answerConfirmationRequired-Eigenschaft fest.
     * 
     */
    public void setAnswerConfirmationRequired(boolean value) {
        this.answerConfirmationRequired = value;
    }

    /**
     * Ruft den Wert der criteriaTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getCriteriaTable() {
        return criteriaTable;
    }

    /**
     * Legt den Wert der criteriaTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setCriteriaTable(OCITable value) {
        this.criteriaTable = value;
    }

}
