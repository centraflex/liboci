//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemExternalEmergencyRoutingParametersGetRequest13mp13.
 *         Contains a list of system External Emergency Routing parameters.
 *       
 * 
 * <p>Java-Klasse für SystemExternalEmergencyRoutingParametersGetResponse13mp13 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemExternalEmergencyRoutingParametersGetResponse13mp13">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceURI" type="{}NetAddress" minOccurs="0"/>
 *         <element name="defaultEmergencyNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="supportsDNSSRV" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="connectionTimeoutSeconds" type="{}ExternalEmergencyRoutingConnectionTimeoutSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemExternalEmergencyRoutingParametersGetResponse13mp13", propOrder = {
    "serviceURI",
    "defaultEmergencyNumber",
    "isActive",
    "supportsDNSSRV",
    "connectionTimeoutSeconds"
})
public class SystemExternalEmergencyRoutingParametersGetResponse13Mp13
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceURI;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultEmergencyNumber;
    protected boolean isActive;
    protected boolean supportsDNSSRV;
    protected int connectionTimeoutSeconds;

    /**
     * Ruft den Wert der serviceURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceURI() {
        return serviceURI;
    }

    /**
     * Legt den Wert der serviceURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceURI(String value) {
        this.serviceURI = value;
    }

    /**
     * Ruft den Wert der defaultEmergencyNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultEmergencyNumber() {
        return defaultEmergencyNumber;
    }

    /**
     * Legt den Wert der defaultEmergencyNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultEmergencyNumber(String value) {
        this.defaultEmergencyNumber = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der supportsDNSSRV-Eigenschaft ab.
     * 
     */
    public boolean isSupportsDNSSRV() {
        return supportsDNSSRV;
    }

    /**
     * Legt den Wert der supportsDNSSRV-Eigenschaft fest.
     * 
     */
    public void setSupportsDNSSRV(boolean value) {
        this.supportsDNSSRV = value;
    }

    /**
     * Ruft den Wert der connectionTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getConnectionTimeoutSeconds() {
        return connectionTimeoutSeconds;
    }

    /**
     * Legt den Wert der connectionTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setConnectionTimeoutSeconds(int value) {
        this.connectionTimeoutSeconds = value;
    }

}
