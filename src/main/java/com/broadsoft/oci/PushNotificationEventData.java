//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The common push notification event elements.
 *       
 * 
 * <p>Java-Klasse für PushNotificationEventData complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="PushNotificationEventData">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="eventName" type="{}PushNotificationEventName"/>
 *         <element name="silent" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="mutableContent" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="pushNotificationEventParameters" type="{}PushNotificationEventParameters" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PushNotificationEventData", propOrder = {
    "eventName",
    "silent",
    "mutableContent",
    "pushNotificationEventParameters"
})
public class PushNotificationEventData {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String eventName;
    protected boolean silent;
    protected boolean mutableContent;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String pushNotificationEventParameters;

    /**
     * Ruft den Wert der eventName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * Legt den Wert der eventName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventName(String value) {
        this.eventName = value;
    }

    /**
     * Ruft den Wert der silent-Eigenschaft ab.
     * 
     */
    public boolean isSilent() {
        return silent;
    }

    /**
     * Legt den Wert der silent-Eigenschaft fest.
     * 
     */
    public void setSilent(boolean value) {
        this.silent = value;
    }

    /**
     * Ruft den Wert der mutableContent-Eigenschaft ab.
     * 
     */
    public boolean isMutableContent() {
        return mutableContent;
    }

    /**
     * Legt den Wert der mutableContent-Eigenschaft fest.
     * 
     */
    public void setMutableContent(boolean value) {
        this.mutableContent = value;
    }

    /**
     * Ruft den Wert der pushNotificationEventParameters-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPushNotificationEventParameters() {
        return pushNotificationEventParameters;
    }

    /**
     * Legt den Wert der pushNotificationEventParameters-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPushNotificationEventParameters(String value) {
        this.pushNotificationEventParameters = value;
    }

}
