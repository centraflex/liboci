//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify an enterprise trunk in an enterprise.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für EnterpriseEnterpriseTrunkModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseEnterpriseTrunkModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="enterpriseTrunkName" type="{}EnterpriseTrunkName"/>
 *         <element name="newEnterpriseTrunkName" type="{}EnterpriseTrunkName" minOccurs="0"/>
 *         <element name="maximumRerouteAttempts" type="{}EnterpriseTrunkMaximumRerouteAttempts" minOccurs="0"/>
 *         <element name="routeExhaustionAction" type="{}EnterpriseTrunkRouteExhaustionAction" minOccurs="0"/>
 *         <element name="routeExhaustionForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <choice minOccurs="0">
 *           <element name="orderedRouting">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="trunkGroupList" type="{}ReplacementEnterpriseEnterpriseTrunkTrunkGroupKeyList" minOccurs="0"/>
 *                     <element name="orderingAlgorithm" type="{}EnterpriseTrunkOrderingAlgorithm" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *           <element name="priorityWeightedRouting">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="maximumRerouteAttemptsWithinPriority" type="{}EnterpriseTrunkMaximumRerouteAttempts" minOccurs="0"/>
 *                     <element name="priorityWeightedTrunkGroupList" type="{}ReplacementEnterpriseEnterpriseTrunkPriorityWeightedTrunkGroupList" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *         </choice>
 *         <element name="enableCapacityManagement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxActiveCalls" type="{}MaxActiveCalls" minOccurs="0"/>
 *         <element name="capacityExceededTrapInitialCalls" type="{}TrapInitialThreshold" minOccurs="0"/>
 *         <element name="capacityExceededTrapOffsetCalls" type="{}TrapOffsetThreshold" minOccurs="0"/>
 *         <element name="maximumActiveIncomingCallsAllowed" type="{}EnterpriseTrunkMaximumActiveIncomingCallsAllowed" minOccurs="0"/>
 *         <element name="maximumActiveOutgoingCallsAllowed" type="{}EnterpriseTrunkMaximumActiveOutgoingCallsAllowed" minOccurs="0"/>
 *         <element name="minimumActiveCallsReserved" type="{}EnterpriseTrunkReservedCapacity" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseEnterpriseTrunkModifyRequest", propOrder = {
    "serviceProviderId",
    "enterpriseTrunkName",
    "newEnterpriseTrunkName",
    "maximumRerouteAttempts",
    "routeExhaustionAction",
    "routeExhaustionForwardAddress",
    "orderedRouting",
    "priorityWeightedRouting",
    "enableCapacityManagement",
    "maxActiveCalls",
    "capacityExceededTrapInitialCalls",
    "capacityExceededTrapOffsetCalls",
    "maximumActiveIncomingCallsAllowed",
    "maximumActiveOutgoingCallsAllowed",
    "minimumActiveCallsReserved"
})
public class EnterpriseEnterpriseTrunkModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String enterpriseTrunkName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newEnterpriseTrunkName;
    protected Integer maximumRerouteAttempts;
    @XmlSchemaType(name = "token")
    protected EnterpriseTrunkRouteExhaustionAction routeExhaustionAction;
    @XmlElementRef(name = "routeExhaustionForwardAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> routeExhaustionForwardAddress;
    protected EnterpriseEnterpriseTrunkModifyRequest.OrderedRouting orderedRouting;
    protected EnterpriseEnterpriseTrunkModifyRequest.PriorityWeightedRouting priorityWeightedRouting;
    protected Boolean enableCapacityManagement;
    @XmlElementRef(name = "maxActiveCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maxActiveCalls;
    @XmlElementRef(name = "capacityExceededTrapInitialCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> capacityExceededTrapInitialCalls;
    @XmlElementRef(name = "capacityExceededTrapOffsetCalls", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> capacityExceededTrapOffsetCalls;
    @XmlElementRef(name = "maximumActiveIncomingCallsAllowed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maximumActiveIncomingCallsAllowed;
    @XmlElementRef(name = "maximumActiveOutgoingCallsAllowed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maximumActiveOutgoingCallsAllowed;
    @XmlElementRef(name = "minimumActiveCallsReserved", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> minimumActiveCallsReserved;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der enterpriseTrunkName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnterpriseTrunkName() {
        return enterpriseTrunkName;
    }

    /**
     * Legt den Wert der enterpriseTrunkName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnterpriseTrunkName(String value) {
        this.enterpriseTrunkName = value;
    }

    /**
     * Ruft den Wert der newEnterpriseTrunkName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewEnterpriseTrunkName() {
        return newEnterpriseTrunkName;
    }

    /**
     * Legt den Wert der newEnterpriseTrunkName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewEnterpriseTrunkName(String value) {
        this.newEnterpriseTrunkName = value;
    }

    /**
     * Ruft den Wert der maximumRerouteAttempts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaximumRerouteAttempts() {
        return maximumRerouteAttempts;
    }

    /**
     * Legt den Wert der maximumRerouteAttempts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaximumRerouteAttempts(Integer value) {
        this.maximumRerouteAttempts = value;
    }

    /**
     * Ruft den Wert der routeExhaustionAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseTrunkRouteExhaustionAction }
     *     
     */
    public EnterpriseTrunkRouteExhaustionAction getRouteExhaustionAction() {
        return routeExhaustionAction;
    }

    /**
     * Legt den Wert der routeExhaustionAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseTrunkRouteExhaustionAction }
     *     
     */
    public void setRouteExhaustionAction(EnterpriseTrunkRouteExhaustionAction value) {
        this.routeExhaustionAction = value;
    }

    /**
     * Ruft den Wert der routeExhaustionForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRouteExhaustionForwardAddress() {
        return routeExhaustionForwardAddress;
    }

    /**
     * Legt den Wert der routeExhaustionForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRouteExhaustionForwardAddress(JAXBElement<String> value) {
        this.routeExhaustionForwardAddress = value;
    }

    /**
     * Ruft den Wert der orderedRouting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseEnterpriseTrunkModifyRequest.OrderedRouting }
     *     
     */
    public EnterpriseEnterpriseTrunkModifyRequest.OrderedRouting getOrderedRouting() {
        return orderedRouting;
    }

    /**
     * Legt den Wert der orderedRouting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseEnterpriseTrunkModifyRequest.OrderedRouting }
     *     
     */
    public void setOrderedRouting(EnterpriseEnterpriseTrunkModifyRequest.OrderedRouting value) {
        this.orderedRouting = value;
    }

    /**
     * Ruft den Wert der priorityWeightedRouting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseEnterpriseTrunkModifyRequest.PriorityWeightedRouting }
     *     
     */
    public EnterpriseEnterpriseTrunkModifyRequest.PriorityWeightedRouting getPriorityWeightedRouting() {
        return priorityWeightedRouting;
    }

    /**
     * Legt den Wert der priorityWeightedRouting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseEnterpriseTrunkModifyRequest.PriorityWeightedRouting }
     *     
     */
    public void setPriorityWeightedRouting(EnterpriseEnterpriseTrunkModifyRequest.PriorityWeightedRouting value) {
        this.priorityWeightedRouting = value;
    }

    /**
     * Ruft den Wert der enableCapacityManagement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCapacityManagement() {
        return enableCapacityManagement;
    }

    /**
     * Legt den Wert der enableCapacityManagement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCapacityManagement(Boolean value) {
        this.enableCapacityManagement = value;
    }

    /**
     * Ruft den Wert der maxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaxActiveCalls() {
        return maxActiveCalls;
    }

    /**
     * Legt den Wert der maxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaxActiveCalls(JAXBElement<Integer> value) {
        this.maxActiveCalls = value;
    }

    /**
     * Ruft den Wert der capacityExceededTrapInitialCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCapacityExceededTrapInitialCalls() {
        return capacityExceededTrapInitialCalls;
    }

    /**
     * Legt den Wert der capacityExceededTrapInitialCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCapacityExceededTrapInitialCalls(JAXBElement<Integer> value) {
        this.capacityExceededTrapInitialCalls = value;
    }

    /**
     * Ruft den Wert der capacityExceededTrapOffsetCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCapacityExceededTrapOffsetCalls() {
        return capacityExceededTrapOffsetCalls;
    }

    /**
     * Legt den Wert der capacityExceededTrapOffsetCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCapacityExceededTrapOffsetCalls(JAXBElement<Integer> value) {
        this.capacityExceededTrapOffsetCalls = value;
    }

    /**
     * Ruft den Wert der maximumActiveIncomingCallsAllowed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaximumActiveIncomingCallsAllowed() {
        return maximumActiveIncomingCallsAllowed;
    }

    /**
     * Legt den Wert der maximumActiveIncomingCallsAllowed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaximumActiveIncomingCallsAllowed(JAXBElement<Integer> value) {
        this.maximumActiveIncomingCallsAllowed = value;
    }

    /**
     * Ruft den Wert der maximumActiveOutgoingCallsAllowed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaximumActiveOutgoingCallsAllowed() {
        return maximumActiveOutgoingCallsAllowed;
    }

    /**
     * Legt den Wert der maximumActiveOutgoingCallsAllowed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaximumActiveOutgoingCallsAllowed(JAXBElement<Integer> value) {
        this.maximumActiveOutgoingCallsAllowed = value;
    }

    /**
     * Ruft den Wert der minimumActiveCallsReserved-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMinimumActiveCallsReserved() {
        return minimumActiveCallsReserved;
    }

    /**
     * Legt den Wert der minimumActiveCallsReserved-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMinimumActiveCallsReserved(JAXBElement<Integer> value) {
        this.minimumActiveCallsReserved = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="trunkGroupList" type="{}ReplacementEnterpriseEnterpriseTrunkTrunkGroupKeyList" minOccurs="0"/>
     *         <element name="orderingAlgorithm" type="{}EnterpriseTrunkOrderingAlgorithm" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "trunkGroupList",
        "orderingAlgorithm"
    })
    public static class OrderedRouting {

        @XmlElementRef(name = "trunkGroupList", type = JAXBElement.class, required = false)
        protected JAXBElement<ReplacementEnterpriseEnterpriseTrunkTrunkGroupKeyList> trunkGroupList;
        @XmlSchemaType(name = "token")
        protected EnterpriseTrunkOrderingAlgorithm orderingAlgorithm;

        /**
         * Ruft den Wert der trunkGroupList-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link ReplacementEnterpriseEnterpriseTrunkTrunkGroupKeyList }{@code >}
         *     
         */
        public JAXBElement<ReplacementEnterpriseEnterpriseTrunkTrunkGroupKeyList> getTrunkGroupList() {
            return trunkGroupList;
        }

        /**
         * Legt den Wert der trunkGroupList-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link ReplacementEnterpriseEnterpriseTrunkTrunkGroupKeyList }{@code >}
         *     
         */
        public void setTrunkGroupList(JAXBElement<ReplacementEnterpriseEnterpriseTrunkTrunkGroupKeyList> value) {
            this.trunkGroupList = value;
        }

        /**
         * Ruft den Wert der orderingAlgorithm-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link EnterpriseTrunkOrderingAlgorithm }
         *     
         */
        public EnterpriseTrunkOrderingAlgorithm getOrderingAlgorithm() {
            return orderingAlgorithm;
        }

        /**
         * Legt den Wert der orderingAlgorithm-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link EnterpriseTrunkOrderingAlgorithm }
         *     
         */
        public void setOrderingAlgorithm(EnterpriseTrunkOrderingAlgorithm value) {
            this.orderingAlgorithm = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="maximumRerouteAttemptsWithinPriority" type="{}EnterpriseTrunkMaximumRerouteAttempts" minOccurs="0"/>
     *         <element name="priorityWeightedTrunkGroupList" type="{}ReplacementEnterpriseEnterpriseTrunkPriorityWeightedTrunkGroupList" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "maximumRerouteAttemptsWithinPriority",
        "priorityWeightedTrunkGroupList"
    })
    public static class PriorityWeightedRouting {

        protected Integer maximumRerouteAttemptsWithinPriority;
        @XmlElementRef(name = "priorityWeightedTrunkGroupList", type = JAXBElement.class, required = false)
        protected JAXBElement<ReplacementEnterpriseEnterpriseTrunkPriorityWeightedTrunkGroupList> priorityWeightedTrunkGroupList;

        /**
         * Ruft den Wert der maximumRerouteAttemptsWithinPriority-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMaximumRerouteAttemptsWithinPriority() {
            return maximumRerouteAttemptsWithinPriority;
        }

        /**
         * Legt den Wert der maximumRerouteAttemptsWithinPriority-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMaximumRerouteAttemptsWithinPriority(Integer value) {
            this.maximumRerouteAttemptsWithinPriority = value;
        }

        /**
         * Ruft den Wert der priorityWeightedTrunkGroupList-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link ReplacementEnterpriseEnterpriseTrunkPriorityWeightedTrunkGroupList }{@code >}
         *     
         */
        public JAXBElement<ReplacementEnterpriseEnterpriseTrunkPriorityWeightedTrunkGroupList> getPriorityWeightedTrunkGroupList() {
            return priorityWeightedTrunkGroupList;
        }

        /**
         * Legt den Wert der priorityWeightedTrunkGroupList-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link ReplacementEnterpriseEnterpriseTrunkPriorityWeightedTrunkGroupList }{@code >}
         *     
         */
        public void setPriorityWeightedTrunkGroupList(JAXBElement<ReplacementEnterpriseEnterpriseTrunkPriorityWeightedTrunkGroupList> value) {
            this.priorityWeightedTrunkGroupList = value;
        }

    }

}
