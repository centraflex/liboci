//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *          Modify a MWI Delivery to Mobile Endpoint Custom Template.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderMWIDeliveryToMobileEndpointCustomTemplateModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderMWIDeliveryToMobileEndpointCustomTemplateModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="language" type="{}Language"/>
 *         <element name="type" type="{}MWIDeliveryToMobileEndpointTemplateType"/>
 *         <element name="isEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="templateBody" type="{}MWIDeliveryToMobileEndpointTemplateBody" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderMWIDeliveryToMobileEndpointCustomTemplateModifyRequest", propOrder = {
    "serviceProviderId",
    "language",
    "type",
    "isEnabled",
    "templateBody"
})
public class ServiceProviderMWIDeliveryToMobileEndpointCustomTemplateModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String language;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MWIDeliveryToMobileEndpointTemplateType type;
    protected Boolean isEnabled;
    protected MWIDeliveryToMobileEndpointTemplateBody templateBody;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der language-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Legt den Wert der language-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MWIDeliveryToMobileEndpointTemplateType }
     *     
     */
    public MWIDeliveryToMobileEndpointTemplateType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MWIDeliveryToMobileEndpointTemplateType }
     *     
     */
    public void setType(MWIDeliveryToMobileEndpointTemplateType value) {
        this.type = value;
    }

    /**
     * Ruft den Wert der isEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsEnabled() {
        return isEnabled;
    }

    /**
     * Legt den Wert der isEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsEnabled(Boolean value) {
        this.isEnabled = value;
    }

    /**
     * Ruft den Wert der templateBody-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MWIDeliveryToMobileEndpointTemplateBody }
     *     
     */
    public MWIDeliveryToMobileEndpointTemplateBody getTemplateBody() {
        return templateBody;
    }

    /**
     * Legt den Wert der templateBody-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MWIDeliveryToMobileEndpointTemplateBody }
     *     
     */
    public void setTemplateBody(MWIDeliveryToMobileEndpointTemplateBody value) {
        this.templateBody = value;
    }

}
