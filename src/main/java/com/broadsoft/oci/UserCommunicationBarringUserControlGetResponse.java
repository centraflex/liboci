//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserCommunicationBarringUserControlGetRequest.
 *         Identifies the profiles available to the user and which one if any
 *         is active as well as the lockout status.
 *         Contains a table with column headings: "Name", "Code", "Activated" and "Primary".
 *       
 * 
 * <p>Java-Klasse für UserCommunicationBarringUserControlGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCommunicationBarringUserControlGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="lockoutStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="profileTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCommunicationBarringUserControlGetResponse", propOrder = {
    "lockoutStatus",
    "profileTable"
})
public class UserCommunicationBarringUserControlGetResponse
    extends OCIDataResponse
{

    protected boolean lockoutStatus;
    @XmlElement(required = true)
    protected OCITable profileTable;

    /**
     * Ruft den Wert der lockoutStatus-Eigenschaft ab.
     * 
     */
    public boolean isLockoutStatus() {
        return lockoutStatus;
    }

    /**
     * Legt den Wert der lockoutStatus-Eigenschaft fest.
     * 
     */
    public void setLockoutStatus(boolean value) {
        this.lockoutStatus = value;
    }

    /**
     * Ruft den Wert der profileTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getProfileTable() {
        return profileTable;
    }

    /**
     * Legt den Wert der profileTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setProfileTable(OCITable value) {
        this.profileTable = value;
    }

}
