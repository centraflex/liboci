//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für UserType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="UserType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Normal"/>
 *     <enumeration value="Auto Attendant"/>
 *     <enumeration value="BroadWorks Anywhere"/>
 *     <enumeration value="Call Center"/>
 *     <enumeration value="Collaborate Bridge"/>
 *     <enumeration value="Find-me/Follow-me"/>
 *     <enumeration value="Flexible Seating Host"/>
 *     <enumeration value="Group Paging"/>
 *     <enumeration value="Hunt Group"/>
 *     <enumeration value="Instant Group Call"/>
 *     <enumeration value="Meet-Me Conferencing"/>
 *     <enumeration value="Music On Hold"/>
 *     <enumeration value="Route Point"/>
 *     <enumeration value="Voice Messaging"/>
 *     <enumeration value="VoiceXML"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "UserType")
@XmlEnum
public enum UserType {

    @XmlEnumValue("Normal")
    NORMAL("Normal"),
    @XmlEnumValue("Auto Attendant")
    AUTO_ATTENDANT("Auto Attendant"),
    @XmlEnumValue("BroadWorks Anywhere")
    BROAD_WORKS_ANYWHERE("BroadWorks Anywhere"),
    @XmlEnumValue("Call Center")
    CALL_CENTER("Call Center"),
    @XmlEnumValue("Collaborate Bridge")
    COLLABORATE_BRIDGE("Collaborate Bridge"),
    @XmlEnumValue("Find-me/Follow-me")
    FIND_ME_FOLLOW_ME("Find-me/Follow-me"),
    @XmlEnumValue("Flexible Seating Host")
    FLEXIBLE_SEATING_HOST("Flexible Seating Host"),
    @XmlEnumValue("Group Paging")
    GROUP_PAGING("Group Paging"),
    @XmlEnumValue("Hunt Group")
    HUNT_GROUP("Hunt Group"),
    @XmlEnumValue("Instant Group Call")
    INSTANT_GROUP_CALL("Instant Group Call"),
    @XmlEnumValue("Meet-Me Conferencing")
    MEET_ME_CONFERENCING("Meet-Me Conferencing"),
    @XmlEnumValue("Music On Hold")
    MUSIC_ON_HOLD("Music On Hold"),
    @XmlEnumValue("Route Point")
    ROUTE_POINT("Route Point"),
    @XmlEnumValue("Voice Messaging")
    VOICE_MESSAGING("Voice Messaging"),
    @XmlEnumValue("VoiceXML")
    VOICE_XML("VoiceXML");
    private final String value;

    UserType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UserType fromValue(String v) {
        for (UserType c: UserType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
