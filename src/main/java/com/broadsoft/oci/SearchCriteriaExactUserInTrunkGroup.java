//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for user in/not in a trunk group.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactUserInTrunkGroup complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactUserInTrunkGroup">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="userInTrunkGroup" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactUserInTrunkGroup", propOrder = {
    "userInTrunkGroup"
})
public class SearchCriteriaExactUserInTrunkGroup
    extends SearchCriteria
{

    protected boolean userInTrunkGroup;

    /**
     * Ruft den Wert der userInTrunkGroup-Eigenschaft ab.
     * 
     */
    public boolean isUserInTrunkGroup() {
        return userInTrunkGroup;
    }

    /**
     * Legt den Wert der userInTrunkGroup-Eigenschaft fest.
     * 
     */
    public void setUserInTrunkGroup(boolean value) {
        this.userInTrunkGroup = value;
    }

}
