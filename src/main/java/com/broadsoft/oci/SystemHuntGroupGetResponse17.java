//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemHuntGroupGetRequest.
 *         Replaced by: SystemHuntGroupGetResponse21 in AS data mode
 *       
 * 
 * <p>Java-Klasse für SystemHuntGroupGetResponse17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemHuntGroupGetResponse17">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="anonymousInsteadOfPrivateCLID" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="removeHuntGroupNameFromCLID" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="uniformCallDistributionPolicyScope" type="{}HuntGroupUniformCallDistributionPolicyScope"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemHuntGroupGetResponse17", propOrder = {
    "anonymousInsteadOfPrivateCLID",
    "removeHuntGroupNameFromCLID",
    "uniformCallDistributionPolicyScope"
})
public class SystemHuntGroupGetResponse17
    extends OCIDataResponse
{

    protected boolean anonymousInsteadOfPrivateCLID;
    protected boolean removeHuntGroupNameFromCLID;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected HuntGroupUniformCallDistributionPolicyScope uniformCallDistributionPolicyScope;

    /**
     * Ruft den Wert der anonymousInsteadOfPrivateCLID-Eigenschaft ab.
     * 
     */
    public boolean isAnonymousInsteadOfPrivateCLID() {
        return anonymousInsteadOfPrivateCLID;
    }

    /**
     * Legt den Wert der anonymousInsteadOfPrivateCLID-Eigenschaft fest.
     * 
     */
    public void setAnonymousInsteadOfPrivateCLID(boolean value) {
        this.anonymousInsteadOfPrivateCLID = value;
    }

    /**
     * Ruft den Wert der removeHuntGroupNameFromCLID-Eigenschaft ab.
     * 
     */
    public boolean isRemoveHuntGroupNameFromCLID() {
        return removeHuntGroupNameFromCLID;
    }

    /**
     * Legt den Wert der removeHuntGroupNameFromCLID-Eigenschaft fest.
     * 
     */
    public void setRemoveHuntGroupNameFromCLID(boolean value) {
        this.removeHuntGroupNameFromCLID = value;
    }

    /**
     * Ruft den Wert der uniformCallDistributionPolicyScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HuntGroupUniformCallDistributionPolicyScope }
     *     
     */
    public HuntGroupUniformCallDistributionPolicyScope getUniformCallDistributionPolicyScope() {
        return uniformCallDistributionPolicyScope;
    }

    /**
     * Legt den Wert der uniformCallDistributionPolicyScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HuntGroupUniformCallDistributionPolicyScope }
     *     
     */
    public void setUniformCallDistributionPolicyScope(HuntGroupUniformCallDistributionPolicyScope value) {
        this.uniformCallDistributionPolicyScope = value;
    }

}
