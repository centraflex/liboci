//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemNumberActivationGetRequest.
 *         Contains the system number activation setting.
 *       
 * 
 * <p>Java-Klasse für SystemNumberActivationGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNumberActivationGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useNumberActivation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNumberActivationGetResponse", propOrder = {
    "useNumberActivation"
})
public class SystemNumberActivationGetResponse
    extends OCIDataResponse
{

    protected boolean useNumberActivation;

    /**
     * Ruft den Wert der useNumberActivation-Eigenschaft ab.
     * 
     */
    public boolean isUseNumberActivation() {
        return useNumberActivation;
    }

    /**
     * Legt den Wert der useNumberActivation-Eigenschaft fest.
     * 
     */
    public void setUseNumberActivation(boolean value) {
        this.useNumberActivation = value;
    }

}
