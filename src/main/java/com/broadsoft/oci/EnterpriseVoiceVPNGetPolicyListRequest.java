//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request the list of Voice VPN locations.
 *         It is possible to search by various criteria to restrict the number of rows returned.
 *         Multiple search criteria are logically ANDed together.
 *         The response is either a EnterpriseVoiceVPNGetPolicyListResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für EnterpriseVoiceVPNGetPolicyListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseVoiceVPNGetPolicyListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaGroupLocationCode" type="{}SearchCriteriaGroupLocationCode" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactPolicySelection" type="{}SearchCriteriaExactPolicySelection" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseVoiceVPNGetPolicyListRequest", propOrder = {
    "serviceProviderId",
    "responseSizeLimit",
    "searchCriteriaGroupLocationCode",
    "searchCriteriaExactPolicySelection"
})
public class EnterpriseVoiceVPNGetPolicyListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected Integer responseSizeLimit;
    protected List<SearchCriteriaGroupLocationCode> searchCriteriaGroupLocationCode;
    protected SearchCriteriaExactPolicySelection searchCriteriaExactPolicySelection;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaGroupLocationCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaGroupLocationCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaGroupLocationCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaGroupLocationCode }
     * 
     * 
     * @return
     *     The value of the searchCriteriaGroupLocationCode property.
     */
    public List<SearchCriteriaGroupLocationCode> getSearchCriteriaGroupLocationCode() {
        if (searchCriteriaGroupLocationCode == null) {
            searchCriteriaGroupLocationCode = new ArrayList<>();
        }
        return this.searchCriteriaGroupLocationCode;
    }

    /**
     * Ruft den Wert der searchCriteriaExactPolicySelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactPolicySelection }
     *     
     */
    public SearchCriteriaExactPolicySelection getSearchCriteriaExactPolicySelection() {
        return searchCriteriaExactPolicySelection;
    }

    /**
     * Legt den Wert der searchCriteriaExactPolicySelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactPolicySelection }
     *     
     */
    public void setSearchCriteriaExactPolicySelection(SearchCriteriaExactPolicySelection value) {
        this.searchCriteriaExactPolicySelection = value;
    }

}
