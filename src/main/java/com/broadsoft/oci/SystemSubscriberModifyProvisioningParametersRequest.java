//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system provisioning configuration for all subscribers.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemSubscriberModifyProvisioningParametersRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSubscriberModifyProvisioningParametersRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="configurableCLIDNormalization" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeDefaultDomain" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSubscriberModifyProvisioningParametersRequest", propOrder = {
    "configurableCLIDNormalization",
    "includeDefaultDomain"
})
public class SystemSubscriberModifyProvisioningParametersRequest
    extends OCIRequest
{

    protected Boolean configurableCLIDNormalization;
    protected Boolean includeDefaultDomain;

    /**
     * Ruft den Wert der configurableCLIDNormalization-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isConfigurableCLIDNormalization() {
        return configurableCLIDNormalization;
    }

    /**
     * Legt den Wert der configurableCLIDNormalization-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setConfigurableCLIDNormalization(Boolean value) {
        this.configurableCLIDNormalization = value;
    }

    /**
     * Ruft den Wert der includeDefaultDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeDefaultDomain() {
        return includeDefaultDomain;
    }

    /**
     * Legt den Wert der includeDefaultDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeDefaultDomain(Boolean value) {
        this.includeDefaultDomain = value;
    }

}
