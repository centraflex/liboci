//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Returns true if the UserService or service pack is assigned, otherwise false.
 *       
 * 
 * <p>Java-Klasse für UserServiceIsAssignedResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserServiceIsAssignedResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isAssigned" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserServiceIsAssignedResponse", propOrder = {
    "isAssigned"
})
public class UserServiceIsAssignedResponse
    extends OCIDataResponse
{

    protected boolean isAssigned;

    /**
     * Ruft den Wert der isAssigned-Eigenschaft ab.
     * 
     */
    public boolean isIsAssigned() {
        return isAssigned;
    }

    /**
     * Legt den Wert der isAssigned-Eigenschaft fest.
     * 
     */
    public void setIsAssigned(boolean value) {
        this.isAssigned = value;
    }

}
