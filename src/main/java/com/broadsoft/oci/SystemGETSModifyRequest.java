//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify GETS system settings.
 *         The following elements are only used in AS data mode and ignored in the Amplify data mode:
 *           callIdentifierMode
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemGETSModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGETSModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableRequireResourcePriority" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="sendAccessResourcePriority" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="callIdentifierMode" type="{}GETSCallIdentifierMode" minOccurs="0"/>
 *         <element name="defaultPriorityAVP" type="{}GETSPriorityAVP" minOccurs="0"/>
 *         <element name="signalingDSCP" type="{}GETSSignalingDSCP" minOccurs="0"/>
 *         <element name="defaultRValue" type="{}GETSPriorityValue" minOccurs="0"/>
 *         <element name="bypassRoRelease" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGETSModifyRequest", propOrder = {
    "enabled",
    "enableRequireResourcePriority",
    "sendAccessResourcePriority",
    "callIdentifierMode",
    "defaultPriorityAVP",
    "signalingDSCP",
    "defaultRValue",
    "bypassRoRelease"
})
public class SystemGETSModifyRequest
    extends OCIRequest
{

    protected Boolean enabled;
    protected Boolean enableRequireResourcePriority;
    protected Boolean sendAccessResourcePriority;
    @XmlSchemaType(name = "token")
    protected GETSCallIdentifierMode callIdentifierMode;
    protected Integer defaultPriorityAVP;
    protected Integer signalingDSCP;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultRValue;
    protected Boolean bypassRoRelease;

    /**
     * Ruft den Wert der enabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnabled() {
        return enabled;
    }

    /**
     * Legt den Wert der enabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Ruft den Wert der enableRequireResourcePriority-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableRequireResourcePriority() {
        return enableRequireResourcePriority;
    }

    /**
     * Legt den Wert der enableRequireResourcePriority-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableRequireResourcePriority(Boolean value) {
        this.enableRequireResourcePriority = value;
    }

    /**
     * Ruft den Wert der sendAccessResourcePriority-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendAccessResourcePriority() {
        return sendAccessResourcePriority;
    }

    /**
     * Legt den Wert der sendAccessResourcePriority-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendAccessResourcePriority(Boolean value) {
        this.sendAccessResourcePriority = value;
    }

    /**
     * Ruft den Wert der callIdentifierMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GETSCallIdentifierMode }
     *     
     */
    public GETSCallIdentifierMode getCallIdentifierMode() {
        return callIdentifierMode;
    }

    /**
     * Legt den Wert der callIdentifierMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GETSCallIdentifierMode }
     *     
     */
    public void setCallIdentifierMode(GETSCallIdentifierMode value) {
        this.callIdentifierMode = value;
    }

    /**
     * Ruft den Wert der defaultPriorityAVP-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDefaultPriorityAVP() {
        return defaultPriorityAVP;
    }

    /**
     * Legt den Wert der defaultPriorityAVP-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDefaultPriorityAVP(Integer value) {
        this.defaultPriorityAVP = value;
    }

    /**
     * Ruft den Wert der signalingDSCP-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSignalingDSCP() {
        return signalingDSCP;
    }

    /**
     * Legt den Wert der signalingDSCP-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSignalingDSCP(Integer value) {
        this.signalingDSCP = value;
    }

    /**
     * Ruft den Wert der defaultRValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultRValue() {
        return defaultRValue;
    }

    /**
     * Legt den Wert der defaultRValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultRValue(String value) {
        this.defaultRValue = value;
    }

    /**
     * Ruft den Wert der bypassRoRelease-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBypassRoRelease() {
        return bypassRoRelease;
    }

    /**
     * Legt den Wert der bypassRoRelease-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBypassRoRelease(Boolean value) {
        this.bypassRoRelease = value;
    }

}
