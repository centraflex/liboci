//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *           Response to SystemSIPDeviceTypeFileGetRequest20.
 *           Take note:
 *           1. accessUrl may have undefined content.
 *           When it is the case, undefined content is put between [].
 *           It may also be set to "Error Access FQDN Not Provisioned" when the access FQDN is not set,
 *           or "Error Access Context Name Not Provisioned" when the context name is not set.
 *           2. repositoryUrl may be set to "DEVICE_CONFIGURATION_FILE_REPOSITORY_MISSING", if there is no file repository defined.
 *           
 *           Replaced by: SystemSIPDeviceTypeFileGetResponse20Sp1
 *         
 * 
 * <p>Java-Klasse für SystemSIPDeviceTypeFileGetResponse20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPDeviceTypeFileGetResponse20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="remoteFileFormat" type="{}DeviceManagementFileFormat"/>
 *         <element name="fileCategory" type="{}DeviceManagementFileCategory"/>
 *         <element name="fileCustomization" type="{}DeviceManagementFileCustomization"/>
 *         <element name="fileSource" type="{}DeviceTypeFileEnhancedConfigurationMode"/>
 *         <element name="configurationFileName" type="{}AccessDeviceEnhancedConfigurationFileName" minOccurs="0"/>
 *         <element name="useHttpDigestAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macBasedFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="userNamePasswordFileAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macInNonRequestURI" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="macFormatInNonRequestURI" type="{}DeviceManagementAccessURI" minOccurs="0"/>
 *         <element name="accessUrl" type="{}URL"/>
 *         <element name="repositoryUrl" type="{}URL" minOccurs="0"/>
 *         <element name="templateUrl" type="{}URL" minOccurs="0"/>
 *         <element name="allowHttp" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowHttps" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowTftp" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableCaching" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowUploadFromDevice" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="defaultExtendedFileCaptureMode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPDeviceTypeFileGetResponse20", propOrder = {
    "remoteFileFormat",
    "fileCategory",
    "fileCustomization",
    "fileSource",
    "configurationFileName",
    "useHttpDigestAuthentication",
    "macBasedFileAuthentication",
    "userNamePasswordFileAuthentication",
    "macInNonRequestURI",
    "macFormatInNonRequestURI",
    "accessUrl",
    "repositoryUrl",
    "templateUrl",
    "allowHttp",
    "allowHttps",
    "allowTftp",
    "enableCaching",
    "allowUploadFromDevice",
    "defaultExtendedFileCaptureMode"
})
public class SystemSIPDeviceTypeFileGetResponse20
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String remoteFileFormat;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceManagementFileCategory fileCategory;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceManagementFileCustomization fileCustomization;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceTypeFileEnhancedConfigurationMode fileSource;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String configurationFileName;
    protected boolean useHttpDigestAuthentication;
    protected boolean macBasedFileAuthentication;
    protected boolean userNamePasswordFileAuthentication;
    protected boolean macInNonRequestURI;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String macFormatInNonRequestURI;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accessUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String repositoryUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String templateUrl;
    protected boolean allowHttp;
    protected boolean allowHttps;
    protected boolean allowTftp;
    protected boolean enableCaching;
    protected boolean allowUploadFromDevice;
    protected Boolean defaultExtendedFileCaptureMode;

    /**
     * Ruft den Wert der remoteFileFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteFileFormat() {
        return remoteFileFormat;
    }

    /**
     * Legt den Wert der remoteFileFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteFileFormat(String value) {
        this.remoteFileFormat = value;
    }

    /**
     * Ruft den Wert der fileCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementFileCategory }
     *     
     */
    public DeviceManagementFileCategory getFileCategory() {
        return fileCategory;
    }

    /**
     * Legt den Wert der fileCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementFileCategory }
     *     
     */
    public void setFileCategory(DeviceManagementFileCategory value) {
        this.fileCategory = value;
    }

    /**
     * Ruft den Wert der fileCustomization-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementFileCustomization }
     *     
     */
    public DeviceManagementFileCustomization getFileCustomization() {
        return fileCustomization;
    }

    /**
     * Legt den Wert der fileCustomization-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementFileCustomization }
     *     
     */
    public void setFileCustomization(DeviceManagementFileCustomization value) {
        this.fileCustomization = value;
    }

    /**
     * Ruft den Wert der fileSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceTypeFileEnhancedConfigurationMode }
     *     
     */
    public DeviceTypeFileEnhancedConfigurationMode getFileSource() {
        return fileSource;
    }

    /**
     * Legt den Wert der fileSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceTypeFileEnhancedConfigurationMode }
     *     
     */
    public void setFileSource(DeviceTypeFileEnhancedConfigurationMode value) {
        this.fileSource = value;
    }

    /**
     * Ruft den Wert der configurationFileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfigurationFileName() {
        return configurationFileName;
    }

    /**
     * Legt den Wert der configurationFileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfigurationFileName(String value) {
        this.configurationFileName = value;
    }

    /**
     * Ruft den Wert der useHttpDigestAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isUseHttpDigestAuthentication() {
        return useHttpDigestAuthentication;
    }

    /**
     * Legt den Wert der useHttpDigestAuthentication-Eigenschaft fest.
     * 
     */
    public void setUseHttpDigestAuthentication(boolean value) {
        this.useHttpDigestAuthentication = value;
    }

    /**
     * Ruft den Wert der macBasedFileAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isMacBasedFileAuthentication() {
        return macBasedFileAuthentication;
    }

    /**
     * Legt den Wert der macBasedFileAuthentication-Eigenschaft fest.
     * 
     */
    public void setMacBasedFileAuthentication(boolean value) {
        this.macBasedFileAuthentication = value;
    }

    /**
     * Ruft den Wert der userNamePasswordFileAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isUserNamePasswordFileAuthentication() {
        return userNamePasswordFileAuthentication;
    }

    /**
     * Legt den Wert der userNamePasswordFileAuthentication-Eigenschaft fest.
     * 
     */
    public void setUserNamePasswordFileAuthentication(boolean value) {
        this.userNamePasswordFileAuthentication = value;
    }

    /**
     * Ruft den Wert der macInNonRequestURI-Eigenschaft ab.
     * 
     */
    public boolean isMacInNonRequestURI() {
        return macInNonRequestURI;
    }

    /**
     * Legt den Wert der macInNonRequestURI-Eigenschaft fest.
     * 
     */
    public void setMacInNonRequestURI(boolean value) {
        this.macInNonRequestURI = value;
    }

    /**
     * Ruft den Wert der macFormatInNonRequestURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacFormatInNonRequestURI() {
        return macFormatInNonRequestURI;
    }

    /**
     * Legt den Wert der macFormatInNonRequestURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacFormatInNonRequestURI(String value) {
        this.macFormatInNonRequestURI = value;
    }

    /**
     * Ruft den Wert der accessUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessUrl() {
        return accessUrl;
    }

    /**
     * Legt den Wert der accessUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessUrl(String value) {
        this.accessUrl = value;
    }

    /**
     * Ruft den Wert der repositoryUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepositoryUrl() {
        return repositoryUrl;
    }

    /**
     * Legt den Wert der repositoryUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepositoryUrl(String value) {
        this.repositoryUrl = value;
    }

    /**
     * Ruft den Wert der templateUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateUrl() {
        return templateUrl;
    }

    /**
     * Legt den Wert der templateUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateUrl(String value) {
        this.templateUrl = value;
    }

    /**
     * Ruft den Wert der allowHttp-Eigenschaft ab.
     * 
     */
    public boolean isAllowHttp() {
        return allowHttp;
    }

    /**
     * Legt den Wert der allowHttp-Eigenschaft fest.
     * 
     */
    public void setAllowHttp(boolean value) {
        this.allowHttp = value;
    }

    /**
     * Ruft den Wert der allowHttps-Eigenschaft ab.
     * 
     */
    public boolean isAllowHttps() {
        return allowHttps;
    }

    /**
     * Legt den Wert der allowHttps-Eigenschaft fest.
     * 
     */
    public void setAllowHttps(boolean value) {
        this.allowHttps = value;
    }

    /**
     * Ruft den Wert der allowTftp-Eigenschaft ab.
     * 
     */
    public boolean isAllowTftp() {
        return allowTftp;
    }

    /**
     * Legt den Wert der allowTftp-Eigenschaft fest.
     * 
     */
    public void setAllowTftp(boolean value) {
        this.allowTftp = value;
    }

    /**
     * Ruft den Wert der enableCaching-Eigenschaft ab.
     * 
     */
    public boolean isEnableCaching() {
        return enableCaching;
    }

    /**
     * Legt den Wert der enableCaching-Eigenschaft fest.
     * 
     */
    public void setEnableCaching(boolean value) {
        this.enableCaching = value;
    }

    /**
     * Ruft den Wert der allowUploadFromDevice-Eigenschaft ab.
     * 
     */
    public boolean isAllowUploadFromDevice() {
        return allowUploadFromDevice;
    }

    /**
     * Legt den Wert der allowUploadFromDevice-Eigenschaft fest.
     * 
     */
    public void setAllowUploadFromDevice(boolean value) {
        this.allowUploadFromDevice = value;
    }

    /**
     * Ruft den Wert der defaultExtendedFileCaptureMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefaultExtendedFileCaptureMode() {
        return defaultExtendedFileCaptureMode;
    }

    /**
     * Legt den Wert der defaultExtendedFileCaptureMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultExtendedFileCaptureMode(Boolean value) {
        this.defaultExtendedFileCaptureMode = value;
    }

}
