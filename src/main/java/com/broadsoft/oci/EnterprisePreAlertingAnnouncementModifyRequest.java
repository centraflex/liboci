//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the enterprise level pre-alerting service settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für EnterprisePreAlertingAnnouncementModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterprisePreAlertingAnnouncementModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="announcementInterruption" type="{}PreAlertingAnnouncementInterrupt" minOccurs="0"/>
 *         <element name="interruptionDigitSequence" type="{}PreAlertingAnnouncementInterruptDigits" minOccurs="0"/>
 *         <element name="audioSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="audioFile" type="{}ExtendedMediaFileResource" minOccurs="0"/>
 *         <element name="videoSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="videoFile" type="{}ExtendedMediaFileResource" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterprisePreAlertingAnnouncementModifyRequest", propOrder = {
    "serviceProviderId",
    "announcementInterruption",
    "interruptionDigitSequence",
    "audioSelection",
    "audioFile",
    "videoSelection",
    "videoFile"
})
public class EnterprisePreAlertingAnnouncementModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlSchemaType(name = "token")
    protected PreAlertingAnnouncementInterrupt announcementInterruption;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String interruptionDigitSequence;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection audioSelection;
    protected ExtendedMediaFileResource audioFile;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection videoSelection;
    protected ExtendedMediaFileResource videoFile;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der announcementInterruption-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PreAlertingAnnouncementInterrupt }
     *     
     */
    public PreAlertingAnnouncementInterrupt getAnnouncementInterruption() {
        return announcementInterruption;
    }

    /**
     * Legt den Wert der announcementInterruption-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PreAlertingAnnouncementInterrupt }
     *     
     */
    public void setAnnouncementInterruption(PreAlertingAnnouncementInterrupt value) {
        this.announcementInterruption = value;
    }

    /**
     * Ruft den Wert der interruptionDigitSequence-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterruptionDigitSequence() {
        return interruptionDigitSequence;
    }

    /**
     * Legt den Wert der interruptionDigitSequence-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterruptionDigitSequence(String value) {
        this.interruptionDigitSequence = value;
    }

    /**
     * Ruft den Wert der audioSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getAudioSelection() {
        return audioSelection;
    }

    /**
     * Legt den Wert der audioSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setAudioSelection(ExtendedFileResourceSelection value) {
        this.audioSelection = value;
    }

    /**
     * Ruft den Wert der audioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public ExtendedMediaFileResource getAudioFile() {
        return audioFile;
    }

    /**
     * Legt den Wert der audioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public void setAudioFile(ExtendedMediaFileResource value) {
        this.audioFile = value;
    }

    /**
     * Ruft den Wert der videoSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getVideoSelection() {
        return videoSelection;
    }

    /**
     * Legt den Wert der videoSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setVideoSelection(ExtendedFileResourceSelection value) {
        this.videoSelection = value;
    }

    /**
     * Ruft den Wert der videoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public ExtendedMediaFileResource getVideoFile() {
        return videoFile;
    }

    /**
     * Legt den Wert der videoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedMediaFileResource }
     *     
     */
    public void setVideoFile(ExtendedMediaFileResource value) {
        this.videoFile = value;
    }

}
