//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *  	       Response to UserCollaborateProjectRoomGetRequest.
 *  	    
 * 
 * <p>Java-Klasse für UserCollaborateProjectRoomGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCollaborateProjectRoomGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="roomName" type="{}CollaborateRoomName"/>
 *         <element name="attendeeNotification" type="{}CollaborateRoomAttendeeNotification"/>
 *         <element name="endCollaborateRoomSessionOnOwnerExit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="ownerRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="roomSchedule" type="{}CollaborateRoomSchedule"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCollaborateProjectRoomGetResponse", propOrder = {
    "roomName",
    "attendeeNotification",
    "endCollaborateRoomSessionOnOwnerExit",
    "ownerRequired",
    "roomSchedule"
})
public class UserCollaborateProjectRoomGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String roomName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CollaborateRoomAttendeeNotification attendeeNotification;
    protected boolean endCollaborateRoomSessionOnOwnerExit;
    protected boolean ownerRequired;
    @XmlElement(required = true)
    protected CollaborateRoomSchedule roomSchedule;

    /**
     * Ruft den Wert der roomName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomName() {
        return roomName;
    }

    /**
     * Legt den Wert der roomName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomName(String value) {
        this.roomName = value;
    }

    /**
     * Ruft den Wert der attendeeNotification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CollaborateRoomAttendeeNotification }
     *     
     */
    public CollaborateRoomAttendeeNotification getAttendeeNotification() {
        return attendeeNotification;
    }

    /**
     * Legt den Wert der attendeeNotification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CollaborateRoomAttendeeNotification }
     *     
     */
    public void setAttendeeNotification(CollaborateRoomAttendeeNotification value) {
        this.attendeeNotification = value;
    }

    /**
     * Ruft den Wert der endCollaborateRoomSessionOnOwnerExit-Eigenschaft ab.
     * 
     */
    public boolean isEndCollaborateRoomSessionOnOwnerExit() {
        return endCollaborateRoomSessionOnOwnerExit;
    }

    /**
     * Legt den Wert der endCollaborateRoomSessionOnOwnerExit-Eigenschaft fest.
     * 
     */
    public void setEndCollaborateRoomSessionOnOwnerExit(boolean value) {
        this.endCollaborateRoomSessionOnOwnerExit = value;
    }

    /**
     * Ruft den Wert der ownerRequired-Eigenschaft ab.
     * 
     */
    public boolean isOwnerRequired() {
        return ownerRequired;
    }

    /**
     * Legt den Wert der ownerRequired-Eigenschaft fest.
     * 
     */
    public void setOwnerRequired(boolean value) {
        this.ownerRequired = value;
    }

    /**
     * Ruft den Wert der roomSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CollaborateRoomSchedule }
     *     
     */
    public CollaborateRoomSchedule getRoomSchedule() {
        return roomSchedule;
    }

    /**
     * Legt den Wert der roomSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CollaborateRoomSchedule }
     *     
     */
    public void setRoomSchedule(CollaborateRoomSchedule value) {
        this.roomSchedule = value;
    }

}
