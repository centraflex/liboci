//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeviceManagementFileCategory22.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DeviceManagementFileCategory22">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Static"/>
 *     <enumeration value="Dynamic Group"/>
 *     <enumeration value="Dynamic Profile"/>
 *     <enumeration value="Template Only"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DeviceManagementFileCategory22")
@XmlEnum
public enum DeviceManagementFileCategory22 {

    @XmlEnumValue("Static")
    STATIC("Static"),
    @XmlEnumValue("Dynamic Group")
    DYNAMIC_GROUP("Dynamic Group"),
    @XmlEnumValue("Dynamic Profile")
    DYNAMIC_PROFILE("Dynamic Profile"),
    @XmlEnumValue("Template Only")
    TEMPLATE_ONLY("Template Only");
    private final String value;

    DeviceManagementFileCategory22(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeviceManagementFileCategory22 fromValue(String v) {
        for (DeviceManagementFileCategory22 c: DeviceManagementFileCategory22 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
