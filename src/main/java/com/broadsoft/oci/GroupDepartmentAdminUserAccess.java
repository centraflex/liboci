//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für GroupDepartmentAdminUserAccess.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="GroupDepartmentAdminUserAccess">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Full"/>
 *     <enumeration value="Read-Only Profile"/>
 *     <enumeration value="No Profile"/>
 *     <enumeration value="None"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "GroupDepartmentAdminUserAccess")
@XmlEnum
public enum GroupDepartmentAdminUserAccess {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("Read-Only Profile")
    READ_ONLY_PROFILE("Read-Only Profile"),
    @XmlEnumValue("No Profile")
    NO_PROFILE("No Profile"),
    @XmlEnumValue("None")
    NONE("None");
    private final String value;

    GroupDepartmentAdminUserAccess(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupDepartmentAdminUserAccess fromValue(String v) {
        for (GroupDepartmentAdminUserAccess c: GroupDepartmentAdminUserAccess.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
