//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for a skill Level.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactSkillLevel complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactSkillLevel">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="skillLevel" type="{}CallCenterAgentSkillLevel"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactSkillLevel", propOrder = {
    "skillLevel"
})
public class SearchCriteriaExactSkillLevel
    extends SearchCriteria
{

    protected int skillLevel;

    /**
     * Ruft den Wert der skillLevel-Eigenschaft ab.
     * 
     */
    public int getSkillLevel() {
        return skillLevel;
    }

    /**
     * Legt den Wert der skillLevel-Eigenschaft fest.
     * 
     */
    public void setSkillLevel(int value) {
        this.skillLevel = value;
    }

}
