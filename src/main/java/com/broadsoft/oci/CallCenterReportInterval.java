//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Report interval for call center enhanced reporting scheduled reports. 
 *       
 * 
 * <p>Java-Klasse für CallCenterReportInterval complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterReportInterval">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <choice>
 *         <element name="dates" type="{}CallCenterReportIntervalDates"/>
 *         <element name="current" type="{}CallCenterReportCurrentInterval"/>
 *         <element name="past" type="{}CallCenterReportPastInterval"/>
 *       </choice>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterReportInterval", propOrder = {
    "dates",
    "current",
    "past"
})
public class CallCenterReportInterval {

    protected CallCenterReportIntervalDates dates;
    protected CallCenterReportCurrentInterval current;
    protected CallCenterReportPastInterval past;

    /**
     * Ruft den Wert der dates-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportIntervalDates }
     *     
     */
    public CallCenterReportIntervalDates getDates() {
        return dates;
    }

    /**
     * Legt den Wert der dates-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportIntervalDates }
     *     
     */
    public void setDates(CallCenterReportIntervalDates value) {
        this.dates = value;
    }

    /**
     * Ruft den Wert der current-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportCurrentInterval }
     *     
     */
    public CallCenterReportCurrentInterval getCurrent() {
        return current;
    }

    /**
     * Legt den Wert der current-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportCurrentInterval }
     *     
     */
    public void setCurrent(CallCenterReportCurrentInterval value) {
        this.current = value;
    }

    /**
     * Ruft den Wert der past-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportPastInterval }
     *     
     */
    public CallCenterReportPastInterval getPast() {
        return past;
    }

    /**
     * Legt den Wert der past-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportPastInterval }
     *     
     */
    public void setPast(CallCenterReportPastInterval value) {
        this.past = value;
    }

}
