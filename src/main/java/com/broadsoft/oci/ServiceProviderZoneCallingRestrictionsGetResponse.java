//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *          Response to ServiceProviderZoneCallingRestrictionsGetRequest
 *       
 * 
 * <p>Java-Klasse für ServiceProviderZoneCallingRestrictionsGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderZoneCallingRestrictionsGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableZoneCallingRestrictions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableOriginationRoamingRestrictions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableEmergencyOriginationRoamingRestrictions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableTerminationRoamingRestrictions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderZoneCallingRestrictionsGetResponse", propOrder = {
    "enableZoneCallingRestrictions",
    "enableOriginationRoamingRestrictions",
    "enableEmergencyOriginationRoamingRestrictions",
    "enableTerminationRoamingRestrictions"
})
public class ServiceProviderZoneCallingRestrictionsGetResponse
    extends OCIDataResponse
{

    protected boolean enableZoneCallingRestrictions;
    protected boolean enableOriginationRoamingRestrictions;
    protected boolean enableEmergencyOriginationRoamingRestrictions;
    protected boolean enableTerminationRoamingRestrictions;

    /**
     * Ruft den Wert der enableZoneCallingRestrictions-Eigenschaft ab.
     * 
     */
    public boolean isEnableZoneCallingRestrictions() {
        return enableZoneCallingRestrictions;
    }

    /**
     * Legt den Wert der enableZoneCallingRestrictions-Eigenschaft fest.
     * 
     */
    public void setEnableZoneCallingRestrictions(boolean value) {
        this.enableZoneCallingRestrictions = value;
    }

    /**
     * Ruft den Wert der enableOriginationRoamingRestrictions-Eigenschaft ab.
     * 
     */
    public boolean isEnableOriginationRoamingRestrictions() {
        return enableOriginationRoamingRestrictions;
    }

    /**
     * Legt den Wert der enableOriginationRoamingRestrictions-Eigenschaft fest.
     * 
     */
    public void setEnableOriginationRoamingRestrictions(boolean value) {
        this.enableOriginationRoamingRestrictions = value;
    }

    /**
     * Ruft den Wert der enableEmergencyOriginationRoamingRestrictions-Eigenschaft ab.
     * 
     */
    public boolean isEnableEmergencyOriginationRoamingRestrictions() {
        return enableEmergencyOriginationRoamingRestrictions;
    }

    /**
     * Legt den Wert der enableEmergencyOriginationRoamingRestrictions-Eigenschaft fest.
     * 
     */
    public void setEnableEmergencyOriginationRoamingRestrictions(boolean value) {
        this.enableEmergencyOriginationRoamingRestrictions = value;
    }

    /**
     * Ruft den Wert der enableTerminationRoamingRestrictions-Eigenschaft ab.
     * 
     */
    public boolean isEnableTerminationRoamingRestrictions() {
        return enableTerminationRoamingRestrictions;
    }

    /**
     * Legt den Wert der enableTerminationRoamingRestrictions-Eigenschaft fest.
     * 
     */
    public void setEnableTerminationRoamingRestrictions(boolean value) {
        this.enableTerminationRoamingRestrictions = value;
    }

}
