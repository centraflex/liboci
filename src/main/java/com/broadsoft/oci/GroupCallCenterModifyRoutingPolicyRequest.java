//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the group call center routing policy.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterModifyRoutingPolicyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterModifyRoutingPolicyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="routingPolicy" type="{}CallCenterRoutingPolicy" minOccurs="0"/>
 *         <element name="routingPriorityOrder" type="{}CallCenterRoutingPriorityOrder" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterModifyRoutingPolicyRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "routingPolicy",
    "routingPriorityOrder"
})
public class GroupCallCenterModifyRoutingPolicyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlSchemaType(name = "token")
    protected CallCenterRoutingPolicy routingPolicy;
    protected List<CallCenterRoutingPriorityOrder> routingPriorityOrder;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der routingPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterRoutingPolicy }
     *     
     */
    public CallCenterRoutingPolicy getRoutingPolicy() {
        return routingPolicy;
    }

    /**
     * Legt den Wert der routingPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterRoutingPolicy }
     *     
     */
    public void setRoutingPolicy(CallCenterRoutingPolicy value) {
        this.routingPolicy = value;
    }

    /**
     * Gets the value of the routingPriorityOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the routingPriorityOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRoutingPriorityOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallCenterRoutingPriorityOrder }
     * 
     * 
     * @return
     *     The value of the routingPriorityOrder property.
     */
    public List<CallCenterRoutingPriorityOrder> getRoutingPriorityOrder() {
        if (routingPriorityOrder == null) {
            routingPriorityOrder = new ArrayList<>();
        }
        return this.routingPriorityOrder;
    }

}
