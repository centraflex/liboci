//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserCallCentersLoginGetListRequest.
 *         Indicates whether the agent is current available (logged in) to each call center in the list.
 *         Contains a table with column headings: "Service User Id", "Phone Number", "Extension", "Available", "Logoff Allowed".
 *         ***** NOTE: Prior to release 14sp1, the AppServer had a bug that caused it to emit column heading "Service User ID" with
 *         *****       the ID capitalized. Beginning in release 14sp1, the appserver is compliant with the schema and emits a
 *         *****       the column heading "Service User Id".
 *         Replaced By: UserCallCenterGetResponse
 *       
 * 
 * <p>Java-Klasse für UserCallCenterGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallCenterGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="userTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallCenterGetListResponse", propOrder = {
    "userTable"
})
public class UserCallCenterGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable userTable;

    /**
     * Ruft den Wert der userTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserTable() {
        return userTable;
    }

    /**
     * Legt den Wert der userTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserTable(OCITable value) {
        this.userTable = value;
    }

}
