//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemShInterfaceRefreshTaskGetRequest.  Provides the status of the system refresh task.  If isRunning is false, numberPublicIdentityRefreshStarted and numberPublicIdentities are omitted.  If isRunning is true, numberPublicIdentities indicates the total number of public identities in the system that will be refreshed by the system refresh task; numberPublicIdentityRefreshesStarted indicates the total number of public identities for which a refresh has been started.
 *       
 * 
 * <p>Java-Klasse für SystemShInterfaceRefreshTaskGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemShInterfaceRefreshTaskGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isRunning" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="numberPublicIdentityRefreshesStarted" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         <element name="numberPublicIdentities" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemShInterfaceRefreshTaskGetResponse", propOrder = {
    "isRunning",
    "numberPublicIdentityRefreshesStarted",
    "numberPublicIdentities"
})
public class SystemShInterfaceRefreshTaskGetResponse
    extends OCIDataResponse
{

    protected boolean isRunning;
    protected Integer numberPublicIdentityRefreshesStarted;
    protected Integer numberPublicIdentities;

    /**
     * Ruft den Wert der isRunning-Eigenschaft ab.
     * 
     */
    public boolean isIsRunning() {
        return isRunning;
    }

    /**
     * Legt den Wert der isRunning-Eigenschaft fest.
     * 
     */
    public void setIsRunning(boolean value) {
        this.isRunning = value;
    }

    /**
     * Ruft den Wert der numberPublicIdentityRefreshesStarted-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberPublicIdentityRefreshesStarted() {
        return numberPublicIdentityRefreshesStarted;
    }

    /**
     * Legt den Wert der numberPublicIdentityRefreshesStarted-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberPublicIdentityRefreshesStarted(Integer value) {
        this.numberPublicIdentityRefreshesStarted = value;
    }

    /**
     * Ruft den Wert der numberPublicIdentities-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberPublicIdentities() {
        return numberPublicIdentities;
    }

    /**
     * Legt den Wert der numberPublicIdentities-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberPublicIdentities(Integer value) {
        this.numberPublicIdentities = value;
    }

}
