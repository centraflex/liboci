//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupCallCenterGetDNISAnnouncementResponse17sp4.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterGetDNISAnnouncementResponse17sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterGetDNISAnnouncementResponse17sp4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="playEntranceMessage" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="mandatoryEntranceMessage" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="entranceAudioMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="entranceMessageAudioUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="entranceMessageAudioFileList" type="{}CallCenterAnnouncementDescriptionList" minOccurs="0"/>
 *         <element name="entranceMessageAudioMediaTypeList" type="{}CallCenterAnnouncementMediaFileTypeList" minOccurs="0"/>
 *         <element name="entranceVideoMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="entranceMessageVideoUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="entranceMessageVideoFileList" type="{}CallCenterAnnouncementDescriptionList" minOccurs="0"/>
 *         <element name="entranceMessageVideoMediaTypeList" type="{}CallCenterAnnouncementMediaFileTypeList" minOccurs="0"/>
 *         <element name="playPeriodicComfortMessage" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="timeBetweenComfortMessagesSeconds" type="{}CallCenterTimeBetweenComfortMessagesSeconds"/>
 *         <element name="periodicComfortAudioMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="periodicComfortMessageAudioUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="periodicComfortMessageAudioFileList" type="{}CallCenterAnnouncementDescriptionList" minOccurs="0"/>
 *         <element name="periodicComfortMessageAudioMediaTypeList" type="{}CallCenterAnnouncementMediaFileTypeList" minOccurs="0"/>
 *         <element name="periodicComfortVideoMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="periodicComfortMessageVideoUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="periodicComfortMessageVideoFileList" type="{}CallCenterAnnouncementDescriptionList" minOccurs="0"/>
 *         <element name="periodicComfortMessageVideoMediaTypeList" type="{}CallCenterAnnouncementMediaFileTypeList" minOccurs="0"/>
 *         <element name="enableMediaOnHoldForQueuedCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="mediaOnHoldSource" type="{}CallCenterMediaOnHoldSourceRead17"/>
 *         <element name="playWhisperMessage" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="whisperAudioMessageSelection" type="{}ExtendedFileResourceSelection"/>
 *         <element name="whisperMessageAudioUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="whisperMessageAudioFileList" type="{}CallCenterAnnouncementDescriptionList" minOccurs="0"/>
 *         <element name="whisperMessageAudioMediaTypeList" type="{}CallCenterAnnouncementMediaFileTypeList" minOccurs="0"/>
 *         <element name="whisperVideoMessageSelection" type="{}ExtendedFileResourceSelection" minOccurs="0"/>
 *         <element name="whisperMessageVideoUrlList" type="{}CallCenterAnnouncementURLList" minOccurs="0"/>
 *         <element name="whisperMessageVideoFileList" type="{}CallCenterAnnouncementDescriptionList" minOccurs="0"/>
 *         <element name="whisperMessageVideoMediaTypeList" type="{}CallCenterAnnouncementMediaFileTypeList" minOccurs="0"/>
 *         <element name="estimatedWaitMessageOptionsRead" type="{}EstimatedWaitMessageOptionsRead17sp4"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterGetDNISAnnouncementResponse17sp4", propOrder = {
    "playEntranceMessage",
    "mandatoryEntranceMessage",
    "entranceAudioMessageSelection",
    "entranceMessageAudioUrlList",
    "entranceMessageAudioFileList",
    "entranceMessageAudioMediaTypeList",
    "entranceVideoMessageSelection",
    "entranceMessageVideoUrlList",
    "entranceMessageVideoFileList",
    "entranceMessageVideoMediaTypeList",
    "playPeriodicComfortMessage",
    "timeBetweenComfortMessagesSeconds",
    "periodicComfortAudioMessageSelection",
    "periodicComfortMessageAudioUrlList",
    "periodicComfortMessageAudioFileList",
    "periodicComfortMessageAudioMediaTypeList",
    "periodicComfortVideoMessageSelection",
    "periodicComfortMessageVideoUrlList",
    "periodicComfortMessageVideoFileList",
    "periodicComfortMessageVideoMediaTypeList",
    "enableMediaOnHoldForQueuedCalls",
    "mediaOnHoldSource",
    "playWhisperMessage",
    "whisperAudioMessageSelection",
    "whisperMessageAudioUrlList",
    "whisperMessageAudioFileList",
    "whisperMessageAudioMediaTypeList",
    "whisperVideoMessageSelection",
    "whisperMessageVideoUrlList",
    "whisperMessageVideoFileList",
    "whisperMessageVideoMediaTypeList",
    "estimatedWaitMessageOptionsRead"
})
public class GroupCallCenterGetDNISAnnouncementResponse17Sp4
    extends OCIDataResponse
{

    protected boolean playEntranceMessage;
    protected boolean mandatoryEntranceMessage;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection entranceAudioMessageSelection;
    protected CallCenterAnnouncementURLList entranceMessageAudioUrlList;
    protected CallCenterAnnouncementDescriptionList entranceMessageAudioFileList;
    protected CallCenterAnnouncementMediaFileTypeList entranceMessageAudioMediaTypeList;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection entranceVideoMessageSelection;
    protected CallCenterAnnouncementURLList entranceMessageVideoUrlList;
    protected CallCenterAnnouncementDescriptionList entranceMessageVideoFileList;
    protected CallCenterAnnouncementMediaFileTypeList entranceMessageVideoMediaTypeList;
    protected boolean playPeriodicComfortMessage;
    protected int timeBetweenComfortMessagesSeconds;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection periodicComfortAudioMessageSelection;
    protected CallCenterAnnouncementURLList periodicComfortMessageAudioUrlList;
    protected CallCenterAnnouncementDescriptionList periodicComfortMessageAudioFileList;
    protected CallCenterAnnouncementMediaFileTypeList periodicComfortMessageAudioMediaTypeList;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection periodicComfortVideoMessageSelection;
    protected CallCenterAnnouncementURLList periodicComfortMessageVideoUrlList;
    protected CallCenterAnnouncementDescriptionList periodicComfortMessageVideoFileList;
    protected CallCenterAnnouncementMediaFileTypeList periodicComfortMessageVideoMediaTypeList;
    protected boolean enableMediaOnHoldForQueuedCalls;
    @XmlElement(required = true)
    protected CallCenterMediaOnHoldSourceRead17 mediaOnHoldSource;
    protected boolean playWhisperMessage;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection whisperAudioMessageSelection;
    protected CallCenterAnnouncementURLList whisperMessageAudioUrlList;
    protected CallCenterAnnouncementDescriptionList whisperMessageAudioFileList;
    protected CallCenterAnnouncementMediaFileTypeList whisperMessageAudioMediaTypeList;
    @XmlSchemaType(name = "token")
    protected ExtendedFileResourceSelection whisperVideoMessageSelection;
    protected CallCenterAnnouncementURLList whisperMessageVideoUrlList;
    protected CallCenterAnnouncementDescriptionList whisperMessageVideoFileList;
    protected CallCenterAnnouncementMediaFileTypeList whisperMessageVideoMediaTypeList;
    @XmlElement(required = true)
    protected EstimatedWaitMessageOptionsRead17Sp4 estimatedWaitMessageOptionsRead;

    /**
     * Ruft den Wert der playEntranceMessage-Eigenschaft ab.
     * 
     */
    public boolean isPlayEntranceMessage() {
        return playEntranceMessage;
    }

    /**
     * Legt den Wert der playEntranceMessage-Eigenschaft fest.
     * 
     */
    public void setPlayEntranceMessage(boolean value) {
        this.playEntranceMessage = value;
    }

    /**
     * Ruft den Wert der mandatoryEntranceMessage-Eigenschaft ab.
     * 
     */
    public boolean isMandatoryEntranceMessage() {
        return mandatoryEntranceMessage;
    }

    /**
     * Legt den Wert der mandatoryEntranceMessage-Eigenschaft fest.
     * 
     */
    public void setMandatoryEntranceMessage(boolean value) {
        this.mandatoryEntranceMessage = value;
    }

    /**
     * Ruft den Wert der entranceAudioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getEntranceAudioMessageSelection() {
        return entranceAudioMessageSelection;
    }

    /**
     * Legt den Wert der entranceAudioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setEntranceAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.entranceAudioMessageSelection = value;
    }

    /**
     * Ruft den Wert der entranceMessageAudioUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getEntranceMessageAudioUrlList() {
        return entranceMessageAudioUrlList;
    }

    /**
     * Legt den Wert der entranceMessageAudioUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setEntranceMessageAudioUrlList(CallCenterAnnouncementURLList value) {
        this.entranceMessageAudioUrlList = value;
    }

    /**
     * Ruft den Wert der entranceMessageAudioFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public CallCenterAnnouncementDescriptionList getEntranceMessageAudioFileList() {
        return entranceMessageAudioFileList;
    }

    /**
     * Legt den Wert der entranceMessageAudioFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public void setEntranceMessageAudioFileList(CallCenterAnnouncementDescriptionList value) {
        this.entranceMessageAudioFileList = value;
    }

    /**
     * Ruft den Wert der entranceMessageAudioMediaTypeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public CallCenterAnnouncementMediaFileTypeList getEntranceMessageAudioMediaTypeList() {
        return entranceMessageAudioMediaTypeList;
    }

    /**
     * Legt den Wert der entranceMessageAudioMediaTypeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public void setEntranceMessageAudioMediaTypeList(CallCenterAnnouncementMediaFileTypeList value) {
        this.entranceMessageAudioMediaTypeList = value;
    }

    /**
     * Ruft den Wert der entranceVideoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getEntranceVideoMessageSelection() {
        return entranceVideoMessageSelection;
    }

    /**
     * Legt den Wert der entranceVideoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setEntranceVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.entranceVideoMessageSelection = value;
    }

    /**
     * Ruft den Wert der entranceMessageVideoUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getEntranceMessageVideoUrlList() {
        return entranceMessageVideoUrlList;
    }

    /**
     * Legt den Wert der entranceMessageVideoUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setEntranceMessageVideoUrlList(CallCenterAnnouncementURLList value) {
        this.entranceMessageVideoUrlList = value;
    }

    /**
     * Ruft den Wert der entranceMessageVideoFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public CallCenterAnnouncementDescriptionList getEntranceMessageVideoFileList() {
        return entranceMessageVideoFileList;
    }

    /**
     * Legt den Wert der entranceMessageVideoFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public void setEntranceMessageVideoFileList(CallCenterAnnouncementDescriptionList value) {
        this.entranceMessageVideoFileList = value;
    }

    /**
     * Ruft den Wert der entranceMessageVideoMediaTypeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public CallCenterAnnouncementMediaFileTypeList getEntranceMessageVideoMediaTypeList() {
        return entranceMessageVideoMediaTypeList;
    }

    /**
     * Legt den Wert der entranceMessageVideoMediaTypeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public void setEntranceMessageVideoMediaTypeList(CallCenterAnnouncementMediaFileTypeList value) {
        this.entranceMessageVideoMediaTypeList = value;
    }

    /**
     * Ruft den Wert der playPeriodicComfortMessage-Eigenschaft ab.
     * 
     */
    public boolean isPlayPeriodicComfortMessage() {
        return playPeriodicComfortMessage;
    }

    /**
     * Legt den Wert der playPeriodicComfortMessage-Eigenschaft fest.
     * 
     */
    public void setPlayPeriodicComfortMessage(boolean value) {
        this.playPeriodicComfortMessage = value;
    }

    /**
     * Ruft den Wert der timeBetweenComfortMessagesSeconds-Eigenschaft ab.
     * 
     */
    public int getTimeBetweenComfortMessagesSeconds() {
        return timeBetweenComfortMessagesSeconds;
    }

    /**
     * Legt den Wert der timeBetweenComfortMessagesSeconds-Eigenschaft fest.
     * 
     */
    public void setTimeBetweenComfortMessagesSeconds(int value) {
        this.timeBetweenComfortMessagesSeconds = value;
    }

    /**
     * Ruft den Wert der periodicComfortAudioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getPeriodicComfortAudioMessageSelection() {
        return periodicComfortAudioMessageSelection;
    }

    /**
     * Legt den Wert der periodicComfortAudioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setPeriodicComfortAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.periodicComfortAudioMessageSelection = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageAudioUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getPeriodicComfortMessageAudioUrlList() {
        return periodicComfortMessageAudioUrlList;
    }

    /**
     * Legt den Wert der periodicComfortMessageAudioUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setPeriodicComfortMessageAudioUrlList(CallCenterAnnouncementURLList value) {
        this.periodicComfortMessageAudioUrlList = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageAudioFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public CallCenterAnnouncementDescriptionList getPeriodicComfortMessageAudioFileList() {
        return periodicComfortMessageAudioFileList;
    }

    /**
     * Legt den Wert der periodicComfortMessageAudioFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public void setPeriodicComfortMessageAudioFileList(CallCenterAnnouncementDescriptionList value) {
        this.periodicComfortMessageAudioFileList = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageAudioMediaTypeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public CallCenterAnnouncementMediaFileTypeList getPeriodicComfortMessageAudioMediaTypeList() {
        return periodicComfortMessageAudioMediaTypeList;
    }

    /**
     * Legt den Wert der periodicComfortMessageAudioMediaTypeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public void setPeriodicComfortMessageAudioMediaTypeList(CallCenterAnnouncementMediaFileTypeList value) {
        this.periodicComfortMessageAudioMediaTypeList = value;
    }

    /**
     * Ruft den Wert der periodicComfortVideoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getPeriodicComfortVideoMessageSelection() {
        return periodicComfortVideoMessageSelection;
    }

    /**
     * Legt den Wert der periodicComfortVideoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setPeriodicComfortVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.periodicComfortVideoMessageSelection = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageVideoUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getPeriodicComfortMessageVideoUrlList() {
        return periodicComfortMessageVideoUrlList;
    }

    /**
     * Legt den Wert der periodicComfortMessageVideoUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setPeriodicComfortMessageVideoUrlList(CallCenterAnnouncementURLList value) {
        this.periodicComfortMessageVideoUrlList = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageVideoFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public CallCenterAnnouncementDescriptionList getPeriodicComfortMessageVideoFileList() {
        return periodicComfortMessageVideoFileList;
    }

    /**
     * Legt den Wert der periodicComfortMessageVideoFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public void setPeriodicComfortMessageVideoFileList(CallCenterAnnouncementDescriptionList value) {
        this.periodicComfortMessageVideoFileList = value;
    }

    /**
     * Ruft den Wert der periodicComfortMessageVideoMediaTypeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public CallCenterAnnouncementMediaFileTypeList getPeriodicComfortMessageVideoMediaTypeList() {
        return periodicComfortMessageVideoMediaTypeList;
    }

    /**
     * Legt den Wert der periodicComfortMessageVideoMediaTypeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public void setPeriodicComfortMessageVideoMediaTypeList(CallCenterAnnouncementMediaFileTypeList value) {
        this.periodicComfortMessageVideoMediaTypeList = value;
    }

    /**
     * Ruft den Wert der enableMediaOnHoldForQueuedCalls-Eigenschaft ab.
     * 
     */
    public boolean isEnableMediaOnHoldForQueuedCalls() {
        return enableMediaOnHoldForQueuedCalls;
    }

    /**
     * Legt den Wert der enableMediaOnHoldForQueuedCalls-Eigenschaft fest.
     * 
     */
    public void setEnableMediaOnHoldForQueuedCalls(boolean value) {
        this.enableMediaOnHoldForQueuedCalls = value;
    }

    /**
     * Ruft den Wert der mediaOnHoldSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterMediaOnHoldSourceRead17 }
     *     
     */
    public CallCenterMediaOnHoldSourceRead17 getMediaOnHoldSource() {
        return mediaOnHoldSource;
    }

    /**
     * Legt den Wert der mediaOnHoldSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterMediaOnHoldSourceRead17 }
     *     
     */
    public void setMediaOnHoldSource(CallCenterMediaOnHoldSourceRead17 value) {
        this.mediaOnHoldSource = value;
    }

    /**
     * Ruft den Wert der playWhisperMessage-Eigenschaft ab.
     * 
     */
    public boolean isPlayWhisperMessage() {
        return playWhisperMessage;
    }

    /**
     * Legt den Wert der playWhisperMessage-Eigenschaft fest.
     * 
     */
    public void setPlayWhisperMessage(boolean value) {
        this.playWhisperMessage = value;
    }

    /**
     * Ruft den Wert der whisperAudioMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getWhisperAudioMessageSelection() {
        return whisperAudioMessageSelection;
    }

    /**
     * Legt den Wert der whisperAudioMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setWhisperAudioMessageSelection(ExtendedFileResourceSelection value) {
        this.whisperAudioMessageSelection = value;
    }

    /**
     * Ruft den Wert der whisperMessageAudioUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getWhisperMessageAudioUrlList() {
        return whisperMessageAudioUrlList;
    }

    /**
     * Legt den Wert der whisperMessageAudioUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setWhisperMessageAudioUrlList(CallCenterAnnouncementURLList value) {
        this.whisperMessageAudioUrlList = value;
    }

    /**
     * Ruft den Wert der whisperMessageAudioFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public CallCenterAnnouncementDescriptionList getWhisperMessageAudioFileList() {
        return whisperMessageAudioFileList;
    }

    /**
     * Legt den Wert der whisperMessageAudioFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public void setWhisperMessageAudioFileList(CallCenterAnnouncementDescriptionList value) {
        this.whisperMessageAudioFileList = value;
    }

    /**
     * Ruft den Wert der whisperMessageAudioMediaTypeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public CallCenterAnnouncementMediaFileTypeList getWhisperMessageAudioMediaTypeList() {
        return whisperMessageAudioMediaTypeList;
    }

    /**
     * Legt den Wert der whisperMessageAudioMediaTypeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public void setWhisperMessageAudioMediaTypeList(CallCenterAnnouncementMediaFileTypeList value) {
        this.whisperMessageAudioMediaTypeList = value;
    }

    /**
     * Ruft den Wert der whisperVideoMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public ExtendedFileResourceSelection getWhisperVideoMessageSelection() {
        return whisperVideoMessageSelection;
    }

    /**
     * Legt den Wert der whisperVideoMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedFileResourceSelection }
     *     
     */
    public void setWhisperVideoMessageSelection(ExtendedFileResourceSelection value) {
        this.whisperVideoMessageSelection = value;
    }

    /**
     * Ruft den Wert der whisperMessageVideoUrlList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public CallCenterAnnouncementURLList getWhisperMessageVideoUrlList() {
        return whisperMessageVideoUrlList;
    }

    /**
     * Legt den Wert der whisperMessageVideoUrlList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementURLList }
     *     
     */
    public void setWhisperMessageVideoUrlList(CallCenterAnnouncementURLList value) {
        this.whisperMessageVideoUrlList = value;
    }

    /**
     * Ruft den Wert der whisperMessageVideoFileList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public CallCenterAnnouncementDescriptionList getWhisperMessageVideoFileList() {
        return whisperMessageVideoFileList;
    }

    /**
     * Legt den Wert der whisperMessageVideoFileList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementDescriptionList }
     *     
     */
    public void setWhisperMessageVideoFileList(CallCenterAnnouncementDescriptionList value) {
        this.whisperMessageVideoFileList = value;
    }

    /**
     * Ruft den Wert der whisperMessageVideoMediaTypeList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public CallCenterAnnouncementMediaFileTypeList getWhisperMessageVideoMediaTypeList() {
        return whisperMessageVideoMediaTypeList;
    }

    /**
     * Legt den Wert der whisperMessageVideoMediaTypeList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterAnnouncementMediaFileTypeList }
     *     
     */
    public void setWhisperMessageVideoMediaTypeList(CallCenterAnnouncementMediaFileTypeList value) {
        this.whisperMessageVideoMediaTypeList = value;
    }

    /**
     * Ruft den Wert der estimatedWaitMessageOptionsRead-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EstimatedWaitMessageOptionsRead17Sp4 }
     *     
     */
    public EstimatedWaitMessageOptionsRead17Sp4 getEstimatedWaitMessageOptionsRead() {
        return estimatedWaitMessageOptionsRead;
    }

    /**
     * Legt den Wert der estimatedWaitMessageOptionsRead-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EstimatedWaitMessageOptionsRead17Sp4 }
     *     
     */
    public void setEstimatedWaitMessageOptionsRead(EstimatedWaitMessageOptionsRead17Sp4 value) {
        this.estimatedWaitMessageOptionsRead = value;
    }

}
