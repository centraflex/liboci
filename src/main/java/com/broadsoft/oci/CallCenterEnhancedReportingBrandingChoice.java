//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterEnhancedReportingBrandingChoice.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterEnhancedReportingBrandingChoice">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="System"/>
 *     <enumeration value="Custom"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterEnhancedReportingBrandingChoice")
@XmlEnum
public enum CallCenterEnhancedReportingBrandingChoice {

    @XmlEnumValue("System")
    SYSTEM("System"),
    @XmlEnumValue("Custom")
    CUSTOM("Custom");
    private final String value;

    CallCenterEnhancedReportingBrandingChoice(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterEnhancedReportingBrandingChoice fromValue(String v) {
        for (CallCenterEnhancedReportingBrandingChoice c: CallCenterEnhancedReportingBrandingChoice.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
