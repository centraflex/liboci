//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ServiceProviderAdminType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ServiceProviderAdminType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Normal"/>
 *     <enumeration value="Customer"/>
 *     <enumeration value="Password Reset Only"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ServiceProviderAdminType")
@XmlEnum
public enum ServiceProviderAdminType {

    @XmlEnumValue("Normal")
    NORMAL("Normal"),
    @XmlEnumValue("Customer")
    CUSTOMER("Customer"),
    @XmlEnumValue("Password Reset Only")
    PASSWORD_RESET_ONLY("Password Reset Only");
    private final String value;

    ServiceProviderAdminType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ServiceProviderAdminType fromValue(String v) {
        for (ServiceProviderAdminType c: ServiceProviderAdminType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
