//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeviceActivationState.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DeviceActivationState">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Deactivated"/>
 *     <enumeration value="Activating"/>
 *     <enumeration value="Activated"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DeviceActivationState")
@XmlEnum
public enum DeviceActivationState {

    @XmlEnumValue("Deactivated")
    DEACTIVATED("Deactivated"),
    @XmlEnumValue("Activating")
    ACTIVATING("Activating"),
    @XmlEnumValue("Activated")
    ACTIVATED("Activated");
    private final String value;

    DeviceActivationState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeviceActivationState fromValue(String v) {
        for (DeviceActivationState c: DeviceActivationState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
