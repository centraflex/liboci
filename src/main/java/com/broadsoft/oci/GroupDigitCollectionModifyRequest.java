//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modifies the group's digit collection attributes.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupDigitCollectionModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDigitCollectionModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="useSetting" type="{}GroupDigitCollectionSettingLevel" minOccurs="0"/>
 *         <element name="accessCode" type="{}AccessCode" minOccurs="0"/>
 *         <element name="publicDigitMap" type="{}DigitMap" minOccurs="0"/>
 *         <element name="privateDigitMap" type="{}DigitMap" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDigitCollectionModifyRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "useSetting",
    "accessCode",
    "publicDigitMap",
    "privateDigitMap"
})
public class GroupDigitCollectionModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlSchemaType(name = "token")
    protected GroupDigitCollectionSettingLevel useSetting;
    @XmlElementRef(name = "accessCode", type = JAXBElement.class, required = false)
    protected JAXBElement<String> accessCode;
    @XmlElementRef(name = "publicDigitMap", type = JAXBElement.class, required = false)
    protected JAXBElement<String> publicDigitMap;
    @XmlElementRef(name = "privateDigitMap", type = JAXBElement.class, required = false)
    protected JAXBElement<String> privateDigitMap;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der useSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDigitCollectionSettingLevel }
     *     
     */
    public GroupDigitCollectionSettingLevel getUseSetting() {
        return useSetting;
    }

    /**
     * Legt den Wert der useSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDigitCollectionSettingLevel }
     *     
     */
    public void setUseSetting(GroupDigitCollectionSettingLevel value) {
        this.useSetting = value;
    }

    /**
     * Ruft den Wert der accessCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAccessCode() {
        return accessCode;
    }

    /**
     * Legt den Wert der accessCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAccessCode(JAXBElement<String> value) {
        this.accessCode = value;
    }

    /**
     * Ruft den Wert der publicDigitMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPublicDigitMap() {
        return publicDigitMap;
    }

    /**
     * Legt den Wert der publicDigitMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPublicDigitMap(JAXBElement<String> value) {
        this.publicDigitMap = value;
    }

    /**
     * Ruft den Wert der privateDigitMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrivateDigitMap() {
        return privateDigitMap;
    }

    /**
     * Legt den Wert der privateDigitMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrivateDigitMap(JAXBElement<String> value) {
        this.privateDigitMap = value;
    }

}
