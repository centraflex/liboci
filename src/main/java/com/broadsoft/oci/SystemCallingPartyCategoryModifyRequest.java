//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a Calling Party Category in system.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCallingPartyCategoryModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallingPartyCategoryModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="category" type="{}CallingPartyCategoryName"/>
 *         <element name="cpcValue" type="{}CallingPartyCategoryValue" minOccurs="0"/>
 *         <element name="isupOliValue" type="{}ISDNUserPartOriginatingLineInformationValue" minOccurs="0"/>
 *         <element name="gtdOliValue" type="{}ISDNGenericTransparencyDescriptorOliValue" minOccurs="0"/>
 *         <element name="userCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="payPhone" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="operator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="becomeDefault" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="collectCall" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="webDisplayKey" type="{}WebDisplayKey" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallingPartyCategoryModifyRequest", propOrder = {
    "category",
    "cpcValue",
    "isupOliValue",
    "gtdOliValue",
    "userCategory",
    "payPhone",
    "operator",
    "becomeDefault",
    "collectCall",
    "webDisplayKey"
})
public class SystemCallingPartyCategoryModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String category;
    @XmlElementRef(name = "cpcValue", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cpcValue;
    @XmlElementRef(name = "isupOliValue", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> isupOliValue;
    @XmlElementRef(name = "gtdOliValue", type = JAXBElement.class, required = false)
    protected JAXBElement<String> gtdOliValue;
    protected Boolean userCategory;
    protected Boolean payPhone;
    protected Boolean operator;
    protected Boolean becomeDefault;
    protected Boolean collectCall;
    @XmlElementRef(name = "webDisplayKey", type = JAXBElement.class, required = false)
    protected JAXBElement<String> webDisplayKey;

    /**
     * Ruft den Wert der category-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategory() {
        return category;
    }

    /**
     * Legt den Wert der category-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategory(String value) {
        this.category = value;
    }

    /**
     * Ruft den Wert der cpcValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCpcValue() {
        return cpcValue;
    }

    /**
     * Legt den Wert der cpcValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCpcValue(JAXBElement<String> value) {
        this.cpcValue = value;
    }

    /**
     * Ruft den Wert der isupOliValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getIsupOliValue() {
        return isupOliValue;
    }

    /**
     * Legt den Wert der isupOliValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setIsupOliValue(JAXBElement<Integer> value) {
        this.isupOliValue = value;
    }

    /**
     * Ruft den Wert der gtdOliValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGtdOliValue() {
        return gtdOliValue;
    }

    /**
     * Legt den Wert der gtdOliValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGtdOliValue(JAXBElement<String> value) {
        this.gtdOliValue = value;
    }

    /**
     * Ruft den Wert der userCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUserCategory() {
        return userCategory;
    }

    /**
     * Legt den Wert der userCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUserCategory(Boolean value) {
        this.userCategory = value;
    }

    /**
     * Ruft den Wert der payPhone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPayPhone() {
        return payPhone;
    }

    /**
     * Legt den Wert der payPhone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPayPhone(Boolean value) {
        this.payPhone = value;
    }

    /**
     * Ruft den Wert der operator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOperator() {
        return operator;
    }

    /**
     * Legt den Wert der operator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOperator(Boolean value) {
        this.operator = value;
    }

    /**
     * Ruft den Wert der becomeDefault-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBecomeDefault() {
        return becomeDefault;
    }

    /**
     * Legt den Wert der becomeDefault-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBecomeDefault(Boolean value) {
        this.becomeDefault = value;
    }

    /**
     * Ruft den Wert der collectCall-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCollectCall() {
        return collectCall;
    }

    /**
     * Legt den Wert der collectCall-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCollectCall(Boolean value) {
        this.collectCall = value;
    }

    /**
     * Ruft den Wert der webDisplayKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWebDisplayKey() {
        return webDisplayKey;
    }

    /**
     * Legt den Wert der webDisplayKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWebDisplayKey(JAXBElement<String> value) {
        this.webDisplayKey = value;
    }

}
