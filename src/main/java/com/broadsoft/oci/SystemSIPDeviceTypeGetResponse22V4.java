//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemSIPDeviceTypeGetRequest22V4.
 *         The following elements are only used in AS data mode:
 *           supportClientSessionInfo, value "false" is returned in XS data mode
 *           supportCallInfoConferenceSubscriptionURI, value "false" is returned in XS data mode
 *           supportRemotePartyInfo, value "false" is returned in XS data mode
 *           supportVisualDeviceManagement, value "false" is returned in XS data mode
 *           bypassMediaTreatment, value "false" is returned in XS data mode
 *           supportCauseParameter, value "false" is returned in XS data mode
 *           resellerId
 *         The following elements are only used in XS data mode:
 *           enhancedForICS, value "false" is returned in AS data mode
 *           supports3G4GContinuity, value "false" is returned in AS data mode
 *           publishesOwnPresence, value "false" is returned in AS data mode
 *           locationNetwork, value "Fixed" is returned in AS data mode
 *           allowTerminationBasedOnICSI, value "false" is returned in AS data mode
 *           roamingMode, value "None" is returned in AS data mode
 *       
 * 
 * <p>Java-Klasse für SystemSIPDeviceTypeGetResponse22V4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPDeviceTypeGetResponse22V4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isObsolete" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="numberOfPorts" type="{}UnboundedPositiveInt"/>
 *         <element name="profile" type="{}SignalingAddressType"/>
 *         <element name="registrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isConferenceDevice" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isMobilityManagerDevice" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isMusicOnHoldDevice" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="holdNormalization" type="{}HoldNormalizationMode"/>
 *         <element name="holdAnnouncementMethod" type="{}HoldAnnouncementMethodMode"/>
 *         <element name="isTrusted" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="E164Capable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="routeAdvance" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forwardingOverride" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="wirelessIntegration" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="webBasedConfigURL" type="{}WebBasedConfigURL" minOccurs="0"/>
 *         <element name="isVideoCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="PBXIntegration" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="staticRegistrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="cpeDeviceOptions" type="{}CPEDeviceOptionsRead22" minOccurs="0"/>
 *         <element name="protocolChoice" type="{}AccessDeviceProtocol" maxOccurs="unbounded"/>
 *         <element name="earlyMediaSupport" type="{}EarlyMediaSupportType"/>
 *         <element name="authenticateRefer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="autoConfigSoftClient" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="authenticationMode" type="{}AuthenticationMode22"/>
 *         <element name="requiresBroadWorksDigitCollection" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="requiresBroadWorksCallWaitingTone" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="requiresMWISubscription" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useHistoryInfoHeaderOnAccessSide" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="adviceOfChargeCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="resetEvent" type="{}AccessDeviceResetEvent" minOccurs="0"/>
 *         <element name="supportCallCenterMIMEType" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="trunkMode" type="{}TrunkMode"/>
 *         <element name="addPCalledPartyId" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="supportIdentityInUpdateAndReInvite" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="unscreenedPresentationIdentityPolicy" type="{}UnscreenedPresentationIdentityPolicy"/>
 *         <element name="enhancedForICS" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="supportEmergencyDisconnectControl" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="deviceTypeConfigurationOption" type="{}DeviceTypeConfigurationOptionType"/>
 *         <element name="supportRFC3398" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="staticLineOrdering" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="supportClientSessionInfo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="supportCallInfoConferenceSubscriptionURI" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="supportRemotePartyInfo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="supportVisualDeviceManagement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="bypassMediaTreatment" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="supports3G4GContinuity" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="publishesOwnPresence" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="supportCauseParameter" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="locationNetwork" type="{}LocationNetworkType"/>
 *         <element name="resellerId" type="{}ResellerId" minOccurs="0"/>
 *         <element name="allowTerminationBasedOnICSI" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="roamingMode" type="{}RoamingMode"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPDeviceTypeGetResponse22V4", propOrder = {
    "isObsolete",
    "numberOfPorts",
    "profile",
    "registrationCapable",
    "isConferenceDevice",
    "isMobilityManagerDevice",
    "isMusicOnHoldDevice",
    "holdNormalization",
    "holdAnnouncementMethod",
    "isTrusted",
    "e164Capable",
    "routeAdvance",
    "forwardingOverride",
    "wirelessIntegration",
    "webBasedConfigURL",
    "isVideoCapable",
    "pbxIntegration",
    "staticRegistrationCapable",
    "cpeDeviceOptions",
    "protocolChoice",
    "earlyMediaSupport",
    "authenticateRefer",
    "autoConfigSoftClient",
    "authenticationMode",
    "requiresBroadWorksDigitCollection",
    "requiresBroadWorksCallWaitingTone",
    "requiresMWISubscription",
    "useHistoryInfoHeaderOnAccessSide",
    "adviceOfChargeCapable",
    "resetEvent",
    "supportCallCenterMIMEType",
    "trunkMode",
    "addPCalledPartyId",
    "supportIdentityInUpdateAndReInvite",
    "unscreenedPresentationIdentityPolicy",
    "enhancedForICS",
    "supportEmergencyDisconnectControl",
    "deviceTypeConfigurationOption",
    "supportRFC3398",
    "staticLineOrdering",
    "supportClientSessionInfo",
    "supportCallInfoConferenceSubscriptionURI",
    "supportRemotePartyInfo",
    "supportVisualDeviceManagement",
    "bypassMediaTreatment",
    "supports3G4GContinuity",
    "publishesOwnPresence",
    "supportCauseParameter",
    "locationNetwork",
    "resellerId",
    "allowTerminationBasedOnICSI",
    "roamingMode"
})
public class SystemSIPDeviceTypeGetResponse22V4
    extends OCIDataResponse
{

    protected boolean isObsolete;
    @XmlElement(required = true)
    protected UnboundedPositiveInt numberOfPorts;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SignalingAddressType profile;
    protected boolean registrationCapable;
    protected boolean isConferenceDevice;
    protected boolean isMobilityManagerDevice;
    protected boolean isMusicOnHoldDevice;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected HoldNormalizationMode holdNormalization;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected HoldAnnouncementMethodMode holdAnnouncementMethod;
    protected boolean isTrusted;
    @XmlElement(name = "E164Capable")
    protected boolean e164Capable;
    protected boolean routeAdvance;
    protected boolean forwardingOverride;
    protected boolean wirelessIntegration;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String webBasedConfigURL;
    protected boolean isVideoCapable;
    @XmlElement(name = "PBXIntegration")
    protected boolean pbxIntegration;
    protected boolean staticRegistrationCapable;
    protected CPEDeviceOptionsRead22 cpeDeviceOptions;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> protocolChoice;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EarlyMediaSupportType earlyMediaSupport;
    protected boolean authenticateRefer;
    protected boolean autoConfigSoftClient;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AuthenticationMode22 authenticationMode;
    protected boolean requiresBroadWorksDigitCollection;
    protected boolean requiresBroadWorksCallWaitingTone;
    protected boolean requiresMWISubscription;
    protected boolean useHistoryInfoHeaderOnAccessSide;
    protected boolean adviceOfChargeCapable;
    @XmlSchemaType(name = "token")
    protected AccessDeviceResetEvent resetEvent;
    protected boolean supportCallCenterMIMEType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkMode trunkMode;
    protected boolean addPCalledPartyId;
    protected boolean supportIdentityInUpdateAndReInvite;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected UnscreenedPresentationIdentityPolicy unscreenedPresentationIdentityPolicy;
    protected boolean enhancedForICS;
    protected boolean supportEmergencyDisconnectControl;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceTypeConfigurationOptionType deviceTypeConfigurationOption;
    protected boolean supportRFC3398;
    protected boolean staticLineOrdering;
    protected boolean supportClientSessionInfo;
    protected boolean supportCallInfoConferenceSubscriptionURI;
    protected boolean supportRemotePartyInfo;
    protected boolean supportVisualDeviceManagement;
    protected boolean bypassMediaTreatment;
    protected boolean supports3G4GContinuity;
    protected boolean publishesOwnPresence;
    protected boolean supportCauseParameter;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected LocationNetworkType locationNetwork;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    protected boolean allowTerminationBasedOnICSI;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected RoamingMode roamingMode;

    /**
     * Ruft den Wert der isObsolete-Eigenschaft ab.
     * 
     */
    public boolean isIsObsolete() {
        return isObsolete;
    }

    /**
     * Legt den Wert der isObsolete-Eigenschaft fest.
     * 
     */
    public void setIsObsolete(boolean value) {
        this.isObsolete = value;
    }

    /**
     * Ruft den Wert der numberOfPorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public UnboundedPositiveInt getNumberOfPorts() {
        return numberOfPorts;
    }

    /**
     * Legt den Wert der numberOfPorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public void setNumberOfPorts(UnboundedPositiveInt value) {
        this.numberOfPorts = value;
    }

    /**
     * Ruft den Wert der profile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SignalingAddressType }
     *     
     */
    public SignalingAddressType getProfile() {
        return profile;
    }

    /**
     * Legt den Wert der profile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SignalingAddressType }
     *     
     */
    public void setProfile(SignalingAddressType value) {
        this.profile = value;
    }

    /**
     * Ruft den Wert der registrationCapable-Eigenschaft ab.
     * 
     */
    public boolean isRegistrationCapable() {
        return registrationCapable;
    }

    /**
     * Legt den Wert der registrationCapable-Eigenschaft fest.
     * 
     */
    public void setRegistrationCapable(boolean value) {
        this.registrationCapable = value;
    }

    /**
     * Ruft den Wert der isConferenceDevice-Eigenschaft ab.
     * 
     */
    public boolean isIsConferenceDevice() {
        return isConferenceDevice;
    }

    /**
     * Legt den Wert der isConferenceDevice-Eigenschaft fest.
     * 
     */
    public void setIsConferenceDevice(boolean value) {
        this.isConferenceDevice = value;
    }

    /**
     * Ruft den Wert der isMobilityManagerDevice-Eigenschaft ab.
     * 
     */
    public boolean isIsMobilityManagerDevice() {
        return isMobilityManagerDevice;
    }

    /**
     * Legt den Wert der isMobilityManagerDevice-Eigenschaft fest.
     * 
     */
    public void setIsMobilityManagerDevice(boolean value) {
        this.isMobilityManagerDevice = value;
    }

    /**
     * Ruft den Wert der isMusicOnHoldDevice-Eigenschaft ab.
     * 
     */
    public boolean isIsMusicOnHoldDevice() {
        return isMusicOnHoldDevice;
    }

    /**
     * Legt den Wert der isMusicOnHoldDevice-Eigenschaft fest.
     * 
     */
    public void setIsMusicOnHoldDevice(boolean value) {
        this.isMusicOnHoldDevice = value;
    }

    /**
     * Ruft den Wert der holdNormalization-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HoldNormalizationMode }
     *     
     */
    public HoldNormalizationMode getHoldNormalization() {
        return holdNormalization;
    }

    /**
     * Legt den Wert der holdNormalization-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HoldNormalizationMode }
     *     
     */
    public void setHoldNormalization(HoldNormalizationMode value) {
        this.holdNormalization = value;
    }

    /**
     * Ruft den Wert der holdAnnouncementMethod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HoldAnnouncementMethodMode }
     *     
     */
    public HoldAnnouncementMethodMode getHoldAnnouncementMethod() {
        return holdAnnouncementMethod;
    }

    /**
     * Legt den Wert der holdAnnouncementMethod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HoldAnnouncementMethodMode }
     *     
     */
    public void setHoldAnnouncementMethod(HoldAnnouncementMethodMode value) {
        this.holdAnnouncementMethod = value;
    }

    /**
     * Ruft den Wert der isTrusted-Eigenschaft ab.
     * 
     */
    public boolean isIsTrusted() {
        return isTrusted;
    }

    /**
     * Legt den Wert der isTrusted-Eigenschaft fest.
     * 
     */
    public void setIsTrusted(boolean value) {
        this.isTrusted = value;
    }

    /**
     * Ruft den Wert der e164Capable-Eigenschaft ab.
     * 
     */
    public boolean isE164Capable() {
        return e164Capable;
    }

    /**
     * Legt den Wert der e164Capable-Eigenschaft fest.
     * 
     */
    public void setE164Capable(boolean value) {
        this.e164Capable = value;
    }

    /**
     * Ruft den Wert der routeAdvance-Eigenschaft ab.
     * 
     */
    public boolean isRouteAdvance() {
        return routeAdvance;
    }

    /**
     * Legt den Wert der routeAdvance-Eigenschaft fest.
     * 
     */
    public void setRouteAdvance(boolean value) {
        this.routeAdvance = value;
    }

    /**
     * Ruft den Wert der forwardingOverride-Eigenschaft ab.
     * 
     */
    public boolean isForwardingOverride() {
        return forwardingOverride;
    }

    /**
     * Legt den Wert der forwardingOverride-Eigenschaft fest.
     * 
     */
    public void setForwardingOverride(boolean value) {
        this.forwardingOverride = value;
    }

    /**
     * Ruft den Wert der wirelessIntegration-Eigenschaft ab.
     * 
     */
    public boolean isWirelessIntegration() {
        return wirelessIntegration;
    }

    /**
     * Legt den Wert der wirelessIntegration-Eigenschaft fest.
     * 
     */
    public void setWirelessIntegration(boolean value) {
        this.wirelessIntegration = value;
    }

    /**
     * Ruft den Wert der webBasedConfigURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebBasedConfigURL() {
        return webBasedConfigURL;
    }

    /**
     * Legt den Wert der webBasedConfigURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebBasedConfigURL(String value) {
        this.webBasedConfigURL = value;
    }

    /**
     * Ruft den Wert der isVideoCapable-Eigenschaft ab.
     * 
     */
    public boolean isIsVideoCapable() {
        return isVideoCapable;
    }

    /**
     * Legt den Wert der isVideoCapable-Eigenschaft fest.
     * 
     */
    public void setIsVideoCapable(boolean value) {
        this.isVideoCapable = value;
    }

    /**
     * Ruft den Wert der pbxIntegration-Eigenschaft ab.
     * 
     */
    public boolean isPBXIntegration() {
        return pbxIntegration;
    }

    /**
     * Legt den Wert der pbxIntegration-Eigenschaft fest.
     * 
     */
    public void setPBXIntegration(boolean value) {
        this.pbxIntegration = value;
    }

    /**
     * Ruft den Wert der staticRegistrationCapable-Eigenschaft ab.
     * 
     */
    public boolean isStaticRegistrationCapable() {
        return staticRegistrationCapable;
    }

    /**
     * Legt den Wert der staticRegistrationCapable-Eigenschaft fest.
     * 
     */
    public void setStaticRegistrationCapable(boolean value) {
        this.staticRegistrationCapable = value;
    }

    /**
     * Ruft den Wert der cpeDeviceOptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CPEDeviceOptionsRead22 }
     *     
     */
    public CPEDeviceOptionsRead22 getCpeDeviceOptions() {
        return cpeDeviceOptions;
    }

    /**
     * Legt den Wert der cpeDeviceOptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CPEDeviceOptionsRead22 }
     *     
     */
    public void setCpeDeviceOptions(CPEDeviceOptionsRead22 value) {
        this.cpeDeviceOptions = value;
    }

    /**
     * Gets the value of the protocolChoice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the protocolChoice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProtocolChoice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the protocolChoice property.
     */
    public List<String> getProtocolChoice() {
        if (protocolChoice == null) {
            protocolChoice = new ArrayList<>();
        }
        return this.protocolChoice;
    }

    /**
     * Ruft den Wert der earlyMediaSupport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EarlyMediaSupportType }
     *     
     */
    public EarlyMediaSupportType getEarlyMediaSupport() {
        return earlyMediaSupport;
    }

    /**
     * Legt den Wert der earlyMediaSupport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EarlyMediaSupportType }
     *     
     */
    public void setEarlyMediaSupport(EarlyMediaSupportType value) {
        this.earlyMediaSupport = value;
    }

    /**
     * Ruft den Wert der authenticateRefer-Eigenschaft ab.
     * 
     */
    public boolean isAuthenticateRefer() {
        return authenticateRefer;
    }

    /**
     * Legt den Wert der authenticateRefer-Eigenschaft fest.
     * 
     */
    public void setAuthenticateRefer(boolean value) {
        this.authenticateRefer = value;
    }

    /**
     * Ruft den Wert der autoConfigSoftClient-Eigenschaft ab.
     * 
     */
    public boolean isAutoConfigSoftClient() {
        return autoConfigSoftClient;
    }

    /**
     * Legt den Wert der autoConfigSoftClient-Eigenschaft fest.
     * 
     */
    public void setAutoConfigSoftClient(boolean value) {
        this.autoConfigSoftClient = value;
    }

    /**
     * Ruft den Wert der authenticationMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationMode22 }
     *     
     */
    public AuthenticationMode22 getAuthenticationMode() {
        return authenticationMode;
    }

    /**
     * Legt den Wert der authenticationMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationMode22 }
     *     
     */
    public void setAuthenticationMode(AuthenticationMode22 value) {
        this.authenticationMode = value;
    }

    /**
     * Ruft den Wert der requiresBroadWorksDigitCollection-Eigenschaft ab.
     * 
     */
    public boolean isRequiresBroadWorksDigitCollection() {
        return requiresBroadWorksDigitCollection;
    }

    /**
     * Legt den Wert der requiresBroadWorksDigitCollection-Eigenschaft fest.
     * 
     */
    public void setRequiresBroadWorksDigitCollection(boolean value) {
        this.requiresBroadWorksDigitCollection = value;
    }

    /**
     * Ruft den Wert der requiresBroadWorksCallWaitingTone-Eigenschaft ab.
     * 
     */
    public boolean isRequiresBroadWorksCallWaitingTone() {
        return requiresBroadWorksCallWaitingTone;
    }

    /**
     * Legt den Wert der requiresBroadWorksCallWaitingTone-Eigenschaft fest.
     * 
     */
    public void setRequiresBroadWorksCallWaitingTone(boolean value) {
        this.requiresBroadWorksCallWaitingTone = value;
    }

    /**
     * Ruft den Wert der requiresMWISubscription-Eigenschaft ab.
     * 
     */
    public boolean isRequiresMWISubscription() {
        return requiresMWISubscription;
    }

    /**
     * Legt den Wert der requiresMWISubscription-Eigenschaft fest.
     * 
     */
    public void setRequiresMWISubscription(boolean value) {
        this.requiresMWISubscription = value;
    }

    /**
     * Ruft den Wert der useHistoryInfoHeaderOnAccessSide-Eigenschaft ab.
     * 
     */
    public boolean isUseHistoryInfoHeaderOnAccessSide() {
        return useHistoryInfoHeaderOnAccessSide;
    }

    /**
     * Legt den Wert der useHistoryInfoHeaderOnAccessSide-Eigenschaft fest.
     * 
     */
    public void setUseHistoryInfoHeaderOnAccessSide(boolean value) {
        this.useHistoryInfoHeaderOnAccessSide = value;
    }

    /**
     * Ruft den Wert der adviceOfChargeCapable-Eigenschaft ab.
     * 
     */
    public boolean isAdviceOfChargeCapable() {
        return adviceOfChargeCapable;
    }

    /**
     * Legt den Wert der adviceOfChargeCapable-Eigenschaft fest.
     * 
     */
    public void setAdviceOfChargeCapable(boolean value) {
        this.adviceOfChargeCapable = value;
    }

    /**
     * Ruft den Wert der resetEvent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceResetEvent }
     *     
     */
    public AccessDeviceResetEvent getResetEvent() {
        return resetEvent;
    }

    /**
     * Legt den Wert der resetEvent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceResetEvent }
     *     
     */
    public void setResetEvent(AccessDeviceResetEvent value) {
        this.resetEvent = value;
    }

    /**
     * Ruft den Wert der supportCallCenterMIMEType-Eigenschaft ab.
     * 
     */
    public boolean isSupportCallCenterMIMEType() {
        return supportCallCenterMIMEType;
    }

    /**
     * Legt den Wert der supportCallCenterMIMEType-Eigenschaft fest.
     * 
     */
    public void setSupportCallCenterMIMEType(boolean value) {
        this.supportCallCenterMIMEType = value;
    }

    /**
     * Ruft den Wert der trunkMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkMode }
     *     
     */
    public TrunkMode getTrunkMode() {
        return trunkMode;
    }

    /**
     * Legt den Wert der trunkMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkMode }
     *     
     */
    public void setTrunkMode(TrunkMode value) {
        this.trunkMode = value;
    }

    /**
     * Ruft den Wert der addPCalledPartyId-Eigenschaft ab.
     * 
     */
    public boolean isAddPCalledPartyId() {
        return addPCalledPartyId;
    }

    /**
     * Legt den Wert der addPCalledPartyId-Eigenschaft fest.
     * 
     */
    public void setAddPCalledPartyId(boolean value) {
        this.addPCalledPartyId = value;
    }

    /**
     * Ruft den Wert der supportIdentityInUpdateAndReInvite-Eigenschaft ab.
     * 
     */
    public boolean isSupportIdentityInUpdateAndReInvite() {
        return supportIdentityInUpdateAndReInvite;
    }

    /**
     * Legt den Wert der supportIdentityInUpdateAndReInvite-Eigenschaft fest.
     * 
     */
    public void setSupportIdentityInUpdateAndReInvite(boolean value) {
        this.supportIdentityInUpdateAndReInvite = value;
    }

    /**
     * Ruft den Wert der unscreenedPresentationIdentityPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnscreenedPresentationIdentityPolicy }
     *     
     */
    public UnscreenedPresentationIdentityPolicy getUnscreenedPresentationIdentityPolicy() {
        return unscreenedPresentationIdentityPolicy;
    }

    /**
     * Legt den Wert der unscreenedPresentationIdentityPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnscreenedPresentationIdentityPolicy }
     *     
     */
    public void setUnscreenedPresentationIdentityPolicy(UnscreenedPresentationIdentityPolicy value) {
        this.unscreenedPresentationIdentityPolicy = value;
    }

    /**
     * Ruft den Wert der enhancedForICS-Eigenschaft ab.
     * 
     */
    public boolean isEnhancedForICS() {
        return enhancedForICS;
    }

    /**
     * Legt den Wert der enhancedForICS-Eigenschaft fest.
     * 
     */
    public void setEnhancedForICS(boolean value) {
        this.enhancedForICS = value;
    }

    /**
     * Ruft den Wert der supportEmergencyDisconnectControl-Eigenschaft ab.
     * 
     */
    public boolean isSupportEmergencyDisconnectControl() {
        return supportEmergencyDisconnectControl;
    }

    /**
     * Legt den Wert der supportEmergencyDisconnectControl-Eigenschaft fest.
     * 
     */
    public void setSupportEmergencyDisconnectControl(boolean value) {
        this.supportEmergencyDisconnectControl = value;
    }

    /**
     * Ruft den Wert der deviceTypeConfigurationOption-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceTypeConfigurationOptionType }
     *     
     */
    public DeviceTypeConfigurationOptionType getDeviceTypeConfigurationOption() {
        return deviceTypeConfigurationOption;
    }

    /**
     * Legt den Wert der deviceTypeConfigurationOption-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceTypeConfigurationOptionType }
     *     
     */
    public void setDeviceTypeConfigurationOption(DeviceTypeConfigurationOptionType value) {
        this.deviceTypeConfigurationOption = value;
    }

    /**
     * Ruft den Wert der supportRFC3398-Eigenschaft ab.
     * 
     */
    public boolean isSupportRFC3398() {
        return supportRFC3398;
    }

    /**
     * Legt den Wert der supportRFC3398-Eigenschaft fest.
     * 
     */
    public void setSupportRFC3398(boolean value) {
        this.supportRFC3398 = value;
    }

    /**
     * Ruft den Wert der staticLineOrdering-Eigenschaft ab.
     * 
     */
    public boolean isStaticLineOrdering() {
        return staticLineOrdering;
    }

    /**
     * Legt den Wert der staticLineOrdering-Eigenschaft fest.
     * 
     */
    public void setStaticLineOrdering(boolean value) {
        this.staticLineOrdering = value;
    }

    /**
     * Ruft den Wert der supportClientSessionInfo-Eigenschaft ab.
     * 
     */
    public boolean isSupportClientSessionInfo() {
        return supportClientSessionInfo;
    }

    /**
     * Legt den Wert der supportClientSessionInfo-Eigenschaft fest.
     * 
     */
    public void setSupportClientSessionInfo(boolean value) {
        this.supportClientSessionInfo = value;
    }

    /**
     * Ruft den Wert der supportCallInfoConferenceSubscriptionURI-Eigenschaft ab.
     * 
     */
    public boolean isSupportCallInfoConferenceSubscriptionURI() {
        return supportCallInfoConferenceSubscriptionURI;
    }

    /**
     * Legt den Wert der supportCallInfoConferenceSubscriptionURI-Eigenschaft fest.
     * 
     */
    public void setSupportCallInfoConferenceSubscriptionURI(boolean value) {
        this.supportCallInfoConferenceSubscriptionURI = value;
    }

    /**
     * Ruft den Wert der supportRemotePartyInfo-Eigenschaft ab.
     * 
     */
    public boolean isSupportRemotePartyInfo() {
        return supportRemotePartyInfo;
    }

    /**
     * Legt den Wert der supportRemotePartyInfo-Eigenschaft fest.
     * 
     */
    public void setSupportRemotePartyInfo(boolean value) {
        this.supportRemotePartyInfo = value;
    }

    /**
     * Ruft den Wert der supportVisualDeviceManagement-Eigenschaft ab.
     * 
     */
    public boolean isSupportVisualDeviceManagement() {
        return supportVisualDeviceManagement;
    }

    /**
     * Legt den Wert der supportVisualDeviceManagement-Eigenschaft fest.
     * 
     */
    public void setSupportVisualDeviceManagement(boolean value) {
        this.supportVisualDeviceManagement = value;
    }

    /**
     * Ruft den Wert der bypassMediaTreatment-Eigenschaft ab.
     * 
     */
    public boolean isBypassMediaTreatment() {
        return bypassMediaTreatment;
    }

    /**
     * Legt den Wert der bypassMediaTreatment-Eigenschaft fest.
     * 
     */
    public void setBypassMediaTreatment(boolean value) {
        this.bypassMediaTreatment = value;
    }

    /**
     * Ruft den Wert der supports3G4GContinuity-Eigenschaft ab.
     * 
     */
    public boolean isSupports3G4GContinuity() {
        return supports3G4GContinuity;
    }

    /**
     * Legt den Wert der supports3G4GContinuity-Eigenschaft fest.
     * 
     */
    public void setSupports3G4GContinuity(boolean value) {
        this.supports3G4GContinuity = value;
    }

    /**
     * Ruft den Wert der publishesOwnPresence-Eigenschaft ab.
     * 
     */
    public boolean isPublishesOwnPresence() {
        return publishesOwnPresence;
    }

    /**
     * Legt den Wert der publishesOwnPresence-Eigenschaft fest.
     * 
     */
    public void setPublishesOwnPresence(boolean value) {
        this.publishesOwnPresence = value;
    }

    /**
     * Ruft den Wert der supportCauseParameter-Eigenschaft ab.
     * 
     */
    public boolean isSupportCauseParameter() {
        return supportCauseParameter;
    }

    /**
     * Legt den Wert der supportCauseParameter-Eigenschaft fest.
     * 
     */
    public void setSupportCauseParameter(boolean value) {
        this.supportCauseParameter = value;
    }

    /**
     * Ruft den Wert der locationNetwork-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocationNetworkType }
     *     
     */
    public LocationNetworkType getLocationNetwork() {
        return locationNetwork;
    }

    /**
     * Legt den Wert der locationNetwork-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationNetworkType }
     *     
     */
    public void setLocationNetwork(LocationNetworkType value) {
        this.locationNetwork = value;
    }

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Ruft den Wert der allowTerminationBasedOnICSI-Eigenschaft ab.
     * 
     */
    public boolean isAllowTerminationBasedOnICSI() {
        return allowTerminationBasedOnICSI;
    }

    /**
     * Legt den Wert der allowTerminationBasedOnICSI-Eigenschaft fest.
     * 
     */
    public void setAllowTerminationBasedOnICSI(boolean value) {
        this.allowTerminationBasedOnICSI = value;
    }

    /**
     * Ruft den Wert der roamingMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RoamingMode }
     *     
     */
    public RoamingMode getRoamingMode() {
        return roamingMode;
    }

    /**
     * Legt den Wert der roamingMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RoamingMode }
     *     
     */
    public void setRoamingMode(RoamingMode value) {
        this.roamingMode = value;
    }

}
