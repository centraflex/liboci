//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemNetworkDeviceMonitorParametersGetListRequest.
 *         Contains a list of system Network Device Polling parameters.
 *       
 * 
 * <p>Java-Klasse für SystemNetworkDeviceMonitorParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNetworkDeviceMonitorParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="pollingIntervalMinutes" type="{}NetworkDeviceMonitorPollingIntervalMinutes"/>
 *         <element name="failedPollingIntervalMinutes" type="{}NetworkDeviceMonitorFailedPollingIntervalMinutes"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNetworkDeviceMonitorParametersGetResponse", propOrder = {
    "pollingIntervalMinutes",
    "failedPollingIntervalMinutes"
})
public class SystemNetworkDeviceMonitorParametersGetResponse
    extends OCIDataResponse
{

    protected int pollingIntervalMinutes;
    protected int failedPollingIntervalMinutes;

    /**
     * Ruft den Wert der pollingIntervalMinutes-Eigenschaft ab.
     * 
     */
    public int getPollingIntervalMinutes() {
        return pollingIntervalMinutes;
    }

    /**
     * Legt den Wert der pollingIntervalMinutes-Eigenschaft fest.
     * 
     */
    public void setPollingIntervalMinutes(int value) {
        this.pollingIntervalMinutes = value;
    }

    /**
     * Ruft den Wert der failedPollingIntervalMinutes-Eigenschaft ab.
     * 
     */
    public int getFailedPollingIntervalMinutes() {
        return failedPollingIntervalMinutes;
    }

    /**
     * Legt den Wert der failedPollingIntervalMinutes-Eigenschaft fest.
     * 
     */
    public void setFailedPollingIntervalMinutes(int value) {
        this.failedPollingIntervalMinutes = value;
    }

}
