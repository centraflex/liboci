//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserPBXIntegrationGetRequest.
 *       
 * 
 * <p>Java-Klasse für UserPBXIntegrationGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPBXIntegrationGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="proxyToHeaderFromNetwork" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPBXIntegrationGetResponse", propOrder = {
    "proxyToHeaderFromNetwork"
})
public class UserPBXIntegrationGetResponse
    extends OCIDataResponse
{

    protected boolean proxyToHeaderFromNetwork;

    /**
     * Ruft den Wert der proxyToHeaderFromNetwork-Eigenschaft ab.
     * 
     */
    public boolean isProxyToHeaderFromNetwork() {
        return proxyToHeaderFromNetwork;
    }

    /**
     * Legt den Wert der proxyToHeaderFromNetwork-Eigenschaft fest.
     * 
     */
    public void setProxyToHeaderFromNetwork(boolean value) {
        this.proxyToHeaderFromNetwork = value;
    }

}
