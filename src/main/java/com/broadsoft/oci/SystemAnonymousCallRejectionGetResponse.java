//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemAnonymousCallRejectionGetRequest.
 *         The response contains the anonymous call rejection system.".        
 *       
 * 
 * <p>Java-Klasse für SystemAnonymousCallRejectionGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAnonymousCallRejectionGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="paiRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="screenOnlyLocalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAnonymousCallRejectionGetResponse", propOrder = {
    "paiRequired",
    "screenOnlyLocalCalls"
})
public class SystemAnonymousCallRejectionGetResponse
    extends OCIDataResponse
{

    protected boolean paiRequired;
    protected boolean screenOnlyLocalCalls;

    /**
     * Ruft den Wert der paiRequired-Eigenschaft ab.
     * 
     */
    public boolean isPaiRequired() {
        return paiRequired;
    }

    /**
     * Legt den Wert der paiRequired-Eigenschaft fest.
     * 
     */
    public void setPaiRequired(boolean value) {
        this.paiRequired = value;
    }

    /**
     * Ruft den Wert der screenOnlyLocalCalls-Eigenschaft ab.
     * 
     */
    public boolean isScreenOnlyLocalCalls() {
        return screenOnlyLocalCalls;
    }

    /**
     * Legt den Wert der screenOnlyLocalCalls-Eigenschaft fest.
     * 
     */
    public void setScreenOnlyLocalCalls(boolean value) {
        this.screenOnlyLocalCalls = value;
    }

}
