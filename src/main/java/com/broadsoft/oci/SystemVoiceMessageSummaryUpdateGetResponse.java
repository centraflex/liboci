//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemVoiceMessageSummaryUpdateGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemVoiceMessageSummaryUpdateGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemVoiceMessageSummaryUpdateGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="sendSavedAndUrgentMWIOnNotification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sendMessageSummaryUpdateOnRegister" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minTimeBetweenMWIOnRegister" type="{}VoiceMessageSummaryUpdateSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemVoiceMessageSummaryUpdateGetResponse", propOrder = {
    "sendSavedAndUrgentMWIOnNotification",
    "sendMessageSummaryUpdateOnRegister",
    "minTimeBetweenMWIOnRegister"
})
public class SystemVoiceMessageSummaryUpdateGetResponse
    extends OCIDataResponse
{

    protected boolean sendSavedAndUrgentMWIOnNotification;
    protected boolean sendMessageSummaryUpdateOnRegister;
    protected int minTimeBetweenMWIOnRegister;

    /**
     * Ruft den Wert der sendSavedAndUrgentMWIOnNotification-Eigenschaft ab.
     * 
     */
    public boolean isSendSavedAndUrgentMWIOnNotification() {
        return sendSavedAndUrgentMWIOnNotification;
    }

    /**
     * Legt den Wert der sendSavedAndUrgentMWIOnNotification-Eigenschaft fest.
     * 
     */
    public void setSendSavedAndUrgentMWIOnNotification(boolean value) {
        this.sendSavedAndUrgentMWIOnNotification = value;
    }

    /**
     * Ruft den Wert der sendMessageSummaryUpdateOnRegister-Eigenschaft ab.
     * 
     */
    public boolean isSendMessageSummaryUpdateOnRegister() {
        return sendMessageSummaryUpdateOnRegister;
    }

    /**
     * Legt den Wert der sendMessageSummaryUpdateOnRegister-Eigenschaft fest.
     * 
     */
    public void setSendMessageSummaryUpdateOnRegister(boolean value) {
        this.sendMessageSummaryUpdateOnRegister = value;
    }

    /**
     * Ruft den Wert der minTimeBetweenMWIOnRegister-Eigenschaft ab.
     * 
     */
    public int getMinTimeBetweenMWIOnRegister() {
        return minTimeBetweenMWIOnRegister;
    }

    /**
     * Legt den Wert der minTimeBetweenMWIOnRegister-Eigenschaft fest.
     * 
     */
    public void setMinTimeBetweenMWIOnRegister(int value) {
        this.minTimeBetweenMWIOnRegister = value;
    }

}
