//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeviceManagementEventLevel.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DeviceManagementEventLevel">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Device"/>
 *     <enumeration value="Device Type"/>
 *     <enumeration value="Device Type Group"/>
 *     <enumeration value="Group"/>
 *     <enumeration value="User"/>
 *     <enumeration value="Device Type Service Provider"/>
 *     <enumeration value="Service Provider"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DeviceManagementEventLevel")
@XmlEnum
public enum DeviceManagementEventLevel {

    @XmlEnumValue("Device")
    DEVICE("Device"),
    @XmlEnumValue("Device Type")
    DEVICE_TYPE("Device Type"),
    @XmlEnumValue("Device Type Group")
    DEVICE_TYPE_GROUP("Device Type Group"),
    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("User")
    USER("User"),
    @XmlEnumValue("Device Type Service Provider")
    DEVICE_TYPE_SERVICE_PROVIDER("Device Type Service Provider"),
    @XmlEnumValue("Service Provider")
    SERVICE_PROVIDER("Service Provider");
    private final String value;

    DeviceManagementEventLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeviceManagementEventLevel fromValue(String v) {
        for (DeviceManagementEventLevel c: DeviceManagementEventLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
