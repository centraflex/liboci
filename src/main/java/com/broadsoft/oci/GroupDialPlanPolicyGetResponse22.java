//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupDialPlanPolicyGetRequest22
 *         The following elements are only used in AS data mode:
 *           overrideResolvedDeviceDigitMap          
 *         The following elements are only used in AS data mode and not returned in XS data mode:        
 *           deviceDigitMap
 *       
 * 
 * <p>Java-Klasse für GroupDialPlanPolicyGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDialPlanPolicyGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useSetting" type="{}GroupDialPlanPolicySettingLevel"/>
 *         <element name="requiresAccessCodeForPublicCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowE164PublicCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="preferE164NumberFormatForCallbackServices" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="publicDigitMap" type="{}DigitMap" minOccurs="0"/>
 *         <element name="privateDigitMap" type="{}DigitMap" minOccurs="0"/>
 *         <element name="overrideResolvedDeviceDigitMap" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="deviceDigitMap" type="{}DigitMap" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDialPlanPolicyGetResponse22", propOrder = {
    "useSetting",
    "requiresAccessCodeForPublicCalls",
    "allowE164PublicCalls",
    "preferE164NumberFormatForCallbackServices",
    "publicDigitMap",
    "privateDigitMap",
    "overrideResolvedDeviceDigitMap",
    "deviceDigitMap"
})
public class GroupDialPlanPolicyGetResponse22
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupDialPlanPolicySettingLevel useSetting;
    protected boolean requiresAccessCodeForPublicCalls;
    protected boolean allowE164PublicCalls;
    protected boolean preferE164NumberFormatForCallbackServices;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String publicDigitMap;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String privateDigitMap;
    protected boolean overrideResolvedDeviceDigitMap;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceDigitMap;

    /**
     * Ruft den Wert der useSetting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDialPlanPolicySettingLevel }
     *     
     */
    public GroupDialPlanPolicySettingLevel getUseSetting() {
        return useSetting;
    }

    /**
     * Legt den Wert der useSetting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDialPlanPolicySettingLevel }
     *     
     */
    public void setUseSetting(GroupDialPlanPolicySettingLevel value) {
        this.useSetting = value;
    }

    /**
     * Ruft den Wert der requiresAccessCodeForPublicCalls-Eigenschaft ab.
     * 
     */
    public boolean isRequiresAccessCodeForPublicCalls() {
        return requiresAccessCodeForPublicCalls;
    }

    /**
     * Legt den Wert der requiresAccessCodeForPublicCalls-Eigenschaft fest.
     * 
     */
    public void setRequiresAccessCodeForPublicCalls(boolean value) {
        this.requiresAccessCodeForPublicCalls = value;
    }

    /**
     * Ruft den Wert der allowE164PublicCalls-Eigenschaft ab.
     * 
     */
    public boolean isAllowE164PublicCalls() {
        return allowE164PublicCalls;
    }

    /**
     * Legt den Wert der allowE164PublicCalls-Eigenschaft fest.
     * 
     */
    public void setAllowE164PublicCalls(boolean value) {
        this.allowE164PublicCalls = value;
    }

    /**
     * Ruft den Wert der preferE164NumberFormatForCallbackServices-Eigenschaft ab.
     * 
     */
    public boolean isPreferE164NumberFormatForCallbackServices() {
        return preferE164NumberFormatForCallbackServices;
    }

    /**
     * Legt den Wert der preferE164NumberFormatForCallbackServices-Eigenschaft fest.
     * 
     */
    public void setPreferE164NumberFormatForCallbackServices(boolean value) {
        this.preferE164NumberFormatForCallbackServices = value;
    }

    /**
     * Ruft den Wert der publicDigitMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPublicDigitMap() {
        return publicDigitMap;
    }

    /**
     * Legt den Wert der publicDigitMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPublicDigitMap(String value) {
        this.publicDigitMap = value;
    }

    /**
     * Ruft den Wert der privateDigitMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrivateDigitMap() {
        return privateDigitMap;
    }

    /**
     * Legt den Wert der privateDigitMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrivateDigitMap(String value) {
        this.privateDigitMap = value;
    }

    /**
     * Ruft den Wert der overrideResolvedDeviceDigitMap-Eigenschaft ab.
     * 
     */
    public boolean isOverrideResolvedDeviceDigitMap() {
        return overrideResolvedDeviceDigitMap;
    }

    /**
     * Legt den Wert der overrideResolvedDeviceDigitMap-Eigenschaft fest.
     * 
     */
    public void setOverrideResolvedDeviceDigitMap(boolean value) {
        this.overrideResolvedDeviceDigitMap = value;
    }

    /**
     * Ruft den Wert der deviceDigitMap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceDigitMap() {
        return deviceDigitMap;
    }

    /**
     * Legt den Wert der deviceDigitMap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceDigitMap(String value) {
        this.deviceDigitMap = value;
    }

}
