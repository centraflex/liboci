//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für BwDiameterPeerInstance.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="BwDiameterPeerInstance">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="XS"/>
 *     <enumeration value="PS"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "BwDiameterPeerInstance")
@XmlEnum
public enum BwDiameterPeerInstance {

    XS,
    PS;

    public String value() {
        return name();
    }

    public static BwDiameterPeerInstance fromValue(String v) {
        return valueOf(v);
    }

}
