//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Replace the list of services and packs used to select which users will be migrated.
 *         Modification is only allowed prior to task execution.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderServicePackMigrationTaskModifyUserSelectionRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderServicePackMigrationTaskModifyUserSelectionRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="taskName" type="{}ServicePackMigrationTaskName"/>
 *         <element name="userSelectionType" type="{}ServicePackMigrationTaskUserSelectionType" minOccurs="0"/>
 *         <element name="userServiceNameList" type="{}ReplacementUserServiceList" minOccurs="0"/>
 *         <element name="servicePackNameList" type="{}ReplacementServicePackNameList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderServicePackMigrationTaskModifyUserSelectionRequest", propOrder = {
    "serviceProviderId",
    "taskName",
    "userSelectionType",
    "userServiceNameList",
    "servicePackNameList"
})
public class ServiceProviderServicePackMigrationTaskModifyUserSelectionRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String taskName;
    @XmlSchemaType(name = "NMTOKEN")
    protected ServicePackMigrationTaskUserSelectionType userSelectionType;
    @XmlElementRef(name = "userServiceNameList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementUserServiceList> userServiceNameList;
    @XmlElementRef(name = "servicePackNameList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementServicePackNameList> servicePackNameList;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der taskName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * Legt den Wert der taskName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskName(String value) {
        this.taskName = value;
    }

    /**
     * Ruft den Wert der userSelectionType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServicePackMigrationTaskUserSelectionType }
     *     
     */
    public ServicePackMigrationTaskUserSelectionType getUserSelectionType() {
        return userSelectionType;
    }

    /**
     * Legt den Wert der userSelectionType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServicePackMigrationTaskUserSelectionType }
     *     
     */
    public void setUserSelectionType(ServicePackMigrationTaskUserSelectionType value) {
        this.userSelectionType = value;
    }

    /**
     * Ruft den Wert der userServiceNameList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserServiceList }{@code >}
     *     
     */
    public JAXBElement<ReplacementUserServiceList> getUserServiceNameList() {
        return userServiceNameList;
    }

    /**
     * Legt den Wert der userServiceNameList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementUserServiceList }{@code >}
     *     
     */
    public void setUserServiceNameList(JAXBElement<ReplacementUserServiceList> value) {
        this.userServiceNameList = value;
    }

    /**
     * Ruft den Wert der servicePackNameList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementServicePackNameList }{@code >}
     *     
     */
    public JAXBElement<ReplacementServicePackNameList> getServicePackNameList() {
        return servicePackNameList;
    }

    /**
     * Legt den Wert der servicePackNameList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementServicePackNameList }{@code >}
     *     
     */
    public void setServicePackNameList(JAXBElement<ReplacementServicePackNameList> value) {
        this.servicePackNameList = value;
    }

}
