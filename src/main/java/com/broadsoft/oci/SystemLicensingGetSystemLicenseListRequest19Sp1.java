//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to get the list of system licenses in the system.
 *         The response is either a SystemLicensingGetSystemLicenseListResponse21 or an ErrorResponse.
 *         
 *         Replaced by: SystemLicensingGetSystemLicenseListRequest21sp1.
 *       
 * 
 * <p>Java-Klasse für SystemLicensingGetSystemLicenseListRequest19sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemLicensingGetSystemLicenseListRequest19sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemLicensingGetSystemLicenseListRequest19sp1")
public class SystemLicensingGetSystemLicenseListRequest19Sp1
    extends OCIRequest
{


}
