//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Generate passwords based on the corresponding password/passcode rules.
 *         The response is either PasswordGenerateResponse or ErrorResponse.                           
 *       
 * 
 * <p>Java-Klasse für PasswordGenerateRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="PasswordGenerateRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="systemAdministratorPassword" type="{}PasswordForSystemAdministrator" minOccurs="0"/>
 *         <element name="serviceProviderAdministratorPassword" type="{}PasswordForServiceProviderAdministrator" minOccurs="0"/>
 *         <element name="groupAdministratorPassword" type="{}PasswordForGroupAdministrator" minOccurs="0"/>
 *         <element name="userPassword" type="{}PasswordForUser" minOccurs="0"/>
 *         <element name="accessDeviceAuthenticationPassword" type="{}PasswordForAccessDevice" minOccurs="0"/>
 *         <element name="trunkGroupAuthenticationPassword" type="{}PasswordForTrunkGroup" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PasswordGenerateRequest", propOrder = {
    "systemAdministratorPassword",
    "serviceProviderAdministratorPassword",
    "groupAdministratorPassword",
    "userPassword",
    "accessDeviceAuthenticationPassword",
    "trunkGroupAuthenticationPassword"
})
public class PasswordGenerateRequest
    extends OCIRequest
{

    protected PasswordForSystemAdministrator systemAdministratorPassword;
    protected PasswordForServiceProviderAdministrator serviceProviderAdministratorPassword;
    protected PasswordForGroupAdministrator groupAdministratorPassword;
    protected PasswordForUser userPassword;
    protected PasswordForAccessDevice accessDeviceAuthenticationPassword;
    protected PasswordForTrunkGroup trunkGroupAuthenticationPassword;

    /**
     * Ruft den Wert der systemAdministratorPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PasswordForSystemAdministrator }
     *     
     */
    public PasswordForSystemAdministrator getSystemAdministratorPassword() {
        return systemAdministratorPassword;
    }

    /**
     * Legt den Wert der systemAdministratorPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PasswordForSystemAdministrator }
     *     
     */
    public void setSystemAdministratorPassword(PasswordForSystemAdministrator value) {
        this.systemAdministratorPassword = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdministratorPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PasswordForServiceProviderAdministrator }
     *     
     */
    public PasswordForServiceProviderAdministrator getServiceProviderAdministratorPassword() {
        return serviceProviderAdministratorPassword;
    }

    /**
     * Legt den Wert der serviceProviderAdministratorPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PasswordForServiceProviderAdministrator }
     *     
     */
    public void setServiceProviderAdministratorPassword(PasswordForServiceProviderAdministrator value) {
        this.serviceProviderAdministratorPassword = value;
    }

    /**
     * Ruft den Wert der groupAdministratorPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PasswordForGroupAdministrator }
     *     
     */
    public PasswordForGroupAdministrator getGroupAdministratorPassword() {
        return groupAdministratorPassword;
    }

    /**
     * Legt den Wert der groupAdministratorPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PasswordForGroupAdministrator }
     *     
     */
    public void setGroupAdministratorPassword(PasswordForGroupAdministrator value) {
        this.groupAdministratorPassword = value;
    }

    /**
     * Ruft den Wert der userPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PasswordForUser }
     *     
     */
    public PasswordForUser getUserPassword() {
        return userPassword;
    }

    /**
     * Legt den Wert der userPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PasswordForUser }
     *     
     */
    public void setUserPassword(PasswordForUser value) {
        this.userPassword = value;
    }

    /**
     * Ruft den Wert der accessDeviceAuthenticationPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PasswordForAccessDevice }
     *     
     */
    public PasswordForAccessDevice getAccessDeviceAuthenticationPassword() {
        return accessDeviceAuthenticationPassword;
    }

    /**
     * Legt den Wert der accessDeviceAuthenticationPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PasswordForAccessDevice }
     *     
     */
    public void setAccessDeviceAuthenticationPassword(PasswordForAccessDevice value) {
        this.accessDeviceAuthenticationPassword = value;
    }

    /**
     * Ruft den Wert der trunkGroupAuthenticationPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PasswordForTrunkGroup }
     *     
     */
    public PasswordForTrunkGroup getTrunkGroupAuthenticationPassword() {
        return trunkGroupAuthenticationPassword;
    }

    /**
     * Legt den Wert der trunkGroupAuthenticationPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PasswordForTrunkGroup }
     *     
     */
    public void setTrunkGroupAuthenticationPassword(PasswordForTrunkGroup value) {
        this.trunkGroupAuthenticationPassword = value;
    }

}
