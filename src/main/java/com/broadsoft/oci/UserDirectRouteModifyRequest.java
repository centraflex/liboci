//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the direct route setting and the list of DTGs/Trunk Identities assigned to a user.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserDirectRouteModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserDirectRouteModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="outgoingDTGPolicy" type="{}DirectRouteOutgoingDTGPolicy" minOccurs="0"/>
 *         <element name="outgoingTrunkIdentityPolicy" type="{}DirectRouteOutgoingTrunkIdentityPolicy" minOccurs="0"/>
 *         <element name="directRouteIdentityList" type="{}DirectRouteReplacementIdentityList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDirectRouteModifyRequest", propOrder = {
    "userId",
    "outgoingDTGPolicy",
    "outgoingTrunkIdentityPolicy",
    "directRouteIdentityList"
})
public class UserDirectRouteModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlSchemaType(name = "token")
    protected DirectRouteOutgoingDTGPolicy outgoingDTGPolicy;
    @XmlSchemaType(name = "token")
    protected DirectRouteOutgoingTrunkIdentityPolicy outgoingTrunkIdentityPolicy;
    @XmlElementRef(name = "directRouteIdentityList", type = JAXBElement.class, required = false)
    protected JAXBElement<DirectRouteReplacementIdentityList> directRouteIdentityList;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der outgoingDTGPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DirectRouteOutgoingDTGPolicy }
     *     
     */
    public DirectRouteOutgoingDTGPolicy getOutgoingDTGPolicy() {
        return outgoingDTGPolicy;
    }

    /**
     * Legt den Wert der outgoingDTGPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DirectRouteOutgoingDTGPolicy }
     *     
     */
    public void setOutgoingDTGPolicy(DirectRouteOutgoingDTGPolicy value) {
        this.outgoingDTGPolicy = value;
    }

    /**
     * Ruft den Wert der outgoingTrunkIdentityPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DirectRouteOutgoingTrunkIdentityPolicy }
     *     
     */
    public DirectRouteOutgoingTrunkIdentityPolicy getOutgoingTrunkIdentityPolicy() {
        return outgoingTrunkIdentityPolicy;
    }

    /**
     * Legt den Wert der outgoingTrunkIdentityPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DirectRouteOutgoingTrunkIdentityPolicy }
     *     
     */
    public void setOutgoingTrunkIdentityPolicy(DirectRouteOutgoingTrunkIdentityPolicy value) {
        this.outgoingTrunkIdentityPolicy = value;
    }

    /**
     * Ruft den Wert der directRouteIdentityList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DirectRouteReplacementIdentityList }{@code >}
     *     
     */
    public JAXBElement<DirectRouteReplacementIdentityList> getDirectRouteIdentityList() {
        return directRouteIdentityList;
    }

    /**
     * Legt den Wert der directRouteIdentityList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DirectRouteReplacementIdentityList }{@code >}
     *     
     */
    public void setDirectRouteIdentityList(JAXBElement<DirectRouteReplacementIdentityList> value) {
        this.directRouteIdentityList = value;
    }

}
