//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemRuntimeDataPublicationGetRequest.
 *         The response contains the system call admission control parameters information.
 *         The following elements are only used in AS data mode:
 *           enableRuntimeDataSync, value "false" is returned in XS data mode.
 *           runtimeDataSyncIntervalInMilliSeconds value "1000" is returned in XS data mode.
 *       
 * 
 * <p>Java-Klasse für SystemRuntimeDataPublicationGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemRuntimeDataPublicationGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableRuntimeDataSync" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="runtimeDataSyncIntervalInMilliSeconds" type="{}RuntimeDataSyncIntervalInMilliSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemRuntimeDataPublicationGetResponse", propOrder = {
    "enableRuntimeDataSync",
    "runtimeDataSyncIntervalInMilliSeconds"
})
public class SystemRuntimeDataPublicationGetResponse
    extends OCIDataResponse
{

    protected boolean enableRuntimeDataSync;
    protected int runtimeDataSyncIntervalInMilliSeconds;

    /**
     * Ruft den Wert der enableRuntimeDataSync-Eigenschaft ab.
     * 
     */
    public boolean isEnableRuntimeDataSync() {
        return enableRuntimeDataSync;
    }

    /**
     * Legt den Wert der enableRuntimeDataSync-Eigenschaft fest.
     * 
     */
    public void setEnableRuntimeDataSync(boolean value) {
        this.enableRuntimeDataSync = value;
    }

    /**
     * Ruft den Wert der runtimeDataSyncIntervalInMilliSeconds-Eigenschaft ab.
     * 
     */
    public int getRuntimeDataSyncIntervalInMilliSeconds() {
        return runtimeDataSyncIntervalInMilliSeconds;
    }

    /**
     * Legt den Wert der runtimeDataSyncIntervalInMilliSeconds-Eigenschaft fest.
     * 
     */
    public void setRuntimeDataSyncIntervalInMilliSeconds(int value) {
        this.runtimeDataSyncIntervalInMilliSeconds = value;
    }

}
