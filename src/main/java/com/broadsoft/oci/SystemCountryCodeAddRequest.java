//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a country code to the system.
 *         If ringPeriodMilliseconds is not specified, a default of 6000 milliseconds is assumed.
 *         If offHookWarningTimerSeconds is not specified, a default of 30 seconds is assumed.
 *         If enableNationalPrefix is not specified, a default of false is assumed.
 *         If nationalPrefix is not specified, a default of null string is assumed.
 *         If disableNationalPrefixForOffNetCalls is not specified, a default of false is assumed.
 *         The following elements are only used in AS data mode:
 *            disableNationalPrefixForOffNetCalls.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCountryCodeAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCountryCodeAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="countryCode" type="{}CountryCode"/>
 *         <element name="countryName" type="{}CountryName"/>
 *         <element name="ringPeriodMilliseconds" type="{}CountryCodeRingPeriodMilliseconds" minOccurs="0"/>
 *         <element name="offHookWarningTimerSeconds" type="{}CountryCodeOffHookWarningTimerSeconds" minOccurs="0"/>
 *         <element name="enableNationalPrefix" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="nationalPrefix" type="{}NationalPrefix" minOccurs="0"/>
 *         <element name="maxCallWaitingTones" type="{}CountryCodeMaxCallWaitingTones" minOccurs="0"/>
 *         <element name="timeBetweenCallWaitingTonesMilliseconds" type="{}CountryCodeTimeBetweenCallWaitingTonesMilliseconds" minOccurs="0"/>
 *         <element name="disableNationalPrefixForOffNetCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCountryCodeAddRequest", propOrder = {
    "countryCode",
    "countryName",
    "ringPeriodMilliseconds",
    "offHookWarningTimerSeconds",
    "enableNationalPrefix",
    "nationalPrefix",
    "maxCallWaitingTones",
    "timeBetweenCallWaitingTonesMilliseconds",
    "disableNationalPrefixForOffNetCalls"
})
public class SystemCountryCodeAddRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NMTOKEN")
    protected String countryCode;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String countryName;
    protected Integer ringPeriodMilliseconds;
    protected Integer offHookWarningTimerSeconds;
    protected Boolean enableNationalPrefix;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String nationalPrefix;
    protected Integer maxCallWaitingTones;
    protected Integer timeBetweenCallWaitingTonesMilliseconds;
    protected Boolean disableNationalPrefixForOffNetCalls;

    /**
     * Ruft den Wert der countryCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Legt den Wert der countryCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Ruft den Wert der countryName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * Legt den Wert der countryName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryName(String value) {
        this.countryName = value;
    }

    /**
     * Ruft den Wert der ringPeriodMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRingPeriodMilliseconds() {
        return ringPeriodMilliseconds;
    }

    /**
     * Legt den Wert der ringPeriodMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRingPeriodMilliseconds(Integer value) {
        this.ringPeriodMilliseconds = value;
    }

    /**
     * Ruft den Wert der offHookWarningTimerSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOffHookWarningTimerSeconds() {
        return offHookWarningTimerSeconds;
    }

    /**
     * Legt den Wert der offHookWarningTimerSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOffHookWarningTimerSeconds(Integer value) {
        this.offHookWarningTimerSeconds = value;
    }

    /**
     * Ruft den Wert der enableNationalPrefix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableNationalPrefix() {
        return enableNationalPrefix;
    }

    /**
     * Legt den Wert der enableNationalPrefix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableNationalPrefix(Boolean value) {
        this.enableNationalPrefix = value;
    }

    /**
     * Ruft den Wert der nationalPrefix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationalPrefix() {
        return nationalPrefix;
    }

    /**
     * Legt den Wert der nationalPrefix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationalPrefix(String value) {
        this.nationalPrefix = value;
    }

    /**
     * Ruft den Wert der maxCallWaitingTones-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCallWaitingTones() {
        return maxCallWaitingTones;
    }

    /**
     * Legt den Wert der maxCallWaitingTones-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCallWaitingTones(Integer value) {
        this.maxCallWaitingTones = value;
    }

    /**
     * Ruft den Wert der timeBetweenCallWaitingTonesMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTimeBetweenCallWaitingTonesMilliseconds() {
        return timeBetweenCallWaitingTonesMilliseconds;
    }

    /**
     * Legt den Wert der timeBetweenCallWaitingTonesMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTimeBetweenCallWaitingTonesMilliseconds(Integer value) {
        this.timeBetweenCallWaitingTonesMilliseconds = value;
    }

    /**
     * Ruft den Wert der disableNationalPrefixForOffNetCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisableNationalPrefixForOffNetCalls() {
        return disableNationalPrefixForOffNetCalls;
    }

    /**
     * Legt den Wert der disableNationalPrefixForOffNetCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisableNationalPrefixForOffNetCalls(Boolean value) {
        this.disableNationalPrefixForOffNetCalls = value;
    }

}
