//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallBeingForwardedResponseCallType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallBeingForwardedResponseCallType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Never"/>
 *     <enumeration value="Internal Calls"/>
 *     <enumeration value="All Calls"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallBeingForwardedResponseCallType")
@XmlEnum
public enum CallBeingForwardedResponseCallType {

    @XmlEnumValue("Never")
    NEVER("Never"),
    @XmlEnumValue("Internal Calls")
    INTERNAL_CALLS("Internal Calls"),
    @XmlEnumValue("All Calls")
    ALL_CALLS("All Calls");
    private final String value;

    CallBeingForwardedResponseCallType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallBeingForwardedResponseCallType fromValue(String v) {
        for (CallBeingForwardedResponseCallType c: CallBeingForwardedResponseCallType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
