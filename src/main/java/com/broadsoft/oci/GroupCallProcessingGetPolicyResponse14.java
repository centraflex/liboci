//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupCallProcessingGetPolicyRequest14.
 *       
 * 
 * <p>Java-Klasse für GroupCallProcessingGetPolicyResponse14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallProcessingGetPolicyResponse14">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useGroupSetting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useMaxSimultaneousCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxSimultaneousCalls" type="{}CallProcessingMaxSimultaneousCalls"/>
 *         <element name="useMaxSimultaneousVideoCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxSimultaneousVideoCalls" type="{}CallProcessingMaxSimultaneousCalls"/>
 *         <element name="useMaxCallTimeForAnsweredCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxCallTimeForAnsweredCallsMinutes" type="{}CallProcessingMaxCallTimeForAnsweredCallsMinutes"/>
 *         <element name="useMaxCallTimeForUnansweredCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxCallTimeForUnansweredCallsMinutes" type="{}CallProcessingMaxCallTimeForUnansweredCallsMinutes"/>
 *         <element name="mediaPolicySelection" type="{}MediaPolicySelection"/>
 *         <element name="supportedMediaSetName" type="{}MediaSetName" minOccurs="0"/>
 *         <element name="networkUsageSelection" type="{}NetworkUsageSelection"/>
 *         <element name="enforceGroupCallingLineIdentityRestriction" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallProcessingGetPolicyResponse14", propOrder = {
    "useGroupSetting",
    "useMaxSimultaneousCalls",
    "maxSimultaneousCalls",
    "useMaxSimultaneousVideoCalls",
    "maxSimultaneousVideoCalls",
    "useMaxCallTimeForAnsweredCalls",
    "maxCallTimeForAnsweredCallsMinutes",
    "useMaxCallTimeForUnansweredCalls",
    "maxCallTimeForUnansweredCallsMinutes",
    "mediaPolicySelection",
    "supportedMediaSetName",
    "networkUsageSelection",
    "enforceGroupCallingLineIdentityRestriction"
})
public class GroupCallProcessingGetPolicyResponse14
    extends OCIDataResponse
{

    protected boolean useGroupSetting;
    protected boolean useMaxSimultaneousCalls;
    protected int maxSimultaneousCalls;
    protected boolean useMaxSimultaneousVideoCalls;
    protected int maxSimultaneousVideoCalls;
    protected boolean useMaxCallTimeForAnsweredCalls;
    protected int maxCallTimeForAnsweredCallsMinutes;
    protected boolean useMaxCallTimeForUnansweredCalls;
    protected int maxCallTimeForUnansweredCallsMinutes;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MediaPolicySelection mediaPolicySelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String supportedMediaSetName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected NetworkUsageSelection networkUsageSelection;
    protected boolean enforceGroupCallingLineIdentityRestriction;

    /**
     * Ruft den Wert der useGroupSetting-Eigenschaft ab.
     * 
     */
    public boolean isUseGroupSetting() {
        return useGroupSetting;
    }

    /**
     * Legt den Wert der useGroupSetting-Eigenschaft fest.
     * 
     */
    public void setUseGroupSetting(boolean value) {
        this.useGroupSetting = value;
    }

    /**
     * Ruft den Wert der useMaxSimultaneousCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxSimultaneousCalls() {
        return useMaxSimultaneousCalls;
    }

    /**
     * Legt den Wert der useMaxSimultaneousCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxSimultaneousCalls(boolean value) {
        this.useMaxSimultaneousCalls = value;
    }

    /**
     * Ruft den Wert der maxSimultaneousCalls-Eigenschaft ab.
     * 
     */
    public int getMaxSimultaneousCalls() {
        return maxSimultaneousCalls;
    }

    /**
     * Legt den Wert der maxSimultaneousCalls-Eigenschaft fest.
     * 
     */
    public void setMaxSimultaneousCalls(int value) {
        this.maxSimultaneousCalls = value;
    }

    /**
     * Ruft den Wert der useMaxSimultaneousVideoCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxSimultaneousVideoCalls() {
        return useMaxSimultaneousVideoCalls;
    }

    /**
     * Legt den Wert der useMaxSimultaneousVideoCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxSimultaneousVideoCalls(boolean value) {
        this.useMaxSimultaneousVideoCalls = value;
    }

    /**
     * Ruft den Wert der maxSimultaneousVideoCalls-Eigenschaft ab.
     * 
     */
    public int getMaxSimultaneousVideoCalls() {
        return maxSimultaneousVideoCalls;
    }

    /**
     * Legt den Wert der maxSimultaneousVideoCalls-Eigenschaft fest.
     * 
     */
    public void setMaxSimultaneousVideoCalls(int value) {
        this.maxSimultaneousVideoCalls = value;
    }

    /**
     * Ruft den Wert der useMaxCallTimeForAnsweredCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxCallTimeForAnsweredCalls() {
        return useMaxCallTimeForAnsweredCalls;
    }

    /**
     * Legt den Wert der useMaxCallTimeForAnsweredCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxCallTimeForAnsweredCalls(boolean value) {
        this.useMaxCallTimeForAnsweredCalls = value;
    }

    /**
     * Ruft den Wert der maxCallTimeForAnsweredCallsMinutes-Eigenschaft ab.
     * 
     */
    public int getMaxCallTimeForAnsweredCallsMinutes() {
        return maxCallTimeForAnsweredCallsMinutes;
    }

    /**
     * Legt den Wert der maxCallTimeForAnsweredCallsMinutes-Eigenschaft fest.
     * 
     */
    public void setMaxCallTimeForAnsweredCallsMinutes(int value) {
        this.maxCallTimeForAnsweredCallsMinutes = value;
    }

    /**
     * Ruft den Wert der useMaxCallTimeForUnansweredCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseMaxCallTimeForUnansweredCalls() {
        return useMaxCallTimeForUnansweredCalls;
    }

    /**
     * Legt den Wert der useMaxCallTimeForUnansweredCalls-Eigenschaft fest.
     * 
     */
    public void setUseMaxCallTimeForUnansweredCalls(boolean value) {
        this.useMaxCallTimeForUnansweredCalls = value;
    }

    /**
     * Ruft den Wert der maxCallTimeForUnansweredCallsMinutes-Eigenschaft ab.
     * 
     */
    public int getMaxCallTimeForUnansweredCallsMinutes() {
        return maxCallTimeForUnansweredCallsMinutes;
    }

    /**
     * Legt den Wert der maxCallTimeForUnansweredCallsMinutes-Eigenschaft fest.
     * 
     */
    public void setMaxCallTimeForUnansweredCallsMinutes(int value) {
        this.maxCallTimeForUnansweredCallsMinutes = value;
    }

    /**
     * Ruft den Wert der mediaPolicySelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MediaPolicySelection }
     *     
     */
    public MediaPolicySelection getMediaPolicySelection() {
        return mediaPolicySelection;
    }

    /**
     * Legt den Wert der mediaPolicySelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MediaPolicySelection }
     *     
     */
    public void setMediaPolicySelection(MediaPolicySelection value) {
        this.mediaPolicySelection = value;
    }

    /**
     * Ruft den Wert der supportedMediaSetName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupportedMediaSetName() {
        return supportedMediaSetName;
    }

    /**
     * Legt den Wert der supportedMediaSetName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupportedMediaSetName(String value) {
        this.supportedMediaSetName = value;
    }

    /**
     * Ruft den Wert der networkUsageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkUsageSelection }
     *     
     */
    public NetworkUsageSelection getNetworkUsageSelection() {
        return networkUsageSelection;
    }

    /**
     * Legt den Wert der networkUsageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkUsageSelection }
     *     
     */
    public void setNetworkUsageSelection(NetworkUsageSelection value) {
        this.networkUsageSelection = value;
    }

    /**
     * Ruft den Wert der enforceGroupCallingLineIdentityRestriction-Eigenschaft ab.
     * 
     */
    public boolean isEnforceGroupCallingLineIdentityRestriction() {
        return enforceGroupCallingLineIdentityRestriction;
    }

    /**
     * Legt den Wert der enforceGroupCallingLineIdentityRestriction-Eigenschaft fest.
     * 
     */
    public void setEnforceGroupCallingLineIdentityRestriction(boolean value) {
        this.enforceGroupCallingLineIdentityRestriction = value;
    }

}
