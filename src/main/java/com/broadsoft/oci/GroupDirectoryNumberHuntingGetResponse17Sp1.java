//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupDirectoryNumberHuntingGetRequest.
 *         Contains a table with column headings: "User Id", "Last Name",
 *         "First Name", "Hiragana Last Name", "Hiragana First Name",
 *         "Phone Number", "Extension", "Department", "Email Address".
 *       
 * 
 * <p>Java-Klasse für GroupDirectoryNumberHuntingGetResponse17sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDirectoryNumberHuntingGetResponse17sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="agentUserTable" type="{C}OCITable"/>
 *         <element name="useTerminateCallToAgentFirst" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useOriginalAgentServicesForBusyAndNoAnswerCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDirectoryNumberHuntingGetResponse17sp1", propOrder = {
    "agentUserTable",
    "useTerminateCallToAgentFirst",
    "useOriginalAgentServicesForBusyAndNoAnswerCalls"
})
public class GroupDirectoryNumberHuntingGetResponse17Sp1
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable agentUserTable;
    protected boolean useTerminateCallToAgentFirst;
    protected boolean useOriginalAgentServicesForBusyAndNoAnswerCalls;

    /**
     * Ruft den Wert der agentUserTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAgentUserTable() {
        return agentUserTable;
    }

    /**
     * Legt den Wert der agentUserTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAgentUserTable(OCITable value) {
        this.agentUserTable = value;
    }

    /**
     * Ruft den Wert der useTerminateCallToAgentFirst-Eigenschaft ab.
     * 
     */
    public boolean isUseTerminateCallToAgentFirst() {
        return useTerminateCallToAgentFirst;
    }

    /**
     * Legt den Wert der useTerminateCallToAgentFirst-Eigenschaft fest.
     * 
     */
    public void setUseTerminateCallToAgentFirst(boolean value) {
        this.useTerminateCallToAgentFirst = value;
    }

    /**
     * Ruft den Wert der useOriginalAgentServicesForBusyAndNoAnswerCalls-Eigenschaft ab.
     * 
     */
    public boolean isUseOriginalAgentServicesForBusyAndNoAnswerCalls() {
        return useOriginalAgentServicesForBusyAndNoAnswerCalls;
    }

    /**
     * Legt den Wert der useOriginalAgentServicesForBusyAndNoAnswerCalls-Eigenschaft fest.
     * 
     */
    public void setUseOriginalAgentServicesForBusyAndNoAnswerCalls(boolean value) {
        this.useOriginalAgentServicesForBusyAndNoAnswerCalls = value;
    }

}
