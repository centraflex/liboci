//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *       Response to SystemEnhancedCallLogsSchemaInstanceActualUsageGetListRequest.
 *       Contains a table with column headings: "Instance Name", "Active Users".     
 *     
 * 
 * <p>Java-Klasse für SystemEnhancedCallLogsActiveSchemaInstanceGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemEnhancedCallLogsActiveSchemaInstanceGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="schemaInstanceUsageTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemEnhancedCallLogsActiveSchemaInstanceGetListResponse", propOrder = {
    "schemaInstanceUsageTable"
})
public class SystemEnhancedCallLogsActiveSchemaInstanceGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable schemaInstanceUsageTable;

    /**
     * Ruft den Wert der schemaInstanceUsageTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getSchemaInstanceUsageTable() {
        return schemaInstanceUsageTable;
    }

    /**
     * Legt den Wert der schemaInstanceUsageTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setSchemaInstanceUsageTable(OCITable value) {
        this.schemaInstanceUsageTable = value;
    }

}
