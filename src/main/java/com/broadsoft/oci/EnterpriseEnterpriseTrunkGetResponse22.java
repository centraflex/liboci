//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to EnterpriseEnterpriseTrunkGetRequest22.
 *       
 * 
 * <p>Java-Klasse für EnterpriseEnterpriseTrunkGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseEnterpriseTrunkGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maximumRerouteAttempts" type="{}EnterpriseTrunkMaximumRerouteAttempts"/>
 *         <element name="routeExhaustionAction" type="{}EnterpriseTrunkRouteExhaustionAction"/>
 *         <element name="routeExhaustionForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <choice>
 *           <element name="orderedRouting">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="trunkGroup" type="{}EnterpriseTrunkTrunkGroupKey" maxOccurs="10" minOccurs="0"/>
 *                     <element name="orderingAlgorithm" type="{}EnterpriseTrunkOrderingAlgorithm"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *           <element name="priorityWeightedRouting">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="maximumRerouteAttemptsWithinPriority" type="{}EnterpriseTrunkMaximumRerouteAttempts"/>
 *                     <element name="priorityWeightedTrunkGroup" type="{}EnterpriseEnterpriseTrunkPriorityWeightedTrunkGroup" maxOccurs="100" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *         </choice>
 *         <element name="enableCapacityManagement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxActiveCalls" type="{}MaxActiveCalls" minOccurs="0"/>
 *         <element name="capacityExceededTrapInitialCalls" type="{}TrapInitialThreshold" minOccurs="0"/>
 *         <element name="capacityExceededTrapOffsetCalls" type="{}TrapOffsetThreshold" minOccurs="0"/>
 *         <element name="maximumActiveIncomingCallsAllowed" type="{}EnterpriseTrunkMaximumActiveIncomingCallsAllowed" minOccurs="0"/>
 *         <element name="maximumActiveOutgoingCallsAllowed" type="{}EnterpriseTrunkMaximumActiveOutgoingCallsAllowed" minOccurs="0"/>
 *         <element name="minimumActiveCallsReserved" type="{}EnterpriseTrunkReservedCapacity" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseEnterpriseTrunkGetResponse22", propOrder = {
    "maximumRerouteAttempts",
    "routeExhaustionAction",
    "routeExhaustionForwardAddress",
    "orderedRouting",
    "priorityWeightedRouting",
    "enableCapacityManagement",
    "maxActiveCalls",
    "capacityExceededTrapInitialCalls",
    "capacityExceededTrapOffsetCalls",
    "maximumActiveIncomingCallsAllowed",
    "maximumActiveOutgoingCallsAllowed",
    "minimumActiveCallsReserved"
})
public class EnterpriseEnterpriseTrunkGetResponse22
    extends OCIDataResponse
{

    protected int maximumRerouteAttempts;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseTrunkRouteExhaustionAction routeExhaustionAction;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String routeExhaustionForwardAddress;
    protected EnterpriseEnterpriseTrunkGetResponse22 .OrderedRouting orderedRouting;
    protected EnterpriseEnterpriseTrunkGetResponse22 .PriorityWeightedRouting priorityWeightedRouting;
    protected boolean enableCapacityManagement;
    protected Integer maxActiveCalls;
    protected Integer capacityExceededTrapInitialCalls;
    protected Integer capacityExceededTrapOffsetCalls;
    protected Integer maximumActiveIncomingCallsAllowed;
    protected Integer maximumActiveOutgoingCallsAllowed;
    protected Integer minimumActiveCallsReserved;

    /**
     * Ruft den Wert der maximumRerouteAttempts-Eigenschaft ab.
     * 
     */
    public int getMaximumRerouteAttempts() {
        return maximumRerouteAttempts;
    }

    /**
     * Legt den Wert der maximumRerouteAttempts-Eigenschaft fest.
     * 
     */
    public void setMaximumRerouteAttempts(int value) {
        this.maximumRerouteAttempts = value;
    }

    /**
     * Ruft den Wert der routeExhaustionAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseTrunkRouteExhaustionAction }
     *     
     */
    public EnterpriseTrunkRouteExhaustionAction getRouteExhaustionAction() {
        return routeExhaustionAction;
    }

    /**
     * Legt den Wert der routeExhaustionAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseTrunkRouteExhaustionAction }
     *     
     */
    public void setRouteExhaustionAction(EnterpriseTrunkRouteExhaustionAction value) {
        this.routeExhaustionAction = value;
    }

    /**
     * Ruft den Wert der routeExhaustionForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRouteExhaustionForwardAddress() {
        return routeExhaustionForwardAddress;
    }

    /**
     * Legt den Wert der routeExhaustionForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRouteExhaustionForwardAddress(String value) {
        this.routeExhaustionForwardAddress = value;
    }

    /**
     * Ruft den Wert der orderedRouting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseEnterpriseTrunkGetResponse22 .OrderedRouting }
     *     
     */
    public EnterpriseEnterpriseTrunkGetResponse22 .OrderedRouting getOrderedRouting() {
        return orderedRouting;
    }

    /**
     * Legt den Wert der orderedRouting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseEnterpriseTrunkGetResponse22 .OrderedRouting }
     *     
     */
    public void setOrderedRouting(EnterpriseEnterpriseTrunkGetResponse22 .OrderedRouting value) {
        this.orderedRouting = value;
    }

    /**
     * Ruft den Wert der priorityWeightedRouting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseEnterpriseTrunkGetResponse22 .PriorityWeightedRouting }
     *     
     */
    public EnterpriseEnterpriseTrunkGetResponse22 .PriorityWeightedRouting getPriorityWeightedRouting() {
        return priorityWeightedRouting;
    }

    /**
     * Legt den Wert der priorityWeightedRouting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseEnterpriseTrunkGetResponse22 .PriorityWeightedRouting }
     *     
     */
    public void setPriorityWeightedRouting(EnterpriseEnterpriseTrunkGetResponse22 .PriorityWeightedRouting value) {
        this.priorityWeightedRouting = value;
    }

    /**
     * Ruft den Wert der enableCapacityManagement-Eigenschaft ab.
     * 
     */
    public boolean isEnableCapacityManagement() {
        return enableCapacityManagement;
    }

    /**
     * Legt den Wert der enableCapacityManagement-Eigenschaft fest.
     * 
     */
    public void setEnableCapacityManagement(boolean value) {
        this.enableCapacityManagement = value;
    }

    /**
     * Ruft den Wert der maxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxActiveCalls() {
        return maxActiveCalls;
    }

    /**
     * Legt den Wert der maxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxActiveCalls(Integer value) {
        this.maxActiveCalls = value;
    }

    /**
     * Ruft den Wert der capacityExceededTrapInitialCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCapacityExceededTrapInitialCalls() {
        return capacityExceededTrapInitialCalls;
    }

    /**
     * Legt den Wert der capacityExceededTrapInitialCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCapacityExceededTrapInitialCalls(Integer value) {
        this.capacityExceededTrapInitialCalls = value;
    }

    /**
     * Ruft den Wert der capacityExceededTrapOffsetCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCapacityExceededTrapOffsetCalls() {
        return capacityExceededTrapOffsetCalls;
    }

    /**
     * Legt den Wert der capacityExceededTrapOffsetCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCapacityExceededTrapOffsetCalls(Integer value) {
        this.capacityExceededTrapOffsetCalls = value;
    }

    /**
     * Ruft den Wert der maximumActiveIncomingCallsAllowed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaximumActiveIncomingCallsAllowed() {
        return maximumActiveIncomingCallsAllowed;
    }

    /**
     * Legt den Wert der maximumActiveIncomingCallsAllowed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaximumActiveIncomingCallsAllowed(Integer value) {
        this.maximumActiveIncomingCallsAllowed = value;
    }

    /**
     * Ruft den Wert der maximumActiveOutgoingCallsAllowed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaximumActiveOutgoingCallsAllowed() {
        return maximumActiveOutgoingCallsAllowed;
    }

    /**
     * Legt den Wert der maximumActiveOutgoingCallsAllowed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaximumActiveOutgoingCallsAllowed(Integer value) {
        this.maximumActiveOutgoingCallsAllowed = value;
    }

    /**
     * Ruft den Wert der minimumActiveCallsReserved-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinimumActiveCallsReserved() {
        return minimumActiveCallsReserved;
    }

    /**
     * Legt den Wert der minimumActiveCallsReserved-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinimumActiveCallsReserved(Integer value) {
        this.minimumActiveCallsReserved = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="trunkGroup" type="{}EnterpriseTrunkTrunkGroupKey" maxOccurs="10" minOccurs="0"/>
     *         <element name="orderingAlgorithm" type="{}EnterpriseTrunkOrderingAlgorithm"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "trunkGroup",
        "orderingAlgorithm"
    })
    public static class OrderedRouting {

        protected List<EnterpriseTrunkTrunkGroupKey> trunkGroup;
        @XmlElement(required = true)
        @XmlSchemaType(name = "token")
        protected EnterpriseTrunkOrderingAlgorithm orderingAlgorithm;

        /**
         * Gets the value of the trunkGroup property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the trunkGroup property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTrunkGroup().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link EnterpriseTrunkTrunkGroupKey }
         * 
         * 
         * @return
         *     The value of the trunkGroup property.
         */
        public List<EnterpriseTrunkTrunkGroupKey> getTrunkGroup() {
            if (trunkGroup == null) {
                trunkGroup = new ArrayList<>();
            }
            return this.trunkGroup;
        }

        /**
         * Ruft den Wert der orderingAlgorithm-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link EnterpriseTrunkOrderingAlgorithm }
         *     
         */
        public EnterpriseTrunkOrderingAlgorithm getOrderingAlgorithm() {
            return orderingAlgorithm;
        }

        /**
         * Legt den Wert der orderingAlgorithm-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link EnterpriseTrunkOrderingAlgorithm }
         *     
         */
        public void setOrderingAlgorithm(EnterpriseTrunkOrderingAlgorithm value) {
            this.orderingAlgorithm = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="maximumRerouteAttemptsWithinPriority" type="{}EnterpriseTrunkMaximumRerouteAttempts"/>
     *         <element name="priorityWeightedTrunkGroup" type="{}EnterpriseEnterpriseTrunkPriorityWeightedTrunkGroup" maxOccurs="100" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "maximumRerouteAttemptsWithinPriority",
        "priorityWeightedTrunkGroup"
    })
    public static class PriorityWeightedRouting {

        protected int maximumRerouteAttemptsWithinPriority;
        protected List<EnterpriseEnterpriseTrunkPriorityWeightedTrunkGroup> priorityWeightedTrunkGroup;

        /**
         * Ruft den Wert der maximumRerouteAttemptsWithinPriority-Eigenschaft ab.
         * 
         */
        public int getMaximumRerouteAttemptsWithinPriority() {
            return maximumRerouteAttemptsWithinPriority;
        }

        /**
         * Legt den Wert der maximumRerouteAttemptsWithinPriority-Eigenschaft fest.
         * 
         */
        public void setMaximumRerouteAttemptsWithinPriority(int value) {
            this.maximumRerouteAttemptsWithinPriority = value;
        }

        /**
         * Gets the value of the priorityWeightedTrunkGroup property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the priorityWeightedTrunkGroup property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPriorityWeightedTrunkGroup().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link EnterpriseEnterpriseTrunkPriorityWeightedTrunkGroup }
         * 
         * 
         * @return
         *     The value of the priorityWeightedTrunkGroup property.
         */
        public List<EnterpriseEnterpriseTrunkPriorityWeightedTrunkGroup> getPriorityWeightedTrunkGroup() {
            if (priorityWeightedTrunkGroup == null) {
                priorityWeightedTrunkGroup = new ArrayList<>();
            }
            return this.priorityWeightedTrunkGroup;
        }

    }

}
