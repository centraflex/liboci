//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the BroadWorks Mobility system parameters.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         Replaced by SystemBroadWorksMobilityModifyRequest21.
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobilityModifyRequest17sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobilityModifyRequest17sp4">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enableLocationServices" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableMSRNLookup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableMobileStateChecking" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="denyCallOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="denyCallTerminations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="imrnTimeoutMilliseconds" type="{}IMRNTimeoutMilliseconds" minOccurs="0"/>
 *         <element name="scfSignalingNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="scfSignalingPort" type="{}Port" minOccurs="0"/>
 *         <element name="refreshPeriodSeconds" type="{}SCFRefreshPeriodSeconds" minOccurs="0"/>
 *         <element name="maxConsecutiveFailures" type="{}SCFMaxConsecutiveFailures" minOccurs="0"/>
 *         <element name="maxResponseWaitTimeMilliseconds" type="{}SCFMaxResponseWaitTimeMilliseconds" minOccurs="0"/>
 *         <element name="enableAnnouncementSuppression" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobilityModifyRequest17sp4", propOrder = {
    "enableLocationServices",
    "enableMSRNLookup",
    "enableMobileStateChecking",
    "denyCallOriginations",
    "denyCallTerminations",
    "imrnTimeoutMilliseconds",
    "scfSignalingNetAddress",
    "scfSignalingPort",
    "refreshPeriodSeconds",
    "maxConsecutiveFailures",
    "maxResponseWaitTimeMilliseconds",
    "enableAnnouncementSuppression"
})
public class SystemBroadWorksMobilityModifyRequest17Sp4
    extends OCIRequest
{

    protected Boolean enableLocationServices;
    protected Boolean enableMSRNLookup;
    protected Boolean enableMobileStateChecking;
    protected Boolean denyCallOriginations;
    protected Boolean denyCallTerminations;
    protected Integer imrnTimeoutMilliseconds;
    @XmlElementRef(name = "scfSignalingNetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> scfSignalingNetAddress;
    @XmlElementRef(name = "scfSignalingPort", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> scfSignalingPort;
    protected Integer refreshPeriodSeconds;
    protected Integer maxConsecutiveFailures;
    protected Integer maxResponseWaitTimeMilliseconds;
    protected Boolean enableAnnouncementSuppression;

    /**
     * Ruft den Wert der enableLocationServices-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableLocationServices() {
        return enableLocationServices;
    }

    /**
     * Legt den Wert der enableLocationServices-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableLocationServices(Boolean value) {
        this.enableLocationServices = value;
    }

    /**
     * Ruft den Wert der enableMSRNLookup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMSRNLookup() {
        return enableMSRNLookup;
    }

    /**
     * Legt den Wert der enableMSRNLookup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMSRNLookup(Boolean value) {
        this.enableMSRNLookup = value;
    }

    /**
     * Ruft den Wert der enableMobileStateChecking-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMobileStateChecking() {
        return enableMobileStateChecking;
    }

    /**
     * Legt den Wert der enableMobileStateChecking-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMobileStateChecking(Boolean value) {
        this.enableMobileStateChecking = value;
    }

    /**
     * Ruft den Wert der denyCallOriginations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDenyCallOriginations() {
        return denyCallOriginations;
    }

    /**
     * Legt den Wert der denyCallOriginations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDenyCallOriginations(Boolean value) {
        this.denyCallOriginations = value;
    }

    /**
     * Ruft den Wert der denyCallTerminations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDenyCallTerminations() {
        return denyCallTerminations;
    }

    /**
     * Legt den Wert der denyCallTerminations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDenyCallTerminations(Boolean value) {
        this.denyCallTerminations = value;
    }

    /**
     * Ruft den Wert der imrnTimeoutMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getImrnTimeoutMilliseconds() {
        return imrnTimeoutMilliseconds;
    }

    /**
     * Legt den Wert der imrnTimeoutMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setImrnTimeoutMilliseconds(Integer value) {
        this.imrnTimeoutMilliseconds = value;
    }

    /**
     * Ruft den Wert der scfSignalingNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getScfSignalingNetAddress() {
        return scfSignalingNetAddress;
    }

    /**
     * Legt den Wert der scfSignalingNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setScfSignalingNetAddress(JAXBElement<String> value) {
        this.scfSignalingNetAddress = value;
    }

    /**
     * Ruft den Wert der scfSignalingPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getScfSignalingPort() {
        return scfSignalingPort;
    }

    /**
     * Legt den Wert der scfSignalingPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setScfSignalingPort(JAXBElement<Integer> value) {
        this.scfSignalingPort = value;
    }

    /**
     * Ruft den Wert der refreshPeriodSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRefreshPeriodSeconds() {
        return refreshPeriodSeconds;
    }

    /**
     * Legt den Wert der refreshPeriodSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRefreshPeriodSeconds(Integer value) {
        this.refreshPeriodSeconds = value;
    }

    /**
     * Ruft den Wert der maxConsecutiveFailures-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxConsecutiveFailures() {
        return maxConsecutiveFailures;
    }

    /**
     * Legt den Wert der maxConsecutiveFailures-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxConsecutiveFailures(Integer value) {
        this.maxConsecutiveFailures = value;
    }

    /**
     * Ruft den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxResponseWaitTimeMilliseconds() {
        return maxResponseWaitTimeMilliseconds;
    }

    /**
     * Legt den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxResponseWaitTimeMilliseconds(Integer value) {
        this.maxResponseWaitTimeMilliseconds = value;
    }

    /**
     * Ruft den Wert der enableAnnouncementSuppression-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableAnnouncementSuppression() {
        return enableAnnouncementSuppression;
    }

    /**
     * Legt den Wert der enableAnnouncementSuppression-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableAnnouncementSuppression(Boolean value) {
        this.enableAnnouncementSuppression = value;
    }

}
