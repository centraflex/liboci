//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemProvisioningValidationGetRequest14sp2.
 *       
 * 
 * <p>Java-Klasse für SystemProvisioningValidationGetResponse14sp2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemProvisioningValidationGetResponse14sp2">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isNetworkServerQueryActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="timeoutSeconds" type="{}ProvisioningValidationTimeoutSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemProvisioningValidationGetResponse14sp2", propOrder = {
    "isActive",
    "isNetworkServerQueryActive",
    "timeoutSeconds"
})
public class SystemProvisioningValidationGetResponse14Sp2
    extends OCIDataResponse
{

    protected boolean isActive;
    protected boolean isNetworkServerQueryActive;
    protected int timeoutSeconds;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der isNetworkServerQueryActive-Eigenschaft ab.
     * 
     */
    public boolean isIsNetworkServerQueryActive() {
        return isNetworkServerQueryActive;
    }

    /**
     * Legt den Wert der isNetworkServerQueryActive-Eigenschaft fest.
     * 
     */
    public void setIsNetworkServerQueryActive(boolean value) {
        this.isNetworkServerQueryActive = value;
    }

    /**
     * Ruft den Wert der timeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getTimeoutSeconds() {
        return timeoutSeconds;
    }

    /**
     * Legt den Wert der timeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setTimeoutSeconds(int value) {
        this.timeoutSeconds = value;
    }

}
