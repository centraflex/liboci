//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify push notification system parameters.
 *         
 *         The following elements are only used in AS data mode and ignored in the Amplify data mode:
 *           subscriptionEventsPerSecond
 *         
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemPushNotificationParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPushNotificationParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enforceAllowedApplicationList" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maximumRegistrationsPerUser" type="{}PushNotificationMaximumRegistrationsPerUser" minOccurs="0"/>
 *         <element name="maximumRegistrationAgeDays" type="{}PushNotificationMaximumRegistrationAgeDays" minOccurs="0"/>
 *         <element name="newCallTimeout" type="{}PushNotificationNewCallTimeout" minOccurs="0"/>
 *         <element name="subscriptionEventsPerSecond" type="{}PushNotificationSubscriptionEventsPerSecond" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPushNotificationParametersModifyRequest", propOrder = {
    "enforceAllowedApplicationList",
    "maximumRegistrationsPerUser",
    "maximumRegistrationAgeDays",
    "newCallTimeout",
    "subscriptionEventsPerSecond"
})
public class SystemPushNotificationParametersModifyRequest
    extends OCIRequest
{

    protected Boolean enforceAllowedApplicationList;
    protected Integer maximumRegistrationsPerUser;
    protected Integer maximumRegistrationAgeDays;
    protected Integer newCallTimeout;
    protected Integer subscriptionEventsPerSecond;

    /**
     * Ruft den Wert der enforceAllowedApplicationList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnforceAllowedApplicationList() {
        return enforceAllowedApplicationList;
    }

    /**
     * Legt den Wert der enforceAllowedApplicationList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnforceAllowedApplicationList(Boolean value) {
        this.enforceAllowedApplicationList = value;
    }

    /**
     * Ruft den Wert der maximumRegistrationsPerUser-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaximumRegistrationsPerUser() {
        return maximumRegistrationsPerUser;
    }

    /**
     * Legt den Wert der maximumRegistrationsPerUser-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaximumRegistrationsPerUser(Integer value) {
        this.maximumRegistrationsPerUser = value;
    }

    /**
     * Ruft den Wert der maximumRegistrationAgeDays-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaximumRegistrationAgeDays() {
        return maximumRegistrationAgeDays;
    }

    /**
     * Legt den Wert der maximumRegistrationAgeDays-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaximumRegistrationAgeDays(Integer value) {
        this.maximumRegistrationAgeDays = value;
    }

    /**
     * Ruft den Wert der newCallTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNewCallTimeout() {
        return newCallTimeout;
    }

    /**
     * Legt den Wert der newCallTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNewCallTimeout(Integer value) {
        this.newCallTimeout = value;
    }

    /**
     * Ruft den Wert der subscriptionEventsPerSecond-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSubscriptionEventsPerSecond() {
        return subscriptionEventsPerSecond;
    }

    /**
     * Legt den Wert der subscriptionEventsPerSecond-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSubscriptionEventsPerSecond(Integer value) {
        this.subscriptionEventsPerSecond = value;
    }

}
