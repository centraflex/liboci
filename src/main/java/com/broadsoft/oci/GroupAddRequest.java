//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a group.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         The following elements are only used in Amplify data mode and ignored in AS and XS data mode:
 *         servicePolicy,
 *         callProcessingSliceId, 
 *         provisioningSliceId, 
 *         subscriberPartition.
 *         When the callProcessingSliceId or provisioningSliceId is not specified in the AmplifyDataMode, 
 *         the default slice Id is assigned to the Group.
 *         Only Provisioning admin and above can change the callProcessingSliceId,  
 *         provisioningSliceId, and subscriberPartition.
 *        
 *         The following elements are only used in Amplify and XS data mode and ignored in AS mode:
 *         preferredDataCenter.
 *         Only Provisioning admin and above can change the preferredDataCenter.
 *         
 *         The following elements are only used in XS data mode and ignored in Amplify and AS data mode:
 *         defaultUserCallingLineIdPhoneNumber.
 *         
 *         Replaced by: GroupConsolidatedAddRequest in AS data mode
 *         
 *       
 * 
 * <p>Java-Klasse für GroupAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="defaultDomain" type="{}NetAddress"/>
 *         <element name="userLimit" type="{}GroupUserLimit"/>
 *         <element name="groupName" type="{}GroupName" minOccurs="0"/>
 *         <element name="callingLineIdName" type="{}GroupCallingLineIdName" minOccurs="0"/>
 *         <element name="timeZone" type="{}TimeZone" minOccurs="0"/>
 *         <element name="locationDialingCode" type="{}LocationDialingCode" minOccurs="0"/>
 *         <element name="contact" type="{}Contact" minOccurs="0"/>
 *         <element name="address" type="{}StreetAddress" minOccurs="0"/>
 *         <element name="servicePolicy" type="{}ServicePolicyName" minOccurs="0"/>
 *         <element name="callProcessingSliceId" type="{}SliceId" minOccurs="0"/>
 *         <element name="provisioningSliceId" type="{}SliceId" minOccurs="0"/>
 *         <element name="subscriberPartition" type="{}SubscriberPartition" minOccurs="0"/>
 *         <element name="preferredDataCenter" type="{}DataCenter" minOccurs="0"/>
 *         <element name="defaultUserCallingLineIdPhoneNumber" type="{}DN" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAddRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "defaultDomain",
    "userLimit",
    "groupName",
    "callingLineIdName",
    "timeZone",
    "locationDialingCode",
    "contact",
    "address",
    "servicePolicy",
    "callProcessingSliceId",
    "provisioningSliceId",
    "subscriberPartition",
    "preferredDataCenter",
    "defaultUserCallingLineIdPhoneNumber"
})
public class GroupAddRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultDomain;
    protected int userLimit;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeZone;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String locationDialingCode;
    protected Contact contact;
    protected StreetAddress address;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String servicePolicy;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callProcessingSliceId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String provisioningSliceId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String subscriberPartition;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String preferredDataCenter;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultUserCallingLineIdPhoneNumber;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der defaultDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultDomain() {
        return defaultDomain;
    }

    /**
     * Legt den Wert der defaultDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultDomain(String value) {
        this.defaultDomain = value;
    }

    /**
     * Ruft den Wert der userLimit-Eigenschaft ab.
     * 
     */
    public int getUserLimit() {
        return userLimit;
    }

    /**
     * Legt den Wert der userLimit-Eigenschaft fest.
     * 
     */
    public void setUserLimit(int value) {
        this.userLimit = value;
    }

    /**
     * Ruft den Wert der groupName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Legt den Wert der groupName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupName(String value) {
        this.groupName = value;
    }

    /**
     * Ruft den Wert der callingLineIdName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdName() {
        return callingLineIdName;
    }

    /**
     * Legt den Wert der callingLineIdName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdName(String value) {
        this.callingLineIdName = value;
    }

    /**
     * Ruft den Wert der timeZone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * Legt den Wert der timeZone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeZone(String value) {
        this.timeZone = value;
    }

    /**
     * Ruft den Wert der locationDialingCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationDialingCode() {
        return locationDialingCode;
    }

    /**
     * Legt den Wert der locationDialingCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationDialingCode(String value) {
        this.locationDialingCode = value;
    }

    /**
     * Ruft den Wert der contact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Contact }
     *     
     */
    public Contact getContact() {
        return contact;
    }

    /**
     * Legt den Wert der contact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Contact }
     *     
     */
    public void setContact(Contact value) {
        this.contact = value;
    }

    /**
     * Ruft den Wert der address-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StreetAddress }
     *     
     */
    public StreetAddress getAddress() {
        return address;
    }

    /**
     * Legt den Wert der address-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetAddress }
     *     
     */
    public void setAddress(StreetAddress value) {
        this.address = value;
    }

    /**
     * Ruft den Wert der servicePolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicePolicy() {
        return servicePolicy;
    }

    /**
     * Legt den Wert der servicePolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicePolicy(String value) {
        this.servicePolicy = value;
    }

    /**
     * Ruft den Wert der callProcessingSliceId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallProcessingSliceId() {
        return callProcessingSliceId;
    }

    /**
     * Legt den Wert der callProcessingSliceId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallProcessingSliceId(String value) {
        this.callProcessingSliceId = value;
    }

    /**
     * Ruft den Wert der provisioningSliceId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvisioningSliceId() {
        return provisioningSliceId;
    }

    /**
     * Legt den Wert der provisioningSliceId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvisioningSliceId(String value) {
        this.provisioningSliceId = value;
    }

    /**
     * Ruft den Wert der subscriberPartition-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberPartition() {
        return subscriberPartition;
    }

    /**
     * Legt den Wert der subscriberPartition-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberPartition(String value) {
        this.subscriberPartition = value;
    }

    /**
     * Ruft den Wert der preferredDataCenter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredDataCenter() {
        return preferredDataCenter;
    }

    /**
     * Legt den Wert der preferredDataCenter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredDataCenter(String value) {
        this.preferredDataCenter = value;
    }

    /**
     * Ruft den Wert der defaultUserCallingLineIdPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultUserCallingLineIdPhoneNumber() {
        return defaultUserCallingLineIdPhoneNumber;
    }

    /**
     * Legt den Wert der defaultUserCallingLineIdPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultUserCallingLineIdPhoneNumber(String value) {
        this.defaultUserCallingLineIdPhoneNumber = value;
    }

}
