//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für OutgoingPinholeDigitPlanOriginatingPermission.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="OutgoingPinholeDigitPlanOriginatingPermission">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Ignore"/>
 *     <enumeration value="Allow"/>
 *     <enumeration value="Authorization Code Required"/>
 *     <enumeration value="Transfer To First Transfer Number"/>
 *     <enumeration value="Transfer To Second Transfer Number"/>
 *     <enumeration value="Transfer To Third Transfer Number"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "OutgoingPinholeDigitPlanOriginatingPermission")
@XmlEnum
public enum OutgoingPinholeDigitPlanOriginatingPermission {

    @XmlEnumValue("Ignore")
    IGNORE("Ignore"),
    @XmlEnumValue("Allow")
    ALLOW("Allow"),
    @XmlEnumValue("Authorization Code Required")
    AUTHORIZATION_CODE_REQUIRED("Authorization Code Required"),
    @XmlEnumValue("Transfer To First Transfer Number")
    TRANSFER_TO_FIRST_TRANSFER_NUMBER("Transfer To First Transfer Number"),
    @XmlEnumValue("Transfer To Second Transfer Number")
    TRANSFER_TO_SECOND_TRANSFER_NUMBER("Transfer To Second Transfer Number"),
    @XmlEnumValue("Transfer To Third Transfer Number")
    TRANSFER_TO_THIRD_TRANSFER_NUMBER("Transfer To Third Transfer Number");
    private final String value;

    OutgoingPinholeDigitPlanOriginatingPermission(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OutgoingPinholeDigitPlanOriginatingPermission fromValue(String v) {
        for (OutgoingPinholeDigitPlanOriginatingPermission c: OutgoingPinholeDigitPlanOriginatingPermission.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
