//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a Auto Attendant instance to a group and assign services to the Auto Attendant.
 *         The domain is required in the serviceUserId.
 *         Only Group and Enterprise level schedules are accepted.
 *         The response is either SuccessResponse or ErrorResponse.
 * 
 *         If the phoneNumber has not been assigned to the group and addPhoneNumberToGroup is set to true,
 *         it will be added to group if the command is executed by a service provider administrator or above
 *         and the number is already assigned to the service provider. The command will fail otherwise.
 *         
 *         The following elements are only used in AS data mode:
 *           type, use AutoAttendantType.BASIC in XS mode.
 *           holidayMenu.
 *         The following elements are only valid for Standard Auto
 *         Attendants:
 *           holidayMenu
 *       
 * 
 * <p>Java-Klasse für GroupAutoAttendantConsolidatedAddInstanceRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAutoAttendantConsolidatedAddInstanceRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="addPhoneNumberToGroup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceAddProfile"/>
 *         <element name="type" type="{}AutoAttendantType"/>
 *         <element name="firstDigitTimeoutSeconds" type="{}FirstDigitTimoutSeconds"/>
 *         <element name="enableVideo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="businessHours" type="{}TimeSchedule" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}HolidaySchedule" minOccurs="0"/>
 *         <element name="extensionDialingScope" type="{}AutoAttendantDialingScope"/>
 *         <element name="nameDialingScope" type="{}AutoAttendantDialingScope"/>
 *         <element name="nameDialingEntries" type="{}AutoAttendantNameDialingEntry"/>
 *         <element name="businessHoursMenu" type="{}AutoAttendantAddMenu20" minOccurs="0"/>
 *         <element name="afterHoursMenu" type="{}AutoAttendantAddMenu20" minOccurs="0"/>
 *         <element name="holidayMenu" type="{}AutoAttendantAddMenu20" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="service" type="{}ConsolidatedUserServiceAssignment" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAutoAttendantConsolidatedAddInstanceRequest", propOrder = {
    "serviceProviderId",
    "groupId",
    "serviceUserId",
    "addPhoneNumberToGroup",
    "serviceInstanceProfile",
    "type",
    "firstDigitTimeoutSeconds",
    "enableVideo",
    "businessHours",
    "holidaySchedule",
    "extensionDialingScope",
    "nameDialingScope",
    "nameDialingEntries",
    "businessHoursMenu",
    "afterHoursMenu",
    "holidayMenu",
    "networkClassOfService",
    "service",
    "isActive"
})
public class GroupAutoAttendantConsolidatedAddInstanceRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected Boolean addPhoneNumberToGroup;
    @XmlElement(required = true)
    protected ServiceInstanceAddProfile serviceInstanceProfile;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AutoAttendantType type;
    protected int firstDigitTimeoutSeconds;
    protected boolean enableVideo;
    protected TimeSchedule businessHours;
    protected HolidaySchedule holidaySchedule;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AutoAttendantDialingScope extensionDialingScope;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AutoAttendantDialingScope nameDialingScope;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AutoAttendantNameDialingEntry nameDialingEntries;
    protected AutoAttendantAddMenu20 businessHoursMenu;
    protected AutoAttendantAddMenu20 afterHoursMenu;
    protected AutoAttendantAddMenu20 holidayMenu;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    protected List<ConsolidatedUserServiceAssignment> service;
    protected boolean isActive;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der addPhoneNumberToGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAddPhoneNumberToGroup() {
        return addPhoneNumberToGroup;
    }

    /**
     * Legt den Wert der addPhoneNumberToGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAddPhoneNumberToGroup(Boolean value) {
        this.addPhoneNumberToGroup = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceAddProfile }
     *     
     */
    public ServiceInstanceAddProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceAddProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceAddProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantType }
     *     
     */
    public AutoAttendantType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantType }
     *     
     */
    public void setType(AutoAttendantType value) {
        this.type = value;
    }

    /**
     * Ruft den Wert der firstDigitTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getFirstDigitTimeoutSeconds() {
        return firstDigitTimeoutSeconds;
    }

    /**
     * Legt den Wert der firstDigitTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setFirstDigitTimeoutSeconds(int value) {
        this.firstDigitTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der enableVideo-Eigenschaft ab.
     * 
     */
    public boolean isEnableVideo() {
        return enableVideo;
    }

    /**
     * Legt den Wert der enableVideo-Eigenschaft fest.
     * 
     */
    public void setEnableVideo(boolean value) {
        this.enableVideo = value;
    }

    /**
     * Ruft den Wert der businessHours-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeSchedule }
     *     
     */
    public TimeSchedule getBusinessHours() {
        return businessHours;
    }

    /**
     * Legt den Wert der businessHours-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeSchedule }
     *     
     */
    public void setBusinessHours(TimeSchedule value) {
        this.businessHours = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HolidaySchedule }
     *     
     */
    public HolidaySchedule getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HolidaySchedule }
     *     
     */
    public void setHolidaySchedule(HolidaySchedule value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der extensionDialingScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public AutoAttendantDialingScope getExtensionDialingScope() {
        return extensionDialingScope;
    }

    /**
     * Legt den Wert der extensionDialingScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public void setExtensionDialingScope(AutoAttendantDialingScope value) {
        this.extensionDialingScope = value;
    }

    /**
     * Ruft den Wert der nameDialingScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public AutoAttendantDialingScope getNameDialingScope() {
        return nameDialingScope;
    }

    /**
     * Legt den Wert der nameDialingScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public void setNameDialingScope(AutoAttendantDialingScope value) {
        this.nameDialingScope = value;
    }

    /**
     * Ruft den Wert der nameDialingEntries-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantNameDialingEntry }
     *     
     */
    public AutoAttendantNameDialingEntry getNameDialingEntries() {
        return nameDialingEntries;
    }

    /**
     * Legt den Wert der nameDialingEntries-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantNameDialingEntry }
     *     
     */
    public void setNameDialingEntries(AutoAttendantNameDialingEntry value) {
        this.nameDialingEntries = value;
    }

    /**
     * Ruft den Wert der businessHoursMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantAddMenu20 }
     *     
     */
    public AutoAttendantAddMenu20 getBusinessHoursMenu() {
        return businessHoursMenu;
    }

    /**
     * Legt den Wert der businessHoursMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantAddMenu20 }
     *     
     */
    public void setBusinessHoursMenu(AutoAttendantAddMenu20 value) {
        this.businessHoursMenu = value;
    }

    /**
     * Ruft den Wert der afterHoursMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantAddMenu20 }
     *     
     */
    public AutoAttendantAddMenu20 getAfterHoursMenu() {
        return afterHoursMenu;
    }

    /**
     * Legt den Wert der afterHoursMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantAddMenu20 }
     *     
     */
    public void setAfterHoursMenu(AutoAttendantAddMenu20 value) {
        this.afterHoursMenu = value;
    }

    /**
     * Ruft den Wert der holidayMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantAddMenu20 }
     *     
     */
    public AutoAttendantAddMenu20 getHolidayMenu() {
        return holidayMenu;
    }

    /**
     * Legt den Wert der holidayMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantAddMenu20 }
     *     
     */
    public void setHolidayMenu(AutoAttendantAddMenu20 value) {
        this.holidayMenu = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Gets the value of the service property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the service property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConsolidatedUserServiceAssignment }
     * 
     * 
     * @return
     *     The value of the service property.
     */
    public List<ConsolidatedUserServiceAssignment> getService() {
        if (service == null) {
            service = new ArrayList<>();
        }
        return this.service;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

}
