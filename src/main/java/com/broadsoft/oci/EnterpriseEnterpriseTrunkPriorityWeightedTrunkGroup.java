//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Trunk group details (order and weight) for each trunk group
 *       
 * 
 * <p>Java-Klasse für EnterpriseEnterpriseTrunkPriorityWeightedTrunkGroup complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseEnterpriseTrunkPriorityWeightedTrunkGroup">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="trunkGroup" type="{}EnterpriseTrunkTrunkGroupKey"/>
 *         <element name="priority" type="{}EnterpriseTrunkTrunkGroupPriority"/>
 *         <element name="weight" type="{}EnterpriseTrunkTrunkGroupWeight"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseEnterpriseTrunkPriorityWeightedTrunkGroup", propOrder = {
    "trunkGroup",
    "priority",
    "weight"
})
public class EnterpriseEnterpriseTrunkPriorityWeightedTrunkGroup {

    @XmlElement(required = true)
    protected EnterpriseTrunkTrunkGroupKey trunkGroup;
    protected int priority;
    protected int weight;

    /**
     * Ruft den Wert der trunkGroup-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseTrunkTrunkGroupKey }
     *     
     */
    public EnterpriseTrunkTrunkGroupKey getTrunkGroup() {
        return trunkGroup;
    }

    /**
     * Legt den Wert der trunkGroup-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseTrunkTrunkGroupKey }
     *     
     */
    public void setTrunkGroup(EnterpriseTrunkTrunkGroupKey value) {
        this.trunkGroup = value;
    }

    /**
     * Ruft den Wert der priority-Eigenschaft ab.
     * 
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Legt den Wert der priority-Eigenschaft fest.
     * 
     */
    public void setPriority(int value) {
        this.priority = value;
    }

    /**
     * Ruft den Wert der weight-Eigenschaft ab.
     * 
     */
    public int getWeight() {
        return weight;
    }

    /**
     * Legt den Wert der weight-Eigenschaft fest.
     * 
     */
    public void setWeight(int value) {
        this.weight = value;
    }

}
