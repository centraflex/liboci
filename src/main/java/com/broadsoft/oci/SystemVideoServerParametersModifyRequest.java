//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify video server system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemVideoServerParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemVideoServerParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="videoServerResponseTimerMilliseconds" type="{}VideoServerResponseTimerMilliseconds" minOccurs="0"/>
 *         <element name="videoServerSelectionRouteTimerMilliseconds" type="{}VideoServerSelectionRouteTimerMilliseconds" minOccurs="0"/>
 *         <element name="useStaticVideoServerDevice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemVideoServerParametersModifyRequest", propOrder = {
    "videoServerResponseTimerMilliseconds",
    "videoServerSelectionRouteTimerMilliseconds",
    "useStaticVideoServerDevice"
})
public class SystemVideoServerParametersModifyRequest
    extends OCIRequest
{

    protected Integer videoServerResponseTimerMilliseconds;
    protected Integer videoServerSelectionRouteTimerMilliseconds;
    protected Boolean useStaticVideoServerDevice;

    /**
     * Ruft den Wert der videoServerResponseTimerMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVideoServerResponseTimerMilliseconds() {
        return videoServerResponseTimerMilliseconds;
    }

    /**
     * Legt den Wert der videoServerResponseTimerMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVideoServerResponseTimerMilliseconds(Integer value) {
        this.videoServerResponseTimerMilliseconds = value;
    }

    /**
     * Ruft den Wert der videoServerSelectionRouteTimerMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVideoServerSelectionRouteTimerMilliseconds() {
        return videoServerSelectionRouteTimerMilliseconds;
    }

    /**
     * Legt den Wert der videoServerSelectionRouteTimerMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVideoServerSelectionRouteTimerMilliseconds(Integer value) {
        this.videoServerSelectionRouteTimerMilliseconds = value;
    }

    /**
     * Ruft den Wert der useStaticVideoServerDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseStaticVideoServerDevice() {
        return useStaticVideoServerDevice;
    }

    /**
     * Legt den Wert der useStaticVideoServerDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseStaticVideoServerDevice(Boolean value) {
        this.useStaticVideoServerDevice = value;
    }

}
