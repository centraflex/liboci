//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Replaced by: SystemRedundancyParametersGetResponse16sp2
 *       
 *         Response to SystemRedundancyParametersGetRequest.
 *         Contains a list of system Redundancy parameters.
 *       
 * 
 * <p>Java-Klasse für SystemRedundancyParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemRedundancyParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="rollBackTimerMinutes" type="{}RedundancyRollBackTimerMinutes"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemRedundancyParametersGetResponse", propOrder = {
    "rollBackTimerMinutes"
})
public class SystemRedundancyParametersGetResponse
    extends OCIDataResponse
{

    protected int rollBackTimerMinutes;

    /**
     * Ruft den Wert der rollBackTimerMinutes-Eigenschaft ab.
     * 
     */
    public int getRollBackTimerMinutes() {
        return rollBackTimerMinutes;
    }

    /**
     * Legt den Wert der rollBackTimerMinutes-Eigenschaft fest.
     * 
     */
    public void setRollBackTimerMinutes(int value) {
        this.rollBackTimerMinutes = value;
    }

}
