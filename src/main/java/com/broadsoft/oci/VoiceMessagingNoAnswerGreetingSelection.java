//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für VoiceMessagingNoAnswerGreetingSelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="VoiceMessagingNoAnswerGreetingSelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Default"/>
 *     <enumeration value="Personal"/>
 *     <enumeration value="Alternate01"/>
 *     <enumeration value="Alternate02"/>
 *     <enumeration value="Alternate03"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "VoiceMessagingNoAnswerGreetingSelection")
@XmlEnum
public enum VoiceMessagingNoAnswerGreetingSelection {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("Personal")
    PERSONAL("Personal"),
    @XmlEnumValue("Alternate01")
    ALTERNATE_01("Alternate01"),
    @XmlEnumValue("Alternate02")
    ALTERNATE_02("Alternate02"),
    @XmlEnumValue("Alternate03")
    ALTERNATE_03("Alternate03");
    private final String value;

    VoiceMessagingNoAnswerGreetingSelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VoiceMessagingNoAnswerGreetingSelection fromValue(String v) {
        for (VoiceMessagingNoAnswerGreetingSelection c: VoiceMessagingNoAnswerGreetingSelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
