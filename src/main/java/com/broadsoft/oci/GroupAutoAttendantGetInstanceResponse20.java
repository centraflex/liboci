//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupAutoAttendantGetInstanceRequest20.
 *         Contains the service profile information.
 * 
 *         
 *         The following elements are only used in AS data mode:
 *           type, use AutoAttendantType.BASIC in XS mode.
 *           holidayMenu.
 *         The following elements are only valid for Standard Auto
 *         Attendants:
 *           holidayMenu
 *       
 * 
 * <p>Java-Klasse für GroupAutoAttendantGetInstanceResponse20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAutoAttendantGetInstanceResponse20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceReadProfile19sp1"/>
 *         <element name="type" type="{}AutoAttendantType"/>
 *         <element name="firstDigitTimeoutSeconds" type="{}FirstDigitTimoutSeconds"/>
 *         <element name="enableVideo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="businessHours" type="{}TimeSchedule" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}HolidaySchedule" minOccurs="0"/>
 *         <element name="extensionDialingScope" type="{}AutoAttendantDialingScope"/>
 *         <element name="nameDialingScope" type="{}AutoAttendantDialingScope"/>
 *         <element name="nameDialingEntries" type="{}AutoAttendantNameDialingEntry"/>
 *         <element name="businessHoursMenu" type="{}AutoAttendantReadMenu20"/>
 *         <element name="afterHoursMenu" type="{}AutoAttendantReadMenu20"/>
 *         <element name="holidayMenu" type="{}AutoAttendantReadMenu20" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAutoAttendantGetInstanceResponse20", propOrder = {
    "serviceInstanceProfile",
    "type",
    "firstDigitTimeoutSeconds",
    "enableVideo",
    "businessHours",
    "holidaySchedule",
    "extensionDialingScope",
    "nameDialingScope",
    "nameDialingEntries",
    "businessHoursMenu",
    "afterHoursMenu",
    "holidayMenu",
    "networkClassOfService"
})
public class GroupAutoAttendantGetInstanceResponse20
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected ServiceInstanceReadProfile19Sp1 serviceInstanceProfile;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AutoAttendantType type;
    protected int firstDigitTimeoutSeconds;
    protected boolean enableVideo;
    protected TimeSchedule businessHours;
    protected HolidaySchedule holidaySchedule;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AutoAttendantDialingScope extensionDialingScope;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AutoAttendantDialingScope nameDialingScope;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AutoAttendantNameDialingEntry nameDialingEntries;
    @XmlElement(required = true)
    protected AutoAttendantReadMenu20 businessHoursMenu;
    @XmlElement(required = true)
    protected AutoAttendantReadMenu20 afterHoursMenu;
    protected AutoAttendantReadMenu20 holidayMenu;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceReadProfile19Sp1 }
     *     
     */
    public ServiceInstanceReadProfile19Sp1 getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceReadProfile19Sp1 }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceReadProfile19Sp1 value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantType }
     *     
     */
    public AutoAttendantType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantType }
     *     
     */
    public void setType(AutoAttendantType value) {
        this.type = value;
    }

    /**
     * Ruft den Wert der firstDigitTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getFirstDigitTimeoutSeconds() {
        return firstDigitTimeoutSeconds;
    }

    /**
     * Legt den Wert der firstDigitTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setFirstDigitTimeoutSeconds(int value) {
        this.firstDigitTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der enableVideo-Eigenschaft ab.
     * 
     */
    public boolean isEnableVideo() {
        return enableVideo;
    }

    /**
     * Legt den Wert der enableVideo-Eigenschaft fest.
     * 
     */
    public void setEnableVideo(boolean value) {
        this.enableVideo = value;
    }

    /**
     * Ruft den Wert der businessHours-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeSchedule }
     *     
     */
    public TimeSchedule getBusinessHours() {
        return businessHours;
    }

    /**
     * Legt den Wert der businessHours-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeSchedule }
     *     
     */
    public void setBusinessHours(TimeSchedule value) {
        this.businessHours = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HolidaySchedule }
     *     
     */
    public HolidaySchedule getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HolidaySchedule }
     *     
     */
    public void setHolidaySchedule(HolidaySchedule value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der extensionDialingScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public AutoAttendantDialingScope getExtensionDialingScope() {
        return extensionDialingScope;
    }

    /**
     * Legt den Wert der extensionDialingScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public void setExtensionDialingScope(AutoAttendantDialingScope value) {
        this.extensionDialingScope = value;
    }

    /**
     * Ruft den Wert der nameDialingScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public AutoAttendantDialingScope getNameDialingScope() {
        return nameDialingScope;
    }

    /**
     * Legt den Wert der nameDialingScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantDialingScope }
     *     
     */
    public void setNameDialingScope(AutoAttendantDialingScope value) {
        this.nameDialingScope = value;
    }

    /**
     * Ruft den Wert der nameDialingEntries-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantNameDialingEntry }
     *     
     */
    public AutoAttendantNameDialingEntry getNameDialingEntries() {
        return nameDialingEntries;
    }

    /**
     * Legt den Wert der nameDialingEntries-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantNameDialingEntry }
     *     
     */
    public void setNameDialingEntries(AutoAttendantNameDialingEntry value) {
        this.nameDialingEntries = value;
    }

    /**
     * Ruft den Wert der businessHoursMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantReadMenu20 }
     *     
     */
    public AutoAttendantReadMenu20 getBusinessHoursMenu() {
        return businessHoursMenu;
    }

    /**
     * Legt den Wert der businessHoursMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantReadMenu20 }
     *     
     */
    public void setBusinessHoursMenu(AutoAttendantReadMenu20 value) {
        this.businessHoursMenu = value;
    }

    /**
     * Ruft den Wert der afterHoursMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantReadMenu20 }
     *     
     */
    public AutoAttendantReadMenu20 getAfterHoursMenu() {
        return afterHoursMenu;
    }

    /**
     * Legt den Wert der afterHoursMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantReadMenu20 }
     *     
     */
    public void setAfterHoursMenu(AutoAttendantReadMenu20 value) {
        this.afterHoursMenu = value;
    }

    /**
     * Ruft den Wert der holidayMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AutoAttendantReadMenu20 }
     *     
     */
    public AutoAttendantReadMenu20 getHolidayMenu() {
        return holidayMenu;
    }

    /**
     * Legt den Wert der holidayMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoAttendantReadMenu20 }
     *     
     */
    public void setHolidayMenu(AutoAttendantReadMenu20 value) {
        this.holidayMenu = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

}
