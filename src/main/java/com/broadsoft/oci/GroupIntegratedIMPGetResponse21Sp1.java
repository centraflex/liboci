//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupIntegratedIMPGetRequest21sp1.
 *         The response contains the group Integrated IMP service attributes.
 *       
 * 
 * <p>Java-Klasse für GroupIntegratedIMPGetResponse21sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupIntegratedIMPGetResponse21sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useServiceProviderSetting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="serviceDomain" type="{}DomainName" minOccurs="0"/>
 *         <element name="effectiveServiceDomain" type="{}DomainName" minOccurs="0"/>
 *         <element name="addServiceProviderInIMPUserId" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="defaultImpIdType" type="{}IntegratedIMPUserIDType"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupIntegratedIMPGetResponse21sp1", propOrder = {
    "useServiceProviderSetting",
    "serviceDomain",
    "effectiveServiceDomain",
    "addServiceProviderInIMPUserId",
    "defaultImpIdType"
})
public class GroupIntegratedIMPGetResponse21Sp1
    extends OCIDataResponse
{

    protected boolean useServiceProviderSetting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceDomain;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String effectiveServiceDomain;
    protected boolean addServiceProviderInIMPUserId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected IntegratedIMPUserIDType defaultImpIdType;

    /**
     * Ruft den Wert der useServiceProviderSetting-Eigenschaft ab.
     * 
     */
    public boolean isUseServiceProviderSetting() {
        return useServiceProviderSetting;
    }

    /**
     * Legt den Wert der useServiceProviderSetting-Eigenschaft fest.
     * 
     */
    public void setUseServiceProviderSetting(boolean value) {
        this.useServiceProviderSetting = value;
    }

    /**
     * Ruft den Wert der serviceDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceDomain() {
        return serviceDomain;
    }

    /**
     * Legt den Wert der serviceDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceDomain(String value) {
        this.serviceDomain = value;
    }

    /**
     * Ruft den Wert der effectiveServiceDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectiveServiceDomain() {
        return effectiveServiceDomain;
    }

    /**
     * Legt den Wert der effectiveServiceDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectiveServiceDomain(String value) {
        this.effectiveServiceDomain = value;
    }

    /**
     * Ruft den Wert der addServiceProviderInIMPUserId-Eigenschaft ab.
     * 
     */
    public boolean isAddServiceProviderInIMPUserId() {
        return addServiceProviderInIMPUserId;
    }

    /**
     * Legt den Wert der addServiceProviderInIMPUserId-Eigenschaft fest.
     * 
     */
    public void setAddServiceProviderInIMPUserId(boolean value) {
        this.addServiceProviderInIMPUserId = value;
    }

    /**
     * Ruft den Wert der defaultImpIdType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IntegratedIMPUserIDType }
     *     
     */
    public IntegratedIMPUserIDType getDefaultImpIdType() {
        return defaultImpIdType;
    }

    /**
     * Legt den Wert der defaultImpIdType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IntegratedIMPUserIDType }
     *     
     */
    public void setDefaultImpIdType(IntegratedIMPUserIDType value) {
        this.defaultImpIdType = value;
    }

}
