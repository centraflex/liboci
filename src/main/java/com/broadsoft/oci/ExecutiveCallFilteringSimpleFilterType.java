//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ExecutiveCallFilteringSimpleFilterType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ExecutiveCallFilteringSimpleFilterType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="All Calls"/>
 *     <enumeration value="All Internal Calls"/>
 *     <enumeration value="All External Calls"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ExecutiveCallFilteringSimpleFilterType")
@XmlEnum
public enum ExecutiveCallFilteringSimpleFilterType {

    @XmlEnumValue("All Calls")
    ALL_CALLS("All Calls"),
    @XmlEnumValue("All Internal Calls")
    ALL_INTERNAL_CALLS("All Internal Calls"),
    @XmlEnumValue("All External Calls")
    ALL_EXTERNAL_CALLS("All External Calls");
    private final String value;

    ExecutiveCallFilteringSimpleFilterType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExecutiveCallFilteringSimpleFilterType fromValue(String v) {
        for (ExecutiveCallFilteringSimpleFilterType c: ExecutiveCallFilteringSimpleFilterType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
