//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a Call Recording platform.
 *         The first system level call recording platform added is made the system default
 *         call recording platform.  The first reseller level call recording platform added
 *         for a reseller is made the reseller default call recording platform.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         Replaced by: SystemCallRecordingAddPlatformRequest22
 *       
 * 
 * <p>Java-Klasse für SystemCallRecordingAddPlatformRequest20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallRecordingAddPlatformRequest20">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="name" type="{}CallRecordingPlatformName"/>
 *         <element name="netAddress" type="{}NetAddress"/>
 *         <element name="port" type="{}Port" minOccurs="0"/>
 *         <element name="mediaStream" type="{}MediaStream"/>
 *         <element name="transportType" type="{}TransportProtocol"/>
 *         <element name="description" type="{}CallRecordingPlatformDescription" minOccurs="0"/>
 *         <element name="schemaVersion" type="{}CallRecordingPlatformSchemaVersion"/>
 *         <element name="supportVideoRecording" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="resellerId" type="{}ResellerId" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallRecordingAddPlatformRequest20", propOrder = {
    "name",
    "netAddress",
    "port",
    "mediaStream",
    "transportType",
    "description",
    "schemaVersion",
    "supportVideoRecording",
    "resellerId"
})
public class SystemCallRecordingAddPlatformRequest20
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String netAddress;
    protected Integer port;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MediaStream mediaStream;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TransportProtocol transportType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String schemaVersion;
    protected boolean supportVideoRecording;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der netAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetAddress() {
        return netAddress;
    }

    /**
     * Legt den Wert der netAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetAddress(String value) {
        this.netAddress = value;
    }

    /**
     * Ruft den Wert der port-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPort() {
        return port;
    }

    /**
     * Legt den Wert der port-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPort(Integer value) {
        this.port = value;
    }

    /**
     * Ruft den Wert der mediaStream-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MediaStream }
     *     
     */
    public MediaStream getMediaStream() {
        return mediaStream;
    }

    /**
     * Legt den Wert der mediaStream-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MediaStream }
     *     
     */
    public void setMediaStream(MediaStream value) {
        this.mediaStream = value;
    }

    /**
     * Ruft den Wert der transportType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TransportProtocol }
     *     
     */
    public TransportProtocol getTransportType() {
        return transportType;
    }

    /**
     * Legt den Wert der transportType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TransportProtocol }
     *     
     */
    public void setTransportType(TransportProtocol value) {
        this.transportType = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der schemaVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchemaVersion() {
        return schemaVersion;
    }

    /**
     * Legt den Wert der schemaVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchemaVersion(String value) {
        this.schemaVersion = value;
    }

    /**
     * Ruft den Wert der supportVideoRecording-Eigenschaft ab.
     * 
     */
    public boolean isSupportVideoRecording() {
        return supportVideoRecording;
    }

    /**
     * Legt den Wert der supportVideoRecording-Eigenschaft fest.
     * 
     */
    public void setSupportVideoRecording(boolean value) {
        this.supportVideoRecording = value;
    }

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

}
