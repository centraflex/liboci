//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *        Modify the service provider level data associated with Call Processing
 *        Policy. The response is either a SuccessResponse or an ErrorResponse.
 *      
 * 
 * <p>Java-Klasse für ServiceProviderCallProcessingModifyPolicyRequest14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderCallProcessingModifyPolicyRequest14">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="useMaxSimultaneousCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxSimultaneousCalls" type="{}CallProcessingMaxSimultaneousCalls" minOccurs="0"/>
 *         <element name="useMaxSimultaneousVideoCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxSimultaneousVideoCalls" type="{}CallProcessingMaxSimultaneousCalls" minOccurs="0"/>
 *         <element name="useMaxCallTimeForAnsweredCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxCallTimeForAnsweredCallsMinutes" type="{}CallProcessingMaxCallTimeForAnsweredCallsMinutes" minOccurs="0"/>
 *         <element name="useMaxCallTimeForUnansweredCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxCallTimeForUnansweredCallsMinutes" type="{}CallProcessingMaxCallTimeForUnansweredCallsMinutes" minOccurs="0"/>
 *         <element name="mediaPolicySelection" type="{}MediaPolicySelection" minOccurs="0"/>
 *         <element name="supportedMediaSetName" type="{}MediaSetName" minOccurs="0"/>
 *         <element name="networkUsageSelection" type="{}NetworkUsageSelection" minOccurs="0"/>
 *         <element name="enforceGroupCallingLineIdentityRestriction" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowEnterpriseGroupCallTypingForPrivateDialingPlan" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowEnterpriseGroupCallTypingForPublicDialingPlan" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="overrideCLIDRestrictionForExternalCallsViaPrivateDialingPlan" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableEnterpriseExtensionDialing" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enforceEnterpriseCallingLineIdentityRestriction" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useSettingLevel" type="{}ServiceProviderConferenceURISettingLevel" minOccurs="0"/>
 *         <element name="conferenceURI" type="{}SIPURI" minOccurs="0"/>
 *         <element name="useMaxConcurrentRedirectedCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxConcurrentRedirectedCalls" type="{}CallProcessingMaxConcurrentRedirectedCalls" minOccurs="0"/>
 *         <element name="useMaxFindMeFollowMeDepth" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxFindMeFollowMeDepth" type="{}CallProcessingMaxFindMeFollowMeDepth" minOccurs="0"/>
 *         <element name="maxRedirectionDepth" type="{}CallProcessingMaxRedirectionDepth" minOccurs="0"/>
 *         <element name="useMaxConcurrentFindMeFollowMeInvocations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxConcurrentFindMeFollowMeInvocations" type="{}CallProcessingMaxConcurrentFindMeFollowMeInvocations" minOccurs="0"/>
 *         <element name="clidPolicy" type="{}CLIDPolicy" minOccurs="0"/>
 *         <element name="emergencyClidPolicy" type="{}CLIDPolicy" minOccurs="0"/>
 *         <element name="allowAlternateNumbersForRedirectingIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderCallProcessingModifyPolicyRequest14", propOrder = {
    "serviceProviderId",
    "useMaxSimultaneousCalls",
    "maxSimultaneousCalls",
    "useMaxSimultaneousVideoCalls",
    "maxSimultaneousVideoCalls",
    "useMaxCallTimeForAnsweredCalls",
    "maxCallTimeForAnsweredCallsMinutes",
    "useMaxCallTimeForUnansweredCalls",
    "maxCallTimeForUnansweredCallsMinutes",
    "mediaPolicySelection",
    "supportedMediaSetName",
    "networkUsageSelection",
    "enforceGroupCallingLineIdentityRestriction",
    "allowEnterpriseGroupCallTypingForPrivateDialingPlan",
    "allowEnterpriseGroupCallTypingForPublicDialingPlan",
    "overrideCLIDRestrictionForExternalCallsViaPrivateDialingPlan",
    "enableEnterpriseExtensionDialing",
    "enforceEnterpriseCallingLineIdentityRestriction",
    "useSettingLevel",
    "conferenceURI",
    "useMaxConcurrentRedirectedCalls",
    "maxConcurrentRedirectedCalls",
    "useMaxFindMeFollowMeDepth",
    "maxFindMeFollowMeDepth",
    "maxRedirectionDepth",
    "useMaxConcurrentFindMeFollowMeInvocations",
    "maxConcurrentFindMeFollowMeInvocations",
    "clidPolicy",
    "emergencyClidPolicy",
    "allowAlternateNumbersForRedirectingIdentity"
})
public class ServiceProviderCallProcessingModifyPolicyRequest14
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected Boolean useMaxSimultaneousCalls;
    protected Integer maxSimultaneousCalls;
    protected Boolean useMaxSimultaneousVideoCalls;
    protected Integer maxSimultaneousVideoCalls;
    protected Boolean useMaxCallTimeForAnsweredCalls;
    protected Integer maxCallTimeForAnsweredCallsMinutes;
    protected Boolean useMaxCallTimeForUnansweredCalls;
    protected Integer maxCallTimeForUnansweredCallsMinutes;
    @XmlSchemaType(name = "token")
    protected MediaPolicySelection mediaPolicySelection;
    @XmlElementRef(name = "supportedMediaSetName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> supportedMediaSetName;
    @XmlSchemaType(name = "token")
    protected NetworkUsageSelection networkUsageSelection;
    protected Boolean enforceGroupCallingLineIdentityRestriction;
    protected Boolean allowEnterpriseGroupCallTypingForPrivateDialingPlan;
    protected Boolean allowEnterpriseGroupCallTypingForPublicDialingPlan;
    protected Boolean overrideCLIDRestrictionForExternalCallsViaPrivateDialingPlan;
    protected Boolean enableEnterpriseExtensionDialing;
    protected Boolean enforceEnterpriseCallingLineIdentityRestriction;
    @XmlSchemaType(name = "token")
    protected ServiceProviderConferenceURISettingLevel useSettingLevel;
    @XmlElementRef(name = "conferenceURI", type = JAXBElement.class, required = false)
    protected JAXBElement<String> conferenceURI;
    protected Boolean useMaxConcurrentRedirectedCalls;
    protected Integer maxConcurrentRedirectedCalls;
    protected Boolean useMaxFindMeFollowMeDepth;
    protected Integer maxFindMeFollowMeDepth;
    protected Integer maxRedirectionDepth;
    protected Boolean useMaxConcurrentFindMeFollowMeInvocations;
    protected Integer maxConcurrentFindMeFollowMeInvocations;
    @XmlSchemaType(name = "token")
    protected CLIDPolicy clidPolicy;
    @XmlSchemaType(name = "token")
    protected CLIDPolicy emergencyClidPolicy;
    protected Boolean allowAlternateNumbersForRedirectingIdentity;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der useMaxSimultaneousCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxSimultaneousCalls() {
        return useMaxSimultaneousCalls;
    }

    /**
     * Legt den Wert der useMaxSimultaneousCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxSimultaneousCalls(Boolean value) {
        this.useMaxSimultaneousCalls = value;
    }

    /**
     * Ruft den Wert der maxSimultaneousCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxSimultaneousCalls() {
        return maxSimultaneousCalls;
    }

    /**
     * Legt den Wert der maxSimultaneousCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxSimultaneousCalls(Integer value) {
        this.maxSimultaneousCalls = value;
    }

    /**
     * Ruft den Wert der useMaxSimultaneousVideoCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxSimultaneousVideoCalls() {
        return useMaxSimultaneousVideoCalls;
    }

    /**
     * Legt den Wert der useMaxSimultaneousVideoCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxSimultaneousVideoCalls(Boolean value) {
        this.useMaxSimultaneousVideoCalls = value;
    }

    /**
     * Ruft den Wert der maxSimultaneousVideoCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxSimultaneousVideoCalls() {
        return maxSimultaneousVideoCalls;
    }

    /**
     * Legt den Wert der maxSimultaneousVideoCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxSimultaneousVideoCalls(Integer value) {
        this.maxSimultaneousVideoCalls = value;
    }

    /**
     * Ruft den Wert der useMaxCallTimeForAnsweredCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxCallTimeForAnsweredCalls() {
        return useMaxCallTimeForAnsweredCalls;
    }

    /**
     * Legt den Wert der useMaxCallTimeForAnsweredCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxCallTimeForAnsweredCalls(Boolean value) {
        this.useMaxCallTimeForAnsweredCalls = value;
    }

    /**
     * Ruft den Wert der maxCallTimeForAnsweredCallsMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCallTimeForAnsweredCallsMinutes() {
        return maxCallTimeForAnsweredCallsMinutes;
    }

    /**
     * Legt den Wert der maxCallTimeForAnsweredCallsMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCallTimeForAnsweredCallsMinutes(Integer value) {
        this.maxCallTimeForAnsweredCallsMinutes = value;
    }

    /**
     * Ruft den Wert der useMaxCallTimeForUnansweredCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxCallTimeForUnansweredCalls() {
        return useMaxCallTimeForUnansweredCalls;
    }

    /**
     * Legt den Wert der useMaxCallTimeForUnansweredCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxCallTimeForUnansweredCalls(Boolean value) {
        this.useMaxCallTimeForUnansweredCalls = value;
    }

    /**
     * Ruft den Wert der maxCallTimeForUnansweredCallsMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCallTimeForUnansweredCallsMinutes() {
        return maxCallTimeForUnansweredCallsMinutes;
    }

    /**
     * Legt den Wert der maxCallTimeForUnansweredCallsMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCallTimeForUnansweredCallsMinutes(Integer value) {
        this.maxCallTimeForUnansweredCallsMinutes = value;
    }

    /**
     * Ruft den Wert der mediaPolicySelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MediaPolicySelection }
     *     
     */
    public MediaPolicySelection getMediaPolicySelection() {
        return mediaPolicySelection;
    }

    /**
     * Legt den Wert der mediaPolicySelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MediaPolicySelection }
     *     
     */
    public void setMediaPolicySelection(MediaPolicySelection value) {
        this.mediaPolicySelection = value;
    }

    /**
     * Ruft den Wert der supportedMediaSetName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSupportedMediaSetName() {
        return supportedMediaSetName;
    }

    /**
     * Legt den Wert der supportedMediaSetName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSupportedMediaSetName(JAXBElement<String> value) {
        this.supportedMediaSetName = value;
    }

    /**
     * Ruft den Wert der networkUsageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NetworkUsageSelection }
     *     
     */
    public NetworkUsageSelection getNetworkUsageSelection() {
        return networkUsageSelection;
    }

    /**
     * Legt den Wert der networkUsageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkUsageSelection }
     *     
     */
    public void setNetworkUsageSelection(NetworkUsageSelection value) {
        this.networkUsageSelection = value;
    }

    /**
     * Ruft den Wert der enforceGroupCallingLineIdentityRestriction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnforceGroupCallingLineIdentityRestriction() {
        return enforceGroupCallingLineIdentityRestriction;
    }

    /**
     * Legt den Wert der enforceGroupCallingLineIdentityRestriction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnforceGroupCallingLineIdentityRestriction(Boolean value) {
        this.enforceGroupCallingLineIdentityRestriction = value;
    }

    /**
     * Ruft den Wert der allowEnterpriseGroupCallTypingForPrivateDialingPlan-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowEnterpriseGroupCallTypingForPrivateDialingPlan() {
        return allowEnterpriseGroupCallTypingForPrivateDialingPlan;
    }

    /**
     * Legt den Wert der allowEnterpriseGroupCallTypingForPrivateDialingPlan-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowEnterpriseGroupCallTypingForPrivateDialingPlan(Boolean value) {
        this.allowEnterpriseGroupCallTypingForPrivateDialingPlan = value;
    }

    /**
     * Ruft den Wert der allowEnterpriseGroupCallTypingForPublicDialingPlan-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowEnterpriseGroupCallTypingForPublicDialingPlan() {
        return allowEnterpriseGroupCallTypingForPublicDialingPlan;
    }

    /**
     * Legt den Wert der allowEnterpriseGroupCallTypingForPublicDialingPlan-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowEnterpriseGroupCallTypingForPublicDialingPlan(Boolean value) {
        this.allowEnterpriseGroupCallTypingForPublicDialingPlan = value;
    }

    /**
     * Ruft den Wert der overrideCLIDRestrictionForExternalCallsViaPrivateDialingPlan-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOverrideCLIDRestrictionForExternalCallsViaPrivateDialingPlan() {
        return overrideCLIDRestrictionForExternalCallsViaPrivateDialingPlan;
    }

    /**
     * Legt den Wert der overrideCLIDRestrictionForExternalCallsViaPrivateDialingPlan-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverrideCLIDRestrictionForExternalCallsViaPrivateDialingPlan(Boolean value) {
        this.overrideCLIDRestrictionForExternalCallsViaPrivateDialingPlan = value;
    }

    /**
     * Ruft den Wert der enableEnterpriseExtensionDialing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableEnterpriseExtensionDialing() {
        return enableEnterpriseExtensionDialing;
    }

    /**
     * Legt den Wert der enableEnterpriseExtensionDialing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableEnterpriseExtensionDialing(Boolean value) {
        this.enableEnterpriseExtensionDialing = value;
    }

    /**
     * Ruft den Wert der enforceEnterpriseCallingLineIdentityRestriction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnforceEnterpriseCallingLineIdentityRestriction() {
        return enforceEnterpriseCallingLineIdentityRestriction;
    }

    /**
     * Legt den Wert der enforceEnterpriseCallingLineIdentityRestriction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnforceEnterpriseCallingLineIdentityRestriction(Boolean value) {
        this.enforceEnterpriseCallingLineIdentityRestriction = value;
    }

    /**
     * Ruft den Wert der useSettingLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderConferenceURISettingLevel }
     *     
     */
    public ServiceProviderConferenceURISettingLevel getUseSettingLevel() {
        return useSettingLevel;
    }

    /**
     * Legt den Wert der useSettingLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderConferenceURISettingLevel }
     *     
     */
    public void setUseSettingLevel(ServiceProviderConferenceURISettingLevel value) {
        this.useSettingLevel = value;
    }

    /**
     * Ruft den Wert der conferenceURI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getConferenceURI() {
        return conferenceURI;
    }

    /**
     * Legt den Wert der conferenceURI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setConferenceURI(JAXBElement<String> value) {
        this.conferenceURI = value;
    }

    /**
     * Ruft den Wert der useMaxConcurrentRedirectedCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxConcurrentRedirectedCalls() {
        return useMaxConcurrentRedirectedCalls;
    }

    /**
     * Legt den Wert der useMaxConcurrentRedirectedCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxConcurrentRedirectedCalls(Boolean value) {
        this.useMaxConcurrentRedirectedCalls = value;
    }

    /**
     * Ruft den Wert der maxConcurrentRedirectedCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxConcurrentRedirectedCalls() {
        return maxConcurrentRedirectedCalls;
    }

    /**
     * Legt den Wert der maxConcurrentRedirectedCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxConcurrentRedirectedCalls(Integer value) {
        this.maxConcurrentRedirectedCalls = value;
    }

    /**
     * Ruft den Wert der useMaxFindMeFollowMeDepth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxFindMeFollowMeDepth() {
        return useMaxFindMeFollowMeDepth;
    }

    /**
     * Legt den Wert der useMaxFindMeFollowMeDepth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxFindMeFollowMeDepth(Boolean value) {
        this.useMaxFindMeFollowMeDepth = value;
    }

    /**
     * Ruft den Wert der maxFindMeFollowMeDepth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxFindMeFollowMeDepth() {
        return maxFindMeFollowMeDepth;
    }

    /**
     * Legt den Wert der maxFindMeFollowMeDepth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxFindMeFollowMeDepth(Integer value) {
        this.maxFindMeFollowMeDepth = value;
    }

    /**
     * Ruft den Wert der maxRedirectionDepth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxRedirectionDepth() {
        return maxRedirectionDepth;
    }

    /**
     * Legt den Wert der maxRedirectionDepth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxRedirectionDepth(Integer value) {
        this.maxRedirectionDepth = value;
    }

    /**
     * Ruft den Wert der useMaxConcurrentFindMeFollowMeInvocations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMaxConcurrentFindMeFollowMeInvocations() {
        return useMaxConcurrentFindMeFollowMeInvocations;
    }

    /**
     * Legt den Wert der useMaxConcurrentFindMeFollowMeInvocations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMaxConcurrentFindMeFollowMeInvocations(Boolean value) {
        this.useMaxConcurrentFindMeFollowMeInvocations = value;
    }

    /**
     * Ruft den Wert der maxConcurrentFindMeFollowMeInvocations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxConcurrentFindMeFollowMeInvocations() {
        return maxConcurrentFindMeFollowMeInvocations;
    }

    /**
     * Legt den Wert der maxConcurrentFindMeFollowMeInvocations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxConcurrentFindMeFollowMeInvocations(Integer value) {
        this.maxConcurrentFindMeFollowMeInvocations = value;
    }

    /**
     * Ruft den Wert der clidPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CLIDPolicy }
     *     
     */
    public CLIDPolicy getClidPolicy() {
        return clidPolicy;
    }

    /**
     * Legt den Wert der clidPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CLIDPolicy }
     *     
     */
    public void setClidPolicy(CLIDPolicy value) {
        this.clidPolicy = value;
    }

    /**
     * Ruft den Wert der emergencyClidPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CLIDPolicy }
     *     
     */
    public CLIDPolicy getEmergencyClidPolicy() {
        return emergencyClidPolicy;
    }

    /**
     * Legt den Wert der emergencyClidPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CLIDPolicy }
     *     
     */
    public void setEmergencyClidPolicy(CLIDPolicy value) {
        this.emergencyClidPolicy = value;
    }

    /**
     * Ruft den Wert der allowAlternateNumbersForRedirectingIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowAlternateNumbersForRedirectingIdentity() {
        return allowAlternateNumbersForRedirectingIdentity;
    }

    /**
     * Legt den Wert der allowAlternateNumbersForRedirectingIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowAlternateNumbersForRedirectingIdentity(Boolean value) {
        this.allowAlternateNumbersForRedirectingIdentity = value;
    }

}
