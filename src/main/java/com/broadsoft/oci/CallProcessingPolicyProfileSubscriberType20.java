//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallProcessingPolicyProfileSubscriberType20.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallProcessingPolicyProfileSubscriberType20">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="User"/>
 *     <enumeration value="Trunk Group Pilot User"/>
 *     <enumeration value="Auto Attendant"/>
 *     <enumeration value="BroadWorks Anywhere"/>
 *     <enumeration value="Call Center"/>
 *     <enumeration value="Find-me/Follow-me"/>
 *     <enumeration value="Group Paging"/>
 *     <enumeration value="Flexible Seating Host"/>
 *     <enumeration value="Hunt Group"/>
 *     <enumeration value="Instant Group Call"/>
 *     <enumeration value="Meet-Me Conferencing"/>
 *     <enumeration value="Route Point"/>
 *     <enumeration value="Voice Portal"/>
 *     <enumeration value="VoiceXML"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallProcessingPolicyProfileSubscriberType20")
@XmlEnum
public enum CallProcessingPolicyProfileSubscriberType20 {

    @XmlEnumValue("User")
    USER("User"),
    @XmlEnumValue("Trunk Group Pilot User")
    TRUNK_GROUP_PILOT_USER("Trunk Group Pilot User"),
    @XmlEnumValue("Auto Attendant")
    AUTO_ATTENDANT("Auto Attendant"),
    @XmlEnumValue("BroadWorks Anywhere")
    BROAD_WORKS_ANYWHERE("BroadWorks Anywhere"),
    @XmlEnumValue("Call Center")
    CALL_CENTER("Call Center"),
    @XmlEnumValue("Find-me/Follow-me")
    FIND_ME_FOLLOW_ME("Find-me/Follow-me"),
    @XmlEnumValue("Group Paging")
    GROUP_PAGING("Group Paging"),
    @XmlEnumValue("Flexible Seating Host")
    FLEXIBLE_SEATING_HOST("Flexible Seating Host"),
    @XmlEnumValue("Hunt Group")
    HUNT_GROUP("Hunt Group"),
    @XmlEnumValue("Instant Group Call")
    INSTANT_GROUP_CALL("Instant Group Call"),
    @XmlEnumValue("Meet-Me Conferencing")
    MEET_ME_CONFERENCING("Meet-Me Conferencing"),
    @XmlEnumValue("Route Point")
    ROUTE_POINT("Route Point"),
    @XmlEnumValue("Voice Portal")
    VOICE_PORTAL("Voice Portal"),
    @XmlEnumValue("VoiceXML")
    VOICE_XML("VoiceXML");
    private final String value;

    CallProcessingPolicyProfileSubscriberType20(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallProcessingPolicyProfileSubscriberType20 fromValue(String v) {
        for (CallProcessingPolicyProfileSubscriberType20 c: CallProcessingPolicyProfileSubscriberType20 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
