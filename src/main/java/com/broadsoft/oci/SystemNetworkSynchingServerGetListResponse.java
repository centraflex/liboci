//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemNetworkSynchingServerGetListRequest. The Network Server table column
 *         headings are: "Net Address", "Port", "Secure", "Description", "Order".
 *         The following columns are only used in XS data mode and not returned in AS data mode:
 *           Order
 *         The following elements are only used in AS data mode and not returned in XS data mode:
 *           preferredNetworkServerNetAddress
 *       
 * 
 * <p>Java-Klasse für SystemNetworkSynchingServerGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNetworkSynchingServerGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="preferredNetworkServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="networkSynchingServerTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNetworkSynchingServerGetListResponse", propOrder = {
    "preferredNetworkServerNetAddress",
    "networkSynchingServerTable"
})
public class SystemNetworkSynchingServerGetListResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String preferredNetworkServerNetAddress;
    @XmlElement(required = true)
    protected OCITable networkSynchingServerTable;

    /**
     * Ruft den Wert der preferredNetworkServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredNetworkServerNetAddress() {
        return preferredNetworkServerNetAddress;
    }

    /**
     * Legt den Wert der preferredNetworkServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredNetworkServerNetAddress(String value) {
        this.preferredNetworkServerNetAddress = value;
    }

    /**
     * Ruft den Wert der networkSynchingServerTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getNetworkSynchingServerTable() {
        return networkSynchingServerTable;
    }

    /**
     * Legt den Wert der networkSynchingServerTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setNetworkSynchingServerTable(OCITable value) {
        this.networkSynchingServerTable = value;
    }

}
