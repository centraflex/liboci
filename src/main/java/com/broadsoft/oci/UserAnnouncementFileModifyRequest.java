//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         This command is used to change the name of the file or upload a new announcement file for 
 *         an existing announcement in the user repository.
 *         When modifying the file type the command will fail if the media type of the new file changes 
 *         the announcement from audio to video (or vice versa).
 * 
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           announcementFileExternalId
 * 
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserAnnouncementFileModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserAnnouncementFileModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <choice>
 *           <sequence>
 *             <element name="userId" type="{}UserId"/>
 *             <element name="announcementFileKey" type="{}AnnouncementFileKey"/>
 *           </sequence>
 *           <element name="announcementFileExternalId" type="{}ExternalId"/>
 *         </choice>
 *         <element name="newAnnouncementFileName" type="{}AnnouncementFileName" minOccurs="0"/>
 *         <element name="announcementFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserAnnouncementFileModifyRequest", propOrder = {
    "userId",
    "announcementFileKey",
    "announcementFileExternalId",
    "newAnnouncementFileName",
    "announcementFile"
})
public class UserAnnouncementFileModifyRequest
    extends OCIRequest
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected AnnouncementFileKey announcementFileKey;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String announcementFileExternalId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newAnnouncementFileName;
    protected LabeledMediaFileResource announcementFile;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der announcementFileKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementFileKey }
     *     
     */
    public AnnouncementFileKey getAnnouncementFileKey() {
        return announcementFileKey;
    }

    /**
     * Legt den Wert der announcementFileKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementFileKey }
     *     
     */
    public void setAnnouncementFileKey(AnnouncementFileKey value) {
        this.announcementFileKey = value;
    }

    /**
     * Ruft den Wert der announcementFileExternalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnouncementFileExternalId() {
        return announcementFileExternalId;
    }

    /**
     * Legt den Wert der announcementFileExternalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnouncementFileExternalId(String value) {
        this.announcementFileExternalId = value;
    }

    /**
     * Ruft den Wert der newAnnouncementFileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewAnnouncementFileName() {
        return newAnnouncementFileName;
    }

    /**
     * Legt den Wert der newAnnouncementFileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewAnnouncementFileName(String value) {
        this.newAnnouncementFileName = value;
    }

    /**
     * Ruft den Wert der announcementFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public LabeledMediaFileResource getAnnouncementFile() {
        return announcementFile;
    }

    /**
     * Legt den Wert der announcementFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public void setAnnouncementFile(LabeledMediaFileResource value) {
        this.announcementFile = value;
    }

}
