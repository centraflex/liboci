//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a user creation task for a trunk group.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupTrunkGroupUserCreationTaskAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupTrunkGroupUserCreationTaskAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="taskName" type="{}TrunkGroupUserCreationTaskName"/>
 *         <element name="userIdFormat" type="{}TrunkGroupUserCreationUserIdFormat"/>
 *         <element name="userIdDomain" type="{}NetAddress"/>
 *         <element name="populateExtension" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="linePortFormat" type="{}TrunkGroupUserCreationSIPURIFormat"/>
 *         <element name="linePortDomain" type="{}NetAddress"/>
 *         <element name="populateContact" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="contactFormat" type="{}TrunkGroupUserCreationSIPURIFormat" minOccurs="0"/>
 *         <element name="contactDomain" type="{}NetAddress" minOccurs="0"/>
 *         <choice>
 *           <element name="userPhoneNumbers">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="phoneNumber" type="{}DN" maxOccurs="unbounded" minOccurs="0"/>
 *                     <element name="dnRange" type="{}DNRange" maxOccurs="unbounded" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *           <element name="userExtensions">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="extension" type="{}Extension" maxOccurs="unbounded" minOccurs="0"/>
 *                     <element name="extensionRange" type="{}ExtensionRange" maxOccurs="unbounded" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *         </choice>
 *         <element name="servicePackName" type="{}ServicePackName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="userService" type="{}UserService" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupTrunkGroupUserCreationTaskAddRequest", propOrder = {
    "serviceUserId",
    "taskName",
    "userIdFormat",
    "userIdDomain",
    "populateExtension",
    "linePortFormat",
    "linePortDomain",
    "populateContact",
    "contactFormat",
    "contactDomain",
    "userPhoneNumbers",
    "userExtensions",
    "servicePackName",
    "userService"
})
public class GroupTrunkGroupUserCreationTaskAddRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String taskName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupUserCreationUserIdFormat userIdFormat;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userIdDomain;
    protected boolean populateExtension;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupUserCreationSIPURIFormat linePortFormat;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String linePortDomain;
    protected boolean populateContact;
    @XmlSchemaType(name = "token")
    protected TrunkGroupUserCreationSIPURIFormat contactFormat;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String contactDomain;
    protected GroupTrunkGroupUserCreationTaskAddRequest.UserPhoneNumbers userPhoneNumbers;
    protected GroupTrunkGroupUserCreationTaskAddRequest.UserExtensions userExtensions;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> servicePackName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> userService;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der taskName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * Legt den Wert der taskName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskName(String value) {
        this.taskName = value;
    }

    /**
     * Ruft den Wert der userIdFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupUserCreationUserIdFormat }
     *     
     */
    public TrunkGroupUserCreationUserIdFormat getUserIdFormat() {
        return userIdFormat;
    }

    /**
     * Legt den Wert der userIdFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupUserCreationUserIdFormat }
     *     
     */
    public void setUserIdFormat(TrunkGroupUserCreationUserIdFormat value) {
        this.userIdFormat = value;
    }

    /**
     * Ruft den Wert der userIdDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserIdDomain() {
        return userIdDomain;
    }

    /**
     * Legt den Wert der userIdDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserIdDomain(String value) {
        this.userIdDomain = value;
    }

    /**
     * Ruft den Wert der populateExtension-Eigenschaft ab.
     * 
     */
    public boolean isPopulateExtension() {
        return populateExtension;
    }

    /**
     * Legt den Wert der populateExtension-Eigenschaft fest.
     * 
     */
    public void setPopulateExtension(boolean value) {
        this.populateExtension = value;
    }

    /**
     * Ruft den Wert der linePortFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupUserCreationSIPURIFormat }
     *     
     */
    public TrunkGroupUserCreationSIPURIFormat getLinePortFormat() {
        return linePortFormat;
    }

    /**
     * Legt den Wert der linePortFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupUserCreationSIPURIFormat }
     *     
     */
    public void setLinePortFormat(TrunkGroupUserCreationSIPURIFormat value) {
        this.linePortFormat = value;
    }

    /**
     * Ruft den Wert der linePortDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinePortDomain() {
        return linePortDomain;
    }

    /**
     * Legt den Wert der linePortDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinePortDomain(String value) {
        this.linePortDomain = value;
    }

    /**
     * Ruft den Wert der populateContact-Eigenschaft ab.
     * 
     */
    public boolean isPopulateContact() {
        return populateContact;
    }

    /**
     * Legt den Wert der populateContact-Eigenschaft fest.
     * 
     */
    public void setPopulateContact(boolean value) {
        this.populateContact = value;
    }

    /**
     * Ruft den Wert der contactFormat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupUserCreationSIPURIFormat }
     *     
     */
    public TrunkGroupUserCreationSIPURIFormat getContactFormat() {
        return contactFormat;
    }

    /**
     * Legt den Wert der contactFormat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupUserCreationSIPURIFormat }
     *     
     */
    public void setContactFormat(TrunkGroupUserCreationSIPURIFormat value) {
        this.contactFormat = value;
    }

    /**
     * Ruft den Wert der contactDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactDomain() {
        return contactDomain;
    }

    /**
     * Legt den Wert der contactDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactDomain(String value) {
        this.contactDomain = value;
    }

    /**
     * Ruft den Wert der userPhoneNumbers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupTrunkGroupUserCreationTaskAddRequest.UserPhoneNumbers }
     *     
     */
    public GroupTrunkGroupUserCreationTaskAddRequest.UserPhoneNumbers getUserPhoneNumbers() {
        return userPhoneNumbers;
    }

    /**
     * Legt den Wert der userPhoneNumbers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupTrunkGroupUserCreationTaskAddRequest.UserPhoneNumbers }
     *     
     */
    public void setUserPhoneNumbers(GroupTrunkGroupUserCreationTaskAddRequest.UserPhoneNumbers value) {
        this.userPhoneNumbers = value;
    }

    /**
     * Ruft den Wert der userExtensions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupTrunkGroupUserCreationTaskAddRequest.UserExtensions }
     *     
     */
    public GroupTrunkGroupUserCreationTaskAddRequest.UserExtensions getUserExtensions() {
        return userExtensions;
    }

    /**
     * Legt den Wert der userExtensions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupTrunkGroupUserCreationTaskAddRequest.UserExtensions }
     *     
     */
    public void setUserExtensions(GroupTrunkGroupUserCreationTaskAddRequest.UserExtensions value) {
        this.userExtensions = value;
    }

    /**
     * Gets the value of the servicePackName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the servicePackName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicePackName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the servicePackName property.
     */
    public List<String> getServicePackName() {
        if (servicePackName == null) {
            servicePackName = new ArrayList<>();
        }
        return this.servicePackName;
    }

    /**
     * Gets the value of the userService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the userService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the userService property.
     */
    public List<String> getUserService() {
        if (userService == null) {
            userService = new ArrayList<>();
        }
        return this.userService;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="extension" type="{}Extension" maxOccurs="unbounded" minOccurs="0"/>
     *         <element name="extensionRange" type="{}ExtensionRange" maxOccurs="unbounded" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "extension",
        "extensionRange"
    })
    public static class UserExtensions {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> extension;
        protected List<ExtensionRange> extensionRange;

        /**
         * Gets the value of the extension property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the extension property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getExtension().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the extension property.
         */
        public List<String> getExtension() {
            if (extension == null) {
                extension = new ArrayList<>();
            }
            return this.extension;
        }

        /**
         * Gets the value of the extensionRange property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the extensionRange property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getExtensionRange().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ExtensionRange }
         * 
         * 
         * @return
         *     The value of the extensionRange property.
         */
        public List<ExtensionRange> getExtensionRange() {
            if (extensionRange == null) {
                extensionRange = new ArrayList<>();
            }
            return this.extensionRange;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="phoneNumber" type="{}DN" maxOccurs="unbounded" minOccurs="0"/>
     *         <element name="dnRange" type="{}DNRange" maxOccurs="unbounded" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "phoneNumber",
        "dnRange"
    })
    public static class UserPhoneNumbers {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> phoneNumber;
        protected List<DNRange> dnRange;

        /**
         * Gets the value of the phoneNumber property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the phoneNumber property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPhoneNumber().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the phoneNumber property.
         */
        public List<String> getPhoneNumber() {
            if (phoneNumber == null) {
                phoneNumber = new ArrayList<>();
            }
            return this.phoneNumber;
        }

        /**
         * Gets the value of the dnRange property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the dnRange property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDnRange().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DNRange }
         * 
         * 
         * @return
         *     The value of the dnRange property.
         */
        public List<DNRange> getDnRange() {
            if (dnRange == null) {
                dnRange = new ArrayList<>();
            }
            return this.dnRange;
        }

    }

}
