//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The call center enhanced reporting scheduled report inclusions related to the Service Level thresholds
 *       
 * 
 * <p>Java-Klasse für CallCenterScheduledReportServiceLevelInclusions complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterScheduledReportServiceLevelInclusions">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="includeOverflowTimeTransferedInServiceLevel" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="includeOtherTransfersInServiceLevel" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="abandonedCallsInServiceLevel" type="{}CallCenterReportAbadonedCallsInServiceLevel"/>
 *         <element name="abandonedCallIntervalSeconds" type="{}CallCenterReportThresholdSeconds" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterScheduledReportServiceLevelInclusions", propOrder = {
    "includeOverflowTimeTransferedInServiceLevel",
    "includeOtherTransfersInServiceLevel",
    "abandonedCallsInServiceLevel",
    "abandonedCallIntervalSeconds"
})
public class CallCenterScheduledReportServiceLevelInclusions {

    protected boolean includeOverflowTimeTransferedInServiceLevel;
    protected boolean includeOtherTransfersInServiceLevel;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportAbadonedCallsInServiceLevel abandonedCallsInServiceLevel;
    protected Integer abandonedCallIntervalSeconds;

    /**
     * Ruft den Wert der includeOverflowTimeTransferedInServiceLevel-Eigenschaft ab.
     * 
     */
    public boolean isIncludeOverflowTimeTransferedInServiceLevel() {
        return includeOverflowTimeTransferedInServiceLevel;
    }

    /**
     * Legt den Wert der includeOverflowTimeTransferedInServiceLevel-Eigenschaft fest.
     * 
     */
    public void setIncludeOverflowTimeTransferedInServiceLevel(boolean value) {
        this.includeOverflowTimeTransferedInServiceLevel = value;
    }

    /**
     * Ruft den Wert der includeOtherTransfersInServiceLevel-Eigenschaft ab.
     * 
     */
    public boolean isIncludeOtherTransfersInServiceLevel() {
        return includeOtherTransfersInServiceLevel;
    }

    /**
     * Legt den Wert der includeOtherTransfersInServiceLevel-Eigenschaft fest.
     * 
     */
    public void setIncludeOtherTransfersInServiceLevel(boolean value) {
        this.includeOtherTransfersInServiceLevel = value;
    }

    /**
     * Ruft den Wert der abandonedCallsInServiceLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportAbadonedCallsInServiceLevel }
     *     
     */
    public CallCenterReportAbadonedCallsInServiceLevel getAbandonedCallsInServiceLevel() {
        return abandonedCallsInServiceLevel;
    }

    /**
     * Legt den Wert der abandonedCallsInServiceLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportAbadonedCallsInServiceLevel }
     *     
     */
    public void setAbandonedCallsInServiceLevel(CallCenterReportAbadonedCallsInServiceLevel value) {
        this.abandonedCallsInServiceLevel = value;
    }

    /**
     * Ruft den Wert der abandonedCallIntervalSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAbandonedCallIntervalSeconds() {
        return abandonedCallIntervalSeconds;
    }

    /**
     * Legt den Wert der abandonedCallIntervalSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAbandonedCallIntervalSeconds(Integer value) {
        this.abandonedCallIntervalSeconds = value;
    }

}
