//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *          Response to a UserBroadWorksMobilityGetRequest21sp1.
 *          Columns for the profileIdentityMobileNumberAlerted table are as follows: "Mobile Number", "Country Code", "National Prefix".
 *          Columns for the mobileIdentity table are as follows: "Mobile Number", "Description", "Country Code", "National Prefix", "Is Primary", "Enable Alerting".
 *       
 * 
 * <p>Java-Klasse für UserBroadWorksMobilityGetResponse21sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksMobilityGetResponse21sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useMobileIdentityCallAnchoring" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="preventCallsToOwnMobiles" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="profileIdentityDevicesToRing" type="{}BroadWorksMobilityPhoneToRing"/>
 *         <element name="profileIdentityIncludeSharedCallAppearance" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="profileIdentityIncludeBroadworksAnywhere" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="profileIdentityIncludeExecutiveAssistant" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="profileIdentityMobileNumberAlertedTable" type="{C}OCITable"/>
 *         <element name="mobileIdentityTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksMobilityGetResponse21sp1", propOrder = {
    "isActive",
    "useMobileIdentityCallAnchoring",
    "preventCallsToOwnMobiles",
    "profileIdentityDevicesToRing",
    "profileIdentityIncludeSharedCallAppearance",
    "profileIdentityIncludeBroadworksAnywhere",
    "profileIdentityIncludeExecutiveAssistant",
    "profileIdentityMobileNumberAlertedTable",
    "mobileIdentityTable"
})
public class UserBroadWorksMobilityGetResponse21Sp1
    extends OCIDataResponse
{

    protected boolean isActive;
    protected boolean useMobileIdentityCallAnchoring;
    protected boolean preventCallsToOwnMobiles;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected BroadWorksMobilityPhoneToRing profileIdentityDevicesToRing;
    protected boolean profileIdentityIncludeSharedCallAppearance;
    protected boolean profileIdentityIncludeBroadworksAnywhere;
    protected boolean profileIdentityIncludeExecutiveAssistant;
    @XmlElement(required = true)
    protected OCITable profileIdentityMobileNumberAlertedTable;
    @XmlElement(required = true)
    protected OCITable mobileIdentityTable;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der useMobileIdentityCallAnchoring-Eigenschaft ab.
     * 
     */
    public boolean isUseMobileIdentityCallAnchoring() {
        return useMobileIdentityCallAnchoring;
    }

    /**
     * Legt den Wert der useMobileIdentityCallAnchoring-Eigenschaft fest.
     * 
     */
    public void setUseMobileIdentityCallAnchoring(boolean value) {
        this.useMobileIdentityCallAnchoring = value;
    }

    /**
     * Ruft den Wert der preventCallsToOwnMobiles-Eigenschaft ab.
     * 
     */
    public boolean isPreventCallsToOwnMobiles() {
        return preventCallsToOwnMobiles;
    }

    /**
     * Legt den Wert der preventCallsToOwnMobiles-Eigenschaft fest.
     * 
     */
    public void setPreventCallsToOwnMobiles(boolean value) {
        this.preventCallsToOwnMobiles = value;
    }

    /**
     * Ruft den Wert der profileIdentityDevicesToRing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksMobilityPhoneToRing }
     *     
     */
    public BroadWorksMobilityPhoneToRing getProfileIdentityDevicesToRing() {
        return profileIdentityDevicesToRing;
    }

    /**
     * Legt den Wert der profileIdentityDevicesToRing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksMobilityPhoneToRing }
     *     
     */
    public void setProfileIdentityDevicesToRing(BroadWorksMobilityPhoneToRing value) {
        this.profileIdentityDevicesToRing = value;
    }

    /**
     * Ruft den Wert der profileIdentityIncludeSharedCallAppearance-Eigenschaft ab.
     * 
     */
    public boolean isProfileIdentityIncludeSharedCallAppearance() {
        return profileIdentityIncludeSharedCallAppearance;
    }

    /**
     * Legt den Wert der profileIdentityIncludeSharedCallAppearance-Eigenschaft fest.
     * 
     */
    public void setProfileIdentityIncludeSharedCallAppearance(boolean value) {
        this.profileIdentityIncludeSharedCallAppearance = value;
    }

    /**
     * Ruft den Wert der profileIdentityIncludeBroadworksAnywhere-Eigenschaft ab.
     * 
     */
    public boolean isProfileIdentityIncludeBroadworksAnywhere() {
        return profileIdentityIncludeBroadworksAnywhere;
    }

    /**
     * Legt den Wert der profileIdentityIncludeBroadworksAnywhere-Eigenschaft fest.
     * 
     */
    public void setProfileIdentityIncludeBroadworksAnywhere(boolean value) {
        this.profileIdentityIncludeBroadworksAnywhere = value;
    }

    /**
     * Ruft den Wert der profileIdentityIncludeExecutiveAssistant-Eigenschaft ab.
     * 
     */
    public boolean isProfileIdentityIncludeExecutiveAssistant() {
        return profileIdentityIncludeExecutiveAssistant;
    }

    /**
     * Legt den Wert der profileIdentityIncludeExecutiveAssistant-Eigenschaft fest.
     * 
     */
    public void setProfileIdentityIncludeExecutiveAssistant(boolean value) {
        this.profileIdentityIncludeExecutiveAssistant = value;
    }

    /**
     * Ruft den Wert der profileIdentityMobileNumberAlertedTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getProfileIdentityMobileNumberAlertedTable() {
        return profileIdentityMobileNumberAlertedTable;
    }

    /**
     * Legt den Wert der profileIdentityMobileNumberAlertedTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setProfileIdentityMobileNumberAlertedTable(OCITable value) {
        this.profileIdentityMobileNumberAlertedTable = value;
    }

    /**
     * Ruft den Wert der mobileIdentityTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getMobileIdentityTable() {
        return mobileIdentityTable;
    }

    /**
     * Legt den Wert der mobileIdentityTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setMobileIdentityTable(OCITable value) {
        this.mobileIdentityTable = value;
    }

}
