//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemSecurityClassificationGetRequest.
 *         Contains a table with column headings:
 *         "Name", "Priority".
 *       
 * 
 * <p>Java-Klasse für SystemSecurityClassificationGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSecurityClassificationGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="meetMeAnncThreshold" type="{}SecurityClassificationMeetMeConferenceAnnouncementThresholdSeconds"/>
 *         <element name="SecurityClassificationTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSecurityClassificationGetResponse", propOrder = {
    "meetMeAnncThreshold",
    "securityClassificationTable"
})
public class SystemSecurityClassificationGetResponse
    extends OCIDataResponse
{

    protected int meetMeAnncThreshold;
    @XmlElement(name = "SecurityClassificationTable", required = true)
    protected OCITable securityClassificationTable;

    /**
     * Ruft den Wert der meetMeAnncThreshold-Eigenschaft ab.
     * 
     */
    public int getMeetMeAnncThreshold() {
        return meetMeAnncThreshold;
    }

    /**
     * Legt den Wert der meetMeAnncThreshold-Eigenschaft fest.
     * 
     */
    public void setMeetMeAnncThreshold(int value) {
        this.meetMeAnncThreshold = value;
    }

    /**
     * Ruft den Wert der securityClassificationTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getSecurityClassificationTable() {
        return securityClassificationTable;
    }

    /**
     * Legt den Wert der securityClassificationTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setSecurityClassificationTable(OCITable value) {
        this.securityClassificationTable = value;
    }

}
