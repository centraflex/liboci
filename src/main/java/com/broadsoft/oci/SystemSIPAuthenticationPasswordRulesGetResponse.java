//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemSIPAuthenticationPasswordRulesGetRequest.
 *         Contains the SIP authentication password rules for the system.
 *       
 * 
 * <p>Java-Klasse für SystemSIPAuthenticationPasswordRulesGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPAuthenticationPasswordRulesGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="disallowAuthenticationName" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disallowOldPassword" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disallowReversedOldPassword" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="restrictMinDigits" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minDigits" type="{}PasswordMinDigits"/>
 *         <element name="restrictMinUpperCaseLetters" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minUpperCaseLetters" type="{}PasswordMinUpperCaseLetters"/>
 *         <element name="restrictMinLowerCaseLetters" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minLowerCaseLetters" type="{}PasswordMinLowerCaseLetters"/>
 *         <element name="restrictMinNonAlphanumericCharacters" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minNonAlphanumericCharacters" type="{}PasswordMinNonAlphanumericCharacters"/>
 *         <element name="minLength" type="{}PasswordMinLength"/>
 *         <element name="sendPermanentLockoutNotification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="permanentLockoutNotifyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="endpointAuthenticationLockoutType" type="{}AuthenticationLockoutType"/>
 *         <element name="endpointTemporaryLockoutThreshold" type="{}AuthenticationTemporaryLockoutThreshold"/>
 *         <element name="endpointWaitAlgorithm" type="{}AuthenticationLockoutWaitAlgorithmType"/>
 *         <element name="endpointLockoutFixedMinutes" type="{}AuthenticationLockoutFixedWaitTimeMinutes"/>
 *         <element name="endpointPermanentLockoutThreshold" type="{}AuthenticationPermanentLockoutThreshold"/>
 *         <element name="trunkGroupAuthenticationLockoutType" type="{}AuthenticationLockoutType"/>
 *         <element name="trunkGroupTemporaryLockoutThreshold" type="{}AuthenticationTemporaryLockoutThreshold"/>
 *         <element name="trunkGroupWaitAlgorithm" type="{}AuthenticationLockoutWaitAlgorithmType"/>
 *         <element name="trunkGroupLockoutFixedMinutes" type="{}AuthenticationLockoutFixedWaitTimeMinutes"/>
 *         <element name="trunkGroupPermanentLockoutThreshold" type="{}AuthenticationPermanentLockoutThreshold"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPAuthenticationPasswordRulesGetResponse", propOrder = {
    "disallowAuthenticationName",
    "disallowOldPassword",
    "disallowReversedOldPassword",
    "restrictMinDigits",
    "minDigits",
    "restrictMinUpperCaseLetters",
    "minUpperCaseLetters",
    "restrictMinLowerCaseLetters",
    "minLowerCaseLetters",
    "restrictMinNonAlphanumericCharacters",
    "minNonAlphanumericCharacters",
    "minLength",
    "sendPermanentLockoutNotification",
    "permanentLockoutNotifyEmailAddress",
    "endpointAuthenticationLockoutType",
    "endpointTemporaryLockoutThreshold",
    "endpointWaitAlgorithm",
    "endpointLockoutFixedMinutes",
    "endpointPermanentLockoutThreshold",
    "trunkGroupAuthenticationLockoutType",
    "trunkGroupTemporaryLockoutThreshold",
    "trunkGroupWaitAlgorithm",
    "trunkGroupLockoutFixedMinutes",
    "trunkGroupPermanentLockoutThreshold"
})
public class SystemSIPAuthenticationPasswordRulesGetResponse
    extends OCIDataResponse
{

    protected boolean disallowAuthenticationName;
    protected boolean disallowOldPassword;
    protected boolean disallowReversedOldPassword;
    protected boolean restrictMinDigits;
    protected int minDigits;
    protected boolean restrictMinUpperCaseLetters;
    protected int minUpperCaseLetters;
    protected boolean restrictMinLowerCaseLetters;
    protected int minLowerCaseLetters;
    protected boolean restrictMinNonAlphanumericCharacters;
    protected int minNonAlphanumericCharacters;
    protected int minLength;
    protected boolean sendPermanentLockoutNotification;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String permanentLockoutNotifyEmailAddress;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AuthenticationLockoutType endpointAuthenticationLockoutType;
    protected int endpointTemporaryLockoutThreshold;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AuthenticationLockoutWaitAlgorithmType endpointWaitAlgorithm;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String endpointLockoutFixedMinutes;
    protected int endpointPermanentLockoutThreshold;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AuthenticationLockoutType trunkGroupAuthenticationLockoutType;
    protected int trunkGroupTemporaryLockoutThreshold;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AuthenticationLockoutWaitAlgorithmType trunkGroupWaitAlgorithm;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String trunkGroupLockoutFixedMinutes;
    protected int trunkGroupPermanentLockoutThreshold;

    /**
     * Ruft den Wert der disallowAuthenticationName-Eigenschaft ab.
     * 
     */
    public boolean isDisallowAuthenticationName() {
        return disallowAuthenticationName;
    }

    /**
     * Legt den Wert der disallowAuthenticationName-Eigenschaft fest.
     * 
     */
    public void setDisallowAuthenticationName(boolean value) {
        this.disallowAuthenticationName = value;
    }

    /**
     * Ruft den Wert der disallowOldPassword-Eigenschaft ab.
     * 
     */
    public boolean isDisallowOldPassword() {
        return disallowOldPassword;
    }

    /**
     * Legt den Wert der disallowOldPassword-Eigenschaft fest.
     * 
     */
    public void setDisallowOldPassword(boolean value) {
        this.disallowOldPassword = value;
    }

    /**
     * Ruft den Wert der disallowReversedOldPassword-Eigenschaft ab.
     * 
     */
    public boolean isDisallowReversedOldPassword() {
        return disallowReversedOldPassword;
    }

    /**
     * Legt den Wert der disallowReversedOldPassword-Eigenschaft fest.
     * 
     */
    public void setDisallowReversedOldPassword(boolean value) {
        this.disallowReversedOldPassword = value;
    }

    /**
     * Ruft den Wert der restrictMinDigits-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinDigits() {
        return restrictMinDigits;
    }

    /**
     * Legt den Wert der restrictMinDigits-Eigenschaft fest.
     * 
     */
    public void setRestrictMinDigits(boolean value) {
        this.restrictMinDigits = value;
    }

    /**
     * Ruft den Wert der minDigits-Eigenschaft ab.
     * 
     */
    public int getMinDigits() {
        return minDigits;
    }

    /**
     * Legt den Wert der minDigits-Eigenschaft fest.
     * 
     */
    public void setMinDigits(int value) {
        this.minDigits = value;
    }

    /**
     * Ruft den Wert der restrictMinUpperCaseLetters-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinUpperCaseLetters() {
        return restrictMinUpperCaseLetters;
    }

    /**
     * Legt den Wert der restrictMinUpperCaseLetters-Eigenschaft fest.
     * 
     */
    public void setRestrictMinUpperCaseLetters(boolean value) {
        this.restrictMinUpperCaseLetters = value;
    }

    /**
     * Ruft den Wert der minUpperCaseLetters-Eigenschaft ab.
     * 
     */
    public int getMinUpperCaseLetters() {
        return minUpperCaseLetters;
    }

    /**
     * Legt den Wert der minUpperCaseLetters-Eigenschaft fest.
     * 
     */
    public void setMinUpperCaseLetters(int value) {
        this.minUpperCaseLetters = value;
    }

    /**
     * Ruft den Wert der restrictMinLowerCaseLetters-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinLowerCaseLetters() {
        return restrictMinLowerCaseLetters;
    }

    /**
     * Legt den Wert der restrictMinLowerCaseLetters-Eigenschaft fest.
     * 
     */
    public void setRestrictMinLowerCaseLetters(boolean value) {
        this.restrictMinLowerCaseLetters = value;
    }

    /**
     * Ruft den Wert der minLowerCaseLetters-Eigenschaft ab.
     * 
     */
    public int getMinLowerCaseLetters() {
        return minLowerCaseLetters;
    }

    /**
     * Legt den Wert der minLowerCaseLetters-Eigenschaft fest.
     * 
     */
    public void setMinLowerCaseLetters(int value) {
        this.minLowerCaseLetters = value;
    }

    /**
     * Ruft den Wert der restrictMinNonAlphanumericCharacters-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinNonAlphanumericCharacters() {
        return restrictMinNonAlphanumericCharacters;
    }

    /**
     * Legt den Wert der restrictMinNonAlphanumericCharacters-Eigenschaft fest.
     * 
     */
    public void setRestrictMinNonAlphanumericCharacters(boolean value) {
        this.restrictMinNonAlphanumericCharacters = value;
    }

    /**
     * Ruft den Wert der minNonAlphanumericCharacters-Eigenschaft ab.
     * 
     */
    public int getMinNonAlphanumericCharacters() {
        return minNonAlphanumericCharacters;
    }

    /**
     * Legt den Wert der minNonAlphanumericCharacters-Eigenschaft fest.
     * 
     */
    public void setMinNonAlphanumericCharacters(int value) {
        this.minNonAlphanumericCharacters = value;
    }

    /**
     * Ruft den Wert der minLength-Eigenschaft ab.
     * 
     */
    public int getMinLength() {
        return minLength;
    }

    /**
     * Legt den Wert der minLength-Eigenschaft fest.
     * 
     */
    public void setMinLength(int value) {
        this.minLength = value;
    }

    /**
     * Ruft den Wert der sendPermanentLockoutNotification-Eigenschaft ab.
     * 
     */
    public boolean isSendPermanentLockoutNotification() {
        return sendPermanentLockoutNotification;
    }

    /**
     * Legt den Wert der sendPermanentLockoutNotification-Eigenschaft fest.
     * 
     */
    public void setSendPermanentLockoutNotification(boolean value) {
        this.sendPermanentLockoutNotification = value;
    }

    /**
     * Ruft den Wert der permanentLockoutNotifyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPermanentLockoutNotifyEmailAddress() {
        return permanentLockoutNotifyEmailAddress;
    }

    /**
     * Legt den Wert der permanentLockoutNotifyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPermanentLockoutNotifyEmailAddress(String value) {
        this.permanentLockoutNotifyEmailAddress = value;
    }

    /**
     * Ruft den Wert der endpointAuthenticationLockoutType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationLockoutType }
     *     
     */
    public AuthenticationLockoutType getEndpointAuthenticationLockoutType() {
        return endpointAuthenticationLockoutType;
    }

    /**
     * Legt den Wert der endpointAuthenticationLockoutType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationLockoutType }
     *     
     */
    public void setEndpointAuthenticationLockoutType(AuthenticationLockoutType value) {
        this.endpointAuthenticationLockoutType = value;
    }

    /**
     * Ruft den Wert der endpointTemporaryLockoutThreshold-Eigenschaft ab.
     * 
     */
    public int getEndpointTemporaryLockoutThreshold() {
        return endpointTemporaryLockoutThreshold;
    }

    /**
     * Legt den Wert der endpointTemporaryLockoutThreshold-Eigenschaft fest.
     * 
     */
    public void setEndpointTemporaryLockoutThreshold(int value) {
        this.endpointTemporaryLockoutThreshold = value;
    }

    /**
     * Ruft den Wert der endpointWaitAlgorithm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationLockoutWaitAlgorithmType }
     *     
     */
    public AuthenticationLockoutWaitAlgorithmType getEndpointWaitAlgorithm() {
        return endpointWaitAlgorithm;
    }

    /**
     * Legt den Wert der endpointWaitAlgorithm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationLockoutWaitAlgorithmType }
     *     
     */
    public void setEndpointWaitAlgorithm(AuthenticationLockoutWaitAlgorithmType value) {
        this.endpointWaitAlgorithm = value;
    }

    /**
     * Ruft den Wert der endpointLockoutFixedMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndpointLockoutFixedMinutes() {
        return endpointLockoutFixedMinutes;
    }

    /**
     * Legt den Wert der endpointLockoutFixedMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndpointLockoutFixedMinutes(String value) {
        this.endpointLockoutFixedMinutes = value;
    }

    /**
     * Ruft den Wert der endpointPermanentLockoutThreshold-Eigenschaft ab.
     * 
     */
    public int getEndpointPermanentLockoutThreshold() {
        return endpointPermanentLockoutThreshold;
    }

    /**
     * Legt den Wert der endpointPermanentLockoutThreshold-Eigenschaft fest.
     * 
     */
    public void setEndpointPermanentLockoutThreshold(int value) {
        this.endpointPermanentLockoutThreshold = value;
    }

    /**
     * Ruft den Wert der trunkGroupAuthenticationLockoutType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationLockoutType }
     *     
     */
    public AuthenticationLockoutType getTrunkGroupAuthenticationLockoutType() {
        return trunkGroupAuthenticationLockoutType;
    }

    /**
     * Legt den Wert der trunkGroupAuthenticationLockoutType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationLockoutType }
     *     
     */
    public void setTrunkGroupAuthenticationLockoutType(AuthenticationLockoutType value) {
        this.trunkGroupAuthenticationLockoutType = value;
    }

    /**
     * Ruft den Wert der trunkGroupTemporaryLockoutThreshold-Eigenschaft ab.
     * 
     */
    public int getTrunkGroupTemporaryLockoutThreshold() {
        return trunkGroupTemporaryLockoutThreshold;
    }

    /**
     * Legt den Wert der trunkGroupTemporaryLockoutThreshold-Eigenschaft fest.
     * 
     */
    public void setTrunkGroupTemporaryLockoutThreshold(int value) {
        this.trunkGroupTemporaryLockoutThreshold = value;
    }

    /**
     * Ruft den Wert der trunkGroupWaitAlgorithm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationLockoutWaitAlgorithmType }
     *     
     */
    public AuthenticationLockoutWaitAlgorithmType getTrunkGroupWaitAlgorithm() {
        return trunkGroupWaitAlgorithm;
    }

    /**
     * Legt den Wert der trunkGroupWaitAlgorithm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationLockoutWaitAlgorithmType }
     *     
     */
    public void setTrunkGroupWaitAlgorithm(AuthenticationLockoutWaitAlgorithmType value) {
        this.trunkGroupWaitAlgorithm = value;
    }

    /**
     * Ruft den Wert der trunkGroupLockoutFixedMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrunkGroupLockoutFixedMinutes() {
        return trunkGroupLockoutFixedMinutes;
    }

    /**
     * Legt den Wert der trunkGroupLockoutFixedMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrunkGroupLockoutFixedMinutes(String value) {
        this.trunkGroupLockoutFixedMinutes = value;
    }

    /**
     * Ruft den Wert der trunkGroupPermanentLockoutThreshold-Eigenschaft ab.
     * 
     */
    public int getTrunkGroupPermanentLockoutThreshold() {
        return trunkGroupPermanentLockoutThreshold;
    }

    /**
     * Legt den Wert der trunkGroupPermanentLockoutThreshold-Eigenschaft fest.
     * 
     */
    public void setTrunkGroupPermanentLockoutThreshold(int value) {
        this.trunkGroupPermanentLockoutThreshold = value;
    }

}
