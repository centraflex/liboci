//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with Cr Interface.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCrInterfaceModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCrInterfaceModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="crAuditEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="crAuditIntervalMilliseconds" type="{}CrAuditIntervalMilliseconds" minOccurs="0"/>
 *         <element name="crAuditTimeoutMilliseconds" type="{}CrAuditTimeoutMilliseconds" minOccurs="0"/>
 *         <element name="crConnectionEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="crConnectionTimeoutMilliseconds" type="{}CrConnectionTimeoutMilliseconds" minOccurs="0"/>
 *         <element name="crTcpConnectionTimeoutSeconds" type="{}CrTcpConnectionTimeoutSeconds" minOccurs="0"/>
 *         <element name="crNumberOfReconnectionAttempts" type="{}CrNumberOfReconnectionAttempts" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCrInterfaceModifyRequest", propOrder = {
    "crAuditEnabled",
    "crAuditIntervalMilliseconds",
    "crAuditTimeoutMilliseconds",
    "crConnectionEnabled",
    "crConnectionTimeoutMilliseconds",
    "crTcpConnectionTimeoutSeconds",
    "crNumberOfReconnectionAttempts"
})
public class SystemCrInterfaceModifyRequest
    extends OCIRequest
{

    protected Boolean crAuditEnabled;
    protected Integer crAuditIntervalMilliseconds;
    protected Integer crAuditTimeoutMilliseconds;
    protected Boolean crConnectionEnabled;
    protected Integer crConnectionTimeoutMilliseconds;
    protected Integer crTcpConnectionTimeoutSeconds;
    protected Integer crNumberOfReconnectionAttempts;

    /**
     * Ruft den Wert der crAuditEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCrAuditEnabled() {
        return crAuditEnabled;
    }

    /**
     * Legt den Wert der crAuditEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCrAuditEnabled(Boolean value) {
        this.crAuditEnabled = value;
    }

    /**
     * Ruft den Wert der crAuditIntervalMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCrAuditIntervalMilliseconds() {
        return crAuditIntervalMilliseconds;
    }

    /**
     * Legt den Wert der crAuditIntervalMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCrAuditIntervalMilliseconds(Integer value) {
        this.crAuditIntervalMilliseconds = value;
    }

    /**
     * Ruft den Wert der crAuditTimeoutMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCrAuditTimeoutMilliseconds() {
        return crAuditTimeoutMilliseconds;
    }

    /**
     * Legt den Wert der crAuditTimeoutMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCrAuditTimeoutMilliseconds(Integer value) {
        this.crAuditTimeoutMilliseconds = value;
    }

    /**
     * Ruft den Wert der crConnectionEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCrConnectionEnabled() {
        return crConnectionEnabled;
    }

    /**
     * Legt den Wert der crConnectionEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCrConnectionEnabled(Boolean value) {
        this.crConnectionEnabled = value;
    }

    /**
     * Ruft den Wert der crConnectionTimeoutMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCrConnectionTimeoutMilliseconds() {
        return crConnectionTimeoutMilliseconds;
    }

    /**
     * Legt den Wert der crConnectionTimeoutMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCrConnectionTimeoutMilliseconds(Integer value) {
        this.crConnectionTimeoutMilliseconds = value;
    }

    /**
     * Ruft den Wert der crTcpConnectionTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCrTcpConnectionTimeoutSeconds() {
        return crTcpConnectionTimeoutSeconds;
    }

    /**
     * Legt den Wert der crTcpConnectionTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCrTcpConnectionTimeoutSeconds(Integer value) {
        this.crTcpConnectionTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der crNumberOfReconnectionAttempts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCrNumberOfReconnectionAttempts() {
        return crNumberOfReconnectionAttempts;
    }

    /**
     * Legt den Wert der crNumberOfReconnectionAttempts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCrNumberOfReconnectionAttempts(Integer value) {
        this.crNumberOfReconnectionAttempts = value;
    }

}
