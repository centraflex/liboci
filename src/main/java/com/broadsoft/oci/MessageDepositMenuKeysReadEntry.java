//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal message deposit menu keys.
 *       
 * 
 * <p>Java-Klasse für MessageDepositMenuKeysReadEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="MessageDepositMenuKeysReadEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="enableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
 *         <element name="disableMessageDeposit" type="{}DigitAny" minOccurs="0"/>
 *         <element name="listenToMessageDepositStatus" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageDepositMenuKeysReadEntry", propOrder = {
    "enableMessageDeposit",
    "disableMessageDeposit",
    "listenToMessageDepositStatus",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class MessageDepositMenuKeysReadEntry {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String enableMessageDeposit;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String disableMessageDeposit;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String listenToMessageDepositStatus;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String repeatMenu;

    /**
     * Ruft den Wert der enableMessageDeposit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnableMessageDeposit() {
        return enableMessageDeposit;
    }

    /**
     * Legt den Wert der enableMessageDeposit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnableMessageDeposit(String value) {
        this.enableMessageDeposit = value;
    }

    /**
     * Ruft den Wert der disableMessageDeposit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisableMessageDeposit() {
        return disableMessageDeposit;
    }

    /**
     * Legt den Wert der disableMessageDeposit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisableMessageDeposit(String value) {
        this.disableMessageDeposit = value;
    }

    /**
     * Ruft den Wert der listenToMessageDepositStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListenToMessageDepositStatus() {
        return listenToMessageDepositStatus;
    }

    /**
     * Legt den Wert der listenToMessageDepositStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListenToMessageDepositStatus(String value) {
        this.listenToMessageDepositStatus = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepeatMenu(String value) {
        this.repeatMenu = value;
    }

}
