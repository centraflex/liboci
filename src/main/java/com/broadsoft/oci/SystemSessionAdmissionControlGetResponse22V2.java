//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the SystemSessionAdmissionControlGetRequest22V2.
 *         The response contains the session admission control settings for the system.
 *       
 * 
 * <p>Java-Klasse für SystemSessionAdmissionControlGetResponse22V2 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSessionAdmissionControlGetResponse22V2">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="countLongConnectionsToMediaServer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sacHandlingForMoH" type="{}SessionAdmissionControlForMusicOnHoldType"/>
 *         <element name="blockVMDepositDueToSACLimits" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sacCodecSelectionPolicy" type="{}SessionAdmissionControlCodecSelectionPolicyType"/>
 *         <element name="countCallToMobileNumberForSACSubscriber" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="countBWAnywhereForSACSubscriber" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="countROForSACSubscriber" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="excludeBWMobilityForSACSubscriber" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableHoldoverOfHighwaterSessionCounts" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="holdoverPeriodMinutes" type="{}SessionAdmissionControlHighwaterSessionCountHoldoverPeriodMinutes"/>
 *         <element name="timeZoneOffsetMinutes" type="{}SessionAdmissionControlTimeZoneOffsetMinutes"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSessionAdmissionControlGetResponse22V2", propOrder = {
    "countLongConnectionsToMediaServer",
    "sacHandlingForMoH",
    "blockVMDepositDueToSACLimits",
    "sacCodecSelectionPolicy",
    "countCallToMobileNumberForSACSubscriber",
    "countBWAnywhereForSACSubscriber",
    "countROForSACSubscriber",
    "excludeBWMobilityForSACSubscriber",
    "enableHoldoverOfHighwaterSessionCounts",
    "holdoverPeriodMinutes",
    "timeZoneOffsetMinutes"
})
public class SystemSessionAdmissionControlGetResponse22V2
    extends OCIDataResponse
{

    protected boolean countLongConnectionsToMediaServer;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SessionAdmissionControlForMusicOnHoldType sacHandlingForMoH;
    protected boolean blockVMDepositDueToSACLimits;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SessionAdmissionControlCodecSelectionPolicyType sacCodecSelectionPolicy;
    protected boolean countCallToMobileNumberForSACSubscriber;
    protected boolean countBWAnywhereForSACSubscriber;
    protected boolean countROForSACSubscriber;
    protected boolean excludeBWMobilityForSACSubscriber;
    protected boolean enableHoldoverOfHighwaterSessionCounts;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String holdoverPeriodMinutes;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeZoneOffsetMinutes;

    /**
     * Ruft den Wert der countLongConnectionsToMediaServer-Eigenschaft ab.
     * 
     */
    public boolean isCountLongConnectionsToMediaServer() {
        return countLongConnectionsToMediaServer;
    }

    /**
     * Legt den Wert der countLongConnectionsToMediaServer-Eigenschaft fest.
     * 
     */
    public void setCountLongConnectionsToMediaServer(boolean value) {
        this.countLongConnectionsToMediaServer = value;
    }

    /**
     * Ruft den Wert der sacHandlingForMoH-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SessionAdmissionControlForMusicOnHoldType }
     *     
     */
    public SessionAdmissionControlForMusicOnHoldType getSacHandlingForMoH() {
        return sacHandlingForMoH;
    }

    /**
     * Legt den Wert der sacHandlingForMoH-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionAdmissionControlForMusicOnHoldType }
     *     
     */
    public void setSacHandlingForMoH(SessionAdmissionControlForMusicOnHoldType value) {
        this.sacHandlingForMoH = value;
    }

    /**
     * Ruft den Wert der blockVMDepositDueToSACLimits-Eigenschaft ab.
     * 
     */
    public boolean isBlockVMDepositDueToSACLimits() {
        return blockVMDepositDueToSACLimits;
    }

    /**
     * Legt den Wert der blockVMDepositDueToSACLimits-Eigenschaft fest.
     * 
     */
    public void setBlockVMDepositDueToSACLimits(boolean value) {
        this.blockVMDepositDueToSACLimits = value;
    }

    /**
     * Ruft den Wert der sacCodecSelectionPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SessionAdmissionControlCodecSelectionPolicyType }
     *     
     */
    public SessionAdmissionControlCodecSelectionPolicyType getSacCodecSelectionPolicy() {
        return sacCodecSelectionPolicy;
    }

    /**
     * Legt den Wert der sacCodecSelectionPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionAdmissionControlCodecSelectionPolicyType }
     *     
     */
    public void setSacCodecSelectionPolicy(SessionAdmissionControlCodecSelectionPolicyType value) {
        this.sacCodecSelectionPolicy = value;
    }

    /**
     * Ruft den Wert der countCallToMobileNumberForSACSubscriber-Eigenschaft ab.
     * 
     */
    public boolean isCountCallToMobileNumberForSACSubscriber() {
        return countCallToMobileNumberForSACSubscriber;
    }

    /**
     * Legt den Wert der countCallToMobileNumberForSACSubscriber-Eigenschaft fest.
     * 
     */
    public void setCountCallToMobileNumberForSACSubscriber(boolean value) {
        this.countCallToMobileNumberForSACSubscriber = value;
    }

    /**
     * Ruft den Wert der countBWAnywhereForSACSubscriber-Eigenschaft ab.
     * 
     */
    public boolean isCountBWAnywhereForSACSubscriber() {
        return countBWAnywhereForSACSubscriber;
    }

    /**
     * Legt den Wert der countBWAnywhereForSACSubscriber-Eigenschaft fest.
     * 
     */
    public void setCountBWAnywhereForSACSubscriber(boolean value) {
        this.countBWAnywhereForSACSubscriber = value;
    }

    /**
     * Ruft den Wert der countROForSACSubscriber-Eigenschaft ab.
     * 
     */
    public boolean isCountROForSACSubscriber() {
        return countROForSACSubscriber;
    }

    /**
     * Legt den Wert der countROForSACSubscriber-Eigenschaft fest.
     * 
     */
    public void setCountROForSACSubscriber(boolean value) {
        this.countROForSACSubscriber = value;
    }

    /**
     * Ruft den Wert der excludeBWMobilityForSACSubscriber-Eigenschaft ab.
     * 
     */
    public boolean isExcludeBWMobilityForSACSubscriber() {
        return excludeBWMobilityForSACSubscriber;
    }

    /**
     * Legt den Wert der excludeBWMobilityForSACSubscriber-Eigenschaft fest.
     * 
     */
    public void setExcludeBWMobilityForSACSubscriber(boolean value) {
        this.excludeBWMobilityForSACSubscriber = value;
    }

    /**
     * Ruft den Wert der enableHoldoverOfHighwaterSessionCounts-Eigenschaft ab.
     * 
     */
    public boolean isEnableHoldoverOfHighwaterSessionCounts() {
        return enableHoldoverOfHighwaterSessionCounts;
    }

    /**
     * Legt den Wert der enableHoldoverOfHighwaterSessionCounts-Eigenschaft fest.
     * 
     */
    public void setEnableHoldoverOfHighwaterSessionCounts(boolean value) {
        this.enableHoldoverOfHighwaterSessionCounts = value;
    }

    /**
     * Ruft den Wert der holdoverPeriodMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoldoverPeriodMinutes() {
        return holdoverPeriodMinutes;
    }

    /**
     * Legt den Wert der holdoverPeriodMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoldoverPeriodMinutes(String value) {
        this.holdoverPeriodMinutes = value;
    }

    /**
     * Ruft den Wert der timeZoneOffsetMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeZoneOffsetMinutes() {
        return timeZoneOffsetMinutes;
    }

    /**
     * Legt den Wert der timeZoneOffsetMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeZoneOffsetMinutes(String value) {
        this.timeZoneOffsetMinutes = value;
    }

}
