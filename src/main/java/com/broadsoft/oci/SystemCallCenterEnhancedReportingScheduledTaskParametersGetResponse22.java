//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemCallCenterEnhancedReportingScheduledTaskParametersGetRequest22
 *         
 *         The following elements are only used in AS data mode:
 *           callCenterEventMode, value “Legacy ECCR" is returned in Amplify data mode
 *       
 * 
 * <p>Java-Klasse für SystemCallCenterEnhancedReportingScheduledTaskParametersGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallCenterEnhancedReportingScheduledTaskParametersGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="scheduledReportSearchIntervalMinutes" type="{}CallCenterScheduledReportSearchIntervalMinutes"/>
 *         <element name="maximumScheduledReportsPerInterval" type="{}CallCenterMaximumScheduledReportsPerInterval"/>
 *         <element name="deleteScheduledReportDaysAfterCompletion" type="{}CallCenterDaysAfterScheduledReportCompletion"/>
 *         <element name="callCenterEventMode" type="{}CallCenterEventRecordingCallCenterEventMode"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallCenterEnhancedReportingScheduledTaskParametersGetResponse22", propOrder = {
    "scheduledReportSearchIntervalMinutes",
    "maximumScheduledReportsPerInterval",
    "deleteScheduledReportDaysAfterCompletion",
    "callCenterEventMode"
})
public class SystemCallCenterEnhancedReportingScheduledTaskParametersGetResponse22
    extends OCIDataResponse
{

    protected int scheduledReportSearchIntervalMinutes;
    protected int maximumScheduledReportsPerInterval;
    protected int deleteScheduledReportDaysAfterCompletion;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterEventRecordingCallCenterEventMode callCenterEventMode;

    /**
     * Ruft den Wert der scheduledReportSearchIntervalMinutes-Eigenschaft ab.
     * 
     */
    public int getScheduledReportSearchIntervalMinutes() {
        return scheduledReportSearchIntervalMinutes;
    }

    /**
     * Legt den Wert der scheduledReportSearchIntervalMinutes-Eigenschaft fest.
     * 
     */
    public void setScheduledReportSearchIntervalMinutes(int value) {
        this.scheduledReportSearchIntervalMinutes = value;
    }

    /**
     * Ruft den Wert der maximumScheduledReportsPerInterval-Eigenschaft ab.
     * 
     */
    public int getMaximumScheduledReportsPerInterval() {
        return maximumScheduledReportsPerInterval;
    }

    /**
     * Legt den Wert der maximumScheduledReportsPerInterval-Eigenschaft fest.
     * 
     */
    public void setMaximumScheduledReportsPerInterval(int value) {
        this.maximumScheduledReportsPerInterval = value;
    }

    /**
     * Ruft den Wert der deleteScheduledReportDaysAfterCompletion-Eigenschaft ab.
     * 
     */
    public int getDeleteScheduledReportDaysAfterCompletion() {
        return deleteScheduledReportDaysAfterCompletion;
    }

    /**
     * Legt den Wert der deleteScheduledReportDaysAfterCompletion-Eigenschaft fest.
     * 
     */
    public void setDeleteScheduledReportDaysAfterCompletion(int value) {
        this.deleteScheduledReportDaysAfterCompletion = value;
    }

    /**
     * Ruft den Wert der callCenterEventMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterEventRecordingCallCenterEventMode }
     *     
     */
    public CallCenterEventRecordingCallCenterEventMode getCallCenterEventMode() {
        return callCenterEventMode;
    }

    /**
     * Legt den Wert der callCenterEventMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterEventRecordingCallCenterEventMode }
     *     
     */
    public void setCallCenterEventMode(CallCenterEventRecordingCallCenterEventMode value) {
        this.callCenterEventMode = value;
    }

}
