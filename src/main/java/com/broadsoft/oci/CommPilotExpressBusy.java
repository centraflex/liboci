//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         CommPilot Express Available In Office Configuration used in the context of a get.
 *       
 * 
 * <p>Java-Klasse für CommPilotExpressBusy complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CommPilotExpressBusy">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="incomingCalls" type="{}CommPilotExpressRedirectionWithException"/>
 *         <element name="voiceMailNotify" type="{}CommPilotExpressEmailNotify"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommPilotExpressBusy", propOrder = {
    "incomingCalls",
    "voiceMailNotify"
})
public class CommPilotExpressBusy {

    @XmlElement(required = true)
    protected CommPilotExpressRedirectionWithException incomingCalls;
    @XmlElement(required = true)
    protected CommPilotExpressEmailNotify voiceMailNotify;

    /**
     * Ruft den Wert der incomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressRedirectionWithException }
     *     
     */
    public CommPilotExpressRedirectionWithException getIncomingCalls() {
        return incomingCalls;
    }

    /**
     * Legt den Wert der incomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressRedirectionWithException }
     *     
     */
    public void setIncomingCalls(CommPilotExpressRedirectionWithException value) {
        this.incomingCalls = value;
    }

    /**
     * Ruft den Wert der voiceMailNotify-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressEmailNotify }
     *     
     */
    public CommPilotExpressEmailNotify getVoiceMailNotify() {
        return voiceMailNotify;
    }

    /**
     * Legt den Wert der voiceMailNotify-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressEmailNotify }
     *     
     */
    public void setVoiceMailNotify(CommPilotExpressEmailNotify value) {
        this.voiceMailNotify = value;
    }

}
