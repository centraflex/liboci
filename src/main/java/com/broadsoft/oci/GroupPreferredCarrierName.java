//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Group can either use it's service provider/enterprise's preferred carrier or use it's own.
 *         The group carrier name is exposed if it was previously configured.
 *       
 * 
 * <p>Java-Klasse für GroupPreferredCarrierName complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupPreferredCarrierName">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="useServiceProviderPreferredCarrier" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="carrier" type="{}PreferredCarrierName" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupPreferredCarrierName", propOrder = {
    "useServiceProviderPreferredCarrier",
    "carrier"
})
public class GroupPreferredCarrierName {

    protected boolean useServiceProviderPreferredCarrier;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String carrier;

    /**
     * Ruft den Wert der useServiceProviderPreferredCarrier-Eigenschaft ab.
     * 
     */
    public boolean isUseServiceProviderPreferredCarrier() {
        return useServiceProviderPreferredCarrier;
    }

    /**
     * Legt den Wert der useServiceProviderPreferredCarrier-Eigenschaft fest.
     * 
     */
    public void setUseServiceProviderPreferredCarrier(boolean value) {
        this.useServiceProviderPreferredCarrier = value;
    }

    /**
     * Ruft den Wert der carrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     * Legt den Wert der carrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrier(String value) {
        this.carrier = value;
    }

}
