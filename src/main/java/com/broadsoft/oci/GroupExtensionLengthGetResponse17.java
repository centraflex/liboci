//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupExtensionLengthGetRequest17.
 *         
 *         Replaced by: GroupExtensionLengthGetResponse22 in AS data mode
 *       
 * 
 * <p>Java-Klasse für GroupExtensionLengthGetResponse17 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupExtensionLengthGetResponse17">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="minExtensionLength" type="{}ExtensionLength"/>
 *         <element name="maxExtensionLength" type="{}ExtensionLength"/>
 *         <element name="defaultExtensionLength" type="{}ExtensionLength"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupExtensionLengthGetResponse17", propOrder = {
    "minExtensionLength",
    "maxExtensionLength",
    "defaultExtensionLength"
})
public class GroupExtensionLengthGetResponse17
    extends OCIDataResponse
{

    protected int minExtensionLength;
    protected int maxExtensionLength;
    protected int defaultExtensionLength;

    /**
     * Ruft den Wert der minExtensionLength-Eigenschaft ab.
     * 
     */
    public int getMinExtensionLength() {
        return minExtensionLength;
    }

    /**
     * Legt den Wert der minExtensionLength-Eigenschaft fest.
     * 
     */
    public void setMinExtensionLength(int value) {
        this.minExtensionLength = value;
    }

    /**
     * Ruft den Wert der maxExtensionLength-Eigenschaft ab.
     * 
     */
    public int getMaxExtensionLength() {
        return maxExtensionLength;
    }

    /**
     * Legt den Wert der maxExtensionLength-Eigenschaft fest.
     * 
     */
    public void setMaxExtensionLength(int value) {
        this.maxExtensionLength = value;
    }

    /**
     * Ruft den Wert der defaultExtensionLength-Eigenschaft ab.
     * 
     */
    public int getDefaultExtensionLength() {
        return defaultExtensionLength;
    }

    /**
     * Legt den Wert der defaultExtensionLength-Eigenschaft fest.
     * 
     */
    public void setDefaultExtensionLength(int value) {
        this.defaultExtensionLength = value;
    }

}
