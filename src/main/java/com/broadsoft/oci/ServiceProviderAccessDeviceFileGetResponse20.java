//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to ServiceProviderAccessDeviceFileGetRequest21.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderAccessDeviceFileGetResponse20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderAccessDeviceFileGetResponse20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="fileSource" type="{}AccessDeviceEnhancedConfigurationMode"/>
 *         <element name="configurationFileName" type="{}AccessDeviceEnhancedConfigurationFileName" minOccurs="0"/>
 *         <element name="accessUrl" type="{}URL"/>
 *         <element name="repositoryUrl" type="{}URL" minOccurs="0"/>
 *         <element name="templateUrl" type="{}URL" minOccurs="0"/>
 *         <element name="extendedCaptureEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="extendedCaptureURL" type="{}URL" minOccurs="0"/>
 *         <element name="allowUploadFromDevice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderAccessDeviceFileGetResponse20", propOrder = {
    "fileSource",
    "configurationFileName",
    "accessUrl",
    "repositoryUrl",
    "templateUrl",
    "extendedCaptureEnabled",
    "extendedCaptureURL",
    "allowUploadFromDevice"
})
public class ServiceProviderAccessDeviceFileGetResponse20
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AccessDeviceEnhancedConfigurationMode fileSource;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String configurationFileName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accessUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String repositoryUrl;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String templateUrl;
    protected Boolean extendedCaptureEnabled;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String extendedCaptureURL;
    protected Boolean allowUploadFromDevice;

    /**
     * Ruft den Wert der fileSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEnhancedConfigurationMode }
     *     
     */
    public AccessDeviceEnhancedConfigurationMode getFileSource() {
        return fileSource;
    }

    /**
     * Legt den Wert der fileSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEnhancedConfigurationMode }
     *     
     */
    public void setFileSource(AccessDeviceEnhancedConfigurationMode value) {
        this.fileSource = value;
    }

    /**
     * Ruft den Wert der configurationFileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfigurationFileName() {
        return configurationFileName;
    }

    /**
     * Legt den Wert der configurationFileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfigurationFileName(String value) {
        this.configurationFileName = value;
    }

    /**
     * Ruft den Wert der accessUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessUrl() {
        return accessUrl;
    }

    /**
     * Legt den Wert der accessUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessUrl(String value) {
        this.accessUrl = value;
    }

    /**
     * Ruft den Wert der repositoryUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepositoryUrl() {
        return repositoryUrl;
    }

    /**
     * Legt den Wert der repositoryUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepositoryUrl(String value) {
        this.repositoryUrl = value;
    }

    /**
     * Ruft den Wert der templateUrl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateUrl() {
        return templateUrl;
    }

    /**
     * Legt den Wert der templateUrl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateUrl(String value) {
        this.templateUrl = value;
    }

    /**
     * Ruft den Wert der extendedCaptureEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExtendedCaptureEnabled() {
        return extendedCaptureEnabled;
    }

    /**
     * Legt den Wert der extendedCaptureEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExtendedCaptureEnabled(Boolean value) {
        this.extendedCaptureEnabled = value;
    }

    /**
     * Ruft den Wert der extendedCaptureURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtendedCaptureURL() {
        return extendedCaptureURL;
    }

    /**
     * Legt den Wert der extendedCaptureURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtendedCaptureURL(String value) {
        this.extendedCaptureURL = value;
    }

    /**
     * Ruft den Wert der allowUploadFromDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowUploadFromDevice() {
        return allowUploadFromDevice;
    }

    /**
     * Legt den Wert der allowUploadFromDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowUploadFromDevice(Boolean value) {
        this.allowUploadFromDevice = value;
    }

}
