//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupPasswordRulesGetRequest.
 *         Contains the password rules applicable to users within the group.
 *         Repleced by: GroupPasswordRulesGetResponse16
 *       
 * 
 * <p>Java-Klasse für GroupPasswordRulesGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupPasswordRulesGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="disallowUserId" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disallowOldPassword" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="disallowReversedOldPassword" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="restrictMinDigits" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minDigits" type="{}PasswordMinDigits"/>
 *         <element name="restrictMinUpperCaseLetters" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minUpperCaseLetters" type="{}PasswordMinUpperCaseLetters"/>
 *         <element name="restrictMinLowerCaseLetters" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minLowerCaseLetters" type="{}PasswordMinLowerCaseLetters"/>
 *         <element name="restrictMinNonAlphanumericCharacters" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="minNonAlphanumericCharacters" type="{}PasswordMinNonAlphanumericCharacters"/>
 *         <element name="minLength" type="{}PasswordMinLength"/>
 *         <element name="maxFailedLoginAttempts" type="{}MaxFailedLoginAttempts"/>
 *         <element name="passwordExpiresDays" type="{}PasswordExpiresDays"/>
 *         <element name="sendLoginDisabledNotifyEmail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="loginDisabledNotifyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="disallowRulesModification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupPasswordRulesGetResponse", propOrder = {
    "serviceProviderId",
    "groupId",
    "disallowUserId",
    "disallowOldPassword",
    "disallowReversedOldPassword",
    "restrictMinDigits",
    "minDigits",
    "restrictMinUpperCaseLetters",
    "minUpperCaseLetters",
    "restrictMinLowerCaseLetters",
    "minLowerCaseLetters",
    "restrictMinNonAlphanumericCharacters",
    "minNonAlphanumericCharacters",
    "minLength",
    "maxFailedLoginAttempts",
    "passwordExpiresDays",
    "sendLoginDisabledNotifyEmail",
    "loginDisabledNotifyEmailAddress",
    "disallowRulesModification"
})
public class GroupPasswordRulesGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected boolean disallowUserId;
    protected boolean disallowOldPassword;
    protected boolean disallowReversedOldPassword;
    protected boolean restrictMinDigits;
    protected int minDigits;
    protected boolean restrictMinUpperCaseLetters;
    protected int minUpperCaseLetters;
    protected boolean restrictMinLowerCaseLetters;
    protected int minLowerCaseLetters;
    protected boolean restrictMinNonAlphanumericCharacters;
    protected int minNonAlphanumericCharacters;
    protected int minLength;
    protected int maxFailedLoginAttempts;
    protected int passwordExpiresDays;
    protected boolean sendLoginDisabledNotifyEmail;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String loginDisabledNotifyEmailAddress;
    protected boolean disallowRulesModification;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der disallowUserId-Eigenschaft ab.
     * 
     */
    public boolean isDisallowUserId() {
        return disallowUserId;
    }

    /**
     * Legt den Wert der disallowUserId-Eigenschaft fest.
     * 
     */
    public void setDisallowUserId(boolean value) {
        this.disallowUserId = value;
    }

    /**
     * Ruft den Wert der disallowOldPassword-Eigenschaft ab.
     * 
     */
    public boolean isDisallowOldPassword() {
        return disallowOldPassword;
    }

    /**
     * Legt den Wert der disallowOldPassword-Eigenschaft fest.
     * 
     */
    public void setDisallowOldPassword(boolean value) {
        this.disallowOldPassword = value;
    }

    /**
     * Ruft den Wert der disallowReversedOldPassword-Eigenschaft ab.
     * 
     */
    public boolean isDisallowReversedOldPassword() {
        return disallowReversedOldPassword;
    }

    /**
     * Legt den Wert der disallowReversedOldPassword-Eigenschaft fest.
     * 
     */
    public void setDisallowReversedOldPassword(boolean value) {
        this.disallowReversedOldPassword = value;
    }

    /**
     * Ruft den Wert der restrictMinDigits-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinDigits() {
        return restrictMinDigits;
    }

    /**
     * Legt den Wert der restrictMinDigits-Eigenschaft fest.
     * 
     */
    public void setRestrictMinDigits(boolean value) {
        this.restrictMinDigits = value;
    }

    /**
     * Ruft den Wert der minDigits-Eigenschaft ab.
     * 
     */
    public int getMinDigits() {
        return minDigits;
    }

    /**
     * Legt den Wert der minDigits-Eigenschaft fest.
     * 
     */
    public void setMinDigits(int value) {
        this.minDigits = value;
    }

    /**
     * Ruft den Wert der restrictMinUpperCaseLetters-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinUpperCaseLetters() {
        return restrictMinUpperCaseLetters;
    }

    /**
     * Legt den Wert der restrictMinUpperCaseLetters-Eigenschaft fest.
     * 
     */
    public void setRestrictMinUpperCaseLetters(boolean value) {
        this.restrictMinUpperCaseLetters = value;
    }

    /**
     * Ruft den Wert der minUpperCaseLetters-Eigenschaft ab.
     * 
     */
    public int getMinUpperCaseLetters() {
        return minUpperCaseLetters;
    }

    /**
     * Legt den Wert der minUpperCaseLetters-Eigenschaft fest.
     * 
     */
    public void setMinUpperCaseLetters(int value) {
        this.minUpperCaseLetters = value;
    }

    /**
     * Ruft den Wert der restrictMinLowerCaseLetters-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinLowerCaseLetters() {
        return restrictMinLowerCaseLetters;
    }

    /**
     * Legt den Wert der restrictMinLowerCaseLetters-Eigenschaft fest.
     * 
     */
    public void setRestrictMinLowerCaseLetters(boolean value) {
        this.restrictMinLowerCaseLetters = value;
    }

    /**
     * Ruft den Wert der minLowerCaseLetters-Eigenschaft ab.
     * 
     */
    public int getMinLowerCaseLetters() {
        return minLowerCaseLetters;
    }

    /**
     * Legt den Wert der minLowerCaseLetters-Eigenschaft fest.
     * 
     */
    public void setMinLowerCaseLetters(int value) {
        this.minLowerCaseLetters = value;
    }

    /**
     * Ruft den Wert der restrictMinNonAlphanumericCharacters-Eigenschaft ab.
     * 
     */
    public boolean isRestrictMinNonAlphanumericCharacters() {
        return restrictMinNonAlphanumericCharacters;
    }

    /**
     * Legt den Wert der restrictMinNonAlphanumericCharacters-Eigenschaft fest.
     * 
     */
    public void setRestrictMinNonAlphanumericCharacters(boolean value) {
        this.restrictMinNonAlphanumericCharacters = value;
    }

    /**
     * Ruft den Wert der minNonAlphanumericCharacters-Eigenschaft ab.
     * 
     */
    public int getMinNonAlphanumericCharacters() {
        return minNonAlphanumericCharacters;
    }

    /**
     * Legt den Wert der minNonAlphanumericCharacters-Eigenschaft fest.
     * 
     */
    public void setMinNonAlphanumericCharacters(int value) {
        this.minNonAlphanumericCharacters = value;
    }

    /**
     * Ruft den Wert der minLength-Eigenschaft ab.
     * 
     */
    public int getMinLength() {
        return minLength;
    }

    /**
     * Legt den Wert der minLength-Eigenschaft fest.
     * 
     */
    public void setMinLength(int value) {
        this.minLength = value;
    }

    /**
     * Ruft den Wert der maxFailedLoginAttempts-Eigenschaft ab.
     * 
     */
    public int getMaxFailedLoginAttempts() {
        return maxFailedLoginAttempts;
    }

    /**
     * Legt den Wert der maxFailedLoginAttempts-Eigenschaft fest.
     * 
     */
    public void setMaxFailedLoginAttempts(int value) {
        this.maxFailedLoginAttempts = value;
    }

    /**
     * Ruft den Wert der passwordExpiresDays-Eigenschaft ab.
     * 
     */
    public int getPasswordExpiresDays() {
        return passwordExpiresDays;
    }

    /**
     * Legt den Wert der passwordExpiresDays-Eigenschaft fest.
     * 
     */
    public void setPasswordExpiresDays(int value) {
        this.passwordExpiresDays = value;
    }

    /**
     * Ruft den Wert der sendLoginDisabledNotifyEmail-Eigenschaft ab.
     * 
     */
    public boolean isSendLoginDisabledNotifyEmail() {
        return sendLoginDisabledNotifyEmail;
    }

    /**
     * Legt den Wert der sendLoginDisabledNotifyEmail-Eigenschaft fest.
     * 
     */
    public void setSendLoginDisabledNotifyEmail(boolean value) {
        this.sendLoginDisabledNotifyEmail = value;
    }

    /**
     * Ruft den Wert der loginDisabledNotifyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoginDisabledNotifyEmailAddress() {
        return loginDisabledNotifyEmailAddress;
    }

    /**
     * Legt den Wert der loginDisabledNotifyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoginDisabledNotifyEmailAddress(String value) {
        this.loginDisabledNotifyEmailAddress = value;
    }

    /**
     * Ruft den Wert der disallowRulesModification-Eigenschaft ab.
     * 
     */
    public boolean isDisallowRulesModification() {
        return disallowRulesModification;
    }

    /**
     * Legt den Wert der disallowRulesModification-Eigenschaft fest.
     * 
     */
    public void setDisallowRulesModification(boolean value) {
        this.disallowRulesModification = value;
    }

}
