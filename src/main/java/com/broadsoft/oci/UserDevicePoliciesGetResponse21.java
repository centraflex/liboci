//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *           Response to UserDevicePoliciesGetRequest21. enableDeviceFeatureSynchronization and 
 *           enableCallDecline are ignored by the application server in Multiple User Shared mode.
 *         The following element is only used in AS data mode:
 *           lineMode, value “Single User Private and Shared” is returned in XS data mode
 *         The following elements are only used in AS data mode:
 *           enableDeviceFeatureSynchronization, value “false” is returned in XS data mode
 *           enableDnd, value “false” is returned in XS data mode
 *           enableCallForwardingAlways, value “false” is returned in XS data mode
 *           enableCallForwardingBusy, value “false” is returned in XS data mode
 *           enableCallForwardingNoAnswer, value “false” is returned in XS data mode
 *           enableAcd, value “false” is returned in XS data mode
 *           enableExecutive, value “false” is returned in XS data mode
 *           enableExecutiveAssistant, value “false” is returned in XS data mode
 *           enableSecurityClassification, value “false” is returned in XS data mode
 *           enableCallRecording, value “false” is returned in XS data mode
 *         
 * 
 * <p>Java-Klasse für UserDevicePoliciesGetResponse21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserDevicePoliciesGetResponse21">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="lineMode" type="{}UserDevicePolicyLineMode"/>
 *         <element name="enableDeviceFeatureSynchronization" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableDnd" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableCallForwardingAlways" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableCallForwardingBusy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableCallForwardingNoAnswer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableAcd" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableExecutive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableExecutiveAssistant" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableSecurityClassification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableCallRecording" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableCallDecline" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDevicePoliciesGetResponse21", propOrder = {
    "lineMode",
    "enableDeviceFeatureSynchronization",
    "enableDnd",
    "enableCallForwardingAlways",
    "enableCallForwardingBusy",
    "enableCallForwardingNoAnswer",
    "enableAcd",
    "enableExecutive",
    "enableExecutiveAssistant",
    "enableSecurityClassification",
    "enableCallRecording",
    "enableCallDecline"
})
public class UserDevicePoliciesGetResponse21
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected UserDevicePolicyLineMode lineMode;
    protected boolean enableDeviceFeatureSynchronization;
    protected boolean enableDnd;
    protected boolean enableCallForwardingAlways;
    protected boolean enableCallForwardingBusy;
    protected boolean enableCallForwardingNoAnswer;
    protected boolean enableAcd;
    protected boolean enableExecutive;
    protected boolean enableExecutiveAssistant;
    protected boolean enableSecurityClassification;
    protected boolean enableCallRecording;
    protected boolean enableCallDecline;

    /**
     * Ruft den Wert der lineMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UserDevicePolicyLineMode }
     *     
     */
    public UserDevicePolicyLineMode getLineMode() {
        return lineMode;
    }

    /**
     * Legt den Wert der lineMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDevicePolicyLineMode }
     *     
     */
    public void setLineMode(UserDevicePolicyLineMode value) {
        this.lineMode = value;
    }

    /**
     * Ruft den Wert der enableDeviceFeatureSynchronization-Eigenschaft ab.
     * 
     */
    public boolean isEnableDeviceFeatureSynchronization() {
        return enableDeviceFeatureSynchronization;
    }

    /**
     * Legt den Wert der enableDeviceFeatureSynchronization-Eigenschaft fest.
     * 
     */
    public void setEnableDeviceFeatureSynchronization(boolean value) {
        this.enableDeviceFeatureSynchronization = value;
    }

    /**
     * Ruft den Wert der enableDnd-Eigenschaft ab.
     * 
     */
    public boolean isEnableDnd() {
        return enableDnd;
    }

    /**
     * Legt den Wert der enableDnd-Eigenschaft fest.
     * 
     */
    public void setEnableDnd(boolean value) {
        this.enableDnd = value;
    }

    /**
     * Ruft den Wert der enableCallForwardingAlways-Eigenschaft ab.
     * 
     */
    public boolean isEnableCallForwardingAlways() {
        return enableCallForwardingAlways;
    }

    /**
     * Legt den Wert der enableCallForwardingAlways-Eigenschaft fest.
     * 
     */
    public void setEnableCallForwardingAlways(boolean value) {
        this.enableCallForwardingAlways = value;
    }

    /**
     * Ruft den Wert der enableCallForwardingBusy-Eigenschaft ab.
     * 
     */
    public boolean isEnableCallForwardingBusy() {
        return enableCallForwardingBusy;
    }

    /**
     * Legt den Wert der enableCallForwardingBusy-Eigenschaft fest.
     * 
     */
    public void setEnableCallForwardingBusy(boolean value) {
        this.enableCallForwardingBusy = value;
    }

    /**
     * Ruft den Wert der enableCallForwardingNoAnswer-Eigenschaft ab.
     * 
     */
    public boolean isEnableCallForwardingNoAnswer() {
        return enableCallForwardingNoAnswer;
    }

    /**
     * Legt den Wert der enableCallForwardingNoAnswer-Eigenschaft fest.
     * 
     */
    public void setEnableCallForwardingNoAnswer(boolean value) {
        this.enableCallForwardingNoAnswer = value;
    }

    /**
     * Ruft den Wert der enableAcd-Eigenschaft ab.
     * 
     */
    public boolean isEnableAcd() {
        return enableAcd;
    }

    /**
     * Legt den Wert der enableAcd-Eigenschaft fest.
     * 
     */
    public void setEnableAcd(boolean value) {
        this.enableAcd = value;
    }

    /**
     * Ruft den Wert der enableExecutive-Eigenschaft ab.
     * 
     */
    public boolean isEnableExecutive() {
        return enableExecutive;
    }

    /**
     * Legt den Wert der enableExecutive-Eigenschaft fest.
     * 
     */
    public void setEnableExecutive(boolean value) {
        this.enableExecutive = value;
    }

    /**
     * Ruft den Wert der enableExecutiveAssistant-Eigenschaft ab.
     * 
     */
    public boolean isEnableExecutiveAssistant() {
        return enableExecutiveAssistant;
    }

    /**
     * Legt den Wert der enableExecutiveAssistant-Eigenschaft fest.
     * 
     */
    public void setEnableExecutiveAssistant(boolean value) {
        this.enableExecutiveAssistant = value;
    }

    /**
     * Ruft den Wert der enableSecurityClassification-Eigenschaft ab.
     * 
     */
    public boolean isEnableSecurityClassification() {
        return enableSecurityClassification;
    }

    /**
     * Legt den Wert der enableSecurityClassification-Eigenschaft fest.
     * 
     */
    public void setEnableSecurityClassification(boolean value) {
        this.enableSecurityClassification = value;
    }

    /**
     * Ruft den Wert der enableCallRecording-Eigenschaft ab.
     * 
     */
    public boolean isEnableCallRecording() {
        return enableCallRecording;
    }

    /**
     * Legt den Wert der enableCallRecording-Eigenschaft fest.
     * 
     */
    public void setEnableCallRecording(boolean value) {
        this.enableCallRecording = value;
    }

    /**
     * Ruft den Wert der enableCallDecline-Eigenschaft ab.
     * 
     */
    public boolean isEnableCallDecline() {
        return enableCallDecline;
    }

    /**
     * Legt den Wert der enableCallDecline-Eigenschaft fest.
     * 
     */
    public void setEnableCallDecline(boolean value) {
        this.enableCallDecline = value;
    }

}
