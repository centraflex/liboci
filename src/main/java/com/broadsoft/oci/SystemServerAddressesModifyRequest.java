//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify Server Addresses.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemServerAddressesModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemServerAddressesModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="webServerClusterPublicFQDN" type="{}NetAddress" minOccurs="0"/>
 *         <element name="applicationServerClusterPrimaryPublicFQDN" type="{}NetAddress" minOccurs="0"/>
 *         <element name="applicationServerClusterSecondaryPublicFQDN" type="{}NetAddress" minOccurs="0"/>
 *         <element name="applicationServerClusterPrimaryPrivateFQDN" type="{}NetAddress" minOccurs="0"/>
 *         <element name="applicationServerClusterSecondaryPrivateFQDN" type="{}NetAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemServerAddressesModifyRequest", propOrder = {
    "webServerClusterPublicFQDN",
    "applicationServerClusterPrimaryPublicFQDN",
    "applicationServerClusterSecondaryPublicFQDN",
    "applicationServerClusterPrimaryPrivateFQDN",
    "applicationServerClusterSecondaryPrivateFQDN"
})
public class SystemServerAddressesModifyRequest
    extends OCIRequest
{

    @XmlElementRef(name = "webServerClusterPublicFQDN", type = JAXBElement.class, required = false)
    protected JAXBElement<String> webServerClusterPublicFQDN;
    @XmlElementRef(name = "applicationServerClusterPrimaryPublicFQDN", type = JAXBElement.class, required = false)
    protected JAXBElement<String> applicationServerClusterPrimaryPublicFQDN;
    @XmlElementRef(name = "applicationServerClusterSecondaryPublicFQDN", type = JAXBElement.class, required = false)
    protected JAXBElement<String> applicationServerClusterSecondaryPublicFQDN;
    @XmlElementRef(name = "applicationServerClusterPrimaryPrivateFQDN", type = JAXBElement.class, required = false)
    protected JAXBElement<String> applicationServerClusterPrimaryPrivateFQDN;
    @XmlElementRef(name = "applicationServerClusterSecondaryPrivateFQDN", type = JAXBElement.class, required = false)
    protected JAXBElement<String> applicationServerClusterSecondaryPrivateFQDN;

    /**
     * Ruft den Wert der webServerClusterPublicFQDN-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWebServerClusterPublicFQDN() {
        return webServerClusterPublicFQDN;
    }

    /**
     * Legt den Wert der webServerClusterPublicFQDN-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWebServerClusterPublicFQDN(JAXBElement<String> value) {
        this.webServerClusterPublicFQDN = value;
    }

    /**
     * Ruft den Wert der applicationServerClusterPrimaryPublicFQDN-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getApplicationServerClusterPrimaryPublicFQDN() {
        return applicationServerClusterPrimaryPublicFQDN;
    }

    /**
     * Legt den Wert der applicationServerClusterPrimaryPublicFQDN-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setApplicationServerClusterPrimaryPublicFQDN(JAXBElement<String> value) {
        this.applicationServerClusterPrimaryPublicFQDN = value;
    }

    /**
     * Ruft den Wert der applicationServerClusterSecondaryPublicFQDN-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getApplicationServerClusterSecondaryPublicFQDN() {
        return applicationServerClusterSecondaryPublicFQDN;
    }

    /**
     * Legt den Wert der applicationServerClusterSecondaryPublicFQDN-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setApplicationServerClusterSecondaryPublicFQDN(JAXBElement<String> value) {
        this.applicationServerClusterSecondaryPublicFQDN = value;
    }

    /**
     * Ruft den Wert der applicationServerClusterPrimaryPrivateFQDN-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getApplicationServerClusterPrimaryPrivateFQDN() {
        return applicationServerClusterPrimaryPrivateFQDN;
    }

    /**
     * Legt den Wert der applicationServerClusterPrimaryPrivateFQDN-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setApplicationServerClusterPrimaryPrivateFQDN(JAXBElement<String> value) {
        this.applicationServerClusterPrimaryPrivateFQDN = value;
    }

    /**
     * Ruft den Wert der applicationServerClusterSecondaryPrivateFQDN-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getApplicationServerClusterSecondaryPrivateFQDN() {
        return applicationServerClusterSecondaryPrivateFQDN;
    }

    /**
     * Legt den Wert der applicationServerClusterSecondaryPrivateFQDN-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setApplicationServerClusterSecondaryPrivateFQDN(JAXBElement<String> value) {
        this.applicationServerClusterSecondaryPrivateFQDN = value;
    }

}
