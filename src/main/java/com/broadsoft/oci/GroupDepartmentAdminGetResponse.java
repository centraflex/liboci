//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupDepartmentAdminGetRequest.
 *         The response contains the group department administrators profile information.
 *       
 * 
 * <p>Java-Klasse für GroupDepartmentAdminGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDepartmentAdminGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="departmentKey" type="{}GroupDepartmentKey"/>
 *         <element name="departmentFullPath" type="{}DepartmentName"/>
 *         <element name="firstName" type="{}FirstName" minOccurs="0"/>
 *         <element name="lastName" type="{}LastName" minOccurs="0"/>
 *         <element name="language" type="{}Language"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDepartmentAdminGetResponse", propOrder = {
    "departmentKey",
    "departmentFullPath",
    "firstName",
    "lastName",
    "language"
})
public class GroupDepartmentAdminGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected GroupDepartmentKey departmentKey;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String departmentFullPath;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String firstName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String lastName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String language;

    /**
     * Ruft den Wert der departmentKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentKey }
     *     
     */
    public GroupDepartmentKey getDepartmentKey() {
        return departmentKey;
    }

    /**
     * Legt den Wert der departmentKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentKey }
     *     
     */
    public void setDepartmentKey(GroupDepartmentKey value) {
        this.departmentKey = value;
    }

    /**
     * Ruft den Wert der departmentFullPath-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartmentFullPath() {
        return departmentFullPath;
    }

    /**
     * Legt den Wert der departmentFullPath-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartmentFullPath(String value) {
        this.departmentFullPath = value;
    }

    /**
     * Ruft den Wert der firstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Legt den Wert der firstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Ruft den Wert der lastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Legt den Wert der lastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Ruft den Wert der language-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Legt den Wert der language-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

}
