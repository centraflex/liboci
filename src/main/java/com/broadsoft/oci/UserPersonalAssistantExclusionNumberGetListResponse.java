//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserPersonalAssistantExclusionNumberGetListRequest.
 *         Contains a table with column headings:
 *         "Number", "Description".
 *       
 * 
 * <p>Java-Klasse für UserPersonalAssistantExclusionNumberGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPersonalAssistantExclusionNumberGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="exclusionNumberTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPersonalAssistantExclusionNumberGetListResponse", propOrder = {
    "exclusionNumberTable"
})
public class UserPersonalAssistantExclusionNumberGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable exclusionNumberTable;

    /**
     * Ruft den Wert der exclusionNumberTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getExclusionNumberTable() {
        return exclusionNumberTable;
    }

    /**
     * Legt den Wert der exclusionNumberTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setExclusionNumberTable(OCITable value) {
        this.exclusionNumberTable = value;
    }

}
