//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a filtering criteria to an executive user.
 *         Both executive and the executive assistant can run this command.
 *         For the callToNumber, the extension element is not used and the number element is only used when the type is BroadWorks Mobility.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserExecutiveAddFilteringSelectiveCriteriaRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserExecutiveAddFilteringSelectiveCriteriaRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="criteriaName" type="{}CriteriaName"/>
 *         <element name="timeSchedule" type="{}TimeSchedule" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}HolidaySchedule" minOccurs="0"/>
 *         <element name="filter" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="fromDnCriteria" type="{}ExecutiveCallFilteringCriteriaFromDn"/>
 *         <element name="callToNumber" type="{}CallToNumber" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserExecutiveAddFilteringSelectiveCriteriaRequest", propOrder = {
    "userId",
    "criteriaName",
    "timeSchedule",
    "holidaySchedule",
    "filter",
    "fromDnCriteria",
    "callToNumber"
})
public class UserExecutiveAddFilteringSelectiveCriteriaRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String criteriaName;
    protected TimeSchedule timeSchedule;
    protected HolidaySchedule holidaySchedule;
    protected boolean filter;
    @XmlElement(required = true)
    protected ExecutiveCallFilteringCriteriaFromDn fromDnCriteria;
    protected List<CallToNumber> callToNumber;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der criteriaName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCriteriaName() {
        return criteriaName;
    }

    /**
     * Legt den Wert der criteriaName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCriteriaName(String value) {
        this.criteriaName = value;
    }

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeSchedule }
     *     
     */
    public TimeSchedule getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeSchedule }
     *     
     */
    public void setTimeSchedule(TimeSchedule value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HolidaySchedule }
     *     
     */
    public HolidaySchedule getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HolidaySchedule }
     *     
     */
    public void setHolidaySchedule(HolidaySchedule value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der filter-Eigenschaft ab.
     * 
     */
    public boolean isFilter() {
        return filter;
    }

    /**
     * Legt den Wert der filter-Eigenschaft fest.
     * 
     */
    public void setFilter(boolean value) {
        this.filter = value;
    }

    /**
     * Ruft den Wert der fromDnCriteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveCallFilteringCriteriaFromDn }
     *     
     */
    public ExecutiveCallFilteringCriteriaFromDn getFromDnCriteria() {
        return fromDnCriteria;
    }

    /**
     * Legt den Wert der fromDnCriteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveCallFilteringCriteriaFromDn }
     *     
     */
    public void setFromDnCriteria(ExecutiveCallFilteringCriteriaFromDn value) {
        this.fromDnCriteria = value;
    }

    /**
     * Gets the value of the callToNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the callToNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCallToNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallToNumber }
     * 
     * 
     * @return
     *     The value of the callToNumber property.
     */
    public List<CallToNumber> getCallToNumber() {
        if (callToNumber == null) {
            callToNumber = new ArrayList<>();
        }
        return this.callToNumber;
    }

}
