//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the TutorialFlagGetRequest.
 *       
 * 
 * <p>Java-Klasse für TutorialFlagGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="TutorialFlagGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableTutorial" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TutorialFlagGetResponse", propOrder = {
    "enableTutorial"
})
public class TutorialFlagGetResponse
    extends OCIDataResponse
{

    protected boolean enableTutorial;

    /**
     * Ruft den Wert der enableTutorial-Eigenschaft ab.
     * 
     */
    public boolean isEnableTutorial() {
        return enableTutorial;
    }

    /**
     * Legt den Wert der enableTutorial-Eigenschaft fest.
     * 
     */
    public void setEnableTutorial(boolean value) {
        this.enableTutorial = value;
    }

}
