//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         AuthenticationRequest/Response is 1st stage of the 2 stage OCI login process.
 *       
 * 
 * <p>Java-Klasse für AuthenticationResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="AuthenticationResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="nonce" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         <element name="passwordAlgorithm" type="{}DigitalSignatureAlgorithm"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuthenticationResponse", propOrder = {
    "userId",
    "nonce",
    "passwordAlgorithm"
})
public class AuthenticationResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    protected String nonce;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected DigitalSignatureAlgorithm passwordAlgorithm;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der nonce-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonce() {
        return nonce;
    }

    /**
     * Legt den Wert der nonce-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonce(String value) {
        this.nonce = value;
    }

    /**
     * Ruft den Wert der passwordAlgorithm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DigitalSignatureAlgorithm }
     *     
     */
    public DigitalSignatureAlgorithm getPasswordAlgorithm() {
        return passwordAlgorithm;
    }

    /**
     * Legt den Wert der passwordAlgorithm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DigitalSignatureAlgorithm }
     *     
     */
    public void setPasswordAlgorithm(DigitalSignatureAlgorithm value) {
        this.passwordAlgorithm = value;
    }

}
