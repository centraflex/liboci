//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the EnterpriseUserCallForwardingSettingsGetListRequest.
 *         Contains a table with column headings: "Group Id", "User Id", "Last Name",
 *         "First Name", "Hiragana Last Name", and "Hiragana First Name",
 *         "Phone Number", "Extension", "Department", "In Trunk Group",
 *         "Email Address", "Is Active", "Forwarding Address".
 *         "Is Active" is "true" or "false".
 *         The "Forwarding Address" is the Call Forwarding service's forwarding address. 
 *         If the service is Call Forwarding Selective, the default forwarding address is returned.
 *         "Phone Number" field is presented in the E164 format.
 *       
 * 
 * <p>Java-Klasse für EnterpriseUserCallForwardingSettingsGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseUserCallForwardingSettingsGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="userCallForwardingTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseUserCallForwardingSettingsGetListResponse", propOrder = {
    "userCallForwardingTable"
})
public class EnterpriseUserCallForwardingSettingsGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable userCallForwardingTable;

    /**
     * Ruft den Wert der userCallForwardingTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getUserCallForwardingTable() {
        return userCallForwardingTable;
    }

    /**
     * Legt den Wert der userCallForwardingTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setUserCallForwardingTable(OCITable value) {
        this.userCallForwardingTable = value;
    }

}
