//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the SystemBroadWorksMobileManagerGetRequest
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobileManagerGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobileManagerGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="scfApiNetAddress1" type="{}NetAddress" minOccurs="0"/>
 *         <element name="scfApiNetAddress2" type="{}NetAddress" minOccurs="0"/>
 *         <element name="userName" type="{}BroadWorksMobileManagerUserName" minOccurs="0"/>
 *         <element name="emailFromAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="scfIMSOnly" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="signalingIPAddress" type="{}IPAddress" minOccurs="0"/>
 *         <element name="signalingPort" type="{}Port" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobileManagerGetResponse", propOrder = {
    "scfApiNetAddress1",
    "scfApiNetAddress2",
    "userName",
    "emailFromAddress",
    "scfIMSOnly",
    "signalingIPAddress",
    "signalingPort"
})
public class SystemBroadWorksMobileManagerGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String scfApiNetAddress1;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String scfApiNetAddress2;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String emailFromAddress;
    protected boolean scfIMSOnly;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String signalingIPAddress;
    protected Integer signalingPort;

    /**
     * Ruft den Wert der scfApiNetAddress1-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScfApiNetAddress1() {
        return scfApiNetAddress1;
    }

    /**
     * Legt den Wert der scfApiNetAddress1-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScfApiNetAddress1(String value) {
        this.scfApiNetAddress1 = value;
    }

    /**
     * Ruft den Wert der scfApiNetAddress2-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScfApiNetAddress2() {
        return scfApiNetAddress2;
    }

    /**
     * Legt den Wert der scfApiNetAddress2-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScfApiNetAddress2(String value) {
        this.scfApiNetAddress2 = value;
    }

    /**
     * Ruft den Wert der userName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Legt den Wert der userName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Ruft den Wert der emailFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailFromAddress() {
        return emailFromAddress;
    }

    /**
     * Legt den Wert der emailFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailFromAddress(String value) {
        this.emailFromAddress = value;
    }

    /**
     * Ruft den Wert der scfIMSOnly-Eigenschaft ab.
     * 
     */
    public boolean isScfIMSOnly() {
        return scfIMSOnly;
    }

    /**
     * Legt den Wert der scfIMSOnly-Eigenschaft fest.
     * 
     */
    public void setScfIMSOnly(boolean value) {
        this.scfIMSOnly = value;
    }

    /**
     * Ruft den Wert der signalingIPAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSignalingIPAddress() {
        return signalingIPAddress;
    }

    /**
     * Legt den Wert der signalingIPAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSignalingIPAddress(String value) {
        this.signalingIPAddress = value;
    }

    /**
     * Ruft den Wert der signalingPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSignalingPort() {
        return signalingPort;
    }

    /**
     * Legt den Wert der signalingPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSignalingPort(Integer value) {
        this.signalingPort = value;
    }

}
