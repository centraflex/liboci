//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserAssignedServicesGetListRequest.
 *         A user can have both user services and group services because of music on hold.
 *       
 * 
 * <p>Java-Klasse für UserAssignedServicesGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserAssignedServicesGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="groupServiceEntry" type="{}AssignedGroupServicesEntry" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="userServiceEntry" type="{}AssignedUserServicesEntry" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserAssignedServicesGetListResponse", propOrder = {
    "groupServiceEntry",
    "userServiceEntry"
})
public class UserAssignedServicesGetListResponse
    extends OCIDataResponse
{

    protected List<AssignedGroupServicesEntry> groupServiceEntry;
    protected List<AssignedUserServicesEntry> userServiceEntry;

    /**
     * Gets the value of the groupServiceEntry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the groupServiceEntry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupServiceEntry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssignedGroupServicesEntry }
     * 
     * 
     * @return
     *     The value of the groupServiceEntry property.
     */
    public List<AssignedGroupServicesEntry> getGroupServiceEntry() {
        if (groupServiceEntry == null) {
            groupServiceEntry = new ArrayList<>();
        }
        return this.groupServiceEntry;
    }

    /**
     * Gets the value of the userServiceEntry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the userServiceEntry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserServiceEntry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssignedUserServicesEntry }
     * 
     * 
     * @return
     *     The value of the userServiceEntry property.
     */
    public List<AssignedUserServicesEntry> getUserServiceEntry() {
        if (userServiceEntry == null) {
            userServiceEntry = new ArrayList<>();
        }
        return this.userServiceEntry;
    }

}
