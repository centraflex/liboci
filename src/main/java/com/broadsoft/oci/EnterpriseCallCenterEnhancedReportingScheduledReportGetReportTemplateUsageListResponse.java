//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to EnterpriseCallCenterEnhancedReportingScheduledReportGetReportTemplateUsageListRequest. 
 *         Contains a table with column headings: "Schedule Name", "Created By", "Created By Supervisor", and
 *         "Is Active".
 *         The "Created By" can be either "Administrator" or user id if created by supervisor.
 *       
 * 
 * <p>Java-Klasse für EnterpriseCallCenterEnhancedReportingScheduledReportGetReportTemplateUsageListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseCallCenterEnhancedReportingScheduledReportGetReportTemplateUsageListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="scheduleReportTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseCallCenterEnhancedReportingScheduledReportGetReportTemplateUsageListResponse", propOrder = {
    "scheduleReportTable"
})
public class EnterpriseCallCenterEnhancedReportingScheduledReportGetReportTemplateUsageListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable scheduleReportTable;

    /**
     * Ruft den Wert der scheduleReportTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getScheduleReportTable() {
        return scheduleReportTable;
    }

    /**
     * Legt den Wert der scheduleReportTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setScheduleReportTable(OCITable value) {
        this.scheduleReportTable = value;
    }

}
