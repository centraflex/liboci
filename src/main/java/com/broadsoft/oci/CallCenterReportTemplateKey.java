//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Uniquely identifies a call center report template created in the system.
 *       
 * 
 * <p>Java-Klasse für CallCenterReportTemplateKey complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterReportTemplateKey">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="templateLevel" type="{}CallCenterReportTemplateLevel"/>
 *         <element name="templateName" type="{}CallCenterReportTemplateName"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterReportTemplateKey", propOrder = {
    "templateLevel",
    "templateName"
})
public class CallCenterReportTemplateKey {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportTemplateLevel templateLevel;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String templateName;

    /**
     * Ruft den Wert der templateLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportTemplateLevel }
     *     
     */
    public CallCenterReportTemplateLevel getTemplateLevel() {
        return templateLevel;
    }

    /**
     * Legt den Wert der templateLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportTemplateLevel }
     *     
     */
    public void setTemplateLevel(CallCenterReportTemplateLevel value) {
        this.templateLevel = value;
    }

    /**
     * Ruft den Wert der templateName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateName() {
        return templateName;
    }

    /**
     * Legt den Wert der templateName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateName(String value) {
        this.templateName = value;
    }

}
