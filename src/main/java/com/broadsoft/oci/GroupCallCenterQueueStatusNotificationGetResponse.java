//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupCallCenterQueueStatusNotificationGetRequest.
 *         The response contains the call center status configuration information.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterQueueStatusNotificationGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterQueueStatusNotificationGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableQueueStatusNotification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableQueueDepthThreshold" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableWaitingTimeThreshold" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="numberOfCallsThreshold" type="{}CallCenterQueueDepthNotificationThreshold"/>
 *         <element name="waitingTimeOfCallsThreshold" type="{}CallCenterWaitingTimeNotificationThresholdSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterQueueStatusNotificationGetResponse", propOrder = {
    "enableQueueStatusNotification",
    "enableQueueDepthThreshold",
    "enableWaitingTimeThreshold",
    "numberOfCallsThreshold",
    "waitingTimeOfCallsThreshold"
})
public class GroupCallCenterQueueStatusNotificationGetResponse
    extends OCIDataResponse
{

    protected boolean enableQueueStatusNotification;
    protected boolean enableQueueDepthThreshold;
    protected boolean enableWaitingTimeThreshold;
    protected int numberOfCallsThreshold;
    protected int waitingTimeOfCallsThreshold;

    /**
     * Ruft den Wert der enableQueueStatusNotification-Eigenschaft ab.
     * 
     */
    public boolean isEnableQueueStatusNotification() {
        return enableQueueStatusNotification;
    }

    /**
     * Legt den Wert der enableQueueStatusNotification-Eigenschaft fest.
     * 
     */
    public void setEnableQueueStatusNotification(boolean value) {
        this.enableQueueStatusNotification = value;
    }

    /**
     * Ruft den Wert der enableQueueDepthThreshold-Eigenschaft ab.
     * 
     */
    public boolean isEnableQueueDepthThreshold() {
        return enableQueueDepthThreshold;
    }

    /**
     * Legt den Wert der enableQueueDepthThreshold-Eigenschaft fest.
     * 
     */
    public void setEnableQueueDepthThreshold(boolean value) {
        this.enableQueueDepthThreshold = value;
    }

    /**
     * Ruft den Wert der enableWaitingTimeThreshold-Eigenschaft ab.
     * 
     */
    public boolean isEnableWaitingTimeThreshold() {
        return enableWaitingTimeThreshold;
    }

    /**
     * Legt den Wert der enableWaitingTimeThreshold-Eigenschaft fest.
     * 
     */
    public void setEnableWaitingTimeThreshold(boolean value) {
        this.enableWaitingTimeThreshold = value;
    }

    /**
     * Ruft den Wert der numberOfCallsThreshold-Eigenschaft ab.
     * 
     */
    public int getNumberOfCallsThreshold() {
        return numberOfCallsThreshold;
    }

    /**
     * Legt den Wert der numberOfCallsThreshold-Eigenschaft fest.
     * 
     */
    public void setNumberOfCallsThreshold(int value) {
        this.numberOfCallsThreshold = value;
    }

    /**
     * Ruft den Wert der waitingTimeOfCallsThreshold-Eigenschaft ab.
     * 
     */
    public int getWaitingTimeOfCallsThreshold() {
        return waitingTimeOfCallsThreshold;
    }

    /**
     * Legt den Wert der waitingTimeOfCallsThreshold-Eigenschaft fest.
     * 
     */
    public void setWaitingTimeOfCallsThreshold(int value) {
        this.waitingTimeOfCallsThreshold = value;
    }

}
