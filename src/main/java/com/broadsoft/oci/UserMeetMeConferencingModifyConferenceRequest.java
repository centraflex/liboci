//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify an existing conference.
 *         The response is either SuccessResponse or ErrorResponse.
 *         The startTime element is adjusted to the first occurrence of the recurrent schedule that comes at or after the startTime.
 *         The startTime, endTime and recurrence information for a conferenceSchedule element will be adjusted to the user Host time zone.
 *       
 * 
 * <p>Java-Klasse für UserMeetMeConferencingModifyConferenceRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserMeetMeConferencingModifyConferenceRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="conferenceKey" type="{}MeetMeConferencingConferenceKey"/>
 *         <element name="title" type="{}MeetMeConferencingConferenceTitle" minOccurs="0"/>
 *         <element name="estimatedParticipants" type="{}MeetMeConferencingNumberOfParticipants" minOccurs="0"/>
 *         <choice minOccurs="0">
 *           <element name="restrictParticipants" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *           <element name="maxParticipants" type="{}MeetMeConferencingNumberOfParticipants"/>
 *         </choice>
 *         <element name="accountCode" type="{}MeetMeConferencingConferenceAccountCode" minOccurs="0"/>
 *         <element name="muteAllAttendeesOnEntry" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="endConferenceOnModeratorExit" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="moderatorRequired" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="requireSecurityPin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="allowUniqueIdentifier" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="attendeeNotification" type="{}MeetMeConferencingConferenceAttendeeNotification" minOccurs="0"/>
 *         <element name="conferenceSchedule" type="{}MeetMeConferencingConferenceSchedule" minOccurs="0"/>
 *         <element name="allowParticipantUnmuteInAutoLectureMode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserMeetMeConferencingModifyConferenceRequest", propOrder = {
    "userId",
    "conferenceKey",
    "title",
    "estimatedParticipants",
    "restrictParticipants",
    "maxParticipants",
    "accountCode",
    "muteAllAttendeesOnEntry",
    "endConferenceOnModeratorExit",
    "moderatorRequired",
    "requireSecurityPin",
    "allowUniqueIdentifier",
    "attendeeNotification",
    "conferenceSchedule",
    "allowParticipantUnmuteInAutoLectureMode"
})
public class UserMeetMeConferencingModifyConferenceRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    protected MeetMeConferencingConferenceKey conferenceKey;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String title;
    @XmlElementRef(name = "estimatedParticipants", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> estimatedParticipants;
    protected Boolean restrictParticipants;
    protected Integer maxParticipants;
    @XmlElementRef(name = "accountCode", type = JAXBElement.class, required = false)
    protected JAXBElement<String> accountCode;
    protected Boolean muteAllAttendeesOnEntry;
    protected Boolean endConferenceOnModeratorExit;
    protected Boolean moderatorRequired;
    protected Boolean requireSecurityPin;
    protected Boolean allowUniqueIdentifier;
    @XmlSchemaType(name = "token")
    protected MeetMeConferencingConferenceAttendeeNotification attendeeNotification;
    protected MeetMeConferencingConferenceSchedule conferenceSchedule;
    protected Boolean allowParticipantUnmuteInAutoLectureMode;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der conferenceKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferenceKey }
     *     
     */
    public MeetMeConferencingConferenceKey getConferenceKey() {
        return conferenceKey;
    }

    /**
     * Legt den Wert der conferenceKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferenceKey }
     *     
     */
    public void setConferenceKey(MeetMeConferencingConferenceKey value) {
        this.conferenceKey = value;
    }

    /**
     * Ruft den Wert der title-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Legt den Wert der title-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Ruft den Wert der estimatedParticipants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getEstimatedParticipants() {
        return estimatedParticipants;
    }

    /**
     * Legt den Wert der estimatedParticipants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setEstimatedParticipants(JAXBElement<Integer> value) {
        this.estimatedParticipants = value;
    }

    /**
     * Ruft den Wert der restrictParticipants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictParticipants() {
        return restrictParticipants;
    }

    /**
     * Legt den Wert der restrictParticipants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictParticipants(Boolean value) {
        this.restrictParticipants = value;
    }

    /**
     * Ruft den Wert der maxParticipants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxParticipants() {
        return maxParticipants;
    }

    /**
     * Legt den Wert der maxParticipants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxParticipants(Integer value) {
        this.maxParticipants = value;
    }

    /**
     * Ruft den Wert der accountCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAccountCode() {
        return accountCode;
    }

    /**
     * Legt den Wert der accountCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAccountCode(JAXBElement<String> value) {
        this.accountCode = value;
    }

    /**
     * Ruft den Wert der muteAllAttendeesOnEntry-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMuteAllAttendeesOnEntry() {
        return muteAllAttendeesOnEntry;
    }

    /**
     * Legt den Wert der muteAllAttendeesOnEntry-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMuteAllAttendeesOnEntry(Boolean value) {
        this.muteAllAttendeesOnEntry = value;
    }

    /**
     * Ruft den Wert der endConferenceOnModeratorExit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEndConferenceOnModeratorExit() {
        return endConferenceOnModeratorExit;
    }

    /**
     * Legt den Wert der endConferenceOnModeratorExit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEndConferenceOnModeratorExit(Boolean value) {
        this.endConferenceOnModeratorExit = value;
    }

    /**
     * Ruft den Wert der moderatorRequired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isModeratorRequired() {
        return moderatorRequired;
    }

    /**
     * Legt den Wert der moderatorRequired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setModeratorRequired(Boolean value) {
        this.moderatorRequired = value;
    }

    /**
     * Ruft den Wert der requireSecurityPin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequireSecurityPin() {
        return requireSecurityPin;
    }

    /**
     * Legt den Wert der requireSecurityPin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequireSecurityPin(Boolean value) {
        this.requireSecurityPin = value;
    }

    /**
     * Ruft den Wert der allowUniqueIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowUniqueIdentifier() {
        return allowUniqueIdentifier;
    }

    /**
     * Legt den Wert der allowUniqueIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowUniqueIdentifier(Boolean value) {
        this.allowUniqueIdentifier = value;
    }

    /**
     * Ruft den Wert der attendeeNotification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferenceAttendeeNotification }
     *     
     */
    public MeetMeConferencingConferenceAttendeeNotification getAttendeeNotification() {
        return attendeeNotification;
    }

    /**
     * Legt den Wert der attendeeNotification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferenceAttendeeNotification }
     *     
     */
    public void setAttendeeNotification(MeetMeConferencingConferenceAttendeeNotification value) {
        this.attendeeNotification = value;
    }

    /**
     * Ruft den Wert der conferenceSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferenceSchedule }
     *     
     */
    public MeetMeConferencingConferenceSchedule getConferenceSchedule() {
        return conferenceSchedule;
    }

    /**
     * Legt den Wert der conferenceSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferenceSchedule }
     *     
     */
    public void setConferenceSchedule(MeetMeConferencingConferenceSchedule value) {
        this.conferenceSchedule = value;
    }

    /**
     * Ruft den Wert der allowParticipantUnmuteInAutoLectureMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowParticipantUnmuteInAutoLectureMode() {
        return allowParticipantUnmuteInAutoLectureMode;
    }

    /**
     * Legt den Wert der allowParticipantUnmuteInAutoLectureMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowParticipantUnmuteInAutoLectureMode(Boolean value) {
        this.allowParticipantUnmuteInAutoLectureMode = value;
    }

}
