//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupTrunkGroupGetRequest14sp1.
 *         The response contains the maximum and bursting maximum permissible active trunk group calls for the group.
 *       
 * 
 * <p>Java-Klasse für GroupTrunkGroupGetResponse14sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupTrunkGroupGetResponse14sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maxActiveCalls" type="{}UnboundedNonNegativeInt"/>
 *         <element name="maxAvailableActiveCalls" type="{}UnboundedNonNegativeInt"/>
 *         <element name="burstingMaxActiveCalls" type="{}UnboundedNonNegativeInt"/>
 *         <element name="burstingMaxAvailableActiveCalls" type="{}UnboundedNonNegativeInt"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupTrunkGroupGetResponse14sp1", propOrder = {
    "maxActiveCalls",
    "maxAvailableActiveCalls",
    "burstingMaxActiveCalls",
    "burstingMaxAvailableActiveCalls"
})
public class GroupTrunkGroupGetResponse14Sp1
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected UnboundedNonNegativeInt maxActiveCalls;
    @XmlElement(required = true)
    protected UnboundedNonNegativeInt maxAvailableActiveCalls;
    @XmlElement(required = true)
    protected UnboundedNonNegativeInt burstingMaxActiveCalls;
    @XmlElement(required = true)
    protected UnboundedNonNegativeInt burstingMaxAvailableActiveCalls;

    /**
     * Ruft den Wert der maxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public UnboundedNonNegativeInt getMaxActiveCalls() {
        return maxActiveCalls;
    }

    /**
     * Legt den Wert der maxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public void setMaxActiveCalls(UnboundedNonNegativeInt value) {
        this.maxActiveCalls = value;
    }

    /**
     * Ruft den Wert der maxAvailableActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public UnboundedNonNegativeInt getMaxAvailableActiveCalls() {
        return maxAvailableActiveCalls;
    }

    /**
     * Legt den Wert der maxAvailableActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public void setMaxAvailableActiveCalls(UnboundedNonNegativeInt value) {
        this.maxAvailableActiveCalls = value;
    }

    /**
     * Ruft den Wert der burstingMaxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public UnboundedNonNegativeInt getBurstingMaxActiveCalls() {
        return burstingMaxActiveCalls;
    }

    /**
     * Legt den Wert der burstingMaxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public void setBurstingMaxActiveCalls(UnboundedNonNegativeInt value) {
        this.burstingMaxActiveCalls = value;
    }

    /**
     * Ruft den Wert der burstingMaxAvailableActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public UnboundedNonNegativeInt getBurstingMaxAvailableActiveCalls() {
        return burstingMaxAvailableActiveCalls;
    }

    /**
     * Legt den Wert der burstingMaxAvailableActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public void setBurstingMaxAvailableActiveCalls(UnboundedNonNegativeInt value) {
        this.burstingMaxAvailableActiveCalls = value;
    }

}
