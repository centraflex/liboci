//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify a system schedule.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemScheduleModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemScheduleModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="scheduleKey" type="{}ScheduleKey"/>
 *         <element name="newScheduleName" type="{}ScheduleName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemScheduleModifyRequest", propOrder = {
    "scheduleKey",
    "newScheduleName"
})
public class SystemScheduleModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    protected ScheduleKey scheduleKey;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newScheduleName;

    /**
     * Ruft den Wert der scheduleKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ScheduleKey }
     *     
     */
    public ScheduleKey getScheduleKey() {
        return scheduleKey;
    }

    /**
     * Legt den Wert der scheduleKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ScheduleKey }
     *     
     */
    public void setScheduleKey(ScheduleKey value) {
        this.scheduleKey = value;
    }

    /**
     * Ruft den Wert der newScheduleName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewScheduleName() {
        return newScheduleName;
    }

    /**
     * Legt den Wert der newScheduleName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewScheduleName(String value) {
        this.newScheduleName = value;
    }

}
