//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify File System parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *             protocolFile-secure        
 *       
 * 
 * <p>Java-Klasse für SystemConfigurableFileSystemModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemConfigurableFileSystemModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="mediaDirectory" type="{}ConfigurableFileSystemDirectory" minOccurs="0"/>
 *         <choice minOccurs="0">
 *           <element name="protocolFile">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="replicated" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                     <element name="secure" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *           <element name="protocolWebDAV">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="secure" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                     <element name="userName" type="{}WebDAVUserName" minOccurs="0"/>
 *                     <element name="password" type="{}WebDAVPassword" minOccurs="0"/>
 *                     <element name="fileServerFQDN" type="{}NetAddress" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *         </choice>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemConfigurableFileSystemModifyRequest", propOrder = {
    "mediaDirectory",
    "protocolFile",
    "protocolWebDAV"
})
public class SystemConfigurableFileSystemModifyRequest
    extends OCIRequest
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mediaDirectory;
    protected SystemConfigurableFileSystemModifyRequest.ProtocolFile protocolFile;
    protected SystemConfigurableFileSystemModifyRequest.ProtocolWebDAV protocolWebDAV;

    /**
     * Ruft den Wert der mediaDirectory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMediaDirectory() {
        return mediaDirectory;
    }

    /**
     * Legt den Wert der mediaDirectory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMediaDirectory(String value) {
        this.mediaDirectory = value;
    }

    /**
     * Ruft den Wert der protocolFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemConfigurableFileSystemModifyRequest.ProtocolFile }
     *     
     */
    public SystemConfigurableFileSystemModifyRequest.ProtocolFile getProtocolFile() {
        return protocolFile;
    }

    /**
     * Legt den Wert der protocolFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemConfigurableFileSystemModifyRequest.ProtocolFile }
     *     
     */
    public void setProtocolFile(SystemConfigurableFileSystemModifyRequest.ProtocolFile value) {
        this.protocolFile = value;
    }

    /**
     * Ruft den Wert der protocolWebDAV-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemConfigurableFileSystemModifyRequest.ProtocolWebDAV }
     *     
     */
    public SystemConfigurableFileSystemModifyRequest.ProtocolWebDAV getProtocolWebDAV() {
        return protocolWebDAV;
    }

    /**
     * Legt den Wert der protocolWebDAV-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemConfigurableFileSystemModifyRequest.ProtocolWebDAV }
     *     
     */
    public void setProtocolWebDAV(SystemConfigurableFileSystemModifyRequest.ProtocolWebDAV value) {
        this.protocolWebDAV = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="replicated" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         <element name="secure" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "replicated",
        "secure"
    })
    public static class ProtocolFile {

        protected Boolean replicated;
        protected Boolean secure;

        /**
         * Ruft den Wert der replicated-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isReplicated() {
            return replicated;
        }

        /**
         * Legt den Wert der replicated-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setReplicated(Boolean value) {
            this.replicated = value;
        }

        /**
         * Ruft den Wert der secure-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSecure() {
            return secure;
        }

        /**
         * Legt den Wert der secure-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSecure(Boolean value) {
            this.secure = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="secure" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         <element name="userName" type="{}WebDAVUserName" minOccurs="0"/>
     *         <element name="password" type="{}WebDAVPassword" minOccurs="0"/>
     *         <element name="fileServerFQDN" type="{}NetAddress" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "secure",
        "userName",
        "password",
        "fileServerFQDN"
    })
    public static class ProtocolWebDAV {

        protected Boolean secure;
        @XmlElementRef(name = "userName", type = JAXBElement.class, required = false)
        protected JAXBElement<String> userName;
        @XmlElementRef(name = "password", type = JAXBElement.class, required = false)
        protected JAXBElement<String> password;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String fileServerFQDN;

        /**
         * Ruft den Wert der secure-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSecure() {
            return secure;
        }

        /**
         * Legt den Wert der secure-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSecure(Boolean value) {
            this.secure = value;
        }

        /**
         * Ruft den Wert der userName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getUserName() {
            return userName;
        }

        /**
         * Legt den Wert der userName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setUserName(JAXBElement<String> value) {
            this.userName = value;
        }

        /**
         * Ruft den Wert der password-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public JAXBElement<String> getPassword() {
            return password;
        }

        /**
         * Legt den Wert der password-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link JAXBElement }{@code <}{@link String }{@code >}
         *     
         */
        public void setPassword(JAXBElement<String> value) {
            this.password = value;
        }

        /**
         * Ruft den Wert der fileServerFQDN-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFileServerFQDN() {
            return fileServerFQDN;
        }

        /**
         * Legt den Wert der fileServerFQDN-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFileServerFQDN(String value) {
            this.fileServerFQDN = value;
        }

    }

}
