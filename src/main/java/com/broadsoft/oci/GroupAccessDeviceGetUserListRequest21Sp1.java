//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Requests the configuration of a specified group access device.
 *         The response is either GroupAccessDeviceGetUserListResponse21sp1 or ErrorResponse.
 *         The following elements are only used in XS data mode and ignored in AS data mode:
 *           searchCriteriaAccessDeviceEndpointPrivateIdentity
 *       
 * 
 * <p>Java-Klasse für GroupAccessDeviceGetUserListRequest21sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAccessDeviceGetUserListRequest21sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="deviceName" type="{}AccessDeviceName"/>
 *         <element name="responseSizeLimit" type="{}ResponseSizeLimit" minOccurs="0"/>
 *         <element name="searchCriteriaLinePortUserPart" type="{}SearchCriteriaLinePortUserPart" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaLinePortDomain" type="{}SearchCriteriaLinePortDomain" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaUserLastName" type="{}SearchCriteriaUserLastName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaUserFirstName" type="{}SearchCriteriaUserFirstName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaDn" type="{}SearchCriteriaDn" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaUserId" type="{}SearchCriteriaUserId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactEndpointType" type="{}SearchCriteriaExactEndpointType21sp1" minOccurs="0"/>
 *         <element name="searchCriteriaExactUserType" type="{}SearchCriteriaExactUserType" minOccurs="0"/>
 *         <element name="searchCriteriaExtension" type="{}SearchCriteriaExtension" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaExactUserDepartment" type="{}SearchCriteriaExactUserDepartment" minOccurs="0"/>
 *         <element name="searchCriteriaExactPortNumber" type="{}SearchCriteriaExactPortNumber" minOccurs="0"/>
 *         <element name="searchCriteriaAccessDeviceEndpointPrivateIdentity" type="{}SearchCriteriaAccessDeviceEndpointPrivateIdentity" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="searchCriteriaUserHotlineContact" type="{}SearchCriteriaUserHotlineContact" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAccessDeviceGetUserListRequest21sp1", propOrder = {
    "serviceProviderId",
    "groupId",
    "deviceName",
    "responseSizeLimit",
    "searchCriteriaLinePortUserPart",
    "searchCriteriaLinePortDomain",
    "searchCriteriaUserLastName",
    "searchCriteriaUserFirstName",
    "searchCriteriaDn",
    "searchCriteriaUserId",
    "searchCriteriaExactEndpointType",
    "searchCriteriaExactUserType",
    "searchCriteriaExtension",
    "searchCriteriaExactUserDepartment",
    "searchCriteriaExactPortNumber",
    "searchCriteriaAccessDeviceEndpointPrivateIdentity",
    "searchCriteriaUserHotlineContact"
})
public class GroupAccessDeviceGetUserListRequest21Sp1
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceName;
    protected Integer responseSizeLimit;
    protected List<SearchCriteriaLinePortUserPart> searchCriteriaLinePortUserPart;
    protected List<SearchCriteriaLinePortDomain> searchCriteriaLinePortDomain;
    protected List<SearchCriteriaUserLastName> searchCriteriaUserLastName;
    protected List<SearchCriteriaUserFirstName> searchCriteriaUserFirstName;
    protected List<SearchCriteriaDn> searchCriteriaDn;
    protected List<SearchCriteriaUserId> searchCriteriaUserId;
    protected SearchCriteriaExactEndpointType21Sp1 searchCriteriaExactEndpointType;
    protected SearchCriteriaExactUserType searchCriteriaExactUserType;
    protected List<SearchCriteriaExtension> searchCriteriaExtension;
    protected SearchCriteriaExactUserDepartment searchCriteriaExactUserDepartment;
    protected SearchCriteriaExactPortNumber searchCriteriaExactPortNumber;
    protected List<SearchCriteriaAccessDeviceEndpointPrivateIdentity> searchCriteriaAccessDeviceEndpointPrivateIdentity;
    protected List<SearchCriteriaUserHotlineContact> searchCriteriaUserHotlineContact;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der deviceName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * Legt den Wert der deviceName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceName(String value) {
        this.deviceName = value;
    }

    /**
     * Ruft den Wert der responseSizeLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseSizeLimit() {
        return responseSizeLimit;
    }

    /**
     * Legt den Wert der responseSizeLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseSizeLimit(Integer value) {
        this.responseSizeLimit = value;
    }

    /**
     * Gets the value of the searchCriteriaLinePortUserPart property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaLinePortUserPart property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaLinePortUserPart().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaLinePortUserPart }
     * 
     * 
     * @return
     *     The value of the searchCriteriaLinePortUserPart property.
     */
    public List<SearchCriteriaLinePortUserPart> getSearchCriteriaLinePortUserPart() {
        if (searchCriteriaLinePortUserPart == null) {
            searchCriteriaLinePortUserPart = new ArrayList<>();
        }
        return this.searchCriteriaLinePortUserPart;
    }

    /**
     * Gets the value of the searchCriteriaLinePortDomain property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaLinePortDomain property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaLinePortDomain().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaLinePortDomain }
     * 
     * 
     * @return
     *     The value of the searchCriteriaLinePortDomain property.
     */
    public List<SearchCriteriaLinePortDomain> getSearchCriteriaLinePortDomain() {
        if (searchCriteriaLinePortDomain == null) {
            searchCriteriaLinePortDomain = new ArrayList<>();
        }
        return this.searchCriteriaLinePortDomain;
    }

    /**
     * Gets the value of the searchCriteriaUserLastName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserLastName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserLastName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserLastName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserLastName property.
     */
    public List<SearchCriteriaUserLastName> getSearchCriteriaUserLastName() {
        if (searchCriteriaUserLastName == null) {
            searchCriteriaUserLastName = new ArrayList<>();
        }
        return this.searchCriteriaUserLastName;
    }

    /**
     * Gets the value of the searchCriteriaUserFirstName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserFirstName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserFirstName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserFirstName }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserFirstName property.
     */
    public List<SearchCriteriaUserFirstName> getSearchCriteriaUserFirstName() {
        if (searchCriteriaUserFirstName == null) {
            searchCriteriaUserFirstName = new ArrayList<>();
        }
        return this.searchCriteriaUserFirstName;
    }

    /**
     * Gets the value of the searchCriteriaDn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaDn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaDn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaDn }
     * 
     * 
     * @return
     *     The value of the searchCriteriaDn property.
     */
    public List<SearchCriteriaDn> getSearchCriteriaDn() {
        if (searchCriteriaDn == null) {
            searchCriteriaDn = new ArrayList<>();
        }
        return this.searchCriteriaDn;
    }

    /**
     * Gets the value of the searchCriteriaUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserId }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserId property.
     */
    public List<SearchCriteriaUserId> getSearchCriteriaUserId() {
        if (searchCriteriaUserId == null) {
            searchCriteriaUserId = new ArrayList<>();
        }
        return this.searchCriteriaUserId;
    }

    /**
     * Ruft den Wert der searchCriteriaExactEndpointType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactEndpointType21Sp1 }
     *     
     */
    public SearchCriteriaExactEndpointType21Sp1 getSearchCriteriaExactEndpointType() {
        return searchCriteriaExactEndpointType;
    }

    /**
     * Legt den Wert der searchCriteriaExactEndpointType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactEndpointType21Sp1 }
     *     
     */
    public void setSearchCriteriaExactEndpointType(SearchCriteriaExactEndpointType21Sp1 value) {
        this.searchCriteriaExactEndpointType = value;
    }

    /**
     * Ruft den Wert der searchCriteriaExactUserType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactUserType }
     *     
     */
    public SearchCriteriaExactUserType getSearchCriteriaExactUserType() {
        return searchCriteriaExactUserType;
    }

    /**
     * Legt den Wert der searchCriteriaExactUserType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactUserType }
     *     
     */
    public void setSearchCriteriaExactUserType(SearchCriteriaExactUserType value) {
        this.searchCriteriaExactUserType = value;
    }

    /**
     * Gets the value of the searchCriteriaExtension property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaExtension property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaExtension().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaExtension }
     * 
     * 
     * @return
     *     The value of the searchCriteriaExtension property.
     */
    public List<SearchCriteriaExtension> getSearchCriteriaExtension() {
        if (searchCriteriaExtension == null) {
            searchCriteriaExtension = new ArrayList<>();
        }
        return this.searchCriteriaExtension;
    }

    /**
     * Ruft den Wert der searchCriteriaExactUserDepartment-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactUserDepartment }
     *     
     */
    public SearchCriteriaExactUserDepartment getSearchCriteriaExactUserDepartment() {
        return searchCriteriaExactUserDepartment;
    }

    /**
     * Legt den Wert der searchCriteriaExactUserDepartment-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactUserDepartment }
     *     
     */
    public void setSearchCriteriaExactUserDepartment(SearchCriteriaExactUserDepartment value) {
        this.searchCriteriaExactUserDepartment = value;
    }

    /**
     * Ruft den Wert der searchCriteriaExactPortNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SearchCriteriaExactPortNumber }
     *     
     */
    public SearchCriteriaExactPortNumber getSearchCriteriaExactPortNumber() {
        return searchCriteriaExactPortNumber;
    }

    /**
     * Legt den Wert der searchCriteriaExactPortNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchCriteriaExactPortNumber }
     *     
     */
    public void setSearchCriteriaExactPortNumber(SearchCriteriaExactPortNumber value) {
        this.searchCriteriaExactPortNumber = value;
    }

    /**
     * Gets the value of the searchCriteriaAccessDeviceEndpointPrivateIdentity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaAccessDeviceEndpointPrivateIdentity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaAccessDeviceEndpointPrivateIdentity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaAccessDeviceEndpointPrivateIdentity }
     * 
     * 
     * @return
     *     The value of the searchCriteriaAccessDeviceEndpointPrivateIdentity property.
     */
    public List<SearchCriteriaAccessDeviceEndpointPrivateIdentity> getSearchCriteriaAccessDeviceEndpointPrivateIdentity() {
        if (searchCriteriaAccessDeviceEndpointPrivateIdentity == null) {
            searchCriteriaAccessDeviceEndpointPrivateIdentity = new ArrayList<>();
        }
        return this.searchCriteriaAccessDeviceEndpointPrivateIdentity;
    }

    /**
     * Gets the value of the searchCriteriaUserHotlineContact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the searchCriteriaUserHotlineContact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchCriteriaUserHotlineContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchCriteriaUserHotlineContact }
     * 
     * 
     * @return
     *     The value of the searchCriteriaUserHotlineContact property.
     */
    public List<SearchCriteriaUserHotlineContact> getSearchCriteriaUserHotlineContact() {
        if (searchCriteriaUserHotlineContact == null) {
            searchCriteriaUserHotlineContact = new ArrayList<>();
        }
        return this.searchCriteriaUserHotlineContact;
    }

}
