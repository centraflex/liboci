//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Access device end point that can have multiple contacts.
 *     
 *     Replaced by AccessDeviceMultipleContactEndpointRead19.
 *       
 * 
 * <p>Java-Klasse für AccessDeviceMultipleContactEndpointRead complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="AccessDeviceMultipleContactEndpointRead">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="accessDevice" type="{}AccessDevice"/>
 *         <element name="linePort" type="{}AccessDeviceEndpointLinePort"/>
 *         <element name="contact" type="{}SIPContact" maxOccurs="5" minOccurs="0"/>
 *         <element name="staticRegistrationCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useDomain" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccessDeviceMultipleContactEndpointRead", propOrder = {
    "accessDevice",
    "linePort",
    "contact",
    "staticRegistrationCapable",
    "useDomain"
})
public class AccessDeviceMultipleContactEndpointRead {

    @XmlElement(required = true)
    protected AccessDevice accessDevice;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String linePort;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> contact;
    protected boolean staticRegistrationCapable;
    protected boolean useDomain;

    /**
     * Ruft den Wert der accessDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDevice }
     *     
     */
    public AccessDevice getAccessDevice() {
        return accessDevice;
    }

    /**
     * Legt den Wert der accessDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDevice }
     *     
     */
    public void setAccessDevice(AccessDevice value) {
        this.accessDevice = value;
    }

    /**
     * Ruft den Wert der linePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinePort() {
        return linePort;
    }

    /**
     * Legt den Wert der linePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinePort(String value) {
        this.linePort = value;
    }

    /**
     * Gets the value of the contact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the contact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the contact property.
     */
    public List<String> getContact() {
        if (contact == null) {
            contact = new ArrayList<>();
        }
        return this.contact;
    }

    /**
     * Ruft den Wert der staticRegistrationCapable-Eigenschaft ab.
     * 
     */
    public boolean isStaticRegistrationCapable() {
        return staticRegistrationCapable;
    }

    /**
     * Legt den Wert der staticRegistrationCapable-Eigenschaft fest.
     * 
     */
    public void setStaticRegistrationCapable(boolean value) {
        this.staticRegistrationCapable = value;
    }

    /**
     * Ruft den Wert der useDomain-Eigenschaft ab.
     * 
     */
    public boolean isUseDomain() {
        return useDomain;
    }

    /**
     * Legt den Wert der useDomain-Eigenschaft fest.
     * 
     */
    public void setUseDomain(boolean value) {
        this.useDomain = value;
    }

}
