//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AutoAttendantKeyAction.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="AutoAttendantKeyAction">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Transfer With Prompt"/>
 *     <enumeration value="Transfer Without Prompt"/>
 *     <enumeration value="Transfer To Operator"/>
 *     <enumeration value="Name Dialing"/>
 *     <enumeration value="Extension Dialing"/>
 *     <enumeration value="Repeat Menu"/>
 *     <enumeration value="Exit"/>
 *     <enumeration value="Transfer To Mailbox"/>
 *     <enumeration value="Transfer To Submenu"/>
 *     <enumeration value="Return to Previous Menu"/>
 *     <enumeration value="Play Announcement"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "AutoAttendantKeyAction")
@XmlEnum
public enum AutoAttendantKeyAction {

    @XmlEnumValue("Transfer With Prompt")
    TRANSFER_WITH_PROMPT("Transfer With Prompt"),
    @XmlEnumValue("Transfer Without Prompt")
    TRANSFER_WITHOUT_PROMPT("Transfer Without Prompt"),
    @XmlEnumValue("Transfer To Operator")
    TRANSFER_TO_OPERATOR("Transfer To Operator"),
    @XmlEnumValue("Name Dialing")
    NAME_DIALING("Name Dialing"),
    @XmlEnumValue("Extension Dialing")
    EXTENSION_DIALING("Extension Dialing"),
    @XmlEnumValue("Repeat Menu")
    REPEAT_MENU("Repeat Menu"),
    @XmlEnumValue("Exit")
    EXIT("Exit"),
    @XmlEnumValue("Transfer To Mailbox")
    TRANSFER_TO_MAILBOX("Transfer To Mailbox"),
    @XmlEnumValue("Transfer To Submenu")
    TRANSFER_TO_SUBMENU("Transfer To Submenu"),
    @XmlEnumValue("Return to Previous Menu")
    RETURN_TO_PREVIOUS_MENU("Return to Previous Menu"),
    @XmlEnumValue("Play Announcement")
    PLAY_ANNOUNCEMENT("Play Announcement");
    private final String value;

    AutoAttendantKeyAction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AutoAttendantKeyAction fromValue(String v) {
        for (AutoAttendantKeyAction c: AutoAttendantKeyAction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
