//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Outgoing Calling Plan originating call permissions.
 *       
 * 
 * <p>Java-Klasse für OutgoingCallingPlanOriginatingPermissions complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OutgoingCallingPlanOriginatingPermissions">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="group" type="{}OutgoingCallingPlanOriginatingPermission"/>
 *         <element name="local" type="{}OutgoingCallingPlanOriginatingPermission"/>
 *         <element name="tollFree" type="{}OutgoingCallingPlanOriginatingPermission"/>
 *         <element name="toll" type="{}OutgoingCallingPlanOriginatingPermission"/>
 *         <element name="international" type="{}OutgoingCallingPlanOriginatingPermission"/>
 *         <element name="operatorAssisted" type="{}OutgoingCallingPlanOriginatingPermission"/>
 *         <element name="chargeableDirectoryAssisted" type="{}OutgoingCallingPlanOriginatingPermission"/>
 *         <element name="specialServicesI" type="{}OutgoingCallingPlanOriginatingPermission"/>
 *         <element name="specialServicesII" type="{}OutgoingCallingPlanOriginatingPermission"/>
 *         <element name="premiumServicesI" type="{}OutgoingCallingPlanOriginatingPermission"/>
 *         <element name="premiumServicesII" type="{}OutgoingCallingPlanOriginatingPermission"/>
 *         <element name="casual" type="{}OutgoingCallingPlanOriginatingPermission"/>
 *         <element name="urlDialing" type="{}OutgoingCallingPlanOriginatingPermission"/>
 *         <element name="unknown" type="{}OutgoingCallingPlanOriginatingPermission"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingCallingPlanOriginatingPermissions", propOrder = {
    "group",
    "local",
    "tollFree",
    "toll",
    "international",
    "operatorAssisted",
    "chargeableDirectoryAssisted",
    "specialServicesI",
    "specialServicesII",
    "premiumServicesI",
    "premiumServicesII",
    "casual",
    "urlDialing",
    "unknown"
})
public class OutgoingCallingPlanOriginatingPermissions {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OutgoingCallingPlanOriginatingPermission group;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OutgoingCallingPlanOriginatingPermission local;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OutgoingCallingPlanOriginatingPermission tollFree;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OutgoingCallingPlanOriginatingPermission toll;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OutgoingCallingPlanOriginatingPermission international;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OutgoingCallingPlanOriginatingPermission operatorAssisted;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OutgoingCallingPlanOriginatingPermission chargeableDirectoryAssisted;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OutgoingCallingPlanOriginatingPermission specialServicesI;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OutgoingCallingPlanOriginatingPermission specialServicesII;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OutgoingCallingPlanOriginatingPermission premiumServicesI;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OutgoingCallingPlanOriginatingPermission premiumServicesII;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OutgoingCallingPlanOriginatingPermission casual;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OutgoingCallingPlanOriginatingPermission urlDialing;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OutgoingCallingPlanOriginatingPermission unknown;

    /**
     * Ruft den Wert der group-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public OutgoingCallingPlanOriginatingPermission getGroup() {
        return group;
    }

    /**
     * Legt den Wert der group-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public void setGroup(OutgoingCallingPlanOriginatingPermission value) {
        this.group = value;
    }

    /**
     * Ruft den Wert der local-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public OutgoingCallingPlanOriginatingPermission getLocal() {
        return local;
    }

    /**
     * Legt den Wert der local-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public void setLocal(OutgoingCallingPlanOriginatingPermission value) {
        this.local = value;
    }

    /**
     * Ruft den Wert der tollFree-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public OutgoingCallingPlanOriginatingPermission getTollFree() {
        return tollFree;
    }

    /**
     * Legt den Wert der tollFree-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public void setTollFree(OutgoingCallingPlanOriginatingPermission value) {
        this.tollFree = value;
    }

    /**
     * Ruft den Wert der toll-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public OutgoingCallingPlanOriginatingPermission getToll() {
        return toll;
    }

    /**
     * Legt den Wert der toll-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public void setToll(OutgoingCallingPlanOriginatingPermission value) {
        this.toll = value;
    }

    /**
     * Ruft den Wert der international-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public OutgoingCallingPlanOriginatingPermission getInternational() {
        return international;
    }

    /**
     * Legt den Wert der international-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public void setInternational(OutgoingCallingPlanOriginatingPermission value) {
        this.international = value;
    }

    /**
     * Ruft den Wert der operatorAssisted-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public OutgoingCallingPlanOriginatingPermission getOperatorAssisted() {
        return operatorAssisted;
    }

    /**
     * Legt den Wert der operatorAssisted-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public void setOperatorAssisted(OutgoingCallingPlanOriginatingPermission value) {
        this.operatorAssisted = value;
    }

    /**
     * Ruft den Wert der chargeableDirectoryAssisted-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public OutgoingCallingPlanOriginatingPermission getChargeableDirectoryAssisted() {
        return chargeableDirectoryAssisted;
    }

    /**
     * Legt den Wert der chargeableDirectoryAssisted-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public void setChargeableDirectoryAssisted(OutgoingCallingPlanOriginatingPermission value) {
        this.chargeableDirectoryAssisted = value;
    }

    /**
     * Ruft den Wert der specialServicesI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public OutgoingCallingPlanOriginatingPermission getSpecialServicesI() {
        return specialServicesI;
    }

    /**
     * Legt den Wert der specialServicesI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public void setSpecialServicesI(OutgoingCallingPlanOriginatingPermission value) {
        this.specialServicesI = value;
    }

    /**
     * Ruft den Wert der specialServicesII-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public OutgoingCallingPlanOriginatingPermission getSpecialServicesII() {
        return specialServicesII;
    }

    /**
     * Legt den Wert der specialServicesII-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public void setSpecialServicesII(OutgoingCallingPlanOriginatingPermission value) {
        this.specialServicesII = value;
    }

    /**
     * Ruft den Wert der premiumServicesI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public OutgoingCallingPlanOriginatingPermission getPremiumServicesI() {
        return premiumServicesI;
    }

    /**
     * Legt den Wert der premiumServicesI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public void setPremiumServicesI(OutgoingCallingPlanOriginatingPermission value) {
        this.premiumServicesI = value;
    }

    /**
     * Ruft den Wert der premiumServicesII-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public OutgoingCallingPlanOriginatingPermission getPremiumServicesII() {
        return premiumServicesII;
    }

    /**
     * Legt den Wert der premiumServicesII-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public void setPremiumServicesII(OutgoingCallingPlanOriginatingPermission value) {
        this.premiumServicesII = value;
    }

    /**
     * Ruft den Wert der casual-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public OutgoingCallingPlanOriginatingPermission getCasual() {
        return casual;
    }

    /**
     * Legt den Wert der casual-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public void setCasual(OutgoingCallingPlanOriginatingPermission value) {
        this.casual = value;
    }

    /**
     * Ruft den Wert der urlDialing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public OutgoingCallingPlanOriginatingPermission getUrlDialing() {
        return urlDialing;
    }

    /**
     * Legt den Wert der urlDialing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public void setUrlDialing(OutgoingCallingPlanOriginatingPermission value) {
        this.urlDialing = value;
    }

    /**
     * Ruft den Wert der unknown-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public OutgoingCallingPlanOriginatingPermission getUnknown() {
        return unknown;
    }

    /**
     * Legt den Wert der unknown-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OutgoingCallingPlanOriginatingPermission }
     *     
     */
    public void setUnknown(OutgoingCallingPlanOriginatingPermission value) {
        this.unknown = value;
    }

}
