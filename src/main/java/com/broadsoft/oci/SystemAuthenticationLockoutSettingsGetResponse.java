//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *        Response to SystemAuthenticationLockoutSettingsGetRequest.
 *        Contains the authentication lockout settings in the system.
 *      
 * 
 * <p>Java-Klasse für SystemAuthenticationLockoutSettingsGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAuthenticationLockoutSettingsGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="counterResetIntervalDays" type="{}CounterResetIntervalDays"/>
 *         <element name="counterResetHour" type="{}CounterResetHour"/>
 *         <element name="counterResetMinute" type="{}CounterResetMinute"/>
 *         <element name="emergencySIPBypassAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAuthenticationLockoutSettingsGetResponse", propOrder = {
    "counterResetIntervalDays",
    "counterResetHour",
    "counterResetMinute",
    "emergencySIPBypassAllowed"
})
public class SystemAuthenticationLockoutSettingsGetResponse
    extends OCIDataResponse
{

    protected int counterResetIntervalDays;
    protected int counterResetHour;
    protected int counterResetMinute;
    protected boolean emergencySIPBypassAllowed;

    /**
     * Ruft den Wert der counterResetIntervalDays-Eigenschaft ab.
     * 
     */
    public int getCounterResetIntervalDays() {
        return counterResetIntervalDays;
    }

    /**
     * Legt den Wert der counterResetIntervalDays-Eigenschaft fest.
     * 
     */
    public void setCounterResetIntervalDays(int value) {
        this.counterResetIntervalDays = value;
    }

    /**
     * Ruft den Wert der counterResetHour-Eigenschaft ab.
     * 
     */
    public int getCounterResetHour() {
        return counterResetHour;
    }

    /**
     * Legt den Wert der counterResetHour-Eigenschaft fest.
     * 
     */
    public void setCounterResetHour(int value) {
        this.counterResetHour = value;
    }

    /**
     * Ruft den Wert der counterResetMinute-Eigenschaft ab.
     * 
     */
    public int getCounterResetMinute() {
        return counterResetMinute;
    }

    /**
     * Legt den Wert der counterResetMinute-Eigenschaft fest.
     * 
     */
    public void setCounterResetMinute(int value) {
        this.counterResetMinute = value;
    }

    /**
     * Ruft den Wert der emergencySIPBypassAllowed-Eigenschaft ab.
     * 
     */
    public boolean isEmergencySIPBypassAllowed() {
        return emergencySIPBypassAllowed;
    }

    /**
     * Legt den Wert der emergencySIPBypassAllowed-Eigenschaft fest.
     * 
     */
    public void setEmergencySIPBypassAllowed(boolean value) {
        this.emergencySIPBypassAllowed = value;
    }

}
