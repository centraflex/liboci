//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a Call Center instance to a group.
 *         The domain is required in the serviceUserId.
 *         The response is either SuccessResponse or ErrorResponse.
 *         
 *         Replaced By: GroupCallCenterAddInstanceRequest16, 
 *                      GroupCallCenterAddAgentListRequest, 
 *                      GroupCallCenterModifyPolicyRequest,
 *                      GroupCallCenterModifyAnnouncementRequest16
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterAddInstanceRequest14sp9 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterAddInstanceRequest14sp9">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceAddProfileCallCenter"/>
 *         <element name="policy" type="{}HuntPolicy"/>
 *         <element name="huntAfterNoAnswer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="noAnswerNumberOfRings" type="{}HuntNoAnswerRings"/>
 *         <element name="forwardAfterTimeout" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forwardTimeoutSeconds" type="{}HuntForwardTimeoutSeconds"/>
 *         <element name="forwardToPhoneNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="enableVideo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="queueLength" type="{}CallCenterQueueLength"/>
 *         <element name="allowAgentLogoff" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="playMusicOnHold" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="playComfortMessage" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="timeBetweenComfortMessagesSeconds" type="{}CallCenterTimeBetweenComfortMessagesSeconds"/>
 *         <element name="enableGuardTimer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="guardTimerSeconds" type="{}CallCenterGuardTimerSeconds"/>
 *         <element name="agentUserId" type="{}UserId" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="allowCallWaitingForAgents" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowCallsToAgentsInWrapUp" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableCallQueueWhenNoAgentsAvailable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="statisticsSource" type="{}CallCenterStatisticsSource"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterAddInstanceRequest14sp9", propOrder = {
    "serviceProviderId",
    "groupId",
    "serviceUserId",
    "serviceInstanceProfile",
    "policy",
    "huntAfterNoAnswer",
    "noAnswerNumberOfRings",
    "forwardAfterTimeout",
    "forwardTimeoutSeconds",
    "forwardToPhoneNumber",
    "enableVideo",
    "queueLength",
    "allowAgentLogoff",
    "playMusicOnHold",
    "playComfortMessage",
    "timeBetweenComfortMessagesSeconds",
    "enableGuardTimer",
    "guardTimerSeconds",
    "agentUserId",
    "allowCallWaitingForAgents",
    "allowCallsToAgentsInWrapUp",
    "enableCallQueueWhenNoAgentsAvailable",
    "statisticsSource"
})
public class GroupCallCenterAddInstanceRequest14Sp9
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    @XmlElement(required = true)
    protected ServiceInstanceAddProfileCallCenter serviceInstanceProfile;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected HuntPolicy policy;
    protected boolean huntAfterNoAnswer;
    protected int noAnswerNumberOfRings;
    protected boolean forwardAfterTimeout;
    protected int forwardTimeoutSeconds;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String forwardToPhoneNumber;
    protected boolean enableVideo;
    protected int queueLength;
    protected boolean allowAgentLogoff;
    protected boolean playMusicOnHold;
    protected boolean playComfortMessage;
    protected int timeBetweenComfortMessagesSeconds;
    protected boolean enableGuardTimer;
    protected int guardTimerSeconds;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> agentUserId;
    protected boolean allowCallWaitingForAgents;
    protected boolean allowCallsToAgentsInWrapUp;
    protected boolean enableCallQueueWhenNoAgentsAvailable;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterStatisticsSource statisticsSource;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceAddProfileCallCenter }
     *     
     */
    public ServiceInstanceAddProfileCallCenter getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceAddProfileCallCenter }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceAddProfileCallCenter value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der policy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HuntPolicy }
     *     
     */
    public HuntPolicy getPolicy() {
        return policy;
    }

    /**
     * Legt den Wert der policy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HuntPolicy }
     *     
     */
    public void setPolicy(HuntPolicy value) {
        this.policy = value;
    }

    /**
     * Ruft den Wert der huntAfterNoAnswer-Eigenschaft ab.
     * 
     */
    public boolean isHuntAfterNoAnswer() {
        return huntAfterNoAnswer;
    }

    /**
     * Legt den Wert der huntAfterNoAnswer-Eigenschaft fest.
     * 
     */
    public void setHuntAfterNoAnswer(boolean value) {
        this.huntAfterNoAnswer = value;
    }

    /**
     * Ruft den Wert der noAnswerNumberOfRings-Eigenschaft ab.
     * 
     */
    public int getNoAnswerNumberOfRings() {
        return noAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der noAnswerNumberOfRings-Eigenschaft fest.
     * 
     */
    public void setNoAnswerNumberOfRings(int value) {
        this.noAnswerNumberOfRings = value;
    }

    /**
     * Ruft den Wert der forwardAfterTimeout-Eigenschaft ab.
     * 
     */
    public boolean isForwardAfterTimeout() {
        return forwardAfterTimeout;
    }

    /**
     * Legt den Wert der forwardAfterTimeout-Eigenschaft fest.
     * 
     */
    public void setForwardAfterTimeout(boolean value) {
        this.forwardAfterTimeout = value;
    }

    /**
     * Ruft den Wert der forwardTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getForwardTimeoutSeconds() {
        return forwardTimeoutSeconds;
    }

    /**
     * Legt den Wert der forwardTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setForwardTimeoutSeconds(int value) {
        this.forwardTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der forwardToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForwardToPhoneNumber() {
        return forwardToPhoneNumber;
    }

    /**
     * Legt den Wert der forwardToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForwardToPhoneNumber(String value) {
        this.forwardToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der enableVideo-Eigenschaft ab.
     * 
     */
    public boolean isEnableVideo() {
        return enableVideo;
    }

    /**
     * Legt den Wert der enableVideo-Eigenschaft fest.
     * 
     */
    public void setEnableVideo(boolean value) {
        this.enableVideo = value;
    }

    /**
     * Ruft den Wert der queueLength-Eigenschaft ab.
     * 
     */
    public int getQueueLength() {
        return queueLength;
    }

    /**
     * Legt den Wert der queueLength-Eigenschaft fest.
     * 
     */
    public void setQueueLength(int value) {
        this.queueLength = value;
    }

    /**
     * Ruft den Wert der allowAgentLogoff-Eigenschaft ab.
     * 
     */
    public boolean isAllowAgentLogoff() {
        return allowAgentLogoff;
    }

    /**
     * Legt den Wert der allowAgentLogoff-Eigenschaft fest.
     * 
     */
    public void setAllowAgentLogoff(boolean value) {
        this.allowAgentLogoff = value;
    }

    /**
     * Ruft den Wert der playMusicOnHold-Eigenschaft ab.
     * 
     */
    public boolean isPlayMusicOnHold() {
        return playMusicOnHold;
    }

    /**
     * Legt den Wert der playMusicOnHold-Eigenschaft fest.
     * 
     */
    public void setPlayMusicOnHold(boolean value) {
        this.playMusicOnHold = value;
    }

    /**
     * Ruft den Wert der playComfortMessage-Eigenschaft ab.
     * 
     */
    public boolean isPlayComfortMessage() {
        return playComfortMessage;
    }

    /**
     * Legt den Wert der playComfortMessage-Eigenschaft fest.
     * 
     */
    public void setPlayComfortMessage(boolean value) {
        this.playComfortMessage = value;
    }

    /**
     * Ruft den Wert der timeBetweenComfortMessagesSeconds-Eigenschaft ab.
     * 
     */
    public int getTimeBetweenComfortMessagesSeconds() {
        return timeBetweenComfortMessagesSeconds;
    }

    /**
     * Legt den Wert der timeBetweenComfortMessagesSeconds-Eigenschaft fest.
     * 
     */
    public void setTimeBetweenComfortMessagesSeconds(int value) {
        this.timeBetweenComfortMessagesSeconds = value;
    }

    /**
     * Ruft den Wert der enableGuardTimer-Eigenschaft ab.
     * 
     */
    public boolean isEnableGuardTimer() {
        return enableGuardTimer;
    }

    /**
     * Legt den Wert der enableGuardTimer-Eigenschaft fest.
     * 
     */
    public void setEnableGuardTimer(boolean value) {
        this.enableGuardTimer = value;
    }

    /**
     * Ruft den Wert der guardTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getGuardTimerSeconds() {
        return guardTimerSeconds;
    }

    /**
     * Legt den Wert der guardTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setGuardTimerSeconds(int value) {
        this.guardTimerSeconds = value;
    }

    /**
     * Gets the value of the agentUserId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the agentUserId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgentUserId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the agentUserId property.
     */
    public List<String> getAgentUserId() {
        if (agentUserId == null) {
            agentUserId = new ArrayList<>();
        }
        return this.agentUserId;
    }

    /**
     * Ruft den Wert der allowCallWaitingForAgents-Eigenschaft ab.
     * 
     */
    public boolean isAllowCallWaitingForAgents() {
        return allowCallWaitingForAgents;
    }

    /**
     * Legt den Wert der allowCallWaitingForAgents-Eigenschaft fest.
     * 
     */
    public void setAllowCallWaitingForAgents(boolean value) {
        this.allowCallWaitingForAgents = value;
    }

    /**
     * Ruft den Wert der allowCallsToAgentsInWrapUp-Eigenschaft ab.
     * 
     */
    public boolean isAllowCallsToAgentsInWrapUp() {
        return allowCallsToAgentsInWrapUp;
    }

    /**
     * Legt den Wert der allowCallsToAgentsInWrapUp-Eigenschaft fest.
     * 
     */
    public void setAllowCallsToAgentsInWrapUp(boolean value) {
        this.allowCallsToAgentsInWrapUp = value;
    }

    /**
     * Ruft den Wert der enableCallQueueWhenNoAgentsAvailable-Eigenschaft ab.
     * 
     */
    public boolean isEnableCallQueueWhenNoAgentsAvailable() {
        return enableCallQueueWhenNoAgentsAvailable;
    }

    /**
     * Legt den Wert der enableCallQueueWhenNoAgentsAvailable-Eigenschaft fest.
     * 
     */
    public void setEnableCallQueueWhenNoAgentsAvailable(boolean value) {
        this.enableCallQueueWhenNoAgentsAvailable = value;
    }

    /**
     * Ruft den Wert der statisticsSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterStatisticsSource }
     *     
     */
    public CallCenterStatisticsSource getStatisticsSource() {
        return statisticsSource;
    }

    /**
     * Legt den Wert der statisticsSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterStatisticsSource }
     *     
     */
    public void setStatisticsSource(CallCenterStatisticsSource value) {
        this.statisticsSource = value;
    }

}
