//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify an existing custom greeting audio file.
 *         The response is either SuccessResponse or
 *         ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserMeetMeConferencingModifyConferenceGreetingRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserMeetMeConferencingModifyConferenceGreetingRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="conferenceKey" type="{}MeetMeConferencingConferenceKey"/>
 *         <element name="playEntranceGreeting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="entranceGreetingFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserMeetMeConferencingModifyConferenceGreetingRequest", propOrder = {
    "userId",
    "conferenceKey",
    "playEntranceGreeting",
    "entranceGreetingFile"
})
public class UserMeetMeConferencingModifyConferenceGreetingRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    protected MeetMeConferencingConferenceKey conferenceKey;
    protected Boolean playEntranceGreeting;
    protected LabeledMediaFileResource entranceGreetingFile;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der conferenceKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferenceKey }
     *     
     */
    public MeetMeConferencingConferenceKey getConferenceKey() {
        return conferenceKey;
    }

    /**
     * Legt den Wert der conferenceKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferenceKey }
     *     
     */
    public void setConferenceKey(MeetMeConferencingConferenceKey value) {
        this.conferenceKey = value;
    }

    /**
     * Ruft den Wert der playEntranceGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayEntranceGreeting() {
        return playEntranceGreeting;
    }

    /**
     * Legt den Wert der playEntranceGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayEntranceGreeting(Boolean value) {
        this.playEntranceGreeting = value;
    }

    /**
     * Ruft den Wert der entranceGreetingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public LabeledMediaFileResource getEntranceGreetingFile() {
        return entranceGreetingFile;
    }

    /**
     * Legt den Wert der entranceGreetingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public void setEntranceGreetingFile(LabeledMediaFileResource value) {
        this.entranceGreetingFile = value;
    }

}
