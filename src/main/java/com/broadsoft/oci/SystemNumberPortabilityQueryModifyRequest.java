//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify system Number Portability Query Parameters.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemNumberPortabilityQueryModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNumberPortabilityQueryModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="continueCallAsDialedOnTimeoutOrError" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="numberPortabilityNameLookupTimeoutMilliseconds" type="{}NumberPortabilityNameLookupTimeoutMilliseconds" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNumberPortabilityQueryModifyRequest", propOrder = {
    "continueCallAsDialedOnTimeoutOrError",
    "numberPortabilityNameLookupTimeoutMilliseconds"
})
public class SystemNumberPortabilityQueryModifyRequest
    extends OCIRequest
{

    protected Boolean continueCallAsDialedOnTimeoutOrError;
    protected Integer numberPortabilityNameLookupTimeoutMilliseconds;

    /**
     * Ruft den Wert der continueCallAsDialedOnTimeoutOrError-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isContinueCallAsDialedOnTimeoutOrError() {
        return continueCallAsDialedOnTimeoutOrError;
    }

    /**
     * Legt den Wert der continueCallAsDialedOnTimeoutOrError-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContinueCallAsDialedOnTimeoutOrError(Boolean value) {
        this.continueCallAsDialedOnTimeoutOrError = value;
    }

    /**
     * Ruft den Wert der numberPortabilityNameLookupTimeoutMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberPortabilityNameLookupTimeoutMilliseconds() {
        return numberPortabilityNameLookupTimeoutMilliseconds;
    }

    /**
     * Legt den Wert der numberPortabilityNameLookupTimeoutMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberPortabilityNameLookupTimeoutMilliseconds(Integer value) {
        this.numberPortabilityNameLookupTimeoutMilliseconds = value;
    }

}
