//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for a particular fully specified DeviceManagement event type.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactDeviceManagementEventType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactDeviceManagementEventType">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="dmEventType" type="{}DeviceManagementEventType"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactDeviceManagementEventType", propOrder = {
    "dmEventType"
})
public class SearchCriteriaExactDeviceManagementEventType
    extends SearchCriteria
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected DeviceManagementEventType dmEventType;

    /**
     * Ruft den Wert der dmEventType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementEventType }
     *     
     */
    public DeviceManagementEventType getDmEventType() {
        return dmEventType;
    }

    /**
     * Legt den Wert der dmEventType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementEventType }
     *     
     */
    public void setDmEventType(DeviceManagementEventType value) {
        this.dmEventType = value;
    }

}
