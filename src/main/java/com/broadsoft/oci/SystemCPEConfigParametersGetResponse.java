//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemCPEConfigParametersGetListRequest.
 *         Contains a list of system CPE Config parameters.
 *         Replaced By: SystemCPEConfigParametersGetResponse14sp6
 *       
 * 
 * <p>Java-Klasse für SystemCPEConfigParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCPEConfigParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableIPDeviceManagement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="ftpConnectTimeoutSeconds" type="{}DeviceManagementFTPConnectTimeoutSeconds"/>
 *         <element name="ftpFileTransferTimeoutSeconds" type="{}DeviceManagementFTPFileTransferTimeoutSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCPEConfigParametersGetResponse", propOrder = {
    "enableIPDeviceManagement",
    "ftpConnectTimeoutSeconds",
    "ftpFileTransferTimeoutSeconds"
})
public class SystemCPEConfigParametersGetResponse
    extends OCIDataResponse
{

    protected boolean enableIPDeviceManagement;
    protected int ftpConnectTimeoutSeconds;
    protected int ftpFileTransferTimeoutSeconds;

    /**
     * Ruft den Wert der enableIPDeviceManagement-Eigenschaft ab.
     * 
     */
    public boolean isEnableIPDeviceManagement() {
        return enableIPDeviceManagement;
    }

    /**
     * Legt den Wert der enableIPDeviceManagement-Eigenschaft fest.
     * 
     */
    public void setEnableIPDeviceManagement(boolean value) {
        this.enableIPDeviceManagement = value;
    }

    /**
     * Ruft den Wert der ftpConnectTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getFtpConnectTimeoutSeconds() {
        return ftpConnectTimeoutSeconds;
    }

    /**
     * Legt den Wert der ftpConnectTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setFtpConnectTimeoutSeconds(int value) {
        this.ftpConnectTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der ftpFileTransferTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getFtpFileTransferTimeoutSeconds() {
        return ftpFileTransferTimeoutSeconds;
    }

    /**
     * Legt den Wert der ftpFileTransferTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setFtpFileTransferTimeoutSeconds(int value) {
        this.ftpFileTransferTimeoutSeconds = value;
    }

}
