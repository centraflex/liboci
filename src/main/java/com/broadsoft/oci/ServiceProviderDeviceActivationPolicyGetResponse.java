//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderDeviceActivationPolicyGetRequest.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderDeviceActivationPolicyGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderDeviceActivationPolicyGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useServiceProviderSettings" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowActivationCodeRequestByUser" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sendActivationCodeInEmail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderDeviceActivationPolicyGetResponse", propOrder = {
    "useServiceProviderSettings",
    "allowActivationCodeRequestByUser",
    "sendActivationCodeInEmail"
})
public class ServiceProviderDeviceActivationPolicyGetResponse
    extends OCIDataResponse
{

    protected boolean useServiceProviderSettings;
    protected boolean allowActivationCodeRequestByUser;
    protected boolean sendActivationCodeInEmail;

    /**
     * Ruft den Wert der useServiceProviderSettings-Eigenschaft ab.
     * 
     */
    public boolean isUseServiceProviderSettings() {
        return useServiceProviderSettings;
    }

    /**
     * Legt den Wert der useServiceProviderSettings-Eigenschaft fest.
     * 
     */
    public void setUseServiceProviderSettings(boolean value) {
        this.useServiceProviderSettings = value;
    }

    /**
     * Ruft den Wert der allowActivationCodeRequestByUser-Eigenschaft ab.
     * 
     */
    public boolean isAllowActivationCodeRequestByUser() {
        return allowActivationCodeRequestByUser;
    }

    /**
     * Legt den Wert der allowActivationCodeRequestByUser-Eigenschaft fest.
     * 
     */
    public void setAllowActivationCodeRequestByUser(boolean value) {
        this.allowActivationCodeRequestByUser = value;
    }

    /**
     * Ruft den Wert der sendActivationCodeInEmail-Eigenschaft ab.
     * 
     */
    public boolean isSendActivationCodeInEmail() {
        return sendActivationCodeInEmail;
    }

    /**
     * Legt den Wert der sendActivationCodeInEmail-Eigenschaft fest.
     * 
     */
    public void setSendActivationCodeInEmail(boolean value) {
        this.sendActivationCodeInEmail = value;
    }

}
