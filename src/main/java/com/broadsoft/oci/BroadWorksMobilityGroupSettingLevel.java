//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für BroadWorksMobilityGroupSettingLevel.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="BroadWorksMobilityGroupSettingLevel">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Group"/>
 *     <enumeration value="ServiceProvider"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "BroadWorksMobilityGroupSettingLevel")
@XmlEnum
public enum BroadWorksMobilityGroupSettingLevel {

    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("ServiceProvider")
    SERVICE_PROVIDER("ServiceProvider");
    private final String value;

    BroadWorksMobilityGroupSettingLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BroadWorksMobilityGroupSettingLevel fromValue(String v) {
        for (BroadWorksMobilityGroupSettingLevel c: BroadWorksMobilityGroupSettingLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
