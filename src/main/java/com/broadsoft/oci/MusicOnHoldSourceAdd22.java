//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Contains the music on hold source configuration.
 *         The following elements are only used in HSS data mode and ignored in AS data mode:
 *           labeledMediaFiles
 *         The following elements are only used in AS data mode and ignored in HSS data mode:
 *           announcementMediaFiles
 *           authenticationRequired
 *           authenticationUserName
 *           authenticationPassword
 *       
 * 
 * <p>Java-Klasse für MusicOnHoldSourceAdd22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="MusicOnHoldSourceAdd22">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="audioFilePreferredCodec" type="{}AudioFileCodec22"/>
 *         <element name="messageSourceSelection" type="{}MusicOnHoldMessageSelection"/>
 *         <choice minOccurs="0">
 *           <element name="labeledCustomSourceMediaFiles">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="audioFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *                     <element name="videoFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *           <element name="announcementCustomSourceMediaFiles">
 *             <complexType>
 *               <complexContent>
 *                 <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   <sequence>
 *                     <element name="audioFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *                     <element name="videoFile" type="{}AnnouncementFileKey" minOccurs="0"/>
 *                   </sequence>
 *                 </restriction>
 *               </complexContent>
 *             </complexType>
 *           </element>
 *         </choice>
 *         <element name="externalSource" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointAdd"/>
 *                   <element name="authenticationRequired" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   <element name="authenticationUserName" type="{}SIPAuthenticationUserName" minOccurs="0"/>
 *                   <element name="authenticationPassword" type="{}Password" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MusicOnHoldSourceAdd22", propOrder = {
    "audioFilePreferredCodec",
    "messageSourceSelection",
    "labeledCustomSourceMediaFiles",
    "announcementCustomSourceMediaFiles",
    "externalSource"
})
public class MusicOnHoldSourceAdd22 {

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AudioFileCodec22 audioFilePreferredCodec;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected MusicOnHoldMessageSelection messageSourceSelection;
    protected MusicOnHoldSourceAdd22 .LabeledCustomSourceMediaFiles labeledCustomSourceMediaFiles;
    protected MusicOnHoldSourceAdd22 .AnnouncementCustomSourceMediaFiles announcementCustomSourceMediaFiles;
    protected MusicOnHoldSourceAdd22 .ExternalSource externalSource;

    /**
     * Ruft den Wert der audioFilePreferredCodec-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AudioFileCodec22 }
     *     
     */
    public AudioFileCodec22 getAudioFilePreferredCodec() {
        return audioFilePreferredCodec;
    }

    /**
     * Legt den Wert der audioFilePreferredCodec-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AudioFileCodec22 }
     *     
     */
    public void setAudioFilePreferredCodec(AudioFileCodec22 value) {
        this.audioFilePreferredCodec = value;
    }

    /**
     * Ruft den Wert der messageSourceSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public MusicOnHoldMessageSelection getMessageSourceSelection() {
        return messageSourceSelection;
    }

    /**
     * Legt den Wert der messageSourceSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldMessageSelection }
     *     
     */
    public void setMessageSourceSelection(MusicOnHoldMessageSelection value) {
        this.messageSourceSelection = value;
    }

    /**
     * Ruft den Wert der labeledCustomSourceMediaFiles-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceAdd22 .LabeledCustomSourceMediaFiles }
     *     
     */
    public MusicOnHoldSourceAdd22 .LabeledCustomSourceMediaFiles getLabeledCustomSourceMediaFiles() {
        return labeledCustomSourceMediaFiles;
    }

    /**
     * Legt den Wert der labeledCustomSourceMediaFiles-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceAdd22 .LabeledCustomSourceMediaFiles }
     *     
     */
    public void setLabeledCustomSourceMediaFiles(MusicOnHoldSourceAdd22 .LabeledCustomSourceMediaFiles value) {
        this.labeledCustomSourceMediaFiles = value;
    }

    /**
     * Ruft den Wert der announcementCustomSourceMediaFiles-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceAdd22 .AnnouncementCustomSourceMediaFiles }
     *     
     */
    public MusicOnHoldSourceAdd22 .AnnouncementCustomSourceMediaFiles getAnnouncementCustomSourceMediaFiles() {
        return announcementCustomSourceMediaFiles;
    }

    /**
     * Legt den Wert der announcementCustomSourceMediaFiles-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceAdd22 .AnnouncementCustomSourceMediaFiles }
     *     
     */
    public void setAnnouncementCustomSourceMediaFiles(MusicOnHoldSourceAdd22 .AnnouncementCustomSourceMediaFiles value) {
        this.announcementCustomSourceMediaFiles = value;
    }

    /**
     * Ruft den Wert der externalSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MusicOnHoldSourceAdd22 .ExternalSource }
     *     
     */
    public MusicOnHoldSourceAdd22 .ExternalSource getExternalSource() {
        return externalSource;
    }

    /**
     * Legt den Wert der externalSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MusicOnHoldSourceAdd22 .ExternalSource }
     *     
     */
    public void setExternalSource(MusicOnHoldSourceAdd22 .ExternalSource value) {
        this.externalSource = value;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="audioFile" type="{}AnnouncementFileKey" minOccurs="0"/>
     *         <element name="videoFile" type="{}AnnouncementFileKey" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "audioFile",
        "videoFile"
    })
    public static class AnnouncementCustomSourceMediaFiles {

        protected AnnouncementFileKey audioFile;
        protected AnnouncementFileKey videoFile;

        /**
         * Ruft den Wert der audioFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AnnouncementFileKey }
         *     
         */
        public AnnouncementFileKey getAudioFile() {
            return audioFile;
        }

        /**
         * Legt den Wert der audioFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AnnouncementFileKey }
         *     
         */
        public void setAudioFile(AnnouncementFileKey value) {
            this.audioFile = value;
        }

        /**
         * Ruft den Wert der videoFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AnnouncementFileKey }
         *     
         */
        public AnnouncementFileKey getVideoFile() {
            return videoFile;
        }

        /**
         * Legt den Wert der videoFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AnnouncementFileKey }
         *     
         */
        public void setVideoFile(AnnouncementFileKey value) {
            this.videoFile = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointAdd"/>
     *         <element name="authenticationRequired" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         <element name="authenticationUserName" type="{}SIPAuthenticationUserName" minOccurs="0"/>
     *         <element name="authenticationPassword" type="{}Password" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accessDeviceEndpoint",
        "authenticationRequired",
        "authenticationUserName",
        "authenticationPassword"
    })
    public static class ExternalSource {

        @XmlElement(required = true)
        protected AccessDeviceEndpointAdd accessDeviceEndpoint;
        protected Boolean authenticationRequired;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String authenticationUserName;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String authenticationPassword;

        /**
         * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link AccessDeviceEndpointAdd }
         *     
         */
        public AccessDeviceEndpointAdd getAccessDeviceEndpoint() {
            return accessDeviceEndpoint;
        }

        /**
         * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link AccessDeviceEndpointAdd }
         *     
         */
        public void setAccessDeviceEndpoint(AccessDeviceEndpointAdd value) {
            this.accessDeviceEndpoint = value;
        }

        /**
         * Ruft den Wert der authenticationRequired-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAuthenticationRequired() {
            return authenticationRequired;
        }

        /**
         * Legt den Wert der authenticationRequired-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAuthenticationRequired(Boolean value) {
            this.authenticationRequired = value;
        }

        /**
         * Ruft den Wert der authenticationUserName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAuthenticationUserName() {
            return authenticationUserName;
        }

        /**
         * Legt den Wert der authenticationUserName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAuthenticationUserName(String value) {
            this.authenticationUserName = value;
        }

        /**
         * Ruft den Wert der authenticationPassword-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAuthenticationPassword() {
            return authenticationPassword;
        }

        /**
         * Legt den Wert der authenticationPassword-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAuthenticationPassword(String value) {
            this.authenticationPassword = value;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="audioFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
     *         <element name="videoFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "audioFile",
        "videoFile"
    })
    public static class LabeledCustomSourceMediaFiles {

        protected LabeledMediaFileResource audioFile;
        protected LabeledMediaFileResource videoFile;

        /**
         * Ruft den Wert der audioFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public LabeledMediaFileResource getAudioFile() {
            return audioFile;
        }

        /**
         * Legt den Wert der audioFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public void setAudioFile(LabeledMediaFileResource value) {
            this.audioFile = value;
        }

        /**
         * Ruft den Wert der videoFile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public LabeledMediaFileResource getVideoFile() {
            return videoFile;
        }

        /**
         * Legt den Wert der videoFile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link LabeledMediaFileResource }
         *     
         */
        public void setVideoFile(LabeledMediaFileResource value) {
            this.videoFile = value;
        }

    }

}
