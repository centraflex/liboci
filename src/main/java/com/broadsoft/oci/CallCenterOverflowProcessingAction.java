//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterOverflowProcessingAction.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterOverflowProcessingAction">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Busy"/>
 *     <enumeration value="Transfer"/>
 *     <enumeration value="Ringing"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterOverflowProcessingAction")
@XmlEnum
public enum CallCenterOverflowProcessingAction {

    @XmlEnumValue("Busy")
    BUSY("Busy"),
    @XmlEnumValue("Transfer")
    TRANSFER("Transfer"),
    @XmlEnumValue("Ringing")
    RINGING("Ringing");
    private final String value;

    CallCenterOverflowProcessingAction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterOverflowProcessingAction fromValue(String v) {
        for (CallCenterOverflowProcessingAction c: CallCenterOverflowProcessingAction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
