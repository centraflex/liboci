//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserBasicCallLogsGetListRequest.
 *         Replaced By: UserBasicCallLogsGetListResponse14sp4
 *       
 * 
 * <p>Java-Klasse für UserBasicCallLogsGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBasicCallLogsGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="placed" type="{}BasicCallLogsEntry" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="received" type="{}BasicCallLogsEntry" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="missed" type="{}BasicCallLogsEntry" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBasicCallLogsGetListResponse", propOrder = {
    "placed",
    "received",
    "missed"
})
public class UserBasicCallLogsGetListResponse
    extends OCIDataResponse
{

    protected List<BasicCallLogsEntry> placed;
    protected List<BasicCallLogsEntry> received;
    protected List<BasicCallLogsEntry> missed;

    /**
     * Gets the value of the placed property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the placed property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlaced().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BasicCallLogsEntry }
     * 
     * 
     * @return
     *     The value of the placed property.
     */
    public List<BasicCallLogsEntry> getPlaced() {
        if (placed == null) {
            placed = new ArrayList<>();
        }
        return this.placed;
    }

    /**
     * Gets the value of the received property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the received property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceived().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BasicCallLogsEntry }
     * 
     * 
     * @return
     *     The value of the received property.
     */
    public List<BasicCallLogsEntry> getReceived() {
        if (received == null) {
            received = new ArrayList<>();
        }
        return this.received;
    }

    /**
     * Gets the value of the missed property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the missed property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMissed().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BasicCallLogsEntry }
     * 
     * 
     * @return
     *     The value of the missed property.
     */
    public List<BasicCallLogsEntry> getMissed() {
        if (missed == null) {
            missed = new ArrayList<>();
        }
        return this.missed;
    }

}
