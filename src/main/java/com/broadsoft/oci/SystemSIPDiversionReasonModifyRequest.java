//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the SIP cause value for a given diversion reason.
 *         The response is either a SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemSIPDiversionReasonModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSIPDiversionReasonModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="diversionReason" type="{}SIPDiversionReason"/>
 *         <element name="causeValue" type="{}SIPCauseValue"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSIPDiversionReasonModifyRequest", propOrder = {
    "diversionReason",
    "causeValue"
})
public class SystemSIPDiversionReasonModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SIPDiversionReason diversionReason;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String causeValue;

    /**
     * Ruft den Wert der diversionReason-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SIPDiversionReason }
     *     
     */
    public SIPDiversionReason getDiversionReason() {
        return diversionReason;
    }

    /**
     * Legt den Wert der diversionReason-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SIPDiversionReason }
     *     
     */
    public void setDiversionReason(SIPDiversionReason value) {
        this.diversionReason = value;
    }

    /**
     * Ruft den Wert der causeValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCauseValue() {
        return causeValue;
    }

    /**
     * Legt den Wert der causeValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCauseValue(String value) {
        this.causeValue = value;
    }

}
