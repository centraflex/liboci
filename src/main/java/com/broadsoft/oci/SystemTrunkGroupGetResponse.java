//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemTrunkGroupGetRequest.
 *         
 *         Replaced by: SystemTrunkGroupGetResponse19sp1.
 *       
 * 
 * <p>Java-Klasse für SystemTrunkGroupGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemTrunkGroupGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enforceCLIDServiceAssignmentForPilotUser" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="terminateUnreachableTriggerDetectionOnReceiptOf18x" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="pilotUserCallingLineAssertedIdentityPolicy" type="{}TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy"/>
 *         <element name="enforceOutOfDialogPBXRedirectionPolicies" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="unscreenedRedirectionHandling" type="{}TrunkGroupUnscreenedRedirectionHandling"/>
 *         <element name="enableHoldoverOfHighwaterCallCounts" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="holdoverPeriod" type="{}TrunkGroupHighwaterCallCountHoldoverPeriodMinutes"/>
 *         <element name="timeZoneOffsetMinutes" type="{}TrunkGroupTimeZoneOffsetMinutes"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemTrunkGroupGetResponse", propOrder = {
    "enforceCLIDServiceAssignmentForPilotUser",
    "terminateUnreachableTriggerDetectionOnReceiptOf18X",
    "pilotUserCallingLineAssertedIdentityPolicy",
    "enforceOutOfDialogPBXRedirectionPolicies",
    "unscreenedRedirectionHandling",
    "enableHoldoverOfHighwaterCallCounts",
    "holdoverPeriod",
    "timeZoneOffsetMinutes"
})
public class SystemTrunkGroupGetResponse
    extends OCIDataResponse
{

    protected boolean enforceCLIDServiceAssignmentForPilotUser;
    @XmlElement(name = "terminateUnreachableTriggerDetectionOnReceiptOf18x")
    protected boolean terminateUnreachableTriggerDetectionOnReceiptOf18X;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy pilotUserCallingLineAssertedIdentityPolicy;
    protected boolean enforceOutOfDialogPBXRedirectionPolicies;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TrunkGroupUnscreenedRedirectionHandling unscreenedRedirectionHandling;
    protected boolean enableHoldoverOfHighwaterCallCounts;
    protected int holdoverPeriod;
    protected int timeZoneOffsetMinutes;

    /**
     * Ruft den Wert der enforceCLIDServiceAssignmentForPilotUser-Eigenschaft ab.
     * 
     */
    public boolean isEnforceCLIDServiceAssignmentForPilotUser() {
        return enforceCLIDServiceAssignmentForPilotUser;
    }

    /**
     * Legt den Wert der enforceCLIDServiceAssignmentForPilotUser-Eigenschaft fest.
     * 
     */
    public void setEnforceCLIDServiceAssignmentForPilotUser(boolean value) {
        this.enforceCLIDServiceAssignmentForPilotUser = value;
    }

    /**
     * Ruft den Wert der terminateUnreachableTriggerDetectionOnReceiptOf18X-Eigenschaft ab.
     * 
     */
    public boolean isTerminateUnreachableTriggerDetectionOnReceiptOf18X() {
        return terminateUnreachableTriggerDetectionOnReceiptOf18X;
    }

    /**
     * Legt den Wert der terminateUnreachableTriggerDetectionOnReceiptOf18X-Eigenschaft fest.
     * 
     */
    public void setTerminateUnreachableTriggerDetectionOnReceiptOf18X(boolean value) {
        this.terminateUnreachableTriggerDetectionOnReceiptOf18X = value;
    }

    /**
     * Ruft den Wert der pilotUserCallingLineAssertedIdentityPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy }
     *     
     */
    public TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy getPilotUserCallingLineAssertedIdentityPolicy() {
        return pilotUserCallingLineAssertedIdentityPolicy;
    }

    /**
     * Legt den Wert der pilotUserCallingLineAssertedIdentityPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy }
     *     
     */
    public void setPilotUserCallingLineAssertedIdentityPolicy(TrunkGroupPilotUserCallingLineAssertedIdentityUsagePolicy value) {
        this.pilotUserCallingLineAssertedIdentityPolicy = value;
    }

    /**
     * Ruft den Wert der enforceOutOfDialogPBXRedirectionPolicies-Eigenschaft ab.
     * 
     */
    public boolean isEnforceOutOfDialogPBXRedirectionPolicies() {
        return enforceOutOfDialogPBXRedirectionPolicies;
    }

    /**
     * Legt den Wert der enforceOutOfDialogPBXRedirectionPolicies-Eigenschaft fest.
     * 
     */
    public void setEnforceOutOfDialogPBXRedirectionPolicies(boolean value) {
        this.enforceOutOfDialogPBXRedirectionPolicies = value;
    }

    /**
     * Ruft den Wert der unscreenedRedirectionHandling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupUnscreenedRedirectionHandling }
     *     
     */
    public TrunkGroupUnscreenedRedirectionHandling getUnscreenedRedirectionHandling() {
        return unscreenedRedirectionHandling;
    }

    /**
     * Legt den Wert der unscreenedRedirectionHandling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupUnscreenedRedirectionHandling }
     *     
     */
    public void setUnscreenedRedirectionHandling(TrunkGroupUnscreenedRedirectionHandling value) {
        this.unscreenedRedirectionHandling = value;
    }

    /**
     * Ruft den Wert der enableHoldoverOfHighwaterCallCounts-Eigenschaft ab.
     * 
     */
    public boolean isEnableHoldoverOfHighwaterCallCounts() {
        return enableHoldoverOfHighwaterCallCounts;
    }

    /**
     * Legt den Wert der enableHoldoverOfHighwaterCallCounts-Eigenschaft fest.
     * 
     */
    public void setEnableHoldoverOfHighwaterCallCounts(boolean value) {
        this.enableHoldoverOfHighwaterCallCounts = value;
    }

    /**
     * Ruft den Wert der holdoverPeriod-Eigenschaft ab.
     * 
     */
    public int getHoldoverPeriod() {
        return holdoverPeriod;
    }

    /**
     * Legt den Wert der holdoverPeriod-Eigenschaft fest.
     * 
     */
    public void setHoldoverPeriod(int value) {
        this.holdoverPeriod = value;
    }

    /**
     * Ruft den Wert der timeZoneOffsetMinutes-Eigenschaft ab.
     * 
     */
    public int getTimeZoneOffsetMinutes() {
        return timeZoneOffsetMinutes;
    }

    /**
     * Legt den Wert der timeZoneOffsetMinutes-Eigenschaft fest.
     * 
     */
    public void setTimeZoneOffsetMinutes(int value) {
        this.timeZoneOffsetMinutes = value;
    }

}
