//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with SystemSelectiveServicesRequest.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemSelectiveServicesModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSelectiveServicesModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="scheduleCombination" type="{}ScheduleCombinationType" minOccurs="0"/>
 *         <element name="screenPrivateNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="emptyHolidayScheduleIsOutOfSchedule" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSelectiveServicesModifyRequest", propOrder = {
    "scheduleCombination",
    "screenPrivateNumber",
    "emptyHolidayScheduleIsOutOfSchedule"
})
public class SystemSelectiveServicesModifyRequest
    extends OCIRequest
{

    @XmlSchemaType(name = "token")
    protected ScheduleCombinationType scheduleCombination;
    protected Boolean screenPrivateNumber;
    protected Boolean emptyHolidayScheduleIsOutOfSchedule;

    /**
     * Ruft den Wert der scheduleCombination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ScheduleCombinationType }
     *     
     */
    public ScheduleCombinationType getScheduleCombination() {
        return scheduleCombination;
    }

    /**
     * Legt den Wert der scheduleCombination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ScheduleCombinationType }
     *     
     */
    public void setScheduleCombination(ScheduleCombinationType value) {
        this.scheduleCombination = value;
    }

    /**
     * Ruft den Wert der screenPrivateNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isScreenPrivateNumber() {
        return screenPrivateNumber;
    }

    /**
     * Legt den Wert der screenPrivateNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setScreenPrivateNumber(Boolean value) {
        this.screenPrivateNumber = value;
    }

    /**
     * Ruft den Wert der emptyHolidayScheduleIsOutOfSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEmptyHolidayScheduleIsOutOfSchedule() {
        return emptyHolidayScheduleIsOutOfSchedule;
    }

    /**
     * Legt den Wert der emptyHolidayScheduleIsOutOfSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEmptyHolidayScheduleIsOutOfSchedule(Boolean value) {
        this.emptyHolidayScheduleIsOutOfSchedule = value;
    }

}
