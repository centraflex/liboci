//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify a system Anonymous Call Rejection parameter.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemAnonymousCallRejectionModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAnonymousCallRejectionModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="paiRequired" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="screenOnlyLocalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAnonymousCallRejectionModifyRequest", propOrder = {
    "paiRequired",
    "screenOnlyLocalCalls"
})
public class SystemAnonymousCallRejectionModifyRequest
    extends OCIRequest
{

    protected Boolean paiRequired;
    protected Boolean screenOnlyLocalCalls;

    /**
     * Ruft den Wert der paiRequired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPaiRequired() {
        return paiRequired;
    }

    /**
     * Legt den Wert der paiRequired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPaiRequired(Boolean value) {
        this.paiRequired = value;
    }

    /**
     * Ruft den Wert der screenOnlyLocalCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isScreenOnlyLocalCalls() {
        return screenOnlyLocalCalls;
    }

    /**
     * Legt den Wert der screenOnlyLocalCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setScreenOnlyLocalCalls(Boolean value) {
        this.screenOnlyLocalCalls = value;
    }

}
