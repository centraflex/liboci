//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the ServiceProviderTrunkGroupGetRequest23.
 *         The response contains the maximum and bursting maximum permissible active trunk group calls for the service provider.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderTrunkGroupGetResponse23 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderTrunkGroupGetResponse23">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maxActiveCalls" type="{}UnboundedNonNegativeInt"/>
 *         <element name="burstingMaxActiveCalls" type="{}UnboundedNonNegativeInt"/>
 *         <element name="numberOfBurstingBTLUs" type="{}UnboundedNonNegativeInt"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderTrunkGroupGetResponse23", propOrder = {
    "maxActiveCalls",
    "burstingMaxActiveCalls",
    "numberOfBurstingBTLUs"
})
public class ServiceProviderTrunkGroupGetResponse23
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected UnboundedNonNegativeInt maxActiveCalls;
    @XmlElement(required = true)
    protected UnboundedNonNegativeInt burstingMaxActiveCalls;
    @XmlElement(required = true)
    protected UnboundedNonNegativeInt numberOfBurstingBTLUs;

    /**
     * Ruft den Wert der maxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public UnboundedNonNegativeInt getMaxActiveCalls() {
        return maxActiveCalls;
    }

    /**
     * Legt den Wert der maxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public void setMaxActiveCalls(UnboundedNonNegativeInt value) {
        this.maxActiveCalls = value;
    }

    /**
     * Ruft den Wert der burstingMaxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public UnboundedNonNegativeInt getBurstingMaxActiveCalls() {
        return burstingMaxActiveCalls;
    }

    /**
     * Legt den Wert der burstingMaxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public void setBurstingMaxActiveCalls(UnboundedNonNegativeInt value) {
        this.burstingMaxActiveCalls = value;
    }

    /**
     * Ruft den Wert der numberOfBurstingBTLUs-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public UnboundedNonNegativeInt getNumberOfBurstingBTLUs() {
        return numberOfBurstingBTLUs;
    }

    /**
     * Legt den Wert der numberOfBurstingBTLUs-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public void setNumberOfBurstingBTLUs(UnboundedNonNegativeInt value) {
        this.numberOfBurstingBTLUs = value;
    }

}
