//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify reseller Stir Shaken service settings.
 *       
 * 
 * <p>Java-Klasse für ResellerStirShakenModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerStirShakenModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="resellerId" type="{}ResellerId22"/>
 *         <element name="useParentLevelSettings" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="signingPolicy" type="{}StirShakenSigningPolicy" minOccurs="0"/>
 *         <element name="taggingPolicy" type="{}StirShakenTaggingPolicy" minOccurs="0"/>
 *         <element name="signEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="tagEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="signingServiceURL" type="{}URL" minOccurs="0"/>
 *         <element name="tagFromOrPAI" type="{}StirShakenTagFromOrPAI" minOccurs="0"/>
 *         <element name="verstatTag" type="{}StirShakenVerstatTag" minOccurs="0"/>
 *         <element name="useOSValueForOrigId" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="origUUID" type="{}StirShakenOrigUUID" minOccurs="0"/>
 *         <element name="attestationLevel" type="{}StirShakenAttestationLevel" minOccurs="0"/>
 *         <element name="enableVerification" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="verificationServiceURL" type="{}URL" minOccurs="0"/>
 *         <element name="verificationErrorHandling" type="{}StirShakenVerificationErrorHandling" minOccurs="0"/>
 *         <element name="proxyVerstatToCNAMSubscribe" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useUnknownHeadersFromCNAMNotify" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableSigningForUnscreenedTrunkGroupOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableTaggingForUnscreenedTrunkGroupOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="unscreenedTrunkGroupOriginationAttestationLevel" type="{}StirShakenUnscreenedTrunkGroupOriginationAttestationLevel" minOccurs="0"/>
 *         <element name="verifyGETSCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerStirShakenModifyRequest", propOrder = {
    "resellerId",
    "useParentLevelSettings",
    "signingPolicy",
    "taggingPolicy",
    "signEmergencyCalls",
    "tagEmergencyCalls",
    "signingServiceURL",
    "tagFromOrPAI",
    "verstatTag",
    "useOSValueForOrigId",
    "origUUID",
    "attestationLevel",
    "enableVerification",
    "verificationServiceURL",
    "verificationErrorHandling",
    "proxyVerstatToCNAMSubscribe",
    "useUnknownHeadersFromCNAMNotify",
    "enableSigningForUnscreenedTrunkGroupOriginations",
    "enableTaggingForUnscreenedTrunkGroupOriginations",
    "unscreenedTrunkGroupOriginationAttestationLevel",
    "verifyGETSCalls"
})
public class ResellerStirShakenModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    protected Boolean useParentLevelSettings;
    @XmlSchemaType(name = "token")
    protected StirShakenSigningPolicy signingPolicy;
    @XmlSchemaType(name = "token")
    protected StirShakenTaggingPolicy taggingPolicy;
    protected Boolean signEmergencyCalls;
    protected Boolean tagEmergencyCalls;
    @XmlElementRef(name = "signingServiceURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> signingServiceURL;
    @XmlSchemaType(name = "token")
    protected StirShakenTagFromOrPAI tagFromOrPAI;
    @XmlSchemaType(name = "token")
    protected StirShakenVerstatTag verstatTag;
    protected Boolean useOSValueForOrigId;
    @XmlElementRef(name = "origUUID", type = JAXBElement.class, required = false)
    protected JAXBElement<String> origUUID;
    @XmlSchemaType(name = "token")
    protected StirShakenAttestationLevel attestationLevel;
    protected Boolean enableVerification;
    @XmlElementRef(name = "verificationServiceURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> verificationServiceURL;
    @XmlSchemaType(name = "token")
    protected StirShakenVerificationErrorHandling verificationErrorHandling;
    protected Boolean proxyVerstatToCNAMSubscribe;
    protected Boolean useUnknownHeadersFromCNAMNotify;
    protected Boolean enableSigningForUnscreenedTrunkGroupOriginations;
    protected Boolean enableTaggingForUnscreenedTrunkGroupOriginations;
    @XmlSchemaType(name = "token")
    protected StirShakenUnscreenedTrunkGroupOriginationAttestationLevel unscreenedTrunkGroupOriginationAttestationLevel;
    protected Boolean verifyGETSCalls;

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Ruft den Wert der useParentLevelSettings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseParentLevelSettings() {
        return useParentLevelSettings;
    }

    /**
     * Legt den Wert der useParentLevelSettings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseParentLevelSettings(Boolean value) {
        this.useParentLevelSettings = value;
    }

    /**
     * Ruft den Wert der signingPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StirShakenSigningPolicy }
     *     
     */
    public StirShakenSigningPolicy getSigningPolicy() {
        return signingPolicy;
    }

    /**
     * Legt den Wert der signingPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StirShakenSigningPolicy }
     *     
     */
    public void setSigningPolicy(StirShakenSigningPolicy value) {
        this.signingPolicy = value;
    }

    /**
     * Ruft den Wert der taggingPolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StirShakenTaggingPolicy }
     *     
     */
    public StirShakenTaggingPolicy getTaggingPolicy() {
        return taggingPolicy;
    }

    /**
     * Legt den Wert der taggingPolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StirShakenTaggingPolicy }
     *     
     */
    public void setTaggingPolicy(StirShakenTaggingPolicy value) {
        this.taggingPolicy = value;
    }

    /**
     * Ruft den Wert der signEmergencyCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSignEmergencyCalls() {
        return signEmergencyCalls;
    }

    /**
     * Legt den Wert der signEmergencyCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSignEmergencyCalls(Boolean value) {
        this.signEmergencyCalls = value;
    }

    /**
     * Ruft den Wert der tagEmergencyCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTagEmergencyCalls() {
        return tagEmergencyCalls;
    }

    /**
     * Legt den Wert der tagEmergencyCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTagEmergencyCalls(Boolean value) {
        this.tagEmergencyCalls = value;
    }

    /**
     * Ruft den Wert der signingServiceURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSigningServiceURL() {
        return signingServiceURL;
    }

    /**
     * Legt den Wert der signingServiceURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSigningServiceURL(JAXBElement<String> value) {
        this.signingServiceURL = value;
    }

    /**
     * Ruft den Wert der tagFromOrPAI-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StirShakenTagFromOrPAI }
     *     
     */
    public StirShakenTagFromOrPAI getTagFromOrPAI() {
        return tagFromOrPAI;
    }

    /**
     * Legt den Wert der tagFromOrPAI-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StirShakenTagFromOrPAI }
     *     
     */
    public void setTagFromOrPAI(StirShakenTagFromOrPAI value) {
        this.tagFromOrPAI = value;
    }

    /**
     * Ruft den Wert der verstatTag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StirShakenVerstatTag }
     *     
     */
    public StirShakenVerstatTag getVerstatTag() {
        return verstatTag;
    }

    /**
     * Legt den Wert der verstatTag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StirShakenVerstatTag }
     *     
     */
    public void setVerstatTag(StirShakenVerstatTag value) {
        this.verstatTag = value;
    }

    /**
     * Ruft den Wert der useOSValueForOrigId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseOSValueForOrigId() {
        return useOSValueForOrigId;
    }

    /**
     * Legt den Wert der useOSValueForOrigId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseOSValueForOrigId(Boolean value) {
        this.useOSValueForOrigId = value;
    }

    /**
     * Ruft den Wert der origUUID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrigUUID() {
        return origUUID;
    }

    /**
     * Legt den Wert der origUUID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrigUUID(JAXBElement<String> value) {
        this.origUUID = value;
    }

    /**
     * Ruft den Wert der attestationLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StirShakenAttestationLevel }
     *     
     */
    public StirShakenAttestationLevel getAttestationLevel() {
        return attestationLevel;
    }

    /**
     * Legt den Wert der attestationLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StirShakenAttestationLevel }
     *     
     */
    public void setAttestationLevel(StirShakenAttestationLevel value) {
        this.attestationLevel = value;
    }

    /**
     * Ruft den Wert der enableVerification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableVerification() {
        return enableVerification;
    }

    /**
     * Legt den Wert der enableVerification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableVerification(Boolean value) {
        this.enableVerification = value;
    }

    /**
     * Ruft den Wert der verificationServiceURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVerificationServiceURL() {
        return verificationServiceURL;
    }

    /**
     * Legt den Wert der verificationServiceURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVerificationServiceURL(JAXBElement<String> value) {
        this.verificationServiceURL = value;
    }

    /**
     * Ruft den Wert der verificationErrorHandling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StirShakenVerificationErrorHandling }
     *     
     */
    public StirShakenVerificationErrorHandling getVerificationErrorHandling() {
        return verificationErrorHandling;
    }

    /**
     * Legt den Wert der verificationErrorHandling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StirShakenVerificationErrorHandling }
     *     
     */
    public void setVerificationErrorHandling(StirShakenVerificationErrorHandling value) {
        this.verificationErrorHandling = value;
    }

    /**
     * Ruft den Wert der proxyVerstatToCNAMSubscribe-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProxyVerstatToCNAMSubscribe() {
        return proxyVerstatToCNAMSubscribe;
    }

    /**
     * Legt den Wert der proxyVerstatToCNAMSubscribe-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProxyVerstatToCNAMSubscribe(Boolean value) {
        this.proxyVerstatToCNAMSubscribe = value;
    }

    /**
     * Ruft den Wert der useUnknownHeadersFromCNAMNotify-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseUnknownHeadersFromCNAMNotify() {
        return useUnknownHeadersFromCNAMNotify;
    }

    /**
     * Legt den Wert der useUnknownHeadersFromCNAMNotify-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseUnknownHeadersFromCNAMNotify(Boolean value) {
        this.useUnknownHeadersFromCNAMNotify = value;
    }

    /**
     * Ruft den Wert der enableSigningForUnscreenedTrunkGroupOriginations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableSigningForUnscreenedTrunkGroupOriginations() {
        return enableSigningForUnscreenedTrunkGroupOriginations;
    }

    /**
     * Legt den Wert der enableSigningForUnscreenedTrunkGroupOriginations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSigningForUnscreenedTrunkGroupOriginations(Boolean value) {
        this.enableSigningForUnscreenedTrunkGroupOriginations = value;
    }

    /**
     * Ruft den Wert der enableTaggingForUnscreenedTrunkGroupOriginations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableTaggingForUnscreenedTrunkGroupOriginations() {
        return enableTaggingForUnscreenedTrunkGroupOriginations;
    }

    /**
     * Legt den Wert der enableTaggingForUnscreenedTrunkGroupOriginations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableTaggingForUnscreenedTrunkGroupOriginations(Boolean value) {
        this.enableTaggingForUnscreenedTrunkGroupOriginations = value;
    }

    /**
     * Ruft den Wert der unscreenedTrunkGroupOriginationAttestationLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StirShakenUnscreenedTrunkGroupOriginationAttestationLevel }
     *     
     */
    public StirShakenUnscreenedTrunkGroupOriginationAttestationLevel getUnscreenedTrunkGroupOriginationAttestationLevel() {
        return unscreenedTrunkGroupOriginationAttestationLevel;
    }

    /**
     * Legt den Wert der unscreenedTrunkGroupOriginationAttestationLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StirShakenUnscreenedTrunkGroupOriginationAttestationLevel }
     *     
     */
    public void setUnscreenedTrunkGroupOriginationAttestationLevel(StirShakenUnscreenedTrunkGroupOriginationAttestationLevel value) {
        this.unscreenedTrunkGroupOriginationAttestationLevel = value;
    }

    /**
     * Ruft den Wert der verifyGETSCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVerifyGETSCalls() {
        return verifyGETSCalls;
    }

    /**
     * Legt den Wert der verifyGETSCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVerifyGETSCalls(Boolean value) {
        this.verifyGETSCalls = value;
    }

}
