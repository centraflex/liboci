//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to delete a user.
 *         
 *         If deleteExistingDevices is set to true, after the user is deleted, any device that is only used by the deleted user prior to the deletion will be deleted if the command is executed with the correct priviledge.
 *          Group administrator or above running this command can delete any group level devices. Service provider administrator or above can delete any service provider and group devices. Provisioning administrator or above can delete any devices. 
 *          An ErrorResponse will be returned if any device cannot be deleted because of insufficient privilege.
 * 
 *          If UnassignPhoneNumbersLevel is set to 'Group', the user's primary phone number, fax number and any alternate numbers, will be un-assigned from the group if the command is executed by a service provider administrator or above.
 *           When set to 'Service Provider', they will be un-assigned from the group and service provider if the command is executed by a provisioning administrator or above.
 *           When omitted, the number(s) will be left assigned to the group.
 *          An ErrorResponse will be returned if any number cannot be unassigned because of insufficient privilege.
 *      
 *       
 * 
 * <p>Java-Klasse für UserConsolidatedDeleteRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserConsolidatedDeleteRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="deleteExistingDevices" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="unassignPhoneNumbers" type="{}UnassignPhoneNumbersLevel" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserConsolidatedDeleteRequest", propOrder = {
    "userId",
    "deleteExistingDevices",
    "unassignPhoneNumbers"
})
public class UserConsolidatedDeleteRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean deleteExistingDevices;
    @XmlSchemaType(name = "token")
    protected UnassignPhoneNumbersLevel unassignPhoneNumbers;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der deleteExistingDevices-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeleteExistingDevices() {
        return deleteExistingDevices;
    }

    /**
     * Legt den Wert der deleteExistingDevices-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeleteExistingDevices(Boolean value) {
        this.deleteExistingDevices = value;
    }

    /**
     * Ruft den Wert der unassignPhoneNumbers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnassignPhoneNumbersLevel }
     *     
     */
    public UnassignPhoneNumbersLevel getUnassignPhoneNumbers() {
        return unassignPhoneNumbers;
    }

    /**
     * Legt den Wert der unassignPhoneNumbers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnassignPhoneNumbersLevel }
     *     
     */
    public void setUnassignPhoneNumbers(UnassignPhoneNumbersLevel value) {
        this.unassignPhoneNumbers = value;
    }

}
