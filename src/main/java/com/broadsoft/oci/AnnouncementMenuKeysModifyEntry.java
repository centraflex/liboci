//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal announcement menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für AnnouncementMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="AnnouncementMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="recordAudio" type="{}DigitAny" minOccurs="0"/>
 *         <element name="recordAudioVideo" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnnouncementMenuKeysModifyEntry", propOrder = {
    "recordAudio",
    "recordAudioVideo",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class AnnouncementMenuKeysModifyEntry {

    @XmlElementRef(name = "recordAudio", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recordAudio;
    @XmlElementRef(name = "recordAudioVideo", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recordAudioVideo;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repeatMenu;

    /**
     * Ruft den Wert der recordAudio-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecordAudio() {
        return recordAudio;
    }

    /**
     * Legt den Wert der recordAudio-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecordAudio(JAXBElement<String> value) {
        this.recordAudio = value;
    }

    /**
     * Ruft den Wert der recordAudioVideo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecordAudioVideo() {
        return recordAudioVideo;
    }

    /**
     * Legt den Wert der recordAudioVideo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecordAudioVideo(JAXBElement<String> value) {
        this.recordAudioVideo = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepeatMenu(JAXBElement<String> value) {
        this.repeatMenu = value;
    }

}
