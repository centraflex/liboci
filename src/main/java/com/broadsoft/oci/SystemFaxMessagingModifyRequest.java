//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with Fax Messaging.
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           statusDurationHours
 *           statusAuditIntervalHours
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemFaxMessagingModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemFaxMessagingModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="statusDurationHours" type="{}FaxMessagingStatusDurationHours" minOccurs="0"/>
 *         <element name="statusAuditIntervalHours" type="{}FaxMessagingStatusAuditIntervalHours" minOccurs="0"/>
 *         <element name="maximumConcurrentFaxesPerUser" type="{}FaxMessagingMaximumConcurrentFaxesPerUser" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemFaxMessagingModifyRequest", propOrder = {
    "statusDurationHours",
    "statusAuditIntervalHours",
    "maximumConcurrentFaxesPerUser"
})
public class SystemFaxMessagingModifyRequest
    extends OCIRequest
{

    protected Integer statusDurationHours;
    protected Integer statusAuditIntervalHours;
    protected Integer maximumConcurrentFaxesPerUser;

    /**
     * Ruft den Wert der statusDurationHours-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStatusDurationHours() {
        return statusDurationHours;
    }

    /**
     * Legt den Wert der statusDurationHours-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStatusDurationHours(Integer value) {
        this.statusDurationHours = value;
    }

    /**
     * Ruft den Wert der statusAuditIntervalHours-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStatusAuditIntervalHours() {
        return statusAuditIntervalHours;
    }

    /**
     * Legt den Wert der statusAuditIntervalHours-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStatusAuditIntervalHours(Integer value) {
        this.statusAuditIntervalHours = value;
    }

    /**
     * Ruft den Wert der maximumConcurrentFaxesPerUser-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaximumConcurrentFaxesPerUser() {
        return maximumConcurrentFaxesPerUser;
    }

    /**
     * Legt den Wert der maximumConcurrentFaxesPerUser-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaximumConcurrentFaxesPerUser(Integer value) {
        this.maximumConcurrentFaxesPerUser = value;
    }

}
