//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserEnhancedCallLogsGetListRequest16.
 *         Total numbers of rows is:
 *         - the total number of retrievable logs of the call log type that was specified in the UserEnhancedCallLogsGetListRequest16, 
 *           if a call log type was specified in the request.
 *         - the total number of retrievable logs, if no call log type was specified in the request.
 *         
 *         Replaced by UserEnhancedCallLogsGetListResponse17sp4          
 *       
 * 
 * <p>Java-Klasse für UserEnhancedCallLogsGetListResponse16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserEnhancedCallLogsGetListResponse16">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="totalNumberOfRows" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         <element name="callLog" type="{}MixedCallLogsEntry" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserEnhancedCallLogsGetListResponse16", propOrder = {
    "totalNumberOfRows",
    "callLog"
})
public class UserEnhancedCallLogsGetListResponse16
    extends OCIDataResponse
{

    protected int totalNumberOfRows;
    protected List<MixedCallLogsEntry> callLog;

    /**
     * Ruft den Wert der totalNumberOfRows-Eigenschaft ab.
     * 
     */
    public int getTotalNumberOfRows() {
        return totalNumberOfRows;
    }

    /**
     * Legt den Wert der totalNumberOfRows-Eigenschaft fest.
     * 
     */
    public void setTotalNumberOfRows(int value) {
        this.totalNumberOfRows = value;
    }

    /**
     * Gets the value of the callLog property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the callLog property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCallLog().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MixedCallLogsEntry }
     * 
     * 
     * @return
     *     The value of the callLog property.
     */
    public List<MixedCallLogsEntry> getCallLog() {
        if (callLog == null) {
            callLog = new ArrayList<>();
        }
        return this.callLog;
    }

}
