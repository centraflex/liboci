//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemMailParametersGetListRequest22.
 *         Contains a list of system Mail parameters.
 *       
 * 
 * <p>Java-Klasse für SystemMailParametersGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemMailParametersGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="primaryServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="secondaryServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="defaultFromAddress" type="{}SMTPFromAddress"/>
 *         <element name="defaultSubject" type="{}SMTPSubject" minOccurs="0"/>
 *         <element name="supportDNSSRVForMailServerAccess" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="secureMode" type="{}SMTPSecureMode"/>
 *         <element name="port" type="{}Port" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemMailParametersGetResponse22", propOrder = {
    "primaryServerNetAddress",
    "secondaryServerNetAddress",
    "defaultFromAddress",
    "defaultSubject",
    "supportDNSSRVForMailServerAccess",
    "secureMode",
    "port"
})
public class SystemMailParametersGetResponse22
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String primaryServerNetAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String secondaryServerNetAddress;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultFromAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultSubject;
    protected boolean supportDNSSRVForMailServerAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected SMTPSecureMode secureMode;
    protected Integer port;

    /**
     * Ruft den Wert der primaryServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryServerNetAddress() {
        return primaryServerNetAddress;
    }

    /**
     * Legt den Wert der primaryServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryServerNetAddress(String value) {
        this.primaryServerNetAddress = value;
    }

    /**
     * Ruft den Wert der secondaryServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryServerNetAddress() {
        return secondaryServerNetAddress;
    }

    /**
     * Legt den Wert der secondaryServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryServerNetAddress(String value) {
        this.secondaryServerNetAddress = value;
    }

    /**
     * Ruft den Wert der defaultFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultFromAddress() {
        return defaultFromAddress;
    }

    /**
     * Legt den Wert der defaultFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultFromAddress(String value) {
        this.defaultFromAddress = value;
    }

    /**
     * Ruft den Wert der defaultSubject-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultSubject() {
        return defaultSubject;
    }

    /**
     * Legt den Wert der defaultSubject-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultSubject(String value) {
        this.defaultSubject = value;
    }

    /**
     * Ruft den Wert der supportDNSSRVForMailServerAccess-Eigenschaft ab.
     * 
     */
    public boolean isSupportDNSSRVForMailServerAccess() {
        return supportDNSSRVForMailServerAccess;
    }

    /**
     * Legt den Wert der supportDNSSRVForMailServerAccess-Eigenschaft fest.
     * 
     */
    public void setSupportDNSSRVForMailServerAccess(boolean value) {
        this.supportDNSSRVForMailServerAccess = value;
    }

    /**
     * Ruft den Wert der secureMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SMTPSecureMode }
     *     
     */
    public SMTPSecureMode getSecureMode() {
        return secureMode;
    }

    /**
     * Legt den Wert der secureMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SMTPSecureMode }
     *     
     */
    public void setSecureMode(SMTPSecureMode value) {
        this.secureMode = value;
    }

    /**
     * Ruft den Wert der port-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPort() {
        return port;
    }

    /**
     * Legt den Wert der port-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPort(Integer value) {
        this.port = value;
    }

}
