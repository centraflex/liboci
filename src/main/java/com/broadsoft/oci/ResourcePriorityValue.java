//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ResourcePriorityValue.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ResourcePriorityValue">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="q735.0"/>
 *     <enumeration value="q735.1"/>
 *     <enumeration value="q735.2"/>
 *     <enumeration value="q735.3"/>
 *     <enumeration value="q735.4"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ResourcePriorityValue")
@XmlEnum
public enum ResourcePriorityValue {

    @XmlEnumValue("q735.0")
    Q_735_0("q735.0"),
    @XmlEnumValue("q735.1")
    Q_735_1("q735.1"),
    @XmlEnumValue("q735.2")
    Q_735_2("q735.2"),
    @XmlEnumValue("q735.3")
    Q_735_3("q735.3"),
    @XmlEnumValue("q735.4")
    Q_735_4("q735.4");
    private final String value;

    ResourcePriorityValue(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ResourcePriorityValue fromValue(String v) {
        for (ResourcePriorityValue c: ResourcePriorityValue.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
