//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Unassign a list of Network Classes of Service from a reseller.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ResellerNetworkClassOfServiceUnassignListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerNetworkClassOfServiceUnassignListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="resellerId" type="{}ResellerId22"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="defaultNetworkClassOfService" type="{}DefaultNetworkClassOfService" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerNetworkClassOfServiceUnassignListRequest", propOrder = {
    "resellerId",
    "networkClassOfService",
    "defaultNetworkClassOfService"
})
public class ResellerNetworkClassOfServiceUnassignListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> networkClassOfService;
    @XmlElementRef(name = "defaultNetworkClassOfService", type = JAXBElement.class, required = false)
    protected JAXBElement<DefaultNetworkClassOfService> defaultNetworkClassOfService;

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Gets the value of the networkClassOfService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the networkClassOfService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNetworkClassOfService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the networkClassOfService property.
     */
    public List<String> getNetworkClassOfService() {
        if (networkClassOfService == null) {
            networkClassOfService = new ArrayList<>();
        }
        return this.networkClassOfService;
    }

    /**
     * Ruft den Wert der defaultNetworkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DefaultNetworkClassOfService }{@code >}
     *     
     */
    public JAXBElement<DefaultNetworkClassOfService> getDefaultNetworkClassOfService() {
        return defaultNetworkClassOfService;
    }

    /**
     * Legt den Wert der defaultNetworkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DefaultNetworkClassOfService }{@code >}
     *     
     */
    public void setDefaultNetworkClassOfService(JAXBElement<DefaultNetworkClassOfService> value) {
        this.defaultNetworkClassOfService = value;
    }

}
