//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupTrunkGroupGetInstanceRequest14sp1.
 *         The publicUserIdentity in the ServiceInstanceReadProfile is not used for trunk groups.
 *         Returns the profile information for the Trunk Group.
 *       
 * 
 * <p>Java-Klasse für GroupTrunkGroupGetInstanceResponse14sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupTrunkGroupGetInstanceResponse14sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceReadProfile"/>
 *         <element name="accessDeviceEndpoint" type="{}AccessDeviceEndpointRead14" minOccurs="0"/>
 *         <element name="maxActiveCalls" type="{}MaxActiveCalls"/>
 *         <element name="maxIncomingCalls" type="{}MaxIncomingCalls" minOccurs="0"/>
 *         <element name="maxOutgoingCalls" type="{}MaxOutgoingCalls" minOccurs="0"/>
 *         <element name="enableBursting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="burstingMaxActiveCalls" type="{}BurstingMaxActiveCalls" minOccurs="0"/>
 *         <element name="burstingMaxIncomingCalls" type="{}BurstingMaxIncomingCalls" minOccurs="0"/>
 *         <element name="burstingMaxOutgoingCalls" type="{}BurstingMaxOutgoingCalls" minOccurs="0"/>
 *         <element name="capacityExceededAction" type="{}TrunkGroupCapacityExceededAction" minOccurs="0"/>
 *         <element name="capacityExceededForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="capacityExceededRerouteTrunkGroupId" type="{}UserId" minOccurs="0"/>
 *         <element name="capacityExceededTrapInitialCalls" type="{}TrapInitialThreshold"/>
 *         <element name="capacityExceededTrapOffsetCalls" type="{}TrapOffsetThreshold"/>
 *         <element name="unreachableDestinationAction" type="{}TrunkGroupUnreachableDestinationAction" minOccurs="0"/>
 *         <element name="unreachableDestinationForwardAddress" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="unreachableDestinationRerouteTrunkGroupId" type="{}UserId" minOccurs="0"/>
 *         <element name="unreachableDestinationTrapInitialCalls" type="{}TrapInitialThreshold"/>
 *         <element name="unreachableDestinationTrapOffsetCalls" type="{}TrapOffsetThreshold"/>
 *         <element name="invitationTimeout" type="{}TrunkGroupInvitationTimeoutSeconds"/>
 *         <element name="requireAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sipAuthenticationUserName" type="{}SIPAuthenticationUserName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupTrunkGroupGetInstanceResponse14sp1", propOrder = {
    "serviceInstanceProfile",
    "accessDeviceEndpoint",
    "maxActiveCalls",
    "maxIncomingCalls",
    "maxOutgoingCalls",
    "enableBursting",
    "burstingMaxActiveCalls",
    "burstingMaxIncomingCalls",
    "burstingMaxOutgoingCalls",
    "capacityExceededAction",
    "capacityExceededForwardAddress",
    "capacityExceededRerouteTrunkGroupId",
    "capacityExceededTrapInitialCalls",
    "capacityExceededTrapOffsetCalls",
    "unreachableDestinationAction",
    "unreachableDestinationForwardAddress",
    "unreachableDestinationRerouteTrunkGroupId",
    "unreachableDestinationTrapInitialCalls",
    "unreachableDestinationTrapOffsetCalls",
    "invitationTimeout",
    "requireAuthentication",
    "sipAuthenticationUserName"
})
public class GroupTrunkGroupGetInstanceResponse14Sp1
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected ServiceInstanceReadProfile serviceInstanceProfile;
    protected AccessDeviceEndpointRead14 accessDeviceEndpoint;
    protected int maxActiveCalls;
    protected Integer maxIncomingCalls;
    protected Integer maxOutgoingCalls;
    protected boolean enableBursting;
    protected Integer burstingMaxActiveCalls;
    protected Integer burstingMaxIncomingCalls;
    protected Integer burstingMaxOutgoingCalls;
    @XmlSchemaType(name = "token")
    protected TrunkGroupCapacityExceededAction capacityExceededAction;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String capacityExceededForwardAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String capacityExceededRerouteTrunkGroupId;
    protected int capacityExceededTrapInitialCalls;
    protected int capacityExceededTrapOffsetCalls;
    @XmlSchemaType(name = "token")
    protected TrunkGroupUnreachableDestinationAction unreachableDestinationAction;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String unreachableDestinationForwardAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String unreachableDestinationRerouteTrunkGroupId;
    protected int unreachableDestinationTrapInitialCalls;
    protected int unreachableDestinationTrapOffsetCalls;
    protected int invitationTimeout;
    protected boolean requireAuthentication;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sipAuthenticationUserName;

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceReadProfile }
     *     
     */
    public ServiceInstanceReadProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceReadProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceReadProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEndpointRead14 }
     *     
     */
    public AccessDeviceEndpointRead14 getAccessDeviceEndpoint() {
        return accessDeviceEndpoint;
    }

    /**
     * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEndpointRead14 }
     *     
     */
    public void setAccessDeviceEndpoint(AccessDeviceEndpointRead14 value) {
        this.accessDeviceEndpoint = value;
    }

    /**
     * Ruft den Wert der maxActiveCalls-Eigenschaft ab.
     * 
     */
    public int getMaxActiveCalls() {
        return maxActiveCalls;
    }

    /**
     * Legt den Wert der maxActiveCalls-Eigenschaft fest.
     * 
     */
    public void setMaxActiveCalls(int value) {
        this.maxActiveCalls = value;
    }

    /**
     * Ruft den Wert der maxIncomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxIncomingCalls() {
        return maxIncomingCalls;
    }

    /**
     * Legt den Wert der maxIncomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxIncomingCalls(Integer value) {
        this.maxIncomingCalls = value;
    }

    /**
     * Ruft den Wert der maxOutgoingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxOutgoingCalls() {
        return maxOutgoingCalls;
    }

    /**
     * Legt den Wert der maxOutgoingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxOutgoingCalls(Integer value) {
        this.maxOutgoingCalls = value;
    }

    /**
     * Ruft den Wert der enableBursting-Eigenschaft ab.
     * 
     */
    public boolean isEnableBursting() {
        return enableBursting;
    }

    /**
     * Legt den Wert der enableBursting-Eigenschaft fest.
     * 
     */
    public void setEnableBursting(boolean value) {
        this.enableBursting = value;
    }

    /**
     * Ruft den Wert der burstingMaxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBurstingMaxActiveCalls() {
        return burstingMaxActiveCalls;
    }

    /**
     * Legt den Wert der burstingMaxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBurstingMaxActiveCalls(Integer value) {
        this.burstingMaxActiveCalls = value;
    }

    /**
     * Ruft den Wert der burstingMaxIncomingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBurstingMaxIncomingCalls() {
        return burstingMaxIncomingCalls;
    }

    /**
     * Legt den Wert der burstingMaxIncomingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBurstingMaxIncomingCalls(Integer value) {
        this.burstingMaxIncomingCalls = value;
    }

    /**
     * Ruft den Wert der burstingMaxOutgoingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBurstingMaxOutgoingCalls() {
        return burstingMaxOutgoingCalls;
    }

    /**
     * Legt den Wert der burstingMaxOutgoingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBurstingMaxOutgoingCalls(Integer value) {
        this.burstingMaxOutgoingCalls = value;
    }

    /**
     * Ruft den Wert der capacityExceededAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupCapacityExceededAction }
     *     
     */
    public TrunkGroupCapacityExceededAction getCapacityExceededAction() {
        return capacityExceededAction;
    }

    /**
     * Legt den Wert der capacityExceededAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupCapacityExceededAction }
     *     
     */
    public void setCapacityExceededAction(TrunkGroupCapacityExceededAction value) {
        this.capacityExceededAction = value;
    }

    /**
     * Ruft den Wert der capacityExceededForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCapacityExceededForwardAddress() {
        return capacityExceededForwardAddress;
    }

    /**
     * Legt den Wert der capacityExceededForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCapacityExceededForwardAddress(String value) {
        this.capacityExceededForwardAddress = value;
    }

    /**
     * Ruft den Wert der capacityExceededRerouteTrunkGroupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCapacityExceededRerouteTrunkGroupId() {
        return capacityExceededRerouteTrunkGroupId;
    }

    /**
     * Legt den Wert der capacityExceededRerouteTrunkGroupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCapacityExceededRerouteTrunkGroupId(String value) {
        this.capacityExceededRerouteTrunkGroupId = value;
    }

    /**
     * Ruft den Wert der capacityExceededTrapInitialCalls-Eigenschaft ab.
     * 
     */
    public int getCapacityExceededTrapInitialCalls() {
        return capacityExceededTrapInitialCalls;
    }

    /**
     * Legt den Wert der capacityExceededTrapInitialCalls-Eigenschaft fest.
     * 
     */
    public void setCapacityExceededTrapInitialCalls(int value) {
        this.capacityExceededTrapInitialCalls = value;
    }

    /**
     * Ruft den Wert der capacityExceededTrapOffsetCalls-Eigenschaft ab.
     * 
     */
    public int getCapacityExceededTrapOffsetCalls() {
        return capacityExceededTrapOffsetCalls;
    }

    /**
     * Legt den Wert der capacityExceededTrapOffsetCalls-Eigenschaft fest.
     * 
     */
    public void setCapacityExceededTrapOffsetCalls(int value) {
        this.capacityExceededTrapOffsetCalls = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TrunkGroupUnreachableDestinationAction }
     *     
     */
    public TrunkGroupUnreachableDestinationAction getUnreachableDestinationAction() {
        return unreachableDestinationAction;
    }

    /**
     * Legt den Wert der unreachableDestinationAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TrunkGroupUnreachableDestinationAction }
     *     
     */
    public void setUnreachableDestinationAction(TrunkGroupUnreachableDestinationAction value) {
        this.unreachableDestinationAction = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationForwardAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnreachableDestinationForwardAddress() {
        return unreachableDestinationForwardAddress;
    }

    /**
     * Legt den Wert der unreachableDestinationForwardAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnreachableDestinationForwardAddress(String value) {
        this.unreachableDestinationForwardAddress = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationRerouteTrunkGroupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnreachableDestinationRerouteTrunkGroupId() {
        return unreachableDestinationRerouteTrunkGroupId;
    }

    /**
     * Legt den Wert der unreachableDestinationRerouteTrunkGroupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnreachableDestinationRerouteTrunkGroupId(String value) {
        this.unreachableDestinationRerouteTrunkGroupId = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationTrapInitialCalls-Eigenschaft ab.
     * 
     */
    public int getUnreachableDestinationTrapInitialCalls() {
        return unreachableDestinationTrapInitialCalls;
    }

    /**
     * Legt den Wert der unreachableDestinationTrapInitialCalls-Eigenschaft fest.
     * 
     */
    public void setUnreachableDestinationTrapInitialCalls(int value) {
        this.unreachableDestinationTrapInitialCalls = value;
    }

    /**
     * Ruft den Wert der unreachableDestinationTrapOffsetCalls-Eigenschaft ab.
     * 
     */
    public int getUnreachableDestinationTrapOffsetCalls() {
        return unreachableDestinationTrapOffsetCalls;
    }

    /**
     * Legt den Wert der unreachableDestinationTrapOffsetCalls-Eigenschaft fest.
     * 
     */
    public void setUnreachableDestinationTrapOffsetCalls(int value) {
        this.unreachableDestinationTrapOffsetCalls = value;
    }

    /**
     * Ruft den Wert der invitationTimeout-Eigenschaft ab.
     * 
     */
    public int getInvitationTimeout() {
        return invitationTimeout;
    }

    /**
     * Legt den Wert der invitationTimeout-Eigenschaft fest.
     * 
     */
    public void setInvitationTimeout(int value) {
        this.invitationTimeout = value;
    }

    /**
     * Ruft den Wert der requireAuthentication-Eigenschaft ab.
     * 
     */
    public boolean isRequireAuthentication() {
        return requireAuthentication;
    }

    /**
     * Legt den Wert der requireAuthentication-Eigenschaft fest.
     * 
     */
    public void setRequireAuthentication(boolean value) {
        this.requireAuthentication = value;
    }

    /**
     * Ruft den Wert der sipAuthenticationUserName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSipAuthenticationUserName() {
        return sipAuthenticationUserName;
    }

    /**
     * Legt den Wert der sipAuthenticationUserName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSipAuthenticationUserName(String value) {
        this.sipAuthenticationUserName = value;
    }

}
