//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserPersonalAssistantCallToNumberGetListRequest.
 *         Contains a list of assigned Call to Numbers".
 *       
 * 
 * <p>Java-Klasse für UserPersonalAssistantCallToNumberGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserPersonalAssistantCallToNumberGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="callToNumber" type="{}CallToNumber" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPersonalAssistantCallToNumberGetListResponse", propOrder = {
    "callToNumber"
})
public class UserPersonalAssistantCallToNumberGetListResponse
    extends OCIDataResponse
{

    protected List<CallToNumber> callToNumber;

    /**
     * Gets the value of the callToNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the callToNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCallToNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CallToNumber }
     * 
     * 
     * @return
     *     The value of the callToNumber property.
     */
    public List<CallToNumber> getCallToNumber() {
        if (callToNumber == null) {
            callToNumber = new ArrayList<>();
        }
        return this.callToNumber;
    }

}
