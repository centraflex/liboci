//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SIPMultipleDialogBehavior.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SIPMultipleDialogBehavior">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Multiple Dialogs"/>
 *     <enumeration value="Multiple Dialogs With Error Correction"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SIPMultipleDialogBehavior")
@XmlEnum
public enum SIPMultipleDialogBehavior {

    @XmlEnumValue("Multiple Dialogs")
    MULTIPLE_DIALOGS("Multiple Dialogs"),
    @XmlEnumValue("Multiple Dialogs With Error Correction")
    MULTIPLE_DIALOGS_WITH_ERROR_CORRECTION("Multiple Dialogs With Error Correction");
    private final String value;

    SIPMultipleDialogBehavior(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SIPMultipleDialogBehavior fromValue(String v) {
        for (SIPMultipleDialogBehavior c: SIPMultipleDialogBehavior.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
