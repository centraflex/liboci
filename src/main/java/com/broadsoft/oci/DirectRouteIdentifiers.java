//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlElementRefs;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Direct Route identifiers. 
 *       
 * 
 * <p>Java-Klasse für DirectRouteIdentifiers complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="DirectRouteIdentifiers">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <choice maxOccurs="10">
 *           <element name="dtgIdentity" type="{}OtgDtgIdentity"/>
 *           <element name="trunkIdentity" type="{}SIPURI"/>
 *         </choice>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DirectRouteIdentifiers", propOrder = {
    "dtgIdentityOrTrunkIdentity"
})
public class DirectRouteIdentifiers {

    @XmlElementRefs({
        @XmlElementRef(name = "dtgIdentity", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "trunkIdentity", type = JAXBElement.class, required = false)
    })
    protected List<JAXBElement<String>> dtgIdentityOrTrunkIdentity;

    /**
     * Gets the value of the dtgIdentityOrTrunkIdentity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the dtgIdentityOrTrunkIdentity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDtgIdentityOrTrunkIdentity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * 
     * @return
     *     The value of the dtgIdentityOrTrunkIdentity property.
     */
    public List<JAXBElement<String>> getDtgIdentityOrTrunkIdentity() {
        if (dtgIdentityOrTrunkIdentity == null) {
            dtgIdentityOrTrunkIdentity = new ArrayList<>();
        }
        return this.dtgIdentityOrTrunkIdentity;
    }

}
