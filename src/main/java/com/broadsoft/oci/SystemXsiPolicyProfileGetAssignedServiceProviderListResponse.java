//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemXsiPolicyProfileUsageGetListRequest.
 *         Contains a table with column headings: "Organization ID", "Organization Name", "Organization Type" and "Reseller Id".
 *  
 * 
 * <p>Java-Klasse für SystemXsiPolicyProfileGetAssignedServiceProviderListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemXsiPolicyProfileGetAssignedServiceProviderListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="spTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemXsiPolicyProfileGetAssignedServiceProviderListResponse", propOrder = {
    "spTable"
})
public class SystemXsiPolicyProfileGetAssignedServiceProviderListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable spTable;

    /**
     * Ruft den Wert der spTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getSpTable() {
        return spTable;
    }

    /**
     * Legt den Wert der spTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setSpTable(OCITable value) {
        this.spTable = value;
    }

}
