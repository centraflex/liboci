//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Request to modify Network Server Sync system parameters.
 *         The response is either SuccessResponse or ErrorResponse.
 *         The following elements are only used in AS data mode:
 *           syncTrunkGroups
 *           syncConnectionTimeoutSeconds
 *       
 * 
 * <p>Java-Klasse für SystemNetworkServerSyncParametersModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemNetworkServerSyncParametersModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enableSync" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="syncLinePorts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="syncDeviceManagementInfo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="syncTrunkGroups" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="syncConnectionTimeoutSeconds" type="{}NetworkServerSyncConnectionTimeoutSeconds" minOccurs="0"/>
 *         <element name="syncEnterpriseNumbers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNetworkServerSyncParametersModifyRequest", propOrder = {
    "enableSync",
    "syncLinePorts",
    "syncDeviceManagementInfo",
    "syncTrunkGroups",
    "syncConnectionTimeoutSeconds",
    "syncEnterpriseNumbers"
})
public class SystemNetworkServerSyncParametersModifyRequest
    extends OCIRequest
{

    protected Boolean enableSync;
    protected Boolean syncLinePorts;
    protected Boolean syncDeviceManagementInfo;
    protected Boolean syncTrunkGroups;
    protected Integer syncConnectionTimeoutSeconds;
    protected Boolean syncEnterpriseNumbers;

    /**
     * Ruft den Wert der enableSync-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableSync() {
        return enableSync;
    }

    /**
     * Legt den Wert der enableSync-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSync(Boolean value) {
        this.enableSync = value;
    }

    /**
     * Ruft den Wert der syncLinePorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSyncLinePorts() {
        return syncLinePorts;
    }

    /**
     * Legt den Wert der syncLinePorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSyncLinePorts(Boolean value) {
        this.syncLinePorts = value;
    }

    /**
     * Ruft den Wert der syncDeviceManagementInfo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSyncDeviceManagementInfo() {
        return syncDeviceManagementInfo;
    }

    /**
     * Legt den Wert der syncDeviceManagementInfo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSyncDeviceManagementInfo(Boolean value) {
        this.syncDeviceManagementInfo = value;
    }

    /**
     * Ruft den Wert der syncTrunkGroups-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSyncTrunkGroups() {
        return syncTrunkGroups;
    }

    /**
     * Legt den Wert der syncTrunkGroups-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSyncTrunkGroups(Boolean value) {
        this.syncTrunkGroups = value;
    }

    /**
     * Ruft den Wert der syncConnectionTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSyncConnectionTimeoutSeconds() {
        return syncConnectionTimeoutSeconds;
    }

    /**
     * Legt den Wert der syncConnectionTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSyncConnectionTimeoutSeconds(Integer value) {
        this.syncConnectionTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der syncEnterpriseNumbers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSyncEnterpriseNumbers() {
        return syncEnterpriseNumbers;
    }

    /**
     * Legt den Wert der syncEnterpriseNumbers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSyncEnterpriseNumbers(Boolean value) {
        this.syncEnterpriseNumbers = value;
    }

}
