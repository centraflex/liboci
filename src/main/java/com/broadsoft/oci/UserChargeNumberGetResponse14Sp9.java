//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserChargeNumberGetRequest14sp9.
 *       
 * 
 * <p>Java-Klasse für UserChargeNumberGetResponse14sp9 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserChargeNumberGetResponse14sp9">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="phoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="useChargeNumberForEnhancedTranslations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sendChargeNumberToNetwork" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserChargeNumberGetResponse14sp9", propOrder = {
    "phoneNumber",
    "useChargeNumberForEnhancedTranslations",
    "sendChargeNumberToNetwork"
})
public class UserChargeNumberGetResponse14Sp9
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String phoneNumber;
    protected boolean useChargeNumberForEnhancedTranslations;
    protected boolean sendChargeNumberToNetwork;

    /**
     * Ruft den Wert der phoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Legt den Wert der phoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber(String value) {
        this.phoneNumber = value;
    }

    /**
     * Ruft den Wert der useChargeNumberForEnhancedTranslations-Eigenschaft ab.
     * 
     */
    public boolean isUseChargeNumberForEnhancedTranslations() {
        return useChargeNumberForEnhancedTranslations;
    }

    /**
     * Legt den Wert der useChargeNumberForEnhancedTranslations-Eigenschaft fest.
     * 
     */
    public void setUseChargeNumberForEnhancedTranslations(boolean value) {
        this.useChargeNumberForEnhancedTranslations = value;
    }

    /**
     * Ruft den Wert der sendChargeNumberToNetwork-Eigenschaft ab.
     * 
     */
    public boolean isSendChargeNumberToNetwork() {
        return sendChargeNumberToNetwork;
    }

    /**
     * Legt den Wert der sendChargeNumberToNetwork-Eigenschaft fest.
     * 
     */
    public void setSendChargeNumberToNetwork(boolean value) {
        this.sendChargeNumberToNetwork = value;
    }

}
