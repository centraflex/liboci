//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to add a system voice portal instance.
 *         The response is either SuccessResponse or ErrorResponse.
 * 
 *         The following elements are only used in IMS mode:
 *         publicUserIdentity, ignored in standalone mode if provided.
 *         
 *         The following elements are only used in AS mode and ignored in XS data mode:
 *             networkClassOfService        
 *       
 * 
 * <p>Java-Klasse für SystemSystemVoicePortalAddRequest21sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSystemVoicePortalAddRequest21sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="systemVoicePortalId" type="{}UserId"/>
 *         <element name="name" type="{}SystemServiceName"/>
 *         <element name="callingLineIdName" type="{}SystemServiceCallingLineIdName"/>
 *         <element name="language" type="{}Language"/>
 *         <element name="timeZone" type="{}TimeZone"/>
 *         <element name="phoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="publicUserIdentity" type="{}SIPURI" minOccurs="0"/>
 *         <element name="networkVoicePortalNumber" type="{}DN" minOccurs="0"/>
 *         <element name="allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useVoicePortalWizard" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="useVoicePortalDefaultGreeting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="voicePortalGreetingFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *         <element name="useVoiceMessagingDefaultGreeting" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="voiceMessagingGreetingFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *         <element name="expressMode" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSystemVoicePortalAddRequest21sp1", propOrder = {
    "systemVoicePortalId",
    "name",
    "callingLineIdName",
    "language",
    "timeZone",
    "phoneNumber",
    "publicUserIdentity",
    "networkVoicePortalNumber",
    "allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin",
    "useVoicePortalWizard",
    "useVoicePortalDefaultGreeting",
    "voicePortalGreetingFile",
    "useVoiceMessagingDefaultGreeting",
    "voiceMessagingGreetingFile",
    "expressMode",
    "networkClassOfService"
})
public class SystemSystemVoicePortalAddRequest21Sp1
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String systemVoicePortalId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String language;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeZone;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String phoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String publicUserIdentity;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkVoicePortalNumber;
    protected boolean allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin;
    protected boolean useVoicePortalWizard;
    protected boolean useVoicePortalDefaultGreeting;
    protected LabeledMediaFileResource voicePortalGreetingFile;
    protected boolean useVoiceMessagingDefaultGreeting;
    protected LabeledMediaFileResource voiceMessagingGreetingFile;
    protected boolean expressMode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;

    /**
     * Ruft den Wert der systemVoicePortalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemVoicePortalId() {
        return systemVoicePortalId;
    }

    /**
     * Legt den Wert der systemVoicePortalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemVoicePortalId(String value) {
        this.systemVoicePortalId = value;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der callingLineIdName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdName() {
        return callingLineIdName;
    }

    /**
     * Legt den Wert der callingLineIdName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdName(String value) {
        this.callingLineIdName = value;
    }

    /**
     * Ruft den Wert der language-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Legt den Wert der language-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Ruft den Wert der timeZone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * Legt den Wert der timeZone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeZone(String value) {
        this.timeZone = value;
    }

    /**
     * Ruft den Wert der phoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Legt den Wert der phoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber(String value) {
        this.phoneNumber = value;
    }

    /**
     * Ruft den Wert der publicUserIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPublicUserIdentity() {
        return publicUserIdentity;
    }

    /**
     * Legt den Wert der publicUserIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPublicUserIdentity(String value) {
        this.publicUserIdentity = value;
    }

    /**
     * Ruft den Wert der networkVoicePortalNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkVoicePortalNumber() {
        return networkVoicePortalNumber;
    }

    /**
     * Legt den Wert der networkVoicePortalNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkVoicePortalNumber(String value) {
        this.networkVoicePortalNumber = value;
    }

    /**
     * Ruft den Wert der allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin-Eigenschaft ab.
     * 
     */
    public boolean isAllowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin() {
        return allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin;
    }

    /**
     * Legt den Wert der allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin-Eigenschaft fest.
     * 
     */
    public void setAllowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin(boolean value) {
        this.allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin = value;
    }

    /**
     * Ruft den Wert der useVoicePortalWizard-Eigenschaft ab.
     * 
     */
    public boolean isUseVoicePortalWizard() {
        return useVoicePortalWizard;
    }

    /**
     * Legt den Wert der useVoicePortalWizard-Eigenschaft fest.
     * 
     */
    public void setUseVoicePortalWizard(boolean value) {
        this.useVoicePortalWizard = value;
    }

    /**
     * Ruft den Wert der useVoicePortalDefaultGreeting-Eigenschaft ab.
     * 
     */
    public boolean isUseVoicePortalDefaultGreeting() {
        return useVoicePortalDefaultGreeting;
    }

    /**
     * Legt den Wert der useVoicePortalDefaultGreeting-Eigenschaft fest.
     * 
     */
    public void setUseVoicePortalDefaultGreeting(boolean value) {
        this.useVoicePortalDefaultGreeting = value;
    }

    /**
     * Ruft den Wert der voicePortalGreetingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public LabeledMediaFileResource getVoicePortalGreetingFile() {
        return voicePortalGreetingFile;
    }

    /**
     * Legt den Wert der voicePortalGreetingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public void setVoicePortalGreetingFile(LabeledMediaFileResource value) {
        this.voicePortalGreetingFile = value;
    }

    /**
     * Ruft den Wert der useVoiceMessagingDefaultGreeting-Eigenschaft ab.
     * 
     */
    public boolean isUseVoiceMessagingDefaultGreeting() {
        return useVoiceMessagingDefaultGreeting;
    }

    /**
     * Legt den Wert der useVoiceMessagingDefaultGreeting-Eigenschaft fest.
     * 
     */
    public void setUseVoiceMessagingDefaultGreeting(boolean value) {
        this.useVoiceMessagingDefaultGreeting = value;
    }

    /**
     * Ruft den Wert der voiceMessagingGreetingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public LabeledMediaFileResource getVoiceMessagingGreetingFile() {
        return voiceMessagingGreetingFile;
    }

    /**
     * Legt den Wert der voiceMessagingGreetingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public void setVoiceMessagingGreetingFile(LabeledMediaFileResource value) {
        this.voiceMessagingGreetingFile = value;
    }

    /**
     * Ruft den Wert der expressMode-Eigenschaft ab.
     * 
     */
    public boolean isExpressMode() {
        return expressMode;
    }

    /**
     * Legt den Wert der expressMode-Eigenschaft fest.
     * 
     */
    public void setExpressMode(boolean value) {
        this.expressMode = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

}
