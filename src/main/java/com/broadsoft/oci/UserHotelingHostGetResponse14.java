//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import javax.xml.datatype.XMLGregorianCalendar;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserHotelingHostGetRequest14.
 *         Replaced by: UserHotelingHostGetResponse14sp4
 *       
 * 
 * <p>Java-Klasse für UserHotelingHostGetResponse14 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserHotelingHostGetResponse14">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="associationLimitHours" type="{}HotelingAssociationLimitHours"/>
 *         <element name="accessLevel" type="{}HotelingHostAccessLevel"/>
 *         <element name="guestLastName" type="{}LastName" minOccurs="0"/>
 *         <element name="guestFirstName" type="{}FirstName" minOccurs="0"/>
 *         <element name="guestPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="guestExtension" type="{}Extension" minOccurs="0"/>
 *         <element name="guestLocationDialingCode" type="{}LocationDialingCode" minOccurs="0"/>
 *         <element name="guestAssociationDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserHotelingHostGetResponse14", propOrder = {
    "isActive",
    "associationLimitHours",
    "accessLevel",
    "guestLastName",
    "guestFirstName",
    "guestPhoneNumber",
    "guestExtension",
    "guestLocationDialingCode",
    "guestAssociationDateTime"
})
public class UserHotelingHostGetResponse14
    extends OCIDataResponse
{

    protected boolean isActive;
    protected int associationLimitHours;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected HotelingHostAccessLevel accessLevel;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String guestLastName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String guestFirstName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String guestPhoneNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String guestExtension;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String guestLocationDialingCode;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar guestAssociationDateTime;

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der associationLimitHours-Eigenschaft ab.
     * 
     */
    public int getAssociationLimitHours() {
        return associationLimitHours;
    }

    /**
     * Legt den Wert der associationLimitHours-Eigenschaft fest.
     * 
     */
    public void setAssociationLimitHours(int value) {
        this.associationLimitHours = value;
    }

    /**
     * Ruft den Wert der accessLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HotelingHostAccessLevel }
     *     
     */
    public HotelingHostAccessLevel getAccessLevel() {
        return accessLevel;
    }

    /**
     * Legt den Wert der accessLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelingHostAccessLevel }
     *     
     */
    public void setAccessLevel(HotelingHostAccessLevel value) {
        this.accessLevel = value;
    }

    /**
     * Ruft den Wert der guestLastName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuestLastName() {
        return guestLastName;
    }

    /**
     * Legt den Wert der guestLastName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuestLastName(String value) {
        this.guestLastName = value;
    }

    /**
     * Ruft den Wert der guestFirstName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuestFirstName() {
        return guestFirstName;
    }

    /**
     * Legt den Wert der guestFirstName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuestFirstName(String value) {
        this.guestFirstName = value;
    }

    /**
     * Ruft den Wert der guestPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuestPhoneNumber() {
        return guestPhoneNumber;
    }

    /**
     * Legt den Wert der guestPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuestPhoneNumber(String value) {
        this.guestPhoneNumber = value;
    }

    /**
     * Ruft den Wert der guestExtension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuestExtension() {
        return guestExtension;
    }

    /**
     * Legt den Wert der guestExtension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuestExtension(String value) {
        this.guestExtension = value;
    }

    /**
     * Ruft den Wert der guestLocationDialingCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuestLocationDialingCode() {
        return guestLocationDialingCode;
    }

    /**
     * Legt den Wert der guestLocationDialingCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuestLocationDialingCode(String value) {
        this.guestLocationDialingCode = value;
    }

    /**
     * Ruft den Wert der guestAssociationDateTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGuestAssociationDateTime() {
        return guestAssociationDateTime;
    }

    /**
     * Legt den Wert der guestAssociationDateTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGuestAssociationDateTime(XMLGregorianCalendar value) {
        this.guestAssociationDateTime = value;
    }

}
