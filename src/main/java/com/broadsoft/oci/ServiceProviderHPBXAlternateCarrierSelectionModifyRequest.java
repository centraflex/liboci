//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify the Alternate Carrier Selection parameters.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *     
 * 
 * <p>Java-Klasse für ServiceProviderHPBXAlternateCarrierSelectionModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderHPBXAlternateCarrierSelectionModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="processCbcCarrierSelection" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="preselectedLocalCarrier" type="{}HPBXAlternateCarrierName" minOccurs="0"/>
 *         <element name="preselectedDistantCarrier" type="{}HPBXAlternateCarrierName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderHPBXAlternateCarrierSelectionModifyRequest", propOrder = {
    "serviceProviderId",
    "processCbcCarrierSelection",
    "preselectedLocalCarrier",
    "preselectedDistantCarrier"
})
public class ServiceProviderHPBXAlternateCarrierSelectionModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected Boolean processCbcCarrierSelection;
    @XmlElementRef(name = "preselectedLocalCarrier", type = JAXBElement.class, required = false)
    protected JAXBElement<String> preselectedLocalCarrier;
    @XmlElementRef(name = "preselectedDistantCarrier", type = JAXBElement.class, required = false)
    protected JAXBElement<String> preselectedDistantCarrier;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der processCbcCarrierSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProcessCbcCarrierSelection() {
        return processCbcCarrierSelection;
    }

    /**
     * Legt den Wert der processCbcCarrierSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProcessCbcCarrierSelection(Boolean value) {
        this.processCbcCarrierSelection = value;
    }

    /**
     * Ruft den Wert der preselectedLocalCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPreselectedLocalCarrier() {
        return preselectedLocalCarrier;
    }

    /**
     * Legt den Wert der preselectedLocalCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPreselectedLocalCarrier(JAXBElement<String> value) {
        this.preselectedLocalCarrier = value;
    }

    /**
     * Ruft den Wert der preselectedDistantCarrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPreselectedDistantCarrier() {
        return preselectedDistantCarrier;
    }

    /**
     * Legt den Wert der preselectedDistantCarrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPreselectedDistantCarrier(JAXBElement<String> value) {
        this.preselectedDistantCarrier = value;
    }

}
