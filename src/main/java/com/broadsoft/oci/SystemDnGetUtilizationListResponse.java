//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemDnUtilizationGetListRequest.
 *         The table columns are: "Service Provider Id", "Phone Numbers", "Assigned to Groups",
 *         "Percentage Assigned", "Is Enterprise", "Activated on Groups", "Reseller Id".
 * 
 * 	    The following columns are only returned in AS data mode:       
 *           "Reseller Id"        
 *       
 * 
 * <p>Java-Klasse für SystemDnGetUtilizationListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDnGetUtilizationListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="dnUtilizationTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDnGetUtilizationListResponse", propOrder = {
    "dnUtilizationTable"
})
public class SystemDnGetUtilizationListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable dnUtilizationTable;

    /**
     * Ruft den Wert der dnUtilizationTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getDnUtilizationTable() {
        return dnUtilizationTable;
    }

    /**
     * Legt den Wert der dnUtilizationTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setDnUtilizationTable(OCITable value) {
        this.dnUtilizationTable = value;
    }

}
