//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *       Modify the DTMF transmission settings.
 *       The response is either SuccessResponse or an ErrorResponse.
 *     
 * 
 * <p>Java-Klasse für SystemDTMFTransmissionModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDTMFTransmissionModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="transmissionMethod" type="{}DTMFTransmissionMethod" minOccurs="0"/>
 *         <element name="signalingContentType" type="{}DtmfTransmissionSignalingContentType" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDTMFTransmissionModifyRequest", propOrder = {
    "transmissionMethod",
    "signalingContentType"
})
public class SystemDTMFTransmissionModifyRequest
    extends OCIRequest
{

    @XmlSchemaType(name = "token")
    protected DTMFTransmissionMethod transmissionMethod;
    @XmlSchemaType(name = "token")
    protected DtmfTransmissionSignalingContentType signalingContentType;

    /**
     * Ruft den Wert der transmissionMethod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DTMFTransmissionMethod }
     *     
     */
    public DTMFTransmissionMethod getTransmissionMethod() {
        return transmissionMethod;
    }

    /**
     * Legt den Wert der transmissionMethod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DTMFTransmissionMethod }
     *     
     */
    public void setTransmissionMethod(DTMFTransmissionMethod value) {
        this.transmissionMethod = value;
    }

    /**
     * Ruft den Wert der signalingContentType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DtmfTransmissionSignalingContentType }
     *     
     */
    public DtmfTransmissionSignalingContentType getSignalingContentType() {
        return signalingContentType;
    }

    /**
     * Legt den Wert der signalingContentType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DtmfTransmissionSignalingContentType }
     *     
     */
    public void setSignalingContentType(DtmfTransmissionSignalingContentType value) {
        this.signalingContentType = value;
    }

}
