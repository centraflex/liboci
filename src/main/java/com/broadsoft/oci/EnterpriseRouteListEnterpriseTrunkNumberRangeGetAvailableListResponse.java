//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to EnterpriseRouteListEnterpriseTrunkNumberRangeGetAvailableListRequest.
 *         Contains a list of number ranges that are assigned to an enterprise and still available for assignment to users within the enterprise.
 *         The column headings are "Number Range Start", "Number Range End" ,"Is Active" and “Extension Length”..
 *       
 * 
 * <p>Java-Klasse für EnterpriseRouteListEnterpriseTrunkNumberRangeGetAvailableListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseRouteListEnterpriseTrunkNumberRangeGetAvailableListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="availableNumberRangeTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseRouteListEnterpriseTrunkNumberRangeGetAvailableListResponse", propOrder = {
    "availableNumberRangeTable"
})
public class EnterpriseRouteListEnterpriseTrunkNumberRangeGetAvailableListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable availableNumberRangeTable;

    /**
     * Ruft den Wert der availableNumberRangeTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAvailableNumberRangeTable() {
        return availableNumberRangeTable;
    }

    /**
     * Legt den Wert der availableNumberRangeTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAvailableNumberRangeTable(OCITable value) {
        this.availableNumberRangeTable = value;
    }

}
