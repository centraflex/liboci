//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a device CPE config file server.
 *         The following elements are only used in AS data mode:
 *           extendedCaptureFileRepositoryName
 *           
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCPEConfigAddFileServerRequest14sp6 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCPEConfigAddFileServerRequest14sp6">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="deviceType" type="{}AccessDeviceType"/>
 *         <element name="fileRepositoryName" type="{}FileRepositoryName"/>
 *         <element name="cpeFileDirectory" type="{}CPEFileDirectory" minOccurs="0"/>
 *         <element name="extendedCaptureFileRepositoryName" type="{}FileRepositoryName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCPEConfigAddFileServerRequest14sp6", propOrder = {
    "deviceType",
    "fileRepositoryName",
    "cpeFileDirectory",
    "extendedCaptureFileRepositoryName"
})
public class SystemCPEConfigAddFileServerRequest14Sp6
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceType;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String fileRepositoryName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String cpeFileDirectory;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String extendedCaptureFileRepositoryName;

    /**
     * Ruft den Wert der deviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Legt den Wert der deviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceType(String value) {
        this.deviceType = value;
    }

    /**
     * Ruft den Wert der fileRepositoryName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileRepositoryName() {
        return fileRepositoryName;
    }

    /**
     * Legt den Wert der fileRepositoryName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileRepositoryName(String value) {
        this.fileRepositoryName = value;
    }

    /**
     * Ruft den Wert der cpeFileDirectory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpeFileDirectory() {
        return cpeFileDirectory;
    }

    /**
     * Legt den Wert der cpeFileDirectory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpeFileDirectory(String value) {
        this.cpeFileDirectory = value;
    }

    /**
     * Ruft den Wert der extendedCaptureFileRepositoryName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtendedCaptureFileRepositoryName() {
        return extendedCaptureFileRepositoryName;
    }

    /**
     * Legt den Wert der extendedCaptureFileRepositoryName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtendedCaptureFileRepositoryName(String value) {
        this.extendedCaptureFileRepositoryName = value;
    }

}
