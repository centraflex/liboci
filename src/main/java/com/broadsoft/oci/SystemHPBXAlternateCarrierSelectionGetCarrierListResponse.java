//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the SystemHPBXAlternateCarrierSelectionGetCarrierListRequest.
 *         Contains a table with column headings: 
 *          "Carrier Name", "Carrier Prefix", "Carrier Domain", "Carrier Type"
 *         The possible values for Carrier Type are "Local And Distant" and "Distant".
 *       
 * 
 * <p>Java-Klasse für SystemHPBXAlternateCarrierSelectionGetCarrierListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemHPBXAlternateCarrierSelectionGetCarrierListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="HPBXAlternateCarriersTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemHPBXAlternateCarrierSelectionGetCarrierListResponse", propOrder = {
    "hpbxAlternateCarriersTable"
})
public class SystemHPBXAlternateCarrierSelectionGetCarrierListResponse
    extends OCIDataResponse
{

    @XmlElement(name = "HPBXAlternateCarriersTable", required = true)
    protected OCITable hpbxAlternateCarriersTable;

    /**
     * Ruft den Wert der hpbxAlternateCarriersTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getHPBXAlternateCarriersTable() {
        return hpbxAlternateCarriersTable;
    }

    /**
     * Legt den Wert der hpbxAlternateCarriersTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setHPBXAlternateCarriersTable(OCITable value) {
        this.hpbxAlternateCarriersTable = value;
    }

}
