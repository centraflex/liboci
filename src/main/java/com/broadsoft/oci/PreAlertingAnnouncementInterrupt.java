//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PreAlertingAnnouncementInterrupt.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="PreAlertingAnnouncementInterrupt">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Not Allowed"/>
 *     <enumeration value="Any Digit"/>
 *     <enumeration value="Digit Sequence"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "PreAlertingAnnouncementInterrupt")
@XmlEnum
public enum PreAlertingAnnouncementInterrupt {

    @XmlEnumValue("Not Allowed")
    NOT_ALLOWED("Not Allowed"),
    @XmlEnumValue("Any Digit")
    ANY_DIGIT("Any Digit"),
    @XmlEnumValue("Digit Sequence")
    DIGIT_SEQUENCE("Digit Sequence");
    private final String value;

    PreAlertingAnnouncementInterrupt(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PreAlertingAnnouncementInterrupt fromValue(String v) {
        for (PreAlertingAnnouncementInterrupt c: PreAlertingAnnouncementInterrupt.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
