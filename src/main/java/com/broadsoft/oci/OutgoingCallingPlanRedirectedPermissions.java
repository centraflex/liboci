//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Outgoing Calling Plan being forwarded/transferred permissions.
 *       
 * 
 * <p>Java-Klasse für OutgoingCallingPlanRedirectedPermissions complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OutgoingCallingPlanRedirectedPermissions">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="outsideGroup" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingCallingPlanRedirectedPermissions", propOrder = {
    "outsideGroup"
})
public class OutgoingCallingPlanRedirectedPermissions {

    protected boolean outsideGroup;

    /**
     * Ruft den Wert der outsideGroup-Eigenschaft ab.
     * 
     */
    public boolean isOutsideGroup() {
        return outsideGroup;
    }

    /**
     * Legt den Wert der outsideGroup-Eigenschaft fest.
     * 
     */
    public void setOutsideGroup(boolean value) {
        this.outsideGroup = value;
    }

}
