//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Adds BroadWorks Mobility IMRN numbers to a geographical pool. It is possible to add either: 
 *         a single number, a list of numbers, or a range of numbers, or any combination thereof.
 *         The response is either a SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobilityGeographicalPoolIMRNAddListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobilityGeographicalPoolIMRNAddListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="geographicalPoolName" type="{}BroadWorksMobilityGeographicalPoolName"/>
 *         <element name="imrnNumber" type="{}DN" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="numberRange" type="{}DNRange" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobilityGeographicalPoolIMRNAddListRequest", propOrder = {
    "geographicalPoolName",
    "imrnNumber",
    "numberRange"
})
public class SystemBroadWorksMobilityGeographicalPoolIMRNAddListRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String geographicalPoolName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> imrnNumber;
    protected List<DNRange> numberRange;

    /**
     * Ruft den Wert der geographicalPoolName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeographicalPoolName() {
        return geographicalPoolName;
    }

    /**
     * Legt den Wert der geographicalPoolName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeographicalPoolName(String value) {
        this.geographicalPoolName = value;
    }

    /**
     * Gets the value of the imrnNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the imrnNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getImrnNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the imrnNumber property.
     */
    public List<String> getImrnNumber() {
        if (imrnNumber == null) {
            imrnNumber = new ArrayList<>();
        }
        return this.imrnNumber;
    }

    /**
     * Gets the value of the numberRange property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the numberRange property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNumberRange().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DNRange }
     * 
     * 
     * @return
     *     The value of the numberRange property.
     */
    public List<DNRange> getNumberRange() {
        if (numberRange == null) {
            numberRange = new ArrayList<>();
        }
        return this.numberRange;
    }

}
