//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the settings for one of the user's BroadWorks Mobility mobile Identities.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         If deleteExistingDevices is set to true, when the devices for the endpoint are changed, devices with no more endpoint will be deleted if the command is executed with the correct privilege.
 * 
 *         The request fails when enableAlerting or timeSchedule or holidaySchedule are included in the request when the use mobile identity call anchoring controls for the user  is disabled.
 * 
 *         The description, enableAlerting, alertAgentCalls, alertClickToDialCalls, alertGroupPagingCalls, useMobilityCallingLineID, enableDiversionInhibitor, requireAnswerConfirmation, broadworksCallControl, devicesToRing, includeSharedCallAppearance, includeBroadworksAnywhere, includeExecutiveAssistance, mobileNumberAlerted, enableCallAnchoring, timeSchedule and holidaySchedule  parameters require an authorization level of User for modification when the BroadWorks Mobility service is on. 
 *         The isPrimary, useSettingLevel, denyCallOrigination, denyCallTerminations, accessDeviceEndpoint, outboundAlternateNumber, enableDirectRouting, markCDRAsEnterpriseGroupCalls and networkTranslationIndex parameters require an authorization level of Group for modification.
 *       
 * 
 * <p>Java-Klasse für UserBroadWorksMobilityMobileIdentityConsolidatedModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserBroadWorksMobilityMobileIdentityConsolidatedModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="mobileNumber" type="{}DN"/>
 *         <element name="deleteExistingDevices" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="description" type="{}BroadWorksMobilityUserMobileIdentityDescription" minOccurs="0"/>
 *         <element name="isPrimary" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableAlerting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="alertAgentCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="alertClickToDialCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="alertGroupPagingCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useMobilityCallingLineID" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableDiversionInhibitor" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="requireAnswerConfirmation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="broadworksCallControl" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useSettingLevel" type="{}BroadWorksMobilityUserSettingLevel" minOccurs="0"/>
 *         <element name="denyCallOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="denyCallTerminations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="devicesToRing" type="{}BroadWorksMobilityPhoneToRing" minOccurs="0"/>
 *         <element name="includeSharedCallAppearance" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeBroadworksAnywhere" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="includeExecutiveAssistant" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="mobileNumbersAlerted" type="{}BroadWorksMobilityAlertingMobileNumberReplacementList" minOccurs="0"/>
 *         <element name="enableCallAnchoring" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="timeSchedule" type="{}ScheduleGlobalKey" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}ScheduleGlobalKey" minOccurs="0"/>
 *         <choice minOccurs="0">
 *           <element name="accessDeviceEndpoint" type="{}ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint"/>
 *           <element name="outboundAlternateNumber" type="{}OutgoingDNorSIPURI"/>
 *         </choice>
 *         <element name="enableDirectRouting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="markCDRAsEnterpriseGroupCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useMobilityConnectedIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="networkTranslationIndex" type="{}NetworkTranslationIndex" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserBroadWorksMobilityMobileIdentityConsolidatedModifyRequest", propOrder = {
    "userId",
    "mobileNumber",
    "deleteExistingDevices",
    "description",
    "isPrimary",
    "enableAlerting",
    "alertAgentCalls",
    "alertClickToDialCalls",
    "alertGroupPagingCalls",
    "useMobilityCallingLineID",
    "enableDiversionInhibitor",
    "requireAnswerConfirmation",
    "broadworksCallControl",
    "useSettingLevel",
    "denyCallOriginations",
    "denyCallTerminations",
    "devicesToRing",
    "includeSharedCallAppearance",
    "includeBroadworksAnywhere",
    "includeExecutiveAssistant",
    "mobileNumbersAlerted",
    "enableCallAnchoring",
    "timeSchedule",
    "holidaySchedule",
    "accessDeviceEndpoint",
    "outboundAlternateNumber",
    "enableDirectRouting",
    "markCDRAsEnterpriseGroupCalls",
    "useMobilityConnectedIdentity",
    "networkTranslationIndex"
})
public class UserBroadWorksMobilityMobileIdentityConsolidatedModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String mobileNumber;
    protected Boolean deleteExistingDevices;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    protected Boolean isPrimary;
    protected Boolean enableAlerting;
    protected Boolean alertAgentCalls;
    protected Boolean alertClickToDialCalls;
    protected Boolean alertGroupPagingCalls;
    protected Boolean useMobilityCallingLineID;
    protected Boolean enableDiversionInhibitor;
    protected Boolean requireAnswerConfirmation;
    protected Boolean broadworksCallControl;
    @XmlSchemaType(name = "token")
    protected BroadWorksMobilityUserSettingLevel useSettingLevel;
    protected Boolean denyCallOriginations;
    protected Boolean denyCallTerminations;
    @XmlSchemaType(name = "token")
    protected BroadWorksMobilityPhoneToRing devicesToRing;
    protected Boolean includeSharedCallAppearance;
    protected Boolean includeBroadworksAnywhere;
    protected Boolean includeExecutiveAssistant;
    @XmlElementRef(name = "mobileNumbersAlerted", type = JAXBElement.class, required = false)
    protected JAXBElement<BroadWorksMobilityAlertingMobileNumberReplacementList> mobileNumbersAlerted;
    protected Boolean enableCallAnchoring;
    @XmlElementRef(name = "timeSchedule", type = JAXBElement.class, required = false)
    protected JAXBElement<ScheduleGlobalKey> timeSchedule;
    @XmlElementRef(name = "holidaySchedule", type = JAXBElement.class, required = false)
    protected JAXBElement<ScheduleGlobalKey> holidaySchedule;
    @XmlElementRef(name = "accessDeviceEndpoint", type = JAXBElement.class, required = false)
    protected JAXBElement<ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint> accessDeviceEndpoint;
    @XmlElementRef(name = "outboundAlternateNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> outboundAlternateNumber;
    protected Boolean enableDirectRouting;
    protected Boolean markCDRAsEnterpriseGroupCalls;
    protected Boolean useMobilityConnectedIdentity;
    @XmlElementRef(name = "networkTranslationIndex", type = JAXBElement.class, required = false)
    protected JAXBElement<String> networkTranslationIndex;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der mobileNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Legt den Wert der mobileNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobileNumber(String value) {
        this.mobileNumber = value;
    }

    /**
     * Ruft den Wert der deleteExistingDevices-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeleteExistingDevices() {
        return deleteExistingDevices;
    }

    /**
     * Legt den Wert der deleteExistingDevices-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeleteExistingDevices(Boolean value) {
        this.deleteExistingDevices = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der isPrimary-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPrimary() {
        return isPrimary;
    }

    /**
     * Legt den Wert der isPrimary-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPrimary(Boolean value) {
        this.isPrimary = value;
    }

    /**
     * Ruft den Wert der enableAlerting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableAlerting() {
        return enableAlerting;
    }

    /**
     * Legt den Wert der enableAlerting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableAlerting(Boolean value) {
        this.enableAlerting = value;
    }

    /**
     * Ruft den Wert der alertAgentCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlertAgentCalls() {
        return alertAgentCalls;
    }

    /**
     * Legt den Wert der alertAgentCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlertAgentCalls(Boolean value) {
        this.alertAgentCalls = value;
    }

    /**
     * Ruft den Wert der alertClickToDialCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlertClickToDialCalls() {
        return alertClickToDialCalls;
    }

    /**
     * Legt den Wert der alertClickToDialCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlertClickToDialCalls(Boolean value) {
        this.alertClickToDialCalls = value;
    }

    /**
     * Ruft den Wert der alertGroupPagingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlertGroupPagingCalls() {
        return alertGroupPagingCalls;
    }

    /**
     * Legt den Wert der alertGroupPagingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlertGroupPagingCalls(Boolean value) {
        this.alertGroupPagingCalls = value;
    }

    /**
     * Ruft den Wert der useMobilityCallingLineID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMobilityCallingLineID() {
        return useMobilityCallingLineID;
    }

    /**
     * Legt den Wert der useMobilityCallingLineID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMobilityCallingLineID(Boolean value) {
        this.useMobilityCallingLineID = value;
    }

    /**
     * Ruft den Wert der enableDiversionInhibitor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableDiversionInhibitor() {
        return enableDiversionInhibitor;
    }

    /**
     * Legt den Wert der enableDiversionInhibitor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableDiversionInhibitor(Boolean value) {
        this.enableDiversionInhibitor = value;
    }

    /**
     * Ruft den Wert der requireAnswerConfirmation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequireAnswerConfirmation() {
        return requireAnswerConfirmation;
    }

    /**
     * Legt den Wert der requireAnswerConfirmation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequireAnswerConfirmation(Boolean value) {
        this.requireAnswerConfirmation = value;
    }

    /**
     * Ruft den Wert der broadworksCallControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBroadworksCallControl() {
        return broadworksCallControl;
    }

    /**
     * Legt den Wert der broadworksCallControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBroadworksCallControl(Boolean value) {
        this.broadworksCallControl = value;
    }

    /**
     * Ruft den Wert der useSettingLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksMobilityUserSettingLevel }
     *     
     */
    public BroadWorksMobilityUserSettingLevel getUseSettingLevel() {
        return useSettingLevel;
    }

    /**
     * Legt den Wert der useSettingLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksMobilityUserSettingLevel }
     *     
     */
    public void setUseSettingLevel(BroadWorksMobilityUserSettingLevel value) {
        this.useSettingLevel = value;
    }

    /**
     * Ruft den Wert der denyCallOriginations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDenyCallOriginations() {
        return denyCallOriginations;
    }

    /**
     * Legt den Wert der denyCallOriginations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDenyCallOriginations(Boolean value) {
        this.denyCallOriginations = value;
    }

    /**
     * Ruft den Wert der denyCallTerminations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDenyCallTerminations() {
        return denyCallTerminations;
    }

    /**
     * Legt den Wert der denyCallTerminations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDenyCallTerminations(Boolean value) {
        this.denyCallTerminations = value;
    }

    /**
     * Ruft den Wert der devicesToRing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksMobilityPhoneToRing }
     *     
     */
    public BroadWorksMobilityPhoneToRing getDevicesToRing() {
        return devicesToRing;
    }

    /**
     * Legt den Wert der devicesToRing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksMobilityPhoneToRing }
     *     
     */
    public void setDevicesToRing(BroadWorksMobilityPhoneToRing value) {
        this.devicesToRing = value;
    }

    /**
     * Ruft den Wert der includeSharedCallAppearance-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeSharedCallAppearance() {
        return includeSharedCallAppearance;
    }

    /**
     * Legt den Wert der includeSharedCallAppearance-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeSharedCallAppearance(Boolean value) {
        this.includeSharedCallAppearance = value;
    }

    /**
     * Ruft den Wert der includeBroadworksAnywhere-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeBroadworksAnywhere() {
        return includeBroadworksAnywhere;
    }

    /**
     * Legt den Wert der includeBroadworksAnywhere-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeBroadworksAnywhere(Boolean value) {
        this.includeBroadworksAnywhere = value;
    }

    /**
     * Ruft den Wert der includeExecutiveAssistant-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeExecutiveAssistant() {
        return includeExecutiveAssistant;
    }

    /**
     * Legt den Wert der includeExecutiveAssistant-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeExecutiveAssistant(Boolean value) {
        this.includeExecutiveAssistant = value;
    }

    /**
     * Ruft den Wert der mobileNumbersAlerted-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BroadWorksMobilityAlertingMobileNumberReplacementList }{@code >}
     *     
     */
    public JAXBElement<BroadWorksMobilityAlertingMobileNumberReplacementList> getMobileNumbersAlerted() {
        return mobileNumbersAlerted;
    }

    /**
     * Legt den Wert der mobileNumbersAlerted-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BroadWorksMobilityAlertingMobileNumberReplacementList }{@code >}
     *     
     */
    public void setMobileNumbersAlerted(JAXBElement<BroadWorksMobilityAlertingMobileNumberReplacementList> value) {
        this.mobileNumbersAlerted = value;
    }

    /**
     * Ruft den Wert der enableCallAnchoring-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCallAnchoring() {
        return enableCallAnchoring;
    }

    /**
     * Legt den Wert der enableCallAnchoring-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCallAnchoring(Boolean value) {
        this.enableCallAnchoring = value;
    }

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ScheduleGlobalKey }{@code >}
     *     
     */
    public JAXBElement<ScheduleGlobalKey> getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ScheduleGlobalKey }{@code >}
     *     
     */
    public void setTimeSchedule(JAXBElement<ScheduleGlobalKey> value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ScheduleGlobalKey }{@code >}
     *     
     */
    public JAXBElement<ScheduleGlobalKey> getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ScheduleGlobalKey }{@code >}
     *     
     */
    public void setHolidaySchedule(JAXBElement<ScheduleGlobalKey> value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der accessDeviceEndpoint-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint }{@code >}
     *     
     */
    public JAXBElement<ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint> getAccessDeviceEndpoint() {
        return accessDeviceEndpoint;
    }

    /**
     * Legt den Wert der accessDeviceEndpoint-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint }{@code >}
     *     
     */
    public void setAccessDeviceEndpoint(JAXBElement<ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint> value) {
        this.accessDeviceEndpoint = value;
    }

    /**
     * Ruft den Wert der outboundAlternateNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOutboundAlternateNumber() {
        return outboundAlternateNumber;
    }

    /**
     * Legt den Wert der outboundAlternateNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOutboundAlternateNumber(JAXBElement<String> value) {
        this.outboundAlternateNumber = value;
    }

    /**
     * Ruft den Wert der enableDirectRouting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableDirectRouting() {
        return enableDirectRouting;
    }

    /**
     * Legt den Wert der enableDirectRouting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableDirectRouting(Boolean value) {
        this.enableDirectRouting = value;
    }

    /**
     * Ruft den Wert der markCDRAsEnterpriseGroupCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMarkCDRAsEnterpriseGroupCalls() {
        return markCDRAsEnterpriseGroupCalls;
    }

    /**
     * Legt den Wert der markCDRAsEnterpriseGroupCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMarkCDRAsEnterpriseGroupCalls(Boolean value) {
        this.markCDRAsEnterpriseGroupCalls = value;
    }

    /**
     * Ruft den Wert der useMobilityConnectedIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseMobilityConnectedIdentity() {
        return useMobilityConnectedIdentity;
    }

    /**
     * Legt den Wert der useMobilityConnectedIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseMobilityConnectedIdentity(Boolean value) {
        this.useMobilityConnectedIdentity = value;
    }

    /**
     * Ruft den Wert der networkTranslationIndex-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNetworkTranslationIndex() {
        return networkTranslationIndex;
    }

    /**
     * Legt den Wert der networkTranslationIndex-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNetworkTranslationIndex(JAXBElement<String> value) {
        this.networkTranslationIndex = value;
    }

}
