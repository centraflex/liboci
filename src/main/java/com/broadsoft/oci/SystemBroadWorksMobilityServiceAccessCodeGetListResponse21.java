//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemBroadWorksMobilityServiceAccessCodeGetListRequest21.
 *         Contains a table with column headings: "Country Code", "Service Access Code", "Description"
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobilityServiceAccessCodeGetListResponse21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobilityServiceAccessCodeGetListResponse21">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceAccessCodeTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobilityServiceAccessCodeGetListResponse21", propOrder = {
    "serviceAccessCodeTable"
})
public class SystemBroadWorksMobilityServiceAccessCodeGetListResponse21
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable serviceAccessCodeTable;

    /**
     * Ruft den Wert der serviceAccessCodeTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getServiceAccessCodeTable() {
        return serviceAccessCodeTable;
    }

    /**
     * Legt den Wert der serviceAccessCodeTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setServiceAccessCodeTable(OCITable value) {
        this.serviceAccessCodeTable = value;
    }

}
