//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The voice portal play greeting menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für PlayGreetingMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="PlayGreetingMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="skipBackward" type="{}DigitAny" minOccurs="0"/>
 *         <element name="pauseOrResume" type="{}DigitAny" minOccurs="0"/>
 *         <element name="skipForward" type="{}DigitAny" minOccurs="0"/>
 *         <element name="jumpToBegin" type="{}DigitAny" minOccurs="0"/>
 *         <element name="jumpToEnd" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlayGreetingMenuKeysModifyEntry", propOrder = {
    "skipBackward",
    "pauseOrResume",
    "skipForward",
    "jumpToBegin",
    "jumpToEnd"
})
public class PlayGreetingMenuKeysModifyEntry {

    @XmlElementRef(name = "skipBackward", type = JAXBElement.class, required = false)
    protected JAXBElement<String> skipBackward;
    @XmlElementRef(name = "pauseOrResume", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pauseOrResume;
    @XmlElementRef(name = "skipForward", type = JAXBElement.class, required = false)
    protected JAXBElement<String> skipForward;
    @XmlElementRef(name = "jumpToBegin", type = JAXBElement.class, required = false)
    protected JAXBElement<String> jumpToBegin;
    @XmlElementRef(name = "jumpToEnd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> jumpToEnd;

    /**
     * Ruft den Wert der skipBackward-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSkipBackward() {
        return skipBackward;
    }

    /**
     * Legt den Wert der skipBackward-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSkipBackward(JAXBElement<String> value) {
        this.skipBackward = value;
    }

    /**
     * Ruft den Wert der pauseOrResume-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPauseOrResume() {
        return pauseOrResume;
    }

    /**
     * Legt den Wert der pauseOrResume-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPauseOrResume(JAXBElement<String> value) {
        this.pauseOrResume = value;
    }

    /**
     * Ruft den Wert der skipForward-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSkipForward() {
        return skipForward;
    }

    /**
     * Legt den Wert der skipForward-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSkipForward(JAXBElement<String> value) {
        this.skipForward = value;
    }

    /**
     * Ruft den Wert der jumpToBegin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getJumpToBegin() {
        return jumpToBegin;
    }

    /**
     * Legt den Wert der jumpToBegin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setJumpToBegin(JAXBElement<String> value) {
        this.jumpToBegin = value;
    }

    /**
     * Ruft den Wert der jumpToEnd-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getJumpToEnd() {
        return jumpToEnd;
    }

    /**
     * Legt den Wert der jumpToEnd-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setJumpToEnd(JAXBElement<String> value) {
        this.jumpToEnd = value;
    }

}
