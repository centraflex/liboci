//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupServiceGetAuthorizedListRequest.
 *       
 * 
 * <p>Java-Klasse für GroupServiceGetAuthorizedListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupServiceGetAuthorizedListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="servicePackName" type="{}ServicePackName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="groupServiceName" type="{}GroupService" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="userServiceName" type="{}UserService" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupServiceGetAuthorizedListResponse", propOrder = {
    "servicePackName",
    "groupServiceName",
    "userServiceName"
})
public class GroupServiceGetAuthorizedListResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> servicePackName;
    @XmlSchemaType(name = "token")
    protected List<GroupService> groupServiceName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> userServiceName;

    /**
     * Gets the value of the servicePackName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the servicePackName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicePackName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the servicePackName property.
     */
    public List<String> getServicePackName() {
        if (servicePackName == null) {
            servicePackName = new ArrayList<>();
        }
        return this.servicePackName;
    }

    /**
     * Gets the value of the groupServiceName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the groupServiceName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupServiceName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupService }
     * 
     * 
     * @return
     *     The value of the groupServiceName property.
     */
    public List<GroupService> getGroupServiceName() {
        if (groupServiceName == null) {
            groupServiceName = new ArrayList<>();
        }
        return this.groupServiceName;
    }

    /**
     * Gets the value of the userServiceName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the userServiceName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserServiceName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the userServiceName property.
     */
    public List<String> getUserServiceName() {
        if (userServiceName == null) {
            userServiceName = new ArrayList<>();
        }
        return this.userServiceName;
    }

}
