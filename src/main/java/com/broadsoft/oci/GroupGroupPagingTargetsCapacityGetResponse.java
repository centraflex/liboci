//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupGroupPagingTargetsCapacityGetRequest.
 *         
 *         Replaced by: GroupGroupPagingTargetsCapacityGetResponse22 in AS data mode.
 *       
 * 
 * <p>Java-Klasse für GroupGroupPagingTargetsCapacityGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupGroupPagingTargetsCapacityGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maximumTargetUsersFromServiceProvider" type="{}GroupPagingMaxTargetCapacity"/>
 *         <element name="maximumTargetUsers" type="{}GroupPagingMaxTargetCapacity"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupGroupPagingTargetsCapacityGetResponse", propOrder = {
    "maximumTargetUsersFromServiceProvider",
    "maximumTargetUsers"
})
public class GroupGroupPagingTargetsCapacityGetResponse
    extends OCIDataResponse
{

    protected int maximumTargetUsersFromServiceProvider;
    protected int maximumTargetUsers;

    /**
     * Ruft den Wert der maximumTargetUsersFromServiceProvider-Eigenschaft ab.
     * 
     */
    public int getMaximumTargetUsersFromServiceProvider() {
        return maximumTargetUsersFromServiceProvider;
    }

    /**
     * Legt den Wert der maximumTargetUsersFromServiceProvider-Eigenschaft fest.
     * 
     */
    public void setMaximumTargetUsersFromServiceProvider(int value) {
        this.maximumTargetUsersFromServiceProvider = value;
    }

    /**
     * Ruft den Wert der maximumTargetUsers-Eigenschaft ab.
     * 
     */
    public int getMaximumTargetUsers() {
        return maximumTargetUsers;
    }

    /**
     * Legt den Wert der maximumTargetUsers-Eigenschaft fest.
     * 
     */
    public void setMaximumTargetUsers(int value) {
        this.maximumTargetUsers = value;
    }

}
