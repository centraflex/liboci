//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterStrandedCallUnavailableProcessingAction.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterStrandedCallUnavailableProcessingAction">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="None"/>
 *     <enumeration value="Busy"/>
 *     <enumeration value="Transfer"/>
 *     <enumeration value="Night Service"/>
 *     <enumeration value="Ringing"/>
 *     <enumeration value="Announcement"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterStrandedCallUnavailableProcessingAction")
@XmlEnum
public enum CallCenterStrandedCallUnavailableProcessingAction {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Busy")
    BUSY("Busy"),
    @XmlEnumValue("Transfer")
    TRANSFER("Transfer"),
    @XmlEnumValue("Night Service")
    NIGHT_SERVICE("Night Service"),
    @XmlEnumValue("Ringing")
    RINGING("Ringing"),
    @XmlEnumValue("Announcement")
    ANNOUNCEMENT("Announcement");
    private final String value;

    CallCenterStrandedCallUnavailableProcessingAction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterStrandedCallUnavailableProcessingAction fromValue(String v) {
        for (CallCenterStrandedCallUnavailableProcessingAction c: CallCenterStrandedCallUnavailableProcessingAction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
