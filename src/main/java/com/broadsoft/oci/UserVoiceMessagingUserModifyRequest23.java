//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the user's voice messaging advanced voice management service setting.
 *         Modify a voice mail distribution list for a users voice message.
 *         Modify the user's voice messaging greeting.  
 *         Modify the user's voice messaging outgoing message waiting indicator service setting.
 *         Modify the user's voice messaging voice portal settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserVoiceMessagingUserModifyRequest23 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserVoiceMessagingUserModifyRequest23">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="mailServerSelection" type="{}VoiceMessagingUserMailServerSelection" minOccurs="0"/>
 *         <element name="groupMailServerEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="groupMailServerUserId" type="{}VoiceMessagingMailServerUserId" minOccurs="0"/>
 *         <element name="groupMailServerPassword" type="{}Password" minOccurs="0"/>
 *         <choice minOccurs="0">
 *           <element name="useGroupDefaultMailServerFullMailboxLimit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *           <element name="groupMailServerFullMailboxLimit" type="{}VoiceMessagingMailboxLengthMinutes"/>
 *         </choice>
 *         <element name="personalMailServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="personalMailServerProtocol" type="{}VoiceMessagingMailServerProtocol" minOccurs="0"/>
 *         <element name="personalMailServerRealDeleteForImap" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="personalMailServerEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="personalMailServerUserId" type="{}VoiceMessagingMailServerUserId" minOccurs="0"/>
 *         <element name="personalMailServerPassword" type="{}Password" minOccurs="0"/>
 *         <element name="voiceMessagingDistributionList" type="{}VoiceMessagingDistributionListModify" maxOccurs="15" minOccurs="0"/>
 *         <element name="busyAnnouncementSelection" type="{}AnnouncementSelection" minOccurs="0"/>
 *         <element name="busyPersonalAudioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="busyPersonalVideoFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="noAnswerAnnouncementSelection" type="{}VoiceMessagingNoAnswerGreetingSelection" minOccurs="0"/>
 *         <element name="noAnswerPersonalAudioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="noAnswerPersonalVideoFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="noAnswerAlternateGreeting01" type="{}VoiceMessagingAlternateNoAnswerGreetingModify20" minOccurs="0"/>
 *         <element name="noAnswerAlternateGreeting02" type="{}VoiceMessagingAlternateNoAnswerGreetingModify20" minOccurs="0"/>
 *         <element name="noAnswerAlternateGreeting03" type="{}VoiceMessagingAlternateNoAnswerGreetingModify20" minOccurs="0"/>
 *         <element name="extendedAwayEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="extendedAwayDisableMessageDeposit" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="extendedAwayAudioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="extendedAwayVideoFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="noAnswerNumberOfRings" type="{}VoiceMessagingNumberOfRings" minOccurs="0"/>
 *         <element name="disableMessageDeposit" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disableMessageDepositAction" type="{}VoiceMessagingDisableMessageDepositSelection" minOccurs="0"/>
 *         <element name="greetingOnlyForwardDestination" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *         <element name="outgoingSMDIMWIisActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="outgoingSMDIMWIPhoneNumberList" type="{}ReplacementOutgoingDNList" minOccurs="0"/>
 *         <element name="voiceManagementisActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="processing" type="{}VoiceMessagingMessageProcessing" minOccurs="0"/>
 *         <element name="voiceMessageDeliveryEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="usePhoneMessageWaitingIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="sendVoiceMessageNotifyEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="voiceMessageNotifyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="sendCarbonCopyVoiceMessage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="voiceMessageCarbonCopyEmailAddress" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="transferOnZeroToPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="transferPhoneNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="alwaysRedirectToVoiceMail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="busyRedirectToVoiceMail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="noAnswerRedirectToVoiceMail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="outOfPrimaryZoneRedirectToVoiceMail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="usePersonalizedName" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="voicePortalAutoLogin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="personalizedNameAudioFile" type="{}AnnouncementFileLevelKey" minOccurs="0"/>
 *         <element name="userMessagingAliasList" type="{}VoiceMessagingAliasReplacementList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserVoiceMessagingUserModifyRequest23", propOrder = {
    "userId",
    "mailServerSelection",
    "groupMailServerEmailAddress",
    "groupMailServerUserId",
    "groupMailServerPassword",
    "useGroupDefaultMailServerFullMailboxLimit",
    "groupMailServerFullMailboxLimit",
    "personalMailServerNetAddress",
    "personalMailServerProtocol",
    "personalMailServerRealDeleteForImap",
    "personalMailServerEmailAddress",
    "personalMailServerUserId",
    "personalMailServerPassword",
    "voiceMessagingDistributionList",
    "busyAnnouncementSelection",
    "busyPersonalAudioFile",
    "busyPersonalVideoFile",
    "noAnswerAnnouncementSelection",
    "noAnswerPersonalAudioFile",
    "noAnswerPersonalVideoFile",
    "noAnswerAlternateGreeting01",
    "noAnswerAlternateGreeting02",
    "noAnswerAlternateGreeting03",
    "extendedAwayEnabled",
    "extendedAwayDisableMessageDeposit",
    "extendedAwayAudioFile",
    "extendedAwayVideoFile",
    "noAnswerNumberOfRings",
    "disableMessageDeposit",
    "disableMessageDepositAction",
    "greetingOnlyForwardDestination",
    "outgoingSMDIMWIisActive",
    "outgoingSMDIMWIPhoneNumberList",
    "voiceManagementisActive",
    "processing",
    "voiceMessageDeliveryEmailAddress",
    "usePhoneMessageWaitingIndicator",
    "sendVoiceMessageNotifyEmail",
    "voiceMessageNotifyEmailAddress",
    "sendCarbonCopyVoiceMessage",
    "voiceMessageCarbonCopyEmailAddress",
    "transferOnZeroToPhoneNumber",
    "transferPhoneNumber",
    "alwaysRedirectToVoiceMail",
    "busyRedirectToVoiceMail",
    "noAnswerRedirectToVoiceMail",
    "outOfPrimaryZoneRedirectToVoiceMail",
    "usePersonalizedName",
    "voicePortalAutoLogin",
    "personalizedNameAudioFile",
    "userMessagingAliasList"
})
public class UserVoiceMessagingUserModifyRequest23
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingUserMailServerSelection mailServerSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupMailServerEmailAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupMailServerUserId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupMailServerPassword;
    protected Boolean useGroupDefaultMailServerFullMailboxLimit;
    protected Integer groupMailServerFullMailboxLimit;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String personalMailServerNetAddress;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingMailServerProtocol personalMailServerProtocol;
    protected Boolean personalMailServerRealDeleteForImap;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String personalMailServerEmailAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String personalMailServerUserId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String personalMailServerPassword;
    protected List<VoiceMessagingDistributionListModify> voiceMessagingDistributionList;
    @XmlSchemaType(name = "token")
    protected AnnouncementSelection busyAnnouncementSelection;
    @XmlElementRef(name = "busyPersonalAudioFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> busyPersonalAudioFile;
    @XmlElementRef(name = "busyPersonalVideoFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> busyPersonalVideoFile;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingNoAnswerGreetingSelection noAnswerAnnouncementSelection;
    @XmlElementRef(name = "noAnswerPersonalAudioFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> noAnswerPersonalAudioFile;
    @XmlElementRef(name = "noAnswerPersonalVideoFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> noAnswerPersonalVideoFile;
    protected VoiceMessagingAlternateNoAnswerGreetingModify20 noAnswerAlternateGreeting01;
    protected VoiceMessagingAlternateNoAnswerGreetingModify20 noAnswerAlternateGreeting02;
    protected VoiceMessagingAlternateNoAnswerGreetingModify20 noAnswerAlternateGreeting03;
    protected Boolean extendedAwayEnabled;
    protected Boolean extendedAwayDisableMessageDeposit;
    @XmlElementRef(name = "extendedAwayAudioFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> extendedAwayAudioFile;
    @XmlElementRef(name = "extendedAwayVideoFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> extendedAwayVideoFile;
    protected Integer noAnswerNumberOfRings;
    protected Boolean disableMessageDeposit;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingDisableMessageDepositSelection disableMessageDepositAction;
    @XmlElementRef(name = "greetingOnlyForwardDestination", type = JAXBElement.class, required = false)
    protected JAXBElement<String> greetingOnlyForwardDestination;
    protected Boolean outgoingSMDIMWIisActive;
    @XmlElementRef(name = "outgoingSMDIMWIPhoneNumberList", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementOutgoingDNList> outgoingSMDIMWIPhoneNumberList;
    protected Boolean voiceManagementisActive;
    @XmlSchemaType(name = "token")
    protected VoiceMessagingMessageProcessing processing;
    @XmlElementRef(name = "voiceMessageDeliveryEmailAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> voiceMessageDeliveryEmailAddress;
    protected Boolean usePhoneMessageWaitingIndicator;
    protected Boolean sendVoiceMessageNotifyEmail;
    @XmlElementRef(name = "voiceMessageNotifyEmailAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> voiceMessageNotifyEmailAddress;
    protected Boolean sendCarbonCopyVoiceMessage;
    @XmlElementRef(name = "voiceMessageCarbonCopyEmailAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> voiceMessageCarbonCopyEmailAddress;
    protected Boolean transferOnZeroToPhoneNumber;
    @XmlElementRef(name = "transferPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transferPhoneNumber;
    protected Boolean alwaysRedirectToVoiceMail;
    protected Boolean busyRedirectToVoiceMail;
    protected Boolean noAnswerRedirectToVoiceMail;
    protected Boolean outOfPrimaryZoneRedirectToVoiceMail;
    protected Boolean usePersonalizedName;
    protected Boolean voicePortalAutoLogin;
    @XmlElementRef(name = "personalizedNameAudioFile", type = JAXBElement.class, required = false)
    protected JAXBElement<AnnouncementFileLevelKey> personalizedNameAudioFile;
    @XmlElementRef(name = "userMessagingAliasList", type = JAXBElement.class, required = false)
    protected JAXBElement<VoiceMessagingAliasReplacementList> userMessagingAliasList;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der mailServerSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingUserMailServerSelection }
     *     
     */
    public VoiceMessagingUserMailServerSelection getMailServerSelection() {
        return mailServerSelection;
    }

    /**
     * Legt den Wert der mailServerSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingUserMailServerSelection }
     *     
     */
    public void setMailServerSelection(VoiceMessagingUserMailServerSelection value) {
        this.mailServerSelection = value;
    }

    /**
     * Ruft den Wert der groupMailServerEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupMailServerEmailAddress() {
        return groupMailServerEmailAddress;
    }

    /**
     * Legt den Wert der groupMailServerEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupMailServerEmailAddress(String value) {
        this.groupMailServerEmailAddress = value;
    }

    /**
     * Ruft den Wert der groupMailServerUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupMailServerUserId() {
        return groupMailServerUserId;
    }

    /**
     * Legt den Wert der groupMailServerUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupMailServerUserId(String value) {
        this.groupMailServerUserId = value;
    }

    /**
     * Ruft den Wert der groupMailServerPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupMailServerPassword() {
        return groupMailServerPassword;
    }

    /**
     * Legt den Wert der groupMailServerPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupMailServerPassword(String value) {
        this.groupMailServerPassword = value;
    }

    /**
     * Ruft den Wert der useGroupDefaultMailServerFullMailboxLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseGroupDefaultMailServerFullMailboxLimit() {
        return useGroupDefaultMailServerFullMailboxLimit;
    }

    /**
     * Legt den Wert der useGroupDefaultMailServerFullMailboxLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseGroupDefaultMailServerFullMailboxLimit(Boolean value) {
        this.useGroupDefaultMailServerFullMailboxLimit = value;
    }

    /**
     * Ruft den Wert der groupMailServerFullMailboxLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGroupMailServerFullMailboxLimit() {
        return groupMailServerFullMailboxLimit;
    }

    /**
     * Legt den Wert der groupMailServerFullMailboxLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGroupMailServerFullMailboxLimit(Integer value) {
        this.groupMailServerFullMailboxLimit = value;
    }

    /**
     * Ruft den Wert der personalMailServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalMailServerNetAddress() {
        return personalMailServerNetAddress;
    }

    /**
     * Legt den Wert der personalMailServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalMailServerNetAddress(String value) {
        this.personalMailServerNetAddress = value;
    }

    /**
     * Ruft den Wert der personalMailServerProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingMailServerProtocol }
     *     
     */
    public VoiceMessagingMailServerProtocol getPersonalMailServerProtocol() {
        return personalMailServerProtocol;
    }

    /**
     * Legt den Wert der personalMailServerProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingMailServerProtocol }
     *     
     */
    public void setPersonalMailServerProtocol(VoiceMessagingMailServerProtocol value) {
        this.personalMailServerProtocol = value;
    }

    /**
     * Ruft den Wert der personalMailServerRealDeleteForImap-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPersonalMailServerRealDeleteForImap() {
        return personalMailServerRealDeleteForImap;
    }

    /**
     * Legt den Wert der personalMailServerRealDeleteForImap-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPersonalMailServerRealDeleteForImap(Boolean value) {
        this.personalMailServerRealDeleteForImap = value;
    }

    /**
     * Ruft den Wert der personalMailServerEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalMailServerEmailAddress() {
        return personalMailServerEmailAddress;
    }

    /**
     * Legt den Wert der personalMailServerEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalMailServerEmailAddress(String value) {
        this.personalMailServerEmailAddress = value;
    }

    /**
     * Ruft den Wert der personalMailServerUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalMailServerUserId() {
        return personalMailServerUserId;
    }

    /**
     * Legt den Wert der personalMailServerUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalMailServerUserId(String value) {
        this.personalMailServerUserId = value;
    }

    /**
     * Ruft den Wert der personalMailServerPassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalMailServerPassword() {
        return personalMailServerPassword;
    }

    /**
     * Legt den Wert der personalMailServerPassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalMailServerPassword(String value) {
        this.personalMailServerPassword = value;
    }

    /**
     * Gets the value of the voiceMessagingDistributionList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the voiceMessagingDistributionList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoiceMessagingDistributionList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VoiceMessagingDistributionListModify }
     * 
     * 
     * @return
     *     The value of the voiceMessagingDistributionList property.
     */
    public List<VoiceMessagingDistributionListModify> getVoiceMessagingDistributionList() {
        if (voiceMessagingDistributionList == null) {
            voiceMessagingDistributionList = new ArrayList<>();
        }
        return this.voiceMessagingDistributionList;
    }

    /**
     * Ruft den Wert der busyAnnouncementSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementSelection }
     *     
     */
    public AnnouncementSelection getBusyAnnouncementSelection() {
        return busyAnnouncementSelection;
    }

    /**
     * Legt den Wert der busyAnnouncementSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementSelection }
     *     
     */
    public void setBusyAnnouncementSelection(AnnouncementSelection value) {
        this.busyAnnouncementSelection = value;
    }

    /**
     * Ruft den Wert der busyPersonalAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getBusyPersonalAudioFile() {
        return busyPersonalAudioFile;
    }

    /**
     * Legt den Wert der busyPersonalAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setBusyPersonalAudioFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.busyPersonalAudioFile = value;
    }

    /**
     * Ruft den Wert der busyPersonalVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getBusyPersonalVideoFile() {
        return busyPersonalVideoFile;
    }

    /**
     * Legt den Wert der busyPersonalVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setBusyPersonalVideoFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.busyPersonalVideoFile = value;
    }

    /**
     * Ruft den Wert der noAnswerAnnouncementSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingNoAnswerGreetingSelection }
     *     
     */
    public VoiceMessagingNoAnswerGreetingSelection getNoAnswerAnnouncementSelection() {
        return noAnswerAnnouncementSelection;
    }

    /**
     * Legt den Wert der noAnswerAnnouncementSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingNoAnswerGreetingSelection }
     *     
     */
    public void setNoAnswerAnnouncementSelection(VoiceMessagingNoAnswerGreetingSelection value) {
        this.noAnswerAnnouncementSelection = value;
    }

    /**
     * Ruft den Wert der noAnswerPersonalAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getNoAnswerPersonalAudioFile() {
        return noAnswerPersonalAudioFile;
    }

    /**
     * Legt den Wert der noAnswerPersonalAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setNoAnswerPersonalAudioFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.noAnswerPersonalAudioFile = value;
    }

    /**
     * Ruft den Wert der noAnswerPersonalVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getNoAnswerPersonalVideoFile() {
        return noAnswerPersonalVideoFile;
    }

    /**
     * Legt den Wert der noAnswerPersonalVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setNoAnswerPersonalVideoFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.noAnswerPersonalVideoFile = value;
    }

    /**
     * Ruft den Wert der noAnswerAlternateGreeting01-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingModify20 }
     *     
     */
    public VoiceMessagingAlternateNoAnswerGreetingModify20 getNoAnswerAlternateGreeting01() {
        return noAnswerAlternateGreeting01;
    }

    /**
     * Legt den Wert der noAnswerAlternateGreeting01-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingModify20 }
     *     
     */
    public void setNoAnswerAlternateGreeting01(VoiceMessagingAlternateNoAnswerGreetingModify20 value) {
        this.noAnswerAlternateGreeting01 = value;
    }

    /**
     * Ruft den Wert der noAnswerAlternateGreeting02-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingModify20 }
     *     
     */
    public VoiceMessagingAlternateNoAnswerGreetingModify20 getNoAnswerAlternateGreeting02() {
        return noAnswerAlternateGreeting02;
    }

    /**
     * Legt den Wert der noAnswerAlternateGreeting02-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingModify20 }
     *     
     */
    public void setNoAnswerAlternateGreeting02(VoiceMessagingAlternateNoAnswerGreetingModify20 value) {
        this.noAnswerAlternateGreeting02 = value;
    }

    /**
     * Ruft den Wert der noAnswerAlternateGreeting03-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingModify20 }
     *     
     */
    public VoiceMessagingAlternateNoAnswerGreetingModify20 getNoAnswerAlternateGreeting03() {
        return noAnswerAlternateGreeting03;
    }

    /**
     * Legt den Wert der noAnswerAlternateGreeting03-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingAlternateNoAnswerGreetingModify20 }
     *     
     */
    public void setNoAnswerAlternateGreeting03(VoiceMessagingAlternateNoAnswerGreetingModify20 value) {
        this.noAnswerAlternateGreeting03 = value;
    }

    /**
     * Ruft den Wert der extendedAwayEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExtendedAwayEnabled() {
        return extendedAwayEnabled;
    }

    /**
     * Legt den Wert der extendedAwayEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExtendedAwayEnabled(Boolean value) {
        this.extendedAwayEnabled = value;
    }

    /**
     * Ruft den Wert der extendedAwayDisableMessageDeposit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExtendedAwayDisableMessageDeposit() {
        return extendedAwayDisableMessageDeposit;
    }

    /**
     * Legt den Wert der extendedAwayDisableMessageDeposit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExtendedAwayDisableMessageDeposit(Boolean value) {
        this.extendedAwayDisableMessageDeposit = value;
    }

    /**
     * Ruft den Wert der extendedAwayAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getExtendedAwayAudioFile() {
        return extendedAwayAudioFile;
    }

    /**
     * Legt den Wert der extendedAwayAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setExtendedAwayAudioFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.extendedAwayAudioFile = value;
    }

    /**
     * Ruft den Wert der extendedAwayVideoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getExtendedAwayVideoFile() {
        return extendedAwayVideoFile;
    }

    /**
     * Legt den Wert der extendedAwayVideoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setExtendedAwayVideoFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.extendedAwayVideoFile = value;
    }

    /**
     * Ruft den Wert der noAnswerNumberOfRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNoAnswerNumberOfRings() {
        return noAnswerNumberOfRings;
    }

    /**
     * Legt den Wert der noAnswerNumberOfRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNoAnswerNumberOfRings(Integer value) {
        this.noAnswerNumberOfRings = value;
    }

    /**
     * Ruft den Wert der disableMessageDeposit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisableMessageDeposit() {
        return disableMessageDeposit;
    }

    /**
     * Legt den Wert der disableMessageDeposit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisableMessageDeposit(Boolean value) {
        this.disableMessageDeposit = value;
    }

    /**
     * Ruft den Wert der disableMessageDepositAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingDisableMessageDepositSelection }
     *     
     */
    public VoiceMessagingDisableMessageDepositSelection getDisableMessageDepositAction() {
        return disableMessageDepositAction;
    }

    /**
     * Legt den Wert der disableMessageDepositAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingDisableMessageDepositSelection }
     *     
     */
    public void setDisableMessageDepositAction(VoiceMessagingDisableMessageDepositSelection value) {
        this.disableMessageDepositAction = value;
    }

    /**
     * Ruft den Wert der greetingOnlyForwardDestination-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGreetingOnlyForwardDestination() {
        return greetingOnlyForwardDestination;
    }

    /**
     * Legt den Wert der greetingOnlyForwardDestination-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGreetingOnlyForwardDestination(JAXBElement<String> value) {
        this.greetingOnlyForwardDestination = value;
    }

    /**
     * Ruft den Wert der outgoingSMDIMWIisActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOutgoingSMDIMWIisActive() {
        return outgoingSMDIMWIisActive;
    }

    /**
     * Legt den Wert der outgoingSMDIMWIisActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOutgoingSMDIMWIisActive(Boolean value) {
        this.outgoingSMDIMWIisActive = value;
    }

    /**
     * Ruft den Wert der outgoingSMDIMWIPhoneNumberList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementOutgoingDNList }{@code >}
     *     
     */
    public JAXBElement<ReplacementOutgoingDNList> getOutgoingSMDIMWIPhoneNumberList() {
        return outgoingSMDIMWIPhoneNumberList;
    }

    /**
     * Legt den Wert der outgoingSMDIMWIPhoneNumberList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementOutgoingDNList }{@code >}
     *     
     */
    public void setOutgoingSMDIMWIPhoneNumberList(JAXBElement<ReplacementOutgoingDNList> value) {
        this.outgoingSMDIMWIPhoneNumberList = value;
    }

    /**
     * Ruft den Wert der voiceManagementisActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVoiceManagementisActive() {
        return voiceManagementisActive;
    }

    /**
     * Legt den Wert der voiceManagementisActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVoiceManagementisActive(Boolean value) {
        this.voiceManagementisActive = value;
    }

    /**
     * Ruft den Wert der processing-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link VoiceMessagingMessageProcessing }
     *     
     */
    public VoiceMessagingMessageProcessing getProcessing() {
        return processing;
    }

    /**
     * Legt den Wert der processing-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link VoiceMessagingMessageProcessing }
     *     
     */
    public void setProcessing(VoiceMessagingMessageProcessing value) {
        this.processing = value;
    }

    /**
     * Ruft den Wert der voiceMessageDeliveryEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVoiceMessageDeliveryEmailAddress() {
        return voiceMessageDeliveryEmailAddress;
    }

    /**
     * Legt den Wert der voiceMessageDeliveryEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVoiceMessageDeliveryEmailAddress(JAXBElement<String> value) {
        this.voiceMessageDeliveryEmailAddress = value;
    }

    /**
     * Ruft den Wert der usePhoneMessageWaitingIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUsePhoneMessageWaitingIndicator() {
        return usePhoneMessageWaitingIndicator;
    }

    /**
     * Legt den Wert der usePhoneMessageWaitingIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUsePhoneMessageWaitingIndicator(Boolean value) {
        this.usePhoneMessageWaitingIndicator = value;
    }

    /**
     * Ruft den Wert der sendVoiceMessageNotifyEmail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendVoiceMessageNotifyEmail() {
        return sendVoiceMessageNotifyEmail;
    }

    /**
     * Legt den Wert der sendVoiceMessageNotifyEmail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendVoiceMessageNotifyEmail(Boolean value) {
        this.sendVoiceMessageNotifyEmail = value;
    }

    /**
     * Ruft den Wert der voiceMessageNotifyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVoiceMessageNotifyEmailAddress() {
        return voiceMessageNotifyEmailAddress;
    }

    /**
     * Legt den Wert der voiceMessageNotifyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVoiceMessageNotifyEmailAddress(JAXBElement<String> value) {
        this.voiceMessageNotifyEmailAddress = value;
    }

    /**
     * Ruft den Wert der sendCarbonCopyVoiceMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendCarbonCopyVoiceMessage() {
        return sendCarbonCopyVoiceMessage;
    }

    /**
     * Legt den Wert der sendCarbonCopyVoiceMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendCarbonCopyVoiceMessage(Boolean value) {
        this.sendCarbonCopyVoiceMessage = value;
    }

    /**
     * Ruft den Wert der voiceMessageCarbonCopyEmailAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVoiceMessageCarbonCopyEmailAddress() {
        return voiceMessageCarbonCopyEmailAddress;
    }

    /**
     * Legt den Wert der voiceMessageCarbonCopyEmailAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVoiceMessageCarbonCopyEmailAddress(JAXBElement<String> value) {
        this.voiceMessageCarbonCopyEmailAddress = value;
    }

    /**
     * Ruft den Wert der transferOnZeroToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTransferOnZeroToPhoneNumber() {
        return transferOnZeroToPhoneNumber;
    }

    /**
     * Legt den Wert der transferOnZeroToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTransferOnZeroToPhoneNumber(Boolean value) {
        this.transferOnZeroToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der transferPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransferPhoneNumber() {
        return transferPhoneNumber;
    }

    /**
     * Legt den Wert der transferPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransferPhoneNumber(JAXBElement<String> value) {
        this.transferPhoneNumber = value;
    }

    /**
     * Ruft den Wert der alwaysRedirectToVoiceMail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlwaysRedirectToVoiceMail() {
        return alwaysRedirectToVoiceMail;
    }

    /**
     * Legt den Wert der alwaysRedirectToVoiceMail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlwaysRedirectToVoiceMail(Boolean value) {
        this.alwaysRedirectToVoiceMail = value;
    }

    /**
     * Ruft den Wert der busyRedirectToVoiceMail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBusyRedirectToVoiceMail() {
        return busyRedirectToVoiceMail;
    }

    /**
     * Legt den Wert der busyRedirectToVoiceMail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBusyRedirectToVoiceMail(Boolean value) {
        this.busyRedirectToVoiceMail = value;
    }

    /**
     * Ruft den Wert der noAnswerRedirectToVoiceMail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNoAnswerRedirectToVoiceMail() {
        return noAnswerRedirectToVoiceMail;
    }

    /**
     * Legt den Wert der noAnswerRedirectToVoiceMail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoAnswerRedirectToVoiceMail(Boolean value) {
        this.noAnswerRedirectToVoiceMail = value;
    }

    /**
     * Ruft den Wert der outOfPrimaryZoneRedirectToVoiceMail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOutOfPrimaryZoneRedirectToVoiceMail() {
        return outOfPrimaryZoneRedirectToVoiceMail;
    }

    /**
     * Legt den Wert der outOfPrimaryZoneRedirectToVoiceMail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOutOfPrimaryZoneRedirectToVoiceMail(Boolean value) {
        this.outOfPrimaryZoneRedirectToVoiceMail = value;
    }

    /**
     * Ruft den Wert der usePersonalizedName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUsePersonalizedName() {
        return usePersonalizedName;
    }

    /**
     * Legt den Wert der usePersonalizedName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUsePersonalizedName(Boolean value) {
        this.usePersonalizedName = value;
    }

    /**
     * Ruft den Wert der voicePortalAutoLogin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVoicePortalAutoLogin() {
        return voicePortalAutoLogin;
    }

    /**
     * Legt den Wert der voicePortalAutoLogin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVoicePortalAutoLogin(Boolean value) {
        this.voicePortalAutoLogin = value;
    }

    /**
     * Ruft den Wert der personalizedNameAudioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public JAXBElement<AnnouncementFileLevelKey> getPersonalizedNameAudioFile() {
        return personalizedNameAudioFile;
    }

    /**
     * Legt den Wert der personalizedNameAudioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AnnouncementFileLevelKey }{@code >}
     *     
     */
    public void setPersonalizedNameAudioFile(JAXBElement<AnnouncementFileLevelKey> value) {
        this.personalizedNameAudioFile = value;
    }

    /**
     * Ruft den Wert der userMessagingAliasList-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link VoiceMessagingAliasReplacementList }{@code >}
     *     
     */
    public JAXBElement<VoiceMessagingAliasReplacementList> getUserMessagingAliasList() {
        return userMessagingAliasList;
    }

    /**
     * Legt den Wert der userMessagingAliasList-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link VoiceMessagingAliasReplacementList }{@code >}
     *     
     */
    public void setUserMessagingAliasList(JAXBElement<VoiceMessagingAliasReplacementList> value) {
        this.userMessagingAliasList = value;
    }

}
