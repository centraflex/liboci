//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupExchangeIntegrationGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupExchangeIntegrationGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupExchangeIntegrationGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableExchangeIntegration" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="exchangeURL" type="{}URL" minOccurs="0"/>
 *         <element name="exchangeUserName" type="{}ExchangeUserName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupExchangeIntegrationGetResponse", propOrder = {
    "enableExchangeIntegration",
    "exchangeURL",
    "exchangeUserName"
})
public class GroupExchangeIntegrationGetResponse
    extends OCIDataResponse
{

    protected boolean enableExchangeIntegration;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String exchangeURL;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String exchangeUserName;

    /**
     * Ruft den Wert der enableExchangeIntegration-Eigenschaft ab.
     * 
     */
    public boolean isEnableExchangeIntegration() {
        return enableExchangeIntegration;
    }

    /**
     * Legt den Wert der enableExchangeIntegration-Eigenschaft fest.
     * 
     */
    public void setEnableExchangeIntegration(boolean value) {
        this.enableExchangeIntegration = value;
    }

    /**
     * Ruft den Wert der exchangeURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeURL() {
        return exchangeURL;
    }

    /**
     * Legt den Wert der exchangeURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeURL(String value) {
        this.exchangeURL = value;
    }

    /**
     * Ruft den Wert der exchangeUserName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeUserName() {
        return exchangeUserName;
    }

    /**
     * Legt den Wert der exchangeUserName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeUserName(String value) {
        this.exchangeUserName = value;
    }

}
