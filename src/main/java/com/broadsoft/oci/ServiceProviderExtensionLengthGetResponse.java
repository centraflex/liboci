//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderExtensionLengthGetRequest.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderExtensionLengthGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderExtensionLengthGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="defaultExtensionLength" type="{}ExtensionLength" minOccurs="0"/>
 *         <element name="locationRoutingPrefixDigit" type="{}LocationRoutingPrefixDigit" minOccurs="0"/>
 *         <element name="locationCodeLength" type="{}EnterpriseLocationDialingCodeLength" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderExtensionLengthGetResponse", propOrder = {
    "defaultExtensionLength",
    "locationRoutingPrefixDigit",
    "locationCodeLength"
})
public class ServiceProviderExtensionLengthGetResponse
    extends OCIDataResponse
{

    protected Integer defaultExtensionLength;
    protected Integer locationRoutingPrefixDigit;
    protected Integer locationCodeLength;

    /**
     * Ruft den Wert der defaultExtensionLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDefaultExtensionLength() {
        return defaultExtensionLength;
    }

    /**
     * Legt den Wert der defaultExtensionLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDefaultExtensionLength(Integer value) {
        this.defaultExtensionLength = value;
    }

    /**
     * Ruft den Wert der locationRoutingPrefixDigit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLocationRoutingPrefixDigit() {
        return locationRoutingPrefixDigit;
    }

    /**
     * Legt den Wert der locationRoutingPrefixDigit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLocationRoutingPrefixDigit(Integer value) {
        this.locationRoutingPrefixDigit = value;
    }

    /**
     * Ruft den Wert der locationCodeLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLocationCodeLength() {
        return locationCodeLength;
    }

    /**
     * Legt den Wert der locationCodeLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLocationCodeLength(Integer value) {
        this.locationCodeLength = value;
    }

}
