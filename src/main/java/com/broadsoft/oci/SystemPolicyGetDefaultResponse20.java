//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemPolicyGetDefaultRequest20.
 *         Contains the default policy settings for the system.
 *         The following elements are only used in AS data mode:
 *             GroupAdminDialableCallerIDAccess
 *             ServiceProviderAdminDialableCallerIDAccess
 *                 GroupAdminCommunicationBarringUserProfileAccess (This element is only used for groups in an Enterprise)
 *             GroupAdminVerifyTranslationAndRoutingAccess
 *             ServiceProviderAdminVerifyTranslationAndRoutingAccess
 *         The following elements are only used in XS data mode:
 *             serviceProviderAdminCommunicationBarringAccess
 *           
 *         Replaced by: SystemPolicyGetDefaultResponse22 in AS mode  
 *       
 * 
 * <p>Java-Klasse für SystemPolicyGetDefaultResponse20 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPolicyGetDefaultResponse20">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="groupCallingPlanAccess" type="{}GroupCallingPlanAccess"/>
 *         <element name="groupExtensionAccess" type="{}GroupExtensionAccess"/>
 *         <element name="groupLDAPIntegrationAccess" type="{}GroupLDAPIntegrationAccess"/>
 *         <element name="groupVoiceMessagingAccess" type="{}GroupVoiceMessagingAccess"/>
 *         <element name="groupDepartmentAdminUserAccess" type="{}GroupDepartmentAdminUserAccess"/>
 *         <element name="groupDepartmentAdminTrunkGroupAccess" type="{}GroupDepartmentAdminTrunkGroupAccess"/>
 *         <element name="groupDepartmentAdminPhoneNumberExtensionAccess" type="{}GroupDepartmentAdminPhoneNumberExtensionAccess"/>
 *         <element name="groupDepartmentAdminCallingLineIdNumberAccess" type="{}GroupDepartmentAdminCallingLineIdNumberAccess"/>
 *         <element name="groupUserAuthenticationAccess" type="{}GroupUserAuthenticationAccess"/>
 *         <element name="groupUserGroupDirectoryAccess" type="{}GroupUserGroupDirectoryAccess"/>
 *         <element name="groupUserProfileAccess" type="{}GroupUserProfileAccess"/>
 *         <element name="groupUserEnhancedCallLogsAccess" type="{}GroupUserCallLogAccess"/>
 *         <element name="groupUserAutoAttendantNameDialingAccess" type="{}GroupUserAutoAttendantNameDialingAccess"/>
 *         <element name="groupAdminProfileAccess" type="{}GroupAdminProfileAccess"/>
 *         <element name="groupAdminUserAccess" type="{}GroupAdminUserAccess"/>
 *         <element name="groupAdminAdminAccess" type="{}GroupAdminAdminAccess"/>
 *         <element name="groupAdminDepartmentAccess" type="{}GroupAdminDepartmentAccess"/>
 *         <element name="groupAdminAccessDeviceAccess" type="{}GroupAdminAccessDeviceAccess"/>
 *         <element name="groupAdminEnhancedServiceInstanceAccess" type="{}GroupAdminEnhancedServiceInstanceAccess"/>
 *         <element name="groupAdminFeatureAccessCodeAccess" type="{}GroupAdminFeatureAccessCodeAccess"/>
 *         <element name="groupAdminPhoneNumberExtensionAccess" type="{}GroupAdminPhoneNumberExtensionAccess"/>
 *         <element name="groupAdminCallingLineIdNumberAccess" type="{}GroupAdminCallingLineIdNumberAccess"/>
 *         <element name="groupAdminServiceAccess" type="{}GroupAdminServiceAccess"/>
 *         <element name="groupAdminTrunkGroupAccess" type="{}GroupAdminTrunkGroupAccess"/>
 *         <element name="groupAdminVerifyTranslationAndRoutingAccess" type="{}GroupAdminVerifyTranslationAndRoutingAccess"/>
 *         <element name="groupAdminSessionAdmissionControlAccess" type="{}GroupAdminSessionAdmissionControlAccess"/>
 *         <element name="groupAdminOfficeZoneAccess" type="{}GroupAdminOfficeZoneAccess"/>
 *         <element name="groupAdminNumberActivationAccess" type="{}GroupAdminNumberActivationAccess"/>
 *         <element name="groupAdminDialableCallerIDAccess" type="{}GroupAdminDialableCallerIDAccess"/>
 *         <element name="groupAdminCommunicationBarringUserProfileAccess" type="{}GroupAdminCommunicationBarringUserProfileAccess"/>
 *         <element name="serviceProviderAdminProfileAccess" type="{}ServiceProviderAdminProfileAccess"/>
 *         <element name="serviceProviderAdminGroupAccess" type="{}ServiceProviderAdminGroupAccess"/>
 *         <element name="serviceProviderAdminUserAccess" type="{}ServiceProviderAdminUserAccess"/>
 *         <element name="serviceProviderAdminAdminAccess" type="{}ServiceProviderAdminAdminAccess"/>
 *         <element name="serviceProviderAdminDepartmentAccess" type="{}ServiceProviderAdminDepartmentAccess"/>
 *         <element name="serviceProviderAdminAccessDeviceAccess" type="{}ServiceProviderAdminAccessDeviceAccess"/>
 *         <element name="serviceProviderAdminPhoneNumberExtensionAccess" type="{}ServiceProviderAdminPhoneNumberExtensionAccess"/>
 *         <element name="serviceProviderAdminCallingLineIdNumberAccess" type="{}ServiceProviderAdminCallingLineIdNumberAccess"/>
 *         <element name="serviceProviderAdminServiceAccess" type="{}ServiceProviderAdminServiceAccess"/>
 *         <element name="serviceProviderAdminServicePackAccess" type="{}ServiceProviderAdminServicePackAccess"/>
 *         <element name="serviceProviderAdminSessionAdmissionControlAccess" type="{}ServiceProviderAdminSessionAdmissionControlAccess"/>
 *         <element name="serviceProviderAdminVerifyTranslationAndRoutingAccess" type="{}ServiceProviderAdminVerifyTranslationAndRoutingAccess"/>
 *         <element name="serviceProviderAdminWebBrandingAccess" type="{}ServiceProviderAdminWebBrandingAccess"/>
 *         <element name="serviceProviderAdminOfficeZoneAccess" type="{}ServiceProviderAdminOfficeZoneAccess"/>
 *         <element name="serviceProviderAdminCommunicationBarringAccess" type="{}ServiceProviderAdminCommunicationBarringAccess"/>
 *         <element name="enterpriseAdminNetworkPolicyAccess" type="{}EnterpriseAdminNetworkPolicyAccess"/>
 *         <element name="enterpriseAdminNumberActivationAccess" type="{}EnterpriseAdminNumberActivationAccess"/>
 *         <element name="serviceProviderAdminDialableCallerIDAccess" type="{}ServiceProviderAdminDialableCallerIDAccess"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPolicyGetDefaultResponse20", propOrder = {
    "groupCallingPlanAccess",
    "groupExtensionAccess",
    "groupLDAPIntegrationAccess",
    "groupVoiceMessagingAccess",
    "groupDepartmentAdminUserAccess",
    "groupDepartmentAdminTrunkGroupAccess",
    "groupDepartmentAdminPhoneNumberExtensionAccess",
    "groupDepartmentAdminCallingLineIdNumberAccess",
    "groupUserAuthenticationAccess",
    "groupUserGroupDirectoryAccess",
    "groupUserProfileAccess",
    "groupUserEnhancedCallLogsAccess",
    "groupUserAutoAttendantNameDialingAccess",
    "groupAdminProfileAccess",
    "groupAdminUserAccess",
    "groupAdminAdminAccess",
    "groupAdminDepartmentAccess",
    "groupAdminAccessDeviceAccess",
    "groupAdminEnhancedServiceInstanceAccess",
    "groupAdminFeatureAccessCodeAccess",
    "groupAdminPhoneNumberExtensionAccess",
    "groupAdminCallingLineIdNumberAccess",
    "groupAdminServiceAccess",
    "groupAdminTrunkGroupAccess",
    "groupAdminVerifyTranslationAndRoutingAccess",
    "groupAdminSessionAdmissionControlAccess",
    "groupAdminOfficeZoneAccess",
    "groupAdminNumberActivationAccess",
    "groupAdminDialableCallerIDAccess",
    "groupAdminCommunicationBarringUserProfileAccess",
    "serviceProviderAdminProfileAccess",
    "serviceProviderAdminGroupAccess",
    "serviceProviderAdminUserAccess",
    "serviceProviderAdminAdminAccess",
    "serviceProviderAdminDepartmentAccess",
    "serviceProviderAdminAccessDeviceAccess",
    "serviceProviderAdminPhoneNumberExtensionAccess",
    "serviceProviderAdminCallingLineIdNumberAccess",
    "serviceProviderAdminServiceAccess",
    "serviceProviderAdminServicePackAccess",
    "serviceProviderAdminSessionAdmissionControlAccess",
    "serviceProviderAdminVerifyTranslationAndRoutingAccess",
    "serviceProviderAdminWebBrandingAccess",
    "serviceProviderAdminOfficeZoneAccess",
    "serviceProviderAdminCommunicationBarringAccess",
    "enterpriseAdminNetworkPolicyAccess",
    "enterpriseAdminNumberActivationAccess",
    "serviceProviderAdminDialableCallerIDAccess"
})
public class SystemPolicyGetDefaultResponse20
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupCallingPlanAccess groupCallingPlanAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupExtensionAccess groupExtensionAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupLDAPIntegrationAccess groupLDAPIntegrationAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupVoiceMessagingAccess groupVoiceMessagingAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminUserAccess groupDepartmentAdminUserAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminTrunkGroupAccess groupDepartmentAdminTrunkGroupAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminPhoneNumberExtensionAccess groupDepartmentAdminPhoneNumberExtensionAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminCallingLineIdNumberAccess groupDepartmentAdminCallingLineIdNumberAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupUserAuthenticationAccess groupUserAuthenticationAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupUserGroupDirectoryAccess groupUserGroupDirectoryAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupUserProfileAccess groupUserProfileAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupUserCallLogAccess groupUserEnhancedCallLogsAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupUserAutoAttendantNameDialingAccess groupUserAutoAttendantNameDialingAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminProfileAccess groupAdminProfileAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminUserAccess groupAdminUserAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminAdminAccess groupAdminAdminAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminDepartmentAccess groupAdminDepartmentAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminAccessDeviceAccess groupAdminAccessDeviceAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminEnhancedServiceInstanceAccess groupAdminEnhancedServiceInstanceAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminFeatureAccessCodeAccess groupAdminFeatureAccessCodeAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminPhoneNumberExtensionAccess groupAdminPhoneNumberExtensionAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminCallingLineIdNumberAccess groupAdminCallingLineIdNumberAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminServiceAccess groupAdminServiceAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminTrunkGroupAccess groupAdminTrunkGroupAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminVerifyTranslationAndRoutingAccess groupAdminVerifyTranslationAndRoutingAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminSessionAdmissionControlAccess groupAdminSessionAdmissionControlAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminOfficeZoneAccess groupAdminOfficeZoneAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminNumberActivationAccess groupAdminNumberActivationAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminDialableCallerIDAccess groupAdminDialableCallerIDAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminCommunicationBarringUserProfileAccess groupAdminCommunicationBarringUserProfileAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminProfileAccess serviceProviderAdminProfileAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminGroupAccess serviceProviderAdminGroupAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminUserAccess serviceProviderAdminUserAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminAdminAccess serviceProviderAdminAdminAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminDepartmentAccess serviceProviderAdminDepartmentAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminAccessDeviceAccess serviceProviderAdminAccessDeviceAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminPhoneNumberExtensionAccess serviceProviderAdminPhoneNumberExtensionAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminCallingLineIdNumberAccess serviceProviderAdminCallingLineIdNumberAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminServiceAccess serviceProviderAdminServiceAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminServicePackAccess serviceProviderAdminServicePackAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminSessionAdmissionControlAccess serviceProviderAdminSessionAdmissionControlAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminVerifyTranslationAndRoutingAccess serviceProviderAdminVerifyTranslationAndRoutingAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminWebBrandingAccess serviceProviderAdminWebBrandingAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminOfficeZoneAccess serviceProviderAdminOfficeZoneAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminCommunicationBarringAccess serviceProviderAdminCommunicationBarringAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseAdminNetworkPolicyAccess enterpriseAdminNetworkPolicyAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseAdminNumberActivationAccess enterpriseAdminNumberActivationAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminDialableCallerIDAccess serviceProviderAdminDialableCallerIDAccess;

    /**
     * Ruft den Wert der groupCallingPlanAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupCallingPlanAccess }
     *     
     */
    public GroupCallingPlanAccess getGroupCallingPlanAccess() {
        return groupCallingPlanAccess;
    }

    /**
     * Legt den Wert der groupCallingPlanAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupCallingPlanAccess }
     *     
     */
    public void setGroupCallingPlanAccess(GroupCallingPlanAccess value) {
        this.groupCallingPlanAccess = value;
    }

    /**
     * Ruft den Wert der groupExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupExtensionAccess }
     *     
     */
    public GroupExtensionAccess getGroupExtensionAccess() {
        return groupExtensionAccess;
    }

    /**
     * Legt den Wert der groupExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupExtensionAccess }
     *     
     */
    public void setGroupExtensionAccess(GroupExtensionAccess value) {
        this.groupExtensionAccess = value;
    }

    /**
     * Ruft den Wert der groupLDAPIntegrationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupLDAPIntegrationAccess }
     *     
     */
    public GroupLDAPIntegrationAccess getGroupLDAPIntegrationAccess() {
        return groupLDAPIntegrationAccess;
    }

    /**
     * Legt den Wert der groupLDAPIntegrationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupLDAPIntegrationAccess }
     *     
     */
    public void setGroupLDAPIntegrationAccess(GroupLDAPIntegrationAccess value) {
        this.groupLDAPIntegrationAccess = value;
    }

    /**
     * Ruft den Wert der groupVoiceMessagingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupVoiceMessagingAccess }
     *     
     */
    public GroupVoiceMessagingAccess getGroupVoiceMessagingAccess() {
        return groupVoiceMessagingAccess;
    }

    /**
     * Legt den Wert der groupVoiceMessagingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupVoiceMessagingAccess }
     *     
     */
    public void setGroupVoiceMessagingAccess(GroupVoiceMessagingAccess value) {
        this.groupVoiceMessagingAccess = value;
    }

    /**
     * Ruft den Wert der groupDepartmentAdminUserAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminUserAccess }
     *     
     */
    public GroupDepartmentAdminUserAccess getGroupDepartmentAdminUserAccess() {
        return groupDepartmentAdminUserAccess;
    }

    /**
     * Legt den Wert der groupDepartmentAdminUserAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminUserAccess }
     *     
     */
    public void setGroupDepartmentAdminUserAccess(GroupDepartmentAdminUserAccess value) {
        this.groupDepartmentAdminUserAccess = value;
    }

    /**
     * Ruft den Wert der groupDepartmentAdminTrunkGroupAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminTrunkGroupAccess }
     *     
     */
    public GroupDepartmentAdminTrunkGroupAccess getGroupDepartmentAdminTrunkGroupAccess() {
        return groupDepartmentAdminTrunkGroupAccess;
    }

    /**
     * Legt den Wert der groupDepartmentAdminTrunkGroupAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminTrunkGroupAccess }
     *     
     */
    public void setGroupDepartmentAdminTrunkGroupAccess(GroupDepartmentAdminTrunkGroupAccess value) {
        this.groupDepartmentAdminTrunkGroupAccess = value;
    }

    /**
     * Ruft den Wert der groupDepartmentAdminPhoneNumberExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminPhoneNumberExtensionAccess }
     *     
     */
    public GroupDepartmentAdminPhoneNumberExtensionAccess getGroupDepartmentAdminPhoneNumberExtensionAccess() {
        return groupDepartmentAdminPhoneNumberExtensionAccess;
    }

    /**
     * Legt den Wert der groupDepartmentAdminPhoneNumberExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminPhoneNumberExtensionAccess }
     *     
     */
    public void setGroupDepartmentAdminPhoneNumberExtensionAccess(GroupDepartmentAdminPhoneNumberExtensionAccess value) {
        this.groupDepartmentAdminPhoneNumberExtensionAccess = value;
    }

    /**
     * Ruft den Wert der groupDepartmentAdminCallingLineIdNumberAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminCallingLineIdNumberAccess }
     *     
     */
    public GroupDepartmentAdminCallingLineIdNumberAccess getGroupDepartmentAdminCallingLineIdNumberAccess() {
        return groupDepartmentAdminCallingLineIdNumberAccess;
    }

    /**
     * Legt den Wert der groupDepartmentAdminCallingLineIdNumberAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminCallingLineIdNumberAccess }
     *     
     */
    public void setGroupDepartmentAdminCallingLineIdNumberAccess(GroupDepartmentAdminCallingLineIdNumberAccess value) {
        this.groupDepartmentAdminCallingLineIdNumberAccess = value;
    }

    /**
     * Ruft den Wert der groupUserAuthenticationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserAuthenticationAccess }
     *     
     */
    public GroupUserAuthenticationAccess getGroupUserAuthenticationAccess() {
        return groupUserAuthenticationAccess;
    }

    /**
     * Legt den Wert der groupUserAuthenticationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserAuthenticationAccess }
     *     
     */
    public void setGroupUserAuthenticationAccess(GroupUserAuthenticationAccess value) {
        this.groupUserAuthenticationAccess = value;
    }

    /**
     * Ruft den Wert der groupUserGroupDirectoryAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserGroupDirectoryAccess }
     *     
     */
    public GroupUserGroupDirectoryAccess getGroupUserGroupDirectoryAccess() {
        return groupUserGroupDirectoryAccess;
    }

    /**
     * Legt den Wert der groupUserGroupDirectoryAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserGroupDirectoryAccess }
     *     
     */
    public void setGroupUserGroupDirectoryAccess(GroupUserGroupDirectoryAccess value) {
        this.groupUserGroupDirectoryAccess = value;
    }

    /**
     * Ruft den Wert der groupUserProfileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserProfileAccess }
     *     
     */
    public GroupUserProfileAccess getGroupUserProfileAccess() {
        return groupUserProfileAccess;
    }

    /**
     * Legt den Wert der groupUserProfileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserProfileAccess }
     *     
     */
    public void setGroupUserProfileAccess(GroupUserProfileAccess value) {
        this.groupUserProfileAccess = value;
    }

    /**
     * Ruft den Wert der groupUserEnhancedCallLogsAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserCallLogAccess }
     *     
     */
    public GroupUserCallLogAccess getGroupUserEnhancedCallLogsAccess() {
        return groupUserEnhancedCallLogsAccess;
    }

    /**
     * Legt den Wert der groupUserEnhancedCallLogsAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserCallLogAccess }
     *     
     */
    public void setGroupUserEnhancedCallLogsAccess(GroupUserCallLogAccess value) {
        this.groupUserEnhancedCallLogsAccess = value;
    }

    /**
     * Ruft den Wert der groupUserAutoAttendantNameDialingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserAutoAttendantNameDialingAccess }
     *     
     */
    public GroupUserAutoAttendantNameDialingAccess getGroupUserAutoAttendantNameDialingAccess() {
        return groupUserAutoAttendantNameDialingAccess;
    }

    /**
     * Legt den Wert der groupUserAutoAttendantNameDialingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserAutoAttendantNameDialingAccess }
     *     
     */
    public void setGroupUserAutoAttendantNameDialingAccess(GroupUserAutoAttendantNameDialingAccess value) {
        this.groupUserAutoAttendantNameDialingAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminProfileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminProfileAccess }
     *     
     */
    public GroupAdminProfileAccess getGroupAdminProfileAccess() {
        return groupAdminProfileAccess;
    }

    /**
     * Legt den Wert der groupAdminProfileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminProfileAccess }
     *     
     */
    public void setGroupAdminProfileAccess(GroupAdminProfileAccess value) {
        this.groupAdminProfileAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminUserAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminUserAccess }
     *     
     */
    public GroupAdminUserAccess getGroupAdminUserAccess() {
        return groupAdminUserAccess;
    }

    /**
     * Legt den Wert der groupAdminUserAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminUserAccess }
     *     
     */
    public void setGroupAdminUserAccess(GroupAdminUserAccess value) {
        this.groupAdminUserAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminAdminAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminAdminAccess }
     *     
     */
    public GroupAdminAdminAccess getGroupAdminAdminAccess() {
        return groupAdminAdminAccess;
    }

    /**
     * Legt den Wert der groupAdminAdminAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminAdminAccess }
     *     
     */
    public void setGroupAdminAdminAccess(GroupAdminAdminAccess value) {
        this.groupAdminAdminAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminDepartmentAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminDepartmentAccess }
     *     
     */
    public GroupAdminDepartmentAccess getGroupAdminDepartmentAccess() {
        return groupAdminDepartmentAccess;
    }

    /**
     * Legt den Wert der groupAdminDepartmentAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminDepartmentAccess }
     *     
     */
    public void setGroupAdminDepartmentAccess(GroupAdminDepartmentAccess value) {
        this.groupAdminDepartmentAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminAccessDeviceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminAccessDeviceAccess }
     *     
     */
    public GroupAdminAccessDeviceAccess getGroupAdminAccessDeviceAccess() {
        return groupAdminAccessDeviceAccess;
    }

    /**
     * Legt den Wert der groupAdminAccessDeviceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminAccessDeviceAccess }
     *     
     */
    public void setGroupAdminAccessDeviceAccess(GroupAdminAccessDeviceAccess value) {
        this.groupAdminAccessDeviceAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminEnhancedServiceInstanceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminEnhancedServiceInstanceAccess }
     *     
     */
    public GroupAdminEnhancedServiceInstanceAccess getGroupAdminEnhancedServiceInstanceAccess() {
        return groupAdminEnhancedServiceInstanceAccess;
    }

    /**
     * Legt den Wert der groupAdminEnhancedServiceInstanceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminEnhancedServiceInstanceAccess }
     *     
     */
    public void setGroupAdminEnhancedServiceInstanceAccess(GroupAdminEnhancedServiceInstanceAccess value) {
        this.groupAdminEnhancedServiceInstanceAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminFeatureAccessCodeAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminFeatureAccessCodeAccess }
     *     
     */
    public GroupAdminFeatureAccessCodeAccess getGroupAdminFeatureAccessCodeAccess() {
        return groupAdminFeatureAccessCodeAccess;
    }

    /**
     * Legt den Wert der groupAdminFeatureAccessCodeAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminFeatureAccessCodeAccess }
     *     
     */
    public void setGroupAdminFeatureAccessCodeAccess(GroupAdminFeatureAccessCodeAccess value) {
        this.groupAdminFeatureAccessCodeAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminPhoneNumberExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminPhoneNumberExtensionAccess }
     *     
     */
    public GroupAdminPhoneNumberExtensionAccess getGroupAdminPhoneNumberExtensionAccess() {
        return groupAdminPhoneNumberExtensionAccess;
    }

    /**
     * Legt den Wert der groupAdminPhoneNumberExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminPhoneNumberExtensionAccess }
     *     
     */
    public void setGroupAdminPhoneNumberExtensionAccess(GroupAdminPhoneNumberExtensionAccess value) {
        this.groupAdminPhoneNumberExtensionAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminCallingLineIdNumberAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminCallingLineIdNumberAccess }
     *     
     */
    public GroupAdminCallingLineIdNumberAccess getGroupAdminCallingLineIdNumberAccess() {
        return groupAdminCallingLineIdNumberAccess;
    }

    /**
     * Legt den Wert der groupAdminCallingLineIdNumberAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminCallingLineIdNumberAccess }
     *     
     */
    public void setGroupAdminCallingLineIdNumberAccess(GroupAdminCallingLineIdNumberAccess value) {
        this.groupAdminCallingLineIdNumberAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminServiceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminServiceAccess }
     *     
     */
    public GroupAdminServiceAccess getGroupAdminServiceAccess() {
        return groupAdminServiceAccess;
    }

    /**
     * Legt den Wert der groupAdminServiceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminServiceAccess }
     *     
     */
    public void setGroupAdminServiceAccess(GroupAdminServiceAccess value) {
        this.groupAdminServiceAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminTrunkGroupAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminTrunkGroupAccess }
     *     
     */
    public GroupAdminTrunkGroupAccess getGroupAdminTrunkGroupAccess() {
        return groupAdminTrunkGroupAccess;
    }

    /**
     * Legt den Wert der groupAdminTrunkGroupAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminTrunkGroupAccess }
     *     
     */
    public void setGroupAdminTrunkGroupAccess(GroupAdminTrunkGroupAccess value) {
        this.groupAdminTrunkGroupAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminVerifyTranslationAndRoutingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminVerifyTranslationAndRoutingAccess }
     *     
     */
    public GroupAdminVerifyTranslationAndRoutingAccess getGroupAdminVerifyTranslationAndRoutingAccess() {
        return groupAdminVerifyTranslationAndRoutingAccess;
    }

    /**
     * Legt den Wert der groupAdminVerifyTranslationAndRoutingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminVerifyTranslationAndRoutingAccess }
     *     
     */
    public void setGroupAdminVerifyTranslationAndRoutingAccess(GroupAdminVerifyTranslationAndRoutingAccess value) {
        this.groupAdminVerifyTranslationAndRoutingAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminSessionAdmissionControlAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminSessionAdmissionControlAccess }
     *     
     */
    public GroupAdminSessionAdmissionControlAccess getGroupAdminSessionAdmissionControlAccess() {
        return groupAdminSessionAdmissionControlAccess;
    }

    /**
     * Legt den Wert der groupAdminSessionAdmissionControlAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminSessionAdmissionControlAccess }
     *     
     */
    public void setGroupAdminSessionAdmissionControlAccess(GroupAdminSessionAdmissionControlAccess value) {
        this.groupAdminSessionAdmissionControlAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminOfficeZoneAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminOfficeZoneAccess }
     *     
     */
    public GroupAdminOfficeZoneAccess getGroupAdminOfficeZoneAccess() {
        return groupAdminOfficeZoneAccess;
    }

    /**
     * Legt den Wert der groupAdminOfficeZoneAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminOfficeZoneAccess }
     *     
     */
    public void setGroupAdminOfficeZoneAccess(GroupAdminOfficeZoneAccess value) {
        this.groupAdminOfficeZoneAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminNumberActivationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminNumberActivationAccess }
     *     
     */
    public GroupAdminNumberActivationAccess getGroupAdminNumberActivationAccess() {
        return groupAdminNumberActivationAccess;
    }

    /**
     * Legt den Wert der groupAdminNumberActivationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminNumberActivationAccess }
     *     
     */
    public void setGroupAdminNumberActivationAccess(GroupAdminNumberActivationAccess value) {
        this.groupAdminNumberActivationAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminDialableCallerIDAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminDialableCallerIDAccess }
     *     
     */
    public GroupAdminDialableCallerIDAccess getGroupAdminDialableCallerIDAccess() {
        return groupAdminDialableCallerIDAccess;
    }

    /**
     * Legt den Wert der groupAdminDialableCallerIDAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminDialableCallerIDAccess }
     *     
     */
    public void setGroupAdminDialableCallerIDAccess(GroupAdminDialableCallerIDAccess value) {
        this.groupAdminDialableCallerIDAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminCommunicationBarringUserProfileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminCommunicationBarringUserProfileAccess }
     *     
     */
    public GroupAdminCommunicationBarringUserProfileAccess getGroupAdminCommunicationBarringUserProfileAccess() {
        return groupAdminCommunicationBarringUserProfileAccess;
    }

    /**
     * Legt den Wert der groupAdminCommunicationBarringUserProfileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminCommunicationBarringUserProfileAccess }
     *     
     */
    public void setGroupAdminCommunicationBarringUserProfileAccess(GroupAdminCommunicationBarringUserProfileAccess value) {
        this.groupAdminCommunicationBarringUserProfileAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminProfileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminProfileAccess }
     *     
     */
    public ServiceProviderAdminProfileAccess getServiceProviderAdminProfileAccess() {
        return serviceProviderAdminProfileAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminProfileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminProfileAccess }
     *     
     */
    public void setServiceProviderAdminProfileAccess(ServiceProviderAdminProfileAccess value) {
        this.serviceProviderAdminProfileAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminGroupAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminGroupAccess }
     *     
     */
    public ServiceProviderAdminGroupAccess getServiceProviderAdminGroupAccess() {
        return serviceProviderAdminGroupAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminGroupAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminGroupAccess }
     *     
     */
    public void setServiceProviderAdminGroupAccess(ServiceProviderAdminGroupAccess value) {
        this.serviceProviderAdminGroupAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminUserAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminUserAccess }
     *     
     */
    public ServiceProviderAdminUserAccess getServiceProviderAdminUserAccess() {
        return serviceProviderAdminUserAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminUserAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminUserAccess }
     *     
     */
    public void setServiceProviderAdminUserAccess(ServiceProviderAdminUserAccess value) {
        this.serviceProviderAdminUserAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminAdminAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminAdminAccess }
     *     
     */
    public ServiceProviderAdminAdminAccess getServiceProviderAdminAdminAccess() {
        return serviceProviderAdminAdminAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminAdminAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminAdminAccess }
     *     
     */
    public void setServiceProviderAdminAdminAccess(ServiceProviderAdminAdminAccess value) {
        this.serviceProviderAdminAdminAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminDepartmentAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminDepartmentAccess }
     *     
     */
    public ServiceProviderAdminDepartmentAccess getServiceProviderAdminDepartmentAccess() {
        return serviceProviderAdminDepartmentAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminDepartmentAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminDepartmentAccess }
     *     
     */
    public void setServiceProviderAdminDepartmentAccess(ServiceProviderAdminDepartmentAccess value) {
        this.serviceProviderAdminDepartmentAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminAccessDeviceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminAccessDeviceAccess }
     *     
     */
    public ServiceProviderAdminAccessDeviceAccess getServiceProviderAdminAccessDeviceAccess() {
        return serviceProviderAdminAccessDeviceAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminAccessDeviceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminAccessDeviceAccess }
     *     
     */
    public void setServiceProviderAdminAccessDeviceAccess(ServiceProviderAdminAccessDeviceAccess value) {
        this.serviceProviderAdminAccessDeviceAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminPhoneNumberExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminPhoneNumberExtensionAccess }
     *     
     */
    public ServiceProviderAdminPhoneNumberExtensionAccess getServiceProviderAdminPhoneNumberExtensionAccess() {
        return serviceProviderAdminPhoneNumberExtensionAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminPhoneNumberExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminPhoneNumberExtensionAccess }
     *     
     */
    public void setServiceProviderAdminPhoneNumberExtensionAccess(ServiceProviderAdminPhoneNumberExtensionAccess value) {
        this.serviceProviderAdminPhoneNumberExtensionAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminCallingLineIdNumberAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminCallingLineIdNumberAccess }
     *     
     */
    public ServiceProviderAdminCallingLineIdNumberAccess getServiceProviderAdminCallingLineIdNumberAccess() {
        return serviceProviderAdminCallingLineIdNumberAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminCallingLineIdNumberAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminCallingLineIdNumberAccess }
     *     
     */
    public void setServiceProviderAdminCallingLineIdNumberAccess(ServiceProviderAdminCallingLineIdNumberAccess value) {
        this.serviceProviderAdminCallingLineIdNumberAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminServiceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminServiceAccess }
     *     
     */
    public ServiceProviderAdminServiceAccess getServiceProviderAdminServiceAccess() {
        return serviceProviderAdminServiceAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminServiceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminServiceAccess }
     *     
     */
    public void setServiceProviderAdminServiceAccess(ServiceProviderAdminServiceAccess value) {
        this.serviceProviderAdminServiceAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminServicePackAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminServicePackAccess }
     *     
     */
    public ServiceProviderAdminServicePackAccess getServiceProviderAdminServicePackAccess() {
        return serviceProviderAdminServicePackAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminServicePackAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminServicePackAccess }
     *     
     */
    public void setServiceProviderAdminServicePackAccess(ServiceProviderAdminServicePackAccess value) {
        this.serviceProviderAdminServicePackAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminSessionAdmissionControlAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminSessionAdmissionControlAccess }
     *     
     */
    public ServiceProviderAdminSessionAdmissionControlAccess getServiceProviderAdminSessionAdmissionControlAccess() {
        return serviceProviderAdminSessionAdmissionControlAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminSessionAdmissionControlAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminSessionAdmissionControlAccess }
     *     
     */
    public void setServiceProviderAdminSessionAdmissionControlAccess(ServiceProviderAdminSessionAdmissionControlAccess value) {
        this.serviceProviderAdminSessionAdmissionControlAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminVerifyTranslationAndRoutingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminVerifyTranslationAndRoutingAccess }
     *     
     */
    public ServiceProviderAdminVerifyTranslationAndRoutingAccess getServiceProviderAdminVerifyTranslationAndRoutingAccess() {
        return serviceProviderAdminVerifyTranslationAndRoutingAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminVerifyTranslationAndRoutingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminVerifyTranslationAndRoutingAccess }
     *     
     */
    public void setServiceProviderAdminVerifyTranslationAndRoutingAccess(ServiceProviderAdminVerifyTranslationAndRoutingAccess value) {
        this.serviceProviderAdminVerifyTranslationAndRoutingAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminWebBrandingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminWebBrandingAccess }
     *     
     */
    public ServiceProviderAdminWebBrandingAccess getServiceProviderAdminWebBrandingAccess() {
        return serviceProviderAdminWebBrandingAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminWebBrandingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminWebBrandingAccess }
     *     
     */
    public void setServiceProviderAdminWebBrandingAccess(ServiceProviderAdminWebBrandingAccess value) {
        this.serviceProviderAdminWebBrandingAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminOfficeZoneAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminOfficeZoneAccess }
     *     
     */
    public ServiceProviderAdminOfficeZoneAccess getServiceProviderAdminOfficeZoneAccess() {
        return serviceProviderAdminOfficeZoneAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminOfficeZoneAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminOfficeZoneAccess }
     *     
     */
    public void setServiceProviderAdminOfficeZoneAccess(ServiceProviderAdminOfficeZoneAccess value) {
        this.serviceProviderAdminOfficeZoneAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminCommunicationBarringAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminCommunicationBarringAccess }
     *     
     */
    public ServiceProviderAdminCommunicationBarringAccess getServiceProviderAdminCommunicationBarringAccess() {
        return serviceProviderAdminCommunicationBarringAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminCommunicationBarringAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminCommunicationBarringAccess }
     *     
     */
    public void setServiceProviderAdminCommunicationBarringAccess(ServiceProviderAdminCommunicationBarringAccess value) {
        this.serviceProviderAdminCommunicationBarringAccess = value;
    }

    /**
     * Ruft den Wert der enterpriseAdminNetworkPolicyAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseAdminNetworkPolicyAccess }
     *     
     */
    public EnterpriseAdminNetworkPolicyAccess getEnterpriseAdminNetworkPolicyAccess() {
        return enterpriseAdminNetworkPolicyAccess;
    }

    /**
     * Legt den Wert der enterpriseAdminNetworkPolicyAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseAdminNetworkPolicyAccess }
     *     
     */
    public void setEnterpriseAdminNetworkPolicyAccess(EnterpriseAdminNetworkPolicyAccess value) {
        this.enterpriseAdminNetworkPolicyAccess = value;
    }

    /**
     * Ruft den Wert der enterpriseAdminNumberActivationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseAdminNumberActivationAccess }
     *     
     */
    public EnterpriseAdminNumberActivationAccess getEnterpriseAdminNumberActivationAccess() {
        return enterpriseAdminNumberActivationAccess;
    }

    /**
     * Legt den Wert der enterpriseAdminNumberActivationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseAdminNumberActivationAccess }
     *     
     */
    public void setEnterpriseAdminNumberActivationAccess(EnterpriseAdminNumberActivationAccess value) {
        this.enterpriseAdminNumberActivationAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminDialableCallerIDAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminDialableCallerIDAccess }
     *     
     */
    public ServiceProviderAdminDialableCallerIDAccess getServiceProviderAdminDialableCallerIDAccess() {
        return serviceProviderAdminDialableCallerIDAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminDialableCallerIDAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminDialableCallerIDAccess }
     *     
     */
    public void setServiceProviderAdminDialableCallerIDAccess(ServiceProviderAdminDialableCallerIDAccess value) {
        this.serviceProviderAdminDialableCallerIDAccess = value;
    }

}
