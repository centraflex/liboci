//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Delete a Call Blocking Service mapping.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         Replaced by: SystemTreatmentMappingCallBlockingServiceDeleteRequest22 in AS data mode
 *       
 * 
 * <p>Java-Klasse für SystemTreatmentMappingCallBlockingServiceDeleteRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemTreatmentMappingCallBlockingServiceDeleteRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="callBlockingService" type="{}CallBlockingService"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemTreatmentMappingCallBlockingServiceDeleteRequest", propOrder = {
    "callBlockingService"
})
public class SystemTreatmentMappingCallBlockingServiceDeleteRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallBlockingService callBlockingService;

    /**
     * Ruft den Wert der callBlockingService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallBlockingService }
     *     
     */
    public CallBlockingService getCallBlockingService() {
        return callBlockingService;
    }

    /**
     * Legt den Wert der callBlockingService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallBlockingService }
     *     
     */
    public void setCallBlockingService(CallBlockingService value) {
        this.callBlockingService = value;
    }

}
