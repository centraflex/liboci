//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         The voice portal fax messaging menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für FaxMessagingMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="FaxMessagingMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="saveFaxMessageAndSkipToNext" type="{}DigitAny" minOccurs="0"/>
 *         <element name="previousFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="playEnvelope" type="{}DigitAny" minOccurs="0"/>
 *         <element name="nextFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="deleteFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="printFaxMessage" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaxMessagingMenuKeysModifyEntry", propOrder = {
    "saveFaxMessageAndSkipToNext",
    "previousFaxMessage",
    "playEnvelope",
    "nextFaxMessage",
    "deleteFaxMessage",
    "printFaxMessage",
    "returnToPreviousMenu"
})
public class FaxMessagingMenuKeysModifyEntry {

    @XmlElementRef(name = "saveFaxMessageAndSkipToNext", type = JAXBElement.class, required = false)
    protected JAXBElement<String> saveFaxMessageAndSkipToNext;
    @XmlElementRef(name = "previousFaxMessage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> previousFaxMessage;
    @XmlElementRef(name = "playEnvelope", type = JAXBElement.class, required = false)
    protected JAXBElement<String> playEnvelope;
    @XmlElementRef(name = "nextFaxMessage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nextFaxMessage;
    @XmlElementRef(name = "deleteFaxMessage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deleteFaxMessage;
    @XmlElementRef(name = "printFaxMessage", type = JAXBElement.class, required = false)
    protected JAXBElement<String> printFaxMessage;
    @XmlElementRef(name = "returnToPreviousMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> returnToPreviousMenu;

    /**
     * Ruft den Wert der saveFaxMessageAndSkipToNext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSaveFaxMessageAndSkipToNext() {
        return saveFaxMessageAndSkipToNext;
    }

    /**
     * Legt den Wert der saveFaxMessageAndSkipToNext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSaveFaxMessageAndSkipToNext(JAXBElement<String> value) {
        this.saveFaxMessageAndSkipToNext = value;
    }

    /**
     * Ruft den Wert der previousFaxMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPreviousFaxMessage() {
        return previousFaxMessage;
    }

    /**
     * Legt den Wert der previousFaxMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPreviousFaxMessage(JAXBElement<String> value) {
        this.previousFaxMessage = value;
    }

    /**
     * Ruft den Wert der playEnvelope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlayEnvelope() {
        return playEnvelope;
    }

    /**
     * Legt den Wert der playEnvelope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlayEnvelope(JAXBElement<String> value) {
        this.playEnvelope = value;
    }

    /**
     * Ruft den Wert der nextFaxMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNextFaxMessage() {
        return nextFaxMessage;
    }

    /**
     * Legt den Wert der nextFaxMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNextFaxMessage(JAXBElement<String> value) {
        this.nextFaxMessage = value;
    }

    /**
     * Ruft den Wert der deleteFaxMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeleteFaxMessage() {
        return deleteFaxMessage;
    }

    /**
     * Legt den Wert der deleteFaxMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeleteFaxMessage(JAXBElement<String> value) {
        this.deleteFaxMessage = value;
    }

    /**
     * Ruft den Wert der printFaxMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrintFaxMessage() {
        return printFaxMessage;
    }

    /**
     * Legt den Wert der printFaxMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrintFaxMessage(JAXBElement<String> value) {
        this.printFaxMessage = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReturnToPreviousMenu(JAXBElement<String> value) {
        this.returnToPreviousMenu = value;
    }

}
