//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a service provider or enterprise. 
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           serviceProviderExtenalId
 *         
 *         The following elements are only used in Amplify data mode and ignored in AS and XS data mode:
 *         servicePolicy,
 *         callProcessingSliceId, 
 *         provisioningSliceId,
 *         subscriberPartition.
 *         When the callProcessingSliceId or provisioningSliceId is not specified in the AmplifyDataMode, 
 *         the default slice Id is assigned to the service provider.
 *         Only Provisioning admin and above can change the callProcessingSliceId,  provisioningSliceId, andsubscriberPartition.
 *         
 *         The following elements are only used in Amplify and XS data mode and ignored in AS data mode:
 *         preferredDataCenter.
 *         Only Provisioning admin and above can change the preferredDataCenter.     
 *         
 *         The following data elements are only used in AS data mode:
 *           resellerId
 *           resellerName  
 * 
 * 	    The following behavior is only applicable in CloudPBX:
 * 	      - when new resellerId, that does not exist in the system, is specified, the new 
 * 	        Reseller is created the given resellerId and resellerName (if provided) and enterprise/Service Provider 
 * 	        is moved to the newly created Reseller.
 * 
 * 	    resellerName element is ignored if the reseller the service provider is being moved to 	already exists.
 * 
 *         The following elements are optional for the service provider. If the elements are included,
 *         they will be either added, authorized, or modified on the service provider. Should any of the 
 *         following elements be rejected to due existing system settings, the service provider will not 
 *         be added and the response will be an ErrorResponse:
 *           domain
 *           admin
 *           groupServiceAuthorization
 *           userServiceAuthorization
 *           servicePack
 *           phoneNumber
 *           dnRange
 *           routingProfile
 *           meetMeConferencingAllocatedPorts
 *           trunkGroupMaxActiveCalls
 *           trunkGroupBurstingMaxActiveCalls
 *           voiceMessagingGroupSettings
 *           voiceMessagingVoicePortalScope
 *           
 *         When a group or user service is included that is not activated or is not licensed,
 *         the response will be an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderConsolidatedAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderConsolidatedAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <choice>
 *           <element name="isEnterprise" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *           <element name="useCustomRoutingProfile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         </choice>
 *         <element name="serviceProviderId" type="{}ServiceProviderId" minOccurs="0"/>
 *         <element name="serviceProviderExternalId" type="{}ExternalId" minOccurs="0"/>
 *         <element name="defaultDomain" type="{}NetAddress"/>
 *         <element name="serviceProviderName" type="{}ServiceProviderName" minOccurs="0"/>
 *         <element name="supportEmail" type="{}EmailAddress" minOccurs="0"/>
 *         <element name="contact" type="{}Contact" minOccurs="0"/>
 *         <element name="address" type="{}StreetAddress" minOccurs="0"/>
 *         <element name="servicePolicy" type="{}ServicePolicyName" minOccurs="0"/>
 *         <element name="callProcessingSliceId" type="{}SliceId" minOccurs="0"/>
 *         <element name="provisioningSliceId" type="{}SliceId" minOccurs="0"/>
 *         <element name="subscriberPartition" type="{}SubscriberPartition" minOccurs="0"/>
 *         <element name="preferredDataCenter" type="{}DataCenter" minOccurs="0"/>
 *         <element name="resellerId" type="{}ResellerId22" minOccurs="0"/>
 *         <element name="resellerName" type="{}ResellerName22" minOccurs="0"/>
 *         <element name="domain" type="{}NetAddress" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="admin" type="{}ServiceProviderAdmin" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="groupServiceAuthorization" type="{}GroupServiceAuthorization" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="userServiceAuthorization" type="{}UserServiceAuthorization" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="servicePack" type="{}ServicePack" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="phoneNumber" type="{}DN" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="dnRange" type="{}DNRange" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="routingProfile" type="{}RoutingProfile" minOccurs="0"/>
 *         <element name="meetMeConferencingAllocatedPorts" type="{}MeetMeConferencingConferencePorts" minOccurs="0"/>
 *         <element name="trunkGroupMaxActiveCalls" type="{}UnboundedNonNegativeInt" minOccurs="0"/>
 *         <element name="trunkGroupBurstingMaxActiveCalls" type="{}UnboundedNonNegativeInt" minOccurs="0"/>
 *         <element name="voiceMessagingGroupSettings" type="{}ServiceProviderVoiceMessagingGroupSettingsAdd" minOccurs="0"/>
 *         <element name="voiceMessagingGroupVoicePortalScope" type="{}ServiceProviderVoicePortalScope" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderConsolidatedAddRequest", propOrder = {
    "isEnterprise",
    "useCustomRoutingProfile",
    "serviceProviderId",
    "serviceProviderExternalId",
    "defaultDomain",
    "serviceProviderName",
    "supportEmail",
    "contact",
    "address",
    "servicePolicy",
    "callProcessingSliceId",
    "provisioningSliceId",
    "subscriberPartition",
    "preferredDataCenter",
    "resellerId",
    "resellerName",
    "domain",
    "admin",
    "groupServiceAuthorization",
    "userServiceAuthorization",
    "servicePack",
    "phoneNumber",
    "dnRange",
    "routingProfile",
    "meetMeConferencingAllocatedPorts",
    "trunkGroupMaxActiveCalls",
    "trunkGroupBurstingMaxActiveCalls",
    "voiceMessagingGroupSettings",
    "voiceMessagingGroupVoicePortalScope"
})
public class ServiceProviderConsolidatedAddRequest
    extends OCIRequest
{

    protected Boolean isEnterprise;
    protected Boolean useCustomRoutingProfile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderExternalId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultDomain;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String supportEmail;
    protected Contact contact;
    protected StreetAddress address;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String servicePolicy;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callProcessingSliceId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String provisioningSliceId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String subscriberPartition;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String preferredDataCenter;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> domain;
    protected List<ServiceProviderAdmin> admin;
    protected List<GroupServiceAuthorization> groupServiceAuthorization;
    protected List<UserServiceAuthorization> userServiceAuthorization;
    protected List<ServicePack> servicePack;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> phoneNumber;
    protected List<DNRange> dnRange;
    @XmlElementRef(name = "routingProfile", type = JAXBElement.class, required = false)
    protected JAXBElement<String> routingProfile;
    protected MeetMeConferencingConferencePorts meetMeConferencingAllocatedPorts;
    protected UnboundedNonNegativeInt trunkGroupMaxActiveCalls;
    protected UnboundedNonNegativeInt trunkGroupBurstingMaxActiveCalls;
    protected ServiceProviderVoiceMessagingGroupSettingsAdd voiceMessagingGroupSettings;
    @XmlSchemaType(name = "token")
    protected ServiceProviderVoicePortalScope voiceMessagingGroupVoicePortalScope;

    /**
     * Ruft den Wert der isEnterprise-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsEnterprise() {
        return isEnterprise;
    }

    /**
     * Legt den Wert der isEnterprise-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsEnterprise(Boolean value) {
        this.isEnterprise = value;
    }

    /**
     * Ruft den Wert der useCustomRoutingProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseCustomRoutingProfile() {
        return useCustomRoutingProfile;
    }

    /**
     * Legt den Wert der useCustomRoutingProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseCustomRoutingProfile(Boolean value) {
        this.useCustomRoutingProfile = value;
    }

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der serviceProviderExternalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderExternalId() {
        return serviceProviderExternalId;
    }

    /**
     * Legt den Wert der serviceProviderExternalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderExternalId(String value) {
        this.serviceProviderExternalId = value;
    }

    /**
     * Ruft den Wert der defaultDomain-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultDomain() {
        return defaultDomain;
    }

    /**
     * Legt den Wert der defaultDomain-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultDomain(String value) {
        this.defaultDomain = value;
    }

    /**
     * Ruft den Wert der serviceProviderName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderName() {
        return serviceProviderName;
    }

    /**
     * Legt den Wert der serviceProviderName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderName(String value) {
        this.serviceProviderName = value;
    }

    /**
     * Ruft den Wert der supportEmail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupportEmail() {
        return supportEmail;
    }

    /**
     * Legt den Wert der supportEmail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupportEmail(String value) {
        this.supportEmail = value;
    }

    /**
     * Ruft den Wert der contact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Contact }
     *     
     */
    public Contact getContact() {
        return contact;
    }

    /**
     * Legt den Wert der contact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Contact }
     *     
     */
    public void setContact(Contact value) {
        this.contact = value;
    }

    /**
     * Ruft den Wert der address-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StreetAddress }
     *     
     */
    public StreetAddress getAddress() {
        return address;
    }

    /**
     * Legt den Wert der address-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetAddress }
     *     
     */
    public void setAddress(StreetAddress value) {
        this.address = value;
    }

    /**
     * Ruft den Wert der servicePolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicePolicy() {
        return servicePolicy;
    }

    /**
     * Legt den Wert der servicePolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicePolicy(String value) {
        this.servicePolicy = value;
    }

    /**
     * Ruft den Wert der callProcessingSliceId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallProcessingSliceId() {
        return callProcessingSliceId;
    }

    /**
     * Legt den Wert der callProcessingSliceId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallProcessingSliceId(String value) {
        this.callProcessingSliceId = value;
    }

    /**
     * Ruft den Wert der provisioningSliceId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvisioningSliceId() {
        return provisioningSliceId;
    }

    /**
     * Legt den Wert der provisioningSliceId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvisioningSliceId(String value) {
        this.provisioningSliceId = value;
    }

    /**
     * Ruft den Wert der subscriberPartition-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberPartition() {
        return subscriberPartition;
    }

    /**
     * Legt den Wert der subscriberPartition-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberPartition(String value) {
        this.subscriberPartition = value;
    }

    /**
     * Ruft den Wert der preferredDataCenter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredDataCenter() {
        return preferredDataCenter;
    }

    /**
     * Legt den Wert der preferredDataCenter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredDataCenter(String value) {
        this.preferredDataCenter = value;
    }

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Ruft den Wert der resellerName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerName() {
        return resellerName;
    }

    /**
     * Legt den Wert der resellerName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerName(String value) {
        this.resellerName = value;
    }

    /**
     * Gets the value of the domain property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the domain property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDomain().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the domain property.
     */
    public List<String> getDomain() {
        if (domain == null) {
            domain = new ArrayList<>();
        }
        return this.domain;
    }

    /**
     * Gets the value of the admin property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the admin property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdmin().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceProviderAdmin }
     * 
     * 
     * @return
     *     The value of the admin property.
     */
    public List<ServiceProviderAdmin> getAdmin() {
        if (admin == null) {
            admin = new ArrayList<>();
        }
        return this.admin;
    }

    /**
     * Gets the value of the groupServiceAuthorization property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the groupServiceAuthorization property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupServiceAuthorization().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupServiceAuthorization }
     * 
     * 
     * @return
     *     The value of the groupServiceAuthorization property.
     */
    public List<GroupServiceAuthorization> getGroupServiceAuthorization() {
        if (groupServiceAuthorization == null) {
            groupServiceAuthorization = new ArrayList<>();
        }
        return this.groupServiceAuthorization;
    }

    /**
     * Gets the value of the userServiceAuthorization property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the userServiceAuthorization property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserServiceAuthorization().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserServiceAuthorization }
     * 
     * 
     * @return
     *     The value of the userServiceAuthorization property.
     */
    public List<UserServiceAuthorization> getUserServiceAuthorization() {
        if (userServiceAuthorization == null) {
            userServiceAuthorization = new ArrayList<>();
        }
        return this.userServiceAuthorization;
    }

    /**
     * Gets the value of the servicePack property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the servicePack property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServicePack().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServicePack }
     * 
     * 
     * @return
     *     The value of the servicePack property.
     */
    public List<ServicePack> getServicePack() {
        if (servicePack == null) {
            servicePack = new ArrayList<>();
        }
        return this.servicePack;
    }

    /**
     * Gets the value of the phoneNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the phoneNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPhoneNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the phoneNumber property.
     */
    public List<String> getPhoneNumber() {
        if (phoneNumber == null) {
            phoneNumber = new ArrayList<>();
        }
        return this.phoneNumber;
    }

    /**
     * Gets the value of the dnRange property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the dnRange property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDnRange().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DNRange }
     * 
     * 
     * @return
     *     The value of the dnRange property.
     */
    public List<DNRange> getDnRange() {
        if (dnRange == null) {
            dnRange = new ArrayList<>();
        }
        return this.dnRange;
    }

    /**
     * Ruft den Wert der routingProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRoutingProfile() {
        return routingProfile;
    }

    /**
     * Legt den Wert der routingProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRoutingProfile(JAXBElement<String> value) {
        this.routingProfile = value;
    }

    /**
     * Ruft den Wert der meetMeConferencingAllocatedPorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferencePorts }
     *     
     */
    public MeetMeConferencingConferencePorts getMeetMeConferencingAllocatedPorts() {
        return meetMeConferencingAllocatedPorts;
    }

    /**
     * Legt den Wert der meetMeConferencingAllocatedPorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferencePorts }
     *     
     */
    public void setMeetMeConferencingAllocatedPorts(MeetMeConferencingConferencePorts value) {
        this.meetMeConferencingAllocatedPorts = value;
    }

    /**
     * Ruft den Wert der trunkGroupMaxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public UnboundedNonNegativeInt getTrunkGroupMaxActiveCalls() {
        return trunkGroupMaxActiveCalls;
    }

    /**
     * Legt den Wert der trunkGroupMaxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public void setTrunkGroupMaxActiveCalls(UnboundedNonNegativeInt value) {
        this.trunkGroupMaxActiveCalls = value;
    }

    /**
     * Ruft den Wert der trunkGroupBurstingMaxActiveCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public UnboundedNonNegativeInt getTrunkGroupBurstingMaxActiveCalls() {
        return trunkGroupBurstingMaxActiveCalls;
    }

    /**
     * Legt den Wert der trunkGroupBurstingMaxActiveCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedNonNegativeInt }
     *     
     */
    public void setTrunkGroupBurstingMaxActiveCalls(UnboundedNonNegativeInt value) {
        this.trunkGroupBurstingMaxActiveCalls = value;
    }

    /**
     * Ruft den Wert der voiceMessagingGroupSettings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderVoiceMessagingGroupSettingsAdd }
     *     
     */
    public ServiceProviderVoiceMessagingGroupSettingsAdd getVoiceMessagingGroupSettings() {
        return voiceMessagingGroupSettings;
    }

    /**
     * Legt den Wert der voiceMessagingGroupSettings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderVoiceMessagingGroupSettingsAdd }
     *     
     */
    public void setVoiceMessagingGroupSettings(ServiceProviderVoiceMessagingGroupSettingsAdd value) {
        this.voiceMessagingGroupSettings = value;
    }

    /**
     * Ruft den Wert der voiceMessagingGroupVoicePortalScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderVoicePortalScope }
     *     
     */
    public ServiceProviderVoicePortalScope getVoiceMessagingGroupVoicePortalScope() {
        return voiceMessagingGroupVoicePortalScope;
    }

    /**
     * Legt den Wert der voiceMessagingGroupVoicePortalScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderVoicePortalScope }
     *     
     */
    public void setVoiceMessagingGroupVoicePortalScope(ServiceProviderVoicePortalScope value) {
        this.voiceMessagingGroupVoicePortalScope = value;
    }

}
