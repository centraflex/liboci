//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with Enhanced Call Logs.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         The following elements are only used in AS data mode:
 *           isSendEnabled
 *           server1NetAddress
 *           server1SendPort
 *           server1RetrievePort
 *           server2NetAddress
 *           server2SendPort
 *           server2RetrievePort
 *           sharedSecret
 *           retransmissionDelayMilliSeconds
 *           maxTransmissions
 *           soapTimeoutSeconds
 *           useDBS
 *           eclQueryApplicationURL
 *           eclQueryDataRepositoryURL
 *       
 * 
 * <p>Java-Klasse für SystemEnhancedCallLogsModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemEnhancedCallLogsModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="isSendEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="server1NetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="server1SendPort" type="{}Port1025" minOccurs="0"/>
 *         <element name="server1RetrievePort" type="{}Port" minOccurs="0"/>
 *         <element name="server2NetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="server2SendPort" type="{}Port1025" minOccurs="0"/>
 *         <element name="server2RetrievePort" type="{}Port" minOccurs="0"/>
 *         <element name="sharedSecret" type="{}EnhancedCallLogsSharedSecret" minOccurs="0"/>
 *         <element name="retransmissionDelayMilliSeconds" type="{}EnhancedCallLogsRetransmissionDelayMilliSeconds" minOccurs="0"/>
 *         <element name="maxTransmissions" type="{}EnhancedCallLogsMaxTransmissions" minOccurs="0"/>
 *         <element name="soapTimeoutSeconds" type="{}EnhancedCallLogsSoapTimeoutSeconds" minOccurs="0"/>
 *         <element name="useDBS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maxNonPagedResponseSize" type="{}EnhancedCallLogsNonPagedResponseSize" minOccurs="0"/>
 *         <element name="eclQueryApplicationURL" type="{}URL" minOccurs="0"/>
 *         <element name="eclQueryDataRepositoryURL" type="{}URL" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemEnhancedCallLogsModifyRequest", propOrder = {
    "isSendEnabled",
    "server1NetAddress",
    "server1SendPort",
    "server1RetrievePort",
    "server2NetAddress",
    "server2SendPort",
    "server2RetrievePort",
    "sharedSecret",
    "retransmissionDelayMilliSeconds",
    "maxTransmissions",
    "soapTimeoutSeconds",
    "useDBS",
    "maxNonPagedResponseSize",
    "eclQueryApplicationURL",
    "eclQueryDataRepositoryURL"
})
public class SystemEnhancedCallLogsModifyRequest
    extends OCIRequest
{

    protected Boolean isSendEnabled;
    @XmlElementRef(name = "server1NetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> server1NetAddress;
    protected Integer server1SendPort;
    protected Integer server1RetrievePort;
    @XmlElementRef(name = "server2NetAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> server2NetAddress;
    protected Integer server2SendPort;
    protected Integer server2RetrievePort;
    @XmlElementRef(name = "sharedSecret", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sharedSecret;
    protected Integer retransmissionDelayMilliSeconds;
    protected Integer maxTransmissions;
    protected Integer soapTimeoutSeconds;
    protected Boolean useDBS;
    protected Integer maxNonPagedResponseSize;
    @XmlElementRef(name = "eclQueryApplicationURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eclQueryApplicationURL;
    @XmlElementRef(name = "eclQueryDataRepositoryURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eclQueryDataRepositoryURL;

    /**
     * Ruft den Wert der isSendEnabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsSendEnabled() {
        return isSendEnabled;
    }

    /**
     * Legt den Wert der isSendEnabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsSendEnabled(Boolean value) {
        this.isSendEnabled = value;
    }

    /**
     * Ruft den Wert der server1NetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServer1NetAddress() {
        return server1NetAddress;
    }

    /**
     * Legt den Wert der server1NetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServer1NetAddress(JAXBElement<String> value) {
        this.server1NetAddress = value;
    }

    /**
     * Ruft den Wert der server1SendPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServer1SendPort() {
        return server1SendPort;
    }

    /**
     * Legt den Wert der server1SendPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServer1SendPort(Integer value) {
        this.server1SendPort = value;
    }

    /**
     * Ruft den Wert der server1RetrievePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServer1RetrievePort() {
        return server1RetrievePort;
    }

    /**
     * Legt den Wert der server1RetrievePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServer1RetrievePort(Integer value) {
        this.server1RetrievePort = value;
    }

    /**
     * Ruft den Wert der server2NetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServer2NetAddress() {
        return server2NetAddress;
    }

    /**
     * Legt den Wert der server2NetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServer2NetAddress(JAXBElement<String> value) {
        this.server2NetAddress = value;
    }

    /**
     * Ruft den Wert der server2SendPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServer2SendPort() {
        return server2SendPort;
    }

    /**
     * Legt den Wert der server2SendPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServer2SendPort(Integer value) {
        this.server2SendPort = value;
    }

    /**
     * Ruft den Wert der server2RetrievePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServer2RetrievePort() {
        return server2RetrievePort;
    }

    /**
     * Legt den Wert der server2RetrievePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServer2RetrievePort(Integer value) {
        this.server2RetrievePort = value;
    }

    /**
     * Ruft den Wert der sharedSecret-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSharedSecret() {
        return sharedSecret;
    }

    /**
     * Legt den Wert der sharedSecret-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSharedSecret(JAXBElement<String> value) {
        this.sharedSecret = value;
    }

    /**
     * Ruft den Wert der retransmissionDelayMilliSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRetransmissionDelayMilliSeconds() {
        return retransmissionDelayMilliSeconds;
    }

    /**
     * Legt den Wert der retransmissionDelayMilliSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRetransmissionDelayMilliSeconds(Integer value) {
        this.retransmissionDelayMilliSeconds = value;
    }

    /**
     * Ruft den Wert der maxTransmissions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxTransmissions() {
        return maxTransmissions;
    }

    /**
     * Legt den Wert der maxTransmissions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxTransmissions(Integer value) {
        this.maxTransmissions = value;
    }

    /**
     * Ruft den Wert der soapTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSoapTimeoutSeconds() {
        return soapTimeoutSeconds;
    }

    /**
     * Legt den Wert der soapTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSoapTimeoutSeconds(Integer value) {
        this.soapTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der useDBS-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseDBS() {
        return useDBS;
    }

    /**
     * Legt den Wert der useDBS-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseDBS(Boolean value) {
        this.useDBS = value;
    }

    /**
     * Ruft den Wert der maxNonPagedResponseSize-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxNonPagedResponseSize() {
        return maxNonPagedResponseSize;
    }

    /**
     * Legt den Wert der maxNonPagedResponseSize-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxNonPagedResponseSize(Integer value) {
        this.maxNonPagedResponseSize = value;
    }

    /**
     * Ruft den Wert der eclQueryApplicationURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEclQueryApplicationURL() {
        return eclQueryApplicationURL;
    }

    /**
     * Legt den Wert der eclQueryApplicationURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEclQueryApplicationURL(JAXBElement<String> value) {
        this.eclQueryApplicationURL = value;
    }

    /**
     * Ruft den Wert der eclQueryDataRepositoryURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEclQueryDataRepositoryURL() {
        return eclQueryDataRepositoryURL;
    }

    /**
     * Legt den Wert der eclQueryDataRepositoryURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEclQueryDataRepositoryURL(JAXBElement<String> value) {
        this.eclQueryDataRepositoryURL = value;
    }

}
