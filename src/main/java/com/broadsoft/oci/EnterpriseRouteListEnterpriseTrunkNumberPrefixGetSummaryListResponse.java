//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to EnterpriseRouteListEnterpriseTrunkNumberPrefixGetSummaryListRequest.
 *         The response contains a table with columns: "Number Prefix", "Group Id", "User Id", 
 *         "Last Name", "First Name", "Hiragana Last Name", "Hiragana First Name", "Phone Number", "Extension", "Department", 
 *         "Email Address", "Enterprise Trunk"","Is Active", “Extension Range Start” and “Extension Range End”.
 *         The "User Id", "Last Name", "First Name", "Hiragana Last Name", "Hiragana First Name", "Phone Number", 
 *         "Extension", "Department" and "Email Address" columns contains the corresponding attributes of the user possessing the number range. 
 *         The "Enterprise Trunk" column contains the enterprise trunk the user possessing the number range belongs to. 
 *         The "Is Active" column indicates if the number range has been activated.
 *         The “Extension Range Start” column indicates the start for an extension range.
 *         The “Extension Range End” column indicates the end for an extension range.
 *       
 * 
 * <p>Java-Klasse für EnterpriseRouteListEnterpriseTrunkNumberPrefixGetSummaryListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseRouteListEnterpriseTrunkNumberPrefixGetSummaryListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="prefixSummaryTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseRouteListEnterpriseTrunkNumberPrefixGetSummaryListResponse", propOrder = {
    "prefixSummaryTable"
})
public class EnterpriseRouteListEnterpriseTrunkNumberPrefixGetSummaryListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable prefixSummaryTable;

    /**
     * Ruft den Wert der prefixSummaryTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getPrefixSummaryTable() {
        return prefixSummaryTable;
    }

    /**
     * Legt den Wert der prefixSummaryTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setPrefixSummaryTable(OCITable value) {
        this.prefixSummaryTable = value;
    }

}
