//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *       Modify the Emergency Call DDos Protection settings.
 *       The response is either SuccessResponse or an ErrorResponse.
 *     
 * 
 * <p>Java-Klasse für SystemEmergencyCallDDoSProtectionModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemEmergencyCallDDoSProtectionModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="enabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="sampleIntervalSeconds" type="{}SampleIntervalInSeconds" minOccurs="0"/>
 *         <element name="protectionRate" type="{}ProtectionRate" minOccurs="0"/>
 *         <element name="protectionAction" type="{}ProtectionAction" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemEmergencyCallDDoSProtectionModifyRequest", propOrder = {
    "enabled",
    "sampleIntervalSeconds",
    "protectionRate",
    "protectionAction"
})
public class SystemEmergencyCallDDoSProtectionModifyRequest
    extends OCIRequest
{

    protected Boolean enabled;
    protected Integer sampleIntervalSeconds;
    @XmlElementRef(name = "protectionRate", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> protectionRate;
    @XmlSchemaType(name = "token")
    protected ProtectionAction protectionAction;

    /**
     * Ruft den Wert der enabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnabled() {
        return enabled;
    }

    /**
     * Legt den Wert der enabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Ruft den Wert der sampleIntervalSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSampleIntervalSeconds() {
        return sampleIntervalSeconds;
    }

    /**
     * Legt den Wert der sampleIntervalSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSampleIntervalSeconds(Integer value) {
        this.sampleIntervalSeconds = value;
    }

    /**
     * Ruft den Wert der protectionRate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getProtectionRate() {
        return protectionRate;
    }

    /**
     * Legt den Wert der protectionRate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setProtectionRate(JAXBElement<Integer> value) {
        this.protectionRate = value;
    }

    /**
     * Ruft den Wert der protectionAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ProtectionAction }
     *     
     */
    public ProtectionAction getProtectionAction() {
        return protectionAction;
    }

    /**
     * Legt den Wert der protectionAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtectionAction }
     *     
     */
    public void setProtectionAction(ProtectionAction value) {
        this.protectionAction = value;
    }

}
