//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modifies the Sh Interface non-transparent data associated with a Public User Identity.
 *         The response is a SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserShInterfaceModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserShInterfaceModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="publicUserIdentity" type="{}PublicUserIdentity"/>
 *         <element name="SCSCFName" type="{}SIPURI" minOccurs="0"/>
 *         <element name="IMSUserState" type="{}IMSUserState" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserShInterfaceModifyRequest", propOrder = {
    "publicUserIdentity",
    "scscfName",
    "imsUserState"
})
public class UserShInterfaceModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    protected PublicUserIdentity publicUserIdentity;
    @XmlElementRef(name = "SCSCFName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> scscfName;
    @XmlElement(name = "IMSUserState")
    @XmlSchemaType(name = "token")
    protected IMSUserState imsUserState;

    /**
     * Ruft den Wert der publicUserIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PublicUserIdentity }
     *     
     */
    public PublicUserIdentity getPublicUserIdentity() {
        return publicUserIdentity;
    }

    /**
     * Legt den Wert der publicUserIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PublicUserIdentity }
     *     
     */
    public void setPublicUserIdentity(PublicUserIdentity value) {
        this.publicUserIdentity = value;
    }

    /**
     * Ruft den Wert der scscfName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSCSCFName() {
        return scscfName;
    }

    /**
     * Legt den Wert der scscfName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSCSCFName(JAXBElement<String> value) {
        this.scscfName = value;
    }

    /**
     * Ruft den Wert der imsUserState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IMSUserState }
     *     
     */
    public IMSUserState getIMSUserState() {
        return imsUserState;
    }

    /**
     * Legt den Wert der imsUserState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IMSUserState }
     *     
     */
    public void setIMSUserState(IMSUserState value) {
        this.imsUserState = value;
    }

}
