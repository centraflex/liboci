//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Used in enhanced call logs group and enterprise queries to restrict the set of result
 *         rows when making a request that can result in a large dataset. The client specifies the
 *         starting row and the number of rows requested. 
 *         The server only provides those rows in results, if available.
 *       
 * 
 * <p>Java-Klasse für EnhancedCallLogsResponsePagingControl complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnhancedCallLogsResponsePagingControl">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="responseStartIndex" type="{}ResponseStartIndex"/>
 *         <element name="responsePageSize" type="{}EnhancedCallLogsResponsePageSize"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnhancedCallLogsResponsePagingControl", propOrder = {
    "responseStartIndex",
    "responsePageSize"
})
public class EnhancedCallLogsResponsePagingControl {

    protected int responseStartIndex;
    protected int responsePageSize;

    /**
     * Ruft den Wert der responseStartIndex-Eigenschaft ab.
     * 
     */
    public int getResponseStartIndex() {
        return responseStartIndex;
    }

    /**
     * Legt den Wert der responseStartIndex-Eigenschaft fest.
     * 
     */
    public void setResponseStartIndex(int value) {
        this.responseStartIndex = value;
    }

    /**
     * Ruft den Wert der responsePageSize-Eigenschaft ab.
     * 
     */
    public int getResponsePageSize() {
        return responsePageSize;
    }

    /**
     * Legt den Wert der responsePageSize-Eigenschaft fest.
     * 
     */
    public void setResponsePageSize(int value) {
        this.responsePageSize = value;
    }

}
