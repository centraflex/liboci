//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallingLineIdentityForRedirectedCalls.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallingLineIdentityForRedirectedCalls">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Originating Identity"/>
 *     <enumeration value="Redirecting User Identity For External Redirections"/>
 *     <enumeration value="Redirecting User Identity For All Redirections"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallingLineIdentityForRedirectedCalls")
@XmlEnum
public enum CallingLineIdentityForRedirectedCalls {

    @XmlEnumValue("Originating Identity")
    ORIGINATING_IDENTITY("Originating Identity"),
    @XmlEnumValue("Redirecting User Identity For External Redirections")
    REDIRECTING_USER_IDENTITY_FOR_EXTERNAL_REDIRECTIONS("Redirecting User Identity For External Redirections"),
    @XmlEnumValue("Redirecting User Identity For All Redirections")
    REDIRECTING_USER_IDENTITY_FOR_ALL_REDIRECTIONS("Redirecting User Identity For All Redirections");
    private final String value;

    CallingLineIdentityForRedirectedCalls(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallingLineIdentityForRedirectedCalls fromValue(String v) {
        for (CallingLineIdentityForRedirectedCalls c: CallingLineIdentityForRedirectedCalls.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
