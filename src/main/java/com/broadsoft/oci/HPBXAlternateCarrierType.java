//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für HPBXAlternateCarrierType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="HPBXAlternateCarrierType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Local And Distant"/>
 *     <enumeration value="Distant"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "HPBXAlternateCarrierType")
@XmlEnum
public enum HPBXAlternateCarrierType {

    @XmlEnumValue("Local And Distant")
    LOCAL_AND_DISTANT("Local And Distant"),
    @XmlEnumValue("Distant")
    DISTANT("Distant");
    private final String value;

    HPBXAlternateCarrierType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HPBXAlternateCarrierType fromValue(String v) {
        for (HPBXAlternateCarrierType c: HPBXAlternateCarrierType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
