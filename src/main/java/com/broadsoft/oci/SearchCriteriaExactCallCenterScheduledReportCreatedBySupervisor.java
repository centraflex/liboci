//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Criteria for searching for call center scheduled report created by a
 *         supervisor or administrator.
 *       
 * 
 * <p>Java-Klasse für SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor">
 *   <complexContent>
 *     <extension base="{}SearchCriteria">
 *       <sequence>
 *         <element name="createdBySupervisor" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor", propOrder = {
    "createdBySupervisor"
})
public class SearchCriteriaExactCallCenterScheduledReportCreatedBySupervisor
    extends SearchCriteria
{

    protected boolean createdBySupervisor;

    /**
     * Ruft den Wert der createdBySupervisor-Eigenschaft ab.
     * 
     */
    public boolean isCreatedBySupervisor() {
        return createdBySupervisor;
    }

    /**
     * Legt den Wert der createdBySupervisor-Eigenschaft fest.
     * 
     */
    public void setCreatedBySupervisor(boolean value) {
        this.createdBySupervisor = value;
    }

}
