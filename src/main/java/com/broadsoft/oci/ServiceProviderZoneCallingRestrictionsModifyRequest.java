//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modifies the Service Provider/Enterprise level Zone Calling Restrictions Policies.
 *         The response is SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ServiceProviderZoneCallingRestrictionsModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderZoneCallingRestrictionsModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="enableZoneCallingRestrictions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableOriginationRoamingRestrictions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableEmergencyOriginationRoamingRestrictions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableTerminationRoamingRestrictions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderZoneCallingRestrictionsModifyRequest", propOrder = {
    "serviceProviderId",
    "enableZoneCallingRestrictions",
    "enableOriginationRoamingRestrictions",
    "enableEmergencyOriginationRoamingRestrictions",
    "enableTerminationRoamingRestrictions"
})
public class ServiceProviderZoneCallingRestrictionsModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    protected Boolean enableZoneCallingRestrictions;
    protected Boolean enableOriginationRoamingRestrictions;
    protected Boolean enableEmergencyOriginationRoamingRestrictions;
    protected Boolean enableTerminationRoamingRestrictions;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der enableZoneCallingRestrictions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableZoneCallingRestrictions() {
        return enableZoneCallingRestrictions;
    }

    /**
     * Legt den Wert der enableZoneCallingRestrictions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableZoneCallingRestrictions(Boolean value) {
        this.enableZoneCallingRestrictions = value;
    }

    /**
     * Ruft den Wert der enableOriginationRoamingRestrictions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableOriginationRoamingRestrictions() {
        return enableOriginationRoamingRestrictions;
    }

    /**
     * Legt den Wert der enableOriginationRoamingRestrictions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableOriginationRoamingRestrictions(Boolean value) {
        this.enableOriginationRoamingRestrictions = value;
    }

    /**
     * Ruft den Wert der enableEmergencyOriginationRoamingRestrictions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableEmergencyOriginationRoamingRestrictions() {
        return enableEmergencyOriginationRoamingRestrictions;
    }

    /**
     * Legt den Wert der enableEmergencyOriginationRoamingRestrictions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableEmergencyOriginationRoamingRestrictions(Boolean value) {
        this.enableEmergencyOriginationRoamingRestrictions = value;
    }

    /**
     * Ruft den Wert der enableTerminationRoamingRestrictions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableTerminationRoamingRestrictions() {
        return enableTerminationRoamingRestrictions;
    }

    /**
     * Legt den Wert der enableTerminationRoamingRestrictions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableTerminationRoamingRestrictions(Boolean value) {
        this.enableTerminationRoamingRestrictions = value;
    }

}
