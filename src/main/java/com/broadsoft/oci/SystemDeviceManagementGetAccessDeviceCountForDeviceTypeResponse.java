//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemDeviceManagementGetAccessDeviceCountForDeviceTypeRequest.
 *       
 * 
 * <p>Java-Klasse für SystemDeviceManagementGetAccessDeviceCountForDeviceTypeResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceManagementGetAccessDeviceCountForDeviceTypeResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="accessDeviceCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceManagementGetAccessDeviceCountForDeviceTypeResponse", propOrder = {
    "accessDeviceCount"
})
public class SystemDeviceManagementGetAccessDeviceCountForDeviceTypeResponse
    extends OCIDataResponse
{

    protected int accessDeviceCount;

    /**
     * Ruft den Wert der accessDeviceCount-Eigenschaft ab.
     * 
     */
    public int getAccessDeviceCount() {
        return accessDeviceCount;
    }

    /**
     * Legt den Wert der accessDeviceCount-Eigenschaft fest.
     * 
     */
    public void setAccessDeviceCount(int value) {
        this.accessDeviceCount = value;
    }

}
