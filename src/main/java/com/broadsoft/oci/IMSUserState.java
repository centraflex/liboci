//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für IMSUserState.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="IMSUserState">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Not Registered"/>
 *     <enumeration value="Registered"/>
 *     <enumeration value="Registered UnReg Services"/>
 *     <enumeration value="Authentication Pending"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "IMSUserState")
@XmlEnum
public enum IMSUserState {

    @XmlEnumValue("Not Registered")
    NOT_REGISTERED("Not Registered"),
    @XmlEnumValue("Registered")
    REGISTERED("Registered"),
    @XmlEnumValue("Registered UnReg Services")
    REGISTERED_UN_REG_SERVICES("Registered UnReg Services"),
    @XmlEnumValue("Authentication Pending")
    AUTHENTICATION_PENDING("Authentication Pending");
    private final String value;

    IMSUserState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static IMSUserState fromValue(String v) {
        for (IMSUserState c: IMSUserState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
