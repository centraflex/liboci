//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SelectiveCallRejectionCriteriaCallTypeSelection.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="SelectiveCallRejectionCriteriaCallTypeSelection">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Any"/>
 *     <enumeration value="Forwarded"/>
 *     <enumeration value="Specified Only"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "SelectiveCallRejectionCriteriaCallTypeSelection")
@XmlEnum
public enum SelectiveCallRejectionCriteriaCallTypeSelection {

    @XmlEnumValue("Any")
    ANY("Any"),
    @XmlEnumValue("Forwarded")
    FORWARDED("Forwarded"),
    @XmlEnumValue("Specified Only")
    SPECIFIED_ONLY("Specified Only");
    private final String value;

    SelectiveCallRejectionCriteriaCallTypeSelection(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SelectiveCallRejectionCriteriaCallTypeSelection fromValue(String v) {
        for (SelectiveCallRejectionCriteriaCallTypeSelection c: SelectiveCallRejectionCriteriaCallTypeSelection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
