//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the user's commPilot express service setting.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         Engineering Note: This command is used internally by Call Processing.
 *       
 * 
 * <p>Java-Klasse für UserCommPilotExpressModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCommPilotExpressModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="profile" type="{}CommPilotExpressProfile" minOccurs="0"/>
 *         <element name="availableInOffice" type="{}CommPilotExpressAvailableInOfficeModify" minOccurs="0"/>
 *         <element name="availableOutOfOffice" type="{}CommPilotExpressAvailableOutOfOfficeModify" minOccurs="0"/>
 *         <element name="busy" type="{}CommPilotExpressBusyModify" minOccurs="0"/>
 *         <element name="unavailable" type="{}CommPilotExpressUnavailableModify" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCommPilotExpressModifyRequest", propOrder = {
    "userId",
    "profile",
    "availableInOffice",
    "availableOutOfOffice",
    "busy",
    "unavailable"
})
public class UserCommPilotExpressModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlElementRef(name = "profile", type = JAXBElement.class, required = false)
    protected JAXBElement<CommPilotExpressProfile> profile;
    protected CommPilotExpressAvailableInOfficeModify availableInOffice;
    protected CommPilotExpressAvailableOutOfOfficeModify availableOutOfOffice;
    protected CommPilotExpressBusyModify busy;
    protected CommPilotExpressUnavailableModify unavailable;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der profile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CommPilotExpressProfile }{@code >}
     *     
     */
    public JAXBElement<CommPilotExpressProfile> getProfile() {
        return profile;
    }

    /**
     * Legt den Wert der profile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CommPilotExpressProfile }{@code >}
     *     
     */
    public void setProfile(JAXBElement<CommPilotExpressProfile> value) {
        this.profile = value;
    }

    /**
     * Ruft den Wert der availableInOffice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressAvailableInOfficeModify }
     *     
     */
    public CommPilotExpressAvailableInOfficeModify getAvailableInOffice() {
        return availableInOffice;
    }

    /**
     * Legt den Wert der availableInOffice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressAvailableInOfficeModify }
     *     
     */
    public void setAvailableInOffice(CommPilotExpressAvailableInOfficeModify value) {
        this.availableInOffice = value;
    }

    /**
     * Ruft den Wert der availableOutOfOffice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressAvailableOutOfOfficeModify }
     *     
     */
    public CommPilotExpressAvailableOutOfOfficeModify getAvailableOutOfOffice() {
        return availableOutOfOffice;
    }

    /**
     * Legt den Wert der availableOutOfOffice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressAvailableOutOfOfficeModify }
     *     
     */
    public void setAvailableOutOfOffice(CommPilotExpressAvailableOutOfOfficeModify value) {
        this.availableOutOfOffice = value;
    }

    /**
     * Ruft den Wert der busy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressBusyModify }
     *     
     */
    public CommPilotExpressBusyModify getBusy() {
        return busy;
    }

    /**
     * Legt den Wert der busy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressBusyModify }
     *     
     */
    public void setBusy(CommPilotExpressBusyModify value) {
        this.busy = value;
    }

    /**
     * Ruft den Wert der unavailable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommPilotExpressUnavailableModify }
     *     
     */
    public CommPilotExpressUnavailableModify getUnavailable() {
        return unavailable;
    }

    /**
     * Legt den Wert der unavailable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommPilotExpressUnavailableModify }
     *     
     */
    public void setUnavailable(CommPilotExpressUnavailableModify value) {
        this.unavailable = value;
    }

}
