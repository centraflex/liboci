//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für IncomingCallingPlanOutsideCallPermission.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="IncomingCallingPlanOutsideCallPermission">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Allow"/>
 *     <enumeration value="Allow Only If Redirected From Another User"/>
 *     <enumeration value="Disallow"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "IncomingCallingPlanOutsideCallPermission")
@XmlEnum
public enum IncomingCallingPlanOutsideCallPermission {

    @XmlEnumValue("Allow")
    ALLOW("Allow"),
    @XmlEnumValue("Allow Only If Redirected From Another User")
    ALLOW_ONLY_IF_REDIRECTED_FROM_ANOTHER_USER("Allow Only If Redirected From Another User"),
    @XmlEnumValue("Disallow")
    DISALLOW("Disallow");
    private final String value;

    IncomingCallingPlanOutsideCallPermission(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static IncomingCallingPlanOutsideCallPermission fromValue(String v) {
        for (IncomingCallingPlanOutsideCallPermission c: IncomingCallingPlanOutsideCallPermission.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
