//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The response to a SystemBroadWorksMobilityGetRequest17sp4.
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksMobilityGetResponse17sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksMobilityGetResponse17sp4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enableLocationServices" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableMSRNLookup" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableMobileStateChecking" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="denyCallOriginations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="denyCallTerminations" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="imrnTimeoutMillisecnds" type="{}IMRNTimeoutMilliseconds"/>
 *         <element name="scfSignalingNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="scfSignalingPort" type="{}Port" minOccurs="0"/>
 *         <element name="refreshPeriodSeconds" type="{}SCFRefreshPeriodSeconds"/>
 *         <element name="maxConsecutiveFailures" type="{}SCFMaxConsecutiveFailures"/>
 *         <element name="maxResponseWaitTimeMilliseconds" type="{}SCFMaxResponseWaitTimeMilliseconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksMobilityGetResponse17sp4", propOrder = {
    "enableLocationServices",
    "enableMSRNLookup",
    "enableMobileStateChecking",
    "denyCallOriginations",
    "denyCallTerminations",
    "imrnTimeoutMillisecnds",
    "scfSignalingNetAddress",
    "scfSignalingPort",
    "refreshPeriodSeconds",
    "maxConsecutiveFailures",
    "maxResponseWaitTimeMilliseconds"
})
public class SystemBroadWorksMobilityGetResponse17Sp4
    extends OCIDataResponse
{

    protected boolean enableLocationServices;
    protected boolean enableMSRNLookup;
    protected boolean enableMobileStateChecking;
    protected boolean denyCallOriginations;
    protected boolean denyCallTerminations;
    protected int imrnTimeoutMillisecnds;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String scfSignalingNetAddress;
    protected Integer scfSignalingPort;
    protected int refreshPeriodSeconds;
    protected int maxConsecutiveFailures;
    protected int maxResponseWaitTimeMilliseconds;

    /**
     * Ruft den Wert der enableLocationServices-Eigenschaft ab.
     * 
     */
    public boolean isEnableLocationServices() {
        return enableLocationServices;
    }

    /**
     * Legt den Wert der enableLocationServices-Eigenschaft fest.
     * 
     */
    public void setEnableLocationServices(boolean value) {
        this.enableLocationServices = value;
    }

    /**
     * Ruft den Wert der enableMSRNLookup-Eigenschaft ab.
     * 
     */
    public boolean isEnableMSRNLookup() {
        return enableMSRNLookup;
    }

    /**
     * Legt den Wert der enableMSRNLookup-Eigenschaft fest.
     * 
     */
    public void setEnableMSRNLookup(boolean value) {
        this.enableMSRNLookup = value;
    }

    /**
     * Ruft den Wert der enableMobileStateChecking-Eigenschaft ab.
     * 
     */
    public boolean isEnableMobileStateChecking() {
        return enableMobileStateChecking;
    }

    /**
     * Legt den Wert der enableMobileStateChecking-Eigenschaft fest.
     * 
     */
    public void setEnableMobileStateChecking(boolean value) {
        this.enableMobileStateChecking = value;
    }

    /**
     * Ruft den Wert der denyCallOriginations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallOriginations() {
        return denyCallOriginations;
    }

    /**
     * Legt den Wert der denyCallOriginations-Eigenschaft fest.
     * 
     */
    public void setDenyCallOriginations(boolean value) {
        this.denyCallOriginations = value;
    }

    /**
     * Ruft den Wert der denyCallTerminations-Eigenschaft ab.
     * 
     */
    public boolean isDenyCallTerminations() {
        return denyCallTerminations;
    }

    /**
     * Legt den Wert der denyCallTerminations-Eigenschaft fest.
     * 
     */
    public void setDenyCallTerminations(boolean value) {
        this.denyCallTerminations = value;
    }

    /**
     * Ruft den Wert der imrnTimeoutMillisecnds-Eigenschaft ab.
     * 
     */
    public int getImrnTimeoutMillisecnds() {
        return imrnTimeoutMillisecnds;
    }

    /**
     * Legt den Wert der imrnTimeoutMillisecnds-Eigenschaft fest.
     * 
     */
    public void setImrnTimeoutMillisecnds(int value) {
        this.imrnTimeoutMillisecnds = value;
    }

    /**
     * Ruft den Wert der scfSignalingNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScfSignalingNetAddress() {
        return scfSignalingNetAddress;
    }

    /**
     * Legt den Wert der scfSignalingNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScfSignalingNetAddress(String value) {
        this.scfSignalingNetAddress = value;
    }

    /**
     * Ruft den Wert der scfSignalingPort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getScfSignalingPort() {
        return scfSignalingPort;
    }

    /**
     * Legt den Wert der scfSignalingPort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setScfSignalingPort(Integer value) {
        this.scfSignalingPort = value;
    }

    /**
     * Ruft den Wert der refreshPeriodSeconds-Eigenschaft ab.
     * 
     */
    public int getRefreshPeriodSeconds() {
        return refreshPeriodSeconds;
    }

    /**
     * Legt den Wert der refreshPeriodSeconds-Eigenschaft fest.
     * 
     */
    public void setRefreshPeriodSeconds(int value) {
        this.refreshPeriodSeconds = value;
    }

    /**
     * Ruft den Wert der maxConsecutiveFailures-Eigenschaft ab.
     * 
     */
    public int getMaxConsecutiveFailures() {
        return maxConsecutiveFailures;
    }

    /**
     * Legt den Wert der maxConsecutiveFailures-Eigenschaft fest.
     * 
     */
    public void setMaxConsecutiveFailures(int value) {
        this.maxConsecutiveFailures = value;
    }

    /**
     * Ruft den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft ab.
     * 
     */
    public int getMaxResponseWaitTimeMilliseconds() {
        return maxResponseWaitTimeMilliseconds;
    }

    /**
     * Legt den Wert der maxResponseWaitTimeMilliseconds-Eigenschaft fest.
     * 
     */
    public void setMaxResponseWaitTimeMilliseconds(int value) {
        this.maxResponseWaitTimeMilliseconds = value;
    }

}
