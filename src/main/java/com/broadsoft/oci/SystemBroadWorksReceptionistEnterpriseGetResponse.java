//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemBroadWorksReceptionistEnterpriseGetRequest.
 *         Contains a list of BroadWorks Receptionist - Enterprise parameters.
 *       
 * 
 * <p>Java-Klasse für SystemBroadWorksReceptionistEnterpriseGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBroadWorksReceptionistEnterpriseGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maxMonitoredUsers" type="{}MaximumMonitoredUsers"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBroadWorksReceptionistEnterpriseGetResponse", propOrder = {
    "maxMonitoredUsers"
})
public class SystemBroadWorksReceptionistEnterpriseGetResponse
    extends OCIDataResponse
{

    protected int maxMonitoredUsers;

    /**
     * Ruft den Wert der maxMonitoredUsers-Eigenschaft ab.
     * 
     */
    public int getMaxMonitoredUsers() {
        return maxMonitoredUsers;
    }

    /**
     * Legt den Wert der maxMonitoredUsers-Eigenschaft fest.
     * 
     */
    public void setMaxMonitoredUsers(int value) {
        this.maxMonitoredUsers = value;
    }

}
