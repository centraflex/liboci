//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserAnnouncementFileGetAvailableListRequest.
 *         The response contains a table with columns: "Name", "Media Type", "File Size", "Repository Type", and "Announcement File External Id".
 *         The "Name" column contains the name of the announcement file.
 *         The "Media Type" column contains the media type of the announcement file with the possible values:
 *                 WMA - Windows Media Audio file
 *                 WAV - A WAV file
 *                 3GP - A 3GP file
 *                 MOV - A MOV file using a H.263 or H.264 codec.
 *         The "File Size" is in Kilobytes.
 *         The "Repository Type" column contains the type of repository for the announcement file such as "User" or "Group"
 *         The "File Size" column contains the file size in kB of the announcement file.
 *         The "Announcement File External Id" column contains the External Id of the announcement file.
 *  
 *         The following columns are populated in AS data mode only:       
 *           "Announcement File External Id"
 *       
 * 
 * <p>Java-Klasse für UserAnnouncementFileGetAvailableListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserAnnouncementFileGetAvailableListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="announcementTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserAnnouncementFileGetAvailableListResponse", propOrder = {
    "announcementTable"
})
public class UserAnnouncementFileGetAvailableListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable announcementTable;

    /**
     * Ruft den Wert der announcementTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAnnouncementTable() {
        return announcementTable;
    }

    /**
     * Legt den Wert der announcementTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAnnouncementTable(OCITable value) {
        this.announcementTable = value;
    }

}
