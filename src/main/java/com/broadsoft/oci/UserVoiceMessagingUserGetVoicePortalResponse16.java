//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to UserVoiceMessagingUserGetVoicePortalRequest16.
 *       
 * 
 * <p>Java-Klasse für UserVoiceMessagingUserGetVoicePortalResponse16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserVoiceMessagingUserGetVoicePortalResponse16">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="usePersonalizedName" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="voicePortalAutoLogin" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="personalizedNameAudioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="personalizedNameMediaType" type="{}MediaFileType" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserVoiceMessagingUserGetVoicePortalResponse16", propOrder = {
    "usePersonalizedName",
    "voicePortalAutoLogin",
    "personalizedNameAudioFileDescription",
    "personalizedNameMediaType"
})
public class UserVoiceMessagingUserGetVoicePortalResponse16
    extends OCIDataResponse
{

    protected boolean usePersonalizedName;
    protected boolean voicePortalAutoLogin;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String personalizedNameAudioFileDescription;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String personalizedNameMediaType;

    /**
     * Ruft den Wert der usePersonalizedName-Eigenschaft ab.
     * 
     */
    public boolean isUsePersonalizedName() {
        return usePersonalizedName;
    }

    /**
     * Legt den Wert der usePersonalizedName-Eigenschaft fest.
     * 
     */
    public void setUsePersonalizedName(boolean value) {
        this.usePersonalizedName = value;
    }

    /**
     * Ruft den Wert der voicePortalAutoLogin-Eigenschaft ab.
     * 
     */
    public boolean isVoicePortalAutoLogin() {
        return voicePortalAutoLogin;
    }

    /**
     * Legt den Wert der voicePortalAutoLogin-Eigenschaft fest.
     * 
     */
    public void setVoicePortalAutoLogin(boolean value) {
        this.voicePortalAutoLogin = value;
    }

    /**
     * Ruft den Wert der personalizedNameAudioFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalizedNameAudioFileDescription() {
        return personalizedNameAudioFileDescription;
    }

    /**
     * Legt den Wert der personalizedNameAudioFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalizedNameAudioFileDescription(String value) {
        this.personalizedNameAudioFileDescription = value;
    }

    /**
     * Ruft den Wert der personalizedNameMediaType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalizedNameMediaType() {
        return personalizedNameMediaType;
    }

    /**
     * Legt den Wert der personalizedNameMediaType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalizedNameMediaType(String value) {
        this.personalizedNameMediaType = value;
    }

}
