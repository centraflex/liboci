//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to GroupCollaborateBridgeGetInstanceRequest.
 *         The system-level collaborate supportOutdial setting is returned in the response when the system-level collaborate 
 *         supportOutdial setting is disabled. 
 *         Contains the service profile information and a table of assigned owners.
 *         The table has column headings: "User Id", "Last Name", "First Name", "Hiragana Last Name", 
 *         "Hiragana First Name", Phone Number", "Extension", "Department", "Email Address".
 *         Collaborate bridge maximum participant's choices unlimited or a quantified number of participants.
 *         
 *         Replaced by: GroupCollaborateBridgeGetInstanceResponse20sp1
 *       
 * 
 * <p>Java-Klasse für GroupCollaborateBridgeGetInstanceResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCollaborateBridgeGetInstanceResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceReadProfile19sp1"/>
 *         <element name="maximumBridgeParticipants" type="{}CollaborateBridgeMaximumParticipants"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *         <element name="isDefault" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxCollaborateRoomParticipants" type="{}CollaborateRoomMaximumParticipants"/>
 *         <element name="supportOutdial" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="collaborateOwnerUserTable" type="{C}OCITable" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCollaborateBridgeGetInstanceResponse", propOrder = {
    "serviceInstanceProfile",
    "maximumBridgeParticipants",
    "networkClassOfService",
    "isDefault",
    "maxCollaborateRoomParticipants",
    "supportOutdial",
    "collaborateOwnerUserTable"
})
public class GroupCollaborateBridgeGetInstanceResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected ServiceInstanceReadProfile19Sp1 serviceInstanceProfile;
    @XmlElement(required = true)
    protected CollaborateBridgeMaximumParticipants maximumBridgeParticipants;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;
    protected boolean isDefault;
    protected int maxCollaborateRoomParticipants;
    protected boolean supportOutdial;
    @XmlElementRef(name = "collaborateOwnerUserTable", type = JAXBElement.class, required = false)
    protected JAXBElement<OCITable> collaborateOwnerUserTable;

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceReadProfile19Sp1 }
     *     
     */
    public ServiceInstanceReadProfile19Sp1 getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceReadProfile19Sp1 }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceReadProfile19Sp1 value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der maximumBridgeParticipants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CollaborateBridgeMaximumParticipants }
     *     
     */
    public CollaborateBridgeMaximumParticipants getMaximumBridgeParticipants() {
        return maximumBridgeParticipants;
    }

    /**
     * Legt den Wert der maximumBridgeParticipants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CollaborateBridgeMaximumParticipants }
     *     
     */
    public void setMaximumBridgeParticipants(CollaborateBridgeMaximumParticipants value) {
        this.maximumBridgeParticipants = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

    /**
     * Ruft den Wert der isDefault-Eigenschaft ab.
     * 
     */
    public boolean isIsDefault() {
        return isDefault;
    }

    /**
     * Legt den Wert der isDefault-Eigenschaft fest.
     * 
     */
    public void setIsDefault(boolean value) {
        this.isDefault = value;
    }

    /**
     * Ruft den Wert der maxCollaborateRoomParticipants-Eigenschaft ab.
     * 
     */
    public int getMaxCollaborateRoomParticipants() {
        return maxCollaborateRoomParticipants;
    }

    /**
     * Legt den Wert der maxCollaborateRoomParticipants-Eigenschaft fest.
     * 
     */
    public void setMaxCollaborateRoomParticipants(int value) {
        this.maxCollaborateRoomParticipants = value;
    }

    /**
     * Ruft den Wert der supportOutdial-Eigenschaft ab.
     * 
     */
    public boolean isSupportOutdial() {
        return supportOutdial;
    }

    /**
     * Legt den Wert der supportOutdial-Eigenschaft fest.
     * 
     */
    public void setSupportOutdial(boolean value) {
        this.supportOutdial = value;
    }

    /**
     * Ruft den Wert der collaborateOwnerUserTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OCITable }{@code >}
     *     
     */
    public JAXBElement<OCITable> getCollaborateOwnerUserTable() {
        return collaborateOwnerUserTable;
    }

    /**
     * Legt den Wert der collaborateOwnerUserTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OCITable }{@code >}
     *     
     */
    public void setCollaborateOwnerUserTable(JAXBElement<OCITable> value) {
        this.collaborateOwnerUserTable = value;
    }

}
