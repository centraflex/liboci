//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Assign a list of Network Classes of Service to a group.
 *         A default Network Class of Service must be specified unless there is already one assigned to the group.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupNetworkClassOfServiceAssignListRequest21 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupNetworkClassOfServiceAssignListRequest21">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="defaultNetworkClassOfService" type="{}DefaultNetworkClassOfService"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupNetworkClassOfServiceAssignListRequest21", propOrder = {
    "serviceProviderId",
    "groupId",
    "networkClassOfService",
    "defaultNetworkClassOfService"
})
public class GroupNetworkClassOfServiceAssignListRequest21
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> networkClassOfService;
    @XmlElement(required = true)
    protected DefaultNetworkClassOfService defaultNetworkClassOfService;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Gets the value of the networkClassOfService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the networkClassOfService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNetworkClassOfService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the networkClassOfService property.
     */
    public List<String> getNetworkClassOfService() {
        if (networkClassOfService == null) {
            networkClassOfService = new ArrayList<>();
        }
        return this.networkClassOfService;
    }

    /**
     * Ruft den Wert der defaultNetworkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DefaultNetworkClassOfService }
     *     
     */
    public DefaultNetworkClassOfService getDefaultNetworkClassOfService() {
        return defaultNetworkClassOfService;
    }

    /**
     * Legt den Wert der defaultNetworkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DefaultNetworkClassOfService }
     *     
     */
    public void setDefaultNetworkClassOfService(DefaultNetworkClassOfService value) {
        this.defaultNetworkClassOfService = value;
    }

}
