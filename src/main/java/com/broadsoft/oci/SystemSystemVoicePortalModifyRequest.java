//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to update a System  Voice portal instance.
 *         The response is either SuccessResponse or ErrorResponse.
 *         
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *             networkClassOfService        
 *       
 * 
 * <p>Java-Klasse für SystemSystemVoicePortalModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemSystemVoicePortalModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="systemVoicePortalId" type="{}UserId"/>
 *         <element name="newSystemVoicePortalId" type="{}UserId" minOccurs="0"/>
 *         <element name="name" type="{}SystemServiceName" minOccurs="0"/>
 *         <element name="callingLineIdName" type="{}SystemServiceCallingLineIdName" minOccurs="0"/>
 *         <element name="language" type="{}Language" minOccurs="0"/>
 *         <element name="timeZone" type="{}TimeZone" minOccurs="0"/>
 *         <element name="phoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="publicUserIdentity" type="{}SIPURI" minOccurs="0"/>
 *         <element name="networkVoicePortalNumber" type="{}DN" minOccurs="0"/>
 *         <element name="allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useVoicePortalWizard" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="becomeDefaultSystemVoicePortal" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="useVoicePortalDefaultGreeting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="voicePortalGreetingFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *         <element name="useVoiceMessagingDefaultGreeting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="voiceMessagingGreetingFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *         <element name="expressMode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemSystemVoicePortalModifyRequest", propOrder = {
    "systemVoicePortalId",
    "newSystemVoicePortalId",
    "name",
    "callingLineIdName",
    "language",
    "timeZone",
    "phoneNumber",
    "publicUserIdentity",
    "networkVoicePortalNumber",
    "allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin",
    "useVoicePortalWizard",
    "becomeDefaultSystemVoicePortal",
    "useVoicePortalDefaultGreeting",
    "voicePortalGreetingFile",
    "useVoiceMessagingDefaultGreeting",
    "voiceMessagingGreetingFile",
    "expressMode",
    "networkClassOfService"
})
public class SystemSystemVoicePortalModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String systemVoicePortalId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newSystemVoicePortalId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callingLineIdName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String language;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeZone;
    @XmlElementRef(name = "phoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> phoneNumber;
    @XmlElementRef(name = "publicUserIdentity", type = JAXBElement.class, required = false)
    protected JAXBElement<String> publicUserIdentity;
    @XmlElementRef(name = "networkVoicePortalNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> networkVoicePortalNumber;
    protected Boolean allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin;
    protected Boolean useVoicePortalWizard;
    protected Boolean becomeDefaultSystemVoicePortal;
    protected Boolean useVoicePortalDefaultGreeting;
    protected LabeledMediaFileResource voicePortalGreetingFile;
    protected Boolean useVoiceMessagingDefaultGreeting;
    protected LabeledMediaFileResource voiceMessagingGreetingFile;
    protected Boolean expressMode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;

    /**
     * Ruft den Wert der systemVoicePortalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemVoicePortalId() {
        return systemVoicePortalId;
    }

    /**
     * Legt den Wert der systemVoicePortalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemVoicePortalId(String value) {
        this.systemVoicePortalId = value;
    }

    /**
     * Ruft den Wert der newSystemVoicePortalId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewSystemVoicePortalId() {
        return newSystemVoicePortalId;
    }

    /**
     * Legt den Wert der newSystemVoicePortalId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewSystemVoicePortalId(String value) {
        this.newSystemVoicePortalId = value;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der callingLineIdName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallingLineIdName() {
        return callingLineIdName;
    }

    /**
     * Legt den Wert der callingLineIdName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallingLineIdName(String value) {
        this.callingLineIdName = value;
    }

    /**
     * Ruft den Wert der language-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Legt den Wert der language-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Ruft den Wert der timeZone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * Legt den Wert der timeZone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeZone(String value) {
        this.timeZone = value;
    }

    /**
     * Ruft den Wert der phoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Legt den Wert der phoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPhoneNumber(JAXBElement<String> value) {
        this.phoneNumber = value;
    }

    /**
     * Ruft den Wert der publicUserIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPublicUserIdentity() {
        return publicUserIdentity;
    }

    /**
     * Legt den Wert der publicUserIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPublicUserIdentity(JAXBElement<String> value) {
        this.publicUserIdentity = value;
    }

    /**
     * Ruft den Wert der networkVoicePortalNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNetworkVoicePortalNumber() {
        return networkVoicePortalNumber;
    }

    /**
     * Legt den Wert der networkVoicePortalNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNetworkVoicePortalNumber(JAXBElement<String> value) {
        this.networkVoicePortalNumber = value;
    }

    /**
     * Ruft den Wert der allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin() {
        return allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin;
    }

    /**
     * Legt den Wert der allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin(Boolean value) {
        this.allowIdentificationByPhoneNumberOrVoiceMailAliasesOnLogin = value;
    }

    /**
     * Ruft den Wert der useVoicePortalWizard-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseVoicePortalWizard() {
        return useVoicePortalWizard;
    }

    /**
     * Legt den Wert der useVoicePortalWizard-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseVoicePortalWizard(Boolean value) {
        this.useVoicePortalWizard = value;
    }

    /**
     * Ruft den Wert der becomeDefaultSystemVoicePortal-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBecomeDefaultSystemVoicePortal() {
        return becomeDefaultSystemVoicePortal;
    }

    /**
     * Legt den Wert der becomeDefaultSystemVoicePortal-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBecomeDefaultSystemVoicePortal(Boolean value) {
        this.becomeDefaultSystemVoicePortal = value;
    }

    /**
     * Ruft den Wert der useVoicePortalDefaultGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseVoicePortalDefaultGreeting() {
        return useVoicePortalDefaultGreeting;
    }

    /**
     * Legt den Wert der useVoicePortalDefaultGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseVoicePortalDefaultGreeting(Boolean value) {
        this.useVoicePortalDefaultGreeting = value;
    }

    /**
     * Ruft den Wert der voicePortalGreetingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public LabeledMediaFileResource getVoicePortalGreetingFile() {
        return voicePortalGreetingFile;
    }

    /**
     * Legt den Wert der voicePortalGreetingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public void setVoicePortalGreetingFile(LabeledMediaFileResource value) {
        this.voicePortalGreetingFile = value;
    }

    /**
     * Ruft den Wert der useVoiceMessagingDefaultGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseVoiceMessagingDefaultGreeting() {
        return useVoiceMessagingDefaultGreeting;
    }

    /**
     * Legt den Wert der useVoiceMessagingDefaultGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseVoiceMessagingDefaultGreeting(Boolean value) {
        this.useVoiceMessagingDefaultGreeting = value;
    }

    /**
     * Ruft den Wert der voiceMessagingGreetingFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public LabeledMediaFileResource getVoiceMessagingGreetingFile() {
        return voiceMessagingGreetingFile;
    }

    /**
     * Legt den Wert der voiceMessagingGreetingFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public void setVoiceMessagingGreetingFile(LabeledMediaFileResource value) {
        this.voiceMessagingGreetingFile = value;
    }

    /**
     * Ruft den Wert der expressMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExpressMode() {
        return expressMode;
    }

    /**
     * Legt den Wert der expressMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExpressMode(Boolean value) {
        this.expressMode = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

}
