//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DeviceAccessProtocol.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="DeviceAccessProtocol">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Http"/>
 *     <enumeration value="Https"/>
 *     <enumeration value="FTP"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "DeviceAccessProtocol")
@XmlEnum
public enum DeviceAccessProtocol {

    @XmlEnumValue("Http")
    HTTP("Http"),
    @XmlEnumValue("Https")
    HTTPS("Https"),
    FTP("FTP");
    private final String value;

    DeviceAccessProtocol(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeviceAccessProtocol fromValue(String v) {
        for (DeviceAccessProtocol c: DeviceAccessProtocol.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
