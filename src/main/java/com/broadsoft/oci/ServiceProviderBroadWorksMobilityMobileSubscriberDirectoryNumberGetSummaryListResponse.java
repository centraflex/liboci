//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to ServiceProviderBroadWorksMobilityMobileSubscriberDirectoryNumberGetSummaryListRequest.
 *         The column headings are "Phone Number", "Group Id" and "Mobile Network". 
 *       
 * 
 * <p>Java-Klasse für ServiceProviderBroadWorksMobilityMobileSubscriberDirectoryNumberGetSummaryListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderBroadWorksMobilityMobileSubscriberDirectoryNumberGetSummaryListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="mobileSubscriberDirectoryNumbersSummaryTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderBroadWorksMobilityMobileSubscriberDirectoryNumberGetSummaryListResponse", propOrder = {
    "mobileSubscriberDirectoryNumbersSummaryTable"
})
public class ServiceProviderBroadWorksMobilityMobileSubscriberDirectoryNumberGetSummaryListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable mobileSubscriberDirectoryNumbersSummaryTable;

    /**
     * Ruft den Wert der mobileSubscriberDirectoryNumbersSummaryTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getMobileSubscriberDirectoryNumbersSummaryTable() {
        return mobileSubscriberDirectoryNumbersSummaryTable;
    }

    /**
     * Legt den Wert der mobileSubscriberDirectoryNumbersSummaryTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setMobileSubscriberDirectoryNumbersSummaryTable(OCITable value) {
        this.mobileSubscriberDirectoryNumbersSummaryTable = value;
    }

}
