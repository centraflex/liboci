//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemPolicyGetDefaultRequest14sp4.
 *         Contains the default policy settings for the system.
 *       
 * 
 * <p>Java-Klasse für SystemPolicyGetDefaultResponse14sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPolicyGetDefaultResponse14sp4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="groupCallingPlanAccess" type="{}GroupCallingPlanAccess"/>
 *         <element name="groupExtensionAccess" type="{}GroupExtensionAccess"/>
 *         <element name="groupLDAPIntegrationAccess" type="{}GroupLDAPIntegrationAccess"/>
 *         <element name="groupVoiceMessagingAccess" type="{}GroupVoiceMessagingAccess"/>
 *         <element name="groupDepartmentAdminUserAccess" type="{}GroupDepartmentAdminUserAccess"/>
 *         <element name="groupDepartmentAdminTrunkGroupAccess" type="{}GroupDepartmentAdminTrunkGroupAccess"/>
 *         <element name="groupUserAuthenticationAccess" type="{}GroupUserAuthenticationAccess"/>
 *         <element name="groupUserGroupDirectoryAccess" type="{}GroupUserGroupDirectoryAccess"/>
 *         <element name="groupUserProfileAccess" type="{}GroupUserProfileAccess"/>
 *         <element name="groupUserEnhancedCallLogsAccess" type="{}GroupUserCallLogAccess"/>
 *         <element name="groupAdminProfileAccess" type="{}GroupAdminProfileAccess"/>
 *         <element name="groupAdminUserAccess" type="{}GroupAdminUserAccess"/>
 *         <element name="groupAdminAdminAccess" type="{}GroupAdminAdminAccess"/>
 *         <element name="groupAdminDepartmentAccess" type="{}GroupAdminDepartmentAccess"/>
 *         <element name="groupAdminAccessDeviceAccess" type="{}GroupAdminAccessDeviceAccess"/>
 *         <element name="groupAdminEnhancedServiceInstanceAccess" type="{}GroupAdminEnhancedServiceInstanceAccess"/>
 *         <element name="groupAdminFeatureAccessCodeAccess" type="{}GroupAdminFeatureAccessCodeAccess"/>
 *         <element name="groupAdminPhoneNumberExtensionAccess" type="{}GroupAdminPhoneNumberExtensionAccess"/>
 *         <element name="groupAdminServiceAccess" type="{}GroupAdminServiceAccess"/>
 *         <element name="groupAdminTrunkGroupAccess" type="{}GroupAdminTrunkGroupAccess"/>
 *         <element name="serviceProviderAdminProfileAccess" type="{}ServiceProviderAdminProfileAccess"/>
 *         <element name="serviceProviderAdminGroupAccess" type="{}ServiceProviderAdminGroupAccess"/>
 *         <element name="serviceProviderAdminUserAccess" type="{}ServiceProviderAdminUserAccess"/>
 *         <element name="serviceProviderAdminAdminAccess" type="{}ServiceProviderAdminAdminAccess"/>
 *         <element name="serviceProviderAdminDepartmentAccess" type="{}ServiceProviderAdminDepartmentAccess"/>
 *         <element name="serviceProviderAdminAccessDeviceAccess" type="{}ServiceProviderAdminAccessDeviceAccess"/>
 *         <element name="serviceProviderAdminPhoneNumberExtensionAccess" type="{}ServiceProviderAdminPhoneNumberExtensionAccess"/>
 *         <element name="serviceProviderAdminServiceAccess" type="{}ServiceProviderAdminServiceAccess"/>
 *         <element name="serviceProviderAdminServicePackAccess" type="{}ServiceProviderAdminServicePackAccess"/>
 *         <element name="serviceProviderAdminWebBrandingAccess" type="{}ServiceProviderAdminWebBrandingAccess"/>
 *         <element name="enterpriseAdminNetworkPolicyAccess" type="{}EnterpriseAdminNetworkPolicyAccess"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPolicyGetDefaultResponse14sp4", propOrder = {
    "groupCallingPlanAccess",
    "groupExtensionAccess",
    "groupLDAPIntegrationAccess",
    "groupVoiceMessagingAccess",
    "groupDepartmentAdminUserAccess",
    "groupDepartmentAdminTrunkGroupAccess",
    "groupUserAuthenticationAccess",
    "groupUserGroupDirectoryAccess",
    "groupUserProfileAccess",
    "groupUserEnhancedCallLogsAccess",
    "groupAdminProfileAccess",
    "groupAdminUserAccess",
    "groupAdminAdminAccess",
    "groupAdminDepartmentAccess",
    "groupAdminAccessDeviceAccess",
    "groupAdminEnhancedServiceInstanceAccess",
    "groupAdminFeatureAccessCodeAccess",
    "groupAdminPhoneNumberExtensionAccess",
    "groupAdminServiceAccess",
    "groupAdminTrunkGroupAccess",
    "serviceProviderAdminProfileAccess",
    "serviceProviderAdminGroupAccess",
    "serviceProviderAdminUserAccess",
    "serviceProviderAdminAdminAccess",
    "serviceProviderAdminDepartmentAccess",
    "serviceProviderAdminAccessDeviceAccess",
    "serviceProviderAdminPhoneNumberExtensionAccess",
    "serviceProviderAdminServiceAccess",
    "serviceProviderAdminServicePackAccess",
    "serviceProviderAdminWebBrandingAccess",
    "enterpriseAdminNetworkPolicyAccess"
})
public class SystemPolicyGetDefaultResponse14Sp4
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupCallingPlanAccess groupCallingPlanAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupExtensionAccess groupExtensionAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupLDAPIntegrationAccess groupLDAPIntegrationAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupVoiceMessagingAccess groupVoiceMessagingAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminUserAccess groupDepartmentAdminUserAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupDepartmentAdminTrunkGroupAccess groupDepartmentAdminTrunkGroupAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupUserAuthenticationAccess groupUserAuthenticationAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupUserGroupDirectoryAccess groupUserGroupDirectoryAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupUserProfileAccess groupUserProfileAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupUserCallLogAccess groupUserEnhancedCallLogsAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminProfileAccess groupAdminProfileAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminUserAccess groupAdminUserAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminAdminAccess groupAdminAdminAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminDepartmentAccess groupAdminDepartmentAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminAccessDeviceAccess groupAdminAccessDeviceAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminEnhancedServiceInstanceAccess groupAdminEnhancedServiceInstanceAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminFeatureAccessCodeAccess groupAdminFeatureAccessCodeAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminPhoneNumberExtensionAccess groupAdminPhoneNumberExtensionAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminServiceAccess groupAdminServiceAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminTrunkGroupAccess groupAdminTrunkGroupAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminProfileAccess serviceProviderAdminProfileAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminGroupAccess serviceProviderAdminGroupAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminUserAccess serviceProviderAdminUserAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminAdminAccess serviceProviderAdminAdminAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminDepartmentAccess serviceProviderAdminDepartmentAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminAccessDeviceAccess serviceProviderAdminAccessDeviceAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminPhoneNumberExtensionAccess serviceProviderAdminPhoneNumberExtensionAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminServiceAccess serviceProviderAdminServiceAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminServicePackAccess serviceProviderAdminServicePackAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ServiceProviderAdminWebBrandingAccess serviceProviderAdminWebBrandingAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected EnterpriseAdminNetworkPolicyAccess enterpriseAdminNetworkPolicyAccess;

    /**
     * Ruft den Wert der groupCallingPlanAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupCallingPlanAccess }
     *     
     */
    public GroupCallingPlanAccess getGroupCallingPlanAccess() {
        return groupCallingPlanAccess;
    }

    /**
     * Legt den Wert der groupCallingPlanAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupCallingPlanAccess }
     *     
     */
    public void setGroupCallingPlanAccess(GroupCallingPlanAccess value) {
        this.groupCallingPlanAccess = value;
    }

    /**
     * Ruft den Wert der groupExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupExtensionAccess }
     *     
     */
    public GroupExtensionAccess getGroupExtensionAccess() {
        return groupExtensionAccess;
    }

    /**
     * Legt den Wert der groupExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupExtensionAccess }
     *     
     */
    public void setGroupExtensionAccess(GroupExtensionAccess value) {
        this.groupExtensionAccess = value;
    }

    /**
     * Ruft den Wert der groupLDAPIntegrationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupLDAPIntegrationAccess }
     *     
     */
    public GroupLDAPIntegrationAccess getGroupLDAPIntegrationAccess() {
        return groupLDAPIntegrationAccess;
    }

    /**
     * Legt den Wert der groupLDAPIntegrationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupLDAPIntegrationAccess }
     *     
     */
    public void setGroupLDAPIntegrationAccess(GroupLDAPIntegrationAccess value) {
        this.groupLDAPIntegrationAccess = value;
    }

    /**
     * Ruft den Wert der groupVoiceMessagingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupVoiceMessagingAccess }
     *     
     */
    public GroupVoiceMessagingAccess getGroupVoiceMessagingAccess() {
        return groupVoiceMessagingAccess;
    }

    /**
     * Legt den Wert der groupVoiceMessagingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupVoiceMessagingAccess }
     *     
     */
    public void setGroupVoiceMessagingAccess(GroupVoiceMessagingAccess value) {
        this.groupVoiceMessagingAccess = value;
    }

    /**
     * Ruft den Wert der groupDepartmentAdminUserAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminUserAccess }
     *     
     */
    public GroupDepartmentAdminUserAccess getGroupDepartmentAdminUserAccess() {
        return groupDepartmentAdminUserAccess;
    }

    /**
     * Legt den Wert der groupDepartmentAdminUserAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminUserAccess }
     *     
     */
    public void setGroupDepartmentAdminUserAccess(GroupDepartmentAdminUserAccess value) {
        this.groupDepartmentAdminUserAccess = value;
    }

    /**
     * Ruft den Wert der groupDepartmentAdminTrunkGroupAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupDepartmentAdminTrunkGroupAccess }
     *     
     */
    public GroupDepartmentAdminTrunkGroupAccess getGroupDepartmentAdminTrunkGroupAccess() {
        return groupDepartmentAdminTrunkGroupAccess;
    }

    /**
     * Legt den Wert der groupDepartmentAdminTrunkGroupAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDepartmentAdminTrunkGroupAccess }
     *     
     */
    public void setGroupDepartmentAdminTrunkGroupAccess(GroupDepartmentAdminTrunkGroupAccess value) {
        this.groupDepartmentAdminTrunkGroupAccess = value;
    }

    /**
     * Ruft den Wert der groupUserAuthenticationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserAuthenticationAccess }
     *     
     */
    public GroupUserAuthenticationAccess getGroupUserAuthenticationAccess() {
        return groupUserAuthenticationAccess;
    }

    /**
     * Legt den Wert der groupUserAuthenticationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserAuthenticationAccess }
     *     
     */
    public void setGroupUserAuthenticationAccess(GroupUserAuthenticationAccess value) {
        this.groupUserAuthenticationAccess = value;
    }

    /**
     * Ruft den Wert der groupUserGroupDirectoryAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserGroupDirectoryAccess }
     *     
     */
    public GroupUserGroupDirectoryAccess getGroupUserGroupDirectoryAccess() {
        return groupUserGroupDirectoryAccess;
    }

    /**
     * Legt den Wert der groupUserGroupDirectoryAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserGroupDirectoryAccess }
     *     
     */
    public void setGroupUserGroupDirectoryAccess(GroupUserGroupDirectoryAccess value) {
        this.groupUserGroupDirectoryAccess = value;
    }

    /**
     * Ruft den Wert der groupUserProfileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserProfileAccess }
     *     
     */
    public GroupUserProfileAccess getGroupUserProfileAccess() {
        return groupUserProfileAccess;
    }

    /**
     * Legt den Wert der groupUserProfileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserProfileAccess }
     *     
     */
    public void setGroupUserProfileAccess(GroupUserProfileAccess value) {
        this.groupUserProfileAccess = value;
    }

    /**
     * Ruft den Wert der groupUserEnhancedCallLogsAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupUserCallLogAccess }
     *     
     */
    public GroupUserCallLogAccess getGroupUserEnhancedCallLogsAccess() {
        return groupUserEnhancedCallLogsAccess;
    }

    /**
     * Legt den Wert der groupUserEnhancedCallLogsAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupUserCallLogAccess }
     *     
     */
    public void setGroupUserEnhancedCallLogsAccess(GroupUserCallLogAccess value) {
        this.groupUserEnhancedCallLogsAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminProfileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminProfileAccess }
     *     
     */
    public GroupAdminProfileAccess getGroupAdminProfileAccess() {
        return groupAdminProfileAccess;
    }

    /**
     * Legt den Wert der groupAdminProfileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminProfileAccess }
     *     
     */
    public void setGroupAdminProfileAccess(GroupAdminProfileAccess value) {
        this.groupAdminProfileAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminUserAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminUserAccess }
     *     
     */
    public GroupAdminUserAccess getGroupAdminUserAccess() {
        return groupAdminUserAccess;
    }

    /**
     * Legt den Wert der groupAdminUserAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminUserAccess }
     *     
     */
    public void setGroupAdminUserAccess(GroupAdminUserAccess value) {
        this.groupAdminUserAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminAdminAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminAdminAccess }
     *     
     */
    public GroupAdminAdminAccess getGroupAdminAdminAccess() {
        return groupAdminAdminAccess;
    }

    /**
     * Legt den Wert der groupAdminAdminAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminAdminAccess }
     *     
     */
    public void setGroupAdminAdminAccess(GroupAdminAdminAccess value) {
        this.groupAdminAdminAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminDepartmentAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminDepartmentAccess }
     *     
     */
    public GroupAdminDepartmentAccess getGroupAdminDepartmentAccess() {
        return groupAdminDepartmentAccess;
    }

    /**
     * Legt den Wert der groupAdminDepartmentAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminDepartmentAccess }
     *     
     */
    public void setGroupAdminDepartmentAccess(GroupAdminDepartmentAccess value) {
        this.groupAdminDepartmentAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminAccessDeviceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminAccessDeviceAccess }
     *     
     */
    public GroupAdminAccessDeviceAccess getGroupAdminAccessDeviceAccess() {
        return groupAdminAccessDeviceAccess;
    }

    /**
     * Legt den Wert der groupAdminAccessDeviceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminAccessDeviceAccess }
     *     
     */
    public void setGroupAdminAccessDeviceAccess(GroupAdminAccessDeviceAccess value) {
        this.groupAdminAccessDeviceAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminEnhancedServiceInstanceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminEnhancedServiceInstanceAccess }
     *     
     */
    public GroupAdminEnhancedServiceInstanceAccess getGroupAdminEnhancedServiceInstanceAccess() {
        return groupAdminEnhancedServiceInstanceAccess;
    }

    /**
     * Legt den Wert der groupAdminEnhancedServiceInstanceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminEnhancedServiceInstanceAccess }
     *     
     */
    public void setGroupAdminEnhancedServiceInstanceAccess(GroupAdminEnhancedServiceInstanceAccess value) {
        this.groupAdminEnhancedServiceInstanceAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminFeatureAccessCodeAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminFeatureAccessCodeAccess }
     *     
     */
    public GroupAdminFeatureAccessCodeAccess getGroupAdminFeatureAccessCodeAccess() {
        return groupAdminFeatureAccessCodeAccess;
    }

    /**
     * Legt den Wert der groupAdminFeatureAccessCodeAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminFeatureAccessCodeAccess }
     *     
     */
    public void setGroupAdminFeatureAccessCodeAccess(GroupAdminFeatureAccessCodeAccess value) {
        this.groupAdminFeatureAccessCodeAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminPhoneNumberExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminPhoneNumberExtensionAccess }
     *     
     */
    public GroupAdminPhoneNumberExtensionAccess getGroupAdminPhoneNumberExtensionAccess() {
        return groupAdminPhoneNumberExtensionAccess;
    }

    /**
     * Legt den Wert der groupAdminPhoneNumberExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminPhoneNumberExtensionAccess }
     *     
     */
    public void setGroupAdminPhoneNumberExtensionAccess(GroupAdminPhoneNumberExtensionAccess value) {
        this.groupAdminPhoneNumberExtensionAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminServiceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminServiceAccess }
     *     
     */
    public GroupAdminServiceAccess getGroupAdminServiceAccess() {
        return groupAdminServiceAccess;
    }

    /**
     * Legt den Wert der groupAdminServiceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminServiceAccess }
     *     
     */
    public void setGroupAdminServiceAccess(GroupAdminServiceAccess value) {
        this.groupAdminServiceAccess = value;
    }

    /**
     * Ruft den Wert der groupAdminTrunkGroupAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminTrunkGroupAccess }
     *     
     */
    public GroupAdminTrunkGroupAccess getGroupAdminTrunkGroupAccess() {
        return groupAdminTrunkGroupAccess;
    }

    /**
     * Legt den Wert der groupAdminTrunkGroupAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminTrunkGroupAccess }
     *     
     */
    public void setGroupAdminTrunkGroupAccess(GroupAdminTrunkGroupAccess value) {
        this.groupAdminTrunkGroupAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminProfileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminProfileAccess }
     *     
     */
    public ServiceProviderAdminProfileAccess getServiceProviderAdminProfileAccess() {
        return serviceProviderAdminProfileAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminProfileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminProfileAccess }
     *     
     */
    public void setServiceProviderAdminProfileAccess(ServiceProviderAdminProfileAccess value) {
        this.serviceProviderAdminProfileAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminGroupAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminGroupAccess }
     *     
     */
    public ServiceProviderAdminGroupAccess getServiceProviderAdminGroupAccess() {
        return serviceProviderAdminGroupAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminGroupAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminGroupAccess }
     *     
     */
    public void setServiceProviderAdminGroupAccess(ServiceProviderAdminGroupAccess value) {
        this.serviceProviderAdminGroupAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminUserAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminUserAccess }
     *     
     */
    public ServiceProviderAdminUserAccess getServiceProviderAdminUserAccess() {
        return serviceProviderAdminUserAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminUserAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminUserAccess }
     *     
     */
    public void setServiceProviderAdminUserAccess(ServiceProviderAdminUserAccess value) {
        this.serviceProviderAdminUserAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminAdminAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminAdminAccess }
     *     
     */
    public ServiceProviderAdminAdminAccess getServiceProviderAdminAdminAccess() {
        return serviceProviderAdminAdminAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminAdminAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminAdminAccess }
     *     
     */
    public void setServiceProviderAdminAdminAccess(ServiceProviderAdminAdminAccess value) {
        this.serviceProviderAdminAdminAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminDepartmentAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminDepartmentAccess }
     *     
     */
    public ServiceProviderAdminDepartmentAccess getServiceProviderAdminDepartmentAccess() {
        return serviceProviderAdminDepartmentAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminDepartmentAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminDepartmentAccess }
     *     
     */
    public void setServiceProviderAdminDepartmentAccess(ServiceProviderAdminDepartmentAccess value) {
        this.serviceProviderAdminDepartmentAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminAccessDeviceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminAccessDeviceAccess }
     *     
     */
    public ServiceProviderAdminAccessDeviceAccess getServiceProviderAdminAccessDeviceAccess() {
        return serviceProviderAdminAccessDeviceAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminAccessDeviceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminAccessDeviceAccess }
     *     
     */
    public void setServiceProviderAdminAccessDeviceAccess(ServiceProviderAdminAccessDeviceAccess value) {
        this.serviceProviderAdminAccessDeviceAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminPhoneNumberExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminPhoneNumberExtensionAccess }
     *     
     */
    public ServiceProviderAdminPhoneNumberExtensionAccess getServiceProviderAdminPhoneNumberExtensionAccess() {
        return serviceProviderAdminPhoneNumberExtensionAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminPhoneNumberExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminPhoneNumberExtensionAccess }
     *     
     */
    public void setServiceProviderAdminPhoneNumberExtensionAccess(ServiceProviderAdminPhoneNumberExtensionAccess value) {
        this.serviceProviderAdminPhoneNumberExtensionAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminServiceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminServiceAccess }
     *     
     */
    public ServiceProviderAdminServiceAccess getServiceProviderAdminServiceAccess() {
        return serviceProviderAdminServiceAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminServiceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminServiceAccess }
     *     
     */
    public void setServiceProviderAdminServiceAccess(ServiceProviderAdminServiceAccess value) {
        this.serviceProviderAdminServiceAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminServicePackAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminServicePackAccess }
     *     
     */
    public ServiceProviderAdminServicePackAccess getServiceProviderAdminServicePackAccess() {
        return serviceProviderAdminServicePackAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminServicePackAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminServicePackAccess }
     *     
     */
    public void setServiceProviderAdminServicePackAccess(ServiceProviderAdminServicePackAccess value) {
        this.serviceProviderAdminServicePackAccess = value;
    }

    /**
     * Ruft den Wert der serviceProviderAdminWebBrandingAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAdminWebBrandingAccess }
     *     
     */
    public ServiceProviderAdminWebBrandingAccess getServiceProviderAdminWebBrandingAccess() {
        return serviceProviderAdminWebBrandingAccess;
    }

    /**
     * Legt den Wert der serviceProviderAdminWebBrandingAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAdminWebBrandingAccess }
     *     
     */
    public void setServiceProviderAdminWebBrandingAccess(ServiceProviderAdminWebBrandingAccess value) {
        this.serviceProviderAdminWebBrandingAccess = value;
    }

    /**
     * Ruft den Wert der enterpriseAdminNetworkPolicyAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseAdminNetworkPolicyAccess }
     *     
     */
    public EnterpriseAdminNetworkPolicyAccess getEnterpriseAdminNetworkPolicyAccess() {
        return enterpriseAdminNetworkPolicyAccess;
    }

    /**
     * Legt den Wert der enterpriseAdminNetworkPolicyAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseAdminNetworkPolicyAccess }
     *     
     */
    public void setEnterpriseAdminNetworkPolicyAccess(EnterpriseAdminNetworkPolicyAccess value) {
        this.enterpriseAdminNetworkPolicyAccess = value;
    }

}
