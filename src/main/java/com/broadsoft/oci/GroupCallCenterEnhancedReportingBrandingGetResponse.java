//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupCallCenterEnhancedReportingBrandingGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupCallCenterEnhancedReportingBrandingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupCallCenterEnhancedReportingBrandingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="brandingChoice" type="{}CallCenterEnhancedReportingBrandingChoice"/>
 *         <element name="brandingFileDescription" type="{}FileDescription" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupCallCenterEnhancedReportingBrandingGetResponse", propOrder = {
    "brandingChoice",
    "brandingFileDescription"
})
public class GroupCallCenterEnhancedReportingBrandingGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterEnhancedReportingBrandingChoice brandingChoice;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String brandingFileDescription;

    /**
     * Ruft den Wert der brandingChoice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterEnhancedReportingBrandingChoice }
     *     
     */
    public CallCenterEnhancedReportingBrandingChoice getBrandingChoice() {
        return brandingChoice;
    }

    /**
     * Legt den Wert der brandingChoice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterEnhancedReportingBrandingChoice }
     *     
     */
    public void setBrandingChoice(CallCenterEnhancedReportingBrandingChoice value) {
        this.brandingChoice = value;
    }

    /**
     * Ruft den Wert der brandingFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandingFileDescription() {
        return brandingFileDescription;
    }

    /**
     * Legt den Wert der brandingFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandingFileDescription(String value) {
        this.brandingFileDescription = value;
    }

}
