//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserSimultaneousRingFamilyGetPhoneNumberRequest.
 *       
 * 
 * <p>Java-Klasse für UserSimultaneousRingFamilyGetPhoneNumberResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserSimultaneousRingFamilyGetPhoneNumberResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="answerConfirmationRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserSimultaneousRingFamilyGetPhoneNumberResponse", propOrder = {
    "answerConfirmationRequired"
})
public class UserSimultaneousRingFamilyGetPhoneNumberResponse
    extends OCIDataResponse
{

    protected boolean answerConfirmationRequired;

    /**
     * Ruft den Wert der answerConfirmationRequired-Eigenschaft ab.
     * 
     */
    public boolean isAnswerConfirmationRequired() {
        return answerConfirmationRequired;
    }

    /**
     * Legt den Wert der answerConfirmationRequired-Eigenschaft fest.
     * 
     */
    public void setAnswerConfirmationRequired(boolean value) {
        this.answerConfirmationRequired = value;
    }

}
