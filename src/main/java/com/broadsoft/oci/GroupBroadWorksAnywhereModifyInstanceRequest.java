//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to modify a BroadWorks Anywhere instance.
 *         The following elements are only used in AS data mode:
 *           networkClassOfService            
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für GroupBroadWorksAnywhereModifyInstanceRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupBroadWorksAnywhereModifyInstanceRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceUserId" type="{}UserId"/>
 *         <element name="serviceInstanceProfile" type="{}ServiceInstanceModifyProfile" minOccurs="0"/>
 *         <element name="broadWorksAnywhereScope" type="{}BroadWorksAnywhereScope" minOccurs="0"/>
 *         <element name="promptForCLID" type="{}BroadWorksAnywhereCLIDPrompt" minOccurs="0"/>
 *         <element name="silentPromptMode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="promptForPasscode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="networkClassOfService" type="{}NetworkClassOfServiceName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupBroadWorksAnywhereModifyInstanceRequest", propOrder = {
    "serviceUserId",
    "serviceInstanceProfile",
    "broadWorksAnywhereScope",
    "promptForCLID",
    "silentPromptMode",
    "promptForPasscode",
    "networkClassOfService"
})
public class GroupBroadWorksAnywhereModifyInstanceRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceUserId;
    protected ServiceInstanceModifyProfile serviceInstanceProfile;
    @XmlSchemaType(name = "token")
    protected BroadWorksAnywhereScope broadWorksAnywhereScope;
    @XmlSchemaType(name = "token")
    protected BroadWorksAnywhereCLIDPrompt promptForCLID;
    protected Boolean silentPromptMode;
    protected Boolean promptForPasscode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String networkClassOfService;

    /**
     * Ruft den Wert der serviceUserId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUserId() {
        return serviceUserId;
    }

    /**
     * Legt den Wert der serviceUserId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUserId(String value) {
        this.serviceUserId = value;
    }

    /**
     * Ruft den Wert der serviceInstanceProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public ServiceInstanceModifyProfile getServiceInstanceProfile() {
        return serviceInstanceProfile;
    }

    /**
     * Legt den Wert der serviceInstanceProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceInstanceModifyProfile }
     *     
     */
    public void setServiceInstanceProfile(ServiceInstanceModifyProfile value) {
        this.serviceInstanceProfile = value;
    }

    /**
     * Ruft den Wert der broadWorksAnywhereScope-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksAnywhereScope }
     *     
     */
    public BroadWorksAnywhereScope getBroadWorksAnywhereScope() {
        return broadWorksAnywhereScope;
    }

    /**
     * Legt den Wert der broadWorksAnywhereScope-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksAnywhereScope }
     *     
     */
    public void setBroadWorksAnywhereScope(BroadWorksAnywhereScope value) {
        this.broadWorksAnywhereScope = value;
    }

    /**
     * Ruft den Wert der promptForCLID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BroadWorksAnywhereCLIDPrompt }
     *     
     */
    public BroadWorksAnywhereCLIDPrompt getPromptForCLID() {
        return promptForCLID;
    }

    /**
     * Legt den Wert der promptForCLID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadWorksAnywhereCLIDPrompt }
     *     
     */
    public void setPromptForCLID(BroadWorksAnywhereCLIDPrompt value) {
        this.promptForCLID = value;
    }

    /**
     * Ruft den Wert der silentPromptMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSilentPromptMode() {
        return silentPromptMode;
    }

    /**
     * Legt den Wert der silentPromptMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSilentPromptMode(Boolean value) {
        this.silentPromptMode = value;
    }

    /**
     * Ruft den Wert der promptForPasscode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPromptForPasscode() {
        return promptForPasscode;
    }

    /**
     * Legt den Wert der promptForPasscode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPromptForPasscode(Boolean value) {
        this.promptForPasscode = value;
    }

    /**
     * Ruft den Wert der networkClassOfService-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkClassOfService() {
        return networkClassOfService;
    }

    /**
     * Legt den Wert der networkClassOfService-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkClassOfService(String value) {
        this.networkClassOfService = value;
    }

}
