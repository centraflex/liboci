//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemGETSGetRequest. 
 *       
 * 
 * <p>Java-Klasse für SystemGETSGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGETSGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="enabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="enableRequireResourcePriority" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="sendAccessResourcePriority" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="callIdentifierMode" type="{}GETSCallIdentifierMode"/>
 *         <element name="defaultPriorityAVP" type="{}GETSPriorityAVP"/>
 *         <element name="signalingDSCP" type="{}GETSSignalingDSCP"/>
 *         <element name="defaultRValue" type="{}GETSPriorityValue"/>
 *         <element name="bypassRoRelease" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGETSGetResponse", propOrder = {
    "enabled",
    "enableRequireResourcePriority",
    "sendAccessResourcePriority",
    "callIdentifierMode",
    "defaultPriorityAVP",
    "signalingDSCP",
    "defaultRValue",
    "bypassRoRelease"
})
public class SystemGETSGetResponse
    extends OCIDataResponse
{

    protected boolean enabled;
    protected boolean enableRequireResourcePriority;
    protected boolean sendAccessResourcePriority;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GETSCallIdentifierMode callIdentifierMode;
    protected int defaultPriorityAVP;
    protected int signalingDSCP;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultRValue;
    protected boolean bypassRoRelease;

    /**
     * Ruft den Wert der enabled-Eigenschaft ab.
     * 
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Legt den Wert der enabled-Eigenschaft fest.
     * 
     */
    public void setEnabled(boolean value) {
        this.enabled = value;
    }

    /**
     * Ruft den Wert der enableRequireResourcePriority-Eigenschaft ab.
     * 
     */
    public boolean isEnableRequireResourcePriority() {
        return enableRequireResourcePriority;
    }

    /**
     * Legt den Wert der enableRequireResourcePriority-Eigenschaft fest.
     * 
     */
    public void setEnableRequireResourcePriority(boolean value) {
        this.enableRequireResourcePriority = value;
    }

    /**
     * Ruft den Wert der sendAccessResourcePriority-Eigenschaft ab.
     * 
     */
    public boolean isSendAccessResourcePriority() {
        return sendAccessResourcePriority;
    }

    /**
     * Legt den Wert der sendAccessResourcePriority-Eigenschaft fest.
     * 
     */
    public void setSendAccessResourcePriority(boolean value) {
        this.sendAccessResourcePriority = value;
    }

    /**
     * Ruft den Wert der callIdentifierMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GETSCallIdentifierMode }
     *     
     */
    public GETSCallIdentifierMode getCallIdentifierMode() {
        return callIdentifierMode;
    }

    /**
     * Legt den Wert der callIdentifierMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GETSCallIdentifierMode }
     *     
     */
    public void setCallIdentifierMode(GETSCallIdentifierMode value) {
        this.callIdentifierMode = value;
    }

    /**
     * Ruft den Wert der defaultPriorityAVP-Eigenschaft ab.
     * 
     */
    public int getDefaultPriorityAVP() {
        return defaultPriorityAVP;
    }

    /**
     * Legt den Wert der defaultPriorityAVP-Eigenschaft fest.
     * 
     */
    public void setDefaultPriorityAVP(int value) {
        this.defaultPriorityAVP = value;
    }

    /**
     * Ruft den Wert der signalingDSCP-Eigenschaft ab.
     * 
     */
    public int getSignalingDSCP() {
        return signalingDSCP;
    }

    /**
     * Legt den Wert der signalingDSCP-Eigenschaft fest.
     * 
     */
    public void setSignalingDSCP(int value) {
        this.signalingDSCP = value;
    }

    /**
     * Ruft den Wert der defaultRValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultRValue() {
        return defaultRValue;
    }

    /**
     * Legt den Wert der defaultRValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultRValue(String value) {
        this.defaultRValue = value;
    }

    /**
     * Ruft den Wert der bypassRoRelease-Eigenschaft ab.
     * 
     */
    public boolean isBypassRoRelease() {
        return bypassRoRelease;
    }

    /**
     * Legt den Wert der bypassRoRelease-Eigenschaft fest.
     * 
     */
    public void setBypassRoRelease(boolean value) {
        this.bypassRoRelease = value;
    }

}
