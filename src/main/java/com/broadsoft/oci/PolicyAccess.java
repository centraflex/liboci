//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PolicyAccess.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="PolicyAccess">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     <enumeration value="Full"/>
 *     <enumeration value="Restricted-FullProfile"/>
 *     <enumeration value="Restricted"/>
 *     <enumeration value="Restricted-NoAuthorize"/>
 *     <enumeration value="Restricted-NoUserAssociation"/>
 *     <enumeration value="Restricted-ReadProfile"/>
 *     <enumeration value="Restricted-NoProfile"/>
 *     <enumeration value="Restricted-NoGroup"/>
 *     <enumeration value="Restricted-FullResource"/>
 *     <enumeration value="Restricted-ReadResource"/>
 *     <enumeration value="None"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "PolicyAccess")
@XmlEnum
public enum PolicyAccess {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("Restricted-FullProfile")
    RESTRICTED_FULL_PROFILE("Restricted-FullProfile"),
    @XmlEnumValue("Restricted")
    RESTRICTED("Restricted"),
    @XmlEnumValue("Restricted-NoAuthorize")
    RESTRICTED_NO_AUTHORIZE("Restricted-NoAuthorize"),
    @XmlEnumValue("Restricted-NoUserAssociation")
    RESTRICTED_NO_USER_ASSOCIATION("Restricted-NoUserAssociation"),
    @XmlEnumValue("Restricted-ReadProfile")
    RESTRICTED_READ_PROFILE("Restricted-ReadProfile"),
    @XmlEnumValue("Restricted-NoProfile")
    RESTRICTED_NO_PROFILE("Restricted-NoProfile"),
    @XmlEnumValue("Restricted-NoGroup")
    RESTRICTED_NO_GROUP("Restricted-NoGroup"),
    @XmlEnumValue("Restricted-FullResource")
    RESTRICTED_FULL_RESOURCE("Restricted-FullResource"),
    @XmlEnumValue("Restricted-ReadResource")
    RESTRICTED_READ_RESOURCE("Restricted-ReadResource"),
    @XmlEnumValue("None")
    NONE("None");
    private final String value;

    PolicyAccess(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PolicyAccess fromValue(String v) {
        for (PolicyAccess c: PolicyAccess.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
