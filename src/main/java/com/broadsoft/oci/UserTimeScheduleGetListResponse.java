//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the UserTimeScheduleGetListRequest. The column headings are "Time Schedule" and
 *         "Type".
 *         The response contains user's personal and user's group's schedule names.
 *       
 * 
 * <p>Java-Klasse für UserTimeScheduleGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserTimeScheduleGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="timeScheduleTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserTimeScheduleGetListResponse", propOrder = {
    "timeScheduleTable"
})
public class UserTimeScheduleGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable timeScheduleTable;

    /**
     * Ruft den Wert der timeScheduleTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getTimeScheduleTable() {
        return timeScheduleTable;
    }

    /**
     * Legt den Wert der timeScheduleTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setTimeScheduleTable(OCITable value) {
        this.timeScheduleTable = value;
    }

}
