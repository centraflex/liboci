//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request to get the configuration for a call processing policy profile user subscriber type profile.
 *         The response is either a SystemCallProcessingPolicyProfileUserProfileGetResponse21sp1 or an
 *         ErrorResponse.
 *         Replaced by: SystemCallProcessingPolicyProfileUserProfileGetRequest22 in AS data mode
 *       
 * 
 * <p>Java-Klasse für SystemCallProcessingPolicyProfileUserProfileGetRequest21sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallProcessingPolicyProfileUserProfileGetRequest21sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="callProcessingPolicyProfileName" type="{}CallProcessingPolicyProfileName"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallProcessingPolicyProfileUserProfileGetRequest21sp1", propOrder = {
    "callProcessingPolicyProfileName"
})
public class SystemCallProcessingPolicyProfileUserProfileGetRequest21Sp1
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callProcessingPolicyProfileName;

    /**
     * Ruft den Wert der callProcessingPolicyProfileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallProcessingPolicyProfileName() {
        return callProcessingPolicyProfileName;
    }

    /**
     * Legt den Wert der callProcessingPolicyProfileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallProcessingPolicyProfileName(String value) {
        this.callProcessingPolicyProfileName = value;
    }

}
