//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupDialableCallerIDGetRequest.
 *         The criteria table's column headings are "Active", "Name", "Description", "Prefix Digits", "Priority".
 *       
 * 
 * <p>Java-Klasse für GroupDialableCallerIDGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupDialableCallerIDGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="useGroupCriteria" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="nsScreeningFailurePolicy" type="{}NsScreeningFailurePolicy"/>
 *         <element name="criteriaTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupDialableCallerIDGetResponse", propOrder = {
    "useGroupCriteria",
    "nsScreeningFailurePolicy",
    "criteriaTable"
})
public class GroupDialableCallerIDGetResponse
    extends OCIDataResponse
{

    protected boolean useGroupCriteria;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected NsScreeningFailurePolicy nsScreeningFailurePolicy;
    @XmlElement(required = true)
    protected OCITable criteriaTable;

    /**
     * Ruft den Wert der useGroupCriteria-Eigenschaft ab.
     * 
     */
    public boolean isUseGroupCriteria() {
        return useGroupCriteria;
    }

    /**
     * Legt den Wert der useGroupCriteria-Eigenschaft fest.
     * 
     */
    public void setUseGroupCriteria(boolean value) {
        this.useGroupCriteria = value;
    }

    /**
     * Ruft den Wert der nsScreeningFailurePolicy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NsScreeningFailurePolicy }
     *     
     */
    public NsScreeningFailurePolicy getNsScreeningFailurePolicy() {
        return nsScreeningFailurePolicy;
    }

    /**
     * Legt den Wert der nsScreeningFailurePolicy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NsScreeningFailurePolicy }
     *     
     */
    public void setNsScreeningFailurePolicy(NsScreeningFailurePolicy value) {
        this.nsScreeningFailurePolicy = value;
    }

    /**
     * Ruft den Wert der criteriaTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getCriteriaTable() {
        return criteriaTable;
    }

    /**
     * Legt den Wert der criteriaTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setCriteriaTable(OCITable value) {
        this.criteriaTable = value;
    }

}
