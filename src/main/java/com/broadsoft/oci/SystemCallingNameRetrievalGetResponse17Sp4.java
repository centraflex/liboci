//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCallingNameRetrievalGetRequest17sp4.
 *         
 *         Replaced by: SystemCallingNameRetrievalGetResponse24
 *       
 * 
 * <p>Java-Klasse für SystemCallingNameRetrievalGetResponse17sp4 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallingNameRetrievalGetResponse17sp4">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="triggerCNAMQueriesForAllNetworkCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="triggerCNAMQueriesForGroupAndEnterpriseCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="queryProtocol" type="{}CallingNameRetrievalQueryProtocol"/>
 *         <element name="queryTimeoutMilliseconds" type="{}CallingNameRetrievalQueryTimeoutMilliseconds"/>
 *         <element name="sipExternalDatabaseNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="sipExternalDatabasePort" type="{}Port1025" minOccurs="0"/>
 *         <element name="sipExternalDatabaseTransport" type="{}TransportProtocol"/>
 *         <element name="soapExternalDatabaseNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="soapSupportsDNSSRV" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="callingNameSource" type="{}CallingNameRetrievalSourceIdentity"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallingNameRetrievalGetResponse17sp4", propOrder = {
    "triggerCNAMQueriesForAllNetworkCalls",
    "triggerCNAMQueriesForGroupAndEnterpriseCalls",
    "queryProtocol",
    "queryTimeoutMilliseconds",
    "sipExternalDatabaseNetAddress",
    "sipExternalDatabasePort",
    "sipExternalDatabaseTransport",
    "soapExternalDatabaseNetAddress",
    "soapSupportsDNSSRV",
    "callingNameSource"
})
public class SystemCallingNameRetrievalGetResponse17Sp4
    extends OCIDataResponse
{

    protected boolean triggerCNAMQueriesForAllNetworkCalls;
    protected boolean triggerCNAMQueriesForGroupAndEnterpriseCalls;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallingNameRetrievalQueryProtocol queryProtocol;
    protected int queryTimeoutMilliseconds;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sipExternalDatabaseNetAddress;
    protected Integer sipExternalDatabasePort;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected TransportProtocol sipExternalDatabaseTransport;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String soapExternalDatabaseNetAddress;
    protected boolean soapSupportsDNSSRV;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallingNameRetrievalSourceIdentity callingNameSource;

    /**
     * Ruft den Wert der triggerCNAMQueriesForAllNetworkCalls-Eigenschaft ab.
     * 
     */
    public boolean isTriggerCNAMQueriesForAllNetworkCalls() {
        return triggerCNAMQueriesForAllNetworkCalls;
    }

    /**
     * Legt den Wert der triggerCNAMQueriesForAllNetworkCalls-Eigenschaft fest.
     * 
     */
    public void setTriggerCNAMQueriesForAllNetworkCalls(boolean value) {
        this.triggerCNAMQueriesForAllNetworkCalls = value;
    }

    /**
     * Ruft den Wert der triggerCNAMQueriesForGroupAndEnterpriseCalls-Eigenschaft ab.
     * 
     */
    public boolean isTriggerCNAMQueriesForGroupAndEnterpriseCalls() {
        return triggerCNAMQueriesForGroupAndEnterpriseCalls;
    }

    /**
     * Legt den Wert der triggerCNAMQueriesForGroupAndEnterpriseCalls-Eigenschaft fest.
     * 
     */
    public void setTriggerCNAMQueriesForGroupAndEnterpriseCalls(boolean value) {
        this.triggerCNAMQueriesForGroupAndEnterpriseCalls = value;
    }

    /**
     * Ruft den Wert der queryProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallingNameRetrievalQueryProtocol }
     *     
     */
    public CallingNameRetrievalQueryProtocol getQueryProtocol() {
        return queryProtocol;
    }

    /**
     * Legt den Wert der queryProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallingNameRetrievalQueryProtocol }
     *     
     */
    public void setQueryProtocol(CallingNameRetrievalQueryProtocol value) {
        this.queryProtocol = value;
    }

    /**
     * Ruft den Wert der queryTimeoutMilliseconds-Eigenschaft ab.
     * 
     */
    public int getQueryTimeoutMilliseconds() {
        return queryTimeoutMilliseconds;
    }

    /**
     * Legt den Wert der queryTimeoutMilliseconds-Eigenschaft fest.
     * 
     */
    public void setQueryTimeoutMilliseconds(int value) {
        this.queryTimeoutMilliseconds = value;
    }

    /**
     * Ruft den Wert der sipExternalDatabaseNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSipExternalDatabaseNetAddress() {
        return sipExternalDatabaseNetAddress;
    }

    /**
     * Legt den Wert der sipExternalDatabaseNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSipExternalDatabaseNetAddress(String value) {
        this.sipExternalDatabaseNetAddress = value;
    }

    /**
     * Ruft den Wert der sipExternalDatabasePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSipExternalDatabasePort() {
        return sipExternalDatabasePort;
    }

    /**
     * Legt den Wert der sipExternalDatabasePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSipExternalDatabasePort(Integer value) {
        this.sipExternalDatabasePort = value;
    }

    /**
     * Ruft den Wert der sipExternalDatabaseTransport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TransportProtocol }
     *     
     */
    public TransportProtocol getSipExternalDatabaseTransport() {
        return sipExternalDatabaseTransport;
    }

    /**
     * Legt den Wert der sipExternalDatabaseTransport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TransportProtocol }
     *     
     */
    public void setSipExternalDatabaseTransport(TransportProtocol value) {
        this.sipExternalDatabaseTransport = value;
    }

    /**
     * Ruft den Wert der soapExternalDatabaseNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoapExternalDatabaseNetAddress() {
        return soapExternalDatabaseNetAddress;
    }

    /**
     * Legt den Wert der soapExternalDatabaseNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoapExternalDatabaseNetAddress(String value) {
        this.soapExternalDatabaseNetAddress = value;
    }

    /**
     * Ruft den Wert der soapSupportsDNSSRV-Eigenschaft ab.
     * 
     */
    public boolean isSoapSupportsDNSSRV() {
        return soapSupportsDNSSRV;
    }

    /**
     * Legt den Wert der soapSupportsDNSSRV-Eigenschaft fest.
     * 
     */
    public void setSoapSupportsDNSSRV(boolean value) {
        this.soapSupportsDNSSRV = value;
    }

    /**
     * Ruft den Wert der callingNameSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallingNameRetrievalSourceIdentity }
     *     
     */
    public CallingNameRetrievalSourceIdentity getCallingNameSource() {
        return callingNameSource;
    }

    /**
     * Legt den Wert der callingNameSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallingNameRetrievalSourceIdentity }
     *     
     */
    public void setCallingNameSource(CallingNameRetrievalSourceIdentity value) {
        this.callingNameSource = value;
    }

}
