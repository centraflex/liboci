//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The common Service Pack elements.
 *       
 * 
 * <p>Java-Klasse für ServicePack complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServicePack">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="servicePackName" type="{}ServicePackName"/>
 *         <element name="servicePackDescription" type="{}ServicePackDescription" minOccurs="0"/>
 *         <element name="isAvailableForUse" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="servicePackQuantity" type="{}UnboundedPositiveInt"/>
 *         <element name="serviceName" type="{}UserService" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServicePack", propOrder = {
    "servicePackName",
    "servicePackDescription",
    "isAvailableForUse",
    "servicePackQuantity",
    "serviceName"
})
public class ServicePack {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String servicePackName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String servicePackDescription;
    protected boolean isAvailableForUse;
    @XmlElement(required = true)
    protected UnboundedPositiveInt servicePackQuantity;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> serviceName;

    /**
     * Ruft den Wert der servicePackName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicePackName() {
        return servicePackName;
    }

    /**
     * Legt den Wert der servicePackName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicePackName(String value) {
        this.servicePackName = value;
    }

    /**
     * Ruft den Wert der servicePackDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicePackDescription() {
        return servicePackDescription;
    }

    /**
     * Legt den Wert der servicePackDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicePackDescription(String value) {
        this.servicePackDescription = value;
    }

    /**
     * Ruft den Wert der isAvailableForUse-Eigenschaft ab.
     * 
     */
    public boolean isIsAvailableForUse() {
        return isAvailableForUse;
    }

    /**
     * Legt den Wert der isAvailableForUse-Eigenschaft fest.
     * 
     */
    public void setIsAvailableForUse(boolean value) {
        this.isAvailableForUse = value;
    }

    /**
     * Ruft den Wert der servicePackQuantity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public UnboundedPositiveInt getServicePackQuantity() {
        return servicePackQuantity;
    }

    /**
     * Legt den Wert der servicePackQuantity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link UnboundedPositiveInt }
     *     
     */
    public void setServicePackQuantity(UnboundedPositiveInt value) {
        this.servicePackQuantity = value;
    }

    /**
     * Gets the value of the serviceName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the serviceName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the serviceName property.
     */
    public List<String> getServiceName() {
        if (serviceName == null) {
            serviceName = new ArrayList<>();
        }
        return this.serviceName;
    }

}
