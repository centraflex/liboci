//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modifies the system's automatic callback attributes.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         Replaced By: SystemAutomaticCallbackModifyRequest15
 *       
 * 
 * <p>Java-Klasse für SystemAutomaticCallbackModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAutomaticCallbackModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="monitorMinutes" type="{}AutomaticCallbackMonitorMinutes" minOccurs="0"/>
 *         <element name="waitBetweenRetryOriginatorMinutes" type="{}AutomaticCallbackWaitBetweenRetryOriginatorMinutes" minOccurs="0"/>
 *         <element name="maxMonitorsPerOriginator" type="{}AutomaticCallbackMaxMonitorsPerOriginator" minOccurs="0"/>
 *         <element name="maxCallbackRings" type="{}AutomaticCallbackMaxCallbackRings" minOccurs="0"/>
 *         <element name="maxRetryOriginatorMinutes" type="{}AutomaticCallbackMaxRetryOriginatorMinutes" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAutomaticCallbackModifyRequest", propOrder = {
    "monitorMinutes",
    "waitBetweenRetryOriginatorMinutes",
    "maxMonitorsPerOriginator",
    "maxCallbackRings",
    "maxRetryOriginatorMinutes"
})
public class SystemAutomaticCallbackModifyRequest
    extends OCIRequest
{

    protected Integer monitorMinutes;
    protected Integer waitBetweenRetryOriginatorMinutes;
    protected Integer maxMonitorsPerOriginator;
    protected Integer maxCallbackRings;
    protected Integer maxRetryOriginatorMinutes;

    /**
     * Ruft den Wert der monitorMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMonitorMinutes() {
        return monitorMinutes;
    }

    /**
     * Legt den Wert der monitorMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMonitorMinutes(Integer value) {
        this.monitorMinutes = value;
    }

    /**
     * Ruft den Wert der waitBetweenRetryOriginatorMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWaitBetweenRetryOriginatorMinutes() {
        return waitBetweenRetryOriginatorMinutes;
    }

    /**
     * Legt den Wert der waitBetweenRetryOriginatorMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWaitBetweenRetryOriginatorMinutes(Integer value) {
        this.waitBetweenRetryOriginatorMinutes = value;
    }

    /**
     * Ruft den Wert der maxMonitorsPerOriginator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxMonitorsPerOriginator() {
        return maxMonitorsPerOriginator;
    }

    /**
     * Legt den Wert der maxMonitorsPerOriginator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxMonitorsPerOriginator(Integer value) {
        this.maxMonitorsPerOriginator = value;
    }

    /**
     * Ruft den Wert der maxCallbackRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxCallbackRings() {
        return maxCallbackRings;
    }

    /**
     * Legt den Wert der maxCallbackRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxCallbackRings(Integer value) {
        this.maxCallbackRings = value;
    }

    /**
     * Ruft den Wert der maxRetryOriginatorMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxRetryOriginatorMinutes() {
        return maxRetryOriginatorMinutes;
    }

    /**
     * Legt den Wert der maxRetryOriginatorMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxRetryOriginatorMinutes(Integer value) {
        this.maxRetryOriginatorMinutes = value;
    }

}
