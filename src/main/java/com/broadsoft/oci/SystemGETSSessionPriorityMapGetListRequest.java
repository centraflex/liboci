//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Get a list of GETS Session Priority maps.
 *         The response is either SystemGETSSessionPriorityMapGetListResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemGETSSessionPriorityMapGetListRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemGETSSessionPriorityMapGetListRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemGETSSessionPriorityMapGetListRequest")
public class SystemGETSSessionPriorityMapGetListRequest
    extends OCIRequest
{


}
