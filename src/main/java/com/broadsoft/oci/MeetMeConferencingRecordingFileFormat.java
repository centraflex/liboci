//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für MeetMeConferencingRecordingFileFormat.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="MeetMeConferencingRecordingFileFormat">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="WAV"/>
 *     <enumeration value="MP3"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "MeetMeConferencingRecordingFileFormat")
@XmlEnum
public enum MeetMeConferencingRecordingFileFormat {

    WAV("WAV"),
    @XmlEnumValue("MP3")
    MP_3("MP3");
    private final String value;

    MeetMeConferencingRecordingFileFormat(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MeetMeConferencingRecordingFileFormat fromValue(String v) {
        for (MeetMeConferencingRecordingFileFormat c: MeetMeConferencingRecordingFileFormat.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
