//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemASRParametersGetRequest14sp5.
 *         Contains a list of system Application Server Registration parameters.
 *       
 * 
 * <p>Java-Klasse für SystemASRParametersGetResponse14sp5 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemASRParametersGetResponse14sp5">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maxTransmissions" type="{}ASRMaxTransmissions"/>
 *         <element name="retransmissionDelayMilliSeconds" type="{}ASRRetransmissionDelayMilliSeconds"/>
 *         <element name="listeningPort" type="{}Port1025"/>
 *         <element name="sourceAddress" type="{}NetAddress" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemASRParametersGetResponse14sp5", propOrder = {
    "maxTransmissions",
    "retransmissionDelayMilliSeconds",
    "listeningPort",
    "sourceAddress"
})
public class SystemASRParametersGetResponse14Sp5
    extends OCIDataResponse
{

    protected int maxTransmissions;
    protected int retransmissionDelayMilliSeconds;
    protected int listeningPort;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sourceAddress;

    /**
     * Ruft den Wert der maxTransmissions-Eigenschaft ab.
     * 
     */
    public int getMaxTransmissions() {
        return maxTransmissions;
    }

    /**
     * Legt den Wert der maxTransmissions-Eigenschaft fest.
     * 
     */
    public void setMaxTransmissions(int value) {
        this.maxTransmissions = value;
    }

    /**
     * Ruft den Wert der retransmissionDelayMilliSeconds-Eigenschaft ab.
     * 
     */
    public int getRetransmissionDelayMilliSeconds() {
        return retransmissionDelayMilliSeconds;
    }

    /**
     * Legt den Wert der retransmissionDelayMilliSeconds-Eigenschaft fest.
     * 
     */
    public void setRetransmissionDelayMilliSeconds(int value) {
        this.retransmissionDelayMilliSeconds = value;
    }

    /**
     * Ruft den Wert der listeningPort-Eigenschaft ab.
     * 
     */
    public int getListeningPort() {
        return listeningPort;
    }

    /**
     * Legt den Wert der listeningPort-Eigenschaft fest.
     * 
     */
    public void setListeningPort(int value) {
        this.listeningPort = value;
    }

    /**
     * Ruft den Wert der sourceAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceAddress() {
        return sourceAddress;
    }

    /**
     * Legt den Wert der sourceAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceAddress(String value) {
        this.sourceAddress = value;
    }

}
