//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the screening and alerting setting of an executive.
 *         Both executive and the executive assistant can run this command.
 *         The response is either SuccessResponse or ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für UserExecutiveModifyScreeningAlertingRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserExecutiveModifyScreeningAlertingRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="enableScreening" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="screeningAlertType" type="{}ExecutiveScreeningAlertType" minOccurs="0"/>
 *         <element name="alertBroadWorksMobilityLocation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="alertBroadWorksAnywhereLocations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="alertSharedCallAppearanceLocations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="alertingMode" type="{}ExecutiveAlertingMode" minOccurs="0"/>
 *         <element name="alertingCallingLineIdNameMode" type="{}ExecutiveAlertingCallingLineIdNameMode" minOccurs="0"/>
 *         <element name="alertingCustomCallingLineIdName" type="{}ExecutiveAlertingCustomCallingLineIdName" minOccurs="0"/>
 *         <element name="unicodeAlertingCustomCallingLineIdName" type="{}ExecutiveAlertingCustomCallingLineIdName" minOccurs="0"/>
 *         <element name="alertingCallingLineIdPhoneNumberMode" type="{}ExecutiveAlertingCallingLineIdPhoneNumberMode" minOccurs="0"/>
 *         <element name="alertingCustomCallingLineIdPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="callPushRecallNumberOfRings" type="{}ExecutiveCallPushRecallNumberOfRings" minOccurs="0"/>
 *         <element name="nextAssistantNumberOfRings" type="{}ExecutiveNextAssistantNumberOfRings" minOccurs="0"/>
 *         <element name="enableRollover" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="rolloverWaitTimeSeconds" type="{}ExecutiveRolloverWaitTimeSeconds" minOccurs="0"/>
 *         <element name="rolloverAction" type="{}ExecutiveRolloverActionType" minOccurs="0"/>
 *         <element name="rolloverForwardToPhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserExecutiveModifyScreeningAlertingRequest", propOrder = {
    "userId",
    "enableScreening",
    "screeningAlertType",
    "alertBroadWorksMobilityLocation",
    "alertBroadWorksAnywhereLocations",
    "alertSharedCallAppearanceLocations",
    "alertingMode",
    "alertingCallingLineIdNameMode",
    "alertingCustomCallingLineIdName",
    "unicodeAlertingCustomCallingLineIdName",
    "alertingCallingLineIdPhoneNumberMode",
    "alertingCustomCallingLineIdPhoneNumber",
    "callPushRecallNumberOfRings",
    "nextAssistantNumberOfRings",
    "enableRollover",
    "rolloverWaitTimeSeconds",
    "rolloverAction",
    "rolloverForwardToPhoneNumber"
})
public class UserExecutiveModifyScreeningAlertingRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    protected Boolean enableScreening;
    @XmlSchemaType(name = "token")
    protected ExecutiveScreeningAlertType screeningAlertType;
    protected Boolean alertBroadWorksMobilityLocation;
    protected Boolean alertBroadWorksAnywhereLocations;
    protected Boolean alertSharedCallAppearanceLocations;
    @XmlSchemaType(name = "token")
    protected ExecutiveAlertingMode alertingMode;
    @XmlSchemaType(name = "token")
    protected ExecutiveAlertingCallingLineIdNameMode alertingCallingLineIdNameMode;
    @XmlElementRef(name = "alertingCustomCallingLineIdName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> alertingCustomCallingLineIdName;
    @XmlElementRef(name = "unicodeAlertingCustomCallingLineIdName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> unicodeAlertingCustomCallingLineIdName;
    @XmlSchemaType(name = "token")
    protected ExecutiveAlertingCallingLineIdPhoneNumberMode alertingCallingLineIdPhoneNumberMode;
    @XmlElementRef(name = "alertingCustomCallingLineIdPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> alertingCustomCallingLineIdPhoneNumber;
    protected Integer callPushRecallNumberOfRings;
    protected Integer nextAssistantNumberOfRings;
    protected Boolean enableRollover;
    @XmlElementRef(name = "rolloverWaitTimeSeconds", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> rolloverWaitTimeSeconds;
    @XmlSchemaType(name = "token")
    protected ExecutiveRolloverActionType rolloverAction;
    @XmlElementRef(name = "rolloverForwardToPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rolloverForwardToPhoneNumber;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der enableScreening-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableScreening() {
        return enableScreening;
    }

    /**
     * Legt den Wert der enableScreening-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableScreening(Boolean value) {
        this.enableScreening = value;
    }

    /**
     * Ruft den Wert der screeningAlertType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveScreeningAlertType }
     *     
     */
    public ExecutiveScreeningAlertType getScreeningAlertType() {
        return screeningAlertType;
    }

    /**
     * Legt den Wert der screeningAlertType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveScreeningAlertType }
     *     
     */
    public void setScreeningAlertType(ExecutiveScreeningAlertType value) {
        this.screeningAlertType = value;
    }

    /**
     * Ruft den Wert der alertBroadWorksMobilityLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlertBroadWorksMobilityLocation() {
        return alertBroadWorksMobilityLocation;
    }

    /**
     * Legt den Wert der alertBroadWorksMobilityLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlertBroadWorksMobilityLocation(Boolean value) {
        this.alertBroadWorksMobilityLocation = value;
    }

    /**
     * Ruft den Wert der alertBroadWorksAnywhereLocations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlertBroadWorksAnywhereLocations() {
        return alertBroadWorksAnywhereLocations;
    }

    /**
     * Legt den Wert der alertBroadWorksAnywhereLocations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlertBroadWorksAnywhereLocations(Boolean value) {
        this.alertBroadWorksAnywhereLocations = value;
    }

    /**
     * Ruft den Wert der alertSharedCallAppearanceLocations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlertSharedCallAppearanceLocations() {
        return alertSharedCallAppearanceLocations;
    }

    /**
     * Legt den Wert der alertSharedCallAppearanceLocations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlertSharedCallAppearanceLocations(Boolean value) {
        this.alertSharedCallAppearanceLocations = value;
    }

    /**
     * Ruft den Wert der alertingMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveAlertingMode }
     *     
     */
    public ExecutiveAlertingMode getAlertingMode() {
        return alertingMode;
    }

    /**
     * Legt den Wert der alertingMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveAlertingMode }
     *     
     */
    public void setAlertingMode(ExecutiveAlertingMode value) {
        this.alertingMode = value;
    }

    /**
     * Ruft den Wert der alertingCallingLineIdNameMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveAlertingCallingLineIdNameMode }
     *     
     */
    public ExecutiveAlertingCallingLineIdNameMode getAlertingCallingLineIdNameMode() {
        return alertingCallingLineIdNameMode;
    }

    /**
     * Legt den Wert der alertingCallingLineIdNameMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveAlertingCallingLineIdNameMode }
     *     
     */
    public void setAlertingCallingLineIdNameMode(ExecutiveAlertingCallingLineIdNameMode value) {
        this.alertingCallingLineIdNameMode = value;
    }

    /**
     * Ruft den Wert der alertingCustomCallingLineIdName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAlertingCustomCallingLineIdName() {
        return alertingCustomCallingLineIdName;
    }

    /**
     * Legt den Wert der alertingCustomCallingLineIdName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAlertingCustomCallingLineIdName(JAXBElement<String> value) {
        this.alertingCustomCallingLineIdName = value;
    }

    /**
     * Ruft den Wert der unicodeAlertingCustomCallingLineIdName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUnicodeAlertingCustomCallingLineIdName() {
        return unicodeAlertingCustomCallingLineIdName;
    }

    /**
     * Legt den Wert der unicodeAlertingCustomCallingLineIdName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUnicodeAlertingCustomCallingLineIdName(JAXBElement<String> value) {
        this.unicodeAlertingCustomCallingLineIdName = value;
    }

    /**
     * Ruft den Wert der alertingCallingLineIdPhoneNumberMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveAlertingCallingLineIdPhoneNumberMode }
     *     
     */
    public ExecutiveAlertingCallingLineIdPhoneNumberMode getAlertingCallingLineIdPhoneNumberMode() {
        return alertingCallingLineIdPhoneNumberMode;
    }

    /**
     * Legt den Wert der alertingCallingLineIdPhoneNumberMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveAlertingCallingLineIdPhoneNumberMode }
     *     
     */
    public void setAlertingCallingLineIdPhoneNumberMode(ExecutiveAlertingCallingLineIdPhoneNumberMode value) {
        this.alertingCallingLineIdPhoneNumberMode = value;
    }

    /**
     * Ruft den Wert der alertingCustomCallingLineIdPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAlertingCustomCallingLineIdPhoneNumber() {
        return alertingCustomCallingLineIdPhoneNumber;
    }

    /**
     * Legt den Wert der alertingCustomCallingLineIdPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAlertingCustomCallingLineIdPhoneNumber(JAXBElement<String> value) {
        this.alertingCustomCallingLineIdPhoneNumber = value;
    }

    /**
     * Ruft den Wert der callPushRecallNumberOfRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCallPushRecallNumberOfRings() {
        return callPushRecallNumberOfRings;
    }

    /**
     * Legt den Wert der callPushRecallNumberOfRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCallPushRecallNumberOfRings(Integer value) {
        this.callPushRecallNumberOfRings = value;
    }

    /**
     * Ruft den Wert der nextAssistantNumberOfRings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNextAssistantNumberOfRings() {
        return nextAssistantNumberOfRings;
    }

    /**
     * Legt den Wert der nextAssistantNumberOfRings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNextAssistantNumberOfRings(Integer value) {
        this.nextAssistantNumberOfRings = value;
    }

    /**
     * Ruft den Wert der enableRollover-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableRollover() {
        return enableRollover;
    }

    /**
     * Legt den Wert der enableRollover-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableRollover(Boolean value) {
        this.enableRollover = value;
    }

    /**
     * Ruft den Wert der rolloverWaitTimeSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getRolloverWaitTimeSeconds() {
        return rolloverWaitTimeSeconds;
    }

    /**
     * Legt den Wert der rolloverWaitTimeSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setRolloverWaitTimeSeconds(JAXBElement<Integer> value) {
        this.rolloverWaitTimeSeconds = value;
    }

    /**
     * Ruft den Wert der rolloverAction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExecutiveRolloverActionType }
     *     
     */
    public ExecutiveRolloverActionType getRolloverAction() {
        return rolloverAction;
    }

    /**
     * Legt den Wert der rolloverAction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutiveRolloverActionType }
     *     
     */
    public void setRolloverAction(ExecutiveRolloverActionType value) {
        this.rolloverAction = value;
    }

    /**
     * Ruft den Wert der rolloverForwardToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRolloverForwardToPhoneNumber() {
        return rolloverForwardToPhoneNumber;
    }

    /**
     * Legt den Wert der rolloverForwardToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRolloverForwardToPhoneNumber(JAXBElement<String> value) {
        this.rolloverForwardToPhoneNumber = value;
    }

}
