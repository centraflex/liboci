//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Replaced by: SystemEnhancedCallLogsGetResponse24.
 *         Response to SystemEnhancedCallLogsGetRequest22.
 *       
 * 
 * <p>Java-Klasse für SystemEnhancedCallLogsGetResponse22 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemEnhancedCallLogsGetResponse22">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="maxNonPagedResponseSize" type="{}EnhancedCallLogsNonPagedResponseSize"/>
 *         <element name="eclQueryApplicationURL" type="{}URL" minOccurs="0"/>
 *         <element name="eclQueryDataRepositoryURL" type="{}URL" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemEnhancedCallLogsGetResponse22", propOrder = {
    "maxNonPagedResponseSize",
    "eclQueryApplicationURL",
    "eclQueryDataRepositoryURL"
})
public class SystemEnhancedCallLogsGetResponse22
    extends OCIDataResponse
{

    protected int maxNonPagedResponseSize;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String eclQueryApplicationURL;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String eclQueryDataRepositoryURL;

    /**
     * Ruft den Wert der maxNonPagedResponseSize-Eigenschaft ab.
     * 
     */
    public int getMaxNonPagedResponseSize() {
        return maxNonPagedResponseSize;
    }

    /**
     * Legt den Wert der maxNonPagedResponseSize-Eigenschaft fest.
     * 
     */
    public void setMaxNonPagedResponseSize(int value) {
        this.maxNonPagedResponseSize = value;
    }

    /**
     * Ruft den Wert der eclQueryApplicationURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEclQueryApplicationURL() {
        return eclQueryApplicationURL;
    }

    /**
     * Legt den Wert der eclQueryApplicationURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEclQueryApplicationURL(String value) {
        this.eclQueryApplicationURL = value;
    }

    /**
     * Ruft den Wert der eclQueryDataRepositoryURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEclQueryDataRepositoryURL() {
        return eclQueryDataRepositoryURL;
    }

    /**
     * Legt den Wert der eclQueryDataRepositoryURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEclQueryDataRepositoryURL(String value) {
        this.eclQueryDataRepositoryURL = value;
    }

}
