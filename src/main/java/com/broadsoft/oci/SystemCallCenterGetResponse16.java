//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCallCenterGetRequest16.
 *       
 * 
 * <p>Java-Klasse für SystemCallCenterGetResponse16 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallCenterGetResponse16">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="defaultFromAddress" type="{}EmailAddress"/>
 *         <element name="statisticsSamplingPeriodMinutes" type="{}CallCenterStatisticsSamplingPeriodMinutes"/>
 *         <element name="defaultEnableGuardTimer" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="defaultGuardTimerSeconds" type="{}CallCenterGuardTimerSeconds"/>
 *         <element name="forceAgentUnavailableOnDNDActivation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forceAgentUnavailableOnPersonalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="forceAgentUnavailableOnBouncedCallLimit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="numberConsecutiveBouncedCallsToForceAgentUnavailable" type="{}CallCenterConsecutiveBouncedCallsToForceAgentUnavailable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallCenterGetResponse16", propOrder = {
    "defaultFromAddress",
    "statisticsSamplingPeriodMinutes",
    "defaultEnableGuardTimer",
    "defaultGuardTimerSeconds",
    "forceAgentUnavailableOnDNDActivation",
    "forceAgentUnavailableOnPersonalCalls",
    "forceAgentUnavailableOnBouncedCallLimit",
    "numberConsecutiveBouncedCallsToForceAgentUnavailable"
})
public class SystemCallCenterGetResponse16
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String defaultFromAddress;
    protected int statisticsSamplingPeriodMinutes;
    protected boolean defaultEnableGuardTimer;
    protected int defaultGuardTimerSeconds;
    protected boolean forceAgentUnavailableOnDNDActivation;
    protected boolean forceAgentUnavailableOnPersonalCalls;
    protected boolean forceAgentUnavailableOnBouncedCallLimit;
    protected int numberConsecutiveBouncedCallsToForceAgentUnavailable;

    /**
     * Ruft den Wert der defaultFromAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultFromAddress() {
        return defaultFromAddress;
    }

    /**
     * Legt den Wert der defaultFromAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultFromAddress(String value) {
        this.defaultFromAddress = value;
    }

    /**
     * Ruft den Wert der statisticsSamplingPeriodMinutes-Eigenschaft ab.
     * 
     */
    public int getStatisticsSamplingPeriodMinutes() {
        return statisticsSamplingPeriodMinutes;
    }

    /**
     * Legt den Wert der statisticsSamplingPeriodMinutes-Eigenschaft fest.
     * 
     */
    public void setStatisticsSamplingPeriodMinutes(int value) {
        this.statisticsSamplingPeriodMinutes = value;
    }

    /**
     * Ruft den Wert der defaultEnableGuardTimer-Eigenschaft ab.
     * 
     */
    public boolean isDefaultEnableGuardTimer() {
        return defaultEnableGuardTimer;
    }

    /**
     * Legt den Wert der defaultEnableGuardTimer-Eigenschaft fest.
     * 
     */
    public void setDefaultEnableGuardTimer(boolean value) {
        this.defaultEnableGuardTimer = value;
    }

    /**
     * Ruft den Wert der defaultGuardTimerSeconds-Eigenschaft ab.
     * 
     */
    public int getDefaultGuardTimerSeconds() {
        return defaultGuardTimerSeconds;
    }

    /**
     * Legt den Wert der defaultGuardTimerSeconds-Eigenschaft fest.
     * 
     */
    public void setDefaultGuardTimerSeconds(int value) {
        this.defaultGuardTimerSeconds = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnDNDActivation-Eigenschaft ab.
     * 
     */
    public boolean isForceAgentUnavailableOnDNDActivation() {
        return forceAgentUnavailableOnDNDActivation;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnDNDActivation-Eigenschaft fest.
     * 
     */
    public void setForceAgentUnavailableOnDNDActivation(boolean value) {
        this.forceAgentUnavailableOnDNDActivation = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnPersonalCalls-Eigenschaft ab.
     * 
     */
    public boolean isForceAgentUnavailableOnPersonalCalls() {
        return forceAgentUnavailableOnPersonalCalls;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnPersonalCalls-Eigenschaft fest.
     * 
     */
    public void setForceAgentUnavailableOnPersonalCalls(boolean value) {
        this.forceAgentUnavailableOnPersonalCalls = value;
    }

    /**
     * Ruft den Wert der forceAgentUnavailableOnBouncedCallLimit-Eigenschaft ab.
     * 
     */
    public boolean isForceAgentUnavailableOnBouncedCallLimit() {
        return forceAgentUnavailableOnBouncedCallLimit;
    }

    /**
     * Legt den Wert der forceAgentUnavailableOnBouncedCallLimit-Eigenschaft fest.
     * 
     */
    public void setForceAgentUnavailableOnBouncedCallLimit(boolean value) {
        this.forceAgentUnavailableOnBouncedCallLimit = value;
    }

    /**
     * Ruft den Wert der numberConsecutiveBouncedCallsToForceAgentUnavailable-Eigenschaft ab.
     * 
     */
    public int getNumberConsecutiveBouncedCallsToForceAgentUnavailable() {
        return numberConsecutiveBouncedCallsToForceAgentUnavailable;
    }

    /**
     * Legt den Wert der numberConsecutiveBouncedCallsToForceAgentUnavailable-Eigenschaft fest.
     * 
     */
    public void setNumberConsecutiveBouncedCallsToForceAgentUnavailable(int value) {
        this.numberConsecutiveBouncedCallsToForceAgentUnavailable = value;
    }

}
