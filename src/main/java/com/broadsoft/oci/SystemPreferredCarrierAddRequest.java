//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Add a carrier to the system.
 *         More than one carrier may be assigned to each country code.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemPreferredCarrierAddRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemPreferredCarrierAddRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="carrier" type="{}PreferredCarrierName"/>
 *         <element name="cic" type="{}PreferredCarrierIdCode"/>
 *         <element name="countryCode" type="{}CountryCode"/>
 *         <element name="isIntraLata" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isInterLata" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isInternational" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemPreferredCarrierAddRequest", propOrder = {
    "carrier",
    "cic",
    "countryCode",
    "isIntraLata",
    "isInterLata",
    "isInternational"
})
public class SystemPreferredCarrierAddRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String carrier;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String cic;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NMTOKEN")
    protected String countryCode;
    protected boolean isIntraLata;
    protected boolean isInterLata;
    protected boolean isInternational;

    /**
     * Ruft den Wert der carrier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     * Legt den Wert der carrier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrier(String value) {
        this.carrier = value;
    }

    /**
     * Ruft den Wert der cic-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCic() {
        return cic;
    }

    /**
     * Legt den Wert der cic-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCic(String value) {
        this.cic = value;
    }

    /**
     * Ruft den Wert der countryCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Legt den Wert der countryCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Ruft den Wert der isIntraLata-Eigenschaft ab.
     * 
     */
    public boolean isIsIntraLata() {
        return isIntraLata;
    }

    /**
     * Legt den Wert der isIntraLata-Eigenschaft fest.
     * 
     */
    public void setIsIntraLata(boolean value) {
        this.isIntraLata = value;
    }

    /**
     * Ruft den Wert der isInterLata-Eigenschaft ab.
     * 
     */
    public boolean isIsInterLata() {
        return isInterLata;
    }

    /**
     * Legt den Wert der isInterLata-Eigenschaft fest.
     * 
     */
    public void setIsInterLata(boolean value) {
        this.isInterLata = value;
    }

    /**
     * Ruft den Wert der isInternational-Eigenschaft ab.
     * 
     */
    public boolean isIsInternational() {
        return isInternational;
    }

    /**
     * Legt den Wert der isInternational-Eigenschaft fest.
     * 
     */
    public void setIsInternational(boolean value) {
        this.isInternational = value;
    }

}
