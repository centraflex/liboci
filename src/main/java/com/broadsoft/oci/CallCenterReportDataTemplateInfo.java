//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Call center reporting data template info.
 *       
 * 
 * <p>Java-Klasse für CallCenterReportDataTemplateInfo complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="CallCenterReportDataTemplateInfo">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="dataTemplate" type="{}CallCenterReportDataTemplateName"/>
 *         <element name="reportType" type="{}CallCenterReportType"/>
 *         <element name="isRealtimeReport" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="isAgentParamRequired" type="{}CallCenterReportDataTemplateInputParameterOption"/>
 *         <element name="isCallCenterParamRequired" type="{}CallCenterReportDataTemplateInputParameterOption"/>
 *         <element name="isCallCenterDnisParamRequired" type="{}CallCenterReportDataTemplateInputParameterOption"/>
 *         <element name="isSamplingPeriodParamRequired" type="{}CallCenterReportDataTemplateInputParameterOption"/>
 *         <element name="isCallCompletionThresholdParamRequired" type="{}CallCenterReportDataTemplateInputParameterOption"/>
 *         <element name="isShortDurationThresholdParamRequired" type="{}CallCenterReportDataTemplateInputParameterOption"/>
 *         <element name="isServiceLevelThresholdParamRequired" type="{}CallCenterReportDataTemplateInputParameterOption"/>
 *         <element name="isServiceLevelInclusionsParamRequired" type="{}CallCenterReportDataTemplateInputParameterOption"/>
 *         <element name="isServiceLevelObjectiveThresholdParamRequired" type="{}CallCenterReportDataTemplateInputParameterOption"/>
 *         <element name="isAbandonedCallThresholdParamRequired" type="{}CallCenterReportDataTemplateInputParameterOption"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CallCenterReportDataTemplateInfo", propOrder = {
    "dataTemplate",
    "reportType",
    "isRealtimeReport",
    "isAgentParamRequired",
    "isCallCenterParamRequired",
    "isCallCenterDnisParamRequired",
    "isSamplingPeriodParamRequired",
    "isCallCompletionThresholdParamRequired",
    "isShortDurationThresholdParamRequired",
    "isServiceLevelThresholdParamRequired",
    "isServiceLevelInclusionsParamRequired",
    "isServiceLevelObjectiveThresholdParamRequired",
    "isAbandonedCallThresholdParamRequired"
})
public class CallCenterReportDataTemplateInfo {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String dataTemplate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportType reportType;
    protected boolean isRealtimeReport;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportDataTemplateInputParameterOption isAgentParamRequired;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportDataTemplateInputParameterOption isCallCenterParamRequired;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportDataTemplateInputParameterOption isCallCenterDnisParamRequired;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportDataTemplateInputParameterOption isSamplingPeriodParamRequired;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportDataTemplateInputParameterOption isCallCompletionThresholdParamRequired;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportDataTemplateInputParameterOption isShortDurationThresholdParamRequired;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportDataTemplateInputParameterOption isServiceLevelThresholdParamRequired;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportDataTemplateInputParameterOption isServiceLevelInclusionsParamRequired;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportDataTemplateInputParameterOption isServiceLevelObjectiveThresholdParamRequired;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallCenterReportDataTemplateInputParameterOption isAbandonedCallThresholdParamRequired;

    /**
     * Ruft den Wert der dataTemplate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataTemplate() {
        return dataTemplate;
    }

    /**
     * Legt den Wert der dataTemplate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataTemplate(String value) {
        this.dataTemplate = value;
    }

    /**
     * Ruft den Wert der reportType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportType }
     *     
     */
    public CallCenterReportType getReportType() {
        return reportType;
    }

    /**
     * Legt den Wert der reportType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportType }
     *     
     */
    public void setReportType(CallCenterReportType value) {
        this.reportType = value;
    }

    /**
     * Ruft den Wert der isRealtimeReport-Eigenschaft ab.
     * 
     */
    public boolean isIsRealtimeReport() {
        return isRealtimeReport;
    }

    /**
     * Legt den Wert der isRealtimeReport-Eigenschaft fest.
     * 
     */
    public void setIsRealtimeReport(boolean value) {
        this.isRealtimeReport = value;
    }

    /**
     * Ruft den Wert der isAgentParamRequired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public CallCenterReportDataTemplateInputParameterOption getIsAgentParamRequired() {
        return isAgentParamRequired;
    }

    /**
     * Legt den Wert der isAgentParamRequired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public void setIsAgentParamRequired(CallCenterReportDataTemplateInputParameterOption value) {
        this.isAgentParamRequired = value;
    }

    /**
     * Ruft den Wert der isCallCenterParamRequired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public CallCenterReportDataTemplateInputParameterOption getIsCallCenterParamRequired() {
        return isCallCenterParamRequired;
    }

    /**
     * Legt den Wert der isCallCenterParamRequired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public void setIsCallCenterParamRequired(CallCenterReportDataTemplateInputParameterOption value) {
        this.isCallCenterParamRequired = value;
    }

    /**
     * Ruft den Wert der isCallCenterDnisParamRequired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public CallCenterReportDataTemplateInputParameterOption getIsCallCenterDnisParamRequired() {
        return isCallCenterDnisParamRequired;
    }

    /**
     * Legt den Wert der isCallCenterDnisParamRequired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public void setIsCallCenterDnisParamRequired(CallCenterReportDataTemplateInputParameterOption value) {
        this.isCallCenterDnisParamRequired = value;
    }

    /**
     * Ruft den Wert der isSamplingPeriodParamRequired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public CallCenterReportDataTemplateInputParameterOption getIsSamplingPeriodParamRequired() {
        return isSamplingPeriodParamRequired;
    }

    /**
     * Legt den Wert der isSamplingPeriodParamRequired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public void setIsSamplingPeriodParamRequired(CallCenterReportDataTemplateInputParameterOption value) {
        this.isSamplingPeriodParamRequired = value;
    }

    /**
     * Ruft den Wert der isCallCompletionThresholdParamRequired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public CallCenterReportDataTemplateInputParameterOption getIsCallCompletionThresholdParamRequired() {
        return isCallCompletionThresholdParamRequired;
    }

    /**
     * Legt den Wert der isCallCompletionThresholdParamRequired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public void setIsCallCompletionThresholdParamRequired(CallCenterReportDataTemplateInputParameterOption value) {
        this.isCallCompletionThresholdParamRequired = value;
    }

    /**
     * Ruft den Wert der isShortDurationThresholdParamRequired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public CallCenterReportDataTemplateInputParameterOption getIsShortDurationThresholdParamRequired() {
        return isShortDurationThresholdParamRequired;
    }

    /**
     * Legt den Wert der isShortDurationThresholdParamRequired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public void setIsShortDurationThresholdParamRequired(CallCenterReportDataTemplateInputParameterOption value) {
        this.isShortDurationThresholdParamRequired = value;
    }

    /**
     * Ruft den Wert der isServiceLevelThresholdParamRequired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public CallCenterReportDataTemplateInputParameterOption getIsServiceLevelThresholdParamRequired() {
        return isServiceLevelThresholdParamRequired;
    }

    /**
     * Legt den Wert der isServiceLevelThresholdParamRequired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public void setIsServiceLevelThresholdParamRequired(CallCenterReportDataTemplateInputParameterOption value) {
        this.isServiceLevelThresholdParamRequired = value;
    }

    /**
     * Ruft den Wert der isServiceLevelInclusionsParamRequired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public CallCenterReportDataTemplateInputParameterOption getIsServiceLevelInclusionsParamRequired() {
        return isServiceLevelInclusionsParamRequired;
    }

    /**
     * Legt den Wert der isServiceLevelInclusionsParamRequired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public void setIsServiceLevelInclusionsParamRequired(CallCenterReportDataTemplateInputParameterOption value) {
        this.isServiceLevelInclusionsParamRequired = value;
    }

    /**
     * Ruft den Wert der isServiceLevelObjectiveThresholdParamRequired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public CallCenterReportDataTemplateInputParameterOption getIsServiceLevelObjectiveThresholdParamRequired() {
        return isServiceLevelObjectiveThresholdParamRequired;
    }

    /**
     * Legt den Wert der isServiceLevelObjectiveThresholdParamRequired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public void setIsServiceLevelObjectiveThresholdParamRequired(CallCenterReportDataTemplateInputParameterOption value) {
        this.isServiceLevelObjectiveThresholdParamRequired = value;
    }

    /**
     * Ruft den Wert der isAbandonedCallThresholdParamRequired-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public CallCenterReportDataTemplateInputParameterOption getIsAbandonedCallThresholdParamRequired() {
        return isAbandonedCallThresholdParamRequired;
    }

    /**
     * Legt den Wert der isAbandonedCallThresholdParamRequired-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallCenterReportDataTemplateInputParameterOption }
     *     
     */
    public void setIsAbandonedCallThresholdParamRequired(CallCenterReportDataTemplateInputParameterOption value) {
        this.isAbandonedCallThresholdParamRequired = value;
    }

}
