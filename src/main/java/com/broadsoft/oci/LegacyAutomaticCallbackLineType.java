//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für LegacyAutomaticCallbackLineType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="LegacyAutomaticCallbackLineType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Individual"/>
 *     <enumeration value="Coin"/>
 *     <enumeration value="Series"/>
 *     <enumeration value="Hunt"/>
 *     <enumeration value="Unassigned"/>
 *     <enumeration value="PBX"/>
 *     <enumeration value="Multiparty"/>
 *     <enumeration value="Choke"/>
 *     <enumeration value="Oos"/>
 *     <enumeration value="Nonspecific"/>
 *     <enumeration value="Telecampus"/>
 *     <enumeration value="ISDN"/>
 *     <enumeration value="Telekibutz"/>
 *     <enumeration value="Spare"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "LegacyAutomaticCallbackLineType")
@XmlEnum
public enum LegacyAutomaticCallbackLineType {

    @XmlEnumValue("Individual")
    INDIVIDUAL("Individual"),
    @XmlEnumValue("Coin")
    COIN("Coin"),
    @XmlEnumValue("Series")
    SERIES("Series"),
    @XmlEnumValue("Hunt")
    HUNT("Hunt"),
    @XmlEnumValue("Unassigned")
    UNASSIGNED("Unassigned"),
    PBX("PBX"),
    @XmlEnumValue("Multiparty")
    MULTIPARTY("Multiparty"),
    @XmlEnumValue("Choke")
    CHOKE("Choke"),
    @XmlEnumValue("Oos")
    OOS("Oos"),
    @XmlEnumValue("Nonspecific")
    NONSPECIFIC("Nonspecific"),
    @XmlEnumValue("Telecampus")
    TELECAMPUS("Telecampus"),
    ISDN("ISDN"),
    @XmlEnumValue("Telekibutz")
    TELEKIBUTZ("Telekibutz"),
    @XmlEnumValue("Spare")
    SPARE("Spare");
    private final String value;

    LegacyAutomaticCallbackLineType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LegacyAutomaticCallbackLineType fromValue(String v) {
        for (LegacyAutomaticCallbackLineType c: LegacyAutomaticCallbackLineType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
