//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the system level data associated with the Busy Lamp Field
 *         Service. The response is either a SuccessResponse or an 
 *         ErrorResponse.
 *         
 *         The following elements are only used in AS data mode:
 *          forceUseOfTCP
 *          
 *         The following elements are only used in AS data mode and ignored in Amplify and XS data mode:
 *          enableRedundancy
 *          redundancyTaskDelayMilliseconds
 *          redundancyTaskIntervalMilliseconds
 *          maxNumberOfSubscriptionsPerRedundancyTaskInterval
 *          
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *          ignoreUnansweredTerminatingCalls
 *       
 * 
 * <p>Java-Klasse für SystemBusyLampFieldModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemBusyLampFieldModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="displayLocalUserIdentityLastNameFirst" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="forceUseOfTCP" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="enableRedundancy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="redundancyTaskDelayMilliseconds" type="{}BusyLampFieldTaskDelayMilliseconds" minOccurs="0"/>
 *         <element name="redundancyTaskIntervalMilliseconds" type="{}BusyLampFieldTaskIntervalMilliseconds" minOccurs="0"/>
 *         <element name="maxNumberOfSubscriptionsPerRedundancyTaskInterval" type="{}BusyLampFieldMaxSubscriptionPerInterval" minOccurs="0"/>
 *         <element name="ignoreUnansweredTerminatingCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemBusyLampFieldModifyRequest", propOrder = {
    "displayLocalUserIdentityLastNameFirst",
    "forceUseOfTCP",
    "enableRedundancy",
    "redundancyTaskDelayMilliseconds",
    "redundancyTaskIntervalMilliseconds",
    "maxNumberOfSubscriptionsPerRedundancyTaskInterval",
    "ignoreUnansweredTerminatingCalls"
})
public class SystemBusyLampFieldModifyRequest
    extends OCIRequest
{

    protected Boolean displayLocalUserIdentityLastNameFirst;
    protected Boolean forceUseOfTCP;
    protected Boolean enableRedundancy;
    protected Integer redundancyTaskDelayMilliseconds;
    protected Integer redundancyTaskIntervalMilliseconds;
    protected Integer maxNumberOfSubscriptionsPerRedundancyTaskInterval;
    protected Boolean ignoreUnansweredTerminatingCalls;

    /**
     * Ruft den Wert der displayLocalUserIdentityLastNameFirst-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisplayLocalUserIdentityLastNameFirst() {
        return displayLocalUserIdentityLastNameFirst;
    }

    /**
     * Legt den Wert der displayLocalUserIdentityLastNameFirst-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayLocalUserIdentityLastNameFirst(Boolean value) {
        this.displayLocalUserIdentityLastNameFirst = value;
    }

    /**
     * Ruft den Wert der forceUseOfTCP-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceUseOfTCP() {
        return forceUseOfTCP;
    }

    /**
     * Legt den Wert der forceUseOfTCP-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceUseOfTCP(Boolean value) {
        this.forceUseOfTCP = value;
    }

    /**
     * Ruft den Wert der enableRedundancy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableRedundancy() {
        return enableRedundancy;
    }

    /**
     * Legt den Wert der enableRedundancy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableRedundancy(Boolean value) {
        this.enableRedundancy = value;
    }

    /**
     * Ruft den Wert der redundancyTaskDelayMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRedundancyTaskDelayMilliseconds() {
        return redundancyTaskDelayMilliseconds;
    }

    /**
     * Legt den Wert der redundancyTaskDelayMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRedundancyTaskDelayMilliseconds(Integer value) {
        this.redundancyTaskDelayMilliseconds = value;
    }

    /**
     * Ruft den Wert der redundancyTaskIntervalMilliseconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRedundancyTaskIntervalMilliseconds() {
        return redundancyTaskIntervalMilliseconds;
    }

    /**
     * Legt den Wert der redundancyTaskIntervalMilliseconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRedundancyTaskIntervalMilliseconds(Integer value) {
        this.redundancyTaskIntervalMilliseconds = value;
    }

    /**
     * Ruft den Wert der maxNumberOfSubscriptionsPerRedundancyTaskInterval-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxNumberOfSubscriptionsPerRedundancyTaskInterval() {
        return maxNumberOfSubscriptionsPerRedundancyTaskInterval;
    }

    /**
     * Legt den Wert der maxNumberOfSubscriptionsPerRedundancyTaskInterval-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxNumberOfSubscriptionsPerRedundancyTaskInterval(Integer value) {
        this.maxNumberOfSubscriptionsPerRedundancyTaskInterval = value;
    }

    /**
     * Ruft den Wert der ignoreUnansweredTerminatingCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreUnansweredTerminatingCalls() {
        return ignoreUnansweredTerminatingCalls;
    }

    /**
     * Legt den Wert der ignoreUnansweredTerminatingCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreUnansweredTerminatingCalls(Boolean value) {
        this.ignoreUnansweredTerminatingCalls = value;
    }

}
