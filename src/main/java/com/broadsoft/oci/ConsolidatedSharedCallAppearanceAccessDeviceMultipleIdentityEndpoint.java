//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Access device end point for Shared Call Appearance Service used in the context of add. 
 *         The endpoint is identified by its linePort (public Identity) and possibly a private Identity.
 *         Port numbers are only used by devices with static line ordering.
 *         
 *         In the case an access device referenced by accessDevice does not exist, the device will be added.
 *       
 *         The device attributes deviceType, protocol, netAddress, port, outboundProxyServerNetAddress, 
 *         stunServerNetAddress, macAddress, serialNumber, description, physicalLocation, transportProtocol,
 *         useCustomUserNamePassword and accessDeviceCredentials will be ignored if the access device already
 *         exists.
 *         
 *         The following elements are only used in XS data mode and ignored in AS data mode: 
 *           privateIdentity
 * 
 *       
 * 
 * <p>Java-Klasse für ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="accessDevice" type="{}AccessDevice"/>
 *         <element name="linePort" type="{}AccessDeviceEndpointLinePort"/>
 *         <element name="privateIdentity" type="{}AccessDeviceEndpointPrivateIdentity" minOccurs="0"/>
 *         <element name="contact" type="{}SIPContact" minOccurs="0"/>
 *         <element name="portNumber" type="{}AccessDevicePortNumber" minOccurs="0"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowOrigination" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowTermination" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="deviceType" type="{}AccessDeviceType" minOccurs="0"/>
 *         <element name="protocol" type="{}AccessDeviceProtocol" minOccurs="0"/>
 *         <element name="netAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="port" type="{}Port1025" minOccurs="0"/>
 *         <element name="outboundProxyServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="stunServerNetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="macAddress" type="{}AccessDeviceMACAddress" minOccurs="0"/>
 *         <element name="serialNumber" type="{}AccessDeviceSerialNumber" minOccurs="0"/>
 *         <element name="description" type="{}AccessDeviceDescription" minOccurs="0"/>
 *         <element name="physicalLocation" type="{}AccessDevicePhysicalLocation" minOccurs="0"/>
 *         <element name="transportProtocol" type="{}ExtendedTransportProtocol" minOccurs="0"/>
 *         <element name="useCustomUserNamePassword" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="accessDeviceCredentials" type="{}DeviceManagementUserNamePassword16" minOccurs="0"/>
 *         <element name="useHotline" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="hotlineContact" type="{}SIPURI" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint", propOrder = {
    "accessDevice",
    "linePort",
    "privateIdentity",
    "contact",
    "portNumber",
    "isActive",
    "allowOrigination",
    "allowTermination",
    "deviceType",
    "protocol",
    "netAddress",
    "port",
    "outboundProxyServerNetAddress",
    "stunServerNetAddress",
    "macAddress",
    "serialNumber",
    "description",
    "physicalLocation",
    "transportProtocol",
    "useCustomUserNamePassword",
    "accessDeviceCredentials",
    "useHotline",
    "hotlineContact"
})
public class ConsolidatedSharedCallAppearanceAccessDeviceMultipleIdentityEndpoint {

    @XmlElement(required = true)
    protected AccessDevice accessDevice;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String linePort;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String privateIdentity;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String contact;
    protected Integer portNumber;
    protected boolean isActive;
    protected boolean allowOrigination;
    protected boolean allowTermination;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deviceType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String protocol;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String netAddress;
    protected Integer port;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String outboundProxyServerNetAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String stunServerNetAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String macAddress;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serialNumber;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String description;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String physicalLocation;
    @XmlSchemaType(name = "token")
    protected ExtendedTransportProtocol transportProtocol;
    protected Boolean useCustomUserNamePassword;
    protected DeviceManagementUserNamePassword16 accessDeviceCredentials;
    protected Boolean useHotline;
    @XmlElementRef(name = "hotlineContact", type = JAXBElement.class, required = false)
    protected JAXBElement<String> hotlineContact;

    /**
     * Ruft den Wert der accessDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDevice }
     *     
     */
    public AccessDevice getAccessDevice() {
        return accessDevice;
    }

    /**
     * Legt den Wert der accessDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDevice }
     *     
     */
    public void setAccessDevice(AccessDevice value) {
        this.accessDevice = value;
    }

    /**
     * Ruft den Wert der linePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinePort() {
        return linePort;
    }

    /**
     * Legt den Wert der linePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinePort(String value) {
        this.linePort = value;
    }

    /**
     * Ruft den Wert der privateIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrivateIdentity() {
        return privateIdentity;
    }

    /**
     * Legt den Wert der privateIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrivateIdentity(String value) {
        this.privateIdentity = value;
    }

    /**
     * Ruft den Wert der contact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContact() {
        return contact;
    }

    /**
     * Legt den Wert der contact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContact(String value) {
        this.contact = value;
    }

    /**
     * Ruft den Wert der portNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPortNumber() {
        return portNumber;
    }

    /**
     * Legt den Wert der portNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPortNumber(Integer value) {
        this.portNumber = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der allowOrigination-Eigenschaft ab.
     * 
     */
    public boolean isAllowOrigination() {
        return allowOrigination;
    }

    /**
     * Legt den Wert der allowOrigination-Eigenschaft fest.
     * 
     */
    public void setAllowOrigination(boolean value) {
        this.allowOrigination = value;
    }

    /**
     * Ruft den Wert der allowTermination-Eigenschaft ab.
     * 
     */
    public boolean isAllowTermination() {
        return allowTermination;
    }

    /**
     * Legt den Wert der allowTermination-Eigenschaft fest.
     * 
     */
    public void setAllowTermination(boolean value) {
        this.allowTermination = value;
    }

    /**
     * Ruft den Wert der deviceType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * Legt den Wert der deviceType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceType(String value) {
        this.deviceType = value;
    }

    /**
     * Ruft den Wert der protocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocol() {
        return protocol;
    }

    /**
     * Legt den Wert der protocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocol(String value) {
        this.protocol = value;
    }

    /**
     * Ruft den Wert der netAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetAddress() {
        return netAddress;
    }

    /**
     * Legt den Wert der netAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetAddress(String value) {
        this.netAddress = value;
    }

    /**
     * Ruft den Wert der port-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPort() {
        return port;
    }

    /**
     * Legt den Wert der port-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPort(Integer value) {
        this.port = value;
    }

    /**
     * Ruft den Wert der outboundProxyServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutboundProxyServerNetAddress() {
        return outboundProxyServerNetAddress;
    }

    /**
     * Legt den Wert der outboundProxyServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutboundProxyServerNetAddress(String value) {
        this.outboundProxyServerNetAddress = value;
    }

    /**
     * Ruft den Wert der stunServerNetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStunServerNetAddress() {
        return stunServerNetAddress;
    }

    /**
     * Legt den Wert der stunServerNetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStunServerNetAddress(String value) {
        this.stunServerNetAddress = value;
    }

    /**
     * Ruft den Wert der macAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacAddress() {
        return macAddress;
    }

    /**
     * Legt den Wert der macAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacAddress(String value) {
        this.macAddress = value;
    }

    /**
     * Ruft den Wert der serialNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Legt den Wert der serialNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNumber(String value) {
        this.serialNumber = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der physicalLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhysicalLocation() {
        return physicalLocation;
    }

    /**
     * Legt den Wert der physicalLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhysicalLocation(String value) {
        this.physicalLocation = value;
    }

    /**
     * Ruft den Wert der transportProtocol-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedTransportProtocol }
     *     
     */
    public ExtendedTransportProtocol getTransportProtocol() {
        return transportProtocol;
    }

    /**
     * Legt den Wert der transportProtocol-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedTransportProtocol }
     *     
     */
    public void setTransportProtocol(ExtendedTransportProtocol value) {
        this.transportProtocol = value;
    }

    /**
     * Ruft den Wert der useCustomUserNamePassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseCustomUserNamePassword() {
        return useCustomUserNamePassword;
    }

    /**
     * Legt den Wert der useCustomUserNamePassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseCustomUserNamePassword(Boolean value) {
        this.useCustomUserNamePassword = value;
    }

    /**
     * Ruft den Wert der accessDeviceCredentials-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DeviceManagementUserNamePassword16 }
     *     
     */
    public DeviceManagementUserNamePassword16 getAccessDeviceCredentials() {
        return accessDeviceCredentials;
    }

    /**
     * Legt den Wert der accessDeviceCredentials-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceManagementUserNamePassword16 }
     *     
     */
    public void setAccessDeviceCredentials(DeviceManagementUserNamePassword16 value) {
        this.accessDeviceCredentials = value;
    }

    /**
     * Ruft den Wert der useHotline-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseHotline() {
        return useHotline;
    }

    /**
     * Legt den Wert der useHotline-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseHotline(Boolean value) {
        this.useHotline = value;
    }

    /**
     * Ruft den Wert der hotlineContact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHotlineContact() {
        return hotlineContact;
    }

    /**
     * Legt den Wert der hotlineContact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHotlineContact(JAXBElement<String> value) {
        this.hotlineContact = value;
    }

}
