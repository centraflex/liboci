//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentListRequest.
 *         The response contains a table with columns: "Mobile Number", "User Id",
 *         "Last Name", "First Name","Phone Number","Extension", "Group Id", "Department" and "Mobile Network".
 *         The "Mobile Number" column contains a single DN.
 *         The "User Id", "Last Name" and "First Name" columns contains the corresponding attributes of the user possessing the DN(s).
 *         The "Phone Number" column contains a single DN.
 *         The "Group Id"   column contains the Group Id of the user.
 *         The "Department" column contains the department of the user if it is part of a department.
 *         The "Mobile Network" column contains the Mobile Network the number belongs to.
 *       
 * 
 * <p>Java-Klasse für EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="mobileSubscriberDirectoryNumbersAssignmentTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentListResponse", propOrder = {
    "mobileSubscriberDirectoryNumbersAssignmentTable"
})
public class EnterpriseBroadWorksMobilityMobileSubscriberDirectoryNumberGetAssignmentListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable mobileSubscriberDirectoryNumbersAssignmentTable;

    /**
     * Ruft den Wert der mobileSubscriberDirectoryNumbersAssignmentTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getMobileSubscriberDirectoryNumbersAssignmentTable() {
        return mobileSubscriberDirectoryNumbersAssignmentTable;
    }

    /**
     * Legt den Wert der mobileSubscriberDirectoryNumbersAssignmentTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setMobileSubscriberDirectoryNumbersAssignmentTable(OCITable value) {
        this.mobileSubscriberDirectoryNumbersAssignmentTable = value;
    }

}
