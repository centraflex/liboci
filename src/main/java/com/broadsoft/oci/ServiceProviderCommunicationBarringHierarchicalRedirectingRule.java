//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Service Provider Communication Barring Hierarchical Redirecting Rule
 *       
 * 
 * <p>Java-Klasse für ServiceProviderCommunicationBarringHierarchicalRedirectingRule complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderCommunicationBarringHierarchicalRedirectingRule">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <choice>
 *           <element name="criteria" type="{}CommunicationBarringCriteriaName"/>
 *           <element name="digitPatternCriteria" type="{}DigitPatternCriteriaName"/>
 *         </choice>
 *         <element name="action" type="{}CommunicationBarringRedirectingAction"/>
 *         <element name="callTimeoutSeconds" type="{}CommunicationBarringTimeoutSeconds" minOccurs="0"/>
 *         <element name="timeSchedule" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="priority" type="{}CommunicationBarringCriteriaPriority"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderCommunicationBarringHierarchicalRedirectingRule", propOrder = {
    "criteria",
    "digitPatternCriteria",
    "action",
    "callTimeoutSeconds",
    "timeSchedule",
    "holidaySchedule",
    "priority"
})
public class ServiceProviderCommunicationBarringHierarchicalRedirectingRule {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String criteria;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String digitPatternCriteria;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CommunicationBarringRedirectingAction action;
    protected Integer callTimeoutSeconds;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String timeSchedule;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String holidaySchedule;
    protected float priority;

    /**
     * Ruft den Wert der criteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCriteria() {
        return criteria;
    }

    /**
     * Legt den Wert der criteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCriteria(String value) {
        this.criteria = value;
    }

    /**
     * Ruft den Wert der digitPatternCriteria-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDigitPatternCriteria() {
        return digitPatternCriteria;
    }

    /**
     * Legt den Wert der digitPatternCriteria-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDigitPatternCriteria(String value) {
        this.digitPatternCriteria = value;
    }

    /**
     * Ruft den Wert der action-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CommunicationBarringRedirectingAction }
     *     
     */
    public CommunicationBarringRedirectingAction getAction() {
        return action;
    }

    /**
     * Legt den Wert der action-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunicationBarringRedirectingAction }
     *     
     */
    public void setAction(CommunicationBarringRedirectingAction value) {
        this.action = value;
    }

    /**
     * Ruft den Wert der callTimeoutSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCallTimeoutSeconds() {
        return callTimeoutSeconds;
    }

    /**
     * Legt den Wert der callTimeoutSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCallTimeoutSeconds(Integer value) {
        this.callTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeSchedule(String value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHolidaySchedule(String value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der priority-Eigenschaft ab.
     * 
     */
    public float getPriority() {
        return priority;
    }

    /**
     * Legt den Wert der priority-Eigenschaft fest.
     * 
     */
    public void setPriority(float value) {
        this.priority = value;
    }

}
