//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Requests a list of non-obsolete Identity/device profile types defined in the system. It is possible
 *         to get either all conference device types or all non-conferernce types. This command is similar
 *         to the SystemSIPDeviceTypeGetListRequest and SystemMGCPDeviceTypeGetListRequest
 *         but gets both SIP and MGCP types.
 *         The response is either SystemDeviceTypeGetlAvailableListResponse14sp1 or ErrorResponse.
 *         Replaced By: SystemDeviceTypeGetAvailableListRequest14sp3
 *       
 * 
 * <p>Java-Klasse für SystemDeviceTypeGetAvailableListRequest14sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemDeviceTypeGetAvailableListRequest14sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="allowConference" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="allowMusicOnHold" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="onlyConference" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="onlyVideoCapable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="onlyOptionalIpAddress" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemDeviceTypeGetAvailableListRequest14sp1", propOrder = {
    "allowConference",
    "allowMusicOnHold",
    "onlyConference",
    "onlyVideoCapable",
    "onlyOptionalIpAddress"
})
public class SystemDeviceTypeGetAvailableListRequest14Sp1
    extends OCIRequest
{

    protected boolean allowConference;
    protected boolean allowMusicOnHold;
    protected boolean onlyConference;
    protected boolean onlyVideoCapable;
    protected boolean onlyOptionalIpAddress;

    /**
     * Ruft den Wert der allowConference-Eigenschaft ab.
     * 
     */
    public boolean isAllowConference() {
        return allowConference;
    }

    /**
     * Legt den Wert der allowConference-Eigenschaft fest.
     * 
     */
    public void setAllowConference(boolean value) {
        this.allowConference = value;
    }

    /**
     * Ruft den Wert der allowMusicOnHold-Eigenschaft ab.
     * 
     */
    public boolean isAllowMusicOnHold() {
        return allowMusicOnHold;
    }

    /**
     * Legt den Wert der allowMusicOnHold-Eigenschaft fest.
     * 
     */
    public void setAllowMusicOnHold(boolean value) {
        this.allowMusicOnHold = value;
    }

    /**
     * Ruft den Wert der onlyConference-Eigenschaft ab.
     * 
     */
    public boolean isOnlyConference() {
        return onlyConference;
    }

    /**
     * Legt den Wert der onlyConference-Eigenschaft fest.
     * 
     */
    public void setOnlyConference(boolean value) {
        this.onlyConference = value;
    }

    /**
     * Ruft den Wert der onlyVideoCapable-Eigenschaft ab.
     * 
     */
    public boolean isOnlyVideoCapable() {
        return onlyVideoCapable;
    }

    /**
     * Legt den Wert der onlyVideoCapable-Eigenschaft fest.
     * 
     */
    public void setOnlyVideoCapable(boolean value) {
        this.onlyVideoCapable = value;
    }

    /**
     * Ruft den Wert der onlyOptionalIpAddress-Eigenschaft ab.
     * 
     */
    public boolean isOnlyOptionalIpAddress() {
        return onlyOptionalIpAddress;
    }

    /**
     * Legt den Wert der onlyOptionalIpAddress-Eigenschaft fest.
     * 
     */
    public void setOnlyOptionalIpAddress(boolean value) {
        this.onlyOptionalIpAddress = value;
    }

}
