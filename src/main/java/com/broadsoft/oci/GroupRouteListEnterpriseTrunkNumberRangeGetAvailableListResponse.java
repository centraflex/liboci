//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupRouteListEnterpriseTrunkNumberRangeGetAvailableListRequest.
 *         Contains a list of number ranges that are assigned to a group and still available for assignment to users within the group.
 *         The column headings are "Number Range Start", "Number Range End" ,"Is Active" and “Extension Length”..
 *       
 * 
 * <p>Java-Klasse für GroupRouteListEnterpriseTrunkNumberRangeGetAvailableListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupRouteListEnterpriseTrunkNumberRangeGetAvailableListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="availableNumberRangeTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupRouteListEnterpriseTrunkNumberRangeGetAvailableListResponse", propOrder = {
    "availableNumberRangeTable"
})
public class GroupRouteListEnterpriseTrunkNumberRangeGetAvailableListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable availableNumberRangeTable;

    /**
     * Ruft den Wert der availableNumberRangeTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAvailableNumberRangeTable() {
        return availableNumberRangeTable;
    }

    /**
     * Legt den Wert der availableNumberRangeTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAvailableNumberRangeTable(OCITable value) {
        this.availableNumberRangeTable = value;
    }

}
