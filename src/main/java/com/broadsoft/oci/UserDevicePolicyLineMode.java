//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für UserDevicePolicyLineMode.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="UserDevicePolicyLineMode">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Single User Private and Shared"/>
 *     <enumeration value="Multiple User Shared"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "UserDevicePolicyLineMode")
@XmlEnum
public enum UserDevicePolicyLineMode {

    @XmlEnumValue("Single User Private and Shared")
    SINGLE_USER_PRIVATE_AND_SHARED("Single User Private and Shared"),
    @XmlEnumValue("Multiple User Shared")
    MULTIPLE_USER_SHARED("Multiple User Shared");
    private final String value;

    UserDevicePolicyLineMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UserDevicePolicyLineMode fromValue(String v) {
        for (UserDevicePolicyLineMode c: UserDevicePolicyLineMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
