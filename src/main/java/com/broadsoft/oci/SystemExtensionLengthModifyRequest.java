//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Modify the group's extension length range.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemExtensionLengthModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemExtensionLengthModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="minExtensionLength" type="{}ExtensionLength" minOccurs="0"/>
 *         <element name="maxExtensionLength" type="{}ExtensionLength" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemExtensionLengthModifyRequest", propOrder = {
    "minExtensionLength",
    "maxExtensionLength"
})
public class SystemExtensionLengthModifyRequest
    extends OCIRequest
{

    protected Integer minExtensionLength;
    protected Integer maxExtensionLength;

    /**
     * Ruft den Wert der minExtensionLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinExtensionLength() {
        return minExtensionLength;
    }

    /**
     * Legt den Wert der minExtensionLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinExtensionLength(Integer value) {
        this.minExtensionLength = value;
    }

    /**
     * Ruft den Wert der maxExtensionLength-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxExtensionLength() {
        return maxExtensionLength;
    }

    /**
     * Legt den Wert der maxExtensionLength-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxExtensionLength(Integer value) {
        this.maxExtensionLength = value;
    }

}
