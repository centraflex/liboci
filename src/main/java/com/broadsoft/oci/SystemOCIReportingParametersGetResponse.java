//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemOCIReportingParametersGetListRequest.
 *         Contains a list of system OCI Reporting parameters.
 *       
 * 
 * <p>Java-Klasse für SystemOCIReportingParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemOCIReportingParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="serverPort" type="{}Port1025"/>
 *         <element name="enableConnectionPing" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="connectionPingIntervalSeconds" type="{}OCIReportingConnectionPingIntervalSeconds"/>
 *         <element name="alterPasswords" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemOCIReportingParametersGetResponse", propOrder = {
    "serverPort",
    "enableConnectionPing",
    "connectionPingIntervalSeconds",
    "alterPasswords"
})
public class SystemOCIReportingParametersGetResponse
    extends OCIDataResponse
{

    protected int serverPort;
    protected boolean enableConnectionPing;
    protected int connectionPingIntervalSeconds;
    protected boolean alterPasswords;

    /**
     * Ruft den Wert der serverPort-Eigenschaft ab.
     * 
     */
    public int getServerPort() {
        return serverPort;
    }

    /**
     * Legt den Wert der serverPort-Eigenschaft fest.
     * 
     */
    public void setServerPort(int value) {
        this.serverPort = value;
    }

    /**
     * Ruft den Wert der enableConnectionPing-Eigenschaft ab.
     * 
     */
    public boolean isEnableConnectionPing() {
        return enableConnectionPing;
    }

    /**
     * Legt den Wert der enableConnectionPing-Eigenschaft fest.
     * 
     */
    public void setEnableConnectionPing(boolean value) {
        this.enableConnectionPing = value;
    }

    /**
     * Ruft den Wert der connectionPingIntervalSeconds-Eigenschaft ab.
     * 
     */
    public int getConnectionPingIntervalSeconds() {
        return connectionPingIntervalSeconds;
    }

    /**
     * Legt den Wert der connectionPingIntervalSeconds-Eigenschaft fest.
     * 
     */
    public void setConnectionPingIntervalSeconds(int value) {
        this.connectionPingIntervalSeconds = value;
    }

    /**
     * Ruft den Wert der alterPasswords-Eigenschaft ab.
     * 
     */
    public boolean isAlterPasswords() {
        return alterPasswords;
    }

    /**
     * Legt den Wert der alterPasswords-Eigenschaft fest.
     * 
     */
    public void setAlterPasswords(boolean value) {
        this.alterPasswords = value;
    }

}
