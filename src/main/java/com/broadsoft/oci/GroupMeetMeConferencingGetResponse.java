//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupMeetMeConferencingGetRequest.
 *       
 * 
 * <p>Java-Klasse für GroupMeetMeConferencingGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupMeetMeConferencingGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="availablePorts" type="{}MeetMeConferencingConferencePorts"/>
 *         <element name="allocatedPorts" type="{}MeetMeConferencingConferencePorts"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupMeetMeConferencingGetResponse", propOrder = {
    "availablePorts",
    "allocatedPorts"
})
public class GroupMeetMeConferencingGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected MeetMeConferencingConferencePorts availablePorts;
    @XmlElement(required = true)
    protected MeetMeConferencingConferencePorts allocatedPorts;

    /**
     * Ruft den Wert der availablePorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferencePorts }
     *     
     */
    public MeetMeConferencingConferencePorts getAvailablePorts() {
        return availablePorts;
    }

    /**
     * Legt den Wert der availablePorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferencePorts }
     *     
     */
    public void setAvailablePorts(MeetMeConferencingConferencePorts value) {
        this.availablePorts = value;
    }

    /**
     * Ruft den Wert der allocatedPorts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeetMeConferencingConferencePorts }
     *     
     */
    public MeetMeConferencingConferencePorts getAllocatedPorts() {
        return allocatedPorts;
    }

    /**
     * Legt den Wert der allocatedPorts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeetMeConferencingConferencePorts }
     *     
     */
    public void setAllocatedPorts(MeetMeConferencingConferencePorts value) {
        this.allocatedPorts = value;
    }

}
