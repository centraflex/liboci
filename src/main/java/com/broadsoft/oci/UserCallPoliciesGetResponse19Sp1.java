//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to UserCallPoliciesGetRequest19sp1.
 *       
 * 
 * <p>Java-Klasse für UserCallPoliciesGetResponse19sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserCallPoliciesGetResponse19sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="redirectedCallsCOLPPrivacy" type="{}ConnectedLineIdentificationPrivacyOnRedirectedCalls"/>
 *         <element name="callBeingForwardedResponseCallType" type="{}CallBeingForwardedResponseCallType"/>
 *         <element name="callingLineIdentityForRedirectedCalls" type="{}CallingLineIdentityForRedirectedCalls"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserCallPoliciesGetResponse19sp1", propOrder = {
    "redirectedCallsCOLPPrivacy",
    "callBeingForwardedResponseCallType",
    "callingLineIdentityForRedirectedCalls"
})
public class UserCallPoliciesGetResponse19Sp1
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected ConnectedLineIdentificationPrivacyOnRedirectedCalls redirectedCallsCOLPPrivacy;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallBeingForwardedResponseCallType callBeingForwardedResponseCallType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CallingLineIdentityForRedirectedCalls callingLineIdentityForRedirectedCalls;

    /**
     * Ruft den Wert der redirectedCallsCOLPPrivacy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ConnectedLineIdentificationPrivacyOnRedirectedCalls }
     *     
     */
    public ConnectedLineIdentificationPrivacyOnRedirectedCalls getRedirectedCallsCOLPPrivacy() {
        return redirectedCallsCOLPPrivacy;
    }

    /**
     * Legt den Wert der redirectedCallsCOLPPrivacy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ConnectedLineIdentificationPrivacyOnRedirectedCalls }
     *     
     */
    public void setRedirectedCallsCOLPPrivacy(ConnectedLineIdentificationPrivacyOnRedirectedCalls value) {
        this.redirectedCallsCOLPPrivacy = value;
    }

    /**
     * Ruft den Wert der callBeingForwardedResponseCallType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallBeingForwardedResponseCallType }
     *     
     */
    public CallBeingForwardedResponseCallType getCallBeingForwardedResponseCallType() {
        return callBeingForwardedResponseCallType;
    }

    /**
     * Legt den Wert der callBeingForwardedResponseCallType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallBeingForwardedResponseCallType }
     *     
     */
    public void setCallBeingForwardedResponseCallType(CallBeingForwardedResponseCallType value) {
        this.callBeingForwardedResponseCallType = value;
    }

    /**
     * Ruft den Wert der callingLineIdentityForRedirectedCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CallingLineIdentityForRedirectedCalls }
     *     
     */
    public CallingLineIdentityForRedirectedCalls getCallingLineIdentityForRedirectedCalls() {
        return callingLineIdentityForRedirectedCalls;
    }

    /**
     * Legt den Wert der callingLineIdentityForRedirectedCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CallingLineIdentityForRedirectedCalls }
     *     
     */
    public void setCallingLineIdentityForRedirectedCalls(CallingLineIdentityForRedirectedCalls value) {
        this.callingLineIdentityForRedirectedCalls = value;
    }

}
