//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Estimated Wait Message Options
 *       
 * 
 * <p>Java-Klasse für EstimatedWaitMessageOptionsModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="EstimatedWaitMessageOptionsModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="enabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="operatingMode" type="{}EstimatedWaitMessageOperatingMode" minOccurs="0"/>
 *         <element name="playPositionHighVolume" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="playTimeHighVolume" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="maximumPositions" type="{}EstimatedWaitMessageMaximumPositions" minOccurs="0"/>
 *         <element name="maximumWaitingMinutes" type="{}EstimatedWaitMessageMaximumWaitingMinutes" minOccurs="0"/>
 *         <element name="defaultCallHandlingMinutes" type="{}EstimatedWaitMessageDefaultCallHandlingMinutes" minOccurs="0"/>
 *         <element name="playUpdatedEWM" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="timeBetweenEWMUpdatesSeconds" type="{}EstimatedWaitMessageTimeBetweenUpdatesSeconds" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EstimatedWaitMessageOptionsModify", propOrder = {
    "enabled",
    "operatingMode",
    "playPositionHighVolume",
    "playTimeHighVolume",
    "maximumPositions",
    "maximumWaitingMinutes",
    "defaultCallHandlingMinutes",
    "playUpdatedEWM",
    "timeBetweenEWMUpdatesSeconds"
})
public class EstimatedWaitMessageOptionsModify {

    protected Boolean enabled;
    @XmlSchemaType(name = "token")
    protected EstimatedWaitMessageOperatingMode operatingMode;
    protected Boolean playPositionHighVolume;
    protected Boolean playTimeHighVolume;
    protected Integer maximumPositions;
    protected Integer maximumWaitingMinutes;
    protected Integer defaultCallHandlingMinutes;
    protected Boolean playUpdatedEWM;
    @XmlElementRef(name = "timeBetweenEWMUpdatesSeconds", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> timeBetweenEWMUpdatesSeconds;

    /**
     * Ruft den Wert der enabled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnabled() {
        return enabled;
    }

    /**
     * Legt den Wert der enabled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Ruft den Wert der operatingMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EstimatedWaitMessageOperatingMode }
     *     
     */
    public EstimatedWaitMessageOperatingMode getOperatingMode() {
        return operatingMode;
    }

    /**
     * Legt den Wert der operatingMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EstimatedWaitMessageOperatingMode }
     *     
     */
    public void setOperatingMode(EstimatedWaitMessageOperatingMode value) {
        this.operatingMode = value;
    }

    /**
     * Ruft den Wert der playPositionHighVolume-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayPositionHighVolume() {
        return playPositionHighVolume;
    }

    /**
     * Legt den Wert der playPositionHighVolume-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayPositionHighVolume(Boolean value) {
        this.playPositionHighVolume = value;
    }

    /**
     * Ruft den Wert der playTimeHighVolume-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayTimeHighVolume() {
        return playTimeHighVolume;
    }

    /**
     * Legt den Wert der playTimeHighVolume-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayTimeHighVolume(Boolean value) {
        this.playTimeHighVolume = value;
    }

    /**
     * Ruft den Wert der maximumPositions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaximumPositions() {
        return maximumPositions;
    }

    /**
     * Legt den Wert der maximumPositions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaximumPositions(Integer value) {
        this.maximumPositions = value;
    }

    /**
     * Ruft den Wert der maximumWaitingMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaximumWaitingMinutes() {
        return maximumWaitingMinutes;
    }

    /**
     * Legt den Wert der maximumWaitingMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaximumWaitingMinutes(Integer value) {
        this.maximumWaitingMinutes = value;
    }

    /**
     * Ruft den Wert der defaultCallHandlingMinutes-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDefaultCallHandlingMinutes() {
        return defaultCallHandlingMinutes;
    }

    /**
     * Legt den Wert der defaultCallHandlingMinutes-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDefaultCallHandlingMinutes(Integer value) {
        this.defaultCallHandlingMinutes = value;
    }

    /**
     * Ruft den Wert der playUpdatedEWM-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayUpdatedEWM() {
        return playUpdatedEWM;
    }

    /**
     * Legt den Wert der playUpdatedEWM-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayUpdatedEWM(Boolean value) {
        this.playUpdatedEWM = value;
    }

    /**
     * Ruft den Wert der timeBetweenEWMUpdatesSeconds-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getTimeBetweenEWMUpdatesSeconds() {
        return timeBetweenEWMUpdatesSeconds;
    }

    /**
     * Legt den Wert der timeBetweenEWMUpdatesSeconds-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setTimeBetweenEWMUpdatesSeconds(JAXBElement<Integer> value) {
        this.timeBetweenEWMUpdatesSeconds = value;
    }

}
