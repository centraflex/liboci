//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemAdviceOfChargeGetRequest.
 *         Contains a list of system Advice of Charge parameters.
 *       
 * 
 * <p>Java-Klasse für SystemAdviceOfChargeGetResponse19sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemAdviceOfChargeGetResponse19sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="delayBetweenNotificationSeconds" type="{}AdviceOfChargeDelayBetweenNotificationSeconds"/>
 *         <element name="incomingAocHandling" type="{}AdviceOfChargeIncomingAocHandling"/>
 *         <element name="useOCSEnquiry" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="OCSEnquiryType" type="{}AdviceOfChargeOCSEnquiryType"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemAdviceOfChargeGetResponse19sp1", propOrder = {
    "delayBetweenNotificationSeconds",
    "incomingAocHandling",
    "useOCSEnquiry",
    "ocsEnquiryType"
})
public class SystemAdviceOfChargeGetResponse19Sp1
    extends OCIDataResponse
{

    protected int delayBetweenNotificationSeconds;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AdviceOfChargeIncomingAocHandling incomingAocHandling;
    protected boolean useOCSEnquiry;
    @XmlElement(name = "OCSEnquiryType", required = true)
    @XmlSchemaType(name = "token")
    protected AdviceOfChargeOCSEnquiryType ocsEnquiryType;

    /**
     * Ruft den Wert der delayBetweenNotificationSeconds-Eigenschaft ab.
     * 
     */
    public int getDelayBetweenNotificationSeconds() {
        return delayBetweenNotificationSeconds;
    }

    /**
     * Legt den Wert der delayBetweenNotificationSeconds-Eigenschaft fest.
     * 
     */
    public void setDelayBetweenNotificationSeconds(int value) {
        this.delayBetweenNotificationSeconds = value;
    }

    /**
     * Ruft den Wert der incomingAocHandling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AdviceOfChargeIncomingAocHandling }
     *     
     */
    public AdviceOfChargeIncomingAocHandling getIncomingAocHandling() {
        return incomingAocHandling;
    }

    /**
     * Legt den Wert der incomingAocHandling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdviceOfChargeIncomingAocHandling }
     *     
     */
    public void setIncomingAocHandling(AdviceOfChargeIncomingAocHandling value) {
        this.incomingAocHandling = value;
    }

    /**
     * Ruft den Wert der useOCSEnquiry-Eigenschaft ab.
     * 
     */
    public boolean isUseOCSEnquiry() {
        return useOCSEnquiry;
    }

    /**
     * Legt den Wert der useOCSEnquiry-Eigenschaft fest.
     * 
     */
    public void setUseOCSEnquiry(boolean value) {
        this.useOCSEnquiry = value;
    }

    /**
     * Ruft den Wert der ocsEnquiryType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AdviceOfChargeOCSEnquiryType }
     *     
     */
    public AdviceOfChargeOCSEnquiryType getOCSEnquiryType() {
        return ocsEnquiryType;
    }

    /**
     * Legt den Wert der ocsEnquiryType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdviceOfChargeOCSEnquiryType }
     *     
     */
    public void setOCSEnquiryType(AdviceOfChargeOCSEnquiryType value) {
        this.ocsEnquiryType = value;
    }

}
