//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemExpensiveCallTypeGetListRequest16sp1.
 *         The column headings are:
 *         "Alternate Call Indicator", "Treatment Audio File".
 *       
 * 
 * <p>Java-Klasse für SystemExpensiveCallTypeGetListResponse16sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemExpensiveCallTypeGetListResponse16sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="alternateCallIndicatorTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemExpensiveCallTypeGetListResponse16sp1", propOrder = {
    "alternateCallIndicatorTable"
})
public class SystemExpensiveCallTypeGetListResponse16Sp1
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable alternateCallIndicatorTable;

    /**
     * Ruft den Wert der alternateCallIndicatorTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAlternateCallIndicatorTable() {
        return alternateCallIndicatorTable;
    }

    /**
     * Legt den Wert der alternateCallIndicatorTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAlternateCallIndicatorTable(OCITable value) {
        this.alternateCallIndicatorTable = value;
    }

}
