//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the GroupMusicOnHoldGetDepartmentListRequest.
 *       
 * 
 * <p>Java-Klasse für GroupMusicOnHoldGetDepartmentListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupMusicOnHoldGetDepartmentListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="hasDepartment" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="department" type="{}DepartmentKey" maxOccurs="unbounded" minOccurs="0"/>
 *         <element name="departmentFullPath" type="{}DepartmentFullPathName" maxOccurs="unbounded" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupMusicOnHoldGetDepartmentListResponse", propOrder = {
    "hasDepartment",
    "department",
    "departmentFullPath"
})
public class GroupMusicOnHoldGetDepartmentListResponse
    extends OCIDataResponse
{

    protected boolean hasDepartment;
    protected List<DepartmentKey> department;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected List<String> departmentFullPath;

    /**
     * Ruft den Wert der hasDepartment-Eigenschaft ab.
     * 
     */
    public boolean isHasDepartment() {
        return hasDepartment;
    }

    /**
     * Legt den Wert der hasDepartment-Eigenschaft fest.
     * 
     */
    public void setHasDepartment(boolean value) {
        this.hasDepartment = value;
    }

    /**
     * Gets the value of the department property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the department property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDepartment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DepartmentKey }
     * 
     * 
     * @return
     *     The value of the department property.
     */
    public List<DepartmentKey> getDepartment() {
        if (department == null) {
            department = new ArrayList<>();
        }
        return this.department;
    }

    /**
     * Gets the value of the departmentFullPath property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the departmentFullPath property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDepartmentFullPath().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     * @return
     *     The value of the departmentFullPath property.
     */
    public List<String> getDepartmentFullPath() {
        if (departmentFullPath == null) {
            departmentFullPath = new ArrayList<>();
        }
        return this.departmentFullPath;
    }

}
