//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *           Response to GroupAccessDeviceFileGetRequest.
 *           Replaced By: GroupAccessDeviceFileGetResponse14sp8
 *         
 * 
 * <p>Java-Klasse für GroupAccessDeviceFileGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAccessDeviceFileGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="fileSource" type="{}AccessDeviceEnhancedConfigurationMode"/>
 *         <element name="configurationFileName" type="{}AccessDeviceEnhancedConfigurationFileName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAccessDeviceFileGetResponse", propOrder = {
    "fileSource",
    "configurationFileName"
})
public class GroupAccessDeviceFileGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AccessDeviceEnhancedConfigurationMode fileSource;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String configurationFileName;

    /**
     * Ruft den Wert der fileSource-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDeviceEnhancedConfigurationMode }
     *     
     */
    public AccessDeviceEnhancedConfigurationMode getFileSource() {
        return fileSource;
    }

    /**
     * Legt den Wert der fileSource-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDeviceEnhancedConfigurationMode }
     *     
     */
    public void setFileSource(AccessDeviceEnhancedConfigurationMode value) {
        this.fileSource = value;
    }

    /**
     * Ruft den Wert der configurationFileName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfigurationFileName() {
        return configurationFileName;
    }

    /**
     * Legt den Wert der configurationFileName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfigurationFileName(String value) {
        this.configurationFileName = value;
    }

}
