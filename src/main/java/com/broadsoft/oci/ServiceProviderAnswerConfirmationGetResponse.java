//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to the ServiceProviderAnswerConfirmationGetRequest.
 *         Replaced By: ServiceProviderAnswerConfirmationGetResponse16            
 *       
 * 
 * <p>Java-Klasse für ServiceProviderAnswerConfirmationGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ServiceProviderAnswerConfirmationGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="announcementMessageSelection" type="{}AnswerConfirmationAnnouncementSelection"/>
 *         <element name="confirmationMessageAudioFileDescription" type="{}FileDescription" minOccurs="0"/>
 *         <element name="confirmationTimoutSeconds" type="{}AnswerConfirmationTimeoutSeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderAnswerConfirmationGetResponse", propOrder = {
    "announcementMessageSelection",
    "confirmationMessageAudioFileDescription",
    "confirmationTimoutSeconds"
})
public class ServiceProviderAnswerConfirmationGetResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected AnswerConfirmationAnnouncementSelection announcementMessageSelection;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String confirmationMessageAudioFileDescription;
    protected int confirmationTimoutSeconds;

    /**
     * Ruft den Wert der announcementMessageSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnswerConfirmationAnnouncementSelection }
     *     
     */
    public AnswerConfirmationAnnouncementSelection getAnnouncementMessageSelection() {
        return announcementMessageSelection;
    }

    /**
     * Legt den Wert der announcementMessageSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnswerConfirmationAnnouncementSelection }
     *     
     */
    public void setAnnouncementMessageSelection(AnswerConfirmationAnnouncementSelection value) {
        this.announcementMessageSelection = value;
    }

    /**
     * Ruft den Wert der confirmationMessageAudioFileDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmationMessageAudioFileDescription() {
        return confirmationMessageAudioFileDescription;
    }

    /**
     * Legt den Wert der confirmationMessageAudioFileDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmationMessageAudioFileDescription(String value) {
        this.confirmationMessageAudioFileDescription = value;
    }

    /**
     * Ruft den Wert der confirmationTimoutSeconds-Eigenschaft ab.
     * 
     */
    public int getConfirmationTimoutSeconds() {
        return confirmationTimoutSeconds;
    }

    /**
     * Legt den Wert der confirmationTimoutSeconds-Eigenschaft fest.
     * 
     */
    public void setConfirmationTimoutSeconds(int value) {
        this.confirmationTimoutSeconds = value;
    }

}
