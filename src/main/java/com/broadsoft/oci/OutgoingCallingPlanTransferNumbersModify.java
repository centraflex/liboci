//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Outgoing Calling Plan transfer numbers.
 *       
 * 
 * <p>Java-Klasse für OutgoingCallingPlanTransferNumbersModify complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OutgoingCallingPlanTransferNumbersModify">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="phoneNumber01" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="phoneNumber02" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="phoneNumber03" type="{}OutgoingDN" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingCallingPlanTransferNumbersModify", propOrder = {
    "phoneNumber01",
    "phoneNumber02",
    "phoneNumber03"
})
public class OutgoingCallingPlanTransferNumbersModify {

    @XmlElementRef(name = "phoneNumber01", type = JAXBElement.class, required = false)
    protected JAXBElement<String> phoneNumber01;
    @XmlElementRef(name = "phoneNumber02", type = JAXBElement.class, required = false)
    protected JAXBElement<String> phoneNumber02;
    @XmlElementRef(name = "phoneNumber03", type = JAXBElement.class, required = false)
    protected JAXBElement<String> phoneNumber03;

    /**
     * Ruft den Wert der phoneNumber01-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPhoneNumber01() {
        return phoneNumber01;
    }

    /**
     * Legt den Wert der phoneNumber01-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPhoneNumber01(JAXBElement<String> value) {
        this.phoneNumber01 = value;
    }

    /**
     * Ruft den Wert der phoneNumber02-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPhoneNumber02() {
        return phoneNumber02;
    }

    /**
     * Legt den Wert der phoneNumber02-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPhoneNumber02(JAXBElement<String> value) {
        this.phoneNumber02 = value;
    }

    /**
     * Ruft den Wert der phoneNumber03-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPhoneNumber03() {
        return phoneNumber03;
    }

    /**
     * Legt den Wert der phoneNumber03-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPhoneNumber03(JAXBElement<String> value) {
        this.phoneNumber03 = value;
    }

}
