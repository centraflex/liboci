//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemEnhancedCallLogsGetRequest20sp1.
 *         The following elements are only used in AS data mode:
 *           isSendEnabled
 *           server1NetAddress
 *           server1SendPort
 *           server1RetrievePort
 *           server2NetAddress
 *           server2SendPort
 *           server2RetrievePort
 *           sharedSecret
 *           retransmissionDelayMilliSeconds
 *           maxTransmissions
 *           soapTimeoutSeconds
 *           useDBS
 *           eclQueryApplicationURL
 *           eclQueryDataRepositoryURL
 *       
 * 
 * <p>Java-Klasse für SystemEnhancedCallLogsGetResponse20sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemEnhancedCallLogsGetResponse20sp1">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="isSendEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="server1NetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="server1SendPort" type="{}Port1025"/>
 *         <element name="server1RetrievePort" type="{}Port"/>
 *         <element name="server2NetAddress" type="{}NetAddress" minOccurs="0"/>
 *         <element name="server2SendPort" type="{}Port1025"/>
 *         <element name="server2RetrievePort" type="{}Port"/>
 *         <element name="sharedSecret" type="{}EnhancedCallLogsSharedSecret" minOccurs="0"/>
 *         <element name="retransmissionDelayMilliSeconds" type="{}EnhancedCallLogsRetransmissionDelayMilliSeconds"/>
 *         <element name="maxTransmissions" type="{}EnhancedCallLogsMaxTransmissions"/>
 *         <element name="soapTimeoutSeconds" type="{}EnhancedCallLogsSoapTimeoutSeconds"/>
 *         <element name="useDBS" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="maxNonPagedResponseSize" type="{}EnhancedCallLogsNonPagedResponseSize"/>
 *         <element name="eclQueryApplicationURL" type="{}URL" minOccurs="0"/>
 *         <element name="eclQueryDataRepositoryURL" type="{}URL" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemEnhancedCallLogsGetResponse20sp1", propOrder = {
    "isSendEnabled",
    "server1NetAddress",
    "server1SendPort",
    "server1RetrievePort",
    "server2NetAddress",
    "server2SendPort",
    "server2RetrievePort",
    "sharedSecret",
    "retransmissionDelayMilliSeconds",
    "maxTransmissions",
    "soapTimeoutSeconds",
    "useDBS",
    "maxNonPagedResponseSize",
    "eclQueryApplicationURL",
    "eclQueryDataRepositoryURL"
})
public class SystemEnhancedCallLogsGetResponse20Sp1
    extends OCIDataResponse
{

    protected boolean isSendEnabled;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String server1NetAddress;
    protected int server1SendPort;
    protected int server1RetrievePort;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String server2NetAddress;
    protected int server2SendPort;
    protected int server2RetrievePort;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sharedSecret;
    protected int retransmissionDelayMilliSeconds;
    protected int maxTransmissions;
    protected int soapTimeoutSeconds;
    protected boolean useDBS;
    protected int maxNonPagedResponseSize;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String eclQueryApplicationURL;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String eclQueryDataRepositoryURL;

    /**
     * Ruft den Wert der isSendEnabled-Eigenschaft ab.
     * 
     */
    public boolean isIsSendEnabled() {
        return isSendEnabled;
    }

    /**
     * Legt den Wert der isSendEnabled-Eigenschaft fest.
     * 
     */
    public void setIsSendEnabled(boolean value) {
        this.isSendEnabled = value;
    }

    /**
     * Ruft den Wert der server1NetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServer1NetAddress() {
        return server1NetAddress;
    }

    /**
     * Legt den Wert der server1NetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServer1NetAddress(String value) {
        this.server1NetAddress = value;
    }

    /**
     * Ruft den Wert der server1SendPort-Eigenschaft ab.
     * 
     */
    public int getServer1SendPort() {
        return server1SendPort;
    }

    /**
     * Legt den Wert der server1SendPort-Eigenschaft fest.
     * 
     */
    public void setServer1SendPort(int value) {
        this.server1SendPort = value;
    }

    /**
     * Ruft den Wert der server1RetrievePort-Eigenschaft ab.
     * 
     */
    public int getServer1RetrievePort() {
        return server1RetrievePort;
    }

    /**
     * Legt den Wert der server1RetrievePort-Eigenschaft fest.
     * 
     */
    public void setServer1RetrievePort(int value) {
        this.server1RetrievePort = value;
    }

    /**
     * Ruft den Wert der server2NetAddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServer2NetAddress() {
        return server2NetAddress;
    }

    /**
     * Legt den Wert der server2NetAddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServer2NetAddress(String value) {
        this.server2NetAddress = value;
    }

    /**
     * Ruft den Wert der server2SendPort-Eigenschaft ab.
     * 
     */
    public int getServer2SendPort() {
        return server2SendPort;
    }

    /**
     * Legt den Wert der server2SendPort-Eigenschaft fest.
     * 
     */
    public void setServer2SendPort(int value) {
        this.server2SendPort = value;
    }

    /**
     * Ruft den Wert der server2RetrievePort-Eigenschaft ab.
     * 
     */
    public int getServer2RetrievePort() {
        return server2RetrievePort;
    }

    /**
     * Legt den Wert der server2RetrievePort-Eigenschaft fest.
     * 
     */
    public void setServer2RetrievePort(int value) {
        this.server2RetrievePort = value;
    }

    /**
     * Ruft den Wert der sharedSecret-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSharedSecret() {
        return sharedSecret;
    }

    /**
     * Legt den Wert der sharedSecret-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSharedSecret(String value) {
        this.sharedSecret = value;
    }

    /**
     * Ruft den Wert der retransmissionDelayMilliSeconds-Eigenschaft ab.
     * 
     */
    public int getRetransmissionDelayMilliSeconds() {
        return retransmissionDelayMilliSeconds;
    }

    /**
     * Legt den Wert der retransmissionDelayMilliSeconds-Eigenschaft fest.
     * 
     */
    public void setRetransmissionDelayMilliSeconds(int value) {
        this.retransmissionDelayMilliSeconds = value;
    }

    /**
     * Ruft den Wert der maxTransmissions-Eigenschaft ab.
     * 
     */
    public int getMaxTransmissions() {
        return maxTransmissions;
    }

    /**
     * Legt den Wert der maxTransmissions-Eigenschaft fest.
     * 
     */
    public void setMaxTransmissions(int value) {
        this.maxTransmissions = value;
    }

    /**
     * Ruft den Wert der soapTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getSoapTimeoutSeconds() {
        return soapTimeoutSeconds;
    }

    /**
     * Legt den Wert der soapTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setSoapTimeoutSeconds(int value) {
        this.soapTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der useDBS-Eigenschaft ab.
     * 
     */
    public boolean isUseDBS() {
        return useDBS;
    }

    /**
     * Legt den Wert der useDBS-Eigenschaft fest.
     * 
     */
    public void setUseDBS(boolean value) {
        this.useDBS = value;
    }

    /**
     * Ruft den Wert der maxNonPagedResponseSize-Eigenschaft ab.
     * 
     */
    public int getMaxNonPagedResponseSize() {
        return maxNonPagedResponseSize;
    }

    /**
     * Legt den Wert der maxNonPagedResponseSize-Eigenschaft fest.
     * 
     */
    public void setMaxNonPagedResponseSize(int value) {
        this.maxNonPagedResponseSize = value;
    }

    /**
     * Ruft den Wert der eclQueryApplicationURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEclQueryApplicationURL() {
        return eclQueryApplicationURL;
    }

    /**
     * Legt den Wert der eclQueryApplicationURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEclQueryApplicationURL(String value) {
        this.eclQueryApplicationURL = value;
    }

    /**
     * Ruft den Wert der eclQueryDataRepositoryURL-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEclQueryDataRepositoryURL() {
        return eclQueryDataRepositoryURL;
    }

    /**
     * Legt den Wert der eclQueryDataRepositoryURL-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEclQueryDataRepositoryURL(String value) {
        this.eclQueryDataRepositoryURL = value;
    }

}
