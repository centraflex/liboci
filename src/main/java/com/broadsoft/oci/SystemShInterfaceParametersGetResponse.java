//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemShInterfaceParametersGetRequest.  Contains the Sh Interface system parameters.
 *         
 *         Replaced by: SystemShInterfaceParametersGetResponse17
 *       
 * 
 * <p>Java-Klasse für SystemShInterfaceParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemShInterfaceParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="hssRealm" type="{}DomainName" minOccurs="0"/>
 *         <element name="requestTimeoutSeconds" type="{}ShInterfaceRequestTimeoutSeconds"/>
 *         <element name="publicIdentityRefreshDelaySeconds" type="{}ShInterfacePublicIdentityRefreshDelaySeconds"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemShInterfaceParametersGetResponse", propOrder = {
    "hssRealm",
    "requestTimeoutSeconds",
    "publicIdentityRefreshDelaySeconds"
})
public class SystemShInterfaceParametersGetResponse
    extends OCIDataResponse
{

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String hssRealm;
    protected int requestTimeoutSeconds;
    protected int publicIdentityRefreshDelaySeconds;

    /**
     * Ruft den Wert der hssRealm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHssRealm() {
        return hssRealm;
    }

    /**
     * Legt den Wert der hssRealm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHssRealm(String value) {
        this.hssRealm = value;
    }

    /**
     * Ruft den Wert der requestTimeoutSeconds-Eigenschaft ab.
     * 
     */
    public int getRequestTimeoutSeconds() {
        return requestTimeoutSeconds;
    }

    /**
     * Legt den Wert der requestTimeoutSeconds-Eigenschaft fest.
     * 
     */
    public void setRequestTimeoutSeconds(int value) {
        this.requestTimeoutSeconds = value;
    }

    /**
     * Ruft den Wert der publicIdentityRefreshDelaySeconds-Eigenschaft ab.
     * 
     */
    public int getPublicIdentityRefreshDelaySeconds() {
        return publicIdentityRefreshDelaySeconds;
    }

    /**
     * Legt den Wert der publicIdentityRefreshDelaySeconds-Eigenschaft fest.
     * 
     */
    public void setPublicIdentityRefreshDelaySeconds(int value) {
        this.publicIdentityRefreshDelaySeconds = value;
    }

}
