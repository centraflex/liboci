//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify the group's intercept group service settings.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *         
 *         The following elements are only used in AS data mode and ignored in XS data mode:
 *           exemptInboundMobilityCalls
 *           exemptOutboundMobilityCalls
 *           disableParallelRingingToNetworkLocations
 *       
 * 
 * <p>Java-Klasse für GroupInterceptGroupModifyRequest21sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupInterceptGroupModifyRequest21sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="serviceProviderId" type="{}ServiceProviderId"/>
 *         <element name="groupId" type="{}GroupId"/>
 *         <element name="isActive" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="announcementSelection" type="{}AnnouncementSelection" minOccurs="0"/>
 *         <element name="audioFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *         <element name="videoFile" type="{}LabeledMediaFileResource" minOccurs="0"/>
 *         <element name="inboundCallMode" type="{}InterceptInboundCall" minOccurs="0"/>
 *         <element name="alternateBlockingAnnouncement" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="exemptInboundMobilityCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="disableParallelRingingToNetworkLocations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="routeToVoiceMail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="playNewPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="newPhoneNumber" type="{}DN" minOccurs="0"/>
 *         <element name="transferOnZeroToPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="transferPhoneNumber" type="{}OutgoingDN" minOccurs="0"/>
 *         <element name="outboundCallMode" type="{}InterceptOutboundCall" minOccurs="0"/>
 *         <element name="exemptOutboundMobilityCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="rerouteOutboundCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="outboundReroutePhoneNumber" type="{}OutgoingDNorSIPURI" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupInterceptGroupModifyRequest21sp1", propOrder = {
    "serviceProviderId",
    "groupId",
    "isActive",
    "announcementSelection",
    "audioFile",
    "videoFile",
    "inboundCallMode",
    "alternateBlockingAnnouncement",
    "exemptInboundMobilityCalls",
    "disableParallelRingingToNetworkLocations",
    "routeToVoiceMail",
    "playNewPhoneNumber",
    "newPhoneNumber",
    "transferOnZeroToPhoneNumber",
    "transferPhoneNumber",
    "outboundCallMode",
    "exemptOutboundMobilityCalls",
    "rerouteOutboundCalls",
    "outboundReroutePhoneNumber"
})
public class GroupInterceptGroupModifyRequest21Sp1
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String serviceProviderId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupId;
    protected Boolean isActive;
    @XmlSchemaType(name = "token")
    protected AnnouncementSelection announcementSelection;
    protected LabeledMediaFileResource audioFile;
    protected LabeledMediaFileResource videoFile;
    @XmlSchemaType(name = "token")
    protected InterceptInboundCall inboundCallMode;
    protected Boolean alternateBlockingAnnouncement;
    protected Boolean exemptInboundMobilityCalls;
    protected Boolean disableParallelRingingToNetworkLocations;
    protected Boolean routeToVoiceMail;
    protected Boolean playNewPhoneNumber;
    @XmlElementRef(name = "newPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> newPhoneNumber;
    protected Boolean transferOnZeroToPhoneNumber;
    @XmlElementRef(name = "transferPhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transferPhoneNumber;
    @XmlSchemaType(name = "token")
    protected InterceptOutboundCall outboundCallMode;
    protected Boolean exemptOutboundMobilityCalls;
    protected Boolean rerouteOutboundCalls;
    @XmlElementRef(name = "outboundReroutePhoneNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> outboundReroutePhoneNumber;

    /**
     * Ruft den Wert der serviceProviderId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderId() {
        return serviceProviderId;
    }

    /**
     * Legt den Wert der serviceProviderId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderId(String value) {
        this.serviceProviderId = value;
    }

    /**
     * Ruft den Wert der groupId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Legt den Wert der groupId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Ruft den Wert der isActive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsActive() {
        return isActive;
    }

    /**
     * Legt den Wert der isActive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsActive(Boolean value) {
        this.isActive = value;
    }

    /**
     * Ruft den Wert der announcementSelection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AnnouncementSelection }
     *     
     */
    public AnnouncementSelection getAnnouncementSelection() {
        return announcementSelection;
    }

    /**
     * Legt den Wert der announcementSelection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnouncementSelection }
     *     
     */
    public void setAnnouncementSelection(AnnouncementSelection value) {
        this.announcementSelection = value;
    }

    /**
     * Ruft den Wert der audioFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public LabeledMediaFileResource getAudioFile() {
        return audioFile;
    }

    /**
     * Legt den Wert der audioFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public void setAudioFile(LabeledMediaFileResource value) {
        this.audioFile = value;
    }

    /**
     * Ruft den Wert der videoFile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public LabeledMediaFileResource getVideoFile() {
        return videoFile;
    }

    /**
     * Legt den Wert der videoFile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LabeledMediaFileResource }
     *     
     */
    public void setVideoFile(LabeledMediaFileResource value) {
        this.videoFile = value;
    }

    /**
     * Ruft den Wert der inboundCallMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InterceptInboundCall }
     *     
     */
    public InterceptInboundCall getInboundCallMode() {
        return inboundCallMode;
    }

    /**
     * Legt den Wert der inboundCallMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InterceptInboundCall }
     *     
     */
    public void setInboundCallMode(InterceptInboundCall value) {
        this.inboundCallMode = value;
    }

    /**
     * Ruft den Wert der alternateBlockingAnnouncement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlternateBlockingAnnouncement() {
        return alternateBlockingAnnouncement;
    }

    /**
     * Legt den Wert der alternateBlockingAnnouncement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlternateBlockingAnnouncement(Boolean value) {
        this.alternateBlockingAnnouncement = value;
    }

    /**
     * Ruft den Wert der exemptInboundMobilityCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExemptInboundMobilityCalls() {
        return exemptInboundMobilityCalls;
    }

    /**
     * Legt den Wert der exemptInboundMobilityCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExemptInboundMobilityCalls(Boolean value) {
        this.exemptInboundMobilityCalls = value;
    }

    /**
     * Ruft den Wert der disableParallelRingingToNetworkLocations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisableParallelRingingToNetworkLocations() {
        return disableParallelRingingToNetworkLocations;
    }

    /**
     * Legt den Wert der disableParallelRingingToNetworkLocations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisableParallelRingingToNetworkLocations(Boolean value) {
        this.disableParallelRingingToNetworkLocations = value;
    }

    /**
     * Ruft den Wert der routeToVoiceMail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRouteToVoiceMail() {
        return routeToVoiceMail;
    }

    /**
     * Legt den Wert der routeToVoiceMail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRouteToVoiceMail(Boolean value) {
        this.routeToVoiceMail = value;
    }

    /**
     * Ruft den Wert der playNewPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPlayNewPhoneNumber() {
        return playNewPhoneNumber;
    }

    /**
     * Legt den Wert der playNewPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPlayNewPhoneNumber(Boolean value) {
        this.playNewPhoneNumber = value;
    }

    /**
     * Ruft den Wert der newPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNewPhoneNumber() {
        return newPhoneNumber;
    }

    /**
     * Legt den Wert der newPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNewPhoneNumber(JAXBElement<String> value) {
        this.newPhoneNumber = value;
    }

    /**
     * Ruft den Wert der transferOnZeroToPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTransferOnZeroToPhoneNumber() {
        return transferOnZeroToPhoneNumber;
    }

    /**
     * Legt den Wert der transferOnZeroToPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTransferOnZeroToPhoneNumber(Boolean value) {
        this.transferOnZeroToPhoneNumber = value;
    }

    /**
     * Ruft den Wert der transferPhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransferPhoneNumber() {
        return transferPhoneNumber;
    }

    /**
     * Legt den Wert der transferPhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransferPhoneNumber(JAXBElement<String> value) {
        this.transferPhoneNumber = value;
    }

    /**
     * Ruft den Wert der outboundCallMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InterceptOutboundCall }
     *     
     */
    public InterceptOutboundCall getOutboundCallMode() {
        return outboundCallMode;
    }

    /**
     * Legt den Wert der outboundCallMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InterceptOutboundCall }
     *     
     */
    public void setOutboundCallMode(InterceptOutboundCall value) {
        this.outboundCallMode = value;
    }

    /**
     * Ruft den Wert der exemptOutboundMobilityCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExemptOutboundMobilityCalls() {
        return exemptOutboundMobilityCalls;
    }

    /**
     * Legt den Wert der exemptOutboundMobilityCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExemptOutboundMobilityCalls(Boolean value) {
        this.exemptOutboundMobilityCalls = value;
    }

    /**
     * Ruft den Wert der rerouteOutboundCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRerouteOutboundCalls() {
        return rerouteOutboundCalls;
    }

    /**
     * Legt den Wert der rerouteOutboundCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRerouteOutboundCalls(Boolean value) {
        this.rerouteOutboundCalls = value;
    }

    /**
     * Ruft den Wert der outboundReroutePhoneNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOutboundReroutePhoneNumber() {
        return outboundReroutePhoneNumber;
    }

    /**
     * Legt den Wert der outboundReroutePhoneNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOutboundReroutePhoneNumber(JAXBElement<String> value) {
        this.outboundReroutePhoneNumber = value;
    }

}
