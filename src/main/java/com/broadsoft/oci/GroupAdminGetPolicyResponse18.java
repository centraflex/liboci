//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to GroupAdminGetPolicyRequest18.
 *         Contains the policy settings for the group administrator.
 *         The following elements are only used in AS data mode:
 *             dialableCallerIDAccess
 *       
 * 
 * <p>Java-Klasse für GroupAdminGetPolicyResponse18 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAdminGetPolicyResponse18">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="profileAccess" type="{}GroupAdminProfileAccess"/>
 *         <element name="userAccess" type="{}GroupAdminUserAccess"/>
 *         <element name="adminAccess" type="{}GroupAdminAdminAccess"/>
 *         <element name="departmentAccess" type="{}GroupAdminDepartmentAccess"/>
 *         <element name="accessDeviceAccess" type="{}GroupAdminAccessDeviceAccess"/>
 *         <element name="enhancedServiceInstanceAccess" type="{}GroupAdminEnhancedServiceInstanceAccess"/>
 *         <element name="featureAccessCodeAccess" type="{}GroupAdminFeatureAccessCodeAccess"/>
 *         <element name="phoneNumberExtensionAccess" type="{}GroupAdminPhoneNumberExtensionAccess"/>
 *         <element name="callingLineIdNumberAccess" type="{}GroupAdminCallingLineIdNumberAccess"/>
 *         <element name="serviceAccess" type="{}GroupAdminServiceAccess"/>
 *         <element name="trunkGroupAccess" type="{}GroupAdminTrunkGroupAccess"/>
 *         <element name="sessionAdmissionControlAccess" type="{}GroupAdminSessionAdmissionControlAccess"/>
 *         <element name="officeZoneAccess" type="{}GroupAdminOfficeZoneAccess"/>
 *         <element name="numberActivationAccess" type="{}GroupAdminNumberActivationAccess"/>
 *         <element name="dialableCallerIDAccess" type="{}GroupAdminDialableCallerIDAccess"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAdminGetPolicyResponse18", propOrder = {
    "profileAccess",
    "userAccess",
    "adminAccess",
    "departmentAccess",
    "accessDeviceAccess",
    "enhancedServiceInstanceAccess",
    "featureAccessCodeAccess",
    "phoneNumberExtensionAccess",
    "callingLineIdNumberAccess",
    "serviceAccess",
    "trunkGroupAccess",
    "sessionAdmissionControlAccess",
    "officeZoneAccess",
    "numberActivationAccess",
    "dialableCallerIDAccess"
})
public class GroupAdminGetPolicyResponse18
    extends OCIDataResponse
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminProfileAccess profileAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminUserAccess userAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminAdminAccess adminAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminDepartmentAccess departmentAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminAccessDeviceAccess accessDeviceAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminEnhancedServiceInstanceAccess enhancedServiceInstanceAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminFeatureAccessCodeAccess featureAccessCodeAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminPhoneNumberExtensionAccess phoneNumberExtensionAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminCallingLineIdNumberAccess callingLineIdNumberAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminServiceAccess serviceAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminTrunkGroupAccess trunkGroupAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminSessionAdmissionControlAccess sessionAdmissionControlAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminOfficeZoneAccess officeZoneAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminNumberActivationAccess numberActivationAccess;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected GroupAdminDialableCallerIDAccess dialableCallerIDAccess;

    /**
     * Ruft den Wert der profileAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminProfileAccess }
     *     
     */
    public GroupAdminProfileAccess getProfileAccess() {
        return profileAccess;
    }

    /**
     * Legt den Wert der profileAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminProfileAccess }
     *     
     */
    public void setProfileAccess(GroupAdminProfileAccess value) {
        this.profileAccess = value;
    }

    /**
     * Ruft den Wert der userAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminUserAccess }
     *     
     */
    public GroupAdminUserAccess getUserAccess() {
        return userAccess;
    }

    /**
     * Legt den Wert der userAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminUserAccess }
     *     
     */
    public void setUserAccess(GroupAdminUserAccess value) {
        this.userAccess = value;
    }

    /**
     * Ruft den Wert der adminAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminAdminAccess }
     *     
     */
    public GroupAdminAdminAccess getAdminAccess() {
        return adminAccess;
    }

    /**
     * Legt den Wert der adminAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminAdminAccess }
     *     
     */
    public void setAdminAccess(GroupAdminAdminAccess value) {
        this.adminAccess = value;
    }

    /**
     * Ruft den Wert der departmentAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminDepartmentAccess }
     *     
     */
    public GroupAdminDepartmentAccess getDepartmentAccess() {
        return departmentAccess;
    }

    /**
     * Legt den Wert der departmentAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminDepartmentAccess }
     *     
     */
    public void setDepartmentAccess(GroupAdminDepartmentAccess value) {
        this.departmentAccess = value;
    }

    /**
     * Ruft den Wert der accessDeviceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminAccessDeviceAccess }
     *     
     */
    public GroupAdminAccessDeviceAccess getAccessDeviceAccess() {
        return accessDeviceAccess;
    }

    /**
     * Legt den Wert der accessDeviceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminAccessDeviceAccess }
     *     
     */
    public void setAccessDeviceAccess(GroupAdminAccessDeviceAccess value) {
        this.accessDeviceAccess = value;
    }

    /**
     * Ruft den Wert der enhancedServiceInstanceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminEnhancedServiceInstanceAccess }
     *     
     */
    public GroupAdminEnhancedServiceInstanceAccess getEnhancedServiceInstanceAccess() {
        return enhancedServiceInstanceAccess;
    }

    /**
     * Legt den Wert der enhancedServiceInstanceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminEnhancedServiceInstanceAccess }
     *     
     */
    public void setEnhancedServiceInstanceAccess(GroupAdminEnhancedServiceInstanceAccess value) {
        this.enhancedServiceInstanceAccess = value;
    }

    /**
     * Ruft den Wert der featureAccessCodeAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminFeatureAccessCodeAccess }
     *     
     */
    public GroupAdminFeatureAccessCodeAccess getFeatureAccessCodeAccess() {
        return featureAccessCodeAccess;
    }

    /**
     * Legt den Wert der featureAccessCodeAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminFeatureAccessCodeAccess }
     *     
     */
    public void setFeatureAccessCodeAccess(GroupAdminFeatureAccessCodeAccess value) {
        this.featureAccessCodeAccess = value;
    }

    /**
     * Ruft den Wert der phoneNumberExtensionAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminPhoneNumberExtensionAccess }
     *     
     */
    public GroupAdminPhoneNumberExtensionAccess getPhoneNumberExtensionAccess() {
        return phoneNumberExtensionAccess;
    }

    /**
     * Legt den Wert der phoneNumberExtensionAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminPhoneNumberExtensionAccess }
     *     
     */
    public void setPhoneNumberExtensionAccess(GroupAdminPhoneNumberExtensionAccess value) {
        this.phoneNumberExtensionAccess = value;
    }

    /**
     * Ruft den Wert der callingLineIdNumberAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminCallingLineIdNumberAccess }
     *     
     */
    public GroupAdminCallingLineIdNumberAccess getCallingLineIdNumberAccess() {
        return callingLineIdNumberAccess;
    }

    /**
     * Legt den Wert der callingLineIdNumberAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminCallingLineIdNumberAccess }
     *     
     */
    public void setCallingLineIdNumberAccess(GroupAdminCallingLineIdNumberAccess value) {
        this.callingLineIdNumberAccess = value;
    }

    /**
     * Ruft den Wert der serviceAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminServiceAccess }
     *     
     */
    public GroupAdminServiceAccess getServiceAccess() {
        return serviceAccess;
    }

    /**
     * Legt den Wert der serviceAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminServiceAccess }
     *     
     */
    public void setServiceAccess(GroupAdminServiceAccess value) {
        this.serviceAccess = value;
    }

    /**
     * Ruft den Wert der trunkGroupAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminTrunkGroupAccess }
     *     
     */
    public GroupAdminTrunkGroupAccess getTrunkGroupAccess() {
        return trunkGroupAccess;
    }

    /**
     * Legt den Wert der trunkGroupAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminTrunkGroupAccess }
     *     
     */
    public void setTrunkGroupAccess(GroupAdminTrunkGroupAccess value) {
        this.trunkGroupAccess = value;
    }

    /**
     * Ruft den Wert der sessionAdmissionControlAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminSessionAdmissionControlAccess }
     *     
     */
    public GroupAdminSessionAdmissionControlAccess getSessionAdmissionControlAccess() {
        return sessionAdmissionControlAccess;
    }

    /**
     * Legt den Wert der sessionAdmissionControlAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminSessionAdmissionControlAccess }
     *     
     */
    public void setSessionAdmissionControlAccess(GroupAdminSessionAdmissionControlAccess value) {
        this.sessionAdmissionControlAccess = value;
    }

    /**
     * Ruft den Wert der officeZoneAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminOfficeZoneAccess }
     *     
     */
    public GroupAdminOfficeZoneAccess getOfficeZoneAccess() {
        return officeZoneAccess;
    }

    /**
     * Legt den Wert der officeZoneAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminOfficeZoneAccess }
     *     
     */
    public void setOfficeZoneAccess(GroupAdminOfficeZoneAccess value) {
        this.officeZoneAccess = value;
    }

    /**
     * Ruft den Wert der numberActivationAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminNumberActivationAccess }
     *     
     */
    public GroupAdminNumberActivationAccess getNumberActivationAccess() {
        return numberActivationAccess;
    }

    /**
     * Legt den Wert der numberActivationAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminNumberActivationAccess }
     *     
     */
    public void setNumberActivationAccess(GroupAdminNumberActivationAccess value) {
        this.numberActivationAccess = value;
    }

    /**
     * Ruft den Wert der dialableCallerIDAccess-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GroupAdminDialableCallerIDAccess }
     *     
     */
    public GroupAdminDialableCallerIDAccess getDialableCallerIDAccess() {
        return dialableCallerIDAccess;
    }

    /**
     * Legt den Wert der dialableCallerIDAccess-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupAdminDialableCallerIDAccess }
     *     
     */
    public void setDialableCallerIDAccess(GroupAdminDialableCallerIDAccess value) {
        this.dialableCallerIDAccess = value;
    }

}
