//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal main menu keys.
 *       
 * 
 * <p>Java-Klasse für VoicePortalMainMenuKeysReadEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="VoicePortalMainMenuKeysReadEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="voiceMessaging" type="{}DigitAny" minOccurs="0"/>
 *         <element name="commPilotExpressProfile" type="{}DigitAny" minOccurs="0"/>
 *         <element name="greetings" type="{}DigitAny" minOccurs="0"/>
 *         <element name="callForwardingOptions" type="{}DigitAny" minOccurs="0"/>
 *         <element name="voicePortalCalling" type="{}DigitAny" minOccurs="0"/>
 *         <element name="hoteling" type="{}DigitAny" minOccurs="0"/>
 *         <element name="passcode" type="{}DigitAny" minOccurs="0"/>
 *         <element name="exitVoicePortal" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="externalRouting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="announcement" type="{}DigitAny" minOccurs="0"/>
 *         <element name="personalAssistant" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoicePortalMainMenuKeysReadEntry", propOrder = {
    "voiceMessaging",
    "commPilotExpressProfile",
    "greetings",
    "callForwardingOptions",
    "voicePortalCalling",
    "hoteling",
    "passcode",
    "exitVoicePortal",
    "repeatMenu",
    "externalRouting",
    "announcement",
    "personalAssistant"
})
public class VoicePortalMainMenuKeysReadEntry {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String voiceMessaging;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String commPilotExpressProfile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String greetings;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callForwardingOptions;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String voicePortalCalling;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String hoteling;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String passcode;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String exitVoicePortal;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String repeatMenu;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String externalRouting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String announcement;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String personalAssistant;

    /**
     * Ruft den Wert der voiceMessaging-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoiceMessaging() {
        return voiceMessaging;
    }

    /**
     * Legt den Wert der voiceMessaging-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoiceMessaging(String value) {
        this.voiceMessaging = value;
    }

    /**
     * Ruft den Wert der commPilotExpressProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommPilotExpressProfile() {
        return commPilotExpressProfile;
    }

    /**
     * Legt den Wert der commPilotExpressProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommPilotExpressProfile(String value) {
        this.commPilotExpressProfile = value;
    }

    /**
     * Ruft den Wert der greetings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGreetings() {
        return greetings;
    }

    /**
     * Legt den Wert der greetings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGreetings(String value) {
        this.greetings = value;
    }

    /**
     * Ruft den Wert der callForwardingOptions-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallForwardingOptions() {
        return callForwardingOptions;
    }

    /**
     * Legt den Wert der callForwardingOptions-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallForwardingOptions(String value) {
        this.callForwardingOptions = value;
    }

    /**
     * Ruft den Wert der voicePortalCalling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoicePortalCalling() {
        return voicePortalCalling;
    }

    /**
     * Legt den Wert der voicePortalCalling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoicePortalCalling(String value) {
        this.voicePortalCalling = value;
    }

    /**
     * Ruft den Wert der hoteling-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoteling() {
        return hoteling;
    }

    /**
     * Legt den Wert der hoteling-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoteling(String value) {
        this.hoteling = value;
    }

    /**
     * Ruft den Wert der passcode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasscode() {
        return passcode;
    }

    /**
     * Legt den Wert der passcode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasscode(String value) {
        this.passcode = value;
    }

    /**
     * Ruft den Wert der exitVoicePortal-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExitVoicePortal() {
        return exitVoicePortal;
    }

    /**
     * Legt den Wert der exitVoicePortal-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExitVoicePortal(String value) {
        this.exitVoicePortal = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepeatMenu(String value) {
        this.repeatMenu = value;
    }

    /**
     * Ruft den Wert der externalRouting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalRouting() {
        return externalRouting;
    }

    /**
     * Legt den Wert der externalRouting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalRouting(String value) {
        this.externalRouting = value;
    }

    /**
     * Ruft den Wert der announcement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnouncement() {
        return announcement;
    }

    /**
     * Legt den Wert der announcement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnouncement(String value) {
        this.announcement = value;
    }

    /**
     * Ruft den Wert der personalAssistant-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalAssistant() {
        return personalAssistant;
    }

    /**
     * Legt den Wert der personalAssistant-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalAssistant(String value) {
        this.personalAssistant = value;
    }

}
