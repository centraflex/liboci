//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemVirtualOnNetCallTypeGetListRequest.
 *         Contains a table with column headings: 
 *         "Virtual On-Net Call Type Name", "Virtual On-Net Call Type CDR Value" 
 *         in a row for each Virtual On-Net Call Type.
 *       
 * 
 * <p>Java-Klasse für SystemVirtualOnNetCallTypeGetListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemVirtualOnNetCallTypeGetListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="virtualOnNetCallTypeTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemVirtualOnNetCallTypeGetListResponse", propOrder = {
    "virtualOnNetCallTypeTable"
})
public class SystemVirtualOnNetCallTypeGetListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable virtualOnNetCallTypeTable;

    /**
     * Ruft den Wert der virtualOnNetCallTypeTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getVirtualOnNetCallTypeTable() {
        return virtualOnNetCallTypeTable;
    }

    /**
     * Legt den Wert der virtualOnNetCallTypeTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setVirtualOnNetCallTypeTable(OCITable value) {
        this.virtualOnNetCallTypeTable = value;
    }

}
