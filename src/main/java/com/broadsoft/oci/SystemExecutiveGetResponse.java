//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemExecutiveGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemExecutiveGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemExecutiveGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="treatVirtualOnNetCallsAsInternal" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemExecutiveGetResponse", propOrder = {
    "treatVirtualOnNetCallsAsInternal"
})
public class SystemExecutiveGetResponse
    extends OCIDataResponse
{

    protected boolean treatVirtualOnNetCallsAsInternal;

    /**
     * Ruft den Wert der treatVirtualOnNetCallsAsInternal-Eigenschaft ab.
     * 
     */
    public boolean isTreatVirtualOnNetCallsAsInternal() {
        return treatVirtualOnNetCallsAsInternal;
    }

    /**
     * Legt den Wert der treatVirtualOnNetCallsAsInternal-Eigenschaft fest.
     * 
     */
    public void setTreatVirtualOnNetCallsAsInternal(boolean value) {
        this.treatVirtualOnNetCallsAsInternal = value;
    }

}
