//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CallCenterReportWebStatisticsSource.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="CallCenterReportWebStatisticsSource">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="CCRS"/>
 *     <enumeration value="Enhanced"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "CallCenterReportWebStatisticsSource")
@XmlEnum
public enum CallCenterReportWebStatisticsSource {

    CCRS("CCRS"),
    @XmlEnumValue("Enhanced")
    ENHANCED("Enhanced");
    private final String value;

    CallCenterReportWebStatisticsSource(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CallCenterReportWebStatisticsSource fromValue(String v) {
        for (CallCenterReportWebStatisticsSource c: CallCenterReportWebStatisticsSource.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
