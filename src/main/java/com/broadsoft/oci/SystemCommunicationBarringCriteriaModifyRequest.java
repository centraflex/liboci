//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modify an existing Communication Barring Criteria.
 *         The following elements are only used in AS data mode:
 *           matchNumberPortabilityStatus  
 * 	    The following elements are only used in XS data mode and ignored in AS data mode:
 * 	      matchOtherGETS
 *           matchNotGETS
 *           matchGETSAN
 *           matchGETSNT
 *           matchGETSFC
 *           matchGETSFCAN
 *           matchGETSFCNT
 * 
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für SystemCommunicationBarringCriteriaModifyRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCommunicationBarringCriteriaModifyRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="name" type="{}CommunicationBarringCriteriaName"/>
 *         <element name="newName" type="{}CommunicationBarringCriteriaName" minOccurs="0"/>
 *         <element name="description" type="{}CommunicationBarringCriteriaDescription" minOccurs="0"/>
 *         <element name="matchCallType" type="{}ReplacementCommunicationBarringCallTypeList" minOccurs="0"/>
 *         <element name="matchAlternateCallIndicator" type="{}ReplacementCommunicationBarringAlternateCallIndicatorList" minOccurs="0"/>
 *         <element name="matchVirtualOnNetCallType" type="{}ReplacementVirtualOnNetCallTypeNameList" minOccurs="0"/>
 *         <element name="matchPublicNetwork" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchPrivateNetwork" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchLocalCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchNationalCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchInterlataCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchIntralataCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchInternationalCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchPrivateCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchEmergencyCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchOtherCategory" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchInterNetwork" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchInterHostingNE" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchInterAS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchIntraAS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchChargeCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchNoChargeCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchGroupCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchEnterpriseCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchNetworkCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchNetworkURLCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchRepairCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchEmergencyCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchInternalCalls" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchOtherGETSGets" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchNotGETSGets" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchGETSANGets" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchGETSNTGets" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchGETSFCGets" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchGETSFCANGets" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchGETSFCNTGets" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="matchLocation" type="{}LocationCriteria" minOccurs="0"/>
 *         <element name="matchRoaming" type="{}RoamingCriteria" minOccurs="0"/>
 *         <element name="timeSchedule" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="holidaySchedule" type="{}ScheduleName" minOccurs="0"/>
 *         <element name="matchNumberPortabilityStatus" type="{}ReplacementNumberPortabilityStatusList" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCommunicationBarringCriteriaModifyRequest", propOrder = {
    "name",
    "newName",
    "description",
    "matchCallType",
    "matchAlternateCallIndicator",
    "matchVirtualOnNetCallType",
    "matchPublicNetwork",
    "matchPrivateNetwork",
    "matchLocalCategory",
    "matchNationalCategory",
    "matchInterlataCategory",
    "matchIntralataCategory",
    "matchInternationalCategory",
    "matchPrivateCategory",
    "matchEmergencyCategory",
    "matchOtherCategory",
    "matchInterNetwork",
    "matchInterHostingNE",
    "matchInterAS",
    "matchIntraAS",
    "matchChargeCalls",
    "matchNoChargeCalls",
    "matchGroupCalls",
    "matchEnterpriseCalls",
    "matchNetworkCalls",
    "matchNetworkURLCalls",
    "matchRepairCalls",
    "matchEmergencyCalls",
    "matchInternalCalls",
    "matchOtherGETSGets",
    "matchNotGETSGets",
    "matchGETSANGets",
    "matchGETSNTGets",
    "matchGETSFCGets",
    "matchGETSFCANGets",
    "matchGETSFCNTGets",
    "matchLocation",
    "matchRoaming",
    "timeSchedule",
    "holidaySchedule",
    "matchNumberPortabilityStatus"
})
public class SystemCommunicationBarringCriteriaModifyRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String name;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String newName;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "matchCallType", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementCommunicationBarringCallTypeList> matchCallType;
    @XmlElementRef(name = "matchAlternateCallIndicator", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementCommunicationBarringAlternateCallIndicatorList> matchAlternateCallIndicator;
    @XmlElementRef(name = "matchVirtualOnNetCallType", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementVirtualOnNetCallTypeNameList> matchVirtualOnNetCallType;
    protected Boolean matchPublicNetwork;
    protected Boolean matchPrivateNetwork;
    protected Boolean matchLocalCategory;
    protected Boolean matchNationalCategory;
    protected Boolean matchInterlataCategory;
    protected Boolean matchIntralataCategory;
    protected Boolean matchInternationalCategory;
    protected Boolean matchPrivateCategory;
    protected Boolean matchEmergencyCategory;
    protected Boolean matchOtherCategory;
    protected Boolean matchInterNetwork;
    protected Boolean matchInterHostingNE;
    protected Boolean matchInterAS;
    protected Boolean matchIntraAS;
    protected Boolean matchChargeCalls;
    protected Boolean matchNoChargeCalls;
    protected Boolean matchGroupCalls;
    protected Boolean matchEnterpriseCalls;
    protected Boolean matchNetworkCalls;
    protected Boolean matchNetworkURLCalls;
    protected Boolean matchRepairCalls;
    protected Boolean matchEmergencyCalls;
    protected Boolean matchInternalCalls;
    protected Boolean matchOtherGETSGets;
    protected Boolean matchNotGETSGets;
    protected Boolean matchGETSANGets;
    protected Boolean matchGETSNTGets;
    protected Boolean matchGETSFCGets;
    protected Boolean matchGETSFCANGets;
    protected Boolean matchGETSFCNTGets;
    @XmlSchemaType(name = "token")
    protected LocationCriteria matchLocation;
    @XmlSchemaType(name = "token")
    protected RoamingCriteria matchRoaming;
    @XmlElementRef(name = "timeSchedule", type = JAXBElement.class, required = false)
    protected JAXBElement<String> timeSchedule;
    @XmlElementRef(name = "holidaySchedule", type = JAXBElement.class, required = false)
    protected JAXBElement<String> holidaySchedule;
    @XmlElementRef(name = "matchNumberPortabilityStatus", type = JAXBElement.class, required = false)
    protected JAXBElement<ReplacementNumberPortabilityStatusList> matchNumberPortabilityStatus;

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der newName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewName() {
        return newName;
    }

    /**
     * Legt den Wert der newName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewName(String value) {
        this.newName = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der matchCallType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringCallTypeList }{@code >}
     *     
     */
    public JAXBElement<ReplacementCommunicationBarringCallTypeList> getMatchCallType() {
        return matchCallType;
    }

    /**
     * Legt den Wert der matchCallType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringCallTypeList }{@code >}
     *     
     */
    public void setMatchCallType(JAXBElement<ReplacementCommunicationBarringCallTypeList> value) {
        this.matchCallType = value;
    }

    /**
     * Ruft den Wert der matchAlternateCallIndicator-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringAlternateCallIndicatorList }{@code >}
     *     
     */
    public JAXBElement<ReplacementCommunicationBarringAlternateCallIndicatorList> getMatchAlternateCallIndicator() {
        return matchAlternateCallIndicator;
    }

    /**
     * Legt den Wert der matchAlternateCallIndicator-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementCommunicationBarringAlternateCallIndicatorList }{@code >}
     *     
     */
    public void setMatchAlternateCallIndicator(JAXBElement<ReplacementCommunicationBarringAlternateCallIndicatorList> value) {
        this.matchAlternateCallIndicator = value;
    }

    /**
     * Ruft den Wert der matchVirtualOnNetCallType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementVirtualOnNetCallTypeNameList }{@code >}
     *     
     */
    public JAXBElement<ReplacementVirtualOnNetCallTypeNameList> getMatchVirtualOnNetCallType() {
        return matchVirtualOnNetCallType;
    }

    /**
     * Legt den Wert der matchVirtualOnNetCallType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementVirtualOnNetCallTypeNameList }{@code >}
     *     
     */
    public void setMatchVirtualOnNetCallType(JAXBElement<ReplacementVirtualOnNetCallTypeNameList> value) {
        this.matchVirtualOnNetCallType = value;
    }

    /**
     * Ruft den Wert der matchPublicNetwork-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchPublicNetwork() {
        return matchPublicNetwork;
    }

    /**
     * Legt den Wert der matchPublicNetwork-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchPublicNetwork(Boolean value) {
        this.matchPublicNetwork = value;
    }

    /**
     * Ruft den Wert der matchPrivateNetwork-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchPrivateNetwork() {
        return matchPrivateNetwork;
    }

    /**
     * Legt den Wert der matchPrivateNetwork-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchPrivateNetwork(Boolean value) {
        this.matchPrivateNetwork = value;
    }

    /**
     * Ruft den Wert der matchLocalCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchLocalCategory() {
        return matchLocalCategory;
    }

    /**
     * Legt den Wert der matchLocalCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchLocalCategory(Boolean value) {
        this.matchLocalCategory = value;
    }

    /**
     * Ruft den Wert der matchNationalCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchNationalCategory() {
        return matchNationalCategory;
    }

    /**
     * Legt den Wert der matchNationalCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchNationalCategory(Boolean value) {
        this.matchNationalCategory = value;
    }

    /**
     * Ruft den Wert der matchInterlataCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchInterlataCategory() {
        return matchInterlataCategory;
    }

    /**
     * Legt den Wert der matchInterlataCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchInterlataCategory(Boolean value) {
        this.matchInterlataCategory = value;
    }

    /**
     * Ruft den Wert der matchIntralataCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchIntralataCategory() {
        return matchIntralataCategory;
    }

    /**
     * Legt den Wert der matchIntralataCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchIntralataCategory(Boolean value) {
        this.matchIntralataCategory = value;
    }

    /**
     * Ruft den Wert der matchInternationalCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchInternationalCategory() {
        return matchInternationalCategory;
    }

    /**
     * Legt den Wert der matchInternationalCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchInternationalCategory(Boolean value) {
        this.matchInternationalCategory = value;
    }

    /**
     * Ruft den Wert der matchPrivateCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchPrivateCategory() {
        return matchPrivateCategory;
    }

    /**
     * Legt den Wert der matchPrivateCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchPrivateCategory(Boolean value) {
        this.matchPrivateCategory = value;
    }

    /**
     * Ruft den Wert der matchEmergencyCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchEmergencyCategory() {
        return matchEmergencyCategory;
    }

    /**
     * Legt den Wert der matchEmergencyCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchEmergencyCategory(Boolean value) {
        this.matchEmergencyCategory = value;
    }

    /**
     * Ruft den Wert der matchOtherCategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchOtherCategory() {
        return matchOtherCategory;
    }

    /**
     * Legt den Wert der matchOtherCategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchOtherCategory(Boolean value) {
        this.matchOtherCategory = value;
    }

    /**
     * Ruft den Wert der matchInterNetwork-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchInterNetwork() {
        return matchInterNetwork;
    }

    /**
     * Legt den Wert der matchInterNetwork-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchInterNetwork(Boolean value) {
        this.matchInterNetwork = value;
    }

    /**
     * Ruft den Wert der matchInterHostingNE-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchInterHostingNE() {
        return matchInterHostingNE;
    }

    /**
     * Legt den Wert der matchInterHostingNE-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchInterHostingNE(Boolean value) {
        this.matchInterHostingNE = value;
    }

    /**
     * Ruft den Wert der matchInterAS-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchInterAS() {
        return matchInterAS;
    }

    /**
     * Legt den Wert der matchInterAS-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchInterAS(Boolean value) {
        this.matchInterAS = value;
    }

    /**
     * Ruft den Wert der matchIntraAS-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchIntraAS() {
        return matchIntraAS;
    }

    /**
     * Legt den Wert der matchIntraAS-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchIntraAS(Boolean value) {
        this.matchIntraAS = value;
    }

    /**
     * Ruft den Wert der matchChargeCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchChargeCalls() {
        return matchChargeCalls;
    }

    /**
     * Legt den Wert der matchChargeCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchChargeCalls(Boolean value) {
        this.matchChargeCalls = value;
    }

    /**
     * Ruft den Wert der matchNoChargeCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchNoChargeCalls() {
        return matchNoChargeCalls;
    }

    /**
     * Legt den Wert der matchNoChargeCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchNoChargeCalls(Boolean value) {
        this.matchNoChargeCalls = value;
    }

    /**
     * Ruft den Wert der matchGroupCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchGroupCalls() {
        return matchGroupCalls;
    }

    /**
     * Legt den Wert der matchGroupCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchGroupCalls(Boolean value) {
        this.matchGroupCalls = value;
    }

    /**
     * Ruft den Wert der matchEnterpriseCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchEnterpriseCalls() {
        return matchEnterpriseCalls;
    }

    /**
     * Legt den Wert der matchEnterpriseCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchEnterpriseCalls(Boolean value) {
        this.matchEnterpriseCalls = value;
    }

    /**
     * Ruft den Wert der matchNetworkCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchNetworkCalls() {
        return matchNetworkCalls;
    }

    /**
     * Legt den Wert der matchNetworkCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchNetworkCalls(Boolean value) {
        this.matchNetworkCalls = value;
    }

    /**
     * Ruft den Wert der matchNetworkURLCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchNetworkURLCalls() {
        return matchNetworkURLCalls;
    }

    /**
     * Legt den Wert der matchNetworkURLCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchNetworkURLCalls(Boolean value) {
        this.matchNetworkURLCalls = value;
    }

    /**
     * Ruft den Wert der matchRepairCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchRepairCalls() {
        return matchRepairCalls;
    }

    /**
     * Legt den Wert der matchRepairCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchRepairCalls(Boolean value) {
        this.matchRepairCalls = value;
    }

    /**
     * Ruft den Wert der matchEmergencyCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchEmergencyCalls() {
        return matchEmergencyCalls;
    }

    /**
     * Legt den Wert der matchEmergencyCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchEmergencyCalls(Boolean value) {
        this.matchEmergencyCalls = value;
    }

    /**
     * Ruft den Wert der matchInternalCalls-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchInternalCalls() {
        return matchInternalCalls;
    }

    /**
     * Legt den Wert der matchInternalCalls-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchInternalCalls(Boolean value) {
        this.matchInternalCalls = value;
    }

    /**
     * Ruft den Wert der matchOtherGETSGets-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchOtherGETSGets() {
        return matchOtherGETSGets;
    }

    /**
     * Legt den Wert der matchOtherGETSGets-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchOtherGETSGets(Boolean value) {
        this.matchOtherGETSGets = value;
    }

    /**
     * Ruft den Wert der matchNotGETSGets-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchNotGETSGets() {
        return matchNotGETSGets;
    }

    /**
     * Legt den Wert der matchNotGETSGets-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchNotGETSGets(Boolean value) {
        this.matchNotGETSGets = value;
    }

    /**
     * Ruft den Wert der matchGETSANGets-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchGETSANGets() {
        return matchGETSANGets;
    }

    /**
     * Legt den Wert der matchGETSANGets-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchGETSANGets(Boolean value) {
        this.matchGETSANGets = value;
    }

    /**
     * Ruft den Wert der matchGETSNTGets-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchGETSNTGets() {
        return matchGETSNTGets;
    }

    /**
     * Legt den Wert der matchGETSNTGets-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchGETSNTGets(Boolean value) {
        this.matchGETSNTGets = value;
    }

    /**
     * Ruft den Wert der matchGETSFCGets-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchGETSFCGets() {
        return matchGETSFCGets;
    }

    /**
     * Legt den Wert der matchGETSFCGets-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchGETSFCGets(Boolean value) {
        this.matchGETSFCGets = value;
    }

    /**
     * Ruft den Wert der matchGETSFCANGets-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchGETSFCANGets() {
        return matchGETSFCANGets;
    }

    /**
     * Legt den Wert der matchGETSFCANGets-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchGETSFCANGets(Boolean value) {
        this.matchGETSFCANGets = value;
    }

    /**
     * Ruft den Wert der matchGETSFCNTGets-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMatchGETSFCNTGets() {
        return matchGETSFCNTGets;
    }

    /**
     * Legt den Wert der matchGETSFCNTGets-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMatchGETSFCNTGets(Boolean value) {
        this.matchGETSFCNTGets = value;
    }

    /**
     * Ruft den Wert der matchLocation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocationCriteria }
     *     
     */
    public LocationCriteria getMatchLocation() {
        return matchLocation;
    }

    /**
     * Legt den Wert der matchLocation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationCriteria }
     *     
     */
    public void setMatchLocation(LocationCriteria value) {
        this.matchLocation = value;
    }

    /**
     * Ruft den Wert der matchRoaming-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RoamingCriteria }
     *     
     */
    public RoamingCriteria getMatchRoaming() {
        return matchRoaming;
    }

    /**
     * Legt den Wert der matchRoaming-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RoamingCriteria }
     *     
     */
    public void setMatchRoaming(RoamingCriteria value) {
        this.matchRoaming = value;
    }

    /**
     * Ruft den Wert der timeSchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTimeSchedule() {
        return timeSchedule;
    }

    /**
     * Legt den Wert der timeSchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTimeSchedule(JAXBElement<String> value) {
        this.timeSchedule = value;
    }

    /**
     * Ruft den Wert der holidaySchedule-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHolidaySchedule() {
        return holidaySchedule;
    }

    /**
     * Legt den Wert der holidaySchedule-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHolidaySchedule(JAXBElement<String> value) {
        this.holidaySchedule = value;
    }

    /**
     * Ruft den Wert der matchNumberPortabilityStatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ReplacementNumberPortabilityStatusList }{@code >}
     *     
     */
    public JAXBElement<ReplacementNumberPortabilityStatusList> getMatchNumberPortabilityStatus() {
        return matchNumberPortabilityStatus;
    }

    /**
     * Legt den Wert der matchNumberPortabilityStatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ReplacementNumberPortabilityStatusList }{@code >}
     *     
     */
    public void setMatchNumberPortabilityStatus(JAXBElement<ReplacementNumberPortabilityStatusList> value) {
        this.matchNumberPortabilityStatus = value;
    }

}
