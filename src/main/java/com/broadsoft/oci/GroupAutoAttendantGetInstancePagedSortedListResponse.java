//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to the GroupAutoAttendantGetInstancePagedSortedListRequest.
 *         The response contains a table with columns:
 *         "Service User Id", "Name", "Phone Number", "Is Phone Number Activated", "Country Code",
 *         "National Prefix", "Extension", "Department", "Department Type", "Parent Department",
 *         "Parent Department Type", "Is Active", "Type".
 *         The column values for "Is Active" can either be true, or false.
 *         The column values for "Type" can either be Basic or Standard.
 *         The "Department Type" and "Parent Department Type" columns will contain the values "Enterprise" or "Group".
 *         
 *         In XS Mode the value for the "Type" column will always be populated with Basic.
 *       
 * 
 * <p>Java-Klasse für GroupAutoAttendantGetInstancePagedSortedListResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="GroupAutoAttendantGetInstancePagedSortedListResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="autoAttendantTable" type="{C}OCITable"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupAutoAttendantGetInstancePagedSortedListResponse", propOrder = {
    "autoAttendantTable"
})
public class GroupAutoAttendantGetInstancePagedSortedListResponse
    extends OCIDataResponse
{

    @XmlElement(required = true)
    protected OCITable autoAttendantTable;

    /**
     * Ruft den Wert der autoAttendantTable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCITable }
     *     
     */
    public OCITable getAutoAttendantTable() {
        return autoAttendantTable;
    }

    /**
     * Legt den Wert der autoAttendantTable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCITable }
     *     
     */
    public void setAutoAttendantTable(OCITable value) {
        this.autoAttendantTable = value;
    }

}
