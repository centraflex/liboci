//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The voice portal greeting menu keys modify entry.
 *       
 * 
 * <p>Java-Klasse für ConferenceGreetingMenuKeysModifyEntry complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ConferenceGreetingMenuKeysModifyEntry">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="activateConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="deactivateConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="recordNewConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="listenToCurrentConfGreeting" type="{}DigitAny" minOccurs="0"/>
 *         <element name="returnToPreviousMenu" type="{}DigitAny" minOccurs="0"/>
 *         <element name="repeatMenu" type="{}DigitAny" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConferenceGreetingMenuKeysModifyEntry", propOrder = {
    "activateConfGreeting",
    "deactivateConfGreeting",
    "recordNewConfGreeting",
    "listenToCurrentConfGreeting",
    "returnToPreviousMenu",
    "repeatMenu"
})
public class ConferenceGreetingMenuKeysModifyEntry {

    @XmlElementRef(name = "activateConfGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> activateConfGreeting;
    @XmlElementRef(name = "deactivateConfGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deactivateConfGreeting;
    @XmlElementRef(name = "recordNewConfGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recordNewConfGreeting;
    @XmlElementRef(name = "listenToCurrentConfGreeting", type = JAXBElement.class, required = false)
    protected JAXBElement<String> listenToCurrentConfGreeting;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String returnToPreviousMenu;
    @XmlElementRef(name = "repeatMenu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> repeatMenu;

    /**
     * Ruft den Wert der activateConfGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActivateConfGreeting() {
        return activateConfGreeting;
    }

    /**
     * Legt den Wert der activateConfGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActivateConfGreeting(JAXBElement<String> value) {
        this.activateConfGreeting = value;
    }

    /**
     * Ruft den Wert der deactivateConfGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeactivateConfGreeting() {
        return deactivateConfGreeting;
    }

    /**
     * Legt den Wert der deactivateConfGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeactivateConfGreeting(JAXBElement<String> value) {
        this.deactivateConfGreeting = value;
    }

    /**
     * Ruft den Wert der recordNewConfGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecordNewConfGreeting() {
        return recordNewConfGreeting;
    }

    /**
     * Legt den Wert der recordNewConfGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecordNewConfGreeting(JAXBElement<String> value) {
        this.recordNewConfGreeting = value;
    }

    /**
     * Ruft den Wert der listenToCurrentConfGreeting-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getListenToCurrentConfGreeting() {
        return listenToCurrentConfGreeting;
    }

    /**
     * Legt den Wert der listenToCurrentConfGreeting-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setListenToCurrentConfGreeting(JAXBElement<String> value) {
        this.listenToCurrentConfGreeting = value;
    }

    /**
     * Ruft den Wert der returnToPreviousMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnToPreviousMenu() {
        return returnToPreviousMenu;
    }

    /**
     * Legt den Wert der returnToPreviousMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnToPreviousMenu(String value) {
        this.returnToPreviousMenu = value;
    }

    /**
     * Ruft den Wert der repeatMenu-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRepeatMenu() {
        return repeatMenu;
    }

    /**
     * Legt den Wert der repeatMenu-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRepeatMenu(JAXBElement<String> value) {
        this.repeatMenu = value;
    }

}
