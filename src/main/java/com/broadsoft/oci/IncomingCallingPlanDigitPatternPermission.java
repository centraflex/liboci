//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Indicates whether calls from specified digit patterns are permitted.
 *       
 * 
 * <p>Java-Klasse für IncomingCallingPlanDigitPatternPermission complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="IncomingCallingPlanDigitPatternPermission">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="digitPatternName" type="{}CallingPlanDigitPatternName"/>
 *         <element name="allow" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IncomingCallingPlanDigitPatternPermission", propOrder = {
    "digitPatternName",
    "allow"
})
public class IncomingCallingPlanDigitPatternPermission {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String digitPatternName;
    protected boolean allow;

    /**
     * Ruft den Wert der digitPatternName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDigitPatternName() {
        return digitPatternName;
    }

    /**
     * Legt den Wert der digitPatternName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDigitPatternName(String value) {
        this.digitPatternName = value;
    }

    /**
     * Ruft den Wert der allow-Eigenschaft ab.
     * 
     */
    public boolean isAllow() {
        return allow;
    }

    /**
     * Legt den Wert der allow-Eigenschaft fest.
     * 
     */
    public void setAllow(boolean value) {
        this.allow = value;
    }

}
