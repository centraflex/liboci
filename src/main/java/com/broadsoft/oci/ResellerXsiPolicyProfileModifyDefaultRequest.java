//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Modifies the default Xsi policy profile for a reseller.
 *         The response is either a SuccessResponse or an ErrorResponse.
 *       
 * 
 * <p>Java-Klasse für ResellerXsiPolicyProfileModifyDefaultRequest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="ResellerXsiPolicyProfileModifyDefaultRequest">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="resellerId" type="{}ResellerId22"/>
 *         <element name="spDefaultXsiPolicyProfile" type="{}XsiPolicyProfileName" minOccurs="0"/>
 *         <element name="groupDefaultXsiPolicyProfile" type="{}XsiPolicyProfileName" minOccurs="0"/>
 *         <element name="userDefaultXsiPolicyProfile" type="{}XsiPolicyProfileName" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerXsiPolicyProfileModifyDefaultRequest", propOrder = {
    "resellerId",
    "spDefaultXsiPolicyProfile",
    "groupDefaultXsiPolicyProfile",
    "userDefaultXsiPolicyProfile"
})
public class ResellerXsiPolicyProfileModifyDefaultRequest
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String resellerId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String spDefaultXsiPolicyProfile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String groupDefaultXsiPolicyProfile;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userDefaultXsiPolicyProfile;

    /**
     * Ruft den Wert der resellerId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * Legt den Wert der resellerId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerId(String value) {
        this.resellerId = value;
    }

    /**
     * Ruft den Wert der spDefaultXsiPolicyProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpDefaultXsiPolicyProfile() {
        return spDefaultXsiPolicyProfile;
    }

    /**
     * Legt den Wert der spDefaultXsiPolicyProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpDefaultXsiPolicyProfile(String value) {
        this.spDefaultXsiPolicyProfile = value;
    }

    /**
     * Ruft den Wert der groupDefaultXsiPolicyProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupDefaultXsiPolicyProfile() {
        return groupDefaultXsiPolicyProfile;
    }

    /**
     * Legt den Wert der groupDefaultXsiPolicyProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupDefaultXsiPolicyProfile(String value) {
        this.groupDefaultXsiPolicyProfile = value;
    }

    /**
     * Ruft den Wert der userDefaultXsiPolicyProfile-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserDefaultXsiPolicyProfile() {
        return userDefaultXsiPolicyProfile;
    }

    /**
     * Legt den Wert der userDefaultXsiPolicyProfile-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserDefaultXsiPolicyProfile(String value) {
        this.userDefaultXsiPolicyProfile = value;
    }

}
