//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ServiceProviderAdminGroupAccess.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="ServiceProviderAdminGroupAccess">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Full"/>
 *     <enumeration value="Restricted from Adding or Removing Groups"/>
 *     <enumeration value="None"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "ServiceProviderAdminGroupAccess")
@XmlEnum
public enum ServiceProviderAdminGroupAccess {

    @XmlEnumValue("Full")
    FULL("Full"),
    @XmlEnumValue("Restricted from Adding or Removing Groups")
    RESTRICTED_FROM_ADDING_OR_REMOVING_GROUPS("Restricted from Adding or Removing Groups"),
    @XmlEnumValue("None")
    NONE("None");
    private final String value;

    ServiceProviderAdminGroupAccess(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ServiceProviderAdminGroupAccess fromValue(String v) {
        for (ServiceProviderAdminGroupAccess c: ServiceProviderAdminGroupAccess.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
