//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Request user's call logs.
 *         If the callLogType is not specified, all types of calls logs (placed, received, missed) are returned.
 *         The filters "dateTimeRange", "numberFilter", "redirectedNumberFilter", "accountAuthorizationCodeFilter"
 *         "callAuthorizationCodeFilter" and "subscriberType" are ignored if call logs are stored in CDS. When
 *         "ReceivedOrMissed" is specified as "callLogType" and call logs are stored in CDS, all call logs including
 *         placed will be returned.
 *         It is possible to restrict the number of rows returned using responsePagingControl. If responsePagingControl
 *         is not specified, the value of Enhanced Call Logs system parameter maxNonPagedResponseSize will control
 *         the maximum number of call logs can be returned. 
 *         The response is either a UserEnhancedCallLogsGetListResponse20sp1 or an ErrorResponse.
 *         The following elements are only used in AS data mode:
 *           callAuthorizationCodeFilter
 *           callSecurityClassification
 *       
 * 
 * <p>Java-Klasse für UserEnhancedCallLogsGetListRequest20sp1 complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="UserEnhancedCallLogsGetListRequest20sp1">
 *   <complexContent>
 *     <extension base="{C}OCIRequest">
 *       <sequence>
 *         <element name="userId" type="{}UserId"/>
 *         <element name="callLogType" type="{}EnhancedCallLogsCallLogsRequestType" minOccurs="0"/>
 *         <element name="dateTimeRange" type="{}EnhancedCallLogsTimeRange"/>
 *         <element name="numberFilter" type="{}EnhancedCallLogsNumberFilter" minOccurs="0"/>
 *         <element name="redirectedNumberFilter" type="{}EnhancedCallLogsRedirectedNumberFilter20sp1" minOccurs="0"/>
 *         <element name="accountAuthorizationCodeFilter" type="{}EnhancedCallLogsAccountAuthorizationCodeFilter" minOccurs="0"/>
 *         <element name="callAuthorizationCodeFilter" type="{}EnhancedCallLogsCallAuthorizationCodeFilter" minOccurs="0"/>
 *         <element name="subscriberType" type="{}EnhancedCallLogsSubscriberType20" minOccurs="0"/>
 *         <element name="responsePagingControl" type="{}ResponsePagingControl" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserEnhancedCallLogsGetListRequest20sp1", propOrder = {
    "userId",
    "callLogType",
    "dateTimeRange",
    "numberFilter",
    "redirectedNumberFilter",
    "accountAuthorizationCodeFilter",
    "callAuthorizationCodeFilter",
    "subscriberType",
    "responsePagingControl"
})
public class UserEnhancedCallLogsGetListRequest20Sp1
    extends OCIRequest
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String userId;
    @XmlSchemaType(name = "token")
    protected EnhancedCallLogsCallLogsRequestType callLogType;
    @XmlElement(required = true)
    protected EnhancedCallLogsTimeRange dateTimeRange;
    protected EnhancedCallLogsNumberFilter numberFilter;
    protected EnhancedCallLogsRedirectedNumberFilter20Sp1 redirectedNumberFilter;
    protected EnhancedCallLogsAccountAuthorizationCodeFilter accountAuthorizationCodeFilter;
    protected EnhancedCallLogsCallAuthorizationCodeFilter callAuthorizationCodeFilter;
    @XmlSchemaType(name = "token")
    protected EnhancedCallLogsSubscriberType20 subscriberType;
    protected ResponsePagingControl responsePagingControl;

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der callLogType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnhancedCallLogsCallLogsRequestType }
     *     
     */
    public EnhancedCallLogsCallLogsRequestType getCallLogType() {
        return callLogType;
    }

    /**
     * Legt den Wert der callLogType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnhancedCallLogsCallLogsRequestType }
     *     
     */
    public void setCallLogType(EnhancedCallLogsCallLogsRequestType value) {
        this.callLogType = value;
    }

    /**
     * Ruft den Wert der dateTimeRange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnhancedCallLogsTimeRange }
     *     
     */
    public EnhancedCallLogsTimeRange getDateTimeRange() {
        return dateTimeRange;
    }

    /**
     * Legt den Wert der dateTimeRange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnhancedCallLogsTimeRange }
     *     
     */
    public void setDateTimeRange(EnhancedCallLogsTimeRange value) {
        this.dateTimeRange = value;
    }

    /**
     * Ruft den Wert der numberFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnhancedCallLogsNumberFilter }
     *     
     */
    public EnhancedCallLogsNumberFilter getNumberFilter() {
        return numberFilter;
    }

    /**
     * Legt den Wert der numberFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnhancedCallLogsNumberFilter }
     *     
     */
    public void setNumberFilter(EnhancedCallLogsNumberFilter value) {
        this.numberFilter = value;
    }

    /**
     * Ruft den Wert der redirectedNumberFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnhancedCallLogsRedirectedNumberFilter20Sp1 }
     *     
     */
    public EnhancedCallLogsRedirectedNumberFilter20Sp1 getRedirectedNumberFilter() {
        return redirectedNumberFilter;
    }

    /**
     * Legt den Wert der redirectedNumberFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnhancedCallLogsRedirectedNumberFilter20Sp1 }
     *     
     */
    public void setRedirectedNumberFilter(EnhancedCallLogsRedirectedNumberFilter20Sp1 value) {
        this.redirectedNumberFilter = value;
    }

    /**
     * Ruft den Wert der accountAuthorizationCodeFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnhancedCallLogsAccountAuthorizationCodeFilter }
     *     
     */
    public EnhancedCallLogsAccountAuthorizationCodeFilter getAccountAuthorizationCodeFilter() {
        return accountAuthorizationCodeFilter;
    }

    /**
     * Legt den Wert der accountAuthorizationCodeFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnhancedCallLogsAccountAuthorizationCodeFilter }
     *     
     */
    public void setAccountAuthorizationCodeFilter(EnhancedCallLogsAccountAuthorizationCodeFilter value) {
        this.accountAuthorizationCodeFilter = value;
    }

    /**
     * Ruft den Wert der callAuthorizationCodeFilter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnhancedCallLogsCallAuthorizationCodeFilter }
     *     
     */
    public EnhancedCallLogsCallAuthorizationCodeFilter getCallAuthorizationCodeFilter() {
        return callAuthorizationCodeFilter;
    }

    /**
     * Legt den Wert der callAuthorizationCodeFilter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnhancedCallLogsCallAuthorizationCodeFilter }
     *     
     */
    public void setCallAuthorizationCodeFilter(EnhancedCallLogsCallAuthorizationCodeFilter value) {
        this.callAuthorizationCodeFilter = value;
    }

    /**
     * Ruft den Wert der subscriberType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnhancedCallLogsSubscriberType20 }
     *     
     */
    public EnhancedCallLogsSubscriberType20 getSubscriberType() {
        return subscriberType;
    }

    /**
     * Legt den Wert der subscriberType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnhancedCallLogsSubscriberType20 }
     *     
     */
    public void setSubscriberType(EnhancedCallLogsSubscriberType20 value) {
        this.subscriberType = value;
    }

    /**
     * Ruft den Wert der responsePagingControl-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ResponsePagingControl }
     *     
     */
    public ResponsePagingControl getResponsePagingControl() {
        return responsePagingControl;
    }

    /**
     * Legt den Wert der responsePagingControl-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsePagingControl }
     *     
     */
    public void setResponsePagingControl(ResponsePagingControl value) {
        this.responsePagingControl = value;
    }

}
