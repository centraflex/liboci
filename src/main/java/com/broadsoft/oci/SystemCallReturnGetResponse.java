//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Response to SystemCallReturnGetRequest.
 *       
 * 
 * <p>Java-Klasse für SystemCallReturnGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemCallReturnGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="twoLevelActivation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="provideDate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="lastUnansweredCallOnly" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="confirmationKey" type="{}DigitAny" minOccurs="0"/>
 *         <element name="allowRestrictedNumber" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="deleteNumberAfterAnsweredCallReturn" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemCallReturnGetResponse", propOrder = {
    "twoLevelActivation",
    "provideDate",
    "lastUnansweredCallOnly",
    "confirmationKey",
    "allowRestrictedNumber",
    "deleteNumberAfterAnsweredCallReturn"
})
public class SystemCallReturnGetResponse
    extends OCIDataResponse
{

    protected boolean twoLevelActivation;
    protected boolean provideDate;
    protected boolean lastUnansweredCallOnly;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String confirmationKey;
    protected boolean allowRestrictedNumber;
    protected boolean deleteNumberAfterAnsweredCallReturn;

    /**
     * Ruft den Wert der twoLevelActivation-Eigenschaft ab.
     * 
     */
    public boolean isTwoLevelActivation() {
        return twoLevelActivation;
    }

    /**
     * Legt den Wert der twoLevelActivation-Eigenschaft fest.
     * 
     */
    public void setTwoLevelActivation(boolean value) {
        this.twoLevelActivation = value;
    }

    /**
     * Ruft den Wert der provideDate-Eigenschaft ab.
     * 
     */
    public boolean isProvideDate() {
        return provideDate;
    }

    /**
     * Legt den Wert der provideDate-Eigenschaft fest.
     * 
     */
    public void setProvideDate(boolean value) {
        this.provideDate = value;
    }

    /**
     * Ruft den Wert der lastUnansweredCallOnly-Eigenschaft ab.
     * 
     */
    public boolean isLastUnansweredCallOnly() {
        return lastUnansweredCallOnly;
    }

    /**
     * Legt den Wert der lastUnansweredCallOnly-Eigenschaft fest.
     * 
     */
    public void setLastUnansweredCallOnly(boolean value) {
        this.lastUnansweredCallOnly = value;
    }

    /**
     * Ruft den Wert der confirmationKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmationKey() {
        return confirmationKey;
    }

    /**
     * Legt den Wert der confirmationKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmationKey(String value) {
        this.confirmationKey = value;
    }

    /**
     * Ruft den Wert der allowRestrictedNumber-Eigenschaft ab.
     * 
     */
    public boolean isAllowRestrictedNumber() {
        return allowRestrictedNumber;
    }

    /**
     * Legt den Wert der allowRestrictedNumber-Eigenschaft fest.
     * 
     */
    public void setAllowRestrictedNumber(boolean value) {
        this.allowRestrictedNumber = value;
    }

    /**
     * Ruft den Wert der deleteNumberAfterAnsweredCallReturn-Eigenschaft ab.
     * 
     */
    public boolean isDeleteNumberAfterAnsweredCallReturn() {
        return deleteNumberAfterAnsweredCallReturn;
    }

    /**
     * Legt den Wert der deleteNumberAfterAnsweredCallReturn-Eigenschaft fest.
     * 
     */
    public void setDeleteNumberAfterAnsweredCallReturn(boolean value) {
        this.deleteNumberAfterAnsweredCallReturn = value;
    }

}
