//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *         Response to SystemMediaServerParametersGetListRequest.
 *         Contains a list of system Media Server parameters.
 *       
 * 
 * <p>Java-Klasse für SystemMediaServerParametersGetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="SystemMediaServerParametersGetResponse">
 *   <complexContent>
 *     <extension base="{C}OCIDataResponse">
 *       <sequence>
 *         <element name="mediaServerResponseTimerMilliseconds" type="{}MediaServerResponseTimerMilliseconds"/>
 *         <element name="mediaServerSelectionRouteTimerMilliseconds" type="{}MediaServerSelectionRouteTimerMilliseconds"/>
 *         <element name="useStaticMediaServerDevice" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemMediaServerParametersGetResponse", propOrder = {
    "mediaServerResponseTimerMilliseconds",
    "mediaServerSelectionRouteTimerMilliseconds",
    "useStaticMediaServerDevice"
})
public class SystemMediaServerParametersGetResponse
    extends OCIDataResponse
{

    protected int mediaServerResponseTimerMilliseconds;
    protected int mediaServerSelectionRouteTimerMilliseconds;
    protected boolean useStaticMediaServerDevice;

    /**
     * Ruft den Wert der mediaServerResponseTimerMilliseconds-Eigenschaft ab.
     * 
     */
    public int getMediaServerResponseTimerMilliseconds() {
        return mediaServerResponseTimerMilliseconds;
    }

    /**
     * Legt den Wert der mediaServerResponseTimerMilliseconds-Eigenschaft fest.
     * 
     */
    public void setMediaServerResponseTimerMilliseconds(int value) {
        this.mediaServerResponseTimerMilliseconds = value;
    }

    /**
     * Ruft den Wert der mediaServerSelectionRouteTimerMilliseconds-Eigenschaft ab.
     * 
     */
    public int getMediaServerSelectionRouteTimerMilliseconds() {
        return mediaServerSelectionRouteTimerMilliseconds;
    }

    /**
     * Legt den Wert der mediaServerSelectionRouteTimerMilliseconds-Eigenschaft fest.
     * 
     */
    public void setMediaServerSelectionRouteTimerMilliseconds(int value) {
        this.mediaServerSelectionRouteTimerMilliseconds = value;
    }

    /**
     * Ruft den Wert der useStaticMediaServerDevice-Eigenschaft ab.
     * 
     */
    public boolean isUseStaticMediaServerDevice() {
        return useStaticMediaServerDevice;
    }

    /**
     * Legt den Wert der useStaticMediaServerDevice-Eigenschaft fest.
     * 
     */
    public void setUseStaticMediaServerDevice(boolean value) {
        this.useStaticMediaServerDevice = value;
    }

}
