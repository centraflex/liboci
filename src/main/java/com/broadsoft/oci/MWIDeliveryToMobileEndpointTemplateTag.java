//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für MWIDeliveryToMobileEndpointTemplateTag.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="MWIDeliveryToMobileEndpointTemplateTag">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="Voice Mail Number"/>
 *     <enumeration value="New Messages Count"/>
 *     <enumeration value="Total Messages Count"/>
 *     <enumeration value="Caller Name"/>
 *     <enumeration value="Caller Number"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "MWIDeliveryToMobileEndpointTemplateTag")
@XmlEnum
public enum MWIDeliveryToMobileEndpointTemplateTag {

    @XmlEnumValue("Voice Mail Number")
    VOICE_MAIL_NUMBER("Voice Mail Number"),
    @XmlEnumValue("New Messages Count")
    NEW_MESSAGES_COUNT("New Messages Count"),
    @XmlEnumValue("Total Messages Count")
    TOTAL_MESSAGES_COUNT("Total Messages Count"),
    @XmlEnumValue("Caller Name")
    CALLER_NAME("Caller Name"),
    @XmlEnumValue("Caller Number")
    CALLER_NUMBER("Caller Number");
    private final String value;

    MWIDeliveryToMobileEndpointTemplateTag(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MWIDeliveryToMobileEndpointTemplateTag fromValue(String v) {
        for (MWIDeliveryToMobileEndpointTemplateTag c: MWIDeliveryToMobileEndpointTemplateTag.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
