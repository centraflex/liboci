//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.oci;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Access device end point used in the context of add.
 *         Port numbers are only used by devices with static line ordering.
 *         The following element is only used in AS data mode and ignored in XS data mode:
 *           pathHeader 
 *           useHotline, use value false in XS data mode
 *           hotlineContact
 * 
 *         The following element is only used in XS data mode and ignored in AS data mode:
 *           privateIdentity
 * 
 *       
 * 
 * <p>Java-Klasse für AccessDeviceEndpointAndHotlineAdd complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="AccessDeviceEndpointAndHotlineAdd">
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="accessDevice" type="{}AccessDevice"/>
 *         <element name="linePort" type="{}AccessDeviceEndpointLinePort"/>
 *         <element name="privateIdentity" type="{}AccessDeviceEndpointPrivateIdentity" minOccurs="0"/>
 *         <element name="contact" type="{}SIPContact" minOccurs="0"/>
 *         <element name="pathHeader" type="{}PathHeader" minOccurs="0"/>
 *         <element name="portNumber" type="{}AccessDevicePortNumber" minOccurs="0"/>
 *         <element name="useHotline" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         <element name="hotlineContact" type="{}SIPURI" minOccurs="0"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccessDeviceEndpointAndHotlineAdd", propOrder = {
    "accessDevice",
    "linePort",
    "privateIdentity",
    "contact",
    "pathHeader",
    "portNumber",
    "useHotline",
    "hotlineContact"
})
public class AccessDeviceEndpointAndHotlineAdd {

    @XmlElement(required = true)
    protected AccessDevice accessDevice;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String linePort;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String privateIdentity;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String contact;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String pathHeader;
    protected Integer portNumber;
    protected boolean useHotline;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String hotlineContact;

    /**
     * Ruft den Wert der accessDevice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AccessDevice }
     *     
     */
    public AccessDevice getAccessDevice() {
        return accessDevice;
    }

    /**
     * Legt den Wert der accessDevice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessDevice }
     *     
     */
    public void setAccessDevice(AccessDevice value) {
        this.accessDevice = value;
    }

    /**
     * Ruft den Wert der linePort-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinePort() {
        return linePort;
    }

    /**
     * Legt den Wert der linePort-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinePort(String value) {
        this.linePort = value;
    }

    /**
     * Ruft den Wert der privateIdentity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrivateIdentity() {
        return privateIdentity;
    }

    /**
     * Legt den Wert der privateIdentity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrivateIdentity(String value) {
        this.privateIdentity = value;
    }

    /**
     * Ruft den Wert der contact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContact() {
        return contact;
    }

    /**
     * Legt den Wert der contact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContact(String value) {
        this.contact = value;
    }

    /**
     * Ruft den Wert der pathHeader-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPathHeader() {
        return pathHeader;
    }

    /**
     * Legt den Wert der pathHeader-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPathHeader(String value) {
        this.pathHeader = value;
    }

    /**
     * Ruft den Wert der portNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPortNumber() {
        return portNumber;
    }

    /**
     * Legt den Wert der portNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPortNumber(Integer value) {
        this.portNumber = value;
    }

    /**
     * Ruft den Wert der useHotline-Eigenschaft ab.
     * 
     */
    public boolean isUseHotline() {
        return useHotline;
    }

    /**
     * Legt den Wert der useHotline-Eigenschaft fest.
     * 
     */
    public void setUseHotline(boolean value) {
        this.useHotline = value;
    }

    /**
     * Ruft den Wert der hotlineContact-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotlineContact() {
        return hotlineContact;
    }

    /**
     * Legt den Wert der hotlineContact-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotlineContact(String value) {
        this.hotlineContact = value;
    }

}
