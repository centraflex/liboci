//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.ocir;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *       This notification is sent for tasks like Trunk Group User Creation or Service Pack
 *       Migration that change provisioning data without invoking OCI-P requests.     
 *     
 * 
 * <p>Java-Klasse für OCIReportingProvisioningTaskReportNotification complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OCIReportingProvisioningTaskReportNotification">
 *   <complexContent>
 *     <extension base="{}OCIReportingNotification">
 *       <sequence>
 *         <element name="id" type="{}OCIReportingProvisioningTaskType"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OCIReportingProvisioningTaskReportNotification", propOrder = {
    "id"
})
public class OCIReportingProvisioningTaskReportNotification
    extends OCIReportingNotification
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OCIReportingProvisioningTaskType id;

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCIReportingProvisioningTaskType }
     *     
     */
    public OCIReportingProvisioningTaskType getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCIReportingProvisioningTaskType }
     *     
     */
    public void setId(OCIReportingProvisioningTaskType value) {
        this.id = value;
    }

}
