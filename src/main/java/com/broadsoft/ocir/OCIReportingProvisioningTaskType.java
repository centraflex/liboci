//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.ocir;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für OCIReportingProvisioningTaskType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <pre>{@code
 * <simpleType name="OCIReportingProvisioningTaskType">
 *   <restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     <enumeration value="OCI-P"/>
 *     <enumeration value="Trunk Group User Creation"/>
 *     <enumeration value="Service Pack Migration"/>
 *   </restriction>
 * </simpleType>
 * }</pre>
 * 
 */
@XmlType(name = "OCIReportingProvisioningTaskType")
@XmlEnum
public enum OCIReportingProvisioningTaskType {

    @XmlEnumValue("OCI-P")
    OCI_P("OCI-P"),
    @XmlEnumValue("Trunk Group User Creation")
    TRUNK_GROUP_USER_CREATION("Trunk Group User Creation"),
    @XmlEnumValue("Service Pack Migration")
    SERVICE_PACK_MIGRATION("Service Pack Migration");
    private final String value;

    OCIReportingProvisioningTaskType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OCIReportingProvisioningTaskType fromValue(String v) {
        for (OCIReportingProvisioningTaskType c: OCIReportingProvisioningTaskType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
