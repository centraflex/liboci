//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.ocir;

import jakarta.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.broadsoft.ocir package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.broadsoft.ocir
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OCIReportingReportPublicIdentityNotification }
     * 
     * @return
     *     the new instance of {@link OCIReportingReportPublicIdentityNotification }
     */
    public OCIReportingReportPublicIdentityNotification createOCIReportingReportPublicIdentityNotification() {
        return new OCIReportingReportPublicIdentityNotification();
    }

    /**
     * Create an instance of {@link OCIReportingMessage }
     * 
     * @return
     *     the new instance of {@link OCIReportingMessage }
     */
    public OCIReportingMessage createOCIReportingMessage() {
        return new OCIReportingMessage();
    }

    /**
     * Create an instance of {@link OCIReportingServerStatusNotification }
     * 
     * @return
     *     the new instance of {@link OCIReportingServerStatusNotification }
     */
    public OCIReportingServerStatusNotification createOCIReportingServerStatusNotification() {
        return new OCIReportingServerStatusNotification();
    }

    /**
     * Create an instance of {@link OCIReportingReportNotification }
     * 
     * @return
     *     the new instance of {@link OCIReportingReportNotification }
     */
    public OCIReportingReportNotification createOCIReportingReportNotification() {
        return new OCIReportingReportNotification();
    }

    /**
     * Create an instance of {@link OCIReportingProvisioningTaskReportNotification }
     * 
     * @return
     *     the new instance of {@link OCIReportingProvisioningTaskReportNotification }
     */
    public OCIReportingProvisioningTaskReportNotification createOCIReportingProvisioningTaskReportNotification() {
        return new OCIReportingProvisioningTaskReportNotification();
    }

    /**
     * Create an instance of {@link OCIReportingReportPublicIdentityNotification.DeletedUser }
     * 
     * @return
     *     the new instance of {@link OCIReportingReportPublicIdentityNotification.DeletedUser }
     */
    public OCIReportingReportPublicIdentityNotification.DeletedUser createOCIReportingReportPublicIdentityNotificationDeletedUser() {
        return new OCIReportingReportPublicIdentityNotification.DeletedUser();
    }

    /**
     * Create an instance of {@link OCIReportingReportPublicIdentityNotification.AddedUser }
     * 
     * @return
     *     the new instance of {@link OCIReportingReportPublicIdentityNotification.AddedUser }
     */
    public OCIReportingReportPublicIdentityNotification.AddedUser createOCIReportingReportPublicIdentityNotificationAddedUser() {
        return new OCIReportingReportPublicIdentityNotification.AddedUser();
    }

    /**
     * Create an instance of {@link OCIReportingReportPublicIdentityNotification.ModifiedUser }
     * 
     * @return
     *     the new instance of {@link OCIReportingReportPublicIdentityNotification.ModifiedUser }
     */
    public OCIReportingReportPublicIdentityNotification.ModifiedUser createOCIReportingReportPublicIdentityNotificationModifiedUser() {
        return new OCIReportingReportPublicIdentityNotification.ModifiedUser();
    }

}
