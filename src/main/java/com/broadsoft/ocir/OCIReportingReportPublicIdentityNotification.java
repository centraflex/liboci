//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.ocir;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.NormalizedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *       Notification of added, deleted and modified public identities.  The user Id is full user Id including
 *       the domain.
 *     
 * 
 * <p>Java-Klasse für OCIReportingReportPublicIdentityNotification complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OCIReportingReportPublicIdentityNotification">
 *   <complexContent>
 *     <extension base="{}OCIReportingNotification">
 *       <sequence>
 *         <element name="deletedUser" maxOccurs="unbounded" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="userId" type="{}OCIReportingUserId"/>
 *                   <element name="deletedAlias" type="{}OCIReportingAlias" maxOccurs="unbounded" minOccurs="0"/>
 *                   <element name="deletedSipURI" type="{}OCIReportingSipURI" maxOccurs="unbounded" minOccurs="0"/>
 *                   <element name="deletedTelURI" type="{}OCIReportingTelURI" maxOccurs="unbounded" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="addedUser" maxOccurs="unbounded" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="userId" type="{}OCIReportingUserId"/>
 *                   <element name="addedAlias" type="{}OCIReportingAlias" maxOccurs="unbounded" minOccurs="0"/>
 *                   <element name="addedSipURI" type="{}OCIReportingSipURI" maxOccurs="unbounded" minOccurs="0"/>
 *                   <element name="addedTelURI" type="{}OCIReportingTelURI" maxOccurs="unbounded" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *         <element name="modifiedUser" maxOccurs="unbounded" minOccurs="0">
 *           <complexType>
 *             <complexContent>
 *               <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 <sequence>
 *                   <element name="userId" type="{}OCIReportingUserId"/>
 *                   <element name="deletedAlias" type="{}OCIReportingAlias" maxOccurs="unbounded" minOccurs="0"/>
 *                   <element name="addedAlias" type="{}OCIReportingAlias" maxOccurs="unbounded" minOccurs="0"/>
 *                   <element name="deletedSipURI" type="{}OCIReportingSipURI" maxOccurs="unbounded" minOccurs="0"/>
 *                   <element name="addedSipURI" type="{}OCIReportingSipURI" maxOccurs="unbounded" minOccurs="0"/>
 *                   <element name="deletedTelURI" type="{}OCIReportingTelURI" maxOccurs="unbounded" minOccurs="0"/>
 *                   <element name="addedTelURI" type="{}OCIReportingTelURI" maxOccurs="unbounded" minOccurs="0"/>
 *                 </sequence>
 *               </restriction>
 *             </complexContent>
 *           </complexType>
 *         </element>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OCIReportingReportPublicIdentityNotification", propOrder = {
    "deletedUser",
    "addedUser",
    "modifiedUser"
})
public class OCIReportingReportPublicIdentityNotification
    extends OCIReportingNotification
{

    protected List<OCIReportingReportPublicIdentityNotification.DeletedUser> deletedUser;
    protected List<OCIReportingReportPublicIdentityNotification.AddedUser> addedUser;
    protected List<OCIReportingReportPublicIdentityNotification.ModifiedUser> modifiedUser;

    /**
     * Gets the value of the deletedUser property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the deletedUser property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeletedUser().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OCIReportingReportPublicIdentityNotification.DeletedUser }
     * 
     * 
     * @return
     *     The value of the deletedUser property.
     */
    public List<OCIReportingReportPublicIdentityNotification.DeletedUser> getDeletedUser() {
        if (deletedUser == null) {
            deletedUser = new ArrayList<>();
        }
        return this.deletedUser;
    }

    /**
     * Gets the value of the addedUser property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the addedUser property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddedUser().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OCIReportingReportPublicIdentityNotification.AddedUser }
     * 
     * 
     * @return
     *     The value of the addedUser property.
     */
    public List<OCIReportingReportPublicIdentityNotification.AddedUser> getAddedUser() {
        if (addedUser == null) {
            addedUser = new ArrayList<>();
        }
        return this.addedUser;
    }

    /**
     * Gets the value of the modifiedUser property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a {@code set} method for the modifiedUser property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getModifiedUser().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OCIReportingReportPublicIdentityNotification.ModifiedUser }
     * 
     * 
     * @return
     *     The value of the modifiedUser property.
     */
    public List<OCIReportingReportPublicIdentityNotification.ModifiedUser> getModifiedUser() {
        if (modifiedUser == null) {
            modifiedUser = new ArrayList<>();
        }
        return this.modifiedUser;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="userId" type="{}OCIReportingUserId"/>
     *         <element name="addedAlias" type="{}OCIReportingAlias" maxOccurs="unbounded" minOccurs="0"/>
     *         <element name="addedSipURI" type="{}OCIReportingSipURI" maxOccurs="unbounded" minOccurs="0"/>
     *         <element name="addedTelURI" type="{}OCIReportingTelURI" maxOccurs="unbounded" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "userId",
        "addedAlias",
        "addedSipURI",
        "addedTelURI"
    })
    public static class AddedUser {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
        @XmlSchemaType(name = "normalizedString")
        protected String userId;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> addedAlias;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> addedSipURI;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> addedTelURI;

        /**
         * Ruft den Wert der userId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUserId() {
            return userId;
        }

        /**
         * Legt den Wert der userId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUserId(String value) {
            this.userId = value;
        }

        /**
         * Gets the value of the addedAlias property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the addedAlias property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddedAlias().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the addedAlias property.
         */
        public List<String> getAddedAlias() {
            if (addedAlias == null) {
                addedAlias = new ArrayList<>();
            }
            return this.addedAlias;
        }

        /**
         * Gets the value of the addedSipURI property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the addedSipURI property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddedSipURI().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the addedSipURI property.
         */
        public List<String> getAddedSipURI() {
            if (addedSipURI == null) {
                addedSipURI = new ArrayList<>();
            }
            return this.addedSipURI;
        }

        /**
         * Gets the value of the addedTelURI property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the addedTelURI property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddedTelURI().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the addedTelURI property.
         */
        public List<String> getAddedTelURI() {
            if (addedTelURI == null) {
                addedTelURI = new ArrayList<>();
            }
            return this.addedTelURI;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="userId" type="{}OCIReportingUserId"/>
     *         <element name="deletedAlias" type="{}OCIReportingAlias" maxOccurs="unbounded" minOccurs="0"/>
     *         <element name="deletedSipURI" type="{}OCIReportingSipURI" maxOccurs="unbounded" minOccurs="0"/>
     *         <element name="deletedTelURI" type="{}OCIReportingTelURI" maxOccurs="unbounded" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "userId",
        "deletedAlias",
        "deletedSipURI",
        "deletedTelURI"
    })
    public static class DeletedUser {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
        @XmlSchemaType(name = "normalizedString")
        protected String userId;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> deletedAlias;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> deletedSipURI;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> deletedTelURI;

        /**
         * Ruft den Wert der userId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUserId() {
            return userId;
        }

        /**
         * Legt den Wert der userId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUserId(String value) {
            this.userId = value;
        }

        /**
         * Gets the value of the deletedAlias property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the deletedAlias property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDeletedAlias().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the deletedAlias property.
         */
        public List<String> getDeletedAlias() {
            if (deletedAlias == null) {
                deletedAlias = new ArrayList<>();
            }
            return this.deletedAlias;
        }

        /**
         * Gets the value of the deletedSipURI property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the deletedSipURI property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDeletedSipURI().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the deletedSipURI property.
         */
        public List<String> getDeletedSipURI() {
            if (deletedSipURI == null) {
                deletedSipURI = new ArrayList<>();
            }
            return this.deletedSipURI;
        }

        /**
         * Gets the value of the deletedTelURI property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the deletedTelURI property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDeletedTelURI().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the deletedTelURI property.
         */
        public List<String> getDeletedTelURI() {
            if (deletedTelURI == null) {
                deletedTelURI = new ArrayList<>();
            }
            return this.deletedTelURI;
        }

    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>{@code
     * <complexType>
     *   <complexContent>
     *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       <sequence>
     *         <element name="userId" type="{}OCIReportingUserId"/>
     *         <element name="deletedAlias" type="{}OCIReportingAlias" maxOccurs="unbounded" minOccurs="0"/>
     *         <element name="addedAlias" type="{}OCIReportingAlias" maxOccurs="unbounded" minOccurs="0"/>
     *         <element name="deletedSipURI" type="{}OCIReportingSipURI" maxOccurs="unbounded" minOccurs="0"/>
     *         <element name="addedSipURI" type="{}OCIReportingSipURI" maxOccurs="unbounded" minOccurs="0"/>
     *         <element name="deletedTelURI" type="{}OCIReportingTelURI" maxOccurs="unbounded" minOccurs="0"/>
     *         <element name="addedTelURI" type="{}OCIReportingTelURI" maxOccurs="unbounded" minOccurs="0"/>
     *       </sequence>
     *     </restriction>
     *   </complexContent>
     * </complexType>
     * }</pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "userId",
        "deletedAlias",
        "addedAlias",
        "deletedSipURI",
        "addedSipURI",
        "deletedTelURI",
        "addedTelURI"
    })
    public static class ModifiedUser {

        @XmlElement(required = true)
        @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
        @XmlSchemaType(name = "normalizedString")
        protected String userId;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> deletedAlias;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> addedAlias;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> deletedSipURI;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> addedSipURI;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> deletedTelURI;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected List<String> addedTelURI;

        /**
         * Ruft den Wert der userId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUserId() {
            return userId;
        }

        /**
         * Legt den Wert der userId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUserId(String value) {
            this.userId = value;
        }

        /**
         * Gets the value of the deletedAlias property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the deletedAlias property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDeletedAlias().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the deletedAlias property.
         */
        public List<String> getDeletedAlias() {
            if (deletedAlias == null) {
                deletedAlias = new ArrayList<>();
            }
            return this.deletedAlias;
        }

        /**
         * Gets the value of the addedAlias property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the addedAlias property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddedAlias().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the addedAlias property.
         */
        public List<String> getAddedAlias() {
            if (addedAlias == null) {
                addedAlias = new ArrayList<>();
            }
            return this.addedAlias;
        }

        /**
         * Gets the value of the deletedSipURI property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the deletedSipURI property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDeletedSipURI().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the deletedSipURI property.
         */
        public List<String> getDeletedSipURI() {
            if (deletedSipURI == null) {
                deletedSipURI = new ArrayList<>();
            }
            return this.deletedSipURI;
        }

        /**
         * Gets the value of the addedSipURI property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the addedSipURI property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddedSipURI().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the addedSipURI property.
         */
        public List<String> getAddedSipURI() {
            if (addedSipURI == null) {
                addedSipURI = new ArrayList<>();
            }
            return this.addedSipURI;
        }

        /**
         * Gets the value of the deletedTelURI property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the deletedTelURI property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDeletedTelURI().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the deletedTelURI property.
         */
        public List<String> getDeletedTelURI() {
            if (deletedTelURI == null) {
                deletedTelURI = new ArrayList<>();
            }
            return this.deletedTelURI;
        }

        /**
         * Gets the value of the addedTelURI property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a {@code set} method for the addedTelURI property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddedTelURI().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         * @return
         *     The value of the addedTelURI property.
         */
        public List<String> getAddedTelURI() {
            if (addedTelURI == null) {
                addedTelURI = new ArrayList<>();
            }
            return this.addedTelURI;
        }

    }

}
