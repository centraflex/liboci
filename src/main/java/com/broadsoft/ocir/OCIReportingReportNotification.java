//
// Diese Datei wurde mit der Eclipse Implementation of JAXB, v4.0.1 generiert 
// Siehe https://eclipse-ee4j.github.io/jaxb-ri 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
//


package com.broadsoft.ocir;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.CollapsedStringAdapter;
import jakarta.xml.bind.annotation.adapters.NormalizedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Indicates to an external system that an OCI modification request (add, delete, modify, etc.) 
 *         was successfully processed.  There is no response to this command.  The userId and login type 
 *         of the account that enacted this modification are included.  The request's serialized XML 
 *         document is included in the request element.  In most cases, the serialized XML is exactly 
 *         as received over the OCI interface.  Exceptions to this rule are requests that contain media 
 *         file contents, conferencing document contents, and passwords.  If these data are altered, 
 *         the isAltered element is included to indicate so.  An identification is also included.  
 *         This takes on the form of "write" followed by a positive integer.  The number is incremented 
 *         for each successfully processed OCI modification request.
 *         If the OCI request generates an ErrorResponse of type Warning or Info, the warningMessage 
 *         element contains the ErrorResponse summary. The presence of the warningMessage element 
 *         indicates partial success. When this occurs, the request changed some data for a request but 
 *         not all as would be expected on a fully successful request. The warningMessage element 
 *         describes what might not have been completed successfully by the request.
 *       
 * 
 * <p>Java-Klasse für OCIReportingReportNotification complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>{@code
 * <complexType name="OCIReportingReportNotification">
 *   <complexContent>
 *     <extension base="{}OCIReportingNotification">
 *       <sequence>
 *         <element name="id" type="{}OCIReportingId"/>
 *         <element name="userId" type="{}OCIReportingUserId"/>
 *         <element name="loginType" type="{}OCIReportingLoginType"/>
 *         <element name="request" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         <element name="isAltered" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         <element name="warningMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       </sequence>
 *     </extension>
 *   </complexContent>
 * </complexType>
 * }</pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OCIReportingReportNotification", propOrder = {
    "id",
    "userId",
    "loginType",
    "request",
    "isAltered",
    "warningMessage"
})
public class OCIReportingReportNotification
    extends OCIReportingNotification
{

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String id;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String userId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected OCIReportingLoginType loginType;
    @XmlElement(required = true)
    protected String request;
    protected Boolean isAltered;
    protected String warningMessage;

    /**
     * Ruft den Wert der id-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Legt den Wert der id-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Ruft den Wert der userId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Legt den Wert der userId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Ruft den Wert der loginType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OCIReportingLoginType }
     *     
     */
    public OCIReportingLoginType getLoginType() {
        return loginType;
    }

    /**
     * Legt den Wert der loginType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OCIReportingLoginType }
     *     
     */
    public void setLoginType(OCIReportingLoginType value) {
        this.loginType = value;
    }

    /**
     * Ruft den Wert der request-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequest() {
        return request;
    }

    /**
     * Legt den Wert der request-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequest(String value) {
        this.request = value;
    }

    /**
     * Ruft den Wert der isAltered-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAltered() {
        return isAltered;
    }

    /**
     * Legt den Wert der isAltered-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAltered(Boolean value) {
        this.isAltered = value;
    }

    /**
     * Ruft den Wert der warningMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarningMessage() {
        return warningMessage;
    }

    /**
     * Legt den Wert der warningMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarningMessage(String value) {
        this.warningMessage = value;
    }

}
