module liboci {
	exports com.broadsoft.oci;
	exports com.broadsoft.ocir;
	exports de.qsc.broadsoft.oci;
	exports de.qsc.broadsoft.oci.coding;
	exports de.qsc.broadsoft.oci.transport;

	opens com.broadsoft.oci to jakarta.xml.bind;
	opens com.broadsoft.ocir to jakarta.xml.bind;

	requires java.rmi;
	requires java.xml;
	requires jakarta.xml.bind;
}